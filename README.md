[![build status](https://git.gorserv.ru/gorserv/workflow/badges/master/build.svg)](https://git.gorserv.ru/gorserv/workflow/commits/master)
[![coverage report](https://git.gorserv.ru/gorserv/workflow/badges/master/coverage.svg)](https://git.gorserv.ru/gorserv/workflow/commits/master)

Важно
===

 - ***Рекомендую к прочтению [CONTRIBUTING.md](CONTRIBUTING.md)***
 - Выполните эти комманды, во избежание недорозумений при работе с этим проектом.
```
git config --global core.autocrlf true
git config --global core.whitespace -trailing-space,-space-before-tab,indent-with-non-tab,-cr-at-eol
```
 - Желательно, так же, что бы Ваша IDE поддерживала или имела плагин для поддержки [EditorConfig](http://editorconfig.org/) файлов [настроек](.editorconfig).

Локальный запуск
===

### Vagrant

```
$ vagrant up --debug
```

Проект доступен по адресу: 
 - http://localhost:8080 - nginx
 - http://localhost:1080 - морда mailcatcher'a
 - localhost:3306 - mysql, снаружи `mysql -u root gerp`, изнутри `mysql -uroot -proot gerp`
 - Логи:
    - /var/log/php-error.log
    - /var/log/nginx/*

После первоначального деплоя надо развернуть дамп.

```
$ mysql -u root gerp < [backupfile.sql]
```
или
```
gunzip < [backupfile.sql.gz] | mysql -u root gerp
```
или
```
bunzip2 < [backupfile.sql.bz2] | mysql -u root gerp
```

Если что то не сработало в виртуалке:

```
...\> vagrant ssh
vagrant@gerp:~$ sudo su
root@gerp:/$ salt-call state.highstate -l debug
```

и смотрим логи.

### Docker
```
$ docker-compose up
```

Проект доступен по адресу: 
 - http://localhost:80 - nginx
 - localhost:3306 - mysql, снаружи `mysql -u root -p root wf_dev`
 - файлы базы данных ./.db/
 
см. `./docker-compose.yml`

### Демонстрация

```
\> ngrok http 8080
```
или

```
\> vagrant share --http 80
```

etc

Тесты
===

 Если php есть локально, то запускаем как обычно.
 Если локально php нет, то так:
 
```
\> vagrant ssh
vagrant@gerp:~$ WF_ENV=test /var/www/html/vendor/bin/codeception run unit
```

#### Приёмочные

```
\> vagrant ssh
vagrant@gerp:~$ php -S 127.0.0.1:9000 -t /var/www/html/web &
vagrant@gerp:~$ WF_ENV=test /var/www/html/vendor/bin/codeception run acceptance

```

Окружение
===

```
vagrant@gerp:/var/www/html$ WF_ENV=dev ./cli
```

Миграции
===
создать миграцию
```
vagrant@gerp:/var/www/html$ WF_ENV=dev ./cli mi:diff --configuration config/migrations.yml
```

применить миграции
```
vagrant@gerp:/var/www/html$ WF_ENV=dev ./cli mi:mi --configuration config/migrations.yml
```

При наличии базы, в таблице с миграциями должна быть только одна запись `20160101200000` (2016.09.20).

### Важно
Если Вы вносите изменения в базу, то так же необходиом *создавать файлы с миграцией и контролировать перенос данных в новую структуру*.
Так как каталог с миграциями находится в .gitignore, то добавить Ваше миграциюю в репозиторий можно только с ключиком --force.

Crontab
===

 TODO: технический долг

Интеграции
===

 TODO: технический долг
 
## Swagger схемы
 
  TODO: технический долг
