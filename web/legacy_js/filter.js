$(document).ready(function(){
    $(".filter input[type=reset], .filter .reset").click(function(){
        $(this).parents("form").find("input[type=text], input[type=hidden]").val('');
        $(this).parents("form").find("select").each(function(){
            $("option", $(this)).each(function() {
                $(this).removeAttr("selected");  
            });
            if (!$(this).attr("multiple")) {
                $("option:first", $(this)).attr("selected", "selected");
            }
        });
        $(this).parents("form").find("input[type='checkbox']").each(function() {
            $(this).prop('checked', false);
        });
        $("#filter_status_id").val([-1]).trigger("change");
        $(this).parents("form").submit();
        return false; 
    });
    
    $(document).on("click", ".pages a", function(){
        var a = $(this).attr("href").split('#');
        var r = new RegExp("^start-(-?[0-9]+)$");        
        var b = r.exec(a[1]);
        if(b && b[1]) {
            if(parseInt(b[1]) < 0) b[1] = 0;
            $(".filter input[name='filter_start']").val(b[1]);            
            $("form.filter").submit();
            return false;    
        }        
    });
    
    $(".filter_sort_ids [filter_sort_id]").click(function(){
        var $idx = $(".filter_sort_ids").index($(this).closest(".filter_sort_ids"));
        if($idx < 0) return;
        var $frm = $(".filter").eq($idx);
        $frm.find("select[name=filter_sort_id]").val($(this).attr("filter_sort_id"));
        $frm.submit();
        return false;
    });
});