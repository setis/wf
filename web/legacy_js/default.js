var upl = false;
var nobase;

function PlaySound(url) {
    document.all.sound.src = url;
    $(".playinie").html("<img title='Воспроизвести' src='/templates/images/play.gif' />");
}
function StopSound() {
    document.all.sound.src = '';
    $(".playinie").attr("action", "play");
}

function link2(url, nobase) {
    var b = nobase ? '' : '/';
    return b + url;
}

function goTo(url) {
    window.location.href = link2(url, nobase);
}


function trimSpaces(s) {
    s = s.replace(/(^\s*)|(\s*$)/gi, "");
    s = s.replace(/[ ]{2,}/gi, " ");
    s = s.replace(/\n /, "\n");
    return s;
}

$(document).ajaxStart(function () {
    NProgress.start();
});

$(document).ajaxError(function () {
    NProgress.remove();
});

$(document).ajaxStop(function () {
    NProgress.done();
});

function getHTMLOfSelection() {
    var range;
    if (document.selection && document.selection.createRange) {
        range = document.selection.createRange();
        return range.htmlText;
    }
    else if (window.getSelection) {
        var selection = window.getSelection();
        if (selection.rangeCount > 0) {
            range = selection.getRangeAt(0);
            var clonedSelection = range.cloneContents();
            var div = document.createElement('div');
            div.appendChild(clonedSelection);
            return div.innerHTML;
        }
        else {
            return '';
        }
    }
    else {
        return '';
    }
}

function resetSelection(){
    if (window.getSelection) {
        if (window.getSelection().empty) {  // Chrome
            window.getSelection().empty();
        } else if (window.getSelection().removeAllRanges) {  // Firefox
            window.getSelection().removeAllRanges();
        }
    } else if (document.selection) {  // IE?
        document.selection.empty();
    }
}

var quote_message = function (nickname, datetime) {
    var use_wysiswyg = !!tinyMCE;

    if (use_wysiswyg) {
        var selected = getHTMLOfSelection();

        if (!selected) {
            alert('Для цитирования следует выделить мышкой цитируемый текст из тела комментария');
            return;
        }

        var author = nickname ? '<div class="quote_author"><i>' + nickname + '</i></div>' : '';
        var date = '<div class="quote_date">' + datetime + '</div>';
        selected = '<div class="quote_quoted">' + selected + '</div>';

        var content = '<blockquote class="quote">' + author + date + selected + '</blockquote><p></p>';

        var editor = tinyMCE.activeEditor;
        editor.setContent(editor.getContent() + content);
        window.scrollTo(0, $("#cmm-txt").offset().top+$("#cmm-txt").height());
        editor.focus();
        resetSelection();


    }
    else {
        var selected = window.getSelection().toString();
        if (!selected) {
            alert('Для цитирования следует выделить мышкой цитируемый текст из тела комментария');
            return;
        }

        if (nickname != '') {
            nickname = nickname.replace('&quot;', '"');
        }

        selected = "Автор: " + nickname.replace(/<.*?>/g, '') + " Дата: " + datetime.replace(/<.*?>/g, '') + "\r\n" + selected + "\r\n";
        document.getElementById("cmm-txt").value+="-- "+selected+"--\r\n";
        document.getElementById("cmm-txt").focus();
        $("#cmm-txt").change();
        window.scrollTo(0, $("#cmm-txt").offset().top+$("#cmm-txt").height());
        resetSelection();


    }
};

function stringToDate(str) {
    var d;
    if (d = str.match(/^(\d{2,2})\.(\d{2,2})\.(\d{4,4})$/)) {
        return new Date(d[3], d[2] - 1, d[1]);
    }
    return false;
}
function dateToString(date) {
    if (typeof(date) != 'object') return false;
    else {
        var dom = date.getDate().toString();
        var month = (date.getMonth() + 1).toString();
        if (month.length == 1) month = "0" + month;
        if (dom.length == 1) dom = "0" + dom;
        return dom + '.' + month + '.' + date.getFullYear();
    }
}

function getURLParameter(name) {
    return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
}

function lockDialog(dialog) {
    $("body").append("<div class='ffholder' style='z-index:90 !important; position: absolute; top:0; left:0;height:100%;width:100%;'></div>");
    $(".ffholder").css("height", $(document).height());
}

function unLockDialog(dialog) {
    $(".ffholder").remove();
}

function is_touch_device() {
    return !!('ontouchstart' in window);
}

document.addEventListener('play', function (e) {
    var audios = document.getElementsByTagName('audio');
    for (var i = 0, len = audios.length; i < len; i++) {
        if (audios[i] != e.target) {
            audios[i].pause();
        }
    }
}, true);

var currentSelItem;
var iscall = false;


function codePrint(link) {
    var http = location.protocol;
    var slashes = http.concat("//");
    var host = slashes.concat(window.location.hostname);
    var pwa = window.open(host+link, "_new");
}

$(document).ready(function () {
    NProgress.configure({ showSpinner: false });
    $("#impersonate_select").select2();
    $("select.select2").select2();

    $(document).on("click", ".printbarcode", function () {
        var link = $(this).attr("link");
        //alert(link2(link));
        if (link) {
            if (confirm('Распечатать штрихкод?')) {
                codePrint(link2(link));
            }
        } else {
            alert("Ошибка печати!");
        }
    });


    // bootstrap multimodals

    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function () {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });
    //-------------

    $(document).on("click", ".calltotech", function () {
        if (iscall) return false;
        iscall = true;
        var callfrom = $(this).attr("callfrom");
        var callto = $(this).attr("calltoc");
        var task_id = $("input[name=task_id]").val();
        if (!task_id) {
            task_id = $(this).attr("task_id");
        }

        if (callfrom) {
            $("body").css("cursor", "wait");
            $.post(link2("callout/dispcalltech_from_rnav"), {callfrom: callfrom, destination: callto, task_id: task_id})

                .success(function (json) {
                    iscall = false;
                    try {
                        var data = $.parseJSON(json);
                        $("body").css("cursor", "default");
                        if (data["error"]) {
                            alert("Cервер: " + data.error);
                            return false;
                        } else {
                            if (data.ok) {
                                if (data.tpl) {
                                    $(".task_comments").append(data.tpl);
                                }
                                alert("Выполняется звонок c номера " + callfrom + ". \r\nПожалуйста, возьмите трубку.");
                            } else {
                                alert("Ошибка обработки ответа от сервера.");
                            }
                        }
                        return false;
                    }
                    catch (exeption) {
                        $("body").css("cursor", "default");
                        if (json.match(/login/ig)) {
                            $("body").html(json);
                        } else {
                            alert("Ошибка разбора ответа от сервера.");
                        }

                        return false;
                    }
                    return false;

                })
                .fail(function () {
                    iscall = true;
                    alert("Ошибка AJAX");
                    $("body").css("cursor", "default");
                });
        } else {
            alert("Недостаточно данных для выполнения операции");
            iscall = false;
        }


        return false;
    });

    $(document).on("click", ".calltotechrnav", function () {
        if (iscall) return false;
        iscall = true;
        var callfrom = $(this).attr("callfrom");
        var callto = $(this).attr("calltoc");
        var task_id = $("input[name=task_id]").val();
        if (!task_id) {
            task_id = $(this).attr("task_id");
        }
        ///alert(task_id + " " + callfrom +  " " + callto);

        if (callfrom && callto) {
            $("body").css("cursor", "wait");
            $.post(link2("callout/dispcalltech_from_rnav"), {callfrom: callfrom, destination: callto, task_id: task_id})

                .success(function (json) {
                    iscall = false;
                    try {
                        var data = $.parseJSON(json);
                        $("body").css("cursor", "default");
                        if (data["error"]) {
                            alert("Cервер: " + data.error);
                            return false;
                        } else {
                            if (data.ok) {
                                if (data.tpl) {
                                    $(".task_comments").append(data.tpl);
                                }
                                alert("Выполняется звонок c номера " + callfrom + ". \r\nПожалуйста, возьмите трубку.");
                            } else {
                                alert("Ошибка обработки ответа от сервера.");
                            }
                        }
                        return false;
                    }
                    catch (exeption) {
                        $("body").css("cursor", "default");
                        if (json.match(/login/ig)) {
                            $("body").html(json);
                        } else {
                            alert("Ошибка разбора ответа от сервера.");
                        }

                        return false;
                    }
                    return false;

                })
                .fail(function () {
                    iscall = true;
                    alert("Ошибка AJAX");
                    $("body").css("cursor", "default");
                });
        } else {
            alert("Недостаточно данных для выполнения операции");
            iscall = false;
        }


        return false;
    });

    $(document).on("click", ".calltotech1", function () {
        if (iscall) return false;
        iscall = true;
        var callfrom = $(this).attr("callfrom");
        var callto = $(this).attr("calltoc");
        var task_id = $("input[name=task_id]").val();
        if (!task_id) {
            task_id = $(this).attr("task_id");
        }
        ///alert(task_id + " " + callfrom +  " " + callto);

        if (callfrom) {
            $("body").css("cursor", "wait");
            $.post(link2("callout/dispcalltech1"), {callfrom: callfrom, destination: callto, task_id: task_id})

                .success(function (json) {
                    iscall = false;
                    try {
                        var data = $.parseJSON(json);
                        $("body").css("cursor", "default");
                        if (data["error"]) {
                            alert("Cервер: " + data.error);
                            return false;
                        } else {
                            if (data.ok) {
                                if (data.tpl) {
                                    $(".task_comments").append(data.tpl);
                                }
                                alert("Выполняется звонок c номера " + callfrom + ". \r\nПожалуйста, возьмите трубку.");
                            } else {
                                alert("Ошибка обработки ответа от сервера.");
                            }
                        }
                        return false;
                    }
                    catch (exeption) {
                        $("body").css("cursor", "default");
                        if (json.match(/login/ig)) {
                            $("body").html(json);
                        } else {
                            alert("Ошибка разбора ответа от сервера.");
                        }

                        return false;
                    }
                    return false;

                })
                .fail(function () {
                    iscall = true;
                    alert("Ошибка AJAX");
                    $("body").css("cursor", "default");
                });
        } else {
            alert("Недостаточно данных для выполнения операции");
            iscall = false;
        }


        return false;
    });


    $(document).on("click", ".playinie", function () {
        if ($(this).attr("action") == "stop") {
            StopSound();
            $(this).attr("action", "play");
            $(this).html("<img title='Воспроизвести' src='/templates/images/play.gif' />");
        } else {
            $(this).attr("action", "stop");
            PlaySound($(this).attr("src"));
            $(this).html("<img title='Остановить' src='/templates/images/stop.gif' />");
        }
    });

    $(document).on("click", "#cancel_calls", function () {
        $("#callticketlist").modal('hide');
        StopSound();
        var audios = document.getElementsByTagName('audio');
        for (var i = 0, len = audios.length; i < len; i++) {
            audios[i].pause();
            audios[i].currentTime = 0;
        }
    });

    $(window).scroll(function () {
        if ($(this).scrollTop()) {
            $("#toTop").fadeIn(250);
        } else {
            $("#toTop").fadeOut(250);
        }
    });
    $("#toTop").hide();

    $("#toTop").click(function () {
        $("html, body").animate({scrollTop: 0}, 400);
    });

    $(document).on('hidden.bs.modal', '#callticketlist', function () {
        StopSound();
        var audios = document.getElementsByTagName('audio');
        for (var i = 0, len = audios.length; i < len; i++) {
            audios[i].pause();
            audios[i].currentTime = 0;
        }
        //$('.modal:visible').length && $(document.body).addClass('modal-open');
    });

    $(document).on("click", "#callticket", function () {
        $("body").css("cursor", "wait");
        $.post(link2("callviewer/getrecordsbytask_id"), {task_id: $("input[name='task_id']").val()})
            .success(function (json) {
                try {
                    var data = $.parseJSON(json);
                    $("body").css("cursor", "default");
                    if (data["error"]) {
                        alert("Cервер: " + data.error);
                        return false;
                    } else {
                        if (data.ok) {

                            if (data.tpl) {

                                $(".calllist").html(data.tpl);

                                $("#callticketlist").modal('show');
                            }

                        } else {
                            alert("Ошибка обработки ответа от сервера.");
                        }
                    }
                    return false;
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert("Ошибка разбора ответа от сервера.");
                    }

                    return false;
                }
                return false;

            })
            .fail(function () {
                alert("Ошибка AJAX");
                $("body").css("cursor", "default");
            });


    });
    $(document).on("click", "#refreshcallticket", function () {
        //$( "#callticketlist" ).modal('hide');
        $("body").css("cursor", "wait");
        $.post(link2("callviewer/refresh_ticketcalls_ajax"), {task_id: $("input[name='task_id']").val()})
            .success(function (json) {
                try {
                    var data = $.parseJSON(json);
                    $("body").css("cursor", "default");
                    if (data["error"]) {
                        alert("Cервер: " + data.error);
                        return false;
                    } else {
                        if (data.ok) {

                            if (data.tpl) {

                                $(".calllist").html(data.tpl);

                                //$( "#callticketlist" ).modal('show');
                            }

                        } else {
                            alert("Ошибка обработки ответа от сервера.");
                        }
                    }
                    return false;
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert("Ошибка разбора ответа от сервера.");
                    }

                    return false;
                }
                return false;

            })
            .fail(function () {
                alert("Ошибка AJAX");
                $("body").css("cursor", "default");
            });


    });

    $(document).on("click", "button[name=callto], .callfromgfx", function () {
        var link = "";
        var callfrom = $(this).attr("callfrom");
        var callto = $(this).attr("callto");
        var task_id = $("input[name=task_id]").val();
        if (callfrom && callto) {
            $("body").css("cursor", "wait");
            if (callfrom.toString().length == 4) {
                link = "callout/dispcalltech";
            } else {
                link = "callout/dispcall";
            }
            $.post(link2(link), {destination: callto, task_id: task_id})

                .success(function (json) {

                    try {
                        var data = $.parseJSON(json);
                        $("body").css("cursor", "default");
                        if (data["error"]) {
                            alert("Cервер: " + data.error);
                            return false;
                        } else {
                            if (data.ok) {
                                if (data.tpl) {
                                    $(".task_comments").append(data.tpl);
                                }
                                alert("Выполняется звонок c номера " + callfrom + " на " + callto + " \r\nПожалуйста, возьмите трубку.");
                            } else {
                                alert("Ошибка обработки ответа от сервера.");
                            }
                        }
                        return false;
                    }
                    catch (exeption) {
                        $("body").css("cursor", "default");
                        if (json.match(/login/ig)) {
                            $("body").html(json);
                        } else {
                            alert("Ошибка разбора ответа от сервера.");
                        }

                        return false;
                    }
                    return false;

                })
                .fail(function () {
                    alert("Ошибка AJAX");
                    $("body").css("cursor", "default");
                });
        } else {
            alert("Недостаточно данных для выполнения операции.");
        }


        return false;
    });

    $("#container-paddinger").css("padding-top", ($("#header").height()) + "px");
    $(window).resize(function () {
        $("#container-paddinger").css("padding-top", ($("#header").height()) + "px");
        if ($(".navbar-toggle").attr("aria-expanded") == "true") {
            $(".navbar-toggle").click();
        }
    });

    $("table.t1 tbody tr:not(.int):odd").addClass("odd");
    $("table.t1 tbody tr:not(.int):even").addClass("even");

    $("[confirm]").click(function () {
        if (!confirm($(this).attr("confirm"))) return false;
    });

    $("form").filter(function () {
        return $(this).find(":input[required]").length;
    }).submit(function () {
        var $empty = $(this).find(":input[required]").filter(function () {
            return $.trim($(this).val()) ? false : true;
        });
        if ($empty.length) {
            alert("Заполните все необходимые поля!" + $empty.eq(0).prop("name"));
            $empty.eq(0).focus();
            return false;
        }
    });

    var tpl = $("input[name=user_template]").val();

    if (tpl != "deafult") {
        $(".selected_lm").parents("ul.closed").removeClass("closed");
    }
    $("input[name=seluser]").click(function () {
        selUser(function () {
            //alert('ad');
        });
    });

    $(document).on("click", '.ffholder', function () {
        unLockDialog();
        $(".floatingform").hide();
        $(".floatingform1").hide();
        $("body").css("cursor", "default");
        currentTargetStep = 1;
    });

});


function htmlspecialchars_decode(string, quote_style) {

    var optTemp = 0,
        i = 0,
        noquotes = false;
    if (typeof quote_style === 'undefined') {
        quote_style = 2;
    }
    string = string.toString()
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>');
    var OPTS = {
        'ENT_NOQUOTES': 0,
        'ENT_HTML_QUOTE_SINGLE': 1,
        'ENT_HTML_QUOTE_DOUBLE': 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE': 4
    };
    if (quote_style === 0) {
        noquotes = true;
    }
    if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
        quote_style = [].concat(quote_style);
        for (i = 0; i < quote_style.length; i++) {
            // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
            if (OPTS[quote_style[i]] === 0) {
                noquotes = true;
            } else if (OPTS[quote_style[i]]) {
                optTemp = optTemp | OPTS[quote_style[i]];
            }
        }
        quote_style = optTemp;
    }
    if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
        string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
        // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
    }
    if (!noquotes) {
        string = string.replace(/&quot;/g, '"');
    }
    // Put this in last place to avoid escape being double-decoded
    string = string.replace(/&amp;/g, '&');

    return string;
}

(function ($) {

    $('.quote_text').on('click', quote_message);

    if ($.fn.style) {
        return;
    }

    // Escape regex chars with \
    var escape = function (text) {
        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    };

    // For those who need them (< IE 9), add support for CSS functions
    var isStyleFuncSupported = !!CSSStyleDeclaration.prototype.getPropertyValue;
    if (!isStyleFuncSupported) {
        CSSStyleDeclaration.prototype.getPropertyValue = function (a) {
            return this.getAttribute(a);
        };
        CSSStyleDeclaration.prototype.setProperty = function (styleName, value, priority) {
            this.setAttribute(styleName, value);
            var priority = typeof priority != 'undefined' ? priority : '';
            if (priority != '') {
                // Add priority manually
                var rule = new RegExp(escape(styleName) + '\\s*:\\s*' + escape(value) +
                    '(\\s*;)?', 'gmi');
                this.cssText =
                    this.cssText.replace(rule, styleName + ': ' + value + ' !' + priority + ';');
            }
        };
        CSSStyleDeclaration.prototype.removeProperty = function (a) {
            return this.removeAttribute(a);
        };
        CSSStyleDeclaration.prototype.getPropertyPriority = function (styleName) {
            var rule = new RegExp(escape(styleName) + '\\s*:\\s*[^\\s]*\\s*!important(\\s*;)?',
                'gmi');
            return rule.test(this.cssText) ? 'important' : '';
        }
    }

    // The style function
    $.fn.style = function (styleName, value, priority) {
        // DOM node
        var node = this.get(0);
        // Ensure we have a DOM node
        if (typeof node == 'undefined') {
            return this;
        }
        // CSSStyleDeclaration
        var style = this.get(0).style;
        // Getter/Setter
        if (typeof styleName != 'undefined') {
            if (typeof value != 'undefined') {
                // Set style property
                priority = typeof priority != 'undefined' ? priority : '';
                style.setProperty(styleName, value, priority);
                return this;
            } else {
                // Get style property
                return style.getPropertyValue(styleName);
            }
        } else {
            // Get CSSStyleDeclaration
            return style;
        }
    };
})(jQuery);






