$(document).ready(function() {
    $("#opros").click(function() {
        var task_id = $(this).attr("task_id");
        var plugin_uid = $(this).attr("plugin_uid");
        var current_status = $(this).attr("current_status");
        var force_reopen = $(this).attr("force_reopen");
        var obj = $(this);
        $('body').css('cursor', 'default'); 
        
        $("body").css("cursor", "wait");        
        $.post(link2("qc/loadoprosform_ajax"), {task_id: task_id, plugin_uid: plugin_uid, current_status: current_status, force_reopen: force_reopen})
        .success(function(json) {
            
            try 
            {
                var data = $.parseJSON( json );
                $("body").css("cursor", "default");
                if (data["error"]) {
                    alert("Cервер: " + data.error);
                    return false;
                } else {
                    $("#oprosbody").html(data.tpl);
                    $("input[name=delaytotime]").maskex("00:00", {reverse: true});
                    $(".opr_ammounts").maskex("###000.00", {reverse: true});
                    if ($(".noitems").length>0) {
                        $("#gosaveopros").hide();
                    } else {
                        $("#gosaveopros").show();                        
                    }
                     
                    $.date_input.initialize();
                    $("#obzvonform").modal('show');
                }
                return false;                       
            }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert("Ошибка разбора ответа сервера: " +  exeption);    
                    }
                    
                    return false;
                }
            return false;
            
        })
        .fail(function() {
            alert("Ошибка AJAX");
            $("body").css("cursor", "default");
        });
        
        return false;
        
    });
    
    $(document).on("keyup", "input[name='oproscomment']", function() {
        $("input[name='oproscomment1']").val($(this).val());
    });
    $(document).on("keyup", "input[name='oproscomment1']", function() {
        $("input[name='oproscomment']").val($(this).val());
    });
    
    $(document).on("click",".radioitem", function() {
         var result = 0;
         $("input[type='radio']:checked").each(function() {
            var d = $(this).attr("oval");
            if (d) {
                result = parseInt(result) + parseInt(d);
            }
         });
         $("input[type='text']").each(function() {
            if ($(this).attr("maxval")>0 && $(this).val()!="") {
                result = parseInt(result) + parseInt($(this).val());
            }
         });
          
         $(".oprresult").html(result);
    });
    
    $("#cancel_opros").click(function() {
        $("#obzvonform").modal('hide');
         
    });
    
    $(document).on("keydown", ".oprosnumonly",  function(e) {
         // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        

    });
    
    $(document).on("blur", ".oprosnumonly",  function(e) {
        if (parseInt($(this).attr("maxval"))>0) {
            if (parseInt($(this).val())>parseInt($(this).attr("maxval"))) {
                $(this).val($(this).attr("maxval"));
            }
        }
        if ($(this).val() == 0) {
            $(this).val("1");
        }
        var result = 0;
         $("input[type='radio']:checked").each(function() {
            var d = $(this).attr("oval");
            if (d) {
                result = parseInt(result) + parseInt(d);
            }
         });
         $("input[type='text']").each(function() {
            if ($(this).attr("maxval")>0 && $(this).val()!="") {
                result = parseInt(result) + parseInt($(this).val());
            }
         });
         $(".oprresult").html(result);
    });
    
    $("#gosaveopros").click(function() {
        var radioerror = false;
        var texterror = false;
        var firstRadioObj = false;
        var firstTextObj = false;
        var alertmsg = "";
        $("#oprosform input[type=radio]").each(function() {
            if ($(this).attr("required") == "required" && !$("input[name='"+$(this).attr("name")+"']:checked").val()) {
                if (!radioerror) {
                    firstRadioObj = $(this);
                }
                radioerror = true;
                
            }
        });
        
        if (radioerror) {
            alertmsg = " выберите значения Да/Нет";
            if (firstRadioObj) {
                firstRadioObj.focus();
            }
        }
        $("#oprosform input[type=text]").each(function() {
            if ($(this).attr("required") == "required" && $(this).val() == "") {
                if (!texterror) {
                    firstTextObj = $(this);
                }
                texterror = true;
                
            }
        });
        
        if (texterror) {
            alertmsg = alertmsg !=""? alertmsg +  ", заполните все(!) текстовые поля!" : " заполните все(!) текстовые поля в таблице опроса!";
            if (firstTextObj) {
                firstTextObj.focus();
            }
        }
        if (texterror || radioerror) {
            alert("Пожалуйста, выполните следующие действия: " + alertmsg);
        } else {
            $("#oprosform").submit();
        }
        return false;
    });
    
    $(document).on("submit", "#oprosform", function() {
        
        
        $.post(link2("qc/saveoprosform_ajax"), $(this).serializeArray())
            .success(function(json) {
                
                try 
                {
                    var data = $.parseJSON( json );
                    $("body").css("cursor", "default");
                    if (data["error"]) {
                        alert("Cервер: " + data.error);
                        return false;
                    } else {
                        if (data.ok) {
                            if (data.tpl) {
                                $(".task_comments").append(data.tpl);
                            }
                            alert("Данные обзвона сохранены!");
                            $("#obzvonform").modal('hide');
                             
                            
                            
                            return false;
                        }
                    }
                    return false;                       
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert("Ошибка разбора ответа сервера: " +  exeption);    
                    }
                        
                    return false;
                }
                return false;
                
            })
            .fail(function() {
                alert("Ошибка AJAX");
                $("body").css("cursor", "default");
            });
            
        
        
        return false; 
    });
    
    $(document).on("click", "button[name=clientreject]", function() {
        if ($("input[name=oproscomment1]").val() == "") {
            alert("Пожалуйста, напишите комментарий!");
            return false;
        } else {
            var task_id = $("input[name='task_id']").val();
            $.post(link2("qc/loadreject_ajax"), {task_id: task_id, cmm: $("input[name=oproscomment1]").val()})
            .success(function(json) {
                try 
                {
                    var data = $.parseJSON( json );
                    $("body").css("cursor", "default");
                    if (data["error"]) {
                        alert("Cервер: " + data.error);
                        return false;
                    } else {
                        if (data.ok) {
                            if (data.tpl) {
                                $(".task_comments").append(data.tpl);
                            }
                            alert("Отказ от опроса зафиксирован!");
                            $("#obzvonform").modal('hide');
                             
                            
                            return false;
                        }
                    }
                    return false;                       
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert("Ошибка разбора ответа сервера: " +  exeption);    
                    }
                        
                    return false;
                }
                return false;
                
            })
            .fail(function() {
                alert("Ошибка AJAX");
                $("body").css("cursor", "default");
            });
            
        }
    });
    
    $(document).on("click", "button[name=clientdelay]", function() {
        if ($("input[name=oproscomment1]").val() == "" || $("input[name=delayto]").val() == "" || $("input[name=delaytotime]").val() == "") {
            alert("Пожалуйста, укажите дату/время, напишите комментарий!");
            return false;
        } else {
            var task_id = $("input[name='task_id']").val();
            $.post(link2("qc/loaddelay_ajax"), {task_id: task_id, cmm: $("input[name=oproscomment1]").val(), delayto: $("input[name=delayto]").val(), delaytotime: $("input[name=delaytotime]").val()})
            .success(function(json) {
                try 
                {
                    var data = $.parseJSON( json );
                    $("body").css("cursor", "default");
                    if (data["error"]) {
                        alert("Cервер: " + data.error);
                        return false;
                    } else {
                        if (data.ok) {
                            if (data.tpl) {
                                $(".task_comments").append(data.tpl);
                            }
                            alert("Перенос опроса зафиксирован!");
                            $("#obzvonform").modal('hide');
                             
                            
                            return false;
                        }
                    }
                    return false;                       
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert("Ошибка разбора ответа сервера: " +  exeption);    
                    }
                        
                    return false;
                }
                return false;
                
            })
            .fail(function() {
                alert("Ошибка AJAX");
                $("body").css("cursor", "default");
            });
            
        }
    });
    
});

