var suggestion = (function () {

    return {

        owner: '',

        init: function (param) {
            var opt = {
                serviceUrl: GLOB_ADDRESS_RECOGN_URL,
                token: GLOB_ADDRESS_TOKEN,
                selector: '.suggest-addr_full',

                ScOdsUrl: '/address/get-ticket-geo-data-by-point',
                AreaUrl: '/address/get-ticket-district',

                selectorFull: '.suggest-addr_full',
                selectorBounds: '.suggest-addr_bounds',
                selectorBoundsRegion: '.suggest-addr_bounds_region',
                selectorBoundsCity: '.suggest-addr_bounds_city',
                selectorBoundsStreet: '.suggest-addr_bounds_street',
                selectorBoundsHouse: '.suggest-addr_bounds_house',

                selectorGP: 'suggest-addr_gp',

                selectorBox: '.suggest-addr_dlg',
                classInsertDataToDOM: 'suggest-data-repository',

                selectorSCselect: 'select#sc_id',
                selectorAreaSelect: 'select#area_id',

                type: 'ADDRESS',
                count: 10,
                deferRequestBy: 800,
                minChars: 3,
                mobileWidth: 400,
                timeout: 5000,
                recognisedAction: true,
                unRecognisedAction: false,
                createAddrDlgSelector: '.suggest-addr_dlg',
                isNotRecognisedSelector: '[name=is_not_recognised]'
            };

            opt = $.merge(opt, param);

            this.implementationSripts(opt);
        },

        implementationSripts: function (opt) {
            var self = this;
            var style = document.createElement('link');
            style.href = "/templates/twozero/css/suggest_address.css";
            style.type = "text/css";
            style.rel = "stylesheet";

            var script_1 = document.createElement('script');
            script_1.src = "/legacy_js/suggestion/jquery.suggestions.min.js";

            var script_2 = document.createElement('script');
            script_2.src = "/legacy_js/suggestion/jquery.xdomainrequest.min.js";

            document.body.appendChild(style);
            if (navigator.userAgent.indexOf('IE 10') >= 0) {
                document.body.appendChild(script_2);
            }
            document.body.appendChild(script_1);

            script_1.onload = function () {
                self.suggestFull(opt);
                self.suggestBoundsRegion(opt);
                self.suggestBoundsCity(opt);
                self.suggestBoundsStreet(opt);
                self.suggestBoundsHouse(opt);
            }
        },

        suggestFull: function (opt, target) {
            var self = this;

            self.insertStatusBlock(opt.selectorFull);

            $(opt.selectorFull).suggestions({
                serviceUrl: opt.serviceUrl,
                token: opt.token,
                type: opt.type,
                count: opt.count,
                deferRequestBy: opt.deferRequestBy,
                minChars: opt.minChars,
                mobileWidth: opt.mobileWidth,
                timeout: opt.timeout,
                onSelect: function (suggestion) {
                    var parent = this;
                    self.doControlRequest(suggestion, opt).success(function(result) {
                        result = result.suggestions[0].data;
                        self.insertDataToDOM(result, opt, parent);
                        self.LegacyIntegration(result);
                        self.selectActions(parent, opt);
                        self.isNotRecognisedFalse(opt, target);
                        self.getSCandODS(result.geo_lat, result.geo_lon, opt, parent);
                        self.getArea(result.city_district, opt, parent);
                        self.insertChangeMarkerToDOM(opt, parent);
                    });
                },
                onInvalidateSelection: function () {
                    self.isNotRecognisedTrue(opt, target);
                }
            });
        },

        doControlRequest: function(variant, opt) {
            return $.post(opt.serviceUrl + 'suggest/address', {count: 1, query: variant.value});
        },

        insertStatusBlock: function (selector) {

            var elem = '<div class="suggest-status suggest-status_regular">Автоматическое распознавание адреса</div>';
            $(selector).after(elem);

        },

        suggestBoundsRegion: function (opt) {
            var self = this;

            $(opt.selectorBoundsRegion).suggestions({
                serviceUrl: opt.serviceUrl,
                token: opt.token,
                type: opt.type,
                hint: false,
                bounds: 'region-area',
                onSelect: function (suggestion) {
                    self.LegacyIntegration(suggestion.data);
                    self.isNotRecognisedFalse(opt);
                },
                onInvalidateSelection: function () {
                    self.isNotRecognisedTrue(opt);
                }
            });

        },

        suggestBoundsCity: function (opt) {
            var self = this;

            $(opt.selectorBoundsCity).suggestions({
                serviceUrl: opt.serviceUrl,
                token: opt.token,
                type: opt.type,
                hint: false,
                bounds: 'city-settlement',
                constraints: $(opt.selectorBoundsRegion),
                onSelect: function (suggestion) {
                    self.LegacyIntegration(suggestion.data);
                    self.isNotRecognisedFalse(opt);
                },
                onInvalidateSelection: function () {
                    self.isNotRecognisedTrue(opt);
                }
            });

        },

        suggestBoundsStreet: function (opt) {
            var self = this;

            $(opt.selectorBoundsStreet).suggestions({
                serviceUrl: opt.serviceUrl,
                token: opt.token,
                type: opt.type,
                hint: false,
                bounds: 'street',
                constraints: $(opt.selectorBoundsCity),
                onSelect: function (suggestion) {
                    self.LegacyIntegration(suggestion.data);
                    self.isNotRecognisedFalse(opt);
                },
                onInvalidateSelection: function () {
                    self.isNotRecognisedTrue(opt);
                }
            });

        },

        suggestBoundsHouse: function (opt) {
            var self = this;

            $(opt.selectorBoundsHouse).suggestions({
                serviceUrl: opt.serviceUrl,
                token: opt.token,
                type: opt.type,
                hint: false,
                bounds: 'house',
                constraints: $(opt.selectorBoundsStreet),
                onSelect: function (suggestion) {
                    self.LegacyIntegration(suggestion.data);
                    self.isNotRecognisedFalse(opt);
                },
                onInvalidateSelection: function () {
                    self.isNotRecognisedTrue(opt);
                }
            });

        },

        isNotRecognisedTrue: function (opt, target) {
            var s = opt.selectorFull + '+ .' + opt.classInsertDataToDOM,
                elem = $(s);
            if (elem.length == 0) {
                $(target).after('<div class="' + opt.classInsertDataToDOM + '" style="display:none"></div>');
                elem = $(s);
            }

            elem.data({
                is_not_recognized: 1
            });

            $(opt.isNotRecognisedSelector).val('1');
        },

        isNotRecognisedFalse: function (opt, target) {
            var s = opt.selectorFull + '+ .' + opt.classInsertDataToDOM,
                elem = $(s);
            if (elem.length == 0) {
                $(target).after('<div class="' + opt.classInsertDataToDOM + '" style="display:none"></div>');
                elem = $(s);
            }

            elem.data({
                is_not_recognized: 0
            });

            $(opt.isNotRecognisedSelector).val('0');
        },

        LegacyIntegration: function (data) {

            $('[name=kladr]').val(data.kladr_id);
            $('[name=house]').val((data.house ? (data.house_type + data.house) : '-') + (data.block ? (data.block_type + data.block) : ''));

            $('[name=latitude]').val(data.geo_lat);
            $('[name=longitude]').val(data.geo_lon);
        },

        insertDataToDOM: function (data, opt, target) {
            var s = opt.selectorFull + '+ .' + opt.classInsertDataToDOM,
                elem = $(s);

            if (elem.length == 0) {
                $(target).after('<div class="' + opt.classInsertDataToDOM + '" style="display:none"></div>');
                elem = $(s);
            }

            elem.data({
                full: $(opt.selectorFull).val(),
                kladr: data.kladr_id,
                house: (data.house ? (data.house_type + data.house) : '-') + (data.block ? (data.block_type + data.block) : ''),
                latitude: data.geo_lat,
                longitude: data.geo_lon
            });
        },

        insertSCtoDOM: function (sc_id, opt, target) {
            var s = opt.selectorFull + '+ .' + opt.classInsertDataToDOM,
                elem = $(s);

            if (elem.length == 0) {
                $(target).after('<div class="' + opt.classInsertDataToDOM + '" style="display:none"></div>');
                elem = $(s);
            }

            elem.data({
                sc_id: sc_id
            });
        },

        insertAreatoDOM: function (area_id, opt, target) {
            var s = opt.selectorFull + '+ .' + opt.classInsertDataToDOM,
                elem = $(s);

            if (elem.length == 0) {
                $(target).after('<div class="' + opt.classInsertDataToDOM + '" style="display:none"></div>');
                elem = $(s);
            }

            elem.data({
                area_id: area_id
            });
        },

        insertChangeMarkerToDOM: function (opt, target) {
            var s = opt.selectorFull + '+ .' + opt.classInsertDataToDOM,
                elem = $(s);

            if (elem.length == 0) {
                $(target).after('<div class="' + opt.classInsertDataToDOM + '" style="display:none"></div>');
                elem = $(s);
            }

            elem.data({
                changeMarker: 1
            });
        },

        selectActions: function (self, opt) {
            if (opt.recognisedAction) {
                $(self).removeClass('suggest-unrecognised');
                $(self).addClass('suggest-recognised');

                var suggest_status = $(self).nextAll('.suggest-status');
                suggest_status.removeClass('suggest-status_regular');
                suggest_status.removeClass('suggest-status_fail');
                suggest_status.addClass('suggest-status_success');
                suggest_status.html('<span class="glyphicon glyphicon-ok"></span> Адрес распознан.');
            }
        },

        getSCandODS: function (lat, lon, opt, target) {
            var self = this;
            if (!lat || !lon) {
                return;
            }
            $.post(
                opt.ScOdsUrl,
                {
                    latitude: lat,
                    longitude: lon
                },
                function (data) {
                    var sc_box = $(opt.selectorSCselect),
                        sc_id = data['service_center'][0] ? data['service_center'][0]['id'] : 0;

                    if (sc_box && sc_id > 0) {

                        // for GP
                        if ($(target).hasClass(opt.selectorGP)) {

                            // if ticket has 0 addresses yet
                            if (!$('[data-order_id]').length) {
                                if (!$(sc_box).val(sc_id))
                                    $(sc_box).select2().val(sc_id).trigger('change');
                                if ($('#sc_status').length == 0) {
                                    $(sc_box).parent().append('' +
                                        '<div class="suggest-status_success" id="sc_status">' +
                                        '<span class="glyphicon glyphicon-ok"> </span>' +
                                        '<small> Определен автоматически</small>' +
                                        '</div>'
                                    );
                                }
                            }

                        }
                        else if ($(sc_box).val(sc_id)) {
                            $(sc_box).select2().val(sc_id).trigger('change');
                            if ($('#sc_status').length == 0) {
                                $(sc_box).parent().append('' +
                                    '<div class="suggest-status_success" id="sc_status">' +
                                    '<span class="glyphicon glyphicon-ok"> </span>' +
                                    '<small> Определен автоматически</small>' +
                                    '</div>'
                                );
                            }
                        }
                        sc_box.change(function () {
                            if ($('#sc_status').length > 0) {
                                $('#sc_status').remove();
                            }
                        });
                    }

                    self.insertSCtoDOM(sc_id, opt, target);
                }
            );
        },

        getArea: function (district, opt, target) {
            var self = this;
            if (!district) {
                return;
            }
            $.post(
                opt.AreaUrl,
                {
                    district: district
                },
                function (data) {

                    var area = data.area[0] ? data.area[0] : 0;

                    var area_box = $(opt.selectorAreaSelect),
                        area_id = area['id'];

                    if (area_box && area != 0) {
                        $(area_box).select2().val(area_id).trigger('change');
                        // $(area_box).prop('disabled', true);

                        if ($('#area_status').length == 0) {
                            $(area_box).parent().append('' +
                                '<div class="suggest-status_success" id="area_status">' +
                                '<span class="glyphicon glyphicon-ok"> </span>' +
                                '<small> Определен автоматически</small>' +
                                '</div>'
                            );
                        }

                        area_box.change(function () {
                            if ($('#area_status').length > 0) {
                                $('#area_status').remove();
                            }
                        });
                    }

                    else {
                        // $(area_box).prop('disabled', true);
                    }

                    if (area == 0) {
                        $(area_box).val(0);
                        $('#area_status').remove();
                        $(area_box).prop('disabled', false);
                    }


                    self.insertAreatoDOM(area['id'], opt, target);
                }
            );
        }

    }
})();

$(document).ready(function () {
    suggestion.init({});
    ScAndAreaInputsMind();
});


// Utils

function insertFormElements() {
    // Insert elements which necessary for POST-sanding data

    var str =
            '<!--API data via POST-->' +
            '<input type="hidden" name="kladr" value="">' +
            '<input type="hidden" name="house" value="">' +
            '<input type="hidden" name="latitude" value="">' +
            '<input type="hidden" name="longitude" value="">' +
            '<input type="hidden" name="is_not_recognised" value="0">' +
            '<!--Legacy compatibility-->' +
            '<input type="hidden" value="7700000000000" name="current_kladr_code" />'
        ;

    document.write(str);
}


function ScAndAreaInputsMind() {

    $(document).ready(function () {
        $('.suggest-addr_full').blur(function () {
            var key = $('.suggest-data-repository').data('is_not_recognized');

            if (key == 1 || key == undefined) {

                $('[name=sc_id]').val('-1');
                $('[name=area_id]').val(0);

                $('[name=area_id]').prop('disabled', false);
                if ($('#area_status').length > 0) {
                    $('#area_status').remove();
                }
                if ($('#sc_status').length > 0) {
                    $('#sc_status').remove();
                }

                $('[name=area_id]').prop('disabled', false);
            }
        });

    });
}























