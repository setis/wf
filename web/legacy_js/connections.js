var backup = false;
var b_code, b_region_title, b_street, b_althouse, b_pod, b_etazh, b_kv, b_domofon, b_k_region, b_k_area, b_k_city, b_k_np, b_k_house, b_area_id;

$(function () {
    var on = false;
    window.setInterval(function () {
        on = !on;
        if (on) {
            $('.invalid').addClass('invalid-blink')
        } else {
            $('.invalid-blink').removeClass('invalid-blink')
        }
    }, 500);
});
var on = false;

$(document).ready(function () {

    $(".floatingform").hide();
    $(".floatingform1").hide();

    $(function () {

        window.setInterval(function () {
            on = !on;
            if (on) {
                $('.invalid').addClass('invalid-blink')
            } else {
                $('.invalid-blink').removeClass('invalid-blink')
            }
        }, 500);
    });

    $.date_input.initialize();

    $("#save_change_contr").click(function () {
        var contr = $("#cnt_id").val();
        var type_id = $("#type_id").val();
        var agr_id = $("#agr_id").val();
        var task_id = $("input[name=task_id]").val();
        if (contr && type_id && agr_id && task_id) {
            $("body").css("cursor", "wait");
            $.post(link2('connections/updateContrDataA'), {
                task_id: task_id,
                contr_id: contr,
                type_id: type_id,
                agr_id: agr_id
            })
                .success(function (data) {
                    if (data == "ok") {
                        if ((contr == "36" || contr == "791") && (type_id == "20")) {
                            $(".mgts_addons").removeClass('hidden');
                            $(".mgts_addons").show();
                            setMGTSRequired();
                            $(".mgts_addons_adsl").hide();
                            dropMGTSADSLRequired();
                        } else {

                            if (contr == "36" && (type_id == "37")) {
                                $(".mgts_addons_adsl").removeClass('hidden');
                                $(".mgts_addons_adsl").show();
                                setMGTSADSLRequired();
                            } else {
                                $(".mgts_addons_adsl").hide();
                                dropMGTSADSLRequired();
                            }

                            $(".mgts_addons").hide();
                            dropMGTSRequired();
                        }
                        tComment = "";
                        if ($("#curr_contr_val").html() != $("#cnt_id option:selected").text()) {
                            tComment = "Контрагент: " + $("#curr_contr_val").html() + " -> " + $("#cnt_id option:selected").text() + "\r\n";
                        }
                        if ($("#curr_work_val").html() != $("#type_id option:selected").text()) {
                            tComment += "Вид работ: " + $("#curr_work_val").html() + " -> " + $("#type_id option:selected").text() + "\r\n";
                        }
                        if ($("#curr_agr_val").html() != $("#agr_id option:selected").text()) {
                            tComment += "Договор: " + $("#curr_agr_val").html() + " -> " + $("#agr_id option:selected").text();
                        }
                        if (tComment) {
                            $("#curr_contr_val").html($("#cnt_id option:selected").text());
                            $("#curr_work_val").html($("#type_id option:selected").text());
                            $("#curr_agr_val").html($("#agr_id option:selected").text());

                            $("#task_comment_loader").show();
                            $.post("/comments/addcmm_ajax/", {
                                task_id: task_id,
                                cmm_text: tComment,
                                tag: "Обновлены данные"
                            }, function (data) {
                                $("#task_comment_loader").hide();
                                if (data) {

                                    $(".task_comments").append(data);

                                } else {
                                    alert("Ошибка добавления комментария!");
                                }
                                $("#gochangecontr").modal('hide');
                                $("body").css("cursor", "default");
                                alert('Изменения сохранены!');
                                location.reload();

                            });
                        } else {
                            alert("Изменения не внесены!");
                            $("body").css("cursor", "default");
                            $("#gochangecontr").modal('hide');
                        }


                    } else {
                        $("body").css("cursor", "default");
                        alert('Ошибка обновления информации о Контрагенте! Обратитесь к разработчикам!');
                    }

                    return false;
                })
                .fail(function () {
                    alert('Ошибка обмена данными с сервером. Обратитесь к разработчикам!');
                    $("body").css("cursor", "default");
                    $("#gochangecontr").modal('hide');
                });
        } else {
            alert("Пожалуйста, выберите Контрагента, Вид работ и Договор!");
        }
    });


    $("#go_edit_ods").click(function () {


        $("body").css("cursor", "wait");
        $.post(link2("adm_ods/getODSParams"), {ods_id: $("input[name=ods_id]").val()})
            .success(function (data) {
                if (data == "error") {
                    alert('Ошибка получение ОДС! Обратитесь к разработчикам!');
                } else {

                    var ods = data.split("^");
                    $("input[name=ods_name]").val(ods[1]);
                    $("input[name=ods_addr]").val(ods[4]);
                    $("input[name=ods_tel]").val(ods[3]);
                    $("input[name=ods_contact]").val(ods[2]);
                    $("textarea[name=ods_comment]").val(ods[5]);
                    $("#goeditods").modal('show');
                    $("input[name=ods_name]").focus();
                }
                $("body").css("cursor", "default");

            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером!');
                $("body").css("cursor", "default");
            });

        return false;
    });

    $("#go_select_ods").click(function () {

        if ($("input[name=dom_id]").val() == "" || $("input[name=dom_id]").val() == "0") {
            alert("Для выбора ОДС необходимо указать адрес в формате системы! Для выбора адреса, пожалуйста, нажмите кнопку Изменить в подразделе Адрес.");
            return false;
        }

        $("body").css("cursor", "wait");
        $.post(link2("adm_ods/getODSListA"), {ods_id: $("input[name=ods_id]").val()})
            .success(function (data) {
                if (data == "error") {
                    alert('Ошибка получения ОДС! Обратитесь к разработчикам!');
                } else {
                    $("#newodsid").html(data);
                    $("#gochangeods").modal('show');
                    $("input[name=ods_name]").focus();
                }
                $("body").css("cursor", "default");

            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером!');
                $("body").css("cursor", "default");

            });

        return false;
    });

    $("#cancel_ods").click(function () {

        $("#goeditods").modal('hide');
    });

    $("#cancel_c_ods").click(function () {

        $("#gochangeods").modal('hide');
    });

    $("#gosavechange").click(function () {
        if ($("input[name=dom_id]").val() == "" || $("input[name=dom_id]").val() == "0") {
            alert("Ошибка! Нельзя изменить ОДС для несуществующего адреса!");
            return false;
        }
        $("body").css("cursor", "wait");
        $.post(link2("addr_interface/setODSIdA"), {
            ods_id: $("select[name=newodsid]").val(),
            dom_id: $("input[name=dom_id]").val()
        })
            .success(function (data) {
                if (data == "error") {
                    $("body").css("cursor", "default");
                    alert('Ошибка изменения ОДС для выбранного адреса! Обратитесь к разработчикам!');
                    return false;
                } else {
                    $("input[name=ods_id]").val($("select[name=newodsid]").val());
                    $.post(link2("adm_ods/getODSParams"), {ods_id: $("select[name=newodsid]").val()})
                        .success(function (data) {
                            if (data == "error") {
                                $("body").css("cursor", "default");
                                alert("Ошибка получения информации о выбранной ОДС! Обратитесь к разработчикам!");
                                return false;
                            }
                            var ods = data.split("^");
                            var tComment = "ОДС: " + $("#e_ods_name").html() + " -> " + ods[1];
                            $("#e_ods_name").html(ods[1]);
                            $("#e_ods_addr").html(ods[4] != "" ? ods[4] : "-");
                            $("#e_ods_tel").html(ods[3] != "" ? ods[3] : "-");
                            $("#e_ods_contact").html(ods[2] != "" ? ods[2] : "-");
                            $("#e_ods_comment").html(ods[5] != "" ? ods[5] : "-");
                            $("body").css("cursor", "default");
                            $("#gochangeods").modal('hide');

                            $("body").css("cursor", "wait");
                            $("#task_comment_loader").show();
                            $("#go_edit_ods").prop('disabled', false);
                            $.post("/comments/addcmm_ajax/", {
                                task_id: $("input[name=task_id]").val(),
                                cmm_text: tComment,
                                tag: "Обновлены данные"
                            }, function (data) {
                                $("#task_comment_loader").hide();
                                if (data) {
                                    $(".task_comments").append(data);

                                } else {
                                    alert("Ошибка добавления комментария!");
                                }
                                $("body").css("cursor", "default");

                            });
                        })
                        .fail(function () {
                            $("body").css("cursor", "default");
                            alert('Ошибка обмена данными с сервером!');
                        });

                }
            })
            .fail(function () {
                $("body").css("cursor", "default");
                alert('Ошибка обмена данными с сервером!');
            })

    });


    $("#gosave").click(function () {
        $("body").css("cursor", "wait");
        $.post(link2("adm_ods/setODSParamsA"), {
            ods_id: $("input[name=ods_id]").val(),
            ods_name: $("input[name=ods_name]").val(),
            ods_addr: $("input[name=ods_addr]").val(),
            ods_tel: $("input[name=ods_tel]").val(),
            ods_contact: $("input[name=ods_contact]").val(),
            ods_comment: $("textarea[name=ods_comment]").val()
        })
            .success(function (data) {
                switch (data) {
                    case "ok":
                        $("#e_ods_name").html($("input[name=ods_name]").val());
                        $("#e_ods_addr").html($("input[name=ods_addr]").val() != "" ? $("input[name=ods_addr]").val() : "-");
                        $("#e_ods_tel").html($("input[name=ods_tel]").val() != "" ? $("input[name=ods_tel]").val() : "-");
                        $("#e_ods_contact").html($("input[name=ods_contact]").val() != "" ? $("input[name=ods_contact]").val() : "-");
                        $("#e_ods_comment").html($("textarea[name=ods_comment]").val() != "" ? $("textarea[name=ods_comment]").val() : "-");

                        break;

                    case "notfilled":
                        alert('Форма заполнена не полностью! Пожалуйста, заполните форму и повторите попытку!');
                        break;

                    case "error":
                        alert('Ошибка на стороне сервера! Обратитесь к разработчикам!');
                        break;

                    default:
                        alert('Ошибка на стороне сервера! Обратитесь к разработчикам!');
                        break;
                }
                $("#goeditods").modal('hide');

                $("body").css("cursor", "default");
            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });
    });

    $("#askforreason").click(function () {

        $("#reason").val('');
        $("#resetticketform").modal('show');
        $("#reason").focus();

        return false;
    });

    $("#cancel").click(function () {
        $("#reason").val('');
        $("#resetticketform").modal('hide');
    });

    $("#goreset").click(function () {
        if ($("#reason").val() == "") {
            alert('Необходимо ввести причину переноса заявки.');
            return false;
        }
        if (confirm('Вы уверены, что необходимо выполнить перенос заявки?')) {
            $.post(link2('connections/removefromgfx?id=' + $('input[name=task_id]').val()), {reason_text: $("#reason").val()}).success(function (data) {
                if (data == "ok") {
                    $('#addticket').submit();
                } else {
                    alert('Ошибка удаления заявки из графика! Обратитесь к разработчикам!');
                }
            }).fail(function (xhr, textStatus, errorThrown) {
                alert("Ошибка удаления!");
            });
        }
    });


    $("#goaddnew").click(function () {

        $("#cancel_c_ods").click();
        $("input[name=new_ods_name]").val('');
        $("input[name=new_ods_addr]").val('');
        $("input[name=new_ods_tel]").val('');
        $("input[name=new_ods_contact]").val('');
        $("textarea[name=new_ods_comment]").val('');
        $("#gocreateods").modal('show');
    });

    $("#gocreate").click(function () {
        var title = $("input[name=new_ods_name]").val();
        var addr = $("input[name=new_ods_addr]").val();
        var tel = $("input[name=new_ods_tel]").val();
        var contact = $("input[name=new_ods_contact]").val();
        var comment = $("textarea[name=new_ods_comment]").val();
        if (!title || !addr || !tel) {
            alert('Заполните поля Название ОДС, Адрес, Телефон и повторите!');
            return false;
        }
        $("body").css("cursor", "wait");
        $.post(link2("adm_ods/saveA"), {
            new_ods_name: title,
            new_ods_contact: contact,
            new_ods_tel: tel,
            new_ods_addr: addr,
            new_ods_comment: comment
        })
            .success(function (data) {
                if (data == "notfilled") {
                    alert('Заполните поля Название ОДС, Адрес, Телефон и повторите!');
                    return;
                }
                if (data == "error") {
                    alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                } else {
                    alert(data);
                    var tComment = "ОДС: " + $("#e_ods_name").html() + " -> " + title;
                    $("#e_ods_name").html(title);
                    $("#e_ods_addr").html(addr != "" ? addr : "-");
                    $("#e_ods_tel").html(tel != "" ? tel : "-");
                    $("#e_ods_contact").html(contact != "" ? contact : "-");
                    $("#e_ods_comment").html(comment != "" ? comment : "-");

                    $("input[name=ods_id]").val(data);
                    $("#go_select_ods").prop('disabled', false);
                    $("#go_edit_ods").prop('disabled', false);
                    $.post(link2("addr_interface/setODSIdA"), {ods_id: data, dom_id: $("input[name=dom_id]").val()})
                        .success(function (data) {
                            $("#gocreateods").modal('hide');
                            $("#task_comment_loader").show();
                            $.post("/comments/addcmm_ajax/", {
                                task_id: $("input[name=task_id]").val(),
                                cmm_text: tComment,
                                tag: "Обновлены данные"
                            }, function (data) {
                                $("#task_comment_loader").hide();
                                if (data) {
                                    $(".task_comments").append(data);

                                } else {
                                    alert("Ошибка добавления комментария!");
                                }
                                $("body").css("cursor", "default");
                            });

                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                            $("body").css("cursor", "default");
                        });

                }

            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });
    });

    $("#cancel_create_ods").click(function () {
        $("#go_select_ods").prop('disabled', false);
        if ($("input[name=ods_id]").val()) {
            $("#go_edit_ods").prop('disabled', false);
        }
        $("#gocreateods").modal('hide');

    });



    $("#calc_tickettmc").click(function () {
        $("body").css("cursor", "wait");
        $("#savetmccalcok").hide();
        $("label[for=savecalcok]").hide();
        var el = $(this);
        $.post(link2('connections/checkTaskStatusA'), {task_id: $("input[name=task_id]").val()}).success(function (data) {
            if (data == "error") {
                $("body").css("cursor", "default");
                alert('Ошибка получения текущего статуса заявки! Обратитесь к разработчикам!');
                return false;
            }

            if (data == "nostatus") {
                $("body").css("cursor", "default");
                alert('Текущий статус заявки не позволяет открыть форму ввода Отчета! Требуемый статус: Выполнена, Сдача отчета, Расчет ТМЦ или Закрыта.');
                return false;
            }
            if (data == "ok" || data == "change") {
                if (data == "change") {
                    $("body").css("cursor", "default");
                    if (!confirm('Для продолжения необходимо перевести заявку в состояние "Сдача отчета". Продолжить?')) {
                        return false;
                    }
                }


                if (data == "change") {
                    var reportStatus_ID = $('input[name=report_status_id]').val();
                    //alert($('input[name=report_status_color]').val());
                    $(".float-panel50 ul").each(function () {
                        $(this).style("background-color", $('input[name=report_status_color]').val(), "important");
                    });
                    //$(".cmm_title").css("background-color", $('input[name=report_status_color]').val());
                    $("body").css("cursor", "wait");
                    $("#task_comment_loader").show();
                    $.post("/comments/addcmm_ajax/", {
                        task_id: $("input[name=task_id]").val(),
                        cmm_text: 'Автоматическая смена статуса при открытии формы Отчета.',
                        tag: '',
                        status_id: reportStatus_ID
                    }, function (data) {
                        $("#task_comment_loader").hide();
                        if (data) {
                            $(".task_comments").append(data);
                        } else {
                            alert("Ошибка добавления комментария!");
                        }
                        $("body").css("cursor", "default");
                    });
                }

                $(".floatingform").hide();
                $(".floatingform1").hide();

                $('body').css('cursor', 'wait');

                $.post(link2('connections/getCurrentTMCCalcA'), {task_id: $("input[name=calc_tsk_id]").val()})
                    .success(function (data) {

                        if (data == "error") {
                            alert('Ошибка получения расчета заявки! Обратитесь к разработчикам!');
                            $("body").css("cursor", "default");
                            return false;
                        } else {
                            $("body").css("cursor", "default");
                            $("#calc_tmc_content").html(data);
                            $(".date_input").datepicker();
                            $(".intonly").mask("?9999");


                            $(".onlynum").maskex('000');

                            // расчет ТМЦ личный кабинет техника
                            $("#add_tmc_to_task").click(function () {
                                $('body').css('cursor', 'default');


                                $("#newtmccat_inform").val(0);
                                $("#tmc_add_list").html("<tr id='tmc_norows'><td colspan='5'><p align='center' style='color: #FF0000;'><strong>Список пуст</strong></p></td></tr>");

                                $("#addtmc_inform").modal('show');
                            });

                            $("#goaddtmctotask_inform").click(function () {
                                $("#addtmc_inform").modal('hide');

                            });

                            $("#canceladdtmctotask_inform").click(function () {
                                $("#addtmc_inform").modal('hide');

                            });

                            $("#newtmccat_inform").change(function () {
                                $("body").css("cursor", "wait");
                                $.post(link2('adm_materials/getListByCat'), {
                                    cat_id: $(this).val(),
                                    tech_id: $("input[name=tech_id]").val()
                                })
                                    .success(function (data) {
                                        if (data == "error") {
                                            $("body").css("cursor", "default");
                                            alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                                            return false;
                                        } else {
                                            $("body").css("cursor", "default");
                                            $("#tmc_add_list").html(data);

                                            // tmc add

                                            $("a.tmc").click(function () {
                                                var tmcId = $(this).attr('class').split("_")[1];
                                                var maxVal_sn = $(this).attr('id');
                                                var row_id = $(this).attr('tst_id');
                                                var metric = $(this).hasClass('metric') ? true : false;
                                                $(".tmc_no_rows").hide();
                                                var tmcTitle = $(".tmctitle_" + tmcId).html();
                                                if (!metric) {
                                                    if (!($(".tmc_list").find("." + row_id + "").length)) {
                                                        //<input required='required' value='1' disabled='disabled' name='tmc_count_" + tmcId + "' type='text' style='width:50px;' />
                                                        $(".tmc_list").append(
                                                            "<tr class='" + tmcId + " " + row_id + "'>" +
                                                            "   <input type=\"hidden\" name=\"row_id[]\" value=\"" + row_id + "\" />" +
                                                            "   <td align='center'>" + tmcTitle + "</td>" +
                                                            "   <td align='center'>" + maxVal_sn + "</td>" +
                                                            "   <td align='center'>" +
                                                            "       <input type='hidden' name='tmcid_" + row_id + "' value='" + maxVal_sn + "' />" +
                                                            "       <input type='hidden' name='tmc_ids[]' value='" + tmcId + "' />" +
                                                            /* более внятное гавное copy-paste */
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][row_id]' value='" + row_id + "' />" +
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][tmc_id]' value='" + tmcId + "' />" +
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][sn]' value='" + maxVal_sn + "' />" +
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][count]' value='1' />" +
                                                            "       1" +
                                                            "   </td>" +
                                                            "   <td align='center'>" +
                                                            "       <input type='checkbox' name='writeoff_tmc[" + row_id + "][rent]' />" +
                                                            "   </td>" +
                                                            "   <td align='center'>" +
                                                            "       <a class='removetmc tmcid_" + tmcId + "' href='#'><img src='/templates/images/cross.png' /></a>" +
                                                            "   </td>" +
                                                            "</tr>");
                                                    } else {
                                                        $(".tmc_list").find("." + row_id + "").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
                                                        return false;
                                                    }
                                                } else {
                                                    if (!($(".tmc_list").find("." + tmcId + "").length)) {
                                                        $(".tmc_list").append(
                                                            "<tr class='" + tmcId + " " + row_id + "'>" +
                                                            "   <input type=\"hidden\" name=\"row_id[]\" value=\"" + row_id + "\" />" +
                                                            "   <td align='center'>" + tmcTitle + "</td>" +
                                                            "   <td align='center'>&mdash;</td>" +
                                                            "   <td align='center'>" +
                                                            "       <input type='hidden' name='tmc_ids[]' value='" + tmcId + "' />" +
                                                            "       <input type='hidden' name='maxval' value=" + maxVal_sn + " />" +
                                                            /* еще одно более внятное гавное для метрических ТМЦ */
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][row_id]' value='" + row_id + "' />" +
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][tmc_id]' value='" + tmcId + "' />" +
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][max_val]' value='" + maxVal_sn + "' />" +
                                                            "       <input type='text' name='writeoff_tmc[" + row_id + "][count]' class='tmccount int' required='required' style='width:50px;' />" +
                                                            "   </td>" +
                                                            "   <td align='center'>&mdash;</td>" +
                                                            "   <td align='center'>" +
                                                            "       <a class='removetmc tmcid_" + tmcId + "' href='#'><img src='/templates/images/cross.png' /></a>" +
                                                            "   </td>" +
                                                            "</tr>");
                                                    } else {
                                                        $(".tmc_list").find("." + tmcId + "").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
                                                        return false;
                                                    }
                                                }
                                                $(".int").maskex('000000');
                                                $(".tmccount").blur(function () {
                                                    var maxVal = $(this).prev("input[name=maxval]").val();
                                                    if (parseInt($(this).val()) > parseInt(maxVal)) {
                                                        $(this).val('');
                                                        alert('Нельзя списать ТМЦ в количестве, больше, чем есть на руках!');
                                                        throw {
                                                            name: 'Ошибка',
                                                            message: 'Нельзя списать ТМЦ в количестве, больше, чем есть на руках!'
                                                        };
                                                        return false;
                                                    }
                                                });
                                                $('a.removetmc').click(function () {
                                                    var tmcId = $(this).attr('class').split("_")[1];
                                                    $("tr." + tmcId).remove();
                                                    if ($(".tmc_list tr").length == 1) {
                                                        $(".tmc_no_rows").show();
                                                    }
                                                    return false;
                                                });
                                                //} else {
                                                //    $(".tmc_list").find("." + tmcId + "").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
                                                //}
                                                return false;
                                            });

                                            //---------------
                                        }
                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                        $("body").css("cursor", "default");
                                    })
                            });

                            // -------------------------------------------

                            $('a.removetmc').click(function () {
                                var tmcId = $(this).attr('class').split("_")[1];
                                //alert(tmcId);
                                $("tr." + tmcId).remove();
                                if ($(".tmc_list tr").length == 1) {
                                    $(".tmc_no_rows").show();
                                }
                                return false;
                            });

                            $("#goaddnew_inform").click(function () {


                                var a = $("#go_select_ods_inform").position();

                                $("input[name=new_ods_name_inform]").val('');
                                $("input[name=new_ods_addr_inform]").val('');
                                $("input[name=new_ods_tel_inform]").val('');
                                $("input[name=new_ods_contact_inform]").val('');
                                $("textarea[name=new_ods_comment_inform]").val('');
                                $("#gocreateods_inform").modal('show');
                            });
                            $("#gocreate_inform").click(function () {
                                var title = $("input[name=new_ods_name_inform]").val();
                                var addr = $("input[name=new_ods_addr_inform]").val();
                                var tel = $("input[name=new_ods_tel_inform]").val();
                                var contact = $("input[name=new_ods_contact_inform]").val();
                                var comment = $("textarea[name=new_ods_comment_inform]").val();
                                if (!title || !addr || !tel) {
                                    alert('Заполните поля Название ОДС, Адрес, Телефон и повторите!');
                                    return false;
                                }
                                $("body").css("cursor", "wait");
                                $.post(link2("adm_ods/saveA"), {
                                    new_ods_name: title,
                                    new_ods_contact: contact,
                                    new_ods_tel: tel,
                                    new_ods_addr: addr,
                                    new_ods_comment: comment
                                })
                                    .success(function (data) {
                                        if (data == "notfilled") {
                                            alert('Заполните поля Название ОДС, Адрес, Телефон и повторите!');
                                            return;
                                        }
                                        if (data == "error") {
                                            alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                                        } else {
                                            //alert(data);
                                            var tComment = "ОДС: " + $("#e_ods_name").html() + " -> " + title;
                                            $("#e_ods_name_inform").html(title);
                                            $("#e_ods_addr_inform").html(addr != "" ? addr : "-");
                                            $("#e_ods_tel_inform").html(tel != "" ? tel : "-");
                                            $("#e_ods_contact_inform").html(contact != "" ? contact : "-");
                                            $("#e_ods_comment_inform").html(comment != "" ? comment : "-");

                                            $("input[name=ods_id_inform]").val(data);
                                            $("#e_ods_name").html(title);
                                            $("#e_ods_addr").html(addr != "" ? addr : "-");
                                            $("#e_ods_tel").html(tel != "" ? tel : "-");
                                            $("#e_ods_contact").html(contact != "" ? contact : "-");
                                            $("#e_ods_comment").html(comment != "" ? comment : "-");

                                            $("input[name=ods_id]").val(data);

                                            $("#report_c input[type=button]").prop('disabled', false);
                                            $.post(link2("addr_interface/setODSIdA"), {
                                                ods_id: data,
                                                dom_id: $("input[name=dom_id]").val()
                                            })
                                                .success(function (data) {
                                                    $("#gocreateods_inform").modal('hide');
                                                    $("#task_comment_loader").show();
                                                    $.post("/comments/addcmm_ajax/", {
                                                        task_id: $("input[name=task_id]").val(),
                                                        cmm_text: tComment,
                                                        tag: "Обновлены данные"
                                                    }, function (data) {
                                                        $("#task_comment_loader").hide();
                                                        if (data) {
                                                            $(".task_comments").append(data);

                                                        } else {
                                                            alert("Ошибка добавления комментария!");
                                                        }
                                                        $("body").css("cursor", "default");
                                                    });

                                                })
                                                .fail(function () {
                                                    alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                                    $("body").css("cursor", "default");
                                                });

                                        }

                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                        $("body").css("cursor", "default");
                                    });
                            });

                            $("#cancel_create_ods_inform").click(function () {
                                $("#gocreateods_inform").modal('hide');
                                $("#go_select_ods_inform").click();

                            });


                            $("#go_edit_ods_inform").click(function () {
                                $("#cancel_calc").prop('disabled', true);
                                $("#save_calc").prop('disabled', true);
                                $("#report_c input[type=button]").prop('disabled', true);
                                $("body").css("cursor", "wait");

                                $.post(link2("adm_ods/getODSParams"), {ods_id: $("input[name=ods_id_inform]").val()})
                                    .success(function (data) {

                                        if (data == "error") {
                                            alert('Ошибка получение ОДС! Обратитесь к разработчикам!');
                                        } else {

                                            var ods = data.split("^");
                                            $("input[name=ods_name_inform]").val(ods[1]);
                                            $("input[name=ods_addr_inform]").val(ods[4]);
                                            $("input[name=ods_tel_inform]").val(ods[3]);
                                            $("input[name=ods_contact_inform]").val(ods[2]);
                                            $("textarea[name=ods_comment_inform]").val(ods[5]);
                                            $("#goeditods_inform").modal('show');
                                            $("input[name=ods_name_inform]").focus();
                                        }
                                        $("#report_c input[type=button]").prop('disabled', false);
                                        $("#cancel_calc").prop('disabled', false);
                                        $("#save_calc").prop('disabled', false);
                                        $("body").css("cursor", "default");

                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером!');
                                        $("body").css("cursor", "default");
                                    });

                                return false;
                            });

                            $("#go_select_ods_inform").click(function () {

                                $("#report_c input[type=button]").prop('disabled', true);
                                if ($("input[name=dom_id]").val() == "" || $("input[name=dom_id]").val() == "0") {
                                    alert("Для выбора ОДС необходимо указать адрес в формате системы! Для выбора адреса, пожалуйста, нажмите кнопку Изменить в подразделе Адрес.");
                                    return false;
                                }


                                $("body").css("cursor", "wait");
                                $.post(link2("adm_ods/getODSListA"), {ods_id: $("input[name=ods_id_inform]").val()})
                                    .success(function (data) {
                                        if (data == "error") {
                                            alert('Ошибка получения ОДС! Обратитесь к разработчикам!');
                                        } else {
                                            $("#newodsid_inform").html(data);
                                            $("#gochangeods_inform").modal('show');
                                            $("input[name=ods_name_inform]").focus();
                                        }
                                        $("#report_c input[type=button]").prop('disabled', false);
                                        $("#cancel_calc").prop('disabled', false);
                                        $("#save_calc").prop('disabled', false);
                                        $("body").css("cursor", "default");

                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером!');
                                        $("body").css("cursor", "default");

                                    });

                                return false;
                            });

                            $("#cancel_ods_inform").click(function () {

                                $("#report_c input[type=button]").prop('disabled', false);
                                $("#goeditods_inform").modal('hide');
                            });

                            $("#cancel_c_ods_inform").click(function () {


                                $("#report_c input[type=button]").prop('disabled', false);
                                $("#gochangeods_inform").modal('hide');
                            });

                            $("#gosavechange_inform").click(function () {
                                if ($("input[name=dom_id]").val() == "" || $("input[name=dom_id]").val() == "0") {
                                    alert("Ошибка! Нельзя изменить ОДС для несуществующего адреса!");
                                    return false;
                                }
                                $("body").css("cursor", "wait");
                                $.post(link2("addr_interface/setODSIdA"), {
                                    ods_id: $("select[name=newodsid_inform]").val(),
                                    dom_id: $("input[name=dom_id]").val()
                                })
                                    .success(function (data) {
                                        if (data == "error") {
                                            $("body").css("cursor", "default");
                                            alert('Ошибка изменения ОДС для выбранного адреса! Обратитесь к разработчикам!');

                                            $("#report_c input[type=button]").prop('disabled', false);
                                            return false;
                                        } else {
                                            $("input[name=ods_id_inform]").val($("select[name=newodsid_inform]").val());
                                            $.post(link2("adm_ods/getODSParams"), {ods_id: $("select[name=newodsid_inform]").val()})
                                                .success(function (data) {
                                                    if (data == "error") {
                                                        $("body").css("cursor", "default");
                                                        alert("Ошибка получения информации о выбранной ОДС! Обратитесь к разработчикам!");

                                                        $("#report_c input[type=button]").prop('disabled', false);
                                                        return false;
                                                    }
                                                    var ods = data.split("^");
                                                    var tComment = "ОДС: " + $("#e_ods_name_inform").html() + " -> " + ods[1];
                                                    $("#e_ods_name_inform").html(ods[1]);
                                                    $("#e_ods_addr_inform").html(ods[4] != "" ? ods[4] : "-");
                                                    $("#e_ods_tel_inform").html(ods[3] != "" ? ods[3] : "-");
                                                    $("#e_ods_contact_inform").html(ods[2] != "" ? ods[2] : "-");
                                                    $("#e_ods_comment_inform").html(ods[5] != "" ? ods[5] : "-");
                                                    $("input[name=ods_id]").val($("select[name=newodsid_inform]").val());
                                                    $("#e_ods_name").html(ods[1]);
                                                    $("#e_ods_addr").html(ods[4] != "" ? ods[4] : "-");
                                                    $("#e_ods_tel").html(ods[3] != "" ? ods[3] : "-");
                                                    $("#e_ods_contact").html(ods[2] != "" ? ods[2] : "-");
                                                    $("#e_ods_comment").html(ods[5] != "" ? ods[5] : "-");
                                                    $("body").css("cursor", "default");
                                                    $("#gochangeods_inform").modal('hide');

                                                    $("#report_c input[type=button]").prop('disabled', false);
                                                    $("body").css("cursor", "wait");
                                                    $("#task_comment_loader").show();
                                                    $("#go_edit_ods_inform").prop('disabled', false);
                                                    $.post("/comments/addcmm_ajax/", {
                                                        task_id: $("input[name=task_id]").val(),
                                                        cmm_text: tComment,
                                                        tag: "Обновлены данные"
                                                    }, function (data) {
                                                        $("#task_comment_loader").hide();
                                                        if (data) {
                                                            $(".task_comments").append(data);

                                                        } else {
                                                            alert("Ошибка добавления комментария!");
                                                        }
                                                        $("body").css("cursor", "default");

                                                    });
                                                })
                                                .fail(function () {
                                                    $("body").css("cursor", "default");
                                                    alert('Ошибка обмена данными с сервером!');
                                                });

                                        }
                                    })
                                    .fail(function () {
                                        $("body").css("cursor", "default");
                                        alert('Ошибка обмена данными с сервером!');
                                    })

                            });


                            $("#gosave_inform").click(function () {
                                $("body").css("cursor", "wait");
                                $.post(link2("adm_ods/setODSParamsA"), {
                                    ods_id: $("input[name=ods_id_inform]").val(),
                                    ods_name: $("input[name=ods_name_inform]").val(),
                                    ods_addr: $("input[name=ods_addr_inform]").val(),
                                    ods_tel: $("input[name=ods_tel_inform]").val(),
                                    ods_contact: $("input[name=ods_contact_inform]").val(),
                                    ods_comment: $("textarea[name=ods_comment_inform]").val()
                                })
                                    .success(function (data) {
                                        switch (data) {
                                            case "ok":
                                                $("#e_ods_name_inform").html($("input[name=ods_name_inform]").val());
                                                $("#e_ods_addr_inform").html($("input[name=ods_addr_inform]").val() != "" ? $("input[name=ods_addr_inform]").val() : "-");
                                                $("#e_ods_tel_inform").html($("input[name=ods_tel_inform]").val() != "" ? $("input[name=ods_tel_inform]").val() : "-");
                                                $("#e_ods_contact_inform").html($("input[name=ods_contact_inform]").val() != "" ? $("input[name=ods_contact_inform]").val() : "-");
                                                $("#e_ods_comment_inform").html($("textarea[name=ods_comment_inform]").val() != "" ? $("textarea[name=ods_comment_inform]").val() : "-");
                                                $("#e_ods_name").html($("input[name=ods_name_inform]").val());
                                                $("#e_ods_addr").html($("input[name=ods_addr_inform]").val() != "" ? $("input[name=ods_addr_inform]").val() : "-");
                                                $("#e_ods_tel").html($("input[name=ods_tel_inform]").val() != "" ? $("input[name=ods_tel_inform]").val() : "-");
                                                $("#e_ods_contact").html($("input[name=ods_contact_inform]").val() != "" ? $("input[name=ods_contact_inform]").val() : "-");
                                                $("#e_ods_comment").html($("textarea[name=ods_comment_inform]").val() != "" ? $("textarea[name=ods_comment_inform]").val() : "-");

                                                break;

                                            case "notfilled":
                                                alert('Форма заполнена не полностью! Пожалуйста, заполните форму и повторите попытку!');
                                                break;

                                            case "error":
                                                alert('Ошибка на стороне сервера! Обратитесь к разработчикам!');
                                                break;

                                            default:
                                                alert('Ошибка на стороне сервера! Обратитесь к разработчикам!');
                                                break;
                                        }
                                        $("#goeditods_inform").modal('hide');

                                        $("#report_c input[type=button]").prop('disabled', false);
                                        $("body").css("cursor", "default");
                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                        $("body").css("cursor", "default");
                                    });
                            });
                            if (!$("input[name=ods_id_inform]").val()) {
                                $("#go_edit_ods_inform").prop('disabled', true);
                            }

                            $("#calctmc").modal('show');
                            $.date_input.initialize();

                            //alert($("input[name=readonly]").val());
                            if ($("input[name=readonly]").val() != "") {
                                $(".hideifreadonly").hide();
                            }
                            return false;
                        }

                    })
                    .fail(function () {
                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                        $("body").css("cursor", "default");
                    });

                return false;
            }
        })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });


    });

    $("#cancel_tmccalc").click(function () {
        $("#calctmc").modal('hide');


    });

    $("#save_calctmc").click(function () {
        //$("#calctmc").modal('hide');

    });


    $("#calc_ticket").click(function () {
        $("body").css("cursor", "wait");

        var el = $(this);
        $.post(link2('connections/checkTaskStatusA'), {task_id: $("input[name=task_id]").val()}).success(function (data) {
            if (data == "error") {
                $("body").css("cursor", "default");
                alert('Ошибка получения текущего статуса заявки! Обратитесь к разработчикам!');
                return false;
            }

            if (data == "nostatus") {
                $("body").css("cursor", "default");
                alert('Текущий статус заявки не позволяет открыть форму ввода Отчета! Требуемый статус: Выполнена, Сдача отчета, Расчет ТМЦ или Закрыта.');
                return false;
            }
            if (data == "ok" || data == "change") {
                if (data == "change") {
                    $("body").css("cursor", "default");
                    if (!confirm('Для продолжения необходимо перевести заявку в состояние "Сдача отчета". Продолжить?')) {
                        return false;
                    }
                }


                if (data == "change") {
                    var reportStatus_ID = $('input[name=report_status_id]').val();
                    //alert($('input[name=report_status_color]').val());
                    $(".float-panel50 ul").each(function () {
                        $(this).style("background-color", $('input[name=report_status_color]').val(), "important");
                    });
                    //$(".cmm_title").css("background-color", $('input[name=report_status_color]').val());
                    $("body").css("cursor", "wait");
                    $("#task_comment_loader").show();
                    $.post("/comments/addcmm_ajax/", {
                        task_id: $("input[name=task_id]").val(),
                        cmm_text: 'Автоматическая смена статуса при открытии формы Отчета.',
                        tag: '',
                        status_id: reportStatus_ID
                    }, function (data) {
                        $("#task_comment_loader").hide();
                        if (data) {
                            $(".task_comments").append(data);
                        } else {
                            alert("Ошибка добавления комментария!");
                        }
                        $("body").css("cursor", "default");
                    });
                }

                $(".floatingform").hide();
                $(".floatingform1").hide();

                $('body').css('cursor', 'wait');

                $.post(link2('connections/getCurrentCalcA'), {task_id: $("input[name=calc_tsk_id]").val()})
                    .success(function (data) {

                        if (data == "error") {
                            alert('Ошибка получения расчета заявки! Обратитесь к разработчикам!');
                            $("body").css("cursor", "default");
                            return false;
                        } else {
                            $("body").css("cursor", "default");
                            $("#calc_content").html(data);
                            $(".date_input").datepicker();
                            $(".intonly").mask("?9999");


                            $(".onlynum").maskex('000');

                            // расчет ТМЦ


                            $("#add_tmc_to_task").click(function () {
                                $('body').css('cursor', 'default');


                                $("#newtmccat_inform").val(0);
                                $("#tmc_add_list").html("<tr id='tmc_norows'><td colspan='5'><p align='center' style='color: #FF0000;'><strong>Список пуст</strong></p></td></tr>");

                                $("#addtmc_inform").modal('show');
                            });

                            $("#goaddtmctotask_inform").click(function () {
                                $("#addtmc_inform").modal('hide');

                            });

                            $("#canceladdtmctotask_inform").click(function () {
                                $("#addtmc_inform").modal('hide');

                            });

                            $("#newtmccat_inform").change(function () {
                                $("body").css("cursor", "wait");
                                $.post(link2('adm_materials/getListByCat'), {
                                    cat_id: $(this).val(),
                                    tech_id: $("input[name=tech_id]").val()
                                })
                                    .success(function (data) {
                                        if (data == "error") {
                                            $("body").css("cursor", "default");
                                            alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                                            return false;
                                        } else {
                                            $("body").css("cursor", "default");
                                            $("#tmc_add_list").html(data);

                                            // tmc add
                                            $("a.tmc").click(function () {
                                                var tmcId = $(this).attr('class').split("_")[1];
                                                var maxVal_sn = $(this).attr('id');
                                                var row_id = $(this).attr('tst_id');
                                                var metric = $(this).hasClass('metric') ? true : false;
                                                $(".tmc_no_rows").hide();
                                                var tmcTitle = $(".tmctitle_" + tmcId).html();
                                                if (!metric) {
                                                    if (!($(".tmc_list").find("." + row_id + "").length)) {
                                                        //<input required='required' value='1' disabled='disabled' name='tmc_count_" + tmcId + "' type='text' style='width:50px;' />
                                                        $(".tmc_list").append(
                                                            "<tr class='" + tmcId + " " + row_id + "'>" +
                                                            "   <input type=\"hidden\" name=\"row_id[]\" value=\"" + row_id + "\" />" +
                                                            "   <td align='center'>" + tmcTitle + "</td>" +
                                                            "   <td align='center'>" + maxVal_sn + "</td>" +
                                                            "   <td align='center'>" +
                                                            "       <input type='hidden' name='tmcid_" + row_id + "' value='" + maxVal_sn + "' />" +
                                                            "       <input type='hidden' name='tmc_ids[]' value='" + tmcId + "' />" +
                                                            /* более внятное гавное copy-paste */
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][row_id]' value='" + row_id + "' />" +
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][sn]' value='" + maxVal_sn + "' />" +
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][count]' value='1' />" +
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][tmc_id]' value='" + tmcId + "' />" +
                                                            "       1" +
                                                            "   </td>" +
                                                            "   <td align='center'>" +
                                                            "       <input type='checkbox' name='writeoff_tmc[" + row_id + "][rent]' />" +
                                                            "   </td>" +
                                                            "   <td align='center'>" +
                                                            "       <a class='removetmc tmcid_" + row_id + "' href='#'><img src='/templates/images/cross.png' /></a>" +
                                                            "   </td>" +
                                                            "</tr>");
                                                    } else {
                                                        $(".tmc_list").find("." + row_id + "").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
                                                        return false;
                                                    }
                                                } else {
                                                    if (!($(".tmc_list").find("." + tmcId + "").length)) {
                                                        $(".tmc_list").append(
                                                            "<tr class='" + tmcId + " " + row_id + "'>" +
                                                            "   <input type=\"hidden\" name=\"row_id[]\" value=\"" + row_id + "\" />" +
                                                            "   <td align='center'>" + tmcTitle + "</td>" +
                                                            "   <td align='center'>&mdash;</td>" +
                                                            "   <td align='center'>" +
                                                            "       <input type='hidden' name='tmc_ids[]' value='" + tmcId + "' />" +
                                                            "       <input type='hidden' name='maxval' value=" + maxVal_sn + " />" +
                                                            //"       <input required='required' class='tmccount int' name='tmc_count_" + tmcId + "' type='text' style='width:50px;' />" +
                                                            /* еще одно более внятное гавное для метрических ТМЦ */
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][row_id]' value='" + row_id + "' />" +
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][max_val]' value='" + maxVal_sn + "' />" +
                                                            "       <input type='text' name='writeoff_tmc[" + row_id + "][count]' class='tmccount int' required='required' style='width:50px;' />" +
                                                            "       <input type='hidden' name='writeoff_tmc[" + row_id + "][tmc_id]' value='" + tmcId + "' />" +
                                                            "   </td>" +
                                                            "   <td align='center'>&mdash;</td>" +
                                                            "   <td align='center'>" +
                                                            "       <a class='removetmc tmcid_" + tmcId + "' href='#'><img src='/templates/images/cross.png' /></a>" +
                                                            "   </td>" +
                                                            "</tr>");
                                                    } else {
                                                        $(".tmc_list").find("." + tmcId + "").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
                                                        return false;
                                                    }
                                                }
                                                $(".int").maskex('000000');
                                                $(".tmccount").blur(function () {
                                                    var maxVal = $(this).prev("input[name=maxval]").val();
                                                    if (parseInt($(this).val()) > parseInt(maxVal)) {
                                                        $(this).val('');
                                                        alert('Нельзя списать ТМЦ в количестве, больше, чем есть на руках!');
                                                        throw {
                                                            name: 'Ошибка',
                                                            message: 'Нельзя списать ТМЦ в количестве, больше, чем есть на руках!'
                                                        };
                                                        return false;
                                                    }
                                                });
                                                $('a.removetmc').click(function () {
                                                    var tmcId = $(this).attr('class').split("_")[1];
                                                    $("tr." + tmcId).remove();
                                                    if ($(".tmc_list tr").length == 1) {
                                                        $(".tmc_no_rows").show();
                                                    }
                                                    return false;
                                                });
                                                //} else {
                                                //    $(".tmc_list").find("." + tmcId + "").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
                                                //}
                                                return false;
                                            });


                                            //---------------
                                        }
                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                        $("body").css("cursor", "default");
                                    })
                            });

                            // -------------------------------------------

                            $("#cw_add").click(function () {

                                $('body').css('cursor', 'default');


                                $("#add_cw_type_dialog").modal('show');

                            });

                            $("#tw_add").click(function () {
                                $('body').css('cursor', 'default');


                                $("#add_tw_type_dialog").modal('show');
                            });

                            $("#tw_shtraf_add").click(function () {
                                $('body').css('cursor', 'default');


                                $("#add_tw_shtraf_type_dialog").modal('show');
                            });
                            //cancel_goaddtwshitem
                            $('a.removetmc').click(function () {
                                var tmcId = $(this).attr('class').split("_")[1];
                                //alert(tmcId);
                                $("tr." + tmcId).remove();
                                if ($(".tmc_list tr").length == 1) {
                                    $(".tmc_no_rows").show();
                                }
                                return false;
                            });
                            $("a.tpremove").click(function () {
                                $("tr.tp" + $(this).attr('class').split("_")[1]).remove();
                                if ($(".tech_works tr").length == 0) {
                                    $(".tech_works").append("<tr class=\"tw_no_rows\"><td colspan=\"5\"><p align=\"center\" style=\"color: #FF0000;\"><strong>Список пуст</strong></p></td></tr>");
                                }
                                //add server query
                            });
                            $("a.tsremove").click(function () {
                                $("tr.ts" + $(this).attr('class').split("_")[1]).remove();
                                if ($(".tech_works tr").length == 0) {
                                    $(".tech_works").append("<tr class=\"tw_no_rows\"><td colspan=\"5\"><p align=\"center\" style=\"color: #FF0000;\"><strong>Список пуст</strong></p></td></tr>");
                                }
                                //add server query
                            });
                            $("a.cpremove").click(function () {
                                $("tr.cp" + $(this).attr('class').split("_")[1]).remove();
                                if ($(".contr_works tr").length == 0) {
                                    $(".contr_works").append("<tr class=\"cw_no_rows\"><td colspan=\"5\"><p align=\"center\" style=\"color: #FF0000;\"><strong>Список пуст</strong></p></td></tr>");
                                }
                                //add server query
                            });

                            $("a.csremove").click(function () {
                                $("tr.cs" + $(this).attr('class').split("_")[1]).remove();
                                if ($(".contr_works tr").length == 1 && $(".cw_no_rows").length) {
                                    $(".cw_no_rows").show();
                                } else {
                                    if ($(".contr_works tr").length == 0) {
                                        $(".contr_works").append("<tr class=\"cw_no_rows\"><td colspan=\"5\"><p align=\"center\" style=\"color: #FF0000;\"><strong>Список пуст</strong></p></td></tr>");
                                    }
                                }
                                //add server query
                            });

                            $("#goaddnew_inform").click(function () {


                                var a = $("#go_select_ods_inform").position();

                                $("input[name=new_ods_name_inform]").val('');
                                $("input[name=new_ods_addr_inform]").val('');
                                $("input[name=new_ods_tel_inform]").val('');
                                $("input[name=new_ods_contact_inform]").val('');
                                $("textarea[name=new_ods_comment_inform]").val('');
                                $("#gocreateods_inform").modal('show');
                            });
                            $("#gocreate_inform").click(function () {
                                var title = $("input[name=new_ods_name_inform]").val();
                                var addr = $("input[name=new_ods_addr_inform]").val();
                                var tel = $("input[name=new_ods_tel_inform]").val();
                                var contact = $("input[name=new_ods_contact_inform]").val();
                                var comment = $("textarea[name=new_ods_comment_inform]").val();
                                if (!title || !addr || !tel) {
                                    alert('Заполните поля Название ОДС, Адрес, Телефон и повторите!');
                                    return false;
                                }
                                $("body").css("cursor", "wait");
                                $.post(link2("adm_ods/saveA"), {
                                    new_ods_name: title,
                                    new_ods_contact: contact,
                                    new_ods_tel: tel,
                                    new_ods_addr: addr,
                                    new_ods_comment: comment
                                })
                                    .success(function (data) {
                                        if (data == "notfilled") {
                                            alert('Заполните поля Название ОДС, Адрес, Телефон и повторите!');
                                            return;
                                        }
                                        if (data == "error") {
                                            alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                                        } else {
                                            //alert(data);
                                            var tComment = "ОДС: " + $("#e_ods_name").html() + " -> " + title;
                                            $("#e_ods_name_inform").html(title);
                                            $("#e_ods_addr_inform").html(addr != "" ? addr : "-");
                                            $("#e_ods_tel_inform").html(tel != "" ? tel : "-");
                                            $("#e_ods_contact_inform").html(contact != "" ? contact : "-");
                                            $("#e_ods_comment_inform").html(comment != "" ? comment : "-");

                                            $("input[name=ods_id_inform]").val(data);
                                            $("#e_ods_name").html(title);
                                            $("#e_ods_addr").html(addr != "" ? addr : "-");
                                            $("#e_ods_tel").html(tel != "" ? tel : "-");
                                            $("#e_ods_contact").html(contact != "" ? contact : "-");
                                            $("#e_ods_comment").html(comment != "" ? comment : "-");

                                            $("input[name=ods_id]").val(data);

                                            $("#report_c input[type=button]").prop('disabled', false);
                                            $.post(link2("addr_interface/setODSIdA"), {
                                                ods_id: data,
                                                dom_id: $("input[name=dom_id]").val()
                                            })
                                                .success(function (data) {
                                                    $("#gocreateods_inform").modal('hide');
                                                    $("#task_comment_loader").show();
                                                    $.post("/comments/addcmm_ajax/", {
                                                        task_id: $("input[name=task_id]").val(),
                                                        cmm_text: tComment,
                                                        tag: "Обновлены данные"
                                                    }, function (data) {
                                                        $("#task_comment_loader").hide();
                                                        if (data) {
                                                            $(".task_comments").append(data);

                                                        } else {
                                                            alert("Ошибка добавления комментария!");
                                                        }
                                                        $("body").css("cursor", "default");
                                                    });

                                                })
                                                .fail(function () {
                                                    alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                                    $("body").css("cursor", "default");
                                                });

                                        }

                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                        $("body").css("cursor", "default");
                                    });
                            });

                            $("#cancel_create_ods_inform").click(function () {
                                $("#gocreateods_inform").modal('hide');
                                $("#go_select_ods_inform").click();

                            });


                            $("#go_edit_ods_inform").click(function () {
                                $("#cancel_calc").prop('disabled', true);
                                $("#save_calc").prop('disabled', true);
                                $("#report_c input[type=button]").prop('disabled', true);
                                $("body").css("cursor", "wait");

                                $.post(link2("adm_ods/getODSParams"), {ods_id: $("input[name=ods_id_inform]").val()})
                                    .success(function (data) {

                                        if (data == "error") {
                                            alert('Ошибка получение ОДС! Обратитесь к разработчикам!');
                                        } else {

                                            var ods = data.split("^");
                                            $("input[name=ods_name_inform]").val(ods[1]);
                                            $("input[name=ods_addr_inform]").val(ods[4]);
                                            $("input[name=ods_tel_inform]").val(ods[3]);
                                            $("input[name=ods_contact_inform]").val(ods[2]);
                                            $("textarea[name=ods_comment_inform]").val(ods[5]);
                                            $("#goeditods_inform").modal('show');
                                            $("input[name=ods_name_inform]").focus();
                                        }
                                        $("#report_c input[type=button]").prop('disabled', false);
                                        $("#cancel_calc").prop('disabled', false);
                                        $("#save_calc").prop('disabled', false);
                                        $("body").css("cursor", "default");

                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером!');
                                        $("body").css("cursor", "default");
                                    });

                                return false;
                            });

                            $("#go_select_ods_inform").click(function () {

                                $("#report_c input[type=button]").prop('disabled', true);
                                if ($("input[name=dom_id]").val() == "" || $("input[name=dom_id]").val() == "0") {
                                    alert("Для выбора ОДС необходимо указать адрес в формате системы! Для выбора адреса, пожалуйста, нажмите кнопку Изменить в подразделе Адрес.");
                                    return false;
                                }


                                $("body").css("cursor", "wait");
                                $.post(link2("adm_ods/getODSListA"), {ods_id: $("input[name=ods_id_inform]").val()})
                                    .success(function (data) {
                                        if (data == "error") {
                                            alert('Ошибка получения ОДС! Обратитесь к разработчикам!');
                                        } else {
                                            $("#newodsid_inform").html(data);
                                            $("#gochangeods_inform").modal('show');
                                            $("input[name=ods_name_inform]").focus();
                                        }
                                        $("#report_c input[type=button]").prop('disabled', false);
                                        $("#cancel_calc").prop('disabled', false);
                                        $("#save_calc").prop('disabled', false);
                                        $("body").css("cursor", "default");

                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером!');
                                        $("body").css("cursor", "default");

                                    });

                                return false;
                            });

                            $("#cancel_ods_inform").click(function () {

                                $("#report_c input[type=button]").prop('disabled', false);
                                $("#goeditods_inform").modal('hide');
                            });

                            $("#cancel_c_ods_inform").click(function () {


                                $("#report_c input[type=button]").prop('disabled', false);
                                $("#gochangeods_inform").modal('hide');
                            });

                            $("#gosavechange_inform").click(function () {
                                if ($("input[name=dom_id]").val() == "" || $("input[name=dom_id]").val() == "0") {
                                    alert("Ошибка! Нельзя изменить ОДС для несуществующего адреса!");
                                    return false;
                                }
                                $("body").css("cursor", "wait");
                                $.post(link2("addr_interface/setODSIdA"), {
                                    ods_id: $("select[name=newodsid_inform]").val(),
                                    dom_id: $("input[name=dom_id]").val()
                                })
                                    .success(function (data) {
                                        if (data == "error") {
                                            $("body").css("cursor", "default");
                                            alert('Ошибка изменения ОДС для выбранного адреса! Обратитесь к разработчикам!');

                                            $("#report_c input[type=button]").prop('disabled', false);
                                            return false;
                                        } else {
                                            $("input[name=ods_id_inform]").val($("select[name=newodsid_inform]").val());
                                            $.post(link2("adm_ods/getODSParams"), {ods_id: $("select[name=newodsid_inform]").val()})
                                                .success(function (data) {
                                                    if (data == "error") {
                                                        $("body").css("cursor", "default");
                                                        alert("Ошибка получения информации о выбранной ОДС! Обратитесь к разработчикам!");

                                                        $("#report_c input[type=button]").prop('disabled', false);
                                                        return false;
                                                    }
                                                    var ods = data.split("^");
                                                    var tComment = "ОДС: " + $("#e_ods_name_inform").html() + " -> " + ods[1];
                                                    $("#e_ods_name_inform").html(ods[1]);
                                                    $("#e_ods_addr_inform").html(ods[4] != "" ? ods[4] : "-");
                                                    $("#e_ods_tel_inform").html(ods[3] != "" ? ods[3] : "-");
                                                    $("#e_ods_contact_inform").html(ods[2] != "" ? ods[2] : "-");
                                                    $("#e_ods_comment_inform").html(ods[5] != "" ? ods[5] : "-");
                                                    $("input[name=ods_id]").val($("select[name=newodsid_inform]").val());
                                                    $("#e_ods_name").html(ods[1]);
                                                    $("#e_ods_addr").html(ods[4] != "" ? ods[4] : "-");
                                                    $("#e_ods_tel").html(ods[3] != "" ? ods[3] : "-");
                                                    $("#e_ods_contact").html(ods[2] != "" ? ods[2] : "-");
                                                    $("#e_ods_comment").html(ods[5] != "" ? ods[5] : "-");
                                                    $("body").css("cursor", "default");
                                                    $("#gochangeods_inform").modal('hide');

                                                    $("#report_c input[type=button]").prop('disabled', false);
                                                    $("body").css("cursor", "wait");
                                                    $("#task_comment_loader").show();
                                                    $("#go_edit_ods_inform").prop('disabled', false);
                                                    $.post("/comments/addcmm_ajax/", {
                                                        task_id: $("input[name=task_id]").val(),
                                                        cmm_text: tComment,
                                                        tag: "Обновлены данные"
                                                    }, function (data) {
                                                        $("#task_comment_loader").hide();
                                                        if (data) {
                                                            $(".task_comments").append(data);

                                                        } else {
                                                            alert("Ошибка добавления комментария!");
                                                        }
                                                        $("body").css("cursor", "default");

                                                    });
                                                })
                                                .fail(function () {
                                                    $("body").css("cursor", "default");
                                                    alert('Ошибка обмена данными с сервером!');
                                                });

                                        }
                                    })
                                    .fail(function () {
                                        $("body").css("cursor", "default");
                                        alert('Ошибка обмена данными с сервером!');
                                    })

                            });


                            $("#gosave_inform").click(function () {
                                $("body").css("cursor", "wait");
                                $.post(link2("adm_ods/setODSParamsA"), {
                                    ods_id: $("input[name=ods_id_inform]").val(),
                                    ods_name: $("input[name=ods_name_inform]").val(),
                                    ods_addr: $("input[name=ods_addr_inform]").val(),
                                    ods_tel: $("input[name=ods_tel_inform]").val(),
                                    ods_contact: $("input[name=ods_contact_inform]").val(),
                                    ods_comment: $("textarea[name=ods_comment_inform]").val()
                                })
                                    .success(function (data) {
                                        switch (data) {
                                            case "ok":
                                                $("#e_ods_name_inform").html($("input[name=ods_name_inform]").val());
                                                $("#e_ods_addr_inform").html($("input[name=ods_addr_inform]").val() != "" ? $("input[name=ods_addr_inform]").val() : "-");
                                                $("#e_ods_tel_inform").html($("input[name=ods_tel_inform]").val() != "" ? $("input[name=ods_tel_inform]").val() : "-");
                                                $("#e_ods_contact_inform").html($("input[name=ods_contact_inform]").val() != "" ? $("input[name=ods_contact_inform]").val() : "-");
                                                $("#e_ods_comment_inform").html($("textarea[name=ods_comment_inform]").val() != "" ? $("textarea[name=ods_comment_inform]").val() : "-");
                                                $("#e_ods_name").html($("input[name=ods_name_inform]").val());
                                                $("#e_ods_addr").html($("input[name=ods_addr_inform]").val() != "" ? $("input[name=ods_addr_inform]").val() : "-");
                                                $("#e_ods_tel").html($("input[name=ods_tel_inform]").val() != "" ? $("input[name=ods_tel_inform]").val() : "-");
                                                $("#e_ods_contact").html($("input[name=ods_contact_inform]").val() != "" ? $("input[name=ods_contact_inform]").val() : "-");
                                                $("#e_ods_comment").html($("textarea[name=ods_comment_inform]").val() != "" ? $("textarea[name=ods_comment_inform]").val() : "-");

                                                break;

                                            case "notfilled":
                                                alert('Форма заполнена не полностью! Пожалуйста, заполните форму и повторите попытку!');
                                                break;

                                            case "error":
                                                alert('Ошибка на стороне сервера! Обратитесь к разработчикам!');
                                                break;

                                            default:
                                                alert('Ошибка на стороне сервера! Обратитесь к разработчикам!');
                                                break;
                                        }
                                        $("#goeditods_inform").modal('hide');

                                        $("#report_c input[type=button]").prop('disabled', false);
                                        $("body").css("cursor", "default");
                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                        $("body").css("cursor", "default");
                                    });
                            });
                            if (!$("input[name=ods_id_inform]").val()) {
                                $("#go_edit_ods_inform").prop('disabled', true);
                            }
                            $("#calc").modal('show');
                            $.date_input.initialize();
                            if ($("input[name=readonly]").val() != "") {
                                $(".hideifreadonly").hide();
                            }
                            return false;
                        }

                    })
                    .fail(function () {
                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                        $("body").css("cursor", "default");
                    });

                return false;
            }
        })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });


    });

    $("#cancel_calc").click(function () {
        $("#calc").modal('hide');


    });

    $("#gosavecalc").click(function () {
        $("#calc").modal('hide');

    });


    if ($("input[name=current_kladr_code]").val()) {
        backup = true;
        b_code = $("input[name=current_kladr_code]").val();
        b_region_title = $("#curr_region_title").html();
        b_street = $("input[name=street]").val();
        b_althouse = $("input[name=althouse]").val();
        b_pod = $("input[name=pod]").val();
        b_etazh = $("input[name=etazh]").val();
        b_kv = $("input[name=kv]").val();
        b_domofon = $("input[name=domofon]").val();
        b_k_region = $("select[name=k_region]").val();
        b_k_area = $("select[name=k_area]").val();
        b_k_city = $("select[name=k_city]").val();
        b_k_np = $("select[name=k_np]").val();
        b_k_house = $("select[name=k_house]").val() != "" && $("select[name=k_house]").val() != "0" ? $("select[name=k_house]").val() : $("input[name=current_house]").val();
        b_area_id = $("select[name=area_id]").val();
    }


    reloadWtypeList();

    $("#cw_add").click(function () {

        $('body').css('cursor', 'default');


        $("#add_cw_type_dialog").modal('show');

    });

    $("#tw_add").click(function () {
        $('body').css('cursor', 'default');


        $("#add_tw_type_dialog").modal('show');
    });

    $("#cancel_goaddcwitem").click(function () {
        $("#add_cw_type_dialog").modal('hide');

    });

    $("#cancel_goaddtwitem").click(function () {
        $("#add_tw_type_dialog").modal('hide');

    });


    $("#cancel_goaddtwshitem").click(function () {
        $("#add_tw_shtraf_type_dialog").modal('hide');

    });

    $(".cpr").click(function () {
        $(".cw_no_rows").hide();
        if (!$(".contr_works tr.cp" + $(this).attr('class').split("_")[1]).length) {
            $(".contr_works").append("<tr class=\"cp" + $(this).attr('class').split("_")[1] + "\"><td><input type=hidden name=cp[] value=" + $(this).attr('class').split("_")[1] + " /><strong>" + $("td.cptitle" + $(this).attr('class').split("_")[1] + " a").html() + "</strong><br /><small>" + $("td.cpdesc" + $(this).attr('class').split("_")[1]).html() + "</small></td><td><input  class='onlynum' type='text' value='1' style='text-align: center; width: 30px' name=cq" + $(this).attr('class').split("_")[1] + " /></td><td align='center'>" + $("td.cprice" + $(this).attr('class').split("_")[1]).html() + "</td><td><center>&mdash;</center></td><td><a class=\"cpremove remove_" + $(this).attr('class').split("_")[1] + "\" style=\"cursor:pointer;\"><img border=\"0\" src=\"../templates/images/cross.png\" /></a></td></tr>");
            $(".onlynum").maskex('000');
            $("a.cpremove").click(function () {
                $("tr.cp" + $(this).attr('class').split("_")[1]).remove();
                if ($(".contr_works tr").length == 1) {
                    $(".cw_no_rows").show();
                }
                //add server query
            });
        } else {
            $(".contr_works tr.cp" + $(this).attr('class').split("_")[1] + " td").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
        }
        // add server query
        return false;
    });
    $("a.cpremove").click(function () {
        $("tr.cp" + $(this).attr('class').split("_")[1]).remove();
        if ($(".contr_works tr").length == 0) {
            $(".contr_works").append("<tr class=\"cw_no_rows\"><td colspan=\"5\"><p align=\"center\" style=\"color: #FF0000;\"><strong>Список пуст</strong></p></td></tr>");
        }
        //add server query
    });
    $(".csr").click(function () {
        if (!$(".contr_works tr.cs" + $(this).attr('class').split("_")[1]).length) {
            if ($("input[name=smc" + $(this).attr('class').split("_")[1] + "]").val()) {
                $(".cw_no_rows").hide();
                $(".contr_works").append("<tr val='csr' class=\"cs" + $(this).attr('class').split("_")[1] + "\"><td><input type='hidden' name='cs" + $(this).attr('class').split("_")[1] + "' value='" + $("input[name='smc" + $(this).attr('class').split("_")[1] + "']").val() + "' /><strong>" + $("td.cstitle" + $(this).attr('class').split("_")[1] + " a").html() + "</strong><br /><small>" + $("td.csdesc" + $(this).attr('class').split("_")[1]).html() + "</small></td><td></td><td></td><td><center>" + $("input[name='smc" + $(this).attr('class').split("_")[1] + "']").val() + "</center></td><td><a class=\"csremove remove_" + $(this).attr('class').split("_")[1] + "\" style=\"cursor:pointer;\"><img border=\"0\" src=\"../templates/images/cross.png\" /></a></td></tr>");
                $("a.csremove").click(function () {
                    $("tr.cs" + $(this).attr('class').split("_")[1]).remove();
                    if ($(".contr_works tr").length == 1) {
                        $(".cw_no_rows").show();
                    }
                    //add server query
                });
            } else {
                alert('Заполните поле Сумма счета!');
            }
        } else {
            $(".contr_works tr.cp" + $(this).attr('class').split("_")[1] + " td").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
        }// add server query
        return false;
    });
    $("a.cpremove").click(function () {
        $("tr.cp" + $(this).attr('class').split("_")[1]).remove();
        if ($(".contr_works tr").length == 0) {
            $(".contr_works").append("<tr class=\"cw_no_rows\"><td colspan=\"5\"><p align=\"center\" style=\"color: #FF0000;\"><strong>Список пуст</strong></p></td></tr>");
        }
        //add server query
    });

    $("a.csremove").click(function () {
        $("tr.cs" + $(this).attr('class').split("_")[1]).remove();
        if ($(".contr_works tr").length == 1) {
            $(".contr_works").append("<tr class=\"cw_no_rows\"><td colspan=\"5\"><p align=\"center\" style=\"color: #FF0000;\"><strong>Список пуст</strong></p></td></tr>");
        }
        //add server query
    });
    $(".tpr").click(function () {
        $(".tw_no_rows").hide();
        if (!$(".tech_works tr.tp" + $(this).attr('class').split("_")[1]).length) {
            $(".tech_works").append("<tr class=\"tp" + $(this).attr('class').split("_")[1] + "\"><td><input type=\"hidden\" name=\"tp[]\" value=\"" + $(this).attr('class').split("_")[1] + "\" /><strong>" + $("td.tptitle" + $(this).attr('class').split("_")[1] + " a").html() + "</strong><br /><small>" + $("td.tpdesc" + $(this).attr('class').split("_")[1]).html() + "</small></td><td><input class='onlynum' type='text' value='1' style='text-align: center; width: 30px' name=tq" + $(this).attr('class').split("_")[1] + " /></td><td align='center'>" + $("td.tprice" + $(this).attr('class').split("_")[1]).html() + "</td><td><center>&mdash;</center></td><td><a class=\"tpremove remove_" + $(this).attr('class').split("_")[1] + "\" style=\"cursor:pointer;\"><img border=\"0\" src=\"../templates/images/cross.png\" /></a></td></tr>");
            //alert("<tr class=\"tp" + $(this).attr('class').split("_")[1] + "\"><td><input type=\"hidden\" name=\"tp[]\" value=\"" + $(this).attr('class').split("_")[1]+ "\" /><strong>" + $("td.tptitle"+ $(this).attr('class').split("_")[1] + " a").html() + "</strong><br /><small>" + $("td.tpdesc" + $(this).attr('class').split("_")[1]).html() + "</small></td><td align='center'>" + $("td.tprice" + $(this).attr('class').split("_")[1]).html() + "</td><td><center>&mdash;</center></td><td><a class=\"tpremove remove_"+ $(this).attr('class').split("_")[1] +"\" style=\"cursor:pointer;\"><img border=\"0\" src=\"images/cross.png\" /></a></td></tr>");
            $(".onlynum").maskex('000');
            $("a.tpremove").click(function () {
                $("tr.tp" + $(this).attr('class').split("_")[1]).remove();
                if ($(".tech_works tr").length == 1) {
                    $(".tw_no_rows").show();
                }
                //add server query
            });
        } else {
            $(".tech_works tr.tp" + $(this).attr('class').split("_")[1] + " td").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
        }// add server query
        return false;
    });

    $(".tsr").click(function () {
        if (!$(".tech_works tr.ts" + $(this).attr('class').split("_")[1]).length) {
            if ($("input[name=smt" + $(this).attr('class').split("_")[1] + "]").val()) {
                $(".tw_no_rows").hide();
                $(".tech_works").append("<tr val='tsr' class=\"ts" + $(this).attr('class').split("_")[1] + "\"><td><input type=\"hidden\" name=\"ts" + $(this).attr('class').split("_")[1] + "\" value=\"" + $("input[name='smt" + $(this).attr('class').split("_")[1] + "']").val() + "\" /><strong>" + $("td.tstitle" + $(this).attr('class').split("_")[1] + " a").html() + "</strong><br /><small>" + $("td.tsdesc" + $(this).attr('class').split("_")[1]).html() + "</small></td><td></td><td align=\"center\">" + Math.round((parseInt($("input[name='smt" + $(this).attr('class').split("_")[1] + "']").val()) * parseInt($("input[name='tsrval" + $(this).attr('class').split("_")[1] + "']").val())) / 100).toString() + "</td><td><center>" + $("input[name='smt" + $(this).attr('class').split("_")[1] + "']").val() + "<small> (" + $("input[name='tsrval" + $(this).attr('class').split("_")[1] + "']").val() + "%)</small></center></td><td><a class=\"tsremove remove_" + $(this).attr('class').split("_")[1] + "\" style=\"cursor:pointer;\"><img border=\"0\" src=\"../templates/images/cross.png\" /></a></td></tr>");
                $("a.tsremove").click(function () {
                    $("tr.ts" + $(this).attr('class').split("_")[1]).remove();
                    if ($(".tech_works tr").length == 1) {
                        $(".tw_no_rows").show();
                    }
                    //add server query
                });
            } else {
                alert('Заполните поле Сумма счета!');
            }
        } else {
            $(".tech_works tr.ts" + $(this).attr('class').split("_")[1] + " td").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
        }// add server query
        return false;
    });
    $("a.tpremove").click(function () {
        $("tr.tp" + $(this).attr('class').split("_")[1]).remove();
        if ($(".tech_works tr").length == 0) {
            $(".tech_works").append("<tr class=\"tw_no_rows\"><td colspan=\"5\"><p align=\"center\" style=\"color: #FF0000;\"><strong>Список пуст</strong></p></td></tr>");
        }
        //add server query
    });
    $("a.tsremove").click(function () {
        $("tr.ts" + $(this).attr('class').split("_")[1]).remove();
        if ($(".tech_works tr").length == 0) {
            $(".tech_works").append("<tr class=\"tw_no_rows\"><td colspan=\"5\"><p align=\"center\" style=\"color: #FF0000;\"><strong>Список пуст</strong></p></td></tr>");
        }
        //add server query
    });

    // -------------------------------------
    $("#calctmc_tsk").submit(function () {
        if (!$("#savecalcok").prop("checked", true)) {
            alert("Для сохранения введенных данных, пожалуйста, установите галочку 'Сохранение отчета' и нажмите кнопку 'Сохранить' еще раз.");
            return false;
        }
        if (!confirm('Сохранить отчет? (Данные расчета заявки будут перезаписаны/обновлены) ПРОДОЛЖИТЬ?')) {
            return false;
        }
        $('body').css('cursor', 'wait');

        var cable = $("input[name='tmc_ids[]']").length;
        var ods = $("input[name=ods_id_inform]").val();
        var cp = $("input[name='cp[]']").val();
        var cs = $("tr[val='csr']").length;
        var tp = $("input[name='tp[]']").val();
        var ts = $("tr[val='tsr']").length;
        var tech_id = $("input[name=tech_id]").val();
        var calc_date = $("input[name=calc_date]").val();
        if (!cable || !tech_id) {
            alert('Пожалуйста, заполните форму Отчета целиком.');
            return false;
        }
        $.post(link2("connections/calc_tmc_ticket"), $("#calctmc_tsk").serializeArray())
            .success(function (data) {
                //alert(data);
                //return false;
                if (data.match(/error/)) {
                    $("body").css("cursor", "default");
                    alert("Ошибка на стороне сервера! Обратитесь к разработчикам!\r\n" + data);
                }
                if (data == "notfilled") {
                    $("body").css("cursor", "default");
                    alert("Информации недостаточно! Проверьте введенные данные!");
                }
                if (data == "ok") {
                    $("body").css("cursor", "default");
                    $.post(link2("connections/queryStatus"), {task_id: $("input[name=task_id]").val()})
                        .success(function (data) {
                            switch (data) {
                                case "error":
                                    alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                                    break;

                                case "none":
                                    alert("Текущий статус заявки не опознан! Обратитесь к разработчикам!");
                                    break;

                                case "report":
                                    if (confirm("Отчет по заявке сохранен! Изменить статус на Расчет ТМЦ?")) {
                                        var closedStatus_ID = "99";
                                        $(".float-panel50 ul").each(function () {
                                            $(this).style("background-color", "#9CFCCE", "important");
                                        });


                                        //$(".ticket-panels ul").css("background-color", $('input[name=closed_status_color]').val());
                                        //$(".cmm_title").css("background-color", $('input[name=closed_status_color]').val());
                                        $("body").css("cursor", "wait");
                                        $("#task_comment_loader").show();
                                        $.post("/comments/addcmm_ajax/", {
                                            task_id: $("input[name=task_id]").val(),
                                            cmm_text: 'Автоматическая смена статуса на Расчет ТМЦ при первом сохранении Отчета по Заявке.',
                                            tag: '',
                                            status_id: closedStatus_ID
                                        }, function (data) {
                                            $("#task_comment_loader").hide();
                                            if (data) {
                                                $(".task_comments").append(data);
                                                if (confirm("Закрыть форму Отчета?")) {
                                                    $("#calctmc").modal('hide');

                                                }
                                            } else {
                                                alert("Ошибка добавления комментария!");
                                            }
                                            $("body").css("cursor", "default");
                                            return false;
                                        });
                                    }

                                    break;

                                default:
                                    $("body").css("cursor", "wait");
                                    $("#task_comment_loader").show();
                                    $.post("/comments/addcmm_ajax/", {
                                        task_id: $("input[name=task_id]").val(),
                                        cmm_text: "Обновлен Отчет по заявке."
                                    }, function (data) {
                                        $("#task_comment_loader").hide();
                                        if (data) {
                                            $(".task_comments").append(data);
                                            if (confirm("Отчет по заявке сохранен! Закрыть форму Отчета?")) {
                                                $("#calctmc").modal('hide');

                                            }
                                        } else {
                                            alert("Ошибка добавления комментария!");
                                        }
                                        $("body").css("cursor", "default");
                                        return false;
                                    });

                                    break;
                            }
                            return false;
                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером. Обратитесь к разработчикам!');
                            return false;
                        });
                }
                return false;
            })
            .fail(function () {
                $("body").css("cursor", "default");
                alert('Ошибка обмена данными с сервером. Обратитесь к разработчикам!');
                return false;
            });
        return false;
    });

    // -------------------------------------
    $("#calc_tsk").submit(function () {
        if (!$("#savecalcok").prop("checked", true)) {
            alert("Для сохранения введенных данных, пожалуйста, установите галочку 'Сохранение отчета' и нажмите кнопку 'Сохранить' еще раз.");
            return false;
        }
        if (!confirm('Сохранить отчет? (Данные расчета заявки будут перезаписаны/обновлены) ПРОДОЛЖИТЬ?')) {
            return false;
        }
        $('body').css('cursor', 'wait');

        var cable = $("input[name='tmc_ids[]']").length;
        var ods = $("input[name=ods_id_inform]").val();
        var cp = $("input[name='cp[]']").val();
        var cs = $("tr[val='csr']").length;
        var tp = $("input[name='tp[]']").val();
        var ts = $("tr[val='tsr']").length;
        var tech_id = $("input[name=tech_id]").val();
        var calc_date = $("input[name=calc_date]").val();
        if (!cable || !ods || !tech_id || !calc_date || (!cp && !cs) || (!tp && !ts)) {
            alert('Пожалуйста, заполните форму Отчета целиком: п.п.1 - 4 и повторите.');
            return false;
        }
        $.post(link2("connections/calcticket"), $("#calc_tsk").serializeArray())
            .success(function (data) {
                //alert(data);
                //return false;
                if (data.match(/error/)) {
                    $("body").css("cursor", "default");
                    alert("Ошибка на стороне сервера! Обратитесь к разработчикам!\r\n" + data);
                }
                if (data == "notfilled") {
                    $("body").css("cursor", "default");
                    alert("Информации недостаточно! Проверьте введенные данные!");
                }
                if (data == "ok") {
                    $("body").css("cursor", "default");
                    $.post(link2("connections/queryStatus"), {task_id: $("input[name=task_id]").val()})
                        .success(function (data) {
                            switch (data) {
                                case "error":
                                    alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                                    break;

                                case "none":
                                    alert("Текущий статус заявки не опознан! Обратитесь к разработчикам!");
                                    break;

                                case "report":
                                    if (confirm("Отчет по заявке сохранен! Закрыть заявку?")) {
                                        var closedStatus_ID = $('input[name=closed_status_id]').val();
                                        $(".float-panel50 ul").each(function () {
                                            $(this).style("background-color", $('input[name=closed_status_color]').val(), "important");
                                        });


                                        //$(".ticket-panels ul").css("background-color", $('input[name=closed_status_color]').val());
                                        //$(".cmm_title").css("background-color", $('input[name=closed_status_color]').val());
                                        $("body").css("cursor", "wait");
                                        $("#task_comment_loader").show();
                                        $.post("/comments/addcmm_ajax/", {
                                            task_id: $("input[name=task_id]").val(),
                                            cmm_text: 'Автоматическая смена статуса при первом сохранении Отчета по Заявке.',
                                            tag: '',
                                            status_id: closedStatus_ID
                                        }, function (data) {
                                            $("#task_comment_loader").hide();
                                            if (data) {
                                                $(".task_comments").append(data);
                                                if (confirm("Закрыть форму Отчета?")) {
                                                    $("#calc").modal('hide');

                                                }
                                            } else {
                                                alert("Ошибка добавления комментария!");
                                            }
                                            $("body").css("cursor", "default");
                                            return false;
                                        });
                                    }

                                    break;
                                case "clctmc":
                                    if (confirm("Отчет по заявке сохранен! Закрыть заявку?")) {
                                        var closedStatus_ID = $('input[name=closed_status_id]').val();
                                        $(".float-panel50 ul").each(function () {
                                            $(this).style("background-color", $('input[name=closed_status_color]').val(), "important");
                                        });


                                        //$(".ticket-panels ul").css("background-color", $('input[name=closed_status_color]').val());
                                        //$(".cmm_title").css("background-color", $('input[name=closed_status_color]').val());
                                        $("body").css("cursor", "wait");
                                        $("#task_comment_loader").show();
                                        $.post("/comments/addcmm_ajax/", {
                                            task_id: $("input[name=task_id]").val(),
                                            cmm_text: 'Автоматическая смена статуса при сохранении Отчета по Заявке.',
                                            tag: '',
                                            status_id: closedStatus_ID
                                        }, function (data) {
                                            $("#task_comment_loader").hide();
                                            if (data) {
                                                $(".task_comments").append(data);
                                                if (confirm("Закрыть форму Отчета?")) {
                                                    $("#calc").modal('hide');

                                                }
                                            } else {
                                                alert("Ошибка добавления комментария!");
                                            }
                                            $("body").css("cursor", "default");
                                            return false;
                                        });
                                    }

                                    break;

                                default:
                                    $("body").css("cursor", "wait");
                                    $("#task_comment_loader").show();
                                    $.post("/comments/addcmm_ajax/", {
                                        task_id: $("input[name=task_id]").val(),
                                        cmm_text: "Обновлен Отчет по заявке."
                                    }, function (data) {
                                        $("#task_comment_loader").hide();
                                        if (data) {
                                            $(".task_comments").append(data);
                                            if (confirm("Отчет по заявке сохранен! Закрыть форму Отчета?")) {
                                                $("#calc").modal('hide');

                                            }
                                        } else {
                                            alert("Ошибка добавления комментария!");
                                        }
                                        $("body").css("cursor", "default");
                                        return false;
                                    });

                                    break;
                            }
                            return false;
                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером. Обратитесь к разработчикам!');
                            return false;
                        });
                }
                return false;
            })
            .fail(function () {
                $("body").css("cursor", "default");
                alert('Ошибка обмена данными с сервером. Обратитесь к разработчикам!');
                return false;
            });
        return false;
    });

    $.date_input.initialize();
    // uploading photos in task
    $(document).on("change", "input[name=filesource]", function () {
        //alert($(this).val());
        var id = $(this).attr("id");
        //var obj = $(this).parent();
        //alert(obj.html());
        /*var html = "<form style='display:none;' action=\"connections/addphoto\" id=\"uploader_"+id+"\" enctype=\"multipart/form-data\" method=\"POST\" target=\"uploadphotoiframe\"><input name=\"filesource1\" type=\"file\" value=\""+$(this).val()+"\" />" + $(this).parent().html() + "</form>";
         $(html).appendTo('body').submit();

         //$("#uploader_"+id).submit();
         $(".loader_"+id).show();
         $("#uploader_"+id).remove();
         */
        $(this).hide();
        $(this).prop("required", false);
        $(".loader_" + id).show();
        //var fileInput = document.querySelector('#file_'+id);

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/connections/addphoto/');

        xhr.upload.onprogress = function (e) {
            /*
             * values that indicate the progression
             * e.loaded
             * e.total
             */
        };

        xhr.onload = function (e) {
            //alert(e + 'upload complete');
            //alert(xhr.responseText);
            $(".loader_" + id).hide();

            try {
                var data = $.parseJSON(xhr.responseText);
                if (data.error) {
                    $(".fileparams_" + id).html("<strong style='color:#FF0000'>" + data.error + "</strong>")
                } else {
                    if (data.ok && data.filelink) {
                        if (data.cmm) {
                            $(".task_comments").append(data.cmm);
                        }
                        if (data.filelink) {
                            $(".fileparams_" + id).html(data.filelink);
                            $(".deluserfile_" + id).show();
                        } else {
                            $(".fileparams_" + id).html("<strong style='color:#FF0000'>ошибка</strong>")
                        }
                    } else {
                        $(".fileparams_" + id).html("<strong style='color:#FF0000'>ошибка</strong>")
                    }
                }
                reloadFileList();
            }

            catch (exeption) {
                $("body").css("cursor", "default");
                if (xhr.responseText.match(/login/ig)) {
                    $("body").html(xhr.responseText);
                } else {
                    alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                }
                return false;
            }
        };

        // upload success
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            // if your server sends a message on upload sucess,
            // get it with xhr.responseText
            //alert(xhr.responseText);
            //$(".loader_"+id).hide();
        }

        var form = new FormData();
        //form.append('title', this.files[0].name);
        form.append('pict', this.files[0]);
        form.append('file_type_id', $(this).parent().find("input[name=file_type_id]").val());
        form.append('task_id', $(this).parent().find("input[name=task_id]").val());
        xhr.send(form);
    });

    $(document).on("click", ".deluserfile", function () {
        var task_id = $(this).attr("task_id");
        var file_id = $(this).attr("filetype_id");
        if (!confirm("Удалить запись из списка фотоотчета по заявке? (При этом, файл будет сохранен)")) {
            return false;
        }
        $.post(link2('connections/deletephotofile/'), {task_id: task_id, filetype_id: file_id})
            .success(function (json) {
                try {
                    var data = $.parseJSON(json);
                    if (data.error) {
                        alert("Ошибка: " + data.error);
                    } else {
                        if (data.ok) {
                            $(".deluserfile_" + file_id).hide();
                            $("input[name=filesource]").each(function () {
                                if ($(this).attr("id") == file_id) {
                                    $(this).show();
                                    $(this).val('');
                                }
                            });
                            $(".fileparams_" + file_id).html('');
                            if (data.cmm) {
                                $(".task_comments").append(data.cmm);
                            }
                        }
                    }
                    return false;
                }

                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                    }
                    return false;
                }
            })
            .fail(function (e) {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });
        return false;

    });
    // --------------------------

    $("input[name=mgts_ats]").keyup(function () {
        if ($(this).val().length >= 3) {
            if ($(this).val().length > 3) {
                $(this).val($(this).val().substr(0, 3));
            }
            var obj = $(this);
            obj.prop("readonly", true);
            $.post(link2('adm_mgts_tc/getByATS_ajax'), {ats: obj.val()})
                .success(function (json) {
                    obj.prop("readonly", false);
                    try {
                        var data = $.parseJSON(json);
                        if (data.error) {
                            alert("Ошибка: " + data.error);
                        } else {
                            if (data.ok) {
                                $("select[name=tc_id] options").prop("selected", false);
                                $("select[name=tc_id]").val(data.id);

                            }
                        }
                        return false;
                    }

                    catch (exeption) {
                        $("body").css("cursor", "default");
                        if (json.match(/login/ig)) {
                            $("body").html(json);
                        } else {
                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                        }
                        return false;
                    }
                })
                .fail(function (e) {
                    alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                    $("body").css("cursor", "default");
                });
            return false;
        }
    });

});



// Addresses
$(document).ready(function(){

    $('#savechanges').click(function () {
        var v = $('[name=area_id]').val();
        $('[name=area_id_hidden]').val(v);
    });

    $("#cancel_edit_addr").click(function () {
        $("#goeditaddr").modal('hide');
    });

    $("#gosaveeditaddr").click(function () {
        var dom_id = $('input[name=dom_id]').val();
        var flats = $('input[name=ed_dom_flat]').val();
        var floors = $('input[name=ed_dom_floors]').val();
        var entrs = $('input[name=ed_dom_entr]').val();
        if (!dom_id) {
            alert('Не выбран адрес!');
            return false;
        }
        $("body").css("cursor", "wait");

        $.post(link2("addr_interface/updateAddrA"), {dom_id: dom_id, entr: entrs, floor: floors, flat: flats})
            .success(function (data) {
                if (entrs.toString() != $("#e_dom_entr").html() || floors.toString() != $("#e_dom_floor").html() || flats.toString() != $("#e_dom_flat").html()) {
                    var tComment = "Параметры адреса: ";
                    var myVar = tComment.concat(((entrs.toString() != $("#e_dom_entr").html()) ? ("Кол-во подъездов: " + $("#e_dom_entr").html() + " -> " + entrs + " ") : " "), ((floors.toString() != $("#e_dom_floor").html()) ? ("Кол-во этажей: " + $("#e_dom_floor").html() + " -> " + floors + " ") : " "), ((flats.toString() != $("#e_dom_flat").html()) ? ("Кол-во квартир: " + $("#e_dom_flat").html() + " -> " + flats + " ") : " "));
                    $("#task_comment_loader").show();
                    $.post("/comments/addcmm_ajax/", {
                        task_id: $("input[name=task_id]").val(),
                        cmm_text: myVar,
                        tag: "Обновлены данные"
                    }, function (data) {
                        $("#task_comment_loader").hide();
                        if (data) {
                            $(".task_comments").append(data);
                        } else {
                            alert("Ошибка добавления комментария!");
                        }
                        $("body").css("cursor", "default");
                    });
                    $("#e_dom_entr").html(entrs);
                    $("#e_dom_floor").html(floors);
                    $("#e_dom_flat").html(flats);
                } else {
                    $("body").css("cursor", "default");
                }
                $("#cancel_edit_addr").click();
            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            })
    });

    $("#go_edit_addr").click(function () {

        $("#ed_dom_entr").val($("#e_dom_entr").html() ? $("#e_dom_entr").html() : "");
        $("#ed_dom_floors").val($("#e_dom_floor").html() ? $("#e_dom_floor").html() : "");
        $("#ed_dom_flat").val($("#e_dom_flat").html() ? $("#e_dom_flat").html() : "");

        $("#goeditaddr").modal('show');

        return false;
    });

    $("#cancel_select_addr").click(function () {
        $("#goselectaddr").modal('hide');
        $("#go_select_addr").prop('disabled', false);
    });

    // button "Save" from modal
    $("#gosaveselectaddr").click(function () {

        var repo = $('.suggest-data-repository');

        var address = $('[name=set_addr]').val(),
            pod = $('[name=set_pod]').val(),
            etazh = $('[name=set_etazh]').val(),
            kv = $('[name=set_kv]').val(),
            domofon = $('[name=set_domofon]').val(),
            area = $('[name=area_id]').val(),

            full = repo.data('full'),
            kladr = repo.data('kladr'),
            house = repo.data('house'),
            is_not_recognized = repo.data('is_not_recognized'),
            latitude = repo.data('latitude'),
            longitude = repo.data('longitude'),
            change_marker = repo.data('changeMarker'),

            sc_id = repo.data('sc_id'); // separate method -> longer delay

        // screen
        $('#caddr').html('<strong>' + address + '</strong>');
        $('#inf_pod').html('<strong>' + pod + '</strong>');
        $('#inf_etazh').html('<strong>' + etazh + '</strong>');
        $('#inf_kv').html('<strong>' + kv + '</strong>');
        $('#inf_domofon').html('<strong>' + domofon + '</strong>');

        // post
        $('[name=address]').val(address);
        $('[name=pod]').val(pod);
        $('[name=etazh]').val(etazh);
        $('[name=kv]').val(kv);
        $('[name=domofon]').val(domofon);


        $.post(link2("addr_interface/getRecByCodeStringIDA_n"),
            {
                code: kladr,
                name: house,
                full_address: address,
                change_marker: change_marker,
                dom_id: $('[name=dom_id]').val()
            })
            .success(function (data) {

                var currentDom;
                if (data == "error") {
                    alert("Ошибка на стороне сервера! Обратитесь к разработчикам!");
                    return false;
                }

                // address found in list_addr
                if (data != "none") {
                    var addr = data.split("^");
                    currentDom = addr[0];

                    // ODS
                    if (addr[12]) {
                        $.post(link2("adm_ods/getODSParams"), {ods_id: addr[12]})
                            .success(function (data) {
                                if (data == "error") {
                                    $("body").css("cursor", "default");
                                    alert("Ошибка получения информации о выбранной ОДС! Обратитесь к разработчикам!");
                                    return false;
                                }
                                var ods = data.split("^");
                                $("#e_ods_name").html(ods[1]);
                                $("#e_ods_addr").html(ods[4] != "" ? ods[4] : "-");
                                $("#e_ods_tel").html(ods[3] != "" ? ods[3] : "-");
                                $("#e_ods_contact").html(ods[2] != "" ? ods[2] : "-");
                                $("#e_ods_comment").html(ods[5] != "" ? ods[5] : "-");
                                $("#go_edit_ods").prop('disabled', false);
                                $("body").css("cursor", "default");
                            })
                            .fail(function () {
                                $("body").css("cursor", "default");
                                alert('Ошибка обмена данными с сервером!');
                            });
                    }
                    else {
                        $("#go_edit_ods").prop('disabled', true);
                        $("#e_ods_name").html("Не Указана");
                        $("#e_ods_addr").html("-");
                        $("#e_ods_tel").html("-");
                        $("#e_ods_contact").html("-");
                        $("#e_ods_comment").html("-");
                    }

                    $("body").css("cursor", "wait");

                    $.post(link2("addr_interface/updateAddrInTaskA_n"),
                        {
                            task_id: $("input[name=task_id]").val(),
                            dom_id: currentDom,
                            pod: pod,
                            etazh: etazh,
                            kv: kv,
                            domofon: domofon,
                            area_id: area,
                            is_not_recognized: is_not_recognized
                        })
                        .success(function (data) {
                            if (data == "error") {
                                $("body").css("cursor", "default");
                                alert("Ошибка на стороне сервера! Обратитесь к разработчикам!");
                                return false;
                            }
                            else {
                                $("#task_comment_loader").show();
                                $.post("/comments/addcmm_ajax/", {
                                    task_id: $("input[name=task_id]").val(),
                                    cmm_text: address,
                                    tag: "Обновлен адрес"
                                }, function (data) {
                                    $("#task_comment_loader").hide();
                                    if (data) {
                                        $(".task_comments").append(data);
                                    } else {
                                        alert("Ошибка добавления комментария!");
                                    }
                                    $("body").css("cursor", "default");
                                });
                                $("#cancel_select_addr").click();
                            }
                            $("body").css("cursor", "default");
                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                            $("body").css("cursor", "default");
                        });

                }

                // address not found in list_addr
                else {
                    //  добавление нового адреса
                    $("body").css("cursor", "wait");
                    $.post(link2("addr_interface/insertAddrA_n"),
                        {
                            full: full,         // repository
                            address: address,   // input (different when unRecognised)
                            house: house,
                            kladr: kladr,
                            is_not_recognized: is_not_recognized,
                            latitude: latitude,
                            longitude: longitude,
                            area: area
                        })
                        .success(function (data) {

                            if (data != "error" && data != "none") {
                                currentDom = data;
                                $("body").css("cursor", "default");
                                $("body").css("cursor", "wait");
                                $.post(link2("addr_interface/updateAddrInTaskA_n"),
                                    {
                                        task_id: $("input[name=task_id]").val(),
                                        dom_id: currentDom,
                                        pod: pod,
                                        etazh: etazh,
                                        kv: kv,
                                        domofon: domofon,
                                        area_id: area,
                                        is_not_recognized: is_not_recognized
                                    })
                                    .success(function (data) {
                                        if (data == "error") {
                                            $("body").css("cursor", "default");
                                            alert("Ошибка на стороне сервера! Обратитесь к разработчикам!");
                                            return false;
                                        } else {
                                            $("#task_comment_loader").show();

                                            $.post("/comments/addcmm_ajax/", {
                                                task_id: $("input[name=task_id]").val(),
                                                cmm_text: address,
                                                tag: "Обновлен адрес"
                                            }, function (data) {
                                                $("#task_comment_loader").hide();
                                                if (data) {
                                                    $(".task_comments").append(data);
                                                } else {
                                                    alert("Ошибка добавления комментария!");
                                                }
                                                $("body").css("cursor", "default");
                                            });

                                            $("#cancel_select_addr").click();
                                        }
                                        $("body").css("cursor", "default");
                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                        $("body").css("cursor", "default");
                                    });

                            } else {
                                $("body").css("cursor", "default");
                                alert("Ошибка на стороне сервера! Обратитесь к разработчикам!");
                                return false;
                            }
                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                            $("body").css("cursor", "default");
                        });
                }

                $("body").css("cursor", "default");
            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });


        $("#go_select_addr").prop('disabled', false);

        $("#goselectaddr").modal('hide');

    });

    $("#go_select_addr").click(function () {

        $('.suggest-addr_full').keyup(function () {
            ////
        });

        $('.suggest-addr_full').val($('#caddr').text());

        $('body').css('cursor', 'default');
        $("#goselectaddr").modal('show');
        return false;
    });

});


function reloadWtypeList() {
    $.post(link2('kontr/getwtypelist/'), {contr_id: $('#cnt_id').val(), type: "connections"})
        .success(function (data) {
            if (data != "error") {
                $('#type_id').html(data);
                $('body').css('cursor', 'default');
                reloadAgrList();
            } else {
                alert('Ошибка загрузки договоров! Обратитесь к разработчикам!');
            }
        })
        .fail(function () {
            alert('Ошибка обмена данными!');
        });
}

function reloadAgrList() {
    $('body').css('cursor', 'wait');
    $.post(link2('kontr/getagrlist/'), {contr_id: $('#cnt_id').val(), wtype_id: $('#type_id').val()})
        .success(function (data) {
            if (data != "error") {
                $('#agr_id').html(data);
                $('body').css('cursor', 'default');
            } else {
                alert('Ошибка загрузки договоров! Обратитесь к разработчикам!');
            }
        })
        .fail(function () {
            alert('Ошибка обмена данными!');
        });
}




