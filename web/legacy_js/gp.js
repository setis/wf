var backup = false;
var b_code, b_region_title, b_street, b_althouse, b_pod, b_etazh, b_kv, b_domofon, b_k_region, b_k_area, b_k_city, b_k_np, b_k_house, b_area_id;

var ch_ods;
var ed_ods;
var ch_addr;
var ed_addr;
ch_ods = false;
ed_ods = false;
ch_addr = false;
ed_addr = false;

$(function () {
    var on = false;
    window.setInterval(function () {
        on = !on;
        if (on) {
            $('.invalid').addClass('invalid-blink')
        } else {
            $('.invalid-blink').removeClass('invalid-blink')
        }
    }, 500);
});
var on = false;

function reloadWtypeList() {
    $("#gochangecontr input").prop('disabled', true);
    $.post(link2('kontr/getwtypelist/'), {contr_id: $('#cnt_id').val(), type: "gp"})
        .success(function (data) {
            if (data != "error") {
                $('#type_id').html(data);
                $('body').css('cursor', 'default');
                reloadAgrList();
            } else {
                alert('Ошибка загрузки договоров! Обратитесь к разработчикам!');
            }
        })
        .fail(function () {
            alert('Ошибка обмена данными!');
        });
}

function reloadAgrList() {
    $('body').css('cursor', 'wait');
    $.post(link2('kontr/getagrlist/'), {contr_id: $('#cnt_id').val(), wtype_id: $('#type_id').val()})
        .success(function (data) {
            $("#gochangecontr input").prop('disabled', false);
            if (data != "error") {
                $('#agr_id').html(data);
                $('body').css('cursor', 'default');
            } else {
                alert('Ошибка загрузки договоров! Обратитесь к разработчикам!');
            }
        })
        .fail(function () {
            alert('Ошибка обмена данными!');
        });
}


function lockCalcDialog() {
    //$("#calc :input").prop('disabled', true);
}

function unLockCalcDialog() {
    //$("#calc :input").prop('disabled', false);
}

//Address
function rsetAddr() {
    $("#area_id [value='']").prop('selected', true);
    $("input[name='entr']").val('');
    $("input[name='floors']").val('');
    $("input[name='flats']").val('');
    $("input[name='addr']").val('0');
    $("input[name='entr']").prop('disabled', false);
    $("input[name='floors']").prop('disabled', false);
    $("input[name='flats']").prop('disabled', false);
    $("input[name='althouse']").prop('disabled', false);
    $("#area_id").prop('disabled', false);
    $("#ods_id").prop('disabled', false);
    $.post(link2('addr_interface/getRecByCodeString/'), {
        code: $('input[name="current_kladr_code"]').val(),
        name: $('input[name="k_house"]').html(),
        altname: $('input[name="althouse"]').val()
    }, function (data) {
        if (data) {
            //alert(data);
            var dataArr = data.split(",");
            if (dataArr[0] != '') {
                $(".althouse").prop('disabled', true);
                $(".althouse").val(dataArr[0]);
            } else {
                $(".althouse").prop('disabled', true);
            }
            if (dataArr[1] != '' && dataArr[1] != '0') {
                $("input[name='floors']").prop('disabled', true);
                $("input[name='floors']").val(dataArr[1]);
            }
            if (dataArr[2] != '' && dataArr[2] != '0') {
                $("input[name='flats']").prop('disabled', true);
                $("input[name='flats']").val(dataArr[2]);
            }
            if (dataArr[3] != '' && dataArr[3] != '0') {
                $("input[name='entr']").prop('disabled', true);
                $("input[name='entr']").val(dataArr[3]);
            }
            if (dataArr[5] != '' && dataArr[5] != '0') {
                ///$("#area_id").prop('disabled', true);
                $("#area_id [value='" + dataArr[5] + "']").attr('selected', 'selected');
            }
            if (dataArr[6] != '' && dataArr[6] != '0') {
                $("#ods_id").prop('disabled', true);
                $("#ods_id [value='" + dataArr[6] + "']").attr('selected', 'selected');
            }
            if (dataArr[7] != '' && dataArr[7] != '0') {
                $("input[name='addr']").val(dataArr[7]);

            }
        }
    });
}

function rsetForm() {
    $('body').css('cursor', 'wait');
    dval = $('input[name="default_code"]').val();
    $('input[name="current_kladr_code"]').val(dval);
    $(".fkr [value='" + dval + "']").attr("selected", "selected");
    $(".fkr").change();
    $.post(link2('addr_interface/getRegionTitle/'), {code: dval}, function (data) {
        if (data) {
            $(".current_region").html(data);

        } else {
            $(".current_region").html('Ошибка! Обратитесь к разработчикам!');

        }
        $('body').css('cursor', 'default');
    });
    reloadStreets();
}

function changeAcceptedRegion() {
    $('body').css('cursor', 'wait');
    $(".current_region").html('...');
    $('input[name="current_kladr_code"]').val($("input[name='skc']").val());
    $.post(link2('addr_interface/getRegionTitle/'), {code: $('input[name="current_kladr_code"]').val()}, function (data) {
        if (data) {
            $(".current_region").html(data);
            reloadStreets();
            codeChanged = true;
        } else {
            $(".current_region").html('Ошибка! Возврат к региону по-умолчанию...');
            rsetForm();
        }
        $('body').css('cursor', 'default');

    });
}

function reloadStreets() {
    $(".fstreet").prop('disabled', true).find('option').remove().end().append('<option value="">-- выберите улицу --</option>').val('');
    $(".fhouse").prop('disabled', true).find('option').remove().end().append('<option value="">-- выберите дом --</option>').val('');
    $('body').css('cursor', 'wait');
    $.post(link2('addr_interface/selStreetT/'), {region: $('input[name="current_kladr_code"]').val()}, function (data) {
        if (data) {
            $(".fstreet").html(data);
            $(".fstreet").prop('disabled', false);
            $('body').css('cursor', 'default');
            $(".fstreet").prop('required', true);

        }
    });
    $('body').css('cursor', 'wait');
    if ($("input[name='street']").val() != "") {

        //$("input[name='streetcode']").val('');
        $.post(link2('addr_interface/selHouseT/'), {region: $('input[name="current_kladr_code"]').val()}, function (data) {
            if (data) {
                $(".fhouse").html(data);
                $(".fhouse").prop('disabled', false);
                $('body').css('cursor', 'default');
                $(".althouse").prop('required', false);
                $(".fstreet").prop('required', false);
                $(".fhouse").prop('required', true);
                $('input[name=savetask]').prop('disabled', false);
            }
        });
    }
    $('body').css('cursor', 'default');

}


$(document).ready(function () {

    $(".floatingform").hide();
    $(".floatingform1").hide();


    $(function () {

        window.setInterval(function () {
            on = !on;
            if (on) {
                $('.invalid').addClass('invalid-blink')
            } else {
                $('.invalid-blink').removeClass('invalid-blink')
            }
        }, 500);
    });

    $.date_input.initialize();

    $("#save_change_contr").click(function () {
        var type_id = $("#type_id").val();
        var agr_id = $("#agr_id").val();
        var task_id = $("input[name=task_id]").val();
        if (type_id && agr_id && task_id) {
            $("body").css("cursor", "wait");
            $.post(link2('gp/updateContrDataA'), {task_id: task_id, type_id: type_id, agr_id: agr_id})
                .success(function (data) {
                    if (data == "ok") {

                        tComment = "";

                        if ($("#curr_work_val").html() != $("#type_id option:selected").text()) {
                            tComment += "Вид работ: " + $("#curr_work_val").html() + " -> " + $("#type_id option:selected").text() + "\r\n";
                        }
                        if ($("#curr_agr_val").html() != $("#agr_id option:selected").text()) {
                            tComment += "Договор: " + $("#curr_agr_val").html() + " -> " + $("#agr_id option:selected").text();
                        }
                        if (tComment) {
                            $("#curr_contr_val").html($("#cnt_id option:selected").text());
                            $("#curr_work_val").html($("#type_id option:selected").text());
                            $("#curr_agr_val").html($("#agr_id option:selected").text());

                            $("#task_comment_loader").show();
                            $.post("/comments/addcmm_ajax/", {
                                task_id: task_id,
                                cmm_text: tComment,
                                tag: "Обновлены данные"
                            }, function (data) {
                                $("#task_comment_loader").hide();
                                if (data) {

                                    $(".task_comments").append(data);

                                } else {
                                    alert("Ошибка добавления комментария!");
                                }
                                $("#gochangecontr").modal('hide');
                                $("body").css("cursor", "default");
                                alert('Изменения сохранены!');
                                location.reload();

                            });
                        } else {
                            alert("Изменения не внесены!");
                            $("body").css("cursor", "default");
                            $("#gochangecontr").modal('hide');
                        }


                    } else {
                        $("body").css("cursor", "default");
                        alert('Ошибка обновления информации о Контрагенте! Обратитесь к разработчикам!');
                    }

                    return false;
                })
                .fail(function () {
                    alert('Ошибка обмена данными с сервером. Обратитесь к разработчикам!');
                    $("body").css("cursor", "default");
                    $("#gochangecontr").modal('hide');
                });
        } else {
            alert("Пожалуйста, выберите Контрагента, Вид работ и Договор!");
        }
    });

    reloadWtypeList();

    $.date_input.initialize();



    // Address
    $("#go_select_addr").click(function () {
        $("#goselectaddr").modal('show');
        return false;
    });

    $("#cancel_select_addr").click(function () {
        $("#goselectaddr").modal('hide');
        $("#go_select_addr").prop('disabled', false);
    });

    // save address button
    $("#gosaveselectaddr").click(function () {
        var repo = $('.suggest-data-repository');

        var address = $('[name=set_addr]').val(),
            pod = $('[name=set_pod]').val(),
            etazh = $('[name=set_etazh]').val(),
            kv = $('[name=set_kv]').val(),
            domofon = $('[name=set_domofon]').val(),
            area = $('[name=area_id]').val(),

            full = repo.data('full'),
            kladr = repo.data('kladr'),
            house = repo.data('house'),
            is_not_recognized = repo.data('is_not_recognized'),
            latitude = repo.data('latitude'),
            longitude = repo.data('longitude'),
            change_marker = repo.data('changeMarker'),

            sc_id = repo.data('sc_id');

        // post
        $('[name=address]').val(address);
        $('[name=pod]').val(pod);
        $('[name=etazh]').val(etazh);
        $('[name=kv]').val(kv);
        $('[name=domofon]').val(domofon);


        $("body").css("cursor", "wait");

        $.post(link2("addr_interface/getRecByCodeStringIDA_GP"),
            {
                code: kladr,
                name: house,
                full_address: address,
                change_marker: change_marker,
                dom_id: $('[name=dom_id]').val()
            })
            .success(function (data) {

                if (data == "error") {
                    alert("Ошибка на стороне сервера! Обратитесь к разработчикам!");
                    return false;
                }

                if (data != "none") {
                    var addr = data.split("^");
                    $("body").css("cursor", "wait");

                    //JN AJAX - создание адреса, привязанного к заявке. Возвращает строку таблицы
                    $.post(link2("gp/createaddr_ajax"), {
                        task_id: $("input[name=task_id]").val(),
                        addr_id: addr["0"],
                        pod: pod,
                        domofon: domofon,
                        n_order_id: $('[data-order_id]').length
                    })
                        .success(function (data) {

                            $("body").css("cursor", "default");
                            if (data != "error") {
                                if (data.match(/login/ig)) {
                                    $("body").html(data);           //показать ошибку
                                    return false;
                                } else {
                                    $(".noaddrrows").remove();
                                    $(".addrtbody").append(data);   //добавить строку таблицы
                                }
                            } else {
                                alert("Ошибка обмена данными с сервером!");
                            }
                            $("#cancel_select_addr").click();
                            return false;
                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                            $("body").css("cursor", "default");
                        });
                }
                else {

                    $("body").css("cursor", "wait");
                    $.post(link2("addr_interface/insertAddrA_n"), {
                        full: full,         // repository
                        address: address,   // input (different when unRecognised)
                        house: house,
                        kladr: kladr,
                        is_not_recognized: is_not_recognized,
                        latitude: latitude,
                        longitude: longitude,
                        area: area
                    })
                        .success(function (data) {
                            if (data != "error" && data != "none") {
                                $("body").css("cursor", "wait");
                                $.post(link2("gp/createaddr_ajax"), {
                                    task_id: $("input[name=task_id]").val(),
                                    addr_id: data,
                                    pod: pod,
                                    domofon: domofon,
                                    n_order_id: $('[data-order_id]').length
                                })
                                    .success(function (data) {

                                        $("body").css("cursor", "default");
                                        if (data != "error") {
                                            if (data.match(/login/ig)) {
                                                $("body").html(data);
                                                return false;
                                            } else {
                                                $(".noaddrrows").remove();
                                                $(".addrtbody").append(data);
                                            }
                                        } else {
                                            alert("Ошибка обмена данными с сервером!");
                                        }
                                        $("#cancel_select_addr").click();
                                        return false;
                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                        $("body").css("cursor", "default");
                                    });
                            } else {
                                $("body").css("cursor", "default");
                                alert("Ошибка на стороне сервера! Обратитесь к разработчикам!");
                                return false;
                            }
                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                            $("body").css("cursor", "default");
                        });
                }
            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });
        $("#go_select_addr").prop('disabled', false);
    });

    reloadWtypeList();

    $("select[name=type_id]").change(function () {
        if ($(this).val() == '16') {
            $("textarea[name=addinfo]").attr("required", "required");
        } else {
            $("textarea[name=addinfo]").removeAttr("required");
        }
    });

    $(document).on("click", ".deleteaddr", function () {
        var id = $(this).attr("id");
        if (!confirm('Удаление адреса из заявки необратимо! Продолжить?')) {
            return false;
        }
        $("body").css("cursor", "wait");
        $.post(link2("gp/deleteaddr_ajax"), {id: id})
            .success(function (data) {
                $("body").css("cursor", "default");
                if (data != "error") {
                    if (data.match(/login/ig)) {
                        $("body").html(json);
                        return false;
                    } else {
                        $(".addrrow").each(function () {
                            if ($(this).attr("id") == "rid_" + id) {
                                $(this).remove();
                            }
                        });

                        if ($(".addrrow").length == 0) {
                            $(".addrtbody").append("<tr class='noaddrrows'><td colspan='8'><p style='text-align:center;' class='redmessage'>Нет записей!</p></td></tr>");
                        }
                    }
                } else {
                    alert("Ошибка обмена данными с сервером!");
                }
                $("#cancel_select_addr").click();
                return false;
            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });
    });


    $("#calc_tsk").submit(function () {

        if (!confirm('Сохранить отчет? (Данные расчета заявки будут перезаписаны/обновлены) ПРОДОЛЖИТЬ?')) {
            return false;
        }
        $('body').css('cursor', 'wait');

        var cable = $("input[name='tmc_ids[]']").length;
        var calc_date = $("input[name=calc_date]").val();
        $.post(link2("gp/calcticket"), $("#calc_tsk").serializeArray())
            .success(function (data) {
                if (data.match(/error/)) {
                    $("body").css("cursor", "default");
                    alert("Ошибка на стороне сервера! Обратитесь к разработчикам!\r\n" + data);
                }
                if (data == "notfilled") {
                    $("body").css("cursor", "default");
                    alert("Информации недостаточно! Проверьте введенные данные!");
                }
                if (data == "ok") {
                    $("body").css("cursor", "default");
                    $.post(link2("connections/queryStatus"), {task_id: $("input[name=task_id]").val()})
                        .success(function (data) {
                            switch (data) {
                                case "error":
                                    alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                                    break;

                                case "none":
                                    alert("Текущий статус заявки не опознан! Обратитесь к разработчикам!");
                                    break;

                                case "report":
                                    if (confirm("Отчет по заявке сохранен! Закрыть заявку?")) {
                                        var closedStatus_ID = $('input[name=closed_status_id]').val();
                                        $(".float-panel50 ul").each(function () {
                                            $(this).style("background-color", $('input[name=closed_status_color]').val(), "important");
                                        });


                                        //$(".ticket-panels ul").css("background-color", $('input[name=closed_status_color]').val());
                                        //$(".cmm_title").css("background-color", $('input[name=closed_status_color]').val());
                                        $("body").css("cursor", "wait");
                                        $("#task_comment_loader").show();
                                        $.post("/comments/addcmm_ajax/", {
                                            task_id: $("input[name=task_id]").val(),
                                            cmm_text: 'Автоматическая смена статуса при первом сохранении Отчета по Заявке.',
                                            tag: '',
                                            status_id: closedStatus_ID
                                        }, function (data) {
                                            $("#task_comment_loader").hide();
                                            if (data) {
                                                $(".task_comments").append(data);
                                                if (confirm("Закрыть форму Отчета?")) {
                                                    $("#calc").modal('hide');

                                                }
                                            } else {
                                                alert("Ошибка добавления комментария!");
                                            }
                                            $("body").css("cursor", "default");
                                        });
                                    }

                                    break;

                                default:
                                    $("body").css("cursor", "wait");
                                    $("#task_comment_loader").show();
                                    $.post("/comments/addcmm_ajax/", {
                                        task_id: $("input[name=task_id]").val(),
                                        cmm_text: "Обновлен Отчет по заявке."
                                    }, function (data) {
                                        $("#task_comment_loader").hide();
                                        if (data) {
                                            $(".task_comments").append(data);
                                            if (confirm("Отчет по заявке сохранен! Закрыть форму Отчета?")) {
                                                $("#calc").modal('hide');

                                            }
                                        } else {
                                            alert("Ошибка добавления комментария!");
                                        }
                                        $("body").css("cursor", "default");
                                    });

                                    break;
                            }
                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером. Обратитесь к разработчикам!');
                            return false;
                        });
                }
                return false;
            })
            .fail(function () {
                $("body").css("cursor", "default");
                alert('Ошибка обмена данными с сервером. Обратитесь к разработчикам!');
                return false;
            });
        return false;
    });


    $("#calc_ticket").click(function () {
        $("body").css("cursor", "wait");

        var el = $(this);

        $.post(link2('gp/checkTaskStatusA'), {task_id: $("input[name=task_id]").val()})
            .success(function (data) {
                if (data == "error") {
                    $("body").css("cursor", "default");
                    alert('Ошибка получения текущего статуса заявки! Обратитесь к разработчикам!');
                    return false;
                }

                if (data == "nostatus") {
                    $("body").css("cursor", "default");
                    alert('Текущий статус заявки не позволяет открыть форму ввода Отчета! Требуемый статус: Выполнена или Закрыта.');
                    return false;
                }
                if (data == "ok") {


                    ch_ods = false;
                    ed_ods = false;
                    $(".floatingform").hide();
                    $(".floatingform1").hide();

                    $('body').css('cursor', 'wait');

                    $.post(link2('gp/getCurrentCalcA'), {task_id: $("input[name=task_id]").val()})
                        .success(function (data) {

                            if (data == "error") {
                                alert('Ошибка получения расчета заявки! Обратитесь к разработчикам!');
                                $("body").css("cursor", "default");
                                return false;
                            } else {
                                $("body").css("cursor", "default");
                                $("#calc_content").html(data);
                                $(".date_input").datepicker();
                                $(".intonly").mask("?9999");

                                $(".onlynum").maskex('000');

                                // расчет ТМЦ

                                $("#add_tmc_to_task").click(function () {
                                    $('body').css('cursor', 'default');

                                    lockCalcDialog();
                                    $("#newtmccat_inform").val(0);
                                    $("#tmc_add_list").html("<tr id='tmc_norows'><td colspan='5'><p align='center' style='color: #FF0000;'><strong>Список пуст</strong></p></td></tr>");
                                    $("#addtmc_inform").modal('show');
                                });

                                $("#goaddtmctotask_inform").click(function () {
                                    $("#addtmc_inform").modal('hide');
                                    unLockCalcDialog();
                                });

                                $("#canceladdtmctotask_inform").click(function () {
                                    $("#addtmc_inform").modal('hide');
                                    unLockCalcDialog();
                                });

                                $("#newtmccat_inform").change(function () {
                                    $("body").css("cursor", "wait");
                                    if ($("#newtmccat_inform").val() == "" || $("#gp_ctech").val() == "") {
                                        return false;
                                    }
                                    $.post(link2('adm_materials/getListByCat'), {
                                        cat_id: $("#newtmccat_inform").val(),
                                        tech_id: $("#gp_ctech").val()
                                    })
                                        .success(function (data) {
                                            if (data == "error") {
                                                $("body").css("cursor", "default");
                                                alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                                                return false;
                                            } else {
                                                $("body").css("cursor", "default");
                                                $("#tmc_add_list").html(data);

                                                // tmc add

                                                $("a.tmc").click(function () {

                                                    var tmcId = $(this).attr('class').split("_")[1];
                                                    var maxVal_sn = $(this).attr('id');
                                                    var row_id = $(this).attr('tst_id');
                                                    var metric = $(this).hasClass('metric') ? true : false;
                                                    $(".tmc_no_rows").hide();
                                                    var tmcTitle = $(".tmctitle_" + tmcId).html();

                                                    if (!metric) {
                                                        if (!($(".tmc_list").find("." + row_id + "").length)) {
                                                            $(".tmc_list").append(
                                                                "<tr class='" + tmcId + " " + row_id + "'>" +
                                                                "   <td align='center'>" + tmcTitle + "</td>" +
                                                                "   <td align='center'>" + maxVal_sn + "</td>" +
                                                                "   <td align='center'>" +
                                                                /* какое то не внятное гавное */
                                                                "       <input type='hidden' name='item_tech_id_" + row_id + "' value='" + $("select#gp_ctech[name=tech_id]").val() + "' />" +
                                                                "       <input type='hidden' name='row_id[]' value='" + row_id + "' />" +
                                                                /* более внятное гавное */
                                                                "       <input type='hidden' name='writeoff_tmc[" + row_id + "][tech_id]' value='" + $("select#gp_ctech[name=tech_id]").val() + "' />" +
                                                                "       <input type='hidden' name='writeoff_tmc[" + row_id + "][row_id]' value='" + row_id + "' />" +
                                                                "       <input type='hidden' name='writeoff_tmc[" + row_id + "][sn]' value='" + maxVal_sn + "' />" +
                                                                "       <input type='hidden' name='writeoff_tmc[" + row_id + "][count]' value='1' />" +
                                                                "       <input type='hidden' name='writeoff_tmc[" + row_id + "][tmc_id]' value='" + tmcId + "' />" +
                                                                "       1" +
                                                                "   </td>" +
                                                                "   <td align='center'>" +
                                                                "       <a class='removetmc tmcid_" + row_id + "' href='#'>" +
                                                                "           <img src='/templates/images/cross.png' />" +
                                                                "       </a>" +
                                                                "   </td>" +
                                                                "</tr>");
                                                        } else {
                                                            $(".tmc_list").find("." + row_id + "").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
                                                            return false;
                                                        }
                                                    } else {
                                                        if (!($(".tmc_list").find("." + tmcId + "").length)) {
                                                            $(".tmc_list").append(
                                                                "<tr class='" + tmcId + " " + row_id + "'>" +
                                                                "   <td align='center'>" + tmcTitle + "</td>" +
                                                                "   <td align='center'>&mdash;</td>" +
                                                                "   <td align='center'>" +
                                                                /* еще одно не внятное гавное для метрических ТМЦ */
                                                                "       <input type='hidden' name='item_tech_id_" + row_id + "' value='" + $("select#gp_ctech[name=tech_id]").val() + "' />" +
                                                                "       <input type='hidden' name='row_id[]' value='" + row_id + "' />" +
                                                                "       <input type='hidden' name='tmc_ids[]' value='" + tmcId + "' />" +
                                                                "       <input type='hidden' name='maxval' value=" + maxVal_sn + " />" +
                                                                /* еще одно более внятное гавное для метрических ТМЦ */
                                                                "       <input type='hidden' name='writeoff_tmc[" + row_id + "][tech_id]' value='" + $("select#gp_ctech[name=tech_id]").val() + "' />" +
                                                                "       <input type='hidden' name='writeoff_tmc[" + row_id + "][row_id]' value='" + row_id + "' />" +
                                                                "       <input type='hidden' name='writeoff_tmc[" + row_id + "][max_val]' value='" + maxVal_sn + "' />" +
                                                                "       <input type='text' name='writeoff_tmc[" + row_id + "][count]' class='tmccount int' required='required' style='width:50px;' />" +
                                                                "       <input type='hidden' name='writeoff_tmc[" + row_id + "][tmc_id]' value='" + tmcId + "' />" +
                                                                "   </td>" +
                                                                "   <td align='center'>" +
                                                                "       <a class='removetmc tmcid_" + tmcId + "' href='#'>" +
                                                                "           <img src='/templates/images/cross.png' />" +
                                                                "       </a>" +
                                                                "   </td>" +
                                                                "</tr>");
                                                        } else {
                                                            $(".tmc_list").find("." + tmcId + "").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
                                                            return false;
                                                        }
                                                    }
                                                    $(".int").maskex('000000');
                                                    $(".tmccount").blur(function () {
                                                        var maxVal = $(this).prev("input[name=maxval]").val();
                                                        if (parseInt($(this).val()) > parseInt(maxVal)) {
                                                            $(this).val('');
                                                            alert('Нельзя списать ТМЦ в количестве, больше, чем есть на руках!');
                                                            throw {
                                                                name: 'Ошибка',
                                                                message: 'Нельзя списать ТМЦ в количестве, больше, чем есть на руках!'
                                                            };
                                                            return false;
                                                        }
                                                    });
                                                    $('a.removetmc').click(function () {
                                                        var tmcId = $(this).attr('class').split("_")[1];
                                                        $("tr." + tmcId).remove();
                                                        if ($(".tmc_list tr").length == 1) {
                                                            $(".tmc_no_rows").show();
                                                        }
                                                        return false;
                                                    });

                                                    return false;
                                                });

                                                //---------------
                                            }
                                        })
                                        .fail(function () {
                                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                            $("body").css("cursor", "default");
                                        })
                                });

                                // -------------------------------------------

                                $('a.removetmc').click(function () {
                                    var tmcId = $(this).attr('class').split("_")[1];
                                    //alert(tmcId);
                                    $("tr." + tmcId).remove();
                                    if ($(".tmc_list tr").length == 1) {
                                        $(".tmc_no_rows").show();
                                    }
                                    return false;
                                });

                                $("#calc").modal('show');
                                if ($("p.error").length > 0) {
                                    $(".hideifreadonly").hide();
                                }
                                return false;
                            }

                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                            $("body").css("cursor", "default");
                        });

                    return false;
                }
            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });


    });
    $("#cancel_calc").click(function () {
        /*if ($(".r_tmc_list").find(".newrtmcid").length > 0) {
         if (!confirm('Вы выполнили возврат ТМЦ по заявке. Данные ТМЦ будут сохранены в любом случае. Продолжить?')) {
         return false;
         }
         }*/
        $("#calc").modal('hide');


    });

    $("#gosavecalc").click(function () {
        $("#calc").modal('hide');

    });

    //  возврат ТМЦ по заявке
    $(document).on("click", "#return_tmc_to_task", function () {
        $('body').css('cursor', 'default');
        lockCalcDialog();

        $("#add_r_tmc_inform").modal('show');
        $("#add_r_tmc_inform form").find("input[type=text]").val('');
        $("#add_r_tmc_inform form").find("input[type=text]").prop('disabled', false);

        $("#add_r_tmc_inform form").find("select").each(function () {
            $("option", $(this)).each(function () {
                $(this).removeAttr("selected");
            });
            if (!$(this).attr("multiple")) {
                $("option:first", $(this)).attr("selected", "selected");
            }
        });
        $("select[name=r_tmc_name_id]").html("<option value='0'>-- выберите наименование --</option>");

    });

    $(document).on("click", "#cancel_r_tmctotask_inform", function () {
        $("#add_r_tmc_inform").modal('hide');

        unLockCalcDialog();

    });

    $("input[name=new_tmc_name]").keyup(function () {
        if ($(this).val() != "") {
            $("select[name=r_tmc_name_id]").prop('disabled', true);
        } else {
            $("select[name=r_tmc_name_id]").prop('disabled', false);
        }
    });

    $("#r_tmccat_inform").change(function () {
        $("body").css("cursor", "wait");
        $.post(link2('adm_materials/getOptionListByCat_ajax'), {cat_id: $(this).val()})
            .success(function (data) {
                if (data == "error") {
                    $("body").css("cursor", "default");
                    alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                    return false;
                } else {
                    $("body").css("cursor", "default");
                    $("select[name=r_tmc_name_id]").html(data);
                }
            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            })
    });

    $("select[name=r_tmc_name_id]").change(function () {
        if ($(this).val() != "" && $(this).val() != "0") {
            $("input[name=new_tmc_name]").prop('disabled', true);
            $("input[name=new_tmc_barcode]").prop('disabled', true);

        } else {
            $("input[name=new_tmc_name]").prop('disabled', false);
            $("input[name=new_tmc_barcode]").prop('disabled', false);
        }
    });


    $("#return_tmc").submit(function () {
        if ($("input[name=new_tmc_serial]").val() == "" || $(".sel_r_tmccatinform").val() == '' || $("select[name=r_tmccat_inform]").val() == "" || $("select[name=r_tmc_name_id]").val() == "") {
            alert("Пожалуйста, укажите категорию ТМЦ, наименование из справочника и серийный номер!");
            return false;
        }
        var currentSerial = $("input[name=new_tmc_serial]").val();
        var gp_r_exists = false;
        $(".r_tmc_list tr").each(function () {
            var ser = $(this).children("td").eq(1).find("input[type='text']").length > 0 ? $(this).children("td").eq(1).find("input[type='text']").val() : $(this).children("td").eq(1).html();
            if (ser == currentSerial) {
                gp_r_exists = true;
                return false;
            }
        });
        if (gp_r_exists) {
            alert('ТМЦ с указанным серийным номером уже принято по данной заявке! Пожалуйста, укажите уникальный серийный номер.');
            return false;
        }
        $.post(link2('gp/return_tmc_from_ticktet'), $(this).serializeArray())
            .success(function (json) {
                try {
                    var data = $.parseJSON(json);
                    //alert(data.addinfo);
                    $("body").css("cursor", "default");
                    if (data.error) {
                        $("body").css("cursor", "default");
                        alert('Ошибка на стороне сервера: ' + data.error);
                        return false;
                    } else {
                        $("body").css("cursor", "default");
                        if (data.tpl) {
                            if (data.comment) {
                                $(".task_comments").append(data.comment);
                            }
                            $(".r_tmc_no_rows").hide();
                            $(".r_tmc_list").append(data.tpl);
                            $("#add_r_tmc_inform").modal('hide');

                            unLockCalcDialog();
                        } else {
                            alert("Ошибка возврата заявки!");
                        }
                    }
                    return false;
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert(json_parse_error + " " + exeption);
                    }

                    return false;
                }


            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });
        return false;
    });

    $(document).on("click", ".removertmc", function () {
        var obj = $(this);
        var gp_rtmc_exists = false;
        var currentSerial = $(".rtmc_" + obj.attr("id")).children("td").eq(1).find("input[type='text']").length > 0 ? $(".rtmc_" + obj.attr("id")).children("td").eq(1).find("input[type='text']").val() : $(".rtmc_" + obj.attr("id")).children("td").eq(1).html();
        $(".tmc_list tr").each(function () {
            var ser = $(this).children("td").eq(1).find("input[type='text']").length > 0 ? $(this).children("td").eq(1).find("input[type='text']").val() : $(this).children("td").eq(1).html();
            if (ser == currentSerial) {
                gp_rtmc_exists = true;
                return false;
            }
        });
        if (gp_rtmc_exists) {
            alert("Вы пытаетесь удалить принятое по данной заявке ТМЦ, предварительно списав его на эту же заявку! Удалить такое ТМЦ нельзя. Удалите запись о списании на заявку и повторите.");
            return false;
        }
        if (!confirm("Удаление записи приведет к откату списания данного ТМЦ с заявки. Продолжить?")) {
            return false;
        }
        var rmtmcname;
        if ($(this).attr("wasnew")) {
            if (confirm("Удалить вид ТМЦ, созданный при списании?")) {
                rmtmcname = "1";
            } else {
                rmtmcname = "0";
            }
        } else {
            rmtmcname = "0";
        }
        var task_id = $(this).attr("task_id");
        var row_id = $(this).attr("id");
        $.post(link2('gp/delete_returned_tmc_from_ticket'), {row_id: row_id, task_id: task_id, and_tmc: rmtmcname})
            .success(function (json) {
                try {
                    var data = $.parseJSON(json);
                    $("body").css("cursor", "default");
                    if (data.error) {
                        $("body").css("cursor", "default");
                        alert('Ошибка на стороне сервера: ' + data.error);
                        return false;
                    } else {
                        $("body").css("cursor", "default");
                        if (data.ok) {
                            if (data.comment) {
                                $(".task_comments").append(data.comment);
                            }
                            $(".rtmc_" + row_id).remove();
                            if ($(".r_tmc_rows").length == 0) {
                                $(".r_tmc_no_rows").show();
                            }
                        } else {
                            alert("Ошибка отката возврата по заявке!");
                        }
                    }
                    return false;
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert(json_parse_error + " " + exeption);
                    }

                    return false;
                }


            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });
        return false;
    });

});
