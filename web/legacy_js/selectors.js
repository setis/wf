var json_parse_error = "Ошибка разбора ответа сервера";


function selUserMultiple(callback, task_id, allusers) {
    
    $(".no_users").hide();
    $(".loader_seluser_mult").show();
    $(".users_data_row").remove();
    $(".user_search_pages").html("&mdash;");
    $(".user_search_total").html("0");
    $("#fs_seluser_mult").val(0);
    var seluserids;
    var seluserfios;
    $(".user_data_row").each(function()  {
        $(this).remove();  
    });
    $("#selectuser_mult_form input[type='reset']").click();
    var utypes = new Array("все", "Пользователь", "Пользователь ЛК", "Техник", "Руководитель СЦ");
     
 
        $(".su_utype").remove();
        $("input[name=su_type_id1]").remove();
        $("select[name=su_type_id]").show();
        $("select[name=su_type_id]").prop("disabled", false);
      
    
          
    
    
    $(document).on("click", ".removeseluseritem", function() {
        $(this).parents("tr.seluser_row:first").remove();
        if ($("tr.seluser_row").length == 0) {
            $(".seluser_mult_norows").show();    
        }
        return false;
    });
    $(document).on("click", ".selectuser_link", function() {
        $(".seluser_mult_norows").hide();
        var disAllow = false;
        var currentID = $(this).attr("id");
        $("tr.seluser_row").each(function() {
            if ($(this).attr("id") == currentID) {
                disAllow = true;
            }
        });
        if (!disAllow) {
            $("#seluser_mult_list").append("<tr id='"+$(this).attr("id")+"' class='seluser_row'><td class='sunamepos'>"+$(this).attr("username")+"\r\n (" + $(this).attr("position")+")</td><td><a href='#' id='"+$(this).attr("id")+"' class='removeseluseritem'><img src='/templates/images/cross.png' title='Удалить' /></a></td></tr>")
        }
        return false;
    });
    
    $("input[name=su_username]").change(function() {
        $("#fs_seluser_mult").val(0);
         $("#selectuser_mult_form").submit();
    });
    
    $("select[name=su_type_id]").change(function() {
        $("#fs_seluser_mult").val(0);
         $("#selectuser_mult_form").submit();
    });
    
    $("select[name=su_podr_id]").change(function() {
        $("#fs_seluser_mult").val(0);
         $("#selectuser_mult_form").submit();
    });
    
    $("input[name=su_archive]").click(function() {
        $("#fs_seluser_mult").val(0);
         $("#selectuser_mult_form").submit();
    });
    
    $("button[name='clear_seluser_mult']").click(function() {
        $(".seluser_row").remove();
        $(".seluser_mult_norows").show(); 
    });
    
    $("button[name='send_seluser_mult_list']").unbind().on("click", function() {
        var item_ids = [];
        var item_names = [];
        $(document).off("click", "input[name='send_seluser_mult_list']");
        $(document).off("click", ".selectuser_link");
        if ($(".seluser_row").length>0) {
            $(".seluser_row").each(function() {
                 item_ids.push($(this).attr("id"));
                 item_names.push($(this).find("td.sunamepos").html());
            });
            callback.call(item_ids.join(",") + "#" + item_names.join("<br />") );
    
            $( "#seluser_mult" ).modal('hide');
        } else {
            alert("Для продолжения, - необходимо выбрать хотя бы одного пользователя!");
            return false;
        }
        return false; 
    });
    
    
    $("select[name=su_sc_id]").change(function() {
        $("#fs_seluser_mult").val(0);
        if ($(this).val()>0) {
            $("select[name=su_type_id] option").each(function() {
                if ($(this).val()==3) {
                    $(this).prop("selected", true);
                } else {
                    $(this).prop("selected", false);
                }
            });
            
        }
         $("#selectuser_mult_form").submit();
    });
    $("body").css("cursor", "wait");       
    //$(".seluser_row").remove();
    var link;
    if (allusers) {
        link = "all_users_by_task_ajax";
    } else {
        link = "users_by_task_ajax";
        
    }
    $("input[name=filter_start]").val(0); 
    if (task_id) {
    $.post(link2("browse/"+link), {task_id: task_id})
            .success(function(json) {
                
                $("body").css("cursor", "default");
                try 
                {
                    var data = $.parseJSON( json );
                    if (data.error) {
                        alert("Ошибка на стороне сервера: " + data.error);
                        return false;
                    } else {
                        if (data.tpl) {
                            $(".seluser_row").remove();
                            $(".seluser_mult_tbody").append(data.tpl);
                            $(".seluser_mult_norows").hide();     
                        }
                        return false;
                    }
                     
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert(json_parse_error + " " +  exeption);    
                    }
                    
                    return false;
                }
                return false;
            })
            .fail(function() {
                alert(ajax_fail+ " searchusers_ajax");
                $("body").css("cursor", "default");
                return false;

            }); 
            
      }             
        $.post(link2("browse/users_ajax?small=1"), $("#selectuser_mult_form").serializeArray())
            .success(function(json) {
                //alert(json);
                try 
                {
                    var data = $.parseJSON( json );
                    if (data.error) {
                        alert("Ошибка на стороне сервера: " + data.error);
                        $("body").css("cursor", "default");
                        return false;
                    } else {
                        $( "#seluser_mult" ).show();
                         $(".user_data_row").remove();
                        if (data.total !=0) {
                            $(".user_search_pages").html(data.pages);
                            $(".user_search_total").html(data.total);
                            
                             
                            $(".loader_seluser_mult").hide();
                            $(".no_users").hide();
                            $(".searchuser").append(data.tpl);
                            $("body").css("cursor", "default");                    
                        } else {                        
                            $(".user_search_pages").html("&mdash;");
                            $(".user_search_total").html("0");
                            $(".user_data_row").remove();
                            $(".no_users").show();
                            $(".loader_seluser_mult").hide();
                            $("body").css("cursor", "default");
                        }
                         
                         
                        $( "#seluser_mult" ).modal('show');
                        $("input[name=su_username]").blur();
                        $("input[name=su_username]").focus();
                        return false;
                    }
                    $("body").css("cursor", "default");
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert(json_parse_error + " " +  exeption);    
                    }
                    
                    return false;
                }
                return false;
            })
            .fail(function() {
                alert(ajax_fail+ " searchusers_ajax");
                $("body").css("cursor", "default");
                return false;

            }); 
            
        $("#selectuser_mult_form button[type=reset]").click(function() {
                $(this).parents("form").find("input[type=text]").val('');
                $(this).parents("form").find("select").each(function(){
                    $("option", $(this)).each(function() {
                        $(this).removeAttr("selected");  
                    });
                    $("option:first", $(this)).attr("selected", "selected");
                });
                $(this).parents("form").find("input[type='checkbox']").each(function(){
                    $(this).prop('checked', false);
                });
                $(this).parents("form").submit();
                
             $("input[name=su_username]").blur();
             $("input[name=su_username]").focus();
             $("#selectuser_mult_form").submit();
             return false; 
            
        });
    
        $("#selectuser_mult_form").submit(function() {
            
            $("body").css("cursor", "wait");
             
            $(".user_data_row").remove();
            $(".no_search").hide();
            $(".no_users").hide();
            $(".loader_seluser_mult").show();
            $.post(link2("browse/users_ajax?small=1"), $(this).serializeArray())
            .success(function(json) {

                try 
                {
                    var data = $.parseJSON( json );

                    if (data["error"]) {
                        alert("Ошибка на стороне сервера: " + data.error);
                        $("body").css("cursor", "default");
                        return false;
                    } else {
                        
                        if (data.total !=0) {
                            $(".user_search_pages").html(data.pages);
                            $(".user_search_total").html(data.total);
                            
                            $(".user_data_row").remove();
                            $(".loader_seluser_mult").hide();
                            $(".no_users").hide();
                            $(".searchuser").append(data.tpl);
                            $("body").css("cursor", "default");  
                                          
                        } else {                        
                            $(".user_search_pages").html("&mdash;");
                            $(".user_search_total").html("0");
                            $(".user_data_row").remove();
                            $(".no_users").show();
                            $(".loader_seluser_mult").hide();
                            $("body").css("cursor", "default");
                        }
                        return false;
                    }
                    $("body").css("cursor", "default");
                    return false;
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert(json_parse_error + " " +  exeption);    
                    }
                    
                    return false;
                }
                
                return false;
            })
            .fail(function() {
                alert(ajax_fail + " searchusers_ajax");
                $("body").css("cursor", "default");
                return false;
            });
            return false;
            
        });

        $("input[name=su_username]").blur();
        $("input[name=su_username]").focus();
        
        return false;                
}



function selUser(callback, callerID, type, sc_id, sc_name, ignore_sc, with_roles) {
    $(".no_users").hide();
    $(".loader_seluser").show();
    $(".users_data_row").remove();
    $(".user_search_pages").html("&mdash;");
    $(".user_search_total").html("0");
    $("#fs_seluser").val(0);
    
    $(".user_data_row").each(function()  {
        $(this).remove();  
    });
    $("#selectuser_form input[type='reset']").click();
    var utypes = ["все", "Пользователь", "Пользователь ЛК", "Техник", "Руководитель СЦ"];
    if (with_roles) {
        $("#selectuser_form").append("<input type='hidden' name='with_roles' value='1' />");
    }
    if (ignore_sc) {
        $("select[name=su_sc_id]").prop("disabled", true);
        $(".su_sc_list").hide();
    } else {
        $("select[name=su_sc_id]").prop("disabled", false);
        $(".su_sc_list").show();
        if (sc_id && sc_name) {
            $(".su_sctype").remove();
            type=3;
            $("select[name=su_sc_id] option").each(function() {
                if ($(this).val() == sc_id) {
                    $(this).prop("selected", true);
                } else {
                    $(this).prop("selected", false);
                }
            });
            $("select[name=su_sc_id]").prop("disabled", true);
            $("select[name=su_sc_id]").hide();
            $("select[name=su_sc_id]").parent("li:first").append("<strong class='su_sctype'>"+sc_name+"</strong>");
            $("#selectuser_form").append("<input type='hidden' name='su_sc_id1' value='" + sc_id + "' />");
        } else {
            $(".su_sctype").remove();
            $("input[name=su_sc_id1]").remove();
            $("select[name=sc_type_id]").show();
            $("select[name=sc_type_id]").prop("disabled", false);
        }
    }
    if (type) {
        $(".su_utype").remove();
        $("select[name=su_type_id] option").each(function() {
            if ($(this).val() == type) {
                $(this).prop("selected", true);
            } else {
                $(this).prop("selected", false);
            }
        });
        $("select[name=su_type_id]").prop("disabled", true);
        $("select[name=su_type_id]").parents("label:first").hide();
        $("select[name=su_type_id]").parent("li:first").append("<strong class='su_utype'>"+utypes[type]+"</strong>");

        $("#selectuser_form").append("<input type='hidden' name='su_type_id1' value='" + type + "' />");
        
    } else {
        $(".su_utype").remove();
        $("input[name=su_type_id1]").remove();
        $("select[name=su_type_id]").parents("label:first").show();
        $("select[name=su_type_id]").prop("disabled", false);
    }  
    
     
    $(document).on("click", ".selectuser_link", function() {
        $(document).off("click", ".selectuser_link");
        callback.call($(this).attr("id") + "#" + $(this).attr("username"));
        $( "#seluser" ).modal('hide');
        return false;
    });
    
    $("input[name=su_username]").change(function() {
        $("#fs_seluser").val(0);
         $("#selectuser_form").submit();
    });
    
    $("select[name=su_type_id]").change(function() {
        $("#fs_seluser").val(0);
         $("#selectuser_form").submit();
    });
    
    $("select[name=su_podr_id]").change(function() {
        $("#fs_seluser").val(0);
         $("#selectuser_form").submit();
    });
    
    $("input[name=su_archive]").click(function() {
        $("#fs_seluser").val(0);
         $("#selectuser_form").submit();
    });
    $("select[name=su_sc_id]").change(function() {
        $("#fs_seluser").val(0);
        if ($(this).val()>0) {
            $("select[name=su_type_id] option").each(function() {
                if ($(this).val()==3) {
                    $(this).prop("selected", true);
                } else {
                    $(this).prop("selected", false);
                }
            });
            
        }
         $("#selectuser_form").submit();
    });
    $("body").css("cursor", "wait");
        var url = link2("browse/users_ajax");
        if (with_roles) {
            url += '?with_roles=1'
        }
        $.post(url, $("#selectuser_form").serializeArray())
            .success(function(json) {
                //alert(json);
                try 
                {
                    var data = $.parseJSON( json );
                    if (data["error"]) {
                        alert("Ошибка на стороне сервера: " + data.error);
                        $("body").css("cursor", "default");
                        return false;
                    } else {
                        $( "#seluser" ).show();
                        if (data.total !=0) {
                            $(".user_search_pages").html(data.pages);
                            $(".user_search_total").html(data.total);
                            
                            $(".user_data_row").remove();
                            $(".loader_seluser").hide();
                            $(".no_users").hide();
                            $(".searchuser").append(data.tpl);
                            $("body").css("cursor", "default");                    
                        } else {                        
                            $(".user_search_pages").html("&mdash;");
                            $(".user_search_total").html("0");
                            $(".user_data_row").remove();
                            $(".no_users").show();
                            $(".loader_seluser").hide();
                            $("body").css("cursor", "default");
                        }
                        $( "#seluser" ).modal('show');
                        return false;
                    }
                    $("body").css("cursor", "default");
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert(json_parse_error + " " +  exeption);    
                    }
                    
                    return false;
                }
                return false;
            })
            .fail(function() {
                alert(ajax_fail+ " searchusers_ajax");
                $("body").css("cursor", "default");
                return false;

            }); 
            
        $(document).on("click", "#selectuser_form input[type=reset]", function() {
                $(this).parents("form").find("input[type=text]").val('');
                $(this).parents("form").find("select").each(function(){
                    $("option", $(this)).each(function() {
                        $(this).removeAttr("selected");  
                    });
                    $("option:first", $(this)).attr("selected", "selected");
                });
                $(this).parents("form").find("input[type='checkbox']").each(function(){
                    $(this).prop('checked', false);
                });
                $(this).parents("form").submit();
                
             $("input[name=su_username]").blur();
             $("input[name=su_username]").focus();
             $("#selectuser_form").submit();
             return false; 
            
        });
    
        $("#selectuser_form").submit(function() {
            
            $("body").css("cursor", "wait");
             
            $(".user_data_row").remove();
            $(".no_search").hide();
            $(".no_users").hide();
            $(".loader_seluser").show();
            $.post(link2("browse/users_ajax"), $(this).serializeArray())
            .success(function(json) {

                try 
                {
                    var data = $.parseJSON( json );

                    if (data["error"]) {
                        alert("Ошибка на стороне сервера: " + data.error);
                        $("body").css("cursor", "default");
                        return false;
                    } else {
                        
                        if (data.total !=0) {
                            $(".user_search_pages").html(data.pages);
                            $(".user_search_total").html(data.total);
                            
                            $(".user_data_row").remove();
                            $(".loader_seluser").hide();
                            $(".no_users").hide();
                            $(".searchuser").append(data.tpl);
                            $("body").css("cursor", "default");  
                                          
                        } else {                        
                            $(".user_search_pages").html("&mdash;");
                            $(".user_search_total").html("0");
                            $(".user_data_row").remove();
                            $(".no_users").show();
                            $(".loader_seluser").hide();
                            $("body").css("cursor", "default");
                        }
                        return false;
                    }
                    $("body").css("cursor", "default");
                    return false;
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert(json_parse_error + " " +  exeption);    
                    }
                    
                    return false;
                }
                
                return false;
            })
            .fail(function() {
                alert(ajax_fail + " searchusers_ajax");
                $("body").css("cursor", "default");
                return false;
            });
            return false;
            
        }); 
    
    $("#cancel_seluser").click(function() {
        $("#seluser").hide();
        unLockDialog();
        return false;
    });
    
    /*$('html, body').animate({
        scrollTop: (parseInt($("#seluser").offset().top - $("#seluser").height()/2))}, 100);
        $("h2.float-title").disableSelection();*/

        $("input[name=su_username]").blur();
        $("input[name=su_username]").focus();
        
        return false;                
}

function selSKPWTypes(callback, callerID, contr_id, donotlock) {
    console.log(callback, callerID, contr_id, donotlock);
    $(".no_skpwtypes").hide();
    $(".loader_skpwtypes").show();
    $(".skp_data_row").remove();
    $(".skpwtypes_search_pages").html("&mdash;");
    $(".skpwtypes_search_total").html("0");
    $("#fs_skpwtype").val(0);
    $("input[name=skpwtype_contr_id]").val(contr_id);
    $(".skp_data_row").each(function()  {
        $(this).remove();  
    });
    $("#selectuser_form input[type='reset']").click();
    $("input[name=filter_id]").blur();
    $("input[name=filter_id]").focus();
     
    
    $("input[name=filter_id]").change(function() {
         $("#skpwtype_form").submit();
    });
     $("input[name=filter_name]").change(function() {
         $("#skpwtype_form").submit();
    });
    
    $(document).on("click", ".selectskpwt_link", function() {
        $(document).off("click", ".selectskpwt_link");
        $( "#selskpwtype" ).modal('hide');
        callback.call($(this).attr("id") + "#" + $(this).attr("wtypetitle")+"#"+$(this).attr("wtypeminval")+"#"+$(this).attr("wtypeprice")+"#"+$(this).attr("wtypecode"));
        return false;
    });
    
     
     
    $("body").css("cursor", "wait");                        
        $.post(link2("adm_skp_price/browse_wtypes"), $("#skpwtype_form").serializeArray())
            .success(function(json) {
                //alert(json);
                try 
                {
                    var data = $.parseJSON( json );
                    //alert(data.sql);
                    if (data["error"]) {
                        alert("Ошибка на стороне сервера: " + data.error);
                        $("body").css("cursor", "default");
                        return false;
                    } else {
                        $("input[name=filter_id]").blur();
                        $("input[name=filter_id]").focus();
                        if (data.total !=0) {
                            $(".skpwtypes_search_pages").html(data.pages);
                            $(".skpwtypes_search_total").html(data.total);
                            
                            $(".skp_data_row").remove();
                            $(".loader_skpwtypes").hide();
                            $(".no_skpwtypes").hide();
                            $(".searchskpwtypes").append(data.tpl);
                            $("body").css("cursor", "default");                    
                        } else {                        
                            $(".skpwtypes_search_pages").html("&mdash;");
                            $(".skpwtypes_search_total").html("0");
                            
                            $(".skp_data_row").remove();
                            $(".no_skpwtypes").show();
                            $(".loader_skpwtypes").hide();
                            $("body").css("cursor", "default");
                        }
                         
                        $( "#selskpwtype" ).modal('show');
                        return false;
                    }
                    $("body").css("cursor", "default");
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert(json_parse_error + " " +  exeption);    
                    }
                    
                    return false;
                }
                return false;
            })
            .fail(function() {
                alert(ajax_fail+ " browse_wtypes");
                $("body").css("cursor", "default");
                return false;

            }); 
            
        $("#skpwtype_form input[type=reset]").click(function() {
                $(this).parents("form").find("input[type=text]").val('');
                $(this).parents("form").find("select").each(function(){
                    $("option", $(this)).each(function() {
                        $(this).removeAttr("selected");  
                    });
                    $("option:first", $(this)).attr("selected", "selected");
                });
                $(this).parents("form").find("input[type='checkbox']").each(function(){
                    $(this).prop('checked', false);
                });
                $(this).parents("form").submit();
                
             
             $("#skpwtype_form").submit();
             return false; 
            
        });
    
        $("#skpwtype_form").submit(function() {
            
            $("body").css("cursor", "wait");
             
            $(".skp_data_row").remove();
            $(".no_search").hide();
            $(".no_skpwtypes").hide();
            $(".loader_skpwtypes").show();
            $.post(link2("adm_skp_price/browse_wtypes"), $(this).serializeArray())
            .success(function(json) {

                try 
                {
                    var data = $.parseJSON( json );

                    if (data["error"]) {
                        alert("Ошибка на стороне сервера: " + data.error);
                        $("body").css("cursor", "default");
                        return false;
                    } else {
                        
                        if (data.total !=0) {
                            $(".skpwtypes_search_pages").html(data.pages);
                            $(".skpwtypes_search_total").html(data.total);
                            
                            $(".skp_data_row").remove();
                            $(".loader_skpwtypes").hide();
                            $(".no_skpwtypes").hide();
                            $(".searchskpwtypes").append(data.tpl);
                            $("body").css("cursor", "default");  
                                          
                        } else {                        
                            $(".skpwtypes_search_pages").html("&mdash;");
                            $(".skpwtypes_search_total").html("0");
                            $(".skp_data_row").remove();
                            $(".no_skpwtypes").show();
                            $(".loader_skpwtypes").hide();
                            $("body").css("cursor", "default");
                        }
                        return false;
                    }
                    $("body").css("cursor", "default");
                    return false;
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert(json_parse_error + " " +  exeption);    
                    }
                    
                    return false;
                }
                
                return false;
            })
            .fail(function() {
                alert(ajax_fail + " browse_wtypes");
                $("body").css("cursor", "default");
                return false;
            });
            return false;
            
        }); 
    
     
     

         
        
        return false;                
}






$(document).ready(function() {
    
    
    $(document).on("click", "a.pages_ajax", function() {
        
        var a = $(this).attr("href").split('#');
        var r = new RegExp("^start-(-?[0-9]+)$");        
        var b = r.exec(a[1]);
        if(b && b[1]) {
            if(parseInt(b[1]) < 0) b[1] = 0;
            $(this).parents("div.selector:first").find(".search_filter input[name='filter_start']").val(b[1]);            
            var targetForm = $(this).parents("div.selector:first").find("form.search_filter").attr("id");
            if (targetForm) {
                $("#"+targetForm).submit();
                
                return false;
            }
        }   
        return false;    
             
    });
    
    
 });