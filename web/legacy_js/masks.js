$(document).ready(function(){ 
    
    $(".phonemask").maskex("+7 (000) 000-00-00"); 
    
    $(".phonemask").bind('paste', function(e) {
     var self = this;
          setTimeout(function(e) {
            var targ = $(self).val();
            targ = targ.replace(/[^0-9]/g, "");
            //alert(targ);
            if (targ.length>10) {
                targ =targ.substring(1, targ.length);
            }
            $(self).val(targ);
          }, 0);
    });
    
    /*$(document).on("blur", ".phonemask", function() {
        if ($(this).val().replace(/[^0-9]/g, "").length < 11) {
            $(this).val('');
        } 
    });*/
     
    
    /*$(".phonemask").maskex("A(000)000-00-00", {placeholder: "(___)___-__-__", clearIfNotMatch: true,
                                                translation: {
                                                'A':{pattern: /([+]7)|(7)|(8)/,optional: true},
                                                'B':{pattern: /([+])/,optional: true}
                                                }});*/
    $(".passpsnmask").mask("99 99 999999");
    $(".ipmask").mask("999.999.999.999");
    $(".percent").mask("99%");
    $(".mnm").maskex("#000");
    $(".money_simple").maskex("####0000.00", {reverse: true});
});