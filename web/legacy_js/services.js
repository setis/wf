function recountTotalResult() {
    var totalAmmount = 0;

    $(".itemprice").each(function () {
        $(this).html((parseFloat($(this).attr("realprice")) * (parseFloat($(this).parents("tr:first").find(".skp_tw_quant").val()))).toFixed(2));
        totalAmmount = totalAmmount + (parseFloat($(this).attr("realprice")) * (parseFloat($(this).parents("tr:first").find(".skp_tw_quant").val())).toFixed(2));
    });
    $(".totalammount").html(totalAmmount.toFixed(2));
    $(".cmritem").val(totalAmmount.toFixed(2));
    return false;
}

var request;
var t;

$(document).ready(function () {

    $("#calc_tsk").submit(function () {
        if ($(".itemprice").length == 0) {
            alert("Не указаны виды работ! Сохранение отменено.");
            return false;
        }
        var status = $("input[name=status_id]").val();
        if (status == 47) {
            if (!confirm("Сохранение отчета переведет заявку в статус Выполнена. Выполнить?")) {
                return false;
            }
        } else {
            if (!confirm("Если Вы не вносили изменений, пожалуйста, нажмите кнопку Закрыть. Выполнить сохранение?")) {
                return false;
            }
        }

        $("body").css("cursor", "wait");
        $.post(link2('services/calcticket'), $(this).serializeArray())
            .success(function (json) {
                //alert(json);
                try {
                    var data = $.parseJSON(json);
                    $("body").css("cursor", "default");
                    if (data["error"]) {
                        alert("Cервер: " + data.error);
                        return false;
                    } else {
                        if (data.ok) {
                            $(".task_comments").append(data.tpl);
                            $("#calc").modal('hide');
                            alert("Отчет принят!");
                            window.location.reload();
                        } else {
                            alert("Ошибка разбора ответа сервера.");
                        }
                    }
                    return false;
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                    }
                    return false;
                }
                //unLockDialog("content");
                return false;


                if (data == "error") {
                    $("body").css("cursor", "default");
                    alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                    return false;
                } else {
                    $("body").css("cursor", "default");
                    alert(data.tpl);
                }
                return false;
            })
            .fail(function (e) {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });
        return false;
    });

    $(document).on("keydown", ".skp_tw_quant", function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything

            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105 )) {
            e.preventDefault();
        }


    });

    $(document).on("keydown", ".cmritem", function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything

            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105 )) {
            e.preventDefault();
        }


    });

    $(document).on("click", ".selectskpwt_link1", function () {
        var res = ($(this).attr("id") + "#" + $(this).attr("wtypetitle") + "#" + $(this).attr("wtypeminval") + "#" + $(this).attr("wtypeprice") + "#" + $(this).attr("wtypecode")).split("#");
        var newrow = false;
        if ($('.row' + res[0]).length) {
            $("input[name=quant" + res[0] + "]").val(parseInt($("input[name=quant" + res[0] + "]").val()) + 1);
        } else {
            newrow = "<tr val='tsr' class=\"prow row" + res[0] + "\"><input type=\"hidden\" name=\"prices" + res[0] + "\" value=\"" + res[3] + "\" /><input type=\"hidden\" name=\"minval" + res[0] + "\" value=\"" + res[2] + "\" /><input type=\"hidden\" name=\"wtype_ids[]\" value=\"" + res[0] + "\" /><td align=\"center\">" + res[4] + "</td><td>" + res[1] + "</td><td><center><input required=\"required\" type=\"text\" style=\"width: 50px;\" minval=\"" + res[2] + "\" name=\"quant" + res[0] + "\" class=\"skp_tw_quant\" value=\"" + res[2] + "\" /></center></td><td><center realprice=\"" + res[3] + "\" class=\"itemprice\">" + (parseFloat(res[3]) * parseFloat(res[2])).toFixed(2) + "</center></td><td width=\"16\"><a class=\"premove premove_" + res[0] + "\" style=\"cursor:pointer;\"><img border=\"0\" src=\"../templates/images/cross.png\" /></a></td></tr>";
            if ($(".prow").length == 0) {
                $(".tw_no_rows").hide();
                $(".tw_total").show();
            }
            $(".tech_works").append(newrow);
            newrow = false;
        }
        recountTotalResult();
        $("#sugg_1").modal('hide');
        $("input[name=rst").hide();
        //$(document).off("click", ".selectskpwt_link1");

        $("input[name=intelsearch]").val("");
        return false;
    });

    $(document).on("click", 'a.removetmc', function () {
        var tmcId = $(this).attr('class').split("_")[1];
        $("tr." + tmcId).remove();
        if ($(".tmc_list tr").length == 1) {
            $(".tmc_no_rows").show();
        }
        return false;
    });

    $(document).on("keyup", "input[name=intelsearch]", function (e) {
        var keyID = e.keyCode || e.which;
        var itemval = $(this).val();
        var object = $(this);


        if ($(this).val().length > 2) {
            $("input[name=rst]").show();
            if (keyID == 38) // Arrow Up
            {

            }
            else if (keyID == 40) // Arrow Down
            {

            }
            else if (keyID == 37 || keyID == 39) // Arrow Left Right
            {
                // do notnig
            }
            else if (keyID == 36 || keyID == 35 || keyID == 33 || keyID == 34) // Home End PgUp PgDn
            {
                // do notnig
            }
            else if (keyID == 9 /*&& $(streetIdContainer).val() > 0*/) // Tab and sterrt already set
            {
                // do notnig
            }
            else if (keyID == 13 /*&& $(streetIdContainer).val() > 0*/) // Enter and sterrt already set
            {
                // do notnig
            }
            else {
                //$("#sugg_1").hide();

                clearTimeout(t);
                if (request) {
                    request.abort();
                    $("body").css("cursor", "default");
                }
                t = setTimeout(function () {

                    $("body").css("cursor", "wait");

                    var index = '';

                    var parent = '';

                    request = $.post(link2("adm_skp_price/browse_wtypes_intel"), {
                        q: itemval,
                        skpwtype_contr_id: $("input[name=skp_contr_id]").val()
                    }, function (responseText, textStatus, XMLHttpRequest) {
                        //$(sugg_1).load(url, {q: $(streetNameContainer).val(), parent_id: parentIdContainer ? $(parentIdContainer).val() : ''}, function(responseText, textStatus, XMLHttpRequest){

                        if (responseText) {
                            try {
                                data = $.parseJSON(responseText);
                            }
                            catch (e) {
                                alert("Ошибка разбора ответа сервера!");
                                return false;
                            }
                            $("#sugg_body").html(data.tpl);
                            if (data.total == 0 || data.total <= 15) {
                                $(".hasrecords").hide();
                            }


                            $("#sugg_body").children("div").mousemove(function (e) {
                                $("#sugg_body").children("div").removeClass("selected");
                                $(this).addClass("selected");
                            });

                            $("#sugg_body").children("div").click(function (e) {
                                $("#sugg_body").children("div").removeClass("selected");
                                $(this).addClass("selected");
                            });
                            //$("#sugg_1").css("top", object.offset().top+object.height());
                            $("input[name=intelsearch]").blur();

                            $("#sugg_1").modal("show");

                            //$("#sugg_1").height("auto");
                            //if($("#sugg_1").height() > 200) $("#sugg_1").height(200);
                            //$("#sugg_1").width("auto");
                            //if($("#sugg_1").width() < object.width()) $("#sugg_1").width(object.width());
                        }
                        else {
                            $("#sugg_1").modal('hide');
                        }

                        $("body").css("cursor", "default");
                    });
                }, 1200);
            }
        }
        else {
            $("#sugg_1").modal('hide');
            $("input[name=rst]").hide();
        }
    });

    $(document).on("hidden.bs.modal", "#sugg_1", function () {
        //$("input[name=rst]").hide();
        //$("input[name=intelsearch]").val('');
        $("input[name=intelsearch]").focus();
    });

    $(document).on("click", "input[name=rst]", function () {
        $("input[name=intelsearch]").val('');
        $(this).hide();
        $("#sugg_1").modal('hide');
    });

    $(document).on("click", ".premove", function () {
        if (confirm("Удалить вид работ?")) {
            $(this).parents("tr:first").remove();
            if (!$(".prow").length) {
                $(".tw_no_rows").show();
                $(".tw_total").hide();
            }
            recountTotalResult();
        }
        return false;

    });

    $(document).on("blur", ".skp_tw_quant", function () {

        if (parseFloat($(this).val()) < parseFloat($(this).attr("minval"))) {
            $(this).val($(this).attr("minval"));
        }
        recountTotalResult();
    });

    $(document).on("click", "#tw_add", function () {
        selSKPWTypes(function () {

            var res = this.split("#");
            var newrow = false;
            if ($('.row' + res[0]).length) {
                $("input[name=quant" + res[0] + "]").val(parseInt($("input[name=quant" + res[0] + "]").val()) + 1);
            } else {
                newrow =
                    "<tr val='tsr' class=\"prow row" + res[0] + "\">" +
                    "   <input type=\"hidden\" name=\"prices" + res[0] + "\" value=\"" + res[3] + "\" />" +
                    "   <input type=\"hidden\" name=\"minval" + res[0] + "\" value=\"" + res[2] + "\" />" +
                    "   <input type=\"hidden\" name=\"wtype_ids[]\" value=\"" + res[0] + "\" />" +
                    "   <td align=\"center\">" + res[4] + "</td>" +
                    "   <td>" + res[1] + "</td>" +
                    "   <td><center>" +
                    "       <input required=\"required\" type=\"text\" style=\"width: 50px;\" minval=\"" + res[2] + "\" name=\"quant" + res[0] + "\" class=\"skp_tw_quant\" value=\"" + res[2] + "\" />" +
                    "   </center></td>" +
                    "   <td><center realprice=\"" + res[3] + "\" class=\"itemprice\">" + (parseFloat(res[3]) * parseFloat(res[2])).toFixed(2) + "</center></td>" +
                    "   <td width=\"16\">" +
                    "       <a class=\"premove premove_" + res[0] + "\" style=\"cursor:pointer;\"><img border=\"0\" src=\"../templates/images/cross.png\" /></a>" +
                    "   </td>" +
                    "</tr>";
                if ($(".prow").length == 0) {
                    $(".tw_no_rows").hide();
                    $(".tw_total").show();
                }
                $(".tech_works").append(newrow);
                newrow = false;
            }
            recountTotalResult();
            return false;
        }, $(this), $("input[name=skp_contr_id]").val(), true);
    });

    $("#calc_ticket").click(function () {
        $("body").css("cursor", "wait");
        var el = $(this);

        $.post(link2('services/checktaskstatus_ajax'), {task_id: $("input[name=task_id]").val()})
            .success(function (data) {
                if (data == "error") {
                    $("body").css("cursor", "default");
                    alert('Ошибка получения текущего статуса заявки! Обратитесь к разработчикам!');
                    return false;
                }
                if (data == "nostatus") {
                    $("body").css("cursor", "default");
                    alert('Текущий статус заявки не позволяет открыть форму ввода Отчета! Требуемый статус: Назначена в график, Выполнена, Сдача отчета или Закрыта.');
                    return false;
                }
                if (data == "ok" || data == "change") {
                    if (data == "change") {
                        $("body").css("cursor", "default");
                        if (!confirm('Для продолжения необходимо перевести заявку в состояние "Выполнение заявки". Продолжить?')) {
                            return false;
                        }
                    }
                    if (data == "change") {
                        var reportStatus_ID = "47";
                        //alert($('input[name=report_status_color]').val());
                        $(".tpane").each(function () {
                            $(this).style("background-color", '#85FFB2', "important");
                        });
                        //$(".cmm_title").css("background-color", $('input[name=report_status_color]').val());
                        $("body").css("cursor", "wait");
                        $("#task_comment_loader").show();
                        $.post("/comments/addcmm_ajax/", {
                            task_id: $("input[name=task_id]").val(),
                            cmm_text: 'Автоматическая смена статуса при открытии формы Отчета.',
                            tag: '',
                            status_id: reportStatus_ID
                        }, function (data) {
                            $("#task_comment_loader").hide();
                            if (data) {
                                $(".task_comments").append(data);
                            } else {
                                alert("Ошибка добавления комментария!");
                            }
                            $("body").css("cursor", "default");
                        });
                    }
                    $(".floatingform").hide();
                    $(".floatingform1").hide();
                    $('body').css('cursor', 'wait');
                    //$("#calc").css("top", el.offset().top + el.height());

                    $.post(link2('services/getcurrentcalc_ajax'), {task_id: $("input[name=calc_tsk_id]").val()})
                        .success(function (data) {
                            if (data == "error") {
                                alert('Ошибка получения расчета заявки! Обратитесь к разработчикам!');
                                $("body").css("cursor", "default");
                                return false;
                            } else {
                                $("body").css("cursor", "default");
                                $("#calc_content").html(data);

                                //$("#calc").css("left", $(document).width()/2 - $("#calc").width()/2);
                                $(".date_input").datepicker();

                                $("#add_tmc_to_task").click(function () {
                                    $('body').css('cursor', 'default');

                                    $("#newtmccat_inform").val(0);
                                    $("#tmc_add_list").html("<tr id='tmc_norows'><td colspan='5'><p align='center' style='color: #FF0000;'><strong>Список пуст</strong></p></td></tr>");
                                    $("#addtmc_inform").modal('show');
                                });

                                $("#goaddtmctotask_inform").click(function () {
                                    $("#addtmc_inform").modal('hide');
                                    //unLockCalcDialog();
                                });

                                $("#canceladdtmctotask_inform").click(function () {
                                    $("#addtmc_inform").modal('hide');
                                    //unLockCalcDialog();
                                });

                                $("#newtmccat_inform").change(function () {
                                    $("body").css("cursor", "wait");
                                    $.post(link2('adm_materials/getListByCat'), {
                                        cat_id: $(this).val(),
                                        tech_id: $("input[name=tech_id]").val()
                                    })
                                        .success(function (data) {
                                            if (data == "error") {
                                                $("body").css("cursor", "default");
                                                alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                                                return false;
                                            } else {
                                                $("body").css("cursor", "default");
                                                $("#tmc_add_list").html(data);

                                                // tmc add

                                                $("a.tmc").click(function () {
                                                    var tmcId = $(this).attr('class').split("_")[1];
                                                    var maxVal_sn = $(this).attr('id');
                                                    var row_id = $(this).attr('tst_id');
                                                    var metric = $(this).hasClass('metric') ? true : false;
                                                    $(".tmc_no_rows").hide();
                                                    var tmcTitle = $(".tmctitle_" + tmcId).html();
                                                    if (!metric) {
                                                        if (!($(".tmc_list").find("." + row_id + "").length)) {
                                                            //<input required='required' value='1' disabled='disabled' name='tmc_count_" + tmcId + "' type='text' style='width:50px;' />
                                                            $(".tmc_list").append("<tr class='" + tmcId + " " + row_id + "'><input type=\"hidden\" name=\"row_id[]\" value=\"" + row_id + "\" /><td align='center'>" + tmcTitle + "</td><td align='center'>" + maxVal_sn + "</td><td align='center'><input type='hidden' name='tmcid_" + row_id + "' value='" + maxVal_sn + "' /><input type='hidden' name='tmc_ids[]' value='" + tmcId + "' />1</td><td align='center'><input type='checkbox' name='isrent" + row_id + "' /></td><td align='center'><a class='removetmc tmcid_" + row_id + "' href='#'><img src='/templates/images/cross.png' /></a></td></tr>");
                                                        } else {
                                                            $(".tmc_list").find("." + row_id + "").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
                                                            return false;
                                                        }
                                                    } else {
                                                        if (!($(".tmc_list").find("." + tmcId + "").length)) {
                                                            $(".tmc_list").append("<tr class='" + tmcId + " " + row_id + "'><input type=\"hidden\" name=\"row_id[]\" value=\"" + row_id + "\" /><td align='center'>" + tmcTitle + "</td><td align='center'>&mdash;</td><td align='center'><input type='hidden' name='tmc_ids[]' value='" + tmcId + "' /><input type='hidden' name='maxval' value=" + maxVal_sn + " /><input required='required' class='tmccount int' name='tmc_count_" + tmcId + "' type='text' style='width:50px;' /></td><td align='center'>&mdash;</td><td align='center'><a class='removetmc tmcid_" + tmcId + "' href='#'><img src='/templates/images/cross.png' /></a></td></tr>");
                                                        } else {
                                                            $(".tmc_list").find("." + tmcId + "").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
                                                            return false;
                                                        }
                                                    }
                                                    $(".int").maskex('000000');
                                                    $(".tmccount").blur(function () {
                                                        var maxVal = $(this).prev("input[name=maxval]").val();
                                                        if (parseInt($(this).val()) > parseInt(maxVal)) {
                                                            $(this).val('');
                                                            alert('Нельзя списать ТМЦ в количестве, больше, чем есть на руках!');
                                                            throw {
                                                                name: 'Ошибка',
                                                                message: 'Нельзя списать ТМЦ в количестве, больше, чем есть на руках!'
                                                            };
                                                            return false;
                                                        }
                                                    });
                                                    $(document).on("click", 'a.removetmc', function () {
                                                        var tmcId = $(this).attr('class').split("_")[1];
                                                        $("tr." + tmcId).remove();
                                                        if ($(".tmc_list tr").length == 1) {
                                                            $(".tmc_no_rows").show();
                                                        }
                                                        return false;
                                                    });
                                                    //} else {
                                                    //    $(".tmc_list").find("." + tmcId + "").fadeOut(50).fadeIn(50).fadeOut(50).fadeIn(50);
                                                    //}
                                                    return false;
                                                });

                                                //---------------
                                            }
                                        })
                                        .fail(function () {
                                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                            $("body").css("cursor", "default");
                                        })
                                });

                                if ($("input[name=readonly]").val() == "1") {
                                    $("#save_calc").hide();
                                    $(".chtitle").hide();
                                } else {
                                    $("#save_calc").show();
                                    $(".chtitle").show();
                                    if ($("#calc_content").find("p.rm11").length) {
                                        $("#save_calc").hide();
                                        $(".chtitle").hide();
                                    }
                                }
                                $("#calc").modal('show');
                                $("#calc h2.float-title").mouseover(function () {
                                    $("body").css("cursor", "move");
                                });
                                $("input[name=intelsearch]").focus();
                                return false;
                            }
                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                            $("body").css("cursor", "default");
                        });
                    return false;
                }
            }).fail(function () {
            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
            $("body").css("cursor", "default");
        });
    });

    $("#cancel_calc").click(function () {
        //unLockCalcDialog();
        $("#calc").modal('hide');


    });

    $("#gosavecalc").click(function () {
        //unLockCalcDialog();
        $("#calc").modal('hide');


    });


});

function lockCalcDialog() {
    $("#calc :input").attr("disabled", "disabled");
}

function unLockCalcDialog() {
    $("#calc :input").removeAttr("disabled");
}

$(document).ready(function () {
    //  возврат ТМЦ по заявке
    $(document).on("click", "#return_tmc_to_task", function () {
        $('body').css('cursor', 'default');

        //lockCalcDialog();

        $("#add_r_tmc_inform").modal('show');
        $("#add_r_tmc_inform form").find("input[type=text]").val('');
        $("#add_r_tmc_inform form").find("input[type=text]").removeAttr("disabled");

        $("#add_r_tmc_inform form").find("select").each(function () {
            $("option", $(this)).each(function () {
                $(this).removeAttr("selected");
            });
            if (!$(this).attr("multiple")) {
                $("option:first", $(this)).attr("selected", "selected");
            }
        });
        $("select[name=r_tmc_name_id]").html("<option value='0'>-- выберите наименование --</option>");

    });

    $(document).on("click", "#cancel_r_tmctotask_inform", function () {
        $("#add_r_tmc_inform").modal('hide');
        $("#ticketbody :button:not(.wasdisabled)").removeAttr("disabled");
        $("#addticket :button:not('.wasdisabled')").removeAttr("disabled");
        $("#ticketbody :submit:not('.wasdisabled')").removeAttr("disabled");
        $("#addticket :submit:not('.wasdisabled')").removeAttr("disabled");
        //unLockCalcDialog();

    });

    $("input[name=new_tmc_name]").keyup(function () {
        if ($(this).val() != "") {
            $("select[name=r_tmc_name_id]").attr("disabled", "disabled");
        } else {
            $("select[name=r_tmc_name_id]").removeAttr("disabled");
        }
    });

    $("#r_tmccat_inform").change(function () {
        $("body").css("cursor", "wait");
        $.post(link2('adm_materials/getOptionListByCat_ajax'), {cat_id: $(this).val()})
            .success(function (data) {
                if (data == "error") {
                    $("body").css("cursor", "default");
                    alert('Ошибка на стороне сервера. Обратитесь к разработчикам!');
                    return false;
                } else {
                    $("body").css("cursor", "default");
                    $("select[name=r_tmc_name_id]").html(data);
                }
            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            })
    });

    $("select[name=r_tmc_name_id]").change(function () {
        if ($(this).val() != "" && $(this).val() != "0") {
            $("input[name=new_tmc_name]").attr("disabled", "disabled");
            $("input[name=new_tmc_barcode]").attr("disabled", "disabled");

        } else {
            $("input[name=new_tmc_name]").removeAttr("disabled");
            $("input[name=new_tmc_barcode]").removeAttr("disabled");
        }
    });


    $("#return_tmc").submit(function () {
        if ($("input[name=new_tmc_serial]").val() == "" || $(".sel_r_tmccatinform").val() == '' || $("select[name=r_tmccat_inform]").val() == "" || ($("select[name=r_tmc_name_id]").val() == "0" && $("input[name=new_tmc_name]").val() == "")) {
            alert("Пожалуйста, укажите категорию ТМЦ, наименование из справочника или произвольное наименование и серийный номер!");
            return false;
        }
        var currentSerial = $("input[name=new_tmc_serial]").val();
        var ac_r_exists = false;
        $(".r_tmc_list tr").each(function () {
            var ser = $(this).children("td").eq(1).find("input[type='text']").length > 0 ? $(this).children("td").eq(1).find("input[type='text']").val() : $(this).children("td").eq(1).html();
            if (ser == currentSerial) {
                ac_r_exists = true;
                return false;
            }
        });
        if (ac_r_exists) {
            alert('ТМЦ с указанным серийным номером уже принято по данной заявке! Пожалуйста, укажите уникальный серийный номер.');
            return false;
        }
        $.post(link2('services/return_tmc_from_ticktet'), $(this).serializeArray())
            .success(function (json) {
                try {
                    var data = $.parseJSON(json);
                    $("body").css("cursor", "default");
                    if (data.error) {
                        $("body").css("cursor", "default");
                        alert('Ошибка на стороне сервера: ' + data.error);
                        return false;
                    } else {
                        $("body").css("cursor", "default");
                        if (data.tpl) {
                            if (data.comment) {
                                $(".task_comments").append(data.comment);
                            }
                            $(".r_tmc_no_rows").hide();
                            $(".r_tmc_list").append(data.tpl);
                            $("#add_r_tmc_inform").hide();
                            $("#ticketbody :button:not(.wasdisabled)").removeAttr("disabled");
                            $("#addticket :button:not('.wasdisabled')").removeAttr("disabled");
                            $("#ticketbody :submit:not('.wasdisabled')").removeAttr("disabled");
                            $("#addticket :submit:not('.wasdisabled')").removeAttr("disabled");
                            //unLockCalcDialog();
                        } else {
                            alert("Ошибка возврата заявки!");
                        }
                    }
                    return false;
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert(json_parse_error + " " + exeption);
                    }

                    return false;
                }


            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });
        return false;
    });

    $(document).on("click", ".removertmc", function () {
        var obj = $(this);
        var gp_rtmc_exists = false;
        var currentSerial = $(".rtmc_" + obj.attr("id")).children("td").eq(1).find("input[type='text']").length > 0 ? $(".rtmc_" + obj.attr("id")).children("td").eq(1).find("input[type='text']").val() : $(".rtmc_" + obj.attr("id")).children("td").eq(1).html();
        $(".tmc_list tr").each(function () {
            var ser = $(this).children("td").eq(1).find("input[type='text']").length > 0 ? $(this).children("td").eq(1).find("input[type='text']").val() : $(this).children("td").eq(1).html();
            if (ser == currentSerial) {
                gp_rtmc_exists = true;
                return false;
            }
        });
        if (gp_rtmc_exists) {
            alert("Вы пытаетесь удалить принятое по данной заявке ТМЦ, предварительно списав его на эту же заявку! Удалить такое ТМЦ нельзя. Удалите запись о списании на заявку и повторите.");
            return false;
        }
        if (!confirm("Удаление записи приведет к откату списания данного ТМЦ с заявки. Продолжить?")) {
            return false;
        }
        var rmtmcname;
        if ($(this).attr("wasnew")) {
            if (confirm("Удалить вид ТМЦ, созданный при списании?")) {
                rmtmcname = "1";
            } else {
                rmtmcname = "0";
            }
        } else {
            rmtmcname = "0";
        }
        var task_id = $(this).attr("task_id");
        var row_id = $(this).attr("id");
        $.post(link2('services/delete_returned_tmc_from_ticket'), {
            row_id: row_id,
            task_id: task_id,
            and_tmc: rmtmcname
        })
            .success(function (json) {
                try {
                    var data = $.parseJSON(json);
                    $("body").css("cursor", "default");
                    if (data.error) {
                        $("body").css("cursor", "default");
                        alert('Ошибка на стороне сервера: ' + data.error);
                        return false;
                    } else {
                        $("body").css("cursor", "default");
                        if (data.ok) {
                            if (data.comment) {
                                $(".task_comments").append(data.comment);
                            }
                            $(".rtmc_" + row_id).remove();
                            if ($(".r_tmc_rows").length == 0) {
                                $(".r_tmc_no_rows").show();
                            }
                        } else {
                            alert("Ошибка отката возврата по заявке!");
                        }
                    }
                    return false;
                }
                catch (exeption) {
                    $("body").css("cursor", "default");
                    if (json.match(/login/ig)) {
                        $("body").html(json);
                    } else {
                        alert(json_parse_error + " " + exeption);
                    }

                    return false;
                }


            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });
        return false;
    });
});



// Addresses

$(document).ready(function(){

    $('#savechanges').click(function () {
        var v = $('[name=area_id]').val();
        $('[name=area_id_hidden]').val(v);
    });

    $("#cancel_edit_addr").click(function () {
        $("#goeditaddr").modal('hide');
    });

    $("#gosaveeditaddr").click(function () {
        var dom_id = $('input[name=dom_id]').val();
        var flats = $('input[name=ed_dom_flat]').val();
        var floors = $('input[name=ed_dom_floors]').val();
        var entrs = $('input[name=ed_dom_entr]').val();
        if (!dom_id) {
            alert('Не выбран адрес!');
            return false;
        }
        $("body").css("cursor", "wait");

        $.post(link2("addr_interface/updateAddrA"), {dom_id: dom_id, entr: entrs, floor: floors, flat: flats})
            .success(function (data) {
                if (entrs.toString() != $("#e_dom_entr").html() || floors.toString() != $("#e_dom_floor").html() || flats.toString() != $("#e_dom_flat").html()) {
                    var tComment = "Параметры адреса: ";
                    var myVar = tComment.concat(((entrs.toString() != $("#e_dom_entr").html()) ? ("Кол-во подъездов: " + $("#e_dom_entr").html() + " -> " + entrs + " ") : " "), ((floors.toString() != $("#e_dom_floor").html()) ? ("Кол-во этажей: " + $("#e_dom_floor").html() + " -> " + floors + " ") : " "), ((flats.toString() != $("#e_dom_flat").html()) ? ("Кол-во квартир: " + $("#e_dom_flat").html() + " -> " + flats + " ") : " "));
                    $("#task_comment_loader").show();
                    $.post("/comments/addcmm_ajax/", {
                        task_id: $("input[name=task_id]").val(),
                        cmm_text: myVar,
                        tag: "Обновлены данные"
                    }, function (data) {
                        $("#task_comment_loader").hide();
                        if (data) {
                            $(".task_comments").append(data);
                        } else {
                            alert("Ошибка добавления комментария!");
                        }
                        $("body").css("cursor", "default");
                    });
                    $("#e_dom_entr").html(entrs);
                    $("#e_dom_floor").html(floors);
                    $("#e_dom_flat").html(flats);
                } else {
                    $("body").css("cursor", "default");
                }
                $("#cancel_edit_addr").click();
            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            })
    });

    $("#go_edit_addr").click(function () {

        $("#ed_dom_entr").val($("#e_dom_entr").html() ? $("#e_dom_entr").html() : "");
        $("#ed_dom_floors").val($("#e_dom_floor").html() ? $("#e_dom_floor").html() : "");
        $("#ed_dom_flat").val($("#e_dom_flat").html() ? $("#e_dom_flat").html() : "");

        $("#goeditaddr").modal('show');

        return false;
    });

    $("#cancel_select_addr").click(function () {
        $("#goselectaddr").modal('hide');
        $("#go_select_addr").prop('disabled', false);
    });

    // button "Save" from modal
    $("#gosaveselectaddr").click(function () {

        var repo = $('.suggest-data-repository');

        var address = $('[name=set_addr]').val(),
            pod = $('[name=set_pod]').val(),
            etazh = $('[name=set_etazh]').val(),
            kv = $('[name=set_kv]').val(),
            domofon = $('[name=set_domofon]').val(),
            area = $('[name=area_id]').val(),

            full = repo.data('full'),
            kladr = repo.data('kladr'),
            house = repo.data('house'),
            is_not_recognized = repo.data('is_not_recognized'),
            latitude = repo.data('latitude'),
            longitude = repo.data('longitude'),
            change_marker = repo.data('changeMarker'),

            sc_id = repo.data('sc_id'); // separate method -> longer delay

        // screen
        $('#caddr').html('<strong>' + address + '</strong>');
        $('#inf_pod').html('<strong>' + pod + '</strong>');
        $('#inf_etazh').html('<strong>' + etazh + '</strong>');
        $('#inf_kv').html('<strong>' + kv + '</strong>');
        $('#inf_domofon').html('<strong>' + domofon + '</strong>');

        // post
        $('[name=address]').val(address);
        $('[name=pod]').val(pod);
        $('[name=etazh]').val(etazh);
        $('[name=kv]').val(kv);
        $('[name=domofon]').val(domofon);


        $.post(link2("addr_interface/getRecByCodeStringIDA_n"),
            {
                code: kladr,
                name: house,
                full_address: address,
                change_marker: change_marker,
                dom_id: $('[name=dom_id]').val()
            })
            .success(function (data) {

                var currentDom;
                if (data == "error") {
                    alert("Ошибка на стороне сервера! Обратитесь к разработчикам!");
                    return false;
                }

                // address found in list_addr
                if (data != "none") {
                    var addr = data.split("^");
                    currentDom = addr[0];

                    // ODS
                    if (addr[12]) {
                        $.post(link2("adm_ods/getODSParams"), {ods_id: addr[12]})
                            .success(function (data) {
                                if (data == "error") {
                                    $("body").css("cursor", "default");
                                    alert("Ошибка получения информации о выбранной ОДС! Обратитесь к разработчикам!");
                                    return false;
                                }
                                var ods = data.split("^");
                                $("#e_ods_name").html(ods[1]);
                                $("#e_ods_addr").html(ods[4] != "" ? ods[4] : "-");
                                $("#e_ods_tel").html(ods[3] != "" ? ods[3] : "-");
                                $("#e_ods_contact").html(ods[2] != "" ? ods[2] : "-");
                                $("#e_ods_comment").html(ods[5] != "" ? ods[5] : "-");
                                $("#go_edit_ods").prop('disabled', false);
                                $("body").css("cursor", "default");
                            })
                            .fail(function () {
                                $("body").css("cursor", "default");
                                alert('Ошибка обмена данными с сервером!');
                            });
                    }
                    else {
                        $("#go_edit_ods").prop('disabled', true);
                        $("#e_ods_name").html("Не Указана");
                        $("#e_ods_addr").html("-");
                        $("#e_ods_tel").html("-");
                        $("#e_ods_contact").html("-");
                        $("#e_ods_comment").html("-");
                    }

                    $("body").css("cursor", "wait");

                    $.post(link2("addr_interface/updateAddrInTaskA_n"),
                        {
                            task_id: $("input[name=task_id]").val(),
                            dom_id: currentDom,
                            pod: pod,
                            etazh: etazh,
                            kv: kv,
                            domofon: domofon,
                            area_id: area,
                            is_not_recognized: is_not_recognized
                        })
                        .success(function (data) {
                            if (data == "error") {
                                $("body").css("cursor", "default");
                                alert("Ошибка на стороне сервера! Обратитесь к разработчикам!");
                                return false;
                            }
                            else {
                                $("#task_comment_loader").show();
                                $.post("/comments/addcmm_ajax/", {
                                    task_id: $("input[name=task_id]").val(),
                                    cmm_text: address,
                                    tag: "Обновлен адрес"
                                }, function (data) {
                                    $("#task_comment_loader").hide();
                                    if (data) {
                                        $(".task_comments").append(data);
                                    } else {
                                        alert("Ошибка добавления комментария!");
                                    }
                                    $("body").css("cursor", "default");
                                });
                                $("#cancel_select_addr").click();
                            }
                            $("body").css("cursor", "default");
                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                            $("body").css("cursor", "default");
                        });

                }

                // address not found in list_addr
                else {
                    //  добавление нового адреса
                    $("body").css("cursor", "wait");
                    $.post(link2("addr_interface/insertAddrA_n"),
                        {
                            full: full,         // repository
                            address: address,   // input (different when unRecognised)
                            house: house,
                            kladr: kladr,
                            is_not_recognized: is_not_recognized,
                            latitude: latitude,
                            longitude: longitude,
                            area: area
                        })
                        .success(function (data) {

                            if (data != "error" && data != "none") {
                                currentDom = data;
                                $("body").css("cursor", "default");
                                $("body").css("cursor", "wait");
                                $.post(link2("addr_interface/updateAddrInTaskA_n"),
                                    {
                                        task_id: $("input[name=task_id]").val(),
                                        dom_id: currentDom,
                                        pod: pod,
                                        etazh: etazh,
                                        kv: kv,
                                        domofon: domofon,
                                        area_id: area,
                                        is_not_recognized: is_not_recognized
                                    })
                                    .success(function (data) {
                                        if (data == "error") {
                                            $("body").css("cursor", "default");
                                            alert("Ошибка на стороне сервера! Обратитесь к разработчикам!");
                                            return false;
                                        } else {
                                            $("#task_comment_loader").show();

                                            $.post("/comments/addcmm_ajax/", {
                                                task_id: $("input[name=task_id]").val(),
                                                cmm_text: address,
                                                tag: "Обновлен адрес"
                                            }, function (data) {
                                                $("#task_comment_loader").hide();
                                                if (data) {
                                                    $(".task_comments").append(data);
                                                } else {
                                                    alert("Ошибка добавления комментария!");
                                                }
                                                $("body").css("cursor", "default");
                                            });

                                            $("#cancel_select_addr").click();
                                        }
                                        $("body").css("cursor", "default");
                                    })
                                    .fail(function () {
                                        alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                                        $("body").css("cursor", "default");
                                    });

                            } else {
                                $("body").css("cursor", "default");
                                alert("Ошибка на стороне сервера! Обратитесь к разработчикам!");
                                return false;
                            }
                        })
                        .fail(function () {
                            alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                            $("body").css("cursor", "default");
                        });
                }

                $("body").css("cursor", "default");
            })
            .fail(function () {
                alert('Ошибка обмена данными с сервером! Обратитесь к разработчикам!');
                $("body").css("cursor", "default");
            });


        $("#go_select_addr").prop('disabled', false);

        $("#goselectaddr").modal('hide');

    });

    $("#go_select_addr").click(function () {

        $('.suggest-addr_full').keyup(function () {
            ////
        });

        $('.suggest-addr_full').val($('#caddr').text());

        $('body').css('cursor', 'default');
        $("#goselectaddr").modal('show');
        return false;
    });




});





