/**
 * Created by Artem on 13.04.2016.
 */

(function () {
    "use strict";

    angular.module("Workflow", ['puElasticInput'])
        .config(['$httpProvider', function ($httpProvider) {
            $httpProvider.interceptors.push(function () {
                return {
                    'request': function (config) {
                        NProgress.start();
                        return config;
                    },
                    'response': function (response) {
                        NProgress.done();
                        return response;
                    }
                };
            });
        }])
        .factory('workflowApi', [function () {
            return {};
        }]);

})();