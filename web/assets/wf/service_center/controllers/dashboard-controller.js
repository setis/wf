angular.module('ServiceCenterApp', ["ui.bootstrap", 'ngRoute', "angular-confirm", 'yaMap', "wf-angular-select-modal", "ngTable", 'ui.select', 'ngSanitize', 'Workflow', 'UserApp'])
    .config(['$routeProvider', '$locationProvider',
        function ($routeProvider, $locationProvider) {
            $routeProvider
                .when('/create', {
                    templateUrl: '/assets/wf/service_center/templates/create_service_center.html',
                    controller: 'ServiceCenterController',
                    controllerAs: 'contr'
                })
                .when('/view/:scId', {
                    templateUrl: '/assets/wf/service_center/templates/service_center.html',
                    controller: 'ServiceCenterController',
                    controllerAs: 'contr'
                })
                .when('/', {
                    templateUrl: '/assets/wf/service_center/templates/dashboard.html',
                    controller: 'ServiceCenterListController',
                    controllerAs: 'contr'
                })
                .otherwise({
                    redirectTo: '/'
                })
            ;

            $locationProvider
                .html5Mode(false)
                .hashPrefix("!");
        }
    ])
    .filter('departmentShortName', function () {
        return function (input) {
            return input.substring(input.lastIndexOf("/") + 1, input.length);
        }
    })
    .filter('propsFilter', function () {
        return function (items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function (item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    })
    .controller('ServiceCenterListController', [
        '$route',
        '$routeParams',
        '$scope',
        '$http',
        'NgTableParams',
        '$location',
        function ($route, $routeParams, $scope, $http, NgTableParams, $location) {
            this.access = 1;

            $http.get('/api/service_center2/access.json').then(function(response){
                $scope.access = parseInt(response.data.access);
            });
            this.reload = function () {
                return $http.get('/api/service_center2/service-centers.json');
            };

            this.reload().then(function (response) {
                $scope.centers = response.data;
                $scope.tableParams = new NgTableParams({count: 50}, {
                    counts: [10, 20, 50],
                    dataset: $scope.centers
                });
            });

            $scope.deleteServiceCenter = function(item) {
                $http.post('/api/service_center2/delete-service-center.json', {
                    id: item.id
                }).then(function() {
                    $route.reload();
                });
            };
        }
    ])
    .controller('ServiceCenterController', [
        '$route',
        '$routeParams',
        '$location',
        '$scope',
        '$http',
        '$q',
        function ($route, $routeParams, $location, $scope, $http, $q) {
            var scId = $routeParams.scId;
            $scope.alerts = [];
            $scope.technicians = [];
            $scope.coordinators = [];
            $scope.chiefs = [];
            $scope.mols = [];
            $scope.super_mols = [];
            $scope.editable = false;
            $scope.area = {
                geometry: {
                    type: 'Polygon',
                    coordinates: [[]]
                }
            };
            $scope.access = 1;

            $http.get('/api/service_center2/access.json').then(function(response){
                $scope.access = parseInt(response.data.access);
            });

            this.reload = function () {
                return $http.get('/api/service_center2/service-centers.json');
            };

            this.reload().then(function (response) {
                $scope.centers = response.data;
                $scope.service_center = _.find($scope.centers, function (e) {
                    return e.id == scId;
                });
                if($scope.service_center) {
                    $scope.technicians = $scope.service_center.technicians || [];
                    $scope.coordinators = $scope.service_center.coordinators || [];
                    $scope.chiefs = $scope.service_center.chiefs || [];
                    $scope.mols = $scope.service_center.mols || [];
                    $scope.super_mols = $scope.service_center.super_mols || [];
                    $scope.area = $scope.service_center.area || $scope.area;
                }
                $scope.editable = ($scope.area.geometry.coordinates[0] && ($scope.area.geometry.coordinates[0].length < 3));

                $scope.other_service_centers = _.filter($scope.centers, function (e) {
                    return e.id !== scId;
                });
                $scope.other_service_centers.forEach(function (e, i) {
                    $scope.other_service_centers[i].area.geometry.fillRule = "nonZero";
                    $scope.other_service_centers[i].area.properties = {
                        hintContent: "Сервисный центр: " + e.name
                    };
                })

            });

            $scope.drawingStop = function ($event) {
                $scope.area.geometry.coordinates = $event.originalEvent.originalEvent.originalEvent.newCoordinates;
            };

            $scope.saveInfo = function () {
                var list = [
                    {url: '/api/service_center2/save-service-center.json', data: $scope.service_center},
                    {
                        url: '/api/service_center2/save-chiefs.json',
                        data: {chiefs: $scope.chiefs, sc_id: $scope.service_center.id}
                    },
                    {
                        url: '/api/service_center2/save-coordinators.json',
                        data: {coordinators: $scope.coordinators, sc_id: $scope.service_center.id}
                    },
                    {
                        url: '/api/service_center2/save-mols.json',
                        data: {mols: $scope.mols, sc_id: $scope.service_center.id}
                    },
                    {
                        url: '/api/service_center2/save-super-mols.json',
                        data: {super_mols: $scope.super_mols, sc_id: $scope.service_center.id}
                    }
                ], promises = [];

                for (var i in list) {
                    promises.push($http.post(list[i].url, list[i].data));
                }

                $q.all(promises).then(function (response) {
                    $scope.alerts.push({
                        msg: 'Операция успешно завершена',
                        type: 'success'
                    });
                }, function (response) {
                    $scope.alerts.push({
                        msg: 'Ошибка на сервере',
                        type: 'danger'
                    });
                });
            };

            $scope.saveArea = function () {
                $http.post('/api/service_center2/save-area.json', {
                    area: $scope.area,
                    sc_id: $scope.service_center.id
                }).then(function (response) {
                    $scope.alerts.push({
                        msg: 'Операция успешно завершена',
                        type: 'success'
                    });
                }, function (response) {
                    $scope.alerts.push({
                        msg: 'Ошибка на сервере',
                        type: 'danger'
                    });
                });
            };

            $scope.saveServiceCenter = function() {
                $http.post('/api/service_center2/save-service-center.json',
                    $scope.service_center
                ).then(function (response) {
                    $scope.alerts.push({
                        msg: 'Операция успешно завершена',
                        type: 'success'
                    });
                   $location.path('/');
                }, function (response) {
                    $scope.alerts.push({
                        msg: 'Ошибка на сервере',
                        type: 'danger'
                    });
                });
            };

            $scope.saveTechnicians = function () {
                $http.post('/api/service_center2/save-technicians.json', {
                    technicians: $scope.technicians,
                    sc_id: $scope.service_center.id
                }).then(function (response) {
                    $scope.selected = [];
                    $scope.alerts.push({
                        msg: 'Операция успешно завершена',
                        type: 'success'
                    });
                }, function (response) {
                    $scope.alerts.push({
                        msg: 'Ошибка на сервере',
                        type: 'danger'
                    });
                });
            };
        }
    ])
    .directive('gssUsersSelect', function () {
        return {
            templateUrl: '/assets/wf/service_center/templates/users-select.html',
            scope: {
                selected: '='
            },
            controller: ['$scope', 'NgTableParams', '$http', '$q', function ($scope, NgTableParams, $http, $q) {
                $scope.count = 10;
                $scope.counts = [];
                $scope.users = [];
                $scope.dep = [];

                $http.get('/api/users/active-users.json').then(function (response) {
                    $scope.users = response.data;
                    $scope.tableParams = new NgTableParams({
                        count: $scope.count,
                        sorting: {selected: "desc"}
                    }, {
                        counts: $scope.counts,
                        dataset: $scope.users
                    });

                    _.each($scope.users, function (e, i) {
                        $scope.users[i].selected = !!_.find($scope.selected, function (o) {
                            return o.id == e.id;
                        });
                    });
                });


                $scope.add = function (row) {
                    $scope.selected.push(row);
                    row.selected = true;
                    $scope.tableParams.settings().data = $scope.selected;
                    $scope.tableParams.reload();
                };

                $scope.remove = function (row) {
                    _.remove($scope.selected, function (e) {
                        return row.id == e.id;
                    });
                    row.selected = false;
                    $scope.tableParams.settings().data = $scope.selected;
                    $scope.tableParams.reload();
                }
            }],
            link: function (scope, el, attr) {
                if (attr.pageSize) {
                    scope.count = attr.pageSize;
                    scope.counts = [attr.pageSize, attr.pageSize * 2, attr.pageSize * 4];
                }

                // if (attr.showJobPositionSeparate) {
                //     // TBD
                // }
            }
        };
    });
