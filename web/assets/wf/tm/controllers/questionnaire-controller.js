/**
 * Created by artem on 22.07.16.
 */
angular
    .module('TelemarketingApp', ["ui.bootstrap", "angular-confirm", "ngTable"])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('[[').endSymbol(']]');
    })
    .filter('propsFilter', function () {
        return function (items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function (item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    })
    .filter('comma2decimal', [
        function () { // should be altered to suit your needs
            return function (input) {
                var ret = (input) ? input.toString().trim().replace(",", ".") : null;
                return parseFloat(ret);
            };
        }])
    .controller('QuestionnaireController', [
        '$scope', '$uibModal', '$http',
        function ($scope, $uibModal, $http) {
            var self = this;

            $scope.isModalOpen = false;

            $scope.open = function (taskId, size) {
                $scope.isModalOpen = true;
                $scope.taskId = taskId;

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/assets/wf/tm/templates/questionnaire-modal.html',
                    controller: 'QuestionnaireModalController',
                    controllerAs: 'cnt',
                    size: size,
                    backdrop: 'static',
                    resolve: {
                        questionnaire: $http.get('/api/telemarketing/get-questionnaire.json?taskId=' + taskId),
                        loyalty: $http.get('/api/telemarketing/get-loyalty.json?taskId=' + taskId)
                    }
                });

                modalInstance.result.then(function (result) {
                    $scope.isModalOpen = false;

                    $http.post('/api/telemarketing/save-questionnaire.json', {
                        questionnaire: result.questionnaire,
                        loyalty: result.loyalty,
                        taskId: taskId
                    });

                }, function () {
                    $scope.isModalOpen = false;
                });
            };
        }
    ])
    .controller('QuestionnaireModalController', ['questionnaire', 'loyalty', '$scope', '$uibModal', '$uibModalInstance',
        function (questionnaire, loyalty, $scope, $uibModal, $uibModalInstance) {
            $scope.questionnaire = {
                questions: _.map(questionnaire.data.questions, function (e) {
                    e.answered = e.answered || [];
                    return e;
                }),
                comment: questionnaire.data.comment
            };

            $scope.loyalty = {
                questions: loyalty.data.questions,
                comment: loyalty.data.comment
            };


            $scope.toggleSelection = function toggleSelection(selected, name) {
                var idx = selected.indexOf(name);

                if (idx > -1) {
                    selected.splice(idx, 1);
                } else {
                    selected.push(name);
                }
            };

            $scope.ok = function () {
                $uibModalInstance.close({
                    questionnaire: $scope.questionnaire,
                    loyalty: $scope.loyalty
                });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }])
;