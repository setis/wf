/**
 * Created by Artem on 13.04.2016.
 */

(function () {
    "use strict";

    angular
        .module("slackManagement", ["ngTable", "ui.bootstrap", "Workflow"])
        .filter('channelsBadges', function () {
            return function (input) {
                input = input || [];
                var out = '';

                if (input.slack_id && input.channels) {
                    for (var x in input.channels) {
                        if (input.channels[x].type == 'channel') {
                            var channel_name = ' #' + input.channels[x].name;
                            out += ' <div class="btn-group fixed-btn-group btn-group-sm" role="channel"> ' +
                                '   <a class="btn btn-success" ng-click="demo.sendMessage(\'' + input.channels[x].id + '\')" title="Отправить сообщение в канал">' + channel_name + '</a> ' +
                                '   <a class="btn btn-danger" ng-click="demo.removeFromGroup(\'' + input.channels[x].id + '\', row.slack_user.id)" title="Удалить пользователя из канала"><i class="glyphicon glyphicon-remove"></i></a> ' +
                                '</div> ';
                        }
                    }
                    out += ' <a class="btn btn-sm btn-primary" ng-click="demo.addToChannel(row.slack_user.id, row.fio)" title="Добавить в группу"><i class="glyphicon glyphicon-plus"></i><a>';
                }
                return out;
            };
        })
        .filter('groupsBadges', function () {
            return function (input) {
                input = input || [];
                var out = [];

                if (input.slack_id && input.channels) {
                    for (var x in input.channels) {
                        if (input.channels[x].type == 'group') {
                            var channel_name = ' <i class="glyphicon glyphicon-lock"></i> ' + input.channels[x].name;
                            out.push(' <div class="btn-group fixed-btn-group btn-group-sm" role="group"> ' +
                                '   <a class="btn btn-warning" ng-click="demo.sendMessage(\'' + input.channels[x].id + '\')" title="Отправить сообщение в группу">' + channel_name + '</a> ' +
                                '   <a class="btn btn-danger" ng-click="demo.removeFromGroup(\'' + input.channels[x].id + '\', row.slack_user.id)" title="Удалить пользователя из группы"><i class="glyphicon glyphicon-remove"></i></a> ' +
                                '</div> ');
                        }
                    }
                    out.push(' <a class="btn btn-sm btn-primary" ng-click="demo.addToGroup(row.slack_user.id, row.fio)" title="Добавить в группу"><i class="glyphicon glyphicon-plus"></i><a>');
                }

                return out.join(' ');
            };
        })
        .factory("colsConfig", ['channelsBadgesFilter', 'groupsBadgesFilter', function (channelsBadgesFilter, groupsBadgesFilter) {
            return [
                {
                    field: "id",
                    title: "ID",
                    show: true,
                    getValue: function ($scope, row) {
                        return '<span>' + row.id + '</span>';
                    },
                    filter: {
                        id: 'number'
                    }
                }, {
                    field: "fio",
                    title: "ФИО",
                    show: true,
                    getValue: function ($scope, row) {
                        return '<span>' + row.fio + '</span>';
                    },
                    filter: {
                        fio: 'text'
                    },
                    sortable: "fio"
                },
                {
                    field: "department.name",
                    title: "Отдел",
                    show: true,
                    getValue: function ($scope, row) {
                        return '<span>' + row.department.name + '</span>';
                    },
                    filter: {
                        "department.name": 'text'
                    },
                    sortable: "department.name"
                }, {
                    field: "channels",
                    title: "Каналы",
                    show: true,
                    getValue: function ($scope, row) {
                        if (row.slack_user && row.slack_user.hasOwnProperty('id')) {
                            return channelsBadgesFilter($scope.data.slack_channels[row.slack_user.id]);
                        }
                    }
                }, {
                    field: "groups",
                    title: "Группы",
                    show: true,
                    getValue: function ($scope, row) {
                        if (row.slack_user && row.slack_user.hasOwnProperty('id')) {
                            return groupsBadgesFilter($scope.data.slack_channels[row.slack_user.id]);
                        }
                    }
                }, {
                    field: "invite",
                    title: "Сообщение",
                    show: true,
                    getValue: function ($scope, row) {
                        if (row.slack_user) {
                            return '<button ng-click="demo.sendMessage(row.slack_user.id, row.slack_user.name)" ' +
                                'class="btn btn-sm btn-success" '+ (row.slack_user.deleted?' disabled="disabled" ':'') +'>' +
                                '<i class="glyphicon glyphicon-envelope"></i> @' + row.slack_user.name +
                                '</button>';
                        } else if (row.email) {
                            return '<button ng-click="demo.inviteUser(row)" class="btn btn-sm btn-default">' +
                                '<i class="glyphicon glyphicon-envelope"></i> ' + row.email +
                                '</button>';
                        } else {
                            return '<span class="label label-danger label-sm">Нет почтового адреса!</span>';
                        }
                    }
                }];
        }])
        .controller("slackController", ['NgTableParams', 'slackApiList', 'slackModals', 'colsConfig', '$scope', function (NgTableParams, api, slackModals, colsConfig, $scope, $filter) {
            // debugger;
            var self = this;

            $scope.data = {
                alerts: [],
                channel_name: '',
                channels: [],
                channel_selected: null,
                channel_new: null,
                slack_channels: [],
                slack_groups: []
            };

            self.cols = colsConfig;

            self.addAlert = function (text, type) {
                type = type || 'danger';
                $scope.data.alerts.push({msg: text, type: type});
            };

            self.closeAlert = function (index) {
                $scope.data.alerts.splice(index, 1);
            };

            self.addToChannel = function (userId, userName) {
                var type = 'channel';
                $scope.data.channel_type = 'канал';
                $scope.data.user_name = userName;
                api.slack.getChannels(type).then(function (result) {
                    if (result.status != 200) {
                        self.addAlert(result.data.error);
                        return;
                    }
                    $scope.data.channels = result.data;
                });
                addChannelModalShow(userId, type);
            };

            self.addToGroup = function (userId, userName) {
                var type = 'group';
                $scope.data.channel_type = 'группу';
                $scope.data.user_name = userName;
                api.slack.getChannels(type).then(function (result) {
                    if (result.status != 200) {
                        self.addAlert(result.data.error);
                        return;
                    }
                    $scope.data.channels = result.data;
                }, function (result) {
                    self.addAlert(result.data.error);
                });
                addChannelModalShow(userId, type);
            };

            var addChannelModalShow = function (userId, type) {
                slackModals.addUserToChannel.open($scope).then(function () {
                    /* Добавление пользователя в канал */
                    if ($scope.data.channel_selected)
                        api.slack.addChannel($scope.data.channel_selected, userId).then(function (result) {
                            if (result.status != 200) {
                                self.addAlert(result.data.error);
                                return;
                            } else {
                                $scope.data.slack_channels[userId].channels.push(result.data);
                            }
                        }, function (result) {
                            self.addAlert(result.data.error);
                        });

                    /* Добавить новый канал и пользователя в него */
                    if ($scope.data.channel_new) {
                        api.slack.addAndCreateChannel($scope.data.channel_new, userId, type).then(function (result) {
                            if (result.status != 200) {
                                self.addAlert(result.data.error);
                                return;
                            } else {
                                $scope.data.slack_channels[userId].channels.push(result.data);
                            }
                        }, function (result) {
                            self.addAlert(result.data.error);
                        });
                    }
                })
            };

            self.removeFromGroup = function (channelId, userId) {
                api.slack.removeFromGroup(channelId, userId).then(function (result) {
                    if (!result.data.ok) {
                        self.addAlert(result.data.error);
                        return;
                    } else {
                        $scope.data.slack_channels[userId].channels = _.reject($scope.data.slack_channels[userId].channels, function (e) {
                            return e.id == channelId
                        });
                    }
                }, function (result) {
                    self.addAlert(result.data.error);
                })
            };

            self.sendMessage = function (userId, userName) {
                $scope.data.user_name = userName;
                slackModals.sendMessageModal.open().then(function ($scope) {
                    api.slack.sendMessage(userId, $scope.message).then(function (result) {
                        if (result.status != 200) {
                            self.addAlert(result.data.error);
                            return;
                        }
                    }, function (result) {
                        self.addAlert(result.data.error);
                    })
                });
            };

            self.inviteUser = function (row) {
                api.slack.inviteUser(row).then(function (result) {
                }, function (result) {
                    self.addAlert(result.data.error);
                })
            };

            api.slack.usersList().then(function (result) {
                self.tableParams = new NgTableParams({}, {
                    dataset: result.data.items
                });

                result.data.items.forEach(function (val, i) {
                    if (val.slack_user && val.slack_user.channels) {
                        $scope.data.slack_channels[val.slack_user.id] = {
                            channels: val.slack_user.channels,
                            fio: val.fio,
                            slack_id: val.slack_user.id,
                            deleted: val.slack_user.deleted,
                            department: val.department.name
                        }
                    }
                });
            }, function (result) {
                self.addAlert(result.data.error || "Ошибка загрузки списка пользователей!");
            });

        }])

})();

(function () {
    "use strict";
    angular.module("slackManagement")
        .factory('slackModals', ['$uibModal', function ($uibModal) {
            return {
                sendMessageModal: {
                    open: function ($scope) {
                        var self = this;

                        var modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: 'sendMessageModal.html',
                            controller: 'modalInstanceController',
                            scope: $scope
                        });

                        return modalInstance.result;
                    }
                },
                addUserToChannel: {
                    open: function ($scope) {
                        var self = this;

                        var modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: 'addUserToChannel.html',
                            controller: 'modalInstanceController',
                            scope: $scope
                        });

                        return modalInstance.result;
                    }
                }
            }
        }])
        .controller('modalInstanceController', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
            $scope.ok = function () {
                $uibModalInstance.close($scope);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }])

})();

(function () {
    "use strict";

    angular.module("slackManagement")
        .directive("demoBindCompiledHtml", bindCompiledHtml);
    bindCompiledHtml.$inject = [];

    function bindCompiledHtml() {
        return {
            restrict: "A",
            controller: bindCompiledHtmlController
        };
    }

    bindCompiledHtmlController.$inject = ["$scope", "$element", "$attrs", "$compile"];
    function bindCompiledHtmlController($scope, $element, $attrs, $compile) {
        $scope.$watch($attrs.demoBindCompiledHtml, compileHtml);

        function compileHtml(html) {
            var compiledElements = $compile(html)($scope);
            $element.html(compiledElements);
        }
    }
})();

(function () {
    "use strict";

    angular.module('slackManagement')
        .directive('ngEnter', function () {
            return function (scope, element, attrs) {
                element.bind('keydown keypress', function (event) {
                    if (event.which === 13) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngEnter || attrs.ngClick, {$event: event});
                        });
                        event.preventDefault();
                    }
                });
            };
        });
})();