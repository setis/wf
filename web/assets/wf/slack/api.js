/**
 * Created by Artem on 29.05.2016.
 */
(function () {
    "use strict";
    angular
        .module("slackManagement")
        .service('slackApi', ['$http', function ($http) {
            return {
                inviteUser: function (row) {
                    return $http.post("/api/slack/invite.json", {email: row.email});
                },
                usersList: function () {
                    return $http.get("/api/slack/users.json");
                },
                departments: function () {
                    return $http.get('/api/slack/departments.json');
                },
                sendMessage: function (recipientId, message) {
                    return $http.post("/api/slack/message.json", {channel: recipientId, text: message});
                },
                addChannel: function (channelId, userId) {
                    return $http.post("/api/slack/add-to-channel.json", {user: userId, channel: channelId});
                },
                removeFromGroup: function (channelId, userId) {
                    return $http.post("/api/slack/remove-from-channel.json", {user: userId, channel: channelId});
                },
                getChannels: function (type) {
                    return $http.post("/api/slack/channels.json", {type: type});
                },
                addAndCreateChannel: function (channelName, userId, type) {
                    var self = this;
                    return $http.post("/api/slack/create-channel.json", {name: channelName, type: type})
                        .then(function (result) {
                            return self.addChannel(result.data.id, userId)
                        }, function (result) {
                            return result;
                        })
                }
            };
        }])
        .factory('slackApiList', ['slackApi', 'workflowApi', function (api, workflowApi) {
            return angular.extend(workflowApi, {slack: api});
        }]);

})();

