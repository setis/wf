/**
 * Created by Artem on 29.05.2016.
 */
(function () {
    "use strict";
    angular
        .module("TaskApp", [])
        .service('taskApi', ['$http', function ($http) {
            return {
                getTask: function (taskId) {
                    return $http.post("/api/task/task.json", {id: taskId})
                },
                getByAgreementWorksList: function (taskId) {
                    return $http.get("/api/task/partner-works-list.json?task_id=" + taskId);
                },
                getByTaskTypeWorksList: function (taskId) {
                    return $http.get("/api/task/works-list.json?task_id=" + taskId);
                },
                getPenaltiesList: function () {
                    return $http.get("/api/task/penalties-list.json");
                },
                getOdsList: function (query) {
                    return $http.post("/api/ods/ods-list.json", {query: query});
                },

                getTaskBilledWorks: function (taskId) {
                    return $http.get("/api/task/partner-works.json?task_id=" + taskId);
                },
                getTaskTechProfitWorks: function (taskId) {
                    return $http.get("/api/task/works.json?task_id=" + taskId);
                },
                getTaskServiceWorksList: function (taskId) {
                    return $http.get("/api/task/task-service-works-list.json?task_id=" + taskId);
                },
                getTaskServiceWorks: function (taskId) {
                    return $http.get("/api/task/task-service-works.json?task_id=" + taskId);
                },
                getTaskPenalties: function () {
                    return $http.get("/api/task/penalties.json");
                },
                getTaskPhotos: function (taskId) {
                    return $http.get("/api/task/photos.json?task_id=" + taskId);
                },
                getTaskOds: function (taskId) {
                    return $http.get("/api/ods/ods.json?task_id=" + taskId);
                },
                setTaskOds: function (taskId, ods) {
                    return $http.post("/api/task/set-ods.json", {task_id: taskId, ods: ods})
                },
                postPartnerBillWorks: function (taskId, works) {
                    return $http.post("/api/task/post-partner-billed-works.json", {task_id: taskId, works: works})
                },
                postTechProfitWorks: function (taskId, works) {
                    return $http.post("/api/task/post-tech-profit-works.json", {task_id: taskId, works: works})
                },
                postTaskServiceWorks: function(taskId, works, cash) {
                    return $http.post("/api/task/post-task-service-works.json", {task_id: taskId, works: works, ticket_sum: cash})
                },
                deletePhoto: function(taskId, photo) {
                    return $http.post("/connections/deletephotofile", {task_id: taskId, filetype_id: photo.id})
                },
                postCalcSaved: function(items) {
                    return $http.post("/api/task/calc-saved.json", {data: items});
                }
            };
        }])
        .factory('taskApiList', ['taskApi', 'workflowApi', function (api, workflowApi) {
            return angular.extend(workflowApi, {task: api});
        }]);


})();

