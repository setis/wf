angular.module('AdminTechnicianApp', ["ui.bootstrap", "yaMap"])
    .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('[[').endSymbol(']]');
    })
    .controller('AreaController', [
        '$scope',
        '$http',
        function ($scope, $http) {
            $scope.alerts = [];
            $scope.editable = false;
            $scope.area = {
                geometry: {
                    type: 'Polygon',
                    coordinates: [[]]
                }
            };
            $scope.sc = {
                geometry: {
                    type: 'Polygon',
                    coordinates: [[]]
                }
            };

            $scope.showOther = 0;
            $scope.otherTechnicians = [];

            $scope.coordsSaved = true;

            $scope.init = function (userId, scId) {
                $scope.userId = userId;
                $scope.scId = scId;
                $http.get('/adm_empl/getTechnicianCoordinates?user_id=' + $scope.userId).then(function (response) {
                    if (response.data.geometry) {
                        $scope.area.geometry = response.data.geometry;
                    }
                    $scope.editable = ($scope.area.geometry.coordinates[0] && ($scope.area.geometry.coordinates[0].length < 3));
                });
                $http.get('/adm_empl/getServiceCenterCoordinates?sc_id=' + $scope.scId).then(function (response) {
                    if (response.data.geometry) {
                        $scope.sc.geometry = response.data.geometry;
                        $scope.sc.properties = {
                            hintContent: 'Сервисный центр'
                        };
                    }
                });
            };

            $scope.drawingStop = function ($event) {
                $scope.area.geometry.coordinates = $event.originalEvent.originalEvent.originalEvent.newCoordinates;
            };

            $scope.toggleOtherAreas = function() {
                if ($scope.showOther) {
                    $scope.loadOtherTechnicians();
                }
                else {
                    $scope.otherTechnicians = [];
                }
            };

            $scope.loadOtherTechnicians = function() {
                $http.get('/adm_empl/getAllTechniciansCoordinates?user_id='+$scope.userId).then(function(response) {
                    $scope.otherTechnicians = _.filter(response.data, function (e) {
                        return e.id !== $scope.userId;
                    });
                    $scope.otherTechnicians.forEach(function (e, i) {
                        $scope.otherTechnicians[i].area.geometry.fillRule = "nonZero";
                        $scope.otherTechnicians[i].area.properties = {
                            hintContent: e.fio
                        };
                    })
                });
            };

            $scope.saveArea = function () {
                $http.post('/adm_empl/setTechnicianCoordinates', {
                    area: $scope.area,
                    user_id: $scope.userId
                }).then(function (response) {
                    $scope.coordsSaved = true;
                    $scope.alerts.push({
                        msg: 'Операция успешно завершена',
                        type: 'success'
                    });
                }, function (response) {
                    $scope.alerts.push({
                        msg: 'Ошибка на сервере',
                        type: 'danger'
                    });
                });
            };

            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };

        }
    ]);
