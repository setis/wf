(function () {
    "use strict";
    angular
        .module("UserApp", [])
        .service('userApi', ['$http', function ($http) {
            return {
                getRoles: function () {
                    return $http.post('/api/users/user-roles.json')
                },
                getTaskPerm: function (taskId) {
                    return $http.get('/api/users/task-permissions.json?task_id='+taskId)
                }
            };
        }])
        .factory('userApiList', ['userApi', 'workflowApi', function (api, workflowApi) {
            return angular.extend(workflowApi, {user: api});
        }]);
})();

