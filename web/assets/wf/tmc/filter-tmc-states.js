(function () {
    "use strict";
    angular
        .module("wf.tmc", [])
        .filter('tmcstate', ['tmcStates', function(tmcStates) {
            return function(input) {
                var states = tmcStates();
                return states[input] ? states[input] : input;
            };
        }])
        .factory('tmcStates', function() {
            return function() {
                return {
                    canceled: 'Отменён',
                    return_request: 'Возврат от техника',
                    transferred: 'Выдача ТМЦ',
                    to_partner: 'Возврат партнёрам',
                    from_partner : 'От партнёров',
                    moved : 'Перемещен',
                    request : 'Запрос',
                    items_changed: 'Изменение состава',
                    request_confirmed : 'Запрос одобрен',
                    request_approved: 'Запрос утверждён',
                    transport_request: 'Запрос транспортировки',
                    is_transported: 'Транспортируется',
                    installed: 'Установка клиенту',
                    uninstalled: 'Демонтаж',
                    write_off: 'Списание',
                    defect: 'Дефект',
                    between_warehouses: 'На другой склад'
                };
            };
        });
})();
