angular.module('TmcApp', ["ngTable", 'ui.bootstrap'])
    .controller('ReportController', ['$scope', '$http', 'NgTableParams','$httpParamSerializerJQLike', function ($scope, $http, NgTableParams,$httpParamSerializerJQLike) {
                $scope.filter = {
                    states: [
                        { id: 'installed', title: 'Установка' },
                        { id: 'uninstalled', title: 'Демонтаж' }
                    ],
                    task_types: [
                        { id: 'accidents', title: 'ТТ' },
                        { id: 'gp', title: 'ГП' },
                        { id: 'connections', title: 'Подключения' },
                        { id: 'services', title: 'СКП' }
                    ],
                    warehouses: $http.get('/api/tmc2/warehouses.json').then(function(response) {
                        return response.data.items.map(function(wh) {
                            wh.title = wh.name;
                            return wh;
                        });
                    })
                };
                var _params = {};
                $scope.tableParams = new NgTableParams({
                    filter: {
                        state: $scope.filter.states[0],
                        task_type: $scope.filter.task_types[0]
                    }
                }, {
                    counts: [],
                    filterDelay : 3000,
                    getData: function (params) {
                        var raw = _params = params.parameters();
                        console.debug(raw);
                        return $http.get('/api/tmc2/report.json', {
                                params: {
                                    count: raw.count,
                                    page: raw.page,
                                    sorting: raw.sorting,
                                    filter: raw.filter
                                },
                                paramSerializer: '$httpParamSerializerJQLike'
                            }).then(function (result) {
                                //params.total(result.data.total);
                                //$scope.expire = result.data.expire;
                                return result.data.items;
                            })  ;
                    }
                });
                $scope.exportCsv = function(){
                    window.location.href = '/api/tmc2/report.csv?'+$httpParamSerializerJQLike(_params);

                };

                $scope.showReason = function(item) {
                    return $scope.tableParams.filter().state.id === 'uninstalled';
                };

                $scope.showComment = function(item) {
                    return $scope.tableParams.filter().state.id === 'uninstalled';
                };

                $scope.datepickerOptions = {
                    datepickerMode: 'month',
                    minMode: 'month',
                    maxDate: new Date()
                };

                $scope.abs = Math.abs;
        }]);
