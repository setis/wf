angular
    .module('TmcApp', ["ui.bootstrap", "angular-confirm", "ngTable", 'lr.upload', "Workflow", 'ui.select', 'ngSanitize', 'yaMap', 'TaskApp', 'UserApp'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('[[').endSymbol(']]');
    })
    .filter('propsFilter', function () {
        return function (items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function (item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    })
    .filter('comma2decimal', [
        function () { // should be altered to suit your needs
            return function (input) {
                var ret = (input) ? input.toString().trim().replace(",", ".") : null;
                return parseFloat(ret);
            };
        }])
    .controller('calcTicketController', [
        '$scope', '$uibModal', '$rootScope', 'tmcApiList', 'taskApiList', 'userApiList',
        function ($scope, $uibModal, $rootScope, tmcApi, taskApi, userApi) {
            var self = this,
                api = _.merge(tmcApi, taskApi, userApi),
                calc = {};

            $scope.isCalcModalOpen = false;
            $scope.installed = {};
            $scope.uninstalled = {};

            $rootScope.categories = [];
            $rootScope.nomenclatureNonMetrical = [];
            $rootScope.penaltiesList = [];
            $rootScope.agreementWorks = [];
            $rootScope.taskTypeWorks = [];
            $rootScope.serviceWorksList = [];
            $rootScope.roles = {};
            $rootScope.taskPerms = {};
            $rootScope.task = {};
            $rootScope._ = _;

            api.user.getRoles().then(function (result) {
                $rootScope.roles = result.data || {};
            });

            api.tmc.getNomenclature().then(function (result) {
                $rootScope.nomenclatureNonMetrical = _.filter(result.data, function (e) {
                    return !e.is_metrical;
                });
            });

            api.tmc.getCategories().then(function (result) {
                $rootScope.categories = result.data.items || [];
            });

            api.tmc.getPartners().then(function (result) {
                $rootScope.partners = result.data.items || [];
            });

            $scope.$watch("taskId", function () {
                api.task.getTask($scope.taskId).then(function (result) {
                    $rootScope.task = result.data || {};
                });

                api.user.getTaskPerm($scope.taskId).then(function (result) {
                    $rootScope.taskPerms = result.data || {};
                }).then(function () {
                    if ($rootScope.taskPerms.canCalcTechWorks) {
                        api.task.getPenaltiesList().then(function (result) {
                            $rootScope.penaltiesList = result.data.items || [];
                        });
                        api.task.getByTaskTypeWorksList($scope.taskId).then(function (result) {
                            $rootScope.taskTypeWorks = result.data.items || [];
                        });
                    }

                    if ($rootScope.taskPerms.canCalcPartnerWorks)
                        api.task.getByAgreementWorksList($scope.taskId).then(function (result) {
                            $rootScope.agreementWorks = result.data.items || [];
                        });

                    if ($rootScope.taskPerms.canCalcServiceWorks)
                        api.task.getTaskServiceWorksList($scope.taskId).then(function (result) {
                            $rootScope.serviceWorksList = result.data || [];
                        });
                });
            });

            $scope.open = function (taskId, size) {
                $scope.isCalcModalOpen = true;
                $scope.taskId = taskId;

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/assets/wf/tmc/templates/tmc-calc-ticket-modal.html',
                    controller: 'calcTicketModalController',
                    controllerAs: 'cnt',
                    size: size,
                    backdrop: 'static',
                    resolve: {
                        api: api,
                        taskId: function () {
                            return $scope.taskId;
                        },
                        installed: api.tmc.getInstalledItems(taskId),
                        uninstalled: api.tmc.getUninstalledItems(taskId),
                        uninstallCommentTemplates: api.tmc.getUninstallCommentTemplates(),

                        billedWorks: $rootScope.taskPerms.canCalcPartnerWorks && api.task.getTaskBilledWorks(taskId),
                        techProfitWorks: $rootScope.taskPerms.canCalcTechWorks &&api.task.getTaskTechProfitWorks(taskId),
                        serviceWorks: $rootScope.taskPerms.canCalcServiceWorks && api.task.getTaskServiceWorks(taskId),
                        taskPhotos: $rootScope.taskPerms.canManageTaskPhotos && api.task.getTaskPhotos(taskId),
                        taskOds: $rootScope.taskPerms.canEditOds && api.task.getTaskOds(taskId),
                        taskPenalties: $rootScope.taskPerms.canCalcTechWorks && api.task.getTaskPenalties(taskId)
                    }
                });

            };
        }
    ])
;
