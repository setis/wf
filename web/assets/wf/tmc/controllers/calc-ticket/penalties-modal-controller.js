angular
    .module('TmcApp')
    .controller('penaltiesModalController', [
        '$scope', '$uibModalInstance', 'NgTableParams', '$rootScope', 'penalties',
        function ($scope, $uibModalInstance, NgTableParams, $rootScope, penalties) {
            var self = this;

            // выбранные penalties
            // общий список $scope.penaltiesList

            $scope.penaltiesList.map(function (el) {
                el.isSelected = _.some(penalties, {id: el.id})
            });

            $scope.selectedItems = penalties || [];

            self.table = new NgTableParams({}, {
                dataset: $scope.penaltiesList
            });

            self.addItem = function (row) {
                row.quantity = 1;
                row.isSelected = true;
                $scope.selectedItems.push(row);
                $scope.selectedItems = _.uniq($scope.selectedItems);
            };

            self.removeItem = function (row) {
                row.isSelected = false;
                _.remove($scope.selectedItems, row);
            };

            $scope.ok = function () {
                $uibModalInstance.close($scope.selectedItems);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }
    ])