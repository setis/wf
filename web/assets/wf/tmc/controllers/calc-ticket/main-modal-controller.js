angular
    .module('TmcApp')
    .controller('calcTicketModalController', [
        '$document', '$scope', '$uibModal', '$uibModalInstance', 'api', 'NgTableParams', 'upload', 'taskId', 'installed', 'uninstalled', 'uninstallCommentTemplates', 'billedWorks', 'techProfitWorks', 'serviceWorks', 'taskPhotos', 'taskOds', '$rootScope', '$q',
        function ($document, $scope, $uibModal, $uibModalInstance, api, NgTableParams, upload, taskId, installed, uninstalled, uninstallCommentTemplates, billedWorks, techProfitWorks, serviceWorks, taskPhotos, taskOds, $rootScope, $q) {
            var self = this,
                uninstallComments = uninstallCommentTemplates.data.comments,
                uninstallReasons = uninstallCommentTemplates.data.reasons;

            $scope.isInstallationModalOpen = false;
            $scope.image = undefined;
            $scope.taskId = taskId;
            $scope.available = [];
            $scope.installItems = installed.data || [];
            $scope.uninstallItems = uninstalled.data.items.map(function (e) {
                    e[0].bro = e["bro"];
                    return e[0];
                }) || [];

            $scope.billedWorks = billedWorks && billedWorks.data.items || [];
            $scope.techProfitWorks = techProfitWorks && techProfitWorks.data.items || [];
            $scope.taskPhotos = taskPhotos && taskPhotos.data.items || [];
            $scope.serviceWorks = serviceWorks && serviceWorks.data.items || [];
            self.cash = serviceWorks && serviceWorks.data.cash;
            $scope.sumWorks = function(o){
                return o.quantity * o.price;
            };
            $scope.ods = taskOds && {selected: taskOds.data, list: []};
            $scope.refreshOdsList = function (query) {
                api.task.getOdsList(query).then(function (response) {
                    $scope.ods.list = response.data;
                })
            };

            // $scope.map = null;
            // var redrawMap = function (map) {
            //     var geolocation = ymaps.geolocation;
            //     var points = [];
            //     $scope.map = map;
            //     map.geoObjects.removeAll();
            //
            //     var haveAllPoints = function () {
            //
            //         var multiRoute = new ymaps.multiRouter.MultiRoute({
            //             referencePoints: points,
            //             params: {
            //                 routingMode: 'masstransit'
            //             }
            //         }, {
            //             // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
            //             boundsAutoApply: true
            //         });
            //         // Добавляем мультимаршрут на карту.
            //         map.geoObjects.add(multiRoute);
            //     };
            //
            //     if ($scope.ods.selected)
            //         ymaps.geocode($scope.ods.selected.address, {results: 1}).then(function (res) {
            //             // Выбираем первый результат геокодирования.
            //             points.push(res.geoObjects.get(0));
            //
            //             geolocation.get({
            //                 provider: 'browser',
            //                 mapStateAutoApply: true
            //             }).then(function (result) {
            //                 // Синим цветом пометим положение, полученное через браузер.
            //                 // Если браузер не поддерживает эту функциональность, метка не будет добавлена на карту.
            //                 result.geoObjects.options.set('preset', 'islands#blueCircleIcon');
            //                 points.push(result.geoObjects);
            //                 $scope.$apply(haveAllPoints.call());
            //             }, function () {
            //                 geolocation.get({
            //                     provider: 'yandex',
            //                     mapStateAutoApply: true
            //                 }).then(function (result) {
            //                     // Красным цветом пометим положение, вычисленное через ip.
            //                     result.geoObjects.options.set('preset', 'islands#redCircleIcon');
            //                     result.geoObjects.get(0).properties.set({
            //                         balloonContentBody: 'Мое местоположение'
            //                     });
            //                     points.push(result.geoObjects.get(0));
            //                     $scope.$apply(haveAllPoints.call());
            //                 });
            //             });
            //         }, function (err) {
            //             // Если геокодирование не удалось, убиваем карту
            //             $scope.map.destroy();
            //         });
            // };
            //
            // $scope.afterMapInit = function (map) {
            //     redrawMap(map);
            //     $scope.$watch('ods.selected', function () {
            //         redrawMap($scope.map);
            //     });
            // };


            var flattenInstalledItems = function (data) {
                return _.flatMapDeep(data, function (n) {
                    if (n.items)
                        return n.items.map(function (e) {
                            e.tech_id = n.user.id;
                            e.fio = n.user.fio;
                            return e;
                        });
                    return [];
                })
            };

            self.installedItemsTable = new NgTableParams({
                count: flattenInstalledItems($scope.installItems).length
            }, {
                counts: [],
                dataset: flattenInstalledItems($scope.installItems)
            });

            self.uninstalledItemsTable = new NgTableParams({
                count: $scope.uninstallItems.length
            }, {
                counts: [],
                dataset: $scope.uninstallItems
            });

            self.billTable = new NgTableParams({
                count: $scope.billedWorks.length
            }, {
                counts: [],
                dataset: $scope.billedWorks
            });

            self.techProfitTable = new NgTableParams({
                count: $scope.techProfitWorks.length
            }, {
                counts: [],
                dataset: $scope.techProfitWorks
            });

            self.serviceTable = new NgTableParams({
                count: $scope.serviceWorks.length
            }, {
                counts: [],
                dataset: $scope.serviceWorks
            });

            self.photosTable = new NgTableParams({
                count: $scope.taskPhotos.length
            }, {
                counts: [],
                dataset: $scope.taskPhotos
            });

            $scope.openInstallationModal = function (size) {
                $scope.isInstallationModalOpen = true;

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/assets/wf/tmc/templates/tmc-install-modal.html',
                    controller: 'tmcInstallModalController',
                    controllerAs: 'cnt',
                    size: size,
                    backdrop: false,
                    resolve: {
                        available: api.tmc.getAvailableToTechsByTaskItems(taskId)
                    }
                });

                modalInstance.result.then(function (items) {
                    $scope.isInstallationModalOpen = false;

                    $scope.installItems = $scope.installItems.map(function (i) {
                        var f = _.find(items, {user: {id: i.user.id}});
                        if (f.items)
                            i.items = _.unionBy(i.items, f.items, function (e) {
                                return e.tech_id + ' ' + e.partner.id + ' ' + e.nomenclature.id + ' ' + e.serial;
                            });
                        return i;
                    });

                    $scope.installItems = _.concat($scope.installItems, _.differenceBy(items, $scope.installItems, 'user.id'));

                    self.installedItemsTable.settings().dataset = flattenInstalledItems($scope.installItems);
                    self.installedItemsTable.parameters().count = self.installedItemsTable.settings().dataset.length;
                    self.installedItemsTable.reload();
                }, function () {
                    $scope.isInstallationModalOpen = false;
                });
            };
            self.removeInstalledItem = function (row) {
                _.remove(
                    _.find($scope.installItems, {user: {id: row.tech_id}}).items, row
                );

                self.installedItemsTable.settings().dataset = flattenInstalledItems($scope.installItems);
                self.installedItemsTable.reload();
            };

            $scope.openUninstallModal = function (size) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/assets/wf/tmc/templates/tmc-uninstall-modal.html',
                    controller: 'tmcUninstallModalController',
                    controllerAs: 'cnt',
                    backdrop: false,
                    size: size,
                    resolve: {
                        api: api,
                        uninstallComments: function () {
                            return uninstallComments;
                        },
                        uninstallReasons: function () {
                            return uninstallReasons;
                        }
                    }
                });

                modalInstance.result.then(function (item) {
                    $scope.uninstallItems.push(item);
                    self.uninstalledItemsTable.settings().dataset = $scope.uninstallItems;
                    self.uninstalledItemsTable.parameters().count = self.uninstalledItemsTable.settings().dataset.length;
                    self.uninstalledItemsTable.reload();
                }, function () {

                });
            };
            self.removeUninstalledItem = function (row) {
                _.remove($scope.uninstallItems, row);
                self.uninstalledItemsTable.settings().dataset = $scope.uninstallItems;
                self.uninstalledItemsTable.reload();
            };

            $scope.openServiceWorksModal = function (size) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/assets/wf/tmc/templates/service-works-modal.html',
                    controller: 'serviceWorksModalController',
                    controllerAs: 'cnt',
                    backdrop: false,
                    size: size,
                    resolve: {
                        serviceWorks: function () {
                            return $scope.serviceWorks;
                        }
                    }
                });

                modalInstance.result.then(function (items) {
                    self.serviceTable.settings().dataset = $scope.serviceWorks = _.uniq($scope.serviceWorks.concat(items));
                    self.serviceTable.parameters().count = self.serviceTable.settings().dataset.length;
                    self.serviceTable.reload();
                }, function () {

                });
            };
            self.removeServiceWork = function (row) {
                _.remove($scope.serviceWorks, row);
                self.serviceTable.settings().dataset = $scope.serviceWorks;
                self.serviceTable.reload();
            };

            $scope.openAgreementWorksModal = function (size) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/assets/wf/tmc/templates/agreement-works-modal.html',
                    controller: 'agreementWorksModalController',
                    controllerAs: 'cnt',
                    backdrop: false,
                    size: size,
                    resolve: {
                        billedWorks: function () {
                            return $scope.billedWorks;
                        }
                    }
                });

                modalInstance.result.then(function (items) {
                    self.billTable.settings().dataset = $scope.billedWorks = _.uniq($scope.billedWorks.concat(items));
                    self.billTable.parameters().count = self.billTable.settings().dataset.length;
                    self.billTable.reload();
                }, function () {

                });
            };
            self.removeBilledWork = function (row) {
                _.remove($scope.billedWorks, row);
                self.billTable.settings().dataset = $scope.billedWorks;
                self.billTable.reload();
            };

            $scope.openTaskTypesWorksModal = function (size) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/assets/wf/tmc/templates/task-type-works-modal.html',
                    controller: 'taskTypeWorksModalController',
                    controllerAs: 'cnt',
                    backdrop: false,
                    size: size,
                    resolve: {
                        techProfitWorks: function () {
                            return $scope.techProfitWorks;
                        }
                    }
                });

                modalInstance.result.then(function (items) {
                    self.techProfitTable.settings().dataset = $scope.techProfitWorks = _.uniq($scope.techProfitWorks.concat(items));
                    self.techProfitTable.parameters().count = self.techProfitTable.settings().dataset.length;
                    self.techProfitTable.reload();
                }, function () {

                });
            };
            self.removeTechWork = function (row) {
                _.remove($scope.techProfitWorks, row);
                self.techProfitTable.settings().dataset = $scope.techProfitWorks;
                self.techProfitTable.reload();
            };

            $scope.openPenaltiesModal = function (size) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/assets/wf/tmc/templates/penalties-modal.html',
                    controller: 'penaltiesModalController',
                    controllerAs: 'cnt',
                    backdrop: false,
                    size: size,
                    resolve: {
                        penalties: function () {
                            return $scope.techProfitWorks;
                        }
                    }
                });

                modalInstance.result.then(function (items) {
                    self.techProfitTable.settings().dataset = $scope.techProfitWorks = _.uniq($scope.techProfitWorks.concat(items));
                    self.techProfitTable.parameters().count = self.techProfitTable.settings().dataset.length;
                    self.techProfitTable.reload();
                }, function () {

                });
            };

            self.deletePhoto = function (photo) {
                api.task.deletePhoto(taskId, photo).then(function () {
                    api.task.getTaskPhotos(taskId).then(function (response) {
                        self.photosTable.settings().dataset = response.data.items;
                        self.photosTable.reload();
                    });
                });
            };

            self.reloadPhotos = function () {
                api.task.getTaskPhotos(taskId).then(function (response) {
                    self.photosTable.settings().dataset = response.data.items;
                    self.photosTable.reload();
                });
            };

            $scope.ok = function () {
                if($scope.installItems.length == 0) {
                    if(confirm('Вы уверены что хотите сохранить расчёт без ТМЦ?')) {
                        sendAndClose();
                    }
                } else {
                    sendAndClose();
                }
            };

            sendAndClose = function(){
                var promises = [];

                promises.push(api.tmc.postInstall(taskId, $scope.installItems));
                promises.push(api.tmc.postUninstall(taskId, $scope.uninstallItems));
                if ($rootScope.taskPerms.canEditOds) {
                    promises.push(api.task.setTaskOds(taskId, $scope.ods.selected));
                }
                if ($rootScope.taskPerms.canCalcServiceWorks) {
                    promises.push(api.task.postTaskServiceWorks(taskId, $scope.serviceWorks, self.cash));
                }
                if ($rootScope.taskPerms.canCalcPartnerWorks) {
                    promises.push(api.task.postPartnerBillWorks(taskId, $scope.billedWorks));
                }
                if ($rootScope.taskPerms.canCalcTechWorks) {
                    promises.push(api.task.postTechProfitWorks(taskId, $scope.techProfitWorks));
                }

                promises.push(api.task.postCalcSaved({
                    taskId: taskId,
                    install: $scope.installItems,
                    uninstall: $scope.uninstallItems,
                    billedWorks: $scope.billedWorks,
                    techProfitWorks: $scope.techProfitWorks,
                    serviceWorks: $scope.serviceWorks,
                    payedByCash: self.cash,
                    ods: $scope.ods.selected
                }));

                $q.all(promises).then(function (response) {
                    $uibModalInstance.close();
                    window.location.reload();
                }, function (response) {
                    alert(response.data);
                });

            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }
    ]);
