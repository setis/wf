angular
    .module('TmcApp')
    .controller('tmcUninstallModalController', [
        '$scope', '$uibModalInstance', 'api', 'uninstallComments', 'uninstallReasons', '$rootScope',
        function ($scope, $uibModalInstance, api, uninstallComments, uninstallReasons, $rootScope) {
            $scope.alerts = [];

            $scope.uninstallReason = null;
            $scope.uninstallComment = null;

            $scope.uninstallComments = uninstallComments;
            $scope.uninstallReasons = uninstallReasons;

            $scope.ok = function () {
                var item = {
                    data: {
                        reason: $scope.reason.title,
                        comment: $scope.comment.title
                    },
                    nomenclature: $scope.nomenclatureNonMetrical.selected,
                    partner: $scope.partners.selected,
                    serial: $scope.serial,
                    quantity: 1,
                    is_uninstalled: true
                };
                api.tmc.getItemsBySerialAndNomenclature(item).then(function (response) {
                    if (response.data.length < 1 || response.data[0][1] < 1) {
                        $uibModalInstance.close(item);
                    } else {
                        $scope.alerts.push({
                            msg: response.data.message,
                            type: 'danger'
                        });
                    }
                });
            };

            $scope.cancel = function () {
                $scope.nomenclatureNonMetrical.selected = [];
                $scope.partners.selected = [];
                $uibModalInstance.dismiss('cancel');
            };

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };
        }
    ]);
