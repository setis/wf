angular
    .module('TmcApp')
    .controller('tmcInstallModalController', [
        '$scope', '$uibModalInstance', 'NgTableParams', 'available',
        function ($scope, $uibModalInstance, NgTableParams, available) {
            var self = this;
            available = available.data;

            $scope.techsSelect = {
                selected: null,
                options: _.map(available, function (e) {
                    return e.user;
                })
            };
            $scope.techsSelect.selected = _.first($scope.techsSelect.options);

            /*
             $scope.selectedItems = [
             {
             user: {...}
             items: [....]
             },
             ....
             ]
             */
            $scope.selectedItems = _.map(available, function (e) {
                return {user: e.user, items: []};
            });

            var tech = _.find(available, {user: {id: $scope.techsSelect.selected && $scope.techsSelect.selected.id}});

            if (tech)
                $scope.tableData = tech.items.map(function (item) {
                    var out = item[0];
                    out.total = item.total;
                    return out;
                });
            else
                $scope.tableData = [];

            self.availableItemsTable = new NgTableParams({}, {
                dataset: $scope.tableData
            });

            self.updateTable = function () {
                $scope.tableData = _.find(available, {user: {id: $scope.techsSelect.selected.id}}).items.map(function (item) {
                    var out = item[0];
                    out.total = item.total;
                    return out;
                });
                self.availableItemsTable.settings().dataset = $scope.tableData;
                self.availableItemsTable.reload();
            };

            self.addItem = function (row) {
                if (!row.nomenclature.is_metrical)
                    row.quantity = 1;
                else
                    row.quantity = null;

                row.isSelected = true;
                row.tech_id = $scope.techsSelect.selected.id;
                _.find($scope.selectedItems, {user: {id: $scope.techsSelect.selected.id}}).items.push(row);
            };

            self.removeItem = function (row) {
                row.isSelected = false;
                _.remove(_.find($scope.selectedItems, {user: {id: $scope.techsSelect.selected.id}}).items, row);
            };

            $scope.ok = function () {
                $uibModalInstance.close($scope.selectedItems);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }
    ]);
