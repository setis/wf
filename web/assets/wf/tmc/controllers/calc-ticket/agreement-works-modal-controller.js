angular
    .module('TmcApp')
    .controller('agreementWorksModalController', [
        '$scope', '$uibModalInstance', 'NgTableParams', '$rootScope', 'billedWorks',
        function ($scope, $uibModalInstance, NgTableParams, $rootScope, billedWorks) {
            var self = this;

            // выбранные billedWorks
            // общий список $scope.agreementWorks

            _.forEach($scope.agreementWorks, function (el) {
                var tmp = _.find(billedWorks, {id: el.id});
                el.isSelected = !!tmp;
                if (tmp && tmp.price_type == 'smeta')
                    el.estimate = parseFloat(tmp.estimate);
            });

            $scope.selectedItems = billedWorks || [];

            self.tableWorks = new NgTableParams({}, {
                dataset: _.filter($scope.agreementWorks, function (e) {
                    return e.price_type != 'smeta'
                })
            });

            self.tableEstimates = new NgTableParams({}, {
                dataset: _.filter($scope.agreementWorks, function (e) {
                    return e.price_type == 'smeta'
                })
            });

            self.addItem = function (row) {
                row.quantity = 1;
                row.isSelected = true;
                $scope.selectedItems.push(row);
                $scope.selectedItems = _.uniq($scope.selectedItems);
            };

            self.removeItem = function (row) {
                row.isSelected = false;
                _.remove($scope.selectedItems, row);
            };

            $scope.ok = function () {
                $uibModalInstance.close($scope.selectedItems);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }
    ]);