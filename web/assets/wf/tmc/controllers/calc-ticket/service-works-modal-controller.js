angular
    .module('TmcApp')
    .controller('serviceWorksModalController', [
        '$scope', '$uibModalInstance', 'NgTableParams', '$rootScope', 'serviceWorks',
        function ($scope, $uibModalInstance, NgTableParams, $rootScope, serviceWorks) {
            var self = this;

            // выбранные serviceWorks
            // общий список $scope.serviceWorksList
            $scope.serviceWorksList.map(function (el) {
                el.isSelected = _.some(serviceWorks, {id: el.id})
            });

            $scope.selectedItems = serviceWorks || [];

            $scope.categories = _.uniqBy(_.map($scope.serviceWorksList, function(e){
                return {id: e.category.title, title: e.category.title};
            }), 'id');
            $scope.partners = _.uniqBy(_.map($scope.serviceWorksList, function(e){
                return {id: e.partner.title, title: e.partner.title};
            }), 'id');

            self.table = new NgTableParams({}, {
                dataset: $scope.serviceWorksList
            });

            self.addItem = function (row) {
                row.quantity = row.ei_min;
                row.isSelected = true;
                _.some($scope.selectedItems, {id: row.id}) || $scope.selectedItems.push(row);
            };

            self.removeItem = function (row) {
                row.isSelected = false;
                _.remove($scope.selectedItems, row);
            };

            $scope.ok = function () {
                $uibModalInstance.close($scope.selectedItems);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }
    ]);