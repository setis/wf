angular
    .module('TmcApp')
    .controller('taskTypeWorksModalController', [
        '$scope', '$uibModalInstance', 'NgTableParams', '$rootScope', 'techProfitWorks',
        function ($scope, $uibModalInstance, NgTableParams, $rootScope, techProfitWorks) {
            var self = this;

            // выбранные techProfitWorks
            // общий список $scope.taskTypeWorks

            $scope.taskTypeWorks.map(function (el) {
                var tmp = _.find(techProfitWorks, {id: el.id});
                el.isSelected = !!tmp;
                if (tmp && tmp.price_type == 'smeta')
                    el.estimate = parseFloat(tmp.estimate);
            });

            $scope.selectedItems = techProfitWorks || [];

            self.tableWorks = new NgTableParams({}, {
                dataset: _.filter($scope.taskTypeWorks, function (e) {
                    return e.price_type != 'smeta'
                })
            });

            self.tableEstimates = new NgTableParams({}, {
                dataset: _.filter($scope.taskTypeWorks, function (e) {
                    return e.price_type == 'smeta'
                })
            });

            self.addItem = function (row) {
                row.quantity = 1;
                row.isSelected = true;
                $scope.selectedItems.push(row);
                $scope.selectedItems = _.uniq($scope.selectedItems);
            };

            self.removeItem = function (row) {
                row.isSelected = false;
                _.remove($scope.selectedItems, row);
            };

            $scope.ok = function () {
                $uibModalInstance.close($scope.selectedItems);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }
    ]);
