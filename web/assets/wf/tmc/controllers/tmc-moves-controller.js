angular.module('TmcApp', ["ui.bootstrap", "angular-confirm", "ngTable", "wf.tmc"])
    .controller('MovesController', ['$scope', '$http', '$location', 'NgTableParams', '$uibModal', 'tmcStates', function ($scope, $http, $location, NgTableParams, $uibModal, tmcStates) {
            $scope.selected = [];
            $scope.alerts = [];
            $scope.filter = {};
            $scope.wh = {};
            
            $scope.filter.states = _.map(tmcStates(), function(val, key) {
                return { id: key, title: val };
            });
            
            $scope.filter.wh = $http.get('/api/tmc2/warehouses.json').then(function(response) {
                return response.data.items.map(function(wh) {
                    return { id: wh.name, title: wh.name };
                });
            });

            $scope.reload = function () {
                $http.get('/api/tmc2/moves.json')
                    .then(function (response) {
                        $scope.wh = response.data.wh;
                        $scope.tableParams = new NgTableParams({}, {
                            dataset: response.data.moves.map(function(row) {
                                row[0].items_count = row[1];
                                return row[0];
                            })
                        });
                    });
            };
            
            $scope.reload();

        }])
    .run(function ($confirmModalDefaults) {
        $confirmModalDefaults.defaultLabels.title = 'Подтвердите операцию';
        $confirmModalDefaults.defaultLabels.ok = 'Да';
        $confirmModalDefaults.defaultLabels.cancel = 'Отмена';
    });