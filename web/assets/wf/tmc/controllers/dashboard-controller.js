/**
 * Created by Artem on 25.05.2016.
 */
angular.module('TmcApp', ["ui.bootstrap", "angular-confirm", "wf-angular-select-modal", "ngTable", 'ui.select', 'ngSanitize', "Workflow", "wf.tmc"])
    .directive('myEnter', function () {
        return {
            templateUrl: '/assets/wf/service_center/templates/users-select.html',
            scope: {
                selected: '='
            },
            link: function (scope, element, attrs) {
                element.bind("keydown", function (event) {
                    if (event.which === 13) {
                        if (scope.$parent.tableParams.data.length > 0) {
                            scope.$parent.addItem(
                                scope.$parent.tableParams.data[0]
                            );
                            event.target.value = '';
                            scope.$parent.tableParams.reload();

                            event.preventDefault();
                        } else {
                            if (confirm('Хотите добавить новую позицию?')) {
                                window.open('/tmc2/from-partner');
                            }
                        }
                    }
                });
            }
        };
    })
    .controller('AvailableController', [
        '$scope',
        '$http',
        '$location',
        '$wfSelectModal',
        '$uibModal',
        '$confirm',
        'NgTableParams',
        'tmcApi',
        'tmcStates',
        function ($scope, $http, $location, $wfSelectModal, $uibModal, $confirm, NgTableParams, tmcApi, tmcStates) {
            $scope.selected = [];
            $scope.alerts = [];
            $scope.wh = {};
            $scope.supermol_whs = {};
            $scope.technicians = {};
            $scope.roles = {};
            $scope.booted = false;

            $scope.groupedMode = {value: false};
            $scope.fullMode = {value: false};

            $scope.reloadList = function () {
                $scope.fullMode.value = false;
                $scope.reload($scope.groupedMode.value);
            };

            $scope.fullReloadList = function () {
                $scope.groupedMode.value = false;
                $scope.reload(false, $scope.fullMode.value);
            };

            $scope.reload = function (grouped, full) {
                var restsUrl = '/api/tmc2/available-items.json?wh_id=' + $scope.wh.id;

                if (grouped) {
                    restsUrl += '&grouped=1';
                }

                if (full) {
                    restsUrl += '&full=1';
                }

                $http.get(restsUrl)
                    .then(function (response) {
                        $scope.wh = response.data.wh;
                        $scope.technicians = response.data.technicians;
                        $scope.tableParams = new NgTableParams({}, {
                            dataset: response.data.items.map(function (item) {
                                var out = item[0];
                                out.total = item['total'];
                                out.quantity = 1;
                                out.missing = false;
                                return out;
                            })
                        });
                    })
                    .then(function () {
                        $scope.selected = [];
                    })
                    .then(function () {
                        $scope.loadRoles();
                    });
            };

            $scope.loadRoles = function () {
                $http.get('/api/users/user-roles.json?wh_id=' + $scope.wh.id).then(function (response) {
                    $scope.roles = response.data;
                });
            };

            $http.get('/api/tmc2/warehouses.json').then(function (response) {
                $scope.booted = true;
                $scope.whs = response.data.items;
                $scope.supermol_whs = response.data.super_mol_items;

                // Для молы выбираем дефолтный склад - первый из его складов
                if (response.data.mol_items && response.data.mol_items[0]) {
                    var id = response.data.mol_items[0].id;
                    $scope.wh = _.find($scope.whs, {id: id});
                }
                else {
                    $scope.wh = _.head($scope.whs);
                }

                $scope.reload($scope.groupedMode.value);
                $scope.loadRoles();
            });

            tmcApi.getNomenclature().then(function (result) {
                $scope.nomenclature = _.filter(result.data, function (e) {
                    return !e.is_metrical;
                });
            });

            $scope.getCategories = function () {
                return $http.get('/api/tmc2/categories.json').then(function (response) {
                    return response.data.items;
                });
            };

            $scope.getPartners = function () {
                return tmcApi.getPartners().then(function (result) {
                    return $scope.partners = result.data.items || [];
                });
            };

            $scope.addItem = function (row) {
                $scope.selected.push(row);
                $scope.tableParams.settings().dataset = _.reject($scope.tableParams.settings().dataset, function (item) {
                    return row.id === item.id;
                });
                $scope.tableParams.reload().then(function (data) {
                    if (data.length === 0 && $scope.tableParams.total() > 0) {
                        $scope.tableParams.page($scope.tableParams.page() - 1);
                        $scope.tableParams.reload();
                    }
                });
            };

            $scope.removeItem = function (row) {
                $scope.selected = _.reject($scope.selected, function (item) {
                    return row.id === item.id;
                });
                $scope.tableParams.settings().dataset.push(row);
                $scope.tableParams.reload();
            };

            $scope.makeRequest = function () {
                if ($scope.roles.isMol) {
                    $http.get('/api/tmc2/mol-warehouses.json').then(function (response) {
                        $wfSelectModal({items: response.data.items})
                            .then(function (result) {
                                $scope.sendTmcRequest({
                                    items: $scope.selected,
                                    source_wh: $scope.wh,
                                    target_wh: result.item
                                });
                            });
                    });
                } else if ($scope.roles.isTechnician) {
                    $scope.sendTmcRequest({
                        items: $scope.selected,
                        source_wh: $scope.wh
                    });
                }
            };

            $scope.transferBetweenWarehouses = function (data) {
                if ($scope.roles.isMolOnThisWarehouse) {
                    $wfSelectModal({
                        items: _.map($scope.whs, function (wh) {
                            return {id: wh.id, name: wh.title};
                        })
                    })
                        .then(function (result) {
                            $scope.transferBetweenWarehousesRequest({
                                items: $scope.selected,
                                source: $scope.wh,
                                target: result.item
                            })
                        });
                }
            };

            $scope.transferBetweenWarehousesRequest = function (data) {
                $http.post('/api/tmc2/transfer-between-warehouses.json', data)
                    .then(function (response) {
                        $scope.clearSelected();
                        $scope.alerts.push({
                            msg: 'Заявка успешно добавлена',
                            type: 'success'
                        });
                    }, function (response) {
                        $scope.alerts.push({
                            msg: response.data.message ? response.data.message : 'Ошибка на сервере',
                            type: 'danger'
                        });
                    })
            };

            $scope.sendTmcRequest = function (data) {
                $http.post('/api/tmc2/tmc-request.json', data)
                    .then(function (response) {
                        $scope.clearSelected();
                        $scope.alerts.push({
                            msg: 'Заявка успешно добавлена',
                            type: 'success'
                        });
                    }, function (response) {
                        $scope.alerts.push({
                            msg: response.data.message ? response.data.message : 'Ошибка на сервере',
                            type: 'danger'
                        });
                    });
            };

            $scope.returnToPartner = function () {
                $http.post('/api/tmc2/post-to-partner.json', {
                    items: $scope.selected,
                    wh: $scope.wh
                }).then(function (response) {
                    $scope.reload();
                    $scope.alerts.push({
                        msg: 'Операция успешно завершена',
                        type: 'success'
                    });
                }, function (response) {
                    $scope.alerts.push({
                        msg: response.data.message ? response.data.message : 'Ошибка на сервере',
                        type: 'danger'
                    });
                });
            };

            $scope.writeOff = function () {
                $http.post('/api/tmc2/write-off.json', {
                    items: $scope.selected,
                    wh: $scope.wh
                }).then(function (response) {
                    $scope.reload();
                    $scope.alerts.push({
                        msg: 'Операция успешно завершена',
                        type: 'success'
                    });
                }, function (response) {
                    $scope.alerts.push({
                        msg: response.data.message ? response.data.message : 'Ошибка на сервере',
                        type: 'danger'
                    });
                });
            };

            $scope.markDefect = function () {
                $http.post('/api/tmc2/mark-defect.json', {
                    items: $scope.selected,
                    wh: $scope.wh
                }).then(function (response) {
                    $scope.reload();
                    $scope.alerts.push({
                        msg: 'Операция успешно завершена',
                        type: 'success'
                    });
                }, function (response) {
                    $scope.alerts.push({
                        msg: response.data.message ? response.data.message : 'Ошибка на сервере',
                        type: 'danger'
                    });
                });
            };

            $scope.transferToTech = function () {
                $wfSelectModal({
                    items: _.map($scope.technicians, function (tech) {
                        return {id: tech.id, name: tech.fio};
                    })
                })
                    .then(function (result) {
                        $scope.transferToTechRequest({
                            items: $scope.selected,
                            wh: $scope.wh,
                            tech: result.item
                        })
                    });

            };

            $scope.transferToTechRequest = function (data) {
                return $http.post('/api/tmc2/transfer-to-tech.json', data)
                    .then(function (response) {
                        $scope.selected = [];
                        $scope.alerts.push({
                            msg: 'Операция успешно завершена',
                            type: 'success'
                        });
                        $scope.clearSelected();
                        return response;
                    }, function (response) {
                        $scope.alerts.push({
                            msg: response.data.message ? response.data.message : 'Ошибка на сервере',
                            type: 'danger'
                        });
                        $scope.clearSelected();
                    });
            };

            $scope.addMissingItems = function () {
                $uibModal.open({
                    templateUrl: '/assets/wf/tmc/templates/add-missing-item.html',
                    controller: 'AddMissingItemModalController',
                    resolve: {
                        data: function () {
                            return {
                                nomenclature: $scope.nomenclature,
                                partners: $scope.partners
                            };
                        }
                    }
                }).result.then(function (item) {
                    $scope.selected.push(item);
                    console.log($scope.selected);
                });
            };

            $scope.setSerials = function () {
                $http.post('/api/tmc2/set-serials.json', {
                    items: $scope.selected,
                    wh: $scope.wh
                }).then(function (response) {
                    $scope.reload();
                    $scope.alerts.push({
                        msg: 'Операция успешно завершена',
                        type: 'success'
                    });
                }, function (response) {
                    $scope.alerts.push({
                        msg: response.data.message ? response.data.message : 'Ошибка на сервере',
                        type: 'danger'
                    });
                });
            };

            $scope.clearSelected = function () {
                _.each(_.filter($scope.selected, {missing: false}), function (item) {
                    $scope.tableParams.settings().dataset.push(item);
                });
                $scope.selected = [];
                $scope.tableParams.reload();
            };

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.getHistory = function (item) {
                $http.get('/api/tmc2/history.json?id=' + item.property.id).then(function (response) {
                    $uibModal.open({
                        controller: 'ModalInstanceCtrl',
                        controllerAs: '$ctrl',
                        templateUrl: 'modals/history.html',
                        size: 'lg',
                        resolve: {
                            bro_tmc: item,
                            items: function () {
                                return response.data;
                            }
                        }
                    });
                }, function (response) {

                });
            }

        }
    ])
    .controller('ModalInstanceCtrl', ['$uibModalInstance', 'items', 'bro_tmc', function ($uibModalInstance, items, bro_tmc) {
        var $ctrl = this;

        $ctrl.items = items;
        $ctrl.bro_tmc = bro_tmc;
        $ctrl.ok = function () {
            $uibModalInstance.close();
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('AddMissingItemModalController', [
        '$scope',
        '$uibModalInstance',
        'data',
        function ($scope, $uibModalInstance, data) {
            $scope.nomenclature = data.nomenclature;
            $scope.partners = data.partners;
            $scope.selected = {};

            $scope.ok = function () {
                if (!($scope.selected.nomenclature && $scope.selected.partner)) {
                    return;
                }

                $uibModalInstance.close({
                    nomenclature: $scope.selected.nomenclature,
                    partner: $scope.selected.partner,
                    quantity: $scope.quantity,
                    missing: true
                });

            };

            $scope.cancel = $uibModalInstance.dismiss;
        }]);
