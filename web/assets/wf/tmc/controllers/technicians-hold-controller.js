/**
 * Created by Artem on 25.05.2016.
 */
angular.module('TmcApp', ["ui.bootstrap", "angular-confirm", "wf-angular-select-modal", "ngTable"])
    .controller('TechnicianHoldController', ['$scope', '$http', '$wfSelectModal', 'NgTableParams', function ($scope, $http, $wfSelectModal, NgTableParams) {
        $scope.selected = [];
        $scope.alerts = [];
        $scope.holder = {};
        $scope.roles = {};

        $scope.reload = function () {
            $http.get('/api/tmc2/available-techs-items.json?wh_id=' + $scope.wh.id)
                .then(function (response) {
                    $scope.tableParams = new NgTableParams({}, {
                        dataset: response.data.items.map(function (item) {
                            var out = item[0];
                            out.total = item['total'];
                            return out;
                        })
                    });
                });
        };

        $scope.loadRoles = function () {
            $http.get('/api/users/user-roles.json').then(function (response) {
                $scope.roles = response.data;
            });
        };

        $http.get('/api/tmc2/warehouses.json').then(function (response) {
            $scope.whs = response.data.mol_items;
            $scope.wh = _.head($scope.whs);

            $scope.reload();
            $scope.loadRoles();
        });

        $scope.getCategories = function () {
            return $http.get('/api/tmc2/categories.json').then(function (response) {
                return response.data.items;
            });
        };

        $scope.getPartners = function(){
            return $http.get('/api/tmc2/partners.json').then(function (response) {
                return response.data.items;
            });
        }
    }]);
