angular
    .module('TmcApp', ['ui.bootstrap', 'wf-angular-select-modal', 'angularFileUpload', 'wf'])
    .controller('FromPartnerController', ['$scope', '$http', '$wfSelectModal', '$uibModal', 'FileUploader', function ($scope, $http, $wfSelectModal, $uibModal, FileUploader) {
        $scope._ = _;
        $scope.wh = null;
        $scope.whs = [];
        $scope.selected = null;
        $scope.selectedPartner = null;
        $scope.serial = null;
        $scope.quantity = null;
        $scope.item = null;
        $scope.partnerItem = null;
        $scope.items = [];
        $scope.alerts = [];
        $scope.uploader = new FileUploader({
            url: '/api/tmc2/from-partner-file.json',
            alias: 'items',
            filters: [{
                name: 'allowOnlyCSV',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return item.type == 'application/vnd.ms-excel' || item.name.includes('csv');
                }
            }],
            removeAfterUpload: true,
            onSuccessItem: function (item, response, status, headers) {
                if (response.length > 0)
                    response.forEach(function (el) {
                        $scope.items.push({
                            id: null,
                            title: el.title,
                            is_metrical: false,
                            category: {id: null, title: el.category},
                            quantity: el.quantity,
                            serial: el.serial,
                            partner: {id: null, title: el.partner},
                            selected: true
                        });
                    });
            }
        });

        $http.get('/api/tmc2/categories.json').then(function (response) {
            $scope.categories = response.data.items;
        });

        $http.get('/api/tmc2/mol-warehouses.json').then(function (result) {
            $scope.whs = result.data.items;
        });

        $http.get('/api/tmc2/nomenclature.json').then(function (result) {
            $scope.nomenclature = result.data;
        });

        $http.get('/api/tmc2/partners.json').then(function (result) {
            $scope.partners = result.data.items;
        });

        $scope.onSelect = function ($item, $model, $label) {
            $scope.item = $item;
            $scope.serial = null;
            $scope.quantity = null;
        };

        $scope.onSelectPartner = function ($item) {
            $scope.partnerItem = $item;
        };

        var counter = 0;
        $scope.addItem = function (item) {
            if ($scope.item === null || $scope.partnerItem === null) return;

            var count = item.is_metrical ? 1 : $scope.quantity;
            var quantity = item.is_metrical ? $scope.quantity : 1;

            for(var i = 0; i < count; ++i) {
                if (!item.is_metrical) {
                    ++counter;
                }
                $scope.items.push({
                    id: item.id,
                    title: item.title,
                    is_metrical: item.is_metrical,
                    category: item.category,
                    quantity: quantity,
                    serial: !item.is_metrical && $scope.quantity == 1 ? $scope.serial : null,
                    partner: $scope.partnerItem,
                    selected: true,
                    input_id: counter
                });
            }

            $scope.selected = null;
            $scope.serial = null;
            $scope.quantity = null;
            $scope.item = null;
            $scope.partnerItem = null;
            $scope.selectedPartner = null;
        };

        $scope.toWarehouse = function () {
            $wfSelectModal({items: $scope.whs})
                .then(function (result) {
                    $http.post('/api/tmc2/post-from-partner.json', {
                        items: $scope.items.filter(function (item) {
                            return item.selected;
                        }),
                        wh: result.item.id
                    })
                        .then(function (response) {
                            $scope.items = [];
                            $scope.alerts.push({
                                msg: 'ТМЦ успешно добавлены на склад',
                                type: 'success'
                            });
                        }, function (response) {
                            $scope.alerts.push({
                                msg: response.data.message ? response.data.message : 'Ошибка на сервере',
                                type: 'danger'
                            });
                        });
                });
        };

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.clearSelected = function () {
            $scope.items = [];
        };

        $scope.addNomenclature = function () {
            $uibModal.open({
                animation: true,
                templateUrl: '/assets/wf/tmc/templates/add-nomenclature-modal.html',
                controller: 'AddNomenclatureModalController',
                size: 'md',
                resolve: {
                    data: {
                        categories: $scope.categories
                    }
                }
            }).result.then(function (result) {
                var item = result.data;
                $scope.nomenclature.push(item);
                $scope.onSelect(item);
                $scope.selected = item.title;
            });
        };

        $scope.onPressEnter = function (item) {
            var id = item.input_id + 1;
            $('#' + id).focus();
        };

        $scope.serialNotUniq = function (item) {
            return item.selected && _.filter($scope.items, {serial: item.serial, is_metrical: false} ).length > 1;
        };
    }]);
