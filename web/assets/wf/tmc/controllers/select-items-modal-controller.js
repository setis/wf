angular
    .module('TmcApp')
    .controller('selectItemsModalController', [
        '$scope', '$uibModalInstance',
        function ($scope, $uibModalInstance) {
            $scope.ok = function () {
                $uibModalInstance.close({});
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }
    ]);
