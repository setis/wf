angular
    .module('TmcApp')
    .controller('ToTransportModalController', ['$scope', '$http', '$uibModalInstance', 'result', function ($scope, $http, $uibModalInstance, result) {
            $scope.items = result.data.items;


            $scope.onSelect = function (item) {
                $scope.item = item;
            };

            $scope.ok = function () {
                if (!$scope.item) {
                    return;
                }

                $uibModalInstance.close({
                    message: 'ok',
                    carrier_id: $scope.item.id
                });

            };

            $scope.cancel = function (dismissMessage) {
                if (angular.isUndefined(dismissMessage)) {
                    dismissMessage = 'cancel';
                }
                $uibModalInstance.dismiss(dismissMessage);
            };
        }]);


