angular.module('TmcApp', ["ui.bootstrap", "angular-confirm", "ngTable", "wf-angular-select-modal"])
    .controller('AvailableController', ['$scope', '$http', '$location', 'NgTableParams', '$wfSelectModal', function ($scope, $http, $location, NgTableParams, $wfSelectModal) {
        //$scope.selectedWhId = $location.search().wh;
        $scope.selected = [];
        $scope.wh = {};
        $scope.alerts = [];
        $scope.splitNonMetrical = false;

        $http.get('/api/tmc2/warehouses.json').then(function (response) {
            $scope.whs = response.data.items;
            $scope.wh = _.head($scope.whs);
        });

        $scope.reload = function () {
            $http.get('/api/tmc2/available-tech-items.json')
                .then(function (response) {
                    $scope.holder = response.data.holder;
                    $scope.tableParams = new NgTableParams({}, {
                        dataset: response.data.items.map(function (item) {
                            var out = item[0];
                            out.total = item['total'];
                            return out;
                        })
                    });
                });
        };

        $scope.getCategories = function () {
            return $http.get('/api/tmc2/categories.json').then(function (response) {
                return response.data.items;
            });
        };


        $scope.getPartners= function () {
            return $http.get('/api/tmc2/partners.json').then(function (response) {
                return response.data.items;
            });
        };

        $scope.reload();

        $scope.addItem = function (row) {
            $scope.selected.push(row);
            $scope.tableParams.settings().dataset = _.reject($scope.tableParams.settings().dataset, function (item) {
                return row.id === item.id;
            });
            $scope.tableParams.reload().then(function (data) {
                if (data.length === 0 && $scope.tableParams.total() > 0) {
                    $scope.tableParams.page($scope.tableParams.page() - 1);
                    $scope.tableParams.reload();
                }
            });
        };

        $scope.removeItem = function (row) {
            $scope.selected = _.reject($scope.selected, function (item) {
                return row.id === item.id;
            });
            $scope.tableParams.settings().dataset.push(row);
            $scope.tableParams.reload();
        };

        $scope.returnToWh = function () {
            if($scope.whs == 1) {
                $scope.returnToWhRequest({
                    items: $scope.selected,
                    target_wh: $scope.whs[0]
                });
            } else {
                $wfSelectModal({items: $scope.whs})
                    .then(function (result) {
                        $scope.returnToWhRequest({
                            items: $scope.selected,
                            target_wh: result.item
                        });
                    });
            }
        };

        $scope.markDefect = function () {
            $http.post('/api/tmc2/mark-defect.json', {
                items: $scope.selected
            }).then(function (response) {
                $scope.reload();
                $scope.alerts.push({
                    msg: 'Операция успешно завершена',
                    type: 'success'
                });
            }, function (response) {
                $scope.alerts.push({
                    msg: 'Ошибка на сервере',
                    type: 'danger'
                });
            });
        };

        $scope.returnToWhRequest = function (data) {
            $http.post('/api/tmc2/return-to-wh-request.json', data)
                .then(function (response) {
                    $scope.clearSelected();
                    $scope.alerts.push({
                        msg: 'Операция успешно завершена',
                        type: 'success'
                    });
                }, function (response) {
                    $scope.alerts.push({
                        msg: 'Ошибка на сервере',
                        type: 'danger'
                    });
                });
        };

        $scope.clearSelected = function () {
            _.each($scope.selected, function (item) {
                $scope.tableParams.settings().dataset.push(item);
            });
            $scope.selected = [];
            $scope.tableParams.reload();
        };

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

    }]);
