angular
.module('TmcApp')
.controller('AddNomenclatureModalController', ['$scope', '$http', '$uibModalInstance', 'data', function ($scope, $http, $uibModalInstance, data) {
        if (data.categories) {
            $scope.categories = data.categories;
        }
        else {
            $http.get('/api/tmc2/categories.json').then(function (response) {
                $scope.categories = response.data.items;
            });
        }
        
        
        $scope.onCategoryChanged = function() {
            if ($scope.category && $scope.category.unit) {
                $scope.is_metrical = $scope.category.unit.is_metrical;
            }
        };
            
        $scope.ok = function () {
            var item = {
                category: $scope.category,
                title: $scope.title,
                is_metrical: $scope.is_metrical
            };
            
            $http.post('/api/tmc2/new-nomenclature.json', {
                item: item
            }).then(function(response) {
                $uibModalInstance.close({
                    message: 'ok',
                    data: response.data
                });
            });
                
        };

        $scope.cancel = function (dismissMessage) {
            if (angular.isUndefined(dismissMessage)) {
                dismissMessage = 'cancel';
            }
            $uibModalInstance.dismiss(dismissMessage);
        };
}]);
