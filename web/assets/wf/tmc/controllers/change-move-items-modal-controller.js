angular
    .module('TmcApp')
    .directive('ngEnterPressed', function () {
        return function (scope, element, attrs) {
            element.bind('keydown keypress', function (event) {
                if (event.which === 13) {
                    var inputs = $('#modal-table input.serial');
                    var i = inputs.index(event.target);
                    inputs[i+1] && inputs[i+1].focus();
                    event.preventDefault();
                }
            });
        };
    })
    .controller('ChangeMoveItemsModalController', [
        '$scope',
        '$http',
        '$uibModalInstance',
        'data',
        function ($scope, $http, $uibModalInstance, data) {
            $scope.alerts = [];
            var TmcItem = function (item) {
                this.nomenclature = item.property.nomenclature;
                this.partner = item.property.partner;
                this.quantity = item.quantity;
                this.serial = item.property.serial;
                this.property = item.property;
            };

            $scope.loadMove = function (move) {

                var items = [];
                move.items.forEach(function (e, j) {
                    var item;
                    if (e.property.nomenclature.is_metrical || (!e.property.nomenclature.is_metrical && e.quantity === 1)) {
                        item = e;
                        item.nomenclature = e.property.nomenclature;
                        item.partner = e.property.partner;
                        item.property = e.property;
                        item.id = items.length;
                        items.push(item);
                        return;
                    }

                    for (var i = 1; i <= e.quantity; i++) {
                        item = new TmcItem(e);
                        item.nomenclature = e.property.nomenclature;
                        item.partner = e.property.partner;
                        item.property = e.property;
                        item.quantity = 1;
                        item.serial = item.property.serial = null;
                        item.id = items.length;
                        item.property.id = null;
                        items.push(item);
                    }
                });

                move.items = items;

                $scope.move = angular.copy(move);
                $scope.move.items = move.items.map(function (item) {
                    return new TmcItem(item);
                });
            };

            $scope.loadMove(data.move);

            $scope.removeItem = function (item) {
                var index = $scope.move.items.indexOf(item);
                $scope.move.items.splice(index, 1);
            };

            $scope.ok = function () {
                $http.post('/api/tmc2/change-items.json', {
                    move: $scope.move
                }).then(function (response) {
                    $uibModalInstance.close({
                        message: 'ok',
                        data: response.data
                    });
                }, function (response) {
                    $scope.alerts.push({
                        msg: response.data.message,
                        type: 'danger'
                    });
                });

            };

            $scope.cancel = function (dismissMessage) {
                if (angular.isUndefined(dismissMessage)) {
                    dismissMessage = 'cancel';
                }
                $uibModalInstance.dismiss(dismissMessage);
            };

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };
        }]);
