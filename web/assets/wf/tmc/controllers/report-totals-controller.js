angular.module('TmcApp', ["ngTable", 'ui.bootstrap'])
    .controller('ReportController', ['$scope', '$http', 'NgTableParams','$httpParamSerializerJQLike', function ($scope, $http, NgTableParams,$httpParamSerializerJQLike) {
                $scope.filterData = {
                    warehouses: [],
                    techs: []
                };
                
                var dt = new Date();
                $scope.filter = {
                    date_from: new Date(dt.getFullYear(), dt.getMonth(), 1),
                    date_to: dt,
                    warehouse: null,
                    tech: null
                };
                
                $scope.$watch('filter.warehouse', function(newValue, oldValue) {
                    $scope.filter.tech = null;
                    $scope.reload();
                });
                $scope.$watch('filter.tech', function(newValue, oldValue) {
                    $scope.reload();
                });
                $scope.$watch('filter.date_from', function(newValue, oldValue) {
                    $scope.reload();
                });
                $scope.$watch('filter.date_to', function(newValue, oldValue) {
                    $scope.reload();
                });
                
                $http.get('/api/tmc2/warehouses.json').then(function(response) {
                    $scope.filterData.warehouses = response.data.items;
                });
                
                $scope.tableParams = new NgTableParams({
                    count: 1000,
                    sorting: {
                        'nomenclature.category.title': 'asc'
                    }
                }, {  counts: [] });
                var _params = {};
                $scope.reload = function() {
                    if (!($scope.filter.warehouse && $scope.filter.date_from && $scope.filter.date_to)) {
                        return;
                    }
                    
                    if ($scope.byTech && !$scope.filter.tech) {
                        return;
                    }
                    _params = {
                        date_to: moment($scope.filter.date_to).format('YYYY-MM-DD'),
                        date_from: moment($scope.filter.date_from).format('YYYY-MM-DD'),
                        warehouse: $scope.filter.warehouse.id,
                        tech: $scope.filter.tech ? $scope.filter.tech.id : null
                    };
                    $http.get('/api/tmc2/report-totals.json', {
                        params: _params,
                        paramSerializer: '$httpParamSerializerJQLike'
                    }).then(function (result) {
                        $scope.tableParams.settings({
                            dataset: result.data.items.map(function(item) {
                                item[0].quantity = parseInt(item[1]);
                                item[0].quantity_in = parseInt(item[2]);
                                item[0].quantity_out = parseInt(item[3]);
                                return item[0];
                            })
                        });
                    });
                };
                $scope.exportCsv = function(){
                    window.location.href = '/api/tmc2/report-totals.csv?'+$httpParamSerializerJQLike(_params);
                };
                $scope.exportCsvDisabled = function(){
                    return !($scope.filter.warehouse || $scope.filter.tech);
                };
                $scope.changeMode = function() {
                    if ($scope.byTech) {
                        $scope.reloadTechs();
                    }
                   $scope.tableParams.settings({
                        dataset: []
                    });
                };
                
                $scope.reloadTechs = function() {
                    if (!$scope.byTech || !$scope.filter.warehouse) {
                        return;
                    }
                    
                    $http
                        .get('/api/users/techs.json', { params: { wh_id: $scope.filter.warehouse.id }})
                        .then(function(response) {
                            $scope.filterData.techs = response.data;
                        });
                };
                
                $scope.reload();
                
                $scope.getExecutor = function(item) {
                    switch ($scope.tableParams.filter().state.id) {
                        case 'installed':
                            return item.move.source_holder;
                            
                        case 'uninstalled':
                            return item.move.target_holder;
                    }
                };
                
                $scope.showReason = function(item) {
                    return $scope.tableParams.filter().state.id === 'uninstalled';
                };
                
                $scope.showComment = function(item) {
                    return $scope.tableParams.filter().state.id === 'uninstalled';
                };
                
                $scope.datepickerOptions = {
                    datepickerMode: 'month',
                    minMode: 'month',
                    maxDate: new Date()
                };

        }]);
