/**
 * Created by Artem on 29.05.2016.
 */
(function () {
    "use strict";
    angular
        .module("TmcApp")
        .service('tmcApi', ['$http', function ($http) {
            return {
                getCategories: function () {
                    return $http.get("/api/tmc2/categories.json");
                },
                getNomenclature: function () {
                    return $http.get("/api/tmc2/nomenclature.json");
                },
                getUninstallCommentTemplates: function () {
                    return $http.post("/api/tmc2/uninstall-comment-templates.json");
                },

                getInstalledItems: function (taskId) {
                    return $http.post("/api/tmc2/installed-to-task.json", {taskId: taskId});
                },

                getUninstalledItems: function (taskId) {
                    return $http.post("/api/tmc2/uninstalled-from-task.json", {taskId: taskId});
                },
                getAvailableItems: function () {
                    return $http.post("/api/tmc2/available-tech-items.json");
                },
                getAvailableToTechsByTaskItems: function (taskId) {
                    return $http.post("/api/tmc2/available-to-techs-by-task-items.json", {taskId: taskId});
                },
                getPartners: function () {
                    return $http.get("/api/tmc2/partners.json");
                },

                postInstall: function(taskId,items) {
                    return $http.post("/api/tmc2/post-install-to-task.json", {task_id: taskId, items: items});
                },
                postUninstall: function(taskId,items) {
                    return $http.post("/api/tmc2/post-uninstall-from-task.json", {task_id: taskId, items: items});
                },
                getItemsBySerialAndNomenclature: function(item) {
                    return $http.get("/api/tmc2/get-items-by-serial-and-nomenclature.json?serial=" + item.serial + "&nomenclature_id="+item.nomenclature.id+"&partner_id="+item.partner.id);
                }
            };
        }])
        .factory('tmcApiList', ['tmcApi', 'workflowApi', function (api, workflowApi) {
            return angular.extend(workflowApi, {tmc: api});
        }]);


})();

