/**
 * Created by DEVELOPER JHON NIKITIN on 12/3/16.
 */

$(document).ready(function () {
    // Feedback button onClick
    $(document).on("click", "#feedback_button", function () {
        $("input[name=object_id]").val('');
        $("#bugreport_dlg").modal("show");
    });

    $('#bugreport_dlg').on('hidden.bs.modal', function (e) {
        $('.tabs_feedback').css('display', 'block');
        $('.br_confirm_tpl').css('display', 'none');
    });

    //BugReport send to Jira & DB
    $(function () {
        $('#Bugreport_form').submit(function (e) {
            $('body').css('cursor', 'wait');
            e.preventDefault();
            var form = $(this);
            var m_method = $(this).attr('method');
            var m_action = $(this).attr('action');
            var m_data = $(this).serialize();

            var target_btn = $(this).find('.btn-primary');
            target_btn.prop('disabled', true).toggleClass('btn-primary').toggleClass('btn-default_custom');

            $.ajax({
                type: "POST",
                url: m_action,
                data: new FormData(form[0]),
                cache: false,
                contentType: false,
                processData: false,
                async: false,
                success: function (result) {
                    $('.tabs__content').find('textarea').val('');

                    $('.tabs_feedback').css('display', 'none');
                    $('.br_confirm_tpl').css('display', 'block');

                    target_btn.prop('disabled', false).toggleClass('btn-primary').toggleClass('btn-default_custom');
                    $('body').css('cursor', 'default');

                    form[0].reset();
                },
                error: function (result) {
                    target_btn.prop('disabled', false).toggleClass('btn-primary').toggleClass('btn-default_custom');
                    alert('Ошибка соединения с сервером. Попробуйте отправить позднее.');
                    $('body').css('cursor', 'default');

                    $("#bugreport_dlg").modal("hide");
                    form[0].reset();
                }
            });
        });
    });

    //FreeForm send to Email
    $(function () {
        $('#Freeform').submit(function (e) {
            $('body').css('cursor', 'wait');
            e.preventDefault();
            var form = $(this);
            var m_method = $(this).attr('method');
            var m_action = $(this).attr('action');
            var m_data = $(this).serialize();

            var target_btn = $(this).find('.btn-primary');
            target_btn.prop('disabled', true).toggleClass('btn-primary').toggleClass('btn-default_custom');

            $.ajax({
                type: "POST",
                url: m_action,
                data: new FormData(form[0]),
                cache: false,
                contentType: false,
                processData: false,
                async: false,
                success: function (result) {
                    $('.tabs__content').find('textarea').val('');

                    $('.tabs_feedback').css('display', 'none');
                    $('.br_confirm_tpl').css('display', 'block');

                    target_btn.prop('disabled', false).toggleClass('btn-primary').toggleClass('btn-default_custom');
                    $('body').css('cursor', 'default');

                    form[0].reset();
                },
                error: function (result) {
                    target_btn.prop('disabled', false).toggleClass('btn-primary').toggleClass('btn-default_custom');
                    alert('Ошибка соединения с сервером. Попробуйте отправить позднее.');
                    $('body').css('cursor', 'default');
                }
            });
        });
    });
});
