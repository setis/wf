<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

//define('C3_CODECOVERAGE_ERROR_LOG_FILE', __DIR__ . '/../var/logs/c3_error.log'); //Optional (if not set the default c3 output dir will be used)
//$_SERVER['HTTP_X_CODECEPTION_CODECOVERAGE'] = 1;
//require_once __DIR__ . '/../c3.php';

umask(0000);

/**
 * @var Composer\Autoload\ClassLoader $loader
 */
$loader = require_once __DIR__.'/../app/autoload.php';
Debug::enable();

$kernel = new AppKernel('test', true);
$kernel->loadClassCache();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
