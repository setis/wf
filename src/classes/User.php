<?php

namespace classes;

use wf;
use WF\Users\Model\GenderInterface;

class User
{
    const ACCESS_NONE = 0;
    const ACCESS_READ = 1;
    const ACCESS_WRITE = 2;

    const CLIENT_TYPE_PHYS = 1;
    const CLIENT_TYPE_YUR = 2;

    protected $row = array();
    /**
     *
     * @var \models\User
     */
    protected $user;
    private $_access = array();
    static private $_isTech = array();
    static private $_isChiefTech = array();

    function __construct($user_id)
    {
        $this->user = wf::$em->find(\models\User::class, $user_id);
        $this->row = wf::$db->getRow("SELECT * FROM `users` WHERE `id`=" . wf::$db->F($user_id), true);
        if (!$this->row) {
            trigger_error("User (id:$user_id) not found!", E_USER_WARNING);
        }
    }

    public function getRow()
    {
        return $this->row;
    }

    function getId()
    {
        if (isset($this->row['id']))
            return $this->row['id'];
        return false;
    }

    function getLogin()
    {
        return $this->row['login'];
    }

    function getPhoto()
    {
        return '/adm_users/user_avatar?id=' . $this->row["id"];
    }

    function getPassword($md5 = false)
    {
        return $md5 ? md5($this->row['pass']) : $this->row['pass'];
    }

    function getFio()
    {
        if (isset($this->row['fio']))
            return $this->row['fio'];

        return false;
    }

    function getEmail()
    {
        return $this->row['email'];
    }

    function getGender()
    {
        return $this->row['gender'];
    }

    function getGenderMemo()
    {
        return GenderInterface::GENDERS[$this->row['gender']];
    }

    function getAccessType()
    {
        return $this->row['access_type'];
    }

    function getPhones()
    {
        return $this->row['phones'];
    }

    function getIntPhone()
    {
        return $this->row['internalphone'];
    }

    function getComment($nl2br = true)
    {
        return $nl2br ? nl2br($this->row['comment']) : $this->row['comment'];
    }

    function getDolzn()
    {
        return $this->row['dolzn'];
    }

    function getPosId()
    {
        return $this->row['pos_id'];
    }

    function getToJobDate()
    {
        return $this->row['tojob'];
    }

    function getOutJobDate()
    {
        return $this->row['outjob'];
    }

    function getOtdelId()
    {
        return $this->row['otdel_id'];
    }

    function getBirthday($format = "d.m.Y")
    {
        return $this->row['birthday'] ? rudate($format, $this->row['birthday']) : false;
    }

    function getRpp()
    {
        return $this->row['rpp'];
    }

    function isActive()
    {
        return $this->row['active'] ? true : false;
    }

    function getTemplate()
    {
        if (isset($this->row["tpl"]) && $this->row["tpl"]) {
            return $this->row["tpl"];
        }
        else {
            return "default";
        }
    }

    static function isTech($id)
    {
        if (!$id)
            return true;

        if (!isset(self::$_isTech[$id])) {
            self::$_isTech[$id] = wf::$db->getField("SELECT COUNT(`user_id`) FROM `list_empl` WHERE `user_id`=" . wf::$db->F($id) . ";");
        }
        return self::$_isTech[$id] > 0 ? true : false;
    }

    static function isChiefTech($id)
    {
        if (!$id)
            return false;

        if (empty(self::$_isChiefTech[$id])) {
            $query = "SELECT `sc_id` FROM `link_sc_chiefs` WHERE `user_id`=" . wf::$db->F($id) . " ;";
            self::$_isChiefTech[$id] = wf::$db->getField($query);
        }

        return self::$_isChiefTech[$id] > 0 ? self::$_isChiefTech[$id] : false;
    }

    static function isChiefTechAllReturn($id)
    {
        if (!$id) return false;

        $query = "SELECT `sc_id` FROM `link_sc_chiefs` WHERE `user_id`=" . wf::$db->F($id) . " ;";
        $out = wf::$db->getAll($query);

        return $out ? $out : false;
    }

    static function getScIdByUserId($user_id)
    {
        if (!$user_id)
            return false;
        return
            wf::$db->getField("SELECT DISTINCT(`sc_id`) FROM `link_sc_user` WHERE `user_id`=" . wf::$db->F($user_id) . ";");
    }

    /**
     * TODO Artem Какой то лютый адище, о том что пользователь Диспетчер можно понять только по должности
     *
     * @return bool
     */
    public function isDispatcher()
    {
        global $DB;

        /**
         * Должности диспетчеров
         */
        $dispatcher_pos = [12, 13, 14, 15];

        $query = "SELECT count(*) FROM users WHERE id = " . $this->getId() . " AND pos_id IN (" . implode(',', $dispatcher_pos) . ")";
        return $DB->getField($query) > 0;
    }


    /**
     * User::getAccess()
     * получить значение разрешения для доступа к плагину
     *
     * @param string $plugin_uid - ид плагина
     * @return array|integer
     *
     * @see константы ACCESS_* в config.php
     */
    function getAccess($plugin_uid = null)
    {
        if (defined('FULL_ACCESS') && FULL_ACCESS)
            return self::ACCESS_WRITE;

        if (!isset($this->_access[$this->getId()])) {
            $this->cacheAccessRules($this->getId());
        }

        if (empty($plugin_uid))
            return $this->_access[$this->getId()];

        if (!isset($this->_access[$this->getId()]) || !isset($this->_access[$this->getId()][$plugin_uid]) || !$this->_access[$this->getId()][$plugin_uid]) {
            return 0;
        }

        return $this->_access[$this->getId()][$plugin_uid];
    }

    /**
     * Simple access caching...
     * @param $user_id
     */
    private function cacheAccessRules($user_id)
    {
        if (empty($user_id)) {
            return;
        }

        $query = "SELECT * FROM `user_access` WHERE `user_id`=" . wf::$db->F($this->getId());

        $tmp = wf::$db->getAll($query);
        foreach ($tmp as $row) {
            $this->_access[$user_id][$row['plugin_uid']] = $row['access'];
        }
    }

    /**
     * User::checkAccess()
     * проверить наличие разрешения для доступа к плагину
     *
     * @param string $plugin_uid - ид плагина
     * @param integer $access - код разрешения
     * @return bool
     *
     * @see константы ACCESS_* в config.php
     */
    function checkAccess($plugin_uid, $access = User::ACCESS_READ)
    {
        if (defined('FULL_ACCESS') && FULL_ACCESS)
            return true;
        return $this->getAccess($plugin_uid) >= $access;
    }

    /**
     * @param $plugin_uid
     * @param int $access
     * @return bool
     */
    public function grantAccess($plugin_uid, $access = User::ACCESS_READ)
    {
        if ($this->getAccess($plugin_uid) != $access) {
            $query = "DELETE
            FROM
                user_access
            WHERE
                user_id = " . wf::$db->F($this->getId()) . " AND
                plugin_uid = " . wf::$db->F($plugin_uid) . "
            ;";
            wf::$db->query($query);

            $query = "INSERT INTO
                user_access
            SET
                user_id = " . wf::$db->F($this->getId()) . ",
                plugin_uid = " . wf::$db->F($plugin_uid) . ",
                access = " . wf::$db->F($access) . "
            ;";
            wf::$db->query($query);
            return wf::$db->errno() == 0;
        } else {
            return true;
        }
    }

    public function revokeAccess($plugin_uid)
    {
        if ($this->getAccess($plugin_uid) > 0) {
            $query = "DELETE
            FROM
                user_access
            WHERE
                user_id = " . wf::$db->F($this->getId()) . " AND
                plugin_uid = " . wf::$db->F($plugin_uid) . "
            ;";
            wf::$db->query($query);

            return wf::$db->errno() == 0;
        } else {
            return true;
        }
    }

    public function getModelUser()
    {
        return wf::$em->getReference(\models\User::class, $this->getId());
    }
}

