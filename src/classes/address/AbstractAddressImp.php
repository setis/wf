<?php
/**
 * mail@artemd.ru
 * 08.03.2016
 */

namespace classes\address;


abstract class AbstractAddressImp
{

    /**
     * @param $address
     * @return Address
     */
    abstract public function cleanupAddress($address);

    abstract public function getKladrCode(Address $address);

    abstract public function getFiasCode(Address $address);

    abstract public function getGeoCords(Address $address);


}
