<?php
/**
 * mail@artemd.ru
 * 08.03.2016
 */

namespace classes\address;


abstract class AbstractAddressBridge
{

    /**
     * @var string
     */
    private $_original_address;

    /**
     * @var Address
     */
    private $address;

    /**
     * @var AbstractAddressImp
     */
    private $imp;

    public function __construct($address, AbstractAddressImp $impl)
    {
        $this->_original_address = $address;
        $this->imp = $impl;

        $this->address = $this->imp->cleanupAddress($this->_original_address);
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address){
        $this->address = $address;
    }

    public function getKladrCode()
    {
        return $this->imp->getKladrCode($this->address);
    }

    public function getFiasCode()
    {
        return $this->imp->getFiasCode($this->address);
    }

    public function getCords()
    {
        return $this->imp->getGeoCords($this->address);
    }


}
