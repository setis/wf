<?php
/**
 * mail@artemd.ru
 * 08.03.2016
 */

namespace classes\address;

class Address
{
    public $raw;

    public $qc_complete;
    public $qc_house;
    public $qc_geo;
    public $postal_code;
    public $postal_box;
    public $country;
    public $region_with_type;
    public $region_type;
    public $region_type_full;
    public $region;
    public $area_with_type;
    public $area_type;
    public $area_type_full;
    public $area;
    public $city_with_type;
    public $city_type;
    public $city_type_full;
    public $city;
    public $city_area;
    public $city_district;
    public $settlement_with_type;
    public $settlement_type;
    public $settlement_type_full;
    public $settlement;
    public $street_with_type;
    public $street_type;
    public $street_type_full;
    public $street;
    public $house_type;
    public $house_type_full;
    public $house;
    public $block_type;
    public $block_type_full;
    public $block;
    public $flat_area;
    public $square_meter_price;
    public $flat_price;
    public $flat_type;
    public $flat_type_full;
    public $flat;
    public $fias_id;
    public $fias_level;
    public $kladr_id;
    public $tax_office;
    public $tax_office_legal;
    public $capital_marker;
    public $okato;
    public $oktmo;
    public $timezone;
    public $geo_lat;
    public $geo_lon;
    public $beltway_hit;
    public $beltway_distance;
    public $unparsed_parts;
    public $qc;

    /*
     * http://dadata.userecho.com/topics/1053
     */
    public $region_fias_id;
    public $area_fias_id;
    public $city_fias_id;
    public $settlement_fias_id;
    public $street_fias_id;
    public $house_fias_id;
    public $region_kladr_id;
    public $area_kladr_id;
    public $city_kladr_id;
    public $settlement_kladr_id;
    public $street_kladr_id;
    public $house_kladr_id;

    public function __toString()
    {
        return json_encode($this, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }
}
