<?php
/**
 * mail@artemd.ru
 * 08.03.2016
 */

namespace classes\address;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class DadataAddressImp extends AbstractAddressImp
{
    public $url = 'https://dadata.ru/api/v2/suggest/address';
    public $token = 'd6d64d1d19b99af0e180928d83e93f274cc3d4a7';

    public function cleanupAddress($raw)
    {
        $client = new Client();
        $request = new Request('POST', $this->url, [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Token '.$this->token,
        ],
        '{"query":"'.$raw.'"}'
        );

        $response = $client->send($request, ['verify' => false]);
        $body = $response->getBody()->getContents();
        $body = json_decode($body, true);

        if(!empty($body) && isset($body['suggestions']) && isset($body['suggestions'][0])) {
            /** @var Address $address */
            $address = new Address();
            foreach ($body['suggestions'][0]['data'] as $k=>$v) {
                if(property_exists(Address::class, $k))
                    $address->$k = $v;
                else {
                    \wf::$logger->alert('Dadata has changed format! Not existing in Address object field: '.$k, $body['suggestions'][0]['data']);
                }
            }
            $address->raw = $raw;
            return $address;
        }

        return null;
    }

    public function getFiasCode(Address $address)
    {
        return $address->fias_id;
    }

    public function getKladrCode(Address $address)
    {
        return $address->kladr_id;
    }

    public function getGeoCords(Address $address)
    {
        return [$address->geo_lat, $address->geo_lon, $address->qc_geo];
    }
}
