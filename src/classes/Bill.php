<?php
/**
 * mail@artemd.ru
 * 2016-02-11
 */

namespace classes;

class Bill extends Task
{
    const UID = 'bills';

    protected $bill = array();

    static function createBill($plat_id,
                               $plat_text,
                               $comment,
                               $sum,
                               $poluch,
                               $client,
                               $num,
                               $srok_date,
                               $poruch_user_ids = 0,
                               $users = 0,
                               $exptype = 0,
                               $agr_id = 0,
                               $clnt_agr = 0,
                               $author_id = null)
    {
        $title = "Cчет $num";
        /** @var $tm \WF\Task\TaskManager */
        $tm = \wf::$container->get('wf.task.task_manager');

        /** @var $em \Doctrine\ORM\EntityManagerInterface */
        $em = \wf::$em;

        $em->beginTransaction();

        try {
            $task = $tm->createTask(\models\Task::BILL, [
                'author_id' => $author_id,
                'title' => $title,
                'comment' => $comment,
                'is_private' => true,
                'executor_ids' => $poruch_user_ids == 0 ? [] : $poruch_user_ids,
                'bounded_user_ids' => $users == 0 ? [] : $users,
            ]);

            $tm->createTicket($task, [
                'payer' => $em->getReference(\models\BillPayer::class, $plat_id),
                'payer_text' => $plat_text,
                'amount' => $sum,
                'supplier' => $poluch ? $em->getReference(\models\ListContr::class, $poluch) : null,
                'number' => $num,
                'payment_date' => new \DateTime($srok_date),
                'type' => $exptype ? $em->getReference(\models\BillType::class, $exptype) : null,
                'agreement' => $agr_id ? $em->getReference(\models\Agreement::class, $agr_id) : null,
                'client' => $client? $em->getReference(\models\ListContr::class, $client) : null,
                'client_agreement' => $clnt_agr ? $em->getReference(\models\Agreement::class, $clnt_agr) : null,
            ]);

            $em->commit();
        }
        catch (\Exception $ex) {
            $em->rollback();
            return false;
        }

        return $task->getId();
    }

    function __construct($task_id)
    {
        global $DB;

        $sql = "SELECT b.*, o.title AS odobr_title, p.name AS plat_name, o.title AS odobr_title, a.title AS agreed_title, e.title AS exec_title FROM `bills` AS b LEFT JOIN `bills_odobr` AS o ON b.odobren=o.id  LEFT JOIN `bills_odobr` AS a ON b.agreed=a.id LEFT JOIN `bills_odobr` AS e ON b.exec=e.id  LEFT JOIN `bills_plat` AS p ON b.plat_id=p.id WHERE `task_id`=".$DB->F($task_id);

        if($this->bill = $DB->getRow($sql, true, false)) {

            parent::__construct($task_id);
        }
    }

    public function getDirection(){
        return $this->bill['direction_id'];
    }

    public function setDirection($direction_id){
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `direction_id`=".$DB->F($direction_id, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getPlatId()
    {
        return $this->bill['plat_id'];
    }

    function getAgrId()
    {
        return $this->bill['agr_id'];
    }

    function setAgrId($value)
    {
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `agr_id`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getClntAgrId()
    {
        return $this->bill['client_agr'];
    }

    function setClntAgrId($value)
    {
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `client_agr`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }


    function getExpTypeId()
    {
        return $this->bill['exp_type_id'];
    }

    function getExpTypeName()
    {
        global $DB;
        $etName = $DB->getField("SELECT `title` FROM `bill_types` WHERE `id`=".$DB->F($this->bill['exp_type_id']).";");
        return $etName ? $etName : false;
    }

    function getExpTypeNameById($id)
    {
        global $DB;
        $etName = $DB->getField("SELECT `title` FROM `bill_types` WHERE `id`=".$DB->F($id).";");
        return $etName ? $etName : false;
    }

    function setExpTypeId($value)
    {
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `exp_type_id`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function setPlatId($value)
    {
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `plat_id`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getPlatName($escape = true)
    {
        return $escape ? htmlspecialchars($this->bill['plat_name']) : $this->bill['plat_name'];
    }

    function getPlatText($escape = true)
    {
        return $escape ? htmlspecialchars($this->bill['plat_text']) : $this->bill['plat_text'];
    }

    function setPlatText($value)
    {
        global $DB;
        $sql = "UPDATE `bills` SET `plat_text`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getSum()
    {
        return $this->bill['sum'];
    }

    function setSum($value)
    {
        global $DB;
        $sql = "UPDATE `bills` SET `sum`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getPoluch($escape = true)
    {
        return $escape ? htmlspecialchars($this->bill['poluch']) : $this->bill['poluch'];
    }

    function setPoluch($value)
    {
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `poluch`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getClient($escape = true)
    {
        return $escape ? htmlspecialchars($this->bill['client']) : $this->bill['client'];
    }

    function setClient($value)
    {
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `client`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getNum($escape = true)
    {
        return $escape ? htmlspecialchars($this->bill['num']) : $this->bill['num'];
    }

    function getBillDesc($escape = true) {
        return $escape ? htmlspecialchars($this->bill['paydesc']) : $this->bill['paydesc'];
    }

    function setBillDesc($value) {
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `paydesc`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getDatePost($escape = true) {
        if ($this->bill['date_srokpost']!= "" && $this->bill['date_srokpost'] != "0000-00-00") {

            return $escape ? htmlspecialchars(date("d.m.Y", strtotime($this->bill['date_srokpost']))) : date("d.m.Y", strtotime($this->bill['date_srokpost']));
        }
    }

    function setDatePost($value) {
        global $DB;
        $value = ($value != "0000-00-00" && $value != "") ? date("Y-m-d", strtotime($value)) : null;

        $sql = "UPDATE `bills` SET `date_srokpost`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        //die($sql);
        $DB->query($sql);
        return $DB->errno() ? false : true;

    }

    function setNum($value)
    {
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `num`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getOdobrId()
    {
        return $this->bill['odobren'];
    }

    function setOdobrId($value)
    {
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `odobren`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getOdobrTitle($escape = true)
    {
        return $escape ? htmlspecialchars($this->bill['odobr_title']) : $this->bill['odobr_title'];
    }

    function setAgreedId($value)
    {
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `agreed`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getAgreedId()
    {
        return $this->bill['agreed'];
    }

    function getAgreedTitle($escape = true)
    {
        return $escape ? htmlspecialchars($this->bill['agree_title']) : $this->bill['agree_title'];
    }

    function setExecId($value)
    {
        global $DB;
        if (empty($value)) {
            $value = null;
        }
        $sql = "UPDATE `bills` SET `exec`=".$DB->F($value, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function getExecId()
    {
        return $this->bill['exec'];
    }


    function getExecTitle($escape = true)
    {
        return $escape ? htmlspecialchars($this->bill['exec_title']) : $this->bill['exec_title'];
    }


    function getDateSrok($format = "d.m.Y")
    {
        return rudate($format, $this->bill['date_srok']);
    }

    function setDateSrok($date)
    {
        global $DB;

        if(preg_match("/^[0-9]+$/", $date)) $date = date("Y-m-d", $date);
        elseif(preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/", $date, $d)) $date = "{$d[3]}-{$d[2]}-{$d[1]}";

        $sql = "UPDATE `bills` SET `date_srok`=".$DB->F($date, true)." WHERE `task_id`=".$DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function setPayment($ammount, $date = false) {
        global $DB, $USER;
        if (!$ammount) return false;
        $sql = "INSERT INTO `bills_payments` (`bill_id`, `ammount`, `pay_date`, `user_id`, `op_date`) VALUES
				(".$DB->F($this->getId()).", ".$DB->F(str_replace(" ", "", $ammount)).", ".$DB->F($date ? $date : date("Y-m-d H:i:s")).",
				".$DB->F($USER->getId()).", ".$DB->F(date("Y-m-d H:i:s")).");";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        else return true;
    }

    function deletePayment($id) {
        global $DB;
        if (!$id) return false;
        $sql = "DELETE FROM `bills_payments` WHERE `id`=".$DB->F($id)." AND `bill_id`=".$DB->F($this->getId()).";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        else return true;
    }

    function getPayments() {
        global $DB;
        $sql = "SELECT * FROM `bills_payments` WHERE `bill_id`=".$DB->F($this->getId()).";";
        $DB->query($sql);
        if ($DB->num_rows()) {
            while($result = $DB->fetch(true)) {
                $r[] = $result;
            }
            $DB->free();
            return $r;
        } else {
            $DB->free();
            return false;
        }
    }

    function addPayment($ammount, $date, $type_id) {
        global $DB, $USER;
        if (!$ammount || !$date) {
            return false;
        } else {
            $opdate = date("Y-m-d H:i:s", strtotime($date));
            $ammount = str_replace(" ", "", $ammount);
            $sql = "INSERT INTO `bills_payments` (`bill_id`, `ammount`, `pay_date`, `user_id`, `op_date`, `type_id`)
					VALUES (
					".$DB->F($this->getId()).",
					".$DB->F($ammount).",
					".$DB->F($opdate).",
					".$DB->F($USER->getId()).",
					".$DB->F(date("Y-m-d H:i:s")).",
					".$DB->F($type_id)."
                );";
            $DB->query($sql);
            if ($DB->errno()) return false;
            else {
                return true;
            }
        }

    }


}
