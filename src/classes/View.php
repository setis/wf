<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 12.01.2016
 */

namespace classes;

use classes\exceptions\ApplicationException;
use classes\web\Asset;
use wf;

class View extends Component
{

    const POS_HEADER = 1;
    const POS_FOOTER = 2;

    /**
     * @var \classes\Plugin
     */
    public $plugin;

    /*
     * @var array
     */
    public $params;

    /**
     * @var string
     */
    private $layout;

    /**
     * @var string
     */
    private $layoutsPath;

    /**
     * @var Asset[][]
     */
    private $_js = [];

    /**
     * @var Asset[][]
     */
    private $_css = [];

    /**
     *
     * @param $view
     * @return string
     * @throws \ErrorException
     */
    public function getViewFile($view)
    {
        if (strncmp($view, '//', 2) === 0) {
            // e.g. "//layouts/main"
            $path = dirname($this->getLayoutsPath()) . DIRECTORY_SEPARATOR . ltrim($view, '/') . '.php';
        } elseif (strncmp($view, DIRECTORY_SEPARATOR, 1) === 0) {
            $path = realpath(getcfg('root_dir') . $view);
        } else {
            $reflection = new \ReflectionClass($this->plugin);
            $dir = dirname($reflection->getFilename());
            $file = $dir . DIRECTORY_SEPARATOR . $view . '.php';
            $path = (DIRECTORY_SEPARATOR === '\\')
                ? str_replace('/', '\\', $file)
                : str_replace('\\', '/', $file);
        }

        $viewFile = realpath($path);
        if (!file_exists($viewFile)) {
            throw new ApplicationException('Not existing view file: ' . $view);
        }

        return $viewFile;
    }

    /**
     *
     * @param $view
     * @param $data
     * @return string
     */
    public function render($view, $data = [])
    {
        $content = $this->renderPartial($view, $data);

        if ($layout = $this->getLayoutFile()) {
            $this->params['content'] = $content;
            $content = $this->renderPartial('//layouts/' . $this->layout, $this->getParams());
        }

        return $content;
    }

    /**
     * @param $view
     * @param array $data
     * @return string
     * @throws \ErrorException
     */
    public function renderPartial($view, $data = [])
    {
        ob_start();
        ob_implicit_flush(false);
        extract($data, EXTR_OVERWRITE);
        require($this->getViewFile($view));
        $content = ob_get_clean();

        return $content;
    }

    /**
     * @return bool|string
     * @throws \ErrorException
     */
    public function getLayoutFile()
    {
        if ($this->layout) {
            $file = $this->layoutsPath . DIRECTORY_SEPARATOR . $this->layout . '.php';
            $path = (DIRECTORY_SEPARATOR === '\\')
                ? str_replace('/', '\\', $file)
                : str_replace('\\', '/', $file);

            $viewFile = realpath($path);
            if (!file_exists($viewFile)) {
                throw new ApplicationException('Not existing layout file: ' . $this->layout);
            }

            return $viewFile;
        }

        return false;
    }

    /**
     *
     * @return array
     */
    private function getParams()
    {
        return $this->params;
    }

    /**
     * @return mixed
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param mixed $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return mixed
     */
    public function getLayoutsPath()
    {
        return $this->layoutsPath;
    }

    /**
     * @param mixed $layoutsPath
     */
    public function setLayoutsPath($layoutsPath)
    {
        $this->layoutsPath = $layoutsPath;
    }

    /**
     * @return string
     */
    public function getPageTitle()
    {
        return getcfg('system.name') . ' - ' . $this->plugin->getPageTitle();
    }

    /**
     * @param $className
     * @param array $params
     * @return mixed
     */
    public function widget($className, $params = [])
    {
        return wf::$wf->widget($className, $params)->run();
    }

    /**
     * @param Asset $code
     */
    public function registerJs(Asset $code)
    {
        $this->_js[$code->position][$code->getId()] = $code;
    }

    /**
     * @param Asset $code
     */
    public function registerCss(Asset $code)
    {
        $this->_css[$code->position][$code->getId()] = $code;
    }

    /**
     * @param $at_pos
     * @return string
     */
    public function renderJs($at_pos)
    {
        $list = '';
        if (!empty($this->_js[$at_pos])) {
            foreach ($this->_js[$at_pos] as $js) {
                $list .= $js->publish();
            }
        }

        return $list;
    }

    /**
     * @param $at_pos
     * @return string
     */
    public function renderCss($at_pos)
    {
        $list = '';
        if (!empty($this->_css[$at_pos])) {
            foreach ($this->_css[$at_pos] as $css) {
                $list .= $css->publish();
            }
        }

        return $list;
    }

    /**
     * @return string
     */
    public function header()
    {
        /**
         * TODO: Assets publication fills assets array in View after clientCall render method View::header();
         */
        $result = '';
        $result .= $this->renderCss(self::POS_HEADER);
        $result .= $this->renderJs(self::POS_HEADER);

        return $result;
    }

    /**
     * @return string
     */
    public function footer()
    {
        $result = '';
        $result .= $this->renderCss(self::POS_FOOTER);
        $result .= $this->renderJs(self::POS_FOOTER);

        return $result;
    }

}
