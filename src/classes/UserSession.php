<?php
/**
 * mail@artemd.ru
 * 2016-02-10
 */

namespace classes;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use models\User as ModelUser;

class UserSession extends User
{
    /**
     *
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;

        if ($this->Alive()) {
            $user = $this->tokenStorage->getToken()->getUser();
            if ($user instanceof ModelUser) {
                parent::__construct($user->getId());
            }
        }
    }

    public function __sleep()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function Alive()
    {
        if (defined('FULL_ACCESS') && FULL_ACCESS)
            return true;

        return $this->isLoggedIn();
    }

    public function isLoggedIn()
    {
        return $this->tokenStorage->getToken()->isAuthenticated();
    }
}
