<?php
namespace classes;

use adm_users_plugin;
use Doctrine\DBAL\Connection;
use LogicException;
use models\Task as OrmTask;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use wf;

/**
 * Class Plugin
 * @property-read array $plugins of plugins config plugins.inc.php
 * @property-read array $config
 */
class Plugin extends Component implements ContainerAwareInterface
{

    public $layout = 'common';
    public $layoutsPath;
    public $params = array();

    protected $uid = '';
    protected $name = '';
    protected $is_hidden = false;

    private $_pageTitle;

    private $view;

    /**
     * @var array
     */
    private $_events;

    /**
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @return HTML_Template_IT
     */
    static public function getTpl($path)
    {
        return new HTML_Template_IT($path);
    }

    /**
     * Plugin::__construct()
     *
     * @param string $uid - ид плагина
     */
    function __construct($uid, $session_id = '', $ip = '')
    {
        parent::__construct();

        $this->layoutsPath = getcfg('root_dir') . '/templates/layouts';

        $plugins = Plugin::getPlugins();

        if (isset($plugins[$uid]) && isset($plugins[$uid]['name']) && file_exists(path2("plugins/$uid/$uid.php"))) {
            $this->uid = $uid;
            $this->name = $plugins[$uid]['name'];
            $this->is_hidden = (isset($plugins[$uid]['name']) && isset($plugins[$uid]['name']['hidden']) && $plugins[$uid]['name']['hidden']) ? true : false;
        }

        $this->view = new View(array(
            'plugin' => $this,
        ));

    }

    /**
     * @return array
     */
    public static function getPlugins()
    {
        global $PLUGINS;
        return $PLUGINS;
    }

    /**
     * @return array
     */
    public function getPluginsAccessRules()
    {
        if (!empty($this->_events))
            return $this->_events;

        $result = [];
        foreach (Plugin::getPlugins() as $plugin_uid => $plugin) {
            $result[$plugin_uid] = $plugin['name'];
        }
        $this->_events = $result;
        return $result;
    }

    /**
     * @param null $alias
     * @return mixed|null
     */
    public function getPluginNameByAlias($alias = null)
    {
        $rules = $this->getPluginsAccessRules();
        return isset($rules[$alias]) ? $rules[$alias] : $alias;
    }

    /**
     * @return mixed
     */
    public function getPageTitle()
    {
        return $this->_pageTitle;
    }

    /**
     * @param mixed $pageTitle
     */
    public function setPageTitle($pageTitle)
    {
        $this->_pageTitle = $pageTitle;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer()
    {
        return $this->container;
    }

    /**
     * @param string $channel
     * @return \Psr\Log\LoggerInterface
     */
    protected function getLogger($channel = 'php')
    {
        return $this->getContainer()->get('monolog.logger.' . $channel);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Session\Session
     */
    protected function getSession()
    {
        return \wf::getSession();
    }

    /**
     *
     * @return \models\User
     */
    protected function getUserModel()
    {
        /* @var $tokenStorage \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface */
        $tokenStorage = $this->getContainer()->get('security.token_storage');
        $token = $tokenStorage->getToken();
        if (null === $token) {
            return null;
        }

        $user = $token->getUser();

        return $user instanceof \models\User ? $user : null;
    }

    /**
     * @return integer|null
     */
    protected static function getUserId()
    {
        return \wf::getUserId();
    }

    /**
     *
     * @return \models\User
     */
    protected function getCurrentUser()
    {
        return $this->getEm()->find(\models\User::class, \wf::getUserId());
    }

    /**
     *
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected function getDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }

    /**
     *
     * @return \Doctrine\ORM\EntityManagerInterface
     */
    protected function getEm()
    {
        return $this->container->get('doctrine.orm.default_entity_manager');
    }

    /**
     *
     * @return Connection
     */
    protected function getConn()
    {
        return $this->container->get('doctrine.dbal.default_connection');
    }

    /**
     * Plugin::getUID() - получить ид плагина
     *
     * @return string ид плагина
     */
    function getUID()
    {
        return strlen($this->uid) ? $this->uid : false;
    }

    /**
     * Plugin::getName() - получить название плагина
     *
     * @param bool $escape - экранировать результат при помощи strip_tags()
     * @return string название плагина
     */
    function getName($escape = true)
    {
        if (!strlen($this->name)) return false;
        return $escape ? strip_tags($this->name) : $this->name;
    }

    /**
     * Plugin::getNameStatic()
     * получить название плагина по его UID
     *
     * @param bool $escape - экранировать результат при помощи strip_tags()
     * @return string название плагина
     */
    static function getNameStatic($uid, $escape = true)
    {
        $plugins = self::getPlugins();
        if (!isset($plugins[$uid]['name'])) return false;
        return $escape ? strip_tags($plugins[$uid]['name']) : $plugins[$uid]['name'];
    }

    function isHidden()
    {
        return $this->is_hidden;
    }

    /**
     * Plugin::getLinkStatic()
     * получить ссылку на событие плагина
     *
     * @param string $uid - идентификатор плагина
     * @param string $event - если не указан, то первое событие
     * @param mixed $params - строка или массив array(param_name=>param_value, ...) параметров запроса
     * @return string
     *
     * @see link3() в lib/common.php
     */
    static function getLinkStatic($uid, $event = '', $params = false)
    {
        $event = trim($event, "/");
        return link3("/$uid/" . ($event ? "$event/" : ''), $params);
    }

    /**
     * Plugin::getLink()
     * получить ссылку на событие плагина
     *
     * @param string $event - если не указан, то первое событие
     * @param mixed $params - строка или массив array(param_name=>param_value, ...) параметров запроса
     * @return string
     *
     * @see Plugin::getLinkStatic()
     */
    function getLink($event = '', $params = false)
    {
        return $this->getLinkStatic($this->getUID(), $event, $params);
    }

    /**
     * Plugin::getPluginOptions()
     * получить HTML option со списком плагинов
     *
     * @param string $sel_id
     * @return string
     */
    static function getPluginOptions($sel_id = '')
    {
        $a = array();
        foreach (Plugin::getPlugins() as $uid => $p) {
            $a[$uid] = $p['name'];
        }
        return array2options($a, $sel_id, true);
    }

    /**
     * Plugin::getEventByIndexStatic()
     * получить plugin_event по номеру в $PLUGINS[$uid]['events']
     *
     * @param string $uid - идентификатор плагина
     * @param integer $index
     * @return string|false
     */
    static function getEventByIndexStatic($uid, $index = 0)
    {
        $plugins = Plugin::getPlugins();
        $events = array_keys($plugins[$uid]['events']);
        return isset($events[$index]) ? $events[$index] : false;
    }

    /**
     * Plugin::getCookie()
     * получить и запомнить Cookie для модуля
     *
     * @param string $cookie_name - название в пределах модуля
     * @param mixed $value - если не указано или null, то получает из cookie
     * @return mixed
     *
     * @see UserSession::getCookie(), UserSession::setCookie()
     */
    function getCookie($cookie_name, $value = null)
    {
        /** @var SessionInterface $bag */
        $bag = $this->getContainer()->get("session");

        $name = $this->getUID() . '_' . $cookie_name;
        $value = isset($value) ? $value : $bag->get($name);

        $bag->set($name, $value);
        return $value;
    }

    /**
     * какая то шаурмячина
     *
     * Plugin::getSessionCookie()
     * получить и запомнить Session Cookie для модуля
     *
     * @param string $cookie_name - название в пределах модуля
     * @param mixed $value - если не указано или null, то получает из session cookie
     * @return string
     */
    function getSessionCookie($cookie_name, $value = null)
    {
        if (!$cookie_name) return false;

        $session = $this->getSession();
        if ($session->has('cookies')) {
            $cookies = $session->get('cookies');
        } else {
            $cookies = [];
        }

        $name = $this->getUID() . '_' . $cookie_name;
        $value = isset($value) ? $value : $cookies[$name];
        $cookies[$name] = $value;

        $session->set('cookies', $cookies);
        return $value;
    }

    /**
     * Plugin::getStatusOptionsStatic()
     * получить список option со статусами для модуля
     *
     * @param string $uid - UID плагина
     * @param integer $sel_id
     * @param bool $show_hidden
     * @return string
     */
    static function getStatusOptionsStatic($uid, $sel_id = 0, $show_hidden = true)
    {
        global $DB, $USER;
        $filter = '';
        if ($USER) {
            $LKUser = adm_users_plugin::isLKUser($USER->getId());
            if ($LKUser) {
                $add_status = array("61", "54", "59", "51", "65", "60", 64, "1", "26", "3", "2", "5", "13", "15", "69", "70", "71", "72", "74", "12", "25", "9", "8", "10", "17", "18");
                $filter = " AND id IN (" . implode(",", $add_status) . ") ";
            }
        }
        $sql = "SELECT `id`, `name` FROM `task_status` WHERE `plugin_uid`=" . $DB->F($uid) . " $filter ORDER BY `name`";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    /**
     * Plugin::getStatusOptions()
     * получить список option со статусами для модуля
     *
     * @param integer $sel_id
     * @param bool $show_hidden
     * @return string
     */
    function getStatusOptions($sel_id = 0, $show_hidden = true)
    {
        return $this->getStatusOptionsStatic($this->getUID(), $sel_id, $show_hidden);
    }

    /**
     * Plugin::getStatusesByTagStatic()
     * получить массив ассоциативных массивов с информацией о подходящих статусах, которые содержат указанный тэг
     *
     * @param string $tag - тег, которому должен соответствовать статус
     * @param string $uid - UID плагина
     * @return array
     */
    static function getStatusesByTagStatic($tag, $uid)
    {
        global $DB;
        $sql = "SELECT * FROM `task_status` WHERE `plugin_uid`=" . $DB->F($uid) . " AND `tag` IS NOT NULL AND `tag` REGEXP " . $DB->F('(^|[;,])' . $tag . '([;,]|$)') . " ORDER BY `name`";
        return $DB->getAll($sql);
    }

    /**
     * Plugin::getStatusesByTag()
     * получить массив ассоциативных массивов с информацией о подходящих статусах, которые содержат указанный тэг
     *
     * @param string $tag - тег, которому должен соответствовать статус
     * @return array
     *
     * @see Plugin::getStatusesByTagStatic()
     */
    function getStatusesByTag($tag)
    {
        return $this->getStatusesByTagStatic($tag, $this->getUID());
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function removefromgfx(Request $request)
    {
        $id = $request->get('id');
        if (null === $id) {
            throw new LogicException("Не указан идентификатор заявки для удаления из Графика работ.");
        }

        /** @var OrmTask $task */
        $task = $this->getEm()->find(OrmTask::class, $id);
        try {
            wf::$taskManager->removeFromSchedule($task, $request->get('reason_text', false), $this->getUserModel());
        } catch (\Exception $exception) {
            return new Response("error");
        }

        if (!$request->isXmlHttpRequest()) {
            return new RedirectResponse($this->getLink('viewticket', "task_id=" . $id), 302);
        }

        return new Response("ok");
    }

    /**
     *
     *
     * @param string $view
     * @param array $data
     * @return string
     */
    public function render($view, $data = array())
    {
        wf::$view->plugin = $this;
        wf::$view->setLayout($this->layout);
        wf::$view->setLayoutsPath($this->layoutsPath);

        return wf::$view->render($view, $data);
    }

}
