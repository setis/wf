<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\web;


class JsCode extends Asset
{
    public $code;

    public function publish()
    {
        return "<script type='text/javascript'>\n{$this->code}\n</script>";
    }
}
