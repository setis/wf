<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\web;

class JsFile extends Asset
{
    public $url;
    public $type = 'text/javascript';

    public function publish()
    {
        return "<script type='{$this->type}' src='{$this->url}'></script>";
    }
}
