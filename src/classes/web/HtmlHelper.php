<?php
namespace classes\web;

use wf;

class HtmlHelper
{
    public static function encode($content, $doubleEncode = true)
    {
        return htmlspecialchars($content, ENT_QUOTES | ENT_SUBSTITUTE, getcfg('encoding'), $doubleEncode);
    }

    public static function decode($content)
    {
        return htmlspecialchars_decode($content, ENT_QUOTES);
    }

}