<?php
/**
 * Usage example
 *
 * $css = Component::createObject(CssCode::className(), [
 * 'code'=>'* {font-size: 10px; }',
 * ]);
 *
 * wf::$view->registerCss( $css );
 */

namespace classes\web;

use classes\Component;
use classes\View;

abstract class Asset extends Component
{
    public $position = View::POS_HEADER;

    private $_id;

    abstract public function publish();

    /**
     * @return mixed
     */
    public function getId()
    {
        if(empty($this->_id)) {
            $this->_id = uniqid('', true);
        }

        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }
}