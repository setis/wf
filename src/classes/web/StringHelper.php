<?php
/**
 * mail@artemd.ru
 * 06.03.2016
 */

namespace classes\web;

use wf;

class StringHelper
{

    public static function encode($content, $doubleEncode = true)
    {
        return htmlspecialchars($content, ENT_QUOTES | ENT_SUBSTITUTE, wf::$container->getParameter('encoding'), $doubleEncode);
    }

    public static function decode($content)
    {
        return htmlspecialchars_decode($content, ENT_QUOTES);
    }

    public static function substr($str, $start, $length = null)
    {
        return mb_substr($str, $start, $length, wf::$container->getParameter('encoding'));
    }

    public static function strlen($s)
    {
        return mb_strlen($s, wf::$container->getParameter('encoding'));
    }

    public static function strrpos($str, $needle, $offset = 0)
    {
        return mb_strrpos($str, $needle, $offset, wf::$container->getParameter('encoding'));
    }
}
