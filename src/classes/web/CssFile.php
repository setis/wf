<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\web;


class CssFile extends Asset
{
    public $url;
    public $rel = 'stylesheet';
    public $media = 'all';
    public $type = 'text/css';

    public function publish()
    {
        return "<link rel='{$this->rel}' type='{$this->type}' href='{$this->url}' media='{$this->media}'/>";
    }
}
