<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\web;


class CssCode extends Asset
{
    public $code;

    public function publish()
    {
        return "<style>\n{$this->code}\n</style>";
    }
}
