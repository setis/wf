<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\widgets\twozero;


use adm_skp_catnames_plugin;
use classes\themes\AbstractWidget;
use kontr_plugin;

class SelectSkpWorkTypes extends AbstractWidget
{
    public function run()
    {
        return $this->render('views/select_skp_work_types', [
            'filter_pricesection' => "<option value=''>-- все --</option>" . adm_skp_catnames_plugin::getOptList(),
            'filter_contrlist_options' => kontr_plugin::getClientList(),
        ]);
    }

}
