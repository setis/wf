<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\widgets\twozero;


use classes\themes\AbstractWidget;

class CallsListWidget extends AbstractWidget
{
    public function run(){
        return $this->render('views/calls_list');
    }
}
