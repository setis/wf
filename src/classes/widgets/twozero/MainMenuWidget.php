<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\widgets\twozero;

use adm_users_plugin;
use classes\Component;
use classes\Plugin;
use classes\themes\AbstractWidget;
use classes\View;
use classes\web\JsCode;
use wf;

class MainMenuWidget extends AbstractWidget
{

    public $profile_url = '/';
    public $name = '';
    public $main_menu = array();
    public $is_prod = true;
    public $user_balance;

    protected function lazyInit($param)
    {
        $this->is_prod = wf::$container->getParameter('kernel.environment') === 'prod';
        /** @var JsCode $js */
        $js = Component::createObject(JsCode::className(), [
            'code' => 'var nobase = "' . getcfg('http_base') . '";',
            'position' => View::POS_FOOTER,
        ]);
        //wf::$view->registerJs( $js );

        $this->setBalance();
        $this->setProfileUrl();
        $this->setUserName();
        $this->setMainMenu();
    }

    private function setBalance()
    {
        /** @var \models\User $user */
        $user = wf::$container->get('security.token_storage')->getToken()->getUser();
        if ($user->isTechnician()) {
            $this->user_balance = wf::$container->get('gerp.core_bundle.twig.user_extension')->balance($user);
        }
    }

    private function setProfileUrl()
    {
        if (wf::$user->checkAccess("adm_users")) {
            $this->profile_url = "/adm_users/user_view?user_id=" . wf::$user->getId();
        } else {
            $this->profile_url = "/";
        }
    }

    private function setUserName()
    {
        $userName = explode(" ", wf::$user->getFio());
        $userName1 = $userName[0] . " " .
            (mb_substr($userName[1], 0, 1) != "" ? mb_substr($userName[1], 0, 1) . ". " : "")
            . " " .
            (mb_substr($userName[2], 0, 1) != "" ? mb_substr($userName[2], 0, 1) . "." : "");

        if (wf::$user->isLoggedIn())
            $this->name = $userName1;
    }

    public function setMainMenu()
    {
        global $PLUGINS, $plugin;

        if ($plugin instanceof Plugin) {
            $current_plugin = $plugin->getUID();
        } else {
            $current_plugin = '';
        }

        $isPersonalUser = adm_users_plugin::isLKUser(wf::$user->getId());

        $a["desktop"] = array("hidden" => false, "name" => "Рабочий стол");

        if (!empty($PLUGINS)) {
            foreach ($PLUGINS as $uid => $p) {
                if ($isPersonalUser) {
                    if ($uid != "services" && $uid != "connections" && $uid != "desktop" && $uid != "accidents" && $uid != "gp")
                        continue;
                }

                if (!$p['hidden'] && wf::$user->checkAccess($uid) > 0) {
                    if ($uid != "desktop")
                        $a[$uid] = Array("hidden" => $p["hidden"], "name" => $p["name"]);
                }
            }
        }

        $showsettings = false;
        $mm = [];
        foreach ($a as $uid => $p) {
            $item = [
                'url' => Plugin::getLinkStatic($uid),
                'name' => htmlspecialchars($p['name']),
                'active' => false,
            ];
            if ($uid == "adm_interface") {
                $showsettings = true;
                continue;
            }


            if ($uid == "agreements") {
                $item['name'] = "
                    <span title=\"Договоры\" class=\"fa fa-file-text-o hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl'>Договоры</span>";
                }

            if ($uid == "addr_interface") {
                $item['name'] = "
                    <span title=\"Адреса\" class=\"fa fa-building hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl'>Адреса</span>";
                }

            if ($uid == "adm_ods") {
                $item['name'] = "
                    <span title=\"ОДС\" class=\"fa fa-key hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl'>ОДС</span>";
                }

            if ($uid == "tmc") {
                $item['name'] = "
                    <span title=\"ТМЦ\" class=\"fa fa-archive hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl'>ТМЦ</span>";
                }

            if ($uid == "tmc2") {
                $item['name'] = "
                    <span title=\"ТМЦv2\" class=\"fa fa-archive hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl\"><sup> 2</sup></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl'>ТМЦv2</span>";
                }

            if ($uid == "reports") {
                $item['name'] = "
                    <span title=\"Отчеты\" class=\"fa fa-bar-chart hidden-xs hidden-sm visible-md hidden-lg visible-xl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xxl'>Отчеты</span>";
                }

            if ($uid == "callviewer") {
                $item['name'] = "
                    <span title=\"Записи телефонных звонков\" class=\"fa fa-play-circle hidden-xs hidden-sm visible-md hidden-lg visible-xl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xxl'>Записи звонков</span>";
                }

            if ($uid == "qc") {
                $item['name'] = "
                    <span title=\"Контроль качества (отзывы)\" class=\"fa fa-thumbs-up hidden-xs hidden-sm visible-md hidden-lg visible-xl\"></span>
                    <span class='visible-sm visible-xs hidden-md hidden-lg visible-xxl'>Отзывы</span>";
                }

            if ($p['name'] == "Отчеты") {
                $item['add'] = 'id="lm_reports"';
            }
            if ($uid == "desktop") {
                $item['name'] = '<span title="Рабочий стол" class="glyphicon glyphicon-home hidden-sm hidden-xs" ></span>
                <span class="visible-xs visible-sm">Рабочий стол</span>';
            }

            if ($current_plugin == $uid ||
                (
                    isset($PLUGINS[$current_plugin]) &&
                    $PLUGINS[$current_plugin]['hidden'] &&
                    $PLUGINS[$current_plugin]['menuparent'] == $uid
                )
            ) {
                $item['active'] = true;
            }
            $mm[] = $item;

        }
        if ($showsettings) {
            $mm[] = [
                'name' => '<span title="Настройки" class="fa fa-cog hidden-xs hidden-sm"> </span></span><span class="visible-sm visible-xs"> Настройки</span>',
                'url' => Plugin::getLinkStatic("adm_interface"),
            ];
        }

        $this->main_menu = $mm;
    }

    protected function doRun()
    {
        return $this->render('views/main_menu', [
            'user_balance' => '',//$this->user_balance,
            'user_name' => $this->name,
            'profile_url' => $this->profile_url,
            'main_menu' => $this->main_menu,
            'is_prod' => $this->is_prod
        ]);

    }
}
