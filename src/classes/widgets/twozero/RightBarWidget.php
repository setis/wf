<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\widgets\twozero;

use classes\themes\AbstractWidget;
use wf;

class RightBarWidget extends AbstractWidget
{
    public function run(){
        $showCallDispButton = false;

        if (wf::$user->isTech(wf::$user->getId()) && wf::$user->getIntPhone()) {
            $showCallDispButton = true;
        }

        return $this->render('views/rightbar', [
            'showCallDispButton' => $showCallDispButton,
            'call_from' => wf::$user->getIntPhone(),
            'disp_number' => getcfg('dispgroupnumber'),
            'fio' => wf::$user->getFio(),
            'email' => wf::$user->getEmail(),
            'phone' => wf::$user->getPhones(),
        ]);
    }
}
