<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\widgets\twozero;


use classes\themes\AbstractWidget;

class BreadcrumbsWidget extends AbstractWidget
{
    public $items = [];

    public function run() {
        return $this->render('views/breadcrumbs', [
            'items' => $this->items,
        ]);
    }
}
