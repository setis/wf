<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

/** @var array $items */

?>
<?php if (!empty($items)): ?>
    <ol class="breadcrumb">
        <?php foreach ($items as $k => $item): ?>
            <?php if (isset($item['active']) && $item['active']): ?>
                <li class="active">
                    <?php if (isset($item['url'])): ?>
                        <a href="<?php echo $item['url']; ?>"><?php echo $item['label']; ?></a>
                    <?php else: ?>
                        <?php echo $item['label']; ?>
                    <?php endif; ?>
                </li>
            <?php else: ?>
                <li><a href="<?php echo $item['url']; ?>"><?php echo $item['label']; ?></a></li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ol>
<?php endif; ?>
