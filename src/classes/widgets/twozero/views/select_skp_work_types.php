<div class="modal fade" role="dialog" id="selskpwtype" style="display: none;z-index:10000 !important">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="float-title" style="padding:0; margin:0;color:#000000;">Выбор вида работ  СКП</h2>
            </div>
            <div class="modal-body selector">
                <div class="container-fluid">
                    <form role="form" class="form-inline search_filter" id="skpwtype_form" method="POST">
                        <input type="hidden" name="filter_start" id="fs_skpwtype" />
                        <input type="hidden" name="skpwtype_contr_id" />

                        <div class="buttons pull-right">
                            <button type='submit' name='filter' value='Поиск' class="btn-primary" >Поиск</button>&nbsp;&nbsp;
                            <button type='reset' name='reset' value='Сброс' class="btn-default" >Сброс</button>
                        </div>
                        <div class="form-group">
                            <label>Код
                                <input type="text" name="filter_id" /></label>
                        </div>
                        <div class="form-group">
                            <label>Название
                                <input type="text" name="filter_name" /></label>
                        </div>
                        <div class="form-group">
                            <label>Раздел прайса
                                <select style="width: 200px;" name='sswt_ps_id'><?php echo $filter_pricesection;?> </select></label>
                        </div>
                    </form>

                    <table class="t1" width="100%">
                        <thead>
                        <tr>
                            <th>Код</th>
                            <th>Название</th>

                            <th>Ед. изм</th>
                            <th>Мин. ед.</th>
                            <th>Цена</th>
                            <th class="hidden-xs hidden-sm">Контрагент</th>
                        </tr>
                        </thead>
                        <tbody class="searchskpwtypes">
                        <tr class="no_skpwtypes"><td colspan="6"><p class="redmessage">Виды работ СКП отсутствуют!</p></td></tr>
                        <tr class="loader_skpwtypes"><td colspan="6"><div id="loader_skpwtypes">
                                    <div class="center"><img src="/templates/twozero/images/loading_animation.gif" /></div>
                                    <div class="center">Поиск, ждите...</div>
                                </div></td>
                        </tr>
                        </tbody>
                    </table>

                    <div>
                        <div class="pull-left label label-primary" style="margin: 3px 6px 0 0;">Найдено: <span class="badge skpwtypes_search_total"></span></div><span class="skpwtypes_search_pages"><ul class="pages">
                                <li class="selected"><span>1</span></li></ul></span>
                    </div>
                    <br />
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class=" btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>