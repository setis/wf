<div class="modal fade" id="obzvonform" role="dialog" style="display: none !important;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Форма обзвона</h4>
            </div>
            <div class="modal-body">
                <div id="oprosbody"></div>
                <br/>
                <center>
                    <button type="button" id="gosaveopros" value="Сохранить" class="btn-primary"><span class="glyphicon glyphicon-save"></span> Сохранить</button>
                    &nbsp;&nbsp;&nbsp;
                    <button type="button" id="cancel_opros" value="Отменить" class="btn-default">Отменить</button>
                </center>
                <br/>
            </div>
        </div>
    </div>

</div>