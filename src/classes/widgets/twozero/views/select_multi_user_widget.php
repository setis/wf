<div class="modal fade" role="dialog" id="seluser_mult" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="float-title" style="padding:0; margin:0;color:#000000;">Выбор пользователей</h2>
            </div>
            <div class="modal-body selector">
                <div class="container-fluid">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
                        <form role="form" class="form-inline search_filter" id="selectuser_mult_form" method="POST">
                            <input type="hidden" name="filter_start" id="fs_seluser"/>
                            <div class="buttons pull-right">
                                <button type='submit' name='filter' value='Поиск' class="btn-primary">Поиск</button>
                                &nbsp;&nbsp;
                                <button type='reset' name='reset' value='Сброс' class="btn-default">Сброс</button>

                            </div>

                            <div class="form-group">
                                <label>ФИО</label>
                                <input type="text" name='su_username'/>
                            </div>
                            <div class="form-group">
                                <label>Тип</label>
                                <select name='su_type_id'>
                                    <option selected="selected" value="0">--все</option>
                                    <option value='1'>Пользователь</option>
                                    <option value='3'>Техник</option>
                                    <option value='4'>Руководитель СЦ</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Подразделение</label>
                                <select style="width: 200px;" name='su_podr_id'>
                                    <?php echo $podr_list; ?>
                                </select>
                            </div>
                        </form>
                        <table class="t1" width="100%">
                            <thead>
                            <tr>
                                <th>ФИО</th>
                                <th>Должность</th>
                            </tr>
                            </thead>
                            <tbody class="searchuser">
                            <tr class="no_users">
                                <td colspan="6"><p class="redmessage">Пользователи отсутствуют!</p></td>
                            </tr>
                            <tr class="loader_seluser_mult">
                                <td colspan="6">
                                    <div id="loader_seluser_mult">
                                        <div class="center"><img src="/templates/twozero/images/loading_animation.gif"/></div>
                                        <div class="center">Поиск, ждите...</div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div>
                            <div class="pull-left label label-primary" style="margin: 3px 6px 0 0;">Найдено: <span class="badge user_search_total"></span></div><span
                                class="user_search_pages"><ul class="pages">
                                    <li class="selected"><span>1</span></li>
                                </ul>
                </span>
                        </div>
                        <br/>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <h2 class="float-title" style="margin-top: 0;">Выбранные записи</h2>
                        <div id="seluser_mult_list" style="margin-bottom: 5px; height: 290px; border: 1px solid; overflow-y: scroll; overflow-x:auto;">
                            <table width="100%" border="0" class="seluser_mult_tbody">
                                <tr class="seluser_mult_norows">
                                    <td width="100%" colspan="2"><p class="redmessage" style="width:90%; text-align:center;">-- не выбрано ни одной записи --</p></td>
                                </tr>
                            </table>
                        </div>
                        <button type="button" class="btn-success" name="send_seluser_mult_list" value="Выбрать список">Выбрать список</button>
                        &nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn-warning" name="clear_seluser_mult" value="Очистить">Очистить</button>
                        <p><br/></p>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class=" btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>