<?php
/**
 * @var $this \classes\View
 * @var $profile_url string
 * @var $user_name string
 * @var $user_balance string
 * @var $main_menu array
 * @var $is_prod boolean
 */
?>

<link rel="stylesheet" href="/assets/legacy/desktop_10.16.css">
<nav class="navbar navbar-inverse navbar-fixed-top navbar__tidy">
    <div class="container-fluid">

        <!-- Logo -->
        <div class="navbar-header logo-menu_box">
            <div class="navbar-brand">
                <span class="text-nowrap logo-menu">
                    <img width='20' class="logo-menu_img gsslogo" src='/templates/images/logo_trnt_100.png'/>
                    <span class="logo-menu_text "><?php echo getcfg('system.name'); ?></span>
                </span>
                <span><img style='display: none;' id='ajaxloader' src='/templates/twozero/images/preload.gif'/></span>

            </div>
            <button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Меню</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Login -->
        <div class="navbar-brand small pull-right hidden-xs hidden-sm login-menu_box">
            <div class="login-menu_name-box">
                <span class="right small">
                    <a href="<?php echo $profile_url; ?>">
                        <span style="color: white !important;"><?php echo $user_balance; ?></span>

                        <span class="fa fa-user" style="color: white !important;"></span>
                        <!-- s.b. DIV v -->
                        <span style="color: white !important;">
                            <?php echo $user_name; ?></span>
                    </a>
                </span>
            </div>
            <div class="login-menu_logout">
                <span class="right small">
                    <a class="right" href="/logout" style="color: white !important;">
                        <span class="fa fa-sign-in"></span>
                        Выход
                    </a>
                </span>
            </div>
        </div>

        <!-- Text menu -->
        <div id="navbar" class="navbar-collapse collapse pull-left text_menu" style="">
            <ul class="nav navbar-nav">
                <?php foreach ($main_menu as $item): ?>
                    <li class="pull-left <?php echo isset($item['active']) && $item['active'] ? "active" : ""; ?>  menu_item__tidy">
                        <a class="<?php echo isset($item['active']) && $item['active'] ? "active" : ""; ?>" <?php echo isset($item['add']) ? $item['add'] : ''; ?>
                           href="<?php echo $item['url']; ?>">
                            <?php echo $item['name']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <ul class="visinmenu nav navbar-nav navbar-right visible-xs visible-sm">
                <li class="nav-divider"></li>
                <li>
                    <a href="<?php echo $profile_url; ?>">
                        <?php echo $user_balance; ?>
                        <span class="glyphicon glyphicon-user"></span> <?php echo $user_name; ?>
                    </a>
                </li>
                <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> Выход</a></li>
            </ul>
        </div><!--/.nav-collapse -->


    </div>
</nav>

