<!-- Impersonate form -->
<div id='loginat' class='modal fade' role="dialog">
    <div class='modal-dialog' role="document">
        <div class='modal-content'>
            <form role='form' action='/adm_users/setfa' method='POST'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                    <h4 class='modal-title'>Воплотиться в...</h4>
                </div>
                <div class='modal-body'>
                    <div class='form-group'>
                        <select id='impersonate_select' class='form-control select2' name='targetuser_id' style="width: 100%"><?php echo $users; ?></select>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='submit' value='Переключить' class='btn btn-primary'>Воплотиться</button>
                </div>
            </form>
        </div>
    </div>
</div>
