<div id="footer">
    <div class="footer">
        <small> <?php echo getcfg('system.name'); ?> &copy; 2011-<?php echo date('Y'); ?></small>
        <small style="float:right;">
            <a href="mailto:<?php echo getcfg('developer_email'); ?>">
                <?php echo getcfg('developer_email'); ?>
            </a>
        </small>
    </div>
</div>
