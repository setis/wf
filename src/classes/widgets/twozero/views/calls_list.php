<div class="modal fade" id="callticketlist" role="dialog" style="display: none !important;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Звонки по заявке</h4>
            </div>
            <div class="modal-body">
                <button type="button" name="refreshcallticket" id="refreshcallticket" value="Перезагрузить записи из АТС"><span
                        class="glyphicon glyphicon-refresh"></span> Перезагрузить записи из АТС
                </button>
                <br/>
                <table class="t1" width="100%">
                    <thead>
                    <tr>
                        <th>Дата/ Пользователь</th>
                        <th>Телефоны</th>
                        <th>Статус</th>
                        <th>Запись</th>
                    </tr>
                    </thead>
                    <tbody class="calllist">
                    </tbody>
                </table>


                <center>
                    <button type="button" id="cancel_calls" value="Отменить" class="btn-default">Закрыть</button>
                </center>
                <br/>
            </div>
        </div>
    </div>
</div>