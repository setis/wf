<?php
/**
 * @var $showCallDispButton bool
 * @var $call_from string
 * @var $disp_number string
 */
?>
<div class="center rightbar hidden-print" style="background: #FFFFFF;">
    <button type="button" id="movepanel" class="rbar_button btn-default" title="Переместить эту панель">
        <span class="fa fa-arrows" ></span>
    </button>

    <button type="button" id="openobject" class="openobject btn-info rbar_button" title="Открыть заявку или объект по номеру">
        <span class="fa fa-folder-open-o fa-3x"></span>
    </button>

    <button type="button" id="feedback_button" class="btn-warning rbar_button" title="Отправить сообщение в техподдержку">
        <span class="fa fa-thumbs-o-up fa-3x"></span>
    </button>

    <?php if ($showCallDispButton): ?>
        <button type="button" title="Позвонить Диспетчеру Горсвязи: <?php echo $disp_number; ?>"
                class="calltotechrnav btn-success rbar_button"
                name="calltotech1" callfrom="<?php echo $call_from; ?>" calltoc="<?php echo $disp_number; ?>">
            <span class="fa fa-phone fa-3x"></span>
        </button>
    <?php endif; ?>

</div>

<!-- Open any ticket modal -->
<div class="modal fade" id="openobject_dlg" role="dialog" style="display: none !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Открыть объект любого типа</h4>
            </div>
            <div class="modal-body">
                <form method="POST" role="form" action="/open" class="form form-inline">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table-condensed">
                        <tr>
                            <td style="padding-right: 10px;" width="33%" align="right" valign="top">Введите ID объекта:<br/>
                                <small>Тип объекта будет определен автоматически</small>
                            </td>
                            <td><input type="text" required="required" placeholder="ID (№) объекта" name="object_id" value=""/></td>
                        </tr>
                    </table>
                    <p>&nbsp;</p>
                    <center>
                        <button type="submit" class="btn-primary"><span class="glyphicon glyphicon-open"></span> Открыть</button>
                        &nbsp;&nbsp;&nbsp;
                        <button type="button" data-dismiss="modal" value="Отменить" class="btn-default">Закрыть</button>
                    </center>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>

<!--BugReport form-->
<div class="modal fade" id=bugreport_dlg role="dialog" style="display: none !important;">
    <div class="modal-dialog">
        <div class="modal-content bagreport-container_regular">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Написать разработчикам GERP</h4>
            </div>

            <div class="modal-body">

                <div class="rep-mode__container">

                    <!-- Send another -->
                    <div class="br_confirm_tpl">
                        <div class="confirm-tpl__container">
                            <div class="confirm-tpl__title">
                                <p>Спасибо! Ваше обращение принято.</p>
                            </div>
                            <div class="confirm-tpl__message">
                            </div>
                            <div class="confirm-tpl__btn text-center">
                                <button type="button" data-dismiss="modal" value="Отменить" class="btn-primary br-button_base">Закрыть</button>
                            </div>
                        </div>
                    </div>

                    <ul class="nav nav-tabs tabs_feedback" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#bug" aria-controls="home" role="tab" data-toggle="tab">
                                <i class="fa fa-bug"></i> Ошибка
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#idea" aria-controls="profile" role="tab" data-toggle="tab">
                                <span class="glyphicon glyphicon-pencil"></span> Обращение
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content tabs_feedback">
                        <div role="tabpanel" class="tab-pane active" id="bug">
                            <!--BugReport-->
                            <form method="POST" id="Bugreport_form" role="form" action="/bugreport/bugreport-service.json" class="form form-inline" enctype="multipart/form-data">
                                <div class="br-notepad__entry br-notepad__entry_main">
                                    <p>Какое действие вы хотели совершить?</p>
                                    <textarea name="br_action" placeholder="Например, я хотел отправить сообщение." required></textarea>
                                </div>
                                <div class="br-notepad__entry br-notepad__entry_main">
                                    <p>Что не получилось? <span class="text-muted">(Не больше 255 символов)</span> </p>
                                    <textarea name="br_failure" maxlength="255" placeholder="Не работает кнопка &quot;Отправить&quot;." required></textarea>
                                </div>
                                <div class="br-notepad__entry br-notepad__entry_main">
                                    <p>Как по вашему мнению это должно работать?</p>
                                    <textarea name="br_wish" placeholder="После нажатия кнопки, сообщение должно отправляться, а форма закрываться."></textarea>
                                </div>
                                <div class="br-notepad__entry br-notepad__entry_main">
                                    <p>Так же вы можете приложить скриншот или другой файл, который может помочь в решении вашей проблемы.</p>
                                    <input type="file" name="files[]" multiple />
                                </div>

                                <!-- User data - readonly -->
                                <div class="br-notepad__secondary-info clearfix">
                                    <div class="secondary__item">
                                        <p>Пользователь</p>
                                        <input name="br_name" readonly disabled class="br-notepad__entry_readonly" type="text" value="<?php echo $fio;?>">
                                    </div>
                                    <div class="secondary__item">
                                        <p>Email</p>
                                        <input name="email" readonly disabled class="br-notepad__entry_readonly" type="text" value="<?php echo $email;?>">
                                    </div>
                                    <div class="secondary__item">
                                        <p>Телефон</p>
                                        <input name="phone" readonly disabled class="br-notepad__entry_readonly" type="text" value="<?php echo $phone;?>">
                                    </div>
                                </div>


                                <div class="button-block button-block_regular">
                                    <button type="submit" class="btn-primary br-button_base"><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Отправить</button>
                                    &nbsp;&nbsp;&nbsp;
                                    <button type="button" data-dismiss="modal" value="Отменить" class="btn-default br-button_base">Закрыть</button>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="idea">
                            <!--FreeForm-->
                            <form method="POST" id="Freeform" role="form" action="/bugreport/bugreport-freeform.json" class="form form-inline" enctype="multipart/form-data">
                                <div class="br-notepad__entry br-notepad__entry_freeform">
                                    <p>Обращения в свободной форме имеют более низкий приоритет обработки. Для отправки важной информации используйте форму "Сообщить об ошибке"</p>
                                    <p>Содержание обращения:</p>
                                    <textarea name="br_freeform" required></textarea>
                                </div>
                                <div class="secondary__item">
                                    <p>Так же вы можете приложить скриншот или другой файл, который может помочь в решении вашей проблемы.</p>
                                    <input type="file" name="files[]" multiple/>
                                </div>

                                <!-- User data - readonly -->
                                <div class="br-notepad__secondary-info clearfix">
                                    <div class="secondary__item">
                                        <p>Пользователь</p>
                                        <input name="br_name" readonly disabled class="br-notepad__entry_readonly" type="text" value="<?php echo $fio;?>">
                                    </div>
                                    <div class="secondary__item">
                                        <p>Email</p>
                                        <input name="email" readonly disabled class="br-notepad__entry_readonly" type="text" value="<?php echo $email;?>">
                                    </div>
                                    <div class="secondary__item">
                                        <p>Телефон</p>
                                        <input name="phone" readonly disabled class="br-notepad__entry_readonly" type="text" value="<?php echo $phone;?>">
                                    </div>
                                </div>

                                <div class="button-block button-block_regular">
                                    <button type="submit" class="btn-primary br-button_base"><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Отправить</button>
                                    &nbsp;&nbsp;&nbsp;
                                    <button type="button" data-dismiss="modal" value="Отменить" class="btn-default br-button_base">Закрыть</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
