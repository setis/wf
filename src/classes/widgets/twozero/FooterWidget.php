<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\widgets\twozero;


use classes\themes\AbstractWidget;

class FooterWidget extends AbstractWidget
{

    public function run(){
        return $this->render('views/footer_widget', [

        ]);
    }

}
