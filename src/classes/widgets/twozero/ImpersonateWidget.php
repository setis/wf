<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\widgets\twozero;

use adm_users_plugin;
use classes\themes\AbstractWidget;
use classes\User;
use wf;

class ImpersonateWidget extends AbstractWidget
{
    public function run()
    {
        /* @var $security \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface */
        $security = wf::$container->get('security.authorization_checker');

        if(!$security->isGranted('ROLE_CAN_TO_BE')) {
            return '';
        }

        $users = ($security->isGranted('ROLE_CAN_TO_BE_ANY_USER') ? adm_users_plugin::getUserActiveList() : adm_users_plugin::getUserActiveTechList());

        return $this->render('views/impersonate', [
            'users' => $users,
        ]);
    }
}
