<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\widgets\twozero;

use adm_sc_plugin;
use adm_users_plugin;
use classes\themes\AbstractWidget;

class SelectSingleUserWidget extends AbstractWidget
{
    public function run()
    {
        return $this->render('views/select_single_user_widget', [
            'podr_list' => "<option value=''>-- все --</option>" . adm_users_plugin::getOtdelOptions(),
            'filter_sc_options' => adm_sc_plugin::getScListByAccess(),
        ]);
    }

}
