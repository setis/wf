<?php
namespace classes;

use Swift_Mailer;
use Swift_Message;

class MultipartEmail implements MultipartEmailInterface
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Swift_Message
     */
    private $message;

    public function __construct( Swift_Mailer $mailer, $fromName, $fromEmail, $email_username = null )
    {
        $this->mailer = $mailer;
        $this->message = $this->mailer->createMessage();

        $this->message->setFrom( $fromEmail, $fromName );

        if ( !empty( $email_username ) ) {
            $header = $this->message->getHeaders();
            $header->addPathHeader( 'Return-Path', $email_username );
        }
    }

    /**
     * @return Swift_Mailer
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * @return Swift_Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    private function explodeEmailAddress( $emails )
    {
        $result = [];

        if ( is_array( $emails ) )
            return $emails;

        if ( $emails = explode( ',', $emails ) ) {
            array_walk( $emails, 'trim' );
        }

        foreach ($emails as $email) {
            if ( strpos( $email, '<' ) !== false ) {
                list( $address, $name ) = array_reverse( array_map( 'trim', explode( "<", str_replace( ">", "", $email ) ) ) );
                $result[ $address ] = $name;
            } else {
                $result[] = trim( $email );
            }
        }

        return $result;
    }

    public function setTo( $to )
    {
        $emails = $this->explodeEmailAddress( $to );
        $this->message->setTo( $emails );
    }

    public function getTo()
    {
        $this->message->getTo();
    }

    public function setFrom( $val )
    {
        $list = $this->explodeEmailAddress( $val );
        list( $email, $name ) = $list[ 0 ];
        $this->message->setFrom( $email, $name );
    }

    public function getFrom()
    {
        return $this->message->getFrom();
    }

    public function setText( $val )
    {
        $this->message->setBody( $val, 'text/plain' );
    }

    public function getText()
    {
        return $this->message->getBody();
    }

    public function setHtml( $val )
    {
        $this->message->setBody( $val, 'text/html' );
    }

    public function getHtml()
    {
        return $this->message->Body;
    }

    public function setSubject( $val )
    {
        $this->message->setSubject( $val );
    }

    public function getSubject()
    {
        return $this->message->getSubject();
    }

    public function send()
    {
        $failedRecipients = [];
        if ( $this->mailer->send( $this->message, $failedRecipients ) === 0 ) {
            echo 'Mailer Error: message was not sent to ' . implode( ', ', $failedRecipients );

            return false;
        } else {
            return true;
        }
    }

}
