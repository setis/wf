<?php
namespace classes;


abstract class AbstractComponent
{

    /**
     * AbstractComponent constructor.
     * @param []|null $attributes
     */
    public function __construct($attributes = null)
    {
        if(!empty($attributes)) {
            foreach ($attributes as $key => $val) {
                if (property_exists($this, $key)) {
                    $this->$key = $val;
                } else {
                    throw new \ErrorException('Attribute ' . $key . ' not exists.');
                }
            }
        }

        if(method_exists(get_called_class(), 'init')) {
            static::init();
        }
    }

    abstract public function init();

    /**
     * Returns the fully qualified name of class.
     * @return string the fully qualified name of this class.
     */
    public static function className()
    {
        return get_called_class();
    }
}