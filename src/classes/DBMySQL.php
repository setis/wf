<?php

namespace classes;

use \wf;
use \Exception;
use Doctrine\DBAL\Connection;
use PDO;
use Doctrine\DBAL\Driver\Statement;

/**
 * Heart of legacy code
 *
 *
 */
class DBMySQL
{

    /**
     * 
     * @var int
     */
    protected $queries_counter = 0;

    /**
     *
     * @var string
     */
    public $encoding = 'utf8';

    /**
     * 
     * @var Statement[]
     */
    protected $result = array();
    
    private $insert_id = null;

    /**
     *
     * @var Connection 
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return bool
     */
    function init()
    {
        return $this->getErrno() == '00000';
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return array();
    }

    public function __wakeup()
    {
        $this->connection = \wf::$container->get('doctrine.dbal.default_connection');
    }

    /**
     * @return bool|Statement
     */
    private function last_result()
    {
        if (sizeof($this->result)) {
            $keys = array_keys($this->result);
            $stmt =  $this->result[array_pop($keys)];
            return $stmt;
        }
        else {
            return false;
        }
    }

    /**
     * @param $sql
     * @throws QueryException On wrong query
     * @return Statement
     */
    function query($sql)
    {
        $this->queries_counter++;
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $this->result[] = $stmt;

        if (preg_match("/^INSERT/i", trim($sql))) {
            $this->insert_id = $this->connection
                ->getWrappedConnection()
                ->lastInsertId();
        }

        return $this->last_result();
    }

    function fetch($assoc = false, $escape = true, $result = null)
    {
        $data = null;
        $result = $result ? $result : $this->last_result();
        if (empty($result)) {
            return false;
        }
        
        if ($assoc) {
            $data = $result->fetch();
        }
        else {
            $data = $result->fetch(PDO::FETCH_NUM);
        }


        if (!$data) {
            return false;
        }

        if ($escape) {
            $data = array_map(array(web\HtmlHelper::class, 'encode'), $data);
        }

        return $data;
    }

    /**
     * @param Resource|null $result
     * @return int
     */
    function num_rows($result = null)
    {
        $result = $result ? $result : $this->last_result();
        if ($result instanceof Statement)
            return $result->rowCount();
        else
            return false;
    }

    function new_id($table)
    {
        if (preg_match("/^`(.+)`$/", $table, $regs)) {
            $table = $regs[1];
        }

        $sql = "SHOW TABLE STATUS LIKE " . $this->F($table);
        $row = $this->getRow($sql, true);
        return $row['Auto_increment'];
    }

    function free($result = null)
    {
        if ($result)
            $key = array_search($result, $this->result, true);

        $result = $result ? $result : array_pop($this->result);
        if ($result instanceof Statement)
            $result->closeCursor();

        if (isset($key))
            unset($this->result[$key]);
    }

    function getError()
    {
        $info = $this->connection->getWrappedConnection()->errorInfo();
        return trim($info[0] . ' [' . $info[1] . ']: ' . $info[2]);
    }

    function getErrno()
    {
        return $this->connection->getWrappedConnection()->errorCode();
    }

    public function errno()
    {
        return $this->connection->getWrappedConnection()->errorCode() !== '00000';
    }

    function destruct()
    {
        foreach ($this->result as $result)
            if ($result instanceof Statement)
                $result->closeCursor();

        return true;
    }

    function getFoundRows()
    {
        $sql = "SELECT FOUND_ROWS()";
        $count = $this->getField($sql, false);

        return $count;
    }

    /**
     * DBMysql::getAll()
     * получить весь результат SQL запроса в виде массива
     *
     * @param string $sql - запрос
     * @param bool $assoc - элементами возврщаемого массива будут ассоциативные массивы
     * @param bool $escape - экранировать значения \lib\web\HtmlHelper::encode
     * @param integer $limit - максимальное количество рядов в результате (-1 = без ограничения), если в $sql не указан LIMIT
     * @return array|bool
     * @throws QueryException
     */
    function getAll($sql, $assoc = true, $escape = true, $limit = 1000)
    {
        $sql = trim(trim($sql), ';');
        if (!preg_match("/^SELECT/i", $sql)) {
            throw new \Doctrine\DBAL\Query\QueryException("Invalid SELECT query: " . $sql);
        }
        if ($limit != -1 && !preg_match("/LIMIT \d+(,\d+)?$/i", $sql))
            $sql .= " LIMIT $limit";

        $out = array();
        $result = $this->query($sql);

        if ($this->getErrno() != '00000')
            return false;

        while ($a = $this->fetch($assoc, $escape, $result))
            $out[] = $a;

        $this->free($result);
        return $out;
    }

    public function error()
    {
        return $this->connection->getWrappedConnection()->errorCode() !== '00000';
    }

    public function insert_id()
    {
        return $this->insert_id;
    }

    /**
     * DB::table() добавляет префикс к имени таблицы
     *
     * @param string $table
     * @param bool $revApostr признак необходимости обернуть название в обатные апострофы (`) применимо только для DB_MySQL
     * @return string имя таблицы с префиксом, готовое для подстановки в запрос
     */
    function table($table, $revApostr = true)
    {
        $table = trim($table, "`'\"");

        switch (get_class($this)) {
            case 'classes\DBMySQL':
                return $revApostr ? "`" . addslashes($table) . "`" : addslashes($table);
            case 'classes\DBOracle':
                return (getcfg('ora_schema') && !strpos($table, '.')) ? getcfg('ora_schema') . ".$table" : $table;
            default:
                throw new \ErrorException("DB::table() called from unsupported class '" . get_class($this) . "'!");
        }
    }

    /**
     * DB::tableField() формирует название поле с названием таблицы
     * например table1.id
     *
     * @param string $table
     * @param string $field
     * @return string
     */
    function tableField($table, $field)
    {
        return self::table($table, false) . '.' . addslashes($field);
    }

    /**
     * DB::T() - алиас к DB::table()
     * @param string $table
     * @return string имя таблицы с префиксом, готовое для подстановки в запрос
     */
    function T($table)
    {
        return self::table($table);
    }

    /**
     * DB::TF() алиас к DB::tableField()
     *
     * @param string $table
     * @param string $field
     * @return string
     */
    function TF($table, $field = 'id')
    {
        return self::tableField($table, $field);
    }

    /**
     * DB::field() - экранирует значение и оборачивает в одинарные кавычки (')
     *
     * @param mixed $value - исходное значение
     * @param integer $maxlength - максимальная длинна, если 0 - то не обрезается
     * @return string значение, готовое для подстановки в запрос
     */
    static function field($value, $maxlength = 0)
    {
        $value = $maxlength ? addslashes(substr($value, 0, $maxlength)) : addslashes($value);
        return "'$value'";
    }

    /**
     * DB::nullfield() - экранирует значение и оборачивает в одинарные кавычки (')
     * в случае пустого значения возвращает NULL
     *
     * @param mixed $value - исходное значение
     * @param integer $maxlength - максимальная длинна, если 0 - то не обрезается
     * @return string значение, готовое для подстановки в запрос
     */
    static function nullfield($value, $maxlength = 0)
    {
        //BUG:sql-mode=STRICT_TRANS_TABLES при null не ставит дефолтное значение а ругается в MySQL 5.x
        if ($value || $value === 0 || $value == '0') {
            return self::field($value, $maxlength);
        }
        else {
            return "NULL";
        }
    }

    /**
     * DB::F() - экранирует значение и оборачивает в одинарные кавычки (')
     * опционально может выозвращать NULL в случае пустого значения
     *
     * @param mixed $value - исходное значение
     * @param bool $allow_null - dthyenm NULL в случае пустого значения
     * @return string значение, готовое для подстановки в запрос
     */
    static function F($value, $allow_null = false)
    {
        return $allow_null ? self::nullfield($value) : self::field($value);
    }

    function getQueriesCount()
    {
        return $this->queries_counter;
    }

    /**
     * DB::getField() - получить одно первое значение из SQL запроса, вида
     * select VALUE from TABLE where ID = 1
     * предполагается, что запрос возвращает только один ряд с одним полем
     *
     * @param string $sql - строка запроса
     * @param bool $escape - экранировать результат
     * @return mixed первое поле из результата или false если записи не найдено
     */
    function getField($sql, $escape = true)
    {
        $result = $this->connection->executeQuery($sql);
        list($ret) = $this->fetch(false, $escape, $result);
        return $ret;
    }

    /**
     * DB::getRow() - получить первый ряд из SQL запроса, вида
     * select NAME, FIO, ... from TABLE where ID = 1
     * предполагается, что запрос возвращает только один ряд
     *
     * @param string $sql - строка запроса
     * @param bool $assoc - вернуть ассоциативный массив
     * @param bool $escape - экранировать результат
     * @return mixed массив (или ассоциативный массив) состоящий из полей результата
     */
    function getRow($sql, $assoc = false, $escape = true)
    {
        $result = $this->connection->executeQuery($sql);
        $ret = $this->fetch($assoc, $escape, $result);
        return $ret;
    }

    /**
     * DB::getCell() - получить первый столбец из SQL запроса, вида
     * select NAME from TABLE where ID > 10 and ID < 100
     * предполагается, что запрос возвращает только одну колонку
     *
     * @param string $sql - строка запроса
     * @param bool $escape - экранировать результат
     * @return mixed массив, элементами которого являются значения первого столбца результата
     */
    function getCell($sql, $escape = true)
    {
        $ret = array();
        $result = $this->connection->executeQuery($sql);
        while (list($celldata) = $this->fetch(false, $escape, $result)) {
            $ret[] = $celldata;
        }
        return $ret;
    }

    /**
     * DB::getCell2()- получить первый и второй столбец из SQL запроса, вида
     * select PARAM_NAME, PARAM_VALUE from TABLE where ID > 10 and ID < 100
     * предполагается, что запрос возвращает 2 колонки, которые помещаются в массив в виде (PARAM_NAME => PARAM_VALUE)
     * предполагается, что значения первого столбца (PARAM_NAME) всегдя будут уникальными, иначе элемент массива перезаписывается
     *
     * @param string $sql - строка запроса
     * @param bool $escape - экранировать результат
     * @return mixed массив, элементами которого являются значения первого столбца результата в качестве ключей, а второго - в качестве значений соответственно
     */
    function getCell2($sql, $escape = true)
    {
        $ret = array();
        $result = $this->connection->executeQuery($sql);
        while (list($key, $value) = $this->fetch(false, $escape, $result)) {
            $ret[$key] = $value;
        }
        return $ret;
    }

    /**
     * @param string $sql - строка запроса
     * @param bool $escape - экранировать результат
     * @return mixed массив, элементами которого являются значения первого столбца результата в качестве ключей, а второго - в качестве значений соответственно
     */
    function getCell3($sql, $escape = true)
    {
        $ret = array();
        $result = $this->connection->executeQuery($sql);
        while ($r = $this->fetch(true, $escape, $result)) {
            if (isset($r['keyval']))
                $ret[$r['keyval']][] = $r;
            else
                throw new \Exception('This table does not have keyval column!');
        }
        return $ret;
    }

    /**
     * DB::updateRow()
     * формирует и выполняет запрос UPDATE для таблицы по ключевому полю и массиву данных
     *
     * @param string $table - название таблицы
     * @param array $data - массив данных array('key_name'=>'key_value', ...)
     * @param string $key - название ключевого поля (его значение должно присутствовать в массиве)
     * @param bool $allow_null - заменять пустые значения на NULL
     * @return bool
     */
    function updateRow($table, $data, $key, $allow_null = true)
    {
        if (empty($key) || !isset($data[$key])) {
            throw new Exception('Data array does not have key value!');
        }

        $update = array();
        foreach ($data as $field => $value) {
            if (!preg_match("/^[0-9]+$/", $field) && $field != $key) {
                $update[] = $this->TF($table, $field) . '=' . $this->F($value, $allow_null);
            }
        }
        if (!sizeof($update)) {
            wf::$logger->warning('No data for update', ['TRACE' => (new Exception())->getTraceAsString()]);
            return false;
        }

        $sql = "UPDATE " . $this->T($table) . " SET " . implode(", ", $update) . " WHERE " . $this->TF($table, $key) . "=" . $this->F($data[$key]);

        $this->connection->executeUpdate($sql);
        return $this->errno() ? false : true;
    }

}
