<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 03.12.2015
 */

namespace classes\mailbox;

use classes\Component;

class IncomingMail extends Component
{

    public $id;
    public $date;
    public $subject;

    public $fromName;
    public $fromAddress;

    public $to = array();
    public $toString;
    public $cc = array();
    public $replyTo = array();

    public $html;
    public $plain;
    public $charset;
    public $attachments;

}
