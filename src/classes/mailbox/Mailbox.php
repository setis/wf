<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 03.12.2015
 */

namespace classes\mailbox;

use classes\Component;
use ErrorException;

class Mailbox extends Component
{

    public $mailbox;
    public $username;
    public $password;

    public $encoding = 'utf-8';

    private $_mailbox;

    public function init()
    {
        $mb = imap_open($this->mailbox, $this->username, $this->password);

        if ($mb === false)
            throw new ErrorException('Cannot open mailbox "' . $this->username . '@' . $this->mailbox . '"');
        $this->setMailbox($mb);

        return $this;
    }

    public function __destruct()
    {
        imap_close($this->getMailbox());
    }

    /**
     * @param bool|true $unseen
     * @return IncomingMail[]|bool
     */
    public function getMailList($unseen = true, $markAsSeen = true)
    {
        $imap = $this->getMailbox();
        if($unseen)
            $list = imap_search($imap, 'UNSEEN');
        else
            $list = imap_search($imap, 'ALL');

        if (empty($list)) {
            return false;
        }

        $messages = array();
        foreach ($list as $message_id) {
            $messages[] = $this->getMessage($message_id, $markAsSeen);
        }

        $messages = array_filter($messages);

        return $messages;
    }

    private function getMessage($id, $markAsSeen = true)
    {
        $message = null;
        // BODY
        $body = imap_fetchstructure($this->getMailbox(), $id);

        if (!$body->parts) {
            $message = $this->getMessagePart($id, $body, 0);  // pass 0 as part-number
        } else {
            foreach ($body->parts as $partno0 => $p)
                $message = $this->getMessagePart($id, $p, $partno0 + 1);
        }

        // HEADER
        $headers = imap_rfc822_parse_headers(imap_fetchheader($this->getMailbox(), $id, FT_UID));
        // add code here to get date, from, to, cc, subject...
        if($message instanceof IncomingMail) {

            $message->id = $id;
            $message->date = isset($headers->date) ? strtotime(preg_replace('/\(.*?\)/', '', $headers->date)) : time();
            $message->subject = isset($headers->subject) ? $this->decodeMimeStr($headers->subject, $this->encoding) : null;
            $message->fromName = isset($headers->from[0]->personal) ? $this->decodeMimeStr($headers->from[0]->personal, $this->encoding) : null;
            $message->fromAddress = strtolower($headers->from[0]->mailbox . '@' . $headers->from[0]->host);

            if(isset($headers->to)) {
                $toStrings = array();
                foreach($headers->to as $to) {
                    if(!empty($to->mailbox) && !empty($to->host)) {
                        $toEmail = strtolower($to->mailbox . '@' . $to->host);
                        $toName = isset($to->personal) ? $this->decodeMimeStr($to->personal, $this->encoding) : null;
                        $toStrings[] = $toName ? "$toName <$toEmail>" : $toEmail;
                        $message->to[$toEmail] = $toName;
                    }
                }
                $message->toString = implode(', ', $toStrings);
            }

            if(isset($headers->cc)) {
                foreach($headers->cc as $cc) {
                    $message->cc[strtolower($cc->mailbox . '@' . $cc->host)] = isset($cc->personal) ? $this->decodeMimeStr($cc->personal, $this->encoding) : null;
                }
            }

            if(isset($headers->reply_to)) {
                foreach($headers->reply_to as $replyTo) {
                    $message->replyTo[strtolower($replyTo->mailbox . '@' . $replyTo->host)] = isset($replyTo->personal) ? $this->decodeMimeStr($replyTo->personal, $this->encoding) : null;
                }
            }
        }

        if($markAsSeen) {
            imap_setflag_full($this->getMailbox(), (string)$id, '\Seen \Deleted');
        }

        return $message;
    }

    private function getMessagePart($mid, $p, $partno, IncomingMail &$message = null)
    {
        $mbox = $this->getMailbox();

        if (empty($message))
            $message = new IncomingMail();

        $options = FT_UID;
//        if(!$markAsSeen) {
//            $options |= FT_PEEK;
//        }

        $data = ($partno) ?
            imap_fetchbody($mbox, $mid, $partno, $options) :  // multipart
            imap_body($mbox, $mid, $options);  // simple

        if($p->encoding == 1) {
            $data = imap_utf8($data);
        } elseif($p->encoding == 2) {
            $data = imap_binary($data);
        } elseif ($p->encoding == 4) {
            $data = quoted_printable_decode($data);
        } elseif ($p->encoding == 3) {
            $data = base64_decode($data);
        }

        $params = array();
        if ($p->parameters)
            foreach ($p->parameters as $x)
                $params[strtolower($x->attribute)] = $x->value;
        if ($p->dparameters)
            foreach ($p->dparameters as $x)
                $params[strtolower($x->attribute)] = $x->value;


        if ($params['filename'] || $params['name']) {
            $filename = ($params['filename']) ? $params['filename'] : $params['name'];
            if (empty($message->attachments))
                $message->attachments = array();

            $message->attachments[$filename] = $data;  // this is a problem if two files have same name
        }

        // TEXT
        if ($p->type == 0 && $data) {
            $message->charset = $params['charset'];

            if (strtolower($p->subtype) == 'plain') {
                if (is_null($message->plain))
                    $message->plain = '';
                $message->plain .= trim($data) . "\n\n";
            } else {
                if (is_null($message->html))
                    $message->html = '';
                $message->html .= $data . "<br><br>";
            }

        } elseif ($p->type == 2 && $data) {
            if (is_null($message->plain))
                $message->plain = '';
            $message->plain .= $data . "\n\n";
        }

        if ($p->parts) {
            foreach ($p->parts as $partno0 => $p2) {
                $this->getMessagePart($mid, $p2, $partno . '.' . ($partno0 + 1), $message);  // 1.2, 1.2.1, etc.
            }
        }

        if($this->encoding != $message->charset) {
            if(!empty($message->plain))
                $message->plain = iconv( $message->charset, $this->encoding, $message->plain );
            if(!empty($message->html))
                $message->html = iconv( $message->charset, $this->encoding, $message->html );
        }

        return $message;
    }

    /**
     * @return mixed
     */
    public function getMailbox()
    {
        return $this->_mailbox;
    }

    /**
     * @param mixed $mailbox
     */
    public function setMailbox($mailbox)
    {
        $this->_mailbox = $mailbox;
    }

    protected function decodeMimeStr($string, $charset = 'utf-8') {
        $newString = '';
        $elements = imap_mime_header_decode($string);
        for($i = 0; $i < count($elements); $i++) {
            if($elements[$i]->charset == 'default') {
                $elements[$i]->charset = 'iso-8859-1';
            }
            $newString .= $this->convertStringEncoding($elements[$i]->text, $elements[$i]->charset, $charset);
        }
        return $newString;
    }

    public function isUrlEncoded($string) {
        $hasInvalidChars = preg_match( '#[^%a-zA-Z0-9\-_\.\+]#', $string );
        $hasEscapedChars = preg_match( '#%[a-zA-Z0-9]{2}#', $string );
        return !$hasInvalidChars && $hasEscapedChars;
    }

    protected function decodeRFC2231($string, $charset = 'utf-8') {
        if(preg_match("/^(.*?)'.*?'(.*?)$/", $string, $matches)) {
            $encoding = $matches[1];
            $data = $matches[2];
            if($this->isUrlEncoded($data)) {
                $string = $this->convertStringEncoding(urldecode($data), $encoding, $charset);
            }
        }
        return $string;
    }

    /**
     * Converts a string from one encoding to another.
     * @param string $string
     * @param string $fromEncoding
     * @param string $toEncoding
     * @return string Converted string if conversion was successful, or the original string if not
     */
    protected function convertStringEncoding($string, $fromEncoding, $toEncoding) {
        $convertedString = null;
        if($string && $fromEncoding != $toEncoding) {
            $convertedString = @iconv($fromEncoding, $toEncoding . '//IGNORE', $string);
            if(!$convertedString && extension_loaded('mbstring')) {
                $convertedString = @mb_convert_encoding($string, $toEncoding, $fromEncoding);
            }
        }
        return $convertedString ?: $string;
    }


}
