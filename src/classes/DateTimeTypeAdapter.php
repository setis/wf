<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes;

use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class DateTimeTypeAdapter extends DateTimeType
{
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $dateTime = parent::convertToPHPValue($value, $platform);

        if ( ! $dateTime) {
            return $dateTime;
        }

        return new DateTimeAdapter($dateTime->format('Y-m-d H:i:s'));
    }

    public function getName()
    {
        return 'datetimepk';
    }
}
