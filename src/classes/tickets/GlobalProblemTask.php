<?php
/**
 * mail@artemd.ru
 * 13.03.2016
 */

namespace classes\tickets;

use addr_interface_plugin;
use adm_ods_plugin;
use adm_sc_plugin;
use adm_statuses_plugin;
use classes\Task;
use gp_plugin;
use models\ListContr;
use models\TaskSource;
use models\TaskType;
use WF\Task\Events\GpCreatedEvent;

class GlobalProblemTask extends Task
{
    const UID = 'gp';

    protected $ticket = array();

    /**
     * @param $cnt_id
     * @param $type_id
     * @param $agr_id
     * @param $contr_number
     * @param $datefrom
     * @param $datetogss
     * @param $contr_user
     * @param $task_from
     * @param $gp_title
     * @param $gp_body
     * @param $cmm
     * @param int $btb
     * @param int $btc
     * @param null $sc_id
     * @return int
     * @throws \Exception
     */
    static function createTicket($cnt_id, $type_id, $agr_id, $contr_number, $datefrom, $datetogss, $contr_user, $task_from, $gp_title, $gp_body, $cmm, $btb = 0, $btc = 0, $sc_id = null)
    {
        // TODO pablo - use em and transacrion
        global $DB;
        $plugin_uid = self::UID;
        $comment = "";
        $comment .= "Контрагент: " . gp_plugin::getContrName($cnt_id, false) .
            " (" . gp_plugin::getTypeName($type_id, false) . ")\r\n";

        $em = \wf::$em;
        $em->beginTransaction();

        try {
            $task_id = Task::createTask($plugin_uid, "Заявка ГП (Линейная авария)", $comment, false, 0, 0);
            $task = $em->find(\models\Task::class, $task_id);
            $gp = (new \models\Gp())
                ->setTask($task)
                ->setOperatorKey($contr_number)
                ->setOperatorStart(new \DateTime($datefrom))
                ->setOperator2gss(new \DateTime($datetogss))
                ->setTitle($gp_title)
                ->setBody($gp_body)
                ->setAuthor($contr_user)
                ->setTaskSource($em->getReference(TaskSource::class, $task_from))
                ->setPartner($em->getReference(ListContr::class, $cnt_id))
                ->setTaskType($em->getReference(TaskType::class, $type_id))
                ->setAgrId($agr_id)
                ->setOpB2b($btb)
                ->setOpB2c($btc);
            if ($sc_id) {
                $gp->setServiceCenter($em->getReference(\models\ListSc::class, $sc_id));
            }
            $em->persist($gp);
            $em->flush();
            $em->commit();

        } catch (\Exception $ex) {
            $em->rollback();
            throw $ex;
        }

        $event = new GpCreatedEvent($task, $gp);
        \wf::$ed->dispatch(GpCreatedEvent::NAME, $event);

        if ($cmm) {
            $task = new Task($task_id);
            $task->addComment($cmm);
        }

        return $task_id;
    }

    function __construct($task_id)
    {
        global $DB;

        $sql = "SELECT * FROM `gp` WHERE `task_id`=" . $DB->F($task_id);
        if ($this->ticket = $DB->getRow($sql, true, false)) {
            parent::__construct($task_id);
        }
    }

    function getAddrList()
    {
        global $DB;
        $DB->query("SELECT gpa.id as gpa, gpa.*, la.* FROM `gp_addr` as gpa left join `list_addr` as la on gpa.dom_id=la.id WHERE gpa.task_id=" . $DB->F($this->getId()) . "ORDER BY gpa.id ASC;");
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            while ($result = $DB->fetch(true)) {
                $r[] = $result;
            }
            $r3 = [];
            foreach ($r as $item) {
                $r1["id"] = $item["gpa"];
                $r1["dom_id"] = $item["dom_id"];
                $r1["pod"] = $item["pod"];
                $r1["domofon"] = $item["domofon"];
                $r1["op_string"] = $item["full_address"];
                $r1["sc_id"] = $item["sc_id"];
                $r1["dom"] = $item["name"];
                $r1["ods_id"] = intval($item["ods_id"]);
                $r2 = addr_interface_plugin::decodeFullKLADRPartial($item["kladrcode"]);
                $r1["city"] = $r2["city"];
                $r1["street"] = $r2["street"];
                $sc = $item["sc_id"] ? adm_sc_plugin::getSC($item["sc_id"]) : false;
                $r1["sc"] = $sc ? $sc["title"] : "&mdash;";
                $ods = $r1["ods_id"] ? adm_ods_plugin::getODSById($r1["ods_id"]) : false;
                $r1["ods"] = $ods ? $ods["title"] : "&mdash;";
                $r1["is_not_recognized"] = $item["is_not_recognized"];
                $r1["order_id"] = count($r3);
                $r3[] = $r1;
            }
            $DB->free();
            return $r3;
        } else {
            $DB->free();
            return false;
        }
    }

    static function getAddrItem($id)
    {
        global $DB;
        if ($id) return false;
        $item = $DB->getRow("SELECT gpa.id as gpa, gpa.*, la.* FROM `gp_addr` as gpa left join `list_addr` as la on gpa.dom_id=la.id WHERE gpa.id=" . $DB->F($id) . ";", true);
        if ($DB->errno()) UIError($DB->error());
        if ($item) {
            $r1["id"] = $item["gpa"];
            $r1["dom_id"] = $item["dom_id"];
            $r1["pod"] = $item["pod"];
            $r1["domofon"] = $item["domofon"];
            $r1["op_string"] = $item["full_address"];
            $r1["sc_id"] = $item["sc_id"];
            $r1["dom"] = $item["name"];
            $r1["ods_id"] = intval($item["ods_id"]);
            $r2 = addr_interface_plugin::decodeFullKLADRPartial($item["kladrcode"]);
            $r1["city"] = $r2["city"];
            $r1["street"] = $r2["street"];
            $sc = $item["sc_id"] ? adm_sc_plugin::getSC($item["sc_id"]) : false;
            $r1["sc"] = $sc ? $sc["title"] : "&mdash;";
            $ods = $r1["ods_id"] ? adm_ods_plugin::getODSById($r1["ods_id"]) : false;
            $r1["ods"] = $ods ? $ods["title"] : "&mdash;";
            return $r1;
        } else {
            return false;
        }
    }


    function getContrId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['cnt_id']) : $this->ticket['cnt_id'];
    }

    function getContrName($escape = true)
    {
        return gp_plugin::getContrName($this->ticket['cnt_id'], $escape);
    }

    function getTypeName($escape = true)
    {
        return gp_plugin::getTypeName($this->ticket['type_id'], $escape);
    }

    function getType($escape = true)
    {
        return $this->ticket['type_id'];
    }

    function getB2b($escape = true)
    {
        return $this->ticket['op_b2b'];
    }

    function getB2c($escape = true)
    {
        return $this->ticket['op_b2c'];
    }

    function getAddInfo($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['body']) : $this->ticket['body'];
    }

    function setAddInfo($value, $commit = false)
    {
        $this->ticket['body'] = $value;
        return $commit ? $this->updateCommit() : true;

    }

    function getRejectReasonId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['reject_id']) : $this->ticket['reject_id'];
    }


    function getGssDate()
    {
        $slotVal = explode(" ", $this->ticket["operator_2gss"]);
        if ($slotVal[0])
            $slotDate = substr($slotVal[0], 8, 2) . "." . substr($slotVal[0], 5, 2) . "." . substr($slotVal[0], 0, 4);
        else $slotDate = false;
        return $slotDate;
    }

    function getOpDate()
    {
        $slotVal = explode(" ", $this->ticket["operator_start"]);

        if ($slotVal[0])
            $slotDate = substr($slotVal[0], 8, 2) . "." . substr($slotVal[0], 5, 2) . "." . substr($slotVal[0], 0, 4);
        else $slotDate = false;
        return $slotDate;
    }

    function getOpTime()
    {
        $slotVal = explode(" ", $this->ticket["operator_start"]);
        if ($slotVal[1])
            $slotTime = substr($slotVal[1], 0, 5);
        else $slotTime = false;
        return $slotTime;
    }

    function getPlanDate()
    {
        $slotVal = explode(" ", $this->ticket["close_time"]);

        if ($slotVal[0])
            $slotDate = substr($slotVal[0], 8, 2) . "." . substr($slotVal[0], 5, 2) . "." . substr($slotVal[0], 0, 4);
        else $slotDate = false;
        return $slotDate;
    }

    function getPlanTime()
    {
        $slotVal = explode(" ", $this->ticket["close_time"]);
        if ($slotVal[1])
            $slotTime = substr($slotVal[1], 0, 5);
        else $slotTime = false;
        return $slotTime;
    }


    function getGssTime()
    {
        $slotVal = explode(" ", $this->ticket["operator_2gss"]);
        if ($slotVal[1])
            $slotTime = substr($slotVal[1], 0, 5);
        else $slotTime = false;
        return $slotTime;
    }

    function getAgrId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['agr_id']) : $this->ticket['agr_id'];
    }

    function getSCId()
    {
        return $this->ticket['sc_id'];
    }

    function setSCId($value, $commit = false)
    {
        $value = (int) $value;
        if ($value < 1) {
            $value = null;
        }
        $this->ticket['sc_id'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function getTaskFrom()
    {
        return $this->ticket['task_from'];
    }

    function setTaskFrom($value, $commit = false)
    {
        $this->ticket['task_from'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function getTaskAuthor()
    {
        return $this->ticket['author'];
    }

    function setTaskAuthor($value, $commit = false)
    {
        $this->ticket['author'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function setB2c($value, $commit = false)
    {
        $this->ticket['op_b2c'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function setB2b($value, $commit = false)
    {
        $this->ticket['op_b2b'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function getTaskOpNum()
    {
        return $this->ticket['operator_key'];
    }

    function setTaskOpNum($value, $commit = false)
    {
        $this->ticket['operator_key'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function getTaskTitle()
    {
        return $this->ticket['title'];
    }

    function setTaskTitle($value, $commit = false)
    {
        $this->ticket['title'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function setOperatorDate($value, $commit = false)
    {
        $this->ticket['operator_start'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function setPlanDate($value, $commit = false)
    {
        $this->ticket['close_time'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function setGssDate($value, $commit = false)
    {
        $this->ticket['operator_2gss'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function getContrNumber($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['operator_key']) : $this->ticket['operator_key'];
    }

    function setContrNumber($value, $commit = false)
    {
        $this->ticket['operator_key'] = $value;
        return $commit ? $this->updateCommit() : true;
    }


    function getDocDate($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['doc_date']) : $this->ticket['doc_date'];
    }

    function setDocDate($value, $commit = false)
    {
        $this->ticket['doc_date'] = date("Y-m-d", strtotime($value));
        return $commit ? $this->updateCommit() : true;
    }

    function getDocID($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['doc_ticket']) : $this->ticket['doc_ticket'];
    }

    function setDocID($value, $commit = false)
    {
        $this->ticket['doc_ticket'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function getTMCDocDate($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tmc_date']) : $this->ticket['tmc_date'];
    }

    function setTMCDocDate($value, $commit = false)
    {
        $this->ticket['tmc_date'] = date("Y-m-d", strtotime($value));
        return $commit ? $this->updateCommit() : true;
    }

    function getTMCDocID($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tmc_ticket']) : $this->ticket['tmc_ticket'];
    }

    function setTMCDocID($value, $commit = false)
    {
        $this->ticket['tmc_ticket'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function getDoneDate()
    {
        global $DB;
        $doneStatus = adm_statuses_plugin::getStatusByTag("done", "gp");
        $donedate = $DB->getField("SELECT DATE_FORMAT(`datetime`, '%d.%m.%Y') FROM `task_comments` WHERE `status_id`=" . $DB->F($doneStatus["id"]) . " AND `task_id`=" . $DB->F($this->ticket["task_id"]) . ";");
        if ($DB->errno()) UIError($DB->error());
        if ($donedate && $donedate != "00.00.0000") {
            return $donedate;
        } else return false;
    }

    function setDoneDate($value)
    {
        global $DB;
        $doneStatus = adm_statuses_plugin::getStatusByTag("done", "gp");
        $sql = "UPDATE `task_comments` SET `datetime`=" . $DB->F(date("Y-m-d H:i:s", strtotime($value))) . " WHERE `task_id`=" . $DB->F($this->ticket["task_id"]) . " AND `status_id`=" . $DB->F($doneStatus["id"]) . ";";
        $DB->query($sql);
        if ($DB->errno()) return false; else return true;
    }

    function updateCommit()
    {
        global $DB;
        $DB->updateRow('gp', $this->ticket, 'task_id');
        if ($DB->errno()) UIError($DB->error());
        return true;

    }

    /**
     *
     * @return \Doctrine\ORM\EntityManagerInterface
     */
    private function getEm()
    {
        return \wf::$em;
    }

}
