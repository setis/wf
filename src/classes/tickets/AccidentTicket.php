<?php

/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 02.01.2016
 */

namespace classes\tickets;

use accidents_plugin;
use addr_interface_plugin;
use adm_areas_plugin;
use adm_statuses_plugin;
use adm_users_plugin;
use agreements_plugin;
use classes\Task;

use classes\HTML_Template_IT;
use classes\User;
use wf;


class AccidentTicket extends Ticket
{

    const UID = 'accidents';

    /** @var array */
    protected $ticket = [];

    /**
     * @param $cnt_id
     * @param $type_id
     * @param $dom_id
     * @param $pod
     * @param $etazh
     * @param $kv
     * @param $domofon
     * @param $clnt_type
     * @param $org_name
     * @param $fio
     * @param $phone1
     * @param $phone2
     * @param $ispoln_ids
     * @param $cmm
     * @param $addinfo
     * @param $agr_id
     * @param $swip
     * @param $swport
     * @param $swplace
     * @param $clntopagr
     * @param $clnttnum
     * @param int $sc_id
     * @param $task_from
     * @param $slotBegin
     * @param $slotEnd
     * @param bool|false $login
     * @param bool|false $password
     *
     * @return integer
     */
    static function createTicket($cnt_id, $type_id, $dom_id, $pod, $etazh, $kv, $domofon, $clnt_type, $org_name, $fio, $phone1, $phone2, $ispoln_ids, $cmm, $addinfo, $agr_id, $swip, $swport, $swplace, $clntopagr, $clnttnum, $sc_id = null, $task_from, $slotBegin, $slotEnd, $login = false, $password = false)
    {
        global $DB, $USER;
        $plugin_uid = self::UID;
        $comment = "";
        $comment .= "Контрагент: " . accidents_plugin::getContrName($cnt_id, false) .
            " (" . accidents_plugin::getTypeName($type_id, false) . ")\r\n";
        $comment .= "Адрес: $dom_id кв. $kv\r\n";
        if ($clnt_type == User::CLIENT_TYPE_PHYS) $comment .= "Физ.лицо: $fio\r\n";
        elseif ($clnt_type == User::CLIENT_TYPE_YUR) $comment .= "Юр.лицо: $org_name ($fio)\r\n";
        else $comment .= "$org_name, $fio\r\n";
        $task_id = Task::createTask($plugin_uid, "Заявка ТТ (Абонентская авария)", $comment, false, 0, $ispoln_ids);

        $sql = "INSERT INTO `tickets` (
                    `task_id`,
                    `cnt_id`,
                    `task_type`,
                    `dom_id`,
                    `pod`,
                    `etazh`,
                    `kv`,
                    `domofon`,
                    `sc_id`,
                    `clnt_type`,
                    `clnt_org_name`,
                    `clnt_fio`,
                    `clnt_tel1`,
                    `clnt_tel2`,
                    `addinfo`,
                    `agr_id`,
                    `swip`,
                    `swport`,
                    `swplace`,
                    `clntopagr`,
                    `clnttnum`,
                    `task_from`,
                    `slot_begin`,
                    `slot_end`,
                    `login`,
                    `password`
                ) VALUES (" .
            $DB->F($task_id) . ", " .
            $DB->F($cnt_id) . ", " .
            $DB->F($type_id) . ", " .
            $DB->F($dom_id ?: null, true) . ", " .
            $DB->F($pod) . ", " .
            $DB->F($etazh) . ", " .
            $DB->F($kv) . ", " .
            $DB->F($domofon, true) . ", " .
            $DB->F($sc_id, true) . ", " .
            $DB->F($clnt_type) . ", " .
            $DB->F($org_name, true) . ", " .
            $DB->F($fio, true) . ", " .
            $DB->F(preg_replace("/[^0-9]/", "", $phone1)) . ", " .
            $DB->F(preg_replace("/[^0-9]/", "", $phone2)) . ", " .
            $DB->F($addinfo) . ", " .
            $DB->F($agr_id) . ", " .
            $DB->F($swip) . ", " .
            $DB->F($swport) . ", " .
            $DB->F($swplace) . ", " .
            $DB->F($clntopagr) . ", " .
            $DB->F($clnttnum) . ", " .
            $DB->F($task_from) . ", " .
            $DB->F($slotBegin) . ", " .
            $DB->F($slotEnd) . ", " .
            $DB->F($login) . ", " .
            $DB->F($password) . ");";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error() . $sql);
        //die ($sql."<br />");
        if ($cmm) {
            $task = new Task($task_id);
            $task->addComment($cmm);
        }
        if ($USER && adm_users_plugin::isLKUser($USER->getId()) && $task_id && agreements_plugin::requireMailOnNewTicket($agr_id) > 0) {

            $t = new AccidentTicket($task_id);
            $tpl = new HTML_Template_IT(path2('templates'));
            $tpl->loadTemplatefile('task_techfindhimselm.tmpl.html');
            $tpl->setVariable('TASK_TITLE', $t->getTitle() . " <a href=\"" . rtrim(getcfg('http_base'), '/') . "/accidents/viewticket?task_id=" . $t->getId() . "\">" .
                $t->getId
                () . "</a>");
            $tpl->setVariable('SYS_NAME', getcfg('system.name'));
            $tpl->setVariable('AREA_NAME', $t->getArea());
            $addr = $t->getAddr($t->getDomId());
            $tpl->setVariable('CITY', $addr["city"]);
            $tpl->setVariable('STREET', $addr["street"]);
            $tpl->setVariable('HOUSE', $addr["dom"]);
            $tpl->setVariable('POD', $t->getPod() ? $t->getPod() : "-");
            $tpl->setVariable('FLOOR', $t->getEtazh() ? $t->getEtazh() : "-");
            $tpl->setVariable('FLAT', $t->getKv() ? $t->getKv() : "-");
            $tpl->setVariable('FIO', $t->getFio());
            $tpl->setVariable('CONTACTS', implode(" ", $t->getPhones()));
            $tpl->setVariable('DESCRIPTION', $t->getAddInfo());
            $to = false;
            $to = getcfg('mailtoonnewticket');
            $to = implode(', ', $to);
            $tpl->setVariable('SYS_HOST', getcfg('http_base'));
            if ($t->getCommentsCount()) {
                $tpl->setVariable('LAST_COMMENT', $t->getLastComment());
            }
            $email = wf::$container->get('wf.deprecated_mail');
            $email->setSubject("Личный кабинет контрагента. Новая заявка ТТ от \"" . accidents_plugin::getContrName($cnt_id, false) . "\". " . $task_id . ' - ' . getcfg('system.name'));
            $email->setTo($to);
            $email->setHtml($tpl->get());
            $email->send();
        }

        return $task_id;
    }

    public static function setFailedTickets()
    {
        global $DB;
        $failed_status_id = $DB->getField("SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("accidents") . " AND `tag` REGEXP " . $DB->F("failed") . ";");
        if (!$failed_status_id) {
            return false;
        }
        $ingfx_status_id = $DB->getField("SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("accidents") . " AND `tag` REGEXP " . $DB->F("grafik") . ";");
        if (!$ingfx_status_id) {
            return false;
        }
        $tList = $DB->getCell("SELECT `id` FROM `tasks` WHERE `status_id`=" . $DB->F($ingfx_status_id) . " AND `id` IN (SELECT `task_id` FROM `gfx` WHERE `c_date`<" . $DB->F(date("Y-m-d")) . ");");
        if ($DB->errno()) {
            return false;
        }
        foreach ($tList as $item) {
            $t = new AccidentTicket($item);
            $t->addComment("Статус: Не выполнена!", '', $failed_status_id);
        }

        return count($tList);
    }

    function setToReportTickets()
    {
        global $DB;
        $to_report_status_id = $DB->getField("SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("accidents") . " AND `tag` REGEXP " . $DB->F("report") . ";");
        if (!$to_report_status_id) {
            return false;
        }
        $complete_status_id = $DB->getField("SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("accidents") . " AND `tag` REGEXP " . $DB->F("done") . ";");
        if (!$complete_status_id) {
            return false;
        }
        $tList = $DB->getCell("SELECT `id` FROM `tasks` WHERE `status_id`=" . $DB->F($complete_status_id) . ";");
        if ($DB->errno()) {
            return false;
        }
        foreach ($tList as $item) {
            $t = new AccidentTicket($item);
            $t->addComment("Статус: Сдача отчета!", '', $to_report_status_id);
        }

        return count($tList);
    }


    function getContrId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['cnt_id']) : $this->ticket['cnt_id'];
    }

    function getContrName($escape = true)
    {
        return accidents_plugin::getContrName($this->ticket['cnt_id'], $escape);
    }

    function getTypeName($escape = true)
    {
        return accidents_plugin::getTypeName($this->ticket['task_type'], $escape);
    }

    function getTypeDuration($escape = true)
    {
        return accidents_plugin::getTypeDuration($this->ticket['task_type'], $escape);
    }


    function getTypeId($escape = true)
    {
        return accidents_plugin::getTypeName($this->ticket['task_type'], $escape);
    }

    function getType($escape = true)
    {
        return $this->ticket['task_type'];
    }

    function getArea($escape = true)
    {
        global $DB;
        $result = false;
        $laddr = $DB->getField("SELECT `area_id` FROM `list_addr` WHERE `id`=" . $DB->F($this->getDomId()) . ";");

        if ($laddr) {
            if (class_exists("adm_areas_plugin", true) && method_exists("adm_areas_plugin", "getArea")) {
                $area = new adm_areas_plugin();
                $result = $area->getArea($laddr);
            }
        }

        return $escape ? htmlspecialchars($result) : $result;
    }

    function getAddrRecord()
    {
        global $DB;
        $laddr = $DB->getRow("SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($this->getDomId()) . ";", true);

        return $laddr ? $laddr : false;

    }


    function getDomId()
    {
        return $this->ticket['dom_id'];
    }

    function getPod($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['pod']) : $this->ticket['pod'];
    }

    function setPod($value, $commit = false)
    {
        $this->ticket['pod'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getEtazh($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['etazh']) : $this->ticket['etazh'];
    }

    function getAddInfo($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['addinfo']) : $this->ticket['addinfo'];
    }

    function setAddInfo($value, $commit = false)
    {
        $this->ticket['addinfo'] = $value;

        return $commit ? $this->updateCommit() : true;

    }

    function setEtazh($value, $commit = false)
    {
        $this->ticket['etazh'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getKv($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['kv']) : $this->ticket['kv'];
    }

    function getRejectReasonId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['reject_id']) : $this->ticket['reject_id'];
    }

    function setKv($value, $commit = false)
    {
        $this->ticket['kv'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getDomofon($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['domofon']) : $this->ticket['domofon'];
    }

    function setDomofon($value, $commit = false)
    {
        $this->ticket['domofon'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getClientType()
    {
        return $this->ticket['clnt_type'];
    }

    function setClientType($value)
    {
        $this->ticket['clnt_type'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getFindateContr()
    {
        return $this->ticket['findatecontr'];
    }

    function getFindateTech()
    {
        return $this->ticket['findatetech'];
    }

    function getSlotDateBegin()
    {
        $slotVal = explode(" ", $this->ticket["slot_begin"]);

        if ($slotVal[0])
            $slotDate = substr($slotVal[0], 8, 2) . "." . substr($slotVal[0], 5, 2) . "." . substr($slotVal[0], 0, 4);
        else $slotDate = false;

        return $slotDate;
    }

    function getSlotDateEnd()
    {
        $slotVal = explode(" ", $this->ticket["slot_end"]);
        if ($slotVal[0])
            $slotDate = substr($slotVal[0], 8, 2) . "." . substr($slotVal[0], 5, 2) . "." . substr($slotVal[0], 0, 4);
        else $slotDate = false;

        return $slotDate;
    }

    function getSlotTimeBegin()
    {
        $slotVal = explode(" ", $this->ticket["slot_begin"]);
        if ($slotVal[1])
            $slotTime = substr($slotVal[1], 0, 5);
        else $slotTime = false;

        return $slotTime;
    }

    function getSlotTimeEnd()
    {
        $slotVal = explode(" ", $this->ticket["slot_end"]);
        if ($slotVal[1])
            $slotTime = substr($slotVal[1], 0, 5);
        else $slotTime = false;

        return $slotTime;
    }


    function getOrgName($escape = true)
    {
        if ($this->getClientType() == User::CLIENT_TYPE_PHYS) return false;

        return $escape ? htmlspecialchars($this->ticket['clnt_org_name']) : $this->ticket['clnt_org_name'];
    }

    function getAgrId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['agr_id']) : $this->ticket['agr_id'];
    }

    function setOrgName($value, $commit = false)
    {
        // TODO: проверять тип клиента
        $this->ticket['clnt_org_name'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getFio($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['clnt_fio']) : $this->ticket['clnt_fio'];
    }

    function setFio($value, $commit = false)
    {
        $this->ticket['clnt_fio'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getCableQuant($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['cablequant']) : $this->ticket['cablequant'];
    }

    function setCableQuant($value, $commit = false)
    {
        $this->ticket['cablequant'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    /* ушло в Ticket
        function getPhones($glue = false)
        {
            return $glue === false ?
                array($this->ticket['clnt_tel1'], $this->ticket['clnt_tel2']) :
                $this->ticket['clnt_tel1'] . $glue . $this->ticket['clnt_tel2'];
        }

        function setPhones($phone1 = null, $phone2 = null, $commit = false)
        {
            $this->ticket['clnt_tel1'] = preg_replace("/[^0-9]/", "", $phone1);
            $this->ticket['clnt_tel2'] = preg_replace("/[^0-9]/", "", $phone2);
            return $commit ? $this->updateCommit() : true;
        }

        function getSCId()
        {
            return $this->ticket['sc_id'];
        }

        function getTaskFrom()
        {
            return $this->ticket['task_from'];
        }

        function setSCId($value, $commit = false)
        {
            $this->ticket['sc_id'] = $value;
            return $commit ? $this->updateCommit() : true;
        }
    */
    function getOpLogin($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['login']) : $this->ticket['login'];
    }

    function getOpPassword($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['password']) : $this->ticket['password'];
    }

    function getSwIp($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['swip']) : $this->ticket['swip'];
    }

    function setSwIp($value, $commit = false)
    {
        $this->ticket['swip'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getSwPort($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['swport']) : $this->ticket['swport'];
    }

    function setSwPort($value, $commit = false)
    {
        $this->ticket['swport'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    /* ушло в Ticket
        function setTaskFrom($value, $commit = false)
        {
            $this->ticket['task_from'] = $value;
            return $commit ? $this->updateCommit() : true;
        }
    */
    function getSwPlace($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['swplace']) : $this->ticket['swplace'];
    }

    function setSwPlace($value, $commit = false)
    {
        $this->ticket['swplace'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function setSlotBegin($value, $commit = false)
    {
        $this->ticket['slot_begin'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function setSlotEnd($value, $commit = false)
    {
        $this->ticket['slot_end'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getClntOpAgr($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['clntopagr']) : $this->ticket['clntopagr'];
    }

    function setClntOpAgr($value, $commit = false)
    {
        $this->ticket['clntopagr'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getTMCActId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tmcactid']) : $this->ticket['tmcactid'];
    }

    function setTMCActId($value, $commit = false)
    {
        $this->ticket['tmcactid'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getAddNumber($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tickaddnumber']) : $this->ticket['tickaddnumber'];
    }

    function setAddNumber($value, $commit = false)
    {
        $this->ticket['tickaddnumber'] = $value;

        return $commit ? $this->updateCommit() : true;
    }


    function getTICKActId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tdataactid']) : $this->ticket['tdataactid'];
    }

    function setTICKActId($value, $commit = false)
    {
        $this->ticket['tdataactid'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getDoneDate()
    {
        global $DB;
        $doneStatus = adm_statuses_plugin::getStatusByTag("done", "accidents");
        $donedate = $DB->getField("SELECT DATE_FORMAT(`datetime`, '%d.%m.%Y') FROM `task_comments` WHERE `status_id`=" . $DB->F($doneStatus["id"]) . " AND `task_id`=" . $DB->F($this->ticket["task_id"]) . ";");
        if ($DB->errno()) UIError($DB->error());
        if ($donedate && $donedate != "00.00.0000") {
            return $donedate;
        } else return false;
    }

    function setDoneDate($value)
    {
        global $DB;
        $doneStatus = adm_statuses_plugin::getStatusByTag("done", "accidents");
        $sql = "UPDATE `task_comments` SET `datetime`=" . $DB->F(date("Y-m-d H:i:s", strtotime($value))) . " WHERE `task_id`=" . $DB->F($this->ticket["task_id"]) . " AND `status_id`=" . $DB->F($doneStatus["id"]) . ";";
        $DB->query($sql);
        if ($DB->errno()) return false; else return true;
    }

    function getDocDate($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['doc_date']) : $this->ticket['doc_date'];
    }

    function setDocDate($value, $commit = false)
    {
        $this->ticket['doc_date'] = date("Y-m-d", strtotime($value));

        return $commit ? $this->updateCommit() : true;
    }

    function getDocID($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['doc_ticket']) : $this->ticket['doc_ticket'];
    }

    function setDocID($value, $commit = false)
    {
        $this->ticket['doc_ticket'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getTMCDocDate($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tmc_date']) : $this->ticket['tmc_date'];
    }

    function setTMCDocDate($value, $commit = false)
    {
        $this->ticket['tmc_date'] = date("Y-m-d", strtotime($value));

        return $commit ? $this->updateCommit() : true;
    }

    function getTMCDocID($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tmc_ticket']) : $this->ticket['tmc_ticket'];
    }

    function setTMCDocID($value, $commit = false)
    {
        $this->ticket['tmc_ticket'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

}

