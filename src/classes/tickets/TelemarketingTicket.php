<?php

/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 02.01.2016
 */

namespace classes\tickets;

use addr_interface_plugin;
use adm_areas_plugin;
use adm_statuses_plugin;
use adm_ticket_rtsubtypes_plugin;
use adm_users_plugin;
use agreements_plugin;
use classes\HTML_Template_IT;
use classes\MultipartEmail;
use classes\Task;
use classes\User;
use tm_module_plugin;

class TelemarketingTicket extends Ticket
{

    const UID = 'tm_module';

    /**
     * Проверка наличия тикета в базе с искомым внешним id
     * @param string $external_id
     * @param integer|null $counterparty_id
     * @return bool
     */
    static public function hasExternalIdExists($external_id, $counterparty_id = null)
    {
        global $DB;

        if (!empty($counterparty_id))
            $conterparty_query_part = 'cnt_id = ' . $DB->F($counterparty_id);
        else
            $conterparty_query_part = '1=1';

        $query = "
        select
            count(task_id)
        from
            tickets
        where
            clnttnum = " . $DB->F($external_id) . "
            and " . $conterparty_query_part;
        $result = $DB->getField($query);

        return $result > 0;
    }

    /**
     * TelemarketingTicket::createTicket()
     * создает заявку на обслуживание/ремонт
     *
     * @param integer $cnt_id - ИД контрагента
     * @param integer $type_id - вид работ
     * @param integer $dom_id - ИД дома
     * @param string $pod - подъезд
     * @param string $etazh - этаж
     * @param string $kv - квартира
     * @param string $domofon - код домофона
     * @param integer $clnt_type - тип клиента (1=физб 2=юр)
     * @param string $org_name - название организации юр.клиента
     * @param string $fio - ФИО клиента
     * @param string $passp_sn - серия номер паспорта
     * @param string $passp_vidan - кем выдан паспорт
     * @param string $passp_date - дата выдачи паспорта
     * @param string $phone1 - теелефон 1
     * @param string $phone2 - телефон 2
     * @param mixed $ispoln_ids - массив или строка вида "1,2,3" ИД исполнителей
     * @param string $cmm - комментарий (первый комментарий к заадче)
     * @param string $addinfo - примечание
     * @param integer $agr_id - ИД догоовра с контрагентом
     * @param string $swip - ИП свитча
     * @param string $swport - порт свитча
     * @param string $swplace - место установки свитча
     * @param string $clntopagr - № договора клиента с контрагентом
     * @param string $clnttnum - № заявки клиента в БД контрагента
     * остальные параметры - см. БД
     * @return integer|false
     */
    static function createTicket($cnt_id, $type_id, $dom_id, $pod, $etazh, $kv, $domofon, $clnt_type, $org_name, $fio, $passp_sn, $passp_vidan, $passp_date, $phone1, $phone2, $ispoln_ids, $cmm, $addinfo, $agr_id, $swip, $swport, $swplace, $clntopagr, $clnttnum, $sc_id = null, $orient, $task_from, $extwtype, $istest = 0, $status_id = 0)
    {
        global $DB, $USER;
        if ($cnt_id == "36" && !empty($clnttnum)) {
            $sql = "SELECT `task_id` FROM `tickets` WHERE `task_type`='10' AND `clnttnum`=" . $DB->F($clnttnum) . " AND `cnt_id`=" . $DB->F($cnt_id) . " ORDER BY `task_id` DESC;";
            $m = $DB->getField($sql);
            if ($m) {
                UIError("Заявка с номером контрагента '" . $clnttnum . "' уже присутствует в системе - ID: '$m'. Для возврата к созданию заявки - используйте кнопку 'Назад' Вашего браузера. Для возврата к чистой форме - кнопку ниже...");
                //return $m;
            }
        }
        $plugin_uid = self::UID;
        $comment = "";
        $comment .= "Контрагент: " . tm_module_plugin::getContrName($cnt_id, false) .
            " (" . tm_module_plugin::getTypeName($type_id, false) . ")\r\n";
        $comment .= "Адрес: $dom_id кв. $kv\r\n";

        if ($clnt_type == User::CLIENT_TYPE_PHYS) $comment .= "Физ.лицо: $fio\r\n";
        elseif ($clnt_type == User::CLIENT_TYPE_YUR) $comment .= "Юр.лицо: $org_name ($fio)\r\n";
        else $comment .= "$org_name, $fio\r\n";

        $task_id = Task::createTask($plugin_uid, "Заявка Телемаркетинг", $comment, false, $status_id, $ispoln_ids);
        if (!$task_id) UIError("Не удалось создать задачу!");


        $passp_date = $passp_date ? rudate("Y-m-d", $passp_date) : '';

        if (!$extwtype) $extwtype = 0; else {
            if (preg_match("/RT_/", $extwtype))
                $extwtype = adm_ticket_rtsubtypes_plugin::getByTag($extwtype);
            //else
            //$extwtype = adm_ticket_rtsubtypes_plugin::getById($extwtype);

        }
        if (!$istest) $istest = 0;
        $sql = "INSERT INTO `tickets` (
                    `task_id`,
                    `cnt_id`,
                    `task_type`,
                    `dom_id`,
                    `pod`,
                    `etazh`,
                    `kv`,
                    `domofon`,
                    `sc_id`,
                    `clnt_type`,
                    `clnt_org_name`,
                    `clnt_fio`,
                    `clnt_passp_sn`,
                    `clnt_passport_vid`,
                    `clnt_passport_viddate`,
                    `clnt_tel1`,
                    `clnt_tel2`,
                    `addinfo`,
                    `agr_id`,
                    `swip`,
                    `swport`,
                    `swplace`,
                    `clntopagr`,
                    `clnttnum`,
                    `orient_price`, `task_from`, `exttype`, `istest`, checked
                ) VALUES (" .
            $DB->F($task_id) . ", " .
            $DB->F($cnt_id) . ", " .
            $DB->F($type_id) . ", " .
            $DB->F($dom_id ?: null, true) . ", " .
            $DB->F($pod) . ", " .
            $DB->F($etazh) . ", " .
            $DB->F($kv) . ", " .
            $DB->F($domofon, true) . ", " .
            $DB->F($sc_id, true) . ", " .
            $DB->F($clnt_type) . ", " .
            $DB->F($org_name, true) . ", " .
            $DB->F($fio, true) . ", " .
            $DB->F($passp_sn, true) . ", " .
            $DB->F($passp_vidan, true) . ", " .
            $DB->F($passp_date, true) . ", " .
            $DB->F(preg_replace("/[^0-9]/", "", $phone1)) . ", " . $DB->F(preg_replace("/[^0-9]/", "", $phone2)) . ", " . $DB->F($addinfo) . ", " . $DB->F($agr_id) . ", " . $DB->F($swip) . ", " . $DB->F($swport) . ", " . $DB->F($swplace) . ", " . $DB->F($clntopagr) . ", " . $DB->F($clnttnum) . ", " . $DB->F(intval($orient)) . ", " . $DB->F($task_from, true) . ", " . $DB->F($extwtype) . ", " . $DB->F($istest) . ", 0 )";
        #echo "<br>".$sql."<br/>\n";
        #exit;
        $DB->query($sql);
        #echo "after query<br/>\n";
        if ($DB->errno()) UIError($DB->error());
        if ($cmm) {
            $task = new Task($task_id);
            #echo "before add comment<br>\n";
            $task->addComment($cmm);
            #echo "after add comment<br>\n";
        }
        #echo "before last if<br>\n";
        #exit;
        if ($USER->getId() && adm_users_plugin::isLKUser($USER->getId()) && $task_id && agreements_plugin::requireMailOnNewTicket($agr_id) > 0) {

            $t = new TelemarketingTicket($task_id);
            $tpl = new HTML_Template_IT(path2('templates'));
            $tpl->loadTemplatefile('task_techfindhimselm.tmpl.html');
            $tpl->setVariable('TASK_TITLE', $t->getTitle() . " <a href=\"" . rtrim(getcfg('http_base'), '/') . "/tm_module/viewticket?task_id=" . $t->getId() . "\">"
                . $t->getId() . "</a>");
            $tpl->setVariable('SYS_NAME', getcfg('system.name'));
            $tpl->setVariable('AREA_NAME', $t->getArea());
            $addr = $t->getAddrN();
            $tpl->setVariable('CITY', $addr["city"]);
            $tpl->setVariable('STREET', $addr["street"]);
            $tpl->setVariable('HOUSE', $addr["dom"]);
            $tpl->setVariable('POD', $t->getPod() ? $t->getPod() : "-");
            $tpl->setVariable('FLOOR', $t->getEtazh() ? $t->getEtazh() : "-");
            $tpl->setVariable('FLAT', $t->getKv() ? $t->getKv() : "-");
            $tpl->setVariable('FIO', $t->getFio());
            $tpl->setVariable('CONTACTS', implode(" ", $t->getPhones()));
            $tpl->setVariable('DESCRIPTION', $t->getAddInfo());
            $to = false;
            $to = getcfg('mailtoonnewticket');
            $to = implode(', ', $to);
            $tpl->setVariable('SYS_HOST', getcfg('http_base'));
            if ($t->getCommentsCount()) {
                $tpl->setVariable('LAST_COMMENT', $t->getLastComment());
            }
            $email = new MultipartEmail();
            $email->setFrom("Личный кабинет контрагента " . trim(getcfg('system.name')) . " <" . getcfg('robot_email') . ">");
            $email->setSubject("Новая заявка ТМ от \"" . tm_module_plugin::getContrName($cnt_id, false) . "\". " . $task_id . ' - ' . getcfg('system.name'));

            $email->setTo($to);
            $email->setHtml($tpl->get());
            $email->send();
        }

        #echo "before return<br>\n";
        #exit;
        return $task_id;
    }

    public function getTaskId()
    {
        return $this->task_id;
    }

    function setFailedTickets()
    {
        global $DB;
        $failed_status_id = $DB->getField("SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("telemarketing") . " AND `tag` REGEXP " . $DB->F("failed") . ";");
        if (!$failed_status_id) {
            return false;
        }
        $ingfx_status_id = $DB->getField("SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("telemarketing") . " AND `tag` REGEXP " . $DB->F("grafik") . ";");
        if (!$ingfx_status_id) {
            return false;
        }
        $tList = $DB->getCell("SELECT `id` FROM `tasks` WHERE (`status_id`=" . $DB->F($ingfx_status_id) . ") AND `id` IN (SELECT `task_id` FROM `gfx` WHERE `c_date`<" . $DB->F(date("Y-m-d")) . ");");
        if ($DB->errno()) {
            return false;
        }
        foreach ($tList as $item) {
            $t = new TelemarketingTicket($item);
            $t->addComment("Статус: Не выполнена!", '', $failed_status_id);
        }

        return count($tList);


    }

    function getContrId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['cnt_id']) : $this->ticket['cnt_id'];
    }

    function getContrName($escape = true)
    {
        return tm_module_plugin::getContrName($this->ticket['cnt_id'], $escape);
    }

    function getTypeName($escape = true)
    {
        return tm_module_plugin::getTypeName($this->ticket['task_type'], $escape);
    }

    function getTypeDuration($escape = true)
    {
        return tm_module_plugin::getTypeDuration($this->ticket['task_type'], $escape);
    }

    function setSlotBegin($value, $commit = false)
    {
        $this->ticket['slot_begin'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function setSlotEnd($value, $commit = false)
    {
        $this->ticket['slot_end'] = $value;

        return $commit ? $this->updateCommit() : true;
    }


    function getArea($escape = true)
    {
        global $DB;
        $result = false;
        $laddr = $DB->getField("SELECT `area_id` FROM `list_addr` WHERE `id`=" . $DB->F($this->getDomId()) . ";");

        if ($laddr) {
            if (class_exists("adm_areas_plugin", true) && method_exists("adm_areas_plugin", "getArea")) {
                $area = new adm_areas_plugin();
                $result = $area->getArea($laddr);
            }
        }

        return $escape ? htmlspecialchars($result) : $result;
    }

    function isTest()
    {
        return $this->ticket['istest'];
    }

    function getAddrPart($escape = true)
    {
        global $DB;
        $result = false;
        $laddr = $DB->getRow("SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($this->getDomId()) . ";");
        if (count($laddr)) {
            //die($laddr['kladrcode]);
            $result = addr_interface_plugin::decodeFullKLADRPartial($laddr['kladrcode']);
            if (count($result)) {
                $result["house"] = $laddr['name'] ? $laddr['name'] : $laddr['althouse'];

            } else {
                $result["nokladr"] = true;
            }
        }

        return $result;

    }


    function getAddrN($escape = true)
    {
        global $DB;
        $result = false;
        $laddr = $DB->getRow("SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($this->getDomId()) . ";");
        if (count($laddr)) {
            if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "decodeFullKLADRPartial")) {
                $addr = new addr_interface_plugin();
                $addrString = $addr->decodeFullKLADRPartial($laddr['kladrcode']);
            }
            if ($addrString) {
                $addrString["dom"] = $laddr['name'];
                $addrString["house"] = $laddr['name'];

                return $addrString;
            } else {
                return false;
            }
        }
    }


    function getDomId()
    {
        return $this->ticket['dom_id'];
    }

    function getPod($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['pod']) : $this->ticket['pod'];
    }

    function setPod($value, $commit = false)
    {
        $this->ticket['pod'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getEtazh($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['etazh']) : $this->ticket['etazh'];
    }

    function setEtazh($value, $commit = false)
    {
        $this->ticket['etazh'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getAddInfo($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['addinfo']) : $this->ticket['addinfo'];
    }

    function getFindateContr()
    {
        return $this->ticket['findatecontr'];
    }

    function getFindateTech()
    {
        return $this->ticket['findatetech'];
    }

    function setAddInfo($value, $commit = false)
    {
        $this->ticket['addinfo'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getKv($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['kv']) : $this->ticket['kv'];
    }

    function getRejectReasonId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['reject_id']) : $this->ticket['reject_id'];
    }

    function setKv($value, $commit = false)
    {
        $this->ticket['kv'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getDomofon($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['domofon']) : $this->ticket['domofon'];
    }

    function setDomofon($value, $commit = false)
    {
        $this->ticket['domofon'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getClientType()
    {
        return $this->ticket['clnt_type'];
    }

    function getOrgName($escape = true)
    {
        if ($this->getClientType() == User::CLIENT_TYPE_PHYS) return false;

        return $escape ? htmlspecialchars($this->ticket['clnt_org_name']) : $this->ticket['clnt_org_name'];
    }

    function getAgrId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['agr_id']) : $this->ticket['agr_id'];
    }


    function setOrgName($value, $commit = false)
    {
        // TODO: проверять тип клиента
        $this->ticket['clnt_org_name'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getFio($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['clnt_fio']) : $this->ticket['clnt_fio'];
    }

    function setFio($value, $commit = false)
    {
        $this->ticket['clnt_fio'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getLineContact($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['lineowner']) : $this->ticket['lineowner'];
    }

    function setLineContact($value, $commit = false)
    {
        $this->ticket['lineowner'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getPasspSN($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['clnt_passp_sn']) : $this->ticket['clnt_passp_sn'];
    }

    function getPasspVidan($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['clnt_passport_vid']) : $this->ticket['clnt_passport_vid'];
    }

    function getPasspVidanDate($format = "d.m.Y")
    {
        return $this->ticket['clnt_passport_viddate'] ? rudate($format, $this->ticket['clnt_passport_viddate']) : false;
    }

    function setPassp($sn, $vidan, $vidan_date, $commit = false)
    {
        $this->ticket['clnt_passp_sn'] = $sn;
        $this->ticket['clnt_passport_vid'] = $vidan;
        $this->ticket['clnt_passport_viddate'] = $vidan_date ? rudate("Y-m-d", $vidan_date) : '';

        return $commit ? $this->updateCommit() : true;
    }

    /* ушло в родительский класс Ticket
        function setPhones($phone1 = null, $phone2 = null, $commit = false)
        {
            $this->ticket['clnt_tel1'] = preg_replace("/[^0-9]/", "", $phone1);
            $this->ticket['clnt_tel2'] = preg_replace("/[^0-9]/", "", $phone2);
            return $commit ? $this->updateCommit() : true;
        }

        function getSCId()
        {
            return $this->ticket['sc_id'];
        }

        function setSCId($value, $commit = false)
        {
            $this->ticket['sc_id'] = $value;
            return $commit ? $this->updateCommit() : true;
        }

        function getExtType()
        {
            return $this->ticket['exttype'];
        }

        function setExtType($value, $commit = false)
        {
            $this->ticket['exttype'] = $value;
            return $commit ? $this->updateCommit() : true;
        }
    */
    function getSwIp($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['swip']) : $this->ticket['swip'];
    }

    function setSwIp($value, $commit = false)
    {
        $this->ticket['swip'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getSwPort($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['swport']) : $this->ticket['swport'];
    }

    function setSwPort($value, $commit = false)
    {
        $this->ticket['swport'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getPrice($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['price']) : $this->ticket['price'];
    }

    function setPrice($value, $commit = true)
    {
        $this->ticket['price'] = $value;

        return $commit ? $this->updateCommit() : true;
    }


    function getSwPlace($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['swplace']) : $this->ticket['swplace'];
    }


    function setSwPlace($value, $commit = false)
    {
        $this->ticket['swplace'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getOrientPrice($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['orient_price']) : $this->ticket['orient_price'];
    }

    function setOrientPrice($value, $commit = false)
    {
        $this->ticket['orient_price'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    /* ушло в Ticket
        function setTaskFrom($value, $commit = false)
        {
            $this->ticket['task_from'] = $value;
            return $commit ? $this->updateCommit() : true;
        }

        function getTaskFrom($escape = true)
        {
            return $escape ? htmlspecialchars($this->ticket['task_from']) : $this->ticket['task_from'];
        }
    */

    function getClntOpAgr($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['clntopagr']) : $this->ticket['clntopagr'];
    }

    function setClntOpAgr($value, $commit = false)
    {
        $this->ticket['clntopagr'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getTicketDocNum($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tdocnum']) : $this->ticket['tdocnum'];
    }

    function setTicketDocNum($value, $commit = false)
    {
        $this->ticket['tdocnum'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getTicketMoneyDate($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['inmoneydate']) : $this->ticket['inmonetdate'];
    }

    function setTicketMoneyDate($value, $commit = false)
    {
        $this->ticket['inmoneydate'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getIsInMoney($escape = true)
    {
        return $this->ticket['inmoney'] ? true : false;;
    }

    function setIsInMoney($value = 1, $commit = false)
    {
        $this->ticket['inmoney'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getAddNumber($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tickaddnumber']) : $this->ticket['tickaddnumber'];
    }

    function setAddNumber($value, $commit = false)
    {
        $this->ticket['tickaddnumber'] = $value;

        return $commit ? $this->updateCommit() : true;
    }


    function getTICKActId($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tdataactid']) : $this->ticket['tdataactid'];
    }

    function setTICKActId($value, $commit = false)
    {
        $this->ticket['tdataactid'] = $value;

        return $commit ? $this->updateCommit() : true;
    }


    function getPodrId()
    {
        /*
        global $DB;
        $sql = "SELECT `contr_id` FROM `podr_tickets` WHERE `task_id`=".$DB->F($this->getId());
        return $DB->getField($sql, false);
        */
        return tm_module_plugin::getPodrId($this->getId());
    }

    function getDoneDate()
    {
        global $DB;
        $doneStatus = adm_statuses_plugin::getStatusByTag("done", "telemarketing");
        $donedate = $DB->getField("SELECT DATE_FORMAT(`datetime`, '%d.%m.%Y') FROM `task_comments` WHERE `status_id`=" . $DB->F($doneStatus["id"]) . " AND `task_id`=" . $DB->F($this->ticket["task_id"]) . ";");
        if ($DB->errno()) UIError($DB->error());
        if ($donedate && $donedate != "00.00.0000") {
            return $donedate;
        } else return false;
    }

    function setDoneDate($value)
    {
        global $DB;
        $doneStatus = adm_statuses_plugin::getStatusByTag("done", "telemarketing");
        $sql = "UPDATE `task_comments` SET `datetime`=" . $DB->F(date("Y-m-d H:i:s", strtotime($value))) . " WHERE `task_id`=" . $DB->F($this->ticket["task_id"]) . ";";
        $DB->query($sql);
        if ($DB->errno()) return false; else return true;
    }

    function getSlotDateBegin()
    {
        $slotVal = explode(" ", $this->ticket["slot_begin"]);

        if ($slotVal[0])
            $slotDate = substr($slotVal[0], 8, 2) . "." . substr($slotVal[0], 5, 2) . "." . substr($slotVal[0], 0, 4);
        else $slotDate = false;

        return $slotDate;
    }

    function getSlotDateEnd()
    {
        $slotVal = explode(" ", $this->ticket["slot_end"]);
        if ($slotVal[0])
            $slotDate = substr($slotVal[0], 8, 2) . "." . substr($slotVal[0], 5, 2) . "." . substr($slotVal[0], 0, 4);
        else $slotDate = false;

        return $slotDate;
    }

    function getSlotTimeBegin()
    {
        $slotVal = explode(" ", $this->ticket["slot_begin"]);
        if ($slotVal[1])
            $slotTime = substr($slotVal[1], 0, 5);
        else $slotTime = false;

        return $slotTime;
    }

    function getSlotTimeEnd()
    {
        $slotVal = explode(" ", $this->ticket["slot_end"]);
        if ($slotVal[1])
            $slotTime = substr($slotVal[1], 0, 5);
        else $slotTime = false;

        return $slotTime;
    }

    function getDocDate($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['doc_date']) : $this->ticket['doc_date'];
    }

    function setDocDate($value, $commit = false)
    {
        $this->ticket['doc_date'] = date("Y-m-d", strtotime($value));

        return $commit ? $this->updateCommit() : true;
    }

    function getDocID($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['doc_ticket']) : $this->ticket['doc_ticket'];
    }

    function setDocID($value, $commit = false)
    {
        $this->ticket['doc_ticket'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    function getTMCDocDate($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tmc_date']) : $this->ticket['tmc_date'];
    }

    function setTMCDocDate($value, $commit = false)
    {
        $this->ticket['tmc_date'] = date("Y-m-d", strtotime($value));

        return $commit ? $this->updateCommit() : true;
    }

    function getTMCDocID($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['tmc_ticket']) : $this->ticket['tmc_ticket'];
    }

    function setTMCDocID($value, $commit = false)
    {
        $this->ticket['tmc_ticket'] = $value;

        return $commit ? $this->updateCommit() : true;
    }

    public function isRtkMo()
    {

        global $DB;

        $query = "select * from `tickets` WHERE `rt_mo`=1 and `task_id`=" . $DB->F($this->task_id) . ";";
        $row = $DB->getRow($query, true);

        return !empty($row);
    }

}
