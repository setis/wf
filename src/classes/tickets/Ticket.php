<?php

namespace classes\tickets;

use classes\Task;

class Ticket extends Task
{
    protected $ticket = array();
    public $service_ticket_extras = array();

    function __construct($task_id)
    {
        global $DB;

        $sql = "SELECT * FROM `tickets` WHERE `task_id`=" . $DB->F($task_id);
        if ($this->ticket = $DB->getRow($sql, true, false)) {
            $sql = "select * from service_ticket_extras where task_id = ".intval($task_id);
            $this->service_ticket_extras = $DB->getRow($sql, true, false);
            parent::__construct($task_id);
        }
    }


    function updateServiceTicketExtras($task_id) {
        global $DB;
        $task_id = intval($task_id);
        if ($task_id > 0) {
            $sql = "replace into service_ticket_extras (";
            $keys = array();
            $values = array();
            foreach($this->service_ticket_extras as $field=>$value) {
                if (!is_numeric($field)) {
                     $keys[$field] = $field;
                     if('0' == $value) {
                         $value = null;
                     }
                     $values[$field] = $DB->F($value, true);

                     if (in_array($field, array('ctv_channel'))) {
                         if (!is_numeric($value)) {
                             $value = null;
                             $values[$field] = $DB->F($value);
                         }
                     }
                }
            }
            $sql .= join(",",$keys)." ) values (".join(",",$values)." )";

            $DB->query($sql);
            return true;
        }
        return false;
    }

    function updateCommit()
    {
        global $DB;
        if ($DB->updateRow('tickets', $this->ticket, 'task_id')) {
            return true;
        } else {
            UIError($DB->error());
        }
    }

    function getClntTNum($escape = true)
    {
        return $escape ? htmlspecialchars($this->ticket['clnttnum']) : $this->ticket['clnttnum'];
    }

    function setClntTNum($value, $commit = false)
    {
        $this->ticket['clnttnum'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    //// EF: Функцкия возвращает телефоны клиента по тикету, вне зависимости от настроек, скрывать или нет
    function getRealPhones($glue = false)
    {
        return $glue === false ?
            array($this->ticket['clnt_tel1'], $this->ticket['clnt_tel2']) :
            $this->ticket['clnt_tel1'] . $glue . $this->ticket['clnt_tel2'];
    }

    function getPhones($glue = false)
    {
        /// EF: Прячем телефонный номер, если нет доступа
        global $USER;
        $tel1 = $this->ticket['clnt_tel1'];
        $tel2 = $this->ticket['clnt_tel2'];
        if ($USER->checkAccess("hidetelnum")) {
                $tel1 = "00000001";
                $tel2 = "00000002";
        }
        return $glue === false ?
            array($tel1, $tel2) :
            $tel1 . $glue . $tel2;
    }

    function setPhones($phone1 = null, $phone2 = null, $commit = false)
    {
        $this->ticket['clnt_tel1'] = preg_replace("/[^0-9]/", "", $phone1);
        $this->ticket['clnt_tel2'] = preg_replace("/[^0-9]/", "", $phone2);
        return $commit ? $this->updateCommit() : true;
    }

    function getSCId()
    {
        return $this->ticket['sc_id'];
    }

    function setSCId($value, $commit = false)
    {
        $this->ticket['sc_id'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function getAreaId()
    {
        return $this->ticket['area_id'];
    }

    function setAreaId($value, $commit = false)
    {
        $this->ticket['area_id'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function getExtType()
    {
        return $this->ticket['exttype'];
    }

    function setExtType($value, $commit = false)
    {
        $this->ticket['exttype'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    function getTaskFrom()
    {
        return $this->ticket['task_from'];
    }

    function setTaskFrom($value, $commit = false)
    {
        $this->ticket['task_from'] = $value;
        return $commit ? $this->updateCommit() : true;
    }

    static public function getAddr($domId)
    {
        global $DB;

        if(null === $domId)
            return false;

        $result = false;
        $laddr = $DB->getRow("SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($domId), true);
        if (count($laddr)) {
            $result = \addr_interface_plugin::decodeFullKLADRPartial($laddr['kladrcode']);
            if (count($result)) {
                $result["house"] = $laddr['name'] ? $laddr['name'] : $laddr['althouse'];

            } else {
                $result["nokladr"] = true;
            }
        }

        return $result;
    }

    function getAddr1($domId)
    {
        global $DB;

        if(null === $domId)
            return false;

        $laddr = $DB->getRow("SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($domId) . ";", true);
        if (count($laddr)) {
            $addrString = \addr_interface_plugin::decodeFullKLADR($laddr['kladrcode']);
            $result = $addrString . " д. " . $laddr['name'];
            if ($laddr['althouse']) {
                $result .= " (" . $laddr['althouse'] . ") ";
            }
        } else {
            return "<font color=#FF0000>Адрес не в формате КЛАДР</font>";
        }

        return $result;
    }

}
