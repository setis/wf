<?php
namespace classes;

interface MultipartEmailInterface
{
    function setText($val);

    function getText();

    function setHtml($val);

    function getHtml();

    function setTo($val);

    function getTo();

    function setFrom($val);

    function getFrom();

    function setSubject($val);

    function getSubject();

    function send();
}