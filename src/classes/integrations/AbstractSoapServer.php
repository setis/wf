<?php
/**
 * mail@artemd.ru
 * 2016-02-17
 */

namespace classes\integrations;


abstract class AbstractSoapServer extends AbstractIntegrationServer
{
    abstract public function run();
}
