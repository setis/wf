<?php
/**
 * mail@artemd.ru
 * 2016-02-16
 */

namespace classes\soap;


use classes\Component;
use classes\integrations\AbstractIntegrationFactory;

class ConnectionsServerFactory extends AbstractIntegrationFactory
{
    public function makeIntegrationServer(array $params)
    {
        return Component::createObject('classes\soap\ConnectionsSoapServer', $params);
    }
}
