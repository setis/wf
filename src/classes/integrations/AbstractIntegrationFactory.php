<?php
/**
 * mail@artemd.ru
 * 2016-02-16
 */

namespace classes\integrations;


use classes\AbstractComponent;

abstract class AbstractIntegrationFactory extends AbstractComponent
{
    /**
     * @param array $params
     * @return AbstractIntegrationServer
     */
    abstract public function makeIntegrationServer(array $params);
}
