<?php
/**
 * mail@artemd.ru
 * 2016-02-16
 */

namespace classes\integrations;

use classes\AbstractComponent;

abstract class AbstractIntegrationServer extends AbstractComponent
{
    abstract public function createTicket();

    abstract public function cancelTicket();

    abstract public function moveTicket();

    abstract public function addCommentToTicket();
}
