<?php
/**
 * mail@artemd.ru
 * 2016-02-16
 */

namespace classes\integrations\soap;

use \wf;
use \SoapServer;
use classes\integrations\AbstractSoapServer;

class ServicesSoapServer extends AbstractSoapServer
{

    private $_soapServer;

    public $wsdl;

    private $_wsdlPath;

    public function init()
    {
        $this->_wsdlPath = realpath(getcfg('root_dir') . trim($this->wsdl, '/'));
    }

    public function run()
    {
        if ($_SERVER["REQUEST_URI"] == $this->wsdl && isset($_GET['wsdl'])) {
            $test =  trim($_SERVER['PHP_SELF'], '/');
            $test2 = getcfg('http_base');
            $endpoint = getcfg('http_base') . trim($_SERVER['PHP_SELF'], '/');
            header('Content-type: text/xml; charset=utf-8');

            $xml = file_get_contents($this->_wsdlPath);
            echo str_replace('{SERVICE_SCRIPT_URL}', $endpoint, $xml);
            exit;
        }


        $this->_soapServer = new SoapServer($this->_wsdlPath);
        $this->_soapServer->setClass(self::className());
        $this->_soapServer->handle();
    }

    public function createTicket()
    {
        // TODO: Implement createTicket() method.
    }

    public function cancelTicket()
    {
        // TODO: Implement cancelTicket() method.
    }

    public function addCommentToTicket()
    {
        // TODO: Implement addCommentToTicket() method.
    }

    public function moveTicket()
    {
        // TODO: Implement moveTicket() method.
    }

    public function getWsdl()
    {
        return $this->wsdl;
    }

}
