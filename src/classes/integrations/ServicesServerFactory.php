<?php
/**
 * mail@artemd.ru
 * 2016-02-16
 */

namespace classes\integrations;

use classes\Component;
use classes\integrations\soap\ServicesSoapServer;

class ServicesServerFactory extends AbstractIntegrationFactory
{
    /**
     * @param array $params
     * @return AbstractIntegrationServer
     */
    public function makeIntegrationServer(array $params)
    {

        if (isset($params['class'])) {
            return Component::createObject($params);
        } else {
            /*
             * creating default ServicesSoapServer
             */
            return Component::createObject(ServicesSoapServer::className(), $params);
        }
    }

}
