<?php
/**
 * mail@artemd.ru
 * 2016-02-16
 */

namespace classes\integrations;


use classes\Component;

class AccidentsServerFactory extends AbstractIntegrationFactory
{
    public function makeIntegrationServer(array $params)
    {
        return Component::createObject('classes\soap\AccidentsSoapServer', $params);
    }
}
