<?php
namespace classes;


class DateTimeAdapter extends \DateTime
{
    public function __toString()
    {
        return $this->format('Y-m-d H:i:s');
    }
}