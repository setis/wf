<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 18.11.2015
 */

namespace classes\transports;

use classes\Component;

class GenericSender extends Component implements  SenderInterface
{
    public $from;

    public function getFrom(){
        return $this->from;
    }
}
