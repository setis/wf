<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 18.11.2015
 */

namespace classes\transports;

use classes\Component;
use wf;

class SimplePostDataTransport extends Component implements TransportInterface
{

    public $service_url;
    public $service_test = false;

    public function send(SenderInterface $sender, ReceiverInterface $receiver, MessageInterface &$message)
    {
        return $this->sendRequest($receiver->getTo(), $message->getBody(), $result);
    }

    private function sendRequest($to, $data, &$result)
    {
        //set POST variables
        $fields = array(
        );

        $data = array_merge($fields, $data);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->service_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        //execute post
        $result = curl_exec($ch);
        $responseInfo = curl_getinfo($ch);

        wf::$logger->emergency('SimplePostData::sendRequest '.$this->service_url, [
            'response'=>json_encode($responseInfo, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT),
            'data'=>json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT),
            'result'=>json_encode($result, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT),
        ]);

        //close connection
        curl_close($ch);
        return true;
    }


}
