<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 18.11.2015
 */

namespace classes\transports;

use classes\Component;

class SimpleGetDataTransport extends Component implements TransportInterface
{
    public $service_url;
    public $service_test = 1;

    public function send(SenderInterface $sender, ReceiverInterface $receiver, MessageInterface &$message)
    {
        return $this->sendRequest($receiver->getTo(), $message->getBody(), $result);
    }

    private function sendRequest($to, $data, &$result)
    {
        //set GET variables
        $fields = array(
        );

        $data = array_merge($fields, $data);

        $fields = http_build_query($data);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->service_url.'?'.$fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        //execute post
        $result = curl_exec($ch);
        $responseInfo = curl_getinfo($ch);
        if ($this->service_test == true) {
            d($this->service_url.'?'.$fields);
            d($responseInfo);
            d($data);
            d($result);
        }
        //close connection
        curl_close($ch);
        return true;
    }


}
