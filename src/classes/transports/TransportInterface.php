<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 18.11.2015
 */

namespace classes\transports;


interface TransportInterface
{
    /**
     * @param SenderInterface $sender
     * @param ReceiverInterface $receiver
     * @param MessageInterface $message
     * @return MessageInterface
     */
    public function send(SenderInterface $sender, ReceiverInterface $receiver, MessageInterface &$message);
}
