<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 18.11.2015
 */

namespace classes\transports;

class GenericMessage extends MessageAbstract
{
    public function getBody(){
        return $this->_body;
    }

}
