<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 18.11.2015
 */

namespace classes\transports;

use classes\Component;
use Exception;
use SimpleXMLElement;

class SmsTransport extends Component implements TransportInterface
{

    public $service_login;
    public $service_password;
    public $service_url;
    public $service_test = 1;

    public function send(SenderInterface $sender, ReceiverInterface $receiver, MessageInterface &$message)
    {
        return $this->sendSMS($receiver->getTo(), $message->getBody(), $result);
    }

    /**
     * очистка телефонного номера от всего, кроме цифр, подходящих для отправки СМС
     * @param string $phone
     *
     * @return string phone || false
     */
    static public function cleanPhone($phone)
    {
        $phone_clean = '7' . ltrim(preg_replace('/[^0-9]/', '', $phone), '78');

        if (strlen($phone_clean) != 11) {
            $phone_clean = false;
        }

        return $phone_clean;
    }

    /**
     * получить из любой строки список телефонов для отправки СМС
     * @param string $str
     * @param bool $clean_only - возвращать только очищенные значения
     *
     * @return array(phone_clean, phone_clean, ...) || array(phone_clean => phone_str, phone_clean => phone_str, ...) || false
     */
    static public function smsPhonesFromString($str, $clean_only = false)
    {
        $phones = array();
        $str = preg_replace('/[^0-9\+;]/', '', $str);
        if (preg_match_all("/[7|8]([0-9]{10})|(9[0-9]{9})/", $str, $matches)) {
            $phones = array_filter(array_map(array('self', 'cleanPhone'), $matches[0]));
            return $phones;
        }
        return $phones;
    }

    /**
     * отправка СМС
     * @param mixed $to - string || array номеров получателей
     * @param string $text
     * @param mixed $result - string || array резултатов отправки в зависимости от количества получателей
     *
     * @return bool. если несколько получателей, то всегда true, для анализа нужно разбирать $result
     */
    private function sendSMS($to, $text, &$result)
    {
        $to_orig = $to;
        if (!is_array($to)) {
            $to = self::smsPhonesFromString($to, true);
        } else {
            foreach ($to as $i => $phone) {
                if ($phone = smsCleanPhone($phone)) {
                    $to[$i] = $phone;
                }
            }
            $to = array_filter($to);
        }

        if (!sizeof($to)) {
            echo ("No phones were found from ".$to_orig);
            return false;
        }

        $xml = new SimpleXMLElement("<message></message>");
        $service = $xml->addChild("service");
        $service->addAttribute('id', sizeof($to) > 1 ? 'bulk' : 'single');
        $service->addAttribute('login', $this->service_login);
        $service->addAttribute('password', $this->service_password);
//        $service->addAttribute('test', $this->service_test);
//        $service->addAttribute('extended', 1);
        foreach ($to as $t)
            $xml->addChild("to", $t);

        $xml->addChild("body", $text);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->service_url);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml->asXML());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-type: text/xml"
        ));

        $data = curl_exec($ch);

        if ($data === false) {
            echo('cURL error: ' . curl_error($ch));
            return false;
        } else {
            try {
                $status = new SimpleXMLElement($data);

                if (sizeof($status->state) > 1) {
                    $result = array();
                    $i = 0;
                    foreach ($status->state as $state) {
                        if ((string)$state != 'Accepted') {
                            $errcode = (int)$state['errcode'];
                            $error = (string)$state['error'];
                            echo ("Send SMS $i error $errcode: $error");
                            $result[$i] = 0;
                        } else {
                            $result[$i] = (string)$status->id[$i];
                        }
                        $i++;
                    }
                } else {
                    if ((string)$status->state != 'Accepted') {
                        $errcode = (int)$status->state['errcode'];
                        $error = (string)$status->state['error'];
                        echo ("Send SMS error $errcode: $error");
                        return false;
                    } else {
                        $result = (string)$status['id'];
                    }
                }
            } catch (Exception $e) {
                echo ('Failed to get or parse result XML: ' . $e->getMessage());
                return false;
            }
        }
        curl_close($ch);

        return true;
    }


}
