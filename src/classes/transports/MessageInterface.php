<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 18.11.2015
 */

namespace classes\transports;


interface MessageInterface
{

    public function setSubject($subject);

    public function getSubject();

    public function setBody($body);

    public function getBody();

    public function getSendStatus();

    public function getSendErrors();

}
