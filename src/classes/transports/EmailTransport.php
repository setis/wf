<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 18.11.2015
 */

namespace classes\transports;

use classes\Component;

class EmailTransport extends Component implements TransportInterface
{
    /**
     * @param SenderInterface $sender
     * @param ReceiverInterface $receiver
     * @param MessageAbstract $message
     * @return bool
     */
    public function send(SenderInterface $sender, ReceiverInterface $receiver, MessageInterface &$message) {
        $message->setSendError('Transport is not implement yet...');
        return false;
    }
}
