<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 18.11.2015
 */

namespace classes\transports;

use classes\Component;

class GenericReceiver extends Component implements ReceiverInterface
{
    public $to;

    public function getTo(){
        return $this->to;
    }
}
