<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 18.11.2015
 */

namespace classes\transports;

use classes\Component;

abstract class MessageAbstract extends Component implements MessageInterface
{

    const SEND_OK = 0;
    const SEND_UNKNOWN_ERROR = 100;

    protected $_body;
    private $_status;
    private $_errors;

    public function setBody($body)
    {
        $this->_body = $body;
    }

    abstract public function getBody();

    public function getSubject()
    {
        // TODO: Implement getSubject() method.
    }

    public function setSubject($subject)
    {
        // TODO: Implement setSubject() method.
    }

    public function getSendStatus(){
        return $this->_status;
    }

    public function setSendStatus($status){
        return $this->_status = $status;
    }

    public function getSendErrors() {
        return $this->_errors;
    }

    public function setSendError($error){
        $this->_errors[] = $error;
    }

}
