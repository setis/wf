<?php
namespace classes;

class Component extends AbstractComponent
{
    /**
     * Simple builder method
     *
     * @param $className
     * @param array $params
     * @return object
     */
    static public function createObject($className, $params = array())
    {
        if(is_array($className) && isset($className['class'])) {
            $params = $className;
            unset($params['class']);
            $className = $className['class'];
        }

        $reflection = new \ReflectionClass($className);

        if ($reflection->isSubclassOf(AbstractComponent::className()))
            return $reflection->newInstance($params);

        if (!empty($params))
            return new $className($params);
        else
            return new $className;
    }

    public function init(){

    }

}

