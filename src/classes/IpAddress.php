<?php
/**
 * mail@artemd.ru
 * 10.02.2016
 */

namespace classes;


class IpAddress
{
    static public function getIp()
    {
        return isset($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER['REMOTE_ADDR'];
    }
}
