<?php
/**
 * mail@artemd.ru
 * 08.03.2016
 */

namespace classes\themes;


abstract class AbstractWidgetFactory
{
    private $baseNameSpace;

    private $theme;

    /**
     * @param string $className
     * @param array $params
     * @return AbstractWidget
     */
    abstract function widget($className, $params = null);

    /**
     * @return mixed
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param mixed $theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    /**
     * @return string
     */
    public function getBaseNameSpace()
    {
        return $this->baseNameSpace;
    }

    /**
     * @param string $baseNameSpace
     */
    public function setBaseNameSpace($baseNameSpace)
    {
        $this->baseNameSpace = $baseNameSpace;
    }

}
