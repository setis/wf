<?php
/**
 * mail@artemd.ru
 * 08.03.2016
 */

namespace classes\themes;


abstract class AbstractThemeDirector
{
    abstract public function __construct(AbstractWidgetFactoryBuilder $builder/*, ....other builders */);

    abstract public function buildWidgetFactory();

    /**
     * @return AbstractWidgetFactory
     */
    abstract public function getWidgetFactory();
}
