<?php
/**
 * mail@artemd.ru
 * 08.03.2016
 */

namespace classes\themes;

abstract class AbstractWidgetFactoryBuilder
{
    protected $widgetFactory;

    private $theme;
    private $ns;

    public function __construct(AbstractWidgetFactory $factory)
    {
        $this->widgetFactory = $factory;
    }

    public function getWidgetFactory()
    {
        $this->widgetFactory->setBaseNameSpace($this->ns);
        $this->widgetFactory->setTheme($this->theme);

        return $this->widgetFactory;
    }

    /**
     * @param mixed $theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    /**
     * @param mixed $ns
     */
    public function setNameSpace($ns)
    {
        $this->ns = $ns;
    }
}
