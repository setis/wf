<?php
/**
 * mail@artemd.ru
 * 08.03.2016
 */

namespace classes\themes;

class ThemeDirector extends AbstractThemeDirector
{

    /** @var  AbstractWidgetFactoryBuilder */
    private $widgetFactoryBuilder;

    public function __construct(AbstractWidgetFactoryBuilder $builder)
    {
        $this->widgetFactoryBuilder = $builder;
    }

    public function buildWidgetFactory()
    {
    }

    public function getWidgetFactory()
    {
        return $this->widgetFactoryBuilder->getWidgetFactory();
    }
}
