<?php
/**
 * mail@artemd.ru
 * 28.02.2016
 */

namespace classes\themes;

use classes\AbstractComponent;
use wf;

abstract class AbstractWidget extends AbstractComponent
{
    private $isInitiated = false;

    public function run()
    {
        if (!$this->isInitiated) {
            $this->lazyInit();
        }

        return $this->doRun($param);
    }

    protected function doRun()
    {

    }

    protected function lazyInit($param)
    {

    }

    public function init()
    {
    }

    public function render($view, $params = array())
    {
        $reflection = new \ReflectionClass(get_called_class());
        $view = realpath(dirname($reflection->getFileName()) . DIRECTORY_SEPARATOR . $view . '.php');
        $view = str_replace(realpath(getcfg('root_dir')), '', $view);
        return wf::$view->renderPartial($view, $params);
    }

    public function __toString()
    {
        return $this->run();
    }
}
