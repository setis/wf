<?php
/**
 * mail@artemd.ru
 * 08.03.2016
 */

namespace classes\themes;


class TwozeroWidgetFactoryBuilder extends AbstractWidgetFactoryBuilder
{
    public function __construct()
    {
        $this->widgetFactory = new TwozeroWidgetFactory();

        $this->setNameSpace('classes\widgets');
        $this->setTheme('twozero');
    }
}
