<?php
/**
 * mail@artemd.ru
 * 08.03.2016
 */

namespace classes\themes;

use classes\Component;
use classes\exceptions\ApplicationException;

class TwozeroWidgetFactory extends AbstractWidgetFactory
{

    /**
     * @param string $className
     * @param null $params
     * @throws ApplicationException
     * @return AbstractWidget
     */
    public function widget($className, $params = array())
    {
        $class = $this->getBaseNameSpace().'\\'.$this->getTheme().'\\'.$className;
        if(class_exists($class)) {
            $reflection = new \ReflectionClass($class);
            if( $reflection->isSubclassOf( AbstractWidget::className() ) ) {
                $widget = Component::createObject($class, $params);
                return $widget;
            }
        }

        throw new ApplicationException('Not existing widget! '. $className);
    }
}
