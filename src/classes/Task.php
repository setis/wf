<?php

namespace classes;

use adm_statuses_plugin;
use adm_ticket_tttypes_plugin;
use adm_users_plugin;
use classes\comments\CommentsProcessor;
use classes\comments\HideExecutorDecorator;
use classes\comments\TextLink2AbsoluteDecorator;
use classes\comments\TextObject;
use classes\comments\TextPhonesRemoverDecorator;
use classes\comments\TextQuoteDecorator;
use classes\comments\TextUrl2AnchorDecorator;
use classes\tickets\ServiceTicket;
use DateTime;
use ErrorException;
use models\Task as OrmTask;
use models\TaskComment;
use models\TaskFile;
use models\TaskStatus;
use models\User as OrmUser;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use wf;
use WF\Task\Events\TaskExecutorsChangedEvent;

class Task extends \classes\Component
{

    /**
     * @Type("integer")
     */
    protected $task_id = 0;
    /**
     * @Type("array")
     */
    protected $task = array();
    /**
     * @Type("array")
     */
    protected $status = array();
    /**
     *
     * @var \WF\Task\TaskManager
     */
    protected $taskManager;
    /**
     *
     * @var OrmTask
     */
    private $taskEntity;

    function __construct($task_id)
    {
        parent::__construct();

        $this->taskManager = wf::$container->get('wf.task.task_manager');
        if ($task_id)
            $this->taskEntity = wf::$em->find(OrmTask::class, $task_id);
        $this->task_id = intval($task_id);
        $this->loadTask();
    }

    protected function loadTask()
    {
        global $DB;

        $this->task = $DB->getRow("SELECT * FROM `tasks` WHERE `id`=" . $DB->F($this->task_id), true);
        if (!$this->task) {
            trigger_error("Task (id:$this->task_id) not found!", E_USER_WARNING);
        }
        $this->status = $DB->getRow("SELECT * FROM `task_status` WHERE `id`=" . $DB->F($this->task['status_id']), true);
    }

    /**
     * Task::createTask()
     * создает новую задачу
     *
     * @param string $plugin_uid - идентификатор плагина
     * @param string $title
     * @param string $comment
     * @param bool $private - признак приавтности (доступна только связанным лицам или нет)
     * @param integer $status_id - начальный статус (0 = автоматически)
     * @param mixed $poruch_user_ids - массив или строка вида "1,2,3" ид исполнителей
     * @param mixed $users - массив или строка вида "1,2,3" ид связанных лиц
     * @return task_id || false
     */
    static function createTask($plugin_uid, $title = null, $comment, $private = false, $status_id = null, $poruch_user_ids = 0, $users = 0)
    {
        global $USER;
#echo "<br>".$plugin_uid."<br>\n";
        if (!$plugin_uid) {
            trigger_error("Plugin UID is not specified!", E_USER_WARNING);
            return false;
        }
#echo "<br>".$title."::".$comment."::".$status_id."<br>\n";
        if (!$title && !$comment) {
            trigger_error("No title and no comment specified!", E_USER_WARNING);
            return false;
        }
        if (!$status_id && !($status_id = self::getDefaultStatusId($plugin_uid))) {
            trigger_error("Can't find default status for plugin '$plugin_uid'!", E_USER_WARNING);
            return false;
        }

        $user_id = $USER && $USER->getId() ? $USER->getId() : null;

        wf::$em->beginTransaction();

        try {
            $task = new \models\Task();
            $task->setPluginUid($plugin_uid)
                ->setAuthor( $user_id ? wf::$em->getReference(OrmUser::class, $user_id) : null )
                ->setDateReg(new DateTime())
                ->setStatus($status_id ? wf::$em->find(TaskStatus::class, $status_id) : null)
                ->setTaskTitle(trim($title))
                ->setTaskComment(trim($comment))
                ->setIsPrivate($private)
                ->setLastStatusDate(new DateTime());

            wf::$em->persist($task);
            wf::$em->flush();

            if (!is_array($poruch_user_ids))
                $poruch_user_ids = (array)explode(',', $poruch_user_ids);

            if (!is_array($users))
                $users = (array)explode(',', $users);

            if ($USER && $USER->getId()) {
                $users[] = $USER->getId();
            }

            $legacyTask = new Task($task->getId());
            $legacyTask->setExecutors($poruch_user_ids);
            $legacyTask->setBoundedUsers($users);
            wf::$em->commit();
        } catch (\Exception $e) {
            wf::$em->rollback();
            throw $e;
        }


        return $task->getId();
    }

    /**
     * Task::getDefaultStatusId()
     * получить ИД статуса по-умолчанию для плагина
     *
     * @param string $plugin_uid - идентификатор плагина
     * @return integer или пусто
     */
    static function getDefaultStatusId($plugin_uid)
    {
        global $DB;
        return $DB->getField("SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F($plugin_uid) . " ORDER BY `is_default` DESC, `is_active` DESC LIMIT 1");
    }

    /**
     * Sets executors
     * @param array $executorIds
     * @return bool|string
     * @throws ErrorException
     */
    public function setExecutors($executorIds)
    {
        $newExecutors = array_filter($executorIds);
        $oldExecutors = array_filter(array_keys($this->getIspoln()));
        /** @var array $addExecutors Список новых отвественных по задаче */
        $addExecutors = array_diff($newExecutors, $oldExecutors);
        /** @var array $deleteExecutors Список более не отвественных по задаче */
        $deleteExecutors = array_diff($oldExecutors, $newExecutors);

        if ($this->changeBoundedUsers($this->getId(), $addExecutors, $deleteExecutors, [], [])) {
            $em = wf::$em;
            $task = $em->find(OrmTask::class, $this->task_id);
            $oldUsr = $em->getRepository(OrmUser::class)->findByIds($oldExecutors);
            $newUsr = $em->getRepository(OrmUser::class)->findByIds($newExecutors);
            $author = wf::$user instanceOf User ? $em->getReference(OrmUser::class, wf::$user->getId()) : null;
            $event = new TaskExecutorsChangedEvent($task, $oldUsr, $newUsr, $author);
            wf::$ed->dispatch(TaskExecutorsChangedEvent::NAME, $event);
            return $this->getOperationComment_ChangeExecutors($addExecutors, $deleteExecutors, [], []);
        }

        return false;
    }

    /**
     * Task::getIspoln()
     * получить исполителей задачи
     *
     * @param string $glue - если указан, то возвращается строка телефонов соединенная $glue
     * @return array || string
     *
     * @see Task::getTaskIspoln()
     */
    function getIspoln($glue = false)
    {
        return $this->getTaskIspoln($this->getId(), $glue);
    }

    /**
     * Task::getTaskIspoln()
     * получить исполителей задачи
     *
     * @param integet $task_id
     * @param string $glue - если указан, то возвращается строка с ФИО соединенная $glue
     * @return array(user_id=>user_fio, ...) || string
     */
    static function getTaskIspoln($task_id, $glue = false)
    {
        global $DB;
        $users = $DB->getCell2("SELECT
            us.id,
            us.fio
        FROM
            `task_users` AS tu
            JOIN `users` AS us ON tu.user_id=us.id
        WHERE
            tu.task_id=" . $DB->F($task_id) . " AND
            tu.ispoln > 0
        GROUP BY
            us.id
        ORDER BY
            us.fio");
        return $glue ? implode($glue, $users) : $users;
    }

    function getId()
    {
        return $this->task['id'];
    }

    /**
     * Do DB queries for change bounded users
     * @param $taskId
     * @param array $newExecutors
     * @param array $deletedExecutors
     * @param array $newBoundedUsers
     * @param array $deletedBoundedUsers
     * @return bool
     * @throws ErrorException
     */
    private function changeBoundedUsers($taskId, $newExecutors = array(), $deletedExecutors = array(), $newBoundedUsers = array(), $deletedBoundedUsers = array())
    {
        global $DB;

        if (!empty($deletedBoundedUsers)) {
            foreach ($deletedBoundedUsers as $userId) {
                $sql = 'DELETE FROM task_users WHERE task_id = ' . $taskId . ' AND user_id=' . $userId . ' AND ispoln = 0;';
                $DB->query($sql);
            }
        }

        if (!empty($deletedExecutors)) {
            foreach ($deletedExecutors as $userId) {
                $sql = 'SELECT * FROM task_users WHERE task_id = ' . $taskId . ' AND user_id= ' . $userId . ' AND ispoln = 0;';
                $result = $DB->getRow($sql, true);
                if (!empty($result)) {
                    $sql = 'DELETE FROM task_users WHERE task_id = ' . $taskId . ' AND user_id= ' . $userId . ' AND ispoln = 1;';
                    $DB->query($sql);
                    if ($DB->errno()) {
                        throw new ErrorException($DB->error());
                    }
                } else {
                    $sql = 'UPDATE task_users SET ispoln = 0 WHERE task_id = ' . $taskId . ' AND user_id=' . $userId . ';';
                    $DB->query($sql);
                    if ($DB->errno()) {
                        throw new ErrorException($DB->error());
                    }
                }
            }
        }

        if (!empty($newExecutors)) {
            foreach ($newExecutors as $userId) {
                $sql = 'SELECT * FROM task_users WHERE task_id = ' . $taskId . ' AND user_id= ' . $userId . ' AND ispoln = 0;';
                $result = $DB->getRow($sql, true);
                if (!empty($result)) {
                    $sql = 'UPDATE task_users SET ispoln = 1 WHERE task_id = ' . $taskId . ' AND user_id=' . $userId . ';';
                    $DB->query($sql);
                    if ($DB->errno()) {
                        throw new ErrorException($DB->error());
                    }
                } else {
                    $sql = 'INSERT INTO task_users SET task_id = ' . $taskId . ', user_id= ' . $userId . ', ispoln = 1;';
                    $DB->query($sql);
                    if ($DB->errno()) {
                        throw new ErrorException($DB->error());
                    }
                }
            }
        }

        if (!empty($newBoundedUsers)) {
            foreach ($newBoundedUsers as $userId) {
                $sql = 'SELECT * FROM task_users WHERE task_id = ' . $taskId . ' AND user_id= ' . $userId . ' AND ispoln = 0;';
                $result = $DB->getRow($sql, true);
                if (empty($result)) {
                    $sql = 'INSERT INTO task_users SET task_id = ' . $taskId . ', user_id= ' . $userId . ', ispoln = 0;';
                    $DB->query($sql);
                    if ($DB->errno()) {
                        throw new ErrorException($DB->error());
                    }
                }
            }
        }

        return true;
    }

    /**
     * Return memo text with list which users are added, deleted etc
     * @param array $newExecutors
     * @param array $deletedExecutors
     * @param array $newBoundedUsers
     * @param array $deletedBoundedUsers
     * @return string
     */
    private function getOperationComment_ChangeExecutors($newExecutors = array(), $deletedExecutors = array(), $newBoundedUsers = array(), $deletedBoundedUsers = array())
    {
        global $DB;
        $comment = '';
        $allUsers = array_merge($newExecutors, $deletedExecutors, $newBoundedUsers, $deletedBoundedUsers);

        if (empty($allUsers))
            return $comment;

        $sql = "SELECT `id`, `fio` FROM `users` WHERE id IN (" . implode(", ", $allUsers) . ");";
        $DB->query($sql);

        $newExecutorsList = [];
        $deletedExecutorsList = [];
        $newBoundedUsersList = [];
        $deletedBoundedUsersList = [];

        while (list($id, $fio) = $DB->fetch()) {
            if (in_array($id, $newExecutors)) {
                $newExecutorsList[] = $fio;
            }
            if (in_array($id, $deletedExecutors)) {
                $deletedExecutorsList[] = $fio;
            }
            if (in_array($id, $newBoundedUsers)) {
                $newBoundedUsersList[] = $fio;
            }
            if (in_array($id, $deletedBoundedUsers)) {
                $deletedBoundedUsersList[] = $fio;
            }
        }

        if (!empty($newExecutorsList)) {
            $comment .= 'Добавлены исполнители: ' . implode(', ', $newExecutorsList) . "\n";
        }
        if (!empty($deletedExecutorsList)) {
            $comment .= 'Удалены исполнители: ' . implode(', ', $deletedExecutorsList) . "\n";
        }
        if (!empty($newBoundedUsersList)) {
            $comment .= 'Добавлены связанные лица: ' . implode(', ', $newBoundedUsersList) . "\n";
        }
        if (!empty($deletedBoundedUsersList)) {
            $comment .= 'Удалены связанные лица: ' . implode(', ', $deletedBoundedUsersList) . "\n";
        }

        return $comment;
    }

    /**
     * Sets bounded users
     * @param $taskId
     * @param $boundedIds
     * @return bool|string
     * @throws ErrorException
     */
    public function setBoundedUsers($boundedIds)
    {
        $newBounded = array_filter($boundedIds);
        $oldBounded = array_filter(array_keys($this->getBoundedUsersIds()));

        /** @var array $boundUsers Список только И пользователей */
        $executors = array_filter(array_keys($this->getIspoln()));

        /** @var array $addBounded Список новых связанных пользователей */
        $addBoundUsers = array_diff($newBounded, $oldBounded, $executors);
        /** @var array $deleteBoundUsers Список более не связанных пользователей */
        $deleteBoundUsers = array_diff($oldBounded, $newBounded);
        $deleteBoundUsers = array_merge($deleteBoundUsers, array_intersect($executors, $oldBounded));

        if ($this->changeBoundedUsers($this->getId(), [], [], $addBoundUsers, $deleteBoundUsers))
            return $this->getOperationComment_ChangeExecutors([], [], $addBoundUsers, $deleteBoundUsers);

        return false;
    }

    public function getBoundedUsersIds($glue = false)
    {
        return $this->getTaskBoundedIds($this->getId(), $glue);
    }

    /**
     * Return bounded users in array or delimited by coma string
     *
     * @param $task_id
     * @param bool $glue
     * @return string|array
     */
    static public function getTaskBoundedIds($task_id, $glue = false)
    {
        global $DB;
        $users = $DB->getCell2("SELECT
            us.id,
            us.fio
        FROM
            `task_users` AS tu
            JOIN `users` AS us ON tu.user_id=us.id
        WHERE
            tu.task_id=" . $DB->F($task_id) . " AND
            tu.ispoln <= 0
        GROUP BY
            us.id
        ORDER BY
            us.fio");
        return $glue ? implode($glue, $users) : $users;
    }

    /**
     * Task::deleteTask()
     * удаляет задачу, все комментарии, чистит связанных лиц, удаляет файлы
     *
     * @param integer $task_id
     * @return bool
     */
    static function deleteTask($task_id)
    {
        global $DB;

        $sql = "SELECT `id` FROM `task_files` WHERE `task_id`=" . $DB->F($task_id);
        foreach ($DB->getCell($sql, false) as $file_id)
            unlink(getcfg('files_path') . "/$file_id.dat");

        $sql = "DELETE FROM `task_users` WHERE `task_id`=" . $DB->F($task_id);
        $DB->query($sql);

        $sql = "DELETE FROM `task_comments` WHERE `task_id`=" . $DB->F($task_id);
        $DB->query($sql);

        $sql = "DELETE FROM `tasks` WHERE `id`=" . $DB->F($task_id);
        $DB->query($sql);

        return $DB->errno() ? false : true;
    }

    /**
     * Возвращает массив связанных заявок
     *
     * @param integer $task_id
     * @return array
     */

    static public function getLinkedTasksList($task_id)
    {
       global $DB;
       $task_id = (int)$task_id;
       $res = $DB->getAll("select t.id, t.plugin_uid, 'child' state
            from joinedtasks j,
            tasks t
            where j.task_id = ".$task_id."
            and j.joined_task_id = t.id
            union all
            select t.id, t.plugin_uid, 'parent' state
            from joinedtasks j,
            tasks t
            where j.joined_task_id = ".$task_id."
            and j.task_id = t.id");
       return $res;
    }

    static function getTaskIspolnIds($task_id, $glue = false)
    {
        global $DB;
        $users = $DB->getCell("SELECT
            tu.user_id
        FROM
            `task_users` AS tu
            LEFT JOIN
            `users` AS us ON us.id=tu.user_id
        WHERE
            us.active AND
            tu.task_id=" . $DB->F($task_id) . " AND
            tu.ispoln > 0
        GROUP BY
            tu.user_id
        ORDER BY
            tu.user_id");
        return $glue ? implode($glue, $users) : $users;
    }

    static function getTaskIspoln1($task_id)
    {
        global $DB;
        $ret = '';
        $title = '';
        $users = $DB->getCell("SELECT
            us.fio
        FROM
            `task_users` AS tu
            JOIN
            `users` AS us ON tu.user_id=us.id
        WHERE
            tu.task_id=" . $DB->F($task_id) . " AND
            tu.ispoln > 0
        GROUP BY
            us.id
        ORDER BY
            us.fio");
        if (count($users) > 2) {
            $fio = explode(" ", $users[0]);
            $ret .= "<table class='t7' cellpadding='0' cellspacing='0' style='margin:0; padding:0;'><tr class='int' style='background:none !important;'><td nowrap style='background: none ;font-size: 9pt;margin:0; padding:0; border:0;'>" . $fio[0] . " " . ($fio[1][0] != "" ? $fio[1] . " " : "") . ($fio[2][0] != "" ? $fio[2] . " " : "") . "</td></tr></table>";
            $fio = explode(" ", $users[1]);
            $ret .= "<table class='t7' cellpadding='0' cellspacing='0' style='margin:0; padding:0;'><tr class='int' style='background:none !important;'><td nowrap style='background: none ;font-size: 9pt;margin:0; padding:0; border:0;'>" . $fio[0] . " " . ($fio[1][0] != "" ? $fio[1] . " " : "") . ($fio[2][0] != "" ? $fio[2] . " " : "") . "</td></tr></table>";
            $iCount = count($users) - 2;
            if ((strlen($iCount) == 2 && substr($iCount, 0, 1) == "1") || (((substr($iCount, 0, 1) != 1) || count($iCount) == 1) && (substr($iCount, strlen($iCount) - 1, 1) == "5" || substr($iCount, strlen($iCount) - 1, 1) == "6" || substr($iCount, strlen($iCount) - 1, 1) == "7" || substr($iCount, strlen($iCount) - 1, 1) == "8" || substr($iCount, strlen($iCount) - 1, 1) == "9" || substr($iCount, strlen($iCount) - 1, 1) == "0"))) {
                $ispolntitle = "исполнителей";
            } else {
                if (substr($iCount, strlen($iCount) - 1, 1) == "1" || $iCount == "1") {
                    $ispolntitle = "исполнитель";
                } else {
                    if (substr($iCount, strlen($iCount) - 1, 1) == "2" || substr($iCount, strlen($iCount) - 1, 1) == "3" || substr($iCount, strlen($iCount) - 1, 1) == "4") {
                        $ispolntitle = "исполнителя";
                    }
                }
            }
            $ret .= "<table class='t7' cellpadding='0' cellspacing='0' style='margin:0; padding:0;'><tr class='int' style='background:none !important;'><td nowrap style='font-weight:bold;background: none ;font-size: 8pt;margin:0; padding:0; border:0;'>и еще " . $iCount . " " . $ispolntitle . "</td></tr></table>";
            foreach ($users as $key => $item) {
                $title .= $item . "\r\n";
            }
        } else {
            foreach ($users as $key => $item) {
                $fio = explode(" ", $item);
                $ret .= "<table class='t7' cellpadding='0' cellspacing='0' style='margin:0; padding:0;'><tr class='int' style='background:none !important;'><td nowrap style='background: none; font-size: 9pt;margin:0; padding:0; border:0;'>" . $fio[0] . " " . ($fio[1][0] != " " ? $fio[1] . " " : "") . ($fio[2][0] != " " ? $fio[2] . " " : "") . "</td></tr></table>";
                $title .= $item . "\r\n";
            }
        }
        return "<span title='$title'>" . preg_replace("/, $/", "", $ret) . "</span>";
    }

    static function getTaskIspoln2($task_id)
    {
        $users = wf::$db->getCell2("SELECT
            us.id,
            us.fio
        FROM
            `task_users` AS tu
            JOIN
            `users` AS us ON tu.user_id=us.id
        WHERE
            tu.task_id=" . wf::$db->F($task_id) . " AND
            tu.ispoln > 0
        GROUP BY
             us.id
        ORDER BY
            us.fio
        ;");
        $ret = '';

        $title = '';
        if (count($users) > 2) {
            $i = 1;
            foreach ($users as $key => $val) {
                if ($i < 3) {
                    $fio = explode(" ", $val);
                    $ret .= "
                    <table class='t7' cellpadding='0' cellspacing='0' style='margin:0; padding:0;'>
                        <tr class='int' style='background:none !important;'>
                            <td nowrap style='background: none ;font-size: 9pt;margin:0; padding:0; border:0;'>
                                " . $fio[0] . " " .
                        ($fio[1][0] != "" ? mb_strtoupper(mb_substr($fio[1], 0, 1)) . "." : "") .
                        ($fio[2][0] != "" ? mb_strtoupper(mb_substr($fio[2], 0, 1)) . ". " : "") . "
                            </td>
                        </tr>
                    </table>";
                }
                $i += 1;
            }

            $iCount = count($users) - 2;
            if ((strlen($iCount) == 2 && substr($iCount, 0, 1) == "1") ||
                (((substr($iCount, 0, 1) != 1) || $iCount == 1) &&
                    (substr($iCount, strlen($iCount) - 1, 1) == "5" ||
                        substr($iCount, strlen($iCount) - 1, 1) == "6" ||
                        substr($iCount, strlen($iCount) - 1, 1) == "7" ||
                        substr($iCount, strlen($iCount) - 1, 1) == "8" ||
                        substr($iCount, strlen($iCount) - 1, 1) == "9" ||
                        substr($iCount, strlen($iCount) - 1, 1) == "0"
                    )
                )
            ) {
                $ispolntitle = "исполнителей";
            } else {
                if (substr($iCount, strlen($iCount) - 1, 1) == "1" || $iCount == "1") {
                    $ispolntitle = "исполнитель";
                } else {
                    if (substr($iCount, strlen($iCount) - 1, 1) == "2" ||
                        substr($iCount, strlen($iCount) - 1, 1) == "3" ||
                        substr($iCount, strlen($iCount) - 1, 1) == "4"
                    ) {
                        $ispolntitle = "исполнителя";
                    }
                }
            }

            $ret .= "<table class='t7' cellpadding='0' cellspacing='0' style='margin:0; padding:0;'>
                <tr class='int' style='background:none !important;'>
                    <td nowrap style='font-weight:bold;background: none ;font-size: 8pt;margin:0; padding:0; border:0;'>
                        и еще " . $iCount . " " . $ispolntitle . "
                    </td>
                </tr>
            </table>";

            foreach ($users as $key => $item) {
                $title .= $item . " (GSSID: " . $key . ")\r\n";
            }
        } else {
            foreach ($users as $key => $item) {
                $fio = explode(" ", $item);
                $ret .= "<table class='t7' cellpadding='0' cellspacing='0' style='margin:0; padding:0;'>
                    <tr class='int' style='background:none !important;'>
                        <td nowrap style='background: none; font-size: 9pt;margin:0; padding:0; border:0;'>
                            " . $fio[0] . " " .
                    ($fio[1][0] != " " ? mb_strtoupper(mb_substr($fio[1], 0, 1)) . "." : "") .
                    ($fio[2][0] != " " ? mb_strtoupper(mb_substr($fio[2], 0, 1)) . ". " : "") . "
                        </td>
                    </tr>
                </table>";
                $title .= $item . " (GSSID: " . $key . ")\r\n";
            }
        }
        return "<span title='$title'>" . preg_replace("/, $/", "", $ret) . "</span>";
    }

    function setTitle($value)
    {
        global $DB;
        $sql = "UPDATE `tasks` SET `task_title`=" . $DB->F($value, true) . " WHERE `id`=" . $DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function setComment($value)
    {
        global $DB;
        $sql = "UPDATE `tasks` SET `task_comment`=" . $DB->F($value, true) . " WHERE `id`=" . $DB->F($this->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    function isAccessable($user_id = 0)
    {
        global $USER, $DB;

        if (defined('FULL_ACCESS') || !$this->isPrivate())
            return true;
        if (!$user_id)
            $user_id = $USER->getId();

        $sql = "SELECT COUNT(*) FROM `task_users` AS tu WHERE tu.task_id=" . $DB->F($this->getId()) . " AND tu.user_id=" . $DB->F($user_id);
        return $DB->getField($sql);
    }

    function isPrivate()
    {
        return $this->task['is_private'] ? true : false;
    }

    /**
     *
     * @param $status integer|integer[] Status id
     * @return boolean
     */
    public function isHadStatus($status)
    {
        global $DB;

        if (is_array($status)) {
            $status = implode(',', array_map(array($DB, 'F'), $status));
        } else {
            $status = $DB->F($status);
        }

        $sql = "SELECT count(*) FROM `task_comments` WHERE task_id = " . $DB->F($this->getId()) . " AND status_id IN (" . $status . ");";
        $result = $DB->getField($sql);
        return $result > 0;
    }

    /**
     * Task::updateTime()
     * обновляет время просмотра задачи для текущего пользователя
     *
     * @return void
     */
    function updateTime()
    {
        global $DB, $USER;
        $sql = "UPDATE `task_users` SET `time`=NOW() WHERE `task_id`=" . $DB->F($this->getId()) . " AND `user_id`=" . $DB->F($USER->getId());
        $DB->query($sql);
        return $DB->errno() ? false : true;
    }

    /**
     * Task::getUsers()
     * получить связанных лиц задачи
     *
     * @param string $glue - если указан, то возвращается строка телефонов соединенная $glue
     * @return array || string
     *
     * @see Task::getTaskIspoln()
     */
    function getUsers($glue = false)
    {
        return $this->getTaskUsers($this->getId(), $glue);
    }

    /**
     * Task::getTaskUsers()
     * получить связанных лиц
     *
     * @param integet $task_id
     * @param string $glue - если указан, то возвращается строка с ФИО соединенная $glue
     * @return array(user_id=>user_fio, ...) || string
     */
    static function getTaskUsers($task_id, $glue = false)
    {
        global $DB;
        $users = $DB->getCell2("SELECT
            us.id,
            us.fio
        FROM
            `task_users` AS tu
            JOIN
            `users` AS us ON tu.user_id=us.id
            JOIN tasks AS t ON t.id = tu.task_id
        WHERE
            tu.task_id=" . $DB->F($task_id) . "
            AND CASE WHEN t.plugin_uid = 'projects' AND us.active = 1 THEN 1
                     WHEN t.plugin_uid != 'projects' THEN 1
                     ELSE 0
                 END = 1
        GROUP BY
            us.id
        ORDER BY
            us.fio");
        return $glue ? implode($glue, $users) : $users;
    }

    /**
     * Task::getUsersExt()
     * получить связанных лиц задачи c датой/временем последнего посещений системы
     *
     * @param string $glue - если указан, то возвращается строка телефонов соединенная $glue
     * @return array || string
     *
     * @see Task::getTaskIspoln()
     */
    function getUsersExt($glue = false)
    {
        return $this->getTaskUsersExt($this->getId(), $glue);
    }

    /**
     * Task::getTaskUsersExt()
     * получить связанных лиц с информацией о времени последнего посещений системы
     *
     * @param integet $task_id
     * @param string $glue - если указан, то возвращается строка с ФИО соединенная $glue
     * @return array(user_id=>user_fio, ...) || string
     */
    static function getTaskUsersExt($task_id)
    {
        global $DB;
        $users = $DB->query("SELECT
            us.id,
            us.fio,
            DATE_FORMAT(tu.time, '%d.%m.%Y %H:%i') AS lastvisit,
            us.email,
            us.phones
        FROM
            `task_users` AS tu
            JOIN
            `users` AS us ON tu.user_id=us.id
            JOIN tasks AS t ON t.id = tu.task_id
        WHERE
            tu.task_id=" . $DB->F($task_id) . "
            AND CASE WHEN t.plugin_uid = 'projects' AND us.active = 1 THEN 1
                     WHEN t.plugin_uid != 'projects' THEN 1
                     ELSE 0
                 END = 1
        GROUP BY
            us.id
        ORDER BY
            us.fio;
        ");
        if ($DB->errno())
            UIError($DB->error());
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $ret[] = $r;
            }
            return $ret;
        } else {
            return false;
        }
    }

    /**
     * Task::getIspolnPhones()
     * получить телефоны исполнителей задачи
     *
     * @param string $glue - если указан, то возвращается строка телефонов соединенная $glue
     * @return array || string
     *
     * @see Task::getTaskIspolnPhones()
     */
    function getIspolnPhones($glue = false)
    {
        return $this->getTaskIspolnPhones($this->getId(), $glue);
    }

    /**
     * Task::getTaskIspolnPhones()
     * получить телефоны исполнителей задачи
     *
     * @param integet $task_id
     * @param string $glue - если указан, то возвращается строка телефонов соединенная $glue
     * @return array || string
     */
    static function getTaskIspolnPhones($task_id, $glue = false)
    {
        global $DB;
        $sql = "SELECT
            tu.user_id,
            us.phones
        FROM
            `task_users` AS tu
            JOIN
            `users` AS us ON tu.user_id=us.id
        WHERE
            tu.task_id=" . $DB->F($task_id) . " AND
            tu.ispoln AND
            us.phones IS NOT NULL
        GROUP BY
            us.id
        ORDER BY
            us.fio";
        $arr = $DB->getCell2($sql);
        return $glue ? implode($glue, $arr) : $arr;
    }

    /**
     * Task::getStatusArray()
     * получить массив информации о текущем статусе
     *
     * @return array
     */
    function getStatusArray()
    {
        return $this->status;
    }

    /**
     * @param string $text
     * @param string $tag
     * @param int $status_id
     * @return int
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    function addComment($text, $tag = '', $status_id = 0)
    {
        global $USER;

        /** @var OrmTask $task */
        $task = wf::$em->find(OrmTask::class, intval($this->getId()));

        if(null === $task) {
            throw new \LogicException('Task with id: '.$this->getId().' not exists!');
        }

        $user_id = $USER && $USER->getId() ? $USER->getId() : 0;

        $newStatus = null;
        /** @var TaskStatus $newStatus */
        if ($status_id > 0)
            $newStatus = wf::$em->find(TaskStatus::class, $status_id);
        /** @var OrmUser $author */
        $author = wf::$em->find(OrmUser::class, $user_id);

        $comment = (new TaskComment())
            ->setText($text)
            ->setTag($tag)
            ->setDatetime(new DateTime())
            ->setTask($task)
            ->setStatus($newStatus)
            ->setAuthor($author);

        $this->taskManager->addComment($task, $comment);

        $this->loadTask();

        return $comment->getId();
    }

    /**
     * Task::setIspoln()
     * меняет исполнителей задачи
     *
     * @param mixed $user_ids - массив или строка вида "1,2,3" ид исполнителей
     * @return bool
     */
    function setIspoln($user_ids)
    {
        global $DB;

        if (!is_array($user_ids))
            $user_ids = explode(',', $user_ids);


        /**
         * TODO Artem refactor this
         *
         * задача от Петра Глухих 86713 - Обнаружена ошибка в заявках на B2B adsl, при изменении статуса заявки,
         * идет рассылка всем техникам, которые были в истории по заявке, так не должно быть,
         * Технику письмо и СМС должно уходить только при назначении заявки в график, по аналогии с СКП.
         * удаление ответсвенных исполнителей при постановке в график
         */
        if ($this->getPluginUid() == "services" || $this->getPluginUid() == "connections") {
            $sql = "DELETE FROM `task_users` WHERE `task_id`=" . $DB->F($this->getId()) . "  AND `user_id` IN (SELECT `user_id` FROM `list_empl` WHERE 1);";
            $DB->query($sql);
        }

        if ($this->getPluginUid() == "services") {
            $DB->query("DELETE FROM `gfx` WHERE `task_id`=" . $DB->F($this->getId()));
        }
        /**/

        $result = $this->setExecutors($user_ids);

        return $result;
    }

    function getPluginUid()
    {
        return $this->task['plugin_uid'];
    }

    /**
     * Task::setUsers()
     * меняет связанных лиц
     *
     * @param mixed $user_ids - массив или строка вида "1,2,3" ид пользователей
     * @return bool
     */
    function setUsers($user_ids)
    {
        if (!is_array($user_ids))
            $user_ids = explode(',', $user_ids);

        return $this->setBoundedUsers($user_ids);
    }

    /**
     * Task::getStatusNameById()
     * поолучить название статуса по его ид в пределеах текущего плагина
     *
     * @param integer $status_id
     * @return string
     */
    function getStatusNameById($status_id)
    {
        global $DB;
        return $DB->getField("SELECT `name` FROM `task_status` WHERE `id`=" . $DB->F($status_id) . " AND `plugin_uid`=" . $DB->F($this->getPluginUid()));
    }

    /**
     * Task::getComments()
     * получить комментарии к задаче + файлы + форма для комментария
     *
     * это какой то ад
     *
     * @return string
     *
     * @see templates/task_comments.tmpl.html
     */
    function getComments($showAddComment = true)
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("templates"));

        $LKUser = adm_users_plugin::isLKUser($USER->getId());
        $currentStatus = $this->getStatusId();
        $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
        $countStatus = adm_statuses_plugin::getStatusByTag("report", "connections");
        $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "connections");
        $targetStatus = false;

        if ($currentStatus == $doneStatus["id"] || $currentStatus == $countStatus["id"] || $currentStatus == $completeStatus["id"])
            $targetStatus = true;

        $sql = "SELECT
            DATE_FORMAT(`datetime`, '%Y-%m-%d') AS doneDate
        FROM
            `task_comments`
        WHERE
            `task_id`=" . $DB->F($this->getId()) . " AND
            `status_id`=" . $DB->F($doneStatus["id"]) . "
        ORDER BY
            doneDate DESC
        ;";

        $doneDate = $DB->getField($sql);

        if (date("m") == "01") {
            $df = date("Y-m-d", strtotime(date("Y-m") . "+" . getcfg('connfinreport_dc_january') . "days"));
        } else
            $df = date("Y-m-d", strtotime(date("Y-m") . "+" . getcfg('connfinreport_dc') . "days"));

        $cd = date("Y-m-d");
        $readonly = false;

        if (!$USER->checkAccess("calcticketafterdeadline", User::ACCESS_WRITE) && $targetStatus) {
            if ($cd > $df) {
                if (date("Y-m", strtotime($doneDate)) <= date("Y-m", strtotime(date("Y-m") . " - 1 month")))
                    $readonly = true;
                else
                    $readonly = false;
            } else {
                if (date("Y-m", strtotime($doneDate)) <= date("Y-m", strtotime(date("Y-m") . " - 2 month")))
                    $readonly = true;
                else
                    $readonly = false;
            }
        }

        if ($this->getPluginUid() == "services" &&
            $this->getStatusId() == 25 &&
            !$USER->checkAccess("edit_closed_skp_task", User::ACCESS_WRITE)
        ) {
            $readonly = true;
        }

        if ($LKUser) {
            if ($USER->getAccess('lkaccess2allcomments')) {
                $tpl->loadTemplatefile("task_comments.tmpl.html");
            } else {
                $tpl->loadTemplatefile("task_comments_lk.tmpl.html");
            }
        } else {
            if ($readonly) {
                $tpl->loadTemplatefile("task_comments_readonly.tmpl.html");
            } else {
                $tpl->loadTemplatefile("task_comments.tmpl.html");
            }
        }

        $tpl->setVariable('TASK_ID', $this->getId());


        $color = false;
        $i = false;
        $wasinGfx = $DB->getField("SELECT COUNT(`task_id`) FROM `task_comments` WHERE `task_id`=" . $DB->F($this->getId()) . " AND `status_id`=9;");
        if ($this->getPluginUid() == "services" && !$wasinGfx) {
            $tpl->setVariable("BLOCKSTATUS", "<input type='hidden' name='blockstatus' value='blockstatus' />");
        }
        $sql = "SELECT `empl_id` FROM `gfx` WHERE `task_id`=" . $DB->F($this->getId()) . " ORDER BY `c_date` DESC;";
        $tech_id = $DB->getField($sql);
        $forbidComments = (
            (
                $this->getStatusId() == 44 ||
                ($this->getStatusId() == 42 && $tech_id != $USER->getId())
            ) &&
            $USER->isTech($USER->getId()) &&
            !$USER->isChiefTech($USER->getId())
        ) ? true : false;


        if (!$forbidComments) {
            $first = true;

            $sql = "SELECT
                cmm.id,
                cmm.datetime,
                cmm.text,
                cmm.tag,
                us.id,
                us.fio,
                cmm.status_id
            FROM
                `task_comments` AS cmm
                LEFT JOIN
                `users` AS us ON cmm.user_id=us.id
            WHERE
                cmm.task_id=" . $DB->F($this->getId()) . "
            ORDER BY
                cmm.datetime ASC";

            $res = $DB->query($sql);

            while (list($cmm_id, $cmm_date, $cmm_text, $tag, $user_id, $user_fio, $status_id) = $DB->fetch(false, false)) {

                $comment = new TextObject($cmm_text);
                /* Using default comment purify strategy */
                $commentContext = new CommentsProcessor();
                $commentContext->purify($comment);

                $tag = new TextObject($tag);

                if ($LKUser && !$USER->getAccess('lkaccess2allcomments')) {

                    if ($USER->getId() != $user_id &&
                        $status_id != 9 &&
                        $status_id != 10 &&
                        $status_id != 12 &&
                        $status_id != 8 &&
                        $status_id != 61 &&
                        $status_id != 54 &&
                        $status_id != 64 &&
                        $status_id != 65 &&
                        $status_id != 60 &&
                        $status_id != 1 &&
                        $status_id != 3 &&
                        $status_id != 2 &&
                        $status_id != 5 &&
                        $status_id != 13 &&
                        $status_id != 71 &&
                        $status_id != 72 &&
                        $status_id != 69 &&
                        $status_id != 74 &&
                        $status_id != 59
                    )
                        continue;

                    if ($status_id == 9) {
                        $tmp = explode(",", $tag->getText());
                        $tag->setText($tmp[0]);
                    }
                }
                if (!$first && $user_id != $USER->getId() && (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) && $this->getPluginUid() == "services") {
                    continue;
                }

                $tpl->setCurrentBlock('comment');

                if ($USER->getTemplate() == "default")
                    $tpl->setVariable('CMM_COLOR', $this->getStatusColor());
                else {
                    if (!$i) {
                        $color = adm_statuses_plugin::getStatusColorByID($this->getDefaultStatusId($this->getPluginUid()));
                        $i = true;
                    }
                    if ($status_id) {
                        $color = adm_statuses_plugin::getStatusColorByID($status_id);
                    } else {
                        if (!$color)
                            $color = $this->getStatusColor();
                    }
                    $tpl->setVariable('CMM_COLOR', $color);
                }
                $tpl->setVariable('CMM_DATE', rudate("d.m.Y H:i", $cmm_date));

                $editcomment = ($this->getPluginUid() == "projects" && $user_id == $USER->getId()) ?
                    "&nbsp;&nbsp;<a href='#' id='" . $cmm_id . "' style='float:right;' class='editcomment'><img src='/templates/images/pencil.png' /></a>" :
                    "";

                $tpl->setVariable('CMM_USER_FIO', ($user_fio != "" ?
                        htmlspecialchars($user_fio) :
                        "<i>Система</i>") . $editcomment);

                if ($showAddComment) {
                    $tpl->setCurrentBlock("showaddcomment");
                    $tpl->setVariable('CMM_USER_FIO_ONLY', htmlspecialchars($user_fio ? $user_fio : "Система"));
                    $tpl->setVariable('CMM_DATE_SADC', rudate("d.m.Y H:i", $cmm_date));
                    $tpl->parse("showaddcomment");
                }

                $tpl->setVariable('CMM_ID', $cmm_id);

                if (!in_array($this->getPluginUid(), [\models\Task::BILL, \models\Task::PROJECT]) &&
                    $USER->checkAccess("hidetelnum") != User::ACCESS_NONE ||
                    (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()))
                ) {
                    (new TextPhonesRemoverDecorator($comment))->process();
                    (new TextPhonesRemoverDecorator($tag))->process();
                }

                if ($LKUser) {
                    (new HideExecutorDecorator($tag))->process();
                }
                $tpl->setVariable('CMM_TEXT', $comment->getText());
                $tpl->setCurrentBlock('tag');
                $tpl->setVariable('CMM_TAG', $tag->getText());
                $tpl->parse('tag');

                $first = false;
                $tpl->parse('comment');
            }

            $DB->free($res);
        }

        $DB->query("UPDATE
            `task_users`
        SET
            `time`=NOW()
        WHERE
            `user_id`=" . $DB->F($USER->getId()) . " AND
            `task_id`=" . $DB->F($this->getId()) . "
        ;");
        $DB->free();

        if ($DB->errno())
            UIError($DB->error());

        if ($showAddComment) {
            $tpl->setCurrentBlock("sac_js");
            $tpl->setVariable("SAC_TASK_ID", $this->getId());
            $tpl->parse("sac_js");
            $tpl->setCurrentBlock("sac");
            $tpl->setVariable("SACC_TASK_ID", $this->getId());
            if($LKUser) {
                $sql = "SELECT 
                    * 
                FROM 
                    `task_status` 
                WHERE 
                    `plugin_uid`=" . $DB->F($this->getPluginUid()) . " 
                    AND NOT `is_hidden`
                     AND id in (". implode(', ', adm_statuses_plugin::getAgentAllowedStatuses()) .")
                ORDER BY `name`";
            } else {
                $sql = "SELECT * FROM `task_status` WHERE `plugin_uid`=" . $DB->F($this->getPluginUid()) . " AND NOT `is_hidden` ORDER BY `name`";

            }
            $result1 = $DB->query($sql);
            $cb_class = $this->getPluginUid() . '_plugin';
            $cb_method = 'statusCallbackGet';
            $cb = method_exists($cb_class, $cb_method);

            if (!$USER->isTech(\wf::getUserId()) || ($USER->isTech(\wf::getUserId()) && $USER->isChiefTech(\wf::getUserId()))) {
                while ($s = $DB->fetch(true, true, $result1)) {
                    if ($cb)
                        $s['name'] = call_user_func(array($cb_class, $cb_method), $this->getId(), $s);

                    if ($s['name']) {
                        if ($cb_class == "services_plugin" && !$USER->checkAccess("edit_status_service", User::ACCESS_WRITE)) {
                            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "services");
                            $countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
                            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "services");
                            $otkaz = adm_statuses_plugin::getStatusByTag("otkaz", "services");
                            $opotkaz = adm_statuses_plugin::getStatusByTag("opotkaz", "services");
                            $tehotkaz = adm_statuses_plugin::getStatusByTag("tehotkaz", "services");
                            $sql = "SELECT COUNT(id) AS cnt FROM `task_comments` WHERE `task_id`=" . $DB->F($this->getId()) . " AND (`status_id`=" . $DB->F($doneStatus["id"]) . " OR `status_id`=" . $DB->F($countStatus["id"]) . " OR `status_id`=" . $DB->F($completeStatus["id"]) . ");";
                            $is_done = $DB->getField($sql);
                            $sql = "SELECT COUNT(id) AS cnt FROM `task_comments` WHERE `task_id`=" . $DB->F($this->getId()) . " AND `status_id`=" . $DB->F($s["id"]) . ";";
                            $cnt = $DB->getField($sql);
                            if ($cnt == 0 && !($is_done && ($s["id"] == $otkaz["id"] || $s["id"] == $opotkaz["id"] || $s["id"] == $tehotkaz["id"]))) {
                                $tpl->setCurrentBlock('status');
                                $tpl->setVariable('STATUS_ID', $s['id']);
                                $tpl->setVariable('STATUS_NAME', $s['name']);
                                $tpl->setVariable('STATUS_COLOR', $s['color']);
                                if (getcfg('nbn_contr_id') == $DB->getField("SELECT `cnt_id` FROM `tickets` WHERE `task_id`=" . $DB->F($this->getId()) . ";") && $s['require_comment'] && preg_match("/(done)(.*)(nbn)/", $s['tag'])) {
                                    $tpl->setVariable("ADDITIONAL_TAGS", "class=\"rc rprice\"");
                                } else {
                                    if ($s['require_comment'] == 1) {
                                        $tpl->setVariable("ADDITIONAL_TAGS", "class=\"rc\"");
                                    }
                                    if (preg_match("/(done).*(nbn)/", $s['tag'])) {
                                        if (getcfg('nbn_contr_id') == $DB->getField("SELECT `cnt_id` FROM `tickets` WHERE `task_id`=" . $DB->F($this->getId()) . ";"))
                                            $tpl->setVariable("ADDITIONAL_TAGS", "class=\"rprice\"");
                                    }
                                }
                                $tpl->parse("status");
                            }
                        } else {
                            $tpl->setCurrentBlock('status');
                            $tpl->setVariable('STATUS_ID', $s['id']);
                            $tpl->setVariable('STATUS_NAME', $s['name']);
                            $tpl->setVariable('STATUS_COLOR', $s['color']);
                            if (getcfg('nbn_contr_id') == $DB->getField("SELECT `cnt_id` FROM `tickets` WHERE `task_id`=" . $DB->F($this->getId()) . ";") && $s['require_comment'] && preg_match("/(done)(.*)(nbn)/", $s['tag'])) {
                                $tpl->setVariable("ADDITIONAL_TAGS", "class=\"rc rprice\"");
                            } else {
                                if ($s['require_comment'] == 1) {
                                    if ($s["id"] == "6" || $s["id"] == "50") {
                                        $tpl->setVariable("ADDITIONAL_TAGS", "class=\"rc rdate\"");
                                    } else
                                        $tpl->setVariable("ADDITIONAL_TAGS", "class=\"rc\"");
                                }
                                if (preg_match("/(done).*(nbn)/", $s['tag'])) {
                                    if (getcfg('nbn_contr_id') == $DB->getField("SELECT `cnt_id` FROM `tickets` WHERE `task_id`=" . $DB->F($this->getId()) . ";"))
                                        $tpl->setVariable("ADDITIONAL_TAGS", "class=\"rprice\"");
                                }
                            }

                            $tpl->parse("status");
                        }
                    }
                }

                $DB->free();
                if(!$LKUser) {
                    $tpl->touchBlock("delegate");
                }

            } else {
                $rejectStatus = adm_statuses_plugin::getStatusByTag("smsreject", "services");
                $delayStatus = adm_statuses_plugin::getStatusByTag("otlozh", "services");
                $status_id = $DB->getField("SELECT `status_id` FROM `tasks` WHERE `id`=" . $DB->F($this->getId()) . ";");
                $forbid_tt = array(61, 54, 62, 56, 51, 65, 60, 55, 80, 58, 57, 52, 64);
                $forbid_conn = array(1, 26, 14, 48, 2, 5, 13, 27, 81, 16, 20, 23, 82, 15);
                if ($this->getPluginUid() == "connections") {

                    if (!in_array($status_id, $forbid_conn)) {
                        if ($USER->checkAccess(CONNECTIONS_ALLOW_CANCELING)) {
                            $allowedStatuses = [
                                adm_statuses_plugin::getStatusByTag("done", "connections"),
                                adm_statuses_plugin::getStatusByTag("tehotkaz", "connections"),
                                adm_statuses_plugin::getStatusByTag("otkaz", "connections"),
                                adm_statuses_plugin::getStatusByTag("opotkaz", "connections"),
                            ];
                        } else {
                            $allowedStatuses = [adm_statuses_plugin::getStatusByTag("done", "connections")];
                        }

                        foreach($allowedStatuses as $st) {
                            $tpl->setCurrentBlock('status');
                            $tpl->setVariable('STATUS_ID', $st['id']);
                            $tpl->setVariable('STATUS_NAME', $st['name']);
                            $tpl->setVariable('STATUS_COLOR', $st['color']);
                            $tpl->setVariable("ADDITIONAL_TAGS", "class=\"rfcloseconnticket\"");
                            $tpl->parse("status");
                        }
                    }
                }

                if ($this->getPluginUid() == "accidents") {
                    if (!in_array($status_id, $forbid_tt)) {

                        if ($USER->checkAccess(ACCIDENTS_ALLOW_CANCELING)) {
                            $allowedStatuses = [
                                adm_statuses_plugin::getStatusByTag("done", "accidents"),
                                adm_statuses_plugin::getStatusByTag("otkaz", "accidents"),
                                adm_statuses_plugin::getStatusByTag("otlozh", "accidents"),
                                adm_statuses_plugin::getStatusByTag("opotkaz", "accidents"),
                            ];
                        } else {
                            $allowedStatuses = [adm_statuses_plugin::getStatusByTag("done", "accidents")];
                        }

                        foreach($allowedStatuses as $st) {
                            $tpl->setCurrentBlock('status');
                            $tpl->setVariable('STATUS_ID', $st['id']);
                            if(TaskStatus::STATUS_ACCIDENT_DONE === $doneStatus['id']) {
                                $tlist = adm_ticket_tttypes_plugin::getTypeOptions();
                                $stlist = adm_ticket_tttypes_plugin::getSubTypeOptions();
                                $tpl->setVariable('STATUS_NAME', $doneStatus['name'] . "</label> - тип: <select style='width:150px;' name='tt_type'>" . $tlist . "</select> подтип: <select style='width:150px;' name='tt_subtype'>" . $stlist . "</select>");
                            } else {
                                $tpl->setVariable('STATUS_NAME', $st['name']);
                            }
                            $tpl->setVariable('STATUS_COLOR', $st['color']);
                            $tpl->setVariable("ADDITIONAL_TAGS", "class=\"rfcloseconnticket\"");
                            $tpl->parse("status");
                        }
                    }
                }
                if ($status_id == 50 && $this->getPluginUid() == "services") {

                } else {
                    if ($status_id == 9 || $status_id == 45 || $status_id == 50 || $status_id == 42) {
                        $tpl->setCurrentBlock('status');
                        $tpl->setVariable('STATUS_ID', $rejectStatus['id']);
                        $tpl->setVariable('STATUS_NAME', $rejectStatus['name']);
                        $tpl->setVariable('STATUS_COLOR', $rejectStatus['color']);
                        $tpl->setVariable("ADDITIONAL_TAGS", "class=\"rc \"");
                        $tpl->parse("status");
                        if (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId()))) {
                            $tpl->setCurrentBlock('status');
                            $tpl->setVariable('STATUS_ID', $delayStatus['id']);
                            $tpl->setVariable('STATUS_NAME', $delayStatus['name'] . " до <input type='text' name='otlozh_date' class='datetime_input'>");
                            $tpl->setVariable('STATUS_COLOR', $delayStatus['color']);
                            $tpl->setVariable("ADDITIONAL_TAGS", "class=\"rc rdate\"");
                            $tpl->parse("status");
                        }
                    }
                }
            }
            if ($LKUser) {

                $cancelledClientStatus = adm_statuses_plugin::getStatusByTag("otkaz", $this->getPluginUid());
                $cancelledStatus = adm_statuses_plugin::getStatusByTag("cancelbyoperator", $this->getPluginUid());
                $doneStatus = adm_statuses_plugin::getStatusByTag("done", $this->getPluginUid());
                $countStatus = adm_statuses_plugin::getStatusByTag("report", $this->getPluginUid());
                $completeStatus = adm_statuses_plugin::getStatusByTag("closed", $this->getPluginUid());

                if (($this->getStatusId() != $doneStatus["id"]) && ($this->getStatusId() != $countStatus["id"]) && ($this->getStatusId() != $completeStatus ["id"]) && $cancelledClientStatus) {
                    $tpl->setCurrentBlock("cbo-status");
                    $tpl->setVariable('CBO_STATUS_ID', $cancelledClientStatus['id']);
                    $tpl->setVariable('CBO_STATUS_NAME', $cancelledClientStatus['name']);
                    $tpl->setVariable('CBO_STATUS_COLOR', $cancelledClientStatus['color']);
                    $tpl->setVariable("CBO_ADDITIONAL_TAGS", "class=\"rc \"");
                    $tpl->parse("cbo-status");
                }
                if (($this->getStatusId() != $doneStatus["id"]) && ($this->getStatusId() != $countStatus["id"]) && ($this->getStatusId() != $completeStatus ["id"]) && $cancelledStatus) {
                    $tpl->setCurrentBlock("cbo-status");
                    $tpl->setVariable('CBO_STATUS_ID', $cancelledStatus['id']);
                    $tpl->setVariable('CBO_STATUS_NAME', $cancelledStatus['name']);
                    $tpl->setVariable('CBO_STATUS_COLOR', $cancelledStatus['color']);
                    $tpl->setVariable("CBO_ADDITIONAL_TAGS", "class=\"rc \"");
                    $tpl->parse("cbo-status");
                }
            }
            $DB->free($result1);
            $tpl->parse("sac");
        }
        return $tpl->get();
    }

    /**
     * Task::getStatusId()
     * получить ид текущего статуса
     *
     * @return integer
     */
    function getStatusId()
    {
        return (int)$this->task['status_id'];
    }

    /**
     * Task::getStatusColor()
     * получить код цвета текущего статуса
     *
     * @return string
     */
    function getStatusColor()
    {
        return $this->status['color'];
    }

    /**
     * Task::getComments()
     * получить комментарии к задаче + файлы + форма для комментария
     *
     * @return string
     *
     * @see templates/task_comments.tmpl.html
     */
    function getCommentsPrint()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("templates"));

        $LKUser = adm_users_plugin::isLKUser($USER->getId());

        $tpl->loadTemplatefile("task_comments_print.tmpl.html");

        $tpl->setVariable('TASK_ID', $this->getId());

        $sql = "SELECT cmm.id, cmm.datetime, cmm.text, cmm.tag, us.id, us.fio, cmm.status_id FROM `task_comments` AS cmm LEFT JOIN `users` AS us ON cmm.user_id=us.id WHERE cmm.task_id=" . $DB->F($this->getId()) . " ORDER BY cmm.id";
        $res = $DB->query($sql);
        $color = false;
        $i = false;
        $wasinGfx = $DB->getField("SELECT COUNT(`task_id`) FROM `task_comments` WHERE `task_id`=" . $DB->F($this->getId()) . " AND `status_id`=9;");
        if ($this->getPluginUid() == "services" && !$wasinGfx) {
            $tpl->setVariable("BLOCKSTATUS", "<input type='hidden' name='blockstatus' value='blockstatus' />");
        }
        $sql = "SELECT `empl_id` FROM `gfx` WHERE `task_id`=" . $DB->F($this->getId()) . " ORDER BY `c_date` DESC;";
        $tech_id = $DB->getField($sql);
        $forbidComments = (($this->getStatusId() == 44 || ($this->getStatusId() == 42 && ($tech_id != $USER->getId()))) && $USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) ? true : false;
        if (!$forbidComments) {
            $first = true;
            while (list($cmm_id, $cmm_date, $cmm_text, $tag, $user_id, $user_fio, $status_id) = $DB->fetch(false, false)) {

                $comment = new TextObject($cmm_text);
                /* Using default comment purify strategy */
                $commentContext = new CommentsProcessor();
                $commentContext->purify($comment);

                $tag = new TextObject($tag);
                (new TextQuoteDecorator($tag))->process();
                (new TextUrl2AnchorDecorator($tag))->process();

                if ($LKUser && !$USER->getAccess('lkaccess2allcomments')) {

                    if ($USER->getId() != $user_id &&
                        $status_id != 9 &&
                        $status_id != 10 &&
                        $status_id != 12 &&
                        $status_id != 8 &&
                        $status_id != 61 &&
                        $status_id != 54 &&
                        $status_id != 64 &&
                        $status_id != 65 &&
                        $status_id != 60 &&
                        $status_id != 1 &&
                        $status_id != 3 &&
                        $status_id != 2 &&
                        $status_id != 5 &&
                        $status_id != 13 &&
                        $status_id != 71 &&
                        $status_id != 72 &&
                        $status_id != 69 &&
                        $status_id != 74 &&
                        $status_id != 59
                    )
                        continue;

                    if ($status_id == 9) {
                        $tmp = explode(",", $tag->getText());
                        $tag->setText($tmp[0]);
                    }
                }
                if (!$first && $user_id != $USER->getId() && (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) && $this->getPluginUid() == "services") {
                    continue;
                }

                $tpl->setCurrentBlock('comment');

                if ($USER->getTemplate() == "default")
                    $tpl->setVariable('CMM_COLOR', $this->getStatusColor());
                else {
                    if (!$i) {
                        $color = adm_statuses_plugin::getStatusColorByID($this->getDefaultStatusId($this->getPluginUid()));
                        $i = true;
                    }
                    if ($status_id) {
                        $color = adm_statuses_plugin::getStatusColorByID($status_id);
                    } else {
                        if (!$color)
                            $color = $this->getStatusColor();
                    }
                    $tpl->setVariable('CMM_COLOR', $color);
                }
                $tpl->setVariable('CMM_DATE', rudate("d.m.Y H:i", $cmm_date));

                $editcomment = ($this->getPluginUid() == "projects" && $user_id == $USER->getId()) ?
                    "&nbsp;&nbsp;<a href='#' id='" . $cmm_id . "' style='float:right;' class='editcomment'><img src='/templates/images/pencil.png' /></a>" :
                    "";

                $tpl->setVariable('CMM_USER_FIO', ($user_fio != "" ?
                        htmlspecialchars($user_fio) :
                        "<i>Система</i>") . $editcomment);

                $tpl->setVariable('CMM_ID', $cmm_id);

                if ($USER->checkAccess("hidetelnum") != User::ACCESS_NONE ||
                    (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()))
                ) {
                    (new TextPhonesRemoverDecorator($comment))->process();
                    (new TextPhonesRemoverDecorator($tag))->process();
                }
                $tpl->setVariable('CMM_TEXT', $comment->getText());
                $tpl->setCurrentBlock('tag');
                $tpl->setVariable('CMM_TAG', $tag->getText());
                $tpl->parse('tag');

                $first = false;
                $tpl->parse('comment');
            }
        }


        $DB->free($res);

        return $tpl->get();
    }

    /**
     * Task::getFirstCommentText()
     * получить первый комментарий к задаче
     *
     * @return integer
     */
    function getFirstCommentText()
    {
        global $DB;
        $text = $DB->getField("SELECT `text` FROM `task_comments` WHERE `task_id`=" . $DB->F($this->getId()) . " ORDER BY `datetime` ASC LIMIT 1");
        $txt_obj = new TextObject($text);

        (new TextPhonesRemoverDecorator($txt_obj))->apply_decoration();
        return $txt_obj->getText();
    }

    /**
     * Task::addFile()
     * добавить файл к задаче
     *
     * @param string $path - путь к файлу, который будет скопирован в хранилище файлов
     * @param string $name - оригинальное имя файла
     * @param string $mime_type
     * @return integer|boolean
     */
    function addFile($path, $name, $mime_type)
    {
        global $USER;

        $file = new \Symfony\Component\HttpFoundation\File\UploadedFile($path, $name);
        $taskFile = (new \models\TaskFile())
            ->setFile($file)
            ->setFileName(sha1($name.$mime_type))
            ->setOrigName($name)
            ->setMimeType($mime_type)
            ->setUser($USER->getModelUser())
            ->setTask($this->taskEntity);
        wf::$em->persist($taskFile);
        wf::$em->flush();

        return $taskFile->getId();
    }

    /**
     * Task::addFile()
     * удалить файл из задачи
     *
     * @param int $id - ID файла
     * @param int $task_id - ID задачи, которой принадлежит файл
     * @return true - если все ок || false - в случае ошибки)
     */
    function deleteFile($id, $task_id)
    {
        /* @var $taskFile \models\TaskFile */
        $taskFile = wf::$em->find(\models\TaskFile::class, $id);

        if (null === $taskFile || $taskFile->getTask()->getId() != $task_id) {
            return false;
        }

        wf::$em->remove($taskFile);
        wf::$em->flush();
        return $taskFile->getOrigName();
    }

    /**
     * Task::getFiles()
     * получить массив файлов задачи
     *
     * @return \models\TaskFile[]
     */
    function getFiles($for_delete = false)
    {
        /** @var \models\TaskFile[] $taskFiles */
        return $taskFiles = \wf::$em->getRepository(TaskFile::class)
            ->findBy(['task'=>$this->getId()]);
    }

    /**
     * Task::toMail()
     * отправить задачу и последний комментарий на почту связанным лицам
     *
     * @return bool
     *
     * @see templates/task_mail.tmpl.html
     */
    function toMail($title = false)
    {
        global $DB, $USER;
        $to = array();
        $protocol = '';
        $totech = array();
        $sql = "SELECT us.email, us.fio FROM `task_users` AS tu JOIN `users` AS us ON tu.user_id=us.id WHERE tu.task_id=" . $DB->F($this->getId()) . " AND us.email IS NOT NULL AND us.active AND us.id != " . $DB->F($USER->getId());
        $i = 0;
        $recipients = $DB->getCell2($sql);

        foreach ($recipients as $eml => $fio) {
            if ($eml != "") {
                $to[] = "$fio <$eml>";
                $totech[] = "$fio <$eml>";
            }
        }
        $tpl = new HTML_Template_IT(path2('templates'));
        $ttype = $DB->getField("SELECT `task_type`  FROM `tickets` WHERE task_id=" . $DB->F($this->getId()) . ";");
        if ($this->getPluginUid() == \models\Task::SERVICE) {
            $t = new ServiceTicket($this->getId());
            $tpl->loadTemplatefile('skp_task_mail.tmpl.html');
            if ($ttype == "10") {
                $to = array();
                $totech = array();
                $sql = "SELECT
                    us.email,
                    us.fio
                FROM
                    `task_users` AS tu
                    JOIN
                    `users` AS us ON tu.user_id=us.id
                WHERE
                    tu.ispoln=1 AND
                    tu.task_id=" . $DB->F($this->getId()) . " AND
                    us.email IS NOT NULL AND
                    us.active AND
                    us.id != " . $DB->F($USER->getId()) . "
                ;";
                foreach ($DB->getCell2($sql) as $eml => $fio) {
                    if ($eml != "") {
                        $to[] = "$fio <$eml>";
                        $totech[] = "$fio <$eml>";
                    }
                }
                if ($t->getStatusId() != TaskStatus::STATUS_SERVICE_ENGINEER_SELECTION) {
                    $to[] = $USER->getFio() . " <" . $USER->getEmail() . ">";
                }
                if ($t->getStatusId() == TaskStatus::STATUS_SERVICE_GFX) {
//$to[] = $DB->getField("SELECT `fio` FROM `users` WHERE `id`=(SELECT `empl_id` FROM `gfx` WHERE `task_id`=".$DB->F($t->getId())." LIMIT 1)") ." <".$DB->getField("SELECT `email` FROM `users` WHERE `id`=(SELECT `empl_id` FROM `gfx` WHERE `task_id`=".$DB->F($t->getId())." LIMIT 1)").">";
                }
                $to[] = "Диспетчеры ГСС <disp@gorserv.ru>";
            }
///$this->addComment(implode(", ", $to));
            $tpl->setVariable('PLUGIN_TITLE', Plugin::getNameStatic($this->getPluginUid()));
            $tpl->setVariable('TASK_ID', $this->getId());
            $tpl->setVariable('SYS_NAME', getcfg('system.name'));
            $tpl->setVariable('TASK_HREF', Plugin::getLinkStatic($this->getPluginUid(), Plugin::getEventByIndexStatic($this->getPluginUid(), 1), "task_id=" . $this->getId()));
            $tpl->setVariable('DATE_REG', $this->getDateReg());
            $tpl->setVariable('STATUS_NAME', $this->getStatusName());
            $tpl->setVariable('STATUS_COLOR', $this->getStatusColor());
            $tpl->setVariable('AUTH_FIO', $this->getAuthorFio());
            $tpl->setVariable('ISPOLN_FIO', $this->getIspoln(', '));
            $tpl->setVariable('TASK_TITLE', $this->getTitle());
            $tpl->setVariable('TASK_COMMENT', $this->getComment());
            $tpl->setVariable('CONTR_NAME', $t->getContrName());
            $tpl->setVariable('TASK_NUMBER', $t->getClntTNum());
            $tpl->setVariable('AREA_NAME', $t->getArea());
            $tpl->setVariable('ADDRESS', $t->getAddr($t->getDomId()) . " " . $t->getKv());
            $tpl->setVariable('FIO', $t->getFio());
            $tpl->setVariable('CONTACTS', implode(" ", $t->getPhones()));
            $tpl->setVariable('DESCRIPTION', $t->getAddInfo());
            $rtpl = new HTML_Template_IT(path2('templates'));
            $rtpl->loadTemplatefile('skp_task_tech_mail.tmpl.html');
            $rtpl->setVariable('PLUGIN_TITLE', Plugin::getNameStatic($this->getPluginUid()));
            $rtpl->setVariable('TASK_ID', $this->getId());
            $rtpl->setVariable('SYS_NAME', getcfg('system.name'));
            $rtpl->setVariable('TASK_HREF', Plugin::getLinkStatic($this->getPluginUid(), Plugin::getEventByIndexStatic($this->getPluginUid(), 1), "task_id=" . $this->getId()));
            $rtpl->setVariable('DATE_REG', $this->getDateReg());
            $rtpl->setVariable('STATUS_NAME', $this->getStatusName());
            $rtpl->setVariable('STATUS_COLOR', $this->getStatusColor());
            $rtpl->setVariable('AUTH_FIO', $this->getAuthorFio());
            $rtpl->setVariable('ISPOLN_FIO', $this->getIspoln(', '));
            $rtpl->setVariable('TASK_TITLE', $this->getTitle());
            $rtpl->setVariable('TASK_COMMENT', $this->getComment());
            $rtpl->setVariable('CONTR_NAME', $t->getContrName());
            $rtpl->setVariable('TASK_NUMBER', $t->getClntTNum());
            $rtpl->setVariable('AREA_NAME', $t->getArea());
            $rtpl->setVariable('ADDRESS', $t->getAddr($t->getDomId()) . " " . $t->getKv());
            $rtpl->setVariable('DESCRIPTION', $t->getAddInfo());
            if ($t->getStatusId() == TaskStatus::STATUS_SERVICE_ENGINEER_SELECTION) {
                $rtpl->setVariable('CONTACTS', "скрыто");
                $rtpl->setVariable('FIO', "скрыто");
            } else {
                $rtpl->setVariable('FIO', $t->getFio());
                $user_id = $DB->getField("SELECT
                    us.id
                FROM
                    `task_users` AS tu
                    JOIN
                    `users` AS us ON tu.user_id=us.id
                WHERE
                    tu.ispoln=1 AND
                    tu.task_id=" . $DB->F($this->getId()) . " AND
                    us.email IS NOT NULL AND
                    us.active AND
                    us.id != " . $DB->F($USER->getId()) . "
                LIMIT 1
                ;");
                $accl = $DB->getField("SELECT `access` FROM `user_access` WHERE `plugin_uid`='hidetelnum' AND `user_id`='$user_id'");
                if (!$accl) {
                    $rtpl->setVariable('CONTACTS', implode(" ", $t->getPhones()));
                } else {
                    $rtpl->setVariable('CONTACTS', "скрыто");
                }
            }
        } else {

            if ($ttype == 16) {
                $t = new ServiceTicket($this->getId());
                $tpl = new HTML_Template_IT(path2('templates'));
                $tpl->loadTemplatefile('task_techfindhimselm.tmpl.html');
                $tpl->setVariable('TASK_TITLE', $this->getTitle());
                $tpl->setVariable('SYS_NAME', getcfg('system.name'));
                $tpl->setVariable('AREA_NAME', $t->getArea());
                $addr = $t->getAddrN();
                $tpl->setVariable('CITY', $addr["city"]);
                $tpl->setVariable('STREET', $addr["street"]);
                $tpl->setVariable('HOUSE', $addr["dom"]);
                $tpl->setVariable('POD', $t->getPod() ? $t->getPod() : "-");
                $tpl->setVariable('FLOOR', $t->getEtazh() ? $t->getEtazh() : "-");
                $tpl->setVariable('FLAT', $t->getKv() ? $t->getKv() : "-");
                $tpl->setVariable('FIO', $t->getFio());
                $tpl->setVariable('CONTACTS', implode(" ", $t->getPhones()));
                $tpl->setVariable('DESCRIPTION', $t->getAddInfo());
                $to = false;
                $to[] = "new@nbn-holding.ru";
                $to[] = "odp-msk@nbn-holding.ru";
                $to[] = "disp@gorserv.ru";
            } else {
                $tpl = new HTML_Template_IT(path2('templates'));
                $tpl->loadTemplatefile('task_mail.tmpl.html');
                switch ($this->getPluginUid()) {
                    case "projects":
                        $tpl->setVariable('TASK_TITLE', "Проект №" . $this->getId() . " - " . $this->getTitle());
                        $tpl->setVariable('TASK_COMMENT', $this->getComment());
                        break;

                    case "bills":
                        $tpl->setVariable('TASK_TITLE', "Счет №" . $this->getId() . " - " . $this->getComment());
                        $tpl->setCurrentBlock("ammount");
                        $tpl->setVariable("AMMOUNT_VAL", number_format(intval($DB->getField("SELECT `sum` FROM `bills` WHERE `task_id`=" . $DB->F($this->getId()) . ";")), 2, ".", " "));
                        $tpl->parse("ammount");
                        $bill_plat_id = $DB->getField("SELECT `plat_id` FROM `bills` WHERE `task_id`=" . $DB->F($this->getId()) . ";");
                        if ($bill_plat_id) {
                            $tpl->setCurrentBlock("payeer");
                            $tpl->setVariable("PAYEER_VAL", $DB->getField("SELECT `name` FROM `bills_plat` WHERE `id`=" . $DB->F($bill_plat_id) . ";"));
                            $tpl->parse("payeer");
                        }
                        $bill_contr_id = $DB->getField("SELECT `poluch` FROM `bills` WHERE `task_id`=" . $DB->F($this->getId()) . ";");
                        if ($bill_contr_id) {
                            if (intval($bill_contr_id)) {
                                $tpl->setCurrentBlock("supplier");
                                $tpl->setVariable("SUPPLIER_VAL", $DB->getField("SELECT `contr_title` FROM `list_contr` WHERE `id`=" . $DB->F($bill_contr_id) . ";"));
                                $tpl->parse("supplier");
                            } else {
                                $tpl->setCurrentBlock("supplier");
                                $tpl->setVariable("SUPPLIER_VAL", $bill_contr_id);
                                $tpl->parse("supplier");
                            }
                        }
                        $bill_contr_id = $DB->getField("SELECT `client` FROM `bills` WHERE `task_id`=" . $DB->F($this->getId()) . ";");
                        if ($bill_contr_id) {
                            $tpl->setCurrentBlock("client");
                            $tpl->setVariable("CLIENT_VAL", $DB->getField("SELECT `contr_title` FROM `list_contr` WHERE `id`=" . $DB->F($bill_contr_id) . ";"));
                            $tpl->parse("client");
                        }
                        $bill_agr = $DB->getField("SELECT `agr_id` FROM `bills` WHERE `task_id`=" . $DB->F($this->getId()) . ";");
                        if ($bill_agr && $bill_agr != "0")
                            $bill_agr_text = $DB->getField("SELECT concat(`number`, ' от ', DATE_FORMAT(`agr_date`, '%d-%m-%Y')) FROM `list_contr_agr` WHERE `id`=" . $DB->F($bill_agr) . ";");
                        $tpl->setCurrentBlock("agreement");
                        if ($bill_agr_text) {
                            $tpl->setVariable("AGREEMENT_VAL", $bill_agr_text);
                        } else {
                            $tpl->setVariable("AGREEMENT_VAL", "&mdash;");
                        }
                        $tpl->parse("agreement");
                        $bill_agr = $DB->getField("SELECT `client_agr` FROM `bills` WHERE `task_id`=" . $DB->F($this->getId()) . ";");
                        if ($bill_agr && $bill_agr != "0")
                            $bill_agr_text = $DB->getField("SELECT concat(`number`, ' от ', DATE_FORMAT(`agr_date`, '%d-%m-%Y')) FROM `list_contr_agr` WHERE `id`=" . $DB->F($bill_agr) . ";");
                        $tpl->setCurrentBlock("cl_agreement");
                        if ($bill_agr_text) {
                            $tpl->setVariable("CL_AGREEMENT_VAL", $bill_agr_text);
                        } else {
                            $tpl->setVariable("CL_AGREEMENT_VAL", "&mdash;");
                        }
                        $tpl->parse("cl_agreement");
                        $exp_type_id = $DB->getField("SELECT `exp_type_id` FROM `bills` WHERE `task_id`=" . $DB->F($this->getId()) . ";");
                        if ($exp_type_id) {
                            $tpl->setCurrentBlock("pay_cat");
                            $tpl->setVariable("PAYCAT_VAL", $DB->getField("SELECT `title` FROM `bill_types` WHERE `id`=" . $DB->F($exp_type_id) . ";"));
                            $tpl->parse("pay_cat");
                        }

                        $tpl->setVariable('TASK_COMMENT', $this->getComment());
                        break;

                    default:
                        $tpl->setVariable('TASK_TITLE', $this->getTitle());
                        $tpl->setVariable('TASK_COMMENT', $this->getComment());
                        break;
                }

                $tpl->setVariable('SYS_NAME', getcfg('system.name'));
                $tpl->setVariable('PLUGIN_TITLE', Plugin::getNameStatic($this->getPluginUid()));
                $tpl->setVariable('TASK_ID', $this->getId());

                $task_url = self::getTaskUrl($this);
                $tpl->setVariable('TASK_HREF', $task_url);
                $tpl->setVariable('DATE_REG', $this->getDateReg());
                $tpl->setVariable('STATUS_NAME', $this->getStatusName());
                $tpl->setVariable('STATUS_COLOR', $this->getStatusColor());
                $tpl->setVariable('AUTH_FIO', $this->getAuthorFio());
                $tpl->setVariable('ISPOLN_FIO', $this->getIspoln(', '));
            }
        }
        $to = implode(', ', $to);
        if (!$to) {
            trigger_error("No emails found for task " . $this->getId(), E_USER_NOTICE);
            return true;
        }
        $tpl->setVariable('SYS_HOST', getcfg('http_base'));
        if ($this->getCommentsCount()) {
            $tpl->setVariable('LAST_COMMENT', $this->getLastComment(false, true));
        }
        $email = wf::$container->get('wf.deprecated_mail');
        if ($ttype == 16) {
            $email->setSubject("Подключение " . $this->getId() . " - Монтажник нашел клиента сам. ");
        } else {
            switch ($this->getPluginUid()) {
                case "projects":
                    $email->setSubject('Проект №' . $this->getId() . ' - ' . $this->getTitle() . " - " . $this->getStatusName());
                    break;

                case "bills":
                    $email->setSubject('Счет №' . $this->getId() . ' - ' . $this->getComment() . " - " . $this->getStatusName());
                    break;

                default:
                    if ($title)
                        $title .= " ";
                    $email->setSubject($title . Plugin::getNameStatic($this->getPluginUid()) . ' ' . $this->getId() . ' - ' . getcfg('system.name'));

                    break;
            }
        }
        $t = new Task($this->getId());

        if ($t->getStatusId() == TaskStatus::STATUS_SERVICE_ENGINEER_SELECTION || $t->getStatusId() == TaskStatus::STATUS_SERVICE_GFX) {
            $totech = implode(', ', $totech);
            if (!$totech) {
                trigger_error("No emails found for task " . $this->getId(), E_USER_NOTICE);
                return true;
            }
            $email->setTo($totech);
            $email->setHtml($rtpl->get().'--0');
            $sql = "INSERT INTO syslog (`userid`, `userfio`, `userip`, `module`, `action`, `get`, `post`, `result`, `session_id`, `useragent`) VALUES
(" . $DB->F($USER->getId()) . ", " . $DB->F($USER->getFio()) . ", " . $DB->F(IpAddress::getIp()) . ", " . $DB->F("mail") . ", " . $DB->F("send") . ", " . $DB->F($totech) . ", " . $DB->F($rtpl->get()) . ", " . $DB->F("-") . ", " . $DB->F(session_id()) . ", " . $DB->F($_SERVER["HTTP_USER_AGENT"] . " Host: " . $protocol . $_SERVER["HTTP_HOST"]) . ");";

            $m3 = $DB->query($sql);
            $DB->free($m3);
            if (!$email->send()) {
                $err[] = "error1";
            }
            $email = wf::$container->get('wf.deprecated_mail');
            $to = array();
            $to[] = "Диспетчеры ГСС <disp@gorserv.ru>";
            if ($t->getStatusId() != TaskStatus::STATUS_SERVICE_ENGINEER_SELECTION)
                $to[] = $USER->getFio() . " <" . $USER->getEmail() . ">";

            $to = implode(', ', $to);
            if (!$to) {
                trigger_error("No emails found for task " . $this->getId(), E_USER_NOTICE);
                return true;
            }
            if ($title)
                $title .= " ";
            $email->setSubject($title . Plugin::getNameStatic($this->getPluginUid()) . ' ' . $this->getId() . ' - ' . getcfg('system.name'));

            $to1 = explode(",", $to);
            if (count($to1) > 34) {
                $i = 0;
                foreach ($to1 as $item) {
                    if ($i < 34) {
                        $to2 .= $item . ",";
                        $i += 1;
                    } else {
                        $i = 0;
                        $to2 = preg_replace("/,$/", "", $to2);
                        $email->setTo($to2);
                        $email->setHtml($tpl->get().'--1');
                        $sql = "INSERT INTO syslog (`userid`, `userfio`, `userip`, `module`, `action`, `get`, `post`, `result`, `session_id`, `useragent`) VALUES
(" . $DB->F($USER->getId()) . ", " . $DB->F($USER->getFio()) . ", " . $DB->F(IpAddress::getIp()) . ", " . $DB->F("mail") . ", " . $DB->F("send") . ", " . $DB->F($to2) . ", " . $DB->F($tpl->get()) . ", " . $DB->F("-") . ", " . $DB->F(session_id()) . ", " . $DB->F($_SERVER["HTTP_USER_AGENT"] . " Host: " . $protocol . $_SERVER["HTTP_HOST"]) . ");";

                        $m3 = $DB->query($sql);
                        $DB->free($m3);
                        if (!$email->send())
                            $err[] = "Ошибка отправки";
                    }
                }
                if (sizeof($err)) {
                    return false;
                } else {
                    return true;
                }
            } else {
                $email->setTo($to);
                $email->setHtml($tpl->get().'--2');
                $sql = "INSERT INTO syslog (`userid`, `userfio`, `userip`, `module`, `action`, `get`, `post`, `result`, `session_id`, `useragent`,`actiondate`) VALUES
                         (" . $DB->F($USER->getId()) . ", " . $DB->F($USER->getFio()) . ", " . $DB->F(IpAddress::getIp()) . ", " . $DB->F("mail") . ", " . $DB->F("send") . ", " . $DB->F($to) . ", " . $DB->F($tpl->get()) . ", " . $DB->F("-") . ", " . $DB->F(session_id()) . ", " . $DB->F($_SERVER["HTTP_USER_AGENT"] . " Host: " . $protocol . $_SERVER["HTTP_HOST"]) . ", now());";
                $m3 = $DB->query($sql);
                $DB->free($m3);
                return $email->send();
            }


        } else {
            if ($t->getStatusId() == TaskStatus::STATUS_SERVICE_SCHEDULED) {
                $us = $DB->getRow("SELECT user.fio, user.email FROM `users` WHERE `id` IN (SELECT `user_id` FROM `task_users` WHERE `ispoln` AND task_id=" . $DB->F($this->getId()) . " LIMIT 1);", true);
                if ($us["fio"] != "" && $us["email"] != "") {
                    $to .= ", " . $us["fio"] . "<" . $us["email"] . ">";
                }
            }
//die($to);
            $to1 = explode(",", $to);
            if (count($to1) > 34) {
                $i = 0;
                foreach ($to1 as $item) {
                    if ($i < 34) {
                        $to2 .= $item . ",";
                        $i += 1;
                    } else {
                        $i = 0;
                        $to2 = preg_replace("/,$/", "", $to2);
                        $email->setTo($to2);
                        $email->setHtml($tpl->get().'--3');
                        $sql = "INSERT INTO syslog (`userid`, `userfio`, `userip`, `module`, `action`, `get`, `post`, `result`, `session_id`, `useragent`, `actiondate`) VALUES
(" . $DB->F($USER->getId()) . ", " . $DB->F($USER->getFio()) . ", " . $DB->F(IpAddress::getIp()) . ", " . $DB->F("mail") . ", " . $DB->F("send") . ", " . $DB->F($to2) . ", " . $DB->F($tpl->get()) . ", " . $DB->F("-") . ", " . $DB->F(session_id()) . ", " . $DB->F($_SERVER["HTTP_USER_AGENT"] . " Host: " . $protocol . $_SERVER["HTTP_HOST"]) . ", now());";

                        $m3 = $DB->query($sql);
                        $DB->free($m3);
                        if (!$email->send())
                            $err[] = "Ошибка отправки";
                    }
                }
                if (sizeof($err)) {
                    return false;
                } else {
                    return true;
                }
            } else {
                $email->setTo($to);
                $sql = "INSERT INTO syslog (`userid`, `userfio`, `userip`, `module`, `action`, `get`, `post`, `result`, `session_id`, `useragent`, `actiondate`) VALUES
(" . $DB->F($USER->getId()) . ", " . $DB->F($USER->getFio()) . ", " . $DB->F(IpAddress::getIp()) . ", " . $DB->F("mail") . ", " . $DB->F("send") . ", " . $DB->F($to) . ", " . $DB->F($tpl->get()) . ", " . $DB->F("-") . ", " . $DB->F(session_id()) . ", " . $DB->F($_SERVER["HTTP_USER_AGENT"] . " Host: " . $protocol . $_SERVER["HTTP_HOST"]) . ", now());";

                $m3 = $DB->query($sql);
                $DB->free($m3);
                $email->setHtml($tpl->get().'--4');
                return $email->send();
            }
            return true;
        }
    }

    function getDateReg($format = "d.m.Y H:i:s")
    {
        return rudate($format, $this->task['date_reg']);
    }

    /**
     * Task::getStatusName()
     * получить название текущего статуса
     *
     * @return string
     */
    function getStatusName()
    {
        return $this->status['name'];
    }

    function getAuthorFio($escape = true)
    {
        global $DB;
        return $DB->getField("SELECT `fio` FROM `users` WHERE `id`=" . $DB->F($this->getAuthorId()), $escape);
    }

    function getAuthorId()
    {
        return $this->task['author_id'];
    }

    function getTitle()
    {
        return $this->task['task_title'];
    }

    function getComment($nl2br = true)
    {
        return $nl2br ? nl2br($this->task['task_comment']) : $this->task['task_comment'];
    }

    /**
     * Returns task url
     * @return string
     */
    static public function getTaskUrl(Task $task)
    {
        switch ($task->getPluginUid()) {
            case \models\Task::PROJECT:
                $vt = "viewtask";
                break;
            case \models\Task::BILL:
                $vt = "viewbill";
                break;
            default:
                $vt = "viewticket";
                break;
        }

        return \wf::$container->get('router')->getGenerator()
            ->generate('legacy_rule_default', [
                'plugin_uid' => $task->getPluginUid(),
                'plugin_event' => $vt,
                'task_id' => $task->getId(),
            ], UrlGeneratorInterface::ABSOLUTE_URL);

    }

    /**
     * Task::getCommentsCount()
     * получить количество комментариев к задаче
     *
     * @return integer
     */
    function getCommentsCount()
    {
        global $DB;
        return (int)$DB->getField("SELECT COUNT(*) FROM `task_comments` WHERE `task_id`=" . $DB->F($this->getId()));
    }

    /**
     * Task::getLastComment()
     * получить последний комментарий к задаче
     *
     * @return HTML
     *
     * @see templates/last_comment.tmpl.html
     */
    function getLastComment($tooltip = false, $as_email_template = false)
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("templates"));
        $tpl->loadTemplatefile("last_comment.tmpl.html");

        $sql = "SELECT cmm.id, cmm.datetime, cmm.text, cmm.tag, us.id, us.fio, cmm.status_id FROM `task_comments` AS cmm LEFT JOIN `users` AS us ON cmm.user_id=us.id WHERE cmm.task_id=" . $DB->F($this->getId()) . " ORDER BY cmm.id DESC LIMIT 1";
        $DB->query($sql);
        if ($DB->num_rows() < 1) {
            $DB->free();
            $sql = "SELECT 0, ts.date_reg, IFNULL(ts.task_comment, ts.task_title), NULL, ts.author_id, us.fio, '' FROM `tasks` AS ts LEFT JOIN `users` AS us ON ts.author_id=us.id WHERE ts.id=" . $DB->F($this->getId());
            $DB->query($sql);
        }
        list($cmm_id, $cmm_date, $cmm_text, $tag, $user_id, $user_fio, $status_id) = $DB->fetch(false, false);
        $DB->free();
        if ($tooltip) {
            return "Дата: " . $cmm_date . " \r\nАвтор: " . htmlspecialchars($user_fio ? $user_fio : "СИСТЕМА") . " \r\nКомментарий: " . $cmm_text . "\r\n";
        } else {
            if ($USER->getTemplate() == "default")
                $tpl->setVariable('CMM_COLOR', $this->getStatusColor());
            else {
                if ($status_id) {
                    $color = adm_statuses_plugin::getStatusColorByID($status_id);
                    $tpl->setVariable('CMM_COLOR', ($color ? $color : $this->getStatusColor()));
                } else
                    $tpl->setVariable('CMM_COLOR', $this->getStatusColor());
            }
            $tpl->setVariable('CMM_DATE', rudate("d.m.Y H:i", $cmm_date));

            $editcomment = '';
            if (!$as_email_template)
                $editcomment = ($this->getPluginUid() == "projects" && $user_id == $USER->getId()) ? "&nbsp;&nbsp;<a href='#' id='" . $cmm_id . "' style='float:right;' class='editcomment'><img src='/templates/images/pencil.png' /></a>" : "";
            else {
                $tpl->touchBlock('as_mail');
                $tpl->setVariable('TASK_URL', self::getTaskUrl($this));
            }

            $tpl->setVariable('CMM_USER_FIO', htmlspecialchars($user_fio ? $user_fio : "Система") . $editcomment);
            $tpl->setVariable('CMM_USER_FIO_ONLY', htmlspecialchars($user_fio ? $user_fio : "Система"));
            $tpl->setVariable('CMM_ID', $cmm_id);

            if ($this->getPluginUid() == "accidents" ||
                $this->getPluginUid() == "connections" ||
                $this->getPluginUid() == "projects" ||
                $this->getPluginUid() == "bills" ||
                $this->getPluginUid() == "services" ||
                $this->getPluginUid() == "gp"
            ) {

                $comment = new TextObject($cmm_text);
                $commentContext = new CommentsProcessor();
                $commentContext->purify($comment);
                $tpl->setVariable('CMM_TEXT', $comment->getText());
            } else {
                $tpl->setVariable('CMM_TEXT', nl2br(htmlspecialchars($cmm_text)));
            }
            if ($tag) {
                $tpl->setCurrentBlock('tag');
                $tag = new TextObject($tag);
                if ($as_email_template) {
                    $context = new TextLink2AbsoluteDecorator($tag);
                    $context->process();
                }
                $tpl->setVariable('CMM_TAG', $tag->getText());
                $tpl->parseCurrentBlock();
            }

            return $tpl->get();
        }
    }

    /**
     * Task::setDateFn()
     *
     * @param mixed $date - дата (yyyy-mm-dd или unix timestamp) или пусто для обнуления
     *
     * @return новое значение
     */
    function setDateFn($date = '')
    {
        global $DB;

        if ($date != "") {
            $date = date("Y-m-d H:i:s", strtotime($date));
        }

        $sql = "UPDATE `tasks` SET `date_fn`=" . $DB->F($date, true) . " WHERE `id`=" . $DB->F($this->getId());
        $DB->query($sql);

        if (!$DB->errno()) {
            $this->task['date_fn'] = $date;
        }

        return $this->task['date_fn'];
    }

}
