<?php
namespace classes;

class Storage extends Component
{

    /** @var  DBMySQL */
    private $db;

    public $tableName = '`key_storage_item`';

    public function init()
    {
        global $DB;
        $this->db = $DB;
    }

    public function has($key)
    {
        if (defined('ENV') && ENV != 'prod')
            return false;

        $query = '
        select
            count(*)
        from
            ' . $this->tableName . '
        where
            `key` = ' . $this->db->F($key) . '
        ;';

        return $this->db->getField($query) > 0 ?: false;
    }

    public function del($key)
    {
        $query = '
            delete from ' . $this->tableName . '
            where
                `key` = ' . $this->db->F($key) . '
            ;';

        return $this->db->query($query);
    }

    public function delLike($key)
    {
        $query = '
            delete from ' . $this->tableName . '
            where
                `key` like ' . $this->db->F('%' . $key . '%') . '
            ;';

        return $this->db->query($query);
    }

    /**
     * @param string $key
     * @param string $value
     * @param null $comment
     * @return bool
     * @throws \ErrorException
     */
    public function set($key, $value, $comment = null)
    {
        if (defined('ENV') && ENV != 'prod')
            return false;

        if (empty($key) || empty($value)) {
            throw new \ErrorException('Empty key-value not allowed!');
        }

        if (empty($comment)) {
            $comment = null;
        }

        if ($this->has($key)) {
            $query = '
            update ' . $this->tableName . ' set
                `value` = "' . serialize($value) . '",
                `comment` = ' . $this->db->F($comment, true) . ',
                `updated_at` = ' . time() . '
            where
                `key` = ' . $this->db->F($key) . '
            ;';
        } else {
            $query = '
            insert into ' . $this->tableName . ' set
                `key` = ' . $this->db->F($key) . ',
                `value` = "' . base64_encode($value) . '",
                `comment` = ' . $this->db->F($comment, true) . ',
                `created_at` = ' . time() . ',
                `updated_at` = null
            ;';
        }

        return $this->db->query($query);

    }

    public function get($key, $onlyValue = true)
    {
        if ($onlyValue) {
            $query = '
            select
                `value`
            from
                ' . $this->tableName . '
            where
                `key` = ' . $this->db->F($key) . '
            ;';

            return base64_decode($this->db->getField($query, false), true);
        }

        $query = '
            select
                *
            from
                ' . $this->tableName . '
            where
                `key` = ' . $this->db->F($key) . '
            ;';

        $row = $this->db->getRow($query, true, false);
        $row['value'] = base64_decode($row['value']);

        return $row;
    }

    public function getLike($key)
    {
        $query = '
            select /* key value has */
                *
            from
                ' . $this->tableName . '
            where
                `key` like ' . $this->db->F('%' . $key . '%') . '
            ;';

        $result = $this->db->getAll($query, true);
        foreach ($result as $i => $row) {
            $result[$i]['value'] = base64_decode($row['value']);
        }

        return $result;
    }

}