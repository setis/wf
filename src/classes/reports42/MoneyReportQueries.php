<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 07.12.2015
 */

namespace classes\reports42;

use DateTime;
use models\Agreement;
use models\TaskStatus;

class MoneyReportQueries
{
    /**
     * Все заявки к начисление для техника за отчётный период
     *
     * @param integer[] $users
     * @param $period
     * @param bool $separate
     * @return array
     */
    public static function getTechMonthClosedTasks(array $users, $period, $separate = false)
    {
        list($year, $month) = explode('-', $period);

        $skpCashTasks = self::collectSkpCashTasks($users, $year, $month);
        $skpCashlessTasks = self::collectSkpCashlessTasks($users, $year, $month);
        $accidentsTasks = self::collectAccidentsTasks($users, $year, $month);
        $connectionsTasks = self::collectConnectionTasks($users, $year, $month);
        $gpTasks = self::collectGpTasks($users, $year, $month);

        if ($separate) {
            return array($skpCashTasks, $skpCashlessTasks, $accidentsTasks, $connectionsTasks, $gpTasks);
        }

        return array_merge($skpCashTasks, $skpCashlessTasks, $accidentsTasks, $connectionsTasks, $gpTasks);
    }

    /**
     * Сбор закрытых заявок СКП(нал)
     * у заявки статус СКП.Принято в кассу
     * деньги получены только в отчётном периоде
     * ответственный по заявке получает деньги
     *
     * @param array $user_id
     * @param int $year
     * @param int $month
     * @return array
     */
    public static function collectSkpCashTasks($user_id, $year, $month)
    {
        global $DB;

        if (!is_array($user_id) && !empty($user_id))
            $user_id = array($user_id);

        $start = new DateTime("{$year}-{$month}-01");
        $end = (clone $start)->modify('+1 month');

        $skpMoneyReceivedStatus = array(
            49, // СКП.Принято в кассу
        );

        $query = 'SELECT /* Сбор выполненых заявок СКП (наличные) */
            tasks.*
        FROM
            tasks 
            LEFT JOIN 
            gfx ON tasks.id = gfx.task_id
            LEFT JOIN 
            tickets ON tasks.id = tickets.task_id
            LEFT JOIN
            list_contr_agr ON (list_contr_agr.id = tickets.agr_id)
        WHERE
            EXISTS (
              SELECT 
                * 
              FROM 
                  task_comments 
              WHERE 
                  task_comments.task_id = tasks.id 
                  AND task_comments.status_id IN ( ' . implode(', ', $skpMoneyReceivedStatus) . ' )
                  AND date(task_comments.datetime) >= "' . $start->format('Y-m-d') . '" AND date(task_comments.datetime) < "' . $end->format('Y-m-d') . '"
            )
            AND gfx.empl_id IN (' . implode(', ', $user_id) . ')
            AND list_contr_agr.paytype = ' . Agreement::CASH . '
        GROUP BY
            tasks.id
        ORDER BY
            tasks.date_reg;
            ';

        $tasks = $DB->getAll($query, true, true, -1);

        if (empty($tasks) || !is_array($tasks))
            return array();

        return $tasks;
    }

    /**
     * Сбор закрытых заявок СКП (безнал)
     * ответственный по заявке получает деньги
     * акт выставлен в отчётный период
     *
     * @param array $user_id
     * @param int $year
     * @param int $month
     * @return array
     */
    public static function collectSkpCashlessTasks($user_id, $year, $month)
    {

        global $DB;

        if (!is_array($user_id) && !empty($user_id))
            $user_id = array($user_id);

        $start = new DateTime("{$year}-{$month}-01");
        $end = (clone $start)->modify('+1 month');

        $query = 'SELECT /* Сбор выполненых заявок СКП (безнал) */
            tasks.*
        FROM
            tasks
            LEFT JOIN
            gfx ON tasks.id = gfx.task_id
            left join 
            tickets ON (tickets.task_id = tasks.id)
            LEFT JOIN
            list_contr_agr ON (list_contr_agr.id = tickets.agr_id)
            LEFT JOIN 
            list_tskpdata_reports on tickets.tdataactid = list_tskpdata_reports.id
        WHERE
            gfx.empl_id IN (' . implode(', ', $user_id) . ')
            /* акт выставлен в отчётный период */
            and list_tskpdata_reports.send_date >= \''.$start->format('Y-m-d').'\' and list_tskpdata_reports.send_date < \''. $end->format('Y-m-d') .'\'
            /* расчёт контрагента только безналом */
             AND list_contr_agr.paytype = '. Agreement::CASHLESS .'
        GROUP BY
            tasks.id
        ORDER BY
            tasks.date_reg;';

        $tasks = $DB->getAll($query, true, true, -1);

        if (empty($tasks) || !is_array($tasks))
            return array();

        return $tasks;
    }

    /**
     * Сбор закрытых заявок ТТ
     * Выплнено случилось с 1 до 31
     * Закрыто случилось с 1 до 31+dc
     * деньги получает техник на которого расчёт
     *
     * @param array $user_id
     * @param int $year
     * @param int $month
     * @return array
     */
    public static function collectAccidentsTasks($user_id, $year, $month)
    {

        global $DB;

        if (!is_array($user_id) && !empty($user_id))
            $user_id = array($user_id);

        $prev_dc_param = getcfg(self::getDcParam($month - 1 > 0 ? $month - 1 : 12));
        $dc_param = getcfg(self::getDcParam($month));

        $period = new DateTime("{$year}-{$month}-01");

        $period_start_date = (clone $period)->modify('midnight first day of this month');
        $period_end_date = (clone $period_start_date)->modify('midnight first day of next month -1 second');
        $period_end_date_with_dc = (clone $period_end_date)->modify("+ {$dc_param} days");

        $closedStatus = array(
            54, // ТТ.Закрыто
        );

        $doneStatus = array(
            61, // ТТ.Выполнено
        );

        $query = "SELECT /* Сбор выполненых заявок ТТ */
            tasks.*
        FROM
            tasks
            LEFT JOIN
            list_tt_tech_calc ON (tasks.id = list_tt_tech_calc.task_id)
        WHERE
            exists ( select 
                        * 
                    from 
                        task_comments
                    where
                        task_comments.task_id = tasks.id and
                        task_comments.status_id IN (" . implode(', ', $doneStatus) . ") AND
                        task_comments.datetime BETWEEN '" . $period_start_date->format(DATE_ATOM) . "' 
                            AND '" . $period_end_date->format(DATE_ATOM) . "'
                    ) AND
            exists ( select 
                        * 
                    from 
                        task_comments 
                    where
                        task_comments.task_id = tasks.id and
                        task_comments.status_id IN (" . implode(', ', $closedStatus) . ") AND
                        task_comments.datetime BETWEEN '" . $period_start_date->format(DATE_ATOM) . "' 
                        AND '" . $period_end_date_with_dc->format(DATE_ATOM) . "'
                )
            /* расчёт на искомого техника!!! */
            " . (!empty($user_id) ? 'AND list_tt_tech_calc.tech_id in (' . implode(', ', $user_id) . ') ' : '') . "
        GROUP BY
            tasks.id
        ORDER BY
            tasks.date_reg
        ;";
        $tasks = $DB->getAll($query, true, true, -1);

        if (empty($tasks) || !is_array($tasks))
            return array();

        return $tasks;
    }

    /**
     * Возвращает нзвание индекса для расчёта сдвига даты окончанию закрытия заявок
     * диспетческой службой
     *
     * @param integer $month
     * @return string
     */
    public static function getDcParam($month)
    {
        if ($month == 12) {
            $dc_param = "connfinreport_dc_january";
        } elseif ($month == 4)
            $dc_param = "connfinreport_dc_may";
        else
            $dc_param = "connfinreport_dc";

        return $dc_param;
    }

    /**
     * Сбор закрытых заявок Подключение
     * Попало в акт отчётный период
     * деньги получает техник на которого расчёт
     * у заявки был статус Подключение.Закрыто
     *
     * @param array $user_id
     * @param int $year
     * @param int $month
     * @return array
     */
    public static function collectConnectionTasks($user_id, $year, $month)
    {
        global $DB;

        if (!is_array($user_id) && !empty($user_id))
            $user_id = array($user_id);

        $dc_param = getcfg(self::getDcParam($month));

        $period = new DateTime("{$year}-{$month}-01");

        $period_start_date = (clone $period)->modify('midnight first day of this month');
        $period_end_date = (clone $period_start_date)->modify('midnight first day of next month -1 second');
        $period_end_date_with_dc = (clone $period_end_date)->modify("+ {$dc_param} days");

        $closedStatus = array(
            TaskStatus::STATUS_CONNECTION_CLOSED // Подключение.Закрыто
        );

        $doneStatus = array(
            TaskStatus::STATUS_CONNECTION_DONE, // Подключение.Выполнено
        );

        $query = "SELECT /* Сбор выполненых заявок Подключение */
            tasks.*
        FROM
            tasks
            LEFT JOIN
            list_tech_calc ON (tasks.id = list_tech_calc.task_id)
        WHERE
            exists ( select 
                        *
                    from 
                        task_comments
                    where
                        task_comments.task_id = tasks.id and
                        task_comments.status_id IN (" . implode(', ', $doneStatus) . ") AND
                        task_comments.datetime BETWEEN '" . $period_start_date->format(DATE_ATOM) . "' 
                            AND '" . $period_end_date->format(DATE_ATOM) . "'
            ) AND
            exists ( select 
                        * 
                    from 
                        task_comments 
                    where
                        task_comments.task_id = tasks.id and
                        task_comments.status_id IN (" . implode(', ', $closedStatus) . ") AND
                        task_comments.datetime BETWEEN '" . $period_start_date->format(DATE_ATOM) . "' 
                        AND '" . $period_end_date_with_dc->format(DATE_ATOM) . "'
            )
            /* расчёт на искомого техника!!! */
            " . (!empty($user_id) ? 'AND list_tech_calc.tech_id in (' . implode(', ', $user_id) . ') ' : '') . "
        GROUP BY
            tasks.id
        ORDER BY
            tasks.date_reg
        ;";

        $tasks = $DB->getAll($query, true, true, -1);
        if (empty($tasks) || !is_array($tasks))
            return array();

        return $tasks;
    }

    /**
     * @param $user_id
     * @param $year
     * @param $month
     * @return array
     */
    public static function collectGpTasks($user_id, $year, $month)
    {
        /**
         * TODO Artem Implement this....
         */
        return array();
    }

    /**
     * Доначисления по заявкам в прошлых периодах
     *
     * @param integer[] $users
     * @param $period
     * @param bool $separate
     * @return array
     */
    public static function getTechAssessedAdditionalTasks(array $users, $period, $separate = false)
    {
        list($year, $month) = explode('-', $period);

        $skpCashTasks = array();
        $skpCashlessTasks = array();
        $accidentsTasks = self::collectAssessedAccidentsTasks($users, $year, $month);
        $connectionsTasks = self::collectAssessedConnectionTasks($users, $year, $month);
        $gpTasks = array();

        if ($separate) {
            return array($skpCashTasks, $skpCashlessTasks, $accidentsTasks, $connectionsTasks, $gpTasks);
        }

        return array_merge($skpCashTasks, $skpCashlessTasks, $accidentsTasks, $connectionsTasks, $gpTasks);
    }

    public static function collectAssessedAccidentsTasks($user_id, $year, $month)
    {

        global $DB;

        if (!is_array($user_id) && !empty($user_id))
            $user_id = array($user_id);

        $dc_param = self::getDcParam($month);

        $period = "$year-$month-01 00:00:00";
        $period_timestamp = strtotime($period);
        /** @var string $period_start_date Начало отчётного периода */
        $period_start_date = date("Y-m-d 00:00:00", $period_timestamp);
        /** @var string $period_start_date Начало отчётного периода */
        $period_start_date_with_dc = date("Y-m-d 00:00:00", strtotime($period . " + " . getcfg($dc_param) . " days"));
        /** @var string $period_end_date Окончание отчётного периода */
        $period_end_date = date("Y-m-d 23:59:59", strtotime($period . " + 1 month - 1 day"));
        /** @var string $period_end_date_with_dc Окончание отчётного периода с гандикапом */
        $period_end_date_with_dc = date("Y-m-d 23:59:59", strtotime($period . " + 1 month + " . getcfg($dc_param) . " days"));

        $closedStatus = array(
            54, // ТТ.Закрыто
        );

        $doneStatus = array(
            61, // ТТ.Выполнено
        );

        $query = "SELECT /* Сбор выполненых заявок ТТ */
            tasks.*
        FROM
            task_comments AS tc1
            LEFT JOIN
            tasks AS t ON (t.id = tc1.task_id),
            task_comments AS tc2
            LEFT JOIN
            tasks ON (tasks.id = tc2.task_id)
            LEFT JOIN
            task_users ON (task_users.task_id = tasks.id)
            LEFT JOIN
            list_tt_tech_calc ON (tasks.id = list_tt_tech_calc.task_id)
        WHERE
            tasks.id = t.id AND
            (
                (
                    /* Выплнено случилось до начала периода */
                    tc1.status_id IN (" . implode(', ', $doneStatus) . ") AND
                    tc1.datetime < " . $DB->F($period_start_date) . "
                ) AND
                (
                    /* и Закрыто случилось до начала периода + dc */
                    tc2.status_id IN (" . implode(', ', $closedStatus) . ") AND
                    tc2.datetime < " . $DB->F($period_start_date_with_dc) . "
                )
            ) AND

            /* а в расчёт добавили в отчётный период, */
            list_tt_tech_calc.update_date BETWEEN " . $DB->F($period_start_date_with_dc) . " AND " . $DB->F($period_end_date_with_dc) . " AND
            /* но уже после заполнения отчёта */
            DATE_FORMAT(list_tt_tech_calc.update_date, '%Y-%m-%d') > DATE_FORMAT(list_tt_tech_calc.calc_date, '%Y-%m-%d') AND
            /* это ж доначисление, а не штраф */
            list_tt_tech_calc.price > 0

            /* расчёт на искомого техника!!! */
            " . (!empty($user_id) ? 'AND list_tt_tech_calc.tech_id in (' . implode(', ', $user_id) . ') ' : '') . "
        GROUP BY
            tasks.id
        ORDER BY
            tasks.date_reg
        ;";
        $tasks = $DB->getAll($query, true, true, -1);

        if (empty($tasks) || !is_array($tasks))
            return array();

        return $tasks;
    }

    public static function collectAssessedConnectionTasks($user_id, $year, $month)
    {

        global $DB;

        if (!is_array($user_id) && !empty($user_id))
            $user_id = array($user_id);

        $dc_param = self::getDcParam($month);

        $period = "$year-$month-01 00:00:00";
        $period_timestamp = strtotime($period);
        /** @var string $period_start_date Начало отчётного периода */
        $period_start_date = date("Y-m-d 00:00:00", $period_timestamp);
        /** @var string $period_start_date Начало отчётного периода + dc */
        $period_start_date_with_dc = date("Y-m-d 00:00:00", strtotime($period . " + " . getcfg($dc_param) . " days"));
        /** @var string $period_end_date_with_dc Окончание отчётного периода с гандикапом */
        $period_end_date_with_dc = date("Y-m-d 23:59:59", strtotime($period . " + 1 month + " . getcfg($dc_param) . " days"));

        $closedStatus = array(
            26 // Подключение.Закрыто
        );

        $doneStatus = array(
            1, // Подключение.Выполнено
        );

        $query = "SELECT /* Сбор выполненых заявок ТТ */
            tasks.*
        FROM
            task_comments AS tc1
            LEFT JOIN
            tasks AS t ON (t.id = tc1.task_id),
            task_comments AS tc2
            LEFT JOIN
            tasks ON (tasks.id = tc2.task_id)
            LEFT JOIN
            task_users ON (task_users.task_id = tasks.id)
            LEFT JOIN
            list_tech_calc ON (tasks.id = list_tech_calc.task_id)
        WHERE
            tasks.id = t.id AND
            (
                (
                    /* Выплнено случилось до начала периода */
                    tc1.status_id IN (" . implode(', ', $doneStatus) . ") AND
                    tc1.datetime < " . $DB->F($period_start_date) . "
                ) AND
                (
                    /* и Закрыто случилось до начала периода + dc */
                    tc2.status_id IN (" . implode(', ', $closedStatus) . ") AND
                    tc2.datetime < " . $DB->F($period_start_date_with_dc) . "
                )
            ) AND

            /* а в расчёт добавили в отчётный период, */
            list_tech_calc.update_date BETWEEN " . $DB->F($period_start_date_with_dc) . " AND " . $DB->F($period_end_date_with_dc) . " AND
            /* но уже после заполнения отчёта */
            DATE_FORMAT(list_tech_calc.update_date, '%Y-%m-%d') > DATE_FORMAT(list_tech_calc.calc_date, '%Y-%m-%d') AND
            /* это ж доначисление, а не штраф */
            list_tech_calc.price > 0

            /* расчёт на искомого техника!!! */
            " . (!empty($user_id) ? 'AND list_tech_calc.tech_id in (' . implode(', ', $user_id) . ') ' : '') . "
        GROUP BY
            tasks.id
        ORDER BY
            tasks.date_reg
        ;";
        $tasks = $DB->getAll($query, true, true, -1);

        if (empty($tasks) || !is_array($tasks))
            return array();

        return $tasks;
    }

    /**
     * Все выполненные заявки, могут быть и без начислений
     *
     * @param integer[] $users
     * @param $period
     * @param bool $separate
     * @return array
     */
    public static function getTechMonthSkpDoneTasks(array $users, $period, $separate = false)
    {
        list($year, $month) = explode('-', $period);

        $skpCashTasks = self::collectSkpCashDoneTasks($users, $year, $month);
        $skpCashlessTasks = self::collectSkpCashlessDoneTasks($users, $year, $month);

        if ($separate) {
            return array($skpCashTasks, $skpCashlessTasks);
        }

        return array_merge($skpCashTasks, $skpCashlessTasks);
    }

    /**
     * Сбор выполненых заявок СКП(нал)
     * у заявки статус СКП.Выполнено в кассу только в отчётном периоде
     *
     * @param array $user_id
     * @param int $year
     * @param int $month
     * @return array
     */
    public static function collectSkpCashDoneTasks($user_id, $year, $month)
    {
        global $DB;

        if (!is_array($user_id) && !empty($user_id))
            $user_id = array($user_id);

        $skpDoneStatus = array(
            12, // СКП.Выполнено
        );

        $query = 'SELECT /* Сбор выполненых заявок СКП (наличные) */
            tasks.*
        FROM
            task_comments
            LEFT JOIN
            task_users ON (task_users.task_id = task_comments.task_id)
            LEFT JOIN
            tickets ON (tickets.task_id = task_comments.task_id)
            LEFT JOIN
            list_contr_agr ON (list_contr_agr.id = tickets.agr_id)
            LEFT JOIN
            tasks ON (task_comments.task_id = tasks.id)
        WHERE
            task_comments.status_id IN (' . implode(', ', $skpDoneStatus) . ') AND
            /* деньги получены только в отчётном периоде */
            ( YEAR(task_comments.datetime) = ' . $DB->F($year) . ' AND MONTH(task_comments.datetime) = ' . $DB->F($month) . ') AND
            ' . (!empty($user_id) ? 'task_users.user_id IN (' . implode(', ', $user_id) . ') AND task_users.ispoln = 1 AND ' : '') . '
            /* расчёт контрагента налом */
            (list_contr_agr.paytype = 1 OR list_contr_agr.paytype = "" OR list_contr_agr.paytype IS NULL)
        GROUP BY
            tasks.id
        ORDER BY
            tasks.date_reg;';

        $tasks = $DB->getAll($query, true, true, -1);

        if (empty($tasks) || !is_array($tasks))
            return array();

        return $tasks;
    }

    /**
     * Сбор закрытых заявок СКП (безнал)
     * ответственный по заявке получает деньги
     * акт выставлен в отчётный период
     *
     * @param array $user_id
     * @param int $year
     * @param int $month
     * @return array
     */
    public static function collectSkpCashlessDoneTasks($user_id, $year, $month)
    {

        global $DB;

        if (!is_array($user_id) && !empty($user_id))
            $user_id = array($user_id);

        $skpDoneStatus = array(
            12, // СКП.Выполнено
        );

        $query = 'SELECT /* Сбор выполненых заявок СКП (безнал) */
            tasks.*
        FROM
            tasks
            LEFT JOIN
            tickets ON(tickets.task_id = tasks.id)
            LEFT JOIN
            task_comments ON(task_comments.task_id = tasks.id)
            LEFT JOIN
            task_users ON(task_users.task_id = tasks.id)
            LEFT JOIN
            list_contr_agr ON(list_contr_agr.id = tickets.agr_id)
        WHERE
            task_comments.status_id IN (' . implode(', ', $skpDoneStatus) . ') AND
            /* деньги получены только в отчётном периоде */
            ( YEAR(task_comments.datetime) = ' . $DB->F($year) . ' AND MONTH(task_comments.datetime) = ' . $DB->F($month) . ') AND
            ' . (!empty($user_id) ? 'task_users.user_id IN (' . implode(', ', $user_id) . ') AND task_users.ispoln = 1 AND ' : '') . '
            /* расчёт контрагента только безналом */
            list_contr_agr.paytype = 2
        GROUP BY
            tasks.id
        ORDER BY
            tasks.date_reg;';
//        d($query);
        $tasks = $DB->getAll($query, true, true, -1);

        if (empty($tasks) || !is_array($tasks))
            return array();

        return $tasks;
    }

    /**
     * Штрафы сотрудника за период
     *
     * @param array|integer $user_id
     * @param string $period Отчётный месяц "Y-m"
     * @param bool $separate
     * @return array
     */
    public static function getTechMonthPenalty($user_id, $year, $month, $separate = false)
    {
        global $DB;

        if (!is_array($user_id) && !empty($user_id))
            $user_id = array($user_id);

        $skpCashPenalties = array();
        $skpCashlessPenalties = array();
        $gpPenalties = array();

        $prev_dc_param = self::getDcParam($month - 1 > 0 ? $month - 1 : 12);
        $dc_param = self::getDcParam($month);

        $period = "$year-$month-01 00:00:00";
        /** @var string $period_start_date_with_dc Начало отчётного периода с гандикапом */
        $period_start_date_with_dc = date("Y-m-d 23:59:59", strtotime($period . " + " . getcfg($prev_dc_param) . " days"));
        /** @var string $period_end_date_with_dc Окончание отчётного периода с гандикапом */
        $period_end_date_with_dc = date("Y-m-d 23:59:59", strtotime($period . " + 1 month + " . getcfg($dc_param) . " days"));

        $query = '
        SELECT /* Штрафы за Подключения */
            task_id, sum(price * quant) AS price
        FROM
            list_tech_calc
        WHERE
            /* штраф добавлен в отчётный период */
            list_tech_calc.update_date BETWEEN ' . $DB->F($period_start_date_with_dc) . ' AND ' . $DB->F($period_end_date_with_dc) . '
            AND
            /* расчёт на искомого техника!!! */
            list_tech_calc.tech_id IN (' . implode(', ', $user_id) . ') AND
            /* цена меньше нуля, это ж штраф */
            list_tech_calc.price < 0
        GROUP BY
            list_tech_calc.task_id
        ORDER BY
            list_tech_calc.task_id
        ;';
        $connectionsPenalties = $DB->getAll($query, true, true, -1);

        $query = '
        SELECT /* Штрафы за ТТ */
            task_id, sum(price * quant) AS price
        FROM
            list_tt_tech_calc
        WHERE
            /* штраф добавлен в отчётный период */
            list_tt_tech_calc.update_date BETWEEN ' . $DB->F($period_start_date_with_dc) . ' AND ' . $DB->F($period_end_date_with_dc) . ' AND
            /* расчёт на искомого техника!!! */
            list_tt_tech_calc.tech_id IN (' . implode(', ', $user_id) . ') AND
            /* цена меньше нуля, это ж штраф */
            list_tt_tech_calc.price < 0
        GROUP BY
            list_tt_tech_calc.task_id
        ORDER BY
            list_tt_tech_calc.task_id
        ;';
        $accidentsPenalties = $DB->getAll($query, true, true, -1);

        if ($separate) {
            return array($skpCashPenalties, $skpCashlessPenalties, $accidentsPenalties, $connectionsPenalties, $gpPenalties);
        }

        return array_merge($skpCashPenalties, $skpCashlessPenalties, $accidentsPenalties, $connectionsPenalties, $gpPenalties);
    }
}
