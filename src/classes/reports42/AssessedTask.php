<?php

/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 29.12.2015
 */
namespace classes\reports42;

use wf;

/**
 * Class AssessedTask
 * Calculates assessed profits by closed tasks
 * @package classes\reports
 */
class AssessedTask extends ReportTask
{

    public function __construct($id, ReportPeriod $reportTechPeriod)
    {
        parent::__construct($id);

        $this->db = wf::$db;
        $this->setPeriod($reportTechPeriod->getPeriod());
    }

    /**
     * @param mixed $profit
     */
    public function setProfit()
    {
        /**
         * Начисления должно случиться в искомый период, заявка должна быть закрыта в предыдущие периоды.
         */

        $dc_param = MoneyReportQueries::getDcParam($this->getMonth());
        $period = $this->getPeriod();

        $period = "$period-01 00:00:00";
        /** @var string $period_start_date Начало отчётного периода + dc */
        $period_start_date_with_dc = date("Y-m-d 00:00:00", strtotime($period . " + " . getcfg($dc_param) . " days"));
        /** @var string $period_end_date_with_dc Окончание отчётного периода с гандикапом */
        $period_end_date_with_dc = date("Y-m-d 23:59:59", strtotime($period . " + 1 month + " . getcfg($dc_param) . " days"));

        $query = '
            select round(price, 2) as price, task_id from (
                select /* Сумма работ по заявке Подключения */
                    sum(if (
                        smeta_val is null or smeta_val = 0,
                        price * quant,
                        ROUND(smeta_val * price / 100)
                    )) as price, list_tech_calc.task_id as task_id
                from
                    list_tech_calc
                where
                    /* цена не меньше нуля, это ж НЕ штраф */
                    list_tech_calc.price > 0 and
                    list_tech_calc.task_id in (' . $this->id . ') and
                    list_tech_calc.tech_id in (' . implode(', ', (array)$this->getTech()) . ') and

                    /* а в расчёт добавили в отчётный период, */
                    list_tech_calc.update_date BETWEEN
                        STR_TO_DATE(' . $this->db->F($period_start_date_with_dc) . ', "%Y-%m-%d %H:%i:%s") AND
                        STR_TO_DATE(' . $this->db->F($period_end_date_with_dc) . ', "%Y-%m-%d %H:%i:%s") AND
                    /* но уже после заполнения отчёта */
                    DATE_FORMAT(list_tech_calc.update_date, "%Y-%m-%d") > DATE_FORMAT(list_tech_calc.calc_date, "%Y-%m-%d")

                group by
                    list_tech_calc.task_id

                UNION

                select /* Сумма работ по заявке ТТ */
                    sum(if (
                        smeta_val is null or smeta_val = 0,
                        price * quant,
                        ROUND(smeta_val * price / 100)
                    )) as price, list_tt_tech_calc.task_id as task_id
                from
                    list_tt_tech_calc
                where
                    /* цена не меньше нуля, это ж НЕ штраф */
                    list_tt_tech_calc.price > 0 and
                    list_tt_tech_calc.task_id in (' . $this->id . ') and
                    list_tt_tech_calc.tech_id in (' . implode(', ', (array)$this->getTech()) . ') and

                    /* а в расчёт добавили в отчётный период, */
                    list_tt_tech_calc.update_date BETWEEN
                        STR_TO_DATE(' . $this->db->F($period_start_date_with_dc) . ', "%Y-%m-%d %H:%i:%s") AND
                        STR_TO_DATE(' . $this->db->F($period_end_date_with_dc) . ', "%Y-%m-%d %H:%i:%s") AND
                    /* но уже после заполнения отчёта */
                    DATE_FORMAT(list_tt_tech_calc.update_date, "%Y-%m-%d") > DATE_FORMAT(list_tt_tech_calc.calc_date, "%Y-%m-%d")

                group by
                    list_tt_tech_calc.task_id

            ) as prices
            where price is not null
            group by task_id
            order by task_id
        ;';
        $total = $this->db->getField($query);

        $this->_profit = $total;
    }
}
