<?php

/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 29.12.2015
 */
namespace classes\reports42;

use classes\DBMySQL;
use JMS\Serializer\Annotation\Type;

abstract class ReportPeriod
{
    /**
     * @var  DBMySQL
     * @Type("DBMySQL")
     */
    protected $db;
    /**
     * @var ReportPeriod[]
     * @Type("array<classes\reports\ReportPeriod>")
     */
    protected $_items = array();
    /**
     * @Type("string")
     */
    private $_period;
    /**
     * @Type("string")
     */
    private $_year;
    /**
     * @Type("string")
     */
    private $_month;

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->_year;
    }

    /**
     * @return mixed
     */
    public function getMonth()
    {
        return $this->_month;
    }

    /**
     * @param ReportPeriod $tech
     */
    public function addItem(ReportPeriod $tech)
    {
        $this->_items[] = $tech;
    }

    /**
     * ==================================================================================================
     */

    public function getConversion()
    {

        $this->getPeriod();

        die;

        $result = 0;
        $items = $this->getItems();
        if (!empty($items && count($items) > 0))
            foreach ($items as $item) {
                $result += $item->getConversion();
            }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getPeriod()
    {
        return $this->_period;
    }

    /**
     * @param mixed $period
     */
    public function setPeriod($period)
    {
        $this->_period = $period;
        list($this->_year, $this->_month) = explode('-', $this->_period);
    }

    /**
     * @return ReportPeriod[]
     */
    public function getItems()
    {
        return $this->_items;
    }

    public function getFotPercent()
    {
        return false;
    }

    public function getFotBonus()
    {
        $result = 0;
        foreach ($this->getItems() as $item) {
            $result += $item->getFotBonus();
        }

        return $result;
    }

    public function getDebt()
    {
        $result = 0;
        foreach ($this->getItems() as $item) {
            $result += $item->getDebt();
        }

        return $result;
    }

    public function getPenalties()
    {
        $result = 0;
        foreach ($this->getItems() as $item) {
            $result += $item->getPenalties();
        }

        return $result;
    }

    public function getSalary()
    {
        $result = 0;
        foreach ($this->getItems() as $item) {
            $result += $item->getSalary();
        }

        return $result;
    }

    public function getProfit()
    {
        $result = 0;
        $items = $this->getItems();
        if (!empty($items && count($items) > 0))
            foreach ($this->getItems() as $item) {
                $result += $item->getProfit();
            }

        return $result;
    }

    public function getTMCPrice()
    {
        // TODO: Implement getConversion() method.
    }

    public function getWorks()
    {
        $result = array();
        $works = $this->getItems();
        if (!empty($works) && count($works) > 0)
            foreach ($works as $task) {
                $tasks = $task->getProfitByWork();
                if (!empty($tasks) && count($tasks) > 0)
                    foreach ($tasks as $id => $profit) {
                        if (isset($result[$id])) {
                            $result[$id] += $profit;
                        } else {
                            $result[$id] = $profit;
                        }
                    }
            }

        return $result;
    }

    public function getQualityPercent()
    {
        $result = array();
        foreach ($this->getItems() as $item) {
            if ($item->getQualityPercent() > 0)
                $result[] = $item->getQualityPercent();
        }

        if(count($result) > 0)
            return round(array_sum($result) / count($result), 2);
        else
            return 0;
    }

    public function getQualityFactor()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            if ($task->getQualityFactor() > 0)
                $result[] = $task->getQualityFactor();
        }
        if(count($result) > 0)
            return round(array_sum($result) / count($result), 2);
        else
            return 0;
    }

    public function getKpiSum() {

        $result = 0;
        $items = $this->getItems();
        if (!empty($items && count($items) > 0))
            foreach ($this->getItems() as $item) {
                $result += $item->getKpiSum();
            }

        return $result;
    }

    public function getTotalPoints() {

        $result = 0;
        $items = $this->getItems();
        if (!empty($items && count($items) > 0))
            foreach ($this->getItems() as $item) {
                $result += $item->getTotalPoints();
            }

        return $result;
    }

    public function getTotalProfit() {
        $result = 0;
        $items = $this->getItems();
        if (!empty($items && count($items) > 0))
            foreach ($this->getItems() as $item) {
                $result += $item->getTotalProfit();
            }

        return $result;
    }

    /**
     * ==================================================
     * СКП наличные
     */

    public function getSkpCashCount()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getSkpCashCount();
        }
        return $result;
    }

    public function getSkpCashProfit()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getSkpCashProfit();
        }
        return $result;
    }

    public function getSkpCashInvoice()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getSkpCashInvoice();
        }
        return $result;
    }

    public function getSkpCashInvoiceNotPayed()
    {
        $result = 0;
        foreach ($this->getNotPayedSkpTasks() as $item) {
            $result += $item->getSkpCashInvoiceNotPayed();
        }
        return $result;
    }

    public function getNotPayedSkpTasks()
    {
        $result = array();
        foreach ($this->getItems() as $item) {
            $result = array_merge($result, $item->getNotPayedSkpTasks());
        }

        return $result;
    }

    public function getSkpCashInvoiceNotPayedByCounterparty()
    {
        $result = array();
        foreach ($this->getNotPayedSkpTasks() as $task) {
            $task_counts = $task->getSkpCashInvoiceNotPayedByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }
        return $result;
    }

    public function getSkpCashConversion()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getSkpCashConversion();
        }
        return $result;
    }

    public function getSkpCashCountByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getSkpCashCountByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }

        return $result;
    }

    public function getSkpCashProfitByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getSkpCashProfitByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }
        return $result;
    }

    public function getSkpCashInvoiceByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getSkpCashInvoiceByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }
        return $result;
    }

    public function getSkpCashConversionByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getSkpCashConversionByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }

        return $result;
    }

    /**
     * СКП Наличные
     * ==================================================
     */

    /**
     * ==================================================
     * СКП безнал
     */

    public function getSkpCashlessCount()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getSkpCashlessCount();
        }
        return $result;
    }

    public function getSkpCashlessProfit()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getSkpCashlessProfit();
        }
        return $result;
    }

    public function getSkpCashlessInvoice()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getSkpCashlessInvoice();
        }
        return $result;
    }

    public function getSkpCashlessInvoiceNotPayed()
    {
        $result = 0;
        foreach ($this->getNotPayedSkpTasks() as $task) {
            $result += $task->getSkpCashlessInvoiceNotPayed();
        }
        return $result;
    }

    public function getSkpCashlessInvoiceNotPayedByCounterparty()
    {
        $result = array();
        foreach ($this->getNotPayedSkpTasks() as $task) {
            $task_counts = $task->getSkpCashlessInvoiceNotPayedByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }

        return $result;
    }

    public function getSkpCashlessConversion()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getSkpCashlessConversion();
        }
        return $result;
    }

    public function getSkpCashlessCountByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getSkpCashlessCountByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }

        return $result;
    }

    public function getSkpCashlessInvoiceByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getSkpCashlessInvoiceByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }
        return $result;
    }

    public function getSkpCashlessProfitByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getSkpCashlessProfitByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }

        return $result;
    }

    public function getSkpCashlessConversionByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getSkpCashlessConversionByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }

        return $result;
    }

    /**
     * СКП безнал
     * ==================================================
     */

    /**
     * ==================================================
     * ТТ
     */

    public function getAccidentsCountsByWork()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getAccidentsCountsByWork();
            foreach ($task_counts as $id => $count) {
                if (isset($result[$id])) {
                    $result[$id] += $count;
                } else {
                    $result[$id] = $count;
                }
            }
        }

        return $result;
    }

    public function getAccidentsProfit()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getAccidentsProfit();
        }
        return $result;
    }

    public function getAccidentsCount()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getAccidentsCount();
        }
        return $result;
    }

    public function getAccidentsConversion()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getAccidentsConversion();
        }
        return $result;
    }

    public function getAccidentsProfitByWork()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getAccidentsProfitByWork();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }

        return $result;
    }

    public function getAccidentsCountsByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getAccidentsCountsByCounterparty();
            foreach ($task_counts as $id => $count) {
                if (isset($result[$id])) {
                    $result[$id] += $count;
                } else {
                    $result[$id] = $count;
                }
            }
        }

        return $result;
    }

    public function getAccidentsProfitByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getAccidentsProfitByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }

        return $result;
    }

    public function getAccidentsConversionByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getAccidentsConversionByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }
        return $result;
    }

    /**
     * ТТ
     * ==================================================
     */

    /**
     * ==================================================
     * Подключения
     */

    public function getConnectionsProfit()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getConnectionsProfit();
        }
        return $result;
    }

    public function getConnectionsCount()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getConnectionsCount();
        }
        return $result;
    }

    public function getConnectionsConversion()
    {
        $result = 0;
        foreach ($this->getItems() as $task) {
            $result += $task->getConnectionsConversion();
        }
        return $result;
    }

    public function getConnectionsCountsByWork()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getConnectionsCountsByWork();
            foreach ($task_counts as $id => $count) {
                if (isset($result[$id])) {
                    $result[$id] += $count;
                } else {
                    $result[$id] = $count;
                }
            }
        }

        return $result;
    }

    public function getConnectionsProfitByWork()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getConnectionsProfitByWork();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }

        return $result;
    }

    public function getConnectionsCountsByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getConnectionsCountsByCounterparty();
            foreach ($task_counts as $id => $count) {
                if (isset($result[$id])) {
                    $result[$id] += $count;
                } else {
                    $result[$id] = $count;
                }
            }
        }

        return $result;
    }

    public function getConnectionsProfitByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getConnectionsProfitByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }
        return $result;
    }

    public function getConnectionsConversionByCounterparty()
    {
        $result = array();
        foreach ($this->getItems() as $task) {
            $task_counts = $task->getConnectionsConversionByCounterparty();
            foreach ($task_counts as $id => $profit) {
                if (isset($result[$id])) {
                    $result[$id] += $profit;
                } else {
                    $result[$id] = $profit;
                }
            }
        }
        return $result;
    }

    /**
     *
     * Подключения
     * ==================================================
     */


}
