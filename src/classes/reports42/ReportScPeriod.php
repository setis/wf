<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 29.12.2015
 */

/**
 * Class ReportScPeriod
 * @method \classes\reports\ReportTechPeriod getItems()
 *
 */
namespace classes\reports42;

use JMS\Serializer\Annotation\Type;
use wf;

class ReportScPeriod extends ReportPeriod
{
    /**
     * @Type("integer")
     */
    public $id;
    /**
     * @Type("string")
     */
    public $title;

    /**
     * @var ReportScPeriod[]
     * @Type("array<classes\reports42\ReportTechPeriod>")
     */
    protected $_items = array();
    /**
     * @Type("array")
     */
    private $_sc_users;

    private $pointPrice;

    public function __construct($sc_id, $period)
    {
        $this->db = wf::$db;

        $pointPrice = wf::$em->find(\models\ListSc::class, $sc_id)->getRegion()->getPointPrice();

        $this->pointPrice = $pointPrice;

        $this->setPeriod($period);
        $this->setSc($sc_id);

        $sc_users = $this->getScUsers();
//        $sc_users = array_slice($sc_users, 0, 2);
//        $sc_users = [3477, 3858, 3349];
//        $sc_users = [3349];

        foreach ($sc_users as $i => $tech_id) {
            $this->addItem(new ReportTechPeriod($tech_id, $period, $this->pointPrice));
        }
    }

    private function setSc($sc)
    {
        $data = \adm_sc_plugin::getSC($sc);
        list($this->id, $this->title) = array($data['id'], $data['title']);

        $query = '
            SELECT
                link_sc_user.user_id
            FROM
                link_sc_user
                LEFT JOIN
                virtual_techs ON ( virtual_techs.virtual_id = link_sc_user.user_id )
                LEFT JOIN
                users ON (users.id = link_sc_user.user_id )
            WHERE
                link_sc_user.sc_id = ' . $sc . ' AND
                virtual_techs.virtual_id IS NULL AND
                users.active = 1
            ORDER BY
                link_sc_user.user_id
            ;';

        $this->_sc_users = $this->db->getCell($query);
    }

    /**
     * @return mixed
     */
    public function getScUsers()
    {
        return $this->_sc_users;
    }

    /**
     * @return float
     */
    public function getPointPrice(): float
    {
        return $this->pointPrice;
    }


}
