<?php

/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 29.12.2015
 */
namespace classes\reports42;

use classes\Task;
use JMS\Serializer\Annotation\Type;
use models\Agreement;
use wf;

class ReportTask extends ReportPeriod
{

    const SKP_FOT = 0.3; // Фонд оплаты труда 30% от сданного по заявкам СКП
    const CONVERSATION_PERCENTAGE = 0.10; // Процент по конвертации
    const CONVERSATION_LIMIT = 0.8; // нижний предел получения конвертации по заявке
    /**
     * @Type("array")
     */
    private static $_total_at_day_tasks = [];
    /**
     * @Type("array")
     */
    private static $_finished_at_day_tasks = [];
    /**
     * @Type("integer")
     */
    public $id;
    /**
     * @Type("string")
     */
    public $uid;
    /**
     * @Type("string")
     */
    public $reg_date;
    /**
     * @Type("array")
     */
    public $ticket;
    /**
     * @Type("array")
     */
    public $counterparty;
    /**
     * @Type("array")
     */
    public $address;
    /**
     * @Type("integer")
     */
    protected $_tech;
    /**
     * @Type("double")
     */
    protected $_profit;
    /**
     * @Type("double")
     */
    protected $_invoice;
    /**
     * @Type("double")
     */
    protected $_salary;
    /**
     * @Type("double")
     */
    protected $_conversion;
    /**
     * @Type("array")
     */
    protected $_works;
    /**
     * @Type("array")
     */
    protected $_works_counts;
    /**
     * @Type("array")
     */
    protected $_tech_aliases = [];
    /**
     * @Type("string")
     */
    protected $_gfx;
    /**
     * @Type("string")
     */
    protected $_profit_date;
    /**
     * @Type("string")
     */
    protected $_done_date;
    /**
     * @Type("double")
     */
    protected $_quality_factor;
    /**
     * @Type("boolean")
     */
    protected $_isSKPCash;
    /**
     * @var Task
     * @Type("classes\Task")
     */
    private $_task;

    public function __construct($id, $uid = null)
    {
        $this->db = $this->db = wf::$db;

        $this->id = $id;

        $query = "
        SELECT
            tasks.plugin_uid,
            tasks.date_reg,
            gfx.empl_id,
            gfx.c_date,
            tickets.*,
            list_contr.*,
            list_addr.*,
            if( exists(SELECT * FROM virtual_techs WHERE virtual_id= gfx.empl_id),
                (SELECT virtual_id FROM virtual_techs WHERE user_id=gfx.empl_id),
                (SELECT user_id FROM virtual_techs WHERE virtual_id=gfx.empl_id)
            ) as empl_id2
        FROM
            tasks
            left join
            gfx on tasks.id = gfx.task_id
            left join
            tickets on tasks.id = tickets.task_id
            LEFT JOIN 
            list_contr on tickets.cnt_id = list_contr.id
            LEFT JOIN 
            list_addr ON tickets.dom_id = list_addr.id
        WHERE
            tasks.id = {$this->id}
        ";

        $task = $this->db->getRow($query, true);

        $this->uid = $task['plugin_uid'];
        $this->reg_date = $task['date_reg'];
        $this->_gfx = $task['c_date'];
        $this->_tech = $task['empl_id'];
        $this->ticket = [
            'clntopagr' => $task['clntopagr'],
            'clnttnum' => $task['clnttnum']
        ];
        $this->counterparty['contr_title'] = $task['contr_title'];
        $this->address['address'] = $task['full_address'];
        $this->address['kv'] = $task['kv'];

        $this->_tech_aliases = array_unique(array_filter([$this->_tech, $task['empl_id2']]));
    }


    public function getUrl()
    {
        if (empty($this->_task)) {
            $this->_task = new Task($this->id);
        };

        return Task::getTaskUrl($this->_task);
    }

    public function getNotPayedSkpTasks()
    {
        return [];
    }

    public function getPenalties()
    {
        return 0;
    }

    public function getQualityFactor()
    {
        if (is_null($this->_quality_factor)) {
            $this->setQualityFactor();
        }

        return $this->_quality_factor;
    }

    private function setQualityFactor()
    {
        $query = "
            SELECT
                tqc.qc_new_result
            FROM
                tickets_qc AS tqc
            WHERE
                 tqc.task_id IN ( " . $this->id . " ) AND
                tqc.qc_status = 1 AND
                tqc.qc_date >= '2015-07-09'
             ";
        $quality_result = $this->db->getField($query);

        return $quality_result;
    }

    /**
     * @return mixed
     */
    public function getProfitDate()
    {
        if (is_null($this->_profit_date)) {
            $this->setProfitDate();
        }

        return $this->_profit_date;
    }

    public function setProfitDate()
    {
        $doneStatus = [
            54, // TT.Закрыта
            26, // Подключение.Закрыта
            49, // СКП.Принято в кассу
        ];

        $query = 'SELECT /* Дата начисления денег по заявке */
            task_comments.datetime
        FROM
            task_comments
        WHERE
            /* оплатный статус у заявки */
            task_comments.status_id IN(' . implode(', ', $doneStatus) . ') AND
            task_comments.task_id = ' . $this->db->F($this->id) . '
        ORDER BY
            task_comments.datetime DESC
        LIMIT 1;';

        $this->_profit_date = $this->db->getField($query);
    }

    /**
     * @return mixed
     */
    public function getDoneDate()
    {
        if (is_null($this->_done_date)) {
            $this->setDoneDate();
        }

        return $this->_done_date;
    }

    public function setDoneDate()
    {
        $doneStatus = [
            61, // TT.Выполнена
            1, // Подключение.Выполнена
            12, // СКП.Выполнена
        ];

        $query = 'SELECT /* Дата выполнения заявки */
            task_comments.datetime
        FROM
            task_comments
        WHERE
            /* статус Подключение.Закрыто */
            task_comments.status_id IN(' . implode(', ', $doneStatus) . ') AND
            task_comments.task_id = ' . $this->db->F($this->id) . '
        ORDER BY
            task_comments.datetime DESC
        LIMIT 1;';

        $this->_done_date = $this->db->getField($query);
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        if (is_null($this->_salary)) {
            $this->setSalary();
        }

        return $this->_salary;
    }

    /**
     * profit
     * @param mixed $salary
     */
    public function setSalary()
    {
        switch ($this->uid) {
            case \classes\tickets\ServiceTicket::UID:
                if ($this->isSkpCash())
                    $profit = 0;
                else
                    $profit = $this->getProfit();
                break;
            default:
                $profit = $this->getProfit();
        }

        $this->_salary = $profit;
    }

    /**
     * @return mixed
     */
    public function getCounterparty()
    {
        return $this->counterparty;
    }

    /**
     * @param mixed $counterparty
     */
    public function setCounterparty()
    {
        $query = '
            SELECT
                *
            FROM
                list_contr
            WHERE
                id = ' . $this->ticket['cnt_id'] . '
        ;';

        $this->counterparty = $this->db->getRow($query, true);
    }

    /**
     * @return mixed
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param mixed $ticket
     */
    public function setTicket()
    {
        $query = '
            SELECT
                *
            FROM
                tickets
            WHERE
                task_id = ' . $this->id . '
        ;';

        $this->ticket = $this->db->getRow($query, true);
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * ==================================================
     */

    public function getCountsByCounterparty()
    {
        return [$this->counterparty['contr_title'] => 1];
    }

    public function getCountsByWork()
    {
        return $this->getWorksCounts();
    }

    /**
     * @return mixed
     */
    public function getWorksCounts()
    {
        if (is_null($this->_works_counts)) {
            $this->setWorks();
        }

        return $this->_works_counts;
    }

    public function getProfitByCounterparty()
    {
        return [$this->counterparty['contr_title'] => $this->getProfit()];
    }

    /**
     * @return mixed
     */
    public function getProfit()
    {
        if (is_null($this->_profit)) {
            $this->setProfit();
        }

        return $this->_profit;
    }

    public function setProfit()
    {
        $query = '
            SELECT round(price, 2) AS price, task_id FROM (
                SELECT /* Сумма работ по заявке Подключения */
                    sum(if (
                        smeta_val IS NULL OR smeta_val = 0,
                        price * quant,
                        ROUND(smeta_val * price / 100)
                    )) AS price, list_tech_calc.task_id AS task_id
                FROM
                    list_tech_calc
                WHERE
                    /* цена не меньше нуля, это ж НЕ штраф */
                    list_tech_calc.price > 0 AND
                    list_tech_calc.task_id IN (' . $this->id . ')
                GROUP BY
                    list_tech_calc.task_id

                UNION

                SELECT /* Сумма работ по заявке ТТ */
                    sum(if (
                        smeta_val IS NULL OR smeta_val = 0,
                        price * quant,
                        ROUND(smeta_val * price / 100)
                    )) AS price, list_tt_tech_calc.task_id AS task_id
                FROM
                    list_tt_tech_calc
                WHERE
                    /* цена не меньше нуля, это ж НЕ штраф */
                    list_tt_tech_calc.price > 0 AND
                    list_tt_tech_calc.task_id IN (' . $this->id . ')
                GROUP BY
                    list_tt_tech_calc.task_id

                UNION

                SELECT /* Сумма работ по заявке СКП */
                    orient_price * ' . self::SKP_FOT . ' AS price, tickets.task_id AS task_id
                FROM
                    tickets
                    LEFT JOIN
                    task_users ON(tickets.task_id = task_users.task_id)
                WHERE
                    tickets.task_id IN (' . $this->id . ')
            ) AS prices
            WHERE price IS NOT NULL
            GROUP BY task_id
            ORDER BY task_id
        ;';
        $total = $this->db->getField($query);

        $this->_profit = $total;
    }

    public function getInvoiceByCounterparty()
    {
        return [$this->counterparty['contr_title'] => $this->getInvoice()];
    }

    /**
     * Счёт выставляемый контрагенту
     *
     * @return mixed
     */
    public function getInvoice()
    {
        if (is_null($this->_invoice)) {

            if ($this->uid == \classes\tickets\ServiceTicket::UID) {
                $this->_invoice = round($this->getProfit() * (1 / self::SKP_FOT));
            } else {
                $this->_invoice = $this->getProfit();
            }

        }

        return $this->_invoice;
    }

    public function getProfitByWork()
    {
        return $this->getWorks();
    }

    /**
     * @return mixed
     */
    public function getWorks()
    {
        if (is_null($this->_works)) {
            $this->setWorks();
        }

        return $this->_works;
    }

    public function setWorks()
    {
        $works = [];
        $works_counts = [];

        $query = '
            SELECT /* Работы по заявке Подключения */
                list_tech_price.title AS title,
                if (
                    list_tech_calc.smeta_val IS NULL OR list_tech_calc.smeta_val = 0,
                    list_tech_calc.price * list_tech_calc.quant,
                    ROUND(list_tech_calc.smeta_val * list_tech_calc.price / 100)
                ) AS price,
                list_tech_calc.quant
            FROM
                  list_tech_price
                  LEFT JOIN
                  list_tech_calc ON(list_tech_price.id = list_tech_calc.work_id)
            WHERE
                  list_tech_calc.task_id IN (' . $this->id . ') AND
                  /* цена не меньше нуля, это ж НЕ штраф */
                  list_tech_calc.price > 0

            UNION

            SELECT /* Работы по заявке Подключения */
                  list_tech_price.title AS title,
                if (
                    list_tt_tech_calc.smeta_val IS NULL OR list_tt_tech_calc.smeta_val = 0,
                    list_tt_tech_calc.price * list_tt_tech_calc.quant,
                    ROUND(list_tt_tech_calc.smeta_val * list_tt_tech_calc.price / 100)
                ) AS price,
                list_tt_tech_calc.quant
            FROM
                  list_tech_price
                  LEFT JOIN
                  list_tt_tech_calc ON(list_tech_price.id = list_tt_tech_calc.work_id)
            WHERE
                  list_tt_tech_calc.task_id IN (' . $this->id . ') AND
                  /* цена не меньше нуля, это ж НЕ штраф */
                  list_tt_tech_calc.price > 0
            ;';

        $tmp = $this->db->getAll($query, true, false, -1);

        foreach ($tmp as $w) {
            $works[$w['title']] = $w['price'];
        }

        foreach ($tmp as $w) {
            $works_counts[$w['title']] = $w['quant'];
        }

        $this->_works = $works;
        $this->_works_counts = $works_counts;
    }

    /**
     * ==================================================
     * СКП нал
     */

    public function getSkpCashCount()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && $this->isSKPCash()) {
            return 1;
        }

        return 0;
    }

    /**
     * @return mixed
     */
    public function isSKPCash()
    {
        if (is_null($this->_isSKPCash)) {
            $query = 'SELECT /* Вяснение типа оплаты заявки */
                count(*)
            FROM
                tasks
                LEFT JOIN
                tickets ON(tickets.task_id = tasks.id)
                LEFT JOIN
                list_contr_agr ON(list_contr_agr.id = tickets.agr_id)
            WHERE
                tasks.id = ' . $this->db->F($this->id) . ' AND
                /* расчёт контрагента безналично */
                list_contr_agr.paytype = ' . Agreement::CASHLESS . '
        ;';

            $result = $this->db->getField($query);

            $this->_isSKPCash = ($result <= 0 ?: false);
        }

        return $this->_isSKPCash;
    }

    public function getSkpCashProfit()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && $this->isSKPCash()) {
            return $this->getProfit();
        }

        return 0;
    }

    public function getSkpCashInvoice()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && $this->isSKPCash()) {
            return $this->getInvoice();
        }

        return 0;
    }

    public function getSkpCashConversion()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && $this->isSKPCash()) {
            return $this->getConversion();
        }

        return 0;
    }

    /**
     * @return integer|null
     */
    public function getConversion()
    {
        if (is_null($this->_conversion)) {
            $this->setConversion();
        }

        return $this->_conversion;
    }

    private function setConversion()
    {
        if (is_null($this->_conversion)) {
            if ($this->conversionPercentAllow($this->getDayFinishedTasks(), $this->getDayTotalTasks())) {
                $this->_conversion = $this->getProfit() * self::CONVERSATION_PERCENTAGE;
            } else {
                $this->_conversion = 0;
            }
        }

        return $this->_conversion;
    }

    /**
     * @param $countFinishedTasksAtDay
     * @param $countTasksAtDay
     * @return bool
     */
    private function conversionPercentAllow($countFinishedTasksAtDay, $countTasksAtDay)
    {
        if (is_array($countFinishedTasksAtDay))
            $countFinishedTasksAtDay = count($countFinishedTasksAtDay);

        if (is_array($countTasksAtDay))
            $countTasksAtDay = count($countTasksAtDay);

        if ($countTasksAtDay > 0 && $countFinishedTasksAtDay / $countTasksAtDay > self::CONVERSATION_LIMIT) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    private function getDayFinishedTasks()
    {
        $doneStatuses = [
            "61", // ТТ.Выполнена
            "1", // Подключение.Выполнена
            "12", // СКП.Выполнена
        ];

        if (isset(self::$_finished_at_day_tasks[$this->getTech()]) && isset(self::$_finished_at_day_tasks[$this->getTech()][$this->getGfx()]) && !is_null(self::$_finished_at_day_tasks[$this->getTech()][$this->getGfx()])) {
            return self::$_finished_at_day_tasks[$this->getTech()][$this->getGfx()];
        }

        $date = new \DateTime($this->getGfx());

        $query = 'SELECT /* Получение только выполненых заявок за день */
            count(*)
        FROM
            gfx
            LEFT JOIN
            task_comments ON(task_comments.task_id = gfx.task_id)
            LEFT JOIN
            tasks ON(gfx.task_id = tasks.id)
        WHERE
            (gfx.c_date >= "' . $date->format('Y-m-d') . '" AND gfx.c_date < "' . $date->add( new \DateInterval('P1D') )->format('Y-m-d') . '")
            AND (task_comments.datetime >= "' . $date->format('Y-m-d') . '" AND task_comments.datetime < "' . $date->add( new \DateInterval('P1D') )->format('Y-m-d') . '")
            /* Выполнительный статус заявки */
            AND task_comments.status_id IN (' . implode(', ', $doneStatuses) . ')
            AND empl_id IN (' . implode(',', $this->getTechAliases()) . ')
        ;';

        self::$_finished_at_day_tasks[$this->getTech()][$this->getGfx()] = $this->db->getField($query);

        return self::$_finished_at_day_tasks[$this->getTech()][$this->getGfx()];
    }

    public function getTech()
    {
        return $this->_tech;
    }

    /**
     * @return mixed
     */
    public function getGfx()
    {
        return $this->_gfx;
    }

    /**
     * @return mixed
     */
    public function getTechAliases()
    {
        return $this->_tech_aliases;
    }

    private function getDayTotalTasks()
    {
        $cancelStatuses = [
            65, // ТТ.Отказ клиента
            60, // ТТ.Отказ оператора
            5, // Подключение.Отказ клиента
            13, // Подключение.Отказ оператора
            10, // СКП.Отказ клиента
            17, // СКП.Отказ оператора
            94, // СКП.Отказ оператора
        ];

        if (isset(self::$_total_at_day_tasks[$this->getTech()]) && isset(self::$_total_at_day_tasks[$this->getTech()][$this->getGfx()]) && !is_null(self::$_total_at_day_tasks[$this->getTech()][$this->getGfx()])) {
            return self::$_total_at_day_tasks[$this->getTech()][$this->getGfx()];
        }

        $date = new \DateTime($this->getGfx());

        $query = 'SELECT /* Получение общего количества заявок на день, без отменённых или отказов */
            count(*)
        FROM
            gfx
            LEFT JOIN
            tasks ON(tasks.id = gfx.task_id)
        WHERE
            (gfx.c_date >= "' . $date->format('Y-m-d') . '" AND gfx.c_date < "' . $date->add( new \DateInterval('P1D') )->format('Y-m-d') . '")
            AND gfx.empl_id IN (' . implode(',', $this->getTechAliases()) . ')
            AND tasks.status_id NOT IN (' . implode(', ', $cancelStatuses) . ')
        ;';

        self::$_total_at_day_tasks[$this->getTech()][$this->getGfx()] = $this->db->getField($query);

        return self::$_total_at_day_tasks[$this->getTech()][$this->getGfx()];
    }

    public function getSkpCashProfitByCounterparty()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && $this->isSKPCash()) {
            return [$this->counterparty['contr_title'] => $this->getProfit()];
        }

        return [];
    }

    public function getSkpCashInvoiceByCounterparty()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && $this->isSKPCash()) {
            return [$this->counterparty['contr_title'] => $this->getInvoice()];
        }

        return [];
    }

    public function getSkpCashCountByCounterparty()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && $this->isSKPCash()) {
            return [$this->counterparty['contr_title'] => 1];
        }

        return [];
    }

    public function getSkpCashConversionByCounterparty()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && $this->isSKPCash()) {
            return [$this->counterparty['contr_title'] => $this->getConversion()];
        }

        return [];
    }

    public function getSkpCashInvoiceNotPayedByCounterparty()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && $this->isSKPCash() && $this->getInvoice() > 0) {
            return [$this->counterparty['contr_title'] => $this->getInvoice()];
        }

        return [];
    }

    public function getSkpCashInvoiceNotPayed()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && $this->isSKPCash()) {
            return $this->getInvoice();
        }

        return 0;
    }

    /**
     * СКП наличные
     * ==================================================
     */

    /**
     * ==================================================
     * СКП безнал
     */

    public function getSkpCashlessCount()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && !$this->isSKPCash()) {
            return 1;
        }

        return 0;
    }

    public function getSkpCashlessProfit()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && !$this->isSKPCash()) {
            return $this->getProfit();
        }

        return 0;
    }

    public function getSkpCashlessInvoice()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && !$this->isSKPCash()) {
            return $this->getInvoice();
        }

        return 0;
    }

    public function getSkpCashlessConversion()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && !$this->isSKPCash()) {
            return $this->getConversion();
        }

        return 0;
    }

    public function getSkpCashlessCountByCounterparty()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && !$this->isSKPCash()) {
            return [$this->counterparty['contr_title'] => 1];
        }

        return [];
    }

    function getSkpCashlessProfitByCounterparty()
    {
        if ($this->uid == 'services' && !$this->isSKPCash()) {
            return [$this->counterparty['contr_title'] => $this->getProfit()];
        }

        return [];
    }

    function getSkpCashlessInvoiceByCounterparty()
    {
        if ($this->uid == 'services' && !$this->isSKPCash()) {
            return [$this->counterparty['contr_title'] => $this->getInvoice()];
        }

        return [];
    }

    public function getSkpCashlessConversionByCounterparty()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && !$this->isSKPCash()) {
            return [$this->counterparty['contr_title'] => $this->getConversion()];
        }

        return [];
    }

    public function getSkpCashlessInvoiceNotPayedByCounterparty()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && !$this->isSKPCash()) {
            return [$this->counterparty['contr_title'] => $this->getInvoice()];
        }

        return [];
    }

    public function getSkpCashlessInvoiceNotPayed()
    {
        if ($this->uid == \classes\tickets\ServiceTicket::UID && !$this->isSKPCash()) {
            return $this->getInvoice();
        }

        return 0;
    }

    /**
     * СКП безнал
     * ==================================================
     */

    /**
     * ==================================================
     * ТТ
     */

    public function getAccidentsCount()
    {
        if ($this->uid == \classes\tickets\AccidentTicket::UID) {
            return 1;
        }

        return 0;
    }

    public function getAccidentsConversion()
    {
        if ($this->uid == \classes\tickets\AccidentTicket::UID) {
            return $this->getConversion();
        }

        return 0;
    }

    public function getAccidentsProfit()
    {
        if ($this->uid == \classes\tickets\AccidentTicket::UID) {
            return $this->getProfit();
        }

        return 0;
    }

    public function getAccidentsCountsByWork()
    {
        if ($this->uid == \classes\tickets\AccidentTicket::UID) {
            return $this->getWorksCounts();
        }

        return [];
    }

    public function getAccidentsCountsByCounterparty()
    {
        if ($this->uid == \classes\tickets\AccidentTicket::UID) {
            return [$this->counterparty['contr_title'] => 1];
        }

        return [];
    }

    public function getAccidentsProfitByWork()
    {
        if ($this->uid == \classes\tickets\AccidentTicket::UID) {
            $tmp = [];
            foreach ($this->getWorks() as $title => $profit) {
                $tmp[$title] += $profit;
            }

            return $tmp;
        }

        return [];
    }

    public function getAccidentsProfitByCounterparty()
    {
        if ($this->uid == \classes\tickets\AccidentTicket::UID) {
            return [$this->counterparty['contr_title'] => $this->getProfit()];
        }

        return [];
    }

    public function getAccidentsConversionByCounterparty()
    {
        if ($this->uid == \classes\tickets\AccidentTicket::UID) {
            return [$this->counterparty['contr_title'] => $this->getConversion()];
        }

        return [];
    }

    /**
     * ТТ
     * ==================================================
     */


    /**
     * ==================================================
     * Подключения
     */

    public function getConnectionsCount()
    {
        if ($this->uid == \classes\tickets\ConnectionTicket::UID) {
            return 1;
        }

        return 0;
    }

    public function getConnectionsConversion()
    {
        if ($this->uid == \classes\tickets\ConnectionTicket::UID) {
            return $this->getConversion();
        }

        return 0;
    }

    public function getConnectionsProfit()
    {
        if ($this->uid == \classes\tickets\ConnectionTicket::UID) {
            return $this->getProfit();
        }

        return 0;
    }

    public function getConnectionsCountsByWork()
    {
        if ($this->uid == \classes\tickets\ConnectionTicket::UID) {
            return $this->getWorksCounts();
        }

        return [];
    }

    public function getConnectionsCountsByCounterparty()
    {
        if ($this->uid == \classes\tickets\ConnectionTicket::UID) {
            return [$this->counterparty['contr_title'] => 1];
        }

        return [];
    }

    public function getConnectionsProfitByWork()
    {
        if ($this->uid == \classes\tickets\ConnectionTicket::UID) {
            $tmp = [];
            foreach ($this->getWorks() as $title => $profit) {
                $tmp[$title] += $profit;
            }

            return $tmp;
        }

        return [];
    }

    public function getConnectionsProfitByCounterparty()
    {
        if ($this->uid == \classes\tickets\ConnectionTicket::UID) {
            return [$this->counterparty['contr_title'] => $this->getProfit()];
        }

        return [];
    }

    public function getConnectionsConversionByCounterparty()
    {
        if ($this->uid == \classes\tickets\ConnectionTicket::UID) {
            return [$this->counterparty['contr_title'] => $this->getConversion()];
        }

        return [];
    }

    /**
     * Подключения
     * ==================================================
     */

}
