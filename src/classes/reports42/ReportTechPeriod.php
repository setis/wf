<?php

/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 29.12.2015
 */

namespace classes\reports42;

use classes\DBMySQL;
use DateTime;
use JMS\Serializer\Annotation\Type;
use models\Task;
use models\TaskStatus;
use wf;

class ReportTechPeriod extends ReportPeriod
{

    const KPI1FAC = 30;
    const KPI2FAC = 30;
    const KPI3FAC = 15;
    const KPI4FAC = 25;

    /**
     * @Type("array<classes\reports\ReportTask>")
     */
    protected $_items = [];
    /**
     * @Type("array")
     */
    private $_tech;
    /**
     * @Type("integer")
     */
    private $_tech_id;
    /**
     * @Type("array")
     */
    private $_tech_id_aliases = [];
    /**
     * @Type("double")
     */
    private $_penalties;
    /**
     * @Type("array")
     */
    private $_penalties_list;
    /**
     * @Type("double")
     */
    private $_quality_percent;
    /**
     * @Type("array")
     */
    private $_debt;
    /**
     * @Type("array<classes\reports\ReportTask>")
     */
    private $_not_payed_skp_tasks = [];
    /**
     * @Type("array<classes\reports\AssessedTask>")
     */
    private $_assessed_items;

    /**
     * @var float
     */
    private $kpi1;

    /**
     * @var float
     */
    private $kpi2;

    /**
     * @var float
     */
    private $kpi3;

    /**
     * @var float
     */
    private $kpi4;

    /**
     * @var int
     */
    private $pointPrice;

    public function __construct($tech_id, $period, $point_price = 1)
    {
        $this->_db = wf::$db;

        $this->pointPrice = $point_price;

        $this->setPeriod($period);
        $this->setTech($tech_id);

        $tasks = MoneyReportQueries::getTechMonthClosedTasks($this->getTechWithAliases(), $this->getPeriod());
        foreach ($tasks as $task) {
            $this->addItem(new ReportTask($task['id']));
        }

        $assessedTasks = MoneyReportQueries::getTechAssessedAdditionalTasks($this->getTechWithAliases(), $this->getPeriod());
        foreach ($assessedTasks as $task) {
            $this->addAssessedItem(new AssessedTask($task['id'], $this));
        }

        $not_payed_skp_tasks = MoneyReportQueries::getTechMonthSkpDoneTasks($this->getTechWithAliases(), $this->getPeriod());
        foreach ($not_payed_skp_tasks as $task) {
            $this->addNotPayedSkpTasks(new ReportTask($task['id']));
        }
    }

    /**
     * @param mixed $tech
     */
    private function setTech($tech)
    {
        $this->_tech_id = $tech;

        if (\adm_empl_plugin::isReal($this->_tech_id)) {
            $this->_tech_id_aliases[] = $this->_tech_id;
            $virt_id = \adm_empl_plugin::isVirtual($this->_tech_id);
            if ($virt_id)
                $this->_tech_id_aliases[] = $virt_id;
        } else {
            $this->_tech_id_aliases[] = $this->_tech_id;
            $this->_tech_id = \adm_empl_plugin::getReal($this->_tech_id);
            $this->_tech_id_aliases[] = $this->_tech_id;
        }

        $query = '
            SELECT
                users.id,
                users.fio,
                user_pos.title,
                user_pos.id AS pos_id
            FROM
                users
                LEFT JOIN
                user_pos ON (users.pos_id = user_pos.id)
            WHERE
                users.id = ' . $this->_tech_id . '
            ;';

        $this->_tech = $this->getDb()->getRow($query, true);

    }

    /**
     * @return DBMySQL
     */
    private function getDb()
    {
        return $this->_db;
    }

    /**
     * @return array
     */
    public function getTechWithAliases()
    {
        return $this->_tech_id_aliases;
    }

    public function addAssessedItem(AssessedTask $item)
    {
        $this->_assessed_items[] = $item;
    }

    /**
     * @param ReportTask $task
     */
    public function addNotPayedSkpTasks(ReportTask $task)
    {
        $this->_not_payed_skp_tasks[] = $task;
    }

    /**
     * Сумма доначсилений за отчётный период
     * @return int
     */
    public function getAssessedProfit()
    {
        $result = 0;
        $items = $this->getAssessedItem();
        if (!empty($items && count($items) > 0))
            foreach ($this->getAssessedItem() as $item) {
                $result += $item->getProfit();
            }

        return $result;
    }

    /**
     * @return AssessedTask[]
     */
    public function getAssessedItem()
    {
        return $this->_assessed_items;
    }

    /**
     * Начислено
     * @return float
     */
    public function getSalary()
    {
        $total_salary = round(
            ($this->getProfit() + $this->getPenalties()) * $this->getFotPercent()
            , 2);

        return $total_salary;
    }

    /**
     * Штрафы
     * @return int
     */
    public function getPenalties()
    {
        if (is_null($this->_penalties)) {
            $this->_penalties = 0;

            $list = MoneyReportQueries::getTechMonthPenalty($this->getTechWithAliases(), $this->getYear(), $this->getMonth());
            foreach ($list as $item) {
                $this->_penalties += $item['price'];
            }
        }

        return $this->_penalties;
    }

    /**
     * Премия от ФОТ
     * @return int
     */
    public function getFotPercent()
    {
        return $this->getKpiSum() / 100;
    }

    /**
     * Премия ОТК
     * @return float|int
     */
    public function getKpiSum()
    {
        return
            self::KPI1FAC * $this->getP1kpi() + self::KPI2FAC * $this->getP2kpi() + self::KPI3FAC * $this->getP3kpi() + self::KPI4FAC * $this->getP4kpi();
    }

    public function getTotalPoints() {
        return $this->getKpiSum() * (($this->getProfit() + $this->getPenalties()) / $this->getPointPrice()) / 100;
    }

    public function getTotalProfit()
    {
        return $this->getKpiSum() * ($this->getProfit() + $this->getPenalties()) / $this->getPointPrice();
    }

    public function getP1kpi()
    {
        $kpi = $this->getKPI1();

        if ($kpi >= 1) {
            $k = 1.3;
        } elseif ($kpi > .9) {
            $k = 1.2;
        } elseif ($kpi > .8) {
            $k = 1.1;
        } elseif ($kpi > .7) {
            $k = 1;
        } else {
            $k = .9;
        }

        return $k;
    }

    public function getKPI1()
    {
        if (null !== $this->kpi1) {
            return $this->kpi1;
        }

        $start = new DateTime($this->getPeriod() . '-01');
        $end = (clone $start)->modify('+1 month');

        $sql = 'SELECT
            gfx.c_date,
            gfx.empl_id,
            (SELECT count(g.id)
                FROM
                    gfx g
                WHERE
                    g.empl_id = gfx.empl_id
                    AND g.c_date = gfx.c_date
                    AND EXISTS(
                        SELECT 
                            *
                        FROM 
                            task_comments tc
                        WHERE
                            tc.task_id = g.task_id
                            AND tc.status_id IN (1, 61, 12)
                            AND date(tc.datetime) = gfx.c_date
                    )
            ) / count(gfx.id) AS day_conversion
            FROM
                gfx
            WHERE
                gfx.empl_id IN (' . implode(', ', $this->getTechWithAliases()) . ')
                AND gfx.c_date >= "' . $start->format('Y-m-d') . '" AND gfx.c_date < "' . $end->format('Y-m-d') . '"
            GROUP BY 
                gfx.c_date
        ;';

        $result = $this->getDb()->query($sql);

        $day_conversion_sum = 0;
        $i = 0;
        foreach ($result as $day) {
            $day_conversion_sum += $day['day_conversion'];
            $i++;
        }

        $this->kpi1 = $day_conversion_sum / (0 === $i ? 1 : $i);

        return $this->kpi1;
    }

    public function getP2kpi()
    {
        return $this->getQualityFactor();
    }

    /**
     * Кэф обзвона
     * @return float|int
     */
    public function getQualityFactor()
    {
        if (is_null($this->_quality_percent)) {
            $this->setQualityPercent();
        }

        if ($this->_quality_percent == 100) {
            $k = 1.1;
        } elseif ($this->_quality_percent > 99.8) {
            $k = 1;
        } elseif ($this->_quality_percent > 99) {
            $k = 0.95;
        } elseif ($this->_quality_percent > 98) {
            $k = 0.9;
        } else if ($this->_quality_percent > 0) {
            $k = 0.8;
        } else {
            $k = 1;
        }

        return $k;
    }

    private function setQualityPercent()
    {
        $start = new \DateTime($this->getPeriod() . '-01');
        $end = (clone $start)->modify('+1 month');

        $query = "
            SELECT
                avg(tqc.qc_new_result)
            FROM
                `tickets_qc` AS tqc
            WHERE
                 (tqc.qc_date >= '" . $start->format('Y-m-d') . "' AND tqc.qc_date < '" . $end->format('Y-m-d') . "' ) AND
                 tqc.task_id IN (
                    SELECT
                        `task_id`
                    FROM
                        `gfx`
                    WHERE
                        `empl_id` IN (" . implode(", ", $this->getTechWithAliases()) . ")
                    ) AND
                tqc.qc_status = 1
             ";
        $this->_quality_percent = $this->getDb()->getField($query);
    }

    public function getP3kpi()
    {
        $kpi = $this->getKPI3();

        if ($kpi >= 30) {
            $k = 1.6;
        } elseif ($kpi > 25) {
            $k = 1.4;
        } elseif ($kpi > 20) {
            $k = 1.2;
        } else {
            $k = 1;
        }

        return $k;
    }

    public function getKPI3()
    {
        if (null !== $this->kpi3) {
            return $this->kpi3;
        }

        $skpCashDoneStatuses = [
            TaskStatus::STATUS_SERVICE_CASH_ADOPTED
        ];
        $skpCashlessDoneStatuses = [
            TaskStatus::STATUS_SERVICE_CLOSED
        ];

        $start = new DateTime($this->getPeriod() . '-01 00:00:00');
        $end = (clone $start)->modify('+1 month');

        $sql = '
        SELECT
          avg(tickets.orient_price)
        FROM gfx
          INNER JOIN tickets ON (gfx.task_id = tickets.task_id)
          INNER JOIN tasks ON gfx.task_id = tasks.id AND tasks.plugin_uid = "' . Task::SERVICE . '"
        WHERE
          gfx.empl_id IN (' . implode(', ', $this->getTechWithAliases()) . ')
          AND (
            EXISTS(SELECT *
                     FROM task_comments tc
                         INNER JOIN tickets ON tc.task_id = tickets.task_id
                         INNER JOIN list_contr_agr ON (list_contr_agr.id = tickets.agr_id AND list_contr_agr.paytype = 1)
                     WHERE
                       tc.task_id = gfx.task_id
                       AND tc.status_id IN ( ' . implode($skpCashDoneStatuses) . ' )
                       AND tc.datetime >= "' . $start->format(DateTime::ATOM) . '"
                       AND tc.datetime < "' . $end->format(DateTime::ATOM) . '"
            ) OR
            EXISTS(SELECT *
                     FROM task_comments tc
                        INNER JOIN tickets ON tc.task_id = tickets.task_id
                        INNER JOIN list_contr_agr ON (list_contr_agr.id = tickets.agr_id AND list_contr_agr.paytype = 2)
                     WHERE
                       tc.task_id = gfx.task_id
                       AND tc.status_id IN ( ' . implode($skpCashlessDoneStatuses) . ' )
                       AND tc.datetime >= "' . $start->format(DateTime::ATOM) . '"
                       AND tc.datetime < "' . $end->format(DateTime::ATOM) . '"
            )
          )
        GROUP BY gfx.empl_id
        ';

        $this->kpi3 = $this->getDb()->getField($sql) / $this->getPointPrice();
        return $this->kpi3;
    }

    /**
     * @return int
     */
    public function getPointPrice()
    {
        return $this->pointPrice;
    }

    public function getP4kpi()
    {
        $kpi = $this->getKPI4();

        if ($kpi >= 650) {
            $k = 1.8;
        } elseif ($kpi > 550) {
            $k = 1.6;
        } elseif ($kpi > 450) {
            $k = 1.4;
        } elseif ($kpi > 350) {
            $k = 1.2;
        } else {
            $k = 1;
        }

        return $k;
    }

    public function getKPI4()
    {
        $this->kpi4 = ($this->getProfit() + $this->getPenalties()) / $this->pointPrice;

        return $this->kpi4;
    }

    public function getKPI2()
    {
        return $this->getQualityPercent();
    }

    /**
     * Процент качества выполнения заявок (обзовон)
     * @return mixed
     */
    public function getQualityPercent()
    {
        if (is_null($this->_quality_percent)) {
            $this->setQualityPercent();
        }

        return $this->_quality_percent;
    }

    /**
     * @return mixed
     */
    public function getTech()
    {
        return $this->_tech;
    }

    /**
     * Задолженности
     * @return int
     */
    public function getDebt()
    {
        if (is_null($this->_debt)) {
            $query = '
            SELECT
                ammount
            FROM
                skp_tech_debts
            WHERE
                tech_id = ' . $this->getDb()->F($this->_tech_id) . '
            ;';
            $this->_debt = (int)$this->getDb()->getField($query);
        }

        return $this->_debt;
    }

    /**
     * @return array
     */
    public function getPenaltiesList()
    {
        if (is_null($this->_penalties_list)) {
            $this->_penalties_list = MoneyReportQueries::getTechMonthPenalty($this->getTechWithAliases(), $this->getYear(), $this->getMonth());
        }

        return $this->_penalties_list;
    }

    /**
     * На оплаченные скп заявки
     * @return ReportTask[]
     */
    public function getNotPayedSkpTasks()
    {
        return $this->_not_payed_skp_tasks;
    }

}
