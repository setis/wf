<?php

/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 * mail@artemd.ru
 * 30.12.2015
 */

namespace classes\reports42;

use classes\DBMySQL;
use JMS\Serializer\Annotation\Type;
use wf;

class ReportAllScPeriod extends ReportPeriod
{

    /**
     * @var ReportScPeriod[]
     * @Type("array<classes\reports\ReportScPeriod>")
     */
    protected $_items = array();

    public function __construct($period)
    {
        $this->db = wf::$db;

        $this->setPeriod($period);

        $sc_list = $this->getScList();
//        $sc_list = [16];

        foreach ($sc_list as $i => $sc_id) {
            $this->addItem(new ReportScPeriod($sc_id, $period));
        }
    }

    private function getScList()
    {
        $query = "SELECT
            `id`,
            `title`
        FROM
            `list_sc`
        WHERE
            `title`!='СКП' AND
            `title`!='АВР' AND
            `title`!='СМР (Росликов)' AND
            `title`!='СЦ по подключению ЮЛ' AND
            `title`!='СЦ Тверь' AND
            `title`!='центральный склад' AND
            `title`!='Тестовый СЦ'
        ORDER BY
            `title`
            ;";

        return $this->getDb()->getCell($query);
    }

    /**
     * @return DBMySQL
     */
    private function getDb()
    {
        return $this->db;
    }

}
