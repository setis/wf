<?php
/**
 * mail@artemd.ru
 * 2016-02-17
 */

namespace classes;

use classes\themes\AbstractWidgetFactory;
use classes\themes\ThemeDirector;
use classes\themes\TwozeroWidgetFactoryBuilder;
use Doctrine\ORM\EntityManager;
use Gelf\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use WF\Task\TaskManager;

/**
 * Class AbstractWF
 * @package classes
 * @deprecated will be removed in 1.0 version
 */
class AbstractWF
{

    protected static $instance;

    /**
     * @var DBMySQL
     */
    public static $db;

    /**
     * @var View
     */
    public static $view;

    /**
     * @var UserSession
     */
    public static $user;

    /**
     * @var EntityManager
     */
    public static $em;

    /**
     * @var EventDispatcherInterface
     */
    public static $ed;

    /**
     * @var AbstractWidgetFactory
     */
    public static $wf;

    /**
     * @var Logger
     */
    public static $logger;

    /**
     * @var Logger
     */
    public static $debug;

    /**
     * @var TaskManager
     */
    public static $taskManager;

    /**
     * @var ContainerInterface
     */
    public static $container;

    public static $start;

    private function __construct(ContainerInterface $container)
    {
        global $DB;

        self::$container = $container;

        self::$logger = $container->get('logger');

        self::$debug = self::$container->get('monolog.logger.debug');

        self::$em = $container->get('doctrine.orm.default_entity_manager');

        self::$ed = $container->get('event_dispatcher');

        self::$taskManager = $container->get('wf.task.task_manager');

        /**** BC ******************/

        self::$db = $container->get('wf.mysql_db');

        $DB = self::$db;
    }

    public static function init()
    {
        self::$wf = self::makeWidgetFactory();

        self::$view = Component::createObject([
            'class' => View::className()
        ]);

    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance(ContainerInterface $container = null)
    {
        if (null === static::$instance) {
            static::$instance = new static($container);
        }

        return static::$instance;
    }


    public static function getEntityManager()
    {
        return self::$container->get('doctrine.orm.entity_manager');
    }

    private static function makeWidgetFactory()
    {
        switch (getcfg('theme')) {
            default: // twozero
                $builder = new TwozeroWidgetFactoryBuilder();
        }

        $director = new ThemeDirector($builder);

        $director->buildWidgetFactory();
        return $director->getWidgetFactory();
    }

    /**
     * @return Session
     */
    public static function getSession()
    {
        return self::$container->get('session');
    }

    /**
     * @return integer|null
     */
    public static function getUserId()
    {
        return self::$user->getId();
    }

    public static function getServerName(){
        return self::$container->getParameter('backend_name');
    }

    public static function getDeveloperEmail(){
        return self::$container->getParameter('developer_email');
    }

    public static function start(){
        self::$start = microtime(true);
    }

    public static function checkpoint(){
        return microtime(true) - self::$start;
    }

}
