<?php

namespace classes\comments;

/**
 * Description of HtmlNl2BrDecorator
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class HtmlNl2BrDecorator
{
    /**
     * @var TextObject
     */
    private $textDecorator;

    /**
     * TextNl2BrDecorator constructor.
     * @param TextObject $textObject
     */
    public function __construct(TextObject $textObject)
    {
        $this->textDecorator = $textObject;
    }

    public function process()
    {
        $str = nl2br($this->textDecorator->getText());

        $this->textDecorator->setText(preg_replace('/(<[^>]+?>)<br\s\/>/u', '$1', $str));
    }
}


