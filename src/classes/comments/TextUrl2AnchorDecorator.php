<?php
/**
 * mail@artemd.ru
 * 06.03.2016
 */

namespace classes\comments;

/**
 * Class TextUrl2AnchorDecorator
 * @package classes\comments
 */
class TextUrl2AnchorDecorator implements TextDecoratorInterface
{
    /**
     * @var TextObject
     */
    private $textDecorator;

    /** @var int */
    public $length = 50;

    /**
     * @param TextObject $textObject
     */
    public function __construct(TextObject $textObject)
    {
        $this->textDecorator = $textObject;
    }

    public function process()
    {
        $text = $this->textDecorator->getText();
        #$text = preg_replace('/\b(?<!href=)((?:https?:\/\/)|(?:www\.))(\S+)\b/is', '<a href="http://$2" target="_blank">$2</a>', $text);
        $text = preg_replace('#(<\w+>)?(http[s]?:\/\/.*?)(<\/\w+>)?$#i', '<a href="$2" target="_blank">$2</a>', $text);
        $this->textDecorator->setText($text);
    }
}
