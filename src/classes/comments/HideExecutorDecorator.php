<?php
/**
 * mail@artemd.ru
 * 06.03.2016
 */

namespace classes\comments;
/**
 * Class HideExecutorDecorator
 * @package classes\comments
 */
class HideExecutorDecorator implements TextDecoratorInterface
{
    /**
     * @var TextObject
     */
    private $textDecorator;

    /**
     * TextPhonesRemoverDecorator constructor.
     * @param TextObject $textObject
     */
    public function __construct(TextObject $textObject)
    {
        $this->textDecorator = $textObject;
    }

    public function process()
    {
        $text = $this->textDecorator->getText();

        $text = preg_replace('/Исполнитель.*$/', '', $text);

        $this->textDecorator->setText($text);
    }
}
