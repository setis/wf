<?php
/**
 * EF: Общий класс по работе с комментариями.
 * 24.02.2016
 */

namespace classes\comments;

use classes\Component;

/**
 * Class CommentsDefaultStrategy
 * Processes comment with Url2Anchor decorator, Nl2Br decorator and Quote decorator
 *
 * @package classes\comments
 */
class CommentsDefaultStrategy extends Component implements CommentProcessorInterface
{
    /**
     * @param TextObject $comment
     * @return TextObject
     */
    public function purify(TextObject $comment)
    {
        (new TextUrl2AnchorDecorator($comment))->process();
        (new HtmlNl2BrDecorator($comment))->process();
        (new TextQuoteDecorator($comment))->process();

        return $comment;
    }

}
