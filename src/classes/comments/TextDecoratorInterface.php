<?php
/**
 * mail@artemd.ru
 * 06.03.2016
 */

namespace classes\comments;


interface TextDecoratorInterface
{
    public function __construct(TextObject $comment);

    public function process();
}
