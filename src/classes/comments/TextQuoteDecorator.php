<?php
/**
 * mail@artemd.ru
 * 06.03.2016
 */

namespace classes\comments;

/**
 * Class TextQuoteDecorator
 * @package classes\comments
 */
class TextQuoteDecorator implements TextDecoratorInterface
{
    /**
     * @var TextObject
     */
    private $textDecorator;

    /**
     * TextQuoteDecorator constructor.
     * @param TextObject $textObject
     */
    public function __construct(TextObject $textObject)
    {
        $this->textDecorator = $textObject;
    }

    public function process()
    {
        $text = $this->textDecorator->getText();
        $text = preg_replace("/\\\/", "&#92;", $text);
        $text = preg_replace("/(-- Автор:)(.*)(--)/is", "<br /><small><i style='color: #b0690c'>Автор: $2 </i></small>", $text);
        $this->textDecorator->setText($text);
    }
}
