<?php
/**
 * mail@artemd.ru
 * 06.03.2016
 */

namespace classes\comments;

class TextObject
{
    private $_original;

    protected $text;

    /**
     * CommentDecorator constructor.
     * @param string $text
     */
    public function __construct($text)
    {
        $this->_original = $text;
        $this->text = $text;
    }

    /**
     * Reset comment decorations
     */
    public function resetText()
    {
        $this->text = $this->_original;
    }

    /**
     * @param string $text
     */
    public function setText($text) {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}
