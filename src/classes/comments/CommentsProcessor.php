<?php
/**
 * EF: Общий класс по работе с комментариями.
 * 24.02.2016
 */
namespace classes\comments;

use classes\Component;
use classes\tools\TextProcessor;
use classes\tools\TextProcessorStrategy;

class CommentsProcessor extends Component
{
    /**
     * @var CommentProcessorInterface
     */
    private $strategy;

    public function init()
    {
        if (empty($this->strategy))
            $this->strategy = CommentsProcessor::createObject(CommentsDefaultStrategy::className());
    }

    /**
     * @param TextObject $comment
     * @return TextObject
     */
    public function purify(TextObject $comment)
    {
        return $this->strategy->purify($comment);
    }
}
