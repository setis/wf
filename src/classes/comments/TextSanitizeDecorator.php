<?php
/**
 * mail@artemd.ru
 * 06.03.2016
 */

namespace classes\comments;

use classes\web\StringHelper;

/**
 * Class TextSanitizeDecorator
 * @package classes\comments
 */
class TextSanitizeDecorator implements TextDecoratorInterface
{
    /**
     * @var TextObject
     */
    private $textDecorator;

    /**
     * @param TextObject $textObject
     */
    public function __construct(TextObject $textObject)
    {
        $this->textDecorator = $textObject;
    }

    public function process()
    {
        $text = $this->textDecorator->getText();

        $text = StringHelper::encode($text);

        $this->textDecorator->setText($text);
    }
}
