<?php
/**
 * mail@artemd.ru
 * 06.03.2016
 */

namespace classes\comments;

/**
 * Class TextNl2BrDecorator
 * @package classes\comments
 */
class TextNl2BrDecorator implements TextDecoratorInterface
{
    /**
     * @var TextObject
     */
    private $textDecorator;

    /**
     * TextNl2BrDecorator constructor.
     * @param TextObject $textObject
     */
    public function __construct(TextObject $textObject)
    {
        $this->textDecorator = $textObject;
    }

    public function process()
    {
        $this->textDecorator->setText(nl2br($this->textDecorator->getText()));
    }
}
