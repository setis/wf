<?php
/**
 * mail@artemd.ru
 * 06.03.2016
 */

namespace classes\comments;

/**
 * Class TextTagsRemoverDecorator
 * @package classes\comments
 */
class TextTagsRemoverDecorator implements TextDecoratorInterface
{
    /**
     * @var TextObject
     */
    private $textDecorator;

    public $allowed_tags = '<a><p><em><b><br><strong><font><i><ul><li><strike><sub><sup><span><div><table><tr><td><th><tbody><thead><colgroup><col>';

    /**
     * TextTagsRemoverDecorator constructor.
     * @param TextObject $textObject
     */
    public function __construct(TextObject $textObject)
    {
        $this->textDecorator = $textObject;
    }

    public function process()
    {
        $text = $this->textDecorator->getText();
//        $text = nl2br($comment);
        $text = preg_replace('/(\n){2,}/', '<br />', $text);
        //$comment = preg_replace("/<table[^>]*>.*<br ?\/>.*<\/table>/", "", $comment);
        //$text = $comment;
        $text = strip_tags($text, $this->allowed_tags);
        //// EF: Решетками закомментирована старая чистилка тегов, которая что-то там меняла криво
        ###$text = preg_replace('/\b(?:(http(s?):\/\/)|(?=www\.))(\S+)/is', '<a href="http$2://$3" target="_blank">$1$3</a>', $text);
        $text = preg_replace('/.*(?<!href=)(https?:\/\/)([a-z0-9-_\/?=&%.]+)\s*/is', '<a href="$1$2" target="_blank">$2</a>', $text);
        #$text=preg_replace("#(https?|ftp)://\S+[^\s.,> )\];'\"!?]#",'<a href="\\0" target="_blank">\\0</a>',$text);
        $text = preg_replace("/((^|\r?\n)&gt; [^\n]+)+/i", "<span class=quoted>\\0</span>", $text);
        $text = "<div style='overflow-y: auto; overflow-x: auto;'>" . $text . "</div>";

        $this->textDecorator->setText($text);
    }
}
