<?php
/**
 * mail@artemd.ru
 * 06.03.2016
 */

namespace classes\comments;

use classes\web\StringHelper;

/**
 * Class TextTruncateDecorator
 * @package classes\comments
 */
class TextTruncateDecorator implements TextDecoratorInterface
{
    /**
     * @var TextObject
     */
    private $textDecorator;

    /** @var int */
    public $length = 50;

    /** @var string */
    public $afterCut = '...';

    /**
     * @param TextObject $textObject
     */
    public function __construct(TextObject $textObject)
    {
        $this->textDecorator = $textObject;
    }

    public function process()
    {
        $text = $this->truncate($this->textDecorator->getText());

        $this->textDecorator->setText($text);
    }

    private function truncate($text)
    {
        $text = $text . " ";
        $text = StringHelper::substr($text, 0, $this->length);

        if( StringHelper::strlen($this->textDecorator->getText()) > StringHelper::strlen($text)) {
            $text = StringHelper::substr($text, 0, StringHelper::strrpos($text, ' '));
            $text = $text . $this->afterCut;
        }

        return $text;
    }
}
