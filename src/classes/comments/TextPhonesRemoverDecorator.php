<?php
/**
 * mail@artemd.ru
 * 06.03.2016
 */

namespace classes\comments;
use classes\User;
/**
 * Class TextPhonesRemoverDecorator
 * @package classes\comments
 */
class TextPhonesRemoverDecorator implements TextDecoratorInterface
{
    /**
     * @var TextObject
     */
    private $textDecorator;

    public $rules = array(
        # EF: эта регулярка слишком жадная, она режет даже номер комментария, что не нужно делать
        #[
        #    'pattern' => "/((8|\+?7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}/",
        #    'replacement' => " <strong>номер скрыт (1)</strong> ",
        #],
        [
            'pattern' => "/\(\d{3}\)[\- ]?[\d\- ]{7,10}/",
            'replacement' => " <strong>номер скрыт (2)</strong> ",
        ],
        [
            'pattern' => "/9\d{9}/",
            'replacement' => " <strong>номер скрыт (3)</strong> ",
        ],
        [
            'pattern' => "/(?:8|\+?7){1}[0-9]{10}/",
            'replacement' => " <strong>номер скрыт (4)</strong> ",
        ],
        [
            'pattern' => "/АТС:\s[0-9]{3,}/",
            'replacement' => " <strong>АТС: номер скрыт (5)</strong> ",
        ],
        [
            'pattern' => "/Номер абонента:\s[0-9]{3,}/",
            'replacement' => " <strong>Номер абонента: номер скрыт (6)</strong> ",
        ],
        [
            'pattern' => "/Код города:\s[0-9]{3,}/",
            'replacement' => " <strong>Код города: номер скрыт (7)</strong> ",
        ],
        [
            'pattern' => "/49[59]\d{7}/",
            'replacement' => " <strong>номер скрыт (8)</strong> ",
        ],
    );

    /**
     * TextPhonesRemoverDecorator constructor.
     * @param TextObject $textObject
     */
    public function __construct(TextObject $textObject)
    {
        $this->textDecorator = $textObject;
    }

    public function process()
    {
        $text = $this->textDecorator->getText();

        if (!empty($text) && is_string($text))
            foreach ($this->rules as $rule) {
                $text = preg_replace($rule['pattern'], $rule['replacement'], $text);
            }

        $this->textDecorator->setText($text);
    }

    /**
     * Проверяет, а требуется ли декорирование для текста.
     */
    public function is_decoration_required() {
        global $USER;
        $a = false;
        if ($USER->checkAccess("hidetelnum") != User::ACCESS_NONE ||
             (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()))
        ) {
            $a = true;
        }
        return $a;
    }

    /**
     * Декорирует текст, если требуется
     */
    public function apply_decoration() {
        if ($this->is_decoration_required()) {
            $this->process();
        }
    }
}
