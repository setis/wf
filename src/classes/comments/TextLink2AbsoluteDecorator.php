<?php
namespace classes\comments;

/**
 * Class TextLink2AbsoluteDecorator
 * @package classes\comments
 */
class TextLink2AbsoluteDecorator implements TextDecoratorInterface
{
    /**
     * @var TextObject
     */
    private $textObject;

    /**
     * @param TextObject $textObject
     */
    public function __construct(TextObject $textObject)
    {
        $this->textObject = $textObject;
    }

    public function process()
    {
        $text = $this->textObject->getText();
        $text = preg_replace('/href=\'\/(comments\/getfile[^\']*)\'/is', 'href=\''.trim(getcfg('http_base'),'/').'/$1\'', $text);
        $this->textObject->setText($text);
    }
}
