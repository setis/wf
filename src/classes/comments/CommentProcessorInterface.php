<?php
/**
 * EF: Общий класс по работе с комментариями.
 * 24.02.2016
 */
namespace classes\comments;

interface CommentProcessorInterface
{
    /**
     * @param TextObject $comment
     * @return TextObject
     */
    public function purify(TextObject $comment);
}

