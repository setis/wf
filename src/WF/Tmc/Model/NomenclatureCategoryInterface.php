<?php

namespace WF\Tmc\Model;

/**
 * Description of NomenclatureCategoryInterface
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
Interface NomenclatureCategoryInterface
{
    public function getCategoryName();
}
