<?php

namespace WF\Tmc\Model;

use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TmcMolInterface extends TmcUserInterface, TmcRequesterInterface
{
    /**
     * @return TmcWarehouseInterface[]|ArrayCollection
     */
    

}
