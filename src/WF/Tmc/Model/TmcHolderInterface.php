<?php

namespace WF\Tmc\Model;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TmcHolderInterface
{
    public function getId();
    
    public function getName();

//    public function getType();
}
