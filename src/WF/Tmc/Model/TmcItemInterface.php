<?php

namespace WF\Tmc\Model;

use models\TmcItemProperty;
use WF\Core\ValuableInterface;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TmcItemInterface extends ValuableInterface
{
    /**
     * @return TmcMoveInterface
     */
    public function getMove();

    /**
     * @return NomenclatureInterface
     */
    public function getNomenclature();

    /**
     * @return float
     */
    public function getQuantity();

    /**
     * @return TmcPartnerInterface
     */
    public function getPartner();

    /**
     * @return TmcItemProperty
     */
    public function getProperty();

}
