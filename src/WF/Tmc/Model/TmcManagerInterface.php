<?php

namespace WF\Tmc\Model;

use WF\Core\TaskInterface;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TmcManagerInterface
{
    /**
     * @return TmcMoveInterface
     */
    public function createMove();

    /**
     * @param TmcUserInterface $user
     * @param TmcMoveInterface $move
     * @return mixed
     */
    public function approve(TmcUserInterface $user, TmcMoveInterface $move);

    /**
     * @param TmcUserInterface $user
     * @param TmcMoveInterface $move
     * @return mixed
     */
    public function cancel(TmcUserInterface $user, TmcMoveInterface $move);

    /**
     * @param TmcHolderInterface $source
     * @param TmcHolderInterface $target
     * @param array $items
     * @param TmcUserInterface $author
     * @return mixed
     */
    public function makeRequest(TmcHolderInterface $source, TmcHolderInterface $target, array $items, TmcUserInterface $author);

    /**
     * @param TmcMolInterface $superMol
     * @param TmcHolderInterface $source
     * @param TmcHolderInterface $target
     * @param array $items
     * @return mixed
     */
    public function makeMoveBetweenWarehouses(TmcMolInterface $superMol, TmcHolderInterface $source, TmcHolderInterface $target, array $items);

    /**
     * @param TechnicianInterface $technician
     * @param TmcWarehouseInterface $wh
     * @param TmcItemInterface[] $items
     * @return TmcMoveInterface
     */
    public function makeReturnRequest(TechnicianInterface $technician, TmcWarehouseInterface $wh, array $items);

    /**
     * Передача ТМЦ технику без запроса
     *
     * @param TmcMolInterface $mol
     * @param TmcWarehouseInterface $wh
     * @param TechnicianInterface $tech
     * @param array $items
     * @return TmcMoveInterface
     */
    public function transferTo(TmcMolInterface $mol, TmcWarehouseInterface $wh, TechnicianInterface $tech, array $items);

    /**
     * Утсановка ТМЦ техником на завяку
     *
     * @param TechnicianInterface $technician
     * @param TaskInterface $task
     * @param TmcUserInterface $approvedBy
     * @param TmcItemInterface[] $items
     * @return TmcMoveInterface
     */
    public function install(TechnicianInterface $technician, TaskInterface $task, TmcUserInterface $approvedBy, array $items);

    /**
     * Снятие ТМЦ техником у клиента (с заявки)
     *
     * @param TechnicianInterface $technician
     * @param TaskInterface $task
     * @param TmcUserInterface $approvedBy
     * @param TmcItemInterface[] $items
     * @return mixed
     */
    public function uninstall(TechnicianInterface $technician, TaskInterface $task, TmcUserInterface $approvedBy, array $items);

    /**
     * @param TmcWarehouseInterface $targetWarehouse
     * @param TmcMolInterface $mol
     * @param array $items
     * @return TmcMoveInterface
     */
    public function fromPartner(TmcWarehouseInterface $targetWarehouse, TmcMolInterface $mol, array $items);

    /**
     * @param TmcWarehouseInterface $sourceWarehouse
     * @param TmcMolInterface $mol
     * @param array $items
     * @return TmcMoveInterface
     */
    public function toPartner(TmcWarehouseInterface $sourceWarehouse, TmcMolInterface $mol, array $items);

    /**
     * @param TmcWarehouseInterface $sourceWarehouse
     * @param TmcMolInterface $mol
     * @param array $items
     * @return TmcMoveInterface
     */
    public function writeOff(TmcWarehouseInterface $sourceWarehouse, TmcMolInterface $mol, array $items);

    /**
     * @param TmcMoveInterface $move
     * @param TmcUserInterface $user
     * @param array $items
     * @return mixed
     */
    public function changeItemsConsumption(TmcMoveInterface $move, TmcUserInterface $user, array $items);

    /**
     * @param TmcMoveInterface $move
     * @param TmcUserInterface $user
     * @param TmcCarrierInterface $carrier
     * @return mixed
     */
    public function defineTransporting(TmcMoveInterface $move, TmcUserInterface $user, TmcCarrierInterface $carrier);

    /**
     * @param TmcHolderInterface $holder
     * @param TmcUserInterface $mol
     * @param array $items
     * @return mixed
     */
    public function markDefect(TmcHolderInterface $holder, TmcUserInterface $mol, array $items);
}
