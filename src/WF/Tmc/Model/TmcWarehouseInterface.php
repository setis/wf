<?php

namespace WF\Tmc\Model;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TmcWarehouseInterface extends TmcHolderInterface
{
    /**
     * @return TmcMolInterface[]
     */
    public function getMols();

    /**
     * @return TechnicianInterface[]
     */
    public function getTechnicians();

    /**
     * @return TmcCoordinatorInterface[]
     */
    public function getCoordinators();

    /**
     * @return TmcMolInterface[]
     */
    public function getSuperMols();
}
