<?php

namespace WF\Tmc\Model\Importer;

use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use models\ListContr;
use models\ListMaterialCats;
use models\Nomenclature;
use models\TmcHolderItem;
use models\TmcItem;
use models\TmcItemProperty;
use repository\TmcHolderItemRepository;
use repository\TmcItemPropertyRepository;
use WF\Tmc\HaveSerialException;

/**
 * Description of TmcItemsBuilder
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TmcItemsBuilder
{
    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Nomenclature[]
     */
    private $nomenclatureCache = [];

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildSome($row)
    {
        $items = [];
        /** @var TmcHolderItemRepository $holderItemRepo */
        $holderItemRepo = $this->em->getRepository(TmcHolderItem::class);
        /** @var TmcItemPropertyRepository $itemPropertyRepo */
        $itemPropertyRepo = $this->em->getRepository(TmcItemProperty::class);
        /** @var ListContr $partner */
        $partner = $this->em->getRepository(ListContr::class)
            ->findOneBy(['contrTitle' => $row['partner']['title']]);

        $nomenclature = $this->getNomenclature($row);

        if (!$nomenclature->isMetrical()) {
            if ($row['quantity'] > 1) {
                throw new LogicException('Cant create non metrical item with quantity more than 1');
            }

            if (empty($row['serial'])) {
                throw new LogicException('Cant create non metrical item without serial');
            }

            $existingProperty = $itemPropertyRepo->findPropertyForNonMetrical($nomenclature, $partner, $row['serial']);
            if (null !== $existingProperty) {
                // Check that positive rests absent on tmc holder
                $tempItem = $holderItemRepo->findItem(new TmcItem($existingProperty));
                if (null === $tempItem || $tempItem['total'] < 1) {
                    $property = $existingProperty;
                } else {
                    throw new HaveSerialException(new TmcItem($existingProperty), $tempItem[0]->getHolder(), $tempItem['total']);
                }
            } else {
                $property = (new TmcItemProperty())
                    ->setNomenclature($nomenclature)
                    ->setPartner($partner)
                    ->setSerial($row['serial']);
            }

            $item = new TmcItem($property);
            $item->setQuantity(1);

            $items[] = $item;

        } else {
            $property = $itemPropertyRepo->findPropertyForMetrical($nomenclature, $partner, false);
            if (null === $property) {
                $property = (new TmcItemProperty())
                    ->setPartner($partner)
                    ->setNomenclature($nomenclature);
            }
            $item = new TmcItem($property);
            $item->setQuantity($row['quantity']);

            $items = [$item];
        }

        return $items;
    }

    /**
     * @param $row
     * @return Nomenclature|object
     */
    private function getNomenclature($row)
    {
        if (isset($row['id']) && !empty($row['id'])) {
            // then item added from inline form (from partner)
            return $this->em->getReference(Nomenclature::class, $row['id']);
        }

        if (isset($this->nomenclatureCache[$row['title']]))
            $this->nomenclatureCache[$row['title']];

        /** @var Nomenclature $nomenclature */
        $nomenclature = $this->nomenclatureCache[$row['title']] = $this->em->getRepository(Nomenclature::class)
            ->findOneBy(['title' => $row['title']]);

        if ($nomenclature) {
            return $nomenclature;
        }

        /* @var $category ListMaterialCats */
        $category = $this->em->getRepository(ListMaterialCats::class)
            ->findOneBy(['title' => $row['category']['title']]);

        $isMetrical = $category && ($unit = $category->getUnit()) && $unit->isMetrical();

        $nomenclature = new Nomenclature();
        $nomenclature->setTitle($row['title'])
            ->setCategory($category)
            ->setUnit($unit)
            ->setIsMetrical($isMetrical);

        $this->em->persist($nomenclature);
        $this->em->flush();

        $this->nomenclatureCache[$row['title']] = $nomenclature;

        return $nomenclature;
    }

}
