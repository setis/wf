<?php

namespace WF\Tmc\Model\Importer;

use Symfony\Component\HttpFoundation\File\File;

/**
 * Description of CsvToDataConverter
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class CsvToDataConverter
{
    public function convert(File $file)
    {
        $o = $file->openFile();
        $data = [];
        $line = $o->fgets();
        $enc = mb_detect_encoding($line, ['WINDOWS-1251', 'UTF-8']);
        $doConvert = 'UTF-8' !== $enc;
        $o->rewind();
        $fields = ['partner', 'category', 'title', 'quantity', 'serial'];
        $max = count($fields);
        while (!$o->eof()) {
            if(false === ($csv = $o->fgetcsv(';')) || count($csv) < count($fields)) {
                continue;
            }

            if(false !== $doConvert) {
                $csv = array_map(function ($msg) use ($enc) {
                    return iconv($enc, 'UTF-8', $msg);
                }, $csv);
            }

            $row = array_combine($fields, array_slice($csv, 0, $max));

            if (false !== $row) {
                $data[] = $row;
            }
        }

        return $data;
    }
}
