<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 16.06.16
 * Time: 19:31
 */

namespace WF\Tmc\Model;


use models\ListSc;

interface ChiefInterface
{
    /**
     * @param ListSc $serviceCenter
     * @return boolean
     */
    public function isServiceCenterChief(ListSc $serviceCenter);

    /**
     * @return ListSc[]
     */
    public function getSlaveServiceCenters();
}