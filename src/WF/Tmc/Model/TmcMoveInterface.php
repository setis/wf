<?php

namespace WF\Tmc\Model;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use models\TmcHolderItem;
use models\TmcHolderLink;
use models\TmcItem;
use WF\Core\TaskInterface;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 * @see \models\TmcMove
 */
interface TmcMoveInterface
{
    /**
     * Конкретное перемещение с изменениями остатков по какому то складу
     * Другие статусы не сигнализируют о изменении остатков по складу
     * Только TmcMove имеющие такой статус связываются с
     */
    const STATE_MOVED = 'moved';

    const STATE_FROM_PARTNER = 'from_partner'; // приём ТМЦ от партнёра
    const STATE_TO_PARTNER = 'to_partner'; // возврат ТМЦ партнёру
    const STATE_WRITE_OFF = 'write_off'; // списание ТМЦ со склада

    const STATE_TRANSFERRED = 'transferred'; // ПЕредача ТМЦ технику без запроса

    const STATE_REQUEST = 'request'; // запрос на ТМЦ
    const STATE_REQUEST_CONFIRMED = 'request_confirmed'; // запрос согласован Согласователем (TmcCoordinatorInterface)
    const STATE_RETURN_REQUEST = 'return_request'; // запрос на возврат техником ТМЦ на склад
    const STATE_REQUEST_APPROVED = 'request_approved'; // запрос подтверждён на выдачу МОЛом (TmcMolInterface)
    const STATE_TRANSFER_BETWEEN_WAREHOUSES = 'between_warehouses'; // Передача со склада на склад, без запроса

    const STATE_INSTALLED = 'installed'; // установка ТМЦ на заявку
    const STATE_UNINSTALLED = 'uninstalled'; // приём ТМЦ с заявки
    const STATE_MARK_DEFECT = 'defect'; // пометка брака

    const STATE_TRANSPORT_REQUEST = 'transport_request'; // перемешается ТМЦ BON VOYAGE!
    const STATE_IS_TRANSPORTED = 'is_transported'; // перемешается ТМЦ BON VOYAGE!

    const STATE_CANCELED = 'canceled'; // отмена цепочки

    const STATE_ITEMS_CHANGED = 'items_changed'; // смена набора итемов в цепочке

    public function getId();

    /**
     * @param $newState
     * @return TmcMoveInterface
     */
    public function toState($newState);

    /**
     * @param string $newState
     */
    public function setState($newState);

    /**
     * @param TmcHolderInterface $source
     * @return TmcMoveInterface
     */
    public function setSourceHolder(TmcHolderInterface $source);

    /**
     * @param TmcHolderInterface $target
     * @return TmcMoveInterface
     */
    public function setTargetHolder(TmcHolderInterface $target);

    /**
     * @param TmcUserInterface $approvedBy
     * @return TmcMoveInterface
     */
    public function setApprovedBy(TmcUserInterface $approvedBy);

    /**
     * @param TmcUserInterface $moveApprovedBy
     * @return TmcMoveInterface
     */
    public function setMoveApprovedBy(TmcUserInterface $moveApprovedBy);

    /**
     * @param TmcCoordinatorInterface $confirmedBy
     * @return TmcMoveInterface
     */
    public function setConfirmedBy(TmcCoordinatorInterface $confirmedBy);

    /**
     * @param TmcUserInterface $canceledBy
     * @return TmcMoveInterface
     */
    public function setCanceledBy(TmcUserInterface $canceledBy);

    /**
     * @param TmcCarrierInterface $carrier
     * @return TmcMoveInterface
     */
    public function setShippedBy(TmcCarrierInterface $carrier);

    /**
     * @param TmcMolInterface $requestApprovedBy
     * @return TmcMoveInterface
     */
    public function setRequestApprovedBy(TmcMolInterface $requestApprovedBy);

    /**
     * @param TmcMoveInterface $previousMove
     * @return TmcMoveInterface
     */
    public function setPreviousMove(TmcMoveInterface $previousMove);

    /**
     * @param TaskInterface $task
     * @return TmcMoveInterface
     */
    public function setTask(TaskInterface $task);

    /**
     * @param DateTime $moveDate
     * @return TmcMoveInterface
     */
    public function setMoveDate(DateTime $moveDate);

    /**
     * @param TmcItemInterface[]|ArrayCollection $items
     * @return TmcMoveInterface
     */
    public function setItems($items);

    /**
     * @return string
     */
    public function getKey();

    /**
     * @return TmcItem[]|TmcItemInterface[]|null
     */
    public function getItems();

    /**
     * @return TmcHolderItem[]|null
     */
    public function getHolderItems();

    /**
     * @return string
     */
    public function getState();

    /**
     * @return TmcMoveInterface
     */
    public function getPreviousMove();

    /**
     * @return TaskInterface Related task
     */
    public function getTask();

    /**
     * @return TmcHolderInterface
     */
    public function getSourceHolder();

    /**
     * @return TmcHolderLink
     */
    public function getSourceHolderLink();

    /**
     * @return TmcHolderInterface
     */
    public function getTargetHolder();

    /**
     * @return TmcHolderLink
     */
    public function getTargetHolderLink();

    /**
     * @return TmcMatcherInterface
     */
    public function getApprovedBy();

    /**
     * @return TmcCoordinatorInterface
     */
    public function getConfirmedBy();

    /**
     * @return TmcMatcherInterface
     */
    public function getCanceledBy();

    /**
     * @return TmcCarrierInterface
     */
    public function getShippedBy();

    /**
     * @return DateTime
     */
    public function getMoveDate();

}
