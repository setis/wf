<?php

namespace WF\Tmc\Model;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TmcAccessManagerInterface
{

    public function canApproveMove(TmcUserInterface $user, TmcMoveInterface $move);

    public function canCancelMove(TmcUserInterface $user, TmcMoveInterface $move);

    public function canMakeRequest(TmcUserInterface $user, TmcWarehouseInterface $sourceWh);
    
    public function canCreateRequest(TmcUserInterface $user, TmcMoveInterface $move);
    
    public function canConfirmRequest(TmcUserInterface $user, TmcMoveInterface $move);

    public function canMoveFromPartner(TmcUserInterface $user, TmcMoveInterface $move);

    public function canMoveToPartner(TmcUserInterface $user, TmcMoveInterface $move);

    public function canCancelRequest(TmcUserInterface $user, TmcMoveInterface $move);
}
