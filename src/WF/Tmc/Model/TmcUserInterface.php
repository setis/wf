<?php

namespace WF\Tmc\Model;

use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TmcUserInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @param TmcWarehouseInterface $wh
     * @return boolean
     */
    public function isMolOnWarehouse(TmcWarehouseInterface $wh);

    /**
     * @return boolean
     */
    public function isMol();

    /**
     * @param TmcWarehouseInterface $wh
     * @return boolean
     */
    public function isSuperMolOnWarehouse(TmcWarehouseInterface $wh);

    /**
     * @return boolean
     */
    public function isSuperMol();

    /**
     * @param TmcWarehouseInterface $wh
     * @return boolean
     */
    public function isCoordinatorOnWarehouse(TmcWarehouseInterface $wh);

    /**
     * @return boolean
     */
    public function isCoordinator();

    /**
     * @return boolean
     */
    public function isCarrier();

    /**
     * @param TmcWarehouseInterface $wh
     * @return boolean
     */
    public function isTechnicianOnWarehouse(TmcWarehouseInterface $wh);

    /**
     * @return boolean
     */
    public function isTechnician();

    /**
     * @return boolean
     */
    public function isDispatcher();

    /**
     *
     * @return ArrayCollection|TmcWarehouseInterface[]
     */
    public function getCoordinatedWarehouses();

    /**
     *
     * @return ArrayCollection|TmcWarehouseInterface[]
     */
    public function getSubWarehouses();

    /**
     * @return ArrayCollection|TmcWarehouseInterface[]
     */
    public function getSuperSubWarehouses();

    /**
     *
     * @return ArrayCollection|TmcWarehouseInterface[]
     */
    public function getWarehouses();
}
