<?php

namespace WF\Tmc\Model;

use WF\Core\MeasureUnitInterface;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface NomenclatureInterface
{
     public function getId();
    
    /**
     * @return string
     */
    public function getName();
    
    /**
     * @return MeasureUnitInterface
     */
    public function getUnit();
    
    /**
     * @return NomenclatureCategoryInterface
     */
    public function getCategoty();
    
    /**
     * @return boolean
     */
    public function isMetrical();
}
