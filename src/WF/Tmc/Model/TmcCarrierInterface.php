<?php

namespace WF\Tmc\Model;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TmcCarrierInterface extends TmcUserInterface, TmcHolderInterface
{
    
}
