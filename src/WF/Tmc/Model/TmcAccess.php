<?php
/**
 * Artem
 * 25.05.2016 17:41
 */

namespace WF\Tmc\Model;


use JMS\Serializer\Annotation as Serializer;
use models\User;

class TmcAccess
{
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $canMoveFromPartner;
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $canMoveToPartner;
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $canRequest;
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $canConfirm;
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $canApprove;
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $canCancel;
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $canCancelRequest;
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $canChangeMoveConsumption;
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $isTechnician;
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $isMol;
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $isCoordinator;
    /**
     * @var boolean
     * @Serializer\Groups({"default"})
     */
    private $isCarrier;

    /**
     * TmcAccess constructor.
     * @param TmcAccessManagerInterface $am
     * @param User $user
     * @param TmcMoveInterface $move
     */
    public function __construct(TmcAccessManagerInterface $am, User $user, TmcMoveInterface $move)
    {
        $this->isTechnician = $user->isTechnician();
        $this->isMol = $user->isMol();
        $this->isCoordinator = $user->isCoordinator();
        $this->isCarrier = $user->isCarrier();

        $this->canMoveFromPartner = $am->canMoveFromPartner($user, $move);
        $this->canMoveToPartner = $am->canMoveToPartner($user, $move);

        $this->canRequest = $am->canMakeRequest($user, $move->getSourceHolder());
        $this->canCancelRequest = $am->canCancelRequest($user, $move);

        $this->canConfirm = $am->canConfirmRequest($user, $move);
        $this->canApprove = $am->canApproveMove($user, $move);
        $this->canCancel = $am->canCancelMove($user, $move);
        $this->canChangeMoveConsumption = $am->canApproveMove($user, $move);
    }

}
