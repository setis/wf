<?php

namespace WF\Tmc\Model;
use models\ListSc;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TechnicianInterface extends TmcHolderInterface, TmcUserInterface
{
    /**
     * @return ListSc[]
     */
    public function getMasterServiceCenters();
}
