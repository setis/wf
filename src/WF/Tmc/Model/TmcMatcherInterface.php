<?php
/**
 * mail@artemd.ru
 * 20.05.2016
 */

namespace WF\Tmc\Model;


interface TmcMatcherInterface
{
    /**
     * @param TmcMoveInterface $move
     * @return boolean
     */
    public function canApproveMove(TmcMoveInterface $move);

    /**
     * @param TmcMoveInterface $move
     * @return boolean
     */
    public function canChangeMoveConsumption(TmcMoveInterface $move);

}
