<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 29.08.16
 * Time: 15:21
 */

namespace WF\Tmc;


use models\TmcItem;
use WF\Tmc\Model\TmcHolderInterface;
use WF\Tmc\Model\TmcItemInterface;

class HaveSerialException extends \LogicException
{
    /**
     * @var TmcItemInterface
     */
    private $item;

    /**
     * @var TmcHolderInterface
     */
    private $holder;

    /**
     * @var int
     */
    private $total;

    public function __construct(TmcItemInterface $item, TmcHolderInterface $holder, $total = 1)
    {
        $message = sprintf('Serial:'.$item->getProperty()->getSerial().' already belongs to '.$holder->getName().'!');
        $this->item = $item;
        $this->holder = $holder;
        $this->total = $total;

        parent::__construct($message, 0, null);
    }

    /**
     * @return TmcItemInterface
     */
    public function getItem():TmcItemInterface
    {
        return $this->item;
    }

    /**
     * @return TmcHolderInterface
     */
    public function getHolder(): TmcHolderInterface
    {
        return $this->holder;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

}
