<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 18.04.2016
 * Time: 10:53
 */
global $PLUGINS;

$plugin_uid = \WF\Tmc\Controller\TmcController::class;
$PLUGINS[$plugin_uid]['name'] = "ТМЦv2: Контроллер";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = \WF\Tmc\Controller\ApiController::class;
$PLUGINS[$plugin_uid]['name'] = "ТМЦv2: API";
$PLUGINS[$plugin_uid]['hidden'] = true;

$PLUGINS['tmc2']['name'] = "ТМЦv2";
$PLUGINS['tmc2']['hidden'] = false;
//$PLUGINS['tmc2']['events'] = [];


