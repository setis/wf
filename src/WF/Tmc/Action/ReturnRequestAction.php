<?php

namespace WF\Tmc\Action;

use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of RequestAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ReturnRequestAction extends AbstractTmcAction
{

    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSubWarehouses()->contains($move->getTargetHolder());
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSubWarehouses()->contains($move->getTargetHolder());
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return false;
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user === $move->getSourceHolder()
            && $user->getWarehouses()->contains($move->getTargetHolder());
    }

}
