<?php

namespace WF\Tmc\Action;

use Doctrine\ORM\EntityManagerInterface;
use models\TmcHolderItem;
use models\TmcMove;
use repository\TmcHolderItemRepository;
use repository\TmcMoveRepository;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of FromPartnerAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class FromPartnerAction extends AbstractTmcAction
{

    public function create(TmcUserInterface $user, TmcMoveInterface $move)
    {
        $em = $this->getEm();

        $nextMove = null;

        $em->transactional(
            function (EntityManagerInterface $em) use ($move, $user, &$nextMove) {
                /** @var TmcMoveRepository $moveRepo */
                $moveRepo = $em->getRepository(TmcMove::class);
                /** @var TmcHolderItemRepository $holderMoveRepo */
                $holderMoveRepo = $em->getRepository(TmcHolderItem::class);

                /** @var TmcMove $existed */
                $existed = $moveRepo->findOneBy(
                    [
                        'task'  => $move->getTask(),
                        'state' => TmcMoveInterface::STATE_UNINSTALLED,
                    ]
                );

                if (null !== $existed) {
                    $nextMove = $moveRepo->getNextMove($existed);
                    if ($nextMove->getState() !== TmcMoveInterface::STATE_MOVED) {
                        throw new \LogicException('Wrong uninstall move! Key:'.$existed->getKey());
                    }

                    foreach ($existed->getItems() as $item) {
                        $em->remove($item);
                    }

                    foreach ($nextMove->getItems() as $item) {
                        $em->remove($item);
                    }

                    foreach ($holderMoveRepo->findBy(['move' => $nextMove]) as $item) {
                        $em->remove($item);
                    }

                    $em->flush();

                    // check serial
                    foreach ($move->getItems() as $item) {
                        $found = $holderMoveRepo->findItem($item);
                        if ($found && $found[0][1] < 1) {
                            throw new \LogicException('Duplicated serial number:'.$item->getSerial().'!');
                        }
                    }

                    $existed->setItems($move->getItems());
                    $em->persist($existed);

                    $nextMove->setItems($move->getItems());
                    $em->persist($nextMove);

                    $this->createTmcHolderItems($nextMove);
                    $em->persist($nextMove);

                    $em->flush();

                } else {
                    $nextMove = parent::create($user, $move);
                }
            }
        );

        return $nextMove;
    }

    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSuperSubWarehouses()->contains($move->getTargetHolder());
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSuperSubWarehouses()->contains($move->getTargetHolder());
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSuperSubWarehouses()->contains($move->getTargetHolder());
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSuperSubWarehouses()->contains($move->getTargetHolder());
    }

}
