<?php

namespace WF\Tmc\Action;

use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of WriteOffAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class WriteOffAction extends AbstractTmcAction
{
    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSuperSubWarehouses()->contains($move->getSourceHolder());
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSuperSubWarehouses()->contains($move->getSourceHolder());
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSuperSubWarehouses()->contains($move->getSourceHolder());
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSuperSubWarehouses()->contains($move->getSourceHolder());
    }
}
