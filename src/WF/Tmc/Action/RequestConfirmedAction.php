<?php

namespace WF\Tmc\Action;

use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TechnicianInterface;
use WF\Tmc\Model\TmcUserInterface;
use WF\Tmc\Model\TmcWarehouseInterface;

/**
 * Description of RequestConfirmedAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class RequestConfirmedAction extends AbstractTmcAction
{
    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        // Для передачи технику все серийники у неметрических должны быть проставлены
        if ($move->getTargetHolder() instanceof TechnicianInterface) {
            foreach ($move->getItems() as $item) {
                if (!$item->getNomenclature()->isMetrical()
                    && ($item->getQuantity() > 1 || empty($item->getSerial()))
                ) {
                    return false;
                }
            }
        }

        return $user->getSubWarehouses()->contains($move->getSourceHolder());
    }

    protected function doApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        $approvedMove = $move
            ->toState(TmcMoveInterface::STATE_REQUEST_APPROVED)
            ->setRequestApprovedBy($user);
        $this->getEm()->persist($approvedMove);

        return parent::doApprove($user, $approvedMove);
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getCoordinatedWarehouses()->contains($move->getSourceHolder())
            || $user->getSubWarehouses()->contains($move->getSourceHolder());
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSubWarehouses()->contains($move->getSourceHolder());
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user === $move->getTargetHolder()
            || $user->getSubWarehouses()->contains($move->getTargetHolder());
    }

    public function canDefineTransporting(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSubWarehouses()->contains($move->getSourceHolder())
        && $move->getTargetHolder() instanceof TmcWarehouseInterface;
    }

}
