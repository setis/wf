<?php

namespace WF\Tmc\Action;

use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of RequestAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class RequestRequestAction extends AbstractTmcAction
{

    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        $whs = $user->getCoordinatedWarehouses();
        $src = $move->getSourceHolder();

        return $whs->contains($src);
    }

    protected function doApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        $nextMove = $move
            ->toState(TmcMoveInterface::STATE_REQUEST_CONFIRMED)
            ->setConfirmedBy($user);

        $this->getEm()->persist($nextMove);
        $this->getEm()->flush();

        $this->dispatchStateChanged($nextMove, $move);

        return $nextMove;
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getCoordinatedWarehouses()->contains($move->getSourceHolder());
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getCoordinatedWarehouses()->contains($move->getSourceHolder())
        || $user->getSubWarehouses()->contains($move->getTargetHolder());
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSubWarehouses()->contains($move->getTargetHolder());
    }

}
