<?php

namespace WF\Tmc\Action;

use Exception;
use WF\Tmc\Model\TechnicianInterface;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;
use WF\Tmc\Model\TmcWarehouseInterface;

/**
 * Description of RequestAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TransferBetweenWarehousesAction extends AbstractTmcAction
{
    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $move->getTargetHolder()->getMols()->contains($user);
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $this->createAction($move)->canCancel($user, $move)
        || $user->getWarehouses()->contains($move->getSourceHolder());
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $this->createAction($move)->canChangeConsumption($user, $move);
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSubWarehouses()->contains($move->getSourceHolder());
    }

    public function canDefineTransporting(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $this->createAction($move)->canDefineTransporting($user, $move);
    }

    private function createAction(TmcMoveInterface $move)
    {
        if ($move->getState() === TmcMoveInterface::STATE_TRANSFER_BETWEEN_WAREHOUSES) {
            return new RequestConfirmedAction($this->getEm(), $this->getDispatcher());
        }

        throw new Exception('Unsupported state: ' . $move->getState());
    }

}
