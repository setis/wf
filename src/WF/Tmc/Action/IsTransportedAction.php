<?php

namespace WF\Tmc\Action;

use Doctrine\ORM\EntityManagerInterface;
use WF\Tmc\Model\TmcMoveInterface as TMI;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of IsTransportedAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class IsTransportedAction extends AbstractTmcAction
{

    protected function doApprove(TmcUserInterface $user, TMI $move)
    {
        $newMove = null;
        $this->getEm()->transactional(function (EntityManagerInterface $em) use ($user, $move, &$newMove) {

            $carrier = $move->getPreviousMove()->getTargetHolder();

            $newMove = $move->toState(TMI::STATE_MOVED)
                ->setSourceHolder($carrier)
                ->setApprovedBy($user);

            $em->persist($newMove);
            $this->createTmcHolderItems($newMove);

            $em->persist($newMove);
            $em->flush();
        });

        return $newMove;
    }

    public function canApprove(TmcUserInterface $user, TMI $move)
    {
        return $user->getSubWarehouses()->contains($move->getTargetHolder());
    }

    public function canCancel(TmcUserInterface $user, TMI $move)
    {
        return false;
    }

    public function canChangeConsumption(TmcUserInterface $user, TMI $move)
    {
        return false;
    }

    public function canCreate(TmcUserInterface $user, TMI $move)
    {
        return false;
    }

    public function canDefineTransporting(TmcUserInterface $user, TMI $move)
    {
        return $user === $move->getApprovedBy();
    }

}
