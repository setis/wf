<?php

namespace WF\Tmc\Action;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use LogicException;
use models\TmcHolderItem;
use models\TmcItem;
use models\TmcItemProperty;
use models\TmcMove;
use repository\TmcItemPropertyRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Tmc\EventDispatcher\TmcMoveStateChangedEvent;
use WF\Tmc\Model\TmcCarrierInterface;
use WF\Tmc\Model\TmcItemInterface;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of AbstractTmcAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
abstract class AbstractTmcAction implements TmcActionInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    public function create(TmcUserInterface $user, TmcMoveInterface $move)
    {
        if (!$this->canCreate($user, $move)) {
            throw new Exception(sprintf('User#%d can\'t create move (%s)', $user->getId(), $move->getState()));
        }

        return $this->doCreate($user, $move);
    }

    protected function doCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        $move->setApprovedBy($user);

        $this->em->persist($move);
        $this->em->flush();

        $this->dispatchStateChanged($move);

        return $move;
    }

    public function approve(TmcUserInterface $user, TmcMoveInterface $move)
    {
        if (!$this->canApprove($user, $move)) {
            throw new Exception(sprintf('User#%d can\'t approve move (%s)', $user->getId(), $move->getState()));
        }

        return $this->doApprove($user, $move);
    }

    protected function doApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        $nextMove = $move
            ->toState(TmcMoveInterface::STATE_MOVED)
            ->setApprovedBy($user);

        $this->unsetReservation($move->getItems());

        $this->em->persist($nextMove);
        $this->createTmcHolderItems($nextMove);

        $this->em->flush();

        $this->dispatchStateChanged($nextMove, $move);

        return $nextMove;
    }

    public function cancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        $nextMove = $move
            ->toState(TmcMoveInterface::STATE_CANCELED)
            ->setCanceledBy($user);

        $this->em->persist($nextMove);

        $this->em->flush();

        $this->dispatchStateChanged($nextMove, $move);

        return $nextMove;
    }

    public function changeItems(TmcUserInterface $user, TmcMoveInterface $move, array $items = [])
    {
        $nextMove = null;
        $itemsChangingMove = null;

        $this->getEm()->transactional(function (EntityManagerInterface $em) use ($move, $user, $items, &$nextMove, &$itemsChangingMove) {
            $state = $move->getState();
            $approvedBy = $move->getApprovedBy();

            $this->unsetReservation($move->getItems());

            $itemsChangingMove = $move
                ->toState(TmcMoveInterface::STATE_ITEMS_CHANGED)
                ->setApprovedBy($user)
                ->setItems($items);

            $em->persist($itemsChangingMove);
            $em->flush();

//        $this->dispatchStateChanged($itemsChangingMove, $move);

            $nextMove = $itemsChangingMove
                ->toState($state)
                ->setApprovedBy($approvedBy);

            $em->persist($nextMove);
            $em->flush();

            $this->setReservation($itemsChangingMove->getItems(), $nextMove);

            $em->persist($nextMove);
            $em->flush();
        });

        $this->dispatchStateChanged($nextMove, $itemsChangingMove);

        return $nextMove;
    }

    public function canDefineTransporting(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return false;
    }

    public function defineTransporting(TmcUserInterface $user, TmcMoveInterface $move, TmcCarrierInterface $carrier)
    {
        $approvedMove = $move->toState(TmcMoveInterface::STATE_REQUEST_APPROVED)
            ->setApprovedBy($user);
        $this->em->persist($approvedMove);

        $newMove = $approvedMove->toState(TmcMoveInterface::STATE_TRANSPORT_REQUEST)
            ->setApprovedBy($carrier);
        $this->em->persist($newMove);

        $this->em->flush();

        return $newMove;
    }

    protected function dispatchStateChanged(TmcMoveInterface $newMove, TmcMoveInterface $oldMove = null)
    {
        if ($oldMove !== null && $newMove->getState() === $oldMove->getState()) {
            throw new LogicException('Cant change state to same');
        }

        $event = new TmcMoveStateChangedEvent($newMove, $oldMove);
        $this->dispatcher->dispatch(TmcMoveStateChangedEvent::NAME, $event);
    }

    /**
     *
     * @param TmcMove|TmcMoveInterface $move
     * @return TmcHolderItem[]
     */
    protected function createTmcHolderItems(TmcMoveInterface $move)
    {
        $sourceHolder = $move->getSourceHolder();
        $targetHolder = $move->getTargetHolder();

        $data = [];

        /**
         * Записали новые остатки
         * принимающий TmcHolderInterface
         */
        if ($targetHolder) {
            foreach ($move->getItems() as $item) {
                $holderItem = new TmcHolderItem();
                $holderItem
                    ->setProperty($item->getProperty())
                    ->setHolder($targetHolder)
                    ->setMove($move)
                    ->setQuantity($item->getQuantity());

                $this->em->persist($holderItem);
                $data['target'] = $holderItem;
            }
        }

        /**
         * Записали новые остатки
         * отадющий TmcHolderInterface
         */
        if ($sourceHolder) {
            foreach ($move->getItems() as $item) {
                $holderItem = new TmcHolderItem();
                $holderItem
                    ->setProperty($item->getProperty())
                    ->setHolder($sourceHolder)
                    ->setMove($move)
                    ->setQuantity(-$item->getQuantity());

                $this->em->persist($holderItem);
                $data['source'] = $holderItem;
            }
        }

        return $data;
    }

    /**
     *
     * @return EntityManagerInterface
     */
    protected function getEm()
    {
        return $this->em;
    }

    /**
     *
     * @return EventDispatcherInterface
     */
    protected function getDispatcher()
    {
        return $this->dispatcher;
    }

    /**
     * @param TmcItemInterface[] $items
     */
    protected function unsetReservation($items)
    {
        foreach ($items as $item) {
            if (!$item->getNomenclature()->isMetrical()) {
                $item->getProperty()->setReservation(null);
                $this->em->persist($item);
            }
        }
    }

    /**
     * @param TmcItemInterface[] $items
     * @param TmcMoveInterface $move
     */
    protected function setReservation($items, $move)
    {
        foreach ($items as $item) {
            if (!$item->getNomenclature()->isMetrical()) {
                $item->getProperty()->setReservation($move);
            }
        }
    }

}
