<?php

namespace WF\Tmc\Action;

use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of ItemsChangedAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ItemsChangedAction extends AbstractTmcAction
{
    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return true;
    }
    
    protected function doApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        $prevMove = $move->getPreviousMove();
        
        if (empty($prevMove)) {
            throw new \LogicException("Previous move for {$move->getId()} is empty");
        }
        
        $nextMove = $move
            ->toState($prevMove->getState())
            ->setApprovedBy($user);

        $this->getEm()->persist($nextMove);       
        $this->getEm()->flush();

        $this->dispatchStateChanged($nextMove, $move);

        return $nextMove;
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return true;
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return false;
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return false;
    }

}
