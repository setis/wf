<?php

namespace WF\Tmc\Action;

use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use models\TmcHolderItem;
use models\TmcMove;
use repository\TmcHolderItemRepository;
use repository\TmcMoveRepository;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of UninstalledState
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class UninstalledAction extends AbstractTmcAction
{
    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user === $move->getApprovedBy() ||
        $user->isDispatcher();
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return false;
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        /**
         * TODO Artem заявку менять нельзя только если отчётный период закрыт или прошло 100500 лет и есть выставленные акты
         */
        return false;
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        if(null === $move->getTask()) {
            return false;
        }

        if($user->isDispatcher()) {
            return true;
        }

        switch ($move->getTask()->getPluginUid()) {
            case Task::GLOBAL_PROBLEM:
                /**
                 * TODO maybe here must be some logic...
                 */
                return true;
                break;
            default:
                return $move->getTask()->getSchedule() !== null
                && $move->getTask()->getSchedule()->getTechnician() === $user;
        }
    }

    public function create(TmcUserInterface $user, TmcMoveInterface $move)
    {
        $em = $this->getEm();

        $nextMove = null;

        $em->transactional(
            function (EntityManagerInterface $em) use ($move, $user, &$nextMove) {
                /** @var TmcMoveRepository $moveRepo */
                $moveRepo = $em->getRepository(TmcMove::class);
                /** @var TmcHolderItemRepository $holderMoveRepo */
                $holderMoveRepo = $em->getRepository(TmcHolderItem::class);

                /** @var TmcMove $existed */
                $existed = $moveRepo->findOneBy(
                    [
                        'task'  => $move->getTask(),
                        'state' => TmcMoveInterface::STATE_UNINSTALLED,
                    ]
                );

                if (null !== $existed) {
                    $nextMove = $moveRepo->getNextMove($existed);
                    if ($nextMove->getState() !== TmcMoveInterface::STATE_MOVED) {
                        throw new \LogicException('Wrong uninstall move! Key:'.$existed->getKey());
                    }

                    foreach ($existed->getItems() as $item) {
                        $em->remove($item);
                    }

                    foreach ($nextMove->getItems() as $item) {
                        $em->remove($item);
                    }

                    foreach ($holderMoveRepo->findBy(['move' => $nextMove]) as $item) {
                        $em->remove($item);
                    }

                    $em->flush();

                    $existed->setItems($move->getItems());
                    $em->persist($existed);

                    $nextMove->setItems($move->getItems());
                    $em->persist($nextMove);

                    $this->createTmcHolderItems($nextMove);
                    $em->persist($nextMove);

                    $em->flush();

                } else {
                    $nextMove = parent::create($user, $move);
                }
            }
        );

        return $nextMove;
    }

    public function approve(TmcUserInterface $user, TmcMoveInterface $move)
    {
        if ($move->getState() === TmcMoveInterface::STATE_MOVED) {
            return $move;
        }

        return parent::approve($user, $move);
    }

}
