<?php

namespace WF\Tmc\Action;

use models\Task;
use models\TmcHolderItem;
use models\TmcMove;
use repository\TmcHolderItemRepository;
use repository\TmcMoveRepository;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of InstalledState
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class InstalledAction extends AbstractTmcAction
{
    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user === $move->getApprovedBy() ||
        $user->isDispatcher();
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return false;
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        /**
         * TODO Artem заявку менять нельзя только если отчётный период закрыт или прошло 100500 лет и есть выставленные акты
         */
        return false;
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        if(null === $move->getTask()) {
            return false;
        }

        if($user->isDispatcher()) {
            return true;
        }

        if($user->isMolOnWarehouse( $move->getTask()->getAnyTicket()->getServiceCenter() )) {
            return true;
        }

        switch ($move->getTask()->getPluginUid()) {
            case Task::GLOBAL_PROBLEM:
                /**
                 * TODO maybe here must some logic...
                 */
                return true;
                break;
            default:
                return $move->getTask()->getSchedule() !== null
            && $move->getTask()->getSchedule()->getTechnician() === $user;
        }
    }

    public function create(TmcUserInterface $user, TmcMoveInterface $move)
    {
        $em = $this->getEm();
        /** @var TmcMoveRepository $moveRepo */
        $moveRepo = $em->getRepository(TmcMove::class);
        /** @var TmcHolderItemRepository $holderItemRepo */
        $holderItemRepo = $em->getRepository(TmcHolderItem::class);

        /** @var TmcMove $moveWithInstalledState */
        $moveWithInstalledState = $moveRepo->createQueryBuilder('move')
            ->join('move.sourceHolderLink', 'l')
            ->where('move.task =:task AND move.state = :state AND l.technician = :technician')
            ->setParameters([
                'task'       => $move->getTask(),
                'state'      => TmcMoveInterface::STATE_INSTALLED,
                'technician' => $move->getSourceHolder(),
            ])
            ->getQuery()->getOneOrNullResult();

        if (null !== $moveWithInstalledState) {
            $moveWithMovedState = $moveRepo->getNextMove($moveWithInstalledState);

            if ($moveWithMovedState->getState() !== TmcMoveInterface::STATE_MOVED)
                throw new \LogicException('This installation move not completed yet! Key:' . $moveWithInstalledState->getKey());

            $em->beginTransaction();
            /** Clean items from moves */
            try {
                foreach ($moveWithInstalledState->getItems() as $item) {
                    $em->remove($item);
                }
                $em->flush();

                foreach ($moveWithMovedState->getItems() as $item) {
                    $em->remove($item);
                }
                $em->flush();

                foreach ($holderItemRepo->findBy(['move' => $moveWithMovedState]) as $item) {
                    $em->remove($item);
                }

                $em->flush();

                $moveWithInstalledState->setItems($move->getItems());
                $moveWithMovedState->setItems($move->getItems());
                $this->createTmcHolderItems($moveWithMovedState);

                $em->persist($moveWithInstalledState);
                $em->persist($moveWithMovedState);

                $em->flush();
                $em->commit();
            } catch (\Exception $e) {
                $em->rollback();
                throw $e;
            }

        } else {
            $moveWithMovedState = parent::create($user, $move);
        }

        return $moveWithMovedState;
    }

    public function approve(TmcUserInterface $user, TmcMoveInterface $move)
    {
        if ($move->getState() === TmcMoveInterface::STATE_MOVED)
            return $move;

        return parent::approve($user, $move);
    }

}
