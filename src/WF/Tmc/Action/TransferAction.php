<?php

namespace WF\Tmc\Action;

use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of TransferAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TransferAction extends AbstractTmcAction
{
    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        // Запрет передачи технику неметрических ТМЦ без серийников
        foreach ($move->getHolderItems() as $item) {
            if (!$item->getNomenclature()->isMetrical()
                && ($item->getQuantity() > 1 || empty($item->getSerial()) )
            ) {
                return false;
            }
        }
        return $user === $move->getTargetHolder();
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user === $move->getTargetHolder()
        || $user->getSubWarehouses()->contains($move->getSourceHolder());
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSubWarehouses()->contains($move->getSourceHolder());
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->isMol()
        && $user->getSubWarehouses()->contains($move->getSourceHolder());
    }

}
