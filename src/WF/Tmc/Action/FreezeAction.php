<?php

namespace WF\Tmc\Action;

use LogicException;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of CanceledAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class FreezeAction implements TmcActionInterface
{
    public function approve(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return new LogicException('Cant change move');
    }

    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return false;
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return false;
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return false;
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return false;
    }

    public function cancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return new LogicException('Cant change move');
    }

    public function changeItems(TmcUserInterface $user, TmcMoveInterface $move, array $items)
    {
        return new LogicException('Cant change move');
    }

    public function create(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return new LogicException('Cant change move');
    }

    public function canDefineTransporting(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return false;
    }

    public function defineTransporting(TmcUserInterface $user, TmcMoveInterface $move, \WF\Tmc\Model\TmcCarrierInterface $carrier)
    {
        return new LogicException('Cant change move');
    }

//put your code here
}
