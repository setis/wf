<?php
/**
 * mail@artemd.ru
 * 06.09.2016
 */

namespace WF\Tmc\Action;


use Doctrine\ORM\EntityManagerInterface;
use models\TmcHolderItem;
use models\TmcMove;
use repository\TmcHolderItemRepository;
use repository\TmcMoveRepository;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

class MarkDefectAction extends AbstractTmcAction
{
    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSubWarehouses()->contains($move->getTargetHolder())
        || ($user === $move->getTargetHolder() && $user === $move->getSourceHolder());
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSubWarehouses()->contains($move->getTargetHolder())
        || ($user === $move->getTargetHolder() && $user === $move->getSourceHolder());
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSubWarehouses()->contains($move->getTargetHolder())
        || ($user === $move->getTargetHolder() && $user === $move->getSourceHolder());
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $user->getSubWarehouses()->contains($move->getTargetHolder())
            || ($user === $move->getTargetHolder() && $user === $move->getSourceHolder());
    }
}
