<?php

namespace WF\Tmc\Action;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Tmc\Model\TmcMoveInterface as TMI;

/**
 * Description of ActionBuilder
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ActionBuilder
{
    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param TMI $move
     * @return TmcActionInterface
     * @throws \Exception
     */
    public function build(TMI $move)
    {
        switch ($move->getState()) {
            case TMI::STATE_FROM_PARTNER:
                return new FromPartnerAction($this->em, $this->dispatcher);

            case TMI::STATE_TO_PARTNER:
                return new ToPartnerAction($this->em, $this->dispatcher);

            case TMI::STATE_TRANSFERRED:
                return new TransferAction($this->em, $this->dispatcher);

            case TMI::STATE_RETURN_REQUEST:
                return new ReturnRequestAction($this->em, $this->dispatcher);

            case TMI::STATE_REQUEST:
            case TMI::STATE_REQUEST_CONFIRMED:
                return new RequestAction($this->em, $this->dispatcher);

            case TMI::STATE_TRANSFER_BETWEEN_WAREHOUSES:
                return new TransferBetweenWarehousesAction($this->em, $this->dispatcher);

            case TMI::STATE_ITEMS_CHANGED:
                return new ItemsChangedAction($this->em, $this->dispatcher);

            case TMI::STATE_TRANSPORT_REQUEST:
                return new TransportRequestAction($this->em, $this->dispatcher);

            case TMI::STATE_IS_TRANSPORTED:
                return new IsTransportedAction($this->em, $this->dispatcher);

            case TMI::STATE_INSTALLED:
                return new InstalledAction($this->em, $this->dispatcher);

            case TMI::STATE_UNINSTALLED:
                return new UninstalledAction($this->em, $this->dispatcher);

            case TMI::STATE_MARK_DEFECT:
                return new MarkDefectAction($this->em, $this->dispatcher);

            case TMI::STATE_WRITE_OFF:
                return new WriteOffAction($this->em, $this->dispatcher);

            case TMI::STATE_MOVED:
            case TMI::STATE_CANCELED:
                return new FreezeAction();
        }

        throw new \Exception('Unsupported state ' . $move->getState());
    }
}
