<?php

namespace WF\Tmc\Action;

use WF\Tmc\Model\TmcCarrierInterface;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TmcActionInterface
{
    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move);
    
    public function create(TmcUserInterface $user, TmcMoveInterface $move);
    
    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move);
    
    public function approve(TmcUserInterface $user, TmcMoveInterface $move);
    
    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move);
    
    public function cancel(TmcUserInterface $user, TmcMoveInterface $move);
    
    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move);
    
    public function changeItems(TmcUserInterface $user, TmcMoveInterface $move, array $items);
    
    public function canDefineTransporting(TmcUserInterface $user, TmcMoveInterface $move);
    
    public function defineTransporting(TmcUserInterface $user, TmcMoveInterface $move, TmcCarrierInterface $carrier);
}
