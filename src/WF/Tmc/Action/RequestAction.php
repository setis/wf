<?php

namespace WF\Tmc\Action;

use Exception;
use WF\Tmc\Model\TechnicianInterface;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;
use WF\Tmc\Model\TmcWarehouseInterface;

/**
 * Description of RequestAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class RequestAction extends AbstractTmcAction
{
    public function canApprove(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $this->createAction($move)->canApprove($user, $move);
    }

    public function approve(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $this->createAction($move)->approve($user, $move);
    }

    public function canCancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $this->createAction($move)->canCancel($user, $move)
        || $user->getWarehouses()->contains($move->getSourceHolder());
    }

    public function canChangeConsumption(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $this->createAction($move)->canChangeConsumption($user, $move) /*|| $user->getWarehouses()->contains($move->getSourceHolder())*/
            ;
    }

    public function canCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $this->createAction($move)->canCreate($user, $move);
    }

    public function canDefineTransporting(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $this->createAction($move)->canDefineTransporting($user, $move);
    }

    private function createAction(TmcMoveInterface $move)
    {
        $source = $move->getSourceHolder();

        // У склада нет координатора - нех конфирмить, бро!
        $skipConfirm = $source instanceof TmcWarehouseInterface && count($source->getCoordinators()) === 0;

        if ($move->getState() === TmcMoveInterface::STATE_REQUEST_CONFIRMED
            || $move->getTargetHolder() instanceof TechnicianInterface
            || $skipConfirm
        ) {
            return new RequestConfirmedAction($this->getEm(), $this->getDispatcher());
        } else if ($move->getState() === TmcMoveInterface::STATE_REQUEST) {
            return new RequestRequestAction($this->getEm(), $this->getDispatcher());
        }

        throw new Exception('Unsupported state: ' . $move->getState());
    }

    protected function doCreate(TmcUserInterface $user, TmcMoveInterface $move)
    {
        parent::doCreate($user, $move);
    }

}
