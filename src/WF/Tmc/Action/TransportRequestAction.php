<?php

namespace WF\Tmc\Action;

use Doctrine\ORM\EntityManagerInterface;
use WF\Tmc\Model\TmcMoveInterface as TMI;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of TransportRequestAction
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TransportRequestAction extends AbstractTmcAction
{
    public function canApprove(TmcUserInterface $user, TMI $move)
    {
        return $move->getApprovedBy() === $user;
    }

    protected function doApprove(TmcUserInterface $user, TMI $move)
    {
        $newMove = null;
        $this->getEm()->transactional(function (EntityManagerInterface $em) use ($user, $move, &$newMove) {
            $originalTarget = $move->getTargetHolder();
            $moveToCarrier = $move->toState(TMI::STATE_MOVED)
                ->setTargetHolder($user)
                ->setApprovedBy($user);

            $em->persist($moveToCarrier);
            $this->createTmcHolderItems($moveToCarrier);

            $em->persist($moveToCarrier);
            $em->flush();

            $newMove = $moveToCarrier->toState(TMI::STATE_IS_TRANSPORTED)
                ->setTargetHolder($originalTarget)
                ->setApprovedBy($move->getApprovedBy());

            $em->persist($newMove);
            $em->flush();
        });

        return $newMove;
    }

    public function canCancel(TmcUserInterface $user, TMI $move)
    {
        return $user->getSubWarehouses()->contains($move->getSourceHolder())
        /** Только выбранный курьре может отмениьт запрос на транспортировку */
        || ($user->isCarrier() && $user === $move->getApprovedBy());
    }

    public function cancel(TmcUserInterface $user, TMI $move)
    {
        $newMove = $move->toState($move->getPreviousMove()->getState())
            ->setApprovedBy($user);

        $this->getEm()->persist($newMove);
        $this->getEm()->flush();

        return $newMove;
    }

    public function canChangeConsumption(TmcUserInterface $user, TMI $move)
    {
        return false;
    }

    public function canCreate(TmcUserInterface $user, TMI $move)
    {
        return false;
    }

    public function canDefineTransporting(TmcUserInterface $user, TMI $move)
    {
        return $user->getSubWarehouses()->contains($move->getSourceHolder());
    }
}
