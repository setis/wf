<?php

namespace WF\Tmc\Doctrine;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use models\TmcHolderItem;
use repository\TmcHolderItemRepository;
use WF\Tmc\NegativeBalanceException;


/**
 * Description of CheckHolderMoveEventListener
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class CheckHolderMoveEventListener
{
    public function prePersist(LifecycleEventArgs $event)
    {
        $obj = $event->getObject();
        if ($obj instanceof TmcHolderItem) {
            /** @var TmcHolderItemRepository $holderItemRepo */
            $holderItemRepo = $event->getObjectManager()->getRepository(TmcHolderItem::class);
            $sum = $holderItemRepo->getBalance($obj);

            if ($sum + $obj->getQuantity() < 0) {
                throw new NegativeBalanceException($obj->getProperty(), $obj->getNomenclature(), $obj->getPartner(), $sum, $obj->getQuantity());
            }
        }
    }
}
