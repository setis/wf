<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 29.08.16
 * Time: 15:07
 */

namespace WF\Tmc\Doctrine;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use models\TmcItemProperty;
use repository\TmcItemPropertyRepository;
use WF\Tmc\DuplicatedSerialException;

class CheckDuplicatedSerialsEventListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $obj = $args->getObject();

        if (!$obj instanceof TmcItemProperty || null === $obj->getSerial()) {
            return;
        }

        $repo = $args->getObjectManager()->getRepository(TmcItemProperty::class);

        $this->check($obj, $repo);

    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $obj = $args->getObject();

        if (!$obj instanceof TmcItemProperty || null === $obj->getSerial()) {
            return;
        }

        $repo = $args->getObjectManager()->getRepository(TmcItemProperty::class);

        $this->check($obj, $repo);
    }

    /**
     * @param TmcItemProperty $item
     * @param TmcItemPropertyRepository $repo
     * @throws DuplicatedSerialException
     */
    private function check(TmcItemProperty $item, TmcItemPropertyRepository $repo)
    {
        $existed = $repo->findOneBy(['serial' => $item->getSerial()]);
        if (null !== $existed && $existed !== $item) {
            throw new DuplicatedSerialException($item);
        }
    }
}
