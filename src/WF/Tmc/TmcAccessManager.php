<?php

namespace WF\Tmc;

use models\TmcMove;
use WF\Tmc\Model\TmcAccessManagerInterface;
use WF\Tmc\Model\TmcMoveInterface as tmi;
use WF\Tmc\Model\TmcUserInterface;
use WF\Tmc\Model\TmcWarehouseInterface;

/**
 * Description of TmcAccessManager
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TmcAccessManager implements TmcAccessManagerInterface
{
    public function canCreateFromPartner(TmcUserInterface $user)
    {
        return $user->isMol();
    }

    /**
     * @param TmcUserInterface $user
     * @param tmi|TmcMove $move
     * @return bool|void
     */
    public function canApproveMove(TmcUserInterface $user, tmi $move)
    {
        switch ($move->getState()) {
            case tmi::STATE_MOVED:
            case tmi::STATE_CANCELED:
                return false;

            case tmi::STATE_FROM_PARTNER:
                return $user->getSubWarehouses()->contains($move->getTargetHolder());

            case tmi::STATE_TO_PARTNER:
                return $move->getSourceHolder() instanceof TmcWarehouseInterface && $move->getSourceHolder()->getMols()->contains($user);

            case tmi::STATE_WRITE_OFF:
                return $user->getSubWarehouses()->contains($move->getSourceHolder());

            case tmi::STATE_TRANSFERRED:
                return $user === $move->getTargetHolder();

            case tmi::STATE_REQUEST:
                return ($user->isMol() && $user->getSubWarehouses()->contains($move->getTargetHolder())
                    || ($user->isTechnician()) && $user === $move->getTargetHolder());

            case tmi::STATE_REQUEST_CONFIRMED:
                return $user->getCoordinatedWarehouses()->contains($move->getTargetHolder());

            case tmi::STATE_REQUEST_APPROVED:
                return $user->getSubWarehouses()->contains($move->getTargetHolder());

            case tmi::STATE_IS_TRANSPORTED:
                return $user->isCarrier();

            case tmi::STATE_ITEMS_CHANGED:
                return ($user->isMol() && $user->getSubWarehouses()->contains($move->getSourceHolder()))
                || $user->getCoordinatedWarehouses()->contains($move->getTargetHolder());
        }

        return false;
    }

    public function canCancelMove(TmcUserInterface $user, tmi $move)
    {
        switch ($move->getState()) {
            case tmi::STATE_MOVED:
            case tmi::STATE_CANCELED:
                return false;

            case tmi::STATE_FROM_PARTNER:
                return $user->getSubWarehouses()->contains($move->getTargetHolder());

            case tmi::STATE_TO_PARTNER:
                return $move->getSourceHolder()->getMols()->contains($user);

            case tmi::STATE_TRANSFERRED:
                return $user === $move->getTargetHolder() || ($user->isMol() && $user->getSubWarehouses()->contains($move->getSourceHolder()));

            case tmi::STATE_REQUEST:
                return $user->getCoordinatedWarehouses()->contains($move->getTargetHolder())
                || $user->getSubWarehouses()->contains($move->getSourceHolder())
                || $user->getSubWarehouses()->contains($move->getTargetHolder())
                || $user === $move->getTargetHolder();

            case tmi::STATE_REQUEST_CONFIRMED:
                return $user->getCoordinatedWarehouses()->contains($move->getTargetHolder())
                || $user->getSubWarehouses()->contains($move->getSourceHolder())
                || $user->getSubWarehouses()->contains($move->getTargetHolder());

            case tmi::STATE_REQUEST_APPROVED:
                return $move->getTargetHolder() instanceof TmcUserInterface;

        }

        return false;
    }

    public function canConfirmRequest(TmcUserInterface $user, tmi $move)
    {
        return $user->getCoordinatedWarehouses()->contains($move->getTargetHolder());
    }

    public function canCarryRequest(TmcUserInterface $user, tmi $move)
    {
        return $user->isCarrier() && $move->getState() === tmi::STATE_REQUEST_APPROVED;
    }

    public function canChangeMoveConsumption(TmcUserInterface $user, tmi $move)
    {
        return ($user->isMol() || $user->isCoordinator())
        && !($move->getState() == tmi::STATE_CANCELED || $move->getState() === tmi::STATE_MOVED);
    }

    public function canMakeRequest(TmcUserInterface $user, TmcWarehouseInterface $sourceWh)
    {
        return $user->isMol()
        || ($user->isTechnician() && $user->getWarehouses()->contains($sourceWh));
    }

    public function canCreateRequest(TmcUserInterface $user, tmi $move)
    {
        // Запрашивать может мол на свой склад или техник со своего
        return $user->isMol() && $user->getSubWarehouses()->contains($move->getTargetHolder())
        || $user->isTechnician() && $user->getWarehouses()->contains($move->getSourceHolder());
    }


    public function canMoveFromPartner(TmcUserInterface $user, tmi $move)
    {
        return $user->getSubWarehouses()->contains($move->getTargetHolder());
    }

    public function canMoveToPartner(TmcUserInterface $user, tmi $move)
    {
        return $user->getSubWarehouses()->contains($move->getSourceHolder());
    }

    public function canCancelRequest(TmcUserInterface $user, tmi $move)
    {
        // или если это тот кто отправил запрос
        return $user === $move->getSourceHolder()
        // если это МОЛ, на склад которого отправили запрос
        || $user->getSubWarehouses()->contains($move->getSourceHolder())
        // если это мол которому поступил запрос на возврат
        || ($user->getSubWarehouses()->contains($move->getTargetHolder()) && $move->getState() === tmi::STATE_RETURN_REQUEST);
    }

}
