<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 29.08.16
 * Time: 15:21
 */

namespace WF\Tmc;


use models\TmcItemProperty;

class DuplicatedSerialException extends \LogicException
{
    /**
     * @var TmcItemProperty
     */
    private $item;

    public function __construct(TmcItemProperty $item)
    {
        $message = sprintf('Duplicated serial number:'.$item->getSerial().'!');
        $this->item = $item;

        parent::__construct($message, 0, null);
    }

    /**
     * @return TmcItemProperty
     */
    public function getItemProperty()
    {
        return $this->item;
    }

}
