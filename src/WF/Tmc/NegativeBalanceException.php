<?php

namespace WF\Tmc;

use Exception;
use models\TmcItemProperty;
use WF\Tmc\Model\NomenclatureInterface;
use WF\Tmc\Model\TmcPartnerInterface;


/**
 * Description of NegativeBalanceException
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NegativeBalanceException extends Exception
{
    /**
     *
     * @var NomenclatureInterface 
     */
    private $nomenclature;
    
    /**
     *
     * @var TmcPartnerInterface 
     */
    private $partner;
    
    /**
     *
     * @var int 
     */
    private $balance;
    
    /**
     *
     * @var int 
     */
    private $quantity;

    /**
     * @var TmcItemProperty
     */
    private $property;
    
    public function __construct(TmcItemProperty $property, NomenclatureInterface $nomenclature, TmcPartnerInterface $partner, $balance, $quantity)
    {
        $message = sprintf('Can\'t create holder move for "%d#%s%s" by "%d#%s". Negative balance.',
                    $nomenclature->getId(),
                    $nomenclature->getName(),
                    $property->getSerial() ? "({$property->getSerial()})" : '',
                    $partner->getId(),
                    $partner->getClientName()
        );
        $this->property = $property;
        $this->nomenclature = $nomenclature;
        $this->partner = $partner;
        $this->balance = $balance;
        $this->quantity = $quantity;
        
        parent::__construct($message, 0, null);
    }

    public function getNomenclature()
    {
        return $this->nomenclature;
    }

    public function getPartner()
    {
        return $this->partner;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getProperty()
    {
        return $this->property;
    }


}
