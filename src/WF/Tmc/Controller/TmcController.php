<?php

namespace WF\Tmc\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WF\HttpKernel\AbstractController;

/**
 * Description of TmcController
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TmcController extends AbstractController
{
    public function movesAction(Request $request) {
        return $this->renderResponse('Tmc/moves.html.php', [
            'titlePart' => 'Открытые входящие запросы',
        ]);
    }

    public function movesChainAction(Request $request) {
        $key = $request->get('key');

        return $this->renderResponse('Tmc/movesChain.html.php', [
            'titlePart' => 'Открытые входящие запросы',
            'key' => $key,
        ]);
    }

    public function dashboardAction(Request $request)
    {
        if($this->getUser()->isTechnician())
            return new RedirectResponse( '/tmc2/tech-hold' );
        else
            return new RedirectResponse( '/tmc2/service-center-hold' );
    }

    public function serviceCenterHoldAction(Request $request) {
        return $this->renderResponse('Tmc/dashboard.html.php', [
            'user' => $this->getUser()
        ]);
    }

    public function fromPartnerAction(Request $request)
    {
        $wh = $this->getUser()->getSubWarehouses();

        return $this->renderResponse('Tmc/fromPartner.html.php', [
            'wh' => $wh,
        ]);
    }

    public function techHoldAction(Request $request)
    {
        return $this->renderResponse('Tmc/techHold.html.php', [

        ]);
    }

    public function techniciansHoldAction(Request $request)
    {
        return $this->renderResponse('Tmc/techniciansHold.html.php', [

        ]);
    }

    public function reportAction(Request $request)
    {
        return $this->renderResponse('Tmc/report.html.php', [

        ]);
    }

    public function reportTotalsAction(Request $request)
    {
        return $this->renderResponse('Tmc/reportTotals.html.php', [

        ]);
    }
}
