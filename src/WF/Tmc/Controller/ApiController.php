<?php

namespace WF\Tmc\Controller;

use Doctrine\ORM\EntityManagerInterface;
use ErrorException;
use Exception;
use LogicException;
use models\ListContr;
use models\ListGpComment;
use models\ListGpReason;
use models\ListMaterialCats;
use models\ListSc;
use models\Nomenclature;
use models\Task;
use models\Ticket;
use models\TmcHolderItem;
use models\TmcItem;
use models\TmcItemProperty;
use models\TmcMove;
use models\User;
use repository\ListMaterialCatsRepository;
use repository\TmcHolderItemRepository;
use repository\TmcItemPropertyRepository;
use repository\TmcItemRepository;
use repository\TmcMoveRepository;
use repository\UserRepository;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use WF\HttpKernel\RestController;
use WF\Task\Model\TechnicalTicketInterface;
use WF\Tmc\Action\AbstractTmcAction;
use WF\Tmc\Action\ActionBuilder;
use WF\Tmc\DuplicatedSerialException;
use WF\Tmc\HaveSerialException;
use WF\Tmc\Model\Importer\TmcItemsBuilder;
use WF\Tmc\Model\TechnicianInterface;
use WF\Tmc\Model\TmcAccess;
use WF\Tmc\Model\TmcManagerInterface;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcWarehouseInterface;
use WF\Tmc\NegativeBalanceException;
use WF\Tmc\NotExistedSerialException;

/**
 * Description of ApiController
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ApiController extends RestController
{
    public function getAccessAction(Request $request)
    {
        $move_id = $request->get('move_id');

        /** @var TmcMove $move */
        $move = $this->getEm()->find(TmcMove::class, $move_id);
        $access = new TmcAccess($this->getContainer()->get('wf.tmc.access_manager'), $this->getUser(), $move);

        return $this->renderJsonResponse($access, ['default']);
    }

    public function getChainAccessAction(Request $request)
    {
        $move_id = $request->get('move_id');
        $user = $this->getUser();
        /** @var TmcMove $move */
        $move = $this->getEm()->find(TmcMove::class, $move_id);
        /** @var ActionBuilder $builder */
        $builder = $this->get('wf.tmc.action_builder');
        /** @var AbstractTmcAction $action */
        $action = $builder->build($move);

        return $this->renderJsonResponse(
            [
                'can_approve' => $action->canApprove($user, $move),
                'can_cancel' => $action->canCancel($user, $move),
                'can_change' => $action->canChangeConsumption($user, $move),
                'can_transport' => $action->canDefineTransporting($user, $move),
            ],
            ['default']
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getHistoryAction(Request $request)
    {
        /** @var TmcItemProperty $property */
        $property = $this->getEm()->find(TmcItemProperty::class, $request->get('id'));
        /** @var TmcMoveRepository $moveRepo */
        $moveRepo = $this->getEm()->getRepository(TmcMove::class);

        $history = $moveRepo->getHistoryOfProperty($property);

        return $this->renderJsonResponse($history, ['default', 'TmcMove.MoveItems', 'TmcMove.Items', 'TmcHolderItem.Property', "TmcMove.SourceHolder", "TmcMove.TargetHolder", 'TmcMove.ApprovedBy']);

    }

    public function getCarriersAction(Request $request)
    {
        $carriers = $this->getEm()
            ->getRepository(User::class)
            ->findBy(['active' => true]);

        return $this->renderJsonResponse(['items' => $carriers], ['default']);
    }

    public function getPartnersAction(Request $request)
    {
        $partners = $this->getEm()
            ->getRepository(ListContr::class)
            ->findBy(['isSupplier' => true]);

        return $this->renderJsonResponse(['items' => $partners], ['default']);
    }

    public function getNomenclatureAction(Request $request)
    {
        $result = $this->getEm()->createQueryBuilder()
            ->select('nomenclature, category')
            ->from(Nomenclature::class, 'nomenclature')
            ->join('nomenclature.category', 'category')
            ->getQuery()->getResult();

        return $this->renderJsonResponse($result, ['default', "Nomenclature.Category", "Nomenclature.Unit"]);
    }

    public function getCategoriesAction(Request $request)
    {
        /** @var ListMaterialCatsRepository $repo */
        $repo = $this->getEm()
            ->getRepository(ListMaterialCats::class);
        $result = $repo->findAllWithUnit();

        return $this->renderJsonResponse(['items' => $result], ['default']);
    }

    public function postFromPartnerAction(Request $request)
    {
        /* @var $whs TmcWarehouseInterface[] */
        $whs = $this->getUser()->getSubWarehouses();

        $whId = $request->get('wh');
        if (empty($whId)) {
            throw new Exception('Warehouse id is empty');
        }

        $wh = null;
        foreach ($whs as $w) {
            if ($w->getId() == $whId) {
                $wh = $w;
                break;
            }
        }

        if ($wh === null) {
            throw new AccessDeniedHttpException(
                sprintf(
                    'Foreign warehouse selected: %d, but [%s] available',
                    $whId,
                    implode(
                        ',',
                        array_map(
                            function (TmcWarehouseInterface $i) {
                                return $i->getId();
                            },
                            $whs
                        )
                    )
                )
            );
        }

        /* @var $tm TmcManagerInterface */
        $tm = $this->get('wf.tmc.manager');

        $builder = new TmcItemsBuilder($this->getEm());

        $data = $request->get('items');

        $move = null;

        try {
            $this->getEm()->transactional(
                function (EntityManagerInterface $em) use ($wh, $tm, $builder, $data, &$move) {
                    $items = [];
                    foreach ($data as $row) {
                        // where row[id] is ID of nomenclature
                        $items = array_merge($items, $builder->buildSome($row));
                    }
                    $move = $tm->fromPartner($wh, $this->getUser(), $items);
                }
            );
        } catch (DuplicatedSerialException $e) {
            return $this->renderJsonResponse(
                [
                    'message' => 'Cерийный номер "' . $e->getItemProperty()->getSerial() . '" уже есть в системе!',
                    'nomenclature' => $e->getItemProperty()->getNomenclature(),
                ],
                ['default'],
                Response::HTTP_BAD_REQUEST
            );
        } catch (HaveSerialException $e) {
            return $this->renderJsonResponse(
                [
                    'message' => sprintf(
                        'Cерийный номер %s уже есть на складе\у техника %s в количестве %d!',
                        $e->getItem()->getProperty()->getSerial(),
                        $e->getHolder()->getName(),
                        $e->getTotal()
                    ),
                    'nomenclature' => $e->getItem()->getNomenclature(),
                ],
                ['default'],
                Response::HTTP_BAD_REQUEST
            );
        } catch (Exception $e) {
            return $this->renderJsonResponse(
                [
                    'message' => $e->getMessage(),
                ],
                ['default'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->renderJsonResponse(['move' => $move], ['default', 'TmcMove.Items']);
    }

    public function postToPartnerAction(Request $request)
    {
        /* @var $wh TmcWarehouseInterface */
        $wh = $this->getEm()->find(ListSc::class, $request->get('wh')['id']);

        if (empty($wh)) {
            throw new ErrorException('Not selected warehouse!');
        }

        if (!$this->getUser()->getSubWarehouses()->contains($wh)) {
            throw new LogicException('You are not MOL on selected warehouse!');
        }

        /* @var $tm TmcManagerInterface */
        $tm = $this->get('wf.tmc.manager');


        $data = $request->get('items');
        if (empty($data)) {
            throw new Exception('Data is empty');
        }

        $items = [];

        foreach ($data as $row) {
            $property = $this->getPropertyById($row['property']['id']);

            $item = new TmcItem($property);

            if ($row['nomenclature']['is_metrical']) {
                $item->setQuantity($row['quantity']);
            } else {
                $item->setQuantity(1);

                if (!empty($row['serial']))
                    $property->setSerial($row['serial']);
            }

            $items[] = $item;
        }

        $move = $tm->toPartner($wh, $this->getUser(), $items);

        return $this->renderJsonResponse(['move' => $move], ['default']);
    }

    /**
     * @param $propertyId
     * @return null|TmcItemProperty
     */
    private function getPropertyById($propertyId)
    {
        /** @var TmcItemPropertyRepository $itemPropertyRepo */
        $itemPropertyRepo = $this->getEm()->getRepository(TmcItemProperty::class);

        return $itemPropertyRepo->find($propertyId);
    }

    /**
     * Остатки на складе
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getAvailableItemsAction(Request $request)
    {
        $id = $request->get('wh_id');

        /** @var ListSc $wh */
        $wh = $this->getEm()->find(ListSc::class, $id);

        if ($wh === null) {
            $content = $this->serializeJson(['error' => "Warehouse with id $id not found"], ['default']);

            return new Response($content, Response::HTTP_NOT_FOUND);
        }

        /* @var $tmcHolderMoveRepo TmcHolderItemRepository */
        $tmcHolderMoveRepo = $this->getEm()->getRepository(TmcHolderItem::class);

        if ($request->query->has('grouped')) {
            $items = $tmcHolderMoveRepo->findAvailableGroupedItems($wh);
        } elseif ($request->query->has('full')) {
            $items = $tmcHolderMoveRepo->findAvailableByWh($wh, ['full' => true]);
        } else {
            $items = $tmcHolderMoveRepo->findAvailableByWh($wh);
        }

        /** @var UserRepository $userRepo */
        $userRepo = $this->getEm()->getRepository(User::class);
        /** @var User $technicians */
        $technicians = $userRepo->getActiveTechniciansByServiceCenter($wh);

        return $this->renderJsonResponse(
            [
                'items' => $items,
                'wh' => $wh,
                'technicians' => $technicians,
            ],
            ['default', "TmcHolderItem.Partner", "TmcHolderItem.Nomenclature", "TmcHolderItem.Property", "TmcItemProperty.Reservation"]
        );
    }

    /**
     * Остатки у техника
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getAvailableTechItemsAction(Request $request)
    {
        /** @var User $tech */
        $tech = $this->getUser();

        /* @var $tmcHolderMoveRepo TmcHolderItemRepository */
        $tmcHolderMoveRepo = $this->getEm()->getRepository(TmcHolderItem::class);
        $items = $tmcHolderMoveRepo->findAvailableByTech($tech);

        return $this->renderJsonResponse(
            [
                'items' => $items,
                'holder' => $tech,
            ],
            ['default', "TmcHolderItem.Partner", "TmcHolderItem.Nomenclature", "TmcHolderItem.Property"]
        );
    }

    public function getAvailableToTechsByTaskItemsAction(Request $request)
    {
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('taskId'));
        /** @var UserRepository $userRepo */
        $userRepo = $this->getEm()->getRepository(User::class);
        /* @var $holderItemRepository TmcHolderItemRepository */
        $holderItemRepository = $this->getEm()->getRepository(TmcHolderItem::class);
        if ($task->getPluginUid() == Task::GLOBAL_PROBLEM) {
            $executors = $userRepo->getTaskExecutors($task);
        } else {
            $executors = [$task->getSchedule()->getTechnician()];
        }
        $result = [];
        foreach ($executors as $tech) {
            if ($items = $holderItemRepository->findAvailableByTech($tech)) {
                $result[] = [
                    'user' => $tech,
                    'items' => $items,
                ];
            }
        }

        return $this->renderJsonResponse($result, ['default', "TmcHolderItem.Nomenclature", "TmcHolderItem.Partner", "Nomenclature.Unit", "Nomenclature.Category"]);
    }

    public function getAvailableTechsItemsAction(Request $request)
    {
        $id = $request->get('wh_id');
        /** @var ListSc $wh */
        $wh = $this->getEm()->getReference(ListSc::class, $id);

        /** @var User $tech */
        $tech = $this->getUser();

        /* @var $tmcHolderMoveRepo TmcHolderItemRepository */
        $tmcHolderMoveRepo = $this->getEm()->getRepository(TmcHolderItem::class);
        $items = $tmcHolderMoveRepo->findAvailableForTechByWh($wh);

        return $this->renderJsonResponse(
            [
                'items' => $items,
            ],
            ['default', "TmcHolderItem.HolderLink", "HolderLink.Technician", "TmcHolderItem.Partner", "TmcHolderItem.Nomenclature", "TmcHolderItem.Property"]
        );
    }

    public function getInstalledToTaskItemsAction(Request $request)
    {
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('taskId'));

        $ticket = $task->getAnyTicket();
        if (!$ticket instanceof TechnicalTicketInterface) {
            throw new AccessDeniedHttpException('This ticket can\'t be calculated!');
        }

        $permissionsFactory = $this->getContainer()->get('wf.task.calc_permissions_factory');
        if (!$permissionsFactory->getChecker('canDoInstall')->can($this->getUser(), $task)) {
            throw new AccessDeniedHttpException(
                sprintf(
                    'User #%d (%s) cant calc task #%d',
                    $this->getUser()->getId(),
                    $this->getUser()->getLogin(),
                    $this->getUser()->getId()
                )
            );
        }

        $result = [];

        /** @var TmcItemRepository $itemRepo */
        $itemRepo = $this->getEm()->getRepository(TmcItem::class);
        /** @var TmcItem[] $items */
        $items = $itemRepo->getInstalledByTask($task);

        foreach ($items as $item) {
            $tech = $item->getMove()->getSourceHolder();

            $result[$tech->getId()]['user'] = $tech;
            $result[$tech->getId()]['items'][] = $item;
        }

        return $this->renderJsonResponse(array_values($result), ['default', 'extended', "TmcHolderItem.Nomenclature", "TmcHolderItem.Partner", "Nomenclature.Unit", "Nomenclature.Category", "TmcItem.ExtendedData"]);
    }

    public function getUninstalledFromTaskItemsAction(Request $request)
    {
        $user = $this->getUser();

        /** @var Task $task */
        /* @var $task Task */
        $task = $this->getEm()->find(Task::class, $request->get('taskId'));

        $ticket = $task->getAnyTicket();
        if (!$ticket instanceof TechnicalTicketInterface) {
            throw new AccessDeniedHttpException('This ticket can\'t be calculated!');
        }

        $permissionsFactory = $this->getContainer()->get('wf.task.calc_permissions_factory');

        if (!$permissionsFactory->getChecker('canDoUninstall')->can($user, $task)) {
            throw new AccessDeniedHttpException(
                sprintf(
                    'User #%d (%s) cant calc task #%d',
                    $user->getId(),
                    $user->getLogin(),
                    $task->getId()
                )
            );
        }

        /** @var TmcItemRepository $itemRepo */
        $itemRepo = $this->getEm()->getRepository(TmcItem::class);

        $items = $itemRepo->getUninstalledByTask($task);

        return $this->renderJsonResponse(
            [
                'items' => $items,
            ],
            ['default', 'extended', "TmcHolderItem.Nomenclature", "TmcHolderItem.Partner", "Nomenclature.Unit", "Nomenclature.Category", "TmcItem.ExtendedData"]
        );
    }

    public function getUninstallCommentAndReasonTemplatesAction(Request $request)
    {
        $reasons = $this->getEm()->getRepository(ListGpReason::class)->findAll();
        $comments = $this->getEm()->getRepository(ListGpComment::class)->findAll();

        return $this->renderJsonResponse(
            [
                'reasons' => $reasons,
                'comments' => $comments,
            ],
            ['default']
        );
    }

    /**
     * Просто список складов
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getWarehousesAction(Request $request)
    {
        $user = $this->getUser();
        $wh = [];

        if ($user->isMol() || $user->isSuperMol()) {
            $wh = $this->getEm()->getRepository(ListSc::class)->findAll();

            $molWh = $user->getSubWarehouses()->toArray();
            $superMolWh = $user->getSuperSubWarehouses()->toArray();

            $keys = [];
            $allWhs = array_filter(array_merge($molWh, $superMolWh), function (ListSc $e) use (&$keys) {
                if (in_array($e->getId(), $keys))
                    return false;

                $keys[] = $e->getId();
                return true;
            });

            return $this->renderJsonResponse(
                [
                    'items' => $wh,
                    'super_mol_items' => $superMolWh,
                    'mol_items' => $allWhs,
                ],
                ['default']
            );
        }

        if ($user->isCoordinator()) {
            $wh = $user->getCoordinatedWarehouses();
        }

        if ($user->isTechnician()) {
            $wh = $user->getWarehouses();
        }

        return $this->renderJsonResponse(['items' => $wh], ['default']);
    }

    public function getMovesAction(Request $request)
    {
        /** @var TmcMoveRepository $moveRepository */
        /* @var $moveRepository TmcMoveRepository */
        $moveRepository = $this->getEm()->getRepository(TmcMove::class);

        $moves = $moveRepository->getMoves($this->getUser());

        return $this->renderJsonResponse(['moves' => $moves], ['default', 'TmcMove.SourceHolder', 'TmcMove.TargetHolder']);
    }

    public function getMovesChainAction(Request $request)
    {
        /** @var TmcMoveRepository $moveRepository */
        /* @var $moveRepository TmcMoveRepository */
        $moveRepository = $this->getEm()->getRepository(TmcMove::class);

        $key = $request->get('key');
        $moves = $moveRepository->getMovesChain($this->getUser(), $key);

        return $this->renderJsonResponse(['moves' => $moves], ['default', 'extended', "TmcHolderItem.Partner", "TmcHolderItem.Nomenclature", "TmcHolderItem.Property", "TmcItemProperty.Nomenclature", "TmcItemProperty.Partner"]);
    }

    /**
     * Cписок складов, к которым прикреплен текущий пользователь
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getTechWarehousesAction(Request $request)
    {
        $wh = $this->getUser()->getWarehouses();

        return $this->renderJsonResponse(['items' => $wh], ['default']);
    }

    public function getMolWarehousesAction(Request $request)
    {
        $wh = $this->getUser()->getSubWarehouses();

        return $this->renderJsonResponse(['items' => $wh], ['default']);
    }

    /**
     * Принимает запрос на ТМЦ от конкретного пользователя на какой либо склад.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postTmcRequestAction(Request $request)
    {
        $data = $request->request->all();

        $target = $this->getUser();
        /** @var ListSc $source */
        $source = $this->getEm()->find(ListSc::class, $data['source_wh']['id']);
        if (!empty($data['target_wh'])) {
            $target = $this->getEm()->find(ListSc::class, $data['target_wh']['id']);
        }

        /* @var $tm TmcManagerInterface */
        $tm = $this->get('wf.tmc.manager');

        $items = [];
        foreach ($data['items'] as $row) {
            $property = $this->getPropertyById($row['property']['id']);

            $item = new TmcItem($property);

            $item->setQuantity($row['quantity']);

            if (!$row['nomenclature']['is_metrical'] && 1 === $row['quantity'])
                $property->setSerial($row['serial']);

            $items[] = $item;
        }

        $move = $tm->makeRequest($source, $target, $items, $this->getUser());

        return $this->renderJsonResponse($move, ['default']);
    }

    public function postReturnToWhRequestAction(Request $request)
    {
        $data = $request->request->all();
        /* @var $wh ListSc */
        $wh = $this->getEm()->find(ListSc::class, $data['target_wh']['id']);

        $items = [];
        foreach ($data['items'] as $row) {
            /** @var TmcItemProperty $property */
            $property = $this->getPropertyById($row['property']['id']);
            $item = new TmcItem($property);

            $item->setQuantity($row['quantity']);

            $items[] = $item;
        }

        $move = $this->getTmcManager()->makeReturnRequest($this->getUser(), $wh, $items);

        return $this->renderJsonResponse($move, ['default']);
    }

    /**
     *
     * @return TmcManagerInterface
     */
    private function getTmcManager()
    {
        return $this->get('wf.tmc.manager');
    }

    public function postTransferToTechAction(Request $request)
    {
        /* @var $mol User */
        $mol = $this->getUser();
        /* @var $wh TmcWarehouseInterface */
        $wh = $this->getEm()->find(ListSc::class, $request->request->get('wh')['id']);
        /* @var $tech User */
        $tech = $this->getEm()->find(User::class, $request->request->get('tech')['id']);

        /* @var $tm TmcManagerInterface */
        $tm = $this->get('wf.tmc.manager');

        $data = $request->get('items');
        $items = [];

        foreach ($data as $row) {
            $property = $this->getPropertyById($row['property']['id']);

            $item = new TmcItem($property);
            $item->setQuantity($row['quantity']);

            if (!$row['nomenclature']['is_metrical'] && 1 === $row['quantity']) {
                $property->setSerial($row['serial']);
            }

            $items[] = $item;
        }

        $move = $tm->transferTo($mol, $wh, $tech, $items);

        return $this->renderJsonResponse(['move' => $move], ['default']);
    }

    public function postTransferBetweenWarehousesAction(Request $request)
    {
        $data = $request->request->all();

        /** @var ListSc $source */
        $source = $this->getEm()->find(ListSc::class, $data['source']['id']);
        /** @var ListSc $target */
        $target = $this->getEm()->find(ListSc::class, $data['target']['id']);

        if (!$this->getUser()->isMolOnWarehouse($source)) {
            throw new AccessDeniedException('You can\'t send items from this warehouse!');
        }

        /* @var $tm TmcManagerInterface */
        $tm = $this->get('wf.tmc.manager');

        $items = [];
        foreach ($data['items'] as $row) {
            $property = $this->getPropertyById($row['property']['id']);

            $item = new TmcItem($property);

            $item->setQuantity($row['quantity']);

            if (!$row['nomenclature']['is_metrical'] && 1 === $row['quantity'])
                $property->setSerial($row['serial']);

            $items[] = $item;
        }

        $move = $tm->makeMoveBetweenWarehouses($this->getUser(), $source, $target, $items);

        return $this->renderJsonResponse($move, ['default']);
    }

    public function putFromPartnerFile(Request $request)
    {
        /* @var $file \Symfony\Component\HttpFoundation\File\File */
        $file = $request->files->get('items');
        $converter = new \WF\Tmc\Model\Importer\CsvToDataConverter();

        $data = $converter->convert($file);

        return $this->renderJsonResponse($data, ['default', 'extended']);
    }

    public function postNewNomenclatureAction(Request $request)
    {
        $item = $request->get('item');
        if (empty($item)) {
            throw new UnprocessableEntityHttpException();
        }

        $cat = $this->getEm()->getReference(ListMaterialCats::class, $item['category']['id']);

        $nmn = new Nomenclature();
        $nmn->setTitle($item['title'])
            ->setCategory($cat)
            ->setIsMetrical($item['is_metrical']);

        $this->getEm()->persist($nmn);
        $this->getEm()->flush();

        return $this->renderJsonResponse($nmn, ['default']);
    }

    public function postChangeItemsAction(Request $request)
    {
        /** @var TmcHolderItemRepository $holderItemRepo */
        $holderItemRepo = $this->getRepository(TmcHolderItem::class);

        $user = $this->getUser();
        $moveData = $request->get('move');
        $em = $this->getEm();
        /** @var TmcMove $move */
        $move = $em->find(TmcMove::class, $moveData['id']);
        $items = [];
        foreach ($moveData['items'] as $row) {
            try {
                /** @var TmcHolderItem $holderItem */
                $holderItem = $holderItemRepo->findItemsByNomenclatureAndPartner($this->getEm()->getReference(Nomenclature::class, $row['nomenclature']['id']),
                    $this->getEm()->getReference(ListContr::class, $row['partner']['id']),
                    $move->getSourceHolder(),
                    $row['serial']);
            } catch (NotExistedSerialException $e) {
                return $this->renderJsonResponse([
                    'message' => 'Плохой серийный номер "' . $e->getSerial() . '"!',
                    'reason' => 'not_existed_serial',
                ],
                    ['default'],
                    Response::HTTP_BAD_REQUEST
                );
            }

            $item = new TmcItem($holderItem->getProperty());
            $item->setQuantity($row['quantity']);

            $items[] = $item;
        }

        $newMove = $this->getTmcManager()->changeItemsConsumption($move, $user, $items);

        return $this->renderJsonResponse($newMove, ['default', 'extended', "TmcMove.Items", "TmcItem.ExtendedData", "TmcItem.Property", "TmcItemProperty.Nomenclature", "TmcItemProperty.Partner"]);
    }

    /**
     * @param $name
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getRepository($name)
    {
        return $this->getEm()->getRepository($name);
    }

    public function postApproveAction(Request $request)
    {
        $user = $this->getUser();
        $moveData = $request->get('move');
        /* @var $move TmcMoveInterface */
        $move = $this->getEm()->find(TmcMove::class, $moveData['id']);

        try {
            $newMove = $this->getTmcManager()->approve($user, $move);
        } catch (NegativeBalanceException $e) {
            return $this->renderJsonResponse(
                [
                    'message' => 'Недостаточное количество для перемещения',
                    'nomenclature' => $e->getNomenclature(),
                    'partner' => $e->getPartner(),
                    'balance' => $e->getBalance(),
                    'quantity' => $e->getQuantity(),
                    'property' => $e->getProperty(),
                    'reason' => 'negative_balance',
                ],
                ['default', "TmcHolderItem.Partner", "TmcHolderItem.Nomenclature", "TmcHolderItem.Property", "TmcItem.Property", "TmcItemProperty.Nomenclature", "TmcItemProperty.Partner"],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->renderJsonResponse($newMove, ['default', 'extended']);
    }

    public function postCancelAction(Request $request)
    {
        $user = $this->getUser();
        $moveData = $request->get('move');
        /* @var $move TmcMoveInterface */
        $move = $this->getEm()->find(TmcMove::class, $moveData['id']);

        $newMove = $this->getTmcManager()->cancel($user, $move);

        return $this->renderJsonResponse($newMove, ['default', 'extended', "TmcMove.Items", "TmcItem.ExtendedData", "TmcItem.Property", "TmcItemProperty.Nomenclature", "TmcItemProperty.Partner"]);
    }

    public function postDefineTransportAction(Request $request)
    {
        $user = $this->getUser();
        $moveData = $request->get('move');
        $carrierId = $request->get('carrier_id');

        /* @var $move TmcMoveInterface */
        $move = $this->getEm()->find(TmcMove::class, $moveData['id']);

        $carrier = $this->getEm()->find(User::class, $carrierId);

        $newMove = $this->getTmcManager()->defineTransporting($move, $user, $carrier);

        return $this->renderJsonResponse($newMove, ['default', 'extended']);
    }

    public function postInstallToTaskAction(Request $request)
    {
        $taskId = $request->get('task_id');
        $calc = $request->get('items');

        /** @var Task $task */
        $task = $this->getEm()->getReference(Task::class, $taskId);

        $moves = [];
        foreach ($calc as $techItems) {
            $items = $this->createItems($techItems['items']);
            $tech = $techItems['user'];

            /** @var TechnicianInterface $tech */
            $tech = $this->getEm()->find(User::class, $tech['id']);
            $moves[] = $this->getTmcManager()->install($tech, $task, $this->getUser(), $items);
        }

        return $this->renderJsonResponse(['moves' => $moves], ['default']);
    }

    /**
     *
     * @param array $requestData
     *
     * @return TmcItem[]
     */
    private function createItems(array $requestData, $allowNewProperty = false)
    {
        /** @var TmcItemPropertyRepository $itemPropertyRepo */
        $itemPropertyRepo = $this->getEm()->getRepository(TmcItemProperty::class);

        $items = [];
        foreach ($requestData as $row) {
            /** @var Nomenclature $nm */
            $nm = $this->getEm()->find(Nomenclature::class, $row['nomenclature']['id']);
            $partner = $this->getEm()->getReference(ListContr::class, $row['partner']['id']);

            $serial = empty($row['serial']) ? null : trim($row['serial']);

            if ($nm->isMetrical()) {
                $property = $itemPropertyRepo->findPropertyForMetrical($nm, $partner);
            } else {
                $property = $itemPropertyRepo->findPropertyForNonMetrical($nm, $partner, $serial);
            }

            if (null === $property) {
                if (!$allowNewProperty) {
                    throw new LogicException('You can\'t install item which you have not!');
                }

                $property = (new TmcItemProperty())
                    ->setPartner($partner)
                    ->setNomenclature($nm)
                    ->setSerial($serial);
            }

            $property
                ->setData($row['data'])
                ->setIsUninstalled(isset($row['is_uninstalled']) ? $row['is_uninstalled'] : false)
                ->setIsDefect(isset($row['is_defect']) ? $row['is_defect'] : false)
                ->setLease(isset($row['lease']) ? $row['lease'] : false);

            $item = new TmcItem($property);
            $item->setQuantity($row['quantity']);

            $items[] = $item;
        }

        return $items;
    }

    public function postUninstallFromTaskAction(Request $request)
    {
        $taskId = $request->get('task_id');
        $itemsData = $request->get('items');

        /** @var Task $task */
        $task = $this->getEm()->getReference(Task::class, $taskId);
        $items = $this->createItems($itemsData, true);

        /** @var UserRepository $userRepo */
        $userRepo = $this->getEm()->getRepository(User::class);

        if ($task->getPluginUid() == Task::GLOBAL_PROBLEM) {
            /** Just take first executor */
            $technician = array_shift($userRepo->getTaskExecutors($task));
        } else {
            $technician = $task->getSchedule()->getTechnician();
        }

        $move = $this->getTmcManager()->uninstall($technician, $task, $this->getUser(), $items);

        return $this->renderJsonResponse(['move' => $move], ['default', 'TmcMove.Items']);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getItemsBySerialAndNomenclatureAction(Request $request)
    {
        $serial = $request->get('serial');
        $nomenclature = $this->getEm()->getReference(Nomenclature::class, $request->get('nomenclature_id'));
        $partner = $this->getEm()->getReference(ListContr::class, $request->get('partner_id'));
        /** @var TmcHolderItemRepository $moveHolderRepo */
        $moveHolderRepo = $this->getEm()->getRepository(TmcHolderItem::class);

        /** @var TmcItemRepository $propertyRepo */
        $propertyRepo = $this->getEm()->getRepository(TmcItemProperty::class);

        /** @var TmcItemProperty $property */
        $property = $propertyRepo->findOneBy(['partner' => $partner, 'nomenclature' => $nomenclature, 'serial' => $serial]);
        if (null === $property)
            return $this->renderJsonResponse([], ['default']);

        $item = new TmcItem($property);
        $result = $moveHolderRepo->findItem($item);
        return $this->renderJsonResponse(($result === null) ? [] : $result, ['default']);
    }

    public function postWriteOffAction(Request $request)
    {
        /* @var $mol User */
        $mol = $this->getUser();

        /* @var $wh TmcWarehouseInterface */
        $wh = $this->getEm()->find(ListSc::class, $request->request->get('wh')['id']);

        /* @var $tm TmcManagerInterface */
        $tm = $this->get('wf.tmc.manager');

        $data = $request->get('items');
        $items = [];

        foreach ($data as $row) {
            /** @var TmcItemProperty $property */
            $property = $this->getPropertyById($row['property']['id']);

            $item = new TmcItem($property);
            $item->setQuantity($row['quantity']);

            if (!$row['nomenclature']['is_metrical'] && 1 === $row['quantity']) {
                $property->setSerial($row['serial']);
            }

            $items[] = $item;
        }

        $move = $tm->writeOff($wh, $mol, $items);

        return $this->renderJsonResponse(['move' => $move], ['default']);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function postSetSerialsAction(Request $request)
    {
        $data = $request->get('items');

        foreach ($data as $row) {
            /** @var TmcItemProperty $property */
            $property = $this->getPropertyById($row['property']['id']);
            if (!empty($row['serial'])) {
                $property->setSerial($row['serial']);
                $this->getEm()->persist($property);
            }
        }
        $this->getEm()->flush();

        return new Response();
    }

    /**
     *
     * @param Request $request
     * @return Response
     */
    public function postMarkDefectAction(Request $request)
    {
        /* @var $mol User */
        $mol = $this->getUser();

        /* @var $tm TmcManagerInterface */
        $tm = $this->get('wf.tmc.manager');

        if ($mol->isTechnician()) {
            $holder = $mol;
        } else {
            /* @var $holder TmcWarehouseInterface */
            $holder = $this->getEm()->find(ListSc::class, $request->request->get('wh')['id']);
        }


        $data = $request->get('items');
        $items = [];

        foreach ($data as $row) {
            /** @var TmcItemProperty $property */
            $property = $this->getPropertyById($row['property']['id']);

            $item = new TmcItem($property);
            $item->setQuantity($row['quantity']);

            if (!$row['nomenclature']['is_metrical'] && 1 === $row['quantity']) {
                $property->setSerial($row['serial']);
            }

            $property->setIsDefect(true);

            $items[] = $item;
        }

        $move = $tm->markDefect($holder, $mol, $items);

        return $this->renderJsonResponse(['move' => $move], ['default']);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     *
     */
    public function getReportAction(Request $request)
    {
        $filter = $request->get('filter');

        if (!empty($filter['move_date'])) {
            $fromDate = (new \DateTime($filter['move_date']))
                ->modify('+1 day')
                ->modify('first day of this month')
                ->setTime(0, 0, 0);

            $toDate = (new \DateTime($filter['move_date']))
                ->modify('+1 day')
                ->modify('last day of this month')
                ->setTime(0, 0, 0);

            $filter['dates']['from'] = $fromDate;
            $filter['dates']['to'] = $toDate;
        }

        if (!empty($filter['state']) && $state = $filter['state']['id']) {
            $filter['state'] = $filter['state']['id'];
        }

        if (!empty($filter['task_type']) && $task_type = $filter['task_type']['id']) {
            $filter['plugin_uid'] = $filter['task_type']['id'];
        }

        /* @var $repo TmcHolderItemRepository */
        $repo = $this->getRepository(TmcHolderItem::class);
        $qb = $repo->createReportQueryBuilder($filter);

        switch ($filter['state']) {
            case TmcMove::STATE_INSTALLED:
                $callback = function(TmcHolderItem $item) {
                    return [
                        'id' => $item->getId(),
                        'task_id' => $item->getMove()->getTask()->getId(),
                        'service_center' => $item->getMove()->getTask()->getAnyTicket()->getServiceCenter()->getTitle(),
                        'partner_task_id' => $item->getMove()->getTask()->getAnyTicket()->getClnttnum(),
                        'partner' => $item->getPartner()->getContrTitle(),
                        'task_partner' => (string) $item->getMove()->getTask()->getAnyTicket()->getPartner(),
                        'nomenclature' => $item->getNomenclature()->getTitle(),
                        'serial' => $item->getSerial(),
                        'quantity' => abs($item->getQuantity()),
                        'address' => $this->getAddress($item),
                        'move_date' => $item->getMove()->getMoveDate()->format('Y-m-d H:i'),
                        'holder' => $item->getMove()->getSourceHolder()->getName(),
                        'move_key' => $item->getMove()->getKey(),
                    ];
                };
                break;
            case TmcMove::STATE_UNINSTALLED:
                $callback = function(TmcHolderItem $item) {
                    return [
                        'id' => $item->getId(),
                        'task_id' => $item->getMove()->getTask()->getId(),
                        'service_center' => $item->getMove()->getTask()->getAnyTicket()->getServiceCenter()->getTitle(),
                        'partner_task_id' => $item->getMove()->getTask()->getAnyTicket()->getClnttnum(),
                        'partner' => $item->getPartner()->getContrTitle(),
                        'task_partner' => (string) $item->getMove()->getTask()->getAnyTicket()->getPartner(),
                        'nomenclature' => $item->getNomenclature()->getTitle(),
                        'serial' => $item->getSerial(),
                        'quantity' => abs($item->getQuantity()),
                        'address' => $this->getAddress($item),
                        'move_date' => $item->getMove()->getMoveDate()->format('Y-m-d H:i'),
                        'holder' => $item->getMove()->getTargetHolder()->getName(),
                        'reason' => $item->getData()['reason'],
                        'comment' => $item->getData()['comment'],
                        'move_key' => $item->getMove()->getKey(),
                    ];
                };
                break;

            default:
                throw new Exception('Unsupported state ' . $filter['state']);
        }

        $resultItems = array_map($callback, $qb->getQuery()->getResult());

        switch ($request->attributes->get('_format')) {
            default:
            case 'json':
                return $this->renderJsonResponse(
                    [
                        'items' => $resultItems,
                    ],
                    ['default']
                );

            case 'csv':
                switch ($filter['state']) {
                    case TmcMove::STATE_INSTALLED:
                        $titles = ['#','Заявка','СЦ','ID заявки партнера','Поставщик','Партнер','Наименование','S/N','К-во','Адресс','Дата','Исполнитель'];
                        break;
                    case TmcMove::STATE_UNINSTALLED:
                        $titles = ['#','Заявка','СЦ','ID заявки партнера','Поставщик','Партнер','Наименование','S/N','К-во','Адресс','Дата','Исполнитель', 'Причина', 'Комментарий'];
                        break;

                    default:
                        throw new Exception('Unsupported state ' . $filter['state']);
                }

                $csvCallback = function($row) {
                    unset($row['move_key']);
                    return $row;
                };
                return $this->renderCsvResponse(
                    $this->convertToCsv($resultItems, $titles, $csvCallback),
                    'tmc_report_' . $filter['plugin_uid'] . '_' . $filter['state'] . '_'
                );
        }
    }

    /**
     * Returns address for installed/uninstalled tmc item
     *
     * @param TmcHolderItem $item
     *
     * @return string|null
     *
     * TODO pablo: we shouldn't use shaurmans shitcode
     */
    private function getAddress(TmcHolderItem $item, $pluginUid)
    {
        $ticket = $item->getMove()->getTask()->getAnyTicket();

        if ($ticket === null || null === $dom = $ticket->getDom()) {
            return null;
        }

        if ($ticket instanceof Ticket) {
            $flat = $ticket->getKv();
        }

        $fullAddress = $dom->getFullAddress();
        $address =  $fullAddress ?: \addr_interface_plugin::decodeFullKLADR($dom->getKladrcode());
        if ($address && $flat) {
            $address .= ', кв. ' . $flat;
        }

        return $address;
    }

    public function getReportTotalsAction(Request $request)
    {

        $dateFrom = new \DateTime($request->get('date_from'));
        $dateTo = new \DateTime($request->get('date_to'));
        $whId = $request->get('warehouse');
        $techId = $request->get('tech');
        /* @var $repo TmcHolderItemRepository */
        $repo = $this->getRepository(TmcHolderItem::class);

        $baseQb = $repo->createReportTotalsBaseQueryBuilder([
            'warehouse_id' => $whId,
            'technician_id' => $techId,
        ]);

        $mainQb = clone $baseQb;
        $mainQb->andWhere('move.moveDate < :date_from')
            ->setParameter('date_from', $dateFrom);

        $inQb = clone $baseQb;
        $inQb->andWhere('move.moveDate BETWEEN :date_from AND :date_to')
            ->andWhere('hm.quantity > 0')
            ->setParameter('date_from', $dateFrom)
            ->setParameter('date_to', $dateTo);

        $outQb = clone $baseQb;
        $outQb->andWhere('move.moveDate BETWEEN :date_from AND :date_to')
            ->andWhere('hm.quantity < 0')
            ->setParameter('date_from', $dateFrom)
            ->setParameter('date_to', $dateTo);

        $main = $mainQb->getQuery()->getResult();
        $in = $inQb->getQuery()->getResult();
        $out = $outQb->getQuery()->getResult();

        foreach ($main as &$mainRow) {
            /* @var $item TmcHolderItem */
            $item = $mainRow[0];
            $mainRow[2] = 0;
            $mainRow[3] = 0;

            /* @var $inItem TmcHolderItem */
            foreach ($in as $key => $inRow) {
                $inItem = $inRow[0];
                if ($item->getNomenclature() == $inItem->getNomenclature()
                    && $item->getPartner() == $inItem->getPartner()
                ) {
                    $mainRow[2] = $inRow[1];
                    unset($in[$key]);
                }
            }
            /* @var $outItem TmcHolderItem */
            foreach ($out as $key => $outRow) {
                $outItem = $outRow[0];
                if ($item->getNomenclature() == $outItem->getNomenclature()
                    && $item->getPartner() == $outItem->getPartner()
                ) {
                    $mainRow[3] = -$outRow[1];
                    unset($out[$key]);
                }
            }
        }

        foreach ($in as &$inRow) {
            $item = $inRow[0];
            $inRow[2] = $inRow[1];
            $inRow[1] = 0;
            $inRow[3] = 0;
            /* @var $outItem TmcHolderItem */
            foreach ($out as $key => $outRow) {
                $outItem = $outRow[0];

                if ($item->getNomenclature() == $outItem->getNomenclature()
                    && $item->getPartner() == $outItem->getPartner()
                ) {
                    $inRow[3] = -$outRow[1];
                    unset($out[$key]);
                }
            }
            $main[] = $inRow;
        }

        switch ($request->attributes->get('_format')) {
            default:
            case 'json':
                return $this->renderJsonResponse(
                    [
                        'items' => $main,
                    ],
                    [
                        'default',
                        'TmcItem.Property',
                        'TmcItemProperty.Partner',
                        'TmcItemProperty.Nomenclature',
                        'TmcHolderItem.Partner',
                        'TmcHolderItem.Nomenclature',
                        'TmcHolderItem.Holder',
                        'TmcHolderItem.Move',
                        'TmcMove.Task',
                        'TmcMove.SourceHolder',
                        'TmcMove.TargetHolder',
                        'Task.Ticket',
                        'Ticket.ServiceCenter',
                    ]
                );
                break;
            case 'csv':
                return $this->renderCsvResponse($this->convertToCsv($main,['#','Категория','Наименование','Контрагент','Было','Пришло','Ушло','Остаток'],function($row):array{
                    /* @var $model TmcHolderItem */
                    $model = $row[0];
                    return [$model->getId(),$model->getNomenclature()->getCategoty()->getTitle(),$model->getNomenclature()->getTitle(),$model->getPartner()->getContrTitle(),$row[1],$row[2],$row[3],($row[1]+$row[2])-$row[3]];
                }),'tmc_report_total_');
                break;
        }


    }

    public function postItemsAutoSelectionAction(Request $request)
    {
        $data = $request->get('move');

        /* @var $move TmcMove */
        $move = $this->getEm()->find(TmcMove::class, $data['id']);
        $owner = $move->getSourceHolder();

        $newMoveItems = [];
        foreach ($data['items'] as $item) {
            $partner = $this->getEm()->getReference(ListContr::class, $item['partner']['id']);
            $nomenclature = $this->getEm()->getReference(Nomenclature::class, $item['nomenclature']['id']);

            if (!$item['is_metrical']) {
                $holderItems = $this->getTmcHolderItemRepo()->findItemsByNomenclatureAndPartner($nomenclature, $partner, $owner, $item['quantity']);
                $items = array_map(function ($holderItem) use ($move) {
                    return (new TmcItem($holderItem[0]->getProperty()))
                        ->setQuantity(1)
                        ->setMove($move);
                }, $holderItems);
                $newMoveItems = array_merge($newMoveItems, $items);
            } else {
                $property = $this->getEm()->getReference(TmcItemProperty::class, $item['property']['id']);
                $newMoveItems[] = (new TmcItem($property))
                    ->setQuantity($item['quantity'])
                    ->setMove($move);
            }
        }
        $move->getItems()->clear();
        $move->setItems($newMoveItems);

        return $this->renderJsonResponse(['move' => $move], [
            'default',
            'TmcMove.Items',
            'TmcItem.Property',
            'TmcItemProperty.Partner',
            'TmcItemProperty.Nomenclature',
            'TmcHolderItem.Partner',
            'TmcHolderItem.Nomenclature']);
    }

    /**
     *
     * @return TmcHolderItemRepository
     */
    private function getTmcHolderItemRepo()
    {
        return $this->getEm()->getRepository(TmcHolderItem::class);
    }
}
