<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 29.08.16
 * Time: 15:21
 */

namespace WF\Tmc;

class NotExistedSerialException extends \LogicException
{
    /**
     * @var string
     */
    private $serial;

    public function __construct($serial)
    {
        $message = sprintf('Not existed serial number:' . $serial . '!');
        $this->serial = $serial;

        parent::__construct($message, 0, null);
    }

    /**
     * @return string
     */
    public function getSerial()
    {
        return $this->serial;
    }

}
