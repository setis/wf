<?php
/**
 * Artem
 * 26.05.2016 12:29
 */

namespace WF\Tmc\EventDispatcher;

use models\TmcMove;
use models\User;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use WF\Notification\AbstractNotificationSubscriber;
use WF\Notification\Model\Notification;
use WF\Tmc\EventDispatcher\Selector\RecipientsSelectorFactory;
use WF\Tmc\Model\TechnicianInterface;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcWarehouseInterface;

/**
 * Class MoveStateChangedSubscriber
 * @package WF\Tmc\EventDispatcher
 */
class MoveStateChangedSubscriber extends AbstractNotificationSubscriber
{

    /**
     * @var RecipientsSelectorFactory
     */
    private $recipientsFactory;

    public static function getSubscribedEvents()
    {
        return [
            TmcMoveStateChangedEvent::NAME => 'onTmcMoveStateChanged',
        ];
    }

    /**
     * @param RecipientsSelectorFactory $factory
     */
    public function setSelectorFactory(RecipientsSelectorFactory $factory) {
        $this->recipientsFactory = $factory;

    }

    /**
     * @param TmcMoveStateChangedEvent $event
     */
    public function onTmcMoveStateChanged(TmcMoveStateChangedEvent $event)
    {
        $recipients = $this->recipientsFactory->getSelector($event->getNewMove())->getRecipients();

        $notification = new Notification();
        $notification->setTo($recipients)
            ->setSubject(empty($event->getOldMove()) ? 'Новое перемещение ТМЦ!' : 'Новый статус у движения ТМЦ: ' . TmcMove::getStateMemo($event->getNewState()));

        $htmlMessage = $this->renderMessage('Tmc/Notification/moveTmcStatusChanged.html.php', [
            'newState' => $event->getNewState(),
            'oldState' => $event->getOldState(),
            'newMove' => $event->getNewMove(),
            'oldMove' => $event->getOldMove(),
            'link' => $this->getMoveLink($event->getNewMove()),
        ]);

        $notification->setMessage($htmlMessage);
        $this->handle('email', $notification);
    }

    /**
     * @param TmcMoveInterface $move
     * @return string
     */
    private function getMoveLink(TmcMoveInterface $move)
    {
        return $this->urlGenerator->generate('tmc_moves_chain', [
            'key' => $move->getKey(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}
