<?php

namespace WF\Tmc\EventDispatcher;

use Symfony\Component\EventDispatcher\Event;
use WF\Tmc\Model\TmcMoveInterface;

/**
 * Description of TmcMoveStateChangedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TmcMoveApprovingEvent extends Event
{
    const NAME = 'tmc.move_approved';
    
    /**
     *
     * @var TmcMoveInterface
     */
    private $move;

    public function __construct(TmcMoveInterface $move)
    {
        $this->move = $move;
    }

    public function getMove()
    {
        return $this->move;
    }
}
