<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.10.16
 * Time: 13:21
 */

namespace WF\Tmc\EventDispatcher\Selector;


use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcWarehouseInterface;

class CoordinatorRecipientSelector extends AbstractRecipientSelector
{
    /**
     * @param TmcMoveInterface $move
     * @return $this
     */
    public function setMove(TmcMoveInterface $move)
    {
        if($move->getSourceHolder() instanceof TmcWarehouseInterface) {
            $this->recipients = $move->getSourceHolder()->getCoordinators();
        }

        if($move->getTargetHolder() instanceof TmcWarehouseInterface) {
            $this->recipients = $move->getTargetHolder()->getCoordinators();
        }

        return $this;
    }
}
