<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.10.16
 * Time: 13:27
 */

namespace WF\Tmc\EventDispatcher\Selector;


use WF\Tmc\Model\TechnicianInterface;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcWarehouseInterface;

class TargetHolderRecipientSelector extends AbstractRecipientSelector
{
    /**
     * @param TmcMoveInterface $move
     * @return $this
     */
    public function setMove(TmcMoveInterface $move)
    {
        if($move->getTargetHolder() instanceof TechnicianInterface) {
            $this->recipients = [$move->getTargetHolder()];
        }

        if($move->getTargetHolder() instanceof TmcWarehouseInterface) {
            $this->recipients = $move->getTargetHolder()->getMols();
        }

        return $this;
    }
}
