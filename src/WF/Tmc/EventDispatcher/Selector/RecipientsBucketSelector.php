<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.10.16
 * Time: 13:29
 */

namespace WF\Tmc\EventDispatcher\Selector;


use Doctrine\Common\Collections\Collection;
use WF\Tmc\Model\TmcMoveInterface;

class RecipientsBucketSelector extends AbstractRecipientSelector
{
    /**
     * @param TmcMoveInterface $move
     * @return void
     *
     * @throws \LogicException
     */
    public function setMove(TmcMoveInterface $move)
    {
        throw new \LogicException('User addRecipients method instead setMove!');
    }

    /**
     * @param RecipientSelectorInterface $selector
     * @return $this
     */
    public function addRecipients(RecipientSelectorInterface $selector)
    {
        $recipients = $selector->getRecipients();
        if ($recipients instanceof Collection) {
            $recipients = $recipients->toArray();
        }

        $this->recipients = array_merge($this->recipients, $recipients);

        return $this;
    }
}
