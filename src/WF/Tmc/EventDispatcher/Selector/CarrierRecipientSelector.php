<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.10.16
 * Time: 12:58
 */

namespace WF\Tmc\EventDispatcher\Selector;

use WF\Tmc\Model\TmcMoveInterface;

class CarrierRecipientSelector extends AbstractRecipientSelector
{
    /**
     * @param TmcMoveInterface $move
     * @return $this
     */
    public function setMove(TmcMoveInterface $move)
    {
        $this->recipients = [$move->getShippedBy()];

        return $this;
    }
}
