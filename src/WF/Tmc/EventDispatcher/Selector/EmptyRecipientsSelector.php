<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.10.16
 * Time: 11:51
 */

namespace WF\Tmc\EventDispatcher\Selector;


use models\User;
use WF\Tmc\Model\TmcMoveInterface;

class EmptyRecipientsSelector extends AbstractRecipientSelector
{
    /**
     * @param TmcMoveInterface $move
     * @return $this
     */
    public function setMove(TmcMoveInterface $move)
    {
        return $this;
    }

    /**
     * @return User[]
     */
    public function getRecipients()
    {
        return [];
    }
}
