<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.10.16
 * Time: 13:24
 */

namespace WF\Tmc\EventDispatcher\Selector;


use WF\Tmc\Model\TechnicianInterface;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcWarehouseInterface;

class SourceHolderRecipientSelector extends AbstractRecipientSelector
{
    /**
     * @param TmcMoveInterface $move
     * @return $this
     */
    public function setMove(TmcMoveInterface $move)
    {
        if($move->getSourceHolder() instanceof TechnicianInterface) {
            $this->recipients = [$move->getSourceHolder()];
        }

        if($move->getSourceHolder() instanceof TmcWarehouseInterface) {
            $this->recipients = $move->getSourceHolder()->getMols();
        }

        return $this;
    }
}
