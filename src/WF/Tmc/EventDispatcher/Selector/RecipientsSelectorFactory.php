<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.10.16
 * Time: 11:48
 */

namespace WF\Tmc\EventDispatcher\Selector;


use models\User;
use WF\Tmc\Model\TmcMoveInterface as Move;

class RecipientsSelectorFactory
{
    /**
     * @param Move $move
     * @param User|null $user
     * @return RecipientSelectorInterface
     */
    public function getSelector(Move $move, User $user = null)
    {
        $group = new RecipientsBucketSelector();
        switch ($move->getState()) {
            case Move::STATE_FROM_PARTNER:
                $group->addRecipients( (new CoordinatorRecipientSelector())->setMove($move));
                $group->addRecipients( (new TargetHolderRecipientSelector())->setMove($move));
                $group->exclude($user);
                break;

            case Move::STATE_TO_PARTNER:
            case Move::STATE_WRITE_OFF:
                $group->addRecipients( (new CoordinatorRecipientSelector())->setMove($move));
                $group->addRecipients( (new SourceHolderRecipientSelector())->setMove($move));
                $group->exclude($user);
                break;

            case Move::STATE_TRANSPORT_REQUEST:
                $group->addRecipients((new CarrierRecipientSelector())->setMove($move));
                break;

            case Move::STATE_TRANSFERRED:
            case Move::STATE_REQUEST_APPROVED:
                $group->addRecipients((new TargetHolderRecipientSelector())->setMove($move));
                break;

            case Move::STATE_REQUEST:
                $group->addRecipients((new SourceHolderRecipientSelector())->setMove($move));
                break;

            case Move::STATE_ITEMS_CHANGED:
            case Move::STATE_IS_TRANSPORTED:
            case Move::STATE_RETURN_REQUEST:
                $group->addRecipients((new TargetHolderRecipientSelector())->setMove($move));
                $group->exclude($user);
                break;

            case Move::STATE_CANCELED:
            case Move::STATE_REQUEST_CONFIRMED:
                $group = new RecipientsBucketSelector();
                $group->addRecipients( (new TargetHolderRecipientSelector())->setMove($move) );
                $group->addRecipients( (new SourceHolderRecipientSelector())->setMove($move) );
                $group->exclude($user);
                break;

            default:
                return (new EmptyRecipientsSelector())->setMove($move);
        }

        return $group;
    }

}
