<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.10.16
 * Time: 12:27
 */

namespace WF\Tmc\EventDispatcher\Selector;


use models\User;
use WF\Tmc\Model\TmcMoveInterface;

interface RecipientSelectorInterface
{
    /**
     * @param TmcMoveInterface $move
     * @return mixed
     */
    public function setMove(TmcMoveInterface $move);

    /**
     * @return User[]
     */
    public function getRecipients();

    /**
     * @param User $user
     * @return void
     */
    public function exclude(User $user);
}
