<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.10.16
 * Time: 12:30
 */

namespace WF\Tmc\EventDispatcher\Selector;


use models\User;
use WF\Tmc\Model\TmcMoveInterface;

abstract class AbstractRecipientSelector implements RecipientSelectorInterface
{
    /**
     * @var User[]
     */
    protected $recipients = [];

    /**
     * @param TmcMoveInterface $move
     * @return RecipientSelectorInterface
     */
    abstract public function setMove(TmcMoveInterface $move);

    /**
     * @return \models\User[]
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    public function exclude(User $user = null)
    {
        if(empty($user))
            return true;

        foreach ($this->recipients as $i=>$recipient)
            if($recipient->getId() === $user->getId())
                unset($this->recipients[$i]);

        return true;
    }
}
