<?php

namespace WF\Tmc\EventDispatcher;

use Symfony\Component\EventDispatcher\Event;
use WF\Tmc\Model\TmcMoveInterface;

/**
 * Description of TmcMoveStateChangedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TmcMoveStateChangedEvent extends Event
{
    const NAME = 'tmc.move_state_changed';

    /**
     * @var TmcMoveInterface
     */
    private $newMove;

    /**
     * @var TmcMoveInterface
     */
    private $oldMove;

    public function __construct(TmcMoveInterface $newMove, TmcMoveInterface $oldMove = null)
    {
        $this->newMove = $newMove;
        $this->oldMove = $oldMove;
    }

    /**
     * @return TmcMoveInterface
     */
    public function getNewMove()
    {
        return $this->newMove;
    }

    /**
     * @return TmcMoveInterface
     */
    public function getOldMove()
    {
        return $this->oldMove;
    }

    public function getOldState()
    {
        return $this->oldMove ? $this->oldMove->getState() : null;
    }

    public function getNewState()
    {
        return $this->newMove->getState();
    }
}
