<?php

namespace WF\Tmc;

use DateTime;
use models\TmcMove;
use WF\Core\TaskInterface;
use WF\Tmc\Action\ActionBuilder;
use WF\Tmc\Model\TechnicianInterface;
use WF\Tmc\Model\TmcCarrierInterface;
use WF\Tmc\Model\TmcHolderInterface;
use WF\Tmc\Model\TmcManagerInterface;
use WF\Tmc\Model\TmcMolInterface;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;
use WF\Tmc\Model\TmcWarehouseInterface;

/**
 * Description of TmcManager
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TmcManager implements TmcManagerInterface
{
    /**
     *
     * @var Action\ActionBuilder
     */
    private $actionBuilder;

    public function __construct(ActionBuilder $actionBuilder)
    {
        $this->actionBuilder = $actionBuilder;
    }

    /**
     *
     * @return TmcMove
     */
    public function createMove()
    {
        $move = new TmcMove();

        return $move;
    }

    public function makeReturnRequest(TechnicianInterface $technician, TmcWarehouseInterface $wh, array $items)
    {
        $move = $this->createMove()
            ->setState(TmcMoveInterface::STATE_RETURN_REQUEST)
            ->setSourceHolder($technician)
            ->setTargetHolder($wh)
            ->setMoveDate(new DateTime())
            ->setItems($items);

        return $this->actionBuilder
            ->build($move)
            ->create($technician, $move);
    }

    public function makeRequest(TmcHolderInterface $source, TmcHolderInterface $target, array $items, TmcUserInterface $author)
    {
        $move = $this->createMove()
            ->setState(TmcMoveInterface::STATE_REQUEST)
            ->setSourceHolder($source)
            ->setTargetHolder($target)
            ->setMoveDate(new DateTime())
            ->setItems($items)
            ->setApprovedBy($author);

        return $this->actionBuilder
            ->build($move)
            ->create($author, $move);
    }

    public function makeMoveBetweenWarehouses(TmcMolInterface $mol, TmcHolderInterface $source, TmcHolderInterface $target, array $items) {

        $move = $this->createMove()
            ->setState(TmcMoveInterface::STATE_TRANSFER_BETWEEN_WAREHOUSES)
            ->setSourceHolder($source)
            ->setTargetHolder($target)
            ->setMoveDate(new DateTime())
            ->setItems($items)
            ->setApprovedBy($mol);

        return $this->actionBuilder
            ->build($move)
            ->create($mol, $move);
    }

    public function transferTo(TmcMolInterface $mol, TmcWarehouseInterface $wh, TechnicianInterface $tech, array $items)
    {
        $move = $this->createMove()
            ->setState(TmcMoveInterface::STATE_TRANSFERRED)
            ->setSourceHolder($wh)
            ->setTargetHolder($tech)
            ->setMoveDate(new DateTime())
            ->setItems($items);

        $action = $this->actionBuilder->build($move);
        $savedMove = $action->create($mol, $move);

        //$nextMove = $action->approve($mol, $savedMove);

        return $savedMove;
    }

    public function install(TechnicianInterface $technician, TaskInterface $task, TmcUserInterface $approvedBy, array $items)
    {
        $move = $this->createMove()
            ->setState(TmcMoveInterface::STATE_INSTALLED)
            ->setSourceHolder($technician)
            ->setMoveDate(new DateTime())
            ->setTask($task)
            ->setItems($items);

        $action = $this->actionBuilder->build($move);
        $savedMove = $action->create($approvedBy, $move);
        $nextMove = $action->approve($approvedBy, $savedMove);

        return $nextMove;
    }

    public function uninstall(TechnicianInterface $technician, TaskInterface $task, TmcUserInterface $approvedBy, array $items)
    {
        $move = $this->createMove()
            ->setState(TmcMoveInterface::STATE_UNINSTALLED)
            ->setTargetHolder($technician)
            ->setMoveDate(new DateTime())
            ->setTask($task)
            ->setItems($items);

        $action = $this->actionBuilder->build($move);
        $savedMove = $action->create($approvedBy, $move);
        $nextMove = $action->approve($approvedBy, $savedMove);

        return $nextMove;
    }

    public function writeOff(TmcWarehouseInterface $sourceWarehouse, TmcMolInterface $mol, array $items)
    {
        $move = $this->createMove()
            ->setState(TmcMoveInterface::STATE_WRITE_OFF)
            ->setSourceHolder($sourceWarehouse)
            ->setMoveDate(new DateTime())
            ->setApprovedBy($mol)
            ->setItems($items);

        $action = $this->actionBuilder->build($move);

        $savedMove = $action->create($mol, $move);
        $nextMove = $action->approve($mol, $savedMove);

        return $nextMove;
    }

    public function markDefect(TmcHolderInterface $holder, TmcUserInterface $mol, array $items)
    {
        $move = $this->createMove()
            ->setState(TmcMoveInterface::STATE_MARK_DEFECT)
            ->setSourceHolder($holder)
            ->setTargetHolder($holder)
            ->setMoveDate(new DateTime())
            ->setApprovedBy($mol)
            ->setItems($items);

        $action = $this->actionBuilder->build($move);

        $savedMove = $action->create($mol, $move);
        $nextMove = $action->approve($mol, $savedMove);

        return $nextMove;
    }

    public function fromPartner(TmcWarehouseInterface $targetWarehouse, TmcMolInterface $mol, array $items)
    {
        $move = $this->createMove()
            ->setState(TmcMoveInterface::STATE_FROM_PARTNER)
            ->setTargetHolder($targetWarehouse)
            ->setMoveDate(new DateTime())
            ->setItems($items);

        $action = $this->actionBuilder->build($move);
        $savedMove = $action->create($mol, $move);
        $nextMove = $action->approve($mol, $savedMove);

        return $nextMove;
    }

    public function toPartner(TmcWarehouseInterface $sourceWarehouse, TmcMolInterface $mol, array $items)
    {
        $move = $this->createMove()
            ->setState(TmcMoveInterface::STATE_TO_PARTNER)
            ->setSourceHolder($sourceWarehouse)
            ->setMoveDate(new DateTime())
            ->setItems($items);

        $action = $this->actionBuilder->build($move);
        $savedMove = $action->create($mol, $move);
        $nextMove = $action->approve($mol, $savedMove);

        return $nextMove;
    }

    public function changeItemsConsumption(TmcMoveInterface $move, TmcUserInterface $user, array $items)
    {
        $nextMove = $this->actionBuilder
            ->build($move)
            ->changeItems($user, $move, $items);

        return $nextMove;
    }

    public function approve(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $this->actionBuilder
            ->build($move)
            ->approve($user, $move);
    }

    public function cancel(TmcUserInterface $user, TmcMoveInterface $move)
    {
        return $this->actionBuilder
            ->build($move)
            ->cancel($user, $move);
    }

    public function defineTransporting(TmcMoveInterface $move, TmcUserInterface $user, TmcCarrierInterface $carrier)
    {
        return $this->actionBuilder->build($move)
            ->defineTransporting($user, $move, $carrier);
    }

}
