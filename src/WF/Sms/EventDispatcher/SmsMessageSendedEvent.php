<?php

namespace WF\Sms\EventDispatcher;

use Symfony\Component\EventDispatcher\Event;
use WF\Sms\Model\SmsMessageInterface;

/**
 * Description of SmsMessageSendedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SmsMessageSendedEvent extends Event
{
    const NAME = 'sms.message_sended';

    /**
     *
     * @var SmsMessageInterface 
     */
    private $message;
    
    public function __construct(SmsMessageInterface $message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }

}
