<?php

namespace WF\Sms\Sender;

use Psr\Log\LoggerInterface;
use WF\Sms\Model\SmsMessageInterface;
use WF\Sms\Model\SmsSenderInterface;

/**
 * Description of DebugLoggerSender
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class DebugLoggerSender implements SmsSenderInterface
{
    /**
     *
     * @var LoggerInterface 
     */
    private $logger;
    
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    
    public function send(SmsMessageInterface $message)
    {
        $receivers = $message->getTo();
        
        if (count($receivers) === 0) {
            $this->logger->debug('Empty receivers');
            return;
        }
        
        $this->logger->debug($this->getLogMessage($message), $message->getTo());
    }
    
    private function getLogMessage(SmsMessageInterface $message)
    {
        $msg = sprintf('Message (%s) sended', $message->getText());
        
        if (null !== $from = $message->getFrom()) {
            $msg .= ' from ' . $from;
        }
        
        return $msg;
    }

}
