<?php

namespace WF\Sms\Sender;

use SimpleXMLElement;
use WF\Sms\Model\SmsMessageInterface;
use WF\Sms\Model\SmsSenderInterface;
use WF\Sms\SmsException;
use RuntimeException;

/**
 * Description of WebsmsRuSender
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class WebsmsRuSender implements SmsSenderInterface
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;
    
    /**
     * @var string
     */
    private $isTestMode;

    public function __construct($url, $login, $password, $isTestMode = false)
    {
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;
        $this->isTestMode = $isTestMode;
    }
    
    /**
     * 
     * @param SmsMessageInterface $message
     *
     * @throws SmsException
     */
    public function send(SmsMessageInterface $message)
    {
        if (count($message->getTo()) === 0) {
            throw new SmsException('Empty receivers', $message);
        }

        $xml = $this->getMessageXml($message);
        $data = $this->sendRequest($xml);
        $response = new SimpleXMLElement($data);
        
        /* @var $state SimpleXMLElement */
        foreach ($response->state as $state) {
            $errCode = (string)$state->attributes()->errcode;
            $error = (string)$state->attributes()->error;
            $state = (string)$state;
            if ($this->isGlobalError($errCode)) {
                throw new SmsException($error, $message);
            }
        }
    }
    
    /**
     * 
     * @param SmsMessageInterface $message
     * @return SimpleXMLElement
     */
    private function getMessageXml(SmsMessageInterface $message)
    {
        $receivers = $message->getTo();
        
        $xml = new SimpleXMLElement("<message></message>");
        $service = $xml->addChild("service");
        $service->addAttribute('id', count($receivers) > 1 ? 'bulk' : 'single');
        $service->addAttribute('login', $this->login);
        $service->addAttribute('password', $this->password);
        $service->addAttribute('test', $this->isTestMode ? '1' : '0');

        foreach ($receivers as $to) {
            $xml->addChild('to', $to);
        }

        $xml->addChild("body", $message->getText());
        
        return $xml;
    }
    
    private function sendRequest(SimpleXMLElement $xml)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml->asXML());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-type: text/xml"
        ));

        $data = curl_exec($ch);
        $error = curl_error($ch);
        
        curl_close($ch);
        
        if ($data === false) {
            throw new RuntimeException($error);
        }
        
        return $data;
    }
    
    private function isGlobalError($code)
    {
        return in_array($code, ['13'], true);
    }
}
