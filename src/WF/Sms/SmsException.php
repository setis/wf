<?php

namespace WF\Sms;

use Exception;
use WF\Sms\Model\SmsMessageInterface;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SmsException extends Exception
{
    /**
     *
     * @var SmsMessageInterface 
     */
    private $smsMessage;
    
    public function __construct($message, SmsMessageInterface $smsMessage)
    {
        $this->smsMessage = $smsMessage;
        parent::__construct($message);
    }

}
