<?php

namespace WF\Sms\Model;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface SmsSenderInterface
{
    /**
     * 
     * @param SmsMessageInterface $message
     */
    public function send(SmsMessageInterface $message);
}
