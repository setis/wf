<?php
/**
 * Artem
 * 26.04.2016 19:00
 */

namespace WF\Sms\Model;


interface SmsMessageInterface
{
    /**
     * @return string
     */
    public function getText();

    /**
     * @return array
     */
    public function getTo();
    
    /**
     * @return string
     */
    public function getFrom();
}