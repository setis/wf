<?php
/**
 * Artem
 * 25.04.2016 18:05
 */

namespace WF\Sms\Model;


interface SmsManagerInterface
{
    /**
     * @param SmsMessageInterface $message
     * @return boolean
     */
    public function send(SmsMessageInterface $message);

}