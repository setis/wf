<?php
/**
 * Artem
 * 25.04.2016 18:05
 */

namespace WF\Sms;

use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Sms\EventDispatcher\SmsMessageSendedEvent;
use WF\Sms\Model\SmsManagerInterface;
use WF\Sms\Model\SmsMessageInterface;
use WF\Sms\Model\SmsSenderInterface;

class SmsManager implements SmsManagerInterface
{

    /**
     * @var SmsSenderInterface
     */
    private $sender;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EventDispatcherInterface
     */
    private $ed;
    
    private $throwExceptions;

    public function __construct(SmsSenderInterface $sender, LoggerInterface $logger, EventDispatcherInterface $ed, $throwExceptions = false)
    {
        $this->sender = $sender;
        $this->logger = $logger;
        $this->ed = $ed;
        $this->throwExceptions = $throwExceptions;
    }

    public function send(SmsMessageInterface $message)
    {
        try {
            $this->sender->send($message);
        }
        catch (Exception $ex) {
            if ($this->throwExceptions) {
                throw $ex;
            }
            
            $this->logger->error($ex->getMessage(), [
                'file' => $ex->getFile(),
                'line' => $ex->getFile(),
                'trace' => $ex->getTraceAsString(),
            ]);
            
            return false;
        }
        
        $this->ed->dispatch(SmsMessageSendedEvent::NAME, new SmsMessageSendedEvent($message));
        
        return true;
    }

}