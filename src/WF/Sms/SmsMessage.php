<?php

namespace WF\Sms;

use WF\Sms\Model\SmsMessageInterface;

/**
 * Description of SmsMessage
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SmsMessage implements SmsMessageInterface
{
    private $text;

    private $to;

    private $from;

    public function __construct($text, array $to = [], $from = null)
    {
        $this->text = $text;
        $this->from = $from;

        foreach ($to as $phone) {
            $this->addTo($phone);
        }
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function addTo($to)
    {
        $clear = $this->cleanPhone($to);

        if ($clear !== FALSE) {
            $this->to[] = $clear;
        }

        return $this;
    }

    private function cleanPhone($phone)
    {
        $phone_clean = preg_replace("/[^0-9]/", "", $phone);

        if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) {
            // 7903...

        } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) {
            // 8903...
            $phone_clean = "7" . substr($phone_clean, 1);
        } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) {
            // 903...
            $phone_clean = "7" . $phone_clean;
        } else {
            $phone_clean = false;
        }

        return $phone_clean;
    }

}
