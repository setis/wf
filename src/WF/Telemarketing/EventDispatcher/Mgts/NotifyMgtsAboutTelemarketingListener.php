<?php
/**
 * Author: Artem <mail@artemd.ru>
 * Date: 14.11.16
 */

namespace WF\Telemarketing\EventDispatcher\Mgts;


use models\ServiceTicketExtras;
use models\Task;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WF\Task\Events\TaskCreatedEvent;

/**
 * Class NotifyMgtsAboutTelemarketingListener
 *
 * Use in local_services.yml
 *
 * wf.task.event_dispatcher.notify_mgts_about_telemarketing_listener:
 * class: WF\Telemarketing\EventDispatcher\Mgts\NotifyMgtsAboutTelemarketingListener
 * arguments:
 * - '@swiftmailer.mailer'
 * - '@wf.templating'
 * - ['diler-box@mts.ru', 'MO-Diler-box@mts.ru', 'D.S.Konstantinov@mgts.ru', 'A.V.Maskaev@mgts.ru', 'M.P.Chachua@mgts.ru']
 * - [36] #МГТС
 * - '@service_container'
 * tags:
 * - { name: kernel.event_listener, event: wf.task.task_created, priority: -2048 }
 *
 * @package WF\Task\EventDispatcher
 * @author ${USER} <mail@artemd.ru>
 */
class NotifyMgtsAboutTelemarketingListener
{

    public $partnerIds = [];

    /**
     * @var
     */
    private $mailer;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var null
     */
    private $recipients;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct($mailer, EngineInterface $templating, array $recipients = [], array $partnerIds = [], ContainerInterface $container)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->recipients = $recipients;
        $this->container = $container;
        $this->partnerIds = $partnerIds;
    }

    public function onWfTaskTaskCreated(TaskCreatedEvent $event)
    {
        $task = $event->getTask();

        if (Task::TELEMARKETING !== $task->getPluginUid() || !in_array($task->getTicket()->getPartner()->getId(), $this->partnerIds, true)) {
            return;
        }

        $extras = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository(ServiceTicketExtras::class)
            ->findOneBy(['task' => $task]);

        try {
            $body = $this->renderMessage(
                'Telemarketing/Notification/mgtsNotify.html.php',
                [
                    'task' => $task,
                    'extras' => $extras,
                ]
            );
        } catch (\RuntimeException $exception) {
            $event->setResult(false);
            return;
        }

        $event->setResult($this->sendEmail($body));
    }

    private function sendEmail($body)
    {
        /** @var \Swift_Mailer $mailer */
        $mailer = $this->mailer;
        /** @var \Swift_Message $message */
        $message = $mailer->createMessage();
        $message
            ->setSubject('Новая заявка Дилер *ООО ВСЕ СЕТИ*')
            ->setFrom($this->container->getParameter('robot_email'))
            ->setTo($this->recipients)
            ->setBody($body, 'text/html');

        $header = $message->getHeaders();
        $header->addPathHeader('Return-Path', $this->container->getParameter('mailer_username'));

        return $mailer->send($message) > 0;
    }

    protected function renderMessage($name, array $parameters)
    {
        return $this->templating->render($name, $parameters);
    }
}
