<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 22.07.16
 * Time: 19:33
 */

namespace WF\Telemarketing\Controller;


use models\Task;
use Symfony\Component\HttpFoundation\Request;
use WF\HttpKernel\RestController;
use WF\Telemarketing\Providers\FeedbackQuestionnaireProvider;

class ApiController extends RestController
{

    /**
     * @return FeedbackQuestionnaireProvider
     */
    private function getProvider()
    {
        return $this->get('wf.telemarketing.quest_provider');
    }

    public function getQuestionnaire(Request $request)
    {
        $task = $this->getEm()->find(Task::class, $request->get('taskId'));
        $q = $this->getProvider()->getQuestionnaire($task);
        unset($q['created_at']);
        unset($q['created_by']);

        return $this->renderJsonResponse($q, ['default']);
    }

    public function getLoyalty(Request $request)
    {
        $task = $this->getEm()->find(Task::class, $request->get('taskId'));
        $l = $this->getProvider()->getLoyalty($task);
        unset($l['created_at']);
        unset($l['created_by']);
        return $this->renderJsonResponse($l, ['default']);
    }

    public function postSaveQuestionnaire(Request $request)
    {
        $task = $this->getEm()->find(Task::class, $request->get('taskId'));
        $questionnaire = $request->get('questionnaire');
        $loyalty = $request->get('loyalty');

        $this->getProvider()->saveQuestionnaire($task, $questionnaire, $loyalty, $this->getUser());
    }
}