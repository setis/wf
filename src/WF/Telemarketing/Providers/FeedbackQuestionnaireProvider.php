<?php
/**
 * mail@artemd.ru
 * 17.03.2016
 */

namespace WF\Telemarketing\Providers;

use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use models\TaskComment;
use models\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Task\Events\TaskLoyaltyEvent;
use WF\Task\Events\TaskQuestionnaireEvent;
use WF\Task\TaskManager;

class FeedbackQuestionnaireProvider
{

    /**
     * @var TaskManager
     */
    private $tm;

    /**
     * @var EventDispatcherInterface
     */
    private $ed;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(TaskManager $tm, EventDispatcherInterface $ed, EntityManagerInterface $em)
    {
        $this->tm = $tm;
        $this->ed = $ed;
        $this->em = $em;
    }

    public function getQuestionnaire(Task $task)
    {
        $quest = $task->getQuestionnaire();
        if (!empty($quest))
            return json_decode($quest, true);

        return $this->defaultQuestionnaire();
    }

    public function getLoyalty(Task $task)
    {
        $quest = $task->getLoyalty();
        if (!empty($quest))
            return json_decode($quest, true);

        return $this->defaultLoyalty();
    }

    public function saveQuestionnaire(Task $task, array $questionnaire, array $loyalty, User $author)
    {
        $questionnaire['created_by'] = $author->getId();
        $questionnaire['created_at'] = date('Y-m-d H:i:s');

        $task->setQuestionnaire(
            json_encode($questionnaire, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)
        )->setLoyalty(
            json_encode($loyalty, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)
        );

        $this->em->persist($task);
        $this->em->flush();

        $comment = new TaskComment();
        $comment->setAuthor($author)
            ->setDatetime(new \DateTime())
            ->setTag('Анекта клиента добавлена.')
            ->setText($this->toString($loyalty) . "\n-\n" . $this->toString($questionnaire));

        $this->tm->addComment($task, $comment);

        $event = new TaskQuestionnaireEvent($questionnaire, $loyalty, $task, $author);
        $this->ed->dispatch(TaskQuestionnaireEvent::NAME, $event);
    }

    private function toString(array $questionnaire)
    {
        $res = '';
        foreach ($questionnaire['questions'] as $q) {
            if (!isset($q['question']))
                continue;

            $res .= $q['question'] . ': ' . (isset($q['answered']) && !empty($q['answered']) ? implode(', ', (array)$q['answered']) : 'Нет ответа') . ".\n";

        }

        if (isset($questionnaire['comment']) && !empty($questionnaire['comment']))
            $res .= "Коментарий: " . $questionnaire['comment'] . "\n\n";

        return $res;
    }

    private function defaultQuestionnaire()
    {
        return [
            'questions'=>[
                [
                    'question' => 'Какой услугой клиент еще пользуется у данного провайдера?',
                    'answers' => [
                        'IP ТВ /ОТТ',
                        'Телефония',
                    ],
                ],
                [
                    'question' => 'Есть услуги другого провайдера?',
                    'answers' => [
                        'Интернет',
                        'ТВ',
                        'Телефония',
                    ],
                ],
                [
                    'question' => 'Клиенту интересно?',
                    'answers' => [
                        'Интернет',
                        'IP ТВ/ОТТ',
                        'Оборудование к IP ТВ',
                        'Оборудование к Интернету',
                    ],
                ],
            ]
        ];
    }

    private function defaultLoyalty()
    {
        return [
            'questions'=>[
                [
                    'question' => 'У клиента есть проблема?',
                    'answers' => [
                        'Да',
                        'Нет',
                    ],
                ],
                [
                    'question' => 'Клиента ВСЕ устраивает?',
                    'answers' => [
                        'Да',
                        'Нет',
                    ],
                ],
                [
                    'question' => 'У клиента есть запрос?',
                    'answers' => [
                        'Да',
                        'Нет',
                    ],
                ],
            ]
        ];

    }
}
