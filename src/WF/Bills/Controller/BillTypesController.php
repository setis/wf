<?php
/**
 * mail@artemd.ru
 * 13.05.2016
 */

namespace WF\Bills\Controller;

use classes\exceptions\NotFoundModelException;
use DateTime;
use models\BillType;
use models\BillTypeLimit;
use models\BusinessUnit;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use WF\HttpKernel\AbstractController;

class BillTypesController extends AbstractController
{
    public function indexAction(Request $request)
    {
        $billTypes = $this->getEm()->getRepository(BillType::class)->findBy([], ['id' => 'asc']);
        /** @var \repository\BillTypeLimitRepository $repo */
        $repo = $this->getEm()->getRepository(BillTypeLimit::class);

        $data = [];
        foreach ($billTypes as $billType) {
            $data[] = [
                'billType' => $billType,
                'monthRest' => $repo->getMonthLimitRest($billType),
                'monthReserved' => $repo->getMonthLimitReserved($billType),
                'yearLimit' => $repo->getYearLimit($billType, DateTime::createFromFormat('m-d', '12-31'))
            ];
        }

        return $this->renderResponse('Bills/Types/index.html.php', [
            'data' => $data
        ]);
    }

    public function createAction(Request $request)
    {
        /** @var BillType $model */
        $model = new BillType();

        /** @var BusinessUnit[] $businessUnits */
        $businessUnits = $this->getEm()->getRepository(BusinessUnit::class)->findAll();

        return $this->renderResponse('Bills/Types/edit.html.php', [
            'model' => $model,
            'limits' => array_fill(0, 12 , new BillTypeLimit()),
            'businessUnits' => $businessUnits
        ]);
    }

    public function editAction(Request $request)
    {
        /** @var BillType $model */
        $model = $this->getEm()->find(BillType::class, $request->get('id'));
        if (empty($model))
            throw new NotFoundModelException('Not found model!');

        /** @var \repository\BillTypeLimitRepository $repo */
        $repo = $this->getEm()->getRepository(BillTypeLimit::class);
        $limits = $repo->getYearLimits($model,
            new DateTime(date('Y') . '-12-01 23:59:59'));

        /** @var BusinessUnit[] $businessUnits */
        $businessUnits = $this->getEm()->getRepository(BusinessUnit::class)->findAll();

        return $this->renderResponse('Bills/Types/edit.html.php', [
            'model' => $model,
            'limits' => $limits,
            'businessUnits' => $businessUnits
        ]);
    }

    public function saveAction(Request $request)
    {


        if ($request->request->get('id')) {
            /** @var BillType $billType */
            $billType = $this->getEm()->find(BillType::class, $request->get('id'));
            if (empty($billType))
                throw new NotFoundModelException('Not found model!');
        } else {
            $billType = new BillType();
        }

        $billType->setTitle($request->get('title'));
        $billType->setTag($request->get('tag'));

        foreach ($request->get('limits', array()) as $limit) {
            $billTypeLimit = $this->getEm()->find(BillTypeLimit::class, $limit['id']);
            if (empty($billTypeLimit))
                $billTypeLimit = new BillTypeLimit();

            $dt = new \DateTime();
            $dt->setTimestamp((integer)$limit['limitAt']);
            $billTypeLimit->setLimitAt($dt);
            $billTypeLimit->setMoneyLimit($limit['moneyLimit']);

            $billType->addLimit($billTypeLimit);
        }

        /** @var BusinessUnit $businessUnit */
        $businessUnit = $this->getEm()->find(BusinessUnit::class, $request->get('business_unit'));
        $billType->setBusinessUnit($businessUnit);

        $this->getEm()->persist($billType);
        $this->getEm()->flush();

        $response = new RedirectResponse($this->generateUrl('bill_types_edit', ['id' => $billType->getId()]));

        $request->getSession()
            ->getFlashBag()
            ->add('success', "Статья расходов сохранена. ID: " . $billType->getId())
        ;

        return $response;
    }

    public function deleteAction(Request $request)
    {
        $id = $request->get('id');

        if (empty($id))
            UIError("Не указан идентификатор записи!");

        $billType = $this->getEm()->find(BillType::class, $request->get('id'));

        if (empty($billType))
            UIError("Объект не найден id:{$id}!");

        $this->getEm()->remove($billType);
        $this->getEm()->flush();

        $response = new RedirectResponse($this->generateUrl('bill_types'));

        $request->getSession()
            ->getFlashBag()
            ->add('success', "Запись успешно удалена.")
        ;

        return $response;

    }


}
