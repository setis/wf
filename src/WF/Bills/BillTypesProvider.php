<?php
/**
 * mail@artemd.ru
 * 13.05.2016
 */

namespace WF\Bills;


use models\BusinessUnit;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BillTypesProvider
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function getEm(){
        return $this->container->get('doctrine.orm.default_entity_manager');
    }

    /**
     * @return BusinessUnit
     */
    public function getBusinessUnit() {
        return $this->getEm()->find(BusinessUnit::class, 1);
    }

    public function getOptions($id = 0)
    {
        global $DB;
        $sql = "SELECT id, title FROM `bill_types` WHERE 1 ORDER BY `title`;";
        $res = $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());
        return array2options($res, $id);
    }

    public function getById($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `bill_types` WHERE `id`=" . $DB->F($id) . ";";
        $res = $DB->getField($sql);
        if ($DB->errno()) UIError($DB->error());
        return $res ? $res : false;
    }
}
