<?php

namespace WF\Legacy;

use classes\Task as ClassTask;

/**
 * Набор методов для отрисовки общих элементов в шаблонах заявок
 * @author Ed <farafonov@gorserv.ru>
 */
class TicketViewModificator
{

    /**
     * Кнопка клонирования заявки в другой тип в заданном шаблоне, с проверкой доступа
     * @param \classes\Task $task 
     * \classes\HTML_Template_IT $tpl
     */
     static public function renderButton(ClassTask $task, \classes\HTML_Template_IT $tpl) {
        if (\wf::$user->checkAccess("clone_ticket",\classes\User::ACCESS_WRITE)) {
            $id = $task->getId();
            $plugins = \classes\Plugin::getPlugins();
            $opts = \clone_ticket_plugin::getOptions($task->getPluginUid());
            $cnt = count($opts);
            if ($cnt > 0) {
                $tpl->setCurrentBlock("cloneticket");
                for ($i=0; $i<$cnt; $i++){
                    $tpl->setCurrentBlock("cloneticket_items");
                    $tpl->setVariable("CLONETICKET_ITM_PLUGIN", $opts[$i]);
                    $tpl->setVariable("CLONETICKET_ITM_NAME", $plugins[$opts[$i]]['name']);
                    $tpl->setVariable("CLONETICKET_ITM_SRC_TASK_ID", $id);
                    $tpl->parse("cloneticket_items");
                }
                $tpl->setVariable("CLONETICKET_SRC_TASK_ID", $id);
                $tpl->parse("cloneticket");
            }
        }
    }
    
    /**
     * Отрисовка блока связанных заявок в заданном шаблоне
     * @param \classes\Task $task 
     * \classes\HTML_Template_IT $tpl
     */
    static public function renderJoinedTasks(ClassTask $task, \classes\HTML_Template_IT $tpl) {
        
        $id = $task->getId();
        $joinedtasks = \classes\tickets\Ticket::getLinkedTasksList($id);
        $plugins = \classes\Plugin::getPlugins();
        if ((count($joinedtasks) > 0) && \wf::$user->checkAccess("joinedtasks_access",\classes\User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("joinedtasks");
            foreach ($joinedtasks as $val) {
                $tpl->setCurrentBlock("joinedtasks_items");
                $tpl->setVariable("J_ITM_PLUGIN", $val['plugin_uid']);
                $tpl->setVariable("J_ITM_ID", $val['id']);
                $anchor = $plugins[ $val['plugin_uid'] ]['name']." №".$val['id'];
                $tpl->setVariable("J_ITM_ANCHOR", $anchor);
                $tpl->parse("joinedtasks_items");
            }
            $tpl->parse("joinedtasks");
        }
    }
    
    static public function renderAll(ClassTask $task, \classes\HTML_Template_IT $tpl) {
        self::renderButton($task, $tpl);
        self::renderJoinedTasks($task, $tpl);
    }
}
