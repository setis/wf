<?php

namespace WF\Address;

/**
 * Description of AddressElement
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class AddressElement implements AddressElementInterface
{
    private $title;
    
    private $type;
    
    public function __construct($title, $type)
    {
        $this->title = $title;
        $this->type = $type;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

}
