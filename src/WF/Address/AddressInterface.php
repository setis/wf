<?php

namespace WF\Address;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 *
 */
interface AddressInterface
{
    const LEVEL_HOUSE = 0;
    const LEVEL_NEAR_HOUSE = 5;
    const LEVEL_STREET = 10;
    const LEVEL_CITY = 20;

    /**
     *
     * @param string $key
     * @return string
     */
    public function getCode($key);

    /**
     * @return AddressElementInterface
     */
    public function getCity();

    /**
     * @return AddressElementInterface
     */
    public function getStreet();

    /**
     * @return AddressElementInterface
     */
    public function getHouse();

    /**
     * @return string Full building title with house, block etc.
     */
    public function getFullBuildingTitle();

    /**
     * @return string
     */
    public function getLatitude();

    /**
     * @return string
     */
    public function getLongitude();

    /**
     * @return integer
     */
    public function getLevel();

    /**
     * @return string
     */
    public function __toString();
}
