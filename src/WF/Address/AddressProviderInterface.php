<?php

namespace WF\Address;
use GuzzleHttp\Exception\ClientException;
use WF\Address\Provider\DadataAddress;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface AddressProviderInterface
{
    /**
     * @param string $address
     * @param array $options
     * @return AddressInterface[]
     * @throws \RuntimeException
     * @throws ClientException
     */
    public function find($address, array $options = []);
}
