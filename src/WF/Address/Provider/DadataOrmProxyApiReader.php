<?php

namespace WF\Address\Provider;

use Doctrine\ORM\EntityManagerInterface;
use models\DadataAddress as AddressEntity;

/**
 * Description of DadataOrmProxyApiReader
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class DadataOrmProxyApiReader implements DadataApiReaderInterface
{

    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var DadataApiReaderInterface
     */
    private $apiReader;

    public function __construct(EntityManagerInterface $em, DadataApiReaderInterface $apiReader)
    {
        $this->em = $em;
        $this->apiReader = $apiReader;
    }

    /**
     * @param string $address
     * @param array $options
     * @return array
     */
    public function read($address, array $options = [])
    {
        $results = $this->em->createQueryBuilder()
            ->select('a')
            ->from(AddressEntity::class, 'a')
            ->where('a.raw LIKE :addr OR a.value = :addr OR a.unrestricted_value = :addr')
            ->andWhere('a.geo_lat IS NOT NULL AND a.geo_lon IS NOT NULL')
            ->setParameter('addr', $address)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();

        if (count($results)) {
            return ['suggestions' => array_map(function(AddressEntity $element) {
                $res = $element->getRawData();
                $res['id'] = $element->getId();
                return $res;
            }, $results)];
        }

        $results = $this->apiReader->read($address, $options);
        foreach ($results['suggestions'] as $suggestion) {
            $addr = AddressEntity::createFromDadata($suggestion)
                ->setRaw($address);
            if (!empty($addr->getGeoLat()) && !empty($addr->getGeoLon())) {
                $this->em->persist($addr);
            }
        }
        $this->em->flush();

        return $results;
    }

}
