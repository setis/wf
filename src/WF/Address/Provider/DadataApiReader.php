<?php

namespace WF\Address\Provider;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use RuntimeException;

/**
 * Description of DadataApiReader
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class DadataApiReader implements DadataApiReaderInterface
{
    private $url;

    private $token;

    private $region = [];

    public function __construct($url, $token, $default_region = null)
    {
        $this->url = $url;
        $this->token = $token;
        $this->region = is_array($default_region) ? $default_region : [$default_region];
    }

    /**
     * @param string $address
     * @param array $options
     * @return mixed
     * @throws RequestException
     * @throws RuntimeException
     */
    public function read($address, array $options = [])
    {
        $payload = [
            'query' => str_replace("\t", ' ', $address),
        ];

        if (!empty($this->region)) {
            $payload['locations_boost'] = [array_combine(
                array_fill(0, count($this->region), 'kladr_id'),
                $this->region
            )];
        }

        $payload = array_merge($options, $payload);

        $client = new Client();
        $request = new Request('POST', $this->url, [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Token ' . $this->token,
        ], json_encode($payload)
        );

        try {
            $response = $client->send($request, ['verify' => false]);
        } catch (RequestException $e) {
            throw $e;
        }

        if ($response->getStatusCode() !== 200) {
            throw new RuntimeException('Dadata api returns response code ' . $response->getStatusCode());
        }

        $body = $response->getBody()->getContents();
        $data = @json_decode($body, true);
        if ($data === null) {
            throw new RuntimeException('Error parsing json from dadata: ' . json_last_error_msg());
        }

        return $data;
    }
}
