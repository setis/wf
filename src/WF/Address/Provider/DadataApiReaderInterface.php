<?php

namespace WF\Address\Provider;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface DadataApiReaderInterface
{
    /**
     * @param $address
     * @param array $options
     * @return mixed
     */
    public function read($address, array $options = []);
}
