<?php

namespace WF\Address\Provider;

use WF\Address\AddressProviderInterface;

/**
 * Description of NullAddressProvider
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class StubAddressProvider implements AddressProviderInterface
{
    public function find($address, array $options = array())
    {
        return [];
    }

}
