<?php

namespace WF\Address\Provider;

use Exception;
use WF\Address\AddressElement;
use WF\Address\AddressInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * Description of DadataAddress
 *
 * Class DadataAddress
 * @package WF\Address\Provider
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class DadataAddress implements AddressInterface
{
    /**
     *
     * @var array Dadata data
     */
    private $data;

    /**
     *
     * @var array
     * @Serializer\Groups({"default"})
     */
    private $suggestion;

    /**
     *
     * @var AddressElement
     * @Serializer\Groups({"default"})
     */
    private $city;

    /**
     *
     * @var AddressElement
     * @Serializer\Groups({"default"})
     */
    private $street;

    /**
     *
     * @var AddressElement
     * @Serializer\Groups({"default"})
     */
    private $house;

    /**
     *
     * @var AddressElement
     * @Serializer\Groups({"default"})
     */
    private $block;

    /**
     *
     * @var integer
     * @Serializer\Groups({"default"})
     */
    private $level;

    public function __get($name)
    {
        return $this->data[$name];
    }

    public function __construct(array $suggestion)
    {
        $this->suggestion = $suggestion;
        $this->data = $this->suggestion['data'];

        switch ($this->data['qc_geo']) {
            case 0:
                $this->level = AddressInterface::LEVEL_HOUSE;
                break;
            case 1:
                $this->level = AddressInterface::LEVEL_NEAR_HOUSE;
                break;
            case 2:
                $this->level = AddressInterface::LEVEL_STREET;
                break;
            case 3:
            case 4:
                $this->level = AddressInterface::LEVEL_CITY;
                break;
            default :
                $this->level = PHP_INT_MAX;
        }

        $this->city = new AddressElement($this->data['city'], $this->data['city_type']);
        $this->street = new AddressElement($this->data['street'], $this->data['street_type']);
        $this->house = new AddressElement($this->data['house'], $this->data['house_type']);
        $this->block = new AddressElement($this->data['block'], $this->data['block_type']);
    }

    public function getSuggestion()
    {
        return $this->suggestion;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getCode($key)
    {
        switch ($key) {
            case 'fias':
                return $this->data['fias_id'];

            case 'kladr':
                return $this->data['kladr_id'];
        }

        throw new Exception("Unsupported key '{$key}'. 'fias', 'kladr' availavle");
    }

    public function getHouse()
    {
        return $this->house;
    }

    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("latitude")
     *
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->data['geo_lat'];
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("longitude")
     *
     * @return double
     */
    public function getLongitude()
    {
        return $this->data['geo_lon'];
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getLevel()
    {
        return $this->level;
    }

    /*
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("full_building_title")
     */
    public function getFullBuildingTitle()
    {
        $house = $this->house->getTitle();
        if (!$house) {
            return null;
        }

        $blockType = $this->block->getType();
        $blocTitle = $this->block->getTitle();

        $block = '';
        if ($blocTitle) {
            $block = $blockType . $blocTitle;
        }

        return $house . $block;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        return !empty($this->getSuggestion()) ? $this->getSuggestion()['value'] : null;
    }
}
