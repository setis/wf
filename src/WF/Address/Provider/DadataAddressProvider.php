<?php

namespace WF\Address\Provider;

use GuzzleHttp\Exception\ClientException;
use WF\Address\AddressInterface;
use WF\Address\AddressProviderInterface;


/**
 * Description of DadataAddressProvider
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class DadataAddressProvider implements AddressProviderInterface
{
    /**
     *
     * @var DadataApiReaderInterface
     */
    private $apiReader;

    public function __construct(DadataApiReaderInterface $apiReader)
    {
        $this->apiReader = $apiReader;
    }

    /**
     *
     * @param string $address
     * @param array $options
     * @return AddressInterface[]
     * @throws \RuntimeException
     * @throws ClientException
     */
    public function find($address, array $options = [])
    {
        $data = $this->apiReader->read($address, $options);

        if (!array_key_exists('suggestions', $data)) {
            throw new \RuntimeException('Wrong dadata format: "suggestions" key not found');
        }

        $addr = [];
        foreach ($data['suggestions'] as $suggestion) {
            $addr[] = new DadataAddress($suggestion);
        }

        return $addr;
    }

}
