<?php

namespace WF\Address;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface AddressElementInterface
{
    public function getTitle();
    
    public function getType();
}
