<?php

namespace WF\HttpKernel;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of RestController
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
abstract class RestController extends AbstractController
{
    /**
     * 
     * @return SerializerInterface
     */
    protected function getSerializer()
    {
        return $this->get('serializer');
    }
    
    protected function serializeJson($data, array $groups = [], callable $callback = null)
    {
        $context = SerializationContext::create()->setGroups($groups);
        
        if ($callback !== null) {
            $callback($context);
        }
        
        return $this->getSerializer()->serialize($data, 'json', $context);
    }

    /**
     * @param $data
     * @return Response
     */
    protected function renderJson($data){
        $response =  (new Response())->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param $data
     * @param array $groups
     * @param int $status
     * @return Response
     */
    protected function renderJsonResponse($data, array $groups = [], $status = Response::HTTP_OK)
    {
        return new Response($this->serializeJson($data, $groups),
            $status, ['Content-Type' => 'application/json']);
    }

    /**
     * @param string $data
     * @param string $prefix
     * @return Response
     */

    protected function renderCsvResponse(string $data,string $prefix=''):Response{
        return new Response(iconv("utf-8", "cp1251", $data),
            Response::HTTP_OK, [
                'Content-Type' => 'application/octet-stream',
                'Content-Disposition'=>'attachment; filename='.$prefix.time().'.csv',
                'Content-Transfer-Encoding'=> 'binary',
                'Pragma'=>'no-cache',
                'Expires'=> '0'

            ]);
    }

    /**
     * @param array $array
     * @param array $titles
     * @param callable $handler
     * @param string $delimiter
     * @return string
     */
    protected function convertToCsv(array $array, array $titles, callable $handler = null, $delimiter = ';')
    {
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, $titles, $delimiter);

        if (null === $handler) {
            foreach ($array as $row) {
                fputcsv($df, $row, $delimiter);
            }
        }
        else {
            foreach ($array as $row) {
                fputcsv($df, $handler($row), $delimiter);
            }
        }

        fclose($df);
        return ob_get_clean();
    }
}
