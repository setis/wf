<?php

namespace WF\HttpKernel\Security;

use models\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\Event;

/**
 * Description of HttpUserAuthenticatedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class HttpUserAuthenticatedEvent extends Event
{
    const NAME = 'security.kernel.user_authenticated';
    
    /**
     *
     * @var GetResponseEvent 
     */
    private $sourceEvent;
    
    /**
     *
     * @var User
     */
    private $user;

    public function __construct(GetResponseEvent $sourceEvent, User $user)
    {
        $this->sourceEvent = $sourceEvent;
        $this->user = $user;
    }
    
    public function getUser()
    {
        return $this->user;
    }
    
    public function getRequest()
    {
        return $this->sourceEvent->getRequest();
    }

    public function setResponse(Response $response)
    {
        $this->sourceEvent->setResponse($response);
        $this->stopPropagation();
    }

}
