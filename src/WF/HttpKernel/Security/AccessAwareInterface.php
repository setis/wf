<?php

namespace WF\HttpKernel\Security;

use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface AccessAwareInterface
{
    public function isGranted(Request $request);
}
