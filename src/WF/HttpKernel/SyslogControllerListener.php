<?php

namespace WF\HttpKernel;

use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\CoreBundle\Controller\DefaultController;
use models\Syslog;
use models\User;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Symfony\Bundle\WebProfilerBundle\Controller\ProfilerController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Logs user activity
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SyslogControllerListener
{

    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var array
     */
    private $ignoreUrl = [];

    private $enableSyslog;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage, $ignoredUrls = [], $enableSyslog)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->ignoreUrl = $ignoredUrls;
        $this->enableSyslog = $enableSyslog;
    }

    public function onKernelController(FilterControllerEvent $event)
    {

        if (!$this->enableSyslog) {
            return;
        }

        $r = $event->getRequest();

        if (in_array($r->getPathInfo(), $this->ignoreUrl)) {
            return;
        }

        $ctrl = $event->getController();
        $module = '(default)';
        $action = null;

        if (is_array($ctrl)) {
            $ctrl_0 = $ctrl[0];
            $action = $ctrl[1];
            if (is_object($ctrl_0)) {
                $module = get_class($ctrl_0);
            } else if (is_string($ctrl_0)) {
                $module = $ctrl_0;
            }
        }

        if (in_array($module, [
            DefaultController::class,
            ExceptionController::class,
            ProfilerController::class
        ])) {
            return;
        }

        if (in_array($r->getPathInfo(), $this->ignoreUrl)) {
            return;
        }

        $token = $this->tokenStorage->getToken();
        $user = null;
        if (null !== $token) {
            $user = $token->getUser();
        }

        if ('anon.' === $user)
            $user = null;

        $log = self::fillSyslog($r, $module, $action, $user instanceof User ? $user : null);

        $this->em->persist($log);
        $this->em->flush();
    }

    /**
     * @param Request $request
     * @param $module
     * @param string $action
     * @param User|null $user
     * @return Syslog
     */
    static public function fillSyslog(Request $request, $module, $action = '(default)', User $user = null)
    {
        $log = new Syslog();

        $log->setUserip($request->getClientIp())
            ->setModule($module)
            ->setAction($action)
            ->setGet($request->query->all())
            ->setPost($request->request->all())
            ->setContent($request->getContent())
//            ->setSession($request->getSession()->all())
            ->setResult('OK')
            ->setSessionId(session_id())
            ->setUseragent(sprintf('%s Host: %s', $request->server->get('HTTP_USER_AGENT'), $request->getSchemeAndHttpHost()));

        if ($user instanceof \models\User) {
            $log->setUserid($user->getId())
                ->setUserfio($user->getFio());
        }

        return $log;
    }

}
