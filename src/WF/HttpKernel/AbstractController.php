<?php

namespace WF\HttpKernel;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use models\User;
use Psr\Log\LoggerInterface;
use Stash\Pool;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use WF\HttpKernel\Security\AccessAwareInterface;

/**
 * Description of AbstractController
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
abstract class AbstractController implements ContainerAwareInterface, AccessAwareInterface
{
    /**
     *
     * @var ContainerInterface 
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer()
    {
        return $this->container;
    }

    /**
     * Returns service from container by id
     *
     * @param string $id Service id
     * @return mixed
     */
    protected function get($id)
    {
        return $this->container->get($id);
    }

    /**
     * @param string $name Logger name
     * @return LoggerInterface
     */
    protected function getLogger($name = 'php')
    {
        return $this->getContainer()->get('monolog.logger.' . $name);
    }

    /**
     *
     * @return EventDispatcherInterface
     */
    protected function getDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }

    /**
     *
     * @return EntityManagerInterface
     */
    protected function getEm()
    {
        return $this->container->get('doctrine.orm.default_entity_manager');
    }

    /**
     *
     * @return Connection
     */
    protected function getConn()
    {
        return $this->container->get('doctrine.dbal.default_connection');
    }

    /**
     * @param string $pool
     * @return Pool
     */
    protected function getCachePool($pool = 'default')
    {
        return $this->container->get('cache');
    }
    
    /**
     * 
     * @param string $name Template name
     * @param array $parameters Template parameters
     * @return string
     */
    protected function render($name, array $parameters = [])
    {
        return $this->container->get('templating')->render($name, $parameters);
    }
    
    /**
     * 
     * @param string $name Template name
     * @param array $parameters Template parameters
     * @param integer $status Response http status code
     * @param array $headers Response headers
     * @return Response
     */
    protected function renderResponse($name, array $parameters = [], $status = 200, $headers = [])
    {
        return new Response($this->container->get('templating')->render($name, $parameters), $status, $headers);
    }

    /**
     * @param $name
     * @param array $parameters
     * @param int $referenceType
     * @return string
     */
    protected function generateUrl($name, $parameters = array(), $referenceType = Router::ABSOLUTE_PATH){
        return $this->getRouter()->generate($name, $parameters, $referenceType);
    }

    /**
     * @return Router
     */
    private function getRouter(){
        return $this->getContainer()->get('router');
    }

    /**
     *
     * @return User
     */
    protected function getUser()
    {
        return $this->getEm()->find(User::class, \wf::getUserId());
    }
    
    /**
     * 
     * @param Request $request
     * @return boolean
     */
    public function isGranted(Request $request)
    {
        return false;
    }
}
