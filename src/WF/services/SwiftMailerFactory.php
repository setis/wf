<?php
/**
 * SwiftMailerFactory.php
 * 2016-03-21
 * wf
 */

namespace WF\services;


use Exception;
use Swift_MailTransport;
use Swift_NullTransport;
use Swift_SendmailTransport;
use Swift_SmtpTransport;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

class SwiftMailerFactory
{

    /**
     * TODO quirk use SwiftmailerBundle
     * @param array $params
     */
    public static function getInstance($params) {
        $default = ['transport'=>null];

        $params = array_merge($default, $params);
        $transport = self::getTransport($params['transport']);

        if(!$transport instanceof Swift_NullTransport) {
            unset($params['transport']);
            foreach ($params as $k => $v) {
                $method = 'set' . ucfirst($k);
                try {
                    call_user_func([$transport, $method], $v);
                } catch (Exception $e) {
                    throw new InvalidConfigurationException('Wrong email configuration!');
                }
            }
        }

        return new \Swift_Mailer($transport);
    }

    public static function getTransport($type) {
        switch($type) {
            case "smtp":
                return new Swift_SmtpTransport();
                break;
            case "sendmail":
                return new Swift_SendmailTransport();
                break;
            case "mail":
                return new Swift_MailTransport();
                break;
            case "nulltransport":
                return new Swift_NullTransport();
            break;
            default:
                throw new InvalidConfigurationException('Wrong email transport type!');
        }
    }
}