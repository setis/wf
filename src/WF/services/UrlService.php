<?php
/**
 * mail@artemd.ru
 * 13.03.2016
 */

namespace WF\services;

use Doctrine\ORM\Mapping\Entity;
use wf;

class UrlService
{
    /**
     * Generate parametrized url
     *
     * @param Entity $model
     * @param string $controller
     * @param string $action
     * @return string
     */
    public function makeUrl($model, $controller, $action, $params = array())
    {
        $meta = wf::$em->getClassMetadata(get_class($model));
        $identifiers = $meta->getIdentifierValues($model);

        $params = array_merge($identifiers, $params);

        return trim(wf::$container->getParameter('http_base'), '/') . '/' . $controller . '/' . $action . (!empty($params) ? '?' . http_build_query($params) : '');
    }
}
