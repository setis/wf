<?php
/**
 * SlackLoggerFactory.php
 * 2016-03-28
 * wf
 */

namespace WF\services;

use Monolog\Handler\SlackHandler;
use Psr\Log\LogLevel;
use WF\Monolog\Handler\DummyHandler;

class SlackHandlerFactory
{
    public static function getInstance($params)
    {
        $default = array(
            'token' => null,
            'channel' => 'wf',
            'username' => 'monolog',
            'use_attachment' => true,
            'icon_emoji' => null,
            'level' => LogLevel::DEBUG,
            'bubble' => true,
            'use_short_sttachment' => false,
            'include_context_and_extra' => true,
        );

        $params = array_merge($default, $params);
        
        if (empty($params['token'])) {
            return new DummyHandler();
        }
        
        return new SlackHandler(
            $params['token'],
            $params['channel'],
            $params['username'],
            $params['use_attachment'],
            $params['icon_emoji'],
            $params['level'],
            $params['bubble'],
            $params['use_short_sttachment'],
            $params['include_context_and_extra']
        );
    }
}