<?php

namespace WF\Import\Command;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use League\Csv\Reader;
use models\ListAddr;
use models\ListArea;
use Psr\Log\LoggerInterface;
use repository\ListAddrRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of ImportAddressCommand
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ImportAddressCommand extends Command
{

    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     *
     * @var ListAddrRepository
     */
    private $addressRepo;

    /**
     *
     * @var EntityRepository
     */
    private $areaRepo;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $help = <<<HELP
Address uploading from external file. Uses csv format in UTF8 encoding as described below:

Дом;УслугаДома;ДомРегион;ДомГород;ДомНаселенныйПункт;ДомУлица;ДомКодКЛАДР;ДомДом;ДомЭтажность;ДомКолВоКвартир;ДомКолВоПодъездов;Район;СЦ
АБК зона, д.- корпус 14;ШПД;Новосибирская обл;;Кольцово рп;АБК зона;5 400 100 003 200 550 000 000 000;-;1;;1;Кольцово;Новосибирск
АБК зона, д.- корпус 3;ШПД;Новосибирская обл;;Кольцово рп;АБК зона;5 400 100 003 200 550 000 000 000;-;2;;;Кольцово;Новосибирск
HELP;


        $this->setName('wf:import:address')
            ->addArgument('path', InputArgument::REQUIRED, 'Path to file')
            ->addArgument('kladr', InputArgument::REQUIRED, 'KLADR code, example: Novosib - 540000010000')
            ->addArgument('sc', InputArgument::OPTIONAL, 'default sc for address', 1)
            ->addOption('rm-head', 'r', InputOption::VALUE_NONE, 'Defines whether csv file contains head row')
            ->setDescription('Address uploading from external file.')
            ->setHelp($help);
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->addressRepo = $this->em->getRepository(ListAddr::class);
        $this->areaRepo = $this->em->getRepository(ListArea::class);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $reader = Reader::createFromPath($input->getArgument('path'));
        $rmHead = $input->getOption('rm-head');
        $baseCode = $input->getArgument('kladr');
        $scId = $input->getArgument('sc');
        $reader->setDelimiter(';');

        $result = $reader->fetch();

        $i = 0;

        foreach ($result as $row) {
            ++$i;

            if ($rmHead) {
                $rmHead = false;
                continue;
            }

            $house = $row[7];
            $floorCount = $row[8] ?: null;
            $flatCount = $row[9] ?: null;
            $areaTitle = trim($row[11]);

            $kladrCode = substr(preg_replace('/[^\d]+/', '', $row[6]), 0, -6);

            if (!$kladrCode) {
                $this->logger->warning('Skip: empty kladr code, row#' . $i, $row);
                continue;
            }

            if (empty($house) || '-' === $house) {
                $this->logger->info('Skip: empty house, row#' . $i, $row);
                continue;
            }

            if ($this->addressRepo->isAddressExists($house, $kladrCode)) {
                $this->logger->info('Skip: exists', [
                    'house' => $house,
                    'kladr' => $kladrCode,
                ]);
                continue;
            }

            $address = (new ListAddr())
                ->setName($house)
                ->setKladrcode($kladrCode)
                ->setFloorcount($floorCount)
                ->setFlatcount($flatCount)
                ->setScId($scId);

            $this->em->persist($address);

            $this->logger->info('Added', [
                'house' => $house,
                'kladr' => $kladrCode,
            ]);

            $area = $this->areaRepo->findOneBy([
                'title' => $areaTitle,
                'kladrCode' => $baseCode]);

            if (null === $area) {
                $area = (new ListArea())
                    ->setKladrCode($baseCode)
                    ->setTitle($areaTitle);

                $this->em->persist($area);

                $this->logger->info('Added area', [
                    'title' => $area->getTitle(),
                    'kladr' => $area->getKladrCode(),
                ]);
            }

            $address->setAreaId($area->getId());

            $this->em->flush();
        }

        $this->logger->debug('Flushed');
        $this->logger->info('Complete');
    }
}
