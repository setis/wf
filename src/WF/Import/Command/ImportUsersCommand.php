<?php

namespace WF\Import\Command;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use League\Csv\Reader;
use models\Department;
use models\JobPosition;
use models\User;
use Psr\Log\LoggerInterface;
use repository\DepartmentRepository;
use repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Description of ImportAddressCommand
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ImportUsersCommand extends Command
{

    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     *
     * @var UserRepository
     */
    private $userRepo;

    /**
     *
     * @var EntityRepository
     */
    private $positionRepo;

    /**
     * @var DepartmentRepository $departmentRepo
     */
    public $departmentRepo;

    /**
     * @var EventDispatcherInterface
     */
    public $ed;

    /**
     * @var string
     */
    private $salt;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger, EventDispatcherInterface $ed, $salt = '')
    {
        $this->em = $em;
        $this->ed = $ed;
        $this->logger = $logger;
        $this->salt = $salt;
        parent::__construct();
    }

    protected function configure()
    {
        $help = <<<HELP
Users uploading from external file. Uses csv format in UTF8 encoding as described below:

Должность;Фио;Логин;Пароль;email
Руководитель СЦ;Розбах Марина Николаевна;m.rozbah;;
HELP;


        $this->setName('wf:import:users')
            ->addArgument('path', InputArgument::REQUIRED, 'Path to file')
            ->addArgument('department_id', InputArgument::REQUIRED, 'Department id')
            ->addArgument('pass', InputArgument::OPTIONAL, 'Regenerate empty passwords, 1 - regenerate all passwords (in file passwords will be ignored), 2 - generate only empty passwords, 3 - use passwords from file', 1)
            ->addOption('rm-head', 'r', InputOption::VALUE_NONE, 'Defines whether csv file contains head row')
            ->setDescription('Users uploading from external file.')
            ->setHelp($help);
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->userRepo = $this->em->getRepository(User::class);
        $this->positionRepo = $this->em->getRepository(JobPosition::class);
        $this->departmentRepo = $this->em->getRepository(Department::class);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $_positionsCache = [];
        $reader = Reader::createFromPath($input->getArgument('path'));
        $rmHead = $input->getOption('rm-head');
        $generatePasswords = $input->getArgument('pass');
        $reader->setDelimiter(';');
        $department_id = $input->getArgument('department_id');

        /** @var Department $department */
        $department = $this->departmentRepo->find($department_id);

        if (null === $department) {
            $this->logger->info('Exit: empty department', [
                'department id' => $department_id,
            ]);

            return false;
        }

        $result = $reader->fetch();

        $i = 0;

        foreach ($result as $row) {
            ++$i;

            if ($rmHead) {
                $rmHead = false;
                continue;
            }

            $position = trim($row[0]) ?: null;
            $name = trim($row[1]) ?: null;
            $login = trim($row[2]) ?: null;
            $pass = trim($row[3]) ?: null;
            $email = trim($row[4]) ?: null;

            if (!$name || !$login) {
                $this->logger->warning('Skip: empty login or name row#' . $i, $row);
                continue;
            }

            $foundUser = $this->userRepo->createQueryBuilder('user')
                ->andWhere('user.login = :login')
                ->setParameter('login', $login)
                ->getQuery()->getOneOrNullResult();

            if (null !== $foundUser) {
                $this->logger->info('Skip: exists user', [
                    'login' => $login,
                ]);
                continue;
            }

            if (!isset($_positionsCache[$position])) {
                $foundJobPosition = $this->positionRepo->createQueryBuilder('position')
                    ->andWhere('position.title = :position')
                    ->setParameter('position', $position)
                    ->getQuery()->getOneOrNullResult();


                if (null === $foundJobPosition) {
                    $this->logger->info('Skip: not existing job position', [
                        'position' => $position,
                    ]);
                    continue;
                }

                $_positionsCache[$position] = $foundJobPosition;
            } else {
                $foundJobPosition = $_positionsCache[$position];
            }

            switch (true) {
                case 1 === $generatePasswords:
                    $pass = hash('sha256', $this->salt . uniqid());
                    break;
                case 2 === $generatePasswords && null === $pass:
                    $pass = hash('sha256', $this->salt . uniqid());
                    break;
                default:
                    $pass = hash('sha256', $this->salt . $pass);
            }

            $user = (new User())
                ->setLogin($login)
                ->setFio($name)
                ->setJobPosition($foundJobPosition)
                ->setPass($pass)
                ->setEmail($email)
                ->setDepartment($department)
                ->setActive(true);

            $this->em->persist($user);

            $this->logger->info('Added', [
                'login' => $login,
            ]);

            $this->em->flush();
        }

        $this->logger->debug('Flushed');
        $this->logger->info('Complete');
    }
}
