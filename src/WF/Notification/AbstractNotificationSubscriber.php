<?php

namespace WF\Notification;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use models\Gfx;
use models\Task;
use models\User;
use Psr\Log\LoggerInterface;
use repository\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Templating\EngineInterface;
use WF\Notification\Model\HandlerInterface;
use WF\Notification\Model\Notification;
use WF\Notification\Model\NotificationInterface;
use WF\Technician\EventDispatcher\GraphicTimeExpiredEvent;

/**
 * Description of AbstractNotificationListener
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
abstract class AbstractNotificationSubscriber implements EventSubscriberInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     *
     * @var EngineInterface
     */
    protected $templating;

    /**
     *
     * @var UrlGeneratorInterface;
     */
    protected $urlGenerator;

    /**
     *
     * @var UserRepository
     */
    protected $userRepo;

    /**
     *
     * @var HandlerInterface[]
     */
    private $handlers;

    public function __construct(
        EntityManagerInterface $em,
        LoggerInterface $logger,
        EngineInterface $templating,
        UrlGeneratorInterface $urlGenerator)
    {
        $this->em = $em;
        $this->userRepo = $em->getRepository(User::class);
        $this->logger = $logger;
        $this->templating = $templating;
        $this->urlGenerator = $urlGenerator;
        $this->handlers = [];
    }

    public function pushHandler($key, HandlerInterface $handler)
    {
        $this->handlers[$key] = $handler;
    }

    public function onTechScheduleTimeExpired(GraphicTimeExpiredEvent $event)
    {
        $tech = $event->getUser();
        /* @var $schedule Gfx */
        $schedule = $tech->getSchedules()->first();
        if (!$schedule || null === $schedule->getTask()) {
            return;
        }

        $task = $schedule->getTask();
        if ($this->notSupported($task)) {
            return;
        }

        $link = $this->getTaskLink($task);

        $slackMessage = $this->renderMessage('Task/Notification/techScheduleTimeExpired.md.php', [
            'task' => $task,
            'link' => $link,
            'tech' => $tech,
        ]);

        $recipients = $this->getUserRepo()->findUserChiefs($tech);
        $notification = (new Notification())
            ->setTo($recipients)
            ->setMessage($slackMessage);

        $this->handle('slack', $notification);
    }

    protected function notSupported(Task $task)
    {
        return true;
    }

    protected function getEm()
    {
        return $this->em;
    }

    protected function getLogger()
    {
        return $this->logger;
    }

    protected function getUserRepo()
    {
        return $this->userRepo;
    }

    protected function handle($handlerKey, NotificationInterface $notification)
    {
        if (!isset($this->handlers[$handlerKey])) {
            $this->logger
                ->error(
                    sprintf('Handler with key "%s" is not setted', $handlerKey),
                    [
                        'file' => $ex->getFile(),
                        'line' => $ex->getLine(),
                    ]);
            return false;
        }

        $handler = $this->handlers[$handlerKey];
        try {
            $handler->handle($notification);
        }
        catch (Exception $ex) {
            $this->logger
                ->error(
                    sprintf('Notification handling exception by handler %s',
                        get_class($handler)),
                    [
                        'message' => $ex->getMessage(),
                        'file' => $ex->getFile(),
                        'line' => $ex->getLine(),
                    ]);
            return false;
        }

        return true;
    }

    /**
     *
     * @param Task $task
     * @param string $action
     * @return string
     */
    protected function getTaskLink(Task $task, $action = 'viewticket')
    {
        return $this->urlGenerator->generate('legacy_rule_default', [
            'plugin_uid' => $task->getPluginUid(),
            'plugin_event' => $action,
            'task_id' => $task->getId(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    protected function renderMessage($name, array $parameters)
    {
        return $this->templating->render($name, $parameters);
    }

    protected function getTaskAuthor(Task $task)
    {
        return $task->getAuthor();
    }

    protected function getTaskExecutors(Task $task, $exclude = null)
    {
        return $this->getUserRepo()->getTaskExecutors($task, $exclude, true);
    }

    protected function logError(Task $task, $message, array $context = [])
    {
        $this->logger->error($message, array_merge([
            'task' => $task->getId()
        ], $context));
    }
}
