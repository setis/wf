<?php
/**
 * Artem
 * 25.04.2016 20:29
 */

namespace WF\Notification\Model;


use models\SlackChannel;
use models\SlackMessage;
use WF\Slack\Model\SlackManagerInterface;
use WF\Slack\Notification\SlackNotification;

class SlackHandler extends AbstractHandler implements HandlerInterface
{
    /**
     * @var string
     */
    private $name = 'slack-уведомление';

    /**
     * @var SlackManagerInterface
     */
    private $manager;

    private $default_sender_name;

    public function __construct(SlackManagerInterface $manager, $default_sender_name = 'SYSTEM')
    {
        $this->manager = $manager;
        $this->default_sender_name = $default_sender_name;
    }

    public function isHandling(NotificationInterface $notification)
    {
        if ($this->mapped($notification->getLevel()))
            return true;

        return false;
    }

    public function handle(NotificationInterface $notification)
    {
        $message = new SlackMessage();

        if($notification instanceof SlackNotification) {
            $message->setAttachments($notification->getAttachments());
        } else {
            $at = [
                [
                    "fallback" => $notification->getMessage(),
                    "color" => $this->getAttachmentColor($notification->getLevel()),
                    "author_name" => $this->default_sender_name,
                    "text" => sprintf(
                        "*%s*\n%s",
                        $notification->getSubject(), $notification->getMessage()),
                ]
            ];
            if ($notification->getFrom() && $notification->getFrom()->getFio()) {
                $at[0]['author_name'] = $notification->getFrom()->getFio();
                $at[0]['fallback'] = $notification->getFrom()->getFio() . ":\n" . $notification->getMessage();
            }

            $message->setAttachments(json_encode($at));
        }


        $channel = new SlackChannel();
        foreach ($notification->getTo() as $to) {
            $user = $to->getSlackUser();
            if ($user === null) {
                //throw new NotificationSendException('Slack user is null');
                continue;
            }

            $channel->setId($to->getSlackUser()->getId());
            if (!$this->manager->sendMessage($channel, $message)) {
                //throw new NotificationSendException('Can\'t send Slack message to ' . $to->getSlackUser()->getName());
                continue;
            }
        }

        return true;
    }

    /**
     * Returned a Slack message attachment color associated with
     * provided level.
     *
     * @param  int $level
     * @return string
     */
    protected function getAttachmentColor($level)
    {
        switch (true) {
            case $level >= NotificationInterface::ALERT:
                return 'danger';
            case $level >= NotificationInterface::CRITICAL:
                return 'warning';
            case $level >= NotificationInterface::NOTICE:
                return 'good';
            default:
                return '#e3e4e6';
        }
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
