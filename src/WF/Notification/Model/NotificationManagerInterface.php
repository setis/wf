<?php
/**
 * Artem
 * 26.04.2016 20:00
 */
namespace WF\Notification\Model;

interface NotificationManagerInterface
{
    public function pushHandler(HandlerInterface $handler);

    public function popHandler();

    public function setHandlers(array $handlers);

    public function getHandlers();

    public function sendNotification(NotificationInterface $notification);

    public function critical(NotificationInterface $notification);

    public function alert(NotificationInterface $notification);

    public function notice(NotificationInterface $notification);

    public function info(NotificationInterface $notification);
}