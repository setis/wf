<?php
/**
 * Artem
 * 25.04.2016 20:24
 */

namespace WF\Notification\Model;


interface HandlerInterface
{
    public function getName();

    public function isHandling(NotificationInterface $notification);

    public function handle(NotificationInterface $notification);

    public function isPublic();
}