<?php

namespace WF\Notification\Model;

use models\SlackChannel;
use models\SlackMessage;
use WF\Slack\Model\SlackManagerInterface;
use WF\Slack\Notification\SlackNotificationInterface;

/**
 * Description of SlackBotHandler
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SlackBotHandler extends AbstractHandler
{
    /**
     * @var string
     */
    private $name = 'slack-уведомление';

    /**
     * @var SlackManagerInterface
     */
    private $manager;

    private $botKey;

    public function __construct(SlackManagerInterface $manager, $botKey)
    {
        $this->manager = $manager;
        $this->botKey = $botKey;
    }

    public function isHandling(NotificationInterface $notification)
    {
        if ($this->mapped($notification->getLevel())) {
            return true;
        }

        return false;
    }

    /**
     * @param SlackNotificationInterface|NotificationInterface $notification
     * @return bool
     */
    public function handle(NotificationInterface $notification)
    {
        $message = new SlackMessage();
        $message->setAttachments( $notification->getEncodedAttachments() );

        if($notification->getSubject()) {
            $message->setText($notification->getSubject());
        } elseif($notification->getMessage()) {
            $message->setText($notification->getMessage());
        }

        $channel = new SlackChannel();
        foreach ($notification->getTo() as $to) {
            $user = $to->getSlackUser();
            if ($user === null) {
                continue;
            }

            $channel->setId($to->getSlackUser()->getId());
            if (!$this->manager->sendMessage($channel, $message, $this->botKey)) {
                continue;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returned a Slack message attachment color associated with
     * provided level.
     *
     * @param  int $level
     * @return string
     */
    protected function getAttachmentColor($level)
    {
        switch (true) {
            case $level >= NotificationInterface::ALERT:
                return 'danger';
            case $level >= NotificationInterface::CRITICAL:
                return 'warning';
            case $level >= NotificationInterface::NOTICE:
                return 'good';
            default:
                return '#e3e4e6';
        }
    }
}
