<?php

namespace WF\Notification\Model;

use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of ConsoleHandler
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ConsoleHandler extends AbstractHandler
{
    /**
     *
     * @var OutputInterface 
     */
    private $output;
    
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
        return $this;
    }
    
    public function onConsoleCommand(ConsoleCommandEvent $event)
    {
        $this->setOutput($event->getOutput());
    }

    public function getName()
    {
        return 'console';
    }

    public function handle(NotificationInterface $notification)
    {
        if ($this->output === null) {
            return;
        }
        
        if (null !== $subj = $notification->getSubject()) {
            $this->output->writeln("<info>Subject:</info> $subj");
        }
        
        if (null !== $msg = $notification->getMessage()) {
            $this->output->writeln("<info>Message:</info> $msg");
        }
        
        if (null !== $from = $notification->getFrom()) {
            $this->output->writeln("<info>From:</info> {$from->getId()}:{$from->getFio()}");
        }
        
        if (null !== $to = $notification->getTo()) {
            $this->output->writeln("<info>To:</info> " . implode(', ', array_map(function($user) { return $user->getFio(); }, $to)));
        }
    }

    public function isHandling(NotificationInterface $notification)
    {
        return true;
    }

    public function isPublic()
    {
        return false;
    }
}
