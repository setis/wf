<?php
/**
 * Artem
 * 25.04.2016 20:29
 */

namespace WF\Notification\Model;


use models\User;
use Swift_Mailer;
use Swift_Message;

class EmailHandler extends AbstractHandler implements HandlerInterface
{
    /**
     * @var string
     */
    private $name = 'email-уведомление';

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    private $default_sender_email;

    private $email_username;

    public function __construct(Swift_Mailer $mailer, $default_sender_email = 'no-replay@example.com', $email_username = null)
    {
        $this->mailer = $mailer;
        $this->default_sender_email = $default_sender_email;
        $this->email_username = $email_username;
    }

    public function isHandling(NotificationInterface $notification)
    {
        if($this->mapped($notification->getLevel()))
            return true;

        return false;
    }

    /**
     * @param NotificationInterface $notification
     * @return bool
     */
    public function handle(NotificationInterface $notification)
    {
        $mailer = $this->mailer;

        foreach ($notification->getTo() as $to) {
            $mail = $to->getEmail();
            if (empty($mail)) {
                continue;
            }
            /** @var Swift_Message $message */
            $message = $mailer->createMessage();
            $message
                ->setSubject($notification->getSubject())
                ->setFrom($this->default_sender_email)
                ->setTo($to->getEmail())
                ->setBody(
                    $notification->getMessage(),
                    'text/html'
                );

            if ($notification->getFrom() && $notification->getFrom()->getEmail())
                $message->setFrom($notification->getFrom()->getEmail());

            if (!empty($this->email_username)) {
                $message->setReturnPath($this->email_username);
            }

            if ($mailer->send($message) <= 0)
                throw new NotificationSendException('Can\'t send EMAIL to '.$to->getEmail());
        }

        return true;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
