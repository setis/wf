<?php
/**
 * Artem
 * 25.04.2016 20:36
 */

namespace WF\Notification\Model;


abstract class AbstractHandler implements HandlerInterface
{
    /**
     * @var array
     */
    private $handledLevel;
    /**
     * @param string $level
     * @return bool
     */

    protected function mapped($level) {
        if(!empty($this->handledLevel)) {
            return $this->handledLevel <= $level;
        }
        return true;
    }
    /**
     * {@inheritdoc}
     */
    public function handleBatch(array $records)
    {
        foreach ($records as $record) {
            $this->handle($record);
        }
    }

    /**
     * Closes the handler.
     *
     * This will be called automatically when the object is destroyed
     */
    public function close()
    {
    }

    /**
     * @return mixed
     */
    public function getHandledLevel()
    {
        return $this->handledLevel;
    }

    /**
     * @param mixed $handledLevel
     */
    public function setHandledLevel($handledLevel)
    {
        $this->handledLevel = $handledLevel;
    }

    public function isPublic()
    {
        return true;
    }
}