<?php
/**
 * Artem
 * 25.04.2016 20:29
 */

namespace WF\Notification\Model;


use WF\Sms\Model\SmsManagerInterface;
use WF\Sms\SmsMessage;
use models\User;

class SmsHandler extends AbstractHandler implements HandlerInterface
{
    /**
     * @var string
     */
    private $name = 'sms-уведомление';

    /**
     * @var SmsManagerInterface
     */
    private $manager;

    public function __construct(SmsManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function isHandling(NotificationInterface $notification)
    {
        if ($this->mapped($notification->getLevel()))
            return true;

        return false;
    }

    public function handle(NotificationInterface $notification)
    {
        $to = array_map(function(User $user) {
            return $user->getPhones();
        }, $notification->getTo());
        $message = new SmsMessage($notification->getMessage(), $to);
        $this->manager->send($message);

        return true;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
