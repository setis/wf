<?php
/**
 * Artem
 * 25.04.2016 20:40
 */

namespace WF\Notification\Model;

use models\User;

class Notification implements NotificationInterface
{

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var User[]
     */
    private $to;

    /**
     * @var User
     */
    private $from;

    /**
     * @var string
     */
    private $level = NotificationInterface::INFO;

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return self
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }


    /**
     * @param User $from
     * @return self
     */
    public function setFrom(User $from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param User|User[] $to
     * @return self
     */
    public function setTo($to)
    {
        if(!is_array($to)) {
            $to = [$to];
        }

        foreach ($to as $recipient) {
            if ($recipient instanceof User) {
                $this->addTo($recipient);
            }
        }

        return $this;
    }

    /**
     * @param $user
     * @return $this
     */
    public function addTo(User $user)
    {
        $this->to[] = $user;
        return $this;
    }

    /**
     * @return User[]
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param string $level
     * @return $this
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    public function setUpperLevel()
    {
        $level = $this->getLevel();
        switch ($level) {
            case NotificationInterface::INFO:
                $this->setLevel(NotificationInterface::NOTICE);
                break;
            case NotificationInterface::NOTICE:
                $this->setLevel(NotificationInterface::CRITICAL);
                break;
            case NotificationInterface::CRITICAL:
                $this->setLevel(NotificationInterface::ALERT);
                break;
            default:
                throw new \LogicException('Can\'t process notification top level.');
        }
    }

}
