<?php
/**
 * Artem
 * 25.04.2016 20:39
 */

namespace WF\Notification\Model;

use LogicException;
use models\User;

interface NotificationInterface
{
    const ALERT = 550;

    const CRITICAL = 500;

    const NOTICE = 250;

    const INFO = 200;

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return string
     */
    public function getSubject();

    /**
     * @return User[]
     */
    public function getTo();

    /**
     * @return User
     */
    public function getFrom();

    /**
     * @return string
     */
    public function getLevel();
    
    /**
     * @return void
     * @throws LogicException
     */
    public function setUpperLevel();
}