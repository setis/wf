<?php

/**
 * Artem
 * 25.04.2016 20:21
 */

namespace WF\Notification;

use Psr\Log\LoggerInterface;
use WF\Notification\Model\HandlerInterface;
use WF\Notification\Model\Notification;
use WF\Notification\Model\NotificationInterface;
use WF\Notification\Model\NotificationManagerInterface;
use Exception;

class NotificationManager implements NotificationManagerInterface
{

    /**
     * @var Model\HandlerInterface[]
     */
    protected $handlers;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * NotificationManager constructor.
     * @param HandlerInterface[] $handlers
     */
    public function __construct(array $handlers = array(), LoggerInterface $logger)
    {
        $this->handlers = $handlers;
        $this->logger = $logger;
    }

    /**
     * Pushes a handler on to the stack.
     *
     * @param  HandlerInterface $handler
     * @return $this
     */
    public function pushHandler(HandlerInterface $handler)
    {
        array_unshift($this->handlers, $handler);

        return $this;
    }

    /**
     * Pops a handler from the stack
     *
     * @return HandlerInterface
     */
    public function popHandler()
    {
        if (!$this->handlers) {
            throw new \LogicException('You tried to pop from an empty handler stack.');
        }

        return array_shift($this->handlers);
    }

    /**
     * Set handlers, replacing all existing ones.
     *
     * If a handledLevel is passed, keys will be ignored.
     *
     * @param  HandlerInterface[] $handlers
     * @return $this
     */
    public function setHandlers(array $handlers)
    {
        $this->handlers = array();
        foreach (array_reverse($handlers) as $handler) {
            $this->pushHandler($handler);
        }

        return $this;
    }

    /**
     * @return HandlerInterface[]
     */
    public function getHandlers()
    {
        return $this->handlers;
    }

    /**
     * Sends notification.
     * если в Notification поменять левел на более высокий, то приоритет вырастет
     * если на менее приоритетном транспорте сломается что то, то вызов пойдёт на более приоритетный
     *
     * @param  Notification $notification The notification message
     * @return Boolean Whether the record has been processed
     */
    public function sendNotification(NotificationInterface $notification)
    {
        if (!$this->handlers) {
            throw new \LogicException('Notification handlers are empty!');
        }

        /** @var HandlerInterface $handler */
        while ($handler = current($this->handlers)) {
            if ($handler->isHandling($notification)) {
                try {
                    if (true === $handler->handle($notification)) {
                        $this->logger->debug('Notification send.', ['notification' => $notification->getLevel(), 'handler' => $handler->getName()]);
                        break;
                    }
                }
                catch (Exception $e) {
                    $this->logger->warning('Notification not send.', ['notification' => $notification->getLevel(), 'handler' => $handler->getName()]);
                    try {
                        $notification->setUpperLevel();
                    }
                    catch (Exception $ex) {
                        return false;
                    }

                    next($this->handlers);
                    if (!current($this->handlers)) {
                        reset($this->handlers);
                    }
                    continue;
                }
            }

            next($this->handlers);
        }

        return true;
    }

    /**
     * @param Notification $notification
     * @return bool
     */
    public function critical(NotificationInterface $notification)
    {
        if ($notification->getLevel() < NotificationInterface::CRITICAL)
            $notification->setLevel(NotificationInterface::CRITICAL);

        return $this->sendNotification($notification);
    }

    /**
     * @param Notification $notification
     * @return bool
     */
    public function alert(NotificationInterface $notification)
    {
        if ($notification->getLevel() < NotificationInterface::ALERT)
            $notification->setLevel(NotificationInterface::ALERT);

        return $this->sendNotification($notification);
    }

    /**
     * @param Notification $notification
     * @return bool
     */
    public function notice(NotificationInterface $notification)
    {
        if ($notification->getLevel() < NotificationInterface::NOTICE)
            $notification->setLevel(NotificationInterface::NOTICE);

        return $this->sendNotification($notification);
    }

    /**
     * @param Notification $notification
     * @return bool
     */
    public function info(NotificationInterface $notification)
    {
        if ($notification->getLevel() < NotificationInterface::INFO)
            $notification->setLevel(NotificationInterface::INFO);

        return $this->sendNotification($notification);
    }

}
