<?php

namespace WF\Core;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface ClientInterface
{
    public function getClientId();
    
    public function getClientName();
}
