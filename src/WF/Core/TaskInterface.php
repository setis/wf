<?php

namespace WF\Core;
use models\Gfx;
use WF\Task\Model\TechnicalTicketInterface;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TaskInterface
{
    public function getId();

    public function getTaskTitle();

    /**
     * Returns models\Ticket for service tasks and models\Gp for global problem tasks
     *
     * @return TechnicalTicketInterface
     */
    public function getAnyTicket();

    /**
     * @return string
     */
    public function getPluginUid();

    /**
     * TODO Pablo сделай интерфейс
     * @return Gfx
     */
    public function getSchedule();
}
