<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 19.04.2016
 * Time: 11:28
 */

namespace WF\Core;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\Helper\Helper;

class ParametersHelper extends Helper
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ContainerHelper constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $name
     * @param null $default
     * @return mixed|null
     */
    public function get($name, $default = null) {
        if($this->container->hasParameter($name))
            return $this->container->getParameter($name);

        return $default;
    }

    /**
     * @param $name
     * @return bool
     */
    public function has($name) {
        return $this->container->hasParameter($name);
    }

    /**
     * Returns the canonical name of this helper.
     *
     * @return string The canonical name
     */
    public function getName()
    {
        return 'param';
    }
}