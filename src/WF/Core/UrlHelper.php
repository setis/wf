<?php
/**
 * Artem
 * 16.05.2016 9:45
 */

namespace WF\Core;


use Symfony\Component\Routing\Router;
use Symfony\Component\Templating\Helper\Helper;

class UrlHelper extends Helper
{

    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function getName()
    {
        return 'url';
    }

    public function generate($name, $parameters = array(), $referenceType = Router::ABSOLUTE_PATH)
    {
        return $this->router->generate($name, $parameters, $referenceType);
    }
}