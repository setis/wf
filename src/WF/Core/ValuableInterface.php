<?php

namespace WF\Core;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface ValuableInterface
{
    /**
     * @return double
     */
    public function getAmount();
    
    /**
     * @return string Currency code from ISO 4217 (RUB, USD, EUR)
     */
    public function getCurrency();
}
