<?php

namespace WF\Core;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface MeasureUnitInterface
{
    public function getTitle();
    
    public function isMetrical();
}
