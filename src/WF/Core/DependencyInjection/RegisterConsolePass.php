<?php

namespace WF\Core\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Description of RegisterConsolePass
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class RegisterConsolePass implements CompilerPassInterface
{
    /**
     * @var string
     */
    protected $cliService;

    /**
     * @var string
     */
    protected $helperSetService;

    /**
     * @var string
     */
    protected $commandTag;

    /**
     * @var string
     */
    protected $helperTag;

    public function __construct($cliService = 'console', $helperSetService = 'console.helper_set', $commandTag = 'console.command', $helperTag = 'console.helper')
    {
        $this->cliService = $cliService;
         $this->helperSetService = $helperSetService;
        $this->commandTag = $commandTag;
        $this->helperTag = $helperTag;
    }

    public function process(ContainerBuilder $container)
    {
        $this->processCommands($container);
        $this->processHelpers($container);
    }

    private function processCommands(ContainerBuilder $container)
    {
        if (!$container->hasDefinition($this->cliService) && !$container->hasAlias($this->cliService)) {
            return;
        }

        $definition = $container->findDefinition($this->cliService);

        foreach ($container->findTaggedServiceIds($this->commandTag) as $id => $attrs) {
            $def = $container->getDefinition($id);

            if ($def->isAbstract()) {
                throw new \InvalidArgumentException(sprintf('The service "%s" must not be abstract as commands are lazy-loaded.', $id));
            }

            $class = $container->getParameterBag()->resolveValue($def->getClass());
            $base = 'Symfony\Component\Console\Command\Command';

            if (!is_subclass_of($class, $base)) {
                throw new \InvalidArgumentException(sprintf('Service "%s" must be subclass of "%s". "%s given"', $id, $base, $class));
            }

            $definition->addMethodCall('add', [$def]);
        }
    }

    private function processHelpers(ContainerBuilder $container)
    {
        if (!$container->hasDefinition($this->helperSetService) && !$container->hasAlias($this->helperSetService)) {
            return;
        }

        $definition = $container->findDefinition($this->helperSetService);

        foreach ($container->findTaggedServiceIds($this->helperTag) as $id => $attrs) {
            $def = $container->getDefinition($id);

            if ($def->isAbstract()) {
                throw new \InvalidArgumentException(sprintf('The service "%s" must not be abstract as commands are lazy-loaded.', $id));
            }

            $class = $container->getParameterBag()->resolveValue($def->getClass());
            $interface = 'Symfony\Component\Console\Helper\HelperInterface';

            if (!is_subclass_of($class, $interface)) {
                throw new \InvalidArgumentException(sprintf('Service "%s" must implement interface "%s".', $id, $interface));
            }

            foreach ($attrs as $attr) {
                if (!isset($attr['helper'])) {
                    throw new \InvalidArgumentException(sprintf('Service "%s" must define the "helper" attribute on "%s" tags.', $id, $this->helperTag));
                }
                $definition->addMethodCall('set', [$def, $attr['helper']]);
            }
        }
    }
}
