<?php
/**
 * mail@artemd.ru
 * 29.03.2016
 */

namespace WF\Core;


interface LoginManagerInterface
{
    public function login($login, $password);

    public function logout();
}
