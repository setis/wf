<?php
/**
 * Artem
 * 16.05.2016 10:31
 */

namespace WF\Core;


use Symfony\Component\Templating\Helper\Helper;

/**
 * TODO Artem implement this class
 *
 * Class AlertsHelper
 * @package WF\Core
 */
class AlertsHelper extends Helper
{

    public function getName()
    {
        return 'alerts';
    }

    public function hasAlerts()
    {
        return false;
    }

    public function getAlerts()
    {
        return [];
    }

    public function addAlert()
    {
        
    }
}