<?php
/**
 * mail@artemd.ru
 * 03.04.2016
 */

namespace WF\System\Controller;


use models\NbnState;
use models\Task;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WF\HttpKernel\AbstractController;

class ToolsController extends AbstractController
{

    public function blackHoleAction(Request $request)
    {
        $request_string = d($request, true, true, true);
        $this->getLogger('debug')->alert($request_string);

        return new Response($request_string);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \ErrorException
     * @throws NotFoundHttpException
     * @throws \InvalidArgumentException
     */
    public function openTaskAction(Request $request)
    {
        $id = $request->get("object_id");
        if (empty($id)) {
            return new RedirectResponse('/');
        }

        /** @var \models\Task $model */
        $model = $this->getEm()->find(Task::class, $id);
        if (null === $model) {
            throw new NotFoundHttpException("Указанный объект не найден!");
        }

        if ($url = $this->makeUrl($model)) {
            return new RedirectResponse($url);
        }

        throw new NotFoundHttpException("Не определен тип объекта!");
    }

    /**
     * TODO Artem move this into some manager
     * Generates url from Task
     *
     * @param \models\Task $task
     * @return bool|string
     */
    public function makeUrl(\models\Task $task)
    {
        switch ($task->getPluginUid()) {
            case Task::PROJECT:
                $subaction = "viewtask";
                $var = "task_id";
                break;

            case Task::BILL:
                $subaction = "viewbill";
                $var = "task_id";
                break;

            case Task::SERVICE:
            case Task::GLOBAL_PROBLEM:
            case Task::TELEMARKETING:
            case Task::ACCIDENT:
            case Task::CONNECTION:
                $subaction = "viewticket";
                $var = "task_id";
                break;

            default:
                $subaction = false;
                $var = false;
                break;
        }
        if ($subaction !== false && $var !== false) {
            return $task->getPluginUid() . "/" . $subaction . "?" . $var . "=" . $task->getId();
        }

        return false;
    }

    public function isGranted(Request $request)
    {
        return true;
    }

}
