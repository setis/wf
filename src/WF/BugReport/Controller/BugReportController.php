<?php
namespace WF\BugReport\Controller;

use GuzzleHttp\Client;
use models\BugReport;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use WF\HttpKernel\RestController;

class BugReportController extends RestController
{
    public function receiveReportAction(Request $request)
    {
        // Jira
        $ticket_body =
            "*Какое действие вы хотели совершить?*\r\n" .
            $request->get('br_action') .

            "\r\n*Что не получилось?*\r\n" .
            $request->get('br_failure') .

            "\r\n*Как по вашему мнению это должно работать?*\r\n" .
            $request->get('br_wish') . '
        
            Пользователь: ' . $this->getUser()->getName() . '
            Email: ' . $this->getUser()->getEmail() . '
            Телефон: ' . $this->getUser()->getPhones() . '
        
            URL: ' . $request->getHttpHost();

        $data = [
            'fields' => [
                'project' => ['key' => 'WF'],
                "summary" => $request->get('br_failure'),
                "description" => ($ticket_body),
                "issuetype" => [
                    "name" => "Bug"
                ]
            ]
        ];

        $jira_action = $this->getContainer()->getParameter('jira_ticket_create_api_address');

        $client = new Client([
            'defaults' => [
                'verify' => true
            ],
            'connect_timeout' => 3.14
        ]);

        $data = [
            'auth' => [
                $this->getContainer()->getParameter('jira_user'),
                $this->getContainer()->getParameter('jira_password')
            ],
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'json' => $data
        ];

        try {
            $res = $client->request('POST', $jira_action, $data);
            $ticket_id = json_decode($res->getBody())->key;

            // DB
            $report = new BugReport();
            $report->setAction($request->get('br_action'))
                ->setFailure($request->get('br_failure'))
                ->setWish($request->get('br_wish'))
                ->setAuthor($this->getUser())
                ->setEmail($this->getUser()->getEmail())
                ->setPhone($this->getUser()->getPhones())
                ->setName($this->getUser()->getName())
                ->setUrl($request->getHttpHost())
                ->setDate(new \DateTime())
                ->setEnvironment(serialize($request->server))
                ->setId($ticket_id);

            $this->getEm()->persist($report);
            $this->getEm()->flush();
        } catch (\Exception $exception) {
            throw $exception;
        }

        if ($request->files->has('files')) {
            foreach ($request->files->get('files') as $file) {
                if ($file instanceof UploadedFile && $file->isReadable()) {

                    $response = $client->request('POST', $jira_action . '/' . $report->getId() . '/attachments', [
                        'auth' => [
                            $this->getContainer()->getParameter('jira_user'),
                            $this->getContainer()->getParameter('jira_password')
                        ],
                        'headers' => [
                            'X-Atlassian-Token' => 'no-check',
                        ],
                        'multipart' => [
                            [
                                'name' => 'file',
                                'contents' => fopen($file->getRealPath(), 'r'),
                                'filename' => $file->getClientOriginalName()
                            ]
                        ]
                    ]);

                }
            }
        }

        // send email to reporter
        $to = $this->getUser()->getEmail();

        if ($to) {
            $subject = "Сообщение GERP [ " . $ticket_id . " ] доставлено разработчикам";
            $name = 'Разработчик GERP';
            $text = '
                <p>Вашему обращению присвоен номер: ' . $ticket_id . '.</p>
                <p>В ближайшее время мы проанализируем полученную информацию и уведомим вас о принятых мерах.</p>
                <p>Спасибо.</p>   
                ';
            $email = $this->getContainer()->getParameter('developer_email');
            $phone = $this->getContainer()->getParameter('company_phone');

            $message_body = $this->tplMaker($name, $text, $email, $phone);
            $this->sender($to, $email, $subject, $message_body);
        }

        $subject = "Новая задача в Jira через форму GERP";
        $name = $this->getUser()->getName();
        $to = $this->getContainer()->getParameter('developer_email');

        $email = $this->getUser()->getEmail();
        $phone = $this->getUser()->getPhones();

        $message_body = $this->tplMaker($name, nl2br($ticket_body), $email, $phone);
        $this->sender($to, $email, $subject, $message_body, $request->files);


        return new Response();
    }

    public function jiraIssueClosedAction(Request $request)
    {
        $res = json_decode($request->getContent());
        $changelog = end($res->changelog->items);

        if (($changelog->fromString == 'To Do') && ($changelog->toString == 'Done')) {

            $current_issue_id = $res->issue->key;
            $db_issue = ($this->getEm()->getRepository(BugReport::class)->findBy(['id' => $current_issue_id]))[0];
            $db_issue_done_status = $db_issue->getDone();

            if (($db_issue) && (!$db_issue_done_status)) {

                // write closure on DB
                $db_issue->setDone(new \DateTime());
                $db_issue->setExecutive($res->user->displayName);
                $this->getEm()->persist($db_issue);
                $this->getEm()->flush();

                // send email
                $subject = 'Ответ на обращение GERP [ ' . $res->issue->key . ' ]';
                $name = 'Разработчик GERP';
                $text = 'Задача по вашему обращению [ ' . $res->issue->key . ' ] закрыта.';
                $email = $this->getContainer()->getParameter('developer_email');
                $phone = $this->getContainer()->getParameter('company_phone');
                $to = $res->user->emailAddress;

                $message_body = $this->tplMaker($name, $text, $email, $phone);
                $this->sender($to, $email, $subject, $message_body);
            }
        }

        return new Response();
    }

    public function sendEmailAction(Request $request)
    {
        // send to developer
        $subject = "Сообщение через форму GERP";
        $name = $this->getUser()->getName();
        $to = $this->getContainer()->getParameter('developer_email');
        $text = $request->get('br_freeform');

        $email = $this->getUser()->getEmail();
        $phone = $this->getUser()->getPhones();

        $message_body = $this->tplMaker($name, nl2br($text), $email, $phone);
        $this->sender($to, $email, $subject, $message_body, $request->files);


        // send to reporter
        $name = 'Разработчик GERP';
        $text = '
            <p>Ваше сообщение получено!</p>
            <p>Обратите внимание, что у обращений в свободной форме более низкий приоритет обработки. 
            Для отправки важной информации используйте специальную форму "Сообщить об ошибке".</p>';

        $phone = $this->getContainer()->getParameter('company_phone');

        $message_body = $this->tplMaker($name, $text, $to, $phone);
        $this->sender($email, $to, $subject, $message_body);

        return new Response();
    }

    private function tplMaker($name, $text, $email, $phone)
    {
        return $this->render('BugReport/email_body.html.php', [
            'name' => $name,
            'text' => $text,
            'email' => $email,
            'phone' => $phone
        ]);
    }

    /**
     * @param $to
     * @param $email
     * @param $subject
     * @param $message_body
     * @param FileBag $files
     * @return mixed
     */
    private function sender($to, $email, $subject, $message_body, FileBag $files = null)
    {
        $message = \Swift_Message::newInstance();

        if (null !== $files) {
            foreach ($files->get('files') as $file) {
                if ($file instanceof UploadedFile) {
                    $message->attach(
                        \Swift_Attachment::fromPath($file->getRealPath())->setFilename($file->getClientOriginalName())
                    );
                }
            }
        }

        $message->setSubject($subject)
            ->setFrom($email ? $email : $this->getContainer()->getParameter('developer_email'))
            ->setTo($to)
            ->setBody($message_body, 'text/html');

        $header = $message->getHeaders();
        $header->addPathHeader('Return-Path', $this->getContainer()->getParameter('mailer_username'));

        return $this->get('swiftmailer.mailer.memory_spooler')->send($message);
    }
}
