<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 05.10.16
 * Time: 18:31
 */

namespace WF\Task\Doctrine;


class AbstractPrice
{
    const PRICE_TYPE_SMETA = 'smeta';
    const PRICE_TYPE_PRICE = 'price';
}
