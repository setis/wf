<?php

namespace WF\Task;

use classes\reports42\MoneyReportQueries;
use DateTime;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Gorserv\Gerp\ScheduleBundle\Model\ExecutorInterface;
use Gorserv\Gerp\ScheduleBundle\Model\ScheduleManagerInterface;
use Gorserv\Gerp\ScheduleBundle\Model\ScheduleOperatorInterface;
use Gorserv\Gerp\ScheduleBundle\ScheduleManager;
use LogicException;
use models\Gfx;
use models\ListAddr;
use models\ListSc;
use models\Task;
use models\TaskComment;
use models\TaskStatus;
use models\TaskUserLink;
use models\User;
use repository\UserRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WF\Task\Events\TaskAddedToScheduleEvent;
use WF\Task\Events\TaskAddressAddedEvent;
use WF\Task\Events\TaskCommentedEvent;
use WF\Task\Events\TaskCreatedEvent;
use WF\Task\Events\TaskRemoveFromScheduleEvent;
use WF\Task\Events\TaskStatusChangedEvent;
use WF\Task\Model\TechnicalTicketInterface;

/**
 * Description of TaskManager
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskManager
{

    /**
     *
     * @var EntityManagerInterface
     */
    private $em;
    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     *
     * @var ScheduleManager
     */
    private $scheduleManager;

    /**
     * TaskManager constructor.
     *
     * @param $em
     * @param $scheduleManager
     * @param $dispatcher
     */
    public function __construct(EntityManagerInterface $em, ScheduleManagerInterface $scheduleManager, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->scheduleManager = $scheduleManager;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Task $task
     * @param TaskComment $comment
     */
    public function changeStatus(Task $task, TaskComment $comment)
    {
        $oldStatus = $task->getStatus();

        $task->setStatus($comment->getStatus())
            ->setLastStatusDate(new DateTime());

        $this->em->persist($task);
        $this->em->flush();

        $event = new TaskStatusChangedEvent($task, $oldStatus, $comment);
        $this->dispatcher->dispatch(TaskStatusChangedEvent::NAME, $event);
    }

    /**
     * @param Task $task
     * @param TaskComment $comment
     * @param boolean $dispatchEvent Defines whether dispatch TaskCommentedEvent or not
     */
    public function addComment(Task $task, TaskComment $comment, $dispatchEvent = true)
    {
        $comment->setTask($task);
        $task->addComment($comment);

        $event = null;
        $status = $comment->getStatus();
        if (null !== $status
            && false === strpos($status->getTag(), TaskStatus::TAG_PARTNER_COMMENT_STATUS) /* костыль что бы не ставился статус при выборе статуса "отправка комментария контрагенту" */
        ) {
            /** @var UserRepository $userRepo */
            $userRepo = $this->em->getRepository(User::class);
            $executors = $userRepo->getTaskExecutors($task);

            $oldStatus = $task->getStatus();
            $task->setStatus($status)
                ->setLastStatusDate($comment->getDatetime());
            $event = new TaskStatusChangedEvent($task, $oldStatus, $comment, $executors);
        }

        $this->em->persist($task);
        $this->em->flush();

        if ($event) {
            $this->dispatcher->dispatch(TaskStatusChangedEvent::NAME, $event);

            $this->em->persist($event->getComment());
            $this->em->flush();
        }

        if ($dispatchEvent) {
            $this->dispatcher->dispatch(TaskCommentedEvent::NAME, new TaskCommentedEvent($task, $comment));
        }

    }

    public function addAddress(Task $task, ListAddr $address)
    {
        $success = $this->buildTicketHandler($task)
            ->addAddress($task, $address);

        if ($success) {
            $event = new TaskAddressAddedEvent($task, $address);
            $this->dispatcher->dispatch(TaskAddressAddedEvent::NAME, $event);
        }

        return $success;
    }

    /**
     *
     * @param Task $task
     *
     * @return Model\TicketHandlerInterface
     * @throws Exception
     */
    private function buildTicketHandler(Task $task)
    {
        switch ($task->getPluginUid()) {
            case Task::GLOBAL_PROBLEM:
                return new TicketHandler\GpHandler($this->em, $this->dispatcher);

            case Task::BILL:
                return new TicketHandler\BillHandler($this->em, $this->dispatcher);

            case Task::CONNECTION:
            case Task::ACCIDENT:
            case Task::SERVICE:
            case Task::TELEMARKETING:
                return new TicketHandler\TicketHandler($this->em, $this->dispatcher);
        }

        throw new Exception('Unsupported ticket handler for task type: ' . $task->getPluginUid());
    }

    /**
     * @param Task $task
     * @param bool|string $moveReason
     *
     * @return string
     * @throws Exception
     * @throws ConnectionException
     */
    public function removeFromSchedule(Task $task, $moveReason = false, ScheduleOperatorInterface $operator = null)
    {
        $schedule = $task->getSchedule();

        $this->em->transactional(function ($em) use ($schedule, $operator, $moveReason) {
            $params = [];
            if ($moveReason) {
                $params = ['reason' => $moveReason];
            }
            $this->scheduleManager->deleteSlot($schedule, $operator, $params);
        });

        $taskScheduleRemovedEvent = new TaskRemoveFromScheduleEvent($task, $schedule);
        $this->dispatcher->dispatch(TaskRemoveFromScheduleEvent::NAME, $taskScheduleRemovedEvent);

        $comment = $taskScheduleRemovedEvent->getComment();

        if ($moveReason) {
            $comment = "Перенос заявки. Причина: " . $moveReason . ". " . $comment;
        } else {
            $comment = "Удалена из графика. " . $comment;
        }

        return trim($comment);
    }

    /**
     * @param Task $task
     * @param Gfx $schedule
     * @param ExecutorInterface $tech
     * @param ScheduleOperatorInterface $operator
     * @return string
     * @throws Exception
     */
    public function insertIntoSchedule(Task $task, Gfx $schedule, ExecutorInterface $tech, ScheduleOperatorInterface $operator = null)
    {
        if (!empty($task->getSchedule())) {
            throw new LogicException('Task already in schedule!');
        }

        $this->em->beginTransaction();
        try {
            $schedule->updateFields();
            $this->scheduleManager->createSlot($task, $schedule->getTimeStart(), $schedule->getTimeEnd(), [$tech], $operator);

            $comment = '';
            if (!$tech->getMasterServiceCenters()->contains($task->getTicket()->getServiceCenter())) {

                $task->getTicket()->setServiceCenter($tech->getMasterServiceCenters()->first());
                $this->em->persist($task->getTicket());
                $this->em->flush();
                $this->em->refresh($task);

                $comment .= ' Обновлен СЦ при установке в график: ' . $tech->getMasterServiceCenters()->first()->getTitle();
            }
            $this->em->commit();
        } catch (Exception $e) {
            $this->em->rollback();
            throw $e;
        }

        $taskAddedToScheduleEvent = new TaskAddedToScheduleEvent($task);
        $this->dispatcher->dispatch(TaskAddedToScheduleEvent::NAME, $taskAddedToScheduleEvent);

        $comment .= $taskAddedToScheduleEvent->getComment();

        return trim($comment);
    }

    /**
     * @param Task $task
     *
     * @return \models\User[]
     */
    public function getTaskExecutors(Task $task)
    {
        /** @var UserRepository $repo */
        $repo = $this->em->getRepository(User::class);

        return $repo->getTaskExecutors($task);
    }

    /**
     * @param Task $task
     * @param DateTime|null $period
     *
     * @return bool
     */
    public function isTaskClosedInPeriod(Task $task, DateTime $period = null)
    {
        if (empty($period))
            $period = new DateTime();

        $dc = getcfg(MoneyReportQueries::getDcParam($period->format('m')));

        $from = new DateTime(date('Y-m-1 00:00:00', $period->getTimestamp()));
        $until = (new DateTime(date('Y-m-t 23:59:59', $period->getTimestamp())))
            ->modify("+ {$dc} days");

        $status = $this->em->getRepository(TaskStatus::class)->findByTag(TaskStatus::TAG_TASK_DONE, $task->getPluginUid());
        if (empty($status))
            throw new LogicException('Unknown task status "done" for plugin uid: ' . $task->getPluginUid());

        $co = $this->em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from(TaskComment::class, 'c')
            ->where('c.task = :task AND c.status in (:status) AND c.datetime BETWEEN :from AND :until')
            ->setParameters([
                'task' => $task,
                'from' => $from,
                'until' => $until,
                'status' => $status,

            ])->getQuery()->getSingleScalarResult();

        return $co > 0;
    }

    /**
     * @param \models\Task $task
     *
     * @return bool
     */
    public function taskIsClosed(Task $task)
    {
        return self::isTaskClosed($task);
    }

    /**
     * @param \models\Task $task
     *
     * @return bool
     */
    public static function isTaskClosed(Task $task)
    {
        $statuses = [];
        switch ($task->getPluginUid()) {
            case Task::CONNECTION:
                $statuses = [TaskStatus::STATUS_CONNECTION_DONE, TaskStatus::STATUS_CONNECTION_REPORT, TaskStatus::STATUS_CONNECTION_CLOSED];
                break;
            case Task::ACCIDENT:
                $statuses = [TaskStatus::STATUS_ACCIDENT_DONE, TaskStatus::STATUS_ACCIDENT_REPORT, TaskStatus::STATUS_ACCIDENT_CLOSED];
                break;
            case Task::SERVICE:
                $statuses = [TaskStatus::STATUS_SERVICE_CLOSED];
                break;
            case Task::GLOBAL_PROBLEM:
                $statuses = [TaskStatus::STATUS_GLOBAL_PROBLEM_DONE, TaskStatus::STATUS_GLOBAL_PROBLEM_CLOSED];
                break;
        }
        return in_array($task->getStatus()->getId(), $statuses);
    }

    /**
     * Creates new task
     *
     * Available options and default values
     * - title              => null,
     * - author_id          => null,
     * - comment            => null, task comment
     * - comments           => [], array of models\TaskComment
     * - addresses          => [], array of models\ListAddr
     * - is_private         => false,
     * - status_id          => null,
     * - executor_ids       => [], array or comma separated string
     * - bounded_user_ids   => [], array or comma separated string
     * @param string $pluginUid models\Task consts @see
     * @param array $options
     *
     * @return Task
     * @throws \InvalidArgumentException
     */
    public function createTask($pluginUid, array $options = [])
    {
        $supportedPlugins = [
            Task::ACCIDENT,
            Task::BILL,
            Task::CONNECTION,
            Task::GLOBAL_PROBLEM,
            Task::PROJECT,
            Task::SERVICE,
            Task::TELEMARKETING
        ];

        if (!in_array($pluginUid, $supportedPlugins)) {
            throw new \InvalidArgumentException( sprintf( 'Unsupported task type "%s". Allowed are: %s.', $pluginUid, implode( ', ', $supportedPlugins ) ) );
        }

        $resolver = new OptionsResolver();
        $this->configureTaskOptions($resolver);
        $o = $resolver->resolve($options);

        $dt = new DateTime();
        $task = (new Task())
            ->setPluginUid($pluginUid)
            ->setAuthor(empty($o['author_id']) ? null : $this->em->getReference(User::class, $o['author_id']))
            ->setStatus($o['status'])
            ->setTaskTitle($o['title'])
            ->setTaskComment($o['comment'])
            ->setIsPrivate($o['is_private'])
            ->setLastStatusDate($dt)
            ->setLastcmmDate($dt);

        $this->em->transactional(function ($em) use ($task, $o) {
            $this->em->persist($task);
            $this->em->flush();
            foreach ($o['addresses'] as $address) {
                if (!$task->getAddresses()->contains($address)) {
                    $task->addAddress($address);
                }
            }

            foreach ($o['comments'] as $comment) {
                $task->addComment($comment);
                $comment->setTask($task);
            }
            $this->em->flush();
            $task->defineLastComment();

            $this->em->flush();
            $this->setTaskBoundedUsers($task, $o['bounded_user_ids']);
            $this->setTaskExecutors($task, $o['executor_ids']);
        });

        return $task;
    }

    protected function configureTaskOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'title' => null,
                'author_id' => null,
                'comment' => null,
                'is_private' => false,
                'status_id' => null,
                'status' => null,
                'executor_ids' => [],
                'bounded_user_ids' => [],
                'addresses' => [],
                'comments' => [],
            ])
            ->setAllowedTypes('executor_ids', ['array', 'string'])
            ->setAllowedTypes('addresses', ['array'])
            ->setAllowedTypes('comments', ['array'])
            ->setAllowedTypes('bounded_user_ids', ['array', 'string'])
            ->setAllowedTypes('status_id', ['null', 'int'])
            ->setAllowedTypes('status', ['null', TaskStatus::class])
            ->setNormalizer('executor_ids', function (Options $options, $value) {
                if (!is_array($value)) {
                    $value = explode(',', $value);
                }
                return $value;
            })
            ->setNormalizer('bounded_user_ids', function (Options $options, $value) {
                if (!is_array($value)) {
                    $value = explode(',', $value);
                }

                $author_id = $options['author_id'];
                if ($author_id && !in_array($author_id, $value)) {
                    $value[] = $author_id;
                }
                return $value;
            })
            ->setDefault('status', function (Options $options) {
                if ($options['status_id'] === null) {
                    return null;
                }

                $status = $this->em
                    ->find(TaskStatus::class, $options['status_id']);
                if ($status === null) {
                    throw new \RuntimeException('Cant find task status with id=' . $options['status_id']);
                }

                return $status;
            });
    }

    /**
     * Replace task bounded users
     *
     * @param Task $task
     * @param User[]|User $bounded
     *
     * @throws Exception
     */
    public function setTaskBoundedUsers(Task $task, $bounded)
    {
        $users = array_map(function ($el) {
            if ($el instanceof User) {
                return $el;
            }

            return $this->em->getReference(User::class, $el);
        }, (array)$bounded);

        $this->em->transactional(function (EntityManagerInterface $em) use ($task, $users) {
            $this->em->createQueryBuilder()
                ->delete(TaskUserLink::class, 't')
                ->where('t.task = :task and t.ispoln = :executor')
                ->setParameter('task', $task)
                ->setParameter('executor', false)
                ->getQuery()
                ->execute();

            foreach ($users as $user) {
                $taskUserLink = new TaskUserLink();
                $taskUserLink->setIspoln(false)
                    ->setUser($user)
                    ->setTask($task);

                $this->em->persist($taskUserLink);
            }
        });
    }

    /**
     * Replace task executors
     *
     * @param Task $task
     * @param User[]|User $executors
     *
     * @throws Exception
     */
    public function setTaskExecutors(Task $task, $executors)
    {
        $users = array_map(function ($el) {
            if ($el instanceof User) {
                return $el;
            }

            return $this->em->getReference(User::class, $el);
        }, (array)$executors);

        $this->em->transactional(function (EntityManagerInterface $em) use ($task, $users) {
            $this->em->createQueryBuilder()
                ->delete(TaskUserLink::class, 't')
                ->where('t.task = :task and t.ispoln = :executor')
                ->setParameter('task', $task)
                ->setParameter('executor', true)
                ->getQuery()
                ->execute();

            foreach ($users as $user) {
                $taskUserLink = new TaskUserLink();
                $taskUserLink->setIspoln(true)
                    ->setUser($user)
                    ->setTask($task);

                $this->em->persist($taskUserLink);
            }
        });
    }

    /**
     *
     * @param Task $task
     * @param array $options
     *
     * @return Task Returns modified task
     */
    public function createTicket(Task $task, array $options = [])
    {
        $handler = $this->buildTicketHandler($task);

        $ticket = $handler->createTicket($task, $options);
        $this->dispatcher->dispatch(TaskCreatedEvent::NAME, new TaskCreatedEvent($task));

        return $task;
    }

    public function createHandler(Task $task)
    {
        return $this->buildTicketHandler($task);
    }

    /**
     * @param Task $task
     * @param ListAddr|null $address
     */
    public function setTaskServiceCenterByAddress(Task $task, ListAddr $address = null)
    {
        if (null === $address) {
            return;
        }

        $sc = $this->em->getRepository(ListSc::class)
            ->getServiceCentersByPoint($address->getCoordinates(), [
                'task_type' => $task->getTicket()->getTaskType()
            ]);

        if (empty($sc)) {
            return;
        }

        $ticket = $task->getAnyTicket();
        switch (true) {
            case $ticket instanceof TechnicalTicketInterface:
                $ticket->setServiceCenter(reset($sc));
                break;
        }
    }
}
