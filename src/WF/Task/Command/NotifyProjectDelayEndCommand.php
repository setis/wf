<?php

namespace WF\Task\Command;

use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use Psr\Log\LoggerInterface;
use repository\TaskRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Task\Events\ProjectDelayEndEvent;

/**
 * Cron job 0 9 * * *
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyProjectDelayEndCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    
    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    
    /**
     *
     * @var LoggerInterface
     */
    private $logger;
    
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        parent::__construct();
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }
    
    protected function configure()
    {
        $this->setName('wf:task:notify-project-delay-end')
            ->setDescription('Dispatch event if Project delay end (Cron job 0 9 * * *)');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->debug(self::class . ' started');
        
        /* @var $repo TaskRepository */
        $repo = $this->em->getRepository(Task::class);
            
        $tasks = $repo->findProjectsWithDelayEnd();

        $this->logger->debug(count($tasks) . ' tasks found');
        
        foreach ($tasks as $task) {
            $event = new ProjectDelayEndEvent($task);
            $this->dispatcher->dispatch(ProjectDelayEndEvent::NAME, $event);
        }
        
        $this->logger->debug(self::class . ' finished');
    }
}
