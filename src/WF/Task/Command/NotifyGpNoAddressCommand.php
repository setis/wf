<?php

namespace WF\Task\Command;

use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use Psr\Log\LoggerInterface;
use repository\TaskRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Task\Events\GpNoAddressEvent;

/**
 * Description of NotifyGpNoAddressCommand
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyGpNoAddressCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        parent::__construct();
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setName('wf:task:notify-gp-no-address')
            ->setDescription('Dispatch event if GP with status "new" has no addresse after X minutes after creation (run every 5 minutes)')
            ->addOption('minutes', 'm', InputOption::VALUE_REQUIRED, 'Minutes from ceration to search', 10);
    }

    /**
     * TODO Pablo use logger here
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $taskRepo TaskRepository */
        $taskRepo = $this->em->getRepository(Task::class);
        $minutes = $input->getOption('minutes');

        $this->logger->debug("Search tasks by $minutes minutes after creation");

        $timeout = DateInterval::createFromDateString($minutes . ' minutes');

        $tasks = $taskRepo->findNewGpWithoutAddress($timeout);

        $this->logger->debug(count($tasks) . " tasks found", array_map(function($task) { return $task->getId(); }, $tasks));

        foreach ($tasks as $task) {
            $event = new GpNoAddressEvent($task);
            $this->dispatcher->dispatch(GpNoAddressEvent::NAME, $event);
        }

        $this->logger->debug('Complete');
    }
}
