<?php

namespace WF\Task\Command;

use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use models\TaskStatus;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Task\Events\GpCloseTimeExpiredEvent;

/**
 * Cron job *\/5 * * * *
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyGpCloseTimeExpiredCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        parent::__construct();
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setName('wf:task:notify-gp-close-time-expired')
            ->addOption('days', null, InputOption::VALUE_OPTIONAL, 'Period in days for serchink tasks', 7)
            ->setDescription('Dispatch event if GP close time expired over 60 minutes (Cron job */5 * * * *)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $end = new DateTime();
        $timeout = DateInterval::createFromDateString('60 minutes');
        $end->sub($timeout);

        $days = $input->getOption('days');
        $begin = new DateTime();
        $begin->modify("-$days days")->setTime(0, 0 , 0);

        $this->logger->debug('Started', [
            'begin' => $begin->format('Y-m-d H:i:s'),
            'end' => $end->format('Y-m-d H:i:s')
        ]);

        $q = $this->em->createQueryBuilder()
            ->select('task, gp')
            ->from(Task::class, 'task')
            ->join('task.gp', 'gp')
            ->where('task.status = :status')
            ->andwhere('gp.closeTime BETWEEN :begin AND :end')
            ->setParameter('status', $this->em->getReference(TaskStatus::class, TaskStatus::STATUS_GLOBAL_PROBLEM_IN_WORK))
            ->setParameter('begin', $begin)
            ->setParameter('end', $end)
            ->getQuery();

        $tasks = $q->getResult();
        $this->logger->debug(sprintf('%d tasks found', count($tasks)));

        foreach ($tasks as $task) {
            $this->dispatcher->dispatch(GpCloseTimeExpiredEvent::NAME, new GpCloseTimeExpiredEvent($task, $timeout));
        }

        $this->logger->debug('complete');
    }
}
