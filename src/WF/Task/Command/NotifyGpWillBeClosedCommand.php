<?php

namespace WF\Task\Command;;

use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use models\TaskStatus;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Task\Events\GpWillBeClosedEvent;
use WF\Task\TicketHandler\GpHandler;

/**
 * Cron job *\/5 * * * *
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyGpWillBeClosedCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    
    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    
    /**
     *
     * @var LoggerInterface
     */
    private $logger;
    
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        parent::__construct();
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }
    
    protected function configure()
    {
        $this->setName('wf:task:notify-gp-will-be-closed')
            ->setDescription('Dispatch event if GP has close time after 30 minutes (Cron job */5 * * * *)');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $begin = new DateTime();
        
        $this->logger->debug('Started', ['date' => $begin->format('Y-m-d H:i:s')]);
        
        $timeout = DateInterval::createFromDateString('30 minutes');
       
        $end = clone $begin;
        $end->add($timeout);
        
        $q = $this->em->createQueryBuilder()
            ->select('task, gp')
            ->from(Task::class, 'task')
            ->join('task.gp', 'gp')
            ->where('task.status = :status')
            ->andwhere('gp.closeTime BETWEEN :begin AND :end')
            ->setParameter('status', $this->em->getReference(TaskStatus::class, TaskStatus::STATUS_GLOBAL_PROBLEM_IN_WORK))
            ->setParameter('begin', $begin)
            ->setParameter('end', $end)
            ->getQuery();
        
        $tasks = $q->getResult();
        $this->logger->debug(sprintf('%d tasks found', count($tasks)));
        
        foreach ($tasks as $task) {
            $this->dispatcher->dispatch(GpWillBeClosedEvent::NAME, new GpWillBeClosedEvent($task, $timeout));
        }
        
        $this->logger->debug('complete');
    }
}
