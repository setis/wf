<?php
namespace WF\Task\Command;

use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use models\ListAddr;
use models\ListContr;
use models\ListSc;
use models\Task;
use models\TaskStatus;
use models\TaskType;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use WF\Task\TaskManager;

/**
 * Class SimpleTaskImportCommand
 *
 * @package WF\Task\Command
 * @author artem <mail@artemd.ru>
 */
class SimpleTaskImportCommand extends ContainerAwareCommand
{
    /**
     * @var TaskManager
     */
    private $taskManager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function configure()
    {
        $this->setName('wf:task:simple-import')
            //"0 ФИО","1 Телефон","2 Район","3 Город","4 Улица","5 Дом","6 Кв.(Оф.)"
            ->setDescription('Imports tickets just by address, phone and client name. Columns: 0-name, 1-phone, 2-district, 3-city, 4-street, 5-house, 6-apartment. ')
            ->addArgument('file', InputArgument::REQUIRED, 'CSV filename')
            ->addArgument('plugin', InputArgument::OPTIONAL, 'Plugin uid (accidents, gp, connections, tm_module, services)', Task::TELEMARKETING)
            ->addOption('skip', 's', InputOption::VALUE_OPTIONAL, 'How many lines must be skipped', 1)
            ->addArgument('partner_id', InputArgument::OPTIONAL, 'Partner ID (Магазин интернета - 704)', 704)
            ->addArgument('work_type_id', InputArgument::OPTIONAL, 'Work type ID (TM - 44)', 44);
    }

    public function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->logger = $this->getContainer()->get('monolog.logger.console');
        $this->taskManager = $this->getContainer()->get('wf.task.task_manager');
        $this->entityManager = $this->getContainer()->get('doctrine.orm.default_entity_manager');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('file');
        $this->logger->debug($filename);

        if (!file_exists($filename)) {
            throw new InvalidArgumentException('Not existing file.');
        }

        $file = new \SplFileObject($filename);

        $skip = $input->getOption('skip');
        $i = 0;
        while (!$file->eof()) {
            if ($i < $skip) {
                $file->fgetcsv(';');
                ++$i;
                continue;
            }
            if (false === ($csv = $file->fgetcsv(';'))) {
                continue;
            }
            $skip++;

            $this->logger->debug('string', $csv);

            $this->entityManager->beginTransaction();
            try {
                $listAddr = $this->findAddress($csv[3], $csv[4], $csv[5]);

                $sc = null;
                if (null !== $listAddr->getCoordinates()) {
                    /** @var ListSc[] $sc */
                    $sc = $this->entityManager->getRepository(ListSc::class)->getServiceCentersByPoint($listAddr->getCoordinates(), [
                        'task_type' => $this->entityManager->getReference(TaskType::class, $input->getArgument('work_type_id'))
                    ]);
                }

                $task = $this->taskManager->createTask($input->getArgument('plugin'), [
                    'status' => $this->entityManager->getRepository(TaskStatus::class)
                        ->getDefault($input->getArgument('plugin')),
                    'addresses' => [$listAddr],
                ]);

                $this->taskManager->createTicket($task, [
                    'partner' => $this->entityManager->getReference(ListContr::class, $input->getArgument('partner_id')),
                    'address' => $listAddr,
                    'client_full_name' => $csv[0],
                    'client_phone1' => $csv[1],
                    'apartment' => $csv[6],
                    'service_center' => !empty($sc) ? reset($sc) : null,
                    'task_type' => $this->entityManager->getReference(TaskType::class, $input->getArgument('work_type_id')),
                    'additional_info' => implode("\n", $csv)
                ]);
                $this->entityManager->commit();
                $this->logger->info('Saved string #' . $skip . ' with id #' . $task->getId());
            } catch (\Exception $e) {
                $this->logger->warning('Skipped string #' . $skip);
                $this->entityManager->rollback();
            }
        }
    }

    /**
     * @param $addr_city
     * @param $addr_street
     * @param $addr_building
     * @return ListAddr|null
     */
    private function findAddress($addr_city, $addr_street, $addr_building)
    {
        return $this->entityManager->getRepository(ListAddr::class)
            ->setAddressProvider($this->getContainer()->get('wf.address.provider'))
            ->findOrCreateByAddressArray([
                'city' => $addr_city,
                'street' => $addr_street,
                'house' => $addr_building,
            ], [
                'count' => 1
            ]);
    }

}
