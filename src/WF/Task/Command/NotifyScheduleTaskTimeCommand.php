<?php

namespace WF\Task\Command;

use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Gfx;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Task\Events\TaskScheduleTimeSoonExpireEvent;;
use Psr\Log\LoggerInterface;

/**
 * Command for cron run: every 30 minutes
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyScheduleTaskTimeCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        parent::__construct();
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setName('system:notify:schedule-task-time')
            ->setDescription('Dispatch event if task started in 30 minutes (Command for cron run: every 30 minutes)')
            ->addOption('date', null, InputOption::VALUE_OPTIONAL , 'Specify date');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dateStr = $input->getOption('date');
        /* @var $dt DateTime */
        $dt = $dateStr ? new DateTime($dateStr) : new DateTime();

        $this->logger->debug('Started', ['date' => $dt->format('Y-m-d H:i:s')]);

        $start = $dt->format('H') * 60 + floor($dt->format('i') / 30) * 30;
        $end = $start + 30;
        $start++;

        $this->logger->debug(sprintf('Start time between %s and %s', $start, $end));

        $queryDt = clone $dt;
        $queryDt->setTime(0, 0, 0);

        $q = $this->em->createQueryBuilder()
            ->select('g')
            ->from(Gfx::class, 'g')
            ->where('g.date = :date')
            ->andWhere('g.startTime BETWEEN :start AND :end')
            ->andWhere('NOT EXISTS ('
                . 'SELECT g1.id FROM '. Gfx::class . ' g1 '
                . 'WHERE g1.technician = g.technician '
                . 'AND g1.date = g.date '
                . 'AND g1.startTime < g.startTime)')
            ->setParameter('date', $queryDt)
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery();

        $schedules = $q->getResult();
        $this->logger->debug(sprintf('%d schedules found', count($schedules)));

        $interval = new DateInterval('PT30M');
        foreach ($schedules as $sched) {
            $this->dispatcher->dispatch(TaskScheduleTimeSoonExpireEvent::NAME, new TaskScheduleTimeSoonExpireEvent($sched, $interval));
        }

        $this->logger->debug('complete');
    }
}
