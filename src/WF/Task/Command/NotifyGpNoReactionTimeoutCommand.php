<?php

namespace WF\Task\Command;

use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use models\TaskStatus;
use Psr\Log\LoggerInterface;
use repository\TaskRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Task\Events\TaskNoReactionTimeoutEvent;

/**
 * Cron command - run every 15 minutes
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyGpNoReactionTimeoutCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        parent::__construct();
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setName('wf:task:notify-task-no-reaction-timeout')
            ->setDescription('Dispatch event if GP has status "new" after 30 minutes after creation (run every 5 minutes)');
    }

    /**
     * TODO Pablo use logger here
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $taskRepo TaskRepository */
        $taskRepo = $this->em->getRepository(Task::class);

        $timeout = DateInterval::createFromDateString('30 minutes');
        $dateTo = (new DateTime())->sub($timeout);
        $tasks = $taskRepo->findByPluginStatusAndCreateDate(
            Task::GLOBAL_PROBLEM,
            $this->em->getReference(TaskStatus::class, TaskStatus::STATUS_GLOBAL_PROBLEM_NEW),
            null, $dateTo);

        foreach ($tasks as $task) {
            $event = new TaskNoReactionTimeoutEvent($task, $timeout);
            $this->dispatcher->dispatch(TaskNoReactionTimeoutEvent::NAME, $event);
        }

        // TODO make it for other plugins
    }
}
