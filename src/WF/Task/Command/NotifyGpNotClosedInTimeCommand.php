<?php

namespace WF\Task\Command;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use models\TaskStatus;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Task\Events\GpNotClosedInTimeEvent;
use WF\Task\TicketHandler\GpHandler;


/**
 * Cron job *\/5 * * * *
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyGpNotClosedInTimeCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    
    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    
    /**
     *
     * @var LoggerInterface
     */
    private $logger;
    
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        parent::__construct();
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }
    
    protected function configure()
    {
        $this->setName('wf:task:notify-gp-not-closed-in-time')
            ->setDescription('Dispatch event if GP did not closed in planned time (Cron job */5 * * * *)');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $end = new DateTime();
        
        // Looks in period 60 minutes ago to now, 
        // for GP with close time more then 60 minutes GpWillBeClosedEvent fired
        $begin = clone $end;
        $begin->modify('-60 minutes');
        
        $this->logger->debug('Started', [
            'begin' => $begin->format('Y-m-d H:i:s'),
            'end' => $end->format('Y-m-d H:i:s')
        ]);
        
        $q = $this->em->createQueryBuilder()
            ->select('task, gp')
            ->from(Task::class, 'task')
            ->join('task.gp', 'gp')
            ->where('task.status = :status')
            ->andwhere('gp.closeTime BETWEEN :begin AND :end')
            ->setParameter('status', $this->em->getReference(TaskStatus::class, TaskStatus::STATUS_GLOBAL_PROBLEM_IN_WORK))
            ->setParameter('begin', $begin)
            ->setParameter('end', $end)
            ->getQuery();
        
        $tasks = $q->getResult();
        $this->logger->debug(sprintf('%d tasks found', count($tasks)));
        
        foreach ($tasks as $task) {
            $this->dispatcher->dispatch(GpNotClosedInTimeEvent::NAME, new GpNotClosedInTimeEvent($task));
        }
        
        $this->logger->debug('complete');
    }
}
