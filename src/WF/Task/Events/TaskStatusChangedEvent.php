<?php

namespace WF\Task\Events;

use models\Task;
use models\TaskComment;
use models\TaskStatus;
use models\User;

/**
 * Description of TaskStatusChangedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskStatusChangedEvent extends TaskEvent
{
    const NAME = 'wf.task.status_changed';

    /**
     * @var TaskStatus
     */
    private $oldStatus;

    /**
     * @var User[]
     */
    private $executors;

    public function __construct(Task $task, TaskStatus $oldStatus, TaskComment $comment, $executors = [])
    {
        parent::__construct($task, $comment);
        $this->oldStatus = $oldStatus;
        $this->executors = $executors;
    }

    /**
     * @return TaskStatus
     */
    public function getNewStatus()
    {
        return $this->getTask()->getStatus();
    }

    /**
     *
     * @return TaskStatus
     */
    public function getOldStatus()
    {
        return $this->oldStatus;
    }

    /**
     * @return User[]
     */
    public function getExecutors(){
        return $this->executors;
    }
}
