<?php

namespace WF\Task\Events;

/**
 * Description of TaskMovedToRms
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskMovedToRmsEvent extends TaskEvent
{
    const NAME = 'wf.tsk.moved_to_rms';
}
