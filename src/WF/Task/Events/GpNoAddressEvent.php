<?php

namespace WF\Task\Events;

/**
 * Description of GpNoAddressEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class GpNoAddressEvent extends TaskEvent
{
    const NAME = 'wf.task.gp_no_address';
}
