<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 24.06.16
 * Time: 18:55
 */

namespace WF\Task\Events;


use models\Gfx as Schedule;
use models\Task;

class TaskRemoveFromScheduleEvent extends TaskEvent
{
    const NAME = 'wf.task.schedule.removed';

    /**
     * @var Schedule
     */
    private $schedule;

    /**
     * @var string
     */
    private $comment;

    public function __construct(Task $task, Schedule $schedule)
    {
        parent::__construct($task);
        $this->schedule = $schedule;
    }

    /**
     * @param Schedule $schedule
     * @return TaskRemoveFromScheduleEvent
     */
    public function setSchedule($schedule)
    {
        $this->schedule = $schedule;
        return $this;
    }

    /**
     * @return Schedule
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }


}