<?php

namespace WF\Task\Events;

use models\Task;
use models\Gp;

/**
 * Description of GpTaskCreatedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class GpCreatedEvent extends TaskEvent
{
    const NAME = 'wf.task.gp_created';

    /**
     *
     * @var Gp
     */
    private $gp;
    
    public function __construct(Task $task, Gp $gp)
    {
        $this->gp = $gp;
        parent::__construct($task);
    }
    
    /**
     * 
     * @return Gp
     */
    public function getGp()
    {
        return $this->gp;
    }


}
