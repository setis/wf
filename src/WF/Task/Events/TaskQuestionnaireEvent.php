<?php
namespace WF\Task\Events;

use models\Task;
use models\User;
use Symfony\Component\EventDispatcher\Event;

class TaskQuestionnaireEvent extends Event
{

    const NAME = 'task.questionnaire_filled';

    /**
     * @var array
     */
    private $questionnaire;

    /**
     * @var array
     */
    private $loyalty;

    /**
     * @var Task
     */
    private $task;

    /**
     * @var User
     */
    private $author;

    public function __construct($q, $l, Task $task, User $author)
    {
        $this->questionnaire = $q;
        $this->loyalty = $l;
        $this->task = $task;
        $this->author = $author;
    }

    /**
     * @return array
     */
    public function getQuestionnaire()
    {
        return $this->questionnaire;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return array
     */
    public function getLoyalty()
    {
        return $this->loyalty;
    }

}