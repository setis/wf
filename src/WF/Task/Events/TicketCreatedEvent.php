<?php

namespace WF\Task\Events;

use models\Task;
use models\Ticket;

/**
 * Description of TicketCreatedEvent
 *
 * @author Ed <farafonov@gorserv.ru>
 */
class TicketCreatedEvent extends TaskEvent
{
    const NAME = 'wf.task.ticket_created';

    /**
     *
     * @var Ticket
     */
    private $ticket;
    
    public function __construct(Task $task, Ticket $ticket)
    {
        $this->ticket = $ticket;
        parent::__construct($task);
    }
    
    /**
     * 
     * @return Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }


}
