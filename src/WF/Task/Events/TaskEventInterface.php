<?php
/**
 * Artem
 * 10.05.2016 1:48
 */
namespace WF\Task\Events;

use models\Task;
use models\TaskComment;
use models\TaskStatus;

interface TaskEventInterface
{
    /**
     * @return Task
     */
    public function getTask();

    /**
     * @return TaskComment
     */
    public function getComment();

    /**
     * @return mixed
     */
    public function getResult();

    /**
     * @param mixed $result
     */
    public function setResult($result);
}