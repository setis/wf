<?php

namespace WF\Task\Events;
use DateInterval;
use models\Task;

/**
 * Description of GpCloseTimeExpiredEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class GpCloseTimeExpiredEvent extends TaskEvent
{
    const NAME = 'wf.task.gp_close_time_expired';
    
    /**
     *
     * @var DateInterval
     */
    private $timeout;
    
    public function __construct(Task $task, DateInterval $timeout)
    {
        $this->timeout = $timeout;
        parent::__construct($task);
    }
    
    public function getTimeout()
    {
        return $this->timeout;
    }
}
