<?php

namespace WF\Task\Events;

use models\ListAddr;
use models\Task;

/**
 * Description of TaskAddressAdded
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskAddressAddedEvent extends TaskEvent
{

    const NAME = 'wf.task.address_added';

    /**
     *
     * @var ListAddr
     */
    private $address;

    public function __construct(Task $task, ListAddr $address)
    {
        parent::__construct($task);

        $this->address = $address;
    }

    /**
     *
     * @return ListAddr
     */
    public function getAddress()
    {
        return $this->address;
    }

    public function isFirstAddress()
    {
        $task = $this->getTask();

        return $task->getAddressInfo()->count() === 1;
    }

}
