<?php

namespace WF\Task\Events;

/**
 * Description of ProjectDelayEndEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ProjectDelayEndEvent extends TaskEvent
{
    const NAME = 'wf.task.project_delay_end';
}
