<?php

namespace WF\Task\Events;

use Symfony\Component\EventDispatcher\Event;
use models\Task;
use models\User;
use WF\Utils\Comparer\ObjectIdComparer;

/**
 * Description of TaskExecutorsChangedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskExecutorsChangedEvent extends Event
{
    const NAME = 'wf.task.executors_changed';
    
    /**
     *
     * @var Task
     */
    private $task;

    /**
     *
     * @var User[] 
     */
    private $oldExecutors;
    
    /**
     *
     * @var User[] 
     */
    private $newExecutors;

    /**
     * @var User
     */
    private $author = null;
    
    public function __construct(Task $task, array $oldExecutors, array $newExecutors, User $author = null)
    {
        $this->task = $task;
        $this->oldExecutors = $oldExecutors;
        $this->newExecutors = $newExecutors;
        $this->author = $author;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function getOldExecutors()
    {
        return $this->oldExecutors;
    }

    public function getNewExecutors()
    {
        return $this->newExecutors;
    }

    public function getAddedExecutors()
    {
        return array_udiff($this->newExecutors, $this->oldExecutors, new ObjectIdComparer());
    }
    
    public function getRemovedExecutors()
    {
        return array_udiff($this->oldExecutors, $this->newExecutors, new ObjectIdComparer());
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

}
