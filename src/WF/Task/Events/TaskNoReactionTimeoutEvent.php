<?php

namespace WF\Task\Events;

use DateInterval;
use models\Task;

/**
 * Description of TaskNoReactionTimeoutEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskNoReactionTimeoutEvent extends TaskEvent
{
    const NAME = 'wf.task.no_reaction_timeout';
    
    /**
     *
     * @var DateInterval
     */
    private $timeout;
    
    public function __construct(Task $task, DateInterval $timeout)
    {
        $this->timeout = $timeout;
        parent::__construct($task);
    }
    
    public function getTimeout()
    {
        return $this->timeout;
    }


}
