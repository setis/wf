<?php

namespace WF\Task\Events;

/**
 * Description of ProjectDelayEndEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class BillDelayEndEvent extends TaskEvent
{
    const NAME = 'wf.task.bill_delay_end';
}
