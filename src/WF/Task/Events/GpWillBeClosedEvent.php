<?php

namespace WF\Task\Events;

use DateInterval;
use models\Task;

/**
 * Description of NotifyGpWillBeClosedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class GpWillBeClosedEvent extends TaskEvent
{
    const NAME = 'wf.task.gp_will_be_closed';
    
    /**
     *
     * @var DateInterval
     */
    private $timeout;
    
    public function __construct(Task $task, DateInterval $timeout)
    {
        $this->timeout = $timeout;
        parent::__construct($task);
    }
    
    public function getTimeout()
    {
        return $this->timeout;
    }

}
