<?php
namespace WF\Task\Events;

use models\Task;
use models\User;
use Symfony\Component\EventDispatcher\Event;

class TaskLoyaltyEvent extends Event
{

    const NAME = 'task.loyalty_filled';

    /**
     * @var array
     */
    private $loyalty;

    /**
     * @var Task
     */
    private $task;

    /**
     * @var User
     */
    private $author;

    public function __construct($data, Task $task, User $author)
    {
        $this->loyalty = $data;
        $this->task = $task;
        $this->author = $author;
    }


    /**
     * @return array
     */
    public function getLoyalty()
    {
        return $this->loyalty;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    public function getAuthor()
    {
        return $this->author;
    }
}