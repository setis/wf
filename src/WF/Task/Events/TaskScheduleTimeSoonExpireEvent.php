<?php

namespace WF\Task\Events;

use DateInterval;
use models\Gfx;
use Symfony\Component\EventDispatcher\Event;

/**
 * Description of TaskScheduleTimeSoonExpireEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskScheduleTimeSoonExpireEvent extends Event
{
    const NAME = 'wf.task.schedule_time_soon_expire';
    
    /**
     *
     * @var Gfx 
     */
    private $schedule;

    /**
     *
     * @var DateInterval
     */
    private $interval;
    
    public function __construct(Gfx $schedule, DateInterval $interval)
    {
        $this->schedule = $schedule;
        $this->interval = $interval;
    }
    
    /**
     * 
     * @return Gfx
     */
    public function getSchedule()
    {
        return $this->schedule;
    }
    
    /**
     * 
     * @return DateInterval
     */
    public function getInterval()
    {
        return $this->interval;
    }

}
