<?php

namespace WF\Task\Events;

use models\Task;
use models\TaskComment;
use Gorserv\Gerp\AppBundle\EventDispatcher\Events\TaskEvent as BaseTaskEvent;

/**
 * Description of TaskEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskEvent extends BaseTaskEvent implements TaskEventInterface
{
    /**
     * @var TaskComment
     */
    private $comment;

    /**
     * @var mixed
     */
    private $result;

    /**
     * ServiceTicketEvent constructor.
     * @param Task $task
     * @param TaskComment $comment
     */
    public function __construct(Task $task, TaskComment $comment = null)
    {
        parent::__construct($task);
        $this->comment = $comment;
    }

    /**
     * @return TaskComment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

}
