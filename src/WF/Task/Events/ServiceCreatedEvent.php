<?php

namespace WF\Task\Events;

use models\Task;
use models\Ticket;

/**
 * Description of ServiceCreatedEvent
 *
 * @author Ed <farafonov@gorserv.ru>
 */
class ServiceCreatedEvent extends TicketCreatedEvent
{
    const NAME = 'wf.task.service_created';
    
    public function __construct(Task $task, Ticket $ticket)
    {
        $this->ticket = $ticket;
        parent::__construct($task, $ticket);
    }
}
