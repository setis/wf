<?php

namespace WF\Task\Events;

use models\Task;
use models\Ticket;

/**
 * Description of TelemarketingCreatedEvent
 *
 * @author Ed <farafonov@gorserv.ru>
 */
class TelemarketingCreatedEvent extends TicketCreatedEvent
{
    const NAME = 'wf.task.telemarketing_created';
    
    public function __construct(Task $task, Ticket $ticket)
    {
        $this->ticket = $ticket;
        parent::__construct($task, $ticket);
    }
}
