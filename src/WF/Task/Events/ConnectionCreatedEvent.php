<?php

namespace WF\Task\Events;

use models\Task;
use models\Ticket;

/**
 * Description of ConnectionCreatedEvent
 *
 * @author Ed <farafonov@gorserv.ru>
 */
class ConnectionCreatedEvent extends TicketCreatedEvent
{
    const NAME = 'wf.task.connection_created';
    
    public function __construct(Task $task, Ticket $ticket)
    {
        $this->ticket = $ticket;
        parent::__construct($task, $ticket);
    }
}
