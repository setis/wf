<?php

namespace WF\Task\Events;

/**
 * Description of TaskQcChangedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskQcChangedEvent extends TaskEvent
{
    const NAME = 'wf.task.qc_changed';
}
