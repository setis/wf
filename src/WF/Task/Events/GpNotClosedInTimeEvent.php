<?php

namespace WF\Task\Events;

/**
 * Description of GpNotClosedInTimeEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class GpNotClosedInTimeEvent extends TaskEvent
{
    const NAME = 'wf.task.gp_not_closed_in_time';
}
