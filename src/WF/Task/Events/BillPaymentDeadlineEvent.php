<?php

namespace WF\Task\Events;

/**
 * Description of BillPaymentDeadlineEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class BillPaymentDeadlineEvent extends TaskEvent
{
    const NAME = 'wf.task.bill_payment_deadline';
}
