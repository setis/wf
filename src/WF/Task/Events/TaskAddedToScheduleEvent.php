<?php

namespace WF\Task\Events;

use models\Task;


/**
 * Description of TaskAddedToScheduleEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskAddedToScheduleEvent extends TaskEvent
{
    const NAME = 'wf.task.added_to_schedule';

    /**
     * @var string
     */
    private $comment;

    public function __construct(Task $task)
    {
        parent::__construct($task);
    }

    public function getSchedule()
    {
        return $this->getTask()->getSchedule();
    }

    public function getTechnician()
    {
        return $this->getTask()->getSchedule()->getTechnician();
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

}
