<?php

namespace WF\Task\Events;

/**
 * Description of ProjectDeadlineExpired
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ProjectDeadlineExpiredEvent extends TaskEvent
{
    const NAME = 'wf.task.project_deadline_expired';
}
