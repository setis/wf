<?php
/**
 * mail@artemd.ru
 * 26.06.2016
 */

namespace WF\Task\Events;


class TaskCommentedEvent extends TaskEvent
{
    const NAME = "wf.task.commented";
}
