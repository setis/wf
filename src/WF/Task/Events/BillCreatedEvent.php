<?php

namespace WF\Task\Events;

/**
 * Description of BillCreatedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class BillCreatedEvent extends TaskEvent
{
    const NAME = 'wf.task.bill_created';
}
