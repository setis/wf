<?php

namespace WF\Task\Events;

use models\Task;

/**
 * Description of TaskCreatedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskCreatedEvent extends TaskEvent
{
    const NAME = 'wf.task.task_created';
    
    public function __construct(Task $task)
    {
        parent::__construct($task);
    }
}
