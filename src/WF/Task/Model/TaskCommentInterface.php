<?php
/**
 * mail@artemd.ru
 * 15.09.2016
 */

namespace WF\Task\Model;
use models\TaskStatus;

/**
 * Interface TaskCommentInterface
 * @package WF\Task\Model
 */
interface TaskCommentInterface
{
    /**
     * @return string
     */
    public function getText();

    /**
     * @return string
     */
    public function getTag();

    /**
     * @return TaskStatus
     */
    public function getStatus();
}
