<?php
/**
 * mail@artemd.ru
 * 15.09.2016
 */

namespace WF\Task\Model\Formatter;
use WF\Task\Model\Formatter\MessageDecorator;

/**
 * Class TextifyFormatter
 * @package WF\Task\EventDispatcher\Formatter
 */
class TextifyFormatter extends MessageDecorator
{
    /**
     * @return string
     */
    public function getTag()
    {
        return strip_tags($this->comment->getTag());
    }

    /**
     * @return string
     */
    public function getText()
    {
        return strip_tags($this->comment->getText());
    }
}

