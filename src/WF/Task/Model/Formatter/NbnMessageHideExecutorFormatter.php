<?php
/**
 * mail@artemd.ru
 * 15.09.2016
 */

namespace WF\Task\Model\Formatter;

/**
 * Class NbnMessageHideExecutorFormatter
 * @package WF\Task\EventDispatcher\Formatter
 */
class NbnMessageHideExecutorFormatter extends MessageDecorator
{
    /**
     * @return string
     */
    public function getTag()
    {
        $tag = $this->comment->getTag();

        return preg_replace('/Исполнитель.*$/', '', $tag);
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->comment->getText();
    }
}
