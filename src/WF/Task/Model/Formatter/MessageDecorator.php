<?php
/**
 * mail@artemd.ru
 * 15.09.2016
 */

namespace WF\Task\Model\Formatter;

use WF\Task\Model\TaskCommentInterface;

/**
 * Class MessageDecorator
 * @package WF\Task\EventDispatcher\Formatter
 */
abstract class MessageDecorator implements TaskCommentInterface
{
    protected $comment;

    /**
     * MessageDecorator constructor.
     * @param TaskCommentInterface $comment
     */
    public function __construct(TaskCommentInterface $comment) {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getText() {
        return $this->comment->getText();
    }

    /**
     * @return string
     */
    public function getTag() {
        return $this->comment->getTag();
    }

    /**
     * @return \models\TaskStatus
     */
    public function getStatus()
    {
        return $this->comment->getStatus();
    }
}
