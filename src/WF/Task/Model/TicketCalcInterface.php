<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 16.06.16
 * Time: 21:04
 */
namespace WF\Task\Model;
use models\Task;

interface TicketCalcInterface
{
    /**
     * @return Task
     */
    public function getTask();
}