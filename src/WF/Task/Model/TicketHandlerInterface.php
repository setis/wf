<?php

namespace WF\Task\Model;

use LogicException;
use models\ListAddr;
use models\Task;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TicketHandlerInterface
{
    public function createTicket(Task $task, array $options = []);

    /**
     * @param Task $task
     * @return mixed
     */
    public function canUpdate(Task $task);

    /**
     * @param Task $task
     * @param array $options
     * @return mixed
     */
    public function updateTicket(Task $task, array $options = []);

    /**
     *
     * @param Task $task
     * @param ListAddr $address
     * @return boolean True if address is added, false otherwise
     * @throws LogicException
     */
    public function addAddress(Task $task, ListAddr $address);
}
