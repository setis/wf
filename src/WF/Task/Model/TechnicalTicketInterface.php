<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.07.16
 * Time: 23:52
 */
namespace WF\Task\Model;

use models\ListSc;


interface TechnicalTicketInterface extends BaseTicketInterface
{

    public function setServiceCenter(ListSc $sc = null);

    /**
     * @return ListSc
     */
    public function getServiceCenter();

    /**
     * @return string
     */
    public function getClnttnum();

    /**
     * @return \models\ListAddr
     */
    public function getDom();
}
