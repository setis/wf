<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 14.10.16
 * Time: 16:21
 */

namespace WF\Task\Model;


use models\ListContr;
use models\TaskType;

interface BaseTicketInterface
{
    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId();

    /**
     * Get taskType
     *
     * @return TaskType
     */
    public function getTaskType();

    /**
     * Get task
     *
     * @return \models\Task
     */
    public function getTask();

    /**
     * @return ListContr
     */
    public function getPartner();
}
