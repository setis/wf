<?php

namespace WF\Task\TicketHandler;

use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use models\Agreement;
use models\ListAddr;
use models\ListContr;
use models\ListSc;
use models\Task;
use models\TaskComment;
use models\TaskStatus;
use models\TaskType;
use models\Ticket;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WF\Task\Events\AccidentCreatedEvent;
use WF\Task\Events\ConnectionCreatedEvent;
use WF\Task\Events\ServiceCreatedEvent;
use WF\Task\Events\TelemarketingCreatedEvent;
use WF\Task\Events\TicketUpdatedEvent;
use WF\Task\TaskManager;

/**
 * Description of TicketHandler
 * @author Ed <farafonov@gorserv.ru>
 */
class TicketHandler extends AbstractTicketHandler
{
    protected function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'task_type' => null,
            'client_type' => 1,
            'agreement_id' => null,
            'agreement' => null,
            'checked' => 0,
            'partner' => null,
            'address' => null,
            'entrance' => null,
            'floor'=> null,
            'apartment' => null,
            'intercom' => null,
            'service_center' => null,
            'external_id' => null,
            'client_operator_agreement_number' => null,
            'client_org_name' => null,
            'client_full_name' => null,
            'client_phone1' => null,
            'client_phone2' => null,
            'landline_phone' => null,
            'additional_info' => null,
            'switch_ip' => null,
            'switch_port' => null,
            'switch_location' => null,
            'slot_begin' => null,
            'slot_end' => null,
            'task_subtype' => null,
            'is_test' => false,

            'mgts_tech_center_id' => 0,
            'mgts_ats' => null,
            'mgts_orsh' => null,
            'mgts_channel_count' => null,
            'mgts_phone_count' => null,
            'mgts_service_type_id' => 0,
            'mgts_agreement_number' => null,
        ])
            ->setDefault('agreement', function (Options $o) {
                if (empty($o['agreement_id'])) {
                    return null;
                }
                return $this->em->getReference(Agreement::class, $o['agreement_id']);
            })
            ->setAllowedTypes('client_type', ['int'])
            ->setAllowedTypes('external_id', ['null', 'string'])
            ->setAllowedTypes('agreement_id', ['null', 'int'])
            ->setAllowedTypes('agreement', ['null', Agreement::class])
            ->setAllowedTypes('task_type', ['null', TaskType::class])
            ->setAllowedTypes('partner', [ListContr::class])
            ->setAllowedTypes('checked', ['null', 'int'])
            ->setAllowedTypes('address', [ListAddr::class])
            ->setAllowedTypes('entrance', ['null', 'string'])
            ->setAllowedTypes('floor', ['null', 'string'])
            ->setAllowedTypes('apartment', ['null', 'string'])
            ->setAllowedTypes('intercom', ['null', 'string'])
            ->setAllowedTypes('service_center', ['null', ListSc::class])
            ->setAllowedTypes('client_operator_agreement_number', ['null', 'string'])
            ->setAllowedTypes('client_org_name', ['null', 'string'])
            ->setAllowedTypes('client_full_name', ['null', 'string'])
            ->setAllowedTypes('client_phone1', ['null', 'string'])
            ->setAllowedTypes('client_phone2', ['null', 'string'])
            ->setAllowedTypes('landline_phone', ['null', 'string'])
            ->setAllowedTypes('additional_info', ['null', 'string'])
            ->setAllowedTypes('switch_ip', ['null', 'string'])
            ->setAllowedTypes('switch_port', ['null', 'string'])
            ->setAllowedTypes('switch_location', ['null', 'string'])
            ->setAllowedTypes('slot_begin', ['null', \DateTime::class])
            ->setAllowedTypes('slot_end', ['null', \DateTime::class])
            ->setAllowedTypes('task_subtype', ['null', 'int'])
            ->setAllowedTypes('is_test', ['boolean'])
            ->setAllowedTypes('mgts_tech_center_id', ['null', 'int', 'string'])
            ->setAllowedTypes('mgts_ats', ['null', 'int', 'string'])
            ->setAllowedTypes('mgts_orsh', ['null', 'int', 'string'])
            ->setAllowedTypes('mgts_channel_count', ['null', 'int', 'string'])
            ->setAllowedTypes('mgts_phone_count', ['null', 'int', 'string'])
            ->setAllowedTypes('mgts_service_type_id', ['null', 'int', 'string'])
        ;

        $phoneNormalizer = function (Options $options, $value) {
            if (null !== $value) {
                $value = preg_replace("/[^0-9]/", "", $value);
            }
            return $value;
        };

        $intNormalizer = function (Options $options, $value) {
            if (null !== $value) {
                $value = (int)$value;
            }
            return $value;
        };

        $resolver
            ->setNormalizer('client_phone1', $phoneNormalizer)
            ->setNormalizer('client_phone2', $phoneNormalizer)
            ->setNormalizer('mgts_tech_center_id', $intNormalizer)
            ->setNormalizer('mgts_ats', $intNormalizer)
            ->setNormalizer('mgts_orsh', $intNormalizer)
            ->setNormalizer('mgts_channel_count', $intNormalizer)
            ->setNormalizer('mgts_phone_count', $intNormalizer)
            ->setNormalizer('mgts_service_type_id', $intNormalizer)
        ;
    }

    /**
     *
     * @param Task $task
     * @param array $options
     * @return Task
     */
    protected function doCreateTicket(Task $task, array $options)
    {
        /* @var $partner ListContr */
        $partner = $options['partner'];

        $taskTitle = sprintf('Контрагент: %s',  $partner->getContrTitle());

        $task->setTaskTitle($taskTitle);
        $pluginUid = $task->getPluginUid();

        switch ($pluginUid) {
            case Task::CONNECTION:
                $task->setStatus($this->em->getReference(TaskStatus::class, TaskStatus::STATUS_CONNECTION_NEW));
                break;
            case Task::ACCIDENT:
                $task->setStatus($this->em->getReference(TaskStatus::class, TaskStatus::STATUS_ACCIDENT_NEW));
                break;
            case Task::SERVICE:
                $task->setStatus($this->em->getReference(TaskStatus::class, TaskStatus::STATUS_SERVICE_NEW));
                break;
            case Task::TELEMARKETING:
                $task->setStatus($this->em->getReference(TaskStatus::class, TaskStatus::STATUS_TELEMARKETING_NEW));
                break;
            default:
                throw new LogicException('Unsupported plugin uid ' . $pluginUid);
        }

        if (isset($options['comment'])) {
            $comment = (new TaskComment())
                ->setText($options['comment'])
                ->setTask($task);
            $task->addComment($comment);
        }

        $ticket = (new Ticket())
            ->setTask($task)
            ->setServiceCenter($options['service_center'])
            ->setPartner($options['partner'])
            ->setTaskType($options['task_type'])
            ->setChecked($options['checked'])
            ->setClnttnum($options['external_id'])
            ->setClntFio($options['client_full_name'])
            ->setClntOrgName($options['client_org_name'])
            ->setClntTel1($options['client_phone1'])
            ->setClntTel2($options['client_phone2'])
            ->setLandlinenum($options['landline_phone'])
            ->setClntType($options['client_type'])
            ->setClntopagr($options['client_operator_agreement_number'])
            ->setSwip($options['switch_ip'])
            ->setSwport($options['switch_port'])
            ->setSwplace($options['switch_location'])
            ->setDom($options['address'])
            ->setDomofon($options['intercom'])
            ->setEtazh($options['floor'])
            ->setKv($options['apartment'])
            ->setPod($options['entrance'])
            ->setAgreement($options['agreement'])
            ->setSlotBegin($options['slot_begin'])
            ->setSlotEnd($options['slot_end'])
            ->setExttype($options['task_subtype'])
            ->setAddinfo($options['additional_info'])
            ->setIstest($options['is_test'])

            ->setMgtsTc($options['mgts_tech_center_id'])
            ->setMgtsAts($options['mgts_ats'])
            ->setMgtsOrsh($options['mgts_orsh'])
            ->setMgtsLines($options['mgts_channel_count'])
            ->setMgtsPhones($options['mgts_phone_count'])
            ->setMgtsSertype($options['mgts_service_type_id'])
            ->setMgtsAgreementNumber($options['mgts_agreement_number'])
        ;

        $this->em->persist($ticket);
        $this->em->flush();

        $task->setTicket($ticket);

        foreach ($task->getAddresses() as $address) {
            if (null === $ticket->getServiceCenter()) {
                break;
            }

            if (null !== $sc = $address->getServiceCenter()) {
                $ticket->setServiceCenter($sc);
                break;
            }
        }

        $this->em->flush();

        if ($pluginUid == Task::CONNECTION) {
            $event = new ConnectionCreatedEvent($task, $ticket);
            $this->dispatcher->dispatch(ConnectionCreatedEvent::NAME, $event);
        }
        elseif ($pluginUid == Task::ACCIDENT) {
            $event = new AccidentCreatedEvent($task, $ticket);
            $this->dispatcher->dispatch(AccidentCreatedEvent::NAME, $event);
        }
        elseif ($pluginUid == Task::SERVICE) {
            $event = new ServiceCreatedEvent($task, $ticket);
            $this->dispatcher->dispatch(ServiceCreatedEvent::NAME, $event);
        }
        elseif ($pluginUid == Task::TELEMARKETING) {
            $event = new TelemarketingCreatedEvent($task, $ticket);
            $this->dispatcher->dispatch(TelemarketingCreatedEvent::NAME, $event);
        }
        else {
            throw new \LogicException('Unknown plugin uid ' . $pluginUid);
        }

        return $task;
    }

    /**
     *
     * @param Task $task
     * @param ListAddr $address
     * @return boolean
     * @throws LogicException
     */
    public function addAddress(Task $task, ListAddr $address)
    {
        if (null === $task->getTicket()) {
            throw new LogicException('Ticket is null for task ' . $task->getId());
        }

        $addresses = $task->getAddresses();

        if ($addresses->contains($address)) {
            return false;
        }

        $task->addAddress($address);
        $this->em->persist($address);
        $this->em->flush();

        return true;
    }

    public function canUpdate(Task $task)
    {
        return in_array($task->getPluginUid(), [
            Task::CONNECTION,
            Task::ACCIDENT,
            Task::SERVICE,
            Task::TELEMARKETING,
        ]) && !TaskManager::isTaskClosed($task);
    }

    protected function doUpdateTicket(Task $task, array $options)
    {
        $ticket = $task->getTicket();
        $commentMsgs = [];

        if (null !== $options['address'] && '' !== $options['address']) {
            $ticket->setDom($options['address']);
            $commentMsgs[] = 'Адрес: ' . $options['address'];
        }

        if (null !== $options['entrance']) {
            $ticket->setPod($options['entrance']);
            $commentMsgs[] = 'Подъезд: ' . $options['entrance'];
        }

        if (null !== $options['floor']) {
            $ticket->setEtazh($options['floor']);
            $commentMsgs[] = 'Этаж: ' . $options['floor'];
        }

        if (null !== $options['apartment']) {
            $ticket->setKv($options['apartment']);
            $commentMsgs[] = 'Квартира: ' . $options['apartment'];
        }

        if (null !== $options['intercom']) {
            $ticket->setDomofon($options['intercom']);
            $commentMsgs[] = 'Домофон: ' . $options['intercom'];
        }

        if (null !== $options['client_type']) {
            $ticket->setClntType($options['client_type']);
            $commentMsgs[] = 'Тип клиента: ' . ('1' == $options['client_type'] ? 'Фл' : 'Юл');
        }

        if (null !== $options['client_org_name']) {
            $ticket->setClntOrgName($options['client_org_name']);
            $commentMsgs[] = 'Организация: ' . $options['client_org_name'];
        }

        if (null !== $options['client_full_name']) {
            $ticket->setClntFio($options['client_full_name']);
            $commentMsgs[] = 'ФИО: ' . $options['client_full_name'];
        }

        if (null !== $options['client_phone1']) {
            $ticket->setClntTel1($options['client_phone1']);
            $commentMsgs[] = 'Тел.1: ' . $options['client_phone1'];
        }

        if (null !== $options['client_phone2']) {
            $ticket->setClntTel2($options['client_phone2']);
            $commentMsgs[] = 'Тел.2: ' . $options['client_phone2'];
        }

        if (null !== $options['additional_info']) {
            $ticket->setAddinfo($options['additional_info']);
            $commentMsgs[] = 'Доп. инфо: ' . $options['additional_info'];
        }

        if ($options['comment']) {
            $commentMsgs[] = $options['comment'];
        }

        if (!empty($commentMsgs)) {
            $comment = (new TaskComment())
                ->setTag($options['comment_tag'])
                ->setText(implode(PHP_EOL, $commentMsgs))
                ->setTask($task);
            $task->addComment($comment);
        }

        $this->em->transactional(function (EntityManagerInterface $em) use ($task) {
            $em->persist($task);
            $em->flush();
        });

        $event = new TicketUpdatedEvent($task, $ticket);
        $this->dispatcher->dispatch(TicketUpdatedEvent::NAME, $event);

        return $task;
    }

    protected function configureUpdateOptions(OptionsResolver $resolver)
    {
        $normalizer = function (Options $options, $value) {
            return '' === $value ? null : $value;
         };

        $resolver->setDefaults([
            'comment' => null,
            'comment_tag' => null,
            'address' => null,
            'entrance' => null,
            'floor'=> null,
            'apartment' => null,
            'intercom' => null,
            'client_type' => null,
            'client_org_name' => null,
            'client_full_name' => null,
            'client_phone1' => null,
            'client_phone2' => null,
            'additional_info' => null,
        ])
            ->setNormalizer('comment', $normalizer)
            ->setNormalizer('entrance', $normalizer)
            ->setNormalizer('floor', $normalizer)
            ->setNormalizer('apartment', $normalizer)
            ->setNormalizer('intercom', $normalizer)
            ->setNormalizer('client_org_name', $normalizer)
            ->setNormalizer('client_full_name', $normalizer)
            ->setNormalizer('client_phone1', $normalizer)
            ->setNormalizer('client_phone2', $normalizer)
            ->setNormalizer('additional_info', $normalizer)
            ->setAllowedTypes('comment', ['null', 'string'])
            ->setAllowedTypes('comment_tag', ['null', 'string'])
            ->setAllowedTypes('address', ['null', ListAddr::class])
            ->setAllowedTypes('entrance', ['null', 'string'])
            ->setAllowedTypes('floor', ['null', 'string'])
            ->setAllowedTypes('apartment', ['null', 'string'])
            ->setAllowedTypes('intercom', ['null', 'string'])
            ->setAllowedTypes('client_org_name', ['null', 'string'])
            ->setAllowedTypes('client_full_name', ['null', 'string'])
            ->setAllowedTypes('client_phone1', ['null', 'string'])
            ->setAllowedTypes('client_phone2', ['null', 'string'])
            ->setAllowedTypes('additional_info', ['null', 'string']);
    }

}
