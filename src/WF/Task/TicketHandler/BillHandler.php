<?php

namespace WF\Task\TicketHandler;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Agreement;
use models\Bill;
use models\BillPayer;
use models\BillType;
use models\ListAddr;
use models\ListContr;
use models\Task;
use models\TaskStatus;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WF\Task\Events\BillCreatedEvent;

/**
 * Description of BillHandler
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class BillHandler extends AbstractTicketHandler
{
    protected function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'payer' => null,
                'payer_text' => null,
                'amount' => null,
                'supplier' => null,
                'client' => null,
                'client_agreement' => null,
                'number' => null,
                'payment_date' => null,
                'type' => null,
                'agreement' => null,
            ])
            ->setAllowedTypes('payer', ['null', BillPayer::class])
            ->setAllowedTypes('payer_text', ['null', 'string'])
            ->setAllowedTypes('amount', ['float', 'string'])
            ->setAllowedTypes('supplier', ['null', ListContr::class])
            ->setAllowedTypes('client', ['null', ListContr::class])
            ->setAllowedTypes('client_agreement', ['null', Agreement::class])
            ->setAllowedTypes('number', 'string')
            ->setAllowedTypes('payment_date', DateTime::class)
            ->setAllowedTypes('type', ['null', BillType::class])
            ->setAllowedTypes('agreement', ['null', Agreement::class])
            ;
    }

    protected function doCreateTicket(Task $task, array $options)
    {
        $bill = (new Bill())
            ->setTask($task)
            ->setPayer($options['payer'])
            ->setPayerText($options['payer_text'])
            ->setAmount($options['amount'])
            ->setSupplier($options['supplier'])
            ->setClient($options['client'])
            ->setClientAgreement($options['client_agreement'])
            ->setNumber($options['number'])
            ->setPaymentDate($options['payment_date'])
            ->setType($options['type'])
            ->setAgreement($options['agreement'])
            ;

        $task->setBill($bill);

        if (null === $task->getStatus()) {
            $status = $this->em
                ->find(TaskStatus::class, TaskStatus::STATUS_BILL_NEW);
            $task->setStatus($status);
        }

        $this->em->transactional(function (EntityManagerInterface $em) use ($task) {
            $em->persist($task);
            $em->flush();
        });

        $event = new BillCreatedEvent($task);
        $this->dispatcher->dispatch(BillCreatedEvent::NAME, $event);

        return $task;
    }

    public function addAddress(Task $task, ListAddr $address)
    {
        return false;
    }

    protected function doUpdateTicket(Task $task, array $options)
    {

    }

    protected function configureUpdateOptions(OptionsResolver $resolver)
    {

    }

}
