<?php

namespace WF\Task\TicketHandler;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use models\Agreement;
use models\Gp;
use models\GpType;
use models\ListAddr;
use models\ListContr;
use models\ListSc;
use models\Task;
use models\TaskComment;
use models\TaskSource;
use models\TaskStatus;
use models\TaskType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WF\Task\Events\GpCreatedEvent;

/**
 * Description of GpHandler
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class GpHandler extends AbstractTicketHandler
{
    protected function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'partner_id' => null,
            'partner' => null,
            'gp_type_id' => null,
            'task_type_id' => null,
            'task_type' => null,
            'agreement_id' => null,
            'agreement' => null,
            'service_center_id' => null,
            'service_center' => null,
            'operator_key' => null,
            'operator_start_dt' => null,
            'operator_gss_dt' => null,
            'author_name' => null,
            'task_source_id' => null,
            'title' => null,
            'body' => null,
            'comment' => null,
            'b2b_count' => 0,
            'b2c_count' => 0
        ])
            ->setAllowedTypes('partner', [ListContr::class])
            ->setAllowedTypes('task_type', [TaskType::class])
            ->setAllowedTypes('partner_id', ['null', 'int'])
            ->setAllowedTypes('gp_type_id', ['null', 'int'])
            ->setAllowedTypes('task_type_id', ['null', 'int'])
            ->setAllowedTypes('agreement_id', ['null', 'int'])
            ->setAllowedTypes('agreement', ['null', Agreement::class])
            ->setAllowedTypes('service_center_id', ['null', 'int'])
            ->setAllowedTypes('service_center', ['null', ListSc::class])
            ->setAllowedTypes('operator_start_dt', ['null', 'string', DateTime::class])
            ->setAllowedTypes('operator_gss_dt', ['null', 'string', DateTime::class])
            ->setAllowedTypes('task_source_id', ['null', 'int'])
            ->setAllowedTypes('b2b_count', ['null', 'int'])
            ->setAllowedTypes('b2c_count', ['null', 'int'])
            ->setDefault('partner', function (Options $o) {
                if (empty($o['partner_id'])) {
                    return null;
                }
                return $this->em->getReference(ListContr::class, $o['partner_id']);
            })
            ->setDefault('gp_type', function (Options $o) {
                if (empty($o['gp_type_id'])) {
                    return null;
                }
                return $this->em->getReference(GpType::class, $o['gp_type_id']);
            })
            ->setDefault('task_type', function (Options $o) {
                if (empty($o['task_type_id'])) {
                    return null;
                }
                return $this->em->getReference(TaskType::class, $o['task_type_id']);
            })
            ->setDefault('service_center', function (Options $o) {
                if (empty($o['service_center_id'])) {
                    return null;
                }
                return $this->em->getReference(ListSc::class, $o['service_center_id']);
            })
            ->setDefault('task_source', function (Options $o) {
                if (empty($o['task_source_id'])) {
                    return null;
                }
                return $this->em->getReference(TaskSource::class, $o['task_source_id']);
            })
            ->setDefault('agreement', function (Options $o) {
                if (empty($o['agreement_id'])) {
                    return null;
                }
                return $this->em->getReference(Agreement::class, $o['agreement_id']);
            })
            ->setNormalizer('operator_start_dt', function (Options $o, $value) {
                if ($value === null || $value instanceof DateTime) {
                    return $value;
                }
                return new DateTime($value);
            })
            ->setNormalizer('operator_gss_dt', function (Options $o, $value) {
                if ($value === null || $value instanceof DateTime) {
                    return $value;
                }
                return new DateTime($value);
            });
    }

    /**
     *
     * @param Task $task
     * @param array $options
     * @return Task
     */
    protected function doCreateTicket(Task $task, array $options)
    {
        /* @var $partner ListContr */
        $partner = $options['partner'];

        /* @var $taskType TaskType */
        $taskType = $options['task_type'];

        $taskTitle = sprintf('Контрагент: %s (%s)',
            $partner->getContrTitle(), $taskType->getTitle());

        $task->setTaskTitle($taskTitle);
        $task->setStatus($this->em->getReference(TaskStatus::class, TaskStatus::STATUS_GLOBAL_PROBLEM_NEW));

        if ($options['comment']) {
            $comment = (new TaskComment())
                ->setText($options['comment'])
                ->setTask($task);
            $task->addComment($comment);
        }

        $gp = (new Gp())
            ->setTask($task)
            ->setServiceCenter($options['service_center'])
            ->setOperatorKey($options['operator_key'])
            ->setOperatorStart($options['operator_start_dt'])
            ->setOperator2gss($options['operator_gss_dt'])
            ->setTitle($options['title'])
            ->setBody($options['body'])
            ->setAuthor($options['author_name'])
            ->setTaskSource($options['task_source'])
            ->setPartner($partner)
            ->setTaskType($options['task_type'])
            ->setGpType($options['gp_type'])
            ->setAgrId($options['agreement_id'])
            ->setOpB2b($options['b2b_count'])
            ->setOpB2c($options['b2c_count']);

        $task->setGp($gp);
        if (null === $task->getStatus()) {
            $status = $this->em
                ->find(TaskStatus::class, TaskStatus::STATUS_GLOBAL_PROBLEM_NEW);
            $task->setStatus($status);
        }

        foreach ($task->getAddresses() as $address) {
            if (null === $gp->getServiceCenter()) {
                break;
            }

            if (null !== $sc = $address->getServiceCenter()) {
                $gp->setServiceCenter($sc);
                break;
            }
        }

        $this->em->transactional(function (EntityManagerInterface $em) use ($task) {
            $em->persist($task);
            $em->flush();
        });

        $event = new GpCreatedEvent($task, $gp);
        $this->dispatcher->dispatch(GpCreatedEvent::NAME, $event);

        return $task;
    }

    /**
     *
     * @param Task $task
     * @param ListAddr $address
     * @return boolean
     * @throws LogicException
     */
    public function addAddress(Task $task, ListAddr $address)
    {
        if (null === $task->getGp()) {
            throw new LogicException('GP is null for task ' . $task->getId());
        }

        $addresses = $task->getAddresses();

        if ($addresses->contains($address)) {
            return false;
        }

        $task->addAddress($address);
        $this->em->persist($address);
        $this->em->flush();

        return true;
    }

    protected function doUpdateTicket(Task $task, array $options)
    {

    }

    protected function configureUpdateOptions(OptionsResolver $resolver)
    {

    }

}
