<?php

namespace WF\Task\TicketHandler;

use models\Task;
use WF\Task\Model\TicketHandlerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Description of AbstractTicketHandler
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
abstract class AbstractTicketHandler implements TicketHandlerInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     *
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    public function createTicket(Task $task, array $options = array())
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        return $this->doCreateTicket($task, $resolver->resolve($options));
    }

    abstract protected function configureOptions(OptionsResolver $resolver);

    abstract protected function configureUpdateOptions(OptionsResolver $resolver);

    abstract protected function doCreateTicket(Task $task, array $options);

    public function canUpdate(Task $task)
    {
        ;
    }

    public function updateTicket(Task $task, array $options = [])
    {
        if (!$this->canUpdate($task)) {
            throw new \LogicException('Can\'t update task');
        }

        $resolver = new OptionsResolver();
        $this->configureUpdateOptions($resolver);

        return $this->doUpdateTicket($task, $resolver->resolve($options));
    }

    abstract protected function doUpdateTicket(Task $task, array $options);

}
