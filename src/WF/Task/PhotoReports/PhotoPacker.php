<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 16.10.16
 * Time: 17:53
 */

namespace WF\Task\PhotoReports;


use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\FileNotFoundException;
use models\Task;
use models\TaskPhoto;
use PHPZip\Zip\Stream\ZipStream;
use Vich\UploaderBundle\Storage\StorageInterface;
use WF\Task\Exception\NoTasksToReport;
use WF\Task\PhotoReports\NamingStrategy\NamingStrategyInterface;

class PhotoPacker
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var NamingStrategyInterface
     */
    private $namer;

    public function __construct(EntityManagerInterface $em, StorageInterface $storage, NamingStrategyInterface $namer)
    {
        $this->storage = $storage;
        $this->em = $em;
        $this->namer = $namer;
    }

    /**
     * @param ZipStream $zip
     * @param TaskPhoto $photo
     * @param $filename
     * @return bool
     * @throws FileNotFoundException
     */
    private function addFilesToArchive(ZipStream $zip, TaskPhoto $photo, $filename)
    {
        $stream = $this->storage->resolveStream($photo->getFile(), 'file');

        if (false === $stream) {
            throw new FileNotFoundException($photo->getFile());
        }

        $zip->openStream($filename);
        $zip->addStreamData(stream_get_contents($stream));
        $zip->closeStream();

        return true;
    }

    /**
     * @param $archiveName
     * @param $tasksIds
     * @return bool
     * @throws NoTasksToReport
     */
    public function buildArchive($archiveName, $tasksIds):bool
    {
        $archiveFile = basename($archiveName);
        $archivePath = dirname($archiveName);

        try {
            $tasks = $this->em->getRepository(Task::class)
                ->findBy([
                    'pluginUid' => Task::CONNECTION,
                    'id' => $tasksIds,
                ]);
        } catch (\Exception $e) {
            throw new NoTasksToReport('Нет заявок для формирования отчёта');
        }

        if (empty($tasks))
            throw new NoTasksToReport('Нет заявок для формирования отчёта');

        $zip = new ZipStream($archivePath . $archiveFile);

        foreach ($tasks as $task) {
            foreach ($task->getPhotos() as $photo) {
                $this->addFilesToArchive($zip, $photo, $this->namer->getFilename($photo));
            }
        }

        return $zip->finalize();
    }
}
