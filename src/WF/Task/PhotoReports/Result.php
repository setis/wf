<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 24.10.16
 * Time: 10:44
 */

namespace WF\Task\PhotoReports;


use PHPZip\Zip\Core\AbstractZipArchive;

class Result
{
    /**
     * @var string[]
     */
    private $errors = [];

    /*
     *
     */
    private $archive;

    /**
     * @return boolean
     */
    public function isSuccess(): bool
    {
        return empty($this->errors);
    }


    /**
     * @param string $error
     * @return Result
     */
    public function pushError(string $error): Result
    {
        $this->errors[] = $error;
        return $this;
    }

    /**
     * @return \string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param AbstractZipArchive $archive
     */
    public function setArchive(AbstractZipArchive $archive)
    {
        $this->archive = $archive;
    }

    /**
     * @return AbstractZipArchive
     */
    public function getArchive():AbstractZipArchive
    {
        return $this->archive;
    }

}
