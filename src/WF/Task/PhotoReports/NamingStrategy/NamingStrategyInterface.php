<?php
/**
 * User: artem
 * Date: 16.10.16
 * Time: 18:04
 */

namespace WF\Task\PhotoReports\NamingStrategy;

use models\TaskPhoto;

interface NamingStrategyInterface
{
    function getFilename(TaskPhoto $photo);
}
