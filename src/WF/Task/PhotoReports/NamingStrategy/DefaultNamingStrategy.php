<?php
/**
 * Author: Artem <mail@artemd.ru>
 * Date: 16.10.16
 */

namespace WF\Task\PhotoReports\NamingStrategy;

use classes\tickets\Ticket;
use models\TaskPhoto;

class DefaultNamingStrategy implements NamingStrategyInterface
{
    /**
     * @param TaskPhoto $photo
     * @return string
     */
    public function getFilename(TaskPhoto $photo)
    {
        /** @var \models\Ticket $ticket */
        $ticket = $photo->getTask()->getAnyTicket();
        $foldername = '' . $ticket->getClnttnum();

        $dom = $ticket->getDom();

        $address = Ticket::getAddr($dom->getId());

        if (isset($address["nokladr"])) {
            $foldername .= "_адрес_не_в_формате";
        } else {
            $foldername .= "_" . $address["street"] . "_" . $address["house"];
        }

        $foldername = preg_replace([
            "/\s+/ui",
            "/[^a-zA-Zа-яА-Я0-9\s_]+/ui"
        ], ['_'], $foldername);

        $ext = substr(strrchr($photo->getFile()->getOrigName(), "."), 1);

        $filename = preg_replace("/\s+/", "_", $ticket->getClnttnum() . "_" . strtolower($photo->getType()->getTitle() . '.' . $ext));

        return $foldername . "/" . $filename;
    }
}
