<?php
/**
 * Artem
 * 01.06.2016 21:22
 */

namespace WF\Task\Controller;

use models\ListAgrPrice;
use models\ListContr;
use models\ListContrCalc;
use models\ListOds;
use models\ListPhotoType;
use models\ListSkpPartnerPrice;
use models\ListSkpPartnerWorkTypes;
use models\ListSkpTechCalc;
use models\ListTechCalc;
use models\ListTechPrice;
use models\ListTtContrCalc;
use models\ListTtTechCalc;
use models\Task;
use models\TaskComment;
use models\TaskStatus;
use models\Ticket;
use models\User;
use repository\ListAgrPriceRepository;
use repository\ListPhotoTypeRepository;
use repository\ListTechPriceRepository;
use repository\TaskStatusRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WF\HttpKernel\RestController;
use WF\Task\Model\TechnicalTicketInterface;
use WF\Task\TaskManager;

class ApiController extends RestController
{
    public function getTaskAction(Request $request)
    {
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('id'));

        return $this->renderJsonResponse($task, ['default']);
    }

    /**
     * Выполняемые работы выставляемые контрагенту
     *
     * @param Request $request
     * @return Response
     */
    public function getPartnerTaskWorksListAction(Request $request)
    {
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('task_id'));
        if ($task->getPluginUid() == Task::GLOBAL_PROBLEM)
            return new Response();
        //"SELECT * FROM `list_agr_price` WHERE `agr_id`=" . $DB->F($ticket->getAgrId(false)) . " AND `price_type`=" . $DB->F("price"|"smeta") . " AND `archive`<1;";
        /** @var ListAgrPriceRepository $techWorksPriceListRepo */
        $techWorksPriceListRepo = $this->getEm()->getRepository(ListAgrPrice::class);


        $result = [];
        if(null !== $task->getTicket()->getAgreement()) {
            $result = $techWorksPriceListRepo->getWorksAndEstimatesPrices($task->getTicket()->getAgreement());
        }

        return $this->renderJsonResponse([
            'items' => $result,
        ], ['default']);
    }

    /**
     * Выполненные работы по заявке для контрагента
     *
     * @param Request $request
     * @return Response
     */
    public function getPartnerTaskWorksAction(Request $request)
    {
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('task_id'));
        $works = [];
        switch ($task->getPluginUid()) {
            case Task::CONNECTION:
                /** @var ListContrCalc[] $works */
                $works = $this->getEm()->getRepository(ListContrCalc::class)->findBy(['task' => $task]);
                break;

            case Task::ACCIDENT:
                /** @var ListTtContrCalc[] $works */
                $works = $this->getEm()->getRepository(ListTtContrCalc::class)->findBy(['task' => $task]);
                break;
        }

        $tmp = [];
        foreach ($works as $wrk) {
            /** @see ListAgrPrice */
            $tmp[] = [
                'title' => $wrk->getWorkTitle(),
                's_title' => $wrk->getWorkTitle(),
                'price' => $wrk->getPrice(),
                'quantity' => $wrk->getQuant(),
                'estimate' => $wrk->getSmetaVal(),
                'price_type' => !empty($wrk->getSmetaVal()) && $wrk->getSmetaVal() > 0 ? ListAgrPrice::PRICE_TYPE_SMETA : ListAgrPrice::PRICE_TYPE_PRICE,
                'agr_id' => $wrk->getWork() ? $wrk->getWork()->getAgrId() : null,
                'id' => $wrk->getWork() ? $wrk->getWork()->getId() : null,
                'archive' => $wrk->getWork() ? $wrk->getWork()->getArchive() : true,
            ];
        }

        return $this->renderJsonResponse([
            'items' => $tmp,
        ], ['default']);

    }

    /**
     * Допустимые работы выполняемые по заявке техника
     *
     * @param Request $request
     * @return Response
     */
    public function getTechTaskWorksListAction(Request $request)
    {
        //"SELECT * FROM `list_tech_price` WHERE `price_type`=".$DB->F("price"|"smeta")." AND `price`>0 AND `archive`<1;"
        /** @var ListTechPriceRepository $techWorksPriceListRepo */
        $techWorksPriceListRepo = $this->getEm()->getRepository(ListTechPrice::class);

        return $this->renderJsonResponse([
            'items' => $techWorksPriceListRepo->getWorksAndEstimatesPrices(),
        ], ['default']);
    }

    public function getTaskServiceWorksListAction(Request $request)
    {
        $task = $this->getTask($request->get('task_id'));

        switch ($task->getPluginUid()) {
            case Task::GLOBAL_PROBLEM:
                $ticket = $task->getGp();
                break;
            default:
                $ticket = $task->getTicket();
                break;
        }

        $prices = $this->getEm()->getRepository(ListSkpPartnerPrice::class)
            ->findBy(['partner' => $ticket->getPartner()]);

        return $this->renderJsonResponse($prices, ['default']);
    }

    /**
     * @param $id
     * @return Task
     */
    private function getTask($id)
    {
        return $this->getEm()->find(Task::class, $id);
    }

    public function getTaskServiceWorksAction(Request $request)
    {
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('task_id'));
        /** @var ListSkpTechCalc $taskSkpWorks */
        $taskSkpWorks = $this->getEm()->getRepository(ListSkpTechCalc::class)
            ->findBy(['task' => $task]);

        return $this->renderJsonResponse(['items' => $taskSkpWorks, 'cash' => $task->getTicket()->getPrice()], ['default']);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function postTaskServiceWorksAction(Request $request)
    {
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('task_id'));

        if (!$this->checkAccess('canCalcServiceWorks', $this->getUser(), $task)) {
            throw new AccessDeniedHttpException(
                sprintf(
                    'User #%d (%s) cant calc task #%d',
                    $this->getUser()->getId(),
                    $this->getUser()->getLogin(),
                    $this->getUser()->getId()
                )
            );
        }

        if ($task->getSchedule() === null || $task->getSchedule()->getTechnician() === null)
            throw new \LogicException('Can\'t calc not scheduled ticket!');

        $this->getEm()->beginTransaction();

        $query = "DELETE " . ListSkpTechCalc::class . ' techSkpCalc WHERE techSkpCalc.task = :task';
        $this->getEm()
            ->createQuery($query)
            ->setParameters(['task' => $task])
            ->execute();

        foreach ($request->get('works', []) as $item) {
            /** @var ListSkpPartnerWorkTypes $workType */
            $workType = $this->getEm()->getReference(ListSkpPartnerWorkTypes::class, $item['work']['id']);
            $work = (new ListSkpTechCalc())
                ->setTask($task)
                ->setUser($this->getUser())
                ->setTechnician($task->getSchedule()->getTechnician())
                ->setWork($workType)
                ->setCrDate(new \DateTime())
                ->setCalcDate(new \DateTime())
                ->setMinval($item['ei_min'])
                ->setQuantity($item['quantity'])
                ->setPrice($item['price']);

            $this->getEm()->persist($work);

            $this->getEm()->persist($task
                ->getTicket()
                ->setOrientPrice($request->get('ticket_sum'))
                ->setPrice($request->get('ticket_sum'))
            );
        }


        $this->getEm()->flush();
        $this->getEm()->commit();

        return new Response();
    }

    private function checkAccess($resource, $user, $task)
    {
        $permissionsFactory = $this->getContainer()->get('wf.task.calc_permissions_factory');
        return $permissionsFactory->getChecker($resource)->can($user, $task);
    }

    /**
     * Сделаныне техников работы по заявке
     *
     * @param Request $request
     * @return Response
     */
    public function getTechTaskWorksAction(Request $request)
    {
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('task_id'));
        $works = [];
        switch ($task->getPluginUid()) {
            case Task::CONNECTION:
                /** @var ListTechCalc[] $works */
                $works = $this->getEm()->getRepository(ListTechCalc::class)->findBy(['task' => $task]);
                break;

            case Task::ACCIDENT:
                /** @var ListTtTechCalc[] $works */
                $works = $this->getEm()->getRepository(ListTtTechCalc::class)->findBy(['task' => $task]);
                break;
        }

        $tmp = [];
        foreach ($works as $wrk) {
            /** @see ListAgrPrice */
            $tmp[] = [
                'title' => $wrk->getWorkTitle(),
                'short_title' => $wrk->getWork() ? $wrk->getWork()->getShortTitle() : null,
                'p_comment' => $wrk->getWork() ? $wrk->getWork()->getPComment() : null,
                'price' => $wrk->getPrice(),
                'quantity' => $wrk->getQuant(),
                'estimate' => $wrk->getSmetaVal(),
                'price_type' => !empty($wrk->getSmetaVal()) && $wrk->getSmetaVal() > 0 ? ListAgrPrice::PRICE_TYPE_SMETA : ListAgrPrice::PRICE_TYPE_PRICE,
                'id' => $wrk->getWork() ? $wrk->getWork()->getId() : null,
                'archive' => $wrk->getWork() ? $wrk->getWork()->getArchive() : true,
            ];
        }

        return $this->renderJsonResponse([
            'items' => $tmp,
        ], ['default']);
    }

    /**
     * Список существующих штрафов
     *
     * @param Request $request
     * @return Response
     */
    public function getPenaltiesListAction(Request $request)
    {
        //"SELECT * FROM `list_tech_price` WHERE `price`<0 AND `archive`<1;"
        /** @var ListTechPriceRepository $techWorksPriceListRepo */
        $techWorksPriceListRepo = $this->getEm()->getRepository(ListTechPrice::class);

        return $this->renderJsonResponse([
            'items' => $techWorksPriceListRepo->getPenalties(),
        ], ['default']);
    }

    /**
     * Назначеные по заявке технику штрафы
     *
     * @param Request $request
     * @return Response
     */
    public function getPenaltiesAction(Request $request)
    {
        return $this->renderJsonResponse([], ['default']);
    }

    /**
     * Список требуемых фоток
     *
     * @param Request $request
     * @return Response
     */
    public function getRequiredToTaskPhotosAction(Request $request)
    {
        //$wtype = $DB->getField("SELECT `task_type` FROM `tickets` WHERE `task_id`=" . $DB->F($task->getId()) . ";");
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('task_id'));

        //"SELECT * FROM `list_photo_types` WHERE `active` AND `wtype`=" . $DB->F($wtype) . " AND `task_type`='accidents' AND `contr_id`=" . $DB->F($task->getContrId()) .";"
        /** @var ListPhotoTypeRepository $photoTypesRepo */
        $photoTypesRepo = $this->getEm()->getRepository(ListPhotoType::class);

        return $this->renderJsonResponse([
            'items' => $photoTypesRepo->getTypesByTask($task),
        ], ['default']);
    }

    /**
     * Загруженные фотки
     *
     * @param Request $request
     * @return Response
     */
    public function getTaskPhotosAction(Request $request)
    {
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('task_id'));
        /** @var ListPhotoTypeRepository $photoTypeRepo */
        $photoTypeRepo = $this->getEm()->getRepository(ListPhotoType::class);

        return $this->renderJsonResponse([
            'items' => $photoTypeRepo->getTypesByTask($task),
        ], ['default']);

    }

    public function postOdsListAction(Request $request)
    {
        $ods = $this->getEm()->createQueryBuilder()
            ->select('ods')
            ->from(ListOds::class, 'ods')
            ->where('ods.title like :query')
            ->orWhere('ods.address like :query')
            ->orWhere('ods.tel like :query')
            ->orWhere('ods.fio like :query')
            ->orWhere('ods.comment like :query')
            ->setParameter('query', '%' . $request->get('query') . '%')
            ->setMaxResults(15)
            ->getQuery()->getResult();

        return $this->renderJsonResponse($ods, ['default']);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getOdsAction(Request $request)
    {
        $task = $this->getTask($request->get('task_id'));

        switch ($task->getPluginUid()) {
            case Task::GLOBAL_PROBLEM:
                $ods = null;
                break;
            default:
                $ticket = $task->getTicket();
                $ods = $ticket
                    ->getDom()
                    ->getOds();
                break;
        }


        return $this->renderJsonResponse($ods, ['default']);
    }

    public function setOdsAction(Request $request)
    {
        $task = $this->getTask($request->get('task_id'));

        $ods = null;
        if ($request->request->has('ods') && isset($request->get('ods')['id']))
            $ods = $this->getEm()->find(ListOds::class, $request->get('ods')['id']);

        $ticket = $task->getAnyTicket();
        if ($ticket instanceof TechnicalTicketInterface) {
            $ticket->getDom()->setOds($ods);
        }

        $this->getEm()->persist($task->getTicket()->getDom());
        $this->getEm()->flush();

        return new Response();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function postPartnerBilledWorksAction(Request $request)
    {
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('task_id'));

        if (!$this->checkAccess('canCalcPartnerWorks', $this->getUser(), $task)) {
            throw new AccessDeniedHttpException(
                sprintf(
                    'User #%d (%s) cant calc task #%d',
                    $this->getUser()->getId(),
                    $this->getUser()->getLogin(),
                    $this->getUser()->getId()
                )
            );
        }

        $this->getEm()->beginTransaction();

        switch ($task->getPluginUid()) {
            case Task::CONNECTION:
                $query = "DELETE " . ListContrCalc::class . ' partnerCalc WHERE partnerCalc.task = :task';
                $this->getEm()
                    ->createQuery($query)
                    ->setParameters(['task' => $task])
                    ->execute();

                foreach ($request->get('works', []) as $item) {
                    /** @var ListAgrPrice $agreementWork */
                    $agreementWork = $this->getEm()->getReference(ListAgrPrice::class, $item['id']);
                    $work = (new ListContrCalc())
                        ->setTask($task)
                        ->setUser($this->getUser())
                        ->setWork($agreementWork)
                        ->setWorkTitle($item['title'])
                        ->setCalcDate(new \DateTime())
                        ->setUpdateDate(new \DateTime())
                        ->setQuant($item['quantity'])
                        ->setPrice(ListTechPrice::PRICE_TYPE_PRICE === $agreementWork->getPriceType() ? $item['price'] : 100)
                        ->setSmetaVal(ListTechPrice::PRICE_TYPE_SMETA === $agreementWork->getPriceType() ? $item['estimate'] : null);

                    $this->getEm()->persist($work);
                }

                break;

            case Task::ACCIDENT:
                $query = "DELETE " . ListTtContrCalc::class . ' partnerCalc WHERE partnerCalc.task = :task';
                $this->getEm()
                    ->createQuery($query)
                    ->setParameters(['task' => $task])
                    ->execute();

                foreach ($request->get('works', []) as $item) {
                    /** @var ListAgrPrice $agreementWork */
                    $agreementWork = $this->getEm()->getReference(ListAgrPrice::class, $item['id']);
                    $work = (new ListTtContrCalc())
                        ->setTask($task)
                        ->setUser($this->getUser())
                        ->setWork($agreementWork)
                        ->setWorkTitle($item['title'])
                        ->setCalcDate(new \DateTime())
                        ->setUpdateDate(new \DateTime())
                        ->setQuant($item['quantity'])
                        ->setPrice(ListTechPrice::PRICE_TYPE_PRICE === $agreementWork->getPriceType() ? $item['price'] : 0)
                        ->setSmetaVal(ListTechPrice::PRICE_TYPE_SMETA === $agreementWork->getPriceType() ? $item['estimate'] : null);

                    $this->getEm()->persist($work);
                    $this->getEm()->flush();
                }

                break;
            default:
                throw new \LogicException(sprintf('Wrong task type "%s":%d!', $task->getPluginUid(), $task->getId()));
        }

        $this->getEm()->flush();
        $this->getEm()->commit();

        return new Response();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function postTechProfitWorksAction(Request $request)
    {
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('task_id'));

        if (!$this->checkAccess('canCalcTechWorks', $this->getUser(), $task)) {
            throw new AccessDeniedHttpException(
                sprintf(
                    'User #%d (%s) cant calc task #%d',
                    $this->getUser()->getId(),
                    $this->getUser()->getLogin(),
                    $this->getUser()->getId()
                )
            );
        }

        $this->getEm()->beginTransaction();

        switch ($task->getPluginUid()) {
            case Task::CONNECTION:
                $query = "DELETE " . ListTechCalc::class . ' techConnectionCalc WHERE techConnectionCalc.task = :task';
                $this->getEm()
                    ->createQuery($query)
                    ->setParameters(['task' => $task])
                    ->execute();

                foreach ($request->get('works', []) as $item) {
                    /** @var ListTechPrice $agreementWork */
                    $agreementWork = $this->getEm()->getReference(ListTechPrice::class, $item['id']);
                    $work = (new ListTechCalc())
                        ->setTask($task)
                        ->setUser($this->getUser())
                        ->setTechnician($task->getSchedule()->getTechnician())
                        ->setWork($agreementWork)
                        ->setWorkTitle($item['title'])
                        ->setCalcDate(new \DateTime())
                        ->setUpdateDate(new \DateTime())
                        ->setQuant($item['quantity'])
                        ->setPrice($item['price'])
                        ->setSmetaVal(ListTechPrice::PRICE_TYPE_SMETA === $agreementWork->getPriceType() ? $item['estimate'] : null);

                    $this->getEm()->persist($work);
                }

                break;

            case Task::ACCIDENT:
                $query = "DELETE " . ListTtTechCalc::class . ' techAccidentCalc WHERE techAccidentCalc.task = :task';
                $this->getEm()
                    ->createQuery($query)
                    ->setParameters(['task' => $task])
                    ->execute();

                foreach ($request->get('works', []) as $item) {
                    /** @var ListTechPrice $agreementWork */
                    $agreementWork = $this->getEm()->getReference(ListTechPrice::class, $item['id']);
                    $work = (new ListTtTechCalc())
                        ->setTask($task)
                        ->setUser($this->getUser())
                        ->setTechnician($task->getSchedule()->getTechnician())
                        ->setWork($agreementWork)
                        ->setWorkTitle($item['title'])
                        ->setCalcDate(new \DateTime())
                        ->setUpdateDate(new \DateTime())
                        ->setQuant($item['quantity'])
                        ->setPrice(ListTechPrice::PRICE_TYPE_PRICE === $agreementWork->getPriceType() ? $item['price'] : null)
                        ->setSmetaVal(ListTechPrice::PRICE_TYPE_SMETA === $agreementWork->getPriceType() ? $item['estimate'] : null);

                    $this->getEm()->persist($work);
                }

                break;
            default:
                throw new \LogicException(sprintf('Wrong task type "%s":%d!', $task->getPluginUid(), $task->getId()));
        }

        $this->getEm()->flush();
        $this->getEm()->commit();

        return new Response();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function postCalcSavedAction(Request $request)
    {
        $data = $request->get('data');

        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $data['taskId']);
        if (null === $task)
            throw new NotFoundHttpException('Нет такой заявки!');

        /** @var TaskStatusRepository $ts */
        $ts = $this->getEm()->getRepository(TaskStatus::class);

        /** @var TaskManager $tm */
        $tm = $this->getContainer()->get('wf.task.task_manager');

        /** @var User $author */
        $author = $this->getUser();

        $ticket = $task->getAnyTicket();
        if (!$ticket instanceof TechnicalTicketInterface) {
            throw new \LogicException("Неверный тип заявки!");
        }

        $msg = '';
        switch ($task->getPluginUid()) {
            case Task::CONNECTION:
                $hasRequiredRole = $author->isCoordinator()
                    || $author->isMolOnWarehouse($ticket->getServiceCenter())
                    || $author->isSuperMolOnWarehouse($ticket->getServiceCenter())
                    || $author->isDispatcher();
                $hasRequiredData = !empty($data['billedWorks']) && !empty($data['techProfitWorks']);
                $hasAllowedStatus = in_array($task->getStatus()->getId(), [
                    TaskStatus::STATUS_CONNECTION_DONE,
                    TaskStatus::STATUS_CONNECTION_CALC_TMC,
                    TaskStatus::STATUS_CONNECTION_REPORT,
                ]);

                if ($hasRequiredRole && $hasRequiredData && $hasAllowedStatus) {
                    $status = $ts->find(TaskStatus::STATUS_CONNECTION_CLOSED);
                    $tag = '<b>Статус</b>: Закрыта (' . $status->getId() . ')';
                }

                if(!$hasRequiredRole && in_array($task->getStatus()->getId(), [TaskStatus::STATUS_CONNECTION_DONE, TaskStatus::STATUS_CONNECTION_REPORT])) {
                    /** @var TaskStatus $status */
                    $status = $ts->find(TaskStatus::STATUS_CONNECTION_CALC_TMC);
                    $tag = '<b>Статус</b>: Расчет ТМЦ (' . $status->getId() . ')';
                }
                break;
            case Task::ACCIDENT:
                $hasRequiredRole = $author->isCoordinator()
                    || $author->isMolOnWarehouse($ticket->getServiceCenter())
                    || $author->isSuperMolOnWarehouse($ticket->getServiceCenter())
                    || $author->isDispatcher();
                $hasRequiredData = !empty($data['billedWorks']) && !empty($data['techProfitWorks']);
                $hasAllowedStatus = in_array($task->getStatus()->getId(), [
                    TaskStatus::STATUS_ACCIDENT_DONE,
                    TaskStatus::STATUS_ACCIDENT_REPORT,
                ]);

                if ($hasRequiredRole && $hasRequiredData && $hasAllowedStatus) {
                    /** @var TaskStatus $status */
                    $status = $ts->find(TaskStatus::STATUS_ACCIDENT_CLOSED);
                    $tag = '<b>Статус</b>: Закрыта (' . $status->getId() . ')';
                }

                if(!$hasRequiredRole && TaskStatus::STATUS_ACCIDENT_DONE === $task->getStatus()->getId()) {
                    /** @var TaskStatus $status */
                    $status = $ts->find(TaskStatus::STATUS_ACCIDENT_REPORT);
                    $tag = '<b>Статус</b>: Сдача отчёта (' . $status->getId() . ')';
                }
                break;
            case Task::GLOBAL_PROBLEM:
                $status = $ts->find(TaskStatus::STATUS_GLOBAL_PROBLEM_DONE);
                $tag = 'Расчёт обновлён.';
                break;
            case Task::SERVICE:
                /** @var TaskStatus $status */
                $status = $ts->find(TaskStatus::STATUS_SERVICE_DONE);
                $tag = '<b>Статус</b>: Выполнена (' . $status->getId() . ')';
                break;
            default:
                throw new \LogicException("Неверный тип заявки!");
        }

//        if (count($data['install'])) {
//            foreach ($data['install'] as $items) {
//                $msg .= $this->render('calc_install.twig', array('items' => $items['items']));
//            }
//        }

//        if (count($data['uninstall'])) {
//            $msg .= $this->render('calc_uninstall.twig', array('items' => $data['uninstall']));
//        }

        if (count($data['serviceWorks'])) {
            $msg .= $this->render('service_works.twig', array('items' => $data['serviceWorks'], 'amount'=>$data['payedByCash']));
        }

        $comment = new TaskComment();
        $comment->setStatus($status)
            ->setAuthor($author)
            ->setTag($tag)
            ->setDatetime(new \DateTime())
            ->setText($msg);

        $tm->addComment($task, $comment);

        return new Response();
    }

    public function postCheckExternalIdAction(Request $request)
    {

        $tickets = $this->getEm()->getRepository(Ticket::class)
            ->findBy([
                'partner' => $this->getEm()->getReference(ListContr::class, $request->get('partner_id')),
                'clnttnum' => $request->get('external_id'),
            ]);

        if (empty($tickets)) {
            return new Response('', 404);
        }

        return $this->renderJsonResponse($tickets, ['default']);
    }

}
