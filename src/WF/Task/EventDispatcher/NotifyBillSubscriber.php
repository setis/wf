<?php

namespace WF\Task\EventDispatcher;

use models\Task;
use models\TaskStatus;
use WF\Notification\AbstractNotificationSubscriber;
use WF\Notification\Model\Notification;
use WF\Slack\Formatter\BoundedUsersChangeFormatter;
use WF\Slack\Formatter\CommentedFormatter;
use WF\Slack\Formatter\FileLinksFormatter;
use WF\Slack\Formatter\TicketFormatterSimpleFactory as FF;
use WF\Slack\Notification\SlackNotification;
use WF\Task\Events\BillCreatedEvent;
use WF\Task\Events\BillDelayEndEvent;
use WF\Task\Events\BillPaymentDeadlineEvent;
use WF\Task\Events\TaskCommentedEvent;
use WF\Task\Events\TaskExecutorsChangedEvent;

/**
 * Description of NotifyBillSubscriber
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyBillSubscriber extends AbstractNotificationSubscriber
{
    public static function getSubscribedEvents()
    {
        return [
            TaskExecutorsChangedEvent::NAME => 'onTaskExecutorsChanged',
            TaskCommentedEvent::NAME => 'onTaskCommented',
            BillCreatedEvent::NAME => 'onCreated',
            BillDelayEndEvent::NAME => 'onDelayEnd',
            BillPaymentDeadlineEvent::NAME => 'onPaymentDeadline',
        ];
    }

    public function onCreated(BillCreatedEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        $recipients = $this->userRepo->getTaskAllBoundedUsers($task, null, true);
        $link = $this->getTaskLink($task, 'viewbill');
        $notification = new SlackNotification();
        $notification->setTo($recipients)
            ->setSubject('Создан счет №' . $task->getId());

        $this->handle('slack',
            FF::getTicketFormatter($task, $notification)
        );

        $htmlMessage = $this->renderMessage('Task/Notification/billCreated.html.php', [
            'task' => $task,
            'link' => $link,
            'executors' => $this->userRepo->getTaskExecutors($task, null, true),
            'author' => $task->getAuthor(),
        ]);
        $notification->setMessage($htmlMessage);
        $this->handle('email', $notification);

    }

    public function onTaskCommented(TaskCommentedEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        $comment = $event->getComment();
        $users = $this->userRepo->getTaskAllBoundedUsers($task, [$comment->getAuthor()], true);
        $link = $this->getTaskLink($task, 'viewbill');

        $notification = new SlackNotification();
        $notification->setTo($users)
            ->setSubject('Добавлен коментарий к счету №' . $task->getId());

        $this->handle('slack',
            FF::getTicketFormatter($task,
                (new FileLinksFormatter(
                    (new CommentedFormatter($notification))
                        ->setComment($comment))
                )
                    ->setComment($comment)
            )
                ->setTask($task)
                ->setTaskLink($link));

        $htmlMessage = $this->renderMessage('Task/Notification/billTaskCommented.html.php', [
            'task' => $task,
            'link' => $link,
            'executors' => $this->userRepo->getTaskExecutors($task, null, true),
            'author' => $task->getAuthor(),
            'comment' => $comment,
        ]);
        $notification->setMessage($htmlMessage);
        $this->handle('email', $notification);
    }

    public function onTaskExecutorsChanged(TaskExecutorsChangedEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        $recipients = $this->userRepo->getTaskAllBoundedUsers($task, [$event->getAuthor()], true);
        $link = $this->getTaskLink($task, 'viewbill');
        $added = $event->getAddedExecutors();
        $removed = $event->getRemovedExecutors();

        $notification = new SlackNotification();
        $notification->setTo($recipients)
            ->setSubject('Изменения в исполнителях счета №' . $task->getId());

        $this->handle('slack',
            (new BoundedUsersChangeFormatter(
                FF::getTicketFormatter($task, $notification)
                    ->setTaskLink($this->getTaskLink($task, 'viewbill'))
            ))->setAddedUsers($added)->setRemovedUsers($removed)
        );

        $htmlMessage = $this->renderMessage('Task/Notification/billExecutorsChanged.html.php', [
            'task' => $task,
            'link' => $link,
            'executors' => $this->userRepo->getTaskExecutors($task, null, true),
            'author' => $task->getAuthor(),
            'added' => $added,
            'removed' => $removed,
        ]);
        $notification->setMessage($htmlMessage);
        $this->handle('email', $notification);
    }

    public function onDelayEnd(BillDelayEndEvent $event)
    {
        $task = $event->getTask();
        $bill = $task->getBill();
        $recipients = $this->userRepo->getTaskExecutors($task, null, true);
        $link = $this->getTaskLink($task, 'viewbill');
        $delayDate = $bill->getDelayed();

        $notification = new Notification();
        $notification->setTo($recipients)
            ->setSubject('Срок отложенного счета №' . $task->getId());

        $htmlMessage = $this->renderMessage('Task/Notification/billDelayEnd.html.php', [
            'task' => $task,
            'link' => $link,
            'executors' => $this->userRepo->getTaskExecutors($task, null, true),
            'author' => $task->getAuthor(),
        ]);
        $notification->setMessage($htmlMessage);
        $this->handle('email', $notification);

    }

    public function onPaymentDeadline(BillPaymentDeadlineEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupportedForPaymentDeadline($task)) {
            return;
        }

        $bill = $task->getBill();
        $recipients = $this->userRepo->getTaskExecutors($task, null, true);
        $link = $this->getTaskLink($task, 'viewbill');
        $paymentDate = $bill->getPaymentDate();

        $notification = new SlackNotification();
        $notification->setTo($recipients)
            ->setSubject('Наступила дата платежа по счету №' . $task->getId());

        $this->handle('slack',
            FF::getTicketFormatter($task, $notification)
        );

        $htmlMessage = $this->renderMessage('Task/Notification/billPaymentDeadline.html.php', [
            'task' => $task,
            'link' => $link,
            'executors' => $this->userRepo->getTaskExecutors($task, null, true),
            'author' => $task->getAuthor(),
        ]);
        $notification->setMessage($htmlMessage);
        $this->handle('email', $notification);
    }

    protected function notSupported(Task $task)
    {
        return $task->getPluginUid() !== Task::BILL;
    }

    private function notSupportedForPaymentDeadline(Task $task)
    {
        if ($task->getPluginUid() !== Task::BILL) {
            return true;
        }

        $statusId = $task->getStatus()->getId();

        return !in_array($statusId, [
            TaskStatus::STATUS_BILL_NEW,
            TaskStatus::STATUS_BILL_APPROVED,
            TaskStatus::STATUS_BILL_AGREED,
            TaskStatus::STATUS_BILL_TO_PAYMENT,
            TaskStatus::STATUS_BILL_PARTLY_PAYED,
        ]);
    }
}
