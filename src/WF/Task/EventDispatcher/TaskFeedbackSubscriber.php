<?php
namespace WF\Task\EventDispatcher;

use Swift_Mailer;
use Swift_Message;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WF\Task\Events\TaskLoyaltyEvent;
use WF\Task\Events\TaskQuestionnaireEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use classes\tickets\TelemarketingTicket;

class TaskFeedbackSubscriber implements EventSubscriberInterface
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $recipient_email;

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * TaskFeedbackSubscriber constructor.
     * @param Swift_Mailer $mailer
     * @param ContainerInterface $container
     * @param $recipient
     */
    public function __construct($mailer, ContainerInterface $container, $recipient = null)
    {
        $this->mailer = $mailer;
        $this->container = $container;
        $this->recipient_email = $recipient ?: $this->container->getParameter('developer_email');
    }

    public static function getSubscribedEvents()
    {
        return array(
            TaskQuestionnaireEvent::NAME => 'onTaskQuestionnaireFilled',
        );
    }

    public function QuestionnarietoString(array $questionnaire)
    {
        $res = '';
        foreach ($questionnaire['questions'] as $q) {
            if (!isset($q['question']))
                continue;

            $res .= $q['question'] . ': ' . (isset($q['answered']) && !empty($q['answered']) ? implode(', ', (array)$q['answered']) : 'Нет ответа') . ".\n";

        }

        if (isset($questionnaire['comment']) && !empty($questionnaire['comment']))
            $res .= "Коментарий: " . $questionnaire['comment'] . "\n\n";

        return $res;
    }

    public function onTaskQuestionnaireFilled(TaskQuestionnaireEvent $event)
    {
        global $DB;
        //$event->getQuestionnaire()
        //$event->getAuthor();
        //$event->getTask();

        #debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,5);
        # EF: создаем заявку, вычисляем номер договора с видом работ ТМ и сохраняем заявку с новым договором
        #error_log(print_r($event->getQuestionnaire(), true)."\n",3,"/ramdisk/onTaskQuestionnaireFilled.txt");
        #error_log(print_r($event->getLoyalty(), true)."\n",3,"/ramdisk/onTaskLoyaltyFilled.txt");
        $task = $event->getTask();
        $task_id = $task->getId();
        $plugin_uid = $task->getPluginUid();
        #error_log($plugin_uid.":".$task_id."\n",3,"onTaskQuestionnaireFilled.txt");
        #$ticket = new TelemarketingTicket($a["TASK_ID"]);
        $sql = "select law.agr_id, law.wtype_id, t.cnt_id
 from task_types tt,
      link_agr_wtypes law,
      list_contr_agr lca,
      tickets t
where law.wtype_id = tt.id
  and law.agr_id = lca.id
  and t.task_id = ".$task_id."
  and lca.contr_id = t.cnt_id
  and tt.plugin_uid = 'tm_module'";
        $result = $DB->getAll($sql);
        
        $parent_request = '';
        if ($plugin_uid == 'accidents') {
            $parent_request = 'ТТ';
        }
        if ($plugin_uid == 'services') {
            $parent_request = 'СКП';
        }
        if ($plugin_uid == 'connections') {
            $parent_request = 'Подключения';
        }

        #error_log(print_r($result,true)."\n",3,"onTaskQuestionnaireFilled.txt");
        #exit;
        $cntr_id = 0;
        $wtype_id = 0;
        $agr_id = 0;
        foreach ($result as $key => $val) {
            $agr_id = $val['agr_id'];
            $wtype_id = $val['wtype_id'];
            $cntr_id = $val['cnt_id'];
        }
        #error_log(print_r($result,true)."\n",3,"onTaskQuestionnaireFilled.txt");
        #error_log($cntr_id.":".$agr_id.":".$wtype_id."\n",3,"onTaskQuestionnaireFilled.txt");
        if ($agr_id > 0) {
            $data_q = json_decode($event->getTask()->getQuestionnaire(), true);
            $data_l = json_decode($event->getTask()->getLoyalty(), true);
            #error_log(print_r($data_q,true)."\n",3,"onTaskQuestionnaireFilled.txt");
            #error_log(print_r($data_l,true)."\n",3,"onTaskQuestionnaireFilled.txt");
            $create_tm = 0;
            ### 1. Если клиент всем доволен, то заявку не создаем. Даже если написан комментарий - так сказал Петр в разговоре по скауп 29.07.2016
            foreach ($data_l['questions'] as $key => $val) {
                #error_log(print_r($data,true)."\n",3,"onTaskQuestionnaireFilled.txt");
                #error_log($key."=>".print_r($val,true)."\n",3,"/ramdisk/loyalty.log");
                #error_log("val[".$key."]=".print_r($val,true)."\n",3,"onTaskQuestionnaireFilled.txt");
                if ( 
                        (($val['question'] == "У клиента есть проблема?") && ($val['answered'] == "Да")) 
                     || (($val['question'] == "Клиента ВСЕ устраивает?") && ($val['answered'] == "Нет")) 
                     || (($val['question'] == "У клиента есть запрос?") && ($val['answered'] == "Да")) 
                   ) {
                    $create_tm = 1;
                }
            }
            #error_log("Лояльность create_tm = ".$create_tm."\n", 3, "/ramdisk/loyalty.log");
            ### 2. Если клиент заполнил опросник, создаем заявку ТМ
            foreach ($data_q['questions'] as $key => $val) {
                 if (count($val['answered']) > 0) {
                     $create_tm = 2;
                 }
            }
            #error_log("Лояльность create_tm = ".$create_tm."\n", 3, "/ramdisk/loyalty.log");
            if ($create_tm > 0) {
                #error_log("Создаем заявку\n",3,"onTaskQuestionnaireFilled.txt");
                $comment = "Опросник:\n".$this->QuestionnarietoString($data_q)."\n\nЛояльность:\n".$this->QuestionnarietoString($data_l);
                #error_log($comment."\n",3,"onTaskQuestionnaireFilled.txt");
                ### для создания ТМ из разных типов заявок нас не интересует сам объект заявки, нас интересует только набор полей в таблице ticket
                #error_log("Комментарий = ".$comment." \n",3,"/ramdisk/tm.log");
                $sql = "select * from tickets t where t.task_id = ".$task_id;
                $res = $DB->getRow($sql, true);
                #error_log("res = ".print_r($res,true)." \n",3,"/ramdisk/tm.log");

                $new_task_id = TelemarketingTicket::createTicket(
                    $cntr_id, ### Ростелеком
                    $wtype_id, ### Тип работ 39, демонтаж оборудования
                    $res['dom_id'],
                    $res['pod'],
                    $res['etazh'],
                    $res['kv'],
                    $res['domofon'],
                    $res['clnt_type'],
                    $res['clnt_org_name'],
                    $res['clnt_fio'],
                    $res['clnt_passp_sn'],
                    $res['clnt_passport_vid'],
                    $res['clnt_passport_viddate'],
                    $res['clnt_tel1'],
                    $res['clnt_tel2'], ## городской телефон в этой заявке не задан
                    0, ### исполнителей пока не заполняем
                    'Заявка создана из опросника/лояльности к заявке '.$parent_request." ".$task_id, ### cmm - но мы дублировать его пока не будем
                    $comment,
                    $agr_id, ## номер договора
                    $res['swip'],
                    $res['swport'],
                    $res['swplace'],
                    $res['clntopagr'],
                    $res['clnttnum'],
                    $res['sc_id'],
                    0, 
                    0, 
                    0, 
                    0, 
                    145 /* статус Анкета Лояльности */
                );

                #error_log("new_task_id = ".$new_task_id." \n",3,"/ramdisk/tm.log");
                $ticket = new TelemarketingTicket($new_task_id);
                $ticket->service_ticket_extras['current_comments'] = '';
                $ticket->service_ticket_extras['subtype'] = 1;
                $ticket->service_ticket_extras['task_id'] = $new_task_id;
                $ticket->updateServiceTicketExtras($new_task_id);
                #error_log("Создана заявка ТМ ID = ".$new_task_id."\n",3,"/ramdisk/tm.log");
            }
        }
        if ($create_tm == 0) {
            #error_log("Заявка не создана\n",3,"/ramdisk/tm.log");
        }
        // $this->sendEmail($event);
    }

    private function sendEmail(TaskLoyaltyEvent $event) {
        /** @var Swift_Mailer $mailer */
        $mailer = $this->mailer;
        /** @var Swift_Message $message */
        $message = $mailer->createMessage();
        $message
            ->setSubject('Новая анкета')
            ->setFrom($this->container->getParameter('robot_email'))
            ->setTo($this->recipient_email)
            ->setBody(
                '<h2>Заявка номер №' . $event->getTask()->getId() . '</h2>' .
                #'<p>' . $this->getAddr($event->getTask()) . '</p>' .
                '<pre>'. $event->getTask()->getQuestionnaire() .'</pre>',
                'text/html'
            );

        $header = $message->getHeaders();
        $header->addPathHeader('Return-Path', $this->container->getParameter('mailer_username'));

        return $mailer->send($message) > 0;
    }

}
