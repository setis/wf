<?php

namespace WF\Task\EventDispatcher;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use models\TaskComment;
use models\TaskStatus;
use Psr\Log\LoggerInterface;
use WF\Sms\Model\SmsManagerInterface;
use WF\Sms\SmsMessage;
use WF\Task\Events\TaskStatusChangedEvent;

/**
 * Description of NotifyClientOnTaskCompleteListener
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyClientOnTaskCompleteListener
{
    /**
     *
     * @var SmsManagerInterface
     */
    private $smsManager;

    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     *
     * @var string
     */
    private $companyPhone;

    /**
     * NotifyClientOnTaskCompleteListener constructor.
     * @param LoggerInterface $logger
     * @param SmsManagerInterface $smsManager
     * @param EntityManagerInterface $em
     */
    public function __construct(LoggerInterface $logger, SmsManagerInterface $smsManager, EntityManagerInterface $em, $companyPhone)
    {
        $this->logger = $logger;
        $this->smsManager = $smsManager;
        $this->em = $em;
        $this->companyPhone = $companyPhone;
    }

    /**
     * @param TaskStatusChangedEvent $event
     */
    public function onWfTaskStatusChanged(TaskStatusChangedEvent $event)
    {
        $task = $event->getTask();
        $plugin = $task->getPluginUid();
        $newStatus = $event->getNewStatus();

        if ($newStatus === null) {
            $this->logger->warning('New status is null for task ' . $task->getId(), [
                'plugin' => $plugin,
                'old_status' => null === $event->getOldStatus() ? null : ($event->getOldStatus()->getId() . ':' . $event->getOldStatus()->getName()),
                'new_status' => null
            ]);
            return;
        }

        if ($this->notSupported($plugin, $newStatus->getId())) {
            return;
        }

        $ticket = $event->getTask()->getTicket();

        if ($ticket === null) {
            $this->logger->warning('Ticket is null for task ' . $task->getId(), [
                'plugin' => $plugin,
                'old_status' => $event->getOldStatus() ?: $event->getOldStatus()->getId() . ':' . $event->getOldStatus()->getName(),
                'new_status' => $newStatus->getId() . ':' . $newStatus->getName(),
            ]);
            return;
        }

        // TODO: add definition which number is mobile
        $phone = $ticket->getClntTel1();
        $phones = [$phone];
        $text = $this->getMessage($event->getTask());
        $message = new SmsMessage($text, $phones);

        if ($this->smsManager->send($message)) {
            $resultComment = 'Смс отправлено клиенту.';
        } else {
            $resultComment = 'СМС НЕ отправлено клиенту!';
        }

        $comment = (new TaskComment())
            ->setText($resultComment)
            ->setTag('СМС Клиенту - ' . $phone)
            ->setTask($event->getTask());
        $this->em->persist($comment);
        $this->em->flush();
    }

    /**
     * @param Task $task
     * @return string
     */
    private function getMessage(Task $task)
    {
        $ticket = $task->getTicket();
        $partner = $ticket->getPartner();

        if ($task->getPluginUid() == Task::SERVICE
            && $partner !== null) {

            if ($partner->getId() == getcfg('mgts_contr_id')) {
                $sms_text = "Ваш заказ №:" . $task->getId() . " выполнен!";
            } else {
                $sms_text = "Заказ №:" . $task->getId() . " выполнен! 20% от чека вернем, если мастер оставил свои контакты. Подробнее т. " . $this->companyPhone;
            }

            return $sms_text;
        }

        $smsbody = 'Заявка №%d выполнена';
        return sprintf($smsbody, $task->getId());
    }


    /**
     * @param $plugin
     * @param $newStatus
     * @return bool
     */
    private function notSupported($plugin, $newStatus)
    {
        // Required plugins with status "done"
        switch ($plugin) {
            case Task::SERVICE:
                return $newStatus != TaskStatus::STATUS_SERVICE_DONE;

            case Task::CONNECTION:
                return $newStatus != TaskStatus::STATUS_CONNECTION_DONE;

            case Task::ACCIDENT:
                return $newStatus != TaskStatus::STATUS_ACCIDENT_DONE;
        }

        return true;
    }
}
