<?php

namespace WF\Task\EventDispatcher;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use models\TaskComment;
use Psr\Log\LoggerInterface;
use WF\Sms\Model\SmsManagerInterface;
use WF\Sms\SmsMessage;
use WF\Task\Events\TaskAddedToScheduleEvent;

/**
 * Description of NotifyClientOnSkpToScheduleListener
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyClientOnSkpToScheduleListener
{
    /**
     *
     * @var SmsManagerInterface
     */
    private $smsManager;

    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var string
     */
    private $companyPhone;

    public function __construct(LoggerInterface $logger, SmsManagerInterface $smsManager, EntityManagerInterface $entityManager, $companyPhone)
    {
        $this->logger = $logger;
        $this->smsManager = $smsManager;
        $this->companyPhone = $companyPhone;
        $this->em = $entityManager;
    }

    public function onWfTaskAddedToSchedule(TaskAddedToScheduleEvent $event)
    {
        return; // TODO: remove after Notification refac
        if ($event->getTask()->getPluginUid() !== Task::SERVICE) {
            return;
        }

        $ticket = $event->getTask()->getTicket();

        // TODO: add definition which number is mobile
        $phone = $ticket->getClntTel1();
        $phones = [$phone];
        $text = $this->getMessage($event->getTask());
        $message = new SmsMessage($text, $phones);

        if ($this->smsManager->send($message)) {
            $resultComment = 'Смс отправлено клиенту.';
        } else {
            $resultComment = 'СМС НЕ отправлено клиенту!';
        }

        $comment = (new TaskComment())
            ->setText($resultComment)
            ->setTag('СМС Клиенту - ' . $phone)
            ->setTask($event->getTask());
        $this->em->persist($comment);
        $this->em->flush();
    }

    private function getMessage(Task $task)
    {
        $smsbody = 'Заявка №%d назначена на %s, ваш мастер - %s. Справки по т. %s';
        return sprintf($smsbody,
            $task->getId(),
            $task->getSchedule()->getStartDateTime()->format('d.m.Y H:i'),
            $task->getSchedule()->getTechnician()->getFio(),
            $this->companyPhone);
    }
}
