<?php

namespace WF\Task\EventDispatcher;

use models\Task;
use models\TaskStatus;
use WF\Notification\AbstractNotificationSubscriber;
use WF\Slack\Formatter\TicketFormatterSimpleFactory as FF;
use WF\Slack\Notification\SlackNotification;
use WF\Task\Events\TaskAddedToScheduleEvent;
use WF\Task\Events\TaskExecutorsChangedEvent;
use WF\Task\Events\TaskStatusChangedEvent;
use WF\Technician\EventDispatcher\GraphicTimeExpiredEvent;
use WF\Technician\EventDispatcher\MoneyLimitExceededEvent;

/**
 * Description of NotifySkpSubscriber
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyServiceSubscriber extends AbstractNotificationSubscriber
{
    public static function getSubscribedEvents()
    {
        return [
            TaskAddedToScheduleEvent::NAME => [
                'onTaskToScheduleTech'
            ],
            TaskExecutorsChangedEvent::NAME => 'onTaskExecutorsChanged',
            TaskStatusChangedEvent::NAME => [
                'onTaskStatusChangedReportTimeout'
            ],
            MoneyLimitExceededEvent::NAME => [
                ['onMoneyLimitAlert', 10],
                ['onMoneyLimitBlock', 0]
            ],
            GraphicTimeExpiredEvent::NAME => 'onTechScheduleTimeExpired',
        ];

    }

    public function onTaskToScheduleTech(TaskAddedToScheduleEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        $link = $this->getTaskLink($task);
        $recipient = $task->getSchedule()->getTechnician();

        $notification = new SlackNotification();
        $notification->setTo($recipient)
            ->setMessage("Заявка назначена на {$task->getSchedule()->getStartDateTime()->format('d.m.Y H:i')}");

        $this->handle('slack', FF::getTicketFormatter($task, $notification)->setTaskLink($link));
    }

    public function onTaskExecutorsChanged(TaskExecutorsChangedEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        $executors = $event->getAddedExecutors();
        $link = $this->getTaskLink($task);

        $notification = new SlackNotification();
        $notification->setTo($executors)
            ->setMessage('Вам поручена заявка №'.$task->getId());

        $this->handle('slack', FF::getTicketFormatter($task, $notification)->setTaskLink($link));
    }

    public function onTaskStatusChangedReportTimeout(TaskStatusChangedEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        $newStatus = $event->getNewStatus() ? $event->getNewStatus()->getId() : 0;
        if ($newStatus !== TaskStatus::STATUS_SERVICE_TIMEOUT) {
            return;
        }

        if (null === $task->getSchedule()
            || null === $task->getSchedule()->getTechnician())
        {
            $this->logError($task, 'Schedule or technician not defined for task', [
                'class' => __CLASS__,
                'method' => __METHOD__,
            ]);
            return;
        }

        $link = $this->getTaskLink($task);
        $tech = $task->getSchedule()->getTechnician();
        $recipients = [$tech];

        foreach ($tech->getMasterServiceCenters() as $sc) {
            $recipients = array_merge($recipients, $sc->getChiefs()->toArray());
        }

        $notification = (new SlackNotification())
            ->setTo($recipients)
            ->setMessage("Вы не отчитались по этой заявке №{$task->getId()}!")
        ;

        $this->handle('slack', FF::getTicketFormatter($task, $notification)->setTaskLink($link));
    }

    public function onMoneyLimitAlert(MoneyLimitExceededEvent $event)
    {
        if (null !== $event->getLimit2()) {
            return;
        }

        $slackMessage = $this->renderMessage('Task/Notification/serviceMoneyLimitAlert.md.php', [
            'debt' => $event->getDebt(),
        ]);

        $notification = (new SlackNotification())
            ->setTo($event->getUser())
            ->setMessage($slackMessage);

        $this->handle('slack', $notification);
    }

    public function onMoneyLimitBlock(MoneyLimitExceededEvent $event)
    {
        if (null === $event->getLimit2()) {
            return;
        }

        $tech = $event->getUser();
        $recipients = [$tech];

        foreach ($tech->getMasterServiceCenters() as $sc) {
            $recipients = array_merge($recipients, $sc->getChiefs()->toArray());
        }

        $slackMessage = $this->renderMessage('Task/Notification/serviceMoneyLimitBlock.md.php', [
            'debt' => $event->getDebt(),
            'tech' => $tech,
            'limit' => $event->getLimit2(),
        ]);

        $notification = (new SlackNotification())
            ->setTo($event->getUser())
            ->setMessage($slackMessage);

        $this->handle('slack', $notification);
    }

    protected function notSupported(Task $task)
    {
        return $task->getPluginUid() !== Task::SERVICE;
    }
}
