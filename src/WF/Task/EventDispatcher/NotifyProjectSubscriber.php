<?php

namespace WF\Task\EventDispatcher;

use models\Task;
use models\TaskComment;
use models\TaskStatus;
use models\User;
use WF\Notification\AbstractNotificationSubscriber;
use WF\Slack\Formatter\CommentedFormatter;
use WF\Slack\Formatter\FileLinksFormatter;
use WF\Slack\Formatter\TicketFormatterSimpleFactory as FF;
use WF\Slack\Model\SlackManagerInterface;
use WF\Slack\Notification\SlackNotification;
use WF\Task\Events\ProjectDeadlineExpiredEvent;
use WF\Task\Events\ProjectDelayEndEvent;
use WF\Task\Events\TaskCommentedEvent;
use WF\Task\TaskManager;

/**
 * Description of NotifyProjectSubscriber
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyProjectSubscriber extends AbstractNotificationSubscriber
{

    /**
     * @var TaskManager
     */
    private $taskManager;

    /**
     * @var SlackManagerInterface
     */
    private $sm;

    public static function getSubscribedEvents()
    {
        return [
            ProjectDeadlineExpiredEvent::NAME => 'onProjectDeadline',
            ProjectDelayEndEvent::NAME => 'onProjectDelayEnd',
            TaskCommentedEvent::NAME => 'onProjectCommented',
        ];
    }

    public function setTaskManager(TaskManager $taskManager)
    {
        $this->taskManager = $taskManager;

        return $this;
    }

    public function setSlackManager(SlackManagerInterface $sm)
    {
        $this->sm = $sm;
    }

    public function onProjectCommented(TaskCommentedEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        $comment = $event->getComment();
        $author = $comment->getAuthor();

        $users = $this->em->getRepository(User::class)->getTaskAllBoundedUsers($event->getTask(), [$author], true);

        $notification = new SlackNotification();
        $notification->setTo($users)
            ->setMessage('<' . $this->getTaskLink($task, 'viewtask') . '|Новый комментарий к проекту №' . $task->getId() . '>');

        $this->handle('slack',
            FF::getTicketFormatter($task, (
            new FileLinksFormatter(
                (new CommentedFormatter($notification))
                    ->setComment($comment)
                ))->setComment($comment)
            )->setTaskLink($this->getTaskLink($task, 'viewtask'))
        );
    }

    protected function notSupported(Task $task)
    {
        return $task->getPluginUid() !== Task::PROJECT;
    }

    public function onProjectDeadline(ProjectDeadlineExpiredEvent $event)
    {
        $task = $event->getTask();
        $users = $this->userRepo->getTaskExecutors($task, null, true);
        $link = $this->getTaskLink($task, 'viewtask');

        $notification = new SlackNotification();
        $notification->setTo($users)
            ->setMessage('Нарушение сроков выполнения проекта №' . $task->getId());

        $this->handle('slack',
            FF::getTicketFormatter($task, $notification)
                ->setTaskLink($link)
        );

        $htmlMessage = $this->renderMessage(
            'Task/Notification/projectDeadline.html.php',
            [
                'task' => $task,
                'project' => $task->getProject(),
                'author' => $task->getAuthor(),
                'executors' => $users,
                'link' => $link,
            ]
        );
        $notification->setMessage($htmlMessage);
        $this->handle('email', $notification);
    }

    public function onProjectDelayEnd(ProjectDelayEndEvent $event)
    {
        $task = $event->getTask();
        $em = $this->em;

        $comment = (new TaskComment())
            ->setStatus($em->getReference(TaskStatus::class, TaskStatus::STATUS_PROJECT_DELEGATED))
            ->setText('Наступила дата, до которой этот проект был отложен. Автоматическая смена статуса на Поручено.');

        $this->taskManager->addComment($task, $comment);

        $recipients = $this->userRepo->getTaskBoundedUsers($task, null, true);
        $dalayTo = $task->getProject()->getDelayTo();

        $notification = new SlackNotification();
        $notification->setTo($recipients)
            ->setSubject('Конец срока отложеного проекта №' . $task->getId());

        $link = $this->getTaskLink($task, 'viewtask');

        $this->handle('slack',
            FF::getTicketFormatter($task, $notification)
                ->setTaskLink($link)
        );

        $htmlMessage = $this->renderMessage(
            'Task/Notification/projectDelayEnd.html.php',
            [
                'task' => $task,
                'project' => $task->getProject(),
                'author' => $task->getAuthor(),
                'executors' => $this->userRepo->getTaskExecutors($task, null, true),
                'link' => $link,
            ]
        );
        $notification->setMessage($htmlMessage);
        $this->handle('email', $notification);
    }
}
