<?php
/**
 * Artem
 * 09.05.2016 13:26
 */

namespace WF\Task\EventDispatcher\RostelekomMo;


use GuzzleHttp\Client as HttpClient;
use models\ListContr;
use models\Task;
use models\TaskStatus;
use models\Ticket;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WF\Task\Events\TaskStatusChangedEvent;

class ServiceTicketRtkMoSubscriber implements EventSubscriberInterface
{

    private $partner_id = [70]; // rtk mo
    private $task_type = [Task::SERVICE]; // СКП
    private $task_statuses = [
        9, // СКП.Назначена в график
        12, // СКП.Выполнена
        10, // СКП.Отказ клиента
        17, // СКП.Отказ оператора
        18, // СКП.Техотказ
    ];

    private $url;
    private $salt;

    /**
     * @var HttpClient
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public static function getSubscribedEvents()
    {
        return array(
            TaskStatusChangedEvent::NAME => 'onStatusChanged',
        );
    }

    public function __construct($url, $salt, HttpClient $client, LoggerInterface $logger)
    {
        $this->url = $url;
        $this->salt = $salt;
        $this->client = $client;
        $this->logger = $logger;
    }


    public function onStatusChanged(TaskStatusChangedEvent $event)
    {
        if (!($this->isAllowedTask($event->getTask()) && $this->isAllowedTaskStatus($event->getNewStatus()))) {
            $event->setResult(['result' => 'This Task does not need rtk mo integration interacts.']);
            return;
        }

        $hex = hash_hmac('sha256', $event->getTask()->getTicket()->getClnttnum() . $event->getNewStatus()->getId(), $this->salt, true);
        $token = base64_encode($hex);

        $data = array(
            'token' => $token,
            'caseId' => $event->getTask()->getTicket()->getClnttnum(),
            'status' => $event->getNewStatus()->getId(),
            'comment' => trim(strip_tags($event->getComment()->getTag() . ' ' . $event->getComment()->getText()))
        );

        $response = $this->client->request('GET', $this->url, [
            'query' => $data
        ]);

        if ('BTMS TICKET TSTATUS SET : OK' == (string)$response->getBody()) {
            $event->setResult(['result' => 'ok']);
        } else {
            $event->setResult(['result' => (string)$response->getBody()]);
            $this->logger->debug('rtk mo integration onStatusChanged', [
                'event' => json_encode($event, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)
            ]);
        }
    }

    private function isAllowedTask(Task $task)
    {

        switch ($task->getPluginUid()) {
            case Task::GLOBAL_PROBLEM:
                $ticket = $task->getGp();
                break;
            default:
                $ticket = $task->getTicket();
                break;
        }

        return $ticket instanceOf Ticket
        && $ticket->getPartner() instanceOf ListContr
        && in_array($ticket->getPartner()->getId(), $this->partner_id)
        && in_array($task->getPluginUid(), $this->task_type)
        && $ticket->getRtMo()
        ;
    }

    private function isAllowedTaskStatus(TaskStatus $status = null)
    {
        return !empty($status) && in_array($status->getId(), $this->task_statuses);
    }

    /**
     * @param array $partner_id
     */
    public function setPartnerId($partner_id)
    {
        $this->partner_id = $partner_id;
    }

    /**
     * @param array $task_type
     */
    public function setTaskType($task_type)
    {
        $this->task_type = $task_type;
    }

    /**
     * @param array $task_statuses
     */
    public function setTaskStatuses($task_statuses)
    {
        $this->task_statuses = $task_statuses;
    }
}
