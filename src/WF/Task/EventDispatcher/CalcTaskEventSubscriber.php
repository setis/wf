<?php
namespace WF\Task\EventDispatcher;


use classes\Task;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use models\ListContrCalc;
use models\ListSkpTechCalc;
use models\ListTechCalc;
use models\ListTtContrCalc;
use models\ListTtTechCalc;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WF\Tmc\Model\TmcMoveInterface;

class CalcTaskEventSubscriber implements EventSubscriberInterface
{
    static public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->process($args->getObject());
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $this->process($args->getObject());
    }

    private function process($obj){
        if ($obj instanceof ListContrCalc || $obj instanceof ListTtContrCalc) {
            // расчёт контрагента ТТ Пдключение
            $legacyTask = new Task($obj->getId());
            $legacyTask->addComment('Расчёт контрагента обновлён.');

            // set task next status....

            return;
        }
        if ($obj instanceof ListTechCalc || $obj instanceof ListTtTechCalc) {
            // расчёт техника ТТ Пдключение
            $legacyTask = new Task($obj->getId());
            $legacyTask->addComment('Расчёт техника обновлён.');

            // set task next status....

            return;
        }
        if($obj instanceof ListSkpTechCalc) {
            // расчёт СКП
            $legacyTask = new Task($obj->getId());
            $legacyTask->addComment('Расчёт заявки обновлён.');

            // set task next status....

            return;
        }
        if($obj instanceof TmcMoveInterface 
            && null !== $obj->getTask() 
            && (in_array($obj->getState(), TmcMoveInterface::STATE_INSTALLED, TmcMoveInterface::STATE_UNINSTALLED) )
        ) {
            // Движение ТМЦ
            $legacyTask = new Task($obj->getId());
            $legacyTask->addComment('Расчёт ТМЦ обновлён.');
            return;
        }
    }
}