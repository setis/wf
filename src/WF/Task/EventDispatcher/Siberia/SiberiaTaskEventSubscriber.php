<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 05.12.16
 * Time: 16:49
 */

namespace WF\Task\EventDispatcher\Siberia;


use classes\tickets\Ticket;
use Gorserv\Gerp\IntegrationBundle\EventDispatcher\BeforeSoapTaskCreateRequestEvent;
use GuzzleHttp\Client;
use models\Task;
use models\TaskComment;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WF\Task\Events\TaskCommentedEvent;
use WF\Task\Events\TaskCreatedEvent;
use WF\Task\Model\TechnicalTicketInterface;

/**
 * SiberiaTaskEventSubscriber for local_services.yml
 *
 *  wf.task.event_dispatcher.siberia_task_event_subscriber:
 *      class: WF\Task\EventDispatcher\Siberia\SiberiaTaskEventSubscriber
 *      arguments: ['@http_client', 'http://10.100.3.105:3000/createTicket', 'http://10.100.3.105:3000/updateTicket']
 *      tags:
 *          - { name: kernel.event_subscriber }
 *
 * @package WF\Task\EventDispatcher
 */
class SiberiaTaskEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var string
     */
    private $createEndpoint;

    /**
     * @var string
     */
    private $updateEndpoint;

    private $codesToWorks = [
        '621' => 34,
        '402-6' => 25,
        '402-1  Платный выезд (инженер)' => 25,
        '402-2  Платный выезд (Настройка оборудования абонента)' => 25,
        '402-3  Платный выезд Установк, настройка ПО)' => 25,
        '402-4  Платный выезд Установк, настройка АО)' => 25,
        '402-5  Платный выезд (Монтажные работы)' => 25,
        '402-7  Платный выезд (Инсталляция IPTV)' => 25,
        '402-8 Демонтаж оборудования' => 25,
        '402-9  Платный выезд (Потери пакетов)' => 25,
        '403-1 Повторные монтажные работы' => 25,
        '403-2 Повторный выезд инженера' => 14,
        '409ф Бесплатный выезд инженера' => 14,
        '402-10  Платный выезд (Настройки подключения)' => 14,
        '705ф Заявка на подключение' => 14,
        '704 Подключение КТВ' => 14,
        'Z3.1Слаботок, На узле связи требуется Модернизация слаботочных каналов ( т.ч. доп. отверстия для ввода кабеля в узел)' => 1,
        'Z3.2 Нетпорто, На узле связи требуется Модернизация- расширенияпорто, установка оборудования.' => 23,
        'Z3.3 НетI, На узле связи требуется Расширение VLАN (дополнительные IP)' => 53,
        'Z3.4Замо, На узле связи требуется Ремонт (замена) замка на узле (в т.ч. нет ключа)' => 53,
        'Z3.5 Доп.узе, На узле связи требуется Монтаж дополнительногоузл, завершение строительно-монтажных работ.' => 53,
        'Z3.7Активк, Неисправно активное оборудование УС' => 53,
        'Z4.1 Нетсоседе, Нет доступа к слаботочным каналам' => 53,
        'Z4.2 Нет ключей Требуется согласование доступа к узлу (служебный - согласование с УК или ТСЖ)' => 53,
    ];

    /**
     * SiberiaTaskEventSubscriber constructor.
     * @param Client $httpClient
     * @param string $createEndpoint
     * @param string $updateEndpoint
     */
    public function __construct(Client $httpClient, $createEndpoint, $updateEndpoint)
    {
        $this->httpClient = $httpClient;
        $this->createEndpoint = $createEndpoint;
        $this->updateEndpoint = $updateEndpoint;
    }

    public static function getSubscribedEvents()
    {
        return [
            TaskCommentedEvent::NAME => ['onTaskCommented', -512],
            TaskCreatedEvent::NAME => ['onTaskCreated', -512],
            BeforeSoapTaskCreateRequestEvent::NAME => ['onSoapTaskCreateRequest', -512]
        ];
    }

    public function onTaskCommented(TaskCommentedEvent $event)
    {
        $this->doRequest($event->getTask(), $this->updateEndpoint, $event->getComment());
    }

    public function onTaskCreated(TaskCreatedEvent $event)
    {
        $this->doRequest($event->getTask(), $this->createEndpoint);
    }

    private function doRequest(Task $task, $endpoint, TaskComment $comment = null)
    {
        /** @var Ticket $ticket */
        $ticket = $task->getAnyTicket();

        if (!$ticket instanceof TechnicalTicketInterface) {
            return;
        }

        $status = null === $comment ? $task->getStatus() : $comment->getStatus();

        try {
            $this->httpClient->request('POST', $endpoint, [
                'allow_redirects' => false,
                'form_params' => [
                    'city' => $ticket->getDom()->getFullAddress(),
                    'street' => $ticket->getDom()->getFullAddress(),
                    'kladr' => $ticket->getDom()->getKladrcode(),
                    'house' => $ticket->getDom()->getName(),
                    'building' => $ticket->getDom()->getName(),
                    'flat' => $ticket->getKv(),
                    'fullName' => $ticket->getClntFio(),
                    'phone' => $ticket->getClntTel1() . ', ' . $ticket->getClntTel2(),
                    'comment' => $task->getTaskComment(),
                    'id' => $ticket->getClnttnum(),
                    'status' => $status ? $status->getId() : null,
                ]
            ]);
        } catch (\Exception $exception) {

        }
    }

    public function onSoapTaskCreateRequest(BeforeSoapTaskCreateRequestEvent $event)
    {
        $params = $event->getParams();

        if(isset($params['task_type_id']) && 0 === strpos('K-', $params['task_type_id'])) {
            $params['task_type_id'] = $this->convertWorkCodeToWorkType($params['task_type_id']);
        }

        $event->setParams($params);
    }

    public function convertWorkCodeToWorkType($code)
    {
        $code = substr($code, 2);

        if(isset($this->codesToWorks[$code])) {
            return $this->codesToWorks[$code];
        }

        return $code;
    }

}
