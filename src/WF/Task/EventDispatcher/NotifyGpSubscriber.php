<?php

namespace WF\Task\EventDispatcher;

use DateInterval;
use DateTime;
use models\KeyStorageItem;
use models\Task;
use models\User;
use repository\UserRepository;
use WF\Notification\AbstractNotificationSubscriber;
use WF\Slack\Formatter\TicketFormatterSimpleFactory as FF;
use WF\Slack\Notification\SlackNotification;
use WF\Task\Events\GpCloseTimeExpiredEvent;
use WF\Task\Events\GpCreatedEvent;
use WF\Task\Events\GpNoAddressEvent;
use WF\Task\Events\GpNotClosedInTimeEvent;
use WF\Task\Events\GpWillBeClosedEvent;
use WF\Task\Events\TaskAddressAddedEvent;
use WF\Task\Events\TaskExecutorsChangedEvent;
use WF\Task\Events\TaskMovedToRmsEvent;
use WF\Task\Events\TaskNoReactionTimeoutEvent;

/**
 * Description of NotifyGpSubscriber
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyGpSubscriber extends AbstractNotificationSubscriber
{
    public static function getSubscribedEvents()
    {
        return [
            TaskAddressAddedEvent::NAME => 'onAddressAdded',
            GpNoAddressEvent::NAME => 'onGpNoAddress',
            GpCreatedEvent::NAME => 'onGpCreated',
            TaskNoReactionTimeoutEvent::NAME => 'onTaskNoReactionTimeout',
            GpWillBeClosedEvent::NAME => 'onGpWillBeClosed',
            GpNotClosedInTimeEvent::NAME => 'onGpNotClosedInTime',
            GpCloseTimeExpiredEvent::NAME => 'onGpCloseTimeExpired',
            TaskMovedToRmsEvent::NAME => 'onTaskMovedToRms',
            TaskExecutorsChangedEvent::NAME => 'onTaskExecutorsChanged',
        ];
    }

    public function onAddressAdded(TaskAddressAddedEvent $event)
    {
        $task = $event->getTask();

        if ($task->getPluginUid() !== Task::GLOBAL_PROBLEM) {
            return;
        }

        if (!$event->isFirstAddress()) {
            return;
        }

        $address = $event->getAddress();
        $sc = $address->getServiceCenter();
        $gp = $task->getGp();

        if ($gp === null) {
            $this->logger->error('GP is null for task ' . $task->getId());
            return;
        }

        $link = $this->getTaskLink($task);

        if ($sc === null) {
            // 121 - Техническая дирекция/ Отдел координации
            $users = $this->userRepo->findActiveByDepartments([121]);
            $notification = (new SlackNotification())
                ->setTo($users)
                ->setSubject('Новая ГП №' . $task->getId() . ' с нераспознанным СЦ');

            $this->handle('slack',
                FF::getTicketFormatter($task, $notification)->setTaskLink($link)
            );

            $htmlMessage = $this->renderMessage('Task/Notification/gpAddressAddedNoSc.html.php', [
                'task' => $task,
                'gp' => $gp,
                'address' => $address,
                'link' => $link,
                'author' => $this->getTaskAuthor($task),
                'executors' => $this->getTaskExecutors($task),
            ]);
            $notification->setMessage($htmlMessage);
            $this->handle('email', $notification);
        } else {
            $users = $sc->getChiefs()->toArray();

            $notification = (new SlackNotification())
                ->setTo($users)
                ->setSubject('Новая ГП№' . $task->getId());

            $this->handle('slack',
                FF::getTicketFormatter($task, $notification)->setTaskLink($link)
            );

            $htmlMessage = $this->renderMessage('Task/Notification/gpAddressAdded.html.php', [
                'task' => $task,
                'gp' => $gp,
                'address' => $address,
                'link' => $link,
                'author' => $this->getTaskAuthor($task),
                'executors' => $this->getTaskExecutors($task),
            ]);
            $notification->setMessage($htmlMessage);
            $this->handle('email', $notification);
        }
    }

    public function onGpNoAddress(GpNoAddressEvent $event)
    {
        $task = $event->getTask();
        $gp = $task->getGp();

        // СЦ определен - пофиг на адрес
        if (null !== $gp->getServiceCenter()) {
            return;
        }

        $key = 'notify.' . GpNoAddressEvent::NAME . '-' . $task->getId();
        $time = DateInterval::createFromDateString('30 minutes');
        if (!$this->isNotifyTimeExpired($key, $time)) {
            $this->logger->debug('Notify time is not expired yet');
            return;
        }

        $link = $this->getTaskLink($task);
        $author = $task->getAuthor();
        $users = $this->userRepo->findActiveByDepartments([121]);

        $notification = (new SlackNotification())
            ->setTo($users)
            ->setSubject('Новая ГП №' . $task->getId() . ' с нераспознанным адресом');

        $this->handle('slack',
            FF::getTicketFormatter($task, $notification)->setTaskLink($link)
        );


        $emailMessage = $this->renderMessage('Task/Notification/gpNoAddress.html.php', [
            'task' => $task,
            'link' => $link,
            'author' => $this->getTaskAuthor($task),
            'executors' => $this->getTaskExecutors($task),
        ]);
        $notification->setMessage($emailMessage);
        $this->handle('email', $notification);

        $this->saveNotifyTime($key);
    }

    public function onGpCreated(GpCreatedEvent $event)
    {
        $gp = $event->getGp();
        $sc = $gp->getServiceCenter();

        if ($sc === null) {
            return;
        }

        $task = $event->getTask();
        $link = $this->getTaskLink($task);
        //брать начальников СЦ
        $recipient = $sc->getChiefs()->toArray();

        $address = $task->getAddresses()->first();
        $addressText = $address ? $address->getFullAddress() : null;
        $notification = (new SlackNotification())
            ->setTo($recipient)
            ->setSubject('Новая ГП№' . $task->getId());

        $this->handle('slack',
            FF::getTicketFormatter($task, $notification)->setTaskLink($link)
        );


        $emailMessage = $this->renderMessage('Task/Notification/gpCreated.html.php', [
            'task' => $task,
            'link' => $link,
            'addressText' => $addressText,
            'author' => $this->getTaskAuthor($task),
            'executors' => $this->getTaskExecutors($task),
        ]);
        $notification->setMessage($emailMessage);
        $this->handle('email', $notification);

    }

    public function onTaskNoReactionTimeout(TaskNoReactionTimeoutEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        $link = $this->getTaskLink($task);
        $key = 'notify.' . TaskNoReactionTimeoutEvent::NAME . '-' . $event->getTask()->getId();
        $time = DateInterval::createFromDateString('15 minutes');

        if (!$this->isNotifyTimeExpired($key, $time)) {
            $this->logger->debug('Notify time is not expired yet');
            return;
        }

        $sc = $event->getTask()->getGp()->getServiceCenter();

        /* @var $repo UserRepository */
        $repo = $this->em->getRepository(User::class);
        if (null === $sc) {
            // 121 - Техническая дирекция/ Отдел координации
            $recipients = $repo->findActiveByDepartments([121]);
        } else {
            $recipients = $sc->getChiefs()->toArray();
        }

        $recipients = array_merge($recipients, $this->userRepo->findActiveByPositions([86]));

        $msg = sprintf('Заявка ГП №%d поступила %s не взята в работу',
            $event->getTask()->getId(),
            $event->getTask()->getDateReg()->format('d.m.Y H:i'));

        $notification = (new SlackNotification())
            ->setTo($recipients)
            ->setSubject(sprintf('ГП №%d не взята в работу', $task->getId()))
            ->setMessage($msg);

        $this->handle('slack', FF::getTicketFormatter($task, $notification)->setTaskLink($link));

        $emailMessage = $this->renderMessage('Task/Notification/gpTaskNoReaction.html.php', [
            'task' => $task,
            'link' => $link,
            'author' => $this->getTaskAuthor($task),
            'executors' => $this->getTaskExecutors($task),
        ]);
        $notification->setMessage($emailMessage);
        $this->handle('email', $notification);

        $this->saveNotifyTime($key);
    }

    public function onGpWillBeClosed(GpWillBeClosedEvent $event)
    {
        $key = 'notify.' . GpWillBeClosedEvent::NAME . '-' . $event->getTask()->getId();
        if (!$this->isNotifyTimeExpired($key)) {
            $this->logger->debug('Notification already send');
            return;
        }

        $task = $event->getTask();
        $link = $this->getTaskLink($task);
        $gp = $task->getGp();
        $sc = $gp->getServiceCenter();
        $recipients = [];

        if ($sc !== null) {
            $recipients = $sc->getChiefs()->toArray();
        }

        $executors = $this->userRepo->getTaskExecutors($task, null, true);

        $recipients = array_merge($executors, $recipients);

        $notification = (new SlackNotification())
            ->setTo($recipients)
            ->setSubject('Окончание интервала закрытия ГП №' . $task->getId())
            ->setMessage("До планируемого времени закрытия осталось {$event->getTimeout()->i} минут. При невозможности выполнить заявку скорректируйте время закрытия.");

        $this->handle('slack', FF::getTicketFormatter($task, $notification)->setTaskLink($link));

        $this->saveNotifyTime($key);
    }

    public function onGpNotClosedInTime(GpNotClosedInTimeEvent $event)
    {
        $key = 'notify.' . GpNotClosedInTimeEvent::NAME . '-' . $event->getTask()->getId();
        if (!$this->isNotifyTimeExpired($key)) {
            $this->logger->debug('Notification already send');
            return;
        }

        $task = $event->getTask();
        $link = $this->getTaskLink($task);
        $gp = $task->getGp();
        $sc = $gp->getServiceCenter();

        $chiefs = [];
        if ($sc !== null) {
            $chiefs = $sc->getChiefs()->toArray();
        }

        $executors = $this->userRepo->getTaskExecutors($task, $chiefs, true);

        $recipients = array_merge($chiefs, $executors);

        $schedule = $task->getSchedule();
        if (null === $schedule) {
            $this->logger->error('GP ' . $task->getId() . ' without schedule');
        } else {
            $recipients[] = $schedule->getTechnician();
        }

        $notification = (new SlackNotification())
            ->setTo($recipients)
            ->setSubject('Не закрытая вовремя ГП №' . $task->getId());

        $this->handle('slack', FF::getTicketFormatter($task, $notification)->setTaskLink($link));

        $this->saveNotifyTime($key);

    }

    public function onGpCloseTimeExpired(GpCloseTimeExpiredEvent $event)
    {
        $task = $event->getTask();

        $key = 'notify.' . GpCloseTimeExpiredEvent::NAME . '-' . $task->getId();
        $time = DateInterval::createFromDateString('30 minutes');
        if (!$this->isNotifyTimeExpired($key, $time)) {
            $this->logger->debug('Notify time is not expired yet');
            return;
        }

        $gp = $task->getGp();
        $sc = $gp->getServiceCenter();
        $link = $this->getTaskLink($task);
        $recipients = $this->userRepo->findActiveByDepartments([135]);

        if ($sc !== null) {
            $recipients = array_merge($recipients, $sc->getChiefs()->toArray());
        }

        $notification = (new SlackNotification())
            ->setTo($recipients)
            ->setSubject('Окончание интервала закрытия ГП №' . $task->getId());

        $this->handle('slack', FF::getTicketFormatter($task, $notification)->setTaskLink($link));

        $this->saveNotifyTime($key);
    }

    public function onTaskMovedToRms(TaskMovedToRmsEvent $event)
    {
        $task = $event->getTask();
        if ($this->notSupported($task)) {
            return;
        }

        $link = $this->getTaskLink($task);
        $gp = $event->getTask()->getGp();
        $address = $gp->getTask()->getAddresses()->first();
        $addressText = $address ? $address->getFullAddress() : null;

        // 17 - Инженер аварийной службы
        $recipients = $this->userRepo->findActiveByPositions([17]);

        $notification = (new SlackNotification())
            ->setTo($recipients)
            ->setSubject('Передача в РМС ГП №' . $task->getId());

        $this->handle('slack', FF::getTicketFormatter($task, $notification)->setTaskLink($link));
    }

    public function onTaskExecutorsChanged(TaskExecutorsChangedEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        $executors = $event->getAddedExecutors();
        $link = $this->getTaskLink($task);

        $slackMessage = sprintf('Вам поручена заявка <%s|ГП №%d>',
            $link, $task->getId());

        $notification = new SlackNotification();
        $notification->setTo($executors)
            ->setSubject('Новая заявка ГП')
            ->setMessage($slackMessage);

        $this->handle('slack', FF::getTicketFormatter($task, $notification)->setTaskLink($link));
    }

    private function isNotifyTimeExpired($key, DateInterval $time = null)
    {
        /* @var $item KeyStorageItem */
        $item = $this->em->find(KeyStorageItem::class, $key);
        if ($item === null) {
            return true;
        }

        // $time is null - not periodical notification
        if ($time === null) {
            return false;
        }

        $date = new DateTime($item->getJsonValue()['date']);
        $date->add($time);

        return (new DateTime()) >= $date;
    }

    private function saveNotifyTime($key)
    {
        /* @var $item KeyStorageItem */
        $item = $this->em->find(KeyStorageItem::class, $key);
        if ($item === null) {
            $item = (new KeyStorageItem())
                ->setCreatedAt(time())
                ->setKey($key)
                ->setComment('Last notify time from ' . self::class);
            $this->em->persist($item);
        }

        $item->setUpdatedAt(time())
            ->setJsonValue(['date' => date('Y-m-d H:i:s')]);

        $this->em->flush();
    }

    protected function notSupported(Task $task)
    {
        return $task->getPluginUid() !== Task::GLOBAL_PROBLEM;
    }
}
