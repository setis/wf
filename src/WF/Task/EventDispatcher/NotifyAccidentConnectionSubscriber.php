<?php

namespace WF\Task\EventDispatcher;

use Exception;
use models\Task;
use models\TaskComment;
use WF\Notification\AbstractNotificationSubscriber;
use WF\Notification\Model\Notification;
use WF\Slack\Notification\SlackNotification;
use WF\Sms\Model\SmsManagerInterface;
use WF\Sms\SmsMessage;
use WF\Task\Events\TaskAddedToScheduleEvent;
use WF\Task\Events\TaskCreatedEvent;
use WF\Task\Events\TaskQcChangedEvent;
use WF\Task\Events\TaskScheduleTimeSoonExpireEvent;
use WF\Technician\EventDispatcher\GraphicTimeExpiredEvent;

/**
 * Description of NotifyAccidentConnectionSubscriber
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyAccidentConnectionSubscriber extends AbstractNotificationSubscriber
{

    /**
     *
     * @var string
     */
    private $companyPhone;

    /**
     *
     * @var SmsManagerInterface
     */
    private $smsManager;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    private $lkUserNotificationEmails;

    public function setCompanyPhone($companyPhone)
    {
        $this->companyPhone = $companyPhone;
        return $this;
    }

    public function setSmsManager(SmsManagerInterface $smsManager)
    {
        $this->smsManager = $smsManager;
        return $this;
    }

    /**
     * @param \Swift_Mailer $mailer
     */
    public function setMailer(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param mixed $lkUserNotificationEmails
     */
    public function setLkUserNotificationEmails($lkUserNotificationEmails)
    {
        $this->lkUserNotificationEmails = $lkUserNotificationEmails;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            TaskAddedToScheduleEvent::NAME => [
                ['onTaskAddedToScheduleNotifyTech'],
                /*['onTaskAddedToScheduleNotifyClient']*/
            ],
            TaskScheduleTimeSoonExpireEvent::NAME => 'onScheduleTimeSoonExpire',
            TaskQcChangedEvent::NAME => 'onTaskQcChanged',
            GraphicTimeExpiredEvent::NAME => 'onTechScheduleTimeExpired',
            TaskCreatedEvent::NAME => 'onTaskCreated',
        ];
    }

    /**
     * @param TaskScheduleTimeSoonExpireEvent $event
     */
    public function onScheduleTimeSoonExpire(TaskScheduleTimeSoonExpireEvent $event)
    {
        $task = $event->getSchedule()->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        $user = $event->getSchedule()->getTechnician();
        $link = $this->getTaskLink($task);
        $taskTitle = $this->getPluginTitle($task);

        $message = $this->renderMessage('Task/Notification/taskScheduleTimeSoonExpire.md.php',
            [
                'link' => $link,
                'task' => $task,
                'minutes' => $event->getInterval()->i,
            ]);
        $notification = new SlackNotification();
        $notification->setTo($user)
            ->setMessage($message);

        $this->handle($this->chooseHandler($task), $notification);
    }

    /**
     * @param TaskAddedToScheduleEvent $event
     */
    public function onTaskAddedToScheduleNotifyClient(TaskAddedToScheduleEvent $event)
    {
        $task = $event->getTask();
        if ($this->notSupported($task)) {
            return;
        }

        $ticket = $task->getTicket();

        $sched = $event->getTask()->getSchedule();
        $tech = $sched->getTechnician();

        if ($tech->isVirtual()) {
            $real = $tech->getRealUser();
            if (!$real) {
                return;
            }

            $tech = $real;
        }

        $phones = [$ticket->getClntTel1(), $ticket->getClntTel2()];

        $msg = sprintf('Заявка №%d назначена на %s, ваш мастер - %s. Справки по т. %s',
            $task->getId(),
            $sched->getStartDateTime()->format('d.m.Y H:i'),
            $tech->getFio(),
            $this->companyPhone);

        $sms = new SmsMessage($msg, $phones);

        if ($this->smsManager->send($sms)) {
            $resultComment = 'Смс отправлено клиенту.';
        } else {
            $resultComment = 'СМС НЕ отправлено клиенту!';
        }

        $comment = (new TaskComment())
            ->setText($resultComment)
            ->setTask($event->getTask());

        $this->em->persist($comment);
        $this->em->flush();
    }

    /**
     * @param TaskAddedToScheduleEvent $event
     */
    public function onTaskAddedToScheduleNotifyTech(TaskAddedToScheduleEvent $event)
    {
        $task = $event->getTask();
        if ($this->notSupported($task)) {
            return;
        }

        $recipients = [];
        $schedule = $task->getSchedule();
        $tech = $schedule->getTechnician();
        $ticket = $task->getTicket();

        if ($ticket === null) {
            $this->logger->error('Ticket is null for task #' . $task->getId());
            return;
        }

        if ($tech->isVirtual()) {
            $real = $tech->getRealUser();
            if ($real) {
                $recipients[] = $real;
            } else {
                $recipients = $ticket->getServiceCenter()->getChiefs()->toArray();
            }
        } else {
            $recipients[] = $tech;
        }

        $link = $this->getTaskLink($task);

        $message = $this->renderMessage('Task/Notification/taskAddedToSchedule.md.php',
            [
                'link' => $link,
                'task' => $task,
                'addressText' => $this->getTaskAddress($task),
            ]);

        $notification = new SlackNotification();
        $notification->setTo($recipients)
            ->setMessage($message);

        $this->handle($this->chooseHandler($task), $notification);
    }

    /**
     * @TODO Pablo Add to all notifications table with qc questions - answers
     *
     * @param TaskQcChangedEvent $event
     */
    public function onTaskQcChanged(TaskQcChangedEvent $event)
    {
        $task = $event->getTask();
        if ($this->notSupported($task)) {
            return;
        }

        $qc = $task->getQc();

        if (null === $qc) {
            $this->logger->error('QC is null for task ' . $task->getId());
            return;
        }

        if ($task->getQc()->getNewResult() === 100) {
            return;
        }

        $sc = $task->getTicket()->getServiceCenter();
        $tech = $task->getSchedule()->getTechnician();
        $users = $sc->getChiefs()->toArray();
        $link = $this->getTaskLink($task);

        $notification = new SlackNotification();
        $notification->setTo($users)
            ->setSubject('Техник нарушил регламент по заявке №' . $task->getId());

        $slackMessage = $this->renderMessage('Task/Notification/taskQcChanged.md.php',
            [
                'link' => $link,
                'task' => $task,
                'tech' => $tech,
            ]);
        $notification->setMessage($slackMessage);
        $this->handle($this->chooseHandler($task), $notification);

        $htmlMessage = $this->renderMessage('Task/Notification/taskQcChanged.html.php', [
            'task' => $task,
            'link' => $link,
            'executors' => $this->getTaskExecutors($task),
            'author' => $this->getTaskAuthor($task),
            'tech' => $tech,
        ]);

        $notification->setMessage($htmlMessage);
        $this->handle('email', $notification);
    }

    public function onTaskCreated(TaskCreatedEvent $event)
    {
        // if ($USER && adm_users_plugin::isLKUser($USER->getId()) && $task_id && agreements_plugin::requireMailOnNewTicket($agr_id) > 0)
        $task = $event->getTask();
        if ($this->notSupported($task)) {
            return;
        }

        $author = $task->getAuthor();
        // Check author is LK user
        if (null === $author || null === $author->getMasterPartner()) {
            return;
        }

        // Check agreement and send email flag
        $agreement = $task->getTicket()->getAgreement();
        if (null === $agreement || true !== $agreement->getMailonlk()) {
            return;
        }


        $htmlMessage = $this->renderMessage('Task/Notification/taskCreatedByLkUser.html.php', [
            'plugin_title' => $this->getPluginTitle($task),
            'task' => $task,
            'address' => $this->getTaskAddress($task),
            'link' => $this->getTaskLink($task),
            'author' => $this->getTaskAuthor($task),
            'executors' => $this->getTaskExecutors($task),
        ]);

        try {
            $message = (new \Swift_Message())
                ->setTo($this->lkUserNotificationEmails)
                ->setFrom('wf@gorserv.ru')
                ->setReturnPath('gss@local')
                ->setBody($htmlMessage);
            $this->mailer->send($message);
        } catch (Exception $ex) {
            // TODO add logs here
        }
    }

    protected function notSupported(Task $task)
    {
        return $task->getPluginUid() !== Task::CONNECTION
            && $task->getPluginUid() !== Task::ACCIDENT;
    }

    private function getPluginTitle(Task $task)
    {
        switch ($task->getPluginUid()) {
            case Task::CONNECTION:
                return 'Подключение';

            case Task::ACCIDENT:
                return 'ТТ';
        }
    }

    private function chooseHandler(Task $task)
    {
        switch ($task->getPluginUid())
        {
            case Task::ACCIDENT:
                return 'slack_accident';

            case Task::CONNECTION:
                return 'slack_connection';
        }

        throw new Exception('Unsupported task type:' . $task->getPluginUid());
    }

    private function getTaskAddress(Task $task)
    {
        $address = $task->getAddresses()->first() ?: $task->getTicket()->getDom();
        return $address ? $address->getFullAddress() : null;
    }
}
