<?php
/**
 * Artem
 * 07.06.2016 15:07
 */

namespace WF\Users\Model;


interface DispatcherInterface
{
    /**
     * @return boolean
     */
    public function isDispatcher();
}