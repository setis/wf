<?php
/**
 * mail@artemd.ru
 * 07.04.2016
 */

namespace WF\Users\Model;

use models\User;

interface UsersManagerInterface
{

    /**
     * @return User[]
     */
    public function getActiveUsers();

    public function getDepartments();

    public function isTechnician(User $user);
}
