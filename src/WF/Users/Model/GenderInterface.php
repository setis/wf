<?php
/**
 * Artem
 * 29.04.2016 16:24
 */
namespace WF\Users\Model;

interface GenderInterface
{
    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    const GENDERS = array(
        self::GENDER_FEMALE => 'Женщина',
        self::GENDER_MALE => 'Мужчина',
    );

    /**
     * @return string
     */
    public function getGender();

}