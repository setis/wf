<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 26.08.16
 * Time: 18:43
 */

namespace WF\Users\TaskPermission;


use classes\User;
use WF\Task\TaskManager;

class CalcTaskPermissionFactory
{

    /**
     * @var \WF\Task\TaskManager
     */
    private $taskManager;

    /**
     * @var \classes\User
     */
    private $legacyUser;

    /**
     * CalcTaskPermissionFactory constructor.
     *
     * @param \WF\Task\TaskManager $tm
     * @param \classes\User        $legacyUser
     */
    public function __construct( TaskManager $tm, User $legacyUser )
    {
        $this->taskManager = $tm;
        $this->legacyUser = $legacyUser;
    }

    /**
     * @param $action
     *
     * @return \WF\Users\TaskPermission\AbstractTaskPermission
     */
    public function getChecker( $action )
    {
        switch ( $action ) {
            case "canManageTaskPhotos":
                $instance = new CanManageTaskPhotosPermission( $this->taskManager, $this->legacyUser );
                break;
            case "canEditOds":
                $instance = new CanEditOdsPermission( $this->taskManager, $this->legacyUser );
                break;
            case "canEditCalcForm":
                $instance = new CanEditCalcFormPermission( $this->taskManager, $this->legacyUser );
                break;
            case "canDoUninstall":
                $instance = new CanDoUninstallPermission( $this->taskManager, $this->legacyUser );
                break;
            case "canDoInstall":
                $instance = new CanDoInstallPermission( $this->taskManager, $this->legacyUser );
                break;
            case "canCalcTechWorks":
                $instance = new CanCalcTechWorksPermission( $this->taskManager, $this->legacyUser );
                break;
            case "canCalcServiceWorks":
                $instance = new CanCalcServiceWorksPermission( $this->taskManager, $this->legacyUser );
                break;
            case "canCalcPartnerWorks":
                $instance = new CanCalcPartnerWorksPermission( $this->taskManager, $this->legacyUser );
                break;
            default:
                throw new \InvalidArgumentException( "Can't find checker for {$action} action!" );
        }

        return $instance;
    }
}
