<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 26.08.16
 * Time: 17:19
 */

namespace WF\Users\TaskPermission;


use models\User;
use models\Task;

class CanCalcTechWorksPermission extends AbstractTaskPermission
{
    public function can( User $user, Task $task )
    {
        if( Task::GLOBAL_PROBLEM === $task->getPluginUid()) {
            return false;
        }

        $toTaskPermission = $this->getByTaskStatusCalcPermissionStatus( $user, $task );

        $toUserPermission = $this->canCalcTechnicianWagePermission( $user, $task );

        return $toTaskPermission && $toUserPermission;
    }
}
