<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 26.08.16
 * Time: 15:44
 */

namespace WF\Users\TaskPermission;


use models\Task;
use models\User;

class CanCalcServiceWorksPermission extends AbstractTaskPermission
{
    public function can( User $user, Task $task )
    {
        if ( Task::SERVICE !== $task->getPluginUid() ) {
            return false;
        }

        $toTaskPermission = $this->getByTaskStatusCalcPermissionStatus( $user, $task );

        $toUserPermission = $this->getByUserRoleCalcPermission( $user, $task );

        return $toTaskPermission && $toUserPermission;
    }

}
