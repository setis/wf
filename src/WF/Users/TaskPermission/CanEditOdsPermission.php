<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 26.08.16
 * Time: 17:21
 */

namespace WF\Users\TaskPermission;


use models\Task;
use models\User;

class CanEditOdsPermission extends AbstractTaskPermission
{
    public function can( User $user, Task $task )
    {
        $toTaskPermission = $this->getByTaskStatusCalcPermissionStatus( $user, $task );

        $toUserPermission = $this->getByUserRoleCalcPermission( $user, $task );

        return $toTaskPermission && $toUserPermission;
    }
}
