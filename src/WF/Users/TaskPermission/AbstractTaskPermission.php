<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 30.06.16
 * Time: 14:27
 */

namespace WF\Users\TaskPermission;


use models\Task;
use models\TaskStatus;
use models\User;
use WF\Task\Model\TechnicalTicketInterface;
use WF\Task\TaskManager;

abstract class AbstractTaskPermission
{
    /**
     * @var TaskManager
     */
    protected $tm;

    /**
     * @var \classes\User
     */
    private $legacyUser;

    /**
     * AbstractTaskPermission constructor.
     *
     * @param \WF\Task\TaskManager $tm
     * @param \classes\User $legacyUser
     */
    public function __construct(TaskManager $tm, \classes\User $legacyUser)
    {
        $this->tm = $tm;
        $this->legacyUser = $legacyUser;
        $this->canUserEditClosedTasks = $this->legacyUser->checkAccess("calcticketafterdeadline", \classes\User::ACCESS_WRITE);
    }

    /**
     * @param \models\User $user
     * @param \models\Task $task
     *
     * @return mixed
     */
    abstract public function can(User $user, Task $task);

    /**
     * @param \models\User $user
     * @param \models\Task $task
     *
     * @return bool
     */
    protected function getByTaskStatusCalcPermissionStatus(User $user, Task $task)
    {
        $statusPermission = false;

        $serviceCenter = $task->getAnyTicket()->getServiceCenter();

        if (
            $user->isSuperMol()
            || $user->isDispatcher()
            || (
                $task->getAnyTicket() instanceOf TechnicalTicketInterface
                && null !== $serviceCenter
                && $user->isMolOnWarehouse($serviceCenter)
            )
        ) {
            return true;
        }

        if ($this->tm->taskIsClosed($task) && !($this->tm->isTaskClosedInPeriod($task) || $this->canUserEditClosedTasks)) {
            return false;
        }

        switch ($task->getPluginUid()) {
            case Task::CONNECTION:
                /**
                 * statuses in which tech allow do calc
                 */
                $statuses = [TaskStatus::STATUS_CONNECTION_DONE, TaskStatus::STATUS_CONNECTION_REPORT, 99];

                $statusPermission = in_array($task->getStatus()->getId(), $statuses);
                break;
            case Task::ACCIDENT:
                /**
                 * statuses in which tech allow do calc
                 */
                $statuses = [TaskStatus::STATUS_ACCIDENT_DONE, TaskStatus::STATUS_ACCIDENT_REPORT];

                $statusPermission = in_array($task->getStatus()->getId(), $statuses);
                break;
            case Task::SERVICE:
                $statuses = [TaskStatus::STATUS_SERVICE_GFX, TaskStatus::STATUS_SERVICE_DONE, 24, 47];

                $statusPermission = in_array($task->getStatus()->getId(), $statuses);
                break;
            case Task::GLOBAL_PROBLEM:
                $statusPermission = true;
                break;
        }

        return $statusPermission;
    }

    /**
     * @param \models\User $user
     * @param \models\Task $task
     *
     * @return bool
     */
    protected function getByUserRoleCalcPermission(User $user, Task $task)
    {
        if ($user->isDispatcher()) {
            return true;
        }

        $serviceCenter = $task->getAnyTicket()->getServiceCenter();
        if (null !== $serviceCenter && $user->isServiceCenterChief($serviceCenter)) {
            return true;
        }

        if ($user->isTechnician()) {
            $executors = $this->tm->getTaskExecutors($task);
            $slot = $task->getSchedule();
            if (null !== $slot) {
                $executors = array_merge($executors, $slot->getExecutors());
            }

            if (in_array($user, $executors)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param \models\User $user
     * @param \models\Task $task
     *
     * @return bool
     */
    protected function canCalcTechnicianWagePermission(User $user, Task $task)
    {
        return $user->isDispatcher() || ($task->getAnyTicket() instanceof TechnicalTicketInterface && $user->isServiceCenterChief($task->getAnyTicket()->getServiceCenter()));
    }

    /**
     * @param \models\User $user
     * @param \models\Task $task
     *
     * @return bool
     */
    protected function canCalcPartnerPricePermission(User $user, Task $task)
    {
        return $user->isDispatcher() || ($task->getAnyTicket() instanceof TechnicalTicketInterface && $user->isServiceCenterChief($task->getAnyTicket()->getServiceCenter()));
    }

}
