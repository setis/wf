<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 26.08.16
 * Time: 17:18
 */

namespace WF\Users\TaskPermission;


use models\Task;
use models\User;

class CanCalcPartnerWorksPermission extends AbstractTaskPermission
{

    public function can( User $user, Task $task )
    {
        if( Task::GLOBAL_PROBLEM === $task->getPluginUid()) {
            return false;
        }

        $toTaskPermission = $this->getByTaskStatusCalcPermissionStatus( $user, $task );

        $toUserPermission = $this->canCalcPartnerPricePermission( $user, $task );

        return $toTaskPermission && $toUserPermission;
    }
}
