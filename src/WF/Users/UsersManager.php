<?php
/**
 * mail@artemd.ru
 * 07.04.2016
 */

namespace WF\Users;


use classes\UserSession;
use Doctrine\ORM\EntityManager;
use models\User;
use Psr\Log\LoggerInterface;
use WF\Users\Model\UsersManagerInterface;

/**
 * Class UsersManager
 * @package WF\Users
 */
class UsersManager implements UsersManagerInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * UsersManager constructor.
     * @param EntityManager $em
     * @param UserSession $userSession
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     *
     */
    public function getActiveUsers()
    {
        $users = $this->em->getRepository(User::class)
            ->createQueryBuilder('u')
            ->where('u.email is not null')
            ->andWhere('u.active > 0')
            ->getQuery()
            ->getResult();

        return $users;
    }

    /**
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getLoggedUser() {
        return $this->em->find(User::class, \wf::getUserId());
    }

    /**
     *
     */
    public function getDepartments()
    {

    }

    public function isTechnician(User $user)
    {
        // TODO Pablo: refactoring required
        return \classes\User::isTech($user->getId());
    }

    public function checkAccess($resource) {
        /**
         * TODO Pablo security refactoring - voter required
         */
        return \wf::$user->getAccess($resource);
    }
}
