<?php
/**
 * Artem
 * 07.06.2016 16:05
 */

namespace WF\Users\Controller;

use models\Department;
use models\ListSc;
use models\Task;
use models\User;
use Symfony\Component\HttpFoundation\Request;
use WF\HttpKernel\RestController;
use WF\Tmc\Model\TmcWarehouseInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WF\Users\TaskPermission\CalcTaskPermissionFactory;

class ApiController extends RestController
{

    public function getUserRolesAction(Request $request)
    {
        $user = $this->getUser();

        $wh = null;
        if ($request->get('wh_id')) {
            /** @var TmcWarehouseInterface $wh */
            $wh = $this->getEm()->find(ListSc::class, $request->get('wh_id'));
        }

        return $this->renderJsonResponse(
            [
                'isDispatcher' => $user->isDispatcher(),
//            'isDispatcherOnThisWarehouse' => $user->isDispatcherOnThisWarehouse(),

                'isTechnician' => $user->isTechnician(),
                'isTechnicianOnThisWarehouse' => $wh && $user->isTechnicianOnWarehouse($wh),

                'isMol' => $user->isMol(),
                'isMolOnThisWarehouse' => $wh && $user->isMolOnWarehouse($wh),

                'isSuperMol' => $user->isSuperMol(),
                'isSuperMolOnThisWarehouse' => $wh && $user->isSuperMolOnWarehouse($wh),

                'isCoordinator' => $user->isCoordinator(),
                'isCoordinatorOnThisWarehouse' => $wh && $user->isCoordinatorOnWarehouse($wh),

                'isCarrier' => $user->isCarrier(),
//            'isServiceCenterChief' => $user->isServiceCenterMaster(),
            ],
            ['default']
        );
    }

    public function getTaskPermissions(Request $request)
    {
        $user = $this->getUser();
        /** @var Task $task */
        $task = $this->getEm()->find(Task::class, $request->get('task_id'));

        /** @var CalcTaskPermissionFactory $permissionsFactory */
        $permissionsFactory = $this->getContainer()->get('wf.task.calc_permissions_factory');

        $access = [
            'canEditCalcForm' => $permissionsFactory->getChecker('canEditCalcForm')->can($user, $task),
            'canDoInstall' => $permissionsFactory->getChecker('canDoInstall')->can($user, $task),
            'canDoUninstall' => $permissionsFactory->getChecker('canDoUninstall')->can($user, $task),
            'canCalcServiceWorks' => $permissionsFactory->getChecker('canCalcServiceWorks')->can($user, $task),
            'canCalcPartnerWorks' => $permissionsFactory->getChecker('canCalcPartnerWorks')->can($user, $task),
            'canCalcTechWorks' => $permissionsFactory->getChecker('canCalcTechWorks')->can($user, $task),
            'canEditOds' => $permissionsFactory->getChecker('canEditOds')->can($user, $task),
            'canManageTaskPhotos' => $permissionsFactory->getChecker('canManageTaskPhotos')->can($user, $task),
        ];

        $flag = false;
        array_walk(
            $access,
            function ($e) use (&$flag) {
                $flag = $flag || $e;
            }
        );

        $access['canOpenCalcForm'] = $flag;

        return $this->renderJsonResponse($access, ['default']);
    }

    /**
     * Returns technician list by given params
     * wh_id - returns tech by warehouse
     *
     * @param Request $request
     */
    public function getTechsAction(Request $request)
    {
        $whId = $request->get('wh_id');
        if ($whId) {
            /* @var $wh ListSc */
            $wh = $this->getEm()->find(ListSc::class, $whId);

            if ($wh === null) {
                throw new NotFoundHttpException('Can\'t find warehouse with id = ' . $whId);
            }

            $qb = $this->getEm()->createQueryBuilder()
                ->select('tech')
                ->from(User::class, 'tech')
                ->join('tech.warehouses', 'wh')
                ->where('tech.active = true')
                ->andWhere('wh = :wh')
                ->setParameter('wh', $wh)
                ->orderBy('tech.fio');

            return $this->renderJsonResponse($qb->getQuery()->getResult(), ['default']);
        }
    }

    public function getActiveUsersAction(Request $request)
    {
        /** @var User[] $users */
        $users = $this->getEm()->getRepository(User::class)
            ->findBy(['active' => true], ['fio' => 'ASC']);

        return $this->renderJsonResponse($users, ['default', 'User.jobPosition']);
    }

    public function getDepartmentsAction()
    {

        /** @var Department[] $departments */
        $departments = $this->getEm()->getRepository(Department::class)
            ->findBy([]);

        return $this->renderJsonResponse($departments, ['default']);
    }
}
