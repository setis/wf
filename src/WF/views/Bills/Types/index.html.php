<?php
/** @var \Symfony\Component\Templating\PhpEngine $view */
/** @var models\BillType[] $bill_types */
/** @var array $data */

$view->extend('layout_bootstrap.html.php');
/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$slots = $view->get('slots');

/** @var \WF\Core\UrlHelper $urlGenerator */
$urlGenerator = $view->get('url');

$breadcrumbs = new \classes\widgets\twozero\BreadcrumbsWidget();
$breadcrumbs->items = [
    ['url' => '/', 'label' => 'Рабочий стол'],
    ['url' => '/bills', 'label' => 'Счета'],
    ['active' => true, 'label' => 'Статьи расходов'],
];

$slots->set('breadcrumbs', $breadcrumbs->run());
$slots->set('title', 'Статьи расходов');
?>


<?php $slots->start('container'); ?>
<div class="text-right">
    <a href="<?php echo $urlGenerator->generate('bill_types_create'); ?>" class="btn btn-success">Добавить статью расходов</a>
</div>
<hr />

<table class="table table-striped table-responsive">
    <thead>
    <tr>
        <th>ID</th>
        <th>Тэг</th>
        <th>Название</th>
        <th>Бизнес единица</th>
        <th>Остаток на текущий месяц, <i class="fa-rub fa"></i></th>
        <th>Резерв на текущий месяц, <i class="fa-rub fa"></i></th>
        <th>Годовой лимит, <i class="fa-rub fa"></i></th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $item): ?>
        <tr>
            <td><a href="<?php echo $urlGenerator->generate('bill_types_edit', ['id'=>$item['billType']->getId()]); ?>">
                    <?php echo $item['billType']->getId(); ?>
                </a></td>
            <td><?php echo $item['billType']->getTag(); ?></td>
            <td><?php echo $item['billType']->getTitle(); ?></td>
            <td><?php echo $item['billType']->getBusinessUnit()->getTitle(); ?></td>
            <td><?php echo rub($item['monthRest']); ?></td>
            <td><?php echo rub($item['monthReserved']); ?></td>
            <th><?php echo rub($item['yearLimit']); ?></th>
            <td>
                <a href="<?php echo $urlGenerator->generate('bill_types_edit', ['id'=>$item['billType']->getId()]); ?>" class="text-success">
                    <i class="glyphicon glyphicon-edit"></i>
                </a>

                <a href="<?php echo $urlGenerator->generate('bill_types_delete', ['id'=>$item['billType']->getId()]); ?>" class="text-danger" onclick="if(!confirm('Вы уверены?')) return false;">
                    <i class="glyphicon glyphicon-remove"></i>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php $slots->stop(); ?>