<?php
/**
 * @var $view \Symfony\Component\Templating\PhpEngine
 * @var $slots \Symfony\Component\Templating\Helper\SlotsHelper
 */

$slots = $view->get('slots');
$view->extend('layout.html.php');

?>

<?php $slots->start('head_meta') ?>

<?php $slots->stop() ?>

<?php $slots->start('css') ?>
<LINK href="/components/nprogress/nprogress.css" rel="stylesheet" type="text/css">
<LINK href="/components/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">
<LINK href="/components/ng-table/dist/ng-table.min.css" rel="stylesheet" type="text/css">
<LINK href="/components/angular-ui-select/dist/select.min.css" rel="stylesheet"/>
<LINK href="/components/angular-upload/src/directives/btnUpload.min.css" rel="stylesheet"/>
<LINK href="/components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<LINK href="/templates/twozero/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<LINK  href="/templates/twozero/css/style_bs.css" rel="stylesheet" type="text/css"/>
<LINK  href="/templates/twozero/css/lightbox.css" rel="stylesheet" type="text/css"/>
<LINK href="/components/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<LINK href="/assets/legacy/bug_report.css" rel="stylesheet"/>
<!--[if lte IE 6]>
<LINK href="/templates/twozero/css/ie.css" rel="stylesheet" type="text/css" />
<![endif]-->

<style type="text/css">
    @media (min-width: 1200px) and (max-width: 1400px) {
        td.tf-hidden, th.tf-hidden {
            display: none !important;
        }

        td.tf-visible, th.tf-visible {
            display: table-cell !important;
        }
    }

    #content ul {
        list-style-type: none;
    }

    a.button {
        display: inline-block;
        line-height: 100%;
        width: 180px;
        margin: 0.3em;
    }

    button {
        line-height: 2;
    }

    textarea[name=comment] {
        width: 300px;
        height: 100px;
    }

    .nbncheck {

        font-weight: bold;
        font-size: 10pt;
        margin-right: 20px;
        color: #FF0000;
        cursor: pointer;
    }

    #toTop {
        cursor: pointer;
        display: none;
        position: fixed;
        right: 1px !important;
        width: 100px;
        height: 10px;
        float: right !important;
        padding: 10px;
        text-align: center;
        line-height: 12px;
        bottom: 50px;
    }

    #header {
        -webkit-backface-visibility: hidden;
    }

    .btn-group.fixed-btn-group, .btn {
        white-space: nowrap;
        margin-bottom: .3em;
    }

    .btn-group.fixed-btn-group > .btn {
        float: inherit;
        margin-bottom: 0;
    }

    .btn-group.fixed-btn-group > .btn + .btn {
        margin-left: -4px;
    }


</style>
<?php $slots->stop() ?>

<?php $slots->start('js') ?>
<script src="/components/jquery/dist/jquery.min.js" type="text/javascript"></script>
<script src="/components/angular/angular.min.js"></script>
<script src="/components/angular-route/angular-route.min.js"></script>
<script src="/components/angular-i18n/angular-locale_ru-ru.js"></script>
<script src="/components/angular-sanitize/angular-sanitize.js"></script>
<script src="/components/angular-ui-select/dist/select.js"></script>
<script src="/components/ng-table/dist/ng-table.min.js"></script>
<script src="/components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
<script src="/components/angular-confirm-modal/angular-confirm.min.js"></script>
<script src="/components/angular-file-upload/dist/angular-file-upload.min.js"></script>
<script src='/components/angular-upload/angular-upload.min.js'></script>
<script src="/components/angular-elastic-input/dist/angular-elastic-input.min.js"></script>
<script src="/components/ya-map/ya-map-2.1.min.js"></script>
<script src="/components/select2/dist/js/select2.min.js"></script>
<script src="/components/select2/dist/js/i18n/ru.js"></script>
<script src="/components/lodash/dist/lodash.min.js"></script>
<script src="/components/ng-table-to-csv/dist/ng-table-to-csv.js"></script>
<script src="/components/moment/min/moment-with-locales.min.js"></script>

<script src="/assets/wf/angular-select-modal.js"></script>
<script src="/assets/wf/wf-ng-enter.js"></script>
<script src="/assets/legacy/bug_report.js"></script>

<script src="/legacy_js/bootstrap.min.js" type="text/javascript"></script>
<script src="/legacy_js/default.js" type="text/javascript"></script>
<script src="/components/nprogress/nprogress.js" type="text/javascript"></script>
<script src="/legacy_js/selectors.js" type="text/javascript" ></script>
<script src="/legacy_js/obzvon.js" type="text/javascript" ></script>
<script src="/legacy_js/lightbox.js" type="text/javascript" ></script>

<script type="text/javascript">
    var cCnt = 0;
    var drag = false;
    var offset = 0;
    $(document).ready(function () {
        $("#content").css("padding-top", $(".navbar-fixed-top").height() + 10);

        $(document).on("keydown", "input[name='object_id']", function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 65, 86, 90, 110, 118]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on("mouseup", function () {
            drag = false;
            $("body").removeClass('noselect');
            $(".rightbar").css("background-color", '#FFFFFF');
        });


        $("#movepanel").mousedown(function (e) {
            $("body").addClass('noselect');
            drag = true;
            offset = Math.abs(e.pageY - $(".rightbar").offset().top - $(document).scrollTop());
            //alert($(document).scrollTop() + " " + e.pageY + " " + offset);
            $(".rightbar").css("background-color", '#FFF700');

        });

        $(document).on("click", "#openobject", function () {
            $("input[name=object_id]").val('');
            $("#openobject_dlg").modal("show");
        });

        $("#movepanel").on("mouseup", function () {
            drag = false;
            $("body").removeClass('noselect');
            var obj = $(".rightbar");
            setTimeout(function () {
                if (obj.offset().top > $(window).height()) {
                    obj.css("top", $(window).height() - (obj.height() + 40));
                } else {
                    if (obj.offset().top < 0) {
                        obj.css("top", 50);
                    }
                }
            }, 10);
        });


        $(document).on("mousemove", function (e) {
            if (drag) {
                $(document).off("click", "#openobject");
                if ((e.pageY - offset ) > $(window).height()) {
                    $(".rightbar").css("top", $(window).height() - $(".rightbar").height() - 7);
                } else {
                    if (e.pageY < 0) {
                        $(".rightbar").css("top", 50);
                    } else {
                        $(".rightbar").css("top", e.pageY - (offset));
                    }
                }

            }
        });

        $(document).on("click", "#openobject", function () {
            if (!drag) {
                $("input[name=object_id]").val('');
                $("#openobject_dlg").modal("show");
            }
        });

        $("#openobject_dlg").on("shown.bs.modal", function () {
            $("input[name=object_id]").focus();
        });

        //$("#content").removeClass("floats-container");
        $("#content").addClass("container-fluid");
        $('nav').on('hidden.bs.collapse', function () {
            onResize();

        });

        $(".gsslogo").click(function () {
            cCnt = cCnt + 1;
            if (cCnt == 5) {
                if ($("#loginat").length) {
                    $("#loginat").modal('show');
                }
                cCnt = 0;
            }
        });


    });

    var onResize = function () {
        $("#content").css("padding-top", $(".navbar-fixed-top").height() + 10);
    };

    $(window).resize(onResize);
    $(function () {
        onResize();
    })
</script>
<?php $slots->stop() ?>

<?= (new \classes\widgets\twozero\MainMenuWidget())->run() ?>
<?= (new \classes\widgets\twozero\RightBarWidget())->run() ?>

<div id="content" class="<?php $slots->output('container_class', 'container') ?>">
    <?php if ($view->get('param')->has('maintenance') && $message = $view->get('param')->get('maintenance')): ?>
        <div class="alert alert-danger text-center" role="alert"><h2 style="margin: 0px;"><?php echo $message === true ? "WF на обслуживании." : $message; ?></h2></div>
    <?php endif; ?>

    <?php if($view->get('alerts')->hasAlerts()): ?>
        <?php foreach ($view->get('alerts')->getAlerts() as $level => $message): ?>
        <div class="alert alert-<?php echo $level;?> text-center" role="alert"><h3 style="margin: 0px;"><?php echo $message; ?></h3></div>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php $slots->output('breadcrumbs'); ?>
    <h1><?php $slots->output('h1', $slots->get('title')) ?></h1>
    <?php $slots->output('container') ?>
</div>

<?= (new \classes\widgets\twozero\ImpersonateWidget())->run() ?>
<?= (new \classes\widgets\twozero\FooterWidget())->run() ?>
