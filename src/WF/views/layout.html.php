<?php
/**
 * @var \Symfony\Component\Templating\PhpEngine $view
 * @var \Symfony\Component\Templating\Helper\SlotsHelper $slots
 */

$slots = $view->get('slots');

?>
<!doctype html>
<html lang="ru">
<head>
    <?php $slots->output('head_meta') ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, maximum-scale=1.0, height=device-height, user-scalable=no"/>
    <meta name="MobileOptimized" content="320"/>
    <meta name="HandheldFriendly" content="true"/>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <title><?php $slots->output('title', 'Горсвязь') ?></title>
    <?php $slots->output('css') ?>
</head>
<body>
<?php $slots->output('_content') ?>
<?php $slots->output('js') ?>
<?php $slots->output('page_js') ?>

<?php $slots->output('footer'); ?>
</body>
</html>
