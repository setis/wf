<?php
/**  @var \Symfony\Component\Templating\PhpEngine $view */
$view->extend('layout_bootstrap.html.php');
/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$slots = $view->get('slots');

$slots->set('title', 'Slack - пользователи');
?>

<?php $slots->start('breadcrumbs'); ?>
<?php
$widget = new \classes\widgets\twozero\BreadcrumbsWidget();
$widget->items = [
    ['url' => '/', 'label' => 'Рабочий стол'],
    ['url' => '/adm_interface', 'label' => 'Настройки'],
    ['active' => true, 'label' => 'Пользователи Slack'],
];
echo $widget->run();
?>
<?php $slots->stop(); ?>

<?php $slots->start('container'); ?>
    <div ng-app="slackManagement" class="ngapp">
        <div ng-controller="slackController as demo">
            <script type="text/ng-template" id="sendMessageModal.html">
                <div class="modal-header">
                    <h3 class="modal-title">Отправить сообщение {{data.channel_name}}</h3>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" style="min-width: 100%" ng-model="message" ng-enter="ok()"></textarea>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning" type="button" ng-click="cancel()">Отмена</button>
                    <button class="btn btn-success" type="button" ng-click="ok()">Отправить</button>
                </div>
            </script>
            <script type="text/ng-template" id="addUserToChannel.html">
                <div class="modal-header">
                    <h3 class="modal-title">Добавить пользователя {{data.user_name}} в {{data.channel_type}}</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <label for="exampleInputEmail1">Выберите {{data.channel_type}}</label>
                            <select ng-model="data.channel_selected" class="form-control">
                                <option ng-repeat="option in data.channels" value="{{option.id}}">{{option.name}}</option>
                            </select>
                        </div>
                        <div class="col-sm-2 text-center">
                            Или
                        </div>
                        <div class="col-sm-5">
                            <label for="exampleInputEmail1">Добавьте {{data.channel_type}}</label>
                            <input ng-model="data.channel_new" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning" type="button" ng-click="cancel()">Отмена</button>
                    <button class="btn btn-success" type="button" ng-click="ok()">Добавить</button>
                </div>
            </script>
            <uib-alert ng-repeat="alert in data.alerts" type="{{alert.type}}" dismiss-on-timeout='3000' close="demo.closeAlert($index)">{{alert.msg}}</uib-alert>
            <table ng-table-dynamic="demo.tableParams with demo.cols" class="table table-condensed table-bordered table-striped table-responsive">
                <tr ng-repeat="row in $data track by row.fio">
                    <td ng-repeat="col in $columns" demo-bind-compiled-html="col.getValue(this, row)"></td>
                </tr>
            </table>
        </div>
    </div>
<?php $slots->stop(); ?>

<?php $slots->start('page_js'); ?>

<script type="text/javascript" src="/assets/wf/app.js"></script>
<script type="text/javascript" src="/assets/wf/slack/controllers.js"></script>
<script type="text/javascript" src="/assets/wf/slack/api.js"></script>
<?php $slots->stop(); ?>
