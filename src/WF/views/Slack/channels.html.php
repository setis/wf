<?php
/**
 * @var \Symfony\Component\Templating\PhpEngine $view
 */

$view->extend('layout_bootstrap.html.php');
$view['slots']->set('title', 'Slack - публичные каналы');
?>

<?php $view['slots']->start('breadcrumbs'); ?>
<?php
$widget = new \classes\widgets\twozero\BreadcrumbsWidget();
$widget->items = [
    ['url' => '/', 'label' => 'Рабочий стол'],
    ['url' => '/adm_interface', 'label' => 'Настройки'],
    ['active' => true, 'label' => 'Каналы Slack'],
];
echo $widget->run();
?>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('container'); ?>
<div ng-app="myApp">
    <div ng-controller="demoController as demo">
        <script type="text/ng-template" id="sendMessageModal.html">
            <div class="modal-header">
                <h3 class="modal-title">Отправить сообщение {{data.channel_name}}</h3>
            </div>
            <div class="modal-body">
                <textarea class="form-control" style="min-width: 100%" ng-model="message" ng-enter="alert('123')"></textarea>
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning" type="button" ng-click="cancel()">Отмена</button>
                <button class="btn btn-success" type="button" ng-click="ok()">Отправить</button>
            </div>
        </script>
        <div class="brn-group">
            <button class="btn btn-default" ng-if="demo.isEditing" ng-click="demo.cancelChanges()">
                <span class="glyphicon glyphicon-remove"></span>
            </button>
            <button class="btn btn-primary" ng-if="!demo.isEditing" ng-click="demo.isEditing = true">
                <span class="glyphicon glyphicon-pencil"></span>
            </button>
            <button class="btn btn-primary" ng-if="demo.isEditing" ng-disabled="!demo.hasChanges() || demo.tableTracker.$invalid" ng-click="demo.saveChanges()">
                <span class="glyphicon glyphicon-ok"></span>
            </button>
            <button class="btn btn-default" ng-click="demo.add()">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
        </div>
        <table ng-table="demo.tableParams" class="table table-bordered table-hover table-condensed editable-table" ng-form="demo.tableForm" disable-filter="demo.isAdding"
               demo-tracked-table="demo.tableTracker">
            <colgroup>
                <col width="10%"/>
                <col width="20%"/>
                <col width="60%"/>
                <col width="5%"/>
                <col width="5%"/>
            </colgroup>
            <tr ng-repeat="row in $data" ng-form="rowForm" demo-tracked-table-row="row">
                <td title="'ID'" filter="{id: 'text'}" sortable="'id'" ng-switch="demo.isEditing" ng-class="name.$dirty ? 'bg-warning' : ''" ng-form="id"
                    demo-tracked-table-cell>
                    <span ng-switch-default class="editable-text">{{row.id}}</span>
                    <div class="controls" ng-class="id.$invalid && id.$dirty ? 'has-error' : ''" ng-switch-when="true">
                        <input type="text" name="id" ng-model="row.id" ng-disabled="demo.isEditing" class="editable-input form-control input-sm" required/>
                    </div>
                </td>
                <td title="'Название'" filter="{name: 'text'}" sortable="'name'" ng-switch="demo.isEditing" ng-class="age.$dirty ? 'bg-warning' : ''" ng-form="name"
                    demo-tracked-table-cell>
                    <span ng-switch-default class="editable-text">#{{row.name}}</span>
                    <div class="controls" ng-class="name.$invalid && name.$dirty ? 'has-error' : ''" ng-switch-when="true">
                        <input type="text" name="name" ng-model="row.name" class="editable-input form-control input-sm" required/>
                    </div>
                </td>
                <td title="'Пользователи'">
                    <span>TODO Artem implement this...</span>
                </td>
                <td title="'Отправить сообщение'">
                    <button class="btn btn-success btn-sm" ng-click="sendMessage(row)" ng-disabled="demo.isEditing"><span
                            class="glyphicon glyphicon-envelope"></span> #{{row.name}}</button>
                </td>
                <td>
                    <button class="btn btn-danger btn-sm" ng-click="demo.del(row)" ng-disabled="!demo.isEditing"><span class="glyphicon glyphicon-trash"></span></button>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('page_js') ?>
<script type="text/javascript">
    angular
        .module("myApp", ["ngTable", "ui.bootstrap"])
        .config(function ($httpProvider) {
            // https://habrahabr.ru/post/181009/
            // Используем x-www-form-urlencoded Content-Type
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

            // Переопределяем дефолтный transformRequest в $http-сервисе
            $httpProvider.defaults.transformRequest = [function (data) {
                /**
                 * рабочая лошадка; преобразует объект в x-www-form-urlencoded строку.
                 * @param {Object} obj
                 * @return {String}
                 */
                var param = function (obj) {
                    var query = '';
                    var name, value, fullSubName, subValue, innerObj, i;

                    for (name in obj) {
                        value = obj[name];

                        if (value instanceof Array) {
                            for (i = 0; i < value.length; ++i) {
                                subValue = value[i];
                                fullSubName = name + '[' + i + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        }
                        else if (value instanceof Object) {
                            for (var subName in value) {
                                subValue = value[subName];
                                fullSubName = name + '[' + subName + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        }
                        else if (value !== undefined && value !== null) {
                            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                        }
                    }

                    return query.length ? query.substr(0, query.length - 1) : query;
                };

                return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
            }];

        })
        .factory('slackApi', ['$http', function ($http, demoList) {
            return {
                inviteUser: function (row) {
                    return $http.post("/api/slack/invite.json", {email: row.email});
                },
                usersList: function () {
                    return $http.get("/api/slack/users.json");
                },
                sendMessage: function (recipientId, message) {
                    return $http.post("/api/slack/message.json", {channel: recipientId, text: message});
                },
                addChannel: function (channelId, userId) {
                    return $http.post("/api/slack/add-to-channel.json", {user: userId, channel: channelId});
                },
                removeFromGroup: function (channelId, userId) {
                    return $http.post("/api/slack/remove-from-channel.json", {user: userId, channel: channelId});
                },
                getChannels: function (type) {
                    return $http.post("/api/slack/channels.json", {type: type});
                },
                saveChannels: function(data, type) {
                    return $http.post("/api/slack/save_channels.json", {data: data});
                }
            };
        }])
        .factory('slackModals', ['$uibModal', '$log', function ($uibModal, $log) {
            return {
                sendMessageModal: {
                    open: function ($scope) {
                        var self = this;

                        var modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: 'sendMessageModal.html',
                            controller: 'modalInstanceController',
                            scope: $scope
                        });

                        return modalInstance.result;
                    }
                },
                addUserToChannel: {
                    open: function ($scope) {
                        var self = this;

                        var modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: 'addUserToChannel.html',
                            controller: 'modalInstanceController',
                            scope: $scope
                        });

                        return modalInstance.result;
                    }
                }
            }
        }])
        .controller("demoController", ['NgTableParams', 'slackModals', 'slackApi', demoController])
        .controller('modalInstanceController', ['$scope', '$uibModalInstance', modalInstanceController]);

    function modalInstanceController($scope, $uibModalInstance) {
        console.log($scope);

        $scope.ok = function () {
            $uibModalInstance.close($scope);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    function demoController(NgTableParams, slackModals, slackApi) {

        var self = this;

        var originalData = [];

        slackApi.getChannels('channel').then(function (result) {
            originalData = angular.copy(result.data);

            self.tableParams = new NgTableParams({}, {
                dataset: result.data
            });
        });

        self.deleteCount = 0;

        self.add = add;
        self.cancelChanges = cancelChanges;
        self.del = del;
        self.hasChanges = hasChanges;
        self.saveChanges = saveChanges;

        //////////

        function sendMessage(row){
            $scope.data.user_name = row.name;
            slackModals.sendMessageModal.open().then(function ($scope) {
                slackApi.sendMessage(row.id, $scope.message).then(function (result) {
                    console.log(result);
                })
            });
        }

        function add() {
            self.isEditing = true;
            self.isAdding = true;
            self.tableParams.settings().dataset.unshift({
                id: "",
                name: null
            });
            // we need to ensure the user sees the new row we've just added.
            // it seems a poor but reliable choice to remove sorting and move them to the first page
            // where we know that our new item was added to
            self.tableParams.sorting({});
            self.tableParams.page(1);
            self.tableParams.reload();
        }

        function cancelChanges() {
            resetTableStatus();
            var currentPage = self.tableParams.page();
            self.tableParams.settings({
                dataset: angular.copy(originalData)
            });
            // keep the user on the current page when we can
            if (!self.isAdding) {
                self.tableParams.page(currentPage);
            }
        }

        function del(row) {
            _.remove(self.tableParams.settings().dataset, function(item) {
                return row === item;
            });
            self.deleteCount++;
            self.tableTracker.untrack(row);
            self.tableParams.reload().then(function(data) {
                if (data.length === 0 && self.tableParams.total() > 0) {
                    self.tableParams.page(self.tableParams.page() - 1);
                    self.tableParams.reload();
                }
            });
        }

        function hasChanges() {
            return self.tableForm.$dirty || self.deleteCount > 0
        }

        function resetTableStatus() {
            self.isEditing = false;
            self.isAdding = false;
            self.deleteCount = 0;
            self.tableTracker.reset();
            self.tableForm.$setPristine();
        }

        function saveChanges() {
            resetTableStatus();
            var currentPage = self.tableParams.page();
            originalData = angular.copy(self.tableParams.settings().dataset);

            slackApi.saveChannels(originalData, 'channel').then(function (result) {
                console.log(result);
            });
        }
    }

    /**********
     The following directives are necessary in order to track dirty state and validity of the rows
     in the table as the user pages within the grid
     ------------------------
     */

    (function () {
        angular.module("myApp").directive("demoTrackedTable", demoTrackedTable);

        demoTrackedTable.$inject = [];

        function demoTrackedTable() {
            return {
                restrict: "A",
                priority: -1,
                require: "ngForm",
                controller: demoTrackedTableController
            };
        }

        demoTrackedTableController.$inject = ["$scope", "$parse", "$attrs", "$element"];

        function demoTrackedTableController($scope, $parse, $attrs, $element) {
            var self = this;
            var tableForm = $element.controller("form");
            var dirtyCellsByRow = [];
            var invalidCellsByRow = [];

            init();

            ////////

            function init() {
                var setter = $parse($attrs.demoTrackedTable).assign;
                setter($scope, self);
                $scope.$on("$destroy", function () {
                    setter(null);
                });

                self.reset = reset;
                self.isCellDirty = isCellDirty;
                self.setCellDirty = setCellDirty;
                self.setCellInvalid = setCellInvalid;
                self.untrack = untrack;
            }

            function getCellsForRow(row, cellsByRow) {
                return _.find(cellsByRow, function (entry) {
                    return entry.row === row;
                })
            }

            function isCellDirty(row, cell) {
                var rowCells = getCellsForRow(row, dirtyCellsByRow);
                return rowCells && rowCells.cells.indexOf(cell) !== -1;
            }

            function reset() {
                dirtyCellsByRow = [];
                invalidCellsByRow = [];
                setInvalid(false);
            }

            function setCellDirty(row, cell, isDirty) {
                setCellStatus(row, cell, isDirty, dirtyCellsByRow);
            }

            function setCellInvalid(row, cell, isInvalid) {
                setCellStatus(row, cell, isInvalid, invalidCellsByRow);
                setInvalid(invalidCellsByRow.length > 0);
            }

            function setCellStatus(row, cell, value, cellsByRow) {
                var rowCells = getCellsForRow(row, cellsByRow);
                if (!rowCells && !value) {
                    return;
                }

                if (value) {
                    if (!rowCells) {
                        rowCells = {
                            row: row,
                            cells: []
                        };
                        cellsByRow.push(rowCells);
                    }
                    if (rowCells.cells.indexOf(cell) === -1) {
                        rowCells.cells.push(cell);
                    }
                } else {
                    _.remove(rowCells.cells, function (item) {
                        return cell === item;
                    });
                    if (rowCells.cells.length === 0) {
                        _.remove(cellsByRow, function (item) {
                            return rowCells === item;
                        });
                    }
                }
            }

            function setInvalid(isInvalid) {
                self.$invalid = isInvalid;
                self.$valid = !isInvalid;
            }

            function untrack(row) {
                _.remove(invalidCellsByRow, function (item) {
                    return item.row === row;
                });
                _.remove(dirtyCellsByRow, function (item) {
                    return item.row === row;
                });
                setInvalid(invalidCellsByRow.length > 0);
            }
        }
    })();

    (function () {
        angular.module("myApp").directive("demoTrackedTableRow", demoTrackedTableRow);

        demoTrackedTableRow.$inject = [];

        function demoTrackedTableRow() {
            return {
                restrict: "A",
                priority: -1,
                require: ["^demoTrackedTable", "ngForm"],
                controller: demoTrackedTableRowController
            };
        }

        demoTrackedTableRowController.$inject = ["$attrs", "$element", "$parse", "$scope"];

        function demoTrackedTableRowController($attrs, $element, $parse, $scope) {
            var self = this;
            var row = $parse($attrs.demoTrackedTableRow)($scope);
            var rowFormCtrl = $element.controller("form");
            var trackedTableCtrl = $element.controller("demoTrackedTable");

            self.isCellDirty = isCellDirty;
            self.setCellDirty = setCellDirty;
            self.setCellInvalid = setCellInvalid;

            function isCellDirty(cell) {
                return trackedTableCtrl.isCellDirty(row, cell);
            }

            function setCellDirty(cell, isDirty) {
                trackedTableCtrl.setCellDirty(row, cell, isDirty)
            }

            function setCellInvalid(cell, isInvalid) {
                trackedTableCtrl.setCellInvalid(row, cell, isInvalid)
            }
        }
    })();

    (function () {
        angular.module("myApp").directive("demoTrackedTableCell", demoTrackedTableCell);

        demoTrackedTableCell.$inject = [];

        function demoTrackedTableCell() {
            return {
                restrict: "A",
                priority: -1,
                scope: true,
                require: ["^demoTrackedTableRow", "ngForm"],
                controller: demoTrackedTableCellController
            };
        }

        demoTrackedTableCellController.$inject = ["$attrs", "$element", "$scope"];

        function demoTrackedTableCellController($attrs, $element, $scope) {
            var self = this;
            var cellFormCtrl = $element.controller("form");
            var cellName = cellFormCtrl.$name;
            var trackedTableRowCtrl = $element.controller("demoTrackedTableRow");

            if (trackedTableRowCtrl.isCellDirty(cellName)) {
                cellFormCtrl.$setDirty();
            } else {
                cellFormCtrl.$setPristine();
            }
            // note: we don't have to force setting validaty as angular will run validations
            // when we page back to a row that contains invalid data

            $scope.$watch(function () {
                return cellFormCtrl.$dirty;
            }, function (newValue, oldValue) {
                if (newValue === oldValue) return;

                trackedTableRowCtrl.setCellDirty(cellName, newValue);
            });

            $scope.$watch(function () {
                return cellFormCtrl.$invalid;
            }, function (newValue, oldValue) {
                if (newValue === oldValue) return;

                trackedTableRowCtrl.setCellInvalid(cellName, newValue);
            });
        }
    })();


</script>
<?php $view['slots']->stop(); ?>
