<?php
/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
/* @var $minutes int */
$view->extend('Task/Notification/taskSlackLayout.md.php');
$slots = $view->get('slots');
?>
<?php $slots->start('content') ?>
> До заявки осталось <?= $minutes ?> минут, при переносе свяжитесь с диспетчером
<?php $slots->stop() ?>

