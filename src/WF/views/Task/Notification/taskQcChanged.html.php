<?php

/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
/* @var $tech \models\User */
$view->extend('Task/Notification/taskEmailLayout.html.php');
$slots = $view->get('slots');
?>

<?php $slots->set('head', 'Техник ' . $tech->getFio() . ' нарушил регламент по заявке №' . $task->getId()); ?>

<?php $slots->start('main_content') ?>
<p>Техник <?= $tech->getFio() ?> нарушил регламент по заявке</p>
<table>
    <tr>
        <td>Вопрос</td>
        <td>Оценка</td>
        <td>Коментарий</td>
    </tr>
<?php foreach ($task->getQcResults() as $qcResult): ?>
    <?php $result = ''; ?>
    <?php
        if ($qcResult->getQuestion()->getYesNo() === 1) {
            $result = $qcResult->getResult() === 2 ? 'Да' : 'Нет';
        }
        elseif ($qcResult->getQuestion()->getMaybeYesNo() === 1) {
            $result = $qcResult->getResult() === 5 ? 'Да'
                : ($qcResult->getResult() === 3 ? 'не уверен' : 'Нет');
        }
        elseif ($qcResult->getQuestion()->getMaxCount() > 0) {
            $result = $qcResult->getMark();
        }
    ?>
    <tr>
        <td><?= $qcResult->getQuestion()->getText() ?></td>
        <td><?= $result ?></td>
        <td><?= $qcResult->getComment() ?></td>
    </tr>
<?php endforeach; ?>
</table>
<?php $slots->stop() ?>