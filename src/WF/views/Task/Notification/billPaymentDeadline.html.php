<?php

/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
$view->extend('Task/Notification/billLayout.html.php');
$slots = $view->get('slots');
?>

<?php $slots->set('head', 'Наступила дата платежа по счету №' . $task->getId()); ?>

<?php $slots->start('footer_content') ?>
<span style="color:red;font-weight:bold;font-size:14px;">Наступила дата  платежа по счету.</span>
<?php $slots->stop() ?>
