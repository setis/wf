<?php
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
/* @var $link string */
/* @var $executors \models\User[] */
$slots = $view->get('slots');
$status = $task->getStatus();
$taskTypeTitle = classes\Plugin::getNameStatic($task->getPluginUid());
?>
*<<?= $link ?>|<?= $taskTypeTitle?> №<?= $task->getId() ?>> <?= $task->getTaskTitle() ?>*

<?php $slots->output('content') ?>