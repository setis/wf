<?php
/* @var $link string */
/* @var $task models\Task */
/* @var $addressText string */
$gp = $task->getGp();
$view->extend('Task/Notification/taskEmailLayout.html.php');
$slots = $view->get('slots');
?>
<?php $slots->start('main_content') ?>
<p>Новая ГП</p>
<div><?= $gp->getBody() ?></div>
<?php $slots->stop() ?>