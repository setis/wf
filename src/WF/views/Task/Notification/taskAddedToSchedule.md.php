<?php
/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
/* @var $addressText string */
$view->extend('Task/Notification/taskSlackLayout.md.php');
$slots = $view->get('slots');
$schedule = $task->getSchedule();
?>
<?php $slots->start('content') ?>
> Заявка назначена на <?= $schedule->getStartDateTime()->format('d.m.Y H:i') ?><?php if ($addressText): ?> по адресу _<?= $addressText ?>_<?php endif; ?>.
<?php $slots->stop() ?>