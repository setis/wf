<?php

/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
/* @var $added \models\User[] */
/* @var $removed \models\User[] */
$view->extend('Task/Notification/taskEmailLayout.html.php');
$slots = $view->get('slots');
?>

<?php $slots->set('head', 'Изменение исполнителей в счете №' . $task->getId()); ?>

<?php $slots->start('main_content') ?>
<?php if (count($added)): ?><p>Добавлены: <?= implode(', ', $added) ?></p><?php endif; ?>
<?php if (count($removed)): ?><p>Удалены: <?= implode(', ', $removed) ?></p><?php endif; ?>
<?php $slots->stop() ?>

