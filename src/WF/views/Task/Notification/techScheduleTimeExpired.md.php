<?php
/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
/* @var $tech \models\User */
$view->extend('Task/Notification/taskSlackLayout.md.php');
$slots = $view->get('slots');
$schedule = $task->getSchedule();
?>
<?php $slots->start('content') ?>
> Техник <?= $tech->getFio() ?> не начал рабочий день, а до заявки осталось менее часа.
<?php $slots->stop() ?>