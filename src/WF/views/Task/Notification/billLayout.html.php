<?php

/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
/* @var $comment models\TaskComment */
$view->extend('Task/Notification/taskEmailLayout.html.php');
$slots = $view->get('slots');
$bill = $task->getBill();
?>

<?php $slots->start('main_content') ?>
<p><b>Плательщик:</b> <?= $bill->getPayer() ?></p>
<p><b>Заказчик:</b> <?= $bill->getClient() ?></p>
<p>
    <b>Договор:</b>
    <?php if ($bill->getClientAgreement()): ?>
    <?= $bill->getClientAgreement()->getNumber() ?> от <?= $bill->getClientAgreement()->getAgrDate()->format('d.m.Y') ?>
    <?php endif; ?>
</p>
<p><b>Поставщик:</b> <?= $bill->getSupplier() ?></p>
<p>
    <b>Договор:</b>
    <?php if ($bill->getAgreement()): ?>
    <?= $bill->getAgreement()->getNumber() ?> от <?= $bill->getAgreement()->getAgrDate()->format('d.m.Y') ?>
    <?php endif; ?>
</p>
<p><b>Статья затра:</b> <?= $bill->getType() ? $bill->getType()->getTitle() : '-' ?></p>
<p><b>Сумма:</b> <?= $bill->getAmount() ?></p>
<?php $slots->stop() ?>
