<?php

/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
$view->extend('Task/Notification/taskEmailLayout.html.php');
$slots = $view->get('slots');
?>

<?php $slots->start('footer_content') ?>
<span style="color:red;font-weight:bold;font-size:14px;">Наступила дата, до которой этот проект был отложен. Автоматическая смена статуса на <i>Поручено</i>.</span>
<?php $slots->stop() ?>