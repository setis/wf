<?php

/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
$view->extend('Task/Notification/taskEmailLayout.html.php');
$slots = $view->get('slots');
?>

<?php $slots->start('footer_content') ?>
<span style="color:red;font-weight:bold;font-size:14px;">Пожалуйста, отчитайтесь о выполнении проекта или о причинах, этому препятствующих.</span>
<?php $slots->stop() ?>