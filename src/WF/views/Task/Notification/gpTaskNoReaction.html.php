<?php
/* @var $link string */
/* @var $task models\Task */
$gp = $task->getGp();
$view->extend('Task/Notification/taskEmailLayout.html.php');
$slots = $view->get('slots');
?>

<?php $slots->start('main_content') ?>
<p>Заявка, поступившая <?= $task->getDateReg()->format('d.m.Y H:i') ?>, не взята в работу</p>
<p>B2B:&nbsp;<?= $gp->getOpB2b() ?>, B2C:&nbsp;<?= $gp->getOpB2c() ?></p>
<div><?= $gp->getBody() ?></div>
<?php $slots->stop() ?>