<?php
/* @var $link string */
/* @var $task models\Task */
/* @var $gp models\Gp */
/* @var $address models\ListAddr */
$view->extend('Task/Notification/taskEmailLayout.html.php');
$slots = $view->get('slots');
?>

<?php $slots->start('main_content') ?>
<p>Новая ГП  по адресу <?= $address->getFullAddress() ?> с нераспознанным СЦ</p>
<p>B2B:&nbsp;<?= $gp->getOpB2b() ?>, B2C:&nbsp;<?= $gp->getOpB2c() ?></p>
<div><?= $gp->getBody() ?></div>
<?php $slots->stop() ?>