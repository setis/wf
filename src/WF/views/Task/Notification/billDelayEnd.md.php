<?php
/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
$view->extend('Task/Notification/taskSlackLayout.md.php');
$slots = $view->get('slots');
?>

<?php $slots->start('content') ?>
> Наступила дата, до которой был отложен счет.
<?php $slots->stop() ?>