<?php
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/** @var $task \models\Task */
/* @var $link string */
/* @var $author \models\User */
/* @var $executors \models\User[] */
$slots = $view->get('slots');
$status = $task->getStatus();
$statusColor = $status ? $status->getColor() : '#FFF';
$taskTypeTitle = classes\Plugin::getNameStatic($task->getPluginUid());

$taskTitle = $task->getTaskTitle() . (\models\Task::BILL === $task->getPluginUid() && $task->getBill() ? ' - ' . $task->getBill()->getPaymentDescription() : '');
?>
<html>
<body style="font-family:Arial,Helvetica,Sans-Serif;font-size:12px;">
<h3>
    GERP - <?= $taskTypeTitle ?> №<?= $task->getId() ?> - <?php $slots->output('head', $taskTitle) ?> |
    <a href="<?php $slots->output('task_link', $link) ?>">Открыть задачу</a>
</h3>

<table style="width:100%;border-collapse:collapse;border:1px solid #888;">
    <tr style="background-color:<?= $statusColor ?>;">
        <th style="border:1px solid #888;font-size:12px;padding:10px;">Дата регистрации</th>
        <th style="border:1px solid #888;font-size:12px;padding:10px;">Статус</th>
        <th style="border:1px solid #888;font-size:12px;padding:10px;">Автор</th>
        <th style="border:1px solid #888;font-size:12px;padding:10px;">Исполнители</th>
    </tr>
    <tr>
        <td style="border:1px solid #888;font-size:12px;padding:10px;">
            <?= $task->getDateReg() ? $task->getDateReg()->format('d.m.Y H:i') : '' ?>
        </td>
        <td style="border:1px solid #888;font-size:12px;padding:10px;">
            <?= $task->getStatus() ? $task->getStatus()->getName() : '' ?>
        </td>
        <td style="border:1px solid #888;font-size:12px;padding:10px;">
            <?= $author ? $author->getFio() : '' ?>
        </td>
        <td rowspan="2" style="border:1px solid #888;font-size:12px;padding:10px;">
            <?= implode(',<br>', $executors) ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border:1px solid #888;font-size:12px;padding:10px;">
            <?php $slots->output('main_content', $task->getTaskComment()) ?>
        </td>
    </tr>
</table>

<div><?php $slots->output('footer_content') ?></div>

<?= date('Y') ?> &copy;GERP
</body>
</html>


