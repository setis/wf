<?php

/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $task \models\Task */
/* @var $comment models\TaskComment */
$view->extend('Task/Notification/billLayout.html.php');
$slots = $view->get('slots');
$commentAuthor = $comment->getAuthor();
$status = $comment->getStatus();
$statusColor = $status ? $status->getColor() : '#FFF';
$tag = $comment->getTag();
?>

<?php $slots->start('footer_content') ?>
<table>
    <tr style="background-color:<?= $statusColor ?>;">
        <th style="text-align: left;"><b>Дата:</b> <?= $comment->getDatetime()->format('d.m.Y H:i') ?> <b>Автор:</b> <?= $commentAuthor ?></th>
    </tr>
    <tr>
        <td>
            <i><?= $comment->getTag() ?></i><br>
            <?= $comment->getText() ?>
        </td>
    </tr>
</table>
<?php $slots->stop() ?>
