<?php
/**
 * @var \models\Task $task
 * @var \models\ServiceTicketExtras $extras
 */

$addr = [];
$ticket = $task->getTicket();
$addr = sprintf('%s, кв. %s, подьезд: %s, домофон: %s', $ticket->getDom(), $ticket->getKv(), $ticket->getPod(), $ticket->getDomofon());
?>

<b>Дилер *ООО ВСЕ СЕТИ*</b>
<?php echo $task->getDateReg()->format('d-m-Y H:i'); ?><br />
<?php echo $ticket->getClntFio()?:'-'; ?><br />
тел.: <?php echo $ticket->getClntTel1().' '. $ticket->getClntTel2(); ?><br />
<?php echo $addr; ?><br />
#Тариф "<?php echo $extras;?>"<br />
<br/>
Примечание "<?php echo $ticket->getAddinfo();?>"<br />

