<?php
/** @var $exception Exception */

/** @var \Symfony\Component\Templating\PhpEngine $view */
$view->extend('layout_bootstrap.html.php');

/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$slots = $view->get('slots');

$slots->set('title', get_class($exception) . ': ' . $exception->getMessage());
?>
<?php $slots->start('container') ?>

<div class="row">
    <div class="col-xs-12">
        <h2><?= $exception->getFile() . ':' . $exception->getLine() ?></h2>
        <pre><?= $exception->getTraceAsString() ?></pre>
    </div>
    <div class="col-xs-12 text-right">
        <img src="/images/angry_darth_vader.png" style="position: fixed;bottom: 25px;right: 10%;"/>
    </div>
</div>

<?php $slots->stop() ?>