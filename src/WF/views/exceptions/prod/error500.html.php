<?php
/** @var $exception Exception */

/** @var \Symfony\Component\Templating\PhpEngine $view */
$view->extend('layout_bootstrap.html.php');

/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$slots = $view->get('slots');

$slots->set('title', "На сервере нашем ошибка непредвиденная случилась.");
?>
<?php $slots->start('container') ?>

    <div class="row">
        <div class="col-xs-12">
            <h2>Но джедаи могучие о ней знают уже и исправят скоро. </h2>
            <p class="lead">Сообщение им оставь ты всё равно <a href="mailto:<?= $developerEmail ?>"><?= $developerEmail ?></a>.</p>

            <?php /* <form method="POST">
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="6" placeholder="Сообщения важного своего тут текст вводи..."></textarea>
                </div>
                <input type="hidden" name="error[trace]" value="<?= $exception->getTraceAsString() ?>"/>
                <input type="hidden" name="error[request]" value="<?= json_encode($request, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) ?>"/>
                <div class="text-right">
                    <button type="submit" class="btn btn-default">Отправить</button>
                </div>
            </form> */ ?>

        </div>
        <div class="col-xs-12 text-right">
            <img src="/images/master_yoda.png" style="position: fixed;bottom: 35px;right: 10%;"/>
        </div>
    </div>

<?php $slots->stop() ?>