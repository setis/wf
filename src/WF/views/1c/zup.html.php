<?php
/**
 * zup.php
 * 2016-03-25
 * wf
 */

/** @var $report \classes\reports\ReportAllScPeriod */
/** @var $period \DateTime */
?>
<report period="<?php echo $period->format('c'); ?>">
    <?php
    foreach ($report->getItems() as $reportSc):
        foreach ($reportSc->getItems() as $tech):
            ?>

            <employee id="<?php echo $tech->getTech()['id']; ?>" title="<?php echo $tech->getTech()['fio']; ?>">
                <accidents>
                    <?php
                    $conversions = $tech->getConnectionsConversionByCounterparty();
                    foreach ($tech->getConnectionsProfitByCounterparty() as $title => $sum):
                        $conversion = $conversions[$title];
                        ?>
                        <partner id="<?php ; ?>" title="<?php echo $title; ?>">
                            <amount><?php echo $sum; ?></amount>
                            <conversion><?php echo $conversion; ?></conversion>
                        </partner>
                    <?php endforeach; ?>
                </accidents>
                <connections>
                    <?php
                    $conversions = $tech->getAccidentsConversionByCounterparty();
                    foreach ($tech->getAccidentsProfitByCounterparty() as $title => $sum):
                        $conversion = $conversions[$title];
                        ?>
                        <partner id="<?php ; ?>" title="<?php echo $title; ?>">
                            <amount><?php echo $sum; ?></amount>
                            <conversion><?php echo $conversion; ?></conversion>
                        </partner>
                    <?php endforeach; ?>
                </connections>
                <skp_cash>
                    <?php
                    $conversions = $tech->getSkpCashConversionByCounterparty();
                    foreach ($tech->getSkpCashProfitByCounterparty() as $title => $sum):
                        $conversion = $conversions[$title];
                        ?>
                        <partner id="<?php ; ?>" title="<?php echo $title; ?>">
                            <amount><?php echo $sum; ?></amount>
                            <conversion><?php echo $conversion; ?></conversion>
                        </partner>
                    <?php endforeach; ?>
                </skp_cash>
                <skp_chless>
                    <?php
                    $conversions = $tech->getSkpCashlessConversionByCounterparty();
                    foreach ($tech->getSkpCashlessProfitByCounterparty() as $title => $sum):
                        $conversion = $conversions[$title];
                        ?>
                        <partner id="<?php ; ?>" title="<?php echo $title; ?>">
                            <amount><?php echo $sum; ?></amount>
                            <conversion><?php echo $conversion; ?></conversion>
                        </partner>
                    <?php endforeach; ?>
                </skp_chless>
                <otk>
                    <?php echo $tech->getKpiSum();?>
                </otk>
                <fot_bonus>
                    <?php echo $tech->getFotBonus();?>
                </fot_bonus>
            </employee>
            <?php
        endforeach;
    endforeach;
    ?>
</report>


<?php /*
                foreach ($report->getItems() as $reportSc):
                    ?>
                    <tbody>
                    <tr class="sc_totals">
                        <td colspan="3" class="report_totals_header">СЦ <?php echo $reportSc->title; ?></td>
                        <?php
                        echo str_repeat('<td></td>', $colspan);
                        ?>

                    </tr>
                    <?php foreach ($reportSc->getItems() as $tech):

                        $technician = $tech->getTech();
                        ?>
                        <tr>
                            <td><?php echo $technician['id']; ?></td>
                            <td><?php echo $technician['fio']; ?></td>
                            <td><?php echo $technician['title']; ?></td>
                            <!-- Подключения (кол-во) (по агенту) -->
                            <?php
                            drawTextCell($tech->getConnectionsCountsByCounterparty(), $report->getConnectionsCountsByCounterparty(), 'conn');
                            ?>

                            <!-- Подключения (кол-во) (по работам) -->
                            <?php
                            drawTextCell($tech->getConnectionsCountsByWork(), $report->getConnectionsCountsByWork(), 'conn');
                            ?>

                            <!-- ТТ (кол-во) (по агенту) -->
                            <?php
                            drawTextCell($tech->getAccidentsCountsByCounterparty(), $report->getAccidentsCountsByCounterparty(), 'tt');
                            ?>

                            <!-- ТТ (кол-во) (по работам) -->
                            <?php
                            drawTextCell($tech->getAccidentsCountsByWork(), $report->getAccidentsCountsByWork(), 'tt');
                            ?>

                            <!-- СКП (кол-во) (наличные) -->
                            <?php
                            drawTextCell($tech->getSkpCashCountByCounterparty(), $report->getSkpCashCountByCounterparty(), 'skp_cash');
                            ?>
                            <th class="skp_cash"><?php echo $tech->getSkpCashCount(); ?></th>

                            <!-- СКП (кол-во) (безналичные) -->
                            <?php
                            drawTextCell($tech->getSkpCashLessCountByCounterparty(), $report->getSkpCashLessCountByCounterparty(), 'skp_cashless');
                            ?>
                            <th class="skp_cashless"><?php echo $tech->getSkpCashlessCount(); ?></th>


                            <!-- Подключения (суммы) (по агенту) -->
                            <?php
                            drawSumCell($tech->getConnectionsProfitByCounterparty(), $report->getConnectionsProfitByCounterparty(), 'conn');
                            ?>
                            <th class="conn"><?php echo rub($tech->getConnectionsProfit()); ?></th>

                            <!-- Подключения (конвертации) (по агенту) -->
                            <?php
                            drawSumCell($tech->getConnectionsConversionByCounterparty(), $report->getConnectionsConversionByCounterparty(), 'conn');
                            ?>
                            <th class="conn"><?php echo rub($tech->getConnectionsConversion()); ?></th>

                            <!-- ТТ (суммы) (по агенту) -->
                            <?php
                            drawSumCell($tech->getAccidentsProfitByCounterparty(), $report->getAccidentsProfitByCounterparty(), 'tt');
                            ?>
                            <th class="tt"><?php echo rub($tech->getAccidentsProfit()); ?></th>

                            <!-- ТТ (конвертации) (по агенту) -->
                            <?php
                            drawSumCell($tech->getAccidentsConversionByCounterparty(), $report->getAccidentsConversionByCounterparty(), 'tt');
                            ?>
                            <th class="tt"><?php echo rub($tech->getAccidentsConversion()); ?></th>

                            <!-- СКП нал (статус "Выполнено") (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashInvoiceNotPayedByCounterparty(), $report->getSkpCashInvoiceNotPayedByCounterparty(), 'skp_cash');
                            ?>
                            <th class="skp_cash"><?php echo rub($tech->getSkpCashInvoiceNotPayed()); ?></th>

                            <!-- СКП нал (суммы для контрагента) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashInvoiceByCounterparty(), $report->getSkpCashProfitByCounterparty(), 'skp_cash');
                            ?>
                            <th class="skp_cash"><?php echo rub($tech->getSkpCashInvoice()); ?></th>

                            <!-- СКП нал (суммы для техника) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashProfitByCounterparty(), $report->getSkpCashProfitByCounterparty(), 'skp_cash');
                            ?>
                            <th class="skp_cash"><?php echo rub($tech->getSkpCashProfit()); ?></th>

                            <!-- СКП нал (конвертации) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashConversionByCounterparty(), $report->getSkpCashConversionByCounterparty(), 'skp_cash');
                            ?>
                            <th class="skp_cash"><?php echo rub($tech->getSkpCashConversion()); ?></th>

                            <!-- СКП безнал (статус "Выполнено") (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashlessInvoiceNotPayedByCounterparty(), $report->getSkpCashlessInvoiceNotPayedByCounterparty(), 'skp_cashless');
                            ?>
                            <th class="skp_cashless"><?php echo rub($tech->getSkpCashlessInvoiceNotPayed()); ?></th>

                            <!-- СКП безнал (суммы для контрагента) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashlessInvoiceByCounterparty(), $report->getSkpCashlessInvoiceByCounterparty(), 'skp_cashless');
                            ?>
                            <th class="skp_cashless"><?php echo rub($tech->getSkpCashlessInvoice()); ?></th>

                            <!-- СКП безнал (суммы для техника) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashlessProfitByCounterparty(), $report->getSkpCashlessProfitByCounterparty(), 'skp_cashless');
                            ?>
                            <th class="skp_cashless"><?php echo rub($tech->getSkpCashlessProfit()); ?></th>

                            <!-- СКП безнал (конвертации) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashlessConversionByCounterparty(), $report->getSkpCashlessConversionByCounterparty(), 'skp_cashless');
                            ?>
                            <th class="skp_cashless"><?php echo rub($tech->getSkpCashlessConversion()); ?></th>

                            <!-- Сумма за все виды работ (подключение, эксплуатация, СКП без штрафов) + конвертация -->
                            <td><?php echo rub($tech->getProfit() + $tech->getConversion()); ?></td>
                            <!-- ОТК -->
                            <td><?php echo rub($tech->getQualityPercent(), false); ?></td>
                            <td><?php echo rub($tech->getQualityFactor(), false); ?></td>
                            <td><?php echo rub($tech->getKpiSum(), false); ?></td>
                            <!-- Премия от ФОТ -->
                            <td><?php echo rub($tech->getPenalties()); ?></td>
                            <td><?php echo rub($tech->getFotPercent()); ?></td>
                            <td><?php echo rub($tech->getFotBonus()); ?></td>
                            <!-- ИТОГО -->
                            <td><?php echo rub($tech->getSalary()); ?></td>
                            <!-- Выплаченные за СКП (нал) (Принято в кассу) -->
                            <td><?php echo rub($tech->getSkpCashProfit()); ?></td>
                            <!-- На руки -->
                            <td><?php echo rub($tech->getSalary() - $tech->getSkpCashProfit()); ?></td>
                            <!-- Задолженности -->
                            <td><?php echo rub($tech->getDebt()); ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endforeach; */ ?>

