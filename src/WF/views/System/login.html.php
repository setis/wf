<?php
/**
 * @var $view \Symfony\Component\Templating\PhpEngine
 */
$view->extend('layout.html.php');
/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$slots = $view->get('slots');

$slots->set('title', 'Горсвязь');
?>

<?php $slots->start('css'); ?>
<link rel="stylesheet" type="text/css" href="/templates/twozero/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="/templates/twozero/css/login.css"/>
<?php $slots->stop(); ?>
<?php $slots->start('page_js'); ?>
<script type="text/javascript" src="/legacy_js/jquery.js"></script>
<script type="text/javascript" src="/legacy_js/bootstrap.min.js"></script>
<?php $slots->stop(); ?>

<div class="container">
    <form class="form-signin" method="POST">
        <h2 class="form-signin-heading text-center">
            <img width="220" src="/templates/default/images/logo_09_2016.png" title="Система Автоматизации Горсвязь"/>
        </h2>
        <br>
        <label for="inputEmail" class="sr-only">Логин</label>
        <input id="inputEmail" class="form-control" placeholder="Логин" name="_username" value="<?= $last_username ?>" required autofocus>
        <label for="inputPassword" class="sr-only">Пароль</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Пароль" name="_password" required>
        <br>
        <button class="btn btn-lg btn-success btn-block btn-login_custom" type="submit">Вход</button>
        <?php if ($error): ?>
            <p></p>
            <div class="alert alert-danger" role="alert">
                <?php echo $view['translator']->trans($error->getMessage()) ?>
            </div>
        <?php endif ?>
    </form>
</div>
