<?php
/** @var \models\User $user */

/** @var \Symfony\Component\Templating\PhpEngine $view */
$view->extend('layout_bootstrap.html.php');

/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$slots = $view->get('slots');

/** @var \WF\Core\UrlHelper $urlGenerator */
$urlGenerator = $view->get('url');

$slots->set('title', 'Сервисные центры');
?>

<?php $slots->start('breadcrumbs'); ?>
<?php
$widget = new \classes\widgets\twozero\BreadcrumbsWidget();
$widget->items = [
    ['url' => '/', 'label' => 'Рабочий стол'],
    ['url' => '/adm_interface', 'label' => 'Настройки'],
    ['active' => true, 'label' => 'Сервисные центры'],
];
echo $widget->run();
?>
<?php $slots->stop(); ?>

<?php $slots->start('container') ?>
<div
<div ng-app="ServiceCenterApp">
    <div ng-view></div>
</div>
<?php $slots->stop() ?>

<?php $slots->start('page_js') ?>
<script src="/assets/wf/app.js"></script>
<script src="/assets/wf/service_center/controllers/dashboard-controller.js"></script>
<script src="/assets/wf/user/api.js"></script>
<?php $slots->stop() ?>
