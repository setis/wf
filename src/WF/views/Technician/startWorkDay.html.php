<?php $view->extend('layout.html.php') ?>

<?php $view['slots']->start('css') ?>
<link rel="stylesheet" type="text/css" href="/templates/twozero/css/style_bs.css"/>
<link rel="stylesheet" type="text/css" href="/templates/twozero/css/bootstrap.min.css"/>
<?php $view['slots']->stop('css') ?>

<div class="container">
    <div class="row">
        <div class="col-sm-2 col-sm-offset-5">
            <form method="post" action="/tech/start-day">
                <input type="submit" class="btn btn-success btn-lg" value="Начать день">
            </form>
        </div>
    </div>
</div>
