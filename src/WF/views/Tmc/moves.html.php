<?php
/** @var \Symfony\Component\Templating\PhpEngine $view */
$view->extend('layout_bootstrap.html.php');

/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$slots = $view->get('slots');

/** @var \WF\Core\UrlHelper $urlGenerator */
$urlGenerator = $view->get('url');

$slots->set('title', 'ТМЦ');
?>

<?php $slots->start('breadcrumbs'); ?>
<?php
$widget = new \classes\widgets\twozero\BreadcrumbsWidget();
$widget->items = [
    ['url' => '/', 'label' => 'Рабочий стол'],
    ['url' => $urlGenerator->generate('tmc_dashboard'), 'label' => 'ТМЦ'],
    ['active' => true, 'label' => 'Движения ТМЦ'],
];
echo $widget->run();
?>
<?php $slots->stop(); ?>

<?php $slots->start('container') ?>

<div ng-app="TmcApp" ng-controller="MovesController">
    <div class="row">
        <div class="col-lg-12 text-right">
            <a class="btn btn-warning"
               href="<?php echo $urlGenerator->generate('tmc_sc_hold'); ?>">
                <span class="glyphicon glyphicon-home"></span> На складе
            </a>

        </div>
    </div>
    <hr/>

    <h3>Движения ТМЦ </h3>

    <uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</uib-alert>

    <div class="row">
        <table ng-table="tableParams" class="table table-condensed table-hover">
            <tr ng-repeat="row in $data track by row.id">
                <td data-title="'#'" filter="{key: 'text'}">{{row.key|limitTo:8}}</td>
                <td data-title="'Дата'" filter="filter.date">{{row.move_date|date:'dd MMM HH:mm'}}</td>
                <td data-title="'Откуда'" filter="{'source_holder.name': 'text'}" filter-data="filter.wh">
                    {{row.source_holder ? row.source_holder.name : "От партнёров" }}
                </td>
                <td data-title="'Куда'" filter="{'target_holder.name': 'text'}" filter-data="filter.wh">{{
                    row.target_holder ? row.target_holder.name : "Партнёрам" }}
                </td>
                <td data-title="'Статус'" filter="{state: 'select'}" filter-data="filter.states" class="text-center">
                    {{row.state|tmcstate}}<br>
                    <span class="text-muted">{{row.approved_by.name}}</span>
                </td>
                <td data-title="'Позиций'" class="text-center">{{row.items_count}}</td>
                <td data-title="'#'" class="text-center">
                    <a ng-href="/tmc2/moves-chain/{{row.key}}" class="btn btn-primary"><span class="glyphicon glyphicon-search"></a></button>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php $slots->stop() ?>

<?php $slots->start('page_js') ?>
<script src="/assets/wf/tmc/filter-tmc-states.js"></script>
<script src="/assets/wf/tmc/controllers/tmc-moves-controller.js"></script>
<?php $slots->stop() ?>
