<?php
/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots \Symfony\Component\Templating\Helper\SlotsHelper */
$view->extend( 'layout_bootstrap.html.php' );
$slots = $view->get( 'slots' );
/** @var \WF\Core\UrlHelper $urlGenerator */
$urlGenerator = $view->get( 'url' );

$slots->set( 'title', 'ТМЦ' );

?>

<?php $slots->start( 'breadcrumbs' ); ?>
<?php
$widget = new \classes\widgets\twozero\BreadcrumbsWidget();
$widget->items = [
    [ 'url' => '/', 'label' => 'Рабочий стол' ],
    [ 'url' => $urlGenerator->generate( 'tmc_dashboard' ), 'label' => 'ТМЦ' ],
    [ 'active' => true, 'label' => 'У техников' ],
];
echo $widget->run();
?>
<?php $slots->stop(); ?>

<?php $slots->start( 'container' ) ?>
<div ng-app="TmcApp" ng-controller="TechnicianHoldController">
    <div class="row">
        <div class="col-xs-12 col-lg-2">
            Остаток техников склада
        </div>

        <div class="col-xs-12  col-lg-2">
            <select ng-model="wh" ng-options="value.name for value in whs track by value.id" ng-change="reload()" required></select>
        </div>
        <div class="col-xs-12 col-lg-8 text-right">
            <a class="btn btn-warning"
               href="<?php echo $urlGenerator->generate( 'tmc_sc_hold' ); ?>">
                <span class="glyphicon glyphicon-home"></span> На складе
            </a>

            <a class="btn btn-default"
               href="<?php echo $urlGenerator->generate( 'tmc_moves' ); ?>">
                <span class="glyphicon glyphicon-retweet"></span> Перемещения и запросы
            </a>
        </div>
    </div>
    <hr/>

    <div class="row">
        <table ng-table="tableParams" class="table table-condensed table-hover">
            <tr ng-repeat="row in $data track by row.id" ng-class="{'danger': row.is_defect, 'warning': row.is_uninstalled}">
                <td data-title="'#'">{{row.id}}</td>
                <td data-title="'Техник'" filter="{'holder_link.technician.name': 'text'}">{{row.holder_link.technician.name}}</td>
                <td data-title="'Категория'" filter="{'nomenclature.category.id': 'select'}" filter-data="getCategories">{{row.nomenclature.category.title}}</td>
                <td data-title="'Название'" filter="{'nomenclature.title': 'text'}">{{row.nomenclature.title}}</td>
                <td data-title="'Поставщик'" filter="{'partner.id': 'select'}" filter-data="getPartners">{{row.partner.title}}</td>
                <td data-title="'Количество'" class="text-center">{{row.total}} {{row.nomenclature.unit.title}}</td>
                <td data-title="'S/N'" filter="{'serial': 'text'}" class="text-center">{{row.serial ? row.serial :
                    "(б/н)"}}
                </td>
                <td data-title="'Демонтаж'" class="text-center">{{row.is_uninstalled ? "Да" : "Нет" }}</td>
            </tr>
        </table>
    </div>
</div>
<?php $slots->stop() ?>

<?php $slots->start( 'page_js' ) ?>
<script src="/assets/wf/tmc/controllers/technicians-hold-controller.js"></script>
<?php $slots->stop() ?>
