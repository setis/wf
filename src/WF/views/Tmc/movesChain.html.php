<?php
/** @var \Symfony\Component\Templating\PhpEngine $view */
$view->extend('layout_bootstrap.html.php');

/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$slots = $view->get('slots');

/** @var \WF\Core\UrlHelper $urlGenerator */
$urlGenerator = $view->get('url');

$slots->set('title', 'ТМЦ');
?>

<?php $slots->start('breadcrumbs'); ?>
<?php
$widget = new \classes\widgets\twozero\BreadcrumbsWidget();
$widget->items = [
    ['url' => '/', 'label' => 'Рабочий стол'],
    ['url' => $urlGenerator->generate('tmc_dashboard'), 'label' => 'ТМЦ'],
    ['url' => $urlGenerator->generate('tmc_moves'), 'label' => 'Движения ТМЦ'],
    ['active' => true, 'label' => 'Цепочка ТМЦ'],
];
echo $widget->run();
?>
<?php $slots->stop(); ?>

<?php $slots->start('container') ?>

<div ng-app="TmcApp" ng-controller="MovesChainController">
    <h3>Цепочка ТМЦ </h3>

    <uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</uib-alert>

    <div class="row">
        <div class="panel panel-default" ng-repeat="move in moves track by move.id">
            <div class="panel-heading">
                Статус: <span class="badge">{{ move.state | tmcstate }}</span>
                <div ng-if="$first" class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-default btn-sm" title="Одобрить" ng-if="access.can_approve" ng-click="updateMove(move, 'approve')"><span
                            class="glyphicon glyphicon-ok"></span></button>

                    <button type="button" class="btn btn-default btn-sm" title="Назначить перевозку" ng-if="access.can_transport" ng-click="toTransport(move)"><span
                            class="fa fa-truck"></span></button>

                    <button type="button" class="btn btn-default btn-sm" ng-class="{active: isItemsEdit, 'btn-danger': doesMoveHaveSerials(move)}" title="Изменить состав" ng-if="access.can_change"
                            ng-click="changeMoveItems(move)"><span class="glyphicon glyphicon-equalizer"></span></button>

                    <button type="button" class="btn btn-default btn-sm" title="Отменить" ng-if="access.can_cancel" ng-click="updateMove(move, 'cancel')"><span
                            class="glyphicon glyphicon-ban-circle"></span></button>

                </div>
                <span class="pull-right">{{ move.approved_by.fio }}</span>
            </div>
            <div class="panel-body">
                <h5>
                    <span class="label label-primary">{{move.source_holder ? move.source_holder.name : "От партнёров" }}</span>
                    <span class="glyphicon glyphicon-arrow-right"></span>
                    <span class="label label-success">{{ move.target_holder ? move.target_holder.name : "Партнёрам" }}</span>
                </h5>
                <table class="table">
                    <tr>
                        <th>id</th>
                        <th>Категория</th>
                        <th>Название</th>
                        <th>Поставщик</th>
                        <th>Кол-во</th>
                        <th class="text-nowrap">S/N</th>
                        <th></th>
                    </tr>
                    <tr ng-repeat="itm in move.items track by itm.id">
                        <td>{{ itm.id }}</td>
                        <td>{{ itm.property.nomenclature.category.title }}</td>
                        <td>{{ itm.property.nomenclature.title }}</td>
                        <td>{{ itm.property.partner.title }}</td>
                        <td>{{ itm.quantity }}</td>
                        <td>
                            <span ng-if="itm.property.nomenclature.is_metrical" class="text-nowrap">(б/н)</span>
                            <span ng-if="!itm.property.nomenclature.is_metrical">{{ itm.property.serial }}</span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-footer">
                <span>{{ move.move_date|date:'medium' }}</span>

            </div>
        </div>

    </div>
</div>
<?php $slots->stop() ?>

<?php $slots->start('page_js') ?>
<script src="/assets/wf/tmc/filter-tmc-states.js"></script>
<script>
    angular.module('TmcApp', ["ui.bootstrap", "angular-confirm", "ngTable", 'ui.select', 'ngSanitize', 'wf', 'wf.tmc'])
        .filter('doesMoveHaveSerials', function () {
            return function (input) {
                return _.filter(input.items, function (e) {
                        return !e.property.nomenclature.is_metrical && (e.quantity > 1 || !e.property.serial);
                    }).length > 0;
            };
        })
        .controller('MovesChainController', [
            '$scope',
            '$http',
            '$location',
            'NgTableParams',
            '$uibModal',
            'doesMoveHaveSerialsFilter',
        function ($scope, $http, $location, NgTableParams, $uibModal, doesMoveHaveSerialsFilter) {
            $scope.selected = [];
            $scope.alerts = [];
            $scope.wh = {};
            $scope.moves = [];
            $scope.access = {};
            $scope.states = {
                canceled: 'Отменён',
                return_request: 'Возврат от техника',
                transferred: 'Выдача ТМЦ',
                to_partner: 'Возврат партнёру',
                from_partner: 'От партнёров',
                moved: 'Перемещен',
                request: 'Запрос',
                items_changed: 'Изменение состава',
                request_confirmed: 'Запрос одобрен',
                request_approved: 'Запрос утверждён',
                transport_request: 'Запрос транспортировки',
                is_transported: 'Транспортируется',
                defect: 'Дефект'
            };

            $scope.doesMoveHaveSerials = function(e){
                return doesMoveHaveSerialsFilter(e);
            };

            $scope.reload = function () {
                return $http.get('<?= $urlGenerator->generate('tmc_api_moves_chain', ['key' => $key]) ?>')
                    .then(function (response) {
                        $scope.moves = response.data.moves;
                        $scope.loadAccess($scope.moves[0].id);
                        return $scope.moves;
                    });
            };

            $scope.loadAccess = function (move_id) {
                $scope.access = {};
                return $http.get('<?= $urlGenerator->generate('tmc_api_chain_access') ?>?move_id=' + move_id)
                    .then(function (response) {
                        $scope.access = response.data;
                    });
            };

            $scope.addMove = function (move) {
                $scope.moves.unshift(move);
                $scope.loadAccess(move.id);
            };

            $scope.reload().then(function(moves) {
                if(!!$location.search().serials) {
                    $scope.changeMoveItems(moves[0]);
                }
            });

            $scope.getState = function (move) {
                return $scope.states[move.state];
            };

            var moveUrls = {
                approve: '<?= $urlGenerator->generate('tmc_api_approve') ?>',
                cancel: '<?= $urlGenerator->generate('tmc_api_cancel') ?>'
            };

            $scope.updateMove = function (move, action) {
                var url = moveUrls[action];
                $http.post(url, {
                    move: move
                })
                    .then(function (response) {
                        $scope.addMove(response.data);
                    }, function (response) {
                        var msg = 'При операции произошла ошибка';

                        if (response.data.reason == 'negative_balance') {
                            msg = 'Невозможно переместить '
                                + response.data.nomenclature.title
                                + (response.data.property.serial ? ' ('+response.data.property.serial+')' : '')
                                + ' от ' + response.data.partner.title
                                + ' запрашиваемое количество '
                                + (-1 * response.data.quantity)
                                + ' доступно ' + response.data.balance;
                        }


                        $scope.alerts.push({
                            type: 'danger',
                            msg: msg
                        });
                    });
            };

            $scope.toTransport = function (move) {
                $uibModal.open({
                    templateUrl: '/assets/wf/tmc/templates/to-transport-modal.html?dt=' + (new Date().getTime()),
                    controller: 'ToTransportModalController',
                    size: 'lg',
                    resolve: {
                        result: $http.get('/api/tmc2/carriers.json')
                    }
                }).result.then(function (data) {
                    $http.post('/api/tmc2/define-transport.json', {
                        move: move,
                        carrier_id: data.carrier_id
                    }).then(function (response) {
                        $scope.addMove(response.data);
                    });

                });
            };

            $scope.changeMoveItems = function (move) {
                $location.search({serials: undefined});
                $uibModal.open({
                    templateUrl: '/assets/wf/tmc/templates/change-move-items-modal.html?dt=' + (new Date().getTime()),
                    controller: 'ChangeMoveItemsModalController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        data: {
                            move: move
                        }
                    }
                }).result.then(function (result) {
                    $scope.addMove(result.data);
                });
            };

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

        }])

        .run(function ($confirmModalDefaults) {
            $confirmModalDefaults.defaultLabels.title = 'Подтвердите операцию';
            $confirmModalDefaults.defaultLabels.ok = 'Да';
            $confirmModalDefaults.defaultLabels.cancel = 'Отмена';
        });

</script>
<script src="/assets/wf/tmc/controllers/change-move-items-modal-controller.js"></script>
<script src="/assets/wf/tmc/controllers/to-transport-modal-controller.js"></script>
<?php $slots->stop() ?>
