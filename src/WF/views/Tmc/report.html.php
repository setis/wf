<?php
/** @var \Symfony\Component\Templating\PhpEngine $view */
$view->extend('layout_bootstrap.html.php');

/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
$slots = $view->get('slots');

/** @var \WF\Core\UrlHelper $urlGenerator */
$urlGenerator = $view->get('url');

$slots->set('title', 'ТМЦ');
?>

<?php $slots->start('breadcrumbs'); ?>
<?php
$widget = new \classes\widgets\twozero\BreadcrumbsWidget();
$widget->items = [
    ['url' => '/', 'label' => 'Рабочий стол'],
    ['url' => $urlGenerator->generate('tmc_dashboard'), 'label' => 'ТМЦ'],
    ['active' => true, 'label' => 'Отчет'],
];
echo $widget->run();
?>
<?php $slots->stop(); ?>

<?php $slots->set('container_class', 'container-fluid') ?>

<?php $slots->start('container') ?>



<div ng-app="TmcApp" ng-controller="ReportController">
    <script type="text/ng-template" id="tmpl/datepicker.html">
        <input ng-click="opened = true" is-open="opened" type="text" class="form-control" uib-datepicker-popup="MMMM yyyy" ng-model="params.filter()[name]" datepicker-options="datepickerOptions" />
    </script>

    <h3>Отчет ТМЦ </h3>
    
    <select ng-model="tableParams.filter().state" ng-options="state.title for state in filter.states track by state.id" required></select>
    <select ng-model="tableParams.filter().task_type" ng-options="type.title for type in filter.task_types track by type.id" required></select>
    <a class="btn btn-success" title="Export Table" ng-click='exportCsv()'>
        <span class="fa fa-file"></span> Экспорт в .csv
    </a>
    
    <uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</uib-alert>

    <div class="row-fluid">
        <table ng-table="tableParams" class="table table-condensed table-hover" export-csv-ignore="ng-table-filters" export-csv="csv" separator=";">
            <tr ng-repeat="row in $data track by row.id">
                <td data-title="'#'">{{row.id}}</td>
                <td data-title="'Заявка'" filter="{'task_id': 'text'}"><a ng-href="/accidents/viewticket?task_id={{row.task_id}}">{{row.task_id}}</a></td>
                <td data-title="'СЦ'" filter="{warehouse: 'select'}" filter-data="filter.warehouses">{{row.service_center}}</td>
                <td data-title="'Поставщик'" filter="{'partner': 'text'}">{{row.partner}}</td>
                <td data-title="'Партнёр'" filter="{'task_partner': 'text'}">{{row.task_partner}}</td>
                <td data-title="'Наименование'" filter="{nomenclature: 'text'}">{{row.nomenclature}}</td>
                <td data-title="'S/N'" filter="{serial: 'text'}">{{row.serial}}</td>
                <td data-title="'К-во'" filter="{'quantity': 'text'}">{{ row.quantity }}</td>
                <td data-title="'Адрес'" filter="{'address': 'text'}">{{row.address}}</td>
                <td data-title="'Дата'" filter="{move_date: 'tmpl/datepicker.html'}">{{row.move_date|date:'dd MMM HH:mm'}}</td>
                <td data-title="'Исполнитель'">{{ row.holder }}</td>
                <td ng-if="showReason(row)" data-title="'Причина'">{{ row.reason }}</td>
                <td ng-if="showComment(row)" data-title="'Комментарий'">{{ row.comment }}</td>
                
                <td data-title="'#'" class="text-center">
                    <a ng-href="/tmc2/moves-chain/{{row.move_key}}" class="btn btn-primary"><span class="glyphicon glyphicon-search"></a></button>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php $slots->stop() ?>

<?php $slots->start('page_js') ?>
<script src="/assets/wf/tmc/filter-tmc-states.js"></script>
<script src="/assets/wf/tmc/controllers/report-controller.js"></script>
<?php $slots->stop() ?>
