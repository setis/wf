<?php
/* @var $view \Symfony\Component\Templating\PhpEngine */
use models\TmcMove;

/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $oldMove TmcMove */
/* @var $newMove TmcMove */
/* @var $newState string */
/* @var $oldState string */
/* @var $link string */

$slots = $view->get('slots');
?>


<h1><?php $slots->output('head') ?>  | <a href="<?php $slots->output('link', $link) ?>">Открыть движение</a></h1>

<table class="task" style="width: 100%; border: solid 1px #888;" cellspacing="0" cellpadding="10" >
    <tr>
        <!-- th>ID</th -->
        <th style="border: solid 1px #888;">ID</th>
        <th style="border: solid 1px #888;">Статус</th>
        <th style="border: solid 1px #888;">Автор</th>
        <th style="border: solid 1px #888;">Склад/Техник</th>
    </tr>
    <tr>
        <td class="hdr" style="border: solid 1px #888;">
            <?= $newMove->getKey() ?>
        </td>
        <td class="hdr" style="border: solid 1px #888;">
            <?php echo TmcMove::getStateMemo($oldState); ?> &rarr; <?php echo TmcMove::getStateMemo($newState); ?>
        </td>
        <td class="hdr" style="border: solid 1px #888;">
            <?php echo $newMove->getApprovedBy() ? $newMove->getApprovedBy()->getFio() : ''; ?>
        </td>
        <td class="hdr" style="border: solid 1px #888;" rowspan="2">
            <?php echo $newMove->getSourceHolder() ? $newMove->getSourceHolder()->getName() : ''; ?> &rarr; <?php echo $newMove->getTargetHolder() ? $newMove->getTargetHolder()->getName() : ''; ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border: solid 1px #888;">
            <h3>Состав движения:</h3>
            <?php $slots->output('content') ?>
        </td>
    </tr>
</table>

<?= date('Y') ?> &copy; GERP
