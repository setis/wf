<?php
/* @var $view \Symfony\Component\Templating\PhpEngine */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
/* @var $oldMove \models\TmcMove */
/* @var $newMove \models\TmcMove */
/* @var $newState string */
/* @var $oldState string */
/* @var $link string */

$view->extend('Tmc/Notification/tmcEmailLayout.html.php');
$slots = $view->get('slots');
?>

<?php $slots->set('head', 'Перемещение №' . $newMove->getId()); ?>

<?php $slots->start('content') ?>

<table border="0" cellpadding="10" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <thead>
    <tr>
        <th style="border: solid 1px #888;">Категория</th>
        <th style="border: solid 1px #888;">Наименование</th>
        <th style="border: solid 1px #888;">Поставщик</th>
        <th style="border: solid 1px #888;">Количество</th>
        <th style="border: solid 1px #888;">S/N</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($newMove->getItems() as $item): ?>
    <tr>
        <td style="border: solid 1px #888;"><?php echo $item->getNomenclature()->getCategoty()->getTitle(); ?></td>
        <td style="border: solid 1px #888;"><?php echo $item->getNomenclature()->getTitle(); ?></td>
        <td style="border: solid 1px #888;"><?php echo $item->getPartner()->getPartnerName(); ?></td>
        <td style="border: solid 1px #888;"><?php echo $item->getQuantity().' '.$item->getNomenclature()->getCategoty()->getUnit(); ?></td>
        <td style="border: solid 1px #888;"><?php echo $item->getProperty()->getSerial()?:'(б/н)'; ?><td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php $slots->stop() ?>
