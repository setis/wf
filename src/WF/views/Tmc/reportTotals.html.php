<?php
/** @var \Symfony\Component\Templating\PhpEngine $view */
$view->extend('layout_bootstrap.html.php');

/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
/* @var $slots Symfony\Component\Templating\Helper\SlotsHelper */
$slots = $view->get('slots');

/** @var \WF\Core\UrlHelper $urlGenerator */
$urlGenerator = $view->get('url');

$slots->set('title', 'ТМЦ');
?>

<?php $slots->start('breadcrumbs'); ?>
<?php
$widget = new \classes\widgets\twozero\BreadcrumbsWidget();
$widget->items = [
    ['url' => '/', 'label' => 'Рабочий стол'],
    ['url' => $urlGenerator->generate('tmc_dashboard'), 'label' => 'ТМЦ'],
    ['active' => true, 'label' => 'Сводный отчет ТМЦ'],
];
echo $widget->run();
?>
<?php $slots->stop(); ?>

<?php $slots->set('container_class', 'container-fluid') ?>

<?php $slots->start('container') ?>

<div ng-app="TmcApp" ng-controller="ReportController">

    <h3>Отчет ТМЦ </h3>

    <form class="form-inline">
        <div class="form-group">
            <label for="date_from">Дата с</label>
            <input id="date_from" ng-model="filter.date_from" ng-click="from_opened = true" is-open="from_opened" type="text" uib-datepicker-popup="dd MMMM yyyy" />
        </div>
        <div class="form-group">
            <label for="date_to">по</label>
            <input id="date_to" ng-model="filter.date_to" ng-click="to_opened = true" is-open="to_opened" type="text" uib-datepicker-popup="dd MMMM yyyy" />
        </div>
        <div class="form-group">
            <label for="warehouse">СЦ</label>
            <select id="warehouse" ng-change="reloadTechs()" ng-model="filter.warehouse" ng-options="wh.name for wh in filterData.warehouses track by wh.id" required></select>
        </div>
        <div class="form-group" ng-if="byTech">
            <label for="tech">Техник</label>
            <select id="tech" ng-model="filter.tech" ng-options="tech.name for tech in filterData.techs track by tech.id" required></select>
        </div>
        <div class="checkbox">
            <label>
                <input ng-change="changeMode()" ng-model="byTech" type="checkbox"> отчет по техникам
            </label>
        </div>
        <a class="btn btn-success" title="Export Table" ng-click='exportCsv()' ng-disabled="exportCsvDisabled()">
            <span class="fa fa-file"></span> Экспорт в .csv
        </a>
    </form>



    <uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</uib-alert>

    <div class="row-fluid">
        <table ng-table="tableParams" class="table table-condensed table-hover" export-csv-ignore="ng-table-filters" export-csv="csv" separator=";">
            <tr ng-repeat="row in $data track by row.id">
                <td data-title="'#'">{{row.id}}</td>
                <td data-title="'Категория'"  sortable="'nomenclature.category.title'">{{row.nomenclature.category.title}}</td>
                <td data-title="'Наименование'">{{row.nomenclature.title}}</td>
                <td data-title="'Контрагент'">{{row.partner.title}}</td>
                <td class="text-right" data-title="'Было'">{{row.quantity}}</td>
                <td class="text-right" data-title="'Пришло'">{{row.quantity_in}}</td>
                <td class="text-right" data-title="'Ушло'">{{row.quantity_out}}</td>
                <td class="text-right" data-title="'Остаток'">{{row.quantity + row.quantity_in - row.quantity_out}}</td>
            </tr>
        </table>
    </div>
</div>
<?php $slots->stop() ?>

<?php $slots->start('page_js') ?>
<script src="/assets/wf/tmc/controllers/report-totals-controller.js"></script>
<?php $slots->stop() ?>
