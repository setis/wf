<?php
/* @var $view \Symfony\Component\Templating\PhpEngine */
$view->extend('layout_bootstrap.html.php');
/* @var $slots \Symfony\Component\Templating\Helper\SlotsHelper */
$slots = $view->get('slots');
/* @var $urlGenerator \WF\Core\UrlHelper */
$urlGenerator = $view->get('url');

$slots->set('title', 'ТМЦ');

?>

<?php $slots->start('breadcrumbs'); ?>
<?php
$widget = new \classes\widgets\twozero\BreadcrumbsWidget();
$widget->items = [
    ['url' => '/', 'label' => 'Рабочий стол'],
    ['url' => $urlGenerator->generate('tmc_dashboard'), 'label' => 'ТМЦ'],
    ['active' => true, 'label' => '"В рюкзаке"'],
];
echo $widget->run();
?>
<?php $slots->stop(); ?>

<?php $slots->start('container') ?>
<div ng-app="TmcApp" ng-controller="AvailableController">
    <div class="row">
        <div class="col-lg-12 text-right">
            <a class="btn btn-warning"
               href="<?php echo $urlGenerator->generate('tmc_sc_hold'); ?>">
                <span class="glyphicon glyphicon-home"></span> На складе
            </a>

            <a class="btn btn-default"
               href="<?php echo $urlGenerator->generate('tmc_moves'); ?>">
                <span class="glyphicon glyphicon-retweet"></span> Перемещения и запросы
            </a>
        </div>
    </div>
    <hr/>

    <h3>Остаток у {{ holder.fio}}</h3>

    <uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</uib-alert>

    <div class="row" ng-show="selected.length > 0">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Выбранное оборудование</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Категория</th>
                            <th>Наименование</th>
                            <th>Партнёр</th>
                            <th>S/N</th>
                            <th>Кол-во</th>
                        </tr>
                        <tr ng-repeat="row in selected track by $index">
                            <td>
                                <button class="btn btn-sm btn-default" ng-click="removeItem(row)"><span class="glyphicon glyphicon-remove"></span></button>
                            </td>
                            <td>
                                {{row.nomenclature.category.title}}
                            </td>
                            <td>{{row.nomenclature.title}}</td>
                            <td>{{row.partner.title}}</td>
                            <td>{{row.serial}}</td>
                            <td><input min="1" max="{{row.total}}" type="number" ng-model="row.quantity"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <button class="btn btn-default" ng-click="returnToWh()">Вернуть на склад</button>
                <button class="btn btn-warning" ng-click="markDefect()">Брак</button>
                <button class="btn btn-warning pull-right" ng-click="clearSelected()"><span class="glyphicon glyphicon-erase"></span> Очистить</button>
            </div>
        </div>
    </div>

    <div class="row">
        <table ng-table="tableParams" class="table table-condensed table-hover">
            <tr ng-repeat="row in $data track by row.id" ng-class="{'danger': row.is_defect, 'warning': row.is_uninstalled}">
                <td data-title="'#'">{{row.id}}</td>
                <td data-title="'Категория'" filter="{'nomenclature.category.id': 'select'}" filter-data="getCategories">{{row.nomenclature.category.title}}</td>
                <td data-title="'Название'" filter="{'nomenclature.title': 'text'}">{{row.nomenclature.title}}</td>
                <td data-title="'Поставщик'" filter="{'partner.id': 'select'}" filter-data="getPartners">{{row.partner.title}}</td>
                <td data-title="'S/N'" filter="{'serial': 'text'}">{{row.serial}}</td>
                <td data-title="'Доступно'" class="text-center">{{row.total}} {{row.nomenclature.unit.title}}</td>
                <td data-title="'#'" class="text-center">
                    <button class="btn btn-primary" ng-click="addItem(row)"><span
                            class="glyphicon glyphicon-plus"></span></button>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php $slots->stop() ?>

<?php $slots->start('page_js') ?>
<script src="/assets/wf/tmc/controllers/tech-hold-controller.js"></script>
<?php $slots->stop() ?>
