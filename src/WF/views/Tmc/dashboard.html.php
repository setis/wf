<?php
/** @var \models\User $user */

/** @var \Symfony\Component\Templating\PhpEngine $view */
$view->extend('layout_bootstrap.html.php');

/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$slots = $view->get('slots');

/** @var \WF\Core\UrlHelper $urlGenerator */
$urlGenerator = $view->get('url');

$slots->set('title', 'ТМЦ');
?>

<?php $slots->start('breadcrumbs'); ?>
<?php
$widget = new \classes\widgets\twozero\BreadcrumbsWidget();
$widget->items = [
    ['url' => '/', 'label' => 'Рабочий стол'],
    ['active' => true, 'label' => 'ТМЦ'],
];
echo $widget->run();
?>
<?php $slots->stop(); ?>

<?php $slots->start('container') ?>
<div ng-app="TmcApp" ng-controller="AvailableController">
    <script type="text/ng-template" id="filters/serial.html">
        <input type="text" name="{{name}}" ng-disabled="$filterRow.disabled" ng-model="params.filter()[name]" class="input-filter form-control" my-enter="doSomething()" />
    </script>
    <script type="text/ng-template" id="modals/history.html">
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title">
                История оборудования <br />

            </h3>
        </div>
        <div class="modal-body" id="modal-body">
            <p>
                <small class="text-muted">{{ $ctrl.bro_tmc.nomenclature.category.title}}</small> {{ $ctrl.bro_tmc.nomenclature.title }}<br />
                Серийный номер: <b>{{ $ctrl.bro_tmc.serial?$ctrl.bro_tmc.serial:"Без номера" }}</b>
            </p>
            <table class="table table-responsive">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Статус</td>
                    <td>Дата</td>
                    <td>Откуда</td>
                    <td>Куда</td>
                    <td>Отвественный</td>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="item in $ctrl.items">
                    <td><a href="/tmc2/moves-chain/{{item.key}}">{{ item.key|limitTo:8 }}</a></td>
                    <td>{{ item.state|tmcstate }}</td>
                    <td>{{ item.move_date|date:'dd MMM HH:mm' }}</td>
                    <td>{{ item.source_holder?item.source_holder.name : "От партнёров" }}</td>
                    <td>{{ item.target_holder?item.target_holder.name : "Партнёрам" }}</td>
                    <td>{{ item.approved_by.name }}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="$ctrl.ok()">OK</button>
        </div>
    </script>
    <div ng-if="roles.isMol || roles.isTechnician || roles.isCoordinator || roles.isCarrier">
        <div class="row">
            <div class="col-lg-12 text-right">
                <a ng-if="roles.isTechnician" class="btn btn-warning" href="<?php echo $urlGenerator->generate('tmc_tech_hold'); ?>">
                    <span class="glyphicon glyphicon-user"></span> "В рюкзаке"
                </a>

                <a ng-if="roles.isMol" class="btn btn-warning" href="<?php echo $urlGenerator->generate('tmc_techs_hold'); ?>">
                    <span class="glyphicon glyphicon-user"></span> У техников
                </a>

                <a ng-if="roles.isSuperMol" class="btn btn-primary" href="<?php echo $urlGenerator->generate('tmc_from_partner'); ?>">
                    <span class="glyphicon glyphicon-save"></span> Приём от партнёров
                </a>

                <a ng-if="roles.isMol || roles.isTechnician || roles.isCoordinator || roles.isCarrier" class="btn btn-default"
                   href="<?php echo $urlGenerator->generate('tmc_moves'); ?>">
                    <span class="glyphicon glyphicon-retweet"></span> Перемещения и запросы
                </a>
            </div>
        </div>
        <hr/>
    </div>


    <div class="row">
        <div class="col-xs-6">
            <p>Остаток на складе:</p>
            <select ng-model="wh" ng-options="value.name for value in whs track by value.id" ng-change="reload()" required></select>
        </div>
        <div class="col-xs-6 text-right" ng-if="roles.isMol || roles.isDispatcher || roles.isCheif">
            <p>
                <label>
                    <input type="checkbox" ng-change="reloadList()" ng-model="groupedMode.value" >
                    Группировать по партнёру и наименованию
                </label>
            </p>
            <p>
                <label>
                    <input type="checkbox" ng-change="fullReloadList()" ng-model="fullMode.value" >
                    Поисковый режим
                </label>
            </p>
        </div>
    </div>

    <hr/>

    <uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</uib-alert>

    <div class="row" ng-show="selected.length > 0">
        <form name="itemsForm">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Выбранное оборудование</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>#</th>
                                <th>Категория</th>
                                <th>Наименование</th>
                                <th>Поставщик</th>
                                <th ng-if="roles.isMolOnThisWarehouse">S/N</th>
                                <th>Кол-во</th>
                            </tr>
                            <tr ng-repeat="row in selected track by $index">
                                <td>
                                    <button class="btn btn-sm btn-danger" ng-click="removeItem(row)"><span class="glyphicon glyphicon-remove"></span></button>
                                </td>
                                <td>
                                    {{row.nomenclature.category.title}}
                                </td>
                                <td>{{row.nomenclature.title}}</td>
                                <td>{{row.partner.title}}</td>
                                <td ng-if="roles.isMolOnThisWarehouse">
                                    <span ng-if="row.nomenclature.is_metrical" class="text-nowrap">(б/н)</span>
                                    <span ng-if="!row.nomenclature.is_metrical">
                                        <span ng-if="!roles.isMolOnThisWarehouse">{{row.serial}}</span>
                                        <input ng-if="roles.isMolOnThisWarehouse" ng-model="row.serial" required>
                                    </span>
                                </td>
                                <td>
                                    <input
                                        required
                                        min="1"
                                        max="{{row.total}}"
                                        type="number"
                                        ng-model="row.quantity"
                                        ng-if="row.nomenclature.is_metrical">

                                    <span ng-if="!row.nomenclature.is_metrical">
                                        <span ng-if="roles.isMol || roles.isDispatcher || roles.isCheif">1</span>

                                        <input
                                            required
                                            min="1"
                                            max="{{row.total}}"
                                            type="number"
                                            ng-model="row.quantity"
                                            ng-if="!(roles.isMol || roles.isDispatcher || roles.isCheif)">
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="panel-footer clearfix">
                    <span>
                        <button class="btn btn-success" ng-disabled="!itemsForm.$valid" ng-if="!roles.isMolOnThisWarehouse && (roles.isTechnician || roles.isMol)"
                                ng-click="makeRequest()">Отправить запрос</button>
                        <button class="btn btn-default" ng-disabled="!itemsForm.$valid" ng-if="roles.isMolOnThisWarehouse" ng-click="transferToTech()">Передать технику</button>
                        <button class="btn btn-default" ng-disabled="!itemsForm.$valid" ng-if="roles.isSuperMolOnThisWarehouse" ng-click="returnToPartner()"
                                confirm="Вы действительно хотите передать выбранные ТМЦ контрагенту?" confirm-ok="Передать" confirm-cancel="Отмена"
                                confirm-title="Подтверждение передачи контрагенту">
                            Вернуть контрагенту
                        </button>
                        <button class="btn btn-info" ng-disabled="!itemsForm.$valid" ng-if="roles.isMolOnThisWarehouse" confirm="Вы действительно хотите передать выбранные ТМЦ на другой склад?" confirm-ok="Передать" confirm-cancel="Отмена"  ng-click="transferBetweenWarehouses()"
                                confirm-title="Передать на другой склад">
                            На другой склад
                        </button>
                        <button class="btn btn-danger" ng-disabled="!itemsForm.$valid" ng-if="roles.isSuperMolOnThisWarehouse" ng-click="writeOff()"
                                confirm="Вы действительно хотите списать выбранные ТМЦ?" confirm-ok="Списать" confirm-cancel="Отмена"
                                confirm-title="Подтверждение списания ТМЦ">
                            Списать
                        </button>
                        <button ng-if="roles.isMolOnThisWarehouse" ng-disabled="!itemsForm.$valid" class="btn btn-warning" ng-click="markDefect()">Брак</button>
                        <button class="btn btn-success" ng-disabled="!itemsForm.$valid" ng-if="roles.isMolOnThisWarehouse" ng-click="setSerials()">Присвоить серийные номера</button>
                        <button ng-if="roles.isTechnicianOnThisWarehouse || !roles.isMolOnThisWarehouse" class="btn btn-warning" ng-click="addMissingItems()" title="Добавить в запрос произовольную позицию"><span class="glyphicon glyphicon-plus"></span></button>
                    </span>
                    <button class="btn btn-danger pull-right" ng-click="clearSelected()"><span class="glyphicon glyphicon-erase"></span> Очистить</button>
                </div>
            </div>
        </form>

    </div>
    <div class="row">
        <table ng-table="tableParams" class="table table-condensed table-hover animate">
            <tr
                ng-repeat="row in $data track by row.id"
                ng-class="{
                    'danger': !groupedMode.value && (roles.isMol || roles.isDispatcher || roles.isCheif) && row.is_defect,
                    'warning': !groupedMode.value && (roles.isMol || roles.isDispatcher || roles.isCheif) && row.is_uninstalled,
                    'info': !groupedMode.value && (roles.isMol || roles.isDispatcher || roles.isCheif) && !!row.property.reservation,
                }"
            >
                <td data-title="'#'">{{row.id}}</td>
                <td data-title="'Категория'" filter="{'nomenclature.category.id': 'select'}" filter-data="getCategories" sortable="'nomenclature.category.title'">{{row.nomenclature.category.title}}</td>
                <td data-title="'Название'" filter="{'nomenclature.title': 'text'}" sortable="'nomenclature.title'">{{row.nomenclature.title}}</td>
                <td data-title="'Поставщик'" filter="{'partner.id': 'select'}" filter-data="getPartners" sortable="'partner.title'">{{row.partner.title}}</td>
                <td data-title="'Демонтаж'"
                    class="text-center"
                    ng-if="!groupedMode.value && (roles.isMol || roles.isDispatcher || roles.isCheif)"
                    sortable="'is_uninstalled'"
                    filter="{is_uninstalled: 'select'}"
                    filter-data="[{id: true, title:'Да'}, {id: false, title:'Нет'}]">
                    {{row.is_uninstalled ? "Да" : "Нет"}}
                </td>
                <td data-title="'Дефект'"
                    class="text-center"
                    ng-if="!groupedMode.value && (roles.isMol || roles.isDispatcher || roles.isCheif)"
                    sortable="'is_defect'"
                    filter="{is_defect: 'select'}"
                    filter-data="[{id: true, title:'Да'}, {id: false, title:'Нет'}]">
                    {{row.is_defect ? "Да" : "Нет"}}
                </td>
                <td data-title="'S/N'"
                    filter="{serial: 'filters/serial.html'}"
                    ng-if="!groupedMode.value && (roles.isMol || roles.isDispatcher || roles.isCheif)"
                    sortable="'serial'">
                    <a href ng-click="getHistory(row)">{{row.serial}}</a>
                </td>
                <td data-title="'Количество'"
                    class="text-center"
                    ng-if="roles.isMol || roles.isDispatcher || roles.isCheif"
                    sortable="'total'">
                    {{row.total}} {{row.nomenclature.unit.title}} <a ng-href="/tmc2/moves-chain/{{row.property.reservation.key}}" ng-if="!!row.property.reservation" target="_blank">({{row.property.reservation.key|limitTo:8}})</a>
                </td>
                <td data-title="'#'" class="text-center" ng-if="!groupedMode.value">
                    <button class="btn btn-success" title="Выбрать" ng-click="addItem(row)" ng-if="row.total > 0"><span class="glyphicon glyphicon-plus"></span></button>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php $slots->stop() ?>

<?php $slots->start('page_js') ?>
<script src="/assets/wf/app.js"></script>
<script src="/assets/wf/tmc/filter-tmc-states.js"></script>
<script src="/assets/wf/tmc/controllers/dashboard-controller.js"></script>
<script src="/assets/wf/tmc/controllers/select-items-modal-controller.js"></script>
<script src="/assets/wf/tmc/api.js"></script>
<script src="/assets/wf/user/api.js"></script>
<?php $slots->stop() ?>
