<?php
/** @var \Symfony\Component\Templating\PhpEngine $view */
$view->extend('layout_bootstrap.html.php');

/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$slots = $view->get('slots');

/** @var \WF\Core\UrlHelper $urlGenerator */
$urlGenerator = $view->get('url');
?>

<?php $slots->set('title', 'ТМЦ') ?>

<?php $slots->start('breadcrumbs'); ?>
<?php
$widget = new \classes\widgets\twozero\BreadcrumbsWidget();
$widget->items = [
    ['url' => '/', 'label' => 'Рабочий стол'],
    ['url' => $urlGenerator->generate('tmc_dashboard'), 'label' => 'ТМЦ'],
    ['active' => true, 'label' => 'Приём давальческого'],
];
echo $widget->run();
?>
<?php $slots->stop(); ?>

<?php $slots->start('container') ?>
<div ng-app="TmcApp" ng-controller="FromPartnerController">
    <h4>Прием ТМЦ на склад</h4>
    <div class="row">
        <div class="col-md-3">
            <form autocomplete="off">
                <div class="form-group">
                    <label for="nomenclature">Номенклатура</label>
                    <div class="input-group">
                        <input id="nomenclature" class="form-control" placeholder="введите название" name="nomenclature" type="text" ng-model="selected" typeahead-editable="false" typeahead-on-select="onSelect($item, $model, $label)" uib-typeahead-editable="false" typeahead-wait-ms="500" uib-typeahead="option.title for option in nomenclature | filter:$viewValue">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" title="Добавить отсутствующую" ng-click="addNomenclature()"><span class="glyphicon glyphicon-plus"></span></button>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="partner">Поставщик</label>
                    <input id="partner" placeholder="выберите поставщика" name="partner" type="text" ng-model="selectedPartner" typeahead-editable="false" typeahead-on-select="onSelectPartner($item, $model, $label)" uib-typeahead-editable="false" typeahead-wait-ms="500" uib-typeahead="option.title for option in partners | filter:$viewValue | limitTo:8" class="form-control">
                </div>
                <div class="form-group">
                    <label for="quantity">Количество</label>
                    <input id="quantity" name="quantity" type="number" ng-model="quantity" class="form-control" >
                </div>
                <div class="form-group" ng-show="!item.is_metrical && quantity == 1">
                    <label for="serial">Серийник</label>
                    <input id="serial" name="serial" type="text" ng-model="serial" class="form-control" >
                </div>
                <button class="btn btn-default" ng-disabled="!selected" ng-click="addItem(item)">Добавить</button>
            </form>

            <div class="form-group">
                <label for="exampleInputFile">Загрузить через файл</label>
                <input type="file" nv-file-select uploader="uploader"/><br/>
                <ul>
                    <li ng-repeat="item in uploader.queue">
                        Файл: <span ng-bind="item.file.name"></span><br/>
                        <button class="btn btn-success" ng-click="item.upload()">Загрузить</button>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-9">
            <form name="fromPartnerForm">
                <uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</uib-alert>
                <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Категория</th>
                        <th>Название</th>
                        <th>Поставщик</th>
                        <th>Кол-во</th>
                        <th>С/Н</th>
                        <th></th>
                    </tr>
                    <tr ng-class="{'text-muted': !itm.selected}" ng-repeat="itm in items track by $index">
                        <td>{{ $index+1 }}</td>
                        <td>{{ itm.category.title }}</td>
                        <td>{{ itm.title }}</td>
                        <td>{{ itm.partner.title }}</td>
                        <td>{{ itm.quantity }}</td>
                        <td>
                            <div class="form-group">
                                <input
                                ng-class="{'has-error': serialNotUniq(itm)}"
                                required
                                ng-if="!itm.is_metrical"
                                id="{{ itm.input_id }}"
                                ng-enter="onPressEnter(itm)"
                                type="text"
                                ng-model="itm.serial"
                                class="form-control"

                                >
                            </div>
                        </td>
                        <td><input type="checkbox" ng-model="itm.selected"></td>
                    </tr>
                </table>
                <button ng-disabled="fromPartnerForm.$invalid || items.length === 0" class="btn btn-success" ng-click="toWarehouse()">На склад</button>
                <button class="btn btn-warning pull-right" ng-click="clearSelected()"><span class="glyphicon glyphicon-erase"></span> Очистить</button>
            </form>
        </div>
    </div>
</div>
<?php $slots->stop() ?>

<?php $slots->start('page_js') ?>
<script src="/assets/wf/tmc/controllers/from-partner-controller.js"></script>
<script src="/assets/wf/tmc/controllers/add-nomenclature-modal-controller.js"></script>
<?php $slots->stop() ?>
