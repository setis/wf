<?php
/**
 * mail@artemd.ru
 * 29.06.2016
 */

namespace WF\Technician\Events;


use DateTime;
use Symfony\Component\EventDispatcher\Event;
use WF\Tmc\Model\TechnicianInterface;

class TechnicianScheduleOpenEvent extends Event
{
    const NAME = 'wf.technician.schedule_open';

    /**
     * @var TechnicianInterface
     */
    private $technician;

    /**
     * @var DateTime
     */
    private $workDayBegin;

    /**
     * @var DateTime
     */
    private $workDayEnd;

    /**
     * @var string
     */
    private $result;

    public function __construct(TechnicianInterface $technician, DateTime $begin, DateTime $end)
    {
        $this->technician = $technician;
        $this->workDayBegin = $begin;
        $this->workDayEnd = $end;
    }

    /**
     * @param TechnicianInterface $technician
     * @return TechnicianScheduleOpenEvent
     */
    public function setTechnician($technician)
    {
        $this->technician = $technician;
        return $this;
    }

    /**
     * @return TechnicianInterface
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     * @param string $result
     * @return TechnicianScheduleOpenEvent
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return DateTime
     */
    public function getWorkDayBegin()
    {
        return $this->workDayBegin;
    }

    /**
     * @return DateTime
     */
    public function getWorkDayEnd()
    {
        return $this->workDayEnd;
    }

}
