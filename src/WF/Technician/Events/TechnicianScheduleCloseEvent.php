<?php
/**
 * mail@artemd.ru
 * 29.06.2016
 */

namespace WF\Technician\Events;


use DateTime;
use Symfony\Component\EventDispatcher\Event;
use WF\Tmc\Model\TechnicianInterface;

class TechnicianScheduleCloseEvent extends Event
{
    const NAME = 'wf.technician.schedule_close';

    /**
     * @var TechnicianInterface
     */
    private $technician;

    /**
     * @var DateTime
     */
    private $closeFrom;

    /**
     * @var DateTime
     */
    private $closeUntil;

    /**
     * @var string
     */
    private $result;

    public function __construct(TechnicianInterface $technician, DateTime $closeFrom, DateTime $closeUntil)
    {
        $this->technician = $technician;
        $this->closeFrom = $closeFrom;
        $this->closeUntil = $closeUntil;
    }

    /**
     * @param TechnicianInterface $technician
     * @return TechnicianScheduleOpenEvent
     */
    public function setTechnician($technician)
    {
        $this->technician = $technician;
        return $this;
    }

    /**
     * @return TechnicianInterface
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     * @param string $result
     * @return TechnicianScheduleOpenEvent
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return DateTime
     */
    public function getCloseUntil()
    {
        return $this->closeUntil;
    }

    /**
     * @return DateTime
     */
    public function getCloseFrom()
    {
        return $this->closeFrom;
    }


}
