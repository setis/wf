<?php

namespace WF\Technician\EventDispatcher;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\UserStatistic;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Templating\EngineInterface;
use WF\HttpKernel\Security\HttpUserAuthenticatedEvent;
use WF\Technician\CookieHelper;
use WF\Users\Model\UsersManagerInterface;


/**
 * Description of CheckDayStartedListener
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class CheckDayStartedListener
{
    /**
     *
     * @var UsersManagerInterface
     */
    private $um;

    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var EngineInterface
     */
    private $templating;

    /**
     *
     * @var DateTime
     */
    private $checkFrom;

    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;


    public function __construct(UsersManagerInterface $um, EntityManagerInterface $em, EngineInterface $templating, EventDispatcherInterface $dispatcher)
    {
        $this->um = $um;
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->templating = $templating;
        $this->checkFrom = new DateTime();
        $this->checkFrom->setTime(6, 0, 0);
    }

    public function onSecurityKernelUserAuthenticated(HttpUserAuthenticatedEvent $event)
    {
        $dt = new DateTime();
        $requestDt = clone $dt;
        $requestDt->setTime(0, 0, 0);

        // Route for handling "start day" button press
        if ($event->getRequest()->get('_route') === 'tech_start_day') {
            return;
        }

        // Don't check before "checkFrom" time
        if ($dt < $this->checkFrom) {
            return;
        }

        // User has valid cookie about magic button is pressed
        if (($cookie = $event->getRequest()->cookies->get(CookieHelper::COOKIE_NAME, false)) &&
            CookieHelper::validate($cookie, $event->getUser(), $requestDt)) {
            return;
        }

        $user = $event->getUser();
        // This is only for technicians
        if (!$this->um->isTechnician($user)) {
            return;
        }

        /* @var $userStat UserStatistic */
        $userStat = $this->em
            ->getRepository(UserStatistic::class)
            ->findOneBy([
                'user' => $user,
                'date' => $requestDt,
            ]);

        // User had started his day already
        if ($userStat && $userStat->getWorkDayStartedAt()) {
            // But he has not valid cookie, let set it up on response
            $this->dispatcher->addListener(KernelEvents::RESPONSE, function(FilterResponseEvent $e) use ($user, $requestDt) {
                $e->getResponse()->headers->setCookie(new Cookie(CookieHelper::COOKIE_NAME, CookieHelper::encode($user, $requestDt), '+12 hours'));
            });
            return;
        }

        $r = new Response($this->templating->render('Technician/startWorkDay.html.php'));
        $event->setResponse($r);
    }
}
