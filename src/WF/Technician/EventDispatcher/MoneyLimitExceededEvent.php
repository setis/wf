<?php

namespace WF\Technician\EventDispatcher;

use models\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Description of MoneyLimitExceeded
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class MoneyLimitExceededEvent extends Event
{
    const NAME = 'wf.tech.money_limit_exceeded';
    
    /**
     *
     * @var User 
     */
    private $user;
    
    /**
     *
     * @var float 
     */
    private $debt;
    
    /**
     *
     * @var float 
     */
    private $limit1;
    
    /**
     * @float
     */
    private $limit2;
    
    public function __construct(User $user, $debt, $limit1 = null, $limit2 = null)
    {
        $this->user = $user;
        $this->debt = (float) $debt;
        
        if ($limit1 === null && $limit2 === null) {
            throw new \InvalidArgumentException('Limit1 and limit2 can not be both null');
        }
        
        $this->limit1 = $limit1;
        $this->limit2 = $limit2;
    }
    
    /**
     * 
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * 
     * @return float
     */
    public function getDebt()
    {
        return $this->debt;
    }

    /**
     * 
     * @return float|null
     */
    public function getLimit1()
    {
        return $this->limit1;
    }

    /**
     * 
     * @return float|null
     */
    public function getLimit2()
    {
        return $this->limit2;
    }

}
