<?php

namespace WF\Technician\EventDispatcher;

use DateInterval;
use DateTime;
use models\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Description of GraphicTimeExpiredEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class GraphicTimeExpiredEvent extends Event
{
    const NAME = 'wf.tech.graphic_time_expired';
    
    /**
     *
     * @var User
     */
    private $user;
    
    /**
     *
     * @var DateInterval 
     */
    private $expireTime;
    
    /**
     *
     * @var DateTime
     */
    private $date;
    
    public function __construct(User $user, DateTime $date, DateInterval $expireTime)
    {
        $this->user = $user;
        $this->date = $date;
        $this->expireTime = $expireTime;
    }
    
    public function getUser()
    {
        return $this->user;
    }
    
    public function getDate()
    {
        return $this->date;
    }

    public function getExpireTime()
    {
        return $this->expireTime;
    }

}
