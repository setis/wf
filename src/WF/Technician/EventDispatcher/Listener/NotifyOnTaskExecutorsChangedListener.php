<?php

namespace WF\Technician\EventDispatcher\Listener;

use models\Task;
use Psr\Log\LoggerInterface;
use WF\Notification\Model\Notification;
use WF\Notification\Model\NotificationInterface;
use WF\Notification\NotificationManager;
use WF\Task\Events\TaskExecutorsChangedEvent;

/**
 * Description of NotifyOnTaskExecutorsChanged
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyOnTaskExecutorsChangedListener extends NotificationManager
{
    public function __construct(LoggerInterface $logger, array $handlers = [])
    {
        parent::__construct($handlers, $logger);
    }

    public function onWfTaskExecutorsChanged(TaskExecutorsChangedEvent $event)
    {
        $executors = $event->getAddedExecutors();
        $pluginUid = $event->getTask()->getPluginUid();

        if (!in_array($pluginUid, [Task::GLOBAL_PROBLEM])
            || count($executors) === 0)
        {
            return;
        }

        $type = $pluginUid === Task::SERVICE ?
            'СКП' :
            ($pluginUid === Task::GLOBAL_PROBLEM ? 'ГП' : '');

        $notification = new Notification();
        $notification->setTo($executors)
            ->setLevel(NotificationInterface::CRITICAL)
            ->setMessage(sprintf('Вам поручена заявка ' . $type .
                ' №%d', $event->getTask()->getId()));

        $this->sendNotification($notification);
    }
}
