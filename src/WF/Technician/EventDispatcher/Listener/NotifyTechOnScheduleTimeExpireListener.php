<?php

namespace WF\Technician\EventDispatcher\Listener;

use models\Task;
use Psr\Log\LoggerInterface;
use WF\Notification\Model\Notification;
use WF\Notification\Model\NotificationInterface;
use WF\Notification\NotificationManager;
use WF\Task\Events\TaskScheduleTimeSoonExpireEvent;

/**
 * Description of NotifyTechOnScheduleTimeExpireListener
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyTechOnScheduleTimeExpireListener extends NotificationManager
{
    public function __construct(LoggerInterface $logger, array $handlers = [])
    {
        parent::__construct($handlers, $logger);
    }

    public function onWfTaskScheduleTimeSoonExpire(TaskScheduleTimeSoonExpireEvent $event)
    {
        $task = $event->getSchedule()->getTask();

        if (!in_array($task->getPluginUid(), [Task::ACCIDENT, Task::CONNECTION, Task::SERVICE])) {
            return;
        }

        $user = $event->getSchedule()->getTechnician();

        $notification = new Notification();
        $notification->setTo($user)
            ->setLevel(NotificationInterface::NOTICE)
            ->setMessage(sprintf('До заявки №%d осталось %s минут, при переносе свяжитесь с диспетчером!',
                $task->getId(), $event->getInterval()->i));

        $this->sendNotification($notification);
    }
}
