<?php

namespace WF\Technician\Command;

use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use models\Gfx;
use models\User;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Technician\EventDispatcher\GraphicTimeExpiredEvent;
use Psr\Log\LoggerInterface;

/**
 * Description of NotifyDayNotStartedCommand
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyDayNotStartedCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    
    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    
    /**
     *
     * @var LoggerInterface
     */
    private $logger;
    
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        parent::__construct();
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }
    
    protected function configure()
    {
        $this->setName('system:notify:tech-day-not-started')
            ->setDescription('Dispatch event if tech day not started and tech has task in 60 minutes')
            ->addOption('date', null, InputOption::VALUE_OPTIONAL , 'Specify date');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dateStr = $input->getOption('date');
        /* @var $dt DateTime */
        $dt = $dateStr ? new DateTime($dateStr) : new DateTime();
        
        $this->logger->debug('Started', ['date' => $dt->format('Y-m-d H:i:s')]);
        
        $start = $dt->format('H') * 60 + floor($dt->format('i') / 30) * 30 + 1;
        $end = $start + 30;
        
        $this->logger->debug(sprintf('Start time between %s and %s', $start, $end));
        
        $queryDt = clone $dt;
        $queryDt->setTime(0, 0, 0);
        $q = $this->em->createQueryBuilder()
            ->select('u, g')
            ->from(User::class, 'u')
            ->leftJoin('u.statistics', 's', Join::WITH, 's.date = :date')
            ->join('u.schedules', 'g', Join::WITH, 'g.date = :date')
            ->where('u.active = true')
            ->andWhere('s.id IS NULL OR s.workDayStartedAt IS NULL')
            ->andWhere('g.startTime BETWEEN :start AND :end')
            ->andWhere('NOT EXISTS ('
                . 'SELECT g1.id FROM '. Gfx::class . ' g1 '
                . 'WHERE g1.technician = g.technician '
                . 'AND g1.date = g.date '
                . 'AND g1.startTime < g.startTime)')
            ->orderBy('u.id')
            ->setParameter('date', $queryDt)
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery();
        
        $users = $q->getResult();
        $this->logger->debug(sprintf('%d users found', count($users)));
        
        $interval = new DateInterval('PT1H');
        foreach ($users as $user) {
            $this->dispatcher->dispatch(GraphicTimeExpiredEvent::NAME, new GraphicTimeExpiredEvent($user, $dt, $interval));
        }
        
        $this->logger->debug('complete');
    }
}
