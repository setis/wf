<?php

namespace WF\Technician\Command;

use Doctrine\ORM\EntityManagerInterface;
use models\SkpTechDebts;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Technician\EventDispatcher\MoneyLimitExceededEvent;

/**
 * Description of CalculateMoneyLimits
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyMoneyLimitsCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        parent::__construct();
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setName('wf:tech:notify-money-limits')
            ->setDescription('Dispatch event if tech money overlimits')
            ->addOption('limit1', NULL, InputOption::VALUE_REQUIRED, 'Limit1 default value', '10000')
            ->addOption('limit2', NULL, InputOption::VALUE_REQUIRED, 'Limit2 default value', '15000')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $qb = $this->em->createQueryBuilder()
            ->select('debt, tech, limit')
            ->from(SkpTechDebts::class, 'debt')
            ->join('debt.tech', 'tech')
            ->leftJoin('tech.moneyLimit', 'limit')
            ->where('tech.active = true');

        $iterableResult = $qb->getQuery()->iterate();
        $this->logger->debug('Iterations started');
        /* @var $debt SkpTechDebts */
        foreach ($iterableResult as $row) {
            $debt = $row[0];
            $limit = $debt->getTech()->getMoneyLimit();
            $amount = $debt->getAmount();

            $limit2 = $limit ? $limit->getLimit2() : $input->getOption('limit2');
            if ($amount >= $limit2) {
                $event = new MoneyLimitExceededEvent($debt->getTech(), $amount, null, $limit2);
                $this->dispatcher->dispatch(MoneyLimitExceededEvent::NAME, $event);

                $this->logger->debug('Limit2 exceeded', [
                    'user' => $debt->getTech()->getLogin(),
                    'debt' => $amount,
                    'limit' => $limit2
                    ]);
                continue;
            }

            $limit1 = $limit ? $limit->getLimit1() : $input->getOption('limit1');
            if ($amount >= $limit1) {
                $event = new MoneyLimitExceededEvent($debt->getTech(), $amount, $limit1, null);
                $this->dispatcher->dispatch(MoneyLimitExceededEvent::NAME, $event);

                $this->logger->debug('Limit1 exceeded', [
                    'user' => $debt->getTech()->getLogin(),
                    'debt' => $amount,
                    'limit' => $limit1
                    ]);
            }
        }
        $this->logger->debug('Iterations finished');
    }
}
