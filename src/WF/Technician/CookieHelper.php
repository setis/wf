<?php

namespace WF\Technician;

use DateTime;
use models\User;

/**
 * Description of CookieHelper
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class CookieHelper
{
    const COOKIE_NAME = '_wf_udtstr';
    
    public static function encode(User $user, DateTime $date)
    {
        return md5($date->format('Ymd').$user->getLogin());
    }
    
    public static function validate($cookie, User $user, DateTime $date)
    {
        return $cookie === self::encode($user, $date);
    }
}
