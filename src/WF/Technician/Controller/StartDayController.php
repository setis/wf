<?php

namespace WF\Technician\Controller;

use DateTime;
use models\User;
use models\UserStatistic;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use wf;
use WF\HttpKernel\AbstractController;
use WF\Technician\CookieHelper;

/**
 * Description of StartDayController
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class StartDayController extends AbstractController
{
    public function startDayAction(Request $request)
    {
        $user = $this->getUser();
        $em = $this->getEm();
        
        $requestDt = new DateTime();
        $requestDt->setTime(0, 0, 0);
        /* @var $userStat UserStatistic */
        $userStat = $em
            ->getRepository(UserStatistic::class)
            ->findOneBy([
                'user' => $user,
                'date' => $requestDt,
            ]);
        
        if ($userStat === null) {
            $userStat = new UserStatistic($user);
        }
        
        if ($userStat->getWorkDayStartedAt()) {
            return new RedirectResponse('/');
        }
        
        $userStat->setWorkDayStartedAt(new DateTime());
        
        $em->persist($userStat);
        $em->flush();
        
        return new RedirectResponse('/');
    }
    
    public function isGranted(Request $request)
    {
        return true;
    }
    
    /**
     * 
     * @return User
     * @todo Pablo remove when feature-tmc_v2 will be merged
     */
    protected function getUser()
    {
        return $this->getEm()->find(User::class, wf::$user->getId());
    }
}
