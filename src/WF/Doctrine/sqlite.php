<?php

namespace WF\Doctrine;

use CrEOF\Spatial\DBAL\Types\GeographyType;
use CrEOF\Spatial\Exception\InvalidValueException;
use CrEOF\Spatial\PHP\Types\Geometry\GeometryInterface;
use WF\Doctrine\AbstractSpatialType;
use CrEOF\Spatial\PHP\Types\Geography\GeographyInterface;
use CrEOF\Geo\WKT\Parser as StringParser;
use CrEOF\Geo\WKB\Parser as BinaryParser;

/**
 * sqlite spatial platform
 *
 */
class sqlite
{
    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration)
    {
        if ($fieldDeclaration['type']->getSQLType() === GeographyInterface::GEOGRAPHY) {
            return 'STRING';
        }

        return strtoupper($fieldDeclaration['type']->getSQLType());
    }

    /**
     * @param AbstractSpatialType $type
     * @param string              $sqlExpr
     *
     * @return string
     */
    public function convertToPHPValueSQL(AbstractSpatialType $type, $sqlExpr)
    {
        return $sqlExpr;
    }

    /**
     * @param AbstractSpatialType $type
     * @param string              $sqlExpr
     *
     * @return string
     */
    public function convertToDatabaseValueSQL(AbstractSpatialType $type, $sqlExpr)
    {
        return $sqlExpr;
    }

    /**
     * @param AbstractSpatialType $type
     * @param string              $sqlExpr
     *
     * @return GeometryInterface
     */
    public function convertStringToPHPValue(AbstractSpatialType $type, $sqlExpr)
    {
        $parser = new StringParser($sqlExpr);

        return $this->newObjectFromValue($type, $parser->parse());
    }

    /**
     * @param AbstractSpatialType $type
     * @param string              $sqlExpr
     *
     * @return GeometryInterface
     */
    public function convertBinaryToPHPValue(AbstractSpatialType $type, $sqlExpr)
    {
        $parser = new BinaryParser($sqlExpr);

        return $this->newObjectFromValue($type, $parser->parse());
    }

    /**
     * @param AbstractSpatialType $type
     * @param GeometryInterface   $value
     *
     * @return string
     */
    public function convertToDatabaseValue(AbstractSpatialType $type, GeometryInterface $value)
    {
        return sprintf('%s(%s)', strtoupper($value->getType()), $value);
    }

    /**
     * Get an array of database types that map to this Doctrine type.
     *
     * @param AbstractSpatialType $type
     *
     * @return string[]
     */
    public function getMappedDatabaseTypes(AbstractSpatialType $type)
    {
        $sqlType = strtolower($type->getSQLType());

        if ($type instanceof GeographyType && $sqlType !== 'geography') {
            $sqlType = sprintf('geography(%s)', $sqlType);
        }

        return array($sqlType);
    }

    /**
     * Create spatial object from parsed value
     *
     * @param AbstractSpatialType $type
     * @param array               $value
     *
     * @return GeometryInterface
     * @throws \CrEOF\Spatial\Exception\InvalidValueException
     */
    private function newObjectFromValue(AbstractSpatialType $type, $value)
    {
        $typeFamily = $type->getTypeFamily();
        $typeName   = strtoupper($value['type']);

        $constName = sprintf('CrEOF\Spatial\PHP\Types\Geometry\GeometryInterface::%s', $typeName);

        if (! defined($constName)) {
            throw new InvalidValueException(sprintf('Unsupported %s type "%s".', $typeFamily, $typeName));
        }

        $class = sprintf('CrEOF\Spatial\PHP\Types\%s\%s', $typeFamily, constant($constName));

        return new $class($value['value'], $value['srid']);
    }
}
