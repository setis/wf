<?php

namespace WF\ShaurmaBridge\EventDispatcher;

use WF\Task\Events\TaskStatusChangedEvent;

/**
 * Description of PluginCallbackOnTaskStatusChangeListener
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class PluginCallbackOnTaskStatusChangedListener
{
    public function onWfTaskStatusChanged(TaskStatusChangedEvent $event)
    {
        $cb_class = $event->getTask()->getPluginUid() . '_plugin';
        $cb_method = 'statusCallbackSet';
        $cb_exec = false;
        
        if (method_exists($cb_class, $cb_method)) {
            $cb_exec = true;
            $s = $this->statusToArray($event->getNewStatus());
            $text = $event->getComment()->getText();
            $tag = $event->getComment()->getTag();
            $cb_ret = call_user_func_array([$cb_class, $cb_method],
                [$event->getTask()->getId(), &$s, &$text, &$tag]
            );

            $event->getComment()->setText($text)
                ->setTag($tag);
            // persist changed comment... see TaskManager

        }
    }
    
    private function statusToArray(\models\TaskStatus $status)
    {
        return [
            'id' => $status->getId(),
            'name' => $status->getName(),
            'color' => $status->getColor(),
            'plugin_uid' => $status->getPluginUid(),
            'tag' => $status->getTag(),
            'is_active' => $status->getIsActive() ? 1 : 0,
            'is_default' => $status->getIsDefault() ? 1 : 0,
            'is_hidden' => $status->getIsHidden() ? 1 : 0,
            'require_comment' => $status->getRequireComment() ? 1 : 0,
        ];
    }
}
