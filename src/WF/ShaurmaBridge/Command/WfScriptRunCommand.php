<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 13.10.16
 * Time: 14:09
 */

namespace WF\ShaurmaBridge\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class WfScriptRunCommand extends Command
{
    protected function configure()
    {
        $this->setName('wf:scripts:run')
            ->setDescription('Run legacy commands (just a wrapper on php executable)')
            ->addArgument('cmd', InputArgument::REQUIRED, "Name of file in scripts/ folder.");
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cmd = $input->getArgument('cmd');
        $filePath =  __DIR__ . '/../../../../scripts/'.$cmd.'.php';

        if(!file_exists($filePath)) {
            throw new FileNotFoundException($filePath);
        }

        $process = new Process('php '.$filePath);
        $process->setTimeout(0);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        echo $process->getOutput();

        return;
    }
}
