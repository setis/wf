<?php

namespace WF\Monolog\Handler;

use Monolog\Handler\AbstractHandler;

/**
 * Description of DummyHandler
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class DummyHandler extends AbstractHandler
{
    public function handle(array $record)
    {
        return false;
    }

}
