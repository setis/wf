<?php

namespace WF\Slack;

use Psr\Log\LoggerInterface;
use WF\Slack\Model\SlackChannelInterface;
use WF\Slack\Model\SlackManagerInterface;
use WF\Slack\Model\SlackMessageInterface;
use WF\Slack\Model\SlackUserInterface;

/**
 * Description of DebugLoggerSlackManager
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class DebugLoggerSlackManager implements SlackManagerInterface
{
    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function archiveChannel(SlackChannelInterface $channel)
    {
        $this->logger->debug('Channel archived', ['channel' => $channel->getName()]);
    }

    public function createChannel(SlackChannelInterface $channel)
    {
        throw new \Exception('SlackManagerInterface::createChannel not implemented');
    }

    public function inviteToChannel(SlackChannelInterface $channel, SlackUserInterface $user)
    {
        $this->logger->debug('User invited to channel', [
            'channel' => $channel->getName(),
            'user' => $user->getName(),
        ]);
    }

    public function inviteUserToTeam($email)
    {
        $this->logger->debug('User invited to team', ['email' => $email]);
    }

    public function kickUser(SlackChannelInterface $channel, SlackUserInterface $user)
    {
        $this->logger->debug('User kicked', [
            'channel' => $channel->getName(),
            'user' => $user->getName(),
        ]);
    }

    public function listChannels($private = false)
    {
        throw new \Exception('SlackManagerInterface::listChannels not implemented');
    }

    public function listUsers()
    {
        throw new \Exception('SlackManagerInterface::listUsers not implemented');
    }

    public function sendMessage(SlackChannelInterface $channel, SlackMessageInterface $message, $from = 'admin')
    {
        $this->logger->debug('Message sended', [
            'channel' => $channel->getName(),
            'message' => $message->getText(),
            'from' => $from,
            'attachments' => \GuzzleHttp\json_decode($message->getAttachments(), true),
        ]);
    }

    public function userInfo(SlackUserInterface $user)
    {
        throw new \Exception('SlackManagerInterface::userInfo not implemented');
    }

}
