<?php
namespace WF\Slack\Notification;
use WF\Notification\Model\Notification;

/**
 * Class SlackNotification
 *
 * @package WF\Slack\Notification
 * @author artem <mail@artemd.ru>
 */
class SlackNotification extends Notification implements SlackNotificationInterface
{
    /**
     * @var array
     */
    private $attachments;

    /**
     * @param array $attachments
     * @return self
     */
    public function setAttachments(array $attachments = [])
    {
        $this->attachments = $attachments;
        return $this;
    }

    public function getAttachments()
    {
        return $this->attachments;
    }

    public function getEncodedAttachments()
    {
        $at = [];

        foreach ($this->getAttachments() as $a) {
            if(!empty($a)) {
                $at[] = $a;
            }
        }

        return json_encode($at);
    }
}
