<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12/13/16
 * Time: 11:36 PM
 */

namespace WF\Slack\Notification;

use WF\Notification\Model\NotificationInterface;

interface SlackNotificationInterface extends NotificationInterface
{
    /**
     * @return array
     */
    public function getAttachments();

    /**
     * @return string
     */
    public function getEncodedAttachments();
}
