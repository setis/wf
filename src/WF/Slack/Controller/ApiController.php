<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 08.04.2016
 * Time: 9:59
 */

namespace WF\Slack\Controller;


use models\SlackChannel;
use models\SlackMessage;
use models\SlackUser;
use models\User;
use models\Department;
use repository\SlackChannelRepository;
use repository\SlackUserRepository;
use repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WF\HttpKernel\RestController;
use WF\Slack\Model\SlackChannelInterface;
use WF\Slack\SlackManager;

/**
 * Class ApiController
 * @package WF\Slack\Controller
 */
class ApiController extends RestController
{
    /**
     * @param Request $request
     * @return Response
     */
    public function getUsersAction(Request $request)
    {
        $limit = $request->get('count');
        $offset = ($request->get('page', 1) - 1) * $limit;

        /** @var UserRepository $usersRepo */
        $usersRepo = $this->getEm()->getRepository(User::class);
        $result = $usersRepo->findByFio($request->get('fio'), $limit, $offset);

        return $this->renderJsonResponse($result, ['default', 'User.SlackUser']);
    }

    public function getDepartmentsAction(Request $request) {

        $departments = $this->getEm()->getRepository(Department::class)->findAll();

        return $this->renderJsonResponse($departments, ['default']);
    }


    /**
     * @param Request $request
     * @return Response
     */
    public function inviteUserAction(Request $request)
    {
        /** @var \Swift_Mailer $mailer */
        $mailer = $this->getContainer()->get('mailer');
        /** @var \Swift_Message $message */
        $message = $mailer->createMessage();
        $message
            ->setSubject('Вас приглашают в комманду Горсвязи!')
            ->setFrom( $this->getContainer()->getParameter('robot_email') )
            ->setTo($request->get('email'))
            ->setBody(
                '<h2>Вас приглашают в команду компании "Горсвязь"!</h2>
                <p>Зарегистрируйтесь по адресу <a href="http://gorserv.slack.com/signup">http://gorserv.slack.com/signup</a>, 
                используя Ваш почтовый ящик в домене gorser.ru.</p>
                <p>Скачать приложения для мобильных и десктопных платформ можно <a href="https://slack.com/downloads">здесь</a>.</p>',
                'text/html'
            );

        $header = $message->getHeaders();
        $header->addPathHeader('Return-Path', $this->getContainer()->getParameter('mailer_username'));

        $result = ['ok'=>true];
        try {
            $result['ok'] = $mailer->send($message) > 0;
        } catch (\Exception $e) {
            $result = ['error'=>'Cannot submit invite email!', 'ok'=>false];
        }

        $response = $this->renderJsonResponse($result, ['api']);
        if (true !== $result['ok']) {
            $response->setStatusCode(500);
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function sendMessageAction(Request $request)
    {
        $manager = $this->getManager();

        $channel = new SlackChannel();
        $channel->setId($request->get('channel'));

        $message = new SlackMessage();
        $message->setText($request->get('text'));

        $result = $manager->sendMessage($channel, $message);

        $response = $this->renderJsonResponse($result, ['api']);
        if (true !== $result['ok']) {
            $response->setStatusCode(500);
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getChannelsAction(Request $request)
    {
        $type = $request->get('type', SlackChannelInterface::CHANNEL_PUBLIC);

        /** @var SlackChannelRepository $repo */
        $repo = $this->getEm()->getRepository(SlackChannel::class);

        return $this->renderJsonResponse($repo->getChannels($type), ['default']);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addToChannelAction(Request $request)
    {
        $manager = $this->getManager();
        /** @var SlackChannelRepository $repo */
        $channelsRepo = $this->getEm()->getRepository(SlackChannel::class);

        /** @var SlackUserRepository $usersRepo */
        $usersRepo = $this->getEm()->getRepository(SlackUser::class);

        $channel = $channelsRepo->find($request->get('channel'));
        $user = $usersRepo->find($request->get('user'));

        $result = $manager->inviteToChannel($channel, $user);

        $response = $this->renderJsonResponse($result, ['api']);
        if (!($result instanceof SlackChannel) && true !== $result['ok']) {
            $response->setStatusCode(500);
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function removeFromChannelAction(Request $request)
    {
        $manager = $this->getManager();
        /** @var SlackChannelRepository $repo */
        $channelsRepo = $this->getEm()->getRepository(SlackChannel::class);

        /** @var SlackUserRepository $usersRepo */
        $usersRepo = $this->getEm()->getRepository(SlackUser::class);

        $channel = $channelsRepo->find($request->get('channel'));
        $user = $usersRepo->find($request->get('user'));

        $result = $manager->kickUser($channel, $user);

        $response = $this->renderJsonResponse($result, ['api']);
        if (true !== $result['ok']) {
            $response->setStatusCode(500);
        }

        return $response;

    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createChannelAction(Request $request)
    {
        $manager = $this->getManager();

        $channel = new SlackChannel();
        $channel->setName($request->get('name'))
            ->setType($request->get('type'));

        $result = $manager->createChannel($channel);

        $response = $this->renderJsonResponse($result, ['api']);
        if (!($result instanceof SlackChannel) && true !== $result['ok']) {
            $response->setStatusCode(500);
        }

        return $response;
    }

    /**
     * @return SlackManager
     */
    private function getManager()
    {
        return $this->getContainer()->get('wf.slack.manager');
    }
}
