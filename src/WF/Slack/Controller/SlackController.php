<?php

namespace WF\Slack\Controller;

use Symfony\Component\HttpFoundation\Request;
use WF\HttpKernel\AbstractController;


class SlackController extends AbstractController
{
    public function usersAction(Request $request)
    {
        return $this->renderResponse('Slack/users.html.php', [

        ]);
    }

    public function channelsAction(Request $request)
    {
        return $this->renderResponse('Slack/channels.html.php', [

        ]);
    }

    public function groupsAction(Request $request)
    {
        return $this->renderResponse('Slack/groups.html.php', [

        ]);
    }
}
