<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 23.08.16
 * Time: 12:36
 */

namespace WF\Slack;

use Symfony\Component\DependencyInjection\ContainerInterface;
use WF\Slack\Model\SlackBotFactoryInterface;

class SlackBotFactory implements SlackBotFactoryInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * SlackBotFactory constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct( ContainerInterface $container )
    {
        $this->container = $container;
    }


    public function create( $key )
    {
        $serviceKey = "slack.{$key}_bot";
        if ( !$this->container->has( $serviceKey ) ) {
            throw new \InvalidArgumentException( 'Slack does not have ' . $key . ' bot! (service key ' . $serviceKey . ')' );
        }

        return $this->container->get( $serviceKey );
    }

}
