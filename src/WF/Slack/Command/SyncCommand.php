<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 16.04.2016
 * Time: 11:34
 */

namespace WF\Slack\Command;


use Doctrine\ORM\EntityManager;
use models\SlackChannel;
use models\SlackUser;
use models\User;
use Psr\Log\LoggerInterface;
use repository\SlackChannelRepository;
use repository\SlackUserRepository;
use repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use WF\Slack\Model\SlackManagerInterface;

class SyncCommand extends Command
{

    /**
     * @var SlackManagerInterface
     */
    private $slackManager;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;
    
    public function __construct(SlackManagerInterface $slackManager, EntityManager $em, LoggerInterface $logger)
    {
        parent::__construct();
        $this->slackManager = $slackManager;
        $this->em = $em;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this
            ->setName('slack:sync')
            ->setDescription('Sync users and groups from youre Slack team');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ma = $this->getSlackManager();

        $message = date('Y-m-d H:i:s')." start sync... ";
        $this->getLogger()->debug(__METHOD__, ['message'=>$message]);

        /** @var SlackUserRepository $slackUserRepo */
        $slackUserRepo = $this->getEm()->getRepository(SlackUser::class);
        /** @var SlackChannelRepository $channelRepo */
        $channelRepo = $this->getEm()->getRepository(SlackChannel::class);
        /** @var UserRepository $userRepo */
        $userRepo = $this->getEm()->getRepository(User::class);

        /**
         * Collect external channels with attached users
         */
        /** @var SlackChannel[] $ext_channels */
        $ext_channels = array_merge($ma->listChannels(), $ma->listChannels(true));
        $ext_ids = [];
        foreach ($ext_channels as $channel) {
            $ext_users_ids = [];
            foreach ($channel->getUsers() as $user) {
                $ext_users_ids[] = $user->getId();
                $user->setUser( $userRepo->findFirstByEmail($user->getEmail()) );
                $this->getEm()->persist($user);
            }
            $this->getEm()->persist($channel);
            $ext_ids[$channel->getId()] = $ext_users_ids;
        }
        $this->getEm()->flush();

        /**
         * Collect internal channels with attached users
         */
        $int_channels = $channelRepo->getAllChannels();
        $int_ids = [];
        foreach ($int_channels as $channel) {
            $int_users_ids = [];
            foreach ($channel->getUsers() as $user) {
                $ext_users_ids[] = $user->getId();
            }
            $int_ids[$channel->getId()] = $int_users_ids;
        }

        /**
         * Remove unsynced local channels
         */
        $delete_channels = array_diff(array_keys($int_ids), array_keys($ext_ids));
        foreach ($delete_channels as $channel_id) {
            $this->getEm()->remove( $channelRepo->find($channel_id) );
        }
        $this->getEm()->flush();

        /**
         * Remove users from unsynced channels
         */
        foreach ($ext_ids as $channel_id => $user_ids) {
            $delete_users = array_diff( $int_ids[$channel_id], $user_ids);
            if(!empty($delete_users)) {
                /** @var SlackChannel $channel */
                $channel = $channelRepo->find($channel_id);
                foreach ($delete_users as $user_id) {
                    /** @var SlackUser $user */
                    $user = $slackUserRepo->find($user_id);
                    $user->removeChannel($channel);
                    $this->getEm()->persist($user);
                }
            }
        }
        $this->getEm()->flush();

        $message = date('Y-m-d H:i:s'). " done!";
        $this->getLogger()->debug(__CLASS__.'::'.__METHOD__, ['message'=>$message]);
    }

    /**
     * @param EntityManager $em
     */
    public function setEm(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param SlackManagerInterface $slackManager
     */
    public function setSlackManager(SlackManagerInterface $slackManager)
    {
        $this->slackManager = $slackManager;
    }

    /**
     * @return SlackManagerInterface
     */
    private function getSlackManager()
    {
        return $this->slackManager;
    }

    /**
     * @return EntityManager
     */
    private function getEm(){
        return $this->em;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }
}