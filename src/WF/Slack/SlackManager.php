<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 07.04.2016
 * Time: 16:27
 */

namespace WF\Slack;

use Doctrine\ORM\EntityManagerInterface;
use models\SlackChannel;
use models\SlackUser;
use Psr\Log\LoggerInterface;
use WF\Slack\Model\SlackBotFactoryInterface;
use WF\Slack\Model\SlackChannelInterface;
use WF\Slack\Model\SlackManagerInterface;
use WF\Slack\Model\SlackMessageInterface;
use WF\Slack\Model\SlackUserInterface;


/**
 * Class SlackManager
 *
 * @package WF\Slack
 */
class SlackManager implements SlackManagerInterface
{

    /**
     *
     * @var \Doctrine\ORM\EntityManagerInterface;
     */
    private $em;

    /**
     * @var string
     */
    private $team;

    private $defaultBotKey;

    private $botFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SlackManager constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface     $em
     * @param                                          $defaultBotKey
     * @param \WF\Slack\Model\SlackBotFactoryInterface $botFactory
     * @param \Psr\Log\LoggerInterface                 $logger
     * @param null                                     $team
     */
    public function __construct( EntityManagerInterface $em,
                                 $defaultBotKey,
                                 SlackBotFactoryInterface $botFactory,
                                 LoggerInterface $logger,
                                 $team = null
    )
    {
        $this->em = $em;
        $this->defaultBotKey = $defaultBotKey;
        $this->botFactory = $botFactory;
        $this->team = $team;
        $this->logger = $logger;
    }

    /**
     * @deprecated Slack accepts this command no more
     *
     * @param $email
     *
     * @return array
     */
    public function inviteUserToTeam( $email )
    {
        if ( empty( $this->team ) )
            throw new \LogicException( 'To use users.admin.invite API method you must provide slack team name.' );

        $this->botFactory->create( $this->defaultBotKey )->setHostEndpoint( $this->team );

        $result = $this->clientCall( 'users.admin.invite', [
            'email' => $email,
        ] );

        $this->botFactory->create( $this->defaultBotKey )->setHostEndpoint( 'api.slack.com' );

        if ( false === $result[ 'ok' ] ) {
            $this->logger->warning( __CLASS__ . '::' . __METHOD__, [ 'result' => json_encode( $result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE ) ] );
        } else {

        }

        return $result;
    }

    /**
     * @param bool $private
     *
     * @return SlackChannelInterface[]|bool
     */
    public function listChannels( $private = false )
    {
        $method = 'channels.list';
        if ( $private ) {
            $method = 'groups.list';
        }

        $result = $this->clientCall( $method );

        if ( false === $result[ 'ok' ] ) {
            $this->logger->warning( __CLASS__ . '::' . __METHOD__, [ 'result' => json_encode( $result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE ) ] );

            return $result;
        }

        return $this->parseChannels( $result );
    }


    /**
     * @param SlackChannelInterface $channel
     *
     * @return array|SlackChannelInterface
     */
    public function createChannel( SlackChannelInterface $channel )
    {
        switch ( $channel->getType() ) {
            case SlackChannelInterface::CHANNEL_PRIVATE:
                $method = 'groups.create';
                break;
            default:
                $method = 'channels.create';
        }

        $result = $this->clientCall( $method, [
            'name' => $channel->getName(),
        ] );

        if ( false === $result[ 'ok' ] ) {
            $this->logger->warning( __CLASS__ . '::' . __METHOD__, [ 'result' => json_encode( $result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE ) ] );

            return $result;
        }

        return $this->parseChannel( $result[ $channel->getType() ] );
    }

    /**
     * @param SlackChannelInterface $channel
     *
     * @return array
     */
    public function archiveChannel( SlackChannelInterface $channel )
    {
        switch ( $channel->getType() ) {
            case SlackChannelInterface::CHANNEL_PRIVATE:
                $method = 'groups.archive';
                break;
            default:
                $method = 'channels.archive';
        }

        $result = $this->clientCall( $method, [
            'channel' => $channel->getId(),
        ] );

        if ( true === $result[ 'ok' ] ) {

        }

        return $result;
    }

    /**
     * @param SlackChannelInterface $channel
     * @param SlackUserInterface    $user
     *
     * @return array
     */
    public function inviteToChannel( SlackChannelInterface $channel, SlackUserInterface $user )
    {
        switch ( $channel->getType() ) {
            case SlackChannelInterface::CHANNEL_PRIVATE:
                $method = 'groups.invite';
                break;
            default:
                $method = 'channels.invite';
        }

        $result = $this->clientCall( $method, [
            'channel' => $channel->getId(),
            'user'    => $user->getId(),
        ] );

        if ( true === $result[ 'ok' ] ) {
            $channel->addUser( $user );
            $user->addChannel( $channel );
            $this->em->persist( $channel );
            $this->em->persist( $user );
            $this->em->flush();

            return $channel;
        }

        return $result;
    }

    /**
     * @param SlackChannelInterface $channel
     * @param SlackUserInterface    $user
     *
     * @return array
     */
    public function kickUser( SlackChannelInterface $channel, SlackUserInterface $user )
    {
        switch ( $channel->getType() ) {
            case SlackChannelInterface::CHANNEL_PRIVATE:
                $method = 'groups.kick';
                break;
            default:
                $method = 'channels.kick';
        }

        $result = $this->clientCall( $method, [
            'channel' => $channel->getId(),
            'user'    => $user->getId(),
        ] );

        if ( false === $result[ 'ok' ] ) {
            $this->logger->warning( __CLASS__ . '::' . __METHOD__, [ 'result' => json_encode( $result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE ) ] );
        } else {
            $user->removeChannel( $channel );
            $this->em->persist( $user );
            $this->em->flush();
        }

        return $result;
    }

    /**
     * @return array|SlackUserInterface[]
     */
    public function listUsers()
    {
        $method = 'users.list';

        $result = $this->clientCall( $method );

        if ( false === $result[ 'ok' ] ) {
            $this->logger->warning( __CLASS__ . '::' . __METHOD__, [ 'result' => json_encode( $result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE ) ] );

            return $result;
        }

        return $this->parseUsers( $result );
    }

    /**
     * @param SlackUserInterface $user
     *
     * @return SlackUser|SlackUserInterface
     */
    public function userInfo( SlackUserInterface $user )
    {
        $method = 'users.info';

        $result = $this->clientCall( $method, [
            'user' => $user->getId(),
        ] );

        if ( false === $result[ 'ok' ] ) {
            $this->logger->warning( __CLASS__ . '::' . __METHOD__, [ 'result' => json_encode( $result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE ) ] );

            return $result;
        }

        return $this->parseUser( $result[ 'user' ] );
    }

    /**
     * @param SlackChannelInterface $channel
     * @param SlackMessageInterface $message
     *
     * @return bool
     */
    public function sendMessage( SlackChannelInterface $channel, SlackMessageInterface $message, $from = 'slackbot' )
    {
        $method = 'chat.postMessage';

        $result = $this->clientCall( $method, [
            'text'        => $message->getText(),
            'attachments' => $message->getAttachments(),
            'channel'     => $channel->getId(),
            'as_user'     => true,
        ], $from );

        return $result;
    }

    /**
     * @param $result
     *
     * @return SlackChannelInterface[]
     */
    protected function parseChannels( $result )
    {
        if ( false === $result[ 'ok' ] )
            return [ ];

        $channels = [ ];

        if ( isset( $result[ 'groups' ] ) && !empty( $result[ 'groups' ] ) ) {
            foreach ($result[ 'groups' ] as $group) {
                $channels[] = $this->parseChannel( $group );
            }
        }

        if ( isset( $result[ 'channels' ] ) && !empty( $result[ 'channels' ] ) ) {
            foreach ($result[ 'channels' ] as $channel) {
                $channels[] = $this->parseChannel( $channel );
            }
        }

        return $channels;
    }

    /**
     * @param $result
     *
     * @return SlackChannelInterface
     */
    protected function parseChannel( $result )
    {

        if ( empty( $channel = $this->em->find( SlackChannel::class, $result[ 'id' ] ) ) )
            $channel = new SlackChannel();

        $channel->setId( $result[ 'id' ] )
            ->setName( $result[ 'name' ] );

        if ( isset( $result[ 'is_channel' ] ) )
            $channel->setType( SlackChannelInterface::CHANNEL_PUBLIC );

        if ( isset( $result[ 'is_group' ] ) )
            $channel->setType( SlackChannelInterface::CHANNEL_PRIVATE );

        if ( isset( $result[ 'members' ] ) ) {
            foreach ($result[ 'members' ] as $user_id) {
                if ( $user = $this->userInfo( ( new SlackUser() )->setId( $user_id ) ) ) {
                    $channel->addUser( $user );
                    $user->addChannel( $channel );
                }
            }
        }

        $this->em->persist( $channel );
        $this->em->flush();

        return $channel;
    }

    /**
     * @param $result
     *
     * @return SlackUserInterface[]
     */
    protected function parseUsers( $result )
    {
        if ( false === $result[ 'ok' ] )
            return [ ];

        $users = [ ];
        foreach ($result[ 'members' ] as $member) {
            $user = $this->parseUser( $member );
            if ( false !== $user )
                $users[] = $user;
        }

        return $users;
    }

    /**
     * @param $result
     *
     * @return SlackUserInterface
     */
    protected function parseUser( $result )
    {
        if ( $result[ 'is_bot' ] )
            return false;

        if ( empty( $user = $this->em->find( SlackUser::class, $result[ 'id' ] ) ) ) {
            $user = new SlackUser();
        }

        $user->setId( $result[ 'id' ] )
            ->setName( $result[ 'name' ] )
            ->setDeleted( $result[ 'deleted' ] );

        if ( isset( $result[ 'profile' ] ) ) {
            if ( isset( $result[ 'profile' ][ 'real_name' ] ) )
                $user->setRealName( $result[ 'profile' ][ 'real_name' ] );
            if ( isset( $result[ 'profile' ][ 'email' ] ) )
                $user->setEmail( $result[ 'profile' ][ 'email' ] );
        }

        $this->em->persist( $user );
        $this->em->flush();

        return $user;
    }

    /**
     * @param string $key
     * @param string $method
     * @param array  $data
     *
     * @return array
     */
    private function clientCall( $method, $data = [ ], $key = 'slackbot' )
    {
        return $this->botFactory->create( $key )->call( $method, $data );
    }
}
