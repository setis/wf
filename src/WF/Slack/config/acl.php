<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 18.04.2016
 * Time: 10:52
 */
global $PLUGINS;

$plugin_uid = \WF\Slack\Controller\SlackController::class;
$PLUGINS[$plugin_uid]['name'] = "Интеграция со Slack";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = \WF\Slack\Controller\ApiController::class;
$PLUGINS[$plugin_uid]['name'] = "Интеграция со Slack API";
$PLUGINS[$plugin_uid]['hidden'] = true;
