<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 07.04.2016
 * Time: 18:43
 */

namespace WF\Slack\Model;


interface SlackChannelInterface
{

    const CHANNEL_PUBLIC = 'channel';

    const CHANNEL_PRIVATE = 'group';

    const CHANNEL_IM = 'im';

    public function getId();

    public function getName();

    public function getType();

}