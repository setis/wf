<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 07.04.2016
 * Time: 18:43
 */

namespace WF\Slack\Model;


interface SlackMessageInterface
{

    public function getText();

    public function getName();

    public function getAttachments();

}
