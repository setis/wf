<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 07.04.2016
 * Time: 18:43
 */

namespace WF\Slack\Model;


interface SlackUserInterface
{

    public function getId();

    public function getName();

    public function getDeleted();

    public function getEmail();

    public function getRealName();
}