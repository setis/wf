<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 07.04.2016
 * Time: 18:28
 */

namespace WF\Slack\Model;


/**
 * Interface SlackManagerInterface
 * @package WF\Slack\Model
 */
interface SlackManagerInterface
{

    /**
     * @param string $email
     * @return bool
     */
    public function inviteUserToTeam($email);

    /**
     * @param bool $private
     * @return SlackChannelInterface[]|bool
     */
    public function listChannels($private = false);

    /**
     * @param SlackChannelInterface $channel
     * @return bool|SlackChannelInterface
     */
    public function createChannel(SlackChannelInterface $channel);

    /**
     * @param SlackChannelInterface $channel
     * @return bool
     */
    public function archiveChannel(SlackChannelInterface $channel);

    /**
     * @param SlackChannelInterface $channel
     * @param SlackUserInterface $user
     * @return bool
     */
    public function inviteToChannel(SlackChannelInterface $channel, SlackUserInterface $user);

    /**
     * @param SlackChannelInterface $channel
     * @param SlackUserInterface $user
     * @return bool
     */
    public function kickUser(SlackChannelInterface $channel, SlackUserInterface $user);

    /**
     * @return bool|SlackUserInterface[]
     */
    public function listUsers();

    /**
     * @param SlackUserInterface $user
     * @return bool|SlackUserInterface
     */
    public function userInfo(SlackUserInterface $user);

    /**
     * @param SlackChannelInterface $channel
     * @param SlackMessageInterface $message
     * @param string $from
     * @return bool
     */
    public function sendMessage(SlackChannelInterface $channel, SlackMessageInterface $message, $from = '');

}
