<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 23.08.16
 * Time: 12:43
 */

namespace WF\Slack\Model;


use tigokr\Slack\Slack;

interface SlackBotFactoryInterface
{
    /**
     * @param string $key
     *
     * @return Slack
     */
    public function create($key);
}
