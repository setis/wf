<?php
namespace WF\Slack\Formatter;

use InvalidArgumentException;
use models\ListAddr;
use models\Task;
use models\Ticket;
use Symfony\Component\Security\Core\User\UserInterface;
use WF\Core\TaskInterface;
use WF\Task\Model\TaskCommentInterface;

/**
 * Class TechnicalTicketFormatter
 *
 * @package WF\Slack\Formatter
 * @author artem <mail@artemd.ru>
 */
class TechnicalTicketFormatter extends SlackFormatter
{

    /**
     * @var TaskCommentInterface
     */
    private $comment;

    /**
     * @var UserInterface
     */
    private $author;

    /**
     * @var Task
     */
    private $task;

    /**
     * @var string
     */
    private $link;

    public function setComment(TaskCommentInterface $comment)
    {
        $this->comment = $comment;
        return $this;
    }

    public function setAuthor(UserInterface $author) {
        $this->author = $author;
        return $this;
    }

    public function setTask(Task $task){
        $this->task = $task;
        return $this;
    }

    public function setTaskLink($link) {
        $this->link = $link;
        return $this;
    }

    /**
     * TODO implement this to use technical ticket
     *
     * @return array
     */
    public function getAttachments()
    {
        $at = $this->notification->getAttachments();

        if (null === $this->task) {
            throw new InvalidArgumentException('This formatter need Task object!');
        }

        $c = count($at);

        $ticket = $this->task->getAnyTicket();

        $at[$c] = [
            "fallback" => $this->notification->getSubject(),
            "color" => $this->task->getStatus()->getColor(),
            "title" => "№{$this->task->getId()} {$this->task->getTaskTitle()}",
            "title_link" => $this->link,
            "text" => strip_tags(html_entity_decode($this->task->getTaskComment())),
            "unfurl_links"=> true,
            "fields" => [
                [
                    'title' => 'Дата создания',
                    'value' => $this->task->getDateReg()->format('Y/m/d'),
                    'short' => true,
                ],
                [
                    'title' => 'Статус',
                    'value' => '_*'.$this->task->getStatus()->getName().'*_',
                    'short' => true,
                ],
            ],
            "mrkdwn" => true,
            "mrkdwn_in"=> ['fields']
        ];

        if($ticket instanceof Ticket && null !== $ticket->getDom()) {
            $at[$c]['fields'][] = [
                'title' => 'Адрес',
                'value' => "<https://maps.yandex.ru/?mode=search&text={$ticket->getDom()}|{$ticket->getDom()}>",
                'short' => false,
            ];
        }

        return $at;
    }
}
