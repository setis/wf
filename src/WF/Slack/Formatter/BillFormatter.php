<?php
namespace WF\Slack\Formatter;

use InvalidArgumentException;
use models\BillPayment;
use models\BillType;
use models\Task;
use models\User;
use Symfony\Component\Security\Core\User\UserInterface;
use WF\Core\TaskInterface;
use WF\Task\Model\TaskCommentInterface;

/**
 * Class BillFormatter
 *
 * @package WF\Slack\Formatter
 * @author artem <mail@artemd.ru>
 */
class BillFormatter extends SlackFormatter
{
    /**
     * @var Task
     */
    private $task;

    /**
     * @var string
     */
    private $link;

    public function setTask(Task $task)
    {
        $this->task = $task;
        return $this;
    }

    public function setTaskLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * TODO implement this to use Bill ticket
     *
     * @return array
     */
    public function getAttachments()
    {
        $at = $this->notification->getAttachments();

        if (null === $this->task) {
            throw new InvalidArgumentException('This formatter need Task object!');
        }

        $fio = $this->task->getAuthor() ? $this->task->getAuthor()->getFio() : 'GERP';
        if(null !== $this->task->getAuthor() && null !== $this->task->getAuthor()->getSlackUser()) {
            $fio = '<@'.$this->task->getAuthor()->getSlackUser()->getId().'> '.$fio;
        }

        $c = count($at);
        $payed = array_sum( $this->task->getBill()->getPayments()->map(function(BillPayment $e){
            return $e->getAmount();
        })->toArray());

        $at[$c] = [
            "fallback" => $this->notification->getSubject(),
            "color" => $this->task->getStatus()->getColor(),
            "title" => "№{$this->task->getId()} {$this->task->getTaskTitle()}",
            "title_link" => $this->link,
            "text" => strip_tags(html_entity_decode($this->task->getTaskComment())),
            "fields" => [
                [
                    'title' => 'Автор',
                    'value' => !$this->task->getAuthor() instanceOf User ? 'GERP' : $fio,
                    'short' => true,
                ],
                [
                    'title' => 'Тип счёта',
                    'value' => $this->task->getBill()->getType() instanceof BillType ? $this->task->getBill()->getType()->getTitle() : '-',
                    'short' => true,
                ],
                [
                    'title' => 'Дата создания',
                    'value' => $this->task->getDateReg()->format('Y/m/d'),
                    'short' => true,
                ],
                [
                    'title' => 'Статус',
                    'value' => '_*' . $this->task->getStatus()->getName() . '*_',
                    'short' => true,
                ],
                [
                    'title' => 'Сумма',
                    'value' => "{$payed} ({$this->task->getBill()->getAmount()})",
                    'short' => true,
                ],
                [
                    'title' => 'Дата платежа',
                    'value' => $this->task->getBill()->getPaymentDate() instanceOf \DateTime ? $this->task->getBill()->getPaymentDate()->format('Y/m/d') : '-',
                    'short' => true,
                ]
            ],
            "mrkdwn" => true,
            "mrkdwn_in" => ['fields']
        ];

        return $at;
    }
}
