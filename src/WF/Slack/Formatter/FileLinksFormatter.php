<?php
namespace WF\Slack\Formatter;

use WF\Task\Model\TaskCommentInterface;

/**
 * Class FileLinksFormatter
 *
 * @package WF\Slack\Formatter
 * @author artem <mail@artemd.ru>
 */
class FileLinksFormatter extends SlackFormatter
{
    /**
     * @var TaskCommentInterface
     */
    private $comment;

    public function setComment(TaskCommentInterface $comment)
    {
        $this->comment = $comment;
        return $this;
    }

    public function getAttachments()
    {
        $at = $this->notification->getAttachments();

        if(null === $this->comment) {
            return $at;
        }

        if ($this->comment->getTag() && preg_match_all('/href=[\'"]([^\'"]*)[\'"][^>]*>([^<]*)<\/a>/siU', $this->comment->getTag(), $files) > 0) {
            $files = array_combine($files[2], $files[1]);
            $files = array_map(function ($v) {
                return link2($v);
            }, $files);
        } else {
            $files = [];
        };

        if (sizeof($files)>0) {
            $at[]['fields'][] = [
                'title' => 'Файлы/Ссылки',
                'value' => implode("\n", array_map(function ($k, $v) {
                    return "<{$v}|{$k}>";
                }, array_keys($files), array_values($files))),
            ];
        }

        return $at;
    }
}
