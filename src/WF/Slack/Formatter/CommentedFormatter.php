<?php
namespace WF\Slack\Formatter;
use models\TaskComment;
use WF\Task\Model\TaskCommentInterface;
use WF\Users\Model\GenderInterface;

/**
 * Class CommentedFormatter
 *
 * @package WF\Slack\Formatter
 * @author artem <mail@artemd.ru>
 */
class CommentedFormatter extends SlackFormatter
{
    /**
     * @var TaskCommentInterface
     */
    private $comment;

    /**
     * @return TaskCommentInterface
     */
    public function getComment(): TaskCommentInterface
    {
        return $this->comment;
    }

    /**
     * @param TaskCommentInterface $comment
     * @return $this
     */
    public function setComment(TaskCommentInterface $comment)
    {
        $this->comment = $comment;
        return $this;
    }

    public function getAttachments()
    {
        $at = $this->notification->getAttachments();

        if(!$this->comment instanceof TaskComment) {
            return $at;
        }

        $comment = trim(strip_tags(html_entity_decode($this->comment->getText())));

        $fio = $this->comment->getAuthor() ? $this->comment->getAuthor()->getFio() : 'GERP';
        $gender = $this->comment->getAuthor() && $this->comment->getAuthor()->getGender() == GenderInterface::GENDER_FEMALE ? 'а' : '';
        $color = $this->comment->getStatus() ? $this->comment->getStatus()->getColor() : null;

        if(null !== $this->comment->getAuthor() && null !== $this->comment->getAuthor()->getSlackUser()) {
            $fio = '<@'.$this->comment->getAuthor()->getSlackUser()->getId().'> '.$fio;
        }

        $c = count($at);
        $at[$c]['text'] = $fio . ' написал' . $gender.':';
        $at[$c]['color'] = $color;
        $at[$c]['mrkdwn'] = true;
        $at[$c]['mrkdwn_in'] = ['fields', 'text'];
        $at[$c]['fields'][] = [
            'value' => $comment,
        ];

        if ($tag = $this->comment->getTag()) {

            $color = $this->comment->getStatus() ? $this->comment->getStatus()->getColor() : null;
            $tag = trim(strip_tags(preg_replace('/Добавлен файл\(ы\):.*<\/a>/', '', $tag)));

            if(!empty($tag)) {
                $at[$c]['fields'][] = [
//                    'title' => 'Тег',
                    'value' => '_'.$tag.'_',
                ];
                $at[$c]['color'] = $color;
            }
        }

        return $at;
    }

}
