<?php
namespace WF\Slack\Formatter;

/**
 * Class CustomMessageFormatter
 *
 * @package WF\Slack\Formatter
 * @author artem <mail@artemd.ru>
 */
class CustomMessageFormatter extends SlackFormatter
{
    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $color;

    public function setMessage($message, $color = null)
    {
        $this->message = $message;
        $this->color = $color;
        return $this;
    }

    public function getAttachments()
    {
        $at = $this->notification->getAttachments();

        if (null === $this->message) {
            return $at;
        }

        $c = count($at);
        $at[$c]['color'] = $this->color;
        $at[$c]['fallback'] = $this->message;
        $at[$c]['mrkdwn'] = true;
        $at[$c]['mrkdwn_in'] = ['fields'];
        $at[$c]['fields'][] = [
            'value' => $this->message,
        ];

        return $at;
    }
}
