<?php
namespace WF\Slack\Formatter;

use models\Task;
use RuntimeException;
use WF\Core\TaskInterface;
use WF\Slack\Notification\SlackNotificationInterface;

/**
 * Class TicketFormatterFactory
 *
 * @package WF\Slack\Formatter
 * @author artem <mail@artemd.ru>
 */
class TicketFormatterSimpleFactory
{
    /**
     * @param TaskInterface $task
     * @param SlackNotificationInterface $notification
     * @return SlackNotificationInterface
     * @throws RuntimeException
     */
    static public function getTicketFormatter(TaskInterface $task, SlackNotificationInterface $notification): SlackNotificationInterface
    {

        switch ($task->getPluginUid()) {
            case Task::ACCIDENT:
            case Task::SERVICE:
            case Task::TELEMARKETING:
            case Task::CONNECTION:
                return (new TechnicalTicketFormatter($notification))
                    ->setTask($task);
                break;
            case Task::GLOBAL_PROBLEM:
                return (new GpTicketFormatter($notification))
                    ->setTask($task);
                break;
            case Task::PROJECT:
                return (new ProjectFormatter($notification))
                    ->setTask($task);
                break;
            case Task::BILL:
                return (new BillFormatter($notification))
                    ->setTask($task);
                break;
        }

        throw new RuntimeException('Unknown ticket type ' . $task->getPluginUid());
    }
}
