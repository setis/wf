<?php
namespace WF\Slack\Formatter;

use models\User;

/**
 * Class BoundedUsersChangeFormatter
 *
 * @package WF\Slack\Formatter
 * @author artem <mail@artemd.ru>
 */
class BoundedUsersChangeFormatter extends SlackFormatter
{

    /**
     * @var User[]
     */
    private $addedUsers = [];

    /**
     * @var User[]
     */
    private $removedUsers = [];

    /**
     * @param array $users
     * @return $this
     */
    public function setAddedUsers(array $users = [])
    {
        $this->addedUsers = $users;
        return $this;
    }

    /**
     * @param array $users
     * @return $this
     */
    public function setRemovedUsers(array $users = [])
    {
        $this->removedUsers = $users;
        return $this;
    }

    public function getAttachments()
    {
        $at = $this->notification->getAttachments();

        $c = count($at);
        if (!empty($this->removedUsers)) {
            $at[$c]['color'] = 'green';
            $at[$c]['fields'] = [
                [
                    'title' => 'Удалённые пользователи',
                    'value' => $this->collectUsers($this->removedUsers),
                    'short' => false
            ]];
        }

        $c = count($at);
        if (!empty($this->addedUsers)) {
            $at[$c]['color'] = 'red';
            $at[$c]['fields'] = [
                [
                    'title' => 'Добавленные пользователи',
                    'value' => $this->collectUsers($this->addedUsers),
                    'short' => false
                ]
            ];
        }

        return $at;
    }

    private function collectUsers(array $users)
    {
        return implode("\n", array_map(function (User $user) {
            return $user->getFio();
        }, $users));
    }

}
