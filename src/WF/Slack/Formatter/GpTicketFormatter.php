<?php
namespace WF\Slack\Formatter;

use InvalidArgumentException;
use models\Gp;
use models\Task;
use Symfony\Component\Security\Core\User\UserInterface;
use WF\Core\TaskInterface;
use WF\Task\Model\TaskCommentInterface;

/**
 * Class GpTicketFormatter
 *
 * @package WF\Slack\Formatter
 * @author artem <mail@artemd.ru>
 */
class GpTicketFormatter extends SlackFormatter
{

    /**
     * @var TaskCommentInterface
     */
    private $comment;

    /**
     * @var UserInterface
     */
    private $author;

    /**
     * @var Task
     */
    private $task;

    /**
     * @var string
     */
    private $link;

    public function setComment(TaskCommentInterface $comment)
    {
        $this->comment = $comment;
        return $this;
    }

    public function setAuthor(UserInterface $author) {
        $this->author = $author;
        return $this;
    }

    public function setTask(Task $task){
        $this->task = $task;
        return $this;
    }

    public function setTaskLink($link) {
        $this->link = $link;
        return $this;
    }

    /**
     * TODO implement this to use technical ticket
     *
     * @return array
     */
    public function getAttachments()
    {
        $at = $this->notification->getAttachments();

        if (null === $this->task) {
            throw new InvalidArgumentException('This formatter need Task object!');
        }

        if(!$this->task->getAnyTicket() instanceof Gp) {
            return $at;
        }

        $c = count($at);

        $at[$c] = [
            "fallback" => $this->notification->getSubject(),
            "color" => $this->task->getStatus()->getColor(),
            "title" => "№{$this->task->getId()} {$this->task->getTaskTitle()}",
            "title_link" => $this->link,
            "text" => strip_tags(html_entity_decode($this->task->getTaskComment())),
            "fields" => [
                [
                    'title' => 'Дата создания',
                    'value' => $this->task->getDateReg()->format('Y/m/d'),
                    'short' => true,
                ],
                [
                    'title' => 'Статус',
                    'value' => '_*'.$this->task->getStatus()->getName().'*_',
                    'short' => true,
                ],
                [
                    'title' => 'Приоритет (B+C)',
                    'value' => $this->task->getGp()->getOpB2b() .' + ' .$this->task->getGp()->getOpB2c(),
                    'short' => true,
                ],
                [
                    'title' => 'Адресов',
                    'value' => $this->task->getAddresses()->count(),
                    'short' => true,
                ],
            ],
            "mrkdwn" => true,
            "mrkdwn_in"=> ['fields']
        ];
        return $at;
    }
}
