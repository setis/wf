<?php
namespace WF\Slack\Formatter;

use DateTime;
use InvalidArgumentException;
use models\ProjectType;
use models\Task;
use models\User;
use Symfony\Component\Security\Core\User\UserInterface;
use WF\Core\TaskInterface;
use WF\Task\Model\TaskCommentInterface;

/**
 * Class ProjectFormatter
 *
 * @package WF\Slack\Formatter
 * @author artem <mail@artemd.ru>
 */
class ProjectFormatter extends SlackFormatter
{

    /**
     * @var Task
     */
    private $task;

    /**
     * @var string
     */
    private $link;

    public function setTask(Task $task)
    {
        $this->task = $task;
        return $this;
    }

    public function setTaskLink($link)
    {
        $this->link = $link;
        return $this;
    }

    public function getAttachments()
    {
        $at = $this->notification->getAttachments();

        if (null === $this->task) {
            throw new InvalidArgumentException('This formatter need Task object!');
        }

        $fio = $this->task->getAuthor() ? $this->task->getAuthor()->getFio() : 'GERP';
        if(null !== $this->task->getAuthor() && null !== $this->task->getAuthor()->getSlackUser()) {
            $fio = '<@'.$this->task->getAuthor()->getSlackUser()->getId().'> '.$fio;
        }

        $c = count($at);

        $at[$c] = [
            "fallback" => $this->notification->getSubject(),
            "color" => $this->task->getStatus()->getColor(),
            "title" => "№{$this->task->getId()} {$this->task->getTaskTitle()}",
            "title_link" => $this->link,
            "text" => strip_tags(html_entity_decode($this->task->getTaskComment())),
            "fields" => [
                [
                    'title' => 'Автор',
                    'value' => !$this->task->getAuthor() instanceOf User ? 'GERP' : $fio,
                    'short' => true,
                ],
                [
                    'title' => 'Тип проекта',
                    'value' => $this->task->getProject()->getType() instanceOf ProjectType ? $this->task->getProject()->getType()->getTitle() : '(без типа)',
                    'short' => true,
                ],
                [
                    'title' => 'Дата создания',
                    'value' => $this->task->getDateReg()->format('Y/m/d'),
                    'short' => true,
                ],
                [
                    'title' => 'Deadline',
                    'value' => (!empty($this->task->getProject()->getDeadline()) ? '_'.$this->task->getProject()->getDeadline()->format('Y/m/d').'_' : '(не установлен)'),
                    'short' => true,
                ],
                [
                    'title' => 'Статус',
                    'value' => '_*'.$this->task->getStatus()->getName().'*_',
                    'short' => true,
                ],
                [
                    'title' => 'Приоритет',
                    'value' => $this->task->getProject()->getPriority(),
                    'short' => true,
                ]
            ],
            "mrkdwn" => true,
            "mrkdwn_in"=> ['fields']
        ];

        if(!empty($this->task->getProject()->getDelayTo()) && $this->task->getProject()->getDelayTo() > (new DateTime()) ) {
            $at[$c]['fields'][] = [
                'title' => 'Отложена до',
                'value' => $this->task->getProject()->getDelayTo()->format('Y/m/d'),
                'short' => true,
            ];
        }

        return $at;
    }
}
