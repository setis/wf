<?php
namespace WF\Slack\Formatter;

use WF\Slack\Notification\SlackNotificationInterface;

/**
 * Class SlackFormatter
 *
 * @package WF\Slack\Formatter
 * @author artem <mail@artemd.ru>
 */
 class SlackFormatter implements SlackNotificationInterface
{
    protected $notification;

    public function __construct(SlackNotificationInterface $notification)
    {
        $this->notification = $notification;
    }

     public function getMessage()
     {
         return $this->notification->getMessage();
     }

     public function getSubject()
     {
         return $this->notification->getSubject();
     }

     public function getTo()
     {
         return $this->notification->getTo();
     }

     public function getFrom()
     {
         return $this->notification->getFrom();
     }

     public function getLevel()
     {
         return $this->notification->getLevel();
     }

     public function setUpperLevel()
     {
         $this->notification->setUpperLevel();
     }

     public function setFallback()
     {
         $this->notification->setFallback();
     }

     public function getFallback()
     {
         return $this->notification->getFallback();
     }

     public function getEncodedAttachments(){
        return json_encode($this->getAttachments());
     }

     public function getAttachments()
     {
         return $this->notification->getAttachments();
     }

 }
