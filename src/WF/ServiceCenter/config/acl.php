<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 18.04.2016
 * Time: 10:53
 */
global $PLUGINS;

$plugin_uid = \WF\ServiceCenter\Controller\ServiceCenterController::class;
$PLUGINS[$plugin_uid]['name'] = "Сервисные центры v2: Контроллер";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = \WF\ServiceCenter\Controller\ApiController::class;
$PLUGINS[$plugin_uid]['name'] = "Сервисные центры v2: API";
$PLUGINS[$plugin_uid]['hidden'] = true;

