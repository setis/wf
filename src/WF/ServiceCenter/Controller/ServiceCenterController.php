<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 18.07.16
 * Time: 19:49
 */

namespace WF\ServiceCenter\Controller;


use Symfony\Component\HttpFoundation\Request;
use WF\HttpKernel\AbstractController;

class ServiceCenterController extends AbstractController
{
    public function dashboardAction(Request $request)
    {
        return $this->renderResponse('ServiceCenter/dashboard.html.php', [

        ]);
    }


}
