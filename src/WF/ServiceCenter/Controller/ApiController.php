<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 18.07.16
 * Time: 19:49
 */

namespace WF\ServiceCenter\Controller;

use CrEOF\Spatial\PHP\Types\Geography\Polygon;
use models\ListSc;
use models\User;
use repository\ListScRepository as ServiceCenterRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use WF\HttpKernel\RestController;
use WF\Users\UsersManager;

class ApiController extends RestController
{

    public function getServiceCentersAction(Request $request)
    {
        return $this->renderJsonResponse(
            $this->getUser()->getAvailableServiceCenters(), [
            'default',
            'ListSc.chiefs',
            'ListSc.mols',
            'ListSc.technicians',
            'ListSc.SuperMols'
        ]);
    }

    public function getAccess()
    {
        /** @var UsersManager $um */
        $um = $this->get('wf.users.manager');

        return $this->renderJsonResponse([
            'access' => $um->checkAccess(self::class)
        ], ['default']);
    }

    public function getServiceCenterAction(Request $request)
    {
        $sc_id = $request->get('id');
        $sc = current(array_filter($this->getUser()->getAvailableServiceCenters(), function(ListSc $sc) use ($sc_id){
            return $sc_id === $sc->getId();
        }));
        return $this->renderJsonResponse($sc, ['default', 'ListSc.chiefs', 'ListSc.technicians', 'ListSc.mols', 'ListSc.coordinators', 'ListSc.SuperMols']);
    }

    public function deleteServiceCenterAction(Request $request)
    {
        $this->accessIsWrite();
        $sc_id = $request->get('id');
        try {
            $sc = $this->getEm()->find(ListSc::class, $sc_id);
            $this->getEm()->remove($sc);
            $this->getEm()->flush();
        } catch (\Exception $exception) {
            return new Response($exception->getMessage(), 500);
        }

        return new Response( );
    }

    public function postSaveServiceCenterAction(Request $request)
    {
        $this->accessIsWrite();
        $user = $this->getUser();

        if ($request->request->get('id')) {
            /** @var ListSc $sc */
            $sc = $this->getEm()->find(ListSc::class, $request->get('id'));
        } else {
            $sc = new ListSc();
        }

        $sc->setTitle($request->get('title'))
            ->setAddress($request->get('address'))
            ->setDesc($request->get('desc'))
            ->setEmail($request->get('email'))
            ->setPhone($request->get('phone'));

        $this->getEm()->persist($sc);
        $user->addAvailableServiceCenter($sc);
        $this->getEm()->flush();

        return new Response();
    }

    public function postSaveAreaAction(Request $request)
    {
        $this->accessIsWrite();
        /** @var ListSc $sc */
        $sc = $this->getEm()->find(ListSc::class, $request->get('sc_id'));

        $polygon = new Polygon($request->get('area')['geometry']['coordinates']);

        $sc->setArea($polygon);

        $this->getEm()->persist($sc);
        $this->getEm()->flush();

        return new Response();
    }

    public function postSaveTechniciansAction(Request $request)
    {
        $this->accessIsWrite();

        /** @var ListSc $sc */
        $sc = $this->getEm()->find(ListSc::class, $request->get('sc_id'));

        $this->getEm()->beginTransaction();
        try {
            $technicians = [];
            foreach ($request->get('technicians', []) as $item) {
                $technicians[] = $this->getEm()->find(User::class, $item['id']);
            }

            $sc->setTechnicians($technicians);

            $this->getEm()->persist($sc);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            throw $e;
        }

        return new Response();
    }

    public function postSaveChiefsAction(Request $request)
    {
        $this->accessIsWrite();
        /** @var ListSc $sc */
        $sc = $this->getEm()->find(ListSc::class, $request->get('sc_id'));

        $this->getEm()->beginTransaction();
        try {
            $chiefs = [];
            foreach ($request->get('chiefs', []) as $item) {
                $chiefs[] = $this->getEm()->find(User::class, $item['id']);
            }

            $sc->setChiefs($chiefs);

            $this->getEm()->persist($sc);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            throw $e;
        }

        return new Response();
    }

    public function postSaveMolsAction(Request $request)
    {
        $this->accessIsWrite();
        /** @var ListSc $sc */
        $sc = $this->getEm()->find(ListSc::class, $request->get('sc_id'));

        $this->getEm()->beginTransaction();
        try {
            $mols = [];
            foreach ($request->get('mols', []) as $item) {
                $mols[] = $this->getEm()->find(User::class, $item['id']);
            }

            $sc->setMols($mols);

            $this->getEm()->persist($sc);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            throw $e;
        }

        return new Response();
    }

    public function postSaveSuperMolsAction(Request $request)
    {
        $this->accessIsWrite();
        /** @var ListSc $sc */
        $sc = $this->getEm()->find(ListSc::class, $request->get('sc_id'));

        $this->getEm()->beginTransaction();
        try {
            $mols = [];
            foreach ($request->get('super_mols', []) as $item) {
                $mols[] = $this->getEm()->find(User::class, $item['id']);
            }

            $sc->setSuperMols($mols);

            $this->getEm()->persist($sc);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            throw $e;
        }

        return new Response();
    }

    public function postSaveCoordinatorsAction(Request $request)
    {
        $this->accessIsWrite();
        /** @var ListSc $sc */
        $sc = $this->getEm()->find(ListSc::class, $request->get('sc_id'));

        $this->getEm()->beginTransaction();
        try {
            $coordinators = [];
            foreach ($request->get('coordinators', []) as $item) {
                $coordinators[] = $this->getEm()->find(User::class, $item['id']);
            }

            $sc->setCoordinators($coordinators);

            $this->getEm()->persist($sc);
            $this->getEm()->flush();
            $this->getEm()->commit();
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            throw $e;
        }

        return new Response();
    }

    public function getServiceCentersCoordinatesAction(Request $request)
    {
        $exclude = [];
        if ($request->query->has('exclude', $request->request->has('exclude'))) {
            $exclude = $request->get('exclude');
            if (!is_array($exclude))
                $exclude = [$exclude];
        }

        /** @var ServiceCenterRepository $serviceCenterRepository */
        $serviceCenterRepository = $this->getEm()->getRepository(ListSc::class);

        $qb = $serviceCenterRepository->createQueryBuilder('sc');

        if (!empty($exclude)) {
            $qb->andWhere('sc.id NOT IN (:list)')
                ->setParameter('list', $exclude);
        }

        /** @var ListSc[] $sc */
        $sc = $qb->getQuery()->getResult();

        return $this->renderJsonResponse($sc, ['default']);
    }

    private function accessIsWrite()
    {
        /** @var UsersManager $um */
        $um = $this->get('wf.users.manager');
        $access = (int) $um->checkAccess(self::class);
        if (\classes\User::ACCESS_WRITE !== $access) {
            throw new AccessDeniedHttpException();
        }
    }
}
