<?php

namespace WF\Utils\Comparer;

/**
 * Description of ObjectIdComparer
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ObjectIdComparer
{
    public function __invoke($obj1, $obj2)
    {
        return $obj1->getId() - $obj2->getId();
    }
}
