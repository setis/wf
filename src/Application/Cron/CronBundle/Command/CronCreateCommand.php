<?php
/**
 * This file is part of the SymfonyCronBundle package.
 *
 * (c) Dries De Peuter <dries@nousefreak.be>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Application\Cron\CronBundle\Command;

use Cron\CronBundle\Command\CronCreateCommand as BaseCommand;
use Cron\CronBundle\Entity\CronJob;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * @author Dries De Peuter <dries@nousefreak.be>
 */
class CronCreateCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $job = new CronJob();

        /* @var $helper QuestionHelper */
        $helper = $this->getHelper('question');

        // Ask name
        $question = (new Question('<question>The unique name how the job will be referenced:</question> '))
            ->setValidator(function($value) {
                return $this->validateJobName($value);
            });
        $name = $helper->ask($input, $output, $question);
        $job->setName($name);

        // Ask command
        $question = (new Question('<question>The command to execute. You may add extra arguments:</question> '))
            ->setValidator(function($value) {
                return $this->validateCommand($value);
            });
        $command = $helper->ask($input, $output, $question);
        $job->setCommand($command);

        // Ask schedule
        $question = (new Question('<question>The schedule in the crontab syntax:</question> '))
            ->setValidator(function($value) {
                return $this->validateSchedule($value);
            });
        $schedule = $helper->ask($input, $output, $question);
        $job->setSchedule($schedule);

        // Ask description
        $question = new Question('<question>Some more information about the job:</question> ');
        $description = $helper->ask($input, $output, $question);
        $job->setDescription($description);

        // Ask enabled
        $question = new ConfirmationQuestion('<question>Should the cron be enabled?</question> ', false);
        $enabled = $helper->ask($input, $output, $question);
        $job->setEnabled($enabled);

        $this->getContainer()->get('cron.manager')
            ->saveJob($job);

        $output->writeln('');
        $output->writeln(sprintf('<info>Cron "%s" was created..</info>', $job->getName()));
    }
}
