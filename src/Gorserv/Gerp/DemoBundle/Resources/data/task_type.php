<?php
/**
 * User: artem
 * Date: 19.10.16
 * Time: 14:06
 */
return [
    [
        'title' => 'Ethernet',
        'desc' => 'Подключение',
        'plugin_uid' => 'connections',
        'duration' => 120
    ],
    [
        'title' => 'СКП',
        'desc' => null,
        'plugin_uid' => 'services',
        'duration' => 60
    ],
    [
        'title' => 'IT',
        'desc' => 'Заявки на обслуживание государственных структур',
        'plugin_uid' => 'services',
        'duration' => 120
    ],
    [
        'title' => 'Срочное подключение',
        'desc' => 'Срочное подключение за один рабочий день.',
        'plugin_uid' => 'connections',
        'duration' => 120
    ],
    [
        'title' => 'Перепротяжка ',
        'desc' => null,
        'plugin_uid' => 'connections',
        'duration' => 120
    ],
    [
        'title' => 'Осмотр юр. лица',
        'desc' => null,
        'plugin_uid' => 'connections',
        'duration' => 120
    ],
    [
        'title' => 'Обследование В2В',
        'desc' => 'Сбор исходных требований для выработки технического предложения, выезд на объект клиента, указание условий производства работ, указание необходимых согласований, контактных лиц, особых условий.',
        'plugin_uid' => 'connections',
        'duration' => 150
    ],
    [
        'title' => 'Подключение B2B',
        'desc' => 'Подключение B2B',
        'plugin_uid' => 'connections',
        'duration' => 150
    ],
    [
        'title' => 'Подключение видеокамер',
        'desc' => null,
        'plugin_uid' => 'connections',
        'duration' => 30
    ],
    [
        'title' => 'Продажа IT услуг',
        'desc' => 'Используется для клиентов: Продажа IT услуг юр. лицам',
        'plugin_uid' => 'services',
        'duration' => 180
    ],
    [
        'title' => 'Обеспечение доступа',
        'desc' => 'Обеспечение доступа - выбирается в том случае, если необходимо для видеокамер получить ключи в ДЕЗ.',
        'plugin_uid' => 'connections',
        'duration' => 30
    ],
    [
        'title' => 'Демонтаж',
        'desc' => null,
        'plugin_uid' => 'connections',
        'duration' => 120
    ],
    [
        'title' => 'Абонентская авария',
        'desc' => null,
        'plugin_uid' => 'accidents',
        'duration' => 60
    ],
    [
        'title' => 'Отсутствует электропитание',
        'desc' => 'Отсутствует электропитание',
        'plugin_uid' => 'gp',
        'duration' => 120
    ],
    [
        'title' => 'Не отвечает оборудование',
        'desc' => 'Не отвечает оборудование',
        'plugin_uid' => 'gp',
        'duration' => 120
    ],
    [
        'title' => 'Нет доступа',
        'desc' => 'Нет доступа',
        'plugin_uid' => 'gp',
        'duration' => 120
    ],
    [
        'title' => 'Забит стояк',
        'desc' => 'Забит стояк',
        'plugin_uid' => 'gp',
        'duration' => 120
    ],
    [
        'title' => 'Линейная авария',
        'desc' => null,
        'plugin_uid' => 'gp',
        'duration' => 120
    ],
    [
        'title' => 'Модернизация',
        'desc' => 'Замена оборудования, исправление монтажа сделанного не по ИТР',
        'plugin_uid' => 'gp',
        'duration' => 120
    ],
    [
        'title' => 'Доп. работы',
        'desc' => 'Заявки на доп работы (замена замков, спил ящиков, модернизация, маркировка кабелей - любые работы, которые делают техники в свободное время.',
        'plugin_uid' => 'accidents',
        'duration' => 60
    ],
    [
        'title' => 'Перепротяжка ',
        'desc' => 'Перепротяжка без настройки аборудования',
        'plugin_uid' => 'accidents',
        'duration' => 60
    ],
    [
        'title' => 'Камера ДИТ',
        'desc' => null,
        'plugin_uid' => 'gp',
        'duration' => 120
    ],
    [
        'title' => 'Предписание',
        'desc' => 'Работа по предписанию заказчика',
        'plugin_uid' => 'gp',
        'duration' => 1410
    ],
    [
        'title' => 'ADSL Подключение В2В ',
        'desc' => null,
        'plugin_uid' => 'connections',
        'duration' => 90
    ],
    [
        'title' => 'Демонтаж оборудования',
        'desc' => 'Демонтаж оборудования ',
        'plugin_uid' => 'accidents',
        'duration' => 60
    ],
    [
        'title' => 'Настройка АТС и SIP шлюза',
        'desc' => null,
        'plugin_uid' => 'services',
        'duration' => 60
    ],
    [
        'title' => 'Доставка',
        'desc' => null,
        'plugin_uid' => 'accidents',
        'duration' => 60
    ],
    [
        'title' => 'ТМ',
        'desc' => null,
        'plugin_uid' => 'tm_module',
        'duration' => 30
    ],
    [
        'title' => 'Доставка оборудования',
        'desc' => null,
        'plugin_uid' => 'connections',
        'duration' => 120
    ],
    [
        'title' => 'PON',
        'desc' => null,
        'plugin_uid' => 'connections',
        'duration' => 120
    ],
];
