<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 17.10.16
 * Time: 17:19
 */

namespace Gorserv\Gerp\DemoBundle\Command;


use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GerpSeedCommand extends Command
{
    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('gerp:seed')
            ->setDescription(<<< DESCRIPTION
Bootstraps base application for demo purposes:
adds admin:admin user;
adds one ListSc;
adds one ListContr;
adds one Department;
JobPosition dictionary;
QcList dictionary;
TaskStatuses dictionary;
TaskTypes dictionary;
DESCRIPTION
);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

    }
}
