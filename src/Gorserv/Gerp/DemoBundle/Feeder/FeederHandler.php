<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 18.10.16
 * Time: 14:05
 */

namespace Gorserv\Gerp\DemoBundle\Feeder;


use Doctrine\ORM\EntityManagerInterface;
use models\Agent;
use models\Agreement;
use models\Department;
use models\JobPosition;
use models\KontrProptypes;
use models\LinkAgreementTaskType;
use models\ListContr;
use models\ListSc;
use models\ListSkpPartnerCatTitle;
use models\ListSkpPartnerMeasureUnit;
use models\ListSkpPartnerPrice;
use models\ListSkpPartnerWorkTypes;
use models\QcQuestion;
use models\TariffType;
use models\TariffTypeProperty;
use models\Task;
use models\TaskStatus;
use models\TaskType;
use models\User;
use models\UserAccess;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class FeederHandler
{
    /**
     * @var array
     */
    public $taskTypes;
    /**
     * @var array
     */
    public $taskStatuses;
    /**
     * @var array
     */
    public $queriesList;
    /**
     * @var array
     */
    public $userAccess;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $em;
        $this->encoder = $encoder;

        $bdsm = DIRECTORY_SEPARATOR;

        $this->taskStatuses = include __DIR__ . "{$bdsm}..{$bdsm}Resources{$bdsm}data{$bdsm}task_status.php";
        $this->taskTypes = include __DIR__ . "{$bdsm}..{$bdsm}Resources{$bdsm}data{$bdsm}task_type.php";
        $this->queriesList = include __DIR__ . "{$bdsm}..{$bdsm}Resources{$bdsm}data{$bdsm}qc_qlist.php";
        $this->userAccess = include __DIR__ . "{$bdsm}..{$bdsm}Resources{$bdsm}data{$bdsm}user_access.php";
    }

    public function feedUser($username = 'admin', $password = 'admin', Department $department = null, JobPosition $position = null, ListSc $sc = null): User
    {
        $user = $this->em->getRepository(User::class)
            ->findOneBy(['login' => $username]);

        if (!empty($user))
            throw new \LogicException('"' . $username . '" already exists!');

        if(null === $department) {
            $department = $this->feedDepartment();
        }
        if(null === $position) {
            $position = $this->feedJobPosition();
        }
        if(null === $sc) {
            $sc = $this->feedServiceCenter();
        }

        $user = new User();
        $user->setLogin($username)
            ->setPass($this->encoder->encodePassword($user, $password))
            ->setFio($username)
            ->setJobPosition($position)
            ->setDepartment($department)
            ->setActive(true)
            ->setRoles(['ROLE_ADMIN']);
        $this->em->persist($user);

        $user->addAvailableServiceCenter($sc);

        foreach ($this->userAccess as $a) {
            $ua = new UserAccess();
            $ua->setUser($user)
                ->setAccess($a['access'])
                ->setPluginUid($a['plugin_uid']);
            $this->em->persist($ua);
        }

        $this->em->flush();

        return $user;
    }

    public function feedDepartment($name = 'IT-отдел'): Department
    {
        $department = new Department();
        $department->setName($name);

        $this->em->persist($department);
        $this->em->flush();

        return $department;
    }

    public function feedJobPosition($name = 'Администратор'): JobPosition
    {
        $position = new JobPosition();
        $position->setTitle($name);

        $this->em->persist($position);
        $this->em->flush();

        return $position;
    }

    public function feedServiceCenter($name = 'Центральный СЦ'): ListSc
    {
        $sc = new ListSc();
        $sc->setTitle($name);

        $this->em->persist($sc);
        $this->em->flush();

        return $sc;
    }

    public function feedPartner($name)
    {

        $propType = (new KontrProptypes())
            ->setTitle('ООО');
        $this->em->persist($propType);

        $partner = new ListContr();
        $partner->setContrTitle($name)
            ->setContrType2(true)
            ->setContrType3(true)
            ->setContrType4(true)
            ->setContrTypeTm(true)
            ->setContrType(true)
            ->setIsClient(true)
            ->setProptypeId($propType->getId());

        $this->em->persist($partner);

        $agreement = new Agreement();
        $agreement->setAgreement('Первый договор')
            ->setAgrDate(new \DateTime())
            ->setPartner($partner)
            ->setAmmount(1000)
            ->setNumber('1/1')
            ->setComent('Первичный договор')
            ->setAccessContact('Владимир Морозов')
            ->setTpContact('Владимир Морозов')
            ->setTypePrice(true)
            ->setTypeSmeta(true)
            ->setTypeAbon(false)
            ->setStatus(
                $this->em->getRepository(TaskStatus::class)
                    ->findOneByTag('inaction', Task::AGREEMENT)
            )
            ->setFindate((new \DateTime())->modify('+1 month'));
        $this->em->persist($agreement);

        foreach ($this->em->getRepository(TaskType::class)
                     ->findAll() as $tt) {
            $l = new LinkAgreementTaskType();
            $l->setAgreement($agreement)
                ->setTaskType($tt)
                ->setQc(true);
            $this->em->persist($l);
        }

        $this->em->flush();

        return [$partner, $agreement, $this->em->getRepository(TaskType::class)
            ->findAll()];
    }

    public function feedAgent(ListContr $partner, User $user, Agreement $agreement, TaskType $tt):Agent
    {
        $agent = (new Agent())
            ->setPartner($partner)
            ->setUser($user)
            ->setAgreement($agreement)
            ->setTaskType($tt);

        $this->em->persist($agent);
        $this->em->flush();

        return $agent;
    }

    public function feedTaskStatus(): int
    {
        foreach ($this->taskStatuses as $status) {
            $taskStatus = new TaskStatus();
            $taskStatus
                ->setId($status['id'])
                ->setName($status['name'])
                ->setColor($status['color'])
                ->setIsActive($status['is_active'])
                ->setIsDefault($status['is_default'])
                ->setIsHidden($status['is_hidden'])
                ->setPluginUid($status['plugin_uid'])
                ->setRequireComment($status['require_comment'])
                ->setTag($status['tag']);
            $this->em->persist($taskStatus);
        }

        $this->em->flush();

        return count($this->taskStatuses);
    }

    public function feedTaskTypes(): int
    {

        foreach ($this->taskTypes as $type) {
            $taskType = new TaskType();
            $taskType->setPluginUid($type['plugin_uid'])
                ->setDesc($type['desc'])
                ->setDuration($type['duration'])
                ->setTitle($type['title']);
            $this->em->persist($taskType);
        }

        $this->em->flush();

        return count($this->taskTypes);
    }

    public function feedQcQlist(): int
    {
        foreach ($this->queriesList as $query) {
            $q = new QcQuestion();
            $q->setPluginUid($query['plugin_uid'])
                ->setOrder($query['iorder'])
                ->setCreatedAt(new \DateTime())
                ->setDescription($query['qtextdesc'])
                ->setIsActive($query['active'])
                ->setIsAmmountRequired($query['req_ammount'])
                ->setIsCommentRequired($query['req_comment'])
                ->setIsDeleted($query['deleted'])
                ->setMaxCount($query['q_maxcount'])
                ->setMaybeYesNo($query['q_mbyesno'])
                ->setNoCount($query['no_count'])
                ->setStatusGroup($query['status_group'])
                ->setText($query['qtext'])
                ->setWeight($query['weight'])
                ->setYesCount($query['yes_count'])
                ->setYesNo($query['q_yesno']);
            $this->em->persist($q);
        }

        $this->em->flush();

        return count($this->queriesList);
    }

    public function feedTm()
    {
        $internet = (new TariffType())
            ->setTarifType('Интернет');
        $this->em->persist($internet);

        $internet_tv = (new TariffType())
            ->setTarifType('Интернет+ТВ');
        $this->em->persist($internet_tv);

        $phones = (new TariffType())
            ->setTarifType('Телефония');
        $this->em->persist($phones);

        $ctv = (new TariffType())
            ->setTarifType('ЦТВ');
        $this->em->persist($ctv);
////
        $internet_prop_1 = (new TariffTypeProperty())
            ->setTarifType($internet)
            ->setPropertyOrder(1)
            ->setPropertyName('Абонентская плата');
        $this->em->persist($internet_prop_1);

        $internet_prop_2 = (new TariffTypeProperty())
            ->setTarifType($internet)
            ->setPropertyOrder(2)
            ->setPropertyName('Скорость');
        $this->em->persist($internet_prop_2);
////
        $internet_tv_prop_1 = (new TariffTypeProperty())
            ->setTarifType($internet_tv)
            ->setPropertyOrder(1)
            ->setPropertyName('Абонентская плата');
        $this->em->persist($internet_tv_prop_1);

        $internet_tv_prop_2 = (new TariffTypeProperty())
            ->setTarifType($internet_tv)
            ->setPropertyOrder(2)
            ->setPropertyName('Кол-во каналов');
        $this->em->persist($internet_tv_prop_2);

        $internet_tv_prop_3 = (new TariffTypeProperty())
            ->setTarifType($internet_tv)
            ->setPropertyOrder(3)
            ->setPropertyName('Поля от провайдера');
        $this->em->persist($internet_tv_prop_3);
/////

        $ctv_prop_1 = (new TariffTypeProperty())
            ->setTarifType($ctv)
            ->setPropertyOrder(1)
            ->setPropertyName('Абонентская плата');
        $this->em->persist($ctv_prop_1);

        $ctv_prop_2 = (new TariffTypeProperty())
            ->setTarifType($ctv)
            ->setPropertyOrder(2)
            ->setPropertyName('Скорость');
        $this->em->persist($ctv_prop_2);

        $ctv_prop_3 = (new TariffTypeProperty())
            ->setTarifType($ctv)
            ->setPropertyOrder(3)
            ->setPropertyName('Кол-во каналов');
        $this->em->persist($ctv_prop_3);

///
        $phones_prop_1 = (new TariffTypeProperty())
            ->setTarifType($phones)
            ->setPropertyOrder(1)
            ->setPropertyName('Абонентская плата');
        $this->em->persist($phones_prop_1);

        $phones_prop_2 = (new TariffTypeProperty())
            ->setTarifType($phones)
            ->setPropertyOrder(2)
            ->setPropertyName('Объем минут вкл в АП');
        $this->em->persist($phones_prop_2);

        $this->em->flush();
    }

    public function feedSkp(User $user, ListContr $partner)
    {
        $unit = new ListSkpPartnerMeasureUnit();
        $unit->setFulltitle('Штука')
            ->setTitle('шт (н.час)')
            ->setAuthor($user);
        $this->em->persist($unit);

        $cat = new ListSkpPartnerCatTitle();
        $cat->setTitle('Установка комплектующих персонального компьютера')
            ->setAuthor($user);
        $this->em->persist($cat);

        $work_type = new ListSkpPartnerWorkTypes();
        $work_type->setCategory($cat)
            ->setAuthor($user)
            ->setUnit($unit)
            ->setTitle('Поиск "плавающей" системной или аппаратной ошибки');
        $this->em->persist($work_type);

        $price = new ListSkpPartnerPrice();
        $price->setCategory($cat)
            ->setPartner($partner)
            ->setAuthor($user)
            ->setPrice(100)
            ->setWork($work_type);

        $this->em->persist($price);
        $this->em->flush();
    }

}
