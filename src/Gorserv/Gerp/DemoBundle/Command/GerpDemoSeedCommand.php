<?php

namespace Gorserv\Gerp\DemoBundle\Command;

use Gorserv\Gerp\DemoBundle\Feeder\FeederHandler;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GerpDemoSeedCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('gerp:demo:seed')
            ->setDescription("Bootstraps base application for demo purposes")
            ->setHelp("Bootstraps base application for demo purposes:
adds admin:admin user;
adds one ListSc;
adds one ListContr;
adds one Department;
adds one JobPosition;
QcList dictionary;
TaskStatuses dictionary;
TaskTypes dictionary;");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var FeederHandler $feeder */
        $feeder = $this->getContainer()->get('gorserv_gerp_demo.feeder.feeder_handler');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $logger = $this->getContainer()->get('logger');

        $output->writeln('Database feeding...');
        $em->beginTransaction();
        try {
            $ts = $feeder->feedTaskStatus();
            $logger->info('task statuses added', ['count'=>$ts]);

            $tt = $feeder->feedTaskTypes();
            $logger->info('task types added', ['count'=>$tt]);

            list($partner, $agreement, $taskTypes) = $feeder->feedPartner($this->getContainer()->getParameter('name'));
            $logger->info('partner added', ['id'=>$partner->getId()]);

            $qc = $feeder->feedQcQlist();
            $logger->info('qc list feed', ['count'=>$qc]);

            $user = $feeder->feedUser();
            $logger->info('user added', ['user'=>$user->getUsername()]);

            $feeder->feedSkp($user, $partner);
            $logger->info('skp feeded');

            $agent = $feeder->feedAgent($partner, $user, $agreement, $taskTypes[0]);
            $logger->info('agent added', ['agent'=>$agent->getUser()->getUsername()]);

            $user = $feeder->feedUser('administrator', 'daspassword750', $user->getDepartment(), $user->getJobPosition(), $user->getAvailableServiceCenters()[0]);
            $logger->info('user added', ['user'=>$user->getUsername()]);

            $feeder->feedTm();
            $logger->info('tm stuff added');

        } catch (\Exception $e) {
            $em->rollback();
            throw $e;
        }
        $em->commit();

        $output->writeln('Database seeded successfully.');
    }

}
