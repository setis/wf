<?php

namespace Gorserv\Gerp\AppBundle;

use Gorserv\Gerp\AppBundle\DependencyInjection\Compiler\SetupExtendedSwitchUserListenerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GorservGerpAppBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new SetupExtendedSwitchUserListenerPass('main'));
    }
}
