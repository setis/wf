<?php

namespace Gorserv\Gerp\AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use phpDocumentor\Reflection\Types\Callable_;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use WF\Tmc\Controller\TmcController;

/**
 * Description of MenuBuilder
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class MenuBuilder
{
    /**
     *
     * @var FactoryInterface
     */
    private $factory;

    /**
     *
     * @var AuthorizationCheckerInterface;
     */
    private $authorizationChecker;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->factory = $factory;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('root')
            ->setChildrenAttribute('class', 'nav navbar-nav');

        $menu->addChild('dashboard', [
            'route' => 'gorserv_gerp_app_dashboard_index',
            'label' => '<span title="Рабочий стол" class="glyphicon glyphicon-home hidden-sm hidden-xs" ></span>
                <span class="visible-xs visible-sm">Рабочий стол</span>',
            'extras' => ['safe_label' => true],
            'attributes' => [
                'class' => 'menu_item__tidy pull-left',
            ],
        ]);

        $this->addChildIfReadGranted('projects', $menu, 'projects', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'projects'],
            'label' => 'Проекты',
            'extras' => ['safe_label' => true],
        ]);

        $this->addChildIfReadGranted('bills', $menu, 'bills', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'bills'],
            'label' => 'Счета',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('gfx', $menu, 'gfx', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'gfx'],
            'label' => 'График',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('connections', $menu, 'connections', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'connections'],
            'label' => 'Подключения',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('services', $menu, 'services', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'services'],
            'label' => 'СКП',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('tm_module', $menu, 'tm_module', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'tm_module'],
            'label' => 'Продажи',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('accidents', $menu, 'accidents', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'accidents'],
            'label' => 'ТТ',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('gp', $menu, 'gp', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'gp'],
            'label' => 'ГП',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('kontr', $menu, 'kontr', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'kontr'],
            'label' => 'Контрагенты',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('agreements', $menu, 'agreements', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'agreements'],
            'extras' => ['safe_label' => true],
            'label' => '<span title="Договоры" class="fa fa-file-text-o hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl"></span>
                    <span class="visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl">Договоры</span>',
        ]);
        $this->addChildIfReadGranted('adm_ods', $menu, 'adm_ods', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'adm_ods'],
            'label' => '<span title="ОДС" class="fa fa-key hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl"></span>
                    <span class=\'visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl\'>ОДС</span>',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('tmc', $menu, 'tmc', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'tmc'],
            'label' => '<span title="ТМЦ" class="fa fa-archive hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl"></span>
                    <span class=\'visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl\'>ТМЦ</span>',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted(TmcController::class, $menu, 'tmc2', [
            'route' => 'tmc_dashboard',
            'label' => '<span title="ТМЦv2" class="fa fa-archive hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl"><sup> 2</sup></span>
                    <span class=\'visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl\'>ТМЦv2</span>',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('reports', $menu, 'reports', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'reports'],
            'label' => '<span title="Отчеты" class="fa fa-bar-chart hidden-xs hidden-sm visible-md hidden-lg visible-xl"></span>
                    <span class=\'visible-xs visible-sm hidden-md hidden-lg visible-xxl\'>Отчеты</span>',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('callviewer', $menu, 'callviewer', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'callviewer'],
            'label' => '<span title="Записи телефонных звонков" class="fa fa-play-circle hidden-xs hidden-sm visible-md hidden-lg visible-xl"></span>
                    <span class=\'visible-xs visible-sm hidden-md hidden-lg visible-xxl\'>Записи звонков</span>',
            'extras' => ['safe_label' => true],
        ]);
        $this->addChildIfReadGranted('qc', $menu, 'qc', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'qc'],
            'label' => '<span title="Контроль качества (отзывы)" class="fa fa-thumbs-up hidden-xs hidden-sm visible-md hidden-lg visible-xl"></span>
                    <span class=\'visible-sm visible-xs hidden-md hidden-lg visible-xxl\'>Отзывы</span>',
            'extras' => ['safe_label' => true],
        ]);

        $this->addChildIfReadGranted('adm_interface', $menu, 'adm_interface', [
            'route' => 'legacy_rule_common',
            'routeParameters' => ['pluginEvent' => 'adm_interface'],
            'label' => '',
            'extras' => ['safe_label' => true],
        ], function (ItemInterface $item) {
            $item->setAttribute('icon', 'fa fa-cog');
        }
        );

        return $menu;
    }

    private function addChildIfReadGranted($plugin, ItemInterface $menu, $child, array $options, callable $callback = null)
    {
        $item = null;
        if ($this->authorizationChecker->isGranted('plugin_read', $plugin)) {
            $item = $menu->addChild($child, $options)
                ->setAttribute('class', 'menu_item__tidy pull-left');
        }

        if ($callback && $item instanceof ItemInterface) {
            $callback($item);
        }

        return $this;
    }
}
