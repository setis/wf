<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 26.01.17
 * Time: 17:28
 */

namespace Gorserv\Gerp\AppBundle\EventDispatcher\Events;


use models\Task;

class TaskRelevanceLevelChangedEvent extends TaskEvent
{
    private $oldLevel;

    public function __construct(Task $task, $oldLevel)
    {
        parent::__construct($task);
        $this->oldLevel = $oldLevel;
    }

    public function getOldLevel()
    {
        return $this->oldLevel;
    }

    public function getNewLevel()
    {
        return $this->getTask()->getRelevanceLevel();
    }
}