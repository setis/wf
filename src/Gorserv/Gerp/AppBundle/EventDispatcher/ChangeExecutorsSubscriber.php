<?php

namespace Gorserv\Gerp\AppBundle\EventDispatcher;

use Gorserv\Gerp\ScheduleBundle\EventDispatcher\Events;
use Gorserv\Gerp\ScheduleBundle\EventDispatcher\SlotEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WF\Task\TaskManager;

/**
 * Description of ChangeExecutorsSubscriber
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ChangeExecutorsSubscriber implements EventSubscriberInterface
{
    /**
     *
     * @var TaskManager
     */
    private $taskManager;

    public function __construct(TaskManager $taskManager)
    {
        $this->taskManager = $taskManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            Events::SLOT_CREATED => ['onSlotCreated', 512],
            Events::SLOT_DELETED => ['onSlotDeleted', 512],
        ];
    }

    public function onSlotCreated(SlotEvent $event)
    {
        $this->taskManager->setTaskExecutors($event->getSlot()->getJob(), $event->getSlot()->getExecutors());
    }

    public function onSlotDeleted(SlotEvent $event)
    {
        $this->taskManager->setTaskExecutors($event->getSlot()->getJob(), []);
    }
}
