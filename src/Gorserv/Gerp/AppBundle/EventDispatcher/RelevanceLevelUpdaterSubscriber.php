<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 26.01.17
 * Time: 15:10
 */

namespace Gorserv\Gerp\AppBundle\EventDispatcher;


use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\AppBundle\EventDispatcher\Events\TaskRelevanceLevelChangedEvent;
use models\Task;
use models\TaskStatus;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WF\Task\Events\TaskStatusChangedEvent;

class RelevanceLevelUpdaterSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $ed;

    /**
     * RelevanceLevelUpdaterSubscriber constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $ed)
    {
        $this->em = $em;
        $this->ed = $ed;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            TaskStatusChangedEvent::NAME => 'onTaskStatusChanged',
        ];
    }

    public function onTaskStatusChanged(TaskStatusChangedEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        $newStatusTags = $event->getNewStatus()->getTags();

        $oldLevel = $task->getRelevanceLevel();

        if (in_array(TaskStatus::TAG_TASK_NEW, $newStatusTags)) {
            $task->setRelevanceLevel(0);
        } else {
            $task->setRelevanceLevel(null);
        }

        $this->em->flush($task);
        $event = new TaskRelevanceLevelChangedEvent($task, $oldLevel);

        $this->ed->dispatch(TaskEvents::RELEVANCE_LEVEL_CHANGED, $event);
    }

    private function notSupported(Task $task)
    {
        return Task::SERVICE !== $task->getPluginUid();
    }
}