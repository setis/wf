<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 24.01.17
 * Time: 19:00
 */

namespace Gorserv\Gerp\AppBundle\EventDispatcher;

use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\AppBundle\EventDispatcher\Events\TaskEvent;
use Gorserv\Gerp\ScheduleBundle\Model\ScheduleManagerInterface;
use models\Task;
use models\TaskComment;
use models\TaskStatus;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WF\Task\Events\TaskStatusChangedEvent;
use WF\Task\TaskManager;

class ProcessDelayedTaskSubscriber implements EventSubscriberInterface
{
    /**
     * @var TaskManager
     */
    private $taskManager;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ScheduleManagerInterface
     */
    private $scheduleManager;

    /**
     * ProcessDelayedTaskListener constructor.
     * @param TaskManager $taskManager
     * @param EntityManagerInterface $em
     */
    public function __construct(TaskManager $taskManager, EntityManagerInterface $em, ScheduleManagerInterface $scheduleManager)
    {
        $this->taskManager = $taskManager;
        $this->em = $em;
        $this->scheduleManager = $scheduleManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            TaskEvents::DELAY_DATE_OCCURRED => 'onDelayDateOccurred',
            TaskStatusChangedEvent::NAME => 'onTaskStatusChanged',
        ];
    }

    public function onTaskStatusChanged(TaskStatusChangedEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        if (in_array(TaskStatus::TAG_TASK_DELAYED, $task->getStatus()->getTags())) {
            return;
        }

        $task->setDelayedTo(null);
        $this->em->flush($task);
    }

    public function onDelayDateOccurred(TaskEvent $event)
    {
        $task = $event->getTask();

        if ($this->notSupported($task)) {
            return;
        }

        if (!in_array(TaskStatus::TAG_TASK_DELAYED, $task->getStatus()->getTags())) {
            return;
        }

        $status = $this->em->getRepository(TaskStatus::class)
            ->findOneByTag('new', $task->getPluginUid());

        $comment = (new TaskComment())
            ->setStatus($status)
            ->setTag('Автоматическая установка статуса Новая при наступлении даты Отложено');

        $this->em->transactional(function (EntityManagerInterface $em) use ($task, $comment) {
            $this->taskManager->addComment($task, $comment);
            $slot = $task->getSlot();
            if (null !== $slot) {
                $this->scheduleManager->deleteSlot($slot);
            }
            $task->setDelayedTo(null);
            $em->flush();
        });
    }

    private function notSupported(Task $task)
    {
        if (null === $task->getDelayedTo()) {
            return;
        }

        return $task->getPluginUid() !== Task::CONNECTION
            && $task->getPluginUid() !== Task::SERVICE;
    }
}