<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 18.01.17
 * Time: 16:44
 */

namespace Gorserv\Gerp\AppBundle\EventDispatcher;


use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use models\TaskComment;
use models\TaskStatus;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WF\Task\Events\TaskCreatedEvent;
use WF\Task\TaskManager;

class TicketCommentUpdaterSubscriber implements EventSubscriberInterface
{
    /**
     * @var TaskManager
     *
     */
    private $taskManager;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * TicketCommentUpdaterSubscriber constructor.
     * @param TaskManager $taskManager
     */
    public function __construct(TaskManager $taskManager, EntityManagerInterface $entityManager)
    {
        $this->taskManager = $taskManager;
        $this->em = $entityManager;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            TaskCreatedEvent::NAME => [
                'onConnectionCreated',
            ],
        ];
    }

    public function onConnectionCreated(TaskCreatedEvent $event)
    {
        $task = $event->getTask();

        if ($task->getPluginUid() !== Task::CONNECTION) {
            return;
        }

        $ticket = $task->getTicket();
        $type = $ticket->getTaskType();
        $addInfo = $ticket->getAddinfo();

        // 20 - Подключение B2B МГТС
        if (null !== $type && $type->getId() == 20 && !empty($addInfo)) {
            $comment = (new TaskComment())
                ->setAuthor($task->getAuthor())
                ->setText($addInfo)
                ->setTag('Описание заявки')
            ;

            $this->taskManager->addComment($task, $comment);
        }

        // 16 - Монтажник нашел клиента сам
        if (null !== $type && $type->getId() == 16) {
            $closedStatus = $this->em
                ->getRepository(TaskStatus::class)
                ->findOneByTag(TaskStatus::TAG_TASK_CLOSED, Task::CONNECTION);

            $comment = (new TaskComment())
                ->setAuthor($task->getAuthor())
                ->setText('Автоматическое закрытие заявки данного типа и рассылка уведомлений')
                ->setStatus($closedStatus)
            ;

            $this->taskManager->addComment($task, $comment);
        }

    }
}