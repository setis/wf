<?php

namespace Gorserv\Gerp\AppBundle\EventDispatcher;

use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\ScheduleBundle\EventDispatcher\Events;
use Gorserv\Gerp\ScheduleBundle\EventDispatcher\SlotDeletedEvent;
use Gorserv\Gerp\ScheduleBundle\EventDispatcher\SlotEvent;
use models\Task;
use models\TaskComment;
use models\TaskStatus;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WF\Task\TaskManager;

/**
 * Description of ChangeTaskScheduleSubscriber
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ChangeTaskScheduleSubscriber implements EventSubscriberInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     *
     * @var TaskManager
     */
    private $taskManager;

    public function __construct(TaskManager $taskManager, EntityManagerInterface $em)
    {
        $this->taskManager = $taskManager;
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return [
            Events::SLOT_CREATED => 'onSlotCreated',
            Events::SLOT_DELETED => 'onSlotDeleted',
        ];
    }

    public function onSlotCreated(SlotEvent $event)
    {
        $task = $event->getSlot()->getJob();
        if (!$task instanceof Task) {
            return;
        }

        $status = $this->findStatus($task->getPluginUid(), TaskStatus::TAG_SCHEDULED);

        $comment = (new TaskComment())
            ->setStatus($status)
            ->setTag(sprintf('Добавлено в график на %s c %s до %s. Исполнитель: %s',
                $event->getSlot()->getTimeStart()->format('Y-m-d'),
                $event->getSlot()->getTimeStart()->format('H:i'),
                $event->getSlot()->getTimeEnd()->format('H:i'),
                implode(', ', $event->getSlot()->getExecutors())
            ))
            ->setText('Поставлена в График Работ.')
            ->setAuthor($event->getSlot()->getCreatedBy());

        $this->taskManager->addComment($task, $comment);
    }

    /**
     * @param $pluginUid
     * @param $tag
     * @return TaskStatus|null
     */
    private function findStatus($pluginUid, $tag)
    {
        try {
            $status = $this->em->getRepository(TaskStatus::class)
                ->findOneByTag($tag, $pluginUid);
            return $status;
        } catch (\Exception $exception) {
            return null;
        }
    }

    public function onSlotDeleted(SlotDeletedEvent $event)
    {
        $task = $event->getSlot()->getJob();
        if (!$task instanceof Task) {
            return;
        }

        /**
         * @TODO fix this dirty hack
         */
        $status = null;
        if (empty(array_intersect([TaskStatus::TAG_PARTNER_CANCEL, TaskStatus::TAG_CLIENT_CANCEL, TaskStatus::TAG_TECH_CANCEL], $task->getStatus()->getTags()))) {
            $status = $this->em->getRepository(TaskStatus::class)
                ->getDefault($task->getPluginUid());
        }

        $text = 'Удалено из графика работ';

        $params = $event->getParams();
        if (isset($params['reason'])) {
            $text = 'Перенос заявки. Причина: ' . $params['reason'];
        }

        $comment = (new TaskComment())
            ->setStatus($status)
            ->setTag('Удалено из графика.')
            ->setText($text)
            ->setAuthor($event->getDeletedBy());

        $this->taskManager->addComment($task, $comment);
    }
}
