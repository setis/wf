<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 28.12.16
 * Time: 20:46
 */

namespace Gorserv\Gerp\AppBundle\EventDispatcher;


use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\AppBundle\Billing\BalanceManager;
use Gorserv\Gerp\AppBundle\Entity\Money;
use Gorserv\Gerp\BillingBundle\Model\BalanceManagerInterface;
use Gorserv\Gerp\BillingBundle\Model\MoneyInterface;
use models\Task;
use models\TaskStatus;
use WF\Task\Events\TaskStatusChangedEvent;

class TaskCalculationListener
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BalanceManagerInterface
     */
    private $bm;

    public function __construct(EntityManagerInterface $em, BalanceManager $bm)
    {
        $this->em = $em;
        $this->bm = $bm;
    }

    public function onWfTaskStatusChanged(TaskStatusChangedEvent $event)
    {
        $task = $event->getTask();

        if (!$this->isSupported($task)) {
            return;
        }

        $amount = $this->getTaskAmount($task);
        $user = $task->getSchedule()->getTechnician();

        $this->bm->updateUserBalance($user, $amount, [
            'task' => $task
        ]);
    }

    private function isSupported(Task $task)
    {
        return Task::SERVICE === $task->getPluginUid() && TaskStatus::STATUS_SERVICE_DONE === $task->getStatus()->getId();
    }

    /**
     * @param Task $task
     * @return MoneyInterface
     * @throws \Exception
     */
    private function getTaskAmount(Task $task)
    {
        switch ($task->getPluginUid()) {
            case Task::SERVICE:
                $amount = 0;
                foreach ($task->getSkpTechCalc() as $calc) {
                    $amount += (float)$calc->getPrice() * $calc->getQuantity();
                }

                return new Money(-$amount, 'RUB');

                break;
        }

        throw new \Exception("Not support task plugin uid {$task->getPluginUid()}!");


    }
}
