<?php
namespace Gorserv\Gerp\AppBundle\EventDispatcher;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\BillingBundle\EventDispatcher\BalanceEvent;
use Gorserv\Gerp\BillingBundle\EventDispatcher\Events;
use models\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WF\Notification\Model\NotificationManagerInterface;
use WF\Slack\Notification\SlackNotification;

class UserBalanceSubscriber implements EventSubscriberInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var NotificationManagerInterface
     */
    private $nm;

    /**
     * UserBalanceSubscriber constructor.
     * @param EntityManagerInterface $em
     * @param NotificationManagerInterface $nm
     */
    public function __construct(EntityManagerInterface $em, NotificationManagerInterface $nm)
    {
        $this->em = $em;
        $this->nm = $nm;
    }


    public static function getSubscribedEvents()
    {
        return [
            #Events::BALANCE_UPDATED => 'onUserBalanceUpdated',
        ];
    }

    public function onUserBalanceUpdated(BalanceEvent $event)
    {
        /** @var User $user */
        $user = $event->getBalanceOwner();

        if ($user->getBalance()->getSum() >= 0) {
            $user->setBalanceBecameNegativeAt(null);
            $this->em->flush();
            return;
        }

        if (null === $user->getBalanceBecameNegativeAt()) {
            $user->setBalanceBecameNegativeAt(new DateTime());
            $this->em->flush();
        }

        if ($user->getBalance()->getSum() >= -$user->getSkpDebt()->getAmount()) {
            $notification = new SlackNotification();
            $notification->setTo($user)
                ->setMessage('У вас отрицательный баланс! Завтра в 11.00 доступ в систему будет заблокирован!');
            $this->nm->info($notification);
        }
    }
}
