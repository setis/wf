<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 24.01.17
 * Time: 18:46
 */

namespace Gorserv\Gerp\AppBundle\EventDispatcher;


class TaskEvents
{
    const DELAY_DATE_OCCURRED = 'gerp.app.task.delay_date_occurred';
    const RELEVANCE_LEVEL_CHANGED = 'gerp.app.task.relevance_level_changed';
}