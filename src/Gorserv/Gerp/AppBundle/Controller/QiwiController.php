<?php

namespace Gorserv\Gerp\AppBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Gorserv\Gerp\AppBundle\Billing\Xml\QiwiTerminalResponse;
use Gorserv\Gerp\AppBundle\Entity\Money;
use Gorserv\Gerp\AppBundle\Entity\PaymentOrder;
use Gorserv\Gerp\BillingBundle\Exceptions\BillingException;
use models\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class QiwiController
 *
 * @package Gorserv\Gerp\AppBundle\Controller
 * @author ${USER} <mail@artemd.ru>
 *
 */
class QiwiController extends FOSRestController
{
    /**
     * @param Request $request
     *
     * @Route("/pay_app", condition="request.get('command') == 'check'")
     * @View()
     *
     * @return QiwiTerminalResponse
     */
    public function checkAction(Request $request)
    {
        try {
            $this->checkRequest($request);
        } catch (Exception $e) {
            return new QiwiTerminalResponse(
                $request->get('txn_id'),
                $e->getMessage(),
                $e->getCode()
            );
        }

        $form = $this->createCheckQiwiForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && !$form->isValid()) {
            return new QiwiTerminalResponse(
                $request->get('txn_id'),
                (string)$form->getErrors(true, false),
                300
            );
        }

        $data = $form->getData();

        return new QiwiTerminalResponse(
            $data['txn_id'],
            null,
            0,
            null,
            $data['sum']
        );
    }

    private function checkRequest(Request $request)
    {
        if (1 !== preg_match('/^\d+$/', $request->get('account'))) {
            throw new Exception('Error', 4);
        }

        if (null === $this->getDoctrine()->getManager()->find(User::class, $request->get('account'))) {
            throw new Exception('Error', 5);
        }
    }

    /**
     * @return Form
     */
    private function createCheckQiwiForm()
    {
        return
            $this->get('form.factory')
                ->createNamedBuilder(null, FormType::class, null, [
                    'csrf_protection' => false
                ])
                ->setMethod('GET')
                ->add('command', TextType::class, [
                    'constraints' => [
                        new NotBlank(),
                        new EqualTo('check')
                    ]
                ])
                ->add('txn_id', IntegerType::class, [
                    'constraints' => [
                        new NotBlank(),
                    ]
                ])
                ->add('account', EntityType::class, [
                    'class' => User::class
                ])
                ->add('sum', MoneyType::class, [
                    'constraints' => [
                        new NotBlank(),
                    ]
                ])
                ->getForm();
    }

    /**
     *
     * @param Request $request
     * @Route("/pay_app", condition="request.get('command') == 'pay'")
     * @View()
     *
     * @return QiwiTerminalResponse
     */
    public function payAction(Request $request)
    {
        try {
            $this->checkRequest($request);
        } catch (Exception $e) {
            return new QiwiTerminalResponse(
                $request->get('txn_id'),
                $e->getMessage(),
                $e->getCode()
            );
        }


        $form = $this->createCheckQiwiForm();
        $form
            ->add('command', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new EqualTo('pay'),
                ]
            ])
            ->add('txn_date', DateTimeType::class, [
                'constraints' => [
                    new NotBlank()
                ],
                'widget' => 'single_text'
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && !$form->isValid()) {
            return new QiwiTerminalResponse(
                $request->get('txn_id'),
                (string)$form->getErrors(true, false),
                300
            );
        }

        $data = $form->getData();

        $amount = new Money($data['sum'], 'RUB');
        $txn_id = $data['txn_id'];
        $txn_date = $data['txn_date'];

        try {
            $paymentOrder = $this->get('gorserv_gerp_app.billing.balance_manager')
                ->updateUserBalance($data['account'], $amount, ['txn_id' => $txn_id, 'txn_date' => $txn_date]);
        } catch (BillingException $exception) {
            return new QiwiTerminalResponse(
                $txn_id,
                $exception->getMessage(),
                300
            );
        } catch (UniqueConstraintViolationException $e) {
            $paymentOrder = $this->getDoctrine()->getManager()
                ->getRepository(PaymentOrder::class)
                ->findOneBy([
                    'transaction' => $txn_id
                ]);

            return new QiwiTerminalResponse(
                $paymentOrder->getTransaction(),
                'OK',
                0,
                $paymentOrder->getId(),
                $paymentOrder->getAmount()->getSum()
            );
        } catch (Exception $exception) {
            return new QiwiTerminalResponse(
                $txn_id,
                $exception->getMessage(),
                300
            );

        }

        return new QiwiTerminalResponse(
            $txn_id,
            'OK',
            0,
            $paymentOrder->getId(),
            sprintf('%.2f', $paymentOrder->getAmount()->getSum())
        );
    }

    /**
     * @Route("/pay_app")
     * @View()
     *
     * @return QiwiTerminalResponse
     */
    public function errorAction()
    {
        return new QiwiTerminalResponse(
            null,
            'UNKNOWN ERROR',
            300
        );
    }
}
