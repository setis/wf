<?php

namespace Gorserv\Gerp\AppBundle\Controller;

use Gorserv\Gerp\AppBundle\Form\BusinessUnitType;
use models\BusinessUnit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Businessunit controller.
 *
 * @Route("bu")
 */
class BusinessUnitController extends Controller
{
    /**
     * Lists all businessUnit entities.
     *
     * @Route("/", name="bu_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $businessUnits = $em->getRepository(BusinessUnit::class)->findAll();

        return $this->render('GorservGerpAppBundle:BusinessUnit:index.html.twig', array(
            'businessUnits' => $businessUnits,
        ));
    }

    /**
     * Creates a new businessUnit entity.
     *
     * @Route("/new", name="bu_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $businessUnit = new BusinessUnit();
        $form = $this->createForm(BusinessUnitType::class, $businessUnit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($businessUnit);
            $em->flush();

            return $this->redirectToRoute('bu_show', array('id' => $businessUnit->getId()));
        }

        return $this->render('GorservGerpAppBundle:BusinessUnit:new.html.twig', array(
            'businessUnit' => $businessUnit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a businessUnit entity.
     *
     * @Route("/{id}", name="bu_show")
     * @Method("GET")
     */
    public function showAction(BusinessUnit $businessUnit)
    {
        $deleteForm = $this->createDeleteForm($businessUnit);

        return $this->render('GorservGerpAppBundle:BusinessUnit:show.html.twig', array(
            'businessUnit' => $businessUnit,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to delete a businessUnit entity.
     *
     * @param BusinessUnit $businessUnit The businessUnit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BusinessUnit $businessUnit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bu_delete', array('id' => $businessUnit->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit an existing businessUnit entity.
     *
     * @Route("/{id}/edit", name="bu_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, BusinessUnit $businessUnit)
    {
        $deleteForm = $this->createDeleteForm($businessUnit);
        $editForm = $this->createForm(BusinessUnitType::class, $businessUnit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bu_edit', array('id' => $businessUnit->getId()));
        }

        return $this->render('GorservGerpAppBundle:BusinessUnit:edit.html.twig', array(
            'businessUnit' => $businessUnit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a businessUnit entity.
     *
     * @Route("/{id}", name="bu_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, BusinessUnit $businessUnit)
    {
        $form = $this->createDeleteForm($businessUnit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($businessUnit);
            $em->flush();
        }

        return $this->redirectToRoute('bu_index');
    }
}
