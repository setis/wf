<?php

namespace Gorserv\Gerp\AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Gorserv\Gerp\AppBundle\Filter\SyslogFilterType;
use models\Syslog;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\Prefix("/syslog")
 * @Rest\NamePrefix("app_syslog_")
 */
class SyslogController extends FOSRestController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getListAction(Request $request)
    {
        $form = $this->get('form.factory')->create(SyslogFilterType::class);
        /* @var $filterBuilder \Doctrine\ORM\QueryBuilder */
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
            ->getRepository(Syslog::class)
            ->createQueryBuilder('e')
            ->orderBy('e.id', 'desc');

        if ($request->query->has($form->getName())) {
            // manually bind values from the request
            $form->submit($request->query->get($form->getName()));

            // build the query from the given form object
            $this->get('lexik_form_filter.query_builder_updater')
                ->addFilterConditions($form, $filterBuilder);

        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filterBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            200/*limit per page*/
        );

        $view = $this->view($pagination->getItems(), 200)
            ->setTemplate('GorservGerpAppBundle:Syslog:list.html.twig')
            ->setTemplateData([
                'form' => $form->createView(),
                'pagination' => $pagination,
            ]);

        return $this->handleView($view);
    }

    /**
     * @param Syslog $syslog
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getViewAction(Syslog $syslog)
    {
        $view = $this->view(['syslog' => print_r($syslog, true)], 200)
            ->setTemplate('GorservGerpAppBundle:Syslog:view.html.twig');

        return $this->handleView($view);
    }
}
