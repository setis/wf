<?php
namespace Gorserv\Gerp\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gorserv\Gerp\BillingBundle\Model\MoneyInterface;

/**
 * Class Money
 *
 * @package Gorserv\Gerp\AppBundle\Entity
 * @author artem <mail@artemd.ru>
 *
 * @ORM\Embeddable
 */
class Money implements MoneyInterface
{
    const DIVISOR = 100;

    /**
     * @var integer
     *
     * @ORM\Column(type = "integer")
     */
    private $sum;

    /**
     * @var string
     *
     * @ORM\Column(type = "string")
     */
    private $currency;

    public function __construct($sum, $currency)
    {
        $this->sum = round($sum * static::DIVISOR);
        $this->currency = $currency;
    }

    public function __toString()
    {
        return sprintf('%.2f %s', $this->getSum(), $this->getCurrency());
    }

    /**
     * @return float
     */
    public function getSum()
    {
        return $this->sum / static::DIVISOR;
    }

    /**
     * @param float $sum
     * @return Money
     */
    public function setSum($sum): Money
    {
        $this->sum = round($sum * static::DIVISOR);
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Money
     */
    public function setCurrency(string $currency): Money
    {
        $this->currency = $currency;
        return $this;
    }
}
