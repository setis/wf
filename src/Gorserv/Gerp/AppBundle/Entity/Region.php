<?php

namespace Gorserv\Gerp\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use models\BillType;
use models\ListContr;
use models\ListSc;
use models\User;

/**
 * Region
 *
 * @ORM\Table(name="region")
 * @ORM\Entity(repositoryClass="Gorserv\Gerp\AppBundle\Repository\RegionRepository")
 */
class Region
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var double
     *
     * @ORM\Column(name="point_price", type="float", nullable=false)
     */
    private $pointPrice;


    /**
     * @var string
     *
     * @ORM\Column(name="kladr_code", type="string", length=80, nullable=true)
     */
    private $kladrCode;

    /**
     * @var ListContr
     *
     * @ORM\ManyToOne(targetEntity="models\ListContr")
     */
    private $company;

    /**
     * @var User[]
     *
     * @ORM\ManyToMany(targetEntity="models\User")
     * @ORM\JoinTable(name="region_director")
     */
    private $directors;

    /**
     * @var ListSc[]
     *
     * @ORM\OneToMany(targetEntity="models\ListSc", mappedBy="region", cascade={"persist"})
     */
    private $serviceCenters;

    /**
     * TODO implement this
     *
     * @var BillType
     */
    private $financialLimits;

    public function __construct()
    {
        $this->directors = new ArrayCollection();
        $this->serviceCenters = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Region
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Region
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return float
     */
    public function getPointPrice()
    {
        return $this->pointPrice;
    }

    /**
     * @param float $pointPrice
     * @return Region
     */
    public function setPointPrice(float $pointPrice)
    {
        $this->pointPrice = $pointPrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getKladrCode()
    {
        return $this->kladrCode;
    }

    /**
     * @param string $kladrCode
     * @return Region
     */
    public function setKladrCode(string $kladrCode)
    {
        $this->kladrCode = $kladrCode;
        return $this;
    }

    /**
     * @return ListContr
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param ListContr $company
     * @return Region
     */
    public function setCompany(ListContr $company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return \models\User[]
     */
    public function getDirectors()
    {
        return $this->directors;
    }

    /**
     * @param \models\User[] $directors
     * @return Region
     */
    public function setDirectors(array $directors)
    {
        $this->directors = $directors;
        return $this;
    }

    /**
     * @return ListSc[]
     */
    public function getServiceCenters()
    {
        return $this->serviceCenters;
    }

    /**
     * @param ListSc[] $serviceCenters
     * @return Region
     */
    public function setServiceCenters(array $serviceCenters)
    {
        $this->serviceCenters = $serviceCenters;
        foreach ($serviceCenters as $sc) {
            $sc->setRegion($this);
        }

        return $this;
    }

    /**
     * @param ListSc $serviceCenter
     * @return $this
     */
    public function addServiceCenter(ListSc $serviceCenter)
    {
        $this->serviceCenters->add($serviceCenter);
        $serviceCenter->setRegion($this);

        return $this;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Region
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }
}

