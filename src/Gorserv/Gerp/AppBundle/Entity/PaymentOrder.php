<?php

namespace Gorserv\Gerp\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gorserv\Gerp\BillingBundle\Model\BalanceOwnerInterface;
use Gorserv\Gerp\BillingBundle\Model\MoneyInterface;
use Gorserv\Gerp\BillingBundle\Model\PaymentOrderInterface;
use JMS\Payment\CoreBundle\Entity\PaymentInstruction;
use JMS\Payment\CoreBundle\Model\PaymentInstructionInterface;
use models\Task;
use models\User;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PaymentOrder
 *
 * @ORM\Table(name="payment_order")
 * @ORM\Entity(repositoryClass="Gorserv\Gerp\AppBundle\Repository\PaymentOrderRepository")
 * @UniqueEntity("transaction")
 */
class PaymentOrder implements PaymentOrderInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Money
     *
     * @ORM\Embedded(class="Gorserv\Gerp\AppBundle\Entity\Money")
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction", type="string", unique=true, nullable=true)
     */
    private $transaction;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="transaction_date", type="datetime")
     */
    private $transactionDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $owner;

    /**
     * @var PaymentInstruction
     *
     * @ORM\OneToOne(targetEntity="JMS\Payment\CoreBundle\Entity\PaymentInstruction", cascade={"remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $paymentInstruction;

    /**
     * @var Task
     *
     * @ORM\ManyToOne(targetEntity="models\Task")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $task;

    public function __construct()
    {
        $this->amount = new Money(0, 'RUB');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return MoneyInterface|Money
     */
    public function getAmount(): MoneyInterface
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     * @return PaymentOrder
     */
    public function setAmount(Money $amount): PaymentOrder
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return PaymentOrder
     */
    public function setCreatedAt(\DateTime $createdAt): PaymentOrder
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param string $transaction
     * @return PaymentOrder
     */
    public function setTransaction($transaction): PaymentOrder
    {
        $this->transaction = $transaction;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTransactionDate(): \DateTime
    {
        return $this->transactionDate;
    }

    /**
     * @param \DateTime $transactionDate
     * @return PaymentOrder
     */
    public function setTransactionDate(\DateTime $transactionDate): PaymentOrder
    {
        $this->transactionDate = $transactionDate;
        return $this;
    }

    /**
     * @return BalanceOwnerInterface|User
     */
    public function getOwner(): BalanceOwnerInterface
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     * @return PaymentOrder
     */
    public function setOwner(BalanceOwnerInterface $owner): PaymentOrder
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return PaymentInstructionInterface
     */
    public function getPaymentInstruction(): PaymentInstructionInterface
    {
        return $this->paymentInstruction;
    }

    /**
     * @param PaymentInstruction $paymentInstruction
     * @return PaymentOrder
     */
    public function setPaymentInstruction(PaymentInstruction $paymentInstruction): PaymentOrder
    {
        $this->paymentInstruction = $paymentInstruction;
        return $this;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param Task $task
     * @return PaymentOrder
     */
    public function setTask($task): PaymentOrder
    {
        $this->task = $task;
        return $this;
    }
}
