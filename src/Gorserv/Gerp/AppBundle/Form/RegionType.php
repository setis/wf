<?php

namespace Gorserv\Gerp\AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Gorserv\Gerp\AppBundle\Entity\Region;
use models\ListContr;
use models\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'Название',
            ])
            ->add('pointPrice', null, [
                'label' => 'Стоимость балла',
            ])
            ->add('kladrCode', null, [
                'label' => 'Код КЛАДР',
            ])
            ->add('company', EntityType::class, array(
                'label' => 'Организация',
                'class' => ListContr::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('partner')
                        ->andWhere('partner.isSupplier = true')
                        ->orderBy('partner.contrTitle', 'ASC');
                },
            ))
            ->add('directors', EntityType::class, [
                    'label' => 'Директора ',
                    'class' => User::class,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->addSelect('slackUser, moneyLimit, skpDebt')
                            ->leftJoin('u.slackUser', 'slackUser')
                            ->leftJoin('u.moneyLimit', 'moneyLimit')
                            ->leftJoin('u.skpDebt', 'skpDebt')
                            ->andWhere('u.active = true')
                            ->orderBy('u.fio', 'ASC');
                    },
                ]
            )
            ->add('serviceCenters', null, [
                'label' => 'Сервисные центры',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Сохранить',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Region::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gorserv_gerp_corebundle_region';
    }


}
