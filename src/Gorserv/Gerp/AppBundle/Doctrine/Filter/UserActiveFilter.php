<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 16.01.17
 * Time: 17:19
 */

namespace Gorserv\Gerp\AppBundle\Doctrine\Filter;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Gorserv\Gerp\AppBundle\Model\UserInterface;

class UserActiveFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        // Check if the entity implements the LocalAware interface
        if (!$targetEntity->reflClass->implementsInterface(UserInterface::class)) {
            return "";
        }

        return $targetTableAlias.'.active = ' . $this->getParameter('active'); // getParameter applies quoting automatically
    }
}
