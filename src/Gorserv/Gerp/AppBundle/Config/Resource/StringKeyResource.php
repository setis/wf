<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 08.02.17
 * Time: 11:50
 */

namespace Gorserv\Gerp\AppBundle\Config\Resource;


use Symfony\Component\Config\Resource\ResourceInterface;

class StringKeyResource implements ResourceInterface
{
    /**
     * @var string
     */
    private $key;

    /**
     * RestResource constructor.
     * @param $key string
     */
    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     *
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->key;
    }
}