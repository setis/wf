<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 24.01.17
 * Time: 18:26
 */

namespace Gorserv\Gerp\AppBundle\Command;

use Gorserv\Gerp\AppBundle\EventDispatcher\Events\TaskEvent;
use Gorserv\Gerp\AppBundle\EventDispatcher\TaskEvents;
use models\Task;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Cron job /10 * * * *
 * @package Gorserv\Gerp\AppBundle\Command
 *
 */
class CheckDelayedTaskCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('gerp:task:check-delayed')
            ->setDescription('Check delayed task and trigger event if delay time is occurred');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $qb = $em->getRepository(Task::class)
            ->createQueryBuilder('task')
            ->where('task.pluginUid IN (:plugins)')
            ->andWhere('task.delayedTo <= :now')
            ->setParameter('plugins', [Task::CONNECTION, Task::SERVICE])
            ->setParameter('now', new \DateTime())
        ;

        $tasks = $qb->getQuery()->iterate();

        $dispatcher = $this->getContainer()->get('event_dispatcher');

        foreach ($tasks as $row) {
            $dispatcher->dispatch(TaskEvents::DELAY_DATE_OCCURRED, new TaskEvent($row[0]));
        }
    }
}