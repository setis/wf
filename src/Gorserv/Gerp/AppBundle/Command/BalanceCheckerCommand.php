<?php

namespace Gorserv\Gerp\AppBundle\Command;

use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\AppBundle\Billing\BlockerHandlerInterface;
use models\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BalanceCheckerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('gerp:balance:check')
            ->setDescription('Check and block owe users.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var BlockerHandlerInterface $blocker */
        $blocker = $this->getContainer()->get('gorserv_gerp_app.billing.simpler_blocker');
        /** @var DateInterval $defaultInterval */
        $defaultInterval = new DateInterval($this->getContainer()->getParameter('technician_financial_block_timeout'));

        $users = $em->getRepository(User::class)
            ->createQueryBuilder('u')
            ->andWhere('u.balance.amount < 0')
            ->andWhere('u.balanceBecameNegativeAt < :interval')
            ->setParameter('interval', (new \DateTime())->sub($defaultInterval))
            ->getQuery()
            ->getResult();

        foreach ($users as $user) {
            $blocker->block($user);
        }

        $output->writeln('Command result.');
    }

}
