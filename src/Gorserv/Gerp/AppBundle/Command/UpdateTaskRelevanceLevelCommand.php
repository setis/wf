<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 25.01.17
 * Time: 15:26
 */

namespace Gorserv\Gerp\AppBundle\Command;


use Gorserv\Gerp\AppBundle\EventDispatcher\Events\TaskRelevanceLevelChangedEvent;
use Gorserv\Gerp\AppBundle\EventDispatcher\TaskEvents;
use models\Task;
use models\TaskStatus;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateTaskRelevanceLevelCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('gerp:task:update-relevance-level')
            ->setDescription('Update tasks relevance level');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $ed = $this->getContainer()->get('event_dispatcher');

        $qb = $em->getRepository(Task::class)
            ->createQueryBuilder('task')
            ->where('task.pluginUid IN (:plugins)')
            ->andWhere('task.relevanceLevel IS NOT NULL')
            ->setParameter('plugins', [ Task::SERVICE ])
        ;

        $tasks = $qb->getQuery()->iterate();
        $now = new \DateTime();

        foreach ($tasks as $row) {
            /** @var Task $task */
            $task = $row[0];
            $dateToCompare = $task->getLastStatusDate() ?? $task->getDateReg();
            $diff = $now->diff($dateToCompare);
            $oldLevel = $task->getRelevanceLevel();
            $task->setRelevanceLevel($this->mapDiffToRelevanceLevel($diff));
            $em->flush($task);

            $event = new TaskRelevanceLevelChangedEvent($task, $oldLevel);
            $ed->dispatch(TaskEvents::RELEVANCE_LEVEL_CHANGED, $event);
        }
    }

    private function mapDiffToRelevanceLevel(\DateInterval $diff)
    {
        $minutes = $diff->h * 60 + $diff->i;

        // 10 minutes is 1 relevance level
        return floor($minutes / 10);
    }

}