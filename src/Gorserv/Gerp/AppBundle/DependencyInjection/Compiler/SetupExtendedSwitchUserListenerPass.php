<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 01.12.16
 * Time: 16:03
 */

namespace Gorserv\Gerp\AppBundle\DependencyInjection\Compiler;


use Gorserv\Gerp\AppBundle\Security\Http\Firewall\ExtendedSwitchUserListener;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SetupExtendedSwitchUserListenerPass implements CompilerPassInterface
{
    /**
     * @var string
     */
    private $firewallId;

    /**
     * SetupExtendedSwitchUserListenerPass constructor.
     * @param string $firewallId
     */
    public function __construct($firewallId)
    {
        $this->firewallId = $firewallId;
    }

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $listener = $container->findDefinition('security.authentication.switchuser_listener.' . $this->firewallId);
        $listener->setClass(ExtendedSwitchUserListener::class);
    }
}
