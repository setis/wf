<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 08.02.17
 * Time: 11:46
 */

namespace Gorserv\Gerp\AppBundle\DependencyInjection\Loader;


use Gorserv\Gerp\AppBundle\Config\Resource\StringKeyResource;
use Gorserv\Gerp\AppBundle\DependencyInjection\ArrayToContainerBuilderInjector;
use GuzzleHttp\ClientInterface;
use Symfony\Component\Config\Resource\ResourceInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class RestJsonLoader extends ExternalLoader
{
    /**
     * @var ClientInterface
     */
    private $httpClient;

    public function __construct(ContainerBuilder $container, ClientInterface $httpClient)
    {
        parent::__construct($container);
        $this->httpClient = $httpClient;
    }

    protected function getResource($resource): ResourceInterface
    {
        return new StringKeyResource($resource);
    }

    protected function doLoad(string $resource, $type = null)
    {
        $response = $this->httpClient->request('GET', $resource);
        if (200 !== $response->getStatusCode()) {
            throw new \Exception($response->getReasonPhrase());
        }
        $content = $response->getBody()->getContents();
        $content = \GuzzleHttp\json_decode($content, true);

        $injector = new ArrayToContainerBuilderInjector();
        $injector->inject($this->container, $content, $resource);
    }

    /**
     * Returns whether this class supports the given resource.
     *
     * @param mixed $resource A resource
     * @param string|null $type The resource type or null if unknown
     *
     * @return bool True if this class supports the given resource, false otherwise
     */
    public function supports($resource, $type = null)
    {
        return false !== strpos($resource, '.json');
    }

}