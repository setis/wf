<?php
/**
 * User: Pavel Stepanets <pahhan.ne@gmail.com>
 * Date: 08.02.17
 * Time: 2:06
 */

namespace Gorserv\Gerp\AppBundle\DependencyInjection\Loader;


use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Config\Resource\ResourceInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

abstract class ExternalLoader extends Loader
{
    /**
     * @var ContainerBuilder
     */
    protected $container;

    /**
     * @param ContainerBuilder     $container A ContainerBuilder instance
     */
    public function __construct(ContainerBuilder $container)
    {
        $this->container = $container;
    }

    public function load($resource, $type = null)
    {
        $resource = $this->getResource($resource);
        $this->container->addResource($resource);

        $this->doLoad($resource, $type);
    }

    abstract protected function getResource($resource) : ResourceInterface;

    abstract protected function doLoad(string $resource, $type = null);
}