<?php
namespace GorservGerpAppBundle;

use DateTime;
use Gorserv\Gerp\AppBundle\Schedule\IntervalCheckerInterface;
use Gorserv\Gerp\ScheduleBundle\Exceptions\ScheduleException;
use models\Task;


class IntervalCheckerTest extends \Codeception\Test\Unit
{
    /**
     * @var \GorservGerpAppBundle\UnitTester
     */
    protected $tester;

    public function testСheckTimeframeInterval()
    {
        /** @var IntervalCheckerInterface $checker */
        $checker = $this->tester->grabService('gorserv_gerp_app.schedule.interval_checker');

        $start = new DateTime();
        $end = (clone $start)->modify('+1 month');
        list($newStart, $newEnd) = $checker->checkTimeframeInterval($start, $end);

        $this->assertEquals($start->modify('+6 hour')->format('Y-m-d H:i:s'), $newStart->format('Y-m-d H:i:s'));
        $this->assertEquals($start->modify('+7 days')->format('Y-m-d H:i:s'), $newEnd->format('Y-m-d H:i:s'));

        $start = (new DateTime())->modify('+1 day');
        $end = (clone $start)->modify('-1 month');
        list($newStart, $newEnd) = $checker->checkTimeframeInterval($start, $end);

        $this->assertEquals($start->format('Y-m-d H:i:s'), $newStart->format('Y-m-d H:i:s'));
        $this->assertEquals($start->modify('+6 hours')->format('Y-m-d H:i:s'), $newEnd->format('Y-m-d H:i:s'));

        $start = (new DateTime())->modify('+1 day');
        $end = (clone $start)->modify('+1 month');
        list($newStart, $newEnd) = $checker->checkTimeframeInterval($start, $end);

        $this->assertEquals($start->format('Y-m-d H:i:s'), $newStart->format('Y-m-d H:i:s'));
        $this->assertEquals($start->modify('+7 day')->format('Y-m-d H:i:s'), $newEnd->format('Y-m-d H:i:s'));
    }

    public function testCheckTaskIntervalSixHours()
    {
        /** @var IntervalCheckerInterface $checker */
        $checker = $this->tester->grabService('gorserv_gerp_app.schedule.interval_checker');

        $s = new DateTime();
        $e = clone $s;
        $this->expectException(ScheduleException::class);
        $checker->checkTaskInterval(new Task(), $s, $e);
    }

    public function testCheckTaskIntervalStartGtEnd()
    {
        /** @var IntervalCheckerInterface $checker */
        $checker = $this->tester->grabService('gorserv_gerp_app.schedule.interval_checker');

        $s = new DateTime();
        $e = (clone $s)->modify('-1 day');
        $this->expectException(ScheduleException::class);
        $checker->checkTaskInterval(new Task(), $s, $e);
    }

    public function testCheckTaskIntervalVeryLongSlot()
    {
        /** @var IntervalCheckerInterface $checker */
        $checker = $this->tester->grabService('gorserv_gerp_app.schedule.interval_checker');

        $s = new DateTime();
        $e = (clone $s)->modify('+2 day');
        $this->expectException(ScheduleException::class);
        $checker->checkTaskInterval(new Task(), $s, $e);
    }
}
