<?php

namespace GorservGerpAppBundle;

use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\AppBundle\Schedule\TimeframeManager;

/**
 * Description of TimeframeManagerTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TimeframeManagerTest extends \Codeception\Test\Unit
{
    /**
     * @var \GorservGerpAppBundle\UnitTester
     */
    protected $tester;

    public function testFindAvailableExecutors()
    {
        /* @var $manager TimeframeManager */
        $manager = $this->tester->grabService('app.schedule.timeframe_manager');
        /** @var EntityManagerInterface $em */
        $em = $this->tester->grabService('doctrine.orm.default_entity_manager');

        // This is bad test
        $job = $em->getRepository(\models\Task::class)->findOneBy([]);
        $start = new \DateTime('2016-11-21 08:00:00');
        $end = clone $start;
        $end->add(new \DateInterval('PT120M'));

        $executors = $manager->findAvailableExecutors($job, $start, $end);
        echo count($executors);
    }
}
