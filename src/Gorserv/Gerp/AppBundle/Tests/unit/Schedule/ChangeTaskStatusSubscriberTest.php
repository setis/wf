<?php

namespace GorservGerpAppBundle;

use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\AppBundle\EventDispatcher\ChangeTaskScheduleSubscriber;
use Gorserv\Gerp\ScheduleBundle\EventDispatcher\SlotDeletedEvent;
use Gorserv\Gerp\ScheduleBundle\EventDispatcher\SlotEvent;
use Gorserv\Gerp\ScheduleBundle\Model\ScheduleManagerInterface;
use models\Gfx;
use models\Task;
use models\TaskComment;
use models\TaskStatus;
use repository\TaskStatusRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Task\TaskManager;

/**
 * Class ChangeTaskStatusSubscriberTest
 *
 * @package Gorserv\Gerp\AppBundle\Tests\Schedule
 * @author artem <mail@artemd.ru>
 */
class ChangeTaskStatusSubscriberTest extends \Codeception\Test\Unit
{
    /**
     * @var \GorservGerpAppBundle\UnitTester
     */
    protected $tester;

    public function testOnSlotCreated()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $sm = $this->createMock(ScheduleManagerInterface::class);
        $ed = $this->createMock(EventDispatcherInterface::class);

        $taskStatusRepo = $this->createMock(TaskStatusRepository::class);
        $taskStatusRepo->method('findOneByTag')
            ->willReturn(
                (new TaskStatus())
            );

        $em->method('getRepository')
            ->willReturn($taskStatusRepo);

        $task = $this->getMockBuilder(Task::class)
            ->setMethods(['getPluginUid'])
            ->getMock();

        $slot = $this->getMockBuilder(Gfx::class)
            ->setConstructorArgs([$task])
            ->setMethods(['getJob', 'getTimeStart', 'getTimeEnd', 'getExecutors'])
            ->getMock();

        $slot->method('getJob')
            ->willReturn($task);

        $slot->method('getTimeStart')
            ->willReturn(new \DateTime('-1 hour'));

        $slot->method('getTimeEnd')
            ->willReturn(new \DateTime());

        $slot->method('getExecutors')
            ->willReturn([]);

        $taskManager = $this->getMockBuilder(TaskManager::class)
            ->setConstructorArgs([$em, $sm, $ed])
            ->setMethods(['addComment'])
            ->getMock();

        $taskManager->method('addComment')
            ->with($this->equalTo($task), $this->isInstanceOf(TaskComment::class));

        $event = new SlotEvent($slot);

        $subscriber = new \Gorserv\Gerp\AppBundle\EventDispatcher\ChangeTaskScheduleSubscriber($taskManager, $em);

        $subscriber->onSlotCreated($event);
    }

    public function testOnSlotDeleted()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $sm = $this->createMock(ScheduleManagerInterface::class);
        $ed = $this->createMock(EventDispatcherInterface::class);

        $taskStatusRepo = $this->createMock(TaskStatusRepository::class);
        $taskStatusRepo->method('findOneBy')
            ->willReturn(
                (new TaskStatus())
            );

        $em->method('getRepository')
            ->willReturn($taskStatusRepo);

        $task = $this->getMockBuilder(Task::class)
            ->setMethods(['getPluginUid'])
            ->getMock();

        $slot = $this->getMockBuilder(Gfx::class)
            ->setConstructorArgs([$task])
            ->setMethods(['getJob'])
            ->getMock();

        $slot->method('getJob')
            ->willReturn($task);

        $taskManager = $this->getMockBuilder(TaskManager::class)
            ->setConstructorArgs([$em, $sm, $ed])
            ->setMethods(['addComment'])
            ->getMock();

        $taskManager->method('addComment')
            ->with($this->equalTo($task), $this->isInstanceOf(TaskComment::class));

        $event = new SlotDeletedEvent($slot);

        $subscriber = new ChangeTaskScheduleSubscriber($taskManager, $em);

        $subscriber->onSlotDeleted($event);
    }
}
