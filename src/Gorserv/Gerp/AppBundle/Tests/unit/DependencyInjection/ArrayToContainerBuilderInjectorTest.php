<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 08.02.17
 * Time: 17:17
 */

namespace GorservGerpAppBundle;

use Gorserv\Gerp\AppBundle\DependencyInjection\ArrayToContainerBuilderInjector;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Codeception\Test\Unit;

class ArrayToContainerBuilderInjectorTest  extends Unit
{
    /**
     * @var \GorservGerpAppBundle\UnitTester
     */
    protected $tester;

    public function testInject()
    {
        $container = new ContainerBuilder();
        $injector = new ArrayToContainerBuilderInjector();

        $content = [
            'parameters' => [
                'integer' => 3,
                'array' => ['foo', 'bar'],
                'string' => 'baz'
            ]
        ];

        $injector->inject($container, $content, null);
        $this->assertEquals(3, $container->getParameter('integer'));
        $this->assertEquals(['foo', 'bar'], $container->getParameter('array'));
        $this->assertEquals('baz', $container->getParameter('string'));

        $content = ['services' => ['test_service' => ['class' => 'stdClass']]];
        $injector->inject($container, $content, null);
        $this->assertTrue($container->hasDefinition('test_service'));
        $test = $container->get('test_service');
        $this->assertInstanceOf('stdClass', $test);

    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testWrongNamespace()
    {
        $container = new ContainerBuilder();
        $injector = new ArrayToContainerBuilderInjector();

        $injector->inject($container, ['foo' => []], null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testParametersNotArray()
    {
        $container = new ContainerBuilder();
        $injector = new ArrayToContainerBuilderInjector();

        $injector->inject($container, ['parameters' => 'foo'], null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testServicesNotArray()
    {
        $container = new ContainerBuilder();
        $injector = new ArrayToContainerBuilderInjector();

        $injector->inject($container, ['services' => 'foo'], null);
    }
}