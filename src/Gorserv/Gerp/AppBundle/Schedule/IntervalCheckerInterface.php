<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 1/12/17
 * Time: 7:51 PM
 */
namespace Gorserv\Gerp\AppBundle\Schedule;

use DateTime;
use models\Task;

interface IntervalCheckerInterface
{
    public function checkTaskInterval(Task $task, DateTime $start, DateTime $end);

    public function checkTimeframeInterval(DateTime $start, DateTime $end);
}
