<?php

namespace Gorserv\Gerp\AppBundle\Schedule;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\ScheduleBundle\Model\AbstractTimeframeManager;
use Gorserv\Gerp\ScheduleBundle\Model\JobInterface;
use InvalidArgumentException;
use LogicException;
use models\ExecutorTimeframe;
use models\Gfx;
use models\Task;
use models\User;
use repository\ExecutorTimeframeRepository;
use repository\GfxRepository;
use repository\UserRepository;

/**
 * Description of TimeframeManager
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TimeframeManager extends AbstractTimeframeManager
{
    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    protected function doDeleteTimeframe($executor, DateTime $time)
    {

    }

    protected function doFindTimeframe($executor, DateTime $time)
    {
        return $this->getTimeframeRepository()->findOneByTime($executor, $time);
    }

    /**
     *
     * @return ExecutorTimeframeRepository
     */
    private function getTimeframeRepository()
    {
        return $this->em->getRepository(ExecutorTimeframe::class);
    }

    /**
     *
     * @param ExecutorTimeframe $timeframe
     * @param DateTime $timeStart
     * @param DateTime $timeEnd
     * @return boolean
     */
    protected function doIsSlotAvailable($timeframe, DateTime $timeStart, DateTime $timeEnd): bool
    {
        if (!$this->isAcceptableToPeriod($timeframe, $timeStart, $timeEnd)) {
            return false;
        }

        $slots = $this->getSlotRepository()
            ->findByExecutorAndTime($timeframe->getExecutor(), $timeStart, $timeEnd);

        return empty($slots);
    }

    /**
     *
     * @return GfxRepository
     */
    private function getSlotRepository()
    {
        return $this->em->getRepository(Gfx::class);
    }

    protected function doSetupTimeframe($executor, DateTime $timeStart, DateTime $timeEnd)
    {

    }

    /**
     *
     * @param Task $job
     * @param DateTime $start
     * @param DateTime $end
     * @return User[]
     */
    protected function doFindAvailableExecutors($job, DateTime $start, DateTime $end)
    {
        $this->validateJob($job);

        $ticket = $job->getTicket();
        if (null === $ticket) {
            throw new LogicException('Ticket is not set in task ' . $job->getId());
        }

        $partner = $ticket->getPartner();
        if (null === $partner) {
            throw new LogicException('Partner is not set in ticket ' . $job->getId());
        }

        $type = $ticket->getTaskType();
        if (null === $type) {
            throw new LogicException('Type is not set in ticket ' . $job->getId());
        }

        $address = $ticket->getDom();
        if (null === $address) {
            throw new LogicException('Address is not set in ticket ' . $job->getId());
        }

        $qb = $this->em->createQueryBuilder()
            ->select('timeframe, user')
            ->from(ExecutorTimeframe::class, 'timeframe')
            ->join('timeframe.executor', 'user')
            ->join('user.partners', 'partner')
            ->join('user.taskTypes', 'types')
            ->where('user.active = true')
            ->andWhere('timeframe.timeStart <= :start AND timeframe.timeEnd >= :end')
            ->andWhere('types.id = :type')
            ->andWhere('ST_Contains(user.area, ST_GeomFromText(:point)) > 0')
            ->andWhere('partner.id = :partner')
            ->andWhere('NOT EXISTS ('
                . 'SELECT slot.id '
                . 'FROM ' . Gfx::class . ' slot '
                . 'WHERE slot.technician = timeframe.executor AND (:start <= slot.timeEnd AND :end >= slot.timeStart))')
            ->setParameter('point', 'Point(' . $address->getCoordinates() . ')')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('type', $type->getId())
            ->setParameter('partner', $partner->getId());

        return array_map(function (ExecutorTimeframe $timeframe) {
            return $timeframe->getExecutor();
        }, $qb->getQuery()->getResult());
    }

    /**
     * @param JobInterface $job
     * @throws InvalidArgumentException
     * @return void
     */
    private function validateJob(JobInterface $job)
    {
        if (!$job instanceof Task) {
            throw new InvalidArgumentException(__CLASS___ . ' supports only job of type ' . Task::class);
        }
    }

    /**
     * @param Task $job
     * @param DateTime $start
     * @param DateTime $end
     * @return ExecutorTimeframe[]
     */
    protected function doFindAvailableTimeframes($job, DateTime $start, DateTime $end): array
    {
        $this->validateJob($job);

        $timeframes = $this->em->getRepository(ExecutorTimeframe::class)
            ->findByJob($job, $start, $end);

        $qb = $this->em->createQueryBuilder()
            ->select('slot')
            ->from(Gfx::class, 'slot')
            ->join('slot.technician', 'user')
            ->where('slot.timeStart >= :start')
            ->andWhere('slot.timeEnd <= :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end);
        $slots = $qb->getQuery()->getResult();

        foreach ($timeframes as $timeframe) {
            $frameSlots = array_filter($slots, function (Gfx $slot) use ($timeframe) {
                if ($slot->getTechnician() !== $timeframe->getUserExecutor()) {
                    return false;
                }

                return ($slot->getTimeStart() >= $timeframe->getTimeStart() && $slot->getStartTime() <= $timeframe->getTimeEnd())
                || ($slot->getTimeEnd() >= $timeframe->getTimeStart() && $slot->getTimeEnd() <= $timeframe->getTimeEnd());
            });

            $timeframe->setSlots($frameSlots);
        }

        return $timeframes;
    }

    /**
     *
     * @return UserRepository
     */
    private function getUserRepository()
    {
        return $this->em->getRepository(User::class);
    }

}
