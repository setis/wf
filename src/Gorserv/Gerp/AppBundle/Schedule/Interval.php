<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 10.12.16
 * Time: 2:38
 */

namespace Gorserv\Gerp\AppBundle\Schedule;


use DateTime;
use Gorserv\Gerp\ScheduleBundle\Model\AbstractInterval;

class Interval extends AbstractInterval
{
    /**
     * @var DateTime
     */
    private $timeStart;

    /**
     * @var DateTime
     */
    private $timeEnd;

    /**
     * Interval constructor.
     * @param DateTime $timeStart
     * @param DateTime $timeEnd
     */
    public function __construct(DateTime $timeStart, DateTime $timeEnd)
    {
        $this->timeStart = $timeStart;
        $this->timeEnd = $timeEnd;
    }

    /**
     * @return DateTime
     */
    public function getTimeStart(): DateTime
    {
        return $this->timeStart;
    }

    /**
     * @return DateTime
     */
    public function getTimeEnd(): DateTime
    {
        return $this->timeEnd;
    }
}