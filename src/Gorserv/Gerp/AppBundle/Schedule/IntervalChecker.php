<?php
namespace Gorserv\Gerp\AppBundle\Schedule;

use DateInterval;
use DateTime;
use Gorserv\Gerp\ScheduleBundle\Exceptions\ScheduleException;
use models\Task;

class IntervalChecker implements IntervalCheckerInterface
{

    private $maxTaskDuration;
    private $slotBeginOffset;
    private $timeframeMaxInterval;
    private $timeframeDefaultInterval;

    public function __construct(
        $maxTaskDuration = 'PT6H',
        $slotBeginOffset = 'PT6H',
        $timeframeMaxInterval = 'P7D',
        $timeframeDefaultInterval = 'P1D'
    )
    {
        $this->maxTaskDuration = new DateInterval($maxTaskDuration);
        $this->slotBeginOffset = new DateInterval($slotBeginOffset);
        $this->timeframeMaxInterval = new DateInterval($timeframeMaxInterval);
        $this->timeframeDefaultInterval = new DateInterval($timeframeDefaultInterval);
    }

    public function checkTaskInterval(Task $task, DateTime $start, DateTime $end)
    {
        $now = new DateTime();

        if ($start > $end) {
            throw new ScheduleException('Slot time start can\'t be grater than end.');
        }

        if ($start < $now->add($this->slotBeginOffset)) {
            throw new ScheduleException('Slot time start must be + 6 hours.');
        }

        $interval = $end->getTimestamp() - $start->getTimestamp();
        $maxTaskDurationSeconds =
            $this->maxTaskDuration->d * 24 * 60 * 60 +
            $this->maxTaskDuration->h * 60 * 60 +
            $this->maxTaskDuration->i * 60 +
            $this->maxTaskDuration->s;

        if ($interval > $maxTaskDurationSeconds) {
            throw new ScheduleException('Very long slot.');
        }
    }

    public function checkTimeframeInterval(DateTime $start, DateTime $end)
    {
        $defaultTimeframeIntervalSeconds = (new DateTime())->add($this->timeframeMaxInterval)->getTimestamp() - (new DateTime())->getTimestamp();

        $defaultStartTime = (new DateTime())->add($this->slotBeginOffset);
        $defaultEndTime = (clone $defaultStartTime)->add($this->timeframeDefaultInterval);

        $startTime = !empty($start) ? $start : $defaultStartTime;

        if ($startTime < $defaultStartTime) {
            $startTime = $defaultStartTime;
        }

        $endTime = !empty($end) ? $end : $defaultEndTime;
        // проверяем что время конца запрашваемого интервала не меньше времени начала
        if (($endTime->getTimestamp() - $startTime->getTimestamp()) < 0) {
            $endTime = $defaultEndTime;
        } elseif (($endTime->getTimestamp() - $startTime->getTimestamp()) > $defaultTimeframeIntervalSeconds) {
            // проверяем что дата конца запрашиваеомго интервала не бошьше 7 дней
            $endTime = (clone $startTime)->add($this->timeframeMaxInterval);
        }

        return [$startTime, $endTime];

    }
}
