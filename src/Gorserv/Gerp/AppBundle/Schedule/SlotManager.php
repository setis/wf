<?php

namespace Gorserv\Gerp\AppBundle\Schedule;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\ScheduleBundle\Model\AbstractSlotManager;
use InvalidArgumentException;
use models\Gfx;
use models\Task;
use models\User;

/**
 * Description of SlotManager
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SlotManager extends AbstractSlotManager
{
    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Task $job
     * @param DateTime $start
     * @param DateTime $end
     * @param User[] $executors
     * @param User $operator
     * @return Gfx
     *
     * @throws InvalidArgumentException
     */
    protected function doBuild($job, DateTime $start, DateTime $end, array $executors, $operator = null)
    {
        if (count($executors) > 1) {
            throw new InvalidArgumentException('This realisation not supports multiple executors');
        }

        // For tickets which ends at last second of day
        $endWithoutSecond = (clone $end)->modify('-1 second');

        if ($start->format('Ymd') !== $endWithoutSecond->format('Ymd')) {
            throw new InvalidArgumentException('This realisation not supports "start" and "end" time form different dates');
        }

        $slot = (new Gfx($job))
            ->setTimeStart($start)
            ->setTimeEnd($end)
            ->setTechnician($executors[0])
            ->setCreatedBy($operator);

        $job->setSchedule($slot);

        $this->em->persist($slot);
        $this->em->flush();

        return $slot;
    }

    /**
     *
     * @param Gfx $slot
     */
    protected function doDelete($slot)
    {
        $task = $slot->getTask();
        $task->setSchedule(null);
        $this->em->remove($slot);
        $this->em->flush();
    }

}
