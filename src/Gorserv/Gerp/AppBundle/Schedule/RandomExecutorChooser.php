<?php

namespace Gorserv\Gerp\AppBundle\Schedule;

use DateTime;
use Gorserv\Gerp\ScheduleBundle\Model\ExecutorChooserInterface;
use Gorserv\Gerp\ScheduleBundle\Model\JobInterface;

/**
 * Description of RandomExecutorChooser
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class RandomExecutorChooser implements ExecutorChooserInterface
{
    public function choose(array $executors, JobInterface $job, DateTime $timeStart, DateTime $timeEnd): array
    {
        if (empty($executors)) {
            return [];
        }

        $key = array_rand($executors);
        return [ $executors[$key] ];
    }

}
