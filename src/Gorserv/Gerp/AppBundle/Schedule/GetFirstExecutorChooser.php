<?php

namespace Gorserv\Gerp\AppBundle\Schedule;

use DateTime;
use Gorserv\Gerp\ScheduleBundle\Model\ExecutorChooserInterface;
use Gorserv\Gerp\ScheduleBundle\Model\JobInterface;

/**
 * Description of GetFirstExecutorChooser
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class GetFirstExecutorChooser implements ExecutorChooserInterface
{
    public function choose(array $executors, JobInterface $job, DateTime $timeStart, DateTime $timeEnd): array
    {
        return array_slice($executors, 0, 1);
    }

}
