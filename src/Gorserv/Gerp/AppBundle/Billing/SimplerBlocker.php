<?php
namespace Gorserv\Gerp\AppBundle\Billing;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Gorserv\Gerp\BillingBundle\Model\BalanceOwnerInterface;
use Gorserv\Gerp\ScheduleBundle\Model\ExecutorChooserInterface;
use Gorserv\Gerp\ScheduleBundle\Model\ScheduleManagerInterface;
use models\Gfx;
use models\TaskComment;
use models\TaskStatus;
use models\User;
use WF\Task\TaskManager;

/**
 * Class SimplerBlocker
 *
 * @package Gorserv\Gerp\AppBundle\Billing
 * @author artem <mail@artemd.ru>
 */
class SimplerBlocker implements BlockerHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ScheduleManagerInterface
     */
    private $sm;

    /**
     * @var TaskManager
     */
    private $tm;

    /**
     * @var ExecutorChooserInterface
     */
    private $executorChooser;

    public function __construct(EntityManagerInterface $em, ScheduleManagerInterface $sm, TaskManager $tm, ExecutorChooserInterface $executorChooser)
    {
        $this->em = $em;
        $this->sm = $sm;
        $this->tm = $tm;
        $this->executorChooser = $executorChooser;
    }

    /**
     * @param BalanceOwnerInterface|User $owner
     */
    public function block(BalanceOwnerInterface $owner)
    {
        $owner->setActive(false);
        $this->em->flush();

        $slots = $this->em->getRepository(Gfx::class)
            ->findByExecutorAndTime($owner, new DateTime(), (new DateTime())->modify('+ 1000 years'));

        foreach ($slots as $slot) {
            $this->sm->deleteSlot($slot);

            $executors = $this->sm->findAvailableExecutors($slot->getTask(), $slot->getTimeStart(), $slot->getTimeEnd(), $this->executorChooser);
            if (!empty($executors)) {
                $this->sm->createSlot($slot->getTask(), $slot->getTimeStart(), $slot->getTimeEnd(), $executors);
            } else {
                $status = $this->em->getRepository(TaskStatus::class)
                    ->getDefault($slot->getTask()->getPluginUid());

                $comment = (new TaskComment())
                    ->setStatus($status)
                    ->setText('Нет исполнителей.')
                    ->setTag('Смена статуса по финансовой блокировке.');

                $this->tm->addComment($slot->getTask(), $comment);
            }
        }

    }
}
