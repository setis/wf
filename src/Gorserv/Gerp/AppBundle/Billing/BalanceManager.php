<?php
namespace Gorserv\Gerp\AppBundle\Billing;


use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Gorserv\Gerp\AppBundle\Entity\Money;
use Gorserv\Gerp\AppBundle\Entity\PaymentOrder;
use Gorserv\Gerp\BillingBundle\EventDispatcher\BalanceEvent;
use Gorserv\Gerp\BillingBundle\EventDispatcher\Events;
use Gorserv\Gerp\BillingBundle\Exceptions\BillingException;
use Gorserv\Gerp\BillingBundle\Model\AbstractBalanceManager;
use Gorserv\Gerp\BillingBundle\Model\BalanceOwnerInterface;
use Gorserv\Gerp\BillingBundle\Model\MoneyInterface;
use Gorserv\Gerp\BillingBundle\Model\PaymentOrderInterface;
use Gorserv\Gerp\BillingBundle\Plugin\QiwiTerminalPlugin;
use JMS\Payment\CoreBundle\Entity\PaymentInstruction;
use JMS\Payment\CoreBundle\PluginController\PluginControllerInterface;
use models\Task;
use models\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BalanceManager
 *
 * @author artem <mail@artemd.ru>
 */
class BalanceManager extends AbstractBalanceManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EventDispatcherInterface
     */
    private $ed;

    /**
     * @var PluginControllerInterface
     */
    private $ppc;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger, EventDispatcherInterface $eventDispatcher, PluginControllerInterface $pluginController)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->ed = $eventDispatcher;
        $this->ppc = $pluginController;
    }

    /**
     * @param BalanceOwnerInterface|User $user
     * @return MoneyInterface
     */
    public function getUserBalance(BalanceOwnerInterface $user): MoneyInterface
    {
        return new Money($user->getBalance()->getSum(), $user->getBalance()->getCurrency());
    }

    /**
     * @param BalanceOwnerInterface $user
     * @param MoneyInterface $amount
     * @param array $extra
     * @return PaymentOrderInterface
     * @throws BillingException
     * @throws Exception
     */
    function doUpdateUserBalance(BalanceOwnerInterface $user, MoneyInterface $amount, array $extra = []): PaymentOrderInterface
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $extra = $resolver->resolve($extra);

        try {
            $instruction = new PaymentInstruction($amount->getSum(), $amount->getCurrency(), QiwiTerminalPlugin::NAME);
            $this->ppc->createPaymentInstruction($instruction);

            if (null !== $extra['task']) {
                $paymentOrder = $this->em->getRepository(PaymentOrder::class)
                    ->findOneBy([
                        'task' => $extra['task']
                    ]);
            }

            /* payment reverting */
            if (null !== $paymentOrder) {
                $this->em->remove($paymentOrder->getPaymentInstruction());
                $this->em->remove($paymentOrder);
                $user->addPayment(new Money(-$paymentOrder->getAmount()->getSum(), $user->getBalance()->getCurrency()));

                $this->em->flush();
            }

            $paymentOrder = (new PaymentOrder())
                ->setTask($extra['task'])
                ->setAmount($amount)
                ->setOwner($user)
                ->setTransaction($extra['txn_id'])
                ->setTransactionDate($extra['txn_date'])
                ->setPaymentInstruction($instruction);

            $this->em->beginTransaction();
            try {
                $this->em->persist($paymentOrder);
                $this->em->flush();

                $payment = $this->createPayment($paymentOrder);

                $user->addPayment($paymentOrder->getAmount());

                $result = $this->ppc->approveAndDeposit($payment->getId(), $amount->getSum());

                $this->ppc->closePaymentInstruction($result->getPaymentInstruction());

                $this->em->persist($user);
                $this->em->commit();
            } catch (Exception $e) {
                $this->em->rollback();
                throw $e;
            }

            $event = new BalanceEvent($user, $paymentOrder);
            $this->ed->dispatch(Events::BALANCE_UPDATED, $event);

            return $paymentOrder;
        } catch (BillingException $exception) {
            $this->logger->emergency('blabla billing');
            throw $exception;
        } catch (Exception $exception) {
            $this->logger->emergency('blabla billing');
            throw $exception;
        }
    }

    private function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefined([
                'txn_id',
                'txn_date'
            ])
            ->setDefaults([
                'task' => null,
                'txn_id' => null,
                'txn_date' => new DateTime()
            ])
            ->setAllowedTypes('task', ['null', Task::class])
            ->setAllowedTypes('txn_id', ['int', 'null'])
            ->setAllowedTypes('txn_date', [DateTime::class, 'string'])
            ->setNormalizer('txn_date', function (Options $options, $value) {
                if (!$value instanceof DateTime) {
                    return new DateTime($value);
                }
                return $value;
            });
    }

    private function createPayment(PaymentOrderInterface $paymentOrder)
    {
        $instruction = $paymentOrder->getPaymentInstruction();
        $pendingTransaction = $instruction->getPendingTransaction();

        if ($pendingTransaction !== null) {
            return $pendingTransaction->getPayment();
        }

        $amount = $instruction->getAmount() - $instruction->getDepositedAmount();

        return $this->ppc->createPayment($instruction->getId(), $amount);
    }
}
