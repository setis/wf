<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 1/11/17
 * Time: 12:34 AM
 */

namespace Gorserv\Gerp\AppBundle\Billing;


use Gorserv\Gerp\BillingBundle\Model\BalanceOwnerInterface;

interface BlockerHandlerInterface
{
    public function block(BalanceOwnerInterface $owner);
}
