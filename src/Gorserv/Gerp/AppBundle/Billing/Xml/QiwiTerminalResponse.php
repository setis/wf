<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 03.01.17
 * Time: 21:47
 */

namespace Gorserv\Gerp\AppBundle\Billing\Xml;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class CheckResponse
 * @package Gorserv\Gerp\AppBundle\Billing\Xml
 *
 * @Serializer\XmlRoot("response")
 */
class QiwiTerminalResponse
{
    private $osmp_txn_id;

    private $comment;

    private $result;

    private $prvn_txn;

    /**
     * @Serializer\Accessor(getter="getNormalizedSum")
     * @Serializer\Type("double")
     */
    private $sum;

    public function __construct($txn_id, $comment, $result, $prvn_txn = null, $sum = null)
    {
        $this->osmp_txn_id = (int)$txn_id;
        $this->comment = $comment;
        $this->result = (int)$result;

        $this->prvn_txn = $prvn_txn;
        $this->sum = (float)$sum;
    }

    public function getNormalizedSum()
    {
        return sprintf('%.2f', $this->sum);
    }
}
