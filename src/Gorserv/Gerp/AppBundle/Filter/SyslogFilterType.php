<?php

namespace Gorserv\Gerp\AppBundle\Filter;

use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use models\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of SyslogFilterType
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SyslogFilterType extends AbstractType
{
    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getBlockPrefix()
    {
        return 'syslog_filter';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userid', Filters\ChoiceFilterType::class, [
                'label' => 'Пользователь',
                'choices' => $this->createUsersOptions(),
                'attr' => [
                        'class' => 'select2'
                    ],
            ])

            ->add('actiondate', Filters\DateTimeRangeFilterType::class, [
                'label' => 'Дата',
                'left_datetime_options' => [
                    'label' => 'от',
                    'widget' => 'single_text',
                    'date_format' => 'yyyy-MM-dd HH:mm',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'attr' => [
                        'class' => 'datepicker'
                    ],
                    'view_timezone' => 'UTC',
                    'model_timezone' => 'UTC',
                ],
                'right_datetime_options' => [
                    'label' => 'до',
                    'widget' => 'single_text',
                    'date_format' => 'yyyy-MM-dd HH:mm',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'attr' => [
                        'class' => 'datepicker'
                    ],
                    'view_timezone' => 'UTC',
                    'model_timezone' => 'UTC',
                ],
            ])

            ->add('module', Filters\ChoiceFilterType::class, [
                'choices' => $this->createModulesOptions(),
                'label' => 'Модуль',
            ]);
    }

    private function createUsersOptions()
    {
        $users = $this->em->createQueryBuilder()
            ->select('u.id, u.fio')
            ->from(User::class, 'u')
            ->orderBy('u.fio')
            ->getQuery()
            ->getScalarResult();
        $options = [];

        foreach ($users as $row) {
            $options[$row['fio']] = $row['id'];
        }

        return $options;
    }

    private function createModulesOptions()
    {
        global $PLUGINS;

        $options["Вход в систему"] = "login";
        $options["Попытка входа в систему"] = "logintry";
        $options["Отправка почты"] = "mail";
        foreach ($PLUGINS as $key => $value) {
            if ($PLUGINS[$key]["name"] != "") {
                $options[str_replace(["&laquo;", "&raquo;"], "", $PLUGINS[$key]["name"])] = $key;
            }
        }

        return $options;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}
