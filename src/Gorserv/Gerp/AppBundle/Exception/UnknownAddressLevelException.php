<?php

namespace Gorserv\Gerp\AppBundle\Exception;

use RuntimeException;

/**
 * Class UnknownAddressLevelException
 *
 * @package Gorserv\Gerp\AppBundle\Exception
 * @author artem <mail@artemd.ru>
 */
class UnknownAddressLevelException extends RuntimeException
{

}
