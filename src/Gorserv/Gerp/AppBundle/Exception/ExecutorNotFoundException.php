<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12/13/16
 * Time: 5:18 PM
 */

namespace Gorserv\Gerp\AppBundle\Exception;


use DateTime;
use Gorserv\Gerp\ScheduleBundle\Exceptions\ScheduleException;
use models\Task;

class ExecutorNotFoundException extends ScheduleException
{
    /**
     * @var Task
     */
    private $task;

    /**
     * @var DateTime
     */
    private $timeStart;

    /**
     * @var DateTime
     */
    private $timeEnd;

    /**
     * ExecutorNotFoundException constructor.
     * @param Task $task
     * @param DateTime $timeStart
     * @param DateTime $timeEnd
     */
    public function __construct(Task $task, DateTime $timeStart, DateTime $timeEnd)
    {
        $this->task = $task;
        $this->timeStart = $timeStart;
        $this->timeEnd = $timeEnd;
    }

    /**
     * @return Task
     */
    public function getTask(): Task
    {
        return $this->task;
    }

    /**
     * @return DateTime
     */
    public function getTimeStart(): DateTime
    {
        return $this->timeStart;
    }

    /**
     * @return DateTime
     */
    public function getTimeEnd(): DateTime
    {
        return $this->timeEnd;
    }

}