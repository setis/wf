<?php

namespace Gorserv\Gerp\AddressBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/address/get-ticket-geo-data-by-point/53.001/37.001/point.json');

        $this->assertContains(200, $client->getResponse()->getStatusCode());
    }
}
