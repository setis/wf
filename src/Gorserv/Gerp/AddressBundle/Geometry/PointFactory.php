<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 03.11.16
 * Time: 19:51
 */

namespace Gorserv\Gerp\AddressBundle\Geometry;


use CrEOF\Spatial\PHP\Types\Geometry\Point;
use WF\Address\AddressInterface;

class PointFactory
{
    /**
     * @param AddressInterface $address
     * @return Point|null
     */
    public static function createByAddress(AddressInterface $address)
    {
        if (empty($address->getLatitude()) || empty($address->getLongitude())) {
            return null;
        }

        return new Point($address->getLongitude(), $address->getLatitude());
    }
}
