<?php

namespace Gorserv\Gerp\AddressBundle\Controller;

use CrEOF\Spatial\PHP\Types\Geometry\Point;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use models\ListArea;
use models\ListOds;
use models\ListSc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Rest\Prefix("/address")
 * @Rest\NamePrefix("address_")
 */
class ApiController extends FOSRestController
{
    /**
     * @Rest\Post("/get-ticket-geo-data-by-point")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getTicketGeoDataByPointAction(Request $request)
    {
        if(!$request->request->has('latitude') || !$request->request->has('longitude')) {
            throw new HttpException(500, 'Wrong coordinates!');
        }

        $lon = $request->get('longitude');
        $lat = $request->get('latitude');

        $point = new Point($lon, $lat);

        $sc = $this->getDoctrine()->getManager()->getRepository(ListSc::class)
            ->getServiceCentersByPoint($point);

        $ods = $this->getDoctrine()->getManager()->getRepository(ListOds::class)
            ->getOdsByPoint($point);

        $view = $this->view(['service_center' => $sc, 'ods' => $ods], 200);

        $context = new Context();
        $context->setGroups(['default']);
        $view->setContext($context)
            ->setFormat('json');

        return $this->handleView($view);
    }


    /**
     * @Rest\Post("/get-ticket-district")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getTicketAreaByNameAction(Request $request)
    {

        if(!$request->request->has('district'))
            throw new HttpException(500, 'Wrong district name!');

        $title = $request->get('district');

        $area = $this->getDoctrine()->getManager()->getRepository(ListArea::class)
            ->getAreaByTitle($title);

        $view = $this->view(['area' => $area], 200);

        $context = new Context();
        $context->setGroups(['default']);
        $view->setContext($context)
            ->setFormat('json');

        return $this->handleView($view);
    }


}
