<?php
namespace Gorserv\Gerp\BillingBundle\Model;

/**
 * Class MoneyInterface
 *
 * @package Gorserv\Gerp\BillingBundle\Model
 * @author artem <mail@artemd.ru>
 */
interface MoneyInterface
{
    public function getSum();

    public function getCurrency(): string;
}
