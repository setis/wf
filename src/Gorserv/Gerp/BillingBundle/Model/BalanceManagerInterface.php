<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 27.12.16
 * Time: 15:16
 */

namespace Gorserv\Gerp\BillingBundle\Model;

interface BalanceManagerInterface
{
    public function getUserBalance(BalanceOwnerInterface $user): MoneyInterface;

    public function updateUserBalance(BalanceOwnerInterface $user, MoneyInterface $amount, array $extra = []): PaymentOrderInterface;
}
