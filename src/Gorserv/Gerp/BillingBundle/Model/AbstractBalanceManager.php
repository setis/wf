<?php
namespace Gorserv\Gerp\BillingBundle\Model;

abstract class AbstractBalanceManager implements BalanceManagerInterface
{
    abstract public function getUserBalance(BalanceOwnerInterface $user): MoneyInterface;

    public function updateUserBalance(BalanceOwnerInterface $user, MoneyInterface $amount, array $extra = []): PaymentOrderInterface
    {
        return $this->doUpdateUserBalance($user, $amount, $extra);
    }

    abstract function doUpdateUserBalance(BalanceOwnerInterface $user, MoneyInterface $amount, array $extra = []): PaymentOrderInterface;
}
