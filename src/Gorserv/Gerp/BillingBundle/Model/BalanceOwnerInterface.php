<?php
namespace Gorserv\Gerp\BillingBundle\Model;

use Gorserv\Gerp\AppBundle\Entity\Money;

interface BalanceOwnerInterface
{
    /**
     * @return MoneyInterface
     */
    public function getBalance(): MoneyInterface;

    /**
     * @param Money $money
     * @return MoneyInterface
     */
    public function addPayment(Money $money): MoneyInterface;

}
