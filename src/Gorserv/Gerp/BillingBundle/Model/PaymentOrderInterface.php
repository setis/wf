<?php
namespace Gorserv\Gerp\BillingBundle\Model;

use JMS\Payment\CoreBundle\Model\PaymentInstructionInterface;

/**
 * PaymentOrder
 *
 * @ORM\Table(name="payment_order")
 * @ORM\Entity(repositoryClass="Gorserv\Gerp\AppBundle\Repository\PaymentOrderRepository")
 */
interface PaymentOrderInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return MoneyInterface
     */
    public function getAmount(): MoneyInterface;

    /**
     * @return BalanceOwnerInterface
     */
    public function getOwner(): BalanceOwnerInterface;

    /**
     * @return PaymentInstructionInterface
     */
    public function getPaymentInstruction(): PaymentInstructionInterface;
}
