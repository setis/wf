<?php
namespace GorservGerpBillingBundle;


use Codeception\Util\Stub;
use Gorserv\Gerp\AppBundle\Entity\Money;
use Gorserv\Gerp\AppBundle\Entity\PaymentOrder;
use Gorserv\Gerp\BillingBundle\Model\AbstractBalanceManager;
use Gorserv\Gerp\BillingBundle\Model\BalanceManagerInterface;
use Gorserv\Gerp\BillingBundle\Model\BalanceOwnerInterface;
use Gorserv\Gerp\BillingBundle\Model\PaymentOrderInterface;

class BalanceManagerTest extends \Codeception\Test\Unit
{
    /**
     * @var \GorservGerpBillingBundle\UnitTester
     */
    protected $tester;

    // tests
    public function testGetUserBalance()
    {
        /** @var BalanceManagerInterface $bm */
        $bm = Stub::makeEmpty(BalanceManagerInterface::class, [
            'getUserBalance' => Stub::once(function () {
                return new Money(0, 'RUB');
            })
        ]);
        $balanceOwner = Stub::makeEmpty(BalanceOwnerInterface::class);
        $balance = $bm->getUserBalance($balanceOwner);
        $this->assertInstanceOf(Money::class, $balance);
    }

    public function testUpdateUserBalance()
    {
        /** @var BalanceManagerInterface $bm */
        $bm = Stub::makeEmpty(AbstractBalanceManager::class, [
            'updateUserBalance' => Stub::once(function (BalanceOwnerInterface $bo, Money $money) {
                $po = (new PaymentOrder())
                    ->setOwner($bo)
                    ->setAmount(
                        new Money($money->getSum(), $money->getCurrency())
                    );
                return $po;
            }),
        ]);

        $balanceOwner = Stub::makeEmpty(BalanceOwnerInterface::class);

        $paymentOrder = $bm->updateUserBalance($balanceOwner, new Money(10, 'RUB'));
        $this->assertInstanceOf(PaymentOrderInterface::class, $paymentOrder);
        $this->assertEquals(10, $paymentOrder->getAmount()->getSum());
    }
}
