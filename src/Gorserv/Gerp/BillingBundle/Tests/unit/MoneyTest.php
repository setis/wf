<?php
namespace GorservGerpBillingBundle;


use Gorserv\Gerp\AppBundle\Entity\Money;

class MoneyTest extends \Codeception\Test\Unit
{
    /**
     * @var \GorservGerpBillingBundle\UnitTester
     */
    protected $tester;

    // tests
    public function testMoney()
    {
        $money = new Money(10.45, 'RUB');

        $this->assertEquals('10.45 RUB', (string)$money);
        $this->assertEquals(10.45, $money->getSum());
        $this->assertEquals('RUB', $money->getCurrency());
    }
}
