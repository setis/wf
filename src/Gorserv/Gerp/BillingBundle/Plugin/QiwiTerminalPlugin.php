<?php
namespace Gorserv\Gerp\BillingBundle\Plugin;


use JMS\Payment\CoreBundle\Model\FinancialTransactionInterface;
use JMS\Payment\CoreBundle\Plugin\AbstractPlugin;
use JMS\Payment\CoreBundle\Plugin\PluginInterface;

class QiwiTerminalPlugin extends AbstractPlugin
{

    const NAME = 'qiwi_terminal';

    /**
     * Whether this plugin can process payments for the given payment system.
     *
     * A plugin may support multiple payment systems. In these cases, the requested
     * payment system for a specific transaction  can be determined by looking at
     * the PaymentInstruction which will always be accessible either directly, or
     * indirectly.
     *
     * @param string $paymentSystemName
     *
     * @return bool
     */
    public function processes($paymentSystemName)
    {
        return static::NAME === $paymentSystemName;
    }

    /**
     * {@inheritdoc}
     */
    public function approveAndDeposit(FinancialTransactionInterface $transaction, $retry)
    {
        $authorizationId = $transaction->getPayment()->getApproveTransaction()->getReferenceNumber();
        $transaction->setReferenceNumber($authorizationId);

        $transaction->setProcessedAmount($transaction->getPayment()->getPaymentInstruction()->getApprovingAmount());
        $transaction->setReasonCode(PluginInterface::REASON_CODE_SUCCESS);
        $transaction->setResponseCode(PluginInterface::RESPONSE_CODE_SUCCESS);
    }
}
