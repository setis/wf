<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 04.01.17
 * Time: 13:57
 */

namespace Gorserv\Gerp\BillingBundle\EventDispatcher;


class Events
{
    const BALANCE_UPDATED = 'billing.balance.updated';
}
