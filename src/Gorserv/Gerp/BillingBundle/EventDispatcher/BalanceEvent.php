<?php
namespace Gorserv\Gerp\BillingBundle\EventDispatcher;

use Gorserv\Gerp\BillingBundle\Model\BalanceOwnerInterface;
use Gorserv\Gerp\BillingBundle\Model\PaymentOrderInterface;
use Symfony\Component\EventDispatcher\Event;


class BalanceEvent extends Event
{
    /**
     * @var BalanceOwnerInterface
     */
    private $balanceOwner;

    /**
     * @var PaymentOrderInterface
     */
    private $order;

    /**
     * BalanceEvent constructor.
     * @param BalanceOwnerInterface $balanceOwner
     * @param PaymentOrderInterface $order
     */
    public function __construct(BalanceOwnerInterface $balanceOwner, PaymentOrderInterface $order)
    {
        $this->order = $order;
        $this->balanceOwner = $balanceOwner;
    }

    /**
     * @return BalanceOwnerInterface
     */
    public function getBalanceOwner(): BalanceOwnerInterface
    {
        return $this->balanceOwner;
    }

    /**
     * @return PaymentOrderInterface
     */
    public function getOrder(): PaymentOrderInterface
    {
        return $this->order;
    }
}
