<?php

namespace Gorserv\Gerp\ScheduleBundle;

use DateTime;
use Gorserv\Gerp\ScheduleBundle\EventDispatcher\Events;
use Gorserv\Gerp\ScheduleBundle\EventDispatcher\SlotDeletedEvent;
use Gorserv\Gerp\ScheduleBundle\EventDispatcher\SlotEvent;
use Gorserv\Gerp\ScheduleBundle\Exceptions\SlotException;
use Gorserv\Gerp\ScheduleBundle\Exceptions\SlotUnavailableException;
use Gorserv\Gerp\ScheduleBundle\Exceptions\TimeframeNotFoundException;
use Gorserv\Gerp\ScheduleBundle\Model\ExecutorChooserInterface;
use Gorserv\Gerp\ScheduleBundle\Model\JobInterface;
use Gorserv\Gerp\ScheduleBundle\Model\ScheduleManagerInterface;
use Gorserv\Gerp\ScheduleBundle\Model\ScheduleOperatorInterface;
use Gorserv\Gerp\ScheduleBundle\Model\SlotInterface;
use Gorserv\Gerp\ScheduleBundle\Model\SlotManagerInterface;
use Gorserv\Gerp\ScheduleBundle\Model\TimeframeManagerInterface;
use InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Description of ScheduleManager
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ScheduleManager implements ScheduleManagerInterface
{

    /**
     *
     * @var TimeframeManagerInterface
     */
    private $timeframeManager;

    /**
     *
     * @var SlotManagerInterface
     */
    private $slotManager;

    /**
     *
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(TimeframeManagerInterface $timeframeManager, SlotManagerInterface $slotManager)
    {
        $this->timeframeManager = $timeframeManager;
        $this->slotManager = $slotManager;
    }

    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param JobInterface $job
     * @param DateTime $start
     * @param DateTime $end
     * @param UserInterface[] $executors
     * @param ScheduleOperatorInterface|null $operator
     * @return SlotInterface
     * @throws SlotException
     * @throws SlotUnavailableException
     * @throws TimeframeNotFoundException
     */
    public function createSlot(JobInterface $job, DateTime $start, DateTime $end, array $executors, ScheduleOperatorInterface $operator = null): SlotInterface
    {
        if (null !== $job->getSlot()) {
            throw new SlotException($job->getSlot(), 'Job already in slot');
        }

        if (empty($executors)) {
            throw new InvalidArgumentException('Executors can not be empty');
        }

        foreach ($executors as $executor) {
            $timeframe = $this->timeframeManager->findTimeframe($executor, $start);
            if (null === $timeframe) {
                throw new TimeframeNotFoundException($executor, $start);
            }

            if (!$this->timeframeManager->isSlotAvailable($timeframe, $start, $end)) {
                throw new SlotUnavailableException($timeframe, $start, $end);
            }
        }

        // Builder creates concrete implementation of SlotInterface using concrete Job (Task) and Operator (User)
        $slot = $this->slotManager->build($job, $start, $end, $executors, $operator);

        $this->dispatchEvent(Events::SLOT_CREATED, function() use ($slot) {
            return new SlotEvent($slot);
        });

        return $slot;
    }

    /**
     * @inheritdoc
     */
    public function findAvailableTimeframes(JobInterface $job, DateTime $start, DateTime $end): array
    {
        return $this->timeframeManager->findAvailableTimeframes($job, $start, $end);
    }

    public function findAvailableExecutors(JobInterface $job, DateTime $start, DateTime $end, ExecutorChooserInterface $chooser = null): array
    {
        $executors = $this->timeframeManager->findAvailableExecutors($job, $start, $end);
        if (null !== $chooser) {
            $executors = $chooser->choose($executors, $job, $start, $end);
        }

        return $executors;
    }

    public function deleteSlot(SlotInterface $slot, ScheduleOperatorInterface $operator = null, array $params = [])
    {
        $this->slotManager->delete($slot);

        $this->dispatchEvent(Events::SLOT_DELETED, function () use ($slot, $operator, $params) {
            return new SlotDeletedEvent($slot, $operator, $params);
        });
    }

    private function dispatchEvent($name, $eventCreator)
    {
        if (null === $this->eventDispatcher) {
            return;
        }

        $this->eventDispatcher->dispatch($name, $eventCreator());
    }

}
