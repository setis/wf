<?php

namespace Gorserv\Gerp\ScheduleBundle\EventDispatcher;

/**
 * Description of Events
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class Events
{
    const SLOT_CREATED = 'schedule.slot.created';
    const SLOT_DELETED = 'schedule.slot.deleted';
}
