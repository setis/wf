<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 26.12.16
 * Time: 11:44
 */

namespace Gorserv\Gerp\ScheduleBundle\EventDispatcher;


use Gorserv\Gerp\ScheduleBundle\Model\ScheduleOperatorInterface;
use Gorserv\Gerp\ScheduleBundle\Model\SlotInterface;

class SlotDeletedEvent extends SlotEvent
{
    /**
     * @var ScheduleOperatorInterface
     */
    private $deletedBy;

    private $params;

    /**
     * SlotDeletedEvent constructor.
     * @param SlotInterface $slot
     * @param ScheduleOperatorInterface|null $deletedBy
     * @param array $params
     */
    public function __construct(SlotInterface $slot, ScheduleOperatorInterface $deletedBy = null, array $params = [])
    {
        parent::__construct($slot);
        $this->deletedBy = $deletedBy;
        $this->params = $params;
    }

    /**
     * @return ScheduleOperatorInterface
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }


    public function getParams()
    {
        return $this->params;
    }
}