<?php

namespace Gorserv\Gerp\ScheduleBundle\EventDispatcher;

use Gorserv\Gerp\ScheduleBundle\Model\SlotInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Description of SlotCreatedEvent
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SlotEvent extends Event
{
    /**
     *
     * @var SlotInterface
     */
    private $slot;

    public function __construct(SlotInterface $slot)
    {
        $this->slot = $slot;
    }

    public function getSlot(): SlotInterface
    {
        return $this->slot;
    }

}
