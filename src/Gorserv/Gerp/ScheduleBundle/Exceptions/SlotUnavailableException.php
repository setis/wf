<?php

namespace Gorserv\Gerp\ScheduleBundle\Exceptions;

use DateTime;
use Gorserv\Gerp\ScheduleBundle\Model\TimeframeInterface;

/**
 * Description of SlotUnavailableException
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SlotUnavailableException extends ScheduleException
{
    /**
     *
     * @var ExecutorTimeframeInterface
     */
    private $timeframe;

    /**
     *
     * @var DateTime
     */
    private $timeStart;

    /**
     *
     * @var DateTime
     */
    private $timeEnd;

    public function __construct(TimeframeInterface $timeframe, DateTime $timeStart, DateTime $timeEnd)
    {
        parent::__construct('Time is unavailable in timeframe');

        $this->timeframe = $timeframe;
        $this->timeStart = $timeStart;
        $this->timeEnd = $timeEnd;
    }

    public function getTimeframe(): TimeframeInterface
    {
        return $this->timeframe;
    }

    public function getTimeStart(): DateTime
    {
        return $this->timeStart;
    }

    public function getTimeEnd(): DateTime
    {
        return $this->timeEnd;
    }

}
