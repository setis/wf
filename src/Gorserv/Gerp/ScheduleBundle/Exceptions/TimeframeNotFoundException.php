<?php

namespace Gorserv\Gerp\ScheduleBundle\Exceptions;

use DateTime;
use Gorserv\Gerp\ScheduleBundle\Model\ExecutorInterface;

/**
 * Description of TimeframeNotFoundException
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TimeframeNotFoundException extends ScheduleException
{
    /**
     *
     * @var ExecutorInterface
     */
    private $executor;
    
    /**
     *
     * @var DateTime
     */
    private $time;
    
    public function __construct(ExecutorInterface $executor, DateTime $time)
    {
        parent::__construct('Timeframe not found');

        $this->executor = $executor;
        $this->time = $time;
    }

    public function getExecutor(): ExecutorInterface
    {
        return $this->executor;
    }

    public function getTime(): DateTime
    {
        return $this->time;
    }


}
