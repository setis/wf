<?php

namespace Gorserv\Gerp\ScheduleBundle\Exceptions;

use Exception;

/**
 * Description of ScheduleException
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ScheduleException extends Exception
{

}
