<?php

namespace Gorserv\Gerp\ScheduleBundle\Exceptions;

use Gorserv\Gerp\ScheduleBundle\Model\SlotInterface;

/**
 * Description of SlotException
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SlotException extends ScheduleException
{
    /**
     *
     * @var SlotInterface
     */
    private $slot;
    
    public function __construct(SlotInterface $slot, string $message = "")
    {
    parent::__construct($message);
        $this->slot = $slot;
    }
    
    public function getSlot(): SlotInterface
    {
        return $this->slot;
    }

}
