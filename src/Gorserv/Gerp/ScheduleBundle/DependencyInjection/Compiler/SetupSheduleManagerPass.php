<?php

namespace Gorserv\Gerp\ScheduleBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Description of SetupSheduleManagerPass
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SetupSheduleManagerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        $config = $container->getParameter('gorserv_gerp_schedule.config');
        $container->getParameterBag()->remove('gorserv_gerp_schedule.config');

        $manager = $container->getDefinition('schedule_manager');
        $timeframeManager = new Reference($config['timeframe_manager']);
        $slotManager = new Reference($config['slot_manager']);

        $manager->setArguments([$timeframeManager, $slotManager]);

        if ($container->has($config['event_dispatcher'])) {
            $dispatcher = new Reference($config['event_dispatcher']);
            $manager->addMethodCall('setEventDispatcher', [$dispatcher]);
        }

    }

}
