<?php

namespace Gorserv\Gerp\ScheduleBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('gorserv_gerp_schedule');

        $rootNode->children()
            ->scalarNode('slot_manager')
                ->isRequired()
                ->cannotBeEmpty()
            ->end()
            ->scalarNode('timeframe_manager')
                ->isRequired()
                ->cannotBeEmpty()
            ->end()
            ->scalarNode('event_dispatcher')
                ->defaultValue('event_dispatcher')
            ->end()
        ->end();

        return $treeBuilder;
    }
}
