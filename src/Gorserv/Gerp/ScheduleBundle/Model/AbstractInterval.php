<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 10.12.16
 * Time: 2:33
 */

namespace Gorserv\Gerp\ScheduleBundle\Model;


abstract class AbstractInterval implements IntervalInterface
{
    public function equals(IntervalInterface $interval) : bool
    {
        return $this->getTimeStart() == $interval->getTimeStart()
            && $this->getTimeEnd() == $interval->getTimeEnd();
    }

    public function contains(IntervalInterface $interval) : bool
    {
        return $this->getTimeStart() <= $interval->getTimeStart()
            && $this->getTimeEnd() >= $interval->getTimeEnd();
    }

    public function intersect(IntervalInterface $interval): bool
    {
        return $this->getTimeStart() <= $interval->getTimeEnd()
            && $this->getTimeEnd() >= $interval->getTimeStart();
    }
}