<?php

namespace Gorserv\Gerp\ScheduleBundle\Model;

use DateTime;

/**
 * Description of AbstractTimeframeManager
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
abstract class AbstractTimeframeManager implements TimeframeManagerInterface
{

    /**
     * @inheritdoc
     */
    public function deleteTimeframe(ExecutorInterface $executor, DateTime $time)
    {
        return $this->doDeleteTimeframe($executor, $time);
    }

    /**
     * @inheritdoc
     */
    public function findTimeframe(ExecutorInterface $executor, DateTime $time)
    {
        return $this->doFindTimeframe($executor, $time);
    }

    /**
     * @inheritdoc
     */
    public function findAvailableExecutors(JobInterface $job, DateTime $start, DateTime $end) : array
    {
        return $this->doFindAvailableExecutors($job, $start, $end);
    }

    /**
     * @inheritdoc
     */
    public function isSlotAvailable(TimeframeInterface $timeframe, DateTime $timeStart, DateTime $timeEnd)
    {
        $this->validateTimes($timeStart, $timeEnd);
        return $this->doIsSlotAvailable($timeframe, $timeStart, $timeEnd);
    }

    /**
     * @inheritdoc
     */
    public function setupTimeframe(ExecutorInterface $executor, DateTime $timeStart, DateTime $timeEnd): TimeframeInterface
    {
        $this->validateTimes($timeStart, $timeEnd);
        return $this->doSetupTimeframe($executor, $timeStart, $timeEnd);
    }

    /**
     * @inheritdoc
     */
    public function findAvailableTimeframes(JobInterface $job, DateTime $start, DateTime $end): array
    {
        $this->validateTimes($start, $end);
        return $this->doFindAvailableTimeframes($job, $start, $end);
    }

    abstract protected function doSetupTimeframe($executor, DateTime $timeStart, DateTime $timeEnd);

    abstract protected function doIsSlotAvailable($timeframe, DateTime $timeStart, DateTime $timeEnd) : bool;

    abstract protected function doDeleteTimeframe($executor, DateTime $time);

    abstract protected function doFindTimeframe($executor, DateTime $time);

    abstract protected function doFindAvailableExecutors($job, DateTime $start, DateTime $end);

    abstract protected function doFindAvailableTimeframes($job, DateTime $start, DateTime $end): array;

    protected function validateTimes(DateTime $timeStart, DateTime $timeEnd)
    {
        if ($timeStart >= $timeEnd) {
            throw new \InvalidArgumentException(sprintf('timeStart (%s) cannot be later than timeEnd (%s)',
                $timeStart->format(\DateTime::ISO8601),
                $timeEnd->format(\DateTime::ISO8601)
                ));
        }
    }

    protected function isAcceptableToPeriod(TimeframeInterface $timeframe, DateTime $startSlot, DateTime $endSlot)
    {
        $interval = $timeframe->getInterval();
        if (null === $interval) {
            return true;
        }
        $intervalMinutes = $interval->h * 60 + $interval->i;

        $startInt = $timeframe->getTimeStart()->diff($startSlot);
        $startMinutes = $startInt->h * 60 + $startInt->i;

        if (0 !== $startMinutes % $intervalMinutes) {
            return false;
        }

        $endInt = $timeframe->getTimeStart()->diff($endSlot);
        $endMinutes = $endInt->h * 60 + $endInt->i;

        return 0 === $endMinutes % $intervalMinutes;
    }

}
