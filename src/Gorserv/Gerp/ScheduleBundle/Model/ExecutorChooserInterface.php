<?php

namespace Gorserv\Gerp\ScheduleBundle\Model;

use DateTime;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface ExecutorChooserInterface
{
    public function choose(array $executors, JobInterface $job, DateTime $timeStart, DateTime $timeEnd) : array;
}
