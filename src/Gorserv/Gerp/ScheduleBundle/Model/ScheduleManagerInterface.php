<?php

namespace Gorserv\Gerp\ScheduleBundle\Model;

use DateTime;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface ScheduleManagerInterface
{
    /**
     *
     * @param JobInterface $job
     * @param DateTime $start
     * @param DateTime $end
     * @param array $executors
     * @param ScheduleOperatorInterface $operator
     * @return SlotInterface
     */
    public function createSlot(JobInterface $job, DateTime $start, DateTime $end, array $executors, ScheduleOperatorInterface $operator = null) : SlotInterface;

    public function deleteSlot(SlotInterface $slot, ScheduleOperatorInterface $operator = null, array $params = []);

    /**
     *
     * @param JobInterface $job
     * @param DateTime $start
     * @param DateTime $end
     * @return ExecutorInterface[]
     */
    public function findAvailableExecutors(JobInterface $job, DateTime $start, DateTime $end, ExecutorChooserInterface $chooser = null): array;

    /**
     *
     * @param JobInterface $job
     * @param DateTime $start
     * @param DateTime $end
     * @return TimeframeInterface[]
     * @todo Think about arguments
     */
    public function findAvailableTimeframes(JobInterface $job, DateTime $start, DateTime $end) : array;
}
