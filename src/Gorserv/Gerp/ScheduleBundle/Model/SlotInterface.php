<?php

namespace Gorserv\Gerp\ScheduleBundle\Model;

use DateTime;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface SlotInterface
{
    /**
     * @return ExecutorInterface[]
     */
    public function getExecutors() : array;
    
    /**
     * @return DateTime
     */
    public function getTimeStart() : DateTime;
    
    /**
     * @return DateTime
     */
    public function getTimeEnd() : DateTime;
    
    /**
     * @return JobInterface
     */
    public function getJob() : JobInterface;
    
    /**
     * @return ScheduleOperatorInterface
     */
    public function getCreatedBy();

    public function getInterval() : IntervalInterface;
}
