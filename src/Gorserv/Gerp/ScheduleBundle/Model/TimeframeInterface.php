<?php

namespace Gorserv\Gerp\ScheduleBundle\Model;

use DateInterval;
use DateTime;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TimeframeInterface
{
    /**
     * @return ExecutorInterface
     */
    public function getExecutor() : ExecutorInterface;

    /**
     * @return DateTime
     */
    public function getTimeStart() : DateTime;

    /**
     * @return DateTime
     */
    public function getTimeEnd() : DateTime;
    
    /**
     * @return DateInterval Returns interval if timeframe has period
     */
    public function getInterval();

    /**
     * @return SlotInterface[]
     */
    public function getSlots();
}
