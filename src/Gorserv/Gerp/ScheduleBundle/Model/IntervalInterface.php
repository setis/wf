<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 10.12.16
 * Time: 2:30
 */

namespace Gorserv\Gerp\ScheduleBundle\Model;

use DateTime;


interface IntervalInterface
{
    /**
     * @return DateTime
     */
    public function getTimeStart() : DateTime;

    /**
     * @return DateTime
     */
    public function getTimeEnd() : DateTime;

    /**
     * @param IntervalInterface $interval
     * @return bool
     */
    public function equals(IntervalInterface $interval) : bool;

    /**
     * @param IntervalInterface $interval
     * @return bool
     */
    public function contains(IntervalInterface $interval) : bool;

    /**
     * @param IntervalInterface $interval
     * @return bool
     */
    public function intersect(IntervalInterface $interval) : bool;
}