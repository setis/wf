<?php

namespace Gorserv\Gerp\ScheduleBundle\Model;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface JobInterface
{
    public function getSlot();
}
