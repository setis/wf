<?php

namespace Gorserv\Gerp\ScheduleBundle\Model;

use DateTime;

/**
 * Description of ExecutorFinderInterface
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface ExecutorFinderInterface
{
    /**
     *
     * @param JobInterface $job
     * @param DateTime $start
     * @param DateTime $end
     *
     * @return ExecutorInterface[]
     */
    public function findAvailableExecutors(JobInterface $job, DateTime $start, DateTime $end);
}
