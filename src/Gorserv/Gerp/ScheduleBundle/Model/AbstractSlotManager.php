<?php

namespace Gorserv\Gerp\ScheduleBundle\Model;

use DateTime;
use InvalidArgumentException;

/**
 * Description of AbstractSlotBuilder
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
abstract class AbstractSlotManager implements SlotManagerInterface
{

    public function build(JobInterface $job, DateTime $start, DateTime $end, array $executors, ScheduleOperatorInterface $operator = null) : SlotInterface
    {
        if ($start >= $end) {
            throw new InvalidArgumentException(sprintf('timeEnd (%s) must be greater then timeStart (%s)',
                $start->format(\DateTime::ISO8601),
                $end->format(\DateTime::ISO8601)
                ));
        }

        return $this->doBuild($job, $start, $end, $executors, $operator);
    }

    public function delete(SlotInterface $slot)
    {
        $this->doDelete($slot);
    }

    abstract protected function doBuild($job, DateTime $start, DateTime $end, array $executors, $operator = null);

    abstract protected function doDelete($slot);
}
