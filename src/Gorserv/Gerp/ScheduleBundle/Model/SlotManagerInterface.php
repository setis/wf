<?php

namespace Gorserv\Gerp\ScheduleBundle\Model;

use DateTime;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface SlotManagerInterface
{
    public function build(JobInterface $job, DateTime $start, DateTime $end, array $executors, ScheduleOperatorInterface $operator = null) : SlotInterface;

    public function delete(SlotInterface $slot);
}
