<?php

namespace Gorserv\Gerp\ScheduleBundle\Model;

use DateTime;

/**
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TimeframeManagerInterface
{

    /**
     *
     * @param ExecutorInterface $executor
     * @param DateTime $timeStart
     * @param DateTime $timeEnd
     * @return TimeframeInterface
     */
    public function setupTimeframe(ExecutorInterface $executor, DateTime $timeStart, DateTime $timeEnd): TimeframeInterface;

    /**
     *
     * @param ExecutorInterface $executor
     * @param DateTime $time
     * @return void
     */
    public function deleteTimeframe(ExecutorInterface $executor, DateTime $time);

    /**
     *
     * @param JobInterface $job
     * @param DateTime $start
     * @param DateTime $end
     * @return ExecutorInterface[]
     */
    public function findAvailableExecutors(JobInterface $job, DateTime $start, DateTime $end) : array;

    /**
     * @param ExecutorInterface $executor
     * @param DateTime $time
     * @return TimeframeInterface|null
     */
    public function findTimeframe(ExecutorInterface $executor, DateTime $time);

    /**
     * @param TimeframeInterface $timeframe
     * @param DateTime $timeStart
     * @param DateTime $timeEnd
     * @return boolean
     */
    public function isSlotAvailable(TimeframeInterface $timeframe, DateTime $timeStart, DateTime $timeEnd);

    /**
     *
     * @param JobInterface $job
     * @param DateTime $start
     * @param DateTime $end
     * @return TimeframeInterface[]
     */
    public function findAvailableTimeframes(JobInterface $job, DateTime $start, DateTime $end) : array;
}
