<?php

namespace Gorserv\Gerp\ScheduleBundle\Tests\Mock;

use DateTime;
use Gorserv\Gerp\ScheduleBundle\Model\JobInterface;
use Gorserv\Gerp\ScheduleBundle\Model\ScheduleOperatorInterface;
use Gorserv\Gerp\ScheduleBundle\Model\SlotInterface;

/**
 * Description of Slot
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class Slot implements SlotInterface
{
    /**
     *
     * @var JobInterface 
     */
    private $job;
    
    /**
     *
     * @var DateTime 
     */
    private $timeStart;
    
    /**
     *
     * @var DateTime 
     */
    private $timeEnd;
    
    /**
     *
     * @var array
     */
    private $executors;
    
    /**
     *
     * @var ScheduleOperatorInterface
     */
    private $createdBy;
    
    public function __construct(JobInterface $job, DateTime $timeStart, DateTime $timeEnd, array $executors, ScheduleOperatorInterface $createdBy = null)
    {
        $this->job = $job;
        $this->timeStart = $timeStart;
        $this->timeEnd = $timeEnd;
        $this->executors = $executors;
        $this->createdBy = $createdBy;
    }

        public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getExecutors(): array
    {
        return $this->executors;
    }

    public function getJob(): JobInterface
    {
        return $this->job;
    }

    public function getTimeEnd(): DateTime
    {
        return $this->timeEnd;
    }

    public function getTimeStart(): DateTime
    {
        return $this->timeStart;
    }

}
