<?php

namespace Gorserv\Gerp\ScheduleBundle\Tests\Mock;

use DateTime;
use Gorserv\Gerp\ScheduleBundle\Model\AbstractSlotManager;

/**
 * Description of SlotBuilder
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SlotManager extends AbstractSlotManager
{
    protected function doBuild($job, DateTime $start, DateTime $end, array $executors, $operator = null)
    {
        return new Slot($job, $end, $start, $executors, $operator);
    }

    protected function doDelete($slot)
    {

    }

}
