<?php

namespace Gorserv\Gerp\ScheduleBundle\Tests;

use DateTime;
use Gorserv\Gerp\ScheduleBundle\EventDispatcher\Events;
use Gorserv\Gerp\ScheduleBundle\EventDispatcher\SlotEvent;
use Gorserv\Gerp\ScheduleBundle\Model\ExecutorInterface;
use Gorserv\Gerp\ScheduleBundle\Model\JobInterface;
use Gorserv\Gerp\ScheduleBundle\Model\SlotManagerInterface;
use Gorserv\Gerp\ScheduleBundle\Model\SlotInterface;
use Gorserv\Gerp\ScheduleBundle\Model\TimeframeInterface;
use Gorserv\Gerp\ScheduleBundle\Model\TimeframeManagerInterface;
use Gorserv\Gerp\ScheduleBundle\ScheduleManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Description of ScheduleManagerTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ScheduleManagerTest extends TestCase
{
    /**
     * @expectedException Gorserv\Gerp\ScheduleBundle\Exceptions\SlotException
     */
    public function testCreateSlotForJobWithSlot()
    {
        $job = $this->getMockForAbstractClass(JobInterface::class);
        $job->method('getSlot')
             ->willReturn($this->getMockForAbstractClass(SlotInterface::class));

        $this->createScheduleManager()->createSlot($job, new DateTime(), new DateTime(), []);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCreateSlotWithEmptyExecutors()
    {
        $job = $this->getMockForAbstractClass(JobInterface::class);

        $this->createScheduleManager()->createSlot($job, new DateTime(), new DateTime(), []);
    }

    /**
     * @expectedException Gorserv\Gerp\ScheduleBundle\Exceptions\TimeframeNotFoundException
     */
    public function testCreateSlotForExecutorWithoutTimeframe()
    {
        $job = $this->getMockForAbstractClass(JobInterface::class);

        $executor = $this->getMockForAbstractClass(ExecutorInterface::class);

        $this->createScheduleManager()->createSlot($job, new DateTime(), new DateTime(), [$executor]);
    }

    /**
     * @expectedException Gorserv\Gerp\ScheduleBundle\Exceptions\SlotUnavailableException
     */
    public function testCreateSlotForTimeframerWithoutAvailableSlots()
    {
        $job = $this->getMockForAbstractClass(JobInterface::class);

        $executor = $this->getMockForAbstractClass(ExecutorInterface::class);

        $timeframe = $this->getMockForAbstractClass(TimeframeInterface::class);

        $timeframeManager = $this->createTimeframeManager();
        $timeframeManager->method('findTimeframe')->willReturn($timeframe);
        $timeframeManager->method('isSlotAvailable')->willReturn(false);

        $this->createScheduleManager($timeframeManager)->createSlot($job, new DateTime(), new DateTime(), [$executor]);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCreateSlotWithWrongStartEnd()
    {
        $job = $this->getMockForAbstractClass(JobInterface::class);

        $executor = $this->getMockForAbstractClass(ExecutorInterface::class);

        $timeframe = $this->getMockForAbstractClass(TimeframeInterface::class);

        $timeframeManager = $this->createTimeframeManager();
        $timeframeManager->method('findTimeframe')->willReturn($timeframe);
        $timeframeManager->method('isSlotAvailable')->willReturn(true);

        $slotBuilder = new Mock\SlotManager();
        $manager = $this->createScheduleManager($timeframeManager, $slotBuilder);

        $slot = $manager->createSlot($job, new DateTime(), new DateTime(), [$executor]);
        $this->assertInstanceOf(SlotInterface::class, $slot);
    }

    public function testCreateSlotSuccess()
    {
        $job = $this->getMockForAbstractClass(JobInterface::class);

        $executor = $this->getMockForAbstractClass(ExecutorInterface::class);

        $timeframe = $this->getMockForAbstractClass(TimeframeInterface::class);

        $timeframeManager = $this->createTimeframeManager();
        $timeframeManager->method('findTimeframe')->willReturn($timeframe);
        $timeframeManager->method('isSlotAvailable')->willReturn(true);

        $slotBuilder = new Mock\SlotManager();

        $dispatcher = $this->getMockForAbstractClass(EventDispatcherInterface::class);
        $dispatcher->expects($this->once())
            ->method('dispatch')
            ->with(
                $this->equalTo(Events::SLOT_CREATED),
                $this->isInstanceOf(SlotEvent::class)
            );

        $manager = $this->createScheduleManager($timeframeManager, $slotBuilder);
        $manager->setEventDispatcher($dispatcher);

        $slot = $manager->createSlot($job, new DateTime(), new DateTime('+1 hour'), [$executor]);
        $this->assertInstanceOf(SlotInterface::class, $slot);
    }

    public function testDeleteSlot()
    {
        $slot = $this->getMockForAbstractClass(SlotInterface::class);
        $manager = $this->createScheduleManager();

        $dispatcher = $this->getMockForAbstractClass(EventDispatcherInterface::class);
        $dispatcher->expects($this->once())
            ->method('dispatch')
            ->with(
                $this->equalTo(Events::SLOT_DELETED),
                $this->isInstanceOf(SlotEvent::class)
            );
        $manager->setEventDispatcher($dispatcher);

        $manager->deleteSlot($slot);
    }

    private function createScheduleManager($timeframeManager = null, $slotBuilder = null)
    {
        if (null === $timeframeManager) {
            $timeframeManager = $this->createTimeframeManager();
        }

        if (null === $slotBuilder) {
            $slotBuilder = $this->getMockForAbstractClass(SlotManagerInterface::class);
        }

        return new ScheduleManager($timeframeManager, $slotBuilder);
    }

    private function createTimeframeManager()
    {
        return $this->getMockForAbstractClass(TimeframeManagerInterface::class);
    }
}
