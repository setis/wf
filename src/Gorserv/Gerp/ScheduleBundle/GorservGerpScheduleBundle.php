<?php

namespace Gorserv\Gerp\ScheduleBundle;

use Gorserv\Gerp\ScheduleBundle\DependencyInjection\Compiler\SetupSheduleManagerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GorservGerpScheduleBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new SetupSheduleManagerPass());
    }
}
