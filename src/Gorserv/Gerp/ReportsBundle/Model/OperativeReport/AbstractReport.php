<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 2/9/17
 * Time: 9:25 PM
 */

namespace Gorserv\Gerp\ReportsBundle\Model\OperativeReport;


abstract class AbstractReport
{

    /**
     * @var AbstractReport[]
     */
    private $items = [];

    /**
     * @var string
     */
    private $label;

    /**
     * @param AbstractReport $item
     * @return self
     */
    public function addItem($key, AbstractReport $item)
    {
        if (isset($this->items[$key])) {
            throw new \LogicException("{$key} already defined in " . self::class . " items!");
        }

        $this->items[$key] = $item;
        return $this;
    }

    /**
     * @return AbstractReport[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param $key
     * @return AbstractReport|null
     */
    public function getItem($key)
    {
        if (!isset($this->items[$key])) {
            return null;
        }
        return $this->items[$key];
    }

    public function getDoneCount()
    {
        $result = 0;
        foreach ($this->getItems() as $item) {
            $result += $item->getDoneCount();
        }

        return $result;
    }

    public function getClosedCount()
    {
        $result = 0;
        foreach ($this->getItems() as $item) {
            $result += $item->getClosedCount();
        }

        return $result;
    }

    public function getYield()
    {
        $result = 0;
        foreach ($this->getItems() as $item) {
            $result += $item->getYield();
        }

        return $result;
    }

    public function getAverageBill()
    {
        if (0 != $this->getDoneCount()) {
            return $this->getYield() / $this->getDoneCount();
        }

        return 0;
    }

    public function getCharges()
    {
        $result = 0;
        foreach ($this->getItems() as $item) {
            $result += $item->getCharges();
        }

        return $result;
    }

    public function getProfit()
    {
        return $this->getYield() - $this->getCharges();
    }

    public function getGrossProfit()
    {
        if (0 != floatval($this->getYield())) {
            return round(100 * $this->getProfit() / $this->getYield());
        }

        return 0;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return self
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

}
