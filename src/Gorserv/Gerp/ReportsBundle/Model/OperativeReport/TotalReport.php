<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 2/10/17
 * Time: 1:38 PM
 */

namespace Gorserv\Gerp\ReportsBundle\Model\OperativeReport;

use DateTime;

/**
 * Class TotalReport
 *
 * @package Gorserv\Gerp\ReportsBundle\Model\OperativeReport
 * @author ${USER} <mail@artemd.ru>
 *
 * @method TotalColumnReport[] getItems()
 */
class TotalReport extends AbstractReport
{
    /**
     * @var DateTime
     */
    private $start;

    /**
     * @var DateTime
     */
    private $end;

    /**
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param DateTime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param DateTime $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

}
