<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 2/9/17
 * Time: 9:29 PM
 */

namespace Gorserv\Gerp\ReportsBundle\Model\OperativeReport;

/**
 * Class WorkTypeReport
 *
 * @package Gorserv\Gerp\ReportsBundle\Model\OperativeReport
 * @author ${USER} <mail@artemd.ru>
 * @method PartnerReport[] getItems()
 */
class WorkTypeReport extends AbstractReport
{
}
