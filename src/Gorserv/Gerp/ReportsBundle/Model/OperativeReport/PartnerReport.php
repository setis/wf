<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 2/9/17
 * Time: 9:28 PM
 */

namespace Gorserv\Gerp\ReportsBundle\Model\OperativeReport;

class PartnerReport extends AbstractReport
{

    /**
     * @var array
     */
    private $data = [];

    /**
     * PartnerReport constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->setLabel($data['partner_title']);
    }

    public function getDoneCount()
    {
        return (int)$this->data['doneCount'];
    }

    public function getClosedCount()
    {
        return $this->data['skpCashlessCount'] + $this->data['skpCashCount'] + $this->data['connectionsCount'] + $this->data['accidentsCount'] + $this->data['gpCount'];
    }

    public function getYield()
    {
        return $this->data['gp_yield'] + $this->data['skp_yield'] + $this->data['accident_yield'] + $this->data['connection_yield'];
    }

    public function getAverageBill()
    {
        if (0 != $this->getDoneCount()) {
            return $this->getYield() / $this->getDoneCount();
        }
        return 0;
    }

    public function getCharges()
    {
        return $this->data['gp_charges'] + $this->data['skp_charges'] + $this->data['accident_charges'] + $this->data['connection_charges'];
    }

    public function getProfit()
    {
        return $this->getYield() - $this->getCharges();
    }

    public function getGrossProfit()
    {
        if (0 != $this->getYield()) {
            return round(100 * $this->getProfit() / $this->getYield());
        }

        return 0;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}
