<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 2/9/17
 * Time: 9:29 PM
 */

namespace Gorserv\Gerp\ReportsBundle\Model\OperativeReport;

/**
 * Class TaskTypeReport
 *
 * @package Gorserv\Gerp\ReportsBundle\Model\OperativeReport
 * @author ${USER} <mail@artemd.ru>
 *
 * @method WorkTypeReport[] getItems()
 */
class TaskTypeReport extends AbstractReport
{

}
