<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 2/9/17
 * Time: 10:00 PM
 */

namespace Gorserv\Gerp\ReportsBundle\Model\OperativeReport;


use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Agreement;
use models\ListContrCalc;
use models\ListTechCalc;
use models\ListTtContrCalc;
use models\ListTtTechCalc;
use models\Task;
use models\TaskComment;
use models\TaskStatus;

class OperativeReportBuilder
{

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function build(array $params)
    {
        /** @var DateTime $start */
        $start = $params['periodStart'];
        /** @var DateTime $end */
        $end = $params['periodEnd'];
        /** @var DateInterval $interval */
        $interval = $params['interval'];

        $curDate = clone $start;
        $total = new TotalReport();
        $total->setStart($start);
        $total->setEnd($end);

        $totalData = $this->fetchData($start, (clone $end)->add($interval), $params);
        $totalCol = new TotalColumnReport();
        $totalCol->setLabel('Итого')
            ->setDate($start)
            ->setInterval($end->diff($start));
        foreach ($totalData as $partnerRow) {
            $this->orderRows($partnerRow, $totalCol);
        }

        while ($curDate->getTimestamp() <= $end->getTimestamp()) {
            $endDate = (clone $curDate)->add($interval);
            $report = $this->fetchData($curDate, $endDate, $params);

            $columnReport = new TotalColumnReport();
            $columnReport->setDate($curDate)
                ->setInterval($interval)
                ->setLabel($curDate->format('Y-m-d') . ' - ' . $endDate->format('Y-m-d'));
            $total->addItem($curDate->format('Y-m-d'), $columnReport);

            foreach ($report as $row) {
                $this->orderRows($row, $columnReport);
            }
            $curDate = $endDate;
        }

        $total->addItem('total', $totalCol);

        return $total;
    }

    private function orderRows(array $row, TotalColumnReport $columnReport)
    {
        $tmp = array_filter($columnReport->getItems(), function (TaskTypeReport $pluginReport) use ($row) {
            return $pluginReport->getLabel() === $row['pluginUid'];
        });
        $pluginReport = reset($tmp);

        if (false === $pluginReport) {
            $pluginReport = new TaskTypeReport();
            $pluginReport->setLabel($row['pluginUid']);
            $columnReport->addItem($row['pluginUid'], $pluginReport);
        }

        $tmp = array_filter($pluginReport->getItems(), function (WorkTypeReport $report) use ($row) {
            return $report->getLabel() === $row['title'];
        });
        $workTypeReport = reset($tmp);

        if (false === $workTypeReport) {
            $workTypeReport = new WorkTypeReport();
            $workTypeReport->setLabel($row['title']);
            $pluginReport->addItem($row['title'], $workTypeReport);
        }

        $workTypeReport->addItem($row['partner_title'], new PartnerReport($row));
    }

    private function fetchData($start, $end, $params)
    {
        $qb = $this->em->createQueryBuilder();

        $qb
            ->addSelect('count(task.id) as doneCount')
            ->addSelect('sum_of_subquery(' . $this->getSkpCashlessClosedQuery()->getDQL() . ') as skpCashlessCount')
            ->addSelect('sum_of_subquery(' . $this->getSkpCashClosedQuery()->getDQL() . ') as skpCashCount')
            ->addSelect('sum_of_subquery(' . $this->getConnectionsClosedQuery()->getDQL() . ') as connectionsCount')
            ->addSelect('sum_of_subquery(' . $this->getAccidentsClosedQuery()->getDQL() . ') as accidentsCount')
            ->addSelect('sum_of_subquery(' . $this->getGpClosedQuery()->getDQL() . ') as gpCount')
            // charges
            ->addSelect('0 as gp_charges')
            ->addSelect('sum_of_subquery(SELECT 
                                sum(CASE techWork.priceType
                                    WHEN :price THEN listTechCalc.price * listTechCalc.quant
                                    WHEN :smeta THEN listTechCalc.smetaVal * (listTechCalc.price/100)
                                    ELSE 0
                                END)
                            FROM 
                                '.ListTechCalc::class.' listTechCalc join listTechCalc.work as techWork
                            WHERE 
                                listTechCalc.task = task) as connection_charges')
            ->addSelect('sum_of_subquery(SELECT 
                                sum(CASE techTtWork.priceType
                                    WHEN :price THEN listTtTechCalc.price * listTtTechCalc.quant
                                    WHEN :smeta THEN listTtTechCalc.smetaVal * (listTtTechCalc.price/100)
                                    ELSE 0
                                END)
                            FROM 
                                '.ListTtTechCalc::class.' listTtTechCalc join listTtTechCalc.work as techTtWork
                            WHERE 
                                listTtTechCalc.task = task) as accident_charges')
            ->addSelect('sum(ticket.orientPrice)* (0.3 + partner.contragentfee / 100) as skp_charges')
            // yields
            ->addSelect('0 as gp_yield')
            ->addSelect('sum_of_subquery(SELECT
                                sum(CASE contrWork.priceType
                                    WHEN :price THEN listContrCalc.price * listContrCalc.quant
                                    WHEN :smeta THEN listContrCalc.smetaVal * (listContrCalc.price/100)
                                    ELSE 0
                                END)
                            FROM
                                '.ListContrCalc::class.' listContrCalc join listContrCalc.work as contrWork
                            WHERE
                                listContrCalc.task = task) as connection_yield')

            ->addSelect('sum_of_subquery(SELECT
                                sum(CASE contrTtWork.priceType
                                    WHEN :price THEN listTtContrCalc.price * listTtContrCalc.quant
                                    WHEN :smeta THEN listTtContrCalc.smetaVal * (listTtContrCalc.price/100)
                                    ELSE 0
                                END)
                            FROM
                                '.ListTtContrCalc::class.' listTtContrCalc join listTtContrCalc.work as contrTtWork
                            WHERE
                                listTtContrCalc.task = task) as accident_yield')
            ->addSelect('sum(ticket.orientPrice) as skp_yield')


            ->addSelect('partner.contrTitle partner_title', 'task.pluginUid', 'taskType.title')
            ->from(Task::class, 'task')
            ->join('task.ticket', 'ticket')
            ->join('ticket.partner', 'partner')
            ->join('ticket.taskType', 'taskType')
            ->join('task.status', 'status')
            ->andWhere('task.status not in (:cancelStatuses)')
            ->andWhere('exists (
                        select 
                            taskComment.id 
                        from 
                            ' . TaskComment::class . ' taskComment 
                        where 
                            taskComment.task = task and 
                            taskComment.status in (:doneStatuses) and 
                            (taskComment.datetime >= :startTime and taskComment.datetime < :endTime)
                    )')
            ->setParameters([
                'startTime' => $start,
                'endTime' => $end,
                'price' => 'price',
                'smeta' => 'smeta',
                'paytype_cash' => Agreement::CASH,
                'paytype_cashless' => Agreement::CASHLESS,

                'cancelStatuses' => [TaskStatus::STATUS_ACCIDENT_BY_PARTNER_CANCELED, 62, 56, 65, 60, 96, 64, 7, 5, 13, 95, 97, 74, 46, 43, 10, 17, 94, 18],
                'doneStatuses' => [TaskStatus::STATUS_ACCIDENT_DONE, TaskStatus::STATUS_CONNECTION_DONE, TaskStatus::STATUS_GLOBAL_PROBLEM_DONE, TaskStatus::STATUS_SERVICE_DONE],
                'closedStatuses' => [TaskStatus::STATUS_ACCIDENT_CLOSED, TaskStatus::STATUS_CONNECTION_CLOSED, TaskStatus::STATUS_SERVICE_CLOSED, TaskStatus::STATUS_GLOBAL_PROBLEM_CLOSED],
                'moneyReceivedStatuses' => [TaskStatus::STATUS_SERVICE_CASH_ADOPTED],

                'skp' => Task::SERVICE,
                'gp' => Task::GLOBAL_PROBLEM,
                'accidents' => Task::ACCIDENT,
                'connections' => Task::CONNECTION,
            ])
            ->groupBy('partner.id, taskType.id, task.pluginUid');

        if (!empty($params['client_type'])) {
            $qb
                ->andWhere('ticket.clntType=:client_type')
                ->setParameter('client_type', $params['client_type']);
        }
        if (!$params['partners']->isEmpty()) {
            $qb
                ->andWhere('partner IN (:partners)')
                ->setParameter('partners', $params['partners']);
        }
        if (!$params['sc']->isEmpty()) {
            $qb
                ->leftJoin('ticket.serviceCenter', 'service_center')
                ->andWhere('service_center.id IN (:sc)')
                ->setParameter('sc', $params['sc']);
        }

        return $qb->getQuery()->getArrayResult();
    }

    private function getSkpCashClosedQuery()
    {
        $qb = $this->em->createQueryBuilder();

        $qb->addSelect('1')
            ->from(Task::class, 'task_skp_cash_closed')
            ->join('task_skp_cash_closed.ticket', 'ticket_skp_cash_closed')
            ->join('ticket_skp_cash_closed.agreement', 'agreement_skp_cash_closed')
            ->andWhere('task_skp_cash_closed = task')
            ->andWhere('task_skp_cash_closed.pluginUid = :skp')
            ->andWhere('agreement_skp_cash_closed.paytype = :paytype_cash')
            ->andWhere('exists (
                            select 
                                tc2.id 
                            from 
                                ' . TaskComment::class . ' tc2 
                            where 
                                tc2.task = task_skp_cash_closed and 
                                tc2.status in (:moneyReceivedStatuses)
                        )');

        return $qb;
    }


    private function getSkpCashlessClosedQuery()
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('1')
            ->from(Task::class, 'task_skp_cashless_closed')
            ->join('task_skp_cashless_closed.ticket', 'ticket_skp_cashless_closed')
            ->join('ticket_skp_cashless_closed.agreement', 'agreement_skp_cashless_closed')
            ->andWhere('task_skp_cashless_closed = task')
            ->andWhere('task_skp_cashless_closed.pluginUid = :skp')
            ->andWhere('agreement_skp_cashless_closed.paytype = :paytype_cashless')
            ->andWhere('exists (
                            select 
                                tc3.id 
                            from 
                                ' . TaskComment::class . ' tc3 
                            where 
                                tc3.task = task_skp_cashless_closed and 
                                tc3.status in (:doneStatuses) and 
                                (tc3.datetime >= :startTime and tc3.datetime < :endTime)
                        )')
        ;

        return $qb;
    }

    private function getConnectionsClosedQuery()
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('1')
            ->from(Task::class, 'task_connections_closed')
            ->join('task_connections_closed.ticket', 'connections_closed_ticket')
            ->andWhere('task_connections_closed = task')
            ->andWhere('task_connections_closed.pluginUid = :connections')
            ->andWhere('
                    exists (
                        select 
                            taskComment2.id 
                        from 
                            ' . TaskComment::class . ' taskComment2 
                        where 
                            taskComment2.task = task_connections_closed and 
                            taskComment2.status in (:closedStatuses)
                    )');

        return $qb;
    }

    private function getAccidentsClosedQuery()
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('1')
            ->from(Task::class, 'task_accidents_closed')
            ->join('task_accidents_closed.ticket', 'accidents_closed_ticket')
            ->andWhere('task_accidents_closed = task')
            ->andWhere('task_accidents_closed.pluginUid = :accidents')
            ->andWhere('
                    exists (
                        select 
                            taskComment12.id 
                        from 
                            ' . TaskComment::class . ' taskComment12 
                        where 
                            taskComment12.task = task_accidents_closed and 
                            taskComment12.status in (:closedStatuses)
                    )');

        return $qb;
    }

    private function getGpClosedQuery()
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('1')
            ->from(Task::class, 'task_gp_closed')
            ->andWhere('task_gp_closed = task')
            ->andWhere('task_gp_closed.pluginUid = :gp')
            ->andWhere('
                    exists (
                        select 
                            taskComment13.id 
                        from 
                            ' . TaskComment::class . ' taskComment13 
                        where 
                            taskComment13.task = task_gp_closed and 
                            taskComment13.status in (:doneStatuses)
                    )');

        return $qb;
    }
}
