<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 2/9/17
 * Time: 9:29 PM
 */

namespace Gorserv\Gerp\ReportsBundle\Model\OperativeReport;


use DateInterval;
use DateTime;

/**
 * Class TotalColumnReport
 *
 * @package Gorserv\Gerp\ReportsBundle\Model\OperativeReport
 * @author ${USER} <mail@artemd.ru>
 *
 * @method TaskTypeReport[] getItems()
 */

class TotalColumnReport extends AbstractReport
{
    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var DateInterval
     */
    private $interval;

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return $this
     */
    public function setDate(DateTime $date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return DateInterval
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * @param DateInterval $interval
     * @return $this
     */
    public function setInterval(DateInterval $interval)
    {
        $this->interval = $interval;
        return $this;
    }


}
