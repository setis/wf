<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 25.01.17
 * Time: 15:48
 */

namespace Gorserv\Gerp\ReportsBundle\Helpers;


use PHPExcel_Style_Border;
use PHPExcel_Style_Fill;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Worksheet;

class MoneyReportExcelHelper
{
    public static function countColumns(array $array)
    {
        $count = count($array);
        if ($count == 0)
            return 1;
        return $count;
    }

    public static function cellCalc(array $a, $style = null)
    {
        if (count($a) <= 0) {
            return [['value' => '-', 'style' => $style]];
        }

        $r = [];
        foreach ($a as $k => $v) {
            $r[] = ['value' => $k, 'style' => $style];
        }
        return $r;
    }

    public static function cellDraw(PHPExcel_Worksheet $sheet, &$col, $row, array $cols, array $values)
    {
        $i = $col;
        if (count($cols) <= 0) {
            $sheet->setCellValueByColumnAndRow($col++, $row, null);
            return 1;
        }

        foreach ($cols as $k => $v) {
            $sheet->setCellValueByColumnAndRow($col++, $row, $values[$k] ?? null);
        }

        return $col - $i;
    }

    public static function setStyle(PHPExcel_Worksheet $sheet, $col, $row, $name)
    {
        if (null === $name) {
            return;
        }
        $sheet->getStyleByColumnAndRow($col, $row)->applyFromArray(self::$name());
    }

    public static function getStyleDefault()
    {
        return [
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                ]
            ]
        ];
    }

    public static function getStyleOutlineBoldBorder()
    {
        return [
            'borders' => [
                'outline' => [
                    'style' => PHPExcel_Style_Border::BORDER_THICK
                ]
            ]
        ];
    }

    public static function getStyleSc()
    {
        return [
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => '808080']
            ],
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            )
        ];
    }

    public static function getStyleTotal()
    {
        return [
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => '292929']
            ],
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            )
        ];
    }

    public static function getStylePercent()
    {
        return ['code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE];
    }

    public static function getStyleGray()
    {
        return [
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'e6e6e6']
            ]
        ];
    }

    public static function getStyleGreen()
    {
        return [
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'c5edd7']
            ]
        ];
    }

    public static function getStyleBlue()
    {
        return [
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => '7db1b3']
            ]
        ];
    }

    public static function getStyleRose()
    {
        return [
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'ffc0cb']
            ]
        ];
    }

    public static function getStyleOrange()
    {
        return [
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'ffc966']
            ]
        ];
    }

    public static function getStyleYellow()
    {
        return [
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'ffffcc']
            ]
        ];
    }


}
