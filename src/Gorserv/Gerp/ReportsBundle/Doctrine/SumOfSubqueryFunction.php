<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 2/13/17
 * Time: 12:38 PM
 */

namespace Gorserv\Gerp\ReportsBundle\Doctrine;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class SumOfSubqueryFunction extends FunctionNode
{

    public $expression = null;
    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        return 'sum((' .
            $this->expression->dispatch($sqlWalker) .
            '))'; // (7)
    }

    /**
     * @param Parser $parser
     *
     * @return void
     */
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER); // (2)
        $parser->match(Lexer::T_OPEN_PARENTHESIS); // (3)
        $this->expression = $parser->Subselect(); // (4)
        $parser->match(Lexer::T_CLOSE_PARENTHESIS); // (3)
    }
}
