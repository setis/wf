<?php

namespace Gorserv\Gerp\ReportsBundle\Controller;

use DateInterval;
use DateTime;
use Gorserv\Gerp\ReportsBundle\Form\OperativeForm;
use Gorserv\Gerp\ReportsBundle\Helpers\MoneyReportExcelHelper as H;
use Gorserv\Gerp\ReportsBundle\Model\OperativeReport\TotalReport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OperativeReportController
 * @package Gorserv\Gerp\ReportsBundle\Controller
 *
 * @Route("/operative")
 */
class OperativeReportController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction(Request $request)
    {
        $form = $this->get('form.factory')->create(OperativeForm::class, ['interval_type'=>0, 'periodStart'=>(new DateTime('-2 days'))->format('Y-m-d'), 'periodEnd'=>(new DateTime())->format('Y-m-d')]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $data['periodStart'] = new DateTime($data['periodStart']);
            $data['periodEnd'] = new DateTime($data['periodEnd']);
            switch ($data['interval_type']) {
                case 1:
                    $data['interval'] = new DateInterval('P7D');
                    break;
                case 2:
                    $data['interval'] = new DateInterval('P1M');
                    break;
                default:
                    $data['interval'] = new DateInterval('P1D');
            }

            $report = $this->get('gorserv_gerp_reports.model_operative_report.operative_report_builder')->build($data);

            if ($form->get('form_excel')->isClicked()) {
                return $this->buildExcel($report);
            }

            return $this->render('GorservGerpReportsBundle:OperativeReport:index.html.twig', [
                'report' => $report,
                'form' => $form->createView()
            ]);
        }

        return $this->render('GorservGerpReportsBundle:OperativeReport:index.html.twig', [
            'report' => null,
            'form' => $form->createView()
        ]);
    }

    private function buildExcel(TotalReport $report)
    {
        $excel = $this->get('phpexcel')->createPHPExcelObject();

        $excel->getProperties()->setCreator($this->getUser()->getFio())
            ->setLastModifiedBy($this->getUser()->getFio())
            ->setTitle("Оперативный отчёт " . $report->getStart()->format('Y-m-d') . ' - ' . $report->getEnd()->format('Y-m-d'));

        $sheet = $excel->setActiveSheetIndex(0);

        $r = 3;
        foreach ($report->getItem('total')->getItems() as $plugin) {
            foreach ($plugin->getItems() as $type) {
                $sheet->setCellValueByColumnAndRow(0, $r, $type->getLabel());
                $c = 1;
                foreach ($report->getItems() as $col) {
                    if (null === $col->getItem($plugin->getLabel()) || null === $col->getItem($plugin->getLabel())->getItem($type->getLabel())) {
                        $c += 7;
                        continue;
                    }
                    $p = $col->getItem($plugin->getLabel())->getItem($type->getLabel());
                    $sheet->setCellValueByColumnAndRow($c++, $r, $p->getDoneCount());
                    $sheet->setCellValueByColumnAndRow($c++, $r, $p->getClosedCount());
                    $sheet->setCellValueByColumnAndRow($c++, $r, $p->getYield());
                    $sheet->setCellValueByColumnAndRow($c++, $r, $p->getAverageBill());
                    $sheet->setCellValueByColumnAndRow($c++, $r, $p->getCharges());
                    $sheet->setCellValueByColumnAndRow($c++, $r, $p->getProfit());
                    $sheet->setCellValueByColumnAndRow($c++, $r, $p->getGrossProfit());
                    $sheet->getStyleByColumnAndRow($c - 1, $r)->applyFromArray(H::getStylePercent());
                }
                $sheet->getStyleByColumnAndRow(0, $r, $c - 1, $r)->applyFromArray(H::getStyleGreen());
                $r++;
                foreach ($type->getItems() as $partner) {
                    $sheet->setCellValueByColumnAndRow(0, $r, $partner->getLabel());
                    $c = 1;
                    foreach ($report->getItems() as $col) {
                        $sheet->setCellValueByColumnAndRow($c, 1, $col->getLabel());
                        $sheet->mergeCellsByColumnAndRow($c, 1, $c + 6, 1);
                        $sheet->setCellValueByColumnAndRow($c, 2, 'Выполнено');
                        $sheet->setCellValueByColumnAndRow($c + 1, 2, 'Закрыто');
                        $sheet->setCellValueByColumnAndRow($c + 2, 2, 'Доход');
                        $sheet->setCellValueByColumnAndRow($c + 3, 2, 'Ср.чек');
                        $sheet->setCellValueByColumnAndRow($c + 4, 2, 'Расходы');
                        $sheet->setCellValueByColumnAndRow($c + 5, 2, 'Прибыль');
                        $sheet->setCellValueByColumnAndRow($c + 6, 2, 'Валовая прибыль, %');

                        if (null === $col->getItem($plugin->getLabel()) || null === $col->getItem($plugin->getLabel())->getItem($type->getLabel()) || null === $col->getItem($plugin->getLabel())->getItem($type->getLabel())->getItem($partner->getLabel())) {
                            $c += 7;
                            continue;
                        }

                        $p = $col->getItem($plugin->getLabel())->getItem($type->getLabel())->getItem($partner->getLabel());
                        $sheet->setCellValueByColumnAndRow($c++, $r, $p->getDoneCount());
                        $sheet->setCellValueByColumnAndRow($c++, $r, $p->getClosedCount());
                        $sheet->setCellValueByColumnAndRow($c++, $r, $p->getYield());
                        $sheet->setCellValueByColumnAndRow($c++, $r, $p->getAverageBill());
                        $sheet->setCellValueByColumnAndRow($c++, $r, $p->getCharges());
                        $sheet->setCellValueByColumnAndRow($c++, $r, $p->getProfit());
                        $sheet->setCellValueByColumnAndRow($c++, $r, $p->getGrossProfit());
                        $sheet->getStyleByColumnAndRow($c - 1, $r)->applyFromArray(H::getStylePercent());
                    }
                    $r++;
                }

            }

            $sheet->setCellValueByColumnAndRow(0, $r, $this->get('translator')->trans($plugin->getLabel()));
            $c = 1;
            foreach ($report->getItems() as $col) {
                if (null === $col->getItem($plugin->getLabel())) {
                    $c += 7;
                    continue;
                }
                $p = $col->getItem($plugin->getLabel());
                $sheet->setCellValueByColumnAndRow($c++, $r, $p->getDoneCount());
                $sheet->setCellValueByColumnAndRow($c++, $r, $p->getClosedCount());
                $sheet->setCellValueByColumnAndRow($c++, $r, $p->getYield());
                $sheet->setCellValueByColumnAndRow($c++, $r, $p->getAverageBill());
                $sheet->setCellValueByColumnAndRow($c++, $r, $p->getCharges());
                $sheet->setCellValueByColumnAndRow($c++, $r, $p->getProfit());
                $sheet->setCellValueByColumnAndRow($c++, $r, $p->getGrossProfit());
                $sheet->getStyleByColumnAndRow($c - 1, $r)->applyFromArray(H::getStylePercent());
            }
            $sheet->getStyleByColumnAndRow(0, $r, $c - 1, $r)->applyFromArray(H::getStyleGray());
            $r++;
        }
        $sheet->getStyleByColumnAndRow(0, 1, $c - 1, $r)->applyFromArray(H::getStyleDefault());

        $sheet->setCellValueByColumnAndRow(0, $r, 'Итоги за период:');
        $c = 1;
        foreach ($report->getItems() as $col) {
            $sheet->setCellValueByColumnAndRow($c++, $r, $col->getDoneCount());
            $sheet->setCellValueByColumnAndRow($c++, $r, $col->getClosedCount());
            $sheet->setCellValueByColumnAndRow($c++, $r, $col->getYield());
            $sheet->setCellValueByColumnAndRow($c++, $r, $col->getAverageBill());
            $sheet->setCellValueByColumnAndRow($c++, $r, $col->getCharges());
            $sheet->setCellValueByColumnAndRow($c++, $r, $col->getProfit());
            $sheet->setCellValueByColumnAndRow($c++, $r, $col->getGrossProfit());
            $sheet->getStyleByColumnAndRow($c - 1, $r)->applyFromArray(H::getStylePercent());
            $sheet->getStyleByColumnAndRow(1, 1, $c - 1, $r)->applyFromArray(H::getStyleOutlineBoldBorder());
        }
        $sheet->getStyleByColumnAndRow(0, $r, $c - 1, $r)->applyFromArray(H::getStyleTotal());

        $sheet->freezePane('B3');
        $sheet->getColumnDimension('A')->setAutoSize(true);

        $writer = $this->get('phpexcel')->createWriter($excel, 'Excel5');
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'operative-report-' . $report->getStart()->format(DATE_ATOM) . '-' . $report->getEnd()->format(DATE_ATOM) . '.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }
}
