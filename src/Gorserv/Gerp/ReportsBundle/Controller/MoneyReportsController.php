<?php

namespace Gorserv\Gerp\ReportsBundle\Controller;

use classes\reports42\ReportAllScPeriod;
use classes\reports42\ReportScPeriod;
use classes\reports42\ReportTechPeriod;
use DateTime;
use FOS\RestBundle\Controller\Annotations\Route;
use Gorserv\Gerp\ReportsBundle\Helpers\MoneyReportExcelHelper as H;
use PHPExcel_Worksheet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class ServiceCenterMoneyReportController
 * @package Gorserv\Gerp\ReportsBundle\Controller
 *
 * @Route("/financial")
 */
class MoneyReportsController extends Controller
{

    /**
     * @Route("/service-center-form")
     */
    public function fullReportFormAction(Request $request)
    {
        $form = $this->buildFullReportForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('renderHtml')->isClicked()) {
                return $this->forward('GorservGerpReportsBundle:MoneyReports:fullReportHtml');
            }

            if ($form->get('renderExcel')->isClicked()) {
                return $this->forward('GorservGerpReportsBundle:MoneyReports:fullReportExcel');
            }
        }


        return $this->render('GorservGerpReportsBundle:ServiceCenterMoneyReport:index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @return Form
     */
    private function buildFullReportForm()
    {
        $defaultDate = ['period' => new DateTime('previous month')];
        $form = $this->createFormBuilder($defaultDate)->add('period', DateType::class, [
            'widget' => 'single_text',
            'html5' => false,
            'attr' => [
                'class' => 'datepicker'
            ]
        ])
            ->add('renderHtml', SubmitType::class, array('label' => 'Просмотр'))
            ->add('renderExcel', SubmitType::class, array('label' => 'Выгрзука Excel'))
            ->getForm();

        return $form;
    }

    /**
     * @Route("/service-center.html")
     */
    public function fullReportHtmlAction(Request $request)
    {
        $form = $this->buildFullReportForm();
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->redirect($this->generateUrl('gorserv_gerp_reports_moneyreports_fullreportform'));
        }

        $report = new ReportAllScPeriod($form->getData()['period']->format('Y-m'));

        return $this->render('GorservGerpReportsBundle:ServiceCenterMoneyReport:report.html.php', [
            'form' => $form->createView(),
            'report' => $report
        ]);
    }

    /**
     * @Route("/service-center.xls")
     */
    public function fullReportExcelAction(Request $request)
    {
        $form = $this->buildFullReportForm();
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->redirect($this->generateUrl('gorserv_gerp_reports_moneyreports_fullreportform'));
        }
        /** @var DateTime $period */
        $period = $form->getData()['period'];
        $report = new ReportAllScPeriod($period->format('Y-m'));
        $excel = $this->get('phpexcel')->createPHPExcelObject();

        $excel->getProperties()->setCreator($this->getUser()->getFio())
            ->setLastModifiedBy($this->getUser()->getFio())
            ->setTitle("Расчётный лист сервисных центров " . $period->format('Y-m-d H:i'));

        $sheet = $excel->setActiveSheetIndex(0);

        $row = $this->drawHeader($report, $sheet);

        list($col, $row) = $this->drawTotalRow($sheet, $report, $row);

        /** Styles */
        $sheet->freezePane('D4');
        $sheet->getStyleByColumnAndRow(0, 4, $col, 4)->applyFromArray(H::getStyleSc());
        $sheet->getStyleByColumnAndRow(0, 1, $col, $row)->applyFromArray(H::getStyleDefault());

        $writer = $this->get('phpexcel')->createWriter($excel, 'Excel5');
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'service-centers-' . $period->format(DATE_ATOM) . '.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    /**
     * @param $report
     * @param $sheet
     * @return int Row
     */
    private function drawHeader(ReportAllScPeriod $report, PHPExcel_Worksheet $sheet)
    {
        $header = [
            ['value' => 'ID', 'rowspan' => 2,],
            ['value' => 'ФИО', 'rowspan' => 2,],
            ['value' => 'Должность', 'rowspan' => 2,],

            ['value' => 'Подключения', 'style' => 'getStyleGreen', 'colspan' => H::countColumns($report->getConnectionsCountsByCounterparty()) + H::countColumns($report->getConnectionsCountsByWork()) - 1],
            ['value' => 'ТТ', 'style' => 'getStyleBlue', 'colspan' => H::countColumns($report->getAccidentsCountsByCounterparty()) + H::countColumns($report->getAccidentsCountsByWork()) - 1],
            ['value' => 'СКП (наличные)', 'style' => 'getStyleYellow', 'colspan' => H::countColumns($report->getSkpCashCountByCounterparty())],
            ['value' => 'СКП (безналичные)', 'style' => 'getStyleOrange', 'colspan' => H::countColumns($report->getSkpCashlessCountByCounterparty())],

            ['value' => 'Подключения', 'style' => 'getStyleGreen', 'colspan' => H::countColumns($report->getConnectionsCountsByCounterparty())],
            ['value' => 'ТТ', 'style' => 'getStyleBlue', 'colspan' => H::countColumns($report->getAccidentsCountsByCounterparty())],

            ['value' => 'СКП (наличные)', 'style' => 'getStyleYellow', 'colspan' => H::countColumns($report->getSkpCashInvoiceNotPayedByCounterparty()) + H::countColumns($report->getSkpCashInvoiceByCounterparty()) + H::countColumns($report->getSkpCashCountByCounterparty()) + 2],
            ['value' => 'СКП (безналичные)', 'style' => 'getStyleOrange', 'colspan' => H::countColumns($report->getSkpCashlessInvoiceNotPayedByCounterparty()) + H::countColumns($report->getSkpCashlessInvoiceByCounterparty()) + H::countColumns($report->getSkpCashlessCountByCounterparty()) + 2],

            ['value' => 'Итого Сделка', 'style' => 'getStyleBlue', 'rowspan' => 2,],
            ['value' => 'Итого Штраф', 'style' => 'getStyleRose', 'rowspan' => 2,],
            ['value' => 'Сделка + штраф, баллы', 'style' => 'getStyleBlue', 'rowspan' => 2,],

            ['value' => 'KPI 1 "Конвертация"', 'rowspan' => 2,],
            ['value' => 'Вес', 'rowspan' => 2,],
            ['value' => 'Коэффицент', 'rowspan' => 2,],
            ['value' => 'KPI 1 #', 'rowspan' => 2,],

            ['value' => 'KPI 2 "Обзвон"', 'style' => 'getStyleGray', 'rowspan' => 2,],
            ['value' => 'Вес', 'style' => 'getStyleGray', 'rowspan' => 2,],
            ['value' => 'Коэффицент', 'style' => 'getStyleGray', 'rowspan' => 2,],
            ['value' => 'KPI 2 #', 'style' => 'getStyleGray', 'rowspan' => 2,],

            ['value' => 'KPI 3 "Средний чек"', 'rowspan' => 2,],
            ['value' => 'Вес', 'rowspan' => 2,],
            ['value' => 'Коэффицент', 'rowspan' => 2,],
            ['value' => 'KPI 3 #', 'rowspan' => 2,],

            ['value' => 'KPI 4 "Объем баллов"', 'style' => 'getStyleGray', 'rowspan' => 2,],
            ['value' => 'Вес', 'style' => 'getStyleGray', 'rowspan' => 2,],
            ['value' => 'Коэффицент', 'style' => 'getStyleGray', 'rowspan' => 2,],
            ['value' => 'KPI 4 #', 'style' => 'getStyleGray', 'rowspan' => 2,],

            ['value' => 'Итого KPI', 'style' => 'getStyleGreen', 'rowspan' => 2,],
            ['value' => 'Итого КРI (Рубли)', 'style' => 'getStyleBlue', 'rowspan' => 2,],
            ['value' => 'Итого(баллы)', 'style' => 'getStyleGreen', 'rowspan' => 2,],
            ['value' => 'Итого(Рубли)', 'style' => 'getStyleBlue', 'rowspan' => 2,],
            ['value' => 'К выплате', 'style' => 'getStyleGreen', 'rowspan' => 2,],
        ];

        $row = 1;
        $col = 0;
        foreach ($header as $cell) {
            $sheet->setCellValueByColumnAndRow($col, $row, $cell['value']);
            H::setStyle($sheet, $col, $row, $cell['style']);
            if (isset($cell['rowspan'])) {
                $sheet->mergeCellsByColumnAndRow($col, $row, $col, $row + $cell['rowspan']);
            }
            if (isset($cell['colspan'])) {
                $sheet->mergeCellsByColumnAndRow($col, $row, $col += $cell['colspan'], $row);
            }
            $col++;
        }

        $header2 = [
            ['value' => 'Кол-во по агенту, шт.', 'style' => 'getStyleGreen', 'colspan' => H::countColumns($report->getConnectionsCountsByCounterparty()) - 1],
            ['value' => 'Кол-во по работам, шт.', 'style' => 'getStyleGreen', 'colspan' => H::countColumns($report->getConnectionsCountsByWork()) - 1],

            ['value' => 'Кол-во по агенту, шт.', 'style' => 'getStyleBlue', 'colspan' => H::countColumns($report->getAccidentsCountsByCounterparty()) - 1],
            ['value' => 'Кол-во по работам, шт.', 'style' => 'getStyleBlue', 'colspan' => H::countColumns($report->getAccidentsCountsByWork()) - 1],

            ['value' => 'Кол-во по агенту, шт.', 'style' => 'getStyleYellow', 'colspan' => H::countColumns($report->getSkpCashCountByCounterparty())],
            ['value' => 'Кол-во по агенту, шт.', 'style' => 'getStyleOrange', 'colspan' => H::countColumns($report->getSkpCashlessCountByCounterparty())],

            ['value' => 'Суммы по агентам, р.', 'style' => 'getStyleGreen', 'colspan' => H::countColumns($report->getConnectionsCountsByCounterparty())],
            ['value' => 'Суммы по агентам, р.', 'style' => 'getStyleBlue', 'colspan' => H::countColumns($report->getAccidentsCountsByCounterparty())],

            ['value' => 'Суммы по агентам (статус "Выполено"), р.', 'style' => 'getStyleYellow', 'colspan' => H::countColumns($report->getSkpCashInvoiceNotPayedByCounterparty())],
            ['value' => 'Суммы по агентам (в отправленном акте), р.', 'style' => 'getStyleYellow', 'colspan' => H::countColumns($report->getSkpCashInvoiceByCounterparty())],
            ['value' => 'ФОТ (30%), р.', 'style' => 'getStyleYellow', 'colspan' => H::countColumns($report->getSkpCashCountByCounterparty())],

            ['value' => 'Суммы по агентам (статус "Выполено"), р.', 'style' => 'getStyleOrange', 'colspan' => H::countColumns($report->getSkpCashlessInvoiceNotPayedByCounterparty())],
            ['value' => 'Суммы по агентам (в отправленном акте), р.', 'style' => 'getStyleOrange', 'colspan' => H::countColumns($report->getSkpCashlessInvoiceByCounterparty())],
            ['value' => 'ФОТ (30%), р.', 'style' => 'getStyleOrange', 'colspan' => H::countColumns($report->getSkpCashlessCountByCounterparty())],
        ];

        $row = 2;
        $col = 3;
        foreach ($header2 as $cell) {
            $sheet->setCellValueByColumnAndRow($col, $row, $cell['value']);
            H::setStyle($sheet, $col, $row, $cell['style']);
            if (isset($cell['rowspan'])) {
                $sheet->mergeCellsByColumnAndRow($col, $row, $col, $row + $cell['rowspan']);
            }
            if (isset($cell['colspan'])) {
                $sheet->mergeCellsByColumnAndRow($col, $row, $col += $cell['colspan'], $row);
            }
            $col++;
        }

        $header3 = array_merge(
            H::cellCalc($report->getConnectionsCountsByCounterparty(), 'getStyleGreen'),
            H::cellCalc($report->getConnectionsCountsByWork(), 'getStyleGreen'),

            H::cellCalc($report->getAccidentsCountsByCounterparty(), 'getStyleBlue'),
            H::cellCalc($report->getAccidentsCountsByWork(), 'getStyleBlue'),

            H::cellCalc($report->getSkpCashCountByCounterparty(), 'getStyleYellow'),
            [['value' => 'Итого:', 'style' => 'getStyleYellow']],
            H::cellCalc($report->getSkpCashlessCountByCounterparty(), 'getStyleOrange'),
            [['value' => 'Итого:', 'style' => 'getStyleOrange']],

            H::cellCalc($report->getConnectionsProfitByCounterparty(), 'getStyleGreen'),
            [['value' => 'Итого:', 'style' => 'getStyleGreen']],

            H::cellCalc($report->getAccidentsProfitByCounterparty(), 'getStyleBlue'),
            [['value' => 'Итого:', 'style' => 'getStyleBlue']],

            H::cellCalc($report->getSkpCashInvoiceNotPayedByCounterparty(), 'getStyleYellow'),
            [['value' => 'Итого:', 'style' => 'getStyleYellow']],
            H::cellCalc($report->getSkpCashInvoiceByCounterparty(), 'getStyleYellow'),
            [['value' => 'Итого:', 'style' => 'getStyleYellow']],
            H::cellCalc($report->getSkpCashProfitByCounterparty(), 'getStyleYellow'),
            [['value' => 'Итого:', 'style' => 'getStyleYellow']],

            H::cellCalc($report->getSkpCashlessInvoiceNotPayedByCounterparty(), 'getStyleOrange'),
            [['value' => 'Итого:', 'style' => 'getStyleOrange']],
            H::cellCalc($report->getSkpCashlessInvoiceByCounterparty(), 'getStyleOrange'),
            [['value' => 'Итого:', 'style' => 'getStyleOrange']],
            H::cellCalc($report->getSkpCashlessProfitByCounterparty(), 'getStyleOrange'),
            [['value' => 'Итого:', 'style' => 'getStyleOrange']]
        );

        $row = 3;
        $col = 3;
        foreach ($header3 as $cell) {
            $sheet->setCellValueByColumnAndRow($col, $row, $cell['value']);
            H::setStyle($sheet, $col, $row, $cell['style']);
            $col++;
        }

        return $row;
    }

    /**
     * @param $sheet
     * @param $report
     * @param $row
     * @param $tech
     */
    private function drawTechRow(PHPExcel_Worksheet $sheet, ReportAllScPeriod $report, &$row, $tech)
    {
        $col = 0;
        /** @var ReportTechPeriod $tech */
        $technician = $tech->getTech();

        $sheet->setCellValueByColumnAndRow($col++, ++$row, $technician['id']);
        $sheet->setCellValueByColumnAndRow($col++, $row, $technician['fio']);
        $sheet->setCellValueByColumnAndRow($col++, $row, $technician['title']);

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getConnectionsCountsByCounterparty(), $tech->getConnectionsCountsByCounterparty());
        $sheet->getStyleByColumnAndRow($col - $beginCol, $row, $col - 1, $row)->applyFromArray(H::getStyleGreen());
        $beginCol = H::cellDraw($sheet, $col, $row, $report->getConnectionsCountsByWork(), $tech->getConnectionsCountsByWork());
        $sheet->getStyleByColumnAndRow($col - $beginCol, $row, $col - 1, $row)->applyFromArray(H::getStyleGreen());

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getAccidentsCountsByCounterparty(), $tech->getAccidentsCountsByCounterparty());
        $sheet->getStyleByColumnAndRow($col - $beginCol, $row, $col - 1, $row)->applyFromArray(H::getStyleBlue());
        $beginCol = H::cellDraw($sheet, $col, $row, $report->getAccidentsCountsByWork(), $tech->getAccidentsCountsByWork());
        $sheet->getStyleByColumnAndRow($col - $beginCol, $row, $col - 1, $row)->applyFromArray(H::getStyleBlue());

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getSkpCashCountByCounterparty(), $tech->getSkpCashCountByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getSkpCashCount());
        $sheet->getStyleByColumnAndRow($col - $beginCol - 1, $row, $col - 1, $row)->applyFromArray(H::getStyleYellow());

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getSkpCashlessCountByCounterparty(), $tech->getSkpCashlessCountByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getSkpCashlessCount());
        $sheet->getStyleByColumnAndRow($col - $beginCol - 1, $row, $col - 1, $row)->applyFromArray(H::getStyleOrange());

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getConnectionsProfitByCounterparty(), $tech->getConnectionsProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getConnectionsProfit());
        $sheet->getStyleByColumnAndRow($col - $beginCol - 1, $row, $col - 1, $row)->applyFromArray(H::getStyleGreen());

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getAccidentsProfitByCounterparty(), $tech->getAccidentsProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getAccidentsProfit());
        $sheet->getStyleByColumnAndRow($col - $beginCol - 1, $row, $col - 1, $row)->applyFromArray(H::getStyleBlue());

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getSkpCashInvoiceNotPayedByCounterparty(), $tech->getSkpCashInvoiceNotPayedByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getSkpCashInvoiceNotPayed());
        $sheet->getStyleByColumnAndRow($col - $beginCol - 1, $row, $col - 1, $row)->applyFromArray(H::getStyleYellow());

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getSkpCashInvoiceByCounterparty(), $tech->getSkpCashInvoiceByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getSkpCashInvoice());
        $sheet->getStyleByColumnAndRow($col - $beginCol - 1, $row, $col - 1, $row)->applyFromArray(H::getStyleYellow());

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getSkpCashProfitByCounterparty(), $tech->getSkpCashProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getSkpCashProfit());
        $sheet->getStyleByColumnAndRow($col - $beginCol - 1, $row, $col - 1, $row)->applyFromArray(H::getStyleYellow());

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getSkpCashlessInvoiceNotPayedByCounterparty(), $tech->getSkpCashlessInvoiceNotPayedByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getSkpCashlessInvoiceNotPayed());
        $sheet->getStyleByColumnAndRow($col - $beginCol - 1, $row, $col - 1, $row)->applyFromArray(H::getStyleOrange());

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getSkpCashlessInvoiceByCounterparty(), $tech->getSkpCashlessInvoiceByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getSkpCashlessInvoice());
        $sheet->getStyleByColumnAndRow($col - $beginCol - 1, $row, $col - 1, $row)->applyFromArray(H::getStyleOrange());

        $beginCol = H::cellDraw($sheet, $col, $row, $report->getSkpCashlessProfitByCounterparty(), $tech->getSkpCashlessProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getSkpCashlessProfit());
        $sheet->getStyleByColumnAndRow($col - $beginCol - 1, $row, $col - 1, $row)->applyFromArray(H::getStyleOrange());

        ///////////////////////////////////////////////////////
        $sheet->getStyleByColumnAndRow($col, $row, $col, $row)->applyFromArray(H::getStyleBlue());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getProfit());

        $sheet->getStyleByColumnAndRow($col, $row, $col, $row)->applyFromArray(H::getStyleRose());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getPenalties());

        $sheet->getStyleByColumnAndRow($col, $row, $col, $row)->applyFromArray(H::getStyleBlue());
        $sheet->setCellValueByColumnAndRow($col++, $row, ($tech->getProfit() + $tech->getPenalties()) / $tech->getPointPrice());

        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getKPI1() * 100);
        $sheet->setCellValueByColumnAndRow($col++, $row, ReportTechPeriod::KPI1FAC);
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getP1kpi());
        $sheet->setCellValueByColumnAndRow($col++, $row, ReportTechPeriod::KPI1FAC * $tech->getP1kpi());

        $sheet->getStyleByColumnAndRow($col, $row, $col + 3, $row)->applyFromArray(H::getStyleGray());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getKPI2());
        $sheet->setCellValueByColumnAndRow($col++, $row, ReportTechPeriod::KPI2FAC);
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getP2kpi());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getP2kpi() * ReportTechPeriod::KPI2FAC);

        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getKPI3());
        $sheet->setCellValueByColumnAndRow($col++, $row, ReportTechPeriod::KPI3FAC);
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getP3kpi());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getP3kpi() * ReportTechPeriod::KPI3FAC);

        $sheet->getStyleByColumnAndRow($col, $row, $col + 3, $row)->applyFromArray(H::getStyleGray());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getKPI4());
        $sheet->setCellValueByColumnAndRow($col++, $row, ReportTechPeriod::KPI4FAC);
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getP4kpi());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getP4kpi() * ReportTechPeriod::KPI4FAC);

        ////////////////////////////
        $sheet->getStyleByColumnAndRow($col, $row, $col, $row)->applyFromArray(H::getStyleGreen());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getKpiSum());

        $sheet->getStyleByColumnAndRow($col, $row, $col, $row)->applyFromArray(H::getStyleBlue());
        $sheet->setCellValueByColumnAndRow($col++, $row, ($tech->getProfit() + $tech->getPenalties()) * ($tech->getKpiSum() - 100) / 100);

        $sheet->getStyleByColumnAndRow($col, $row, $col, $row)->applyFromArray(H::getStyleGreen());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getTotalPoints());

        $sheet->getStyleByColumnAndRow($col, $row, $col, $row)->applyFromArray(H::getStyleBlue());
        $sheet->setCellValueByColumnAndRow($col++, $row, $tech->getKpiSum() * ($tech->getProfit() + $tech->getPenalties()) / $tech->getPointPrice());

        $sheet->getStyleByColumnAndRow($col, $row, $col, $row)->applyFromArray(H::getStyleGreen());
        $sheet->setCellValueByColumnAndRow($col, $row, round($tech->getSalary() - $tech->getSkpCashProfit()));
        return $row;
    }

    /**
     * @param $sheet
     * @param $report
     * @param $reportSc
     * @param $row
     * @return int
     */
    private function drawScRow(PHPExcel_Worksheet $sheet, ReportAllScPeriod $report, ReportScPeriod $reportSc, $row)
    {
        $col = 0;
        $firstRow = ++$row;
        $sheet->setCellValueByColumnAndRow($col, $row, $reportSc->title);
        $sheet->mergeCellsByColumnAndRow($col, $row, $col + 2, $row);
        foreach ($reportSc->getItems() as $tech) {
            $this->drawTechRow($sheet, $report, $row, $tech);
        }

        $sheet->setCellValueByColumnAndRow($col, ++$row, $reportSc->title);
        $sheet->mergeCellsByColumnAndRow($col, $row, $col + 2, $row);
        $col += 3;
        H::cellDraw($sheet, $col, $row, $report->getConnectionsCountsByCounterparty(), $reportSc->getConnectionsCountsByCounterparty());
        H::cellDraw($sheet, $col, $row, $report->getConnectionsCountsByWork(), $reportSc->getConnectionsCountsByWork());
        H::cellDraw($sheet, $col, $row, $report->getAccidentsCountsByCounterparty(), $reportSc->getAccidentsCountsByCounterparty());
        H::cellDraw($sheet, $col, $row, $report->getAccidentsCountsByWork(), $reportSc->getAccidentsCountsByWork());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashCountByCounterparty(), $reportSc->getSkpCashCountByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getSkpCashCount());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashlessCountByCounterparty(), $reportSc->getSkpCashlessCountByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getSkpCashlessCount());

        H::cellDraw($sheet, $col, $row, $report->getConnectionsProfitByCounterparty(), $reportSc->getConnectionsProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getConnectionsProfit());

        H::cellDraw($sheet, $col, $row, $report->getAccidentsProfitByCounterparty(), $reportSc->getAccidentsProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getAccidentsProfit());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashInvoiceNotPayedByCounterparty(), $reportSc->getSkpCashInvoiceNotPayedByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getSkpCashInvoiceNotPayed());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashInvoiceByCounterparty(), $reportSc->getSkpCashInvoiceByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getSkpCashInvoice());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashProfitByCounterparty(), $reportSc->getSkpCashProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getSkpCashProfit());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashlessInvoiceNotPayedByCounterparty(), $reportSc->getSkpCashlessInvoiceNotPayedByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getSkpCashlessInvoiceNotPayed());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashlessInvoiceByCounterparty(), $reportSc->getSkpCashlessInvoiceByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getSkpCashlessInvoice());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashlessProfitByCounterparty(), $reportSc->getSkpCashlessProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getSkpCashlessProfit());

        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getProfit());
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getPenalties());
        $sheet->setCellValueByColumnAndRow($col++, $row, ($reportSc->getProfit() + $reportSc->getPenalties()) / $reportSc->getPointPrice());

        $col += 17;

        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getTotalPoints());
        $col++;
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportSc->getTotalProfit());
        $sheet->setCellValueByColumnAndRow($col, $row, $reportSc->getSalary() - $reportSc->getSkpCashProfit());

        $sheet->getStyleByColumnAndRow(0, $firstRow, $col, $firstRow)->applyFromArray(H::getStyleSc());
        $sheet->getStyleByColumnAndRow(0, $row, $col, $row)->applyFromArray(H::getStyleSc());

        return $row;
    }

    /**
     * @param $sheet
     * @param $report
     * @param $row
     * @return array
     */
    private function drawTotalRow(PHPExcel_Worksheet $sheet, ReportAllScPeriod $report, $row)
    {
        /** @var ReportScPeriod $reportSc */
        foreach ($report->getItems() as $reportSc) {
            $row = $this->drawScRow($sheet, $report, $reportSc, $row);
        }
        $col = 0;
        $sheet->setCellValueByColumnAndRow($col, ++$row, 'Полный итог:');
        $sheet->mergeCellsByColumnAndRow($col, $row, $col + 2, $row);
        $col += 2;
        $col++;

        H::cellDraw($sheet, $col, $row, $report->getConnectionsCountsByCounterparty(), $report->getConnectionsCountsByCounterparty());
        H::cellDraw($sheet, $col, $row, $report->getConnectionsCountsByWork(), $report->getConnectionsCountsByWork());

        H::cellDraw($sheet, $col, $row, $report->getAccidentsCountsByCounterparty(), $report->getAccidentsCountsByCounterparty());
        H::cellDraw($sheet, $col, $row, $report->getAccidentsCountsByWork(), $report->getAccidentsCountsByWork());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashCountByCounterparty(), $report->getSkpCashCountByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getSkpCashCount());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashlessCountByCounterparty(), $report->getSkpCashlessCountByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getSkpCashlessCount());

        H::cellDraw($sheet, $col, $row, $report->getConnectionsProfitByCounterparty(), $report->getConnectionsProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getConnectionsProfit());

        H::cellDraw($sheet, $col, $row, $report->getAccidentsProfitByCounterparty(), $report->getAccidentsProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getAccidentsProfit());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashInvoiceNotPayedByCounterparty(), $report->getSkpCashInvoiceNotPayedByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getSkpCashInvoiceNotPayed());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashInvoiceByCounterparty(), $report->getSkpCashInvoiceByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getSkpCashInvoice());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashProfitByCounterparty(), $report->getSkpCashProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getSkpCashProfit());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashlessInvoiceNotPayedByCounterparty(), $report->getSkpCashlessInvoiceNotPayedByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getSkpCashlessInvoiceNotPayed());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashlessInvoiceByCounterparty(), $report->getSkpCashlessInvoiceByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getSkpCashlessInvoice());

        H::cellDraw($sheet, $col, $row, $report->getSkpCashlessProfitByCounterparty(), $report->getSkpCashlessProfitByCounterparty());
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getSkpCashlessProfit());

        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getProfit());
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getPenalties());

        $col += 18;
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getTotalPoints());
        $col++;
        $sheet->setCellValueByColumnAndRow($col++, $row, $report->getTotalProfit());
        $sheet->setCellValueByColumnAndRow($col, $row, $report->getSalary() - $report->getSkpCashProfit());

        $sheet->getStyleByColumnAndRow(0, $row, $col, $row)->applyFromArray(H::getStyleTotal());

        return [$col, $row];
    }

}
