<?php

namespace Gorserv\Gerp\ReportsBundle\Form;

use models\ListContr;
use models\ListSc;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OperativeForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('interval_type', ChoiceType::class, array(
                'label' => 'Период детализации',
                'choices' => [
                    'Дни' => 0,
                    'Недели' => 1,
                    'Месяцы' => 2
                ],
                'multiple' => false,
                'required' => true,
                'data' => 0,
                'attr' => ['class' => 'interval-picker'],
            ))
            ->add('periodStart', TextType::class, array(
                'attr' => ['class' => 'datepicker-start'],
            ))
            ->add('periodEnd', TextType::class, array(
                'attr' => ['class' => 'datepicker-end'],
            ))
            ->add('partners', EntityType::class, array(
                'class' => ListContr::class,
                'label' => 'Контрагенты',
                'attr' => [
                    'class' => 'select2'
                ],
                'multiple' => true,
                'required' => false,
                'empty_data' => null
            ))
            ->add('sc', EntityType::class, array(
                'class' => ListSc::class,
                'label' => 'Сервисные центры',
                'attr' => [
                    'class' => 'select2'
                ],
                'multiple' => true,
                'required' => false,
                'empty_data' => null

            ))
            ->add('client_type', ChoiceType::class, array(
                'label' => 'Тип клиента',
                'choices' => [
                    'Любой' => 0,
                    'Физическое лицо' => 1,
                    'Юридическое лицо' => 2
                ]
            ))
            ->add('form_view', SubmitType::class, [
                'label' => 'Обзор',
                'attr' => [
                    'class' => 'btn btn-block btn-primary',
                ]
            ])
            ->add('form_excel', SubmitType::class, [
                'label' => 'В формате Excel',
                'attr' => [
                    'class' => 'btn btn-block btn-success',
                ]
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'gorserv_gerp_reports_bundle_operative_form';
    }
}
