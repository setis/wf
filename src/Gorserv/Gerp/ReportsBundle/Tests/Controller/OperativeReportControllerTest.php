<?php

namespace Gorserv\Gerp\ReportsBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OperativeReportControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testReport()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/report');
    }

}
