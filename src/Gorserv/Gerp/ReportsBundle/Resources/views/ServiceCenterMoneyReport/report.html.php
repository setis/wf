<?php
/** @var \models\User $user */

/** @var \Symfony\Component\Templating\PhpEngine $view */
$view->extend('layout_bootstrap.html.php');

/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$slots = $view->get('slots');

/** @var \WF\Core\UrlHelper $urlGenerator */
$urlGenerator = $view->get('url');
?>

<?php $slots->start('title'); ?>Расчётный лист<?php $slots->stop();?>

<?php $slots->set('container_class', 'container-fluid');?>

<?php $slots->start('container'); ?>

<?php
use classes\reports42\ReportTechPeriod;

/** @var $report \classes\reports42\ReportAllScPeriod */
/** @var $start integer */

function colspan_count(array $array)
{
    $count = count($array);
    if ($count == 0)
        return 1;
    return $count;
}

function drawTextSmallTh($data, $class, $openTag = '<th>', $closeTag = '</th>')
{
    if (count($data))
        foreach ($data as $k => $v) {
            echo str_replace('>', ' class="' . $class . '">', $openTag) . $k . $closeTag;
        }
    else
        echo str_replace('>', ' class="' . $class . '">', $openTag) . $closeTag;
}

function drawTextCell($data, $cols, $class, $openTag = '<td>', $closeTag = '</td>')
{
    if (count($cols))
        foreach ($cols as $k => $v) {
            echo str_replace('>', ' class="' . $class . '">', $openTag) . $data[$k] . $closeTag;
        }
    elseif (count($cols) > 0)
        foreach ($cols as $k => $v) {
            echo str_replace('>', ' class="' . $class . '">', $openTag) . $closeTag;
        }
    else
        echo str_replace('>', ' class="' . $class . '">', $openTag) . $closeTag;
}

function drawSumCell($data, $cols, $class, $openTag = '<td>', $closeTag = '</td>')
{
    if (count($cols))
        foreach ($cols as $k => $v) {
            echo str_replace('>', ' class="' . $class . '">', $openTag) . rub($data[$k], false) . $closeTag;
        }
    elseif (count($cols) > 0)
        foreach ($cols as $k => $v) {
            echo str_replace('>', ' class="' . $class . '">', $openTag) . $closeTag;
        }
    else
        echo str_replace('>', ' class="' . $class . '">', $openTag) . $closeTag;
}

if (!empty($alert))
    echo $alert;
?>

<style>
    body {
        background: #FFFFFF !important;
        font-family: 'Lucida Grande', Verdana, Arial, sans-serif;
    }

    .report_body {
        border: 1px solid #808080 !important;
        border-collapse: collapse !important;
    }

    .report_body th {
        border: 1px solid #808080 !important;
        border-collapse: collapse !important;
        white-space: nowrap;
        vertical-align: bottom;
    }

    .report_body th.small_th {
        font-weight: normal;
        font-size: 0.8em;
        white-space: normal;
    }

    .report_body tbody th {
        text-align: left;
    }

    .report_body td {
        border: 1px solid #808080 !important;
        border-collapse: collapse !important;
        white-space: nowrap;
    }

    .conn {
        background: #badbad;
    }

    .tt {
        background: #afe1f0;
    }

    .skp_cash {
        background: #ffff99;
    }

    .skp_cashless {
        background: #fcd975;
    }

    .sc_totals {
        background: #646464;
        font-weight: bold;
        text-align: left;
        color: white;
    }

    .sc_totals .conn {
        background: #1d8a1d;
    }

    .sc_totals .tt {
        background: #2c6986;
    }

    .sc_totals .skp_cash {
        background: #968c12;
    }

    .sc_totals .skp_cashless {
        background: #bb731b;
    }

    .report_totals_header {
        text-align: right;
    }

    .report_totals {
        background: #292929;
        font-weight: bold;
        color: white;
        text-align: left;
        /*font-size: larger;*/
    }

    .report_totals .conn {
        background: #1d8a1d;
        background: #292929;
    }

    .report_totals .tt {
        background: #2c6986;
        background: #292929;
    }

    .report_totals .skp_cash {
        background: #968c12;
        background: #292929;
    }

    .report_totals .skp_cashless {
        background: #bb731b;
        background: #292929;
    }

</style>


<table cellpadding="10" bgcolor="white">
    <tr>
        <td colspan="2">
            <h2>Расчетный лист по СЦ (Подключение, ТТ, СКП)</h2>
            <p>Период: <strong><?php echo $report->getPeriod(); ?> г.</strong></p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table border="1" cellspacing="0" cellpadding="5" width="100%" class="report_body table table-condensed ">
                <thead>
                <tr>
                    <th rowspan="3">ID</th>
                    <th rowspan="3">ФИО</th>
                    <th rowspan="3">Должность</th>
                    <!-- колтчество заявок -->
                    <th colspan="<?php echo colspan_count($report->getConnectionsCountsByCounterparty()) + colspan_count($report->getConnectionsCountsByWork()); ?>"
                        class="conn">Подключения
                    </th>
                    <th colspan="<?php echo colspan_count($report->getAccidentsCountsByCounterparty()) + colspan_count($report->getAccidentsCountsByWork()); ?>"
                        class="tt">ТТ
                    </th>
                    <th colspan="<?php echo colspan_count($report->getSkpCashCountByCounterparty()) + 1; ?>"
                        class="skp_cash">СКП (наличные)
                    </th>
                    <th colspan="<?php echo colspan_count($report->getSkpCashlessCountByCounterparty()) + 1; ?>"
                        class="skp_cashless">СКП (безналичные)
                    </th>
                    <!-- суммы по заявокам -->
                    <th colspan="<?php echo 2 * (colspan_count($report->getConnectionsCountsByCounterparty()) + 1); ?>"
                        class="conn">Подключения
                    </th>
                    <th colspan="<?php echo 2 * (colspan_count($report->getAccidentsCountsByCounterparty()) + 1); ?>"
                        class="tt">ТТ
                    </th>
                    <th colspan="<?php echo 3 * (colspan_count($report->getSkpCashCountByCounterparty()) + 1) + colspan_count($report->getSkpCashInvoiceNotPayedByCounterparty()) + 1; ?>"
                        class="skp_cash">СКП (наличные)
                    </th>
                    <th colspan="<?php echo 3 * (colspan_count($report->getSkpCashlessCountByCounterparty()) + 1) + colspan_count($report->getSkpCashlessInvoiceNotPayedByCounterparty()) + 1; ?>"
                        class="skp_cashless">СКП (безналичные)
                    </th>

                    <!--->
                    <th rowspan="3" class="success">Итого Сделка</th>
                    <th rowspan="3" class="success">Итого Штраф</th>
                    <th rowspan="3" class="success">Сделка + штраф, баллы</th>

                    <th rowspan="3" class="active">KPI 1 "Конвертация"</th>
                    <th rowspan="3" class="active">Вес</th>
                    <th rowspan="3" class="active">Коэффицент</th>
                    <th rowspan="3" class="active">KPI 1 #</th>

                    <th rowspan="3" class="info">KPI 2 "Обзвон"</th>
                    <th rowspan="3" class="info">Вес</th>
                    <th rowspan="3" class="info">Коэффицент</th>
                    <th rowspan="3" class="info">KPI 2 #</th>

                    <th rowspan="3" class="danger">KPI 3 "Средний чек"</th>
                    <th rowspan="3" class="danger">Вес</th>
                    <th rowspan="3" class="danger">Коэффицент</th>
                    <th rowspan="3" class="danger">KPI 3 #</th>

                    <th rowspan="3" class="warning">KPI 4 "Объем баллов"</th>
                    <th rowspan="3" class="warning">Вес</th>
                    <th rowspan="3" class="warning">Коэффицент</th>
                    <th rowspan="3" class="warning">KPI 4 #</th>

                    <th rowspan="3" class="success">Итого KPI</th>
                    <th rowspan="3" class="success">Итого(баллы)</th>
                    <th rowspan="3" class="success">Итого(Рубли)</th>
                    <th rowspan="3" class="success">К выплате</th>
                </tr>
                <tr>
                    <!-- колтчество заявок -->
                    <!-- Подключения (кол-во) -->
                    <th colspan="<?php echo colspan_count($report->getConnectionsCountsByCounterparty()); ?>"
                        class="conn">Кол-во по агенту, шт.
                    </th>
                    <th colspan="<?php echo colspan_count($report->getConnectionsCountsByWork()); ?>" class="conn">
                        Кол-во по работам, шт.
                    </th>
                    <!-- ТТ (кол-во) -->
                    <th colspan="<?php echo colspan_count($report->getAccidentsCountsByCounterparty()); ?>" class="tt">
                        Кол-во по агенту, шт.
                    </th>
                    <th colspan="<?php echo colspan_count($report->getAccidentsCountsByWork()); ?>" class="tt">Кол-во по
                        работам, шт.
                    </th>
                    <!-- СКП (наличные) (кол-во) -->
                    <th colspan="<?php echo colspan_count($report->getSkpCashCountByCounterparty()) + 1; ?>"
                        class="skp_cash">Кол-во по агенту, шт.
                    </th>
                    <!-- СКП (безналичные) (кол-во) -->
                    <th colspan="<?php echo colspan_count($report->getSkpCashlessCountByCounterparty()) + 1; ?>"
                        class="skp_cashless">Кол-во по агенту, шт.
                    </th>

                    <!-- суммы по заявкам -->
                    <!-- Подключения (суммы и конвертации) -->
                    <th colspan="<?php echo colspan_count($report->getConnectionsCountsByCounterparty()) + 1; ?>"
                        class="conn">Суммы по агентам, р.
                    </th>
                    <th colspan="<?php echo colspan_count($report->getConnectionsCountsByCounterparty()) + 1; ?>"
                        class="conn">Суммы конвертаций по агенту, р.
                    </th>

                    <!-- ТТ (суммы и конвертации) -->
                    <th colspan="<?php echo colspan_count($report->getAccidentsCountsByCounterparty()) + 1; ?>"
                        class="tt">Суммы по агентам, р.
                    </th>
                    <th colspan="<?php echo colspan_count($report->getAccidentsCountsByCounterparty()) + 1; ?>"
                        class="tt">Суммы конвертаций по агенту, р.
                    </th>

                    <!-- СКП (нал) (суммы и конвертации) -->
                    <th colspan="<?php echo colspan_count($report->getSkpCashInvoiceNotPayedByCounterparty()) + 1; ?>"
                        class="skp_cash">Суммы по агентам <br/>(статус
                        "Выполено"), р.
                    </th>
                    <th colspan="<?php echo colspan_count($report->getSkpCashCountByCounterparty()) + 1; ?>"
                        class="skp_cash">Суммы по агентам <br/>(в отправленном акте),
                        р.
                    </th>
                    <th colspan="<?php echo colspan_count($report->getSkpCashCountByCounterparty()) + 1; ?>"
                        class="skp_cash">ФОТ (30%), р.
                    </th>
                    <th colspan="<?php echo colspan_count($report->getSkpCashCountByCounterparty()) + 1; ?>"
                        class="skp_cash">Суммы конвертаций по агенту, р.
                    </th>

                    <!-- СКП безнал (суммы и конвертации) -->
                    <th colspan="<?php echo colspan_count($report->getSkpCashlessInvoiceNotPayedByCounterparty()) + 1; ?>"
                        class="skp_cashless">Суммы по агентам <br/>(статус
                        "Выполено"), р.
                    </th>
                    <th colspan="<?php echo colspan_count($report->getSkpCashlessCountByCounterparty()) + 1; ?>"
                        class="skp_cashless">Суммы по агентам <br/>(в
                        отправленном
                        акте), р.
                    </th>
                    <th colspan="<?php echo colspan_count($report->getSkpCashlessCountByCounterparty()) + 1; ?>"
                        class="skp_cashless">ФОТ (30%), р.
                    </th>
                    <th colspan="<?php echo colspan_count($report->getSkpCashlessCountByCounterparty()) + 1; ?>"
                        class="skp_cashless">Суммы конвертаций по агенту, р.
                    </th>
                </tr>
                <tr>
                    <!-- колтчество заявок -->
                    <!-- Подключения (кол-во) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getConnectionsCountsByCounterparty(), 'small_th conn');
                    ?>

                    <!-- Подключения (кол-во) (по работам) -->
                    <?php
                    drawTextSmallTh($report->getConnectionsCountsByWork(), 'small_th conn');
                    ?>

                    <!-- ТТ (кол-во) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getAccidentsCountsByCounterparty(), 'small_th tt');
                    ?>

                    <!-- ТТ (кол-во) (по работам) -->
                    <?php
                    drawTextSmallTh($report->getAccidentsCountsByWork(), 'small_th tt');
                    ?>

                    <!-- СКП (нал) (кол-во) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getSkpCashCountByCounterparty(), 'small_th skp_cash');
                    ?>
                    <th class="skp_cash">Итого</th>

                    <!-- СКП (безнал) (кол-во) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getSkpCashlessCountByCounterparty(), 'small_th skp_cashless');
                    ?>
                    <th class="skp_cashless">Итого</th>

                    <!-- суммы по заявокам -->
                    <!-- Подключения (суммы) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getConnectionsProfitByCounterparty(), 'small_th conn');
                    ?>
                    <th class="conn">Итого</th>

                    <!-- Подключения (конвертации) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getConnectionsConversionByCounterparty(), 'small_th conn');
                    ?>
                    <th class="conn">Итого</th>

                    <!-- ТТ (суммы) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getAccidentsProfitByCounterparty(), 'small_th tt');
                    ?>
                    <th class="tt">Итого</th>

                    <!-- ТТ (конвертации) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getAccidentsConversionByCounterparty(), 'small_th tt');
                    ?>
                    <th class="tt">Итого</th>

                    <!-- СКП нал (invoice) (статус "Выполнено") (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getSkpCashInvoiceNotPayedByCounterparty(), 'small_th skp_cash');
                    ?>
                    <th class="skp_cash">Итого</th>

                    <!-- СКП (нал) (invoice) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getSkpCashInvoiceByCounterparty(), 'small_th skp_cash');
                    ?>
                    <th class="skp_cash">Итого</th>

                    <!-- СКП (нал) (суммы) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getSkpCashProfitByCounterparty(), 'small_th skp_cash');
                    ?>
                    <th class="skp_cash">Итого</th>

                    <!-- СКП (нал) (конвертации) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getSkpCashConversionByCounterparty(), 'small_th skp_cash');
                    ?>
                    <th class="skp_cash">Итого</th>

                    <!-- СКП безнал (invoice) (статус "Выполнено") (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getSkpCashlessInvoiceNotPayedByCounterparty(), 'small_th skp_cashless');
                    ?>
                    <th class="skp_cashless">Итого</th>

                    <!-- СКП безнал (invoice) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getSkpCashlessInvoiceByCounterparty(), 'small_th skp_cashless');
                    ?>
                    <th class="skp_cashless">Итого</th>

                    <!-- СКП безнал (суммы) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getSkpCashlessProfitByCounterparty(), 'small_th skp_cashless');
                    ?>
                    <th class="skp_cashless">Итого</th>

                    <!-- СКП безнал (конвертации) (по агенту) -->
                    <?php
                    drawTextSmallTh($report->getSkpCashlessConversionByCounterparty(), 'small_th skp_cashless');
                    ?>
                    <th class="skp_cashless">Итого, р.</th>
                </tr>
                </thead>

                <?php
                foreach ($report->getItems() as $reportSc):
                    /** @var \classes\reports42\ReportScPeriod $reportSc */
                    $colspan = colspan_count($report->getConnectionsCountsByCounterparty())
                            + colspan_count($report->getConnectionsCountsByWork())
                            + colspan_count($report->getAccidentsCountsByCounterparty())
                            + colspan_count($report->getAccidentsCountsByWork())
                            + colspan_count($report->getSkpCashCountByCounterparty()) + 1
                            + colspan_count($report->getSkpCashlessCountByCounterparty()) + 1
                            + colspan_count($report->getConnectionsProfitByCounterparty()) + 1
                            + colspan_count($report->getConnectionsConversionByCounterparty()) + 1
                            + colspan_count($report->getAccidentsProfitByCounterparty()) + 1
                            + colspan_count($report->getAccidentsConversionByCounterparty()) + 1
                            + colspan_count($report->getSkpCashInvoiceNotPayedByCounterparty()) + 1
                            + colspan_count($report->getSkpCashProfitByCounterparty()) + 1
                            + colspan_count($report->getSkpCashProfitByCounterparty()) + 1
                            + colspan_count($report->getSkpCashConversionByCounterparty()) + 1
                            + colspan_count($report->getSkpCashlessInvoiceNotPayedByCounterparty()) + 1
                            + colspan_count($report->getSkpCashlessInvoiceByCounterparty()) + 1
                            + colspan_count($report->getSkpCashlessProfitByCounterparty()) + 1
                            + colspan_count($report->getSkpCashlessConversionByCounterparty()) + 1
                            + 23;

                    ?>
                    <tbody>
                    <tr class="sc_totals">
                        <td colspan="3" class="report_totals_header">СЦ <?php echo $reportSc->title; ?></td>
                        <?php
                        echo str_repeat('<td></td>', $colspan);
                        ?>

                    </tr>
                    <?php foreach ($reportSc->getItems() as $tech):
                        /** @var ReportTechPeriod $tech */
                        $technician = $tech->getTech();
                        ?>
                        <tr>
                            <td><?php echo $technician['id']; ?></td>
                            <td><?php echo $technician['fio']; ?></td>
                            <td><?php echo $technician['title']; ?></td>
                            <!-- Подключения (кол-во) (по агенту) -->
                            <?php
                            drawTextCell($tech->getConnectionsCountsByCounterparty(), $report->getConnectionsCountsByCounterparty(), 'conn');
                            ?>

                            <!-- Подключения (кол-во) (по работам) -->
                            <?php
                            drawTextCell($tech->getConnectionsCountsByWork(), $report->getConnectionsCountsByWork(), 'conn');
                            ?>

                            <!-- ТТ (кол-во) (по агенту) -->
                            <?php
                            drawTextCell($tech->getAccidentsCountsByCounterparty(), $report->getAccidentsCountsByCounterparty(), 'tt');
                            ?>

                            <!-- ТТ (кол-во) (по работам) -->
                            <?php
                            drawTextCell($tech->getAccidentsCountsByWork(), $report->getAccidentsCountsByWork(), 'tt');
                            ?>

                            <!-- СКП (кол-во) (наличные) -->
                            <?php
                            drawTextCell($tech->getSkpCashCountByCounterparty(), $report->getSkpCashCountByCounterparty(), 'skp_cash');
                            ?>
                            <th class="skp_cash"><?php echo $tech->getSkpCashCount(); ?></th>

                            <!-- СКП (кол-во) (безналичные) -->
                            <?php
                            drawTextCell($tech->getSkpCashlessCountByCounterparty(), $report->getSkpCashlessCountByCounterparty(), 'skp_cashless');
                            ?>
                            <th class="skp_cashless"><?php echo $tech->getSkpCashlessCount(); ?></th>


                            <!-- Подключения (суммы) (по агенту) -->
                            <?php
                            drawSumCell($tech->getConnectionsProfitByCounterparty(), $report->getConnectionsProfitByCounterparty(), 'conn');
                            ?>
                            <th class="conn"><?php echo rub($tech->getConnectionsProfit()); ?></th>

                            <!-- Подключения (конвертации) (по агенту) -->
                            <?php
                            drawSumCell($tech->getConnectionsConversionByCounterparty(), $report->getConnectionsConversionByCounterparty(), 'conn');
                            ?>
                            <th class="conn"><?php echo rub($tech->getConnectionsConversion()); ?></th>

                            <!-- ТТ (суммы) (по агенту) -->
                            <?php
                            drawSumCell($tech->getAccidentsProfitByCounterparty(), $report->getAccidentsProfitByCounterparty(), 'tt');
                            ?>
                            <th class="tt"><?php echo rub($tech->getAccidentsProfit()); ?></th>

                            <!-- ТТ (конвертации) (по агенту) -->
                            <?php
                            drawSumCell($tech->getAccidentsConversionByCounterparty(), $report->getAccidentsConversionByCounterparty(), 'tt');
                            ?>
                            <th class="tt"><?php echo rub($tech->getAccidentsConversion()); ?></th>

                            <!-- СКП нал (статус "Выполнено") (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashInvoiceNotPayedByCounterparty(), $report->getSkpCashInvoiceNotPayedByCounterparty(), 'skp_cash');
                            ?>
                            <th class="skp_cash"><?php echo rub($tech->getSkpCashInvoiceNotPayed()); ?></th>

                            <!-- СКП нал (суммы для контрагента) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashInvoiceByCounterparty(), $report->getSkpCashProfitByCounterparty(), 'skp_cash');
                            ?>
                            <th class="skp_cash"><?php echo rub($tech->getSkpCashInvoice()); ?></th>

                            <!-- СКП нал (суммы для техника) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashProfitByCounterparty(), $report->getSkpCashProfitByCounterparty(), 'skp_cash');
                            ?>
                            <th class="skp_cash"><?php echo rub($tech->getSkpCashProfit()); ?></th>

                            <!-- СКП нал (конвертации) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashConversionByCounterparty(), $report->getSkpCashConversionByCounterparty(), 'skp_cash');
                            ?>
                            <th class="skp_cash"><?php echo rub($tech->getSkpCashConversion()); ?></th>

                            <!-- СКП безнал (статус "Выполнено") (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashlessInvoiceNotPayedByCounterparty(), $report->getSkpCashlessInvoiceNotPayedByCounterparty(), 'skp_cashless');
                            ?>
                            <th class="skp_cashless"><?php echo rub($tech->getSkpCashlessInvoiceNotPayed()); ?></th>

                            <!-- СКП безнал (суммы для контрагента) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashlessInvoiceByCounterparty(), $report->getSkpCashlessInvoiceByCounterparty(), 'skp_cashless');
                            ?>
                            <th class="skp_cashless"><?php echo rub($tech->getSkpCashlessInvoice()); ?></th>

                            <!-- СКП безнал (суммы для техника) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashlessProfitByCounterparty(), $report->getSkpCashlessProfitByCounterparty(), 'skp_cashless');
                            ?>
                            <th class="skp_cashless"><?php echo rub($tech->getSkpCashlessProfit()); ?></th>

                            <!-- СКП безнал (конвертации) (по агенту) -->
                            <?php
                            drawSumCell($tech->getSkpCashlessConversionByCounterparty(), $report->getSkpCashlessConversionByCounterparty(), 'skp_cashless');
                            ?>
                            <th class="skp_cashless"><?php echo rub($tech->getSkpCashlessConversion()); ?></th>

                            <!--->
                            <td class="success"><?php echo rub($tech->getProfit()); ?></td>
                            <td class="success"><?php echo rub($tech->getPenalties()); ?></td>
                            <td class="success"><?php echo rub(($tech->getProfit() + $tech->getPenalties()) / $tech->getPointPrice()); ?></td>
                            <!-- KPI1 -->
                            <td class="active"><?php echo rub($tech->getKPI1() * 100); ?>%</td>
                            <td class="active"><?php echo ReportTechPeriod::KPI1FAC; ?>%</td>
                            <td class="active"><?php echo $tech->getP1kpi(); ?></td>
                            <td class="active"><?php echo rub(ReportTechPeriod::KPI1FAC * $tech->getP1kpi()); ?>%</td>

                            <!-- KPI2 -->
                            <td class="info"><?php echo rub($tech->getKPI2()); ?>%</td>
                            <td class="info"><?php echo ReportTechPeriod::KPI2FAC; ?>%</td>
                            <td class="info"><?php echo $tech->getP2kpi(); ?></td>
                            <td class="info"><?php echo rub($tech->getP2kpi() * ReportTechPeriod::KPI2FAC); ?>%</td>

                            <!-- KPI3 -->
                            <td class="danger"><?php echo rub($tech->getKPI3()); ?> баллов</td>
                            <td class="danger"><?php echo ReportTechPeriod::KPI3FAC; ?>%</td>
                            <td class="danger"><?php echo $tech->getP3kpi(); ?></td>
                            <td class="danger"><?php echo rub($tech->getP3kpi() * ReportTechPeriod::KPI3FAC); ?>%</td>

                            <!-- KPI4 -->
                            <td class="warning"><?php echo rub($tech->getKPI4()); ?> баллов</td>
                            <td class="warning"><?php echo ReportTechPeriod::KPI4FAC; ?>%</td>
                            <td class="warning"><?php echo $tech->getP4kpi(); ?></td>
                            <td class="warning"><?php echo rub($tech->getP4kpi() * ReportTechPeriod::KPI4FAC); ?>%</td>

                            <!-- KPI TOTALS -->
                            <td class="success"><?php echo rub($tech->getKpiSum()); ?>%</td>
                            <td class="success"><?php echo rub($tech->getTotalPoints()); ?></td>
                            <td class="success"><?php echo rub($tech->getKpiSum() * ($tech->getProfit() + $tech->getPenalties()) / $tech->getPointPrice()); ?></td>
                            <td class="success"><?php echo rub(round($tech->getSalary() - $tech->getSkpCashProfit())); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr class="sc_totals">
                        <td colspan="3" class="report_totals_header">Итоги по СЦ <?php echo $reportSc->title; ?>:</td>
                        <?php
                        drawTextCell($reportSc->getConnectionsCountsByCounterparty(), $report->getConnectionsCountsByCounterparty(), 'conn');
                        drawTextCell($reportSc->getConnectionsCountsByWork(), $report->getConnectionsCountsByWork(), 'conn');
                        drawTextCell($reportSc->getAccidentsCountsByCounterparty(), $report->getAccidentsCountsByCounterparty(), 'tt');
                        drawTextCell($reportSc->getAccidentsCountsByWork(), $report->getAccidentsCountsByWork(), 'tt');
                        ?>

                        <!-- СКП (кол-во) (наличные) -->
                        <?php
                        drawTextCell($reportSc->getSkpCashCountByCounterparty(), $report->getSkpCashCountByCounterparty(), 'skp_cash');
                        ?>
                        <td class="skp_cash"><?php echo $reportSc->getSkpCashCount(); ?></td>

                        <!-- СКП (кол-во) (безналичные) -->
                        <?php
                        drawTextCell($reportSc->getSkpCashlessCountByCounterparty(), $report->getSkpCashlessCountByCounterparty(), 'skp_cashless');
                        ?>
                        <td class="skp_cashless"><?php echo $reportSc->getSkpCashlessCount(); ?></td>


                        <!-- Подключения (суммы) (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getConnectionsProfitByCounterparty(), $report->getConnectionsProfitByCounterparty(), 'conn');
                        ?>
                        <td class="conn"><?php echo rub($reportSc->getConnectionsProfit()); ?></td>

                        <!-- Подключения (конвертации) (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getConnectionsConversionByCounterparty(), $report->getConnectionsConversionByCounterparty(), 'conn');
                        ?>
                        <td class="conn"><?php echo rub($reportSc->getConnectionsConversion()); ?></td>

                        <!-- ТТ (суммы) (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getAccidentsProfitByCounterparty(), $report->getAccidentsProfitByCounterparty(), 'tt');
                        ?>
                        <td class="tt"><?php echo rub($reportSc->getAccidentsProfit()); ?></td>

                        <!-- ТТ (конвертации) (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getAccidentsConversionByCounterparty(), $report->getAccidentsConversionByCounterparty(), 'tt');
                        ?>
                        <td class="tt"><?php echo rub($reportSc->getAccidentsConversion()); ?></td>

                        <!-- СКП нал (статус "Выполнено") (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getSkpCashInvoiceNotPayedByCounterparty(), $report->getSkpCashInvoiceNotPayedByCounterparty(), 'skp_cash');
                        ?>
                        <td class="skp_cash"><?php echo rub($reportSc->getSkpCashInvoiceNotPayed()); ?></td>

                        <!-- СКП нал (суммы для контрагента) (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getSkpCashInvoiceByCounterparty(), $report->getSkpCashProfitByCounterparty(), 'skp_cash');
                        ?>
                        <td class="skp_cash"><?php echo rub($reportSc->getSkpCashInvoice()); ?></td>

                        <!-- СКП нал (суммы для техника) (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getSkpCashProfitByCounterparty(), $report->getSkpCashProfitByCounterparty(), 'skp_cash');
                        ?>
                        <td class="skp_cash"><?php echo rub($reportSc->getSkpCashProfit()); ?></td>

                        <!-- СКП нал (конвертации) (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getSkpCashConversionByCounterparty(), $report->getSkpCashConversionByCounterparty(), 'skp_cash');
                        ?>
                        <td class="skp_cash"><?php echo rub($reportSc->getSkpCashConversion()); ?></td>

                        <!-- СКП безнал (статус "Выполнено") (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getSkpCashlessInvoiceNotPayedByCounterparty(), $report->getSkpCashlessInvoiceNotPayedByCounterparty(), 'skp_cashless');
                        ?>
                        <td class="skp_cashless"><?php echo rub($reportSc->getSkpCashlessInvoiceNotPayed()); ?></td>

                        <!-- СКП безнал (суммы для контрагента) (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getSkpCashlessInvoiceByCounterparty(), $report->getSkpCashlessInvoiceByCounterparty(), 'skp_cashless');
                        ?>
                        <td class="skp_cashless"><?php echo rub($reportSc->getSkpCashlessInvoice()); ?></td>

                        <!-- СКП безнал (суммы для техника) (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getSkpCashlessProfitByCounterparty(), $report->getSkpCashlessProfitByCounterparty(), 'skp_cashless');
                        ?>
                        <td class="skp_cashless"><?php echo rub($reportSc->getSkpCashlessProfit()); ?></td>

                        <!-- СКП безнал (конвертации) (по агенту) -->
                        <?php
                        drawSumCell($reportSc->getSkpCashlessConversionByCounterparty(), $report->getSkpCashlessConversionByCounterparty(), 'skp_cashless');
                        ?>
                        <td class="skp_cashless"><?php echo rub($reportSc->getSkpCashlessConversion()); ?></td>

                        <!--->
                        <td><?php echo rub($reportSc->getProfit()); ?></td>
                        <td><?php echo rub($reportSc->getPenalties()); ?></td>
                        <td><?php echo rub( ($reportSc->getProfit() + $reportSc->getPenalties()) / $reportSc->getPointPrice()); ?></td>
                        <td colspan="17"></td>
                        <!-- KPI TOTALS -->
                        <td><?php echo rub($reportSc->getTotalPoints()); ?></td>
                        <td><?php echo rub($reportSc->getTotalProfit()); ?></td>
                        <td><?php echo rub(round($reportSc->getSalary() - $reportSc->getSkpCashProfit())); ?></td>
                    </tr>
                    </tbody>
                <?php endforeach; ?>
                <tr class="report_totals">
                    <td colspan="3" class="report_totals_header">Итоги по всем СЦ:</td>
                    <?php
                    drawTextCell($report->getConnectionsCountsByCounterparty(), $report->getConnectionsCountsByCounterparty(), 'conn');
                    drawTextCell($report->getConnectionsCountsByWork(), $report->getConnectionsCountsByWork(), 'conn');
                    drawTextCell($report->getAccidentsCountsByCounterparty(), $report->getAccidentsCountsByCounterparty(), 'tt');
                    drawTextCell($report->getAccidentsCountsByWork(), $report->getAccidentsCountsByWork(), 'tt');
                    ?>

                    <!-- СКП (кол-во) (наличные) -->
                    <?php
                    drawTextCell($report->getSkpCashCountByCounterparty(), $report->getSkpCashCountByCounterparty(), 'skp_cash');
                    ?>
                    <td class="skp_cash"><?php echo $report->getSkpCashCount(); ?></td>

                    <!-- СКП (кол-во) (безналичные) -->
                    <?php
                    drawTextCell($report->getSkpCashlessCountByCounterparty(), $report->getSkpCashlessCountByCounterparty(), 'skp_cashless');
                    ?>
                    <td class="skp_cashless"><?php echo $report->getSkpCashlessCount(); ?></td>


                    <!-- Подключения (суммы) (по агенту) -->
                    <?php
                    drawSumCell($report->getConnectionsProfitByCounterparty(), $report->getConnectionsProfitByCounterparty(), 'conn');
                    ?>
                    <td class="conn"><?php echo rub($report->getConnectionsProfit()); ?></td>

                    <!-- Подключения (конвертации) (по агенту) -->
                    <?php
                    drawSumCell($report->getConnectionsConversionByCounterparty(), $report->getConnectionsConversionByCounterparty(), 'conn');
                    ?>
                    <td class="conn"><?php echo rub($report->getConnectionsConversion()); ?></td>

                    <!-- ТТ (суммы) (по агенту) -->
                    <?php
                    drawSumCell($report->getAccidentsProfitByCounterparty(), $report->getAccidentsProfitByCounterparty(), 'tt');
                    ?>
                    <td class="tt"><?php echo rub($report->getAccidentsProfit()); ?></td>

                    <!-- ТТ (конвертации) (по агенту) -->
                    <?php
                    drawSumCell($report->getAccidentsConversionByCounterparty(), $report->getAccidentsConversionByCounterparty(), 'tt');
                    ?>
                    <td class="tt"><?php echo rub($report->getAccidentsConversion()); ?></td>

                    <!-- СКП нал (статус "Выполнено") (по агенту) -->
                    <?php
                    drawSumCell($report->getSkpCashInvoiceNotPayedByCounterparty(), $report->getSkpCashInvoiceNotPayedByCounterparty(), 'skp_cash');
                    ?>
                    <td class="skp_cash"><?php echo rub($report->getSkpCashInvoiceNotPayed()); ?></td>

                    <!-- СКП нал (суммы для контрагента) (по агенту) -->
                    <?php
                    drawSumCell($report->getSkpCashInvoiceByCounterparty(), $report->getSkpCashProfitByCounterparty(), 'skp_cash');
                    ?>
                    <td class="skp_cash"><?php echo rub($report->getSkpCashInvoice()); ?></td>

                    <!-- СКП нал (суммы для техника) (по агенту) -->
                    <?php
                    drawSumCell($report->getSkpCashProfitByCounterparty(), $report->getSkpCashProfitByCounterparty(), 'skp_cash');
                    ?>
                    <td class="skp_cash"><?php echo rub($report->getSkpCashProfit()); ?></td>

                    <!-- СКП нал (конвертации) (по агенту) -->
                    <?php
                    drawSumCell($report->getSkpCashConversionByCounterparty(), $report->getSkpCashConversionByCounterparty(), 'skp_cash');
                    ?>
                    <td class="skp_cash"><?php echo rub($report->getSkpCashConversion()); ?></td>

                    <!-- СКП безнал (статус "Выполнено") (по агенту) -->
                    <?php
                    drawSumCell($report->getSkpCashlessInvoiceNotPayedByCounterparty(), $report->getSkpCashlessInvoiceNotPayedByCounterparty(), 'skp_cashless');
                    ?>
                    <td class="skp_cashless"><?php echo rub($report->getSkpCashlessInvoiceNotPayed()); ?></td>

                    <!-- СКП безнал (суммы для контрагента) (по агенту) -->
                    <?php
                    drawSumCell($report->getSkpCashlessInvoiceByCounterparty(), $report->getSkpCashlessInvoiceByCounterparty(), 'skp_cashless');
                    ?>
                    <td class="skp_cashless"><?php echo rub($report->getSkpCashlessInvoice()); ?></td>

                    <!-- СКП безнал (суммы для техника) (по агенту) -->
                    <?php
                    drawSumCell($report->getSkpCashlessProfitByCounterparty(), $report->getSkpCashlessProfitByCounterparty(), 'skp_cashless');
                    ?>
                    <td class="skp_cashless"><?php echo rub($report->getSkpCashlessProfit()); ?></td>

                    <!-- СКП безнал (конвертации) (по агенту) -->
                    <?php
                    drawSumCell($report->getSkpCashlessConversionByCounterparty(), $report->getSkpCashlessConversionByCounterparty(), 'skp_cashless');
                    ?>
                    <td class="skp_cashless"><?php echo rub($report->getSkpCashlessConversion()); ?></td>

                    <!-- ------->
                    <td><?php echo rub($report->getProfit()); ?></td>
                    <td><?php echo rub($report->getPenalties()); ?></td>
                    <td><?php echo rub( ($report->getProfit() + $report->getPenalties()) / $reportSc->getPointPrice()); ?></td>
                    <td colspan="17"></td>
                    <!-- KPI TOTALS -->
                    <td><?php echo rub($report->getTotalPoints()); ?></td>
                    <td><?php echo rub($report->getTotalProfit()); ?></td>
                    <td><?php echo rub(round($report->getSalary() - $report->getSkpCashProfit())); ?></td>
                </tr>
            </table>


        </td>
    </tr>
</table>
<p>Отчет создан: <?php echo date('Y-m-d H:i:s'); ?></p>
<p><?php echo time() - $start; ?>s</p>


<?php $slots->stop();?>

