<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 26.01.17
 * Time: 18:58
 */

namespace Gorserv\Gerp\IntegrationBundle\EventDispatcher;

use Symfony\Component\EventDispatcher\Event;

class BeforeSoapTaskCreateRequestEvent extends Event
{
    const NAME = 'gorserv.gerp.integration.before_soap_task_create_request';

    private $params;

    public function __construct(array $params = [])
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function setParams(array $params)
    {
        $this->params = $params;
        return $this;
    }

}
