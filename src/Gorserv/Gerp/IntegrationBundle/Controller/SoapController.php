<?php
/**
 * mail@artemd.ru
 * 21.03.2016
 */

namespace Gorserv\Gerp\IntegrationBundle\Controller;

use Gorserv\Gerp\IntegrationBundle\Server\AbstractIntegrationServer;
use Gorserv\Gerp\IntegrationBundle\Server\IntegrationServerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SoapServer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/soap")
 */
class SoapController extends Controller
{
    /**
     * @Route(
     *   "/{server_name}.php",
     *   requirements={"server_name": "atickets|rtmo_atickets|services|connections|accidents|numbre_uno_connections|numbre_uno_accidents"}
     * )
     */
    public function soapAction($server_name, Request $request)
    {
        $wsdl_file = $this->getWsdlFilename($server_name);
        $endpoint = trim($this->getParameter('http_base'), '/') . $request->getPathInfo();

        if ($request->query->has('wsdl')) {
            return $this->getWsdl($wsdl_file, $endpoint);
        }

        /** @var AbstractIntegrationServer $server */
        $server = $this->getServer($server_name);
        $response = $this->processRequest($server, $this->saveXmlToTmpFile(
            $this->prepareWsdlFile($wsdl_file, $endpoint)));

        return $response;
    }

    public function getWsdlFilename($server_name)
    {
        if ('numbre_uno_accidents' === $server_name) {
            $server_name = 'accidents';
        } elseif ('numbre_uno_connections' === $server_name) {
            $server_name = 'connections';
        } elseif ('atickets' === $server_name || 'rtmo_atickets' === $server_name) {
            $server_name = 'services';
        }

        $wsdl_file = __DIR__ . "/../Resources/wsdl/$server_name.tmpl.wsdl";
        if (!file_exists($wsdl_file)) {
            throw new \RuntimeException('WSDL file not found!');
        }
        return $wsdl_file;
    }

    /**
     *
     * @param string $server_name
     * @return IntegrationServerInterface
     */
    private function getServer($server_name)
    {
        switch ($server_name) {
            case 'rtmo_atickets':
            case 'atickets':
            case 'services':
                return $this->get('gorserv.gerp.integration.soap_server.agent_tickets');

            case 'numbre_uno_connections':
            case 'connections':
                return $this->get('gorserv.gerp.integration.soap_server.connections_tickets');

            case 'numbre_uno_accidents':
            case 'accidents':
                return $this->get('gorserv.gerp.integration.soap_server.accidents_tickets');
        }
    }


    /**
     * @param IntegrationServerInterface $server
     * @param string $wsdl_file
     * @return Response
     */
    private function processRequest(IntegrationServerInterface $server, $wsdl_file)
    {
        $soap = new SoapServer($wsdl_file);
        $soap->setObject($server);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=utf-8');

        ob_start();
        $soap->handle();
        $response->setContent(ob_get_clean());

        if ($this->has('monolog.logger.soap')) {
            $this->get('monolog.logger.soap')->info('SOAP logging', [
                'request' => json_encode($_REQUEST, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
                'response' => json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)
            ]);
        }

        return $response;
    }

    /**
     * @param string $wsdl_file
     * @param string $endpoint
     * @return Response
     */
    private function getWsdl($wsdl_file, $endpoint)
    {
        $xml = $this->prepareWsdlFile($wsdl_file, $endpoint);

        $response = new Response();
        $response->setContent($xml);
        $response->headers->set('Content-Type', 'text/xml; charset=utf-8');

        return $response;
    }

    private function prepareWsdlFile($wsdl_file, $endpoint)
    {
        $xml = file_get_contents($wsdl_file);
        $xml = str_replace(['{SERVICE_SCRIPT_URL}', '{SERVICE_SCRIPT_WSDL}'], [$endpoint, $endpoint . '?wsdl'], $xml);

        return $xml;
    }

    private function saveXmlToTmpFile($xml)
    {
        $filename = sys_get_temp_dir() . '/' . uniqid('soap_tmp_');
        file_put_contents($filename, $xml);

        return $filename;
    }
}
