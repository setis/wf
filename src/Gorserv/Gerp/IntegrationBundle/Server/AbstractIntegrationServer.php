<?php
/**
 * User: artem
 * Date: 04.10.16
 * Time: 17:35
 */

namespace Gorserv\Gerp\IntegrationBundle\Server;

use classes\DBMySQL;
use classes\User as User2;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Gorserv\Gerp\AppBundle\Exception\ExecutorNotFoundException;
use Gorserv\Gerp\AppBundle\Schedule\IntervalCheckerInterface;
use Gorserv\Gerp\IntegrationBundle\EventDispatcher\BeforeSoapTaskCreateRequestEvent;
use Gorserv\Gerp\ScheduleBundle\Exceptions\ScheduleException;
use Gorserv\Gerp\ScheduleBundle\Model\ExecutorChooserInterface;
use Gorserv\Gerp\ScheduleBundle\Model\TimeframeInterface;
use Gorserv\Gerp\ScheduleBundle\ScheduleManager;
use models\Agent;
use models\Gfx;
use models\ListAddr;
use models\ListContr;
use models\ListRtsubtype;
use models\ListSc;
use models\Task;
use models\TaskComment;
use models\TaskStatus;
use models\TaskType;
use models\Ticket;
use models\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use WF\Address\AddressProviderInterface;
use WF\Task\Model\TechnicalTicketInterface;
use WF\Task\TaskManager;

abstract class AbstractIntegrationServer implements IntegrationServerInterface
{
    const RETCODE_GETLASTERROR = -1;

    const OK = 0;
    const BAD_CREDENTIALS = 1;
    const DATABASE_ERROR = 10;
    const CANT_CREATE_TICKET = 20;
    const CANT_CANCEL_TICKET = 30;
    const WRONG_DATA = 40;
    const NOT_OWNED_TICKET = 50;
    const ALREADY_CANCELED = 60;
    const ALREADY_CREATED = 70;
    const WRONG_TICKET_NUMBER = 80;
    const WRONG_TICKET_TYPE = 90;
    const RESERVED_SCHEDULE = 100;
    const WRONG_SCHEDULE_TIME = 110;
    const UNKNOWN_ERROR = 1000;

    protected $retcodes = array(
        self::OK => 'OK',
        self::BAD_CREDENTIALS => 'Неверный логин или пароль',
        self::DATABASE_ERROR => 'Ошибка БД',
        self::CANT_CREATE_TICKET => 'Ошибка создания тикета',
        self::CANT_CANCEL_TICKET => 'Ошибка отмены заявки',
        self::WRONG_DATA => 'Недопустимый запрос',
        self::NOT_OWNED_TICKET => 'Указанный ID заявки не принадлежит Вашему контрагенту или не является заявкой нужного типа',
        self::ALREADY_CANCELED => 'Указанная заявка уже отменена',
        self::ALREADY_CREATED => 'Заявка с указанным идентификатором контрагента уже создана',
        self::WRONG_TICKET_NUMBER => 'Неверно указан номер заявки',
        self::WRONG_TICKET_TYPE => 'Неверный тип заявки',
        self::RESERVED_SCHEDULE => 'Слот занят',
        self::WRONG_SCHEDULE_TIME => 'Неверно укзан слот заявки',
        self::UNKNOWN_ERROR => 'Неизвестная ошибка',
    );

    /**
     * @var Agent
     */
    protected $agent;

    protected $contr_id = 0;

    protected $task_type_id = 0;
    protected $agr_id = 0;
    protected $istest = 0;
    protected $extwtype = 0;

    protected $last_error = 0;

    /** @var User */
    protected $user;

    /** @var DBMySQL */
    protected $db;

    /** @var AddressProviderInterface */
    private $addressProvider;

    /** @var EntityManagerInterface */
    private $em;

    /** @var EventDispatcherInterface  */
    private $ed;

    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    /** @var TaskManager */
    private $tm;

    /**
     *
     * @var ScheduleManager
     */
    private $scheduleManager;

    private $executorChooser;

    /**
     * @var IntervalCheckerInterface
     */
    private $intervalChecker;

    /**
     * AbstractIntegrationServer constructor.
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @param EventDispatcherInterface $ed
     * @param TaskManager $tm
     * @param ScheduleManager $scheduleManager
     * @param ExecutorChooserInterface $executorChooser
     * @param AddressProviderInterface $addressProvider
     * @param DBMySQL $db
     * @param IntervalCheckerInterface $intervalChecker
     */
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, EventDispatcherInterface $ed, TaskManager $tm, ScheduleManager $scheduleManager, ExecutorChooserInterface $executorChooser, AddressProviderInterface $addressProvider, DBMySQL $db, IntervalCheckerInterface $intervalChecker)
    {
        $this->em = $em;
        $this->passwordEncoder = $encoder;
        $this->ed = $ed;
        $this->tm = $tm;
        $this->scheduleManager = $scheduleManager;
        $this->executorChooser = $executorChooser;
        $this->addressProvider = $addressProvider;
        $this->db = $db;
        $this->intervalChecker = $intervalChecker;
    }

    protected function doCheckStatus(TechnicalTicketInterface $ticket)
    {
        $task = $ticket->getTask();

        $ret = $this->getRetArray(self::OK);
        $ret['ticket_id'] = $task->getId();
        $ret['status_id'] = $task->getStatus()->getId();
        $ret['status_name'] = $task->getStatus()->getName();
        $ret['status_date'] = $task->getLastStatusDate()->format(\DateTime::ATOM);
        $ret['status_reason'] = 'TBD';

        return $ret;
    }

    protected function getRetArray($code = self::RETCODE_GETLASTERROR)
    {
        if ($code == self::RETCODE_GETLASTERROR) {
            $code = $this->last_error;
        }
        return array('code' => $code, 'message' => $this->getRetmessageByCode($code));
    }

    protected function getRetmessageByCode($code)
    {
        return isset($this->retcodes[$code]) ? $this->retcodes[$code] : 'Неизвестная ошибка';
    }

    /**
     * @param $login
     * @param $password
     * @return array
     */
    protected function doGetAvailableTypes($login, $password)
    {
        if (!$this->logIn($login, $password)) {
            return $this->getRetArray(self::BAD_CREDENTIALS);
        }
        $types = $this->em
            ->getRepository(TaskType::class)
            ->findAvailableByPartner($this->agent->getPartner(), $this->getManagedTaskType());
        $types = array_map(function(TaskType $type) {
            return ['id' => $type->getId(), 'title' => $type->getTitle()];
        }, $types);

        return ['code' => self::OK, 'message' => '', 'taskTypeList' => $types];
    }

    /**
     * @param $login
     * @param $password
     * @return bool
     */
    protected function logIn($login, $password)
    {
        try {
            if (empty($login) || empty($password)) {
                throw new BadCredentialsException();
            }

            $this->user = $this->getEm()->getRepository(User::class)
                ->findOneBy(['login' => $login]);

            if (null === $this->user || !$this->passwordEncoder->isPasswordValid($this->user, $password)) {
                throw new BadCredentialsException();
            }
        } catch (\Exception $e) {
            return false;
        }

        /** @var Agent $agent */
        if (null === $agent = $this->getEm()->getRepository(Agent::class)
                ->findOneBy(['user' => $this->user])
        ) {
            return false;
        }
        $this->agent = $agent;
        $this->contr_id = $agent->getPartner()->getId();
        $this->task_type_id = $agent->getTaskType() instanceof TaskType ? $agent->getTaskType()->getId() : null;
        $this->agr_id = $agent->getAgreement()->getId();

        if ($this->contr_id && $this->agr_id) {
            return true;
        }

        return false;
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getEm(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * @return string
     */
    abstract protected function getManagedTaskType();

    /**
     * @param $login
     * @param $password
     * @param $timeStart
     * @param $timeEnd
     * @param $city
     * @param $street
     * @param $building
     * @param null $taskTypeId
     * @return array
     */
    protected function doGetAvailableSlots($login, $password, $timeStart, $timeEnd, $city, $street, $building, $taskTypeId = null)
    {
        if (!$this->logIn($login, $password)) {
            return $this->getRetArray(self::BAD_CREDENTIALS);
        }

        if (null === $taskTypeId) {
            $taskType = $this->agent->getTaskType();
        }
        else {
            $taskType = $this->em->find(TaskType::class, $taskTypeId);
        }

        $address = $this->findAddress($city, $street, $building);

        $ticket = (new Ticket())
            ->setTaskType($taskType)
            ->setPartner($this->agent->getPartner())
            ->setDom($address);

        $job = (new Task())
            ->setTicket($ticket);

        list($startTime, $endTime) = $this->intervalChecker->checkTimeframeInterval($timeStart, $timeEnd);

        $timeframes = $this->scheduleManager->findAvailableTimeframes($job, $startTime, $endTime);
        $typeInterval =$taskType->getDefaultDuration();

        $slots = [];
        /** @var TimeframeInterface $timeframe */
        foreach ($timeframes as $timeframe) {
            $timeframeStart = $timeframe->getTimeStart();
            while ($timeframeStart < $timeframe->getTimeEnd()) {
                // Define start, end time using default type interval
                $slotStart = clone $timeframeStart;
                $slotEnd = clone $timeframeStart;
                $slotEnd->add($typeInterval);

                if($slotStart < $startTime) {
                    $timeframeStart = $slotEnd;
                    continue;
                }

                // Create slot instance
                /** @var Gfx $slot */
                $slot = (new Gfx($job))
                    ->setTimeStart($slotStart)
                    ->setTimeEnd($slotEnd)
                    ->setTechnician($timeframe->getExecutor());

                // Check for occupied slots
                $occupied = array_filter($timeframe->getSlots(), function (Gfx $busySlot) use($slot) {
                    return $slot->getInterval()->intersect($busySlot->getInterval());
                });

                if (0 === count($occupied)) {
                    $slots[] = $slot;
                }

                $timeframeStart = $slotEnd;
            }
        }

        $aggregate = [];
        foreach ($slots as $slot) {
            $key = $slot->getTimeStart()->format(DateTime::ATOM) . $slot->getTimeEnd()->format(DateTime::ATOM);
            if (isset($aggregate[$key])) {
                ++$aggregate[$key]['count'];
            }
            else {
                $aggregate[$key]['count'] = 1;
                $aggregate[$key]['start'] = $slot->getTimeStart()->format(DateTime::ATOM);
                $aggregate[$key]['end'] = $slot->getTimeEnd()->format(DateTime::ATOM);
            }
        }

        return ['code' => self::OK, 'message' => '', 'slotList' => array_values($aggregate)];
    }

    /**
     * @param $addr_city
     * @param $addr_street
     * @param $addr_building
     * @return ListAddr|null
     */
    protected function findAddress($addr_city, $addr_street, $addr_building)
    {
        return $this->getEm()->getRepository(ListAddr::class)
            ->setAddressProvider($this->getAddressProvider())
            ->findOrCreateByAddressArray([
                'city' => $addr_city,
                'street' => $addr_street,
                'house' => $addr_building,
            ], [
                'count' => 1
            ]);
    }

    /**
     * @return AddressProviderInterface
     */
    protected function getAddressProvider(): AddressProviderInterface
    {
        return $this->addressProvider;
    }

    /**
     * @param $pluginUid
     * @param $addr_gor
     * @param $addr_ul
     * @param $addr_dom
     * @param $addr_pod
     * @param $addr_etazh
     * @param $addr_kv
     * @param $domofon
     * @param $client_type
     * @param $org_name
     * @param $fio
     * @param $phones
     * @param $comment
     * @param $lsnum
     * @param ListRtsubtype|null $workSubtype
     * @param $start_date
     * @param $end_date
     * @param $task_type_id
     * @param $istest
     * @return array
     */
    protected function doCreateTicket($pluginUid, $addr_gor, $addr_ul, $addr_dom, $addr_pod, $addr_etazh, $addr_kv, $domofon, $client_type, $org_name, $fio, $phones, $comment, $lsnum, ListRtsubtype $workSubtype = null, $start_date, $end_date, $task_type_id, $istest)
    {
        $params = [
            'pluginUid' => $pluginUid,
            'addr_gor' => $addr_gor,
            'addr_ul' => $addr_ul,
            'addr_dom' => $addr_dom,
            'addr_pod' => $addr_pod,
            'addr_etazh' => $addr_etazh,
            'addr_kv' => $addr_kv,
            'domofon' => $domofon,
            'client_type' => $client_type,
            'org_name' => $org_name,
            'fio' => $fio,
            'phones' => $phones,
            'comment' => $comment,
            'lsnum' => $lsnum,
            'workSubtype' => $workSubtype,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'task_type_id' => $task_type_id,
            'istest' => $istest,
        ];

        $beforeEvent = new BeforeSoapTaskCreateRequestEvent($params);
        $this->ed->dispatch(BeforeSoapTaskCreateRequestEvent::NAME, $beforeEvent);
        extract($beforeEvent->getParams());

        $timeStart = empty($start_date) ? null : new DateTime($start_date);
        $timeEnd = empty($end_date) ? null : new DateTime($end_date);

        $cmm = sprintf("
        Заявка от агента: %s\n
        Время выполнения: с %s по %s\n
        Адрес клиента: %s, %s этаж, %s подъезд, %s кв., домофон %s\n
        Телефоны: %s\n
        Комментарий клиента:\n%s
        ",
            $this->user->getFio(),
            null !== $timeStart ? $timeStart->format('Y-m-d H:i:s') : '(не указан)',
            null !== $timeEnd ? $timeEnd->format('Y-m-d H:i:s') : '(не указан)',
            "$addr_gor, $addr_ul, $addr_dom",
            $addr_etazh,
            $addr_pod,
            $addr_kv,
            $domofon,
            $phones,
            $comment
        );
        $taskComment = new TaskComment();
        $taskComment->setText($cmm)
            ->setTag('SOAP интеграция. Заведение заявки.');

        $mobtel = $this->getPhone($phones, 0);
        $gortel = $this->getPhone($phones, 1);
        if ($gortel[1] === '9') {
            list($mobtel, $gortel) = [$gortel, $mobtel];
        }

        $this->getEm()->beginTransaction();
        try {
            $listAddr = $this->findAddress($addr_gor, $addr_ul, $addr_dom);

            $sc = null;
            if (null !== $listAddr->getCoordinates()) {
                /** @var ListSc[] $sc */
                $sc = $this->getEm()->getRepository(ListSc::class)->getServiceCentersByPoint(
                    $listAddr->getCoordinates(), [
                        'task_type' => $this->getEm()->getReference(TaskType::class, empty($task_type_id) ? $this->task_type_id : $task_type_id)
                    ]
                );
            }

            $task = $this->getTaskManager()->createTask($pluginUid, [
                'title' => "Заявка на подключение",
                'author_id' => $this->user->getId(),
                'comment' => $cmm,
                'status' => $this->getEm()->getRepository(TaskStatus::class)
                    ->getDefault($pluginUid),
                'addresses' => [$listAddr],
                'comments' => [$taskComment],
            ]);

            $this->getTaskManager()->createTicket($task, [
                'client_type' => !empty($client_type) && $client_type === 2 ? User2::CLIENT_TYPE_YUR : User2::CLIENT_TYPE_PHYS,
                'agreement_id' => (int)$this->agr_id,
                'partner' => $this->getEm()->getReference(ListContr::class, $this->contr_id),
                'task_type' => $this->getEm()->getReference(TaskType::class, empty($task_type_id) ? $this->task_type_id : $task_type_id),
                'external_id' => $lsnum,
                'address' => $listAddr,
                'entrance' => $addr_pod,
                'floor' => $addr_etazh,
                'apartment' => $addr_kv,
                'intercom' => $domofon,
                'service_center' => !empty($sc) ? reset($sc) : null,
                'client_org_name' => $org_name,
                'client_full_name' => $fio,
                'client_phone1' => $mobtel,
                'client_phone2' => $gortel,
                'additional_info' => $comment,
                'slot_begin' => $timeStart,
                'slot_end' => $timeEnd,
                'task_subtype' => !empty($workSubtype) ? $workSubtype->getId() : null,
                'is_test' => (bool)$istest
            ]);

            /**
             * TODO Refactoring needed
             */
            if (null !== $timeStart && null !== $timeEnd) {
                $this->intervalChecker->checkTaskInterval($task, $timeStart, $timeEnd);

                try {
                    $executors = $this->scheduleManager->findAvailableExecutors($task, $timeStart, $timeEnd, $this->executorChooser);
                    if (!empty($executors)) {
                        $this->scheduleManager->createSlot($task, $timeStart, $timeEnd, $executors, $this->user);
                    } else {
                        throw new ExecutorNotFoundException($task, $timeStart, $timeEnd);
                    }
                } catch (Exception $e) {

                }
            }

            $this->getEm()->commit();
        } catch (ScheduleException $e) {
            $this->getEm()->rollback();
            return $this->getRetArray(self::WRONG_SCHEDULE_TIME);
        } catch (Exception $e) {
            $this->getEm()->rollback();
            return $this->getRetArray(self::CANT_CREATE_TICKET);
        }

        $ret = $this->getRetArray(self::OK);
        $ret['ticket_id'] = $task->getId();

        return $ret;
    }

    /**
     * @param $string
     * @param int $index
     * @return null|string
     */
    protected function getPhone($string, $index = 0)
    {
        $string = preg_replace('/[^\d]/s', '', $string);

        if (!preg_match_all("/(?:(?:7(\d{10}))|(?:8(\d{10}))|(\d{10}))/", $string, $phone)) {
            return null;
        }

        if (isset($phone[0][$index])) {
            return '7' . substr($phone[0][$index], -10);
        }

        return null;
    }

    /**
     * @return TaskManager
     */
    protected function getTaskManager(): TaskManager
    {
        return $this->tm;
    }

    /**
     * @param string $city
     * @param string $street
     * @param string $building
     * @return ListSc|null
     */
    protected function findServiceCenter($city, $street, $building)
    {
        $address = $this->findAddress($city, $street, $building);

        $sc = null;
        if (null !== $address->getCoordinates()) {
            /** @var ListSc[] $sc */
            $sc = $this->getEm()->getRepository(ListSc::class)
                ->getServiceCentersByPoint($address->getCoordinates());
        }

        return empty($sc) ? null : $sc[0];
    }

    /**
     * @param TechnicalTicketInterface $ticket
     * @param null $start_date
     * @param null $end_date
     * @param $message
     * @return array
     */
    protected function doAppendComment(TechnicalTicketInterface $ticket, $start_date = null, $end_date = null, $message)
    {
        if (empty($message)) {
            return $this->getCancelRetArray(self::WRONG_DATA);
        }

        $text = "Комментарий контрагента: " . $message;
        if (!empty($start_date) || !empty($end_date)) {
            try {
                $text .= "\n" . sprintf('Новый слот: %s (%s) - %s (%s).',
                        (string)$start_date,
                        (new DateTime($start_date))->format('Y-m-d H:i:s'),
                        (string)$end_date,
                        (new DateTime($end_date))->format('Y-m-d H:i:s')
                    );
            } catch (Exception $e) {
                return $this->getCancelRetArray(self::UNKNOWN_ERROR);
            }
        }

        try {
            $this->getTaskManager()->addComment($ticket->getTask(), (new TaskComment())
                ->setTag('SOAP интеграция. Добавление комментария.')
                ->setText($text));
        } catch (Exception $e) {
            return $this->getCancelRetArray(self::DATABASE_ERROR);
        }

        return $this->getCancelRetArray(self::OK);
    }

    /**
     *
     * @param integer $code
     * @param string $additionalMessage
     * @return array
     */
    protected function getCancelRetArray($code = self::RETCODE_GETLASTERROR, $additionalMessage = null)
    {
        if ($code == self::RETCODE_GETLASTERROR) {
            $code = $this->last_error;
        }

        $message = $this->getRetmessageByCode($code);

        if (null !== $additionalMessage) {
            $message .= " ($additionalMessage)";
        }
        return ['code' => $code, 'message' => $message];
    }

    /**
     * @param TechnicalTicketInterface $ticket
     * @param $comment
     * @return array
     */
    protected function doCancelTicket(TechnicalTicketInterface $ticket, $comment)
    {
        $status = $this->getEm()->getRepository(TaskStatus::class)
            ->findOneByTag('opotkaz', $ticket->getTask()->getPluginUid());

        if (!$ticket->getTask()->getStatus()->getIsDefault()) {
            $ret = $this->getCancelRetArray(self::CANT_CANCEL_TICKET);
            $ret['message'] .= ' (because work already begin)';
            return $ret;
        }

        try {
            $this->getTaskManager()->addComment(
                $ticket->getTask(),
                (new TaskComment())
                    ->setStatus($status)
                    ->setText("Комментарий контрагента: " . $comment)
                    ->setTag("SOAP интеграция. Отмена контрагента.")
            );

            $slot = $ticket->getTask()->getSlot();
            if (null !== $slot) {
                $this->scheduleManager->deleteSlot($slot);
            }
        } catch (Exception $e) {

            $this->getEm()->refresh($ticket);
            if ($status === $ticket->getTask()->getStatus()) {
                $ret = $this->getCancelRetArray(self::UNKNOWN_ERROR);
                $ret['message'] .= ' (TICKET CANCELED but some error occurred; please contact with dispatcher)';
                return $ret;
            }

            return $this->getCancelRetArray(self::CANT_CANCEL_TICKET);
        }

        return $this->getCancelRetArray(self::OK);
    }

    /**
     * @param TechnicalTicketInterface $ticket
     * @param $addr_gor
     * @param $addr_ul
     * @param $addr_dom
     * @param $addr_pod
     * @param $addr_etazh
     * @param $addr_kv
     * @param $domofon
     * @param $client_type
     * @param $org_name
     * @param $fio
     * @param $phone
     * @param $comment
     * @return array
     */
    protected function doUpdateTicket(TechnicalTicketInterface $ticket, $addr_gor, $addr_ul, $addr_dom, $addr_pod, $addr_etazh, $addr_kv, $domofon, $client_type, $org_name, $fio, $phone, $comment)
    {
        $task = $ticket->getTask();

        list($phone1, $phone2) = $this->getPhones($phone);

        $this->getTaskManager()->createHandler($task)->updateTicket($task, [
            'comment' => $comment,
            'comment_tag' => 'SOAP интеграция. Обновление данных заявки.',
            'additional_info' => $comment,
            'address' => $this->findAddress($addr_gor, $addr_ul, $addr_dom),
            'entrance' => $addr_pod,
            'floor' => $addr_etazh,
            'apartment' => $addr_kv,
            'intercom' => $domofon,
            'client_type' => $client_type,
            'client_org_name' => $org_name,
            'client_full_name' => $fio,
            'client_phone1' => $phone1,
            'client_phone2' => $phone2,
        ]);

        if ($task->getId()) {
            $ret = $this->getRetArray(0);
            $ret['ticket_id'] = $task->getId();
        } else {
            $ret = $this->getRetArray(20);
            $ret['ticket_id'] = $task->getId();
        }

        return $ret;
    }

    protected function getPhones($phone)
    {
        return [
            preg_match("/^((7499\d+)|(7495\d+))/", $this->getPhone($phone, 0)) ? (preg_match("/^((7499\d+)|(7495\d+))/", $this->getPhone($phone, 1)) ? $this->getPhone($phone, 0) : $this->getPhone($phone, 1)) : $this->getPhone($phone, 0),
            preg_match("/^((7499\d+)|(7495\d+))/", $this->getPhone($phone, 1)) ? $this->getPhone($phone, 1) : $this->getPhone($phone, 0),
        ];
    }

    /**
     * @param $partner_id
     * @param $lsnum
     * @return array|bool
     */
    protected function checkTaskExisted($partner_id, $lsnum)
    {
        if (!empty($lsnum)) {
            $tickets = $this->getEm()->getRepository(Ticket::class)
                ->findBy([
                    'partner' => $this->getEm()->getReference(ListContr::class, $partner_id),
                    'clnttnum' => $lsnum
                ]);
            if (count($tickets) > 0) {
                $ret = $this->getRetArray(self::ALREADY_CREATED);
                $ret["ticket_id"] = $tickets[0]->getTaskId();
                return $ret;
            }
        }

        return true;
    }

    /**
     * @param string $login
     * @param string $password
     * @param string|null $lsnum
     * @param string|null $ticket_id
     * @return TechnicalTicketInterface|array
     */
    protected function checkTask($login, $password, $lsnum = null, $ticket_id = null)
    {
        // auth
        if (!$this->logIn($login, $password)) {
            return $this->getRetArray(self::BAD_CREDENTIALS);
        }

        // ticket existing by id or lsnum
        if (empty($ticket_id)) {
            $ticket = $this->getEm()->getRepository(Ticket::class)
                ->findOneBy([
                    'clnttnum' => $lsnum,
                    'partner' => $this->getEm()->getReference(ListContr::class, $this->contr_id)
                ]);
        } else {
            $ticket = $this->getEm()->find(Ticket::class, $ticket_id);
        }

        if (null === $ticket) {
            return $this->getCancelRetArray(self::WRONG_TICKET_NUMBER, "Ticket not found by provided id '" . trim($lsnum . ' ' . $ticket_id) . "'");
        }

        // task not closed
        if ($ticket->getTask()->getStatus() === $this->getEm()->getRepository(TaskStatus::class)
                ->findOneByTag('opotkaz', $ticket->getTask()->getPluginUid())
        ) {
            return $this->getCancelRetArray(self::ALREADY_CANCELED, "Ticket already canceled!");
        }

        // task belongs to authenticated user partner
        if ($ticket->getPartner()->getId() !== $this->contr_id) {
            return $this->getCancelRetArray(self::NOT_OWNED_TICKET, "Ticket owner id '{$ticket->getPartner()->getId()}' not equal to user id '{$this->user->getContrUser()}'");
        }

        // task type
        if ($ticket->getTask()->getPluginUid() !== $this->getManagedTaskType()) {
            return $this->getCancelRetArray(self::WRONG_TICKET_TYPE, "Used task is '{$ticket->getTask()->getPluginUid()}' type but method allows only '{$this->getManagedTaskType()}'!");
        }

        return $ticket;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function parseBoolean($value)
    {
        if (
            (is_bool($value) && true === $value)
            || (is_string($value) && ("true" === strtolower($value) || "1" === strtolower($value)))
            || (is_int($value) && 1 === $value)
        ) {
            $value = true;
        } else {
            $value = false;
        }

        return $value;
    }
}
