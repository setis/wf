<?php
/**
 * AgentTicketServer.php
 * 2016-03-22
 * wf
 */

namespace Gorserv\Gerp\IntegrationBundle\Server;

use Exception;
use Gorserv\Gerp\AppBundle\Exception\UnknownAddressLevelException;
use models\ListRtsubtype;
use WF\Task\Model\Formatter\NbnMessageHideExecutorFormatter;
use WF\Task\Model\TechnicalTicketInterface;

/**
 * Class ServicesTicketServer
 * @package \Gorserv\Gerp\IntegrationBundle
 */
class ServicesTicketServer extends AbstractIntegrationServer
{
    const UID = \models\Task::SERVICE;

    /**
     * @param $login
     * @param $password
     * @param $addr_gor
     * @param $addr_ul
     * @param $addr_dom
     * @param $addr_pod
     * @param $addr_etazh
     * @param $addr_kv
     * @param $domofon
     * @param $client_type
     * @param $org_name
     * @param $fio
     * @param $phones
     * @param $comment
     * @param $lsnum
     * @param $req_type
     * @param $start_date
     * @param $end_date
     * @param $work_type
     * @param bool $istest
     * @return array
     */
    public function CreateTicket($login, $password, $addr_gor, $addr_ul, $addr_dom, $addr_pod, $addr_etazh, $addr_kv, $domofon, $client_type, $org_name, $fio, $phones, $comment, $lsnum, $req_type, $start_date, $end_date, $work_type, $istest = false)
    {
        if (!$this->logIn($login, $password)) {
            return $this->getRetArray(self::BAD_CREDENTIALS);
        }

        if (true !== $ret = $this->checkTaskExisted($this->contr_id, $lsnum)) {
            return $ret;
        }

        $istest = $this->parseBoolean($istest);

        $workSubtype = null;
        if (!empty($req_type) && null === $workSubtype = $this->getEm()->getRepository(ListRtsubtype::class)
                ->findOneBy(['tag' => $req_type])
        ) {
            $workSubtype = $this->getEm()->getReference(ListRtsubtype::class, ListRtsubtype::DEFAULT_ID);
        }

        try {
            return $this->doCreateTicket($this->getManagedTaskType(), $addr_gor, $addr_ul, $addr_dom, $addr_pod, $addr_etazh, $addr_kv, $domofon, $client_type, $org_name, $fio, $phones, $comment, $lsnum, $workSubtype, $start_date, $end_date, $work_type, $istest);
        } catch (UnknownAddressLevelException $exception) {
            return $this->getRetArray(self::WRONG_DATA);
        } catch (Exception $exception) {
            return $this->getRetArray(self::CANT_CREATE_TICKET);
        }
    }

    /**
     * @return string
     */
    protected function getManagedTaskType()
    {
        return self::UID;
    }

    /**
     * @param $login
     * @param $password
     * @param null $lsnum
     * @param null $ticket_id
     * @param null $start_date
     * @param null $end_date
     * @param $message
     * @return array
     */
    public function AppendComment($login, $password, $lsnum = null, $ticket_id = null, $start_date = null, $end_date = null, $message)
    {
        $ticket = $this->checkTask($login, $password, $lsnum, $ticket_id);
        if (!($ticket instanceof TechnicalTicketInterface)) {
            return $ticket;
        }

        try {
            return $this->doAppendComment($ticket, $start_date, $end_date, $message);
        } catch (Exception $exception) {
            return $this->getRetArray(self::UNKNOWN_ERROR);
        }
    }

    /**
     * @param $login
     * @param $password
     * @param $lsnum
     * @param $ticket_id
     * @param $comment
     * @return array
     */
    public function CancelTicket($login, $password, $lsnum, $ticket_id, $comment)
    {
        $ticket = $this->checkTask($login, $password, $lsnum, $ticket_id);
        if (!($ticket instanceof TechnicalTicketInterface)) {
            return $ticket;
        }

        try {
            return $this->doCancelTicket($ticket, $comment);
        } catch (Exception $exception) {
            return $this->getRetArray(self::CANT_CANCEL_TICKET);
        }
    }

    /**
     * @param $login
     * @param $password
     * @param $lsnum
     * @param $addr_gor
     * @param $addr_ul
     * @param $addr_dom
     * @param $addr_pod
     * @param $addr_etazh
     * @param $addr_kv
     * @param $domofon
     * @param $client_type
     * @param $org_name
     * @param $fio
     * @param $phone
     * @param $comment
     * @return array
     */
    public function updateTicket($login, $password, $lsnum, $addr_gor, $addr_ul, $addr_dom, $addr_pod, $addr_etazh, $addr_kv, $domofon, $client_type, $org_name, $fio, $phone, $comment)
    {
        $checkResult = $this->checkTask($login, $password, $lsnum);
        if (!($checkResult instanceof TechnicalTicketInterface)) {
            return $checkResult;
        }

        try {
            return $this->doUpdateTicket($checkResult, $addr_gor, $addr_ul, $addr_dom, $addr_pod, $addr_etazh, $addr_kv, $domofon, $client_type, $org_name, $fio, $phone, $comment);
        } catch (UnknownAddressLevelException $exception) {
            return $this->getRetArray(self::WRONG_DATA);
        } catch (Exception $exception) {
            return $this->getRetArray(self::UNKNOWN_ERROR);
        }
    }

    /**
     * @param $login
     * @param $password
     * @param null $lsnum
     * @param null $ticket_id
     * @return array
     */
    public function checkStatus($login, $password, $lsnum = null, $ticket_id = null)
    {
        $ticket = $this->checkTask($login, $password, $lsnum, $ticket_id);
        if (!($ticket instanceof TechnicalTicketInterface)) {
            return $ticket;
        }

        try {
            $result = $this->doCheckStatus($ticket);
            $task = $ticket->getTask();

            if(null !== $task->getSchedule()) {
                $schedule = new \stdClass();
                $schedule->start = $task->getSchedule()->getStartDateTime()->format(\DateTime::ATOM);
                $schedule->end = $task->getSchedule()->getEndDateTime()->format(\DateTime::ATOM);
                $result['schedule'] = $schedule;
            }

            if(null !== $task->getSkpTechCalc()) {
                $linesEnc = [];
                foreach ($task->getSkpTechCalc() as $item) {

                    $orderLine = [];
                    $orderLine['name'] = $item->getWork()->getTitle();
                    $orderLine['price'] = $item->getPrice();
                    $orderLine['quantity'] = $item->getQuantity();
                    $orderLine['unit'] = $item->getWork()->getUnit()->getTitle();

                    $linesEnc[] = new \SoapVar($orderLine, SOAP_ENC_OBJECT, null, null, 'OrderLine');
                }
                $linesEnc['total'] = floatval($task->getTicket()->getOrientPrice());
                $result['order'] = new \SoapVar($linesEnc, SOAP_ENC_OBJECT, NULL, NULL, 'order');
            }

            if(null !== $task->getComments()) {
                foreach ($task->getComments() as $item) {
                    if(null === $item->getStatus()) {
                        continue;
                    }

                    $status = [];
                    $status['status_id'] = $item->getStatus()->getId();
                    $status['status_name'] = $item->getStatus()->getName();
                    $status['status_date'] = $item->getDatetime()->format(\DateTime::ATOM);
                    $status['status_reason'] = strip_tags((new NbnMessageHideExecutorFormatter($item))->getTag());

                    $historyEnc[] = new \SoapVar($status, SOAP_ENC_OBJECT, null, null, 'TaskStatus');

                    $result['history'] = new \SoapVar($historyEnc, SOAP_ENC_OBJECT, NULL, NULL, 'history');
                }
            }



            return $result;
        } catch (Exception $exception) {
            return $this->getRetArray(self::UNKNOWN_ERROR);
        }
    }

    /**
     * @param $login
     * @param $password
     * @return array
     */
    public function getAvailableTypes($login, $password) {
        try {
            return $this->doGetAvailableTypes($login, $password);
        } catch (Exception $exception) {
            return $this->getRetArray(self::UNKNOWN_ERROR);
        }
    }

    public function getAvailableSlots($login, $password, $timeStart, $timeEnd, $city, $street, $building, $taskType)
    {
        try {
            return $this->doGetAvailableSlots($login, $password, $timeStart, $timeEnd, $city, $street, $building, $taskType);
        } catch (Exception $exception) {
            return $this->getRetArray(self::UNKNOWN_ERROR);
        }
    }

}
