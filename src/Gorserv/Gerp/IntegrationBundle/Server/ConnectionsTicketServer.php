<?php
/**
 * ConnectionsTicketServer.php
 * 2016-03-22
 * wf
 */

namespace Gorserv\Gerp\IntegrationBundle\Server;

use Exception;
use Gorserv\Gerp\AppBundle\Exception\UnknownAddressLevelException;
use WF\Task\Model\TechnicalTicketInterface;

/**
 * Class ConnectionsTicketServer
 * @package \Gorserv\Gerp\IntegrationBundle
 */
class ConnectionsTicketServer extends AbstractIntegrationServer
{
    const UID = \models\Task::CONNECTION;

    /**
     * @param $login
     * @param $password
     * @param $addr_gor
     * @param $addr_ul
     * @param $addr_dom
     * @param $addr_pod
     * @param $addr_etazh
     * @param $addr_kv
     * @param $domofon
     * @param $client_type
     * @param $org_name
     * @param $fio
     * @param $phones
     * @param $comment
     * @param $lsnum
     * @param $req_type
     * @param $start_date
     * @param $end_date
     * @param $work_type
     * @param bool $istest
     * @return array
     */
    public function CreateConnectionTicket($login, $password, $addr_gor, $addr_ul, $addr_dom, $addr_pod, $addr_etazh, $addr_kv, $domofon, $client_type, $org_name, $fio, $phones, $comment, $lsnum, $req_type, $start_date, $end_date, $work_type, $istest = false)
    {
        if (!$this->logIn($login, $password)) {
            return $this->getRetArray(self::BAD_CREDENTIALS);
        }

        if (true !== $ret = $this->checkTaskExisted($this->contr_id, $lsnum)) {
            return $ret;
        }

        $istest = $this->parseBoolean($istest);

        try {
            return $this->doCreateTicket($this->getManagedTaskType(), $addr_gor, $addr_ul, $addr_dom, $addr_pod, $addr_etazh, $addr_kv, $domofon, $client_type, $org_name, $fio, $phones, $comment, $lsnum, null, $start_date, $end_date, $work_type, $istest);
        } catch (UnknownAddressLevelException $exception) {
            return $this->getRetArray(self::WRONG_DATA);
        } catch (Exception $exception) {
            return $this->getRetArray(self::CANT_CREATE_TICKET);
        }
    }

    /**
     * @return string
     */
    protected function getManagedTaskType()
    {
        return self::UID;
    }

    /**
     * @param $login
     * @param $password
     * @param null $lsnum
     * @param null $ticket_id
     * @param null $start_date
     * @param null $end_date
     * @param $message
     * @return array
     */
    public function AppendComment($login, $password, $lsnum = null, $ticket_id = null, $start_date = null, $end_date = null, $message)
    {
        $ticket = $this->checkTask($login, $password, $lsnum, $ticket_id);
        if (!($ticket instanceof TechnicalTicketInterface)) {
            return $ticket;
        }

        try {
            return $this->doAppendComment($ticket, $start_date, $end_date, $message);
        } catch (Exception $exception) {
            return $this->getRetArray(self::UNKNOWN_ERROR);
        }
    }

    /**
     * @param $login
     * @param $password
     * @param $lsnum
     * @param $ticket_id
     * @param $comment
     * @return array
     */
    public function CancelConnectionTicket($login, $password, $lsnum, $ticket_id, $comment)
    {
        $ticket = $this->checkTask($login, $password, $lsnum, $ticket_id);
        if (!($ticket instanceof TechnicalTicketInterface)) {
            return $ticket;
        }

        try {
            return $this->doCancelTicket($ticket, $comment);
        } catch (Exception $exception) {
            return $this->getRetArray(self::CANT_CANCEL_TICKET);
        }
    }

    /**
     * @param $login
     * @param $password
     * @param $lsnum
     * @param $addr_gor
     * @param $addr_ul
     * @param $addr_dom
     * @param $addr_pod
     * @param $addr_etazh
     * @param $addr_kv
     * @param $domofon
     * @param $client_type
     * @param $org_name
     * @param $fio
     * @param $phone
     * @param $comment
     * @return array
     */
    public function updateConnectionTicket($login, $password, $lsnum, $addr_gor, $addr_ul, $addr_dom, $addr_pod, $addr_etazh, $addr_kv, $domofon, $client_type, $org_name, $fio, $phone, $comment)
    {
        $ticket = $this->checkTask($login, $password, $lsnum);
        if (!($ticket instanceof TechnicalTicketInterface)) {
            return $ticket;
        }

        try {
            return $this->doUpdateTicket($ticket, $addr_gor, $addr_ul, $addr_dom, $addr_pod, $addr_etazh, $addr_kv, $domofon, $client_type, $org_name, $fio, $phone, $comment);
        } catch (UnknownAddressLevelException $exception) {
            return $this->getRetArray(self::WRONG_DATA);
        } catch (Exception $exception) {
            return $this->getRetArray(self::UNKNOWN_ERROR);
        }
    }

    /**
     * @param $login
     * @param $password
     * @param null $lsnum
     * @param null $ticket_id
     * @return array
     */
    public function checkStatus($login, $password, $lsnum = null, $ticket_id = null)
    {
        $ticket = $this->checkTask($login, $password, $lsnum, $ticket_id);
        if (!($ticket instanceof TechnicalTicketInterface)) {
            return $ticket;
        }

        try {
            return $this->doCheckStatus($ticket);
        } catch (Exception $exception) {
            return $this->getRetArray(self::UNKNOWN_ERROR);
        }
    }

    /**
     * @param $login
     * @param $password
     * @return array
     */
    public function getAvailableTypes($login, $password) {
        try {
            return $this->doGetAvailableTypes($login, $password);
        } catch (Exception $exception) {
            return $this->getRetArray(self::UNKNOWN_ERROR);
        }
    }

    public function getAvailableSlots($login, $password, $timeStart, $timeEnd, $city, $street, $building, $taskType)
    {
        try {
            return $this->doGetAvailableSlots($login, $password, $timeStart, $timeEnd, $city, $street, $building, $taskType);
        } catch (Exception $exception) {
            return $this->getRetArray(self::UNKNOWN_ERROR);
        }
    }

}
