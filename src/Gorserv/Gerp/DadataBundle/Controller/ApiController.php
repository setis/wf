<?php

namespace Gorserv\Gerp\DadataBundle\Controller;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use WF\Address\AddressProviderInterface;

/**
 * @Rest\Prefix("/address_finder")
 * @Rest\NamePrefix("address_recognizer_")
 */
class ApiController extends FOSRestController
{
    /**
     * @Rest\Post("/suggest/address")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postSuggestAddressAction(Request $request)
    {
        $count = $request->get('count', 10);
        $locations_boost = $request->get('locations_boost');
        $query = $request->get('query');

        /** @var AddressProviderInterface $addressProvider */
        $addressProvider = $this->get('wf.address.provider');

        $suggestions = [];
        foreach ($addressProvider->find($query, ['count'=>$count]) as $dadataAddress) {
            $i = count($suggestions);
            $suggestions[$i] = $dadataAddress->getSuggestion();
            $suggestions[$i]['data']['latitude'] = $dadataAddress->getLatitude();
            $suggestions[$i]['data']['longitude'] = $dadataAddress->getLongitude();
        }

        $view = $this->view(['suggestions' => $suggestions], 200)
            ->setFormat('json');

        $context = new Context();
        $context->setGroups(['default']);
        $view->setContext($context);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/status/address")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getStatusAddressAction(Request $request)
    {
        $view = $this->view([], 200)
            ->setFormat('json');

        $context = new Context();
        $context->setGroups(['default']);
        $view->setContext($context);

        return $this->handleView($view);
    }


    /**
     * @Rest\Get("/detectAddressByIp")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getDetectAddressByIpAction(Request $request)
    {
        $view = $this->view([], 200)
            ->setFormat('json');

        $context = new Context();
        $context->setGroups(['default']);
        $view->setContext($context);

        return $this->handleView($view);
    }

}


