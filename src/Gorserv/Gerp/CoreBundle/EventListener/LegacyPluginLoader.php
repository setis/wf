<?php

namespace Gorserv\Gerp\CoreBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use wf;
use function path2;
use classes\UserSession;
use WF\HttpKernel\Security\HttpUserAuthenticatedEvent;

/**
 * Description of LegacyPluginLoader
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class LegacyPluginLoader
{

    /**
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     *
     * @var TokenStorageInterface
     */
    private $tokenStorage;


    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->tokenStorage = $container->get('security.token_storage');
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        global $USER, $PLUGINS;
        wf::$user = $USER = new UserSession($this->tokenStorage);
        wf::init();
        $PLUGINS = [];
        require(path2('/plugins/plugins.inc.php'));

        if (wf::$user->getId()) {
            $authEvent = new HttpUserAuthenticatedEvent($event, $token->getUser());
            $this->container
                ->get('event_dispatcher')
                ->dispatch(HttpUserAuthenticatedEvent::NAME, $authEvent);
        }
    }

}
