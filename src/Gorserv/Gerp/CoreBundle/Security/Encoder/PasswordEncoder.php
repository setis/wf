<?php

namespace Gorserv\Gerp\CoreBundle\Security\Encoder;

use Symfony\Component\Security\Core\Encoder\BasePasswordEncoder;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * Description of PasswordEncoder
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class PasswordEncoder extends BasePasswordEncoder
{
    private $salt;

    public function __construct($salt)
    {
        $this->salt = $salt;
    }

    public function encodePassword($raw, $salt)
    {
        if ($this->isPasswordTooLong($raw)) {
            throw new BadCredentialsException('Invalid password.');
        }

        return hash('sha256', $this->salt . $raw);
    }

    public function isPasswordValid($encoded, $raw, $salt)
    {
        return !$this->isPasswordTooLong($raw)
            && $this->comparePasswords($encoded, hash('sha256', $this->salt . $raw));
    }



}
