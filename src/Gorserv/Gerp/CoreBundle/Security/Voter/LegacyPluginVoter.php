<?php

namespace Gorserv\Gerp\CoreBundle\Security\Voter;

use classes\User;
use Doctrine\ORM\EntityManagerInterface;
use models\User as ModelUser;
use models\UserAccess;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Description of LegacyPluginVoter
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class LegacyPluginVoter extends Voter
{
    /**
     *
     * @var EntityManagerInterface
     */
    private $em;

    private $access;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    protected function supports($attribute, $subject)
    {
        if ($attribute !== 'plugin_read' && $attribute !== 'plugin_write') {
            return false;
        }

        if (!is_string($subject)) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof ModelUser) {
            return false;
        }

        if (empty($this->access)) {
            $this->access = $this->em->createQueryBuilder()
                ->select('ua.access, ua.pluginUid')
                ->from(UserAccess::class, 'ua', 'ua.pluginUid')
                ->where('ua.user = :uid')
                ->setParameter('uid', $user->getId())
                ->getQuery()
                ->useResultCache(true, 120)
                ->getArrayResult();
        }

        if ($attribute === 'plugin_write') {
            $level = 2;
        }
        elseif ($attribute === 'plugin_read') {
            $level = 1;
        }
        else {
            $level = 0;
        }

        return isset($this->access[$subject]) &&
            $this->access[$subject]['access'] >= $level;
    }

}
