<?php

namespace Gorserv\Gerp\CoreBundle\Twig;

use Symfony\Component\Security\Core\User\UserInterface;
use Twig_Extension;
use Twig_SimpleFilter;

/**
 * Description of UserExtension
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class UserExtension extends Twig_Extension
{
    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('short_fio', array($this, 'shortFio')),
            new Twig_SimpleFilter('balance', array($this, 'balance')),
        ];
    }

    public function shortFio(UserInterface $user)
    {
        if (!$user instanceof \models\User) {
            return $user->getUsername();
        }

        $fio = $user->getFio();

        $fioSplitted = explode(' ', $user->getFio());
        $wordsCount = count($fioSplitted);

        if (0 === $wordsCount) {
            return $user->getUsername();
        }

        if (1 === $wordsCount) {
            return $fioSplitted[0];
        }

        if (2 === $wordsCount) {
            return $fioSplitted[0] . ' ' . mb_substr($fioSplitted[1], 0, 1) . '.';
        }

        return $fioSplitted[0] . ' ' . mb_substr($fioSplitted[1], 0, 1) . '. ' . mb_substr($fioSplitted[2], 0, 1) . '.';
    }

    public function balance(UserInterface $user)
    {
        return '';
        if (!$user instanceof \models\User) {
            return $user->getUsername();
        }

        if (!$user->isTechnician()) {
            return '';
        }

        $sum = $user->getBalance()->getSum();
        $currency = $user->getBalance()->getCurrency();

        if ('RUB' === $currency) {
            return $sum . ' <i class="fa fa-rub"></i>';
        }

        return $sum . ' ' . $currency;
    }

    public function getName()
    {
        return 'gerp_core_user_extension';
    }
}
