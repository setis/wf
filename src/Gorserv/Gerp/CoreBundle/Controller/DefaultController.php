<?php

namespace Gorserv\Gerp\CoreBundle\Controller;

use models\Syslog;
use models\User;
use ReflectionClass;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WF\HttpKernel\SyslogControllerListener;

class DefaultController extends Controller
{
    /**
     * @Route("/{plugin_uid}/{plugin_event}", name="legacy_rule_default")
     */
    public function legacyDefaultAction($plugin_uid, $plugin_event, Request $request)
    {
        return $this->handleLegacyRequest($request, $plugin_uid, $plugin_event);
    }

    /**
     * @Route("/{pluginEvent}",
     *  name="legacy_rule_common",
     *  requirements={"pluginEvent"=".+"},
     *  defaults={"pluginEvent" = "desktop"}
     * )
     */
    public function legacyCommonAction($pluginEvent, Request $request)
    {
        $pluginEvents = preg_split('/\//', $pluginEvent, NULL, PREG_SPLIT_NO_EMPTY);

        return $this->handleLegacyRequest($request, $pluginEvents[0],
            count($pluginEvents) > 1 ? $pluginEvents[1] : null);
    }

    private function handleLegacyRequest(Request $request, $pluginUid, $pluginEvent)
    {
        global $plugin, $plugin_event;

        $plugin = $pluginUid;

        $plugin_event = $this->getDefaultEventOrValidate($plugin, $pluginEvent);

        $class = $plugin . '_plugin';
        $refClass = new ReflectionClass($class);
        if (!$refClass->hasMethod($plugin_event)) {
            throw new NotFoundHttpException();
        }

        $method = $refClass->getMethod($plugin_event);
        $plugin = $refClass->newInstanceArgs();
        if ($plugin instanceof ContainerAwareInterface) {
            $plugin->setContainer($this->container);
        }

        if ($this->getParameter('enable_syslog')) {
            $user = $this->getUser();
            $log = SyslogControllerListener::fillSyslog($request, $pluginUid, $pluginEvent ?: '(default)', $user);

            $this->getDoctrine()->getManager()->persist($log);
            $this->getDoctrine()->getManager()->flush();
        }

        ob_start();
        $result = $method->invoke($plugin, $request);
        $out = ob_get_contents();
        ob_end_clean();

        if ($result instanceof Response) {
            return $result;
        }

        $content = $result ? $result : $out;

        return new Response($content);
    }

    private function getDefaultEventOrValidate($plugin, $event = null)
    {
        global $PLUGINS;

        if (!isset($PLUGINS[$plugin]['events']) || !is_array($PLUGINS[$plugin]['events'])) {
            UIErrorWrapped('Ошибка доступа', 403);
        }

        $events = $PLUGINS[$plugin]['events'];

        if (null === $event) {
            reset($events);
            return key($events);
        }

        if (!isset($events[$event])) {
            UIErrorWrapped('Ошибка доступа', 403);
        }

        return $event;
    }
}
