<?php

namespace repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use models\ListContr;
use models\ListSc;
use models\Nomenclature;
use models\Task;
use models\TmcHolderItem;
use models\TmcItem;
use models\User;
use WF\Tmc\Model\TmcHolderInterface;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcPartnerInterface;
use WF\Tmc\Model\TmcWarehouseInterface;
use WF\Tmc\NotExistedSerialException;

/**
 * Description of TmcHolderMoveRepository
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TmcHolderItemRepository extends EntityRepository
{

    /**
     * Возвращает остатки склада.
     * Сгруппированные по номенклатурным позициям и источникам давальческого и, по параметру, серийникам.
     *
     * @param ListSc $wh
     * @param array $params
     *
     * @return TmcHolderItem[]
     */
    public function findAvailableByWh(ListSc $wh, array $params = [])
    {
        $qb = $this->createQueryBuilder('holderItem')
            ->addSelect('partner, holderLink, warehouse, nomenclature, category, property')
            ->addSelect('SUM(holderItem.quantity) total')
            ->join('holderItem.property', 'property')
            ->join('holderItem.holderLink', 'holderLink')
            ->join('holderLink.warehouse', 'warehouse')
            ->leftJoin('property.partner', 'partner')
            ->leftJoin('property.nomenclature', 'nomenclature')
            ->leftJoin('nomenclature.category', 'category')
            ->andWhere('holderLink.warehouse = :warehouse')
            ->groupBy('property.id')
            ->setParameter('warehouse', $wh->getId());

        if(!isset($params['full'])) {
            $qb->having('sum(holderItem.quantity) != 0');
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param TmcWarehouseInterface $owner
     * @param Nomenclature|null $nomenclature
     * @param TmcPartnerInterface|null $partner
     *
     * @return array
     */
    public function findAvailableGroupedItems(TmcWarehouseInterface $owner, Nomenclature $nomenclature = null, TmcPartnerInterface $partner = null)
    {
        $qb = $this->createQueryBuilder('holderItem')
            ->addSelect('property, holderLink, partner, nomenclature')
            ->addSelect('SUM(holderItem.quantity) total')
            ->join('holderItem.property', 'property')
            ->join('holderItem.holderLink', 'holderLink')
            ->join('property.nomenclature', 'nomenclature')
            ->join('property.partner', 'partner')
            ->groupBy('nomenclature', 'partner');

        switch (true) {
            case $owner instanceof ListSc:
                $qb->join('holderLink.warehouse', 'warehouse')
                    ->andWhere('warehouse = :warehouse')
                    ->setParameter('warehouse', $owner)
                    ->addGroupBy('warehouse')
                    ->addSelect('warehouse');
                break;
            case $owner instanceof User:
                $qb->join('holderLink.technician', 'technician')
                    ->andWhere('technician = :technician')
                    ->setParameter('technician', $owner)
                    ->addGroupBy('technician')
                    ->addSelect('technician');
                break;
        }

        if (!empty($nomenclature)) {
            $qb->andWhere('property.nomenclature = :nomenclature')
                ->setParameter('nomenclature', $nomenclature);
        }

        if (!empty($partner)) {
            $qb->andWhere('property.partner = :partner')
                ->setParameter('partner', $partner);
        }

        $qb->having('sum(holderItem.quantity) != 0');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param TmcItem $item
     *
     * @return TmcHolderItem
     */
    public function findItem(TmcItem $item)
    {
        if (empty($item->getProperty()) || empty($item->getProperty()->getId())) {
            return null;
        }

        $qb = $this->createQueryBuilder('holderItem')
            ->addSelect('partner, holderLink, warehouse, nomenclature, category, property')
            ->addSelect('SUM(holderItem.quantity) total')
            ->join('holderItem.property', 'property')
            ->join('holderItem.holderLink', 'holderLink')
            ->leftJoin('holderLink.warehouse', 'warehouse')
            ->leftJoin('property.partner', 'partner')
            ->leftJoin('property.nomenclature', 'nomenclature')
            ->leftJoin('nomenclature.category', 'category')
            ->andWhere('property.serial = :serial')
            ->andWhere('nomenclature = :nomenclature')
            ->andWhere('partner = :partner')
            ->groupBy('property.id')
            ->having('sum(holderItem.quantity) > 0')
            ->setParameter('serial', $item->getSerial())
            ->setParameter('nomenclature', $item->getNomenclature())
            ->setParameter('partner', $item->getPartner());

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Nomenclature $nomenclature
     * @param ListContr $partner
     * @param TmcHolderInterface $owner
     * @param null $serial
     * @return mixed
     */
    public function findItemsByNomenclatureAndPartner(Nomenclature $nomenclature, ListContr $partner, TmcHolderInterface $owner, $serial = null)
    {
        $qb = $this->createQueryBuilder('holderItem')
            ->addSelect('partner, holderLink, warehouse, nomenclature, category, property')
            ->addSelect('SUM(holderItem.quantity) total')
            ->join('holderItem.property', 'property')
            ->join('holderItem.holderLink', 'holderLink')
            ->join('holderLink.warehouse', 'warehouse')
            ->leftJoin('property.partner', 'partner')
            ->leftJoin('property.nomenclature', 'nomenclature')
            ->leftJoin('nomenclature.category', 'category')
            ->andWhere('nomenclature = :nomenclature')
            ->andWhere('partner = :partner')
            ->groupBy('property.id')

            ->having('sum(holderItem.quantity) > 0')

            ->setParameter('nomenclature', $nomenclature)
            ->setParameter('partner', $partner);

        if(!$nomenclature->isMetrical()) {
            $qb->andWhere('property.serial = :serial')
                ->setParameter('serial', $serial);
        }

        switch (true) {
            case $owner instanceof ListSc:
                $qb->andWhere('holderLink.warehouse = :warehouse')
                    ->setParameter('warehouse', $owner);
                break;
            case $owner instanceof User:
                $qb->andWhere('holderLink.technician = :technician')
                    ->setParameter('technician', $owner);
                break;
        }

        $result = $qb->getQuery()->getResult();

        if(1 !== count($result)) {
            throw new NotExistedSerialException($serial);
        }

        return $result[0][0];
    }

    /**
     * Возвращает остатки у техника.
     * Сгруппированные по номенклатурным позициям и источникам давальческого и, по параметру, серийникам.
     *
     * @param User $tech
     *
     * @return array
     */
    public function findAvailableByTech(User $tech)
    {
        $qb = $this->createQueryBuilder('holderItem')
            ->addSelect('partner, holderLink, technician, nomenclature, category, property')
            ->addSelect('SUM(holderItem.quantity) total')
            ->join('holderItem.property', 'property')
            ->join('holderItem.holderLink', 'holderLink')
            ->join('holderLink.technician', 'technician')
            ->leftJoin('property.partner', 'partner')
            ->leftJoin('property.nomenclature', 'nomenclature')
            ->leftJoin('nomenclature.category', 'category')
            ->where('holderLink.technician = :technician')
            ->groupBy('property')
            ->having('sum(holderItem.quantity) > 0')
            ->setParameter('technician', $tech->getId());

        return $qb->getQuery()->getResult();
    }


    public function findAvailableForTechByWh(ListSc $wh)
    {
        $qb = $this->createQueryBuilder('holderItem')
            ->addSelect('p, l, tech, n, c, property')
            ->addSelect('SUM(holderItem.quantity) total')
            ->join('holderItem.holderLink', 'l')
            ->join('l.technician', 'tech')
            ->join('tech.warehouses', 'wh')
            ->join('holderItem.property', 'property')
            ->leftJoin('property.partner', 'p')
            ->leftJoin('property.nomenclature', 'n')
            ->leftJoin('n.category', 'c')
            ->where('wh = :wh')
            ->groupBy('property, tech')
            ->having('sum(holderItem.quantity) != 0')
            ->setParameter('wh', $wh)
        ;

        return $qb->getQuery()->getResult();
    }

    public function getBalance(TmcHolderItem $item)
    {
        $qb = $this->createQueryBuilder('hm')
            ->addSelect('property')
            ->select('SUM(hm.quantity) quantity')
            ->join('hm.property', 'property')
            ->join('hm.holderLink', 'holderLink')
            ->where('property.nomenclature = :nomenclature')
            ->andWhere('property.partner = :partner')
            ->groupBy('property.serial')
            ->setParameter('nomenclature', $item->getNomenclature())
            ->setParameter('partner', $item->getPartner());

        if ($item->getNomenclature()->isMetrical()) {
            $qb->andWhere('property.serial is null');
        } else {
            $qb->andWhere('property.serial = :serial')
                ->setParameter('serial', $item->getProperty()->getSerial());
        }

        switch (true) {
            case ($item->getHolder() instanceOf User):
                $qb->join('holderLink.technician', 'technician')
                    ->andWhere('technician = :tech')
                    ->setParameter('tech', $item->getHolder());
                break;
            case ($item->getHolder() instanceOf ListSc):
                $qb->join('holderLink.warehouse', 'warehouse')
                    ->andWhere('warehouse = :wh')
                    ->setParameter('wh', $item->getHolder());
                break;
        }

        $sum = $qb->getQuery()->getScalarResult();

        $res = 0;
        if (isset($sum[0]) && isset($sum[0]['quantity']))
            return $res = (integer)$sum[0]['quantity'];

        return $res;
    }

    /**
     * @param array $filter
     * @return QueryBuilder
     * @throws \Exception
     */
    public function createReportQueryBuilder(array $filter = [])
    {
        $qb = $this->createQueryBuilder('item')
            ->addSelect('property, nom, partner, move, previousMove, task')
            ->join('item.property', 'property')
            ->join('property.nomenclature', 'nom')
            ->join('property.partner', 'partner')
            ->join('item.move', 'move')
            ->join('move.previousMove', 'previousMove')
            ->join('previousMove.task', 'task');

        if (!empty($filter['task_id'])) {
            $qb->andWhere('task.id = :task_id')
                ->setParameter('task_id', $filter['task_id']);
        }

        if (!empty($filter['dates'])) {
            $qb->andWhere('move.moveDate BETWEEN :from_date AND :to_date')
                ->setParameter('from_date', $filter['dates']['from'])
                ->setParameter('to_date', $filter['dates']['to']);
        }

        if (!empty($filter['partner'])) {
            $qb->andWhere('partner.contrTitle LIKE :partner')
                ->setParameter('partner', '%'.$filter['partner'].'%');
        }

        if (!empty($filter['serial'])) {
            $qb->andWhere('property.serial LIKE :serial')
                ->setParameter('serial', $filter['serial'] . '%');
        }

        if (!empty($filter['nomenclature'])) {
            $qb->andWhere('nom.title LIKE :nomenclature')
                ->setParameter('nomenclature', '%' . $filter['nomenclature'] . '%');
        }

        if (!empty($filter['state'])) {
            $state = $filter['state'];
            $qb->andWhere('previousMove.state = :previousMoveState')
                ->setParameter('previousMoveState', $state)
                ->andWhere('move.state = :state')
                ->setParameter('state', TmcMoveInterface::STATE_MOVED);
        }

        if (!empty($filter['plugin_uid'])) {

            $pluginUid = $filter['plugin_uid'];
            $qb->andWhere('task.pluginUid = :plugin_uid')
                ->setParameter('plugin_uid', $pluginUid);

            switch ($pluginUid) {
                case Task::ACCIDENT:
                case Task::CONNECTION:
                case Task::SERVICE:
                    $qb->join('task.ticket', 'ticket')
                        ->join('ticket.serviceCenter', 'sc')
                        ->leftJoin('ticket.dom', 'dom')
                        ->leftJoin('ticket.partner', 'task_partner')
                        ->addSelect('ticket, sc, dom, task_partner');
                    break;

                case Task::GLOBAL_PROBLEM:
                    $qb->join('task.gp', 'gp')
                        ->leftJoin('task.addressInfo', 'address_info')
                        ->leftJoin('gp.partner', 'task_partner')
                        ->join('gp.serviceCenter', 'sc')
                        ->addSelect('gp, sc, address_info, task_partner');
                    break;

                default:
                    throw new \Exception('Not implemented case for plugin uid ' . $pluginUid);
            }

            if (!empty($filter['warehouse'])) {
                $qb->andWhere('sc.id = :wh')
                    ->setParameter('wh', $filter['warehouse']);
            }

            if (!empty($filter['task_partner'])) {
                $qb->andWhere('task_partner.contrTitle LIKE :task_partner')
                    ->setParameter('task_partner', '%'.$filter['task_partner'].'%');
            }

        }

        return $qb;
    }

    public function createReportTotalsBaseQueryBuilder(array $params)
    {
        $baseQb = $this->createQueryBuilder('hm')
            ->addSelect('property, nomenclature, partner, SUM(hm.quantity)')
            ->join('hm.property', 'property')
            ->join('property.nomenclature', 'nomenclature')
            ->join('property.partner', 'partner')
            ->join('hm.move', 'move')
            ->groupBy('property.nomenclature, property.partner');

        if (empty($params['technician_id'])) {
            if (empty($params['warehouse_id'])) {
                throw new \LogicException("'technician_id' or 'warehouse_id' must be defined");
            }

            $baseQb->join('hm.holderLink', 'l')
                ->where('l.warehouse = :wh')
                ->setParameter('wh', $params['warehouse_id']);
        }
        else {
            $baseQb->join('hm.holderLink', 'l')
                ->where('l.technician = :tech')
                ->setParameter('tech', $params['technician_id']);
        }

        return $baseQb;
    }

    /**
     * @param Task $task
     * @param string $state
     * @return TmcHolderItem[]
     */
    public function findTaskItems(Task $task, $state = \WF\Tmc\Model\TmcMoveInterface::STATE_MOVED)
    {
        $qb = $this->createQueryBuilder('holderItem')
            ->addSelect('partner, move, nomenclature, category, property')
//            ->addSelect('SUM(holderItem.quantity) total')
            ->join('holderItem.property', 'property')
            ->join('holderItem.move', 'move')
            ->join('move.previousMove', 'previousMove')
            ->leftJoin('property.partner', 'partner')
            ->leftJoin('property.nomenclature', 'nomenclature')
            ->leftJoin('nomenclature.category', 'category')
            ->andWhere('move.state = :state')
            ->andWhere('previousMove.state = :prev_move_state')
            ->andWhere('move.task = :task')
//            ->groupBy('move.task')
            ->setParameter('state', $state)
            ->setParameter('task', $task)
//            ->having('sum(holderItem.quantity) != 0')
        ;

        if (TmcMoveInterface::STATE_MOVED !== $state) {
            $qb->setParameter('prev_move_state', $state)
                ->setParameter('state', TmcMoveInterface::STATE_MOVED);
        }

        return $qb->getQuery()->getResult();
    }
}
