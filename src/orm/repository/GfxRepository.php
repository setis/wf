<?php

namespace repository;

use DateTime;
use Doctrine\ORM\EntityRepository;
use models\Gfx;
use models\User;

/**
 * Description of GfxRepository
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class GfxRepository extends EntityRepository
{
    /**
     *
     * @param User $user
     * @param DateTime $timeStart
     * @param DateTime $timeEnd
     * @return Gfx[]
     */
    public function findByExecutorAndTime(User $user, DateTime $timeStart, DateTime $timeEnd)
    {
        $qb = $this->createQueryBuilder('slot')
            ->where('slot.technician = :user');
        $dateExp = $qb->expr()->orX('slot.timeStart > :start AND slot.timeStart < :end', 'slot.timeEnd > :start AND slot.timeEnd < :end', ' slot.timeStart = :start AND slot.timeEnd = :end');
        $qb->andWhere($dateExp)
            ->setParameter('user', $user)
            ->setParameter('start', $timeStart)
            ->setParameter('end', $timeEnd);

        return $qb->getQuery()
            ->getResult();
    }
}
