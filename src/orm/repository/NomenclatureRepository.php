<?php

namespace repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of NomenclatureRepository
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NomenclatureRepository extends EntityRepository
{
    public function findByTitle($title)
    {
        $qb = $this->createQueryBuilder('n')
            ->where('n.title = :t')
            ->setParameter('t', $title)
            ->getQuery()
            ->useResultCache(true, 60);
        
        return $qb->getOneOrNullResult();
    }
    
    public function getCount()
    {
        $qb = $this->createQueryBuilder('n')
            ->select('COUNT(n.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }
}
