<?php
/**
 * Artem
 * 09.05.2016 12:40
 */

namespace repository;


use Doctrine\ORM\EntityRepository;
use models\TaskStatus;

class TaskStatusRepository extends EntityRepository
{
    public function findByTag($tags, $plugin)
    {
        if (!is_array($tags))
            $tags = [$tags];

        $qb = $this->createQueryBuilder('status')
            ->where('status.pluginUid = :type')
//            ->andWhere('status.isActive = :active')
            ->setParameter('type', $plugin)//            ->setParameter('active', true)
        ;

        $expr = [];
        foreach ($tags as $i => $tag) {
            $expr[] = "status.tag like :tag{$i}0 or status.tag like :tag{$i}1 or status.tag like :tag{$i}2 or status.tag like :tag{$i}3";
            $qb->setParameter("tag{$i}0", $tag);
            $qb->setParameter("tag{$i}1", '%,' . $tag . ',%');
            $qb->setParameter("tag{$i}2", '%,' . $tag . '');
            $qb->setParameter("tag{$i}3", $tag . ',%');
        }

        $qb->andWhere(implode(' or ', $expr));

        return $qb->getQuery()
            ->getResult();
    }

    /**
     * @param $tags
     * @param $plugin
     * @return TaskStatus
     */
    public function findOneByTag($tags, $plugin)
    {
        if (!is_array($tags))
            $tags = [$tags];

        $qb = $this->createQueryBuilder('status')
            ->where('status.pluginUid = :type')
            ->setParameter('type', $plugin);

        $expr = [];
        foreach ($tags as $i => $tag) {
            $expr[] = 'status.tag like :tag' . $i;
            $qb->setParameter('tag' . $i, '%' . $tag . '%');
        }

        $qb->andWhere(implode(' or ', $expr));

        return $qb->getQuery()
            ->getSingleResult();
    }

    /**
     * @param $plugin
     * @return TaskStatus
     */
    public function getDefault($plugin)
    {
        $qb = $this->createQueryBuilder('status')
            ->where('status.pluginUid = :uid')
            ->andWhere('status.isActive = :active')
            ->andWhere('status.isDefault = :default')
            ->setParameters([
                'uid' => $plugin,
                'default' => true,
                'active' => true
            ]);

        return $qb->getQuery()
            ->getSingleResult();
    }
}
