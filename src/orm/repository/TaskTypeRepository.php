<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 28.11.16
 * Time: 16:31
 */

namespace repository;

use Doctrine\ORM\EntityRepository;
use models\LinkAgreementTaskType;
use models\ListContr as Partner;
use models\TaskStatus;
use models\TaskType;

class TaskTypeRepository extends EntityRepository
{
    /**
     * @param Partner $partner
     * @param string $pluginUid
     * @return TaskType[]
     */
    public function findAvailableByPartner(Partner $partner, $pluginUid)
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('link, type')
            ->from(LinkAgreementTaskType::class, 'link')
            ->join('link.taskType', 'type')
            ->join('link.agreement', 'agreement')
            ->where('agreement.partner = :partner')
            ->andWhere('agreement.status = :status')
            ->andWhere('type.pluginUid = :plugin_uid')
            ->setParameter('partner', $partner)
            ->setParameter('status', TaskStatus::STATUS_AGREEMENT_ACTIVE)
            ->setParameter('plugin_uid', $pluginUid);

        $links = $qb->getQuery()->getResult();

        return array_map(function (LinkAgreementTaskType $link) {
            return $link->getTaskType();
        }, $links);
    }
}