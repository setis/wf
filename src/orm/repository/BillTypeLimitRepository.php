<?php

namespace repository;

use classes\exceptions\ApplicationException;
use DateTime;
use Doctrine\ORM\EntityRepository;
use models\Bill;
use models\BillType;
use models\BillTypeLimit;

/**
 * BillTypeLimitRepository
 *
 */
class BillTypeLimitRepository extends EntityRepository
{

    /**
     * @param $limits
     * @return int
     * @throws ApplicationException
     */
    private function aggregateLimits($limits)
    {
        $limit = 0;
        if (!empty($limits)) {

            if (!is_array($limits))
                $limits = array($limits);

            foreach ($limits as $model) {
                if ($model instanceof BillTypeLimit)
                    $limit += $model->getMoneyLimit();
                else
                    throw new ApplicationException('Wrong aggregated type!');
            }
        }

        return $limit;
    }

    /**
     * @param \models\BillType $billType
     * @param DateTime $start
     * @param DateTime $end
     * @return array
     */
    public function getLimits(BillType $billType, DateTime $start, DateTime $end)
    {
        $dql = "SELECT
            b
        FROM
            ".BillTypeLimit::class." b
        WHERE
            b.billType = ?1 AND
            b.limitAt >= ?2 AND b.limitAt < ?3
        ";

        $limits = $this->getEntityManager()->createQuery($dql)
            ->setParameter(1, $billType)
            ->setParameter(2, $start->getTimestamp())
            ->setParameter(3, $end->getTimestamp())
            ->getResult();

        return $limits;
    }

    /**
     *
     * @param \models\BillType $billType
     * @param DateTime|null $date
     * @return array
     */
    public function getYearLimits(BillType $billType, DateTime $date = null)
    {
        if (empty($date))
            $date = new DateTime();

        $start = new DateTime($date->format('Y-01-01 00:00:00'));
        $end = new DateTime($date->format('Y-m-t 23:59:00'));

        $limits = $this->getLimits($billType, $start, $end);

        if (count($limits) < 12) {
            for ($i = 12 - count($limits); $i > 0; $i--) {
                $limits[] = new BillTypeLimit();
            }
        }

        return $limits;
    }

    /**
     * @param \models\BillType $billType
     * @param DateTime|null $date
     * @return int
     */
    public function getYearLimit(BillType $billType, DateTime $date = null)
    {
        $limits = $this->getYearLimits($billType, $date);

        $limit = $this->aggregateLimits($limits);

        return $limit;
    }

    /**
     * @param \models\BillType $billType
     * @param DateTime|null $date
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMonthLimit(BillType $billType, DateTime $date = null)
    {
        if (empty($date))
            $date = new DateTime();

        $start = new DateTime($date->format('Y-m-01 00:00:00'));
        $end = new DateTime($date->format('Y-m-t 23:59:00'));

        $limits = $this->getLimits($billType, $start, $end);
        $limit = $this->aggregateLimits($limits);

        return $limit;
    }

    /**
     * @param \models\BillType $billType
     * @param DateTime|null $date
     * @return float
     */
    public function getMonthLimitRest(BillType $billType = null, DateTime $date = null)
    {
        if (empty($billType))
            return 0;

        if (empty($date))
            $date = new DateTime();

        $monthLimit = $this->getMonthLimit($billType, $date);
        /** @var BillRepository $repo */
        $repo = $this->_em->getRepository(Bill::class);

        $wasted = $repo->getSpendingsAtPeriodByType($billType,
            (new DateTime())->setTimestamp(strtotime($date->format('Y-m-01 00:00:00'))),
            (new DateTime())->setTimestamp(strtotime($date->format('Y-m-t 23:59:59'))));

        return (float)$monthLimit - (float)$wasted;
    }

    /**
     * @param \models\BillType|null $billType
     * @param DateTime|null $date
     * @return float|int
     */
    public function getMonthLimitReserved(BillType $billType = null, DateTime $date = null)
    {
        // получаем полную сумму расходов по счетам на период
        // вычиатем из неё фактически потраченную сумму
        // остатётся резерв
        if (empty($billType))
            return 0;

        if (empty($date))
            $date = new DateTime();

        /** @var BillRepository $repo */
        $repo = $this->_em->getRepository(Bill::class);

        $planned = $repo->getPlannedSpendingsAtPeriodByType($billType,
                (new DateTime())->setTimestamp(strtotime($date->format('Y-m-01 00:00:00'))),
                (new DateTime())->setTimestamp(strtotime($date->format('Y-m-t 23:59:59'))));

        $wasted = $repo->getPartialSpendingsAtPeriodByType($billType,
            (new DateTime())->setTimestamp(strtotime($date->format('Y-m-01 00:00:00'))),
            (new DateTime())->setTimestamp(strtotime($date->format('Y-m-t 23:59:59'))));

        return (float)$planned - (float)$wasted;
    }

    /**
     * Нарастающий остаток за год
     * то есть счета в оплату считаются только до текущего месяца
     *
     * @param \models\BillType $billType
     * @param DateTime|null $date
     * @return float
     */
    public function getYearLimitRest(BillType $billType = null, DateTime $date = null)
    {
        if (empty($billType))
            return 0;

        if (empty($date))
            $date = new DateTime();

        $yearLimit = $this->getYearLimit($billType, $date);
        /** @var BillRepository $repo */
        $repo = $this->_em->getRepository(Bill::class);

        $wasted = $repo->getPartialSpendingsAtPeriodByType($billType,
                (new DateTime())->setTimestamp(strtotime($date->format('Y-01-01 00:00:00'))),
                (new DateTime())->setTimestamp(strtotime($date->format('Y-m-t 23:59:59'))));

        return (float)$yearLimit - (float)$wasted;
    }

    /**
     * @param \models\BillType|null $billType
     * @param DateTime|null $date
     * @return float|int
     */
    public function getYearLimitReserved(BillType $billType = null, DateTime $date = null)
    {
        // получаем полную сумму расходов по счетам на период
        // вычиатем из неё фактически потраченную сумму
        // остатётся резерв
        if (empty($billType))
            return 0;

        if (empty($date))
            $date = new DateTime();

        /** @var BillRepository $repo */
        $repo = $this->_em->getRepository(Bill::class);

        $planned = $repo->getPlannedSpendingsAtPeriodByType($billType,
            (new DateTime())->setTimestamp(strtotime($date->format('Y-01-01 00:00:00'))),
            (new DateTime())->setTimestamp(strtotime($date->format('Y-m-t 23:59:59'))));

        $wasted = $repo->getSpendingsAtPeriodByType($billType,
            (new DateTime())->setTimestamp(strtotime($date->format('Y-01-01 00:00:00'))),
            (new DateTime())->setTimestamp(strtotime($date->format('Y-m-t 23:59:59'))));

        return (float)$planned - (float)$wasted;
    }

}
