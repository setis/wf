<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 21.07.16
 * Time: 21:19
 */
namespace repository;


use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Doctrine\ORM\EntityRepository;
use models\ListSc;
use models\TaskType;

class ListScRepository extends EntityRepository
{

    /**
     * @param Point $point
     * @param array $extra
     * @return ListSc[]
     */
    public function getServiceCentersByPoint(Point $point = null, array $extra = [])
    {
        if (null === $point) {
            return [];
        }

        $qb = $this->createQueryBuilder('sc')
            ->andWhere('ST_Contains(sc.area, ST_GeomFromText(:point)) > 0')
            ->setParameter('point', 'Point(' . $point . ')');

        if (isset($extra['task_type']) && $extra['task_type'] instanceof TaskType) {
            $qb
                ->join('sc.technicians', 'technicians')
                ->leftJoin('technicians.taskTypes', 'task_types', 'WITH', 'task_types = :task_type')
                ->setParameter('task_type', $extra['task_type'])
                ->addGroupBy('task_types.id, sc.id')
                ->addOrderBy('task_types.id', 'DESC');
        }

        return $qb->getQuery()
            ->getResult();

    }
}
