<?php

namespace repository;

use Doctrine\ORM\EntityRepository;
use models\ListMaterialCats;

/**
 * Description of ListMaterialCatsRepository
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ListMaterialCatsRepository extends EntityRepository
{
    /**
     *
     * @return ListMaterialCats[]
     */
    public function findAllWithUnit()
    {
        return $this->createQueryBuilder('c')
            ->addSelect('u, nomenclature')
            ->leftJoin('c.unit', 'u')
            ->leftJoin('c.nomenclature', 'nomenclature')
            ->getQuery()
            ->getResult();
    }
}
