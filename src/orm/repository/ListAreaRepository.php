<?php
/**
 * Created by PhpStorm.
 * User: jn
 * Date: 21.07.16
 * Time: 21:19
 */
namespace repository;


use Doctrine\ORM\EntityRepository;
use models\ListArea;

class ListAreaRepository extends EntityRepository
{

    /**
     * @param String $title
     * @return ListArea[]|null
     */
    public function getAreaByTitle($title)
    {
        return $this->createQueryBuilder('area')
            ->select('area.id, area.title, area.disId, area.nbnDepartmentKey')
            ->where('area.title = :title')
            ->setParameter('title', $title)
            ->getQuery()
            ->getResult();
    }
}
