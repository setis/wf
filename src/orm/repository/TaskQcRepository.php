<?php

namespace repository;

use Doctrine\ORM\EntityRepository;
use models\Task;
use models\TaskQc;

/**
 * Description of TaskQcRepository
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskQcRepository extends EntityRepository
{
    /**
     * 
     * @param Task $task
     * @return TaskQc[]
     */
    public function findByTaskWithResults(Task $task)
    {
        $qb = $this->createQueryBuilder('qc')
            ->addSelect('task', 'qcResults', 'question')
            ->join('qc.task', 'task')
            ->leftJoin('task.qcResults', 'qcResults')
            ->leftJoin('qcResults.question', 'question')
            ->where('qc.task = :task')
            ->setParameter('task', $task);
        
        return $qb->getQuery()->getResult();
    }
}
