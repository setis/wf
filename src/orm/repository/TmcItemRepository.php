<?php
/**
 * Artem
 * 31.05.2016 12:51
 */

namespace repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use models\Nomenclature;
use models\Task;
use models\TmcHolderLink;
use models\TmcItem;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcMoveInterface as TMI;
use WF\Tmc\Model\TmcPartnerInterface;
use WF\Tmc\Model\TmcWarehouseInterface;

class TmcItemRepository extends EntityRepository
{

    public function getInstalledByTask(Task $task)
    {
        $qb = $this->createQueryBuilder('item')
            ->addSelect('nomenclature, partner, move, sourceHolder, technician, property')
            ->join('item.move', 'move')
            ->join('item.property', 'property')
            ->leftJoin('property.nomenclature', 'nomenclature')
            ->leftJoin('property.partner', 'partner')
            ->leftJoin('move.sourceHolderLink', 'sourceHolder', Join::WITH, 'sourceHolder.type = :type')
            ->leftJoin('sourceHolder.technician', 'technician')
            ->andWhere('move.state = :state')
            ->andWhere('move.task = :task')
            ->setParameter(':task', $task)
            ->setParameter(':state', TMI::STATE_INSTALLED)
            ->setParameter(':type', TmcHolderLink::TYPE_TECHNICIAN);

        return $qb->getQuery()->getResult();
    }

    public function getUninstalledByTask(Task $task)
    {
        $qb = $this->createQueryBuilder('item')
            ->addSelect('nomenclature, partner, move, property')
            ->join('item.property', 'property')
            ->addSelect('(CASE WHEN (SELECT COUNT(item2.id) FROM ' . TmcItem::class . ' AS item2 '
                . 'JOIN item2.move move2 '
                . 'JOIN item2.property property2 '
                . 'WHERE property2.nomenclature = property.nomenclature '
                . 'AND property2.serial = property.serial '
                . 'AND move2.id > move.id '
                . 'AND move2.state = :state_moved '
                . ') > 1 THEN 1 ELSE 0 END) bro')
            ->leftJoin('item.move', 'move')
            ->leftJoin('property.nomenclature', 'nomenclature')
            ->leftJoin('property.partner', 'partner')
            ->andWhere('move.state = :state')
            ->andWhere('move.task = :task')
            ->setParameter(':state_moved', TMI::STATE_MOVED)
            ->setParameter(':task', $task)
            ->setParameter(':state', TMI::STATE_UNINSTALLED);

        return $qb->getQuery()->getResult();
    }

}
