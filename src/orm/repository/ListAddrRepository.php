<?php

namespace repository;

use Doctrine\ORM\EntityRepository;
use Gorserv\Gerp\AddressBundle\Geometry\PointFactory;
use LogicException;
use models\ListAddr;
use RuntimeException;
use WF\Address\AddressInterface;
use WF\Address\AddressProviderInterface;

/**
 * Description of ListAddrRepository
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ListAddrRepository extends EntityRepository
{
    /**
     *
     * @var AddressProviderInterface
     */
    private $addressProvider;

    public function setAddressProvider(AddressProviderInterface $addressProvider)
    {
        $this->addressProvider = $addressProvider;
        return $this;
    }

    /**
     * Find or try to create ListAddr object by given address data
     *
     * @param array $data Available params are:
     *  - city
     *  - street
     *  - house
     * @param array $options
     *  - count
     * @return ListAddr Returns founded or created object or null
     * @throws RuntimeException
     * @throws LogicException
     */
    public function findOrCreateByAddressArray(array $data, array $options = array())
    {
        if ($this->addressProvider === null) {
            throw new LogicException('Address provider is not set.');
        }

        $addressString = sprintf('%s, %s, %s',
            $data['city'],
            $data['street'],
            $data['house']);

        $addressString = trim($addressString, ', ');

        try {
            $addrArray = $this->addressProvider->find($addressString, $options);
        } catch (\Exception $e) {

        }

        if (empty($addrArray)) {
            // Check this address is already saved
            $addresses = $this->tryToFindByFullAddress($addressString);
            if (!empty($addresses)) {
                return $addresses[0];
            }

            return $this->saveUnrecognizedAddress($data, $addressString);
        }

        return $this->tryToFindInDatabase(current($addrArray), $addressString);
    }

    private function tryToFindByFullAddress($fullAddress)
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.fullAddress = :full_address')
            ->andWhere('a.isNotRecognized = true')
            ->setParameter('full_address', $fullAddress);

        $addresses = $qb->getQuery()->getResult();

        return $addresses;
    }

    private function saveUnrecognizedAddress(array $data, $addressString)
    {
        $address = (new ListAddr())
            ->setFullAddress($addressString)
            ->setIsNotRecognized(true);
        if (!empty($data['house'])) {
            $address->setName($data['house']);
        }
        $this->getEntityManager()->persist($address);
        $this->getEntityManager()->flush($address);

        return $address;
    }

    /**
     * @param AddressInterface $address
     * @param $addressString
     * @return ListAddr
     */
    private function tryToFindInDatabase(AddressInterface $address, $addressString)
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.kladrcode = :kladr')
            ->andWhere('a.isNotRecognized = false')
            ->andWhere('a.coordinates IS NOT NULL')
            ->orderBy('a.name', 'DESC')
            ->addOrderBy('a.althouse', 'ASC')
            ->setParameter('kladr', $address->getCode('kladr'));

        if(!empty($address->getFullBuildingTitle())) {
            $qb->andWhere('a.name = :house or a.althouse = :house')
                ->setParameter('house', $address->getFullBuildingTitle());
        }

        $listAddrs = $qb
            ->getQuery()
            ->getResult();

        if (count($listAddrs) > 0) {
            return $listAddrs[0];
        }

        $listAddr = (new ListAddr())
            ->setName($address->getFullBuildingTitle())
            ->setAlthouse($address->getFullBuildingTitle())
            ->setIsNotRecognized(true)
            ->setKladrcode($address->getCode('kladr'))
            ->setFullAddress((string)$address)
            ->setIsNotRecognized(false)
            ->setComments(sprintf('Добавлен при поиске адреса "%s"', $addressString))
        ;

        $point = PointFactory::createByAddress($address);
        if (null !== $point && $address->getLevel() <= AddressInterface::LEVEL_STREET) {
            $listAddr->setCoordinates($point);
        }

        $this->getEntityManager()->persist($listAddr);
        $this->getEntityManager()->flush();

        return $listAddr;
    }

    /**
     * @param $house
     * @param $kladrCode
     * @return bool
     */
    public function isAddressExists($house, $kladrCode)
    {
        $qb = $this->createQueryBuilder('addr')
            ->select('COUNT(addr.id)')
            ->where('addr.name = :name')
            ->andWhere('addr.kladrcode = :code')
            ->setParameter('name', $house)
            ->setParameter('code', $kladrCode);

        $count = $qb->getQuery()->getSingleScalarResult();

        return $count > 0;
    }
}
