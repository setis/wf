<?php
namespace repository;

use Doctrine\ORM\EntityRepository;
use models\ListSc;
use models\Task;
use models\User;

class UserRepository extends EntityRepository
{
    public function findByFio($fio, $limit = null, $offset = null)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('COUNT(u.id)')
            ->where('u.active > 0');
        if (!empty($fio)) {
            $qb->andWhere('u.fio LIKE :fio')
                ->setParameter('fio', '%' . $fio . '%');
        }
        $count = $qb->getQuery()->getSingleScalarResult();
        $qb->select('u')
            ->addSelect('d')
            ->addSelect('sl')
            ->leftJoin('u.department', 'd')
            ->leftJoin('u.slackUser', 'sl');

        if ($offset !== null) {
            $qb->setFirstResult($offset);
        }
        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }
        /** @var User[] $items */
        $items = $qb->getQuery()->getResult();

        return [
            'total' => $count,
            'items' => $items,
        ];
    }

    public function getCount()
    {
        $qb = $this->createQueryBuilder('u')
            ->select('COUNT(u.id)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param string $email
     * @return User|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findFirstByEmail($email)
    {
//        $this->_em->getConnection()
//            ->getConfiguration()
//            ->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());
        return $this->createQueryBuilder('u')
            ->where('u.email like :email')
            ->setParameter('email', trim($email))
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getTaskAllBoundedUsers(Task $task, $excludeUsers = null, $activeOnly = false )
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u')
            ->from(User::class, 'u')
            ->join('u.userTasks', 'ut')
            ->join('ut.task', 't')
            ->where('t = :task')
            ->setParameter(':task', $task)
            ->orderBy('u.fio', 'ASC');

        if (!empty($excludeUsers)) {
            $qb->andWhere('u not in (:excluded)')
                ->setParameter('excluded', $excludeUsers);
        }

        if ($activeOnly) {
            $qb->andWhere('u.active = true');
        }

        $users = $qb
            ->getQuery()
            ->getResult();
        return $users;
    }

    /**
     * @param Task $task
     * @param User[]|null $excludeUsers
     * @param boolean $activeOnly
     * @return User[]
     */
    public function getTaskExecutors(Task $task, array $excludeUsers = null, $activeOnly = false)
    {
        $qb = $this->createQueryBuilder('u')
            ->join('u.userTasks', 'ut')
            ->join('ut.task', 't')
            ->where('t = :task and ut.ispoln > 0')
            ->setParameter(':task', $task)
            ->orderBy('u.fio', 'ASC');

        if (!empty($excludeUsers)) {
            $qb->andWhere('u not in (:excluded)')->setParameter('excluded', $excludeUsers);
        }

        if ($activeOnly) {
            $qb->andWhere('u.active = true');
        }

        $users = $qb
            ->getQuery()
            ->getResult();

        return $users;
    }

    /**
     * @param Task $task
     * @param User[]|null $excludeUsers
     * @param boolean $activeOnly
     * @return User[]
     */
    public function getTaskBoundedUsers(Task $task, array $excludeUsers = null, $activeOnly = false)
    {
        $qb = $this->createQueryBuilder('u')
            ->join('u.userTasks', 'ut')
            ->join('ut.task', 't')
            ->where('t.id = ?1 and ut.ispoln <= 0')
            ->setParameter(1, $task->getId())
            ->orderBy('u.fio', 'ASC');

        if (!empty($excludeUsers)) {
            $qb->andWhere('u not in (:excluded)')->setParameter('excluded', $excludeUsers);
        }

        if ($activeOnly) {
            $qb->andWhere('u.active = true');
        }

        $users = $qb
            ->getQuery()
            ->getResult();
        return $users;
    }



    /**
     * TODO Artem refactor this
     * @param User|null $user
     * @return string
     */
    public function getAvatar(User $user = null)
    {
        if (!empty($user) && is_file(getcfg("files_path") . "/photos/" . $user->getId() . ".jpg")) {
            return getcfg('http_base') . "gss_files/photos/" . $user->getId() . ".jpg";
        } else {
            return getcfg('http_base') . "gss_files/photos/nophoto.jpg";
        }
    }

    /**
     * Returns chiefs of service center of given user
     * @param User $user Technician
     * @return User[]
     */
    public function findUserChiefs(User $user)
    {
        return $this->createQueryBuilder('chiefs')
            ->join('chiefs.slaveServiceCenters', 'sc')
            ->join('sc.technicians', 't')
            ->where('chiefs.active = true')
            ->andWhere('t.id = :u')
            ->setParameter('u', $user)
            ->getQuery()
            ->getResult();
    }

    /**
     *
     * @param array $ids
     * @return User[]
     */
    public function findByIds(array $ids)
    {
        if (empty($ids)) {
            return [];
        }

        $qb = $this->createQueryBuilder('u');
        return $qb->where($qb->expr()->in('u.id', $ids))
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ListSc $serviceCenter
     * @return User[]
     */
    public function getActiveTechniciansByServiceCenter(ListSc $serviceCenter)
    {
        return $this->createQueryBuilder('u')
            ->addSelect('wh, department')
            ->join('u.warehouses', 'wh')
            ->join('u.department', 'department')
            ->where('wh = :sc')
            ->andWhere('u.active = :active')
            ->setParameters([
                'active'=>true,
                'sc' => $serviceCenter
            ])
            ->orderBy( 'u.fio' )
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ListSc $sc
     * @param array $positions
     * @return array
     */
    public function findActiveByServiceCenterAndPositions(ListSc $sc, array $positions)
    {
        $qb = $this->createQueryBuilder('user');
        $qb ->addSelect('position')
            ->join('user.jobPosition', 'position')
            ->leftJoin('user.warehouses', 'wh')
            ->leftJoin('user.subWarehouses', 'subWh')
            ->leftJoin('user.coordinatedWarehouses', 'coordWh')
            ->leftJoin('user.slaveServiceCenters', 'slaveSc')
            ->where('user.active = true')
            ->andWhere($qb->expr()->in('user.jobPosition', ':positions'))
            ->andWhere($qb->expr()->orX(
                'wh.id = :sc',
                'subWh.id = :sc',
                'coordWh.id = :sc',
                'slaveSc.id = :sc'
            ))
            ->setParameter('positions', $positions)
            ->setParameter('sc', $sc);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $positions
     * @return array
     */
    public function findActiveByPositions(array $positions)
    {
        $qb = $this->createQueryBuilder('user');
        $qb ->addSelect('position')
            ->join('user.jobPosition', 'position')
            ->where('user.active = true')
            ->andWhere($qb->expr()->in('user.jobPosition', ':positions'))
            ->setParameter('positions', $positions);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $departments
     * @return array
     */
    public function findActiveByDepartments(array $departments)
    {
        $qb = $this->createQueryBuilder('user');
        $qb ->addSelect('department')
            ->join('user.department', 'department')
            ->leftJoin('user.warehouses', 'wh')
            ->leftJoin('user.subWarehouses', 'subWh')
            ->leftJoin('user.coordinatedWarehouses', 'coordWh')
            ->leftJoin('user.slaveServiceCenters', 'slaveSc')
            ->where('user.active = true')
            ->andWhere('department.id IN (:depts)')
            ->setParameter('depts', $departments);

        return $qb->getQuery()->getResult();
    }

    public function findActiveByTaskType(\models\TaskType $type)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('link, user')
            ->from(\models\ListEmpl::class, 'link')
            ->join('link.user', 'user')
            ->where('link.wtypeId = :type')
            ->andWhere('u.active = true')
            ->setParameter('type', $type->getId())
            ;

        $result = $qb->getQuery()->getResult();

        return array_map(function(\models\ListEmpl $link) {
            return $link->getUser();
        }, $result);
    }

    /**
     * @return User[]
     */
    public function findTechnicians(array $params = [])
    {
        $qb = $this->createQueryBuilder('user')
            ->join('user.taskTypes', 'task_types')
            ->where('user.active = true');

        if(isset($params['serviceCenters'])) {
            $qb
                ->join('user.warehouses', 'wh')
                ->andWhere('wh in (:wh)')
                ->setParameter('wh', $params['serviceCenters']);
        }

        return $qb->getQuery()->getResult();
    }
}
