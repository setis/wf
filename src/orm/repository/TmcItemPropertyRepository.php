<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 05.09.16
 * Time: 14:44
 */

namespace repository;


use Doctrine\ORM\EntityRepository;
use models\ListContr;
use models\Nomenclature;
use models\TmcItemProperty;

class TmcItemPropertyRepository extends EntityRepository
{

    /**
     * @param \models\Nomenclature $nomenclature
     * @param \models\ListContr $partner
     * @param null $serial
     * @param null $isDefect
     *
     * @return null|\models\TmcItemProperty
     */
    public function findPropertyForNonMetrical(Nomenclature $nomenclature, ListContr $partner, $serial = null, $isDefect = null)
    {
        if ($nomenclature->isMetrical()) {
            throw new \InvalidArgumentException('Nomenclature must be non metrical');
        }

        $qb = $this->createQueryBuilder('property')
            ->where('property.nomenclature = :nomenclature')
            ->andWhere('property.partner = :partner')
            ->setParameter('nomenclature', $nomenclature)
            ->setParameter('partner', $partner);

        if (null !== $serial) {
            $qb->andWhere('property.serial = :serial')
                ->setParameter('serial', $serial);
        }

        if (null !== $isDefect) {
            $qb->andWhere('property.isDefect = :isDefect')
                ->setParameter('isDefect', $isDefect);
        }

        $property = $qb->getQuery()->getOneOrNullResult();

        return $property;
    }

    /**
     * @param Nomenclature $nomenclature
     * @param ListContr $partner
     * @param null $isDefect
     * @return mixed|TmcItemProperty
     */
    public function findPropertyForMetrical(Nomenclature $nomenclature, ListContr $partner, $isDefect = null)
    {
        if (!$nomenclature->isMetrical()) {
            throw new \InvalidArgumentException('Nomenclature must be metrical');
        }

        $qb = $this->createQueryBuilder('property')
            ->where('property.nomenclature = :nomenclature')
            ->andWhere('property.partner = :partner')
            ->setParameter('nomenclature', $nomenclature)
            ->setParameter('partner', $partner)
            ->setMaxResults(1);

        if (null !== $isDefect) {
            $qb->andWhere('property.isDefect = :isDefect')
                ->setParameter('isDefect', $isDefect);
        }

        $property = $qb->getQuery()->getOneOrNullResult();

        return $property;
    }
}
