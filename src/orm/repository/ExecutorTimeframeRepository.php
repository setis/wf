<?php

namespace repository;

use DateTime;
use Doctrine\ORM\EntityRepository;
use LogicException;
use models\ExecutorTimeframe;
use models\Task;
use models\User;

/**
 * Description of ExecutorTimeframeRepository
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ExecutorTimeframeRepository extends EntityRepository
{
    const STRICT_ALL = self::STRICT_GEO | self::STRICT_PARTNER;
    const STRICT_GEO = 1;
    const STRICT_PARTNER = 2;

    /**
     * @param Task $job
     * @param DateTime $start
     * @param DateTime $end
     * @param array $extra
     * @throws LogicException
     * @return ExecutorTimeframe[]
     */
    public function findByJob($job, DateTime $start, DateTime $end, array $extra = [])
    {
        $strict = $extra['strict'] ?? null;
        if (null === $strict) {
            $strict = self::STRICT_ALL;
        }

        $ticket = $job->getTicket();
        if (null === $ticket) {
            throw new LogicException('Ticket is not set in task ' . $job->getId());
        }

        $partner = $ticket->getPartner();
        if (null === $partner) {
            throw new LogicException('Partner is not set in ticket ' . $job->getId());
        }

        $type = $ticket->getTaskType();
        if (null === $type) {
            throw new LogicException('Type is not set in ticket ' . $job->getId());
        }

        $address = $ticket->getDom();
        if (null === $address) {
            throw new LogicException('Address is not set in ticket ' . $job->getId());
        }

        $qb = $this->createQueryBuilder('timeframe')
            ->select('timeframe')
            ->join('timeframe.executor', 'user')
            ->join('user.partners', 'partner')
            ->join('user.taskTypes', 'types')
            ->where('user.active = true')
            ->andWhere(':start <= timeframe.timeEnd AND :end >= timeframe.timeStart')
            ->andWhere('types.id = :type')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('type', $type->getId())
            ->addOrderBy('user.balance.sum', 'DESC')
            ->addOrderBy('user.fio')
        ;

        if (isset($extra['task_area_radius'])) {
            $qb->andWhere('ST_Intersects(user.area, ST_Buffer(ST_GeomFromText(:point), :radius)) = 1')
                ->setParameter('point', 'Point(' . $address->getCoordinates() . ')')
                ->setParameter('radius', $extra['task_area_radius'])
            ;
        }

        if ($strict & self::STRICT_GEO) {
            $qb->andWhere('ST_Contains(user.area, ST_GeomFromText(:point)) > 0')
                ->setParameter('point', 'Point(' . $address->getCoordinates() . ')');
        }

        if ($strict & self::STRICT_PARTNER) {
            $qb->andWhere('partner.id = :partner')
                ->setParameter('partner', $partner->getId());
        }

        return $qb->getQuery()->getResult();
    }

    /**
     *
     * @param User $user
     * @param DateTime $time
     * @return ExecutorTimeframe
     */
    public function findOneByTime(User $user, DateTime $time)
    {
        return $this->createQueryBuilder('frame')
            ->where('frame.executor = :user')
            ->andWhere(':time BETWEEN frame.timeStart AND frame.timeEnd')
            ->setParameter('user', $user)
            ->setParameter('time', $time)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param User $user
     * @param DateTime $start
     * @param DateTime $end
     * @return ExecutorTimeframe[]
     */
    public function findByInterval(User $user, DateTime $start, DateTime $end)
    {
        return $this->createQueryBuilder('frame')
            ->where('frame.executor = :user')
            ->andWhere(':start < frame.timeEnd and :end > frame.timeStart')
            ->setParameter('user', $user)
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();
    }
}
