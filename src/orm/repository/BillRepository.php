<?php
namespace repository;

use DateTime;
use Doctrine\ORM\EntityRepository;
use models\Bill;
use models\BillType;
use models\BusinessUnit;
use models\Task;
use models\TaskStatus;

/**
 * BillRepository
 *
 */
class BillRepository extends EntityRepository
{

    /**
     * @param BillType $billType
     * @param BusinessUnit $businessUnit
     * @param DateTime $start
     * @param DateTime $end
     * @return mixed
     */
    public function getPlannedSpendingsAtPeriodByType(BillType $billType, DateTime $start, DateTime $end)
    {
        $qb = $this->_em->createQueryBuilder();


        /** @var TaskStatusRepository $statusRepo */
        $statusRepo = $this->getEntityManager()->getRepository(TaskStatus::class);

        // Счёт не отменён (40)
        $statuses = [
            $statusRepo->findByTag('otmenen', Task::BILL),
            $statusRepo->findByTag('paid', Task::BILL),
            $statusRepo->find(39 /* Завершено  */),
        ];

        $reserved = $qb->select('SUM(b.amount)')
            ->from(Bill::class, 'b')
            ->join('b.task', 't')
            ->join('b.payer', 'payer')
            ->andWhere(
                $qb->expr()->eq('b.type', '?1')
            )
            ->andWhere('t.status not in (:statuses)')->setParameter('statuses', $statuses)
            ->andWhere(
                $qb->expr()->eq('payer.businessUnit', $billType->getBusinessUnit()->getId())
            )
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->between('b.delayed', '?2', '?3'),
                    $qb->expr()->andX(
                        $qb->expr()->between('b.paymentDate', '?2', '?3'),
                        $qb->expr()->isNull('b.delayed')
                    )
                )
            )
            ->setParameter(1, $billType->getId())
            ->setParameter(2, $start->format('Y-m-d H:i:s'))
            ->setParameter(3, $end->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getSingleScalarResult();

        return $reserved;
    }

    /**
     *
     * @param BillType $billType
     * @param BusinessUnit $businessUnit
     * @param DateTime $start
     * @param DateTime $end
     * @return mixed
     */
    public function getPartialSpendingsAtPeriodByType(BillType $billType, DateTime $start, DateTime $end)
    {
        $qb = $this->_em->createQueryBuilder();

        /** @var TaskStatusRepository $statusRepo */
        $statusRepo = $this->getEntityManager()->getRepository(TaskStatus::class);

        // Счёт не отменён (40)
        $statuses = [
            $statusRepo->findByTag('otmenen', Task::BILL),
            $statusRepo->findByTag('paid', Task::BILL),
            $statusRepo->find(39 /* Завершено  */),
    ];

        $wasted = $qb->select('SUM(p.ammount)')
            ->from(Bill::class, 'b')
            ->join('b.task', 't')
            ->join('b.payments', 'p')
            ->join('b.payer', 'payer')
            ->andWhere('p.type = :type')->setParameter('type', $billType)
            ->andWhere('t.status not in (:statuses)')->setParameter('statuses', $statuses)
            ->andWhere('payer.businessUnit = :bu')->setParameter('bu', $billType->getBusinessUnit()->getId())
            ->andWhere('p.opDate between :start and :end')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getSingleScalarResult();

        return $wasted;

    }

    /**
     *
     * @param BillType $billType
     * @param BusinessUnit $businessUnit
     * @param DateTime $start
     * @param DateTime $end
     * @return mixed
     */
    public function getSpendingsAtPeriodByType(BillType $billType, DateTime $start, DateTime $end)
    {
        $qb = $this->_em->createQueryBuilder();

        /** @var TaskStatusRepository $statusRepo */
        $statusRepo = $this->getEntityManager()->getRepository(TaskStatus::class);

        // Счёт не отменён (40)
        $statuses = [
            $statusRepo->findByTag('otmenen', Task::BILL)
        ];

        $wasted = $qb->select('SUM(p.ammount)')
            ->from(Bill::class, 'b')
            ->join('b.task', 't')
            ->join('b.payments', 'p')
            ->join('b.payer', 'payer')
            ->andWhere('p.type = :type')->setParameter('type', $billType)
            ->andWhere('t.status not in (:statuses)')->setParameter('statuses', $statuses)
            ->andWhere('payer.businessUnit = :bu')->setParameter('bu', $billType->getBusinessUnit()->getId())
            ->andWhere('p.opDate between :start and :end')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getSingleScalarResult();

        return $wasted;

    }
}