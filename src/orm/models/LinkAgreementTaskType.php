<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * LinkAgrWtypes
 *
 * @ORM\Table(name="link_agr_wtypes", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"agr_id", "wtype_id"})
 * })
 * @ORM\Entity
 */
class LinkAgreementTaskType
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_qc", type="boolean")
     */
    private $isQc;

    /**
     * @var TaskType
     *
     * @ORM\ManyToOne(targetEntity="TaskType")
     * @ORM\JoinColumn(name="wtype_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $taskType;

    /**
     * @var Agreement
     *
     * @ORM\ManyToOne(targetEntity="Agreement", inversedBy="workTypes")
     * @ORM\JoinColumn(name="agr_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $agreement;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return TaskType
     */
    public function getTaskType(): TaskType
    {
        return $this->taskType;
    }

    /**
     * @param TaskType $taskType
     * @return LinkAgreementTaskType
     */
    public function setTaskType(TaskType $taskType): LinkAgreementTaskType
    {
        $this->taskType = $taskType;
        return $this;
    }

    /**
     * @return Agreement
     */
    public function getAgreement(): Agreement
    {
        return $this->agreement;
    }

    /**
     * @param Agreement $agreement
     * @return LinkAgreementTaskType
     */
    public function setAgreement(Agreement $agreement): LinkAgreementTaskType
    {
        $this->agreement = $agreement;
        return $this;
    }

    /**
     * @return int
     */
    public function isQc(): int
    {
        return $this->isQc;
    }

    /**
     * @param int $isQc
     * @return LinkAgreementTaskType
     */
    public function setQc(int $isQc): LinkAgreementTaskType
    {
        $this->isQc = $isQc;
        return $this;
    }



}

