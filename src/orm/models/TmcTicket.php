<?php

namespace models;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TmcTicket
 *
 * @ORM\Table(name="tmc_ticket", indexes={
 *     @ORM\Index(columns={"task_id"}),
 *     @ORM\Index(columns={"tmc_tech_sc_id"}),
 *     @ORM\Index(columns={"tech_id"}),
 *     @ORM\Index(columns={"user_id"})
 * })
 * @ORM\Entity
 */
class TmcTicket
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $count;

    /**
     * @var integer
     *
     * @ORM\Column(name="isrent", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isrent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ledit", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $ledit;

    /**
     * @var integer
     *
     * @ORM\Column(name="isreturn", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isreturn;

    /**
     * @var string
     *
     * @ORM\Column(name="desc", type="text", length=16777215, precision=0, scale=0, nullable=true, unique=false)
     */
    private $desc;

    /**
     * @var string
     *
     * @ORM\Column(name="cmm", type="text", length=16777215, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cmm;

    /**
     * @var integer
     *
     * @ORM\Column(name="reason_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $reasonId;

    /**
     * @var integer
     *
     * @ORM\Column(name="comment_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $commentId;

    /**
     * @var \models\Task
     *
     * @ORM\ManyToOne(targetEntity="models\Task", inversedBy="tmcTickets")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $task;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User", inversedBy="tmcTechTickets")
     * @ORM\JoinColumn(name="tech_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $tech;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User", inversedBy="tmcUserTickets")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @var Collection
     * Если включить эту связь с FK то вклочья разрывает всё работу с ТМЦ в легаси
     *
     * #ORM\ManyToOne(targetEntity="models\TmcScTech", inversedBy="tmcTickets")
     * #ORM\JoinColumn(name="tmc_tech_sc_id", referencedColumnName="id")
     */

    /**
     * @var integer
     *
     * @ORM\Column(name="tmc_tech_sc_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tmcScTechId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set taskId
     *
     * @param integer $taskId
     *
     * @return TmcTicket
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return TmcTicket
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set techId
     *
     * @param integer $techId
     *
     * @return TmcTicket
     */
    public function setTechId($techId)
    {
        $this->techId = $techId;

        return $this;
    }

    /**
     * Get techId
     *
     * @return integer
     */
    public function getTechId()
    {
        return $this->techId;
    }

    /**
     * Set isrent
     *
     * @param integer $isrent
     *
     * @return TmcTicket
     */
    public function setIsrent($isrent)
    {
        $this->isrent = $isrent;

        return $this;
    }

    /**
     * Get isrent
     *
     * @return integer
     */
    public function getIsrent()
    {
        return $this->isrent;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return TmcTicket
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set ledit
     *
     * @param \DateTime $ledit
     *
     * @return TmcTicket
     */
    public function setLedit($ledit)
    {
        $this->ledit = $ledit;

        return $this;
    }

    /**
     * Get ledit
     *
     * @return \DateTime
     */
    public function getLedit()
    {
        return $this->ledit;
    }

    /**
     * Set isreturn
     *
     * @param integer $isreturn
     *
     * @return TmcTicket
     */
    public function setIsreturn($isreturn)
    {
        $this->isreturn = $isreturn;

        return $this;
    }

    /**
     * Get isreturn
     *
     * @return integer
     */
    public function getIsreturn()
    {
        return $this->isreturn;
    }

    /**
     * Set desc
     *
     * @param string $desc
     *
     * @return TmcTicket
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set cmm
     *
     * @param string $cmm
     *
     * @return TmcTicket
     */
    public function setCmm($cmm)
    {
        $this->cmm = $cmm;

        return $this;
    }

    /**
     * Get cmm
     *
     * @return string
     */
    public function getCmm()
    {
        return $this->cmm;
    }

    /**
     * Set reasonId
     *
     * @param integer $reasonId
     *
     * @return TmcTicket
     */
    public function setReasonId($reasonId)
    {
        $this->reasonId = $reasonId;

        return $this;
    }

    /**
     * Get reasonId
     *
     * @return integer
     */
    public function getReasonId()
    {
        return $this->reasonId;
    }

    /**
     * Set commentId
     *
     * @param integer $commentId
     *
     * @return TmcTicket
     */
    public function setCommentId($commentId)
    {
        $this->commentId = $commentId;

        return $this;
    }

    /**
     * Get commentId
     *
     * @return integer
     */
    public function getCommentId()
    {
        return $this->commentId;
    }

    /**
     * @return Collection
     */
    public function getTmcScTech()
    {
        return $this->tmcScTech;
    }

    /**
     * @param Collection $tmcScTech
     * @return TmcTicket
     */
    public function setTmcScTech($tmcScTech)
    {
        $this->tmcScTech = $tmcScTech;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return TmcTicket
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getTech()
    {
        return $this->tech;
    }

    /**
     * @param User $tech
     * @return TmcTicket
     */
    public function setTech(User $tech)
    {
        $this->tech = $tech;
        return $this;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param Task $task
     * @return TmcTicket
     */
    public function setTask(Task $task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @param int $tmcScTechId
     * @return TmcTicket
     */
    public function setTmcScTechId($tmcScTechId)
    {
        $this->tmcScTechId = $tmcScTechId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTmcScTechId()
    {
        return $this->tmcScTechId;
    }

}

