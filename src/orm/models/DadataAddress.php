<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Address
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 *
 * @ORM\Table(name="dadata_address", indexes={
 *     @ORM\Index(columns={"raw"}),
 *     @ORM\Index(columns={"value"}),
 *     @ORM\Index(columns={"unrestricted_value"}),
 *     @ORM\Index(columns={"fias_id"}),
 *     @ORM\Index(columns={"kladr_id"})
 * })
 * @ORM\Entity
 */
class DadataAddress
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var string
     * @ORM\Column(name="raw", type="string", length=255)
     */
    private $raw;

    /**
     *
     * @var string
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    /**
     *
     * @var string
     * @ORM\Column(name="unrestricted_value", type="string", length=255)
     */
    private $unrestricted_value;

    /**
     *
     * @var string
     * @ORM\Column(name="postal_code", type="string", length=50, nullable=true)
     */
    private $postal_code;

    /**
     *
     * @var string
     * @ORM\Column(name="postal_box", type="string", length=50, nullable=true)
     */
    private $postal_box;

    /**
     *
     * @var string
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     *
     * @var string
     * @ORM\Column(name="region_with_type", type="string", length=255, nullable=true)
     */
    private $region_with_type;

    /**
     *
     * @var string
     * @ORM\Column(name="region_type", type="string", length=255, nullable=true)
     */
    private $region_type;

    /**
     *
     * @var string
     * @ORM\Column(name="region_type_full", type="string", length=255, nullable=true)
     */
    private $region_type_full;

    /**
     *
     * @var string
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     *
     * @var string
     * @ORM\Column(name="area_with_type", type="string", length=255, nullable=true)
     */
    private $area_with_type;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $area_type;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $area_type_full;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $area;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city_with_type;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city_type;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city_type_full;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city_area;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city_district;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $settlement_with_type;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $settlement_type;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $settlement_type_full;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $settlement;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street_with_type;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street_type;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street_type_full;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $house_type;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $house_type_full;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $house;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $block_type;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $block_type_full;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $block;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fias_id;

    /**
     *
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $fias_level;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $kladr_id;

    /**
     *
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $capital_marker;

    /**
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $geo_lat;

    /**
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $geo_lon;

    /**
     *
     * @var array
     * @ORM\Column(name="raw_data", type="json_array")
     */
    private $raw_data;

    /**
     * @return array
     */
    public function toArray()
    {
        $refClass = new \ReflectionClass($this);
        $data = [];
        /* @var $property \ReflectionProperty */
        foreach ($refClass->getProperties() as $property) {
            $property->setAccessible(true);
            $data[$property->getName()] = $property->getValue($this);
        }

        return $data;
    }

    /**
     *
     * @param array $suggestion
     * @return DadataAddress
     */
    public static function createFromDadata(array $suggestion)
    {
        $address = new DadataAddress();

        $address->setValue($suggestion['value']);
        $address->setUnrestrictedValue($suggestion['unrestricted_value']);

        $refClass = new \ReflectionClass($address);
        foreach ($suggestion['data'] as $name => $value) {
            if (!$refClass->hasProperty($name)) {
                continue;
            }

            $prop = $refClass->getProperty($name);
            $prop->setAccessible(true);
            $prop->setValue($address, $value);
        }

        $address->setRawData($suggestion);

        return $address;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * @param $raw
     * @return $this
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnrestrictedValue()
    {
        return $this->unrestricted_value;
    }

    /**
     * @param $unrestricted_value
     * @return $this
     */
    public function setUnrestrictedValue($unrestricted_value)
    {
        $this->unrestricted_value = $unrestricted_value;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * @param string $postal_code
     * @return DadataAddress
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostalBox()
    {
        return $this->postal_box;
    }

    /**
     * @param string $postal_box
     * @return DadataAddress
     */
    public function setPostalBox($postal_box)
    {
        $this->postal_box = $postal_box;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return DadataAddress
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegionWithType()
    {
        return $this->region_with_type;
    }

    /**
     * @param string $region_with_type
     * @return DadataAddress
     */
    public function setRegionWithType($region_with_type)
    {
        $this->region_with_type = $region_with_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegionType()
    {
        return $this->region_type;
    }

    /**
     * @param string $region_type
     * @return DadataAddress
     */
    public function setRegionType($region_type)
    {
        $this->region_type = $region_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegionTypeFull()
    {
        return $this->region_type_full;
    }

    /**
     * @param string $region_type_full
     * @return DadataAddress
     */
    public function setRegionTypeFull($region_type_full)
    {
        $this->region_type_full = $region_type_full;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     * @return DadataAddress
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string
     */
    public function getAreaWithType()
    {
        return $this->area_with_type;
    }

    /**
     * @param string $area_with_type
     * @return DadataAddress
     */
    public function setAreaWithType($area_with_type)
    {
        $this->area_with_type = $area_with_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getAreaType()
    {
        return $this->area_type;
    }

    /**
     * @param string $area_type
     * @return DadataAddress
     */
    public function setAreaType($area_type)
    {
        $this->area_type = $area_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getAreaTypeFull()
    {
        return $this->area_type_full;
    }

    /**
     * @param string $area_type_full
     * @return DadataAddress
     */
    public function setAreaTypeFull($area_type_full)
    {
        $this->area_type_full = $area_type_full;
        return $this;
    }

    /**
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param string $area
     * @return DadataAddress
     */
    public function setArea($area)
    {
        $this->area = $area;
        return $this;
    }

    /**
     * @return string
     */
    public function getCityWithType()
    {
        return $this->city_with_type;
    }

    /**
     * @param string $city_with_type
     * @return DadataAddress
     */
    public function setCityWithType($city_with_type)
    {
        $this->city_with_type = $city_with_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getCityType()
    {
        return $this->city_type;
    }

    /**
     * @param string $city_type
     * @return DadataAddress
     */
    public function setCityType($city_type)
    {
        $this->city_type = $city_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getCityTypeFull()
    {
        return $this->city_type_full;
    }

    /**
     * @param string $city_type_full
     * @return DadataAddress
     */
    public function setCityTypeFull($city_type_full)
    {
        $this->city_type_full = $city_type_full;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return DadataAddress
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCityArea()
    {
        return $this->city_area;
    }

    /**
     * @param string $city_area
     * @return DadataAddress
     */
    public function setCityArea($city_area)
    {
        $this->city_area = $city_area;
        return $this;
    }

    /**
     * @return string
     */
    public function getCityDistrict()
    {
        return $this->city_district;
    }

    /**
     * @param string $city_district
     * @return DadataAddress
     */
    public function setCityDistrict($city_district)
    {
        $this->city_district = $city_district;
        return $this;
    }

    /**
     * @return string
     */
    public function getSettlementWithType()
    {
        return $this->settlement_with_type;
    }

    /**
     * @param string $settlement_with_type
     * @return DadataAddress
     */
    public function setSettlementWithType($settlement_with_type)
    {
        $this->settlement_with_type = $settlement_with_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getSettlementType()
    {
        return $this->settlement_type;
    }

    /**
     * @param string $settlement_type
     * @return DadataAddress
     */
    public function setSettlementType($settlement_type)
    {
        $this->settlement_type = $settlement_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getSettlementTypeFull()
    {
        return $this->settlement_type_full;
    }

    /**
     * @param string $settlement_type_full
     * @return DadataAddress
     */
    public function setSettlementTypeFull($settlement_type_full)
    {
        $this->settlement_type_full = $settlement_type_full;
        return $this;
    }

    /**
     * @return string
     */
    public function getSettlement()
    {
        return $this->settlement;
    }

    /**
     * @param string $settlement
     * @return DadataAddress
     */
    public function setSettlement($settlement)
    {
        $this->settlement = $settlement;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreetWithType()
    {
        return $this->street_with_type;
    }

    /**
     * @param string $street_with_type
     * @return DadataAddress
     */
    public function setStreetWithType($street_with_type)
    {
        $this->street_with_type = $street_with_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreetType()
    {
        return $this->street_type;
    }

    /**
     * @param string $street_type
     * @return DadataAddress
     */
    public function setStreetType($street_type)
    {
        $this->street_type = $street_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreetTypeFull()
    {
        return $this->street_type_full;
    }

    /**
     * @param string $street_type_full
     * @return DadataAddress
     */
    public function setStreetTypeFull($street_type_full)
    {
        $this->street_type_full = $street_type_full;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return DadataAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getHouseType()
    {
        return $this->house_type;
    }

    /**
     * @param string $house_type
     * @return DadataAddress
     */
    public function setHouseType($house_type)
    {
        $this->house_type = $house_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getHouseTypeFull()
    {
        return $this->house_type_full;
    }

    /**
     * @param string $house_type_full
     * @return DadataAddress
     */
    public function setHouseTypeFull($house_type_full)
    {
        $this->house_type_full = $house_type_full;
        return $this;
    }

    /**
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @param string $house
     * @return DadataAddress
     */
    public function setHouse($house)
    {
        $this->house = $house;
        return $this;
    }

    /**
     * @return string
     */
    public function getBlockType()
    {
        return $this->block_type;
    }

    /**
     * @param string $block_type
     * @return DadataAddress
     */
    public function setBlockType($block_type)
    {
        $this->block_type = $block_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getBlockTypeFull()
    {
        return $this->block_type_full;
    }

    /**
     * @param string $block_type_full
     * @return DadataAddress
     */
    public function setBlockTypeFull($block_type_full)
    {
        $this->block_type_full = $block_type_full;
        return $this;
    }

    /**
     * @return string
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @param string $block
     * @return DadataAddress
     */
    public function setBlock($block)
    {
        $this->block = $block;
        return $this;
    }

    /**
     * @return string
     */
    public function getFiasId()
    {
        return $this->fias_id;
    }

    /**
     * @param string $fias_id
     * @return DadataAddress
     */
    public function setFiasId($fias_id)
    {
        $this->fias_id = $fias_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getFiasLevel()
    {
        return $this->fias_level;
    }

    /**
     * @param int $fias_level
     * @return DadataAddress
     */
    public function setFiasLevel($fias_level)
    {
        $this->fias_level = $fias_level;
        return $this;
    }

    /**
     * @return string
     */
    public function getKladrId()
    {
        return $this->kladr_id;
    }

    /**
     * @param string $kladr_id
     * @return DadataAddress
     */
    public function setKladrId($kladr_id)
    {
        $this->kladr_id = $kladr_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCapitalMarker()
    {
        return $this->capital_marker;
    }

    /**
     * @param int $capital_marker
     * @return DadataAddress
     */
    public function setCapitalMarker($capital_marker)
    {
        $this->capital_marker = $capital_marker;
        return $this;
    }

    /**
     * @return float
     */
    public function getGeoLat()
    {
        return $this->geo_lat;
    }

    /**
     * @param float $geo_lat
     * @return DadataAddress
     */
    public function setGeoLat($geo_lat)
    {
        $this->geo_lat = $geo_lat;
        return $this;
    }

    /**
     * @return float
     */
    public function getGeoLon()
    {
        return $this->geo_lon;
    }

    /**
     * @param float $geo_lon
     * @return DadataAddress
     */
    public function setGeoLon($geo_lon)
    {
        $this->geo_lon = $geo_lon;
        return $this;
    }

    /**
     *
     * @return type
     */
    public function getRawData()
    {
        return $this->raw_data;
    }

    /**
     * @param array $rawData
     * @return DadataAddress
     */
    public function setRawData(array $rawData)
    {
        $this->raw_data = $rawData;
        return $this;
    }



}
