<?php

namespace models;

use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ListAddr
 *
 * @ORM\Table(name="list_addr", indexes={
 *     @ORM\Index(columns={"name", "kladrcode"}),
 *     @ORM\Index(columns={"kladrcode"})
 * })
 * @ORM\Entity(repositoryClass="repository\ListAddrRepository")
 */
class ListAddr
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="kladrcode", type="string", length=30, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $kladrcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="floorcount", type="integer", nullable=true)
     */
    private $floorcount;

    /**
     * @var integer
     *
     * @ORM\Column(name="flatcount", type="integer", nullable=true)
     */
    private $flatcount;

    /**
     * @var integer
     *
     * @ORM\Column(name="entr_count", type="integer", nullable=true)
     */
    private $entrCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="sc_id", type="integer", nullable=true)
     */
    private $scId;

    /**
     * @var integer
     *
     * @ORM\Column(name="area_id", type="integer", nullable=true)
     */
    private $areaId = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", nullable=true)
     */
    private $comments;

    /**
     * @var string
     *
     * @ORM\Column(name="althouse", type="string", length=30, nullable=true)
     */
    private $althouse;

    /**
     * @var ListOds
     *
     * @ORM\ManyToOne(targetEntity="models\ListOds")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ods_id", referencedColumnName="id", nullable=true)
     * })
     * @Serializer\Groups({"default"})
     */
    private $ods;

    /**
     * @var string
     *
     * @ORM\Column(name="full_address", type="string", length=255, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $fullAddress;

    /**
     * @var Point
     *
     * @ORM\Column(name="coordinates", type="point", nullable=true)
     * @Serializer\Groups({"default"})
     * @Serializer\Accessor(getter="getSerializedCoordinates")
     */
    private $coordinates;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="is_not_recognized", type="boolean")
     */
    private $isNotRecognized;

    /**
     *
     * @var ListSc
     *
     * @ORM\ManyToOne(targetEntity="ListSc")
     * @ORM\JoinColumn(name="sc_id", referencedColumnName="id", nullable=true)
     *
     */
    private $serviceCenter;

    /**
     * ListAddr constructor.
     */
    public function __construct()
    {
        $this->isNotRecognized = false;
    }

        /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ListAddr
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get kladrcode
     *
     * @return string
     */
    public function getKladrcode()
    {
        return $this->kladrcode;
    }

    /**
     * Set kladrcode
     *
     * @param string $kladrcode
     *
     * @return ListAddr
     */
    public function setKladrcode($kladrcode)
    {
        $this->kladrcode = $kladrcode;

        return $this;
    }

    /**
     * Get floorcount
     *
     * @return integer
     */
    public function getFloorcount()
    {
        return $this->floorcount;
    }

    /**
     * Set floorcount
     *
     * @param integer $floorcount
     *
     * @return ListAddr
     */
    public function setFloorcount($floorcount)
    {
        $this->floorcount = $floorcount;

        return $this;
    }

    /**
     * Get flatcount
     *
     * @return integer
     */
    public function getFlatcount()
    {
        return $this->flatcount;
    }

    /**
     * Set flatcount
     *
     * @param integer $flatcount
     *
     * @return ListAddr
     */
    public function setFlatcount($flatcount)
    {
        $this->flatcount = $flatcount;

        return $this;
    }

    /**
     * Get entrCount
     *
     * @return integer
     */
    public function getEntrCount()
    {
        return $this->entrCount;
    }

    /**
     * Set entrCount
     *
     * @param integer $entrCount
     *
     * @return ListAddr
     */
    public function setEntrCount($entrCount)
    {
        $this->entrCount = $entrCount;

        return $this;
    }

    /**
     * Get scId
     *
     * @return integer
     */
    public function getScId()
    {
        return $this->scId;
    }

    /**
     * Set scId
     *
     * @param integer $scId
     *
     * @return ListAddr
     */
    public function setScId($scId)
    {
        $this->scId = $scId;

        return $this;
    }

    /**
     * Get areaId
     *
     * @return integer
     */
    public function getAreaId()
    {
        return $this->areaId;
    }

    /**
     * Set areaId
     *
     * @param integer $areaId
     *
     * @return ListAddr
     */
    public function setAreaId($areaId)
    {
        $this->areaId = $areaId;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return ListAddr
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get althouse
     *
     * @return string
     */
    public function getAlthouse()
    {
        return $this->althouse;
    }

    /**
     * Set althouse
     *
     * @param string $althouse
     *
     * @return ListAddr
     */
    public function setAlthouse($althouse)
    {
        $this->althouse = $althouse;

        return $this;
    }

    /**
     * @return ListOds
     */
    public function getOds()
    {
        return $this->ods;
    }

    /**
     * @param ListOds|null $ods
     * @return $this
     */
    public function setOds(ListOds $ods = null)
    {
        $this->ods = $ods;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNotRecognized()
    {
        return $this->isNotRecognized;
    }

    /**
     * @return bool
     */
    public function isRecognized()
    {
        return !$this->isNotRecognized;
    }

    /**
     * @param $isNotRecognized
     * @return $this
     */
    public function setIsNotRecognized($isNotRecognized)
    {
        $this->isNotRecognized = $isNotRecognized;
        return $this;
    }

    /**
     * @return ListSc
     */
    public function getServiceCenter()
    {
        return $this->serviceCenter;
    }

    /**
     * @param ListSc $serviceCenter
     * @return $this
     */
    public function setServiceCenter(ListSc $serviceCenter)
    {
        $this->serviceCenter = $serviceCenter;
        return $this;
    }

    /**
     * @return array
     */
    public function getSerializedArea()
    {
        if($this->getCoordinates())
            return ['geometry'=>['type'=>'Point', 'coordinates'=>$this->getCoordinates()->toArray()]];

        return ['geometry'=>['type'=>'Point', 'coordinates'=>[]]];
    }

    /**
     * @return Point
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param Point $coordinates
     * @return ListAddr
     */
    public function setCoordinates(Point $coordinates = null)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFullAddress() ?: '';
    }

    /**
     * @return string
     */
    public function getFullAddress()
    {
        return $this->fullAddress;
    }

    /**
     * @param $fullAddress
     * @return $this
     */
    public function setFullAddress($fullAddress)
    {
        $this->fullAddress = $fullAddress;
        return $this;
    }

    /**
     * @return array
     */
    public function getSerializedCoordinates(){
        if($this->getCoordinates() instanceof Point) {
            return $this->getCoordinates()->toArray();
        }

        return [];
    }

}
