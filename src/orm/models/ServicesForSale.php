<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServicesForSale
 *
 * @ORM\Table(name="services_for_sale", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"service_name"})
 * })
 * @ORM\Entity
 */
class ServicesForSale
{
    /**
     * @var string
     *
     * @ORM\Column(name="service_name", type="string", length=50, nullable=false)
     */
    private $serviceName;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set serviceName
     *
     * @param string $serviceName
     *
     * @return ServicesForSale
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * Get serviceName
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

