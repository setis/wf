<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Joinedbills
 *
 * @ORM\Table(name="joinedbills")
 * @ORM\Entity
 */
class Joinedbills
{
    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $taskId;

    /**
     * @var integer
     *
     * @ORM\Column(name="joined_bill_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $joinedBillId;


    /**
     * Set taskId
     *
     * @param integer $taskId
     *
     * @return Joinedbills
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set joinedBillId
     *
     * @param integer $joinedBillId
     *
     * @return Joinedbills
     */
    public function setJoinedBillId($joinedBillId)
    {
        $this->joinedBillId = $joinedBillId;

        return $this;
    }

    /**
     * Get joinedBillId
     *
     * @return integer
     */
    public function getJoinedBillId()
    {
        return $this->joinedBillId;
    }
}

