<?php

namespace models;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use WF\Task\Model\BaseTicketInterface;

/**
 * Bill
 *
 * @ORM\Table(name="bills", indexes={
 *     @ORM\Index(columns={"num"})
 * })
 * @ORM\Entity(repositoryClass="repository\BillRepository")
 */
class Bill implements BaseTicketInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taskId;

    /**
     * @var string
     *
     * @ORM\Column(name="plat_text", type="string", length=255, nullable=true)
     */
    private $payerText;

    /**
     * @var string
     *
     * @ORM\Column(name="`sum`", type="decimal", precision=10, scale=2)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="num", type="string", length=20)
     */
    private $number;

    /**
     * @var boolean
     *
     * @ORM\Column(name="odobren", type="boolean")
     */
    private $isApproved;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_srok", type="date", nullable=true)
     */
    private $paymentDate;

    /**
     *
     * @var Agreement
     *
     * @ORM\ManyToOne(targetEntity="Agreement")
     * @ORM\JoinColumn(name="agr_id", referencedColumnName="id")
     */
    private $agreement;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="`delayed`", type="date", nullable=true)
     */
    private $delayed;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_srokpost", type="date", nullable=true)
     */
    private $deliveryDate;

    /**
     * @var string
     *
     * @ORM\Column(name="paydesc", type="text", nullable=true)
     */
    private $paymentDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="exec", type="integer", nullable=true)
     */
    private $exec;

    /**
     * @var integer
     *
     * @ORM\Column(name="agreed", type="integer", nullable=true)
     */
    private $agreed;

    /**
     * @ORM\ManyToOne(targetEntity="ListContr")
     * @ORM\JoinColumn(name="client", referencedColumnName="id")
     */
    private $client;

    /**
     *
     * @var Agreement
     *
     * @ORM\ManyToOne(targetEntity="Agreement")
     * @ORM\JoinColumn(name="client_agr", referencedColumnName="id")
     */
    private $clientAgreement;

    /**
     *
     * @var BillDirection
     *
     * @ORM\ManyToOne(targetEntity="BillDirection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    private $direction;

    /**
     * @var BillType
     *
     * @ORM\ManyToOne(targetEntity="BillType", inversedBy="bills")
     * @ORM\JoinColumn(name="exp_type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @var Task
     *
     * @ORM\OneToOne(targetEntity="Task", inversedBy="bill", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $task;

    /**
     * @var BillPayment[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="BillPayment", mappedBy="bill")
     */
    private $payments;

    /**
     * @var BillPayer
     *
     * @ORM\ManyToOne(targetEntity="BillPayer", inversedBy="bills")
     * @ORM\JoinColumn(name="plat_id", referencedColumnName="id")
     */
    private $payer;

    /**
     * @var ListContr
     *
     * @ORM\ManyToOne(targetEntity="ListContr")
     * @ORM\JoinColumn(name="poluch", referencedColumnName="id")
     */
    private $supplier;

    public function __construct()
    {
        $this->isApproved = false;
        $this->payments = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPayerText()
    {
        return $this->payerText;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getSupplier()
    {
        return $this->supplier;
    }

    public function isApproved()
    {
        return $this->isApproved;
    }

    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    public function getAgreement()
    {
        return $this->agreement;
    }

    public function getDelayed()
    {
        return $this->delayed;
    }

    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    public function getPaymentDescription()
    {
        return $this->paymentDescription;
    }

    public function getExec()
    {
        return $this->exec;
    }

    public function getAgreed()
    {
        return $this->agreed;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function getClientAgreement()
    {
        return $this->clientAgreement;
    }

    public function getDirection()
    {
        return $this->direction;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function getPayments()
    {
        return $this->payments;
    }

    public function getPayer()
    {
        return $this->payer;
    }

    public function setPayerText($payerText)
    {
        $this->payerText = $payerText;
        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
        return $this;
    }

    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;
        return $this;
    }

    public function setPaymentDate(DateTime $paymentDate = null)
    {
        $this->paymentDate = $paymentDate;
        return $this;
    }

    public function setAgreement(Agreement $agreement = null)
    {
        $this->agreement = $agreement;
        return $this;
    }

    public function setDelayed(DateTime $delayed = null)
    {
        $this->delayed = $delayed;
        return $this;
    }

    public function setDeliveryDate(DateTime $deliveryDate = null)
    {
        $this->deliveryDate = $deliveryDate;
        return $this;
    }

    public function setPaymentDescription($paymentDescription)
    {
        $this->paymentDescription = $paymentDescription;
        return $this;
    }

    public function setExec($exec)
    {
        $this->exec = $exec;
        return $this;
    }

    public function setAgreed($agreed)
    {
        $this->agreed = $agreed;
        return $this;
    }

    public function setClient(ListContr $client = null)
    {
        $this->client = $client;
        return $this;
    }

    public function setClientAgreement(Agreement $clientAgreement = null)
    {
        $this->clientAgreement = $clientAgreement;
        return $this;
    }

    public function setDirection(BillDirection $direction = null)
    {
        $this->direction = $direction;
        return $this;
    }

    public function setType(BillType $type = null)
    {
        $this->type = $type;
        return $this;
    }

    public function setTask(Task $task = null)
    {
        $this->task = $task;
        return $this;
    }

    public function addPayment(BillPayment $payment)
    {
        $this->payments->add($payment);
        return $this;
    }

    public function setPayer(BillPayer $payer)
    {
        $this->payer = $payer;
        return $this;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return int
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * @return ListContr
     */
    public function getPartner()
    {
        return $this->getSupplier();
    }

    /**
     * @return BillType
     */
    public function getTaskType()
    {
        return $this->getType();
    }

}
