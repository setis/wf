<?php

namespace models;

use CrEOF\Spatial\PHP\Types\Geography\Polygon;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ListOds
 *
 * @ORM\Table(name="list_ods")
 * @ORM\Entity(repositoryClass="repository\ListOdsRepository")
 */
class ListOds
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Serializer\Groups({"default"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="fio", type="string", length=255)
     * @Serializer\Groups({"default"})
     */
    private $fio;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=20)
     * @Serializer\Groups({"default"})
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     * @Serializer\Groups({"default"})
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=16777215)
     * @Serializer\Groups({"default"})
     */
    private $comment;

    /**
     * @var Polygon
     *
     * @ORM\Column(name="area", type="polygon", nullable=true)
     * @Serializer\Groups({"default"})
     * @Serializer\Accessor(getter="getSerializedArea")
     */
    private $area;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ListOds
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set fio
     *
     * @param string $fio
     *
     * @return ListOds
     */
    public function setFio($fio)
    {
        $this->fio = $fio;

        return $this;
    }

    /**
     * Get fio
     *
     * @return string
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return ListOds
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return ListOds
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ListOds
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param Polygon $area
     * @return self
     */
    public function setArea(Polygon $area)
    {
        $this->area = $area;
        return $this;
    }

    /**
     * @return Polygon
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @return array
     */
    public function getSerializedArea()
    {
        if($this->getArea())
            return ['geometry'=>['type'=>'Polygon', 'coordinates'=>$this->getArea()->toArray()]];

        return ['geometry'=>['type'=>'Polygon', 'coordinates'=>[]]];
    }
}

