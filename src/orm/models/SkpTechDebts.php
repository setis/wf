<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * SkpTechDebts
 *
 * @ORM\Table(name="skp_tech_debts")
 * @ORM\Entity
 */
class SkpTechDebts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var User
     * 
     * @ORM\OneToOne(targetEntity="User", inversedBy="skpDebt")
     * @ORM\JoinColumn(name="tech_id", referencedColumnName="id")
     */
    private $tech;

    /**
     * @var string
     *
     * @ORM\Column(name="ammount", type="decimal", precision=10, scale=2)
     */
    private $amount;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 
     * @return User
     */
    public function getTech()
    {
        return $this->tech;
    }

    /**
     * 
     * @param User $tech
     * @return self
     */
    public function setTech(User $tech)
    {
        $this->tech = $tech;
        return $this;
    }

    /**
     * Set ammount
     *
     * @param string $amount
     *
     * @return SkpTechDebts
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get ammount
     *
     * @return float
     */
    public function getAmount()
    {
        return (float) $this->amount;
    }
}
