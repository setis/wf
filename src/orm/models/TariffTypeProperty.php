<?php
namespace models;


use Doctrine\ORM\Mapping as ORM;

/**
 * TarifTypeProperties
 *
 * @ORM\Table(name="tarif_type_properties", indexes={
 *     @ORM\Index(columns={"tarif_type_id"})
 * })
 * @ORM\Entity
 */
class TariffTypeProperty
{
    /**
     * @var string
     *
     * @ORM\Column(name="property_name", type="string", length=50, nullable=false)
     */
    private $propertyName;

    /**
     * @var integer
     *
     * @ORM\Column(name="property_order", type="integer", nullable=false)
     */
    private $propertyOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var TariffType
     *
     * @ORM\ManyToOne(targetEntity="models\TariffType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tarif_type_id", referencedColumnName="id")
     * })
     */
    private $tarifType;


    /**
     * Set propertyName
     *
     * @param string $propertyName
     *
     * @return TariffTypeProperty
     */
    public function setPropertyName($propertyName)
    {
        $this->propertyName = $propertyName;

        return $this;
    }

    /**
     * Get propertyName
     *
     * @return string
     */
    public function getPropertyName()
    {
        return $this->propertyName;
    }

    /**
     * Set propertyOrder
     *
     * @param integer $propertyOrder
     *
     * @return TariffTypeProperty
     */
    public function setPropertyOrder($propertyOrder)
    {
        $this->propertyOrder = $propertyOrder;

        return $this;
    }

    /**
     * Get propertyOrder
     *
     * @return integer
     */
    public function getPropertyOrder()
    {
        return $this->propertyOrder;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tarifType
     *
     * @param TariffType $tarifType
     *
     * @return TariffTypeProperty
     */
    public function setTarifType(TariffType $tarifType = null)
    {
        $this->tarifType = $tarifType;

        return $this;
    }

    /**
     * Get tarifType
     *
     * @return TariffType
     */
    public function getTarifType()
    {
        return $this->tarifType;
    }
}

