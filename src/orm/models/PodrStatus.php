<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * PodrStatus
 *
 * @ORM\Table(name="podr_status")
 * @ORM\Entity
 */
class PodrStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="contr_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $contrId;

    /**
     * @var string
     *
     * @ORM\Column(name="podr_status", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $podrStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="status_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $statusId;


    /**
     * Set contrId
     *
     * @param integer $contrId
     *
     * @return PodrStatus
     */
    public function setContrId($contrId)
    {
        $this->contrId = $contrId;

        return $this;
    }

    /**
     * Get contrId
     *
     * @return integer
     */
    public function getContrId()
    {
        return $this->contrId;
    }

    /**
     * Set podrStatus
     *
     * @param string $podrStatus
     *
     * @return PodrStatus
     */
    public function setPodrStatus($podrStatus)
    {
        $this->podrStatus = $podrStatus;

        return $this;
    }

    /**
     * Get podrStatus
     *
     * @return string
     */
    public function getPodrStatus()
    {
        return $this->podrStatus;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return PodrStatus
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }
}

