<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * BillsPayments
 *
 * @ORM\Table(name="bills_payments")
 * @ORM\Entity
 */
class BillPayment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="ammount", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $ammount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pay_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $payDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="op_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $opDate;

    /**
     * @var Bill
     *
     * @ORM\ManyToOne(targetEntity="models\Bill", inversedBy="payments")
     * @ORM\JoinColumn(name="bill_id", referencedColumnName="task_id", nullable=true, onDelete="CASCADE")
     */
    private $bill;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="models\User", inversedBy="payments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $user;

    /**
     * @var BillType
     *
     * @ORM\ManyToOne(targetEntity="models\BillType", inversedBy="payments")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=true)
     */
    private $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return BillPayment
     */
    public function setAmount($amount)
    {
        $this->ammount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->ammount;
    }

    /**
     * Set payDate
     *
     * @param \DateTime $payDate
     *
     * @return BillPayment
     */
    public function setPayDate($payDate)
    {
        $this->payDate = $payDate;

        return $this;
    }

    /**
     * Get payDate
     *
     * @return \DateTime
     */
    public function getPayDate()
    {
        return $this->payDate;
    }

    /**
     * Set opDate
     *
     * @param \DateTime $opDate
     *
     * @return BillPayment
     */
    public function setOpDate($opDate)
    {
        $this->opDate = $opDate;

        return $this;
    }

    /**
     * Get opDate
     *
     * @return \DateTime
     */
    public function getOpDate()
    {
        return $this->opDate;
    }

    public function getAmmount()
    {
        return $this->ammount;
    }

    public function getBill()
    {
        return $this->bill;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setAmmount($ammount)
    {
        $this->ammount = $ammount;
        return $this;
    }

    public function setBill(Bill $bill)
    {
        $this->bill = $bill;
        return $this;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function setType(BillType $type)
    {
        $this->type = $type;
        return $this;
    }


}

