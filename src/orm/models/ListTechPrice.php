<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Task\Doctrine\AbstractPrice;

/**
 * ListTechPrice
 *
 * @ORM\Table(name="list_tech_price", indexes={
 *     @ORM\Index(columns={"id", "price"})
 * })
 * @ORM\Entity(repositoryClass="repository\ListTechPriceRepository")
 */
class ListTechPrice extends AbstractPrice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="short_title", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $shortTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="price_type", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $priceType;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="p_comment", type="text", length=16777215, precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $pComment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="archive", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $archive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="convert_bonus", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $convertBonus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="timeslot_bonus", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $timeslotBonus;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ListTechPrice
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set shortTitle
     *
     * @param string $shortTitle
     *
     * @return ListTechPrice
     */
    public function setShortTitle($shortTitle)
    {
        $this->shortTitle = $shortTitle;

        return $this;
    }

    /**
     * Get shortTitle
     *
     * @return string
     */
    public function getShortTitle()
    {
        return $this->shortTitle;
    }

    /**
     * Set priceType
     *
     * @param string $priceType
     *
     * @return ListTechPrice
     */
    public function setPriceType($priceType)
    {
        $this->priceType = $priceType;

        return $this;
    }

    /**
     * Get priceType
     *
     * @return string
     */
    public function getPriceType()
    {
        return $this->priceType;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return ListTechPrice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set pComment
     *
     * @param string $pComment
     *
     * @return ListTechPrice
     */
    public function setPComment($pComment)
    {
        $this->pComment = $pComment;

        return $this;
    }

    /**
     * Get pComment
     *
     * @return string
     */
    public function getPComment()
    {
        return $this->pComment;
    }

    /**
     * Set archive
     *
     * @param boolean $archive
     *
     * @return ListTechPrice
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;

        return $this;
    }

    /**
     * Get archive
     *
     * @return boolean
     */
    public function getArchive()
    {
        return $this->archive;
    }

    /**
     * Set convertBonus
     *
     * @param boolean $convertBonus
     *
     * @return ListTechPrice
     */
    public function setConvertBonus($convertBonus)
    {
        $this->convertBonus = $convertBonus;

        return $this;
    }

    /**
     * Get convertBonus
     *
     * @return boolean
     */
    public function getConvertBonus()
    {
        return $this->convertBonus;
    }

    /**
     * Set timeslotBonus
     *
     * @param boolean $timeslotBonus
     *
     * @return ListTechPrice
     */
    public function setTimeslotBonus($timeslotBonus)
    {
        $this->timeslotBonus = $timeslotBonus;

        return $this;
    }

    /**
     * Get timeslotBonus
     *
     * @return boolean
     */
    public function getTimeslotBonus()
    {
        return $this->timeslotBonus;
    }
}

