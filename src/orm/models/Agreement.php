<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Agreement
 *
 * @ORM\Table(name="list_contr_agr")
 * @ORM\Entity
 */
class Agreement
{
    const CASH = 1;
    const CASHLESS = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255)
     */
    private $number;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="agr_date", type="date")
     */
    private $agrDate;

    /**
     * @var ListContr
     *
     * @ORM\ManyToOne(targetEntity="ListContr", inversedBy="agreements")
     * @ORM\JoinColumn(name="contr_id", referencedColumnName="id")
     */
    private $partner;

    /**
     * @var string
     *
     * @ORM\Column(name="coment", type="text")
     */
    private $coment;

    /**
     * @var string
     *
     * @ORM\Column(name="access_contact", type="text")
     */
    private $accessContact;

    /**
     * @var string
     *
     * @ORM\Column(name="tp_contact", type="text")
     */
    private $tpContact;

    /**
     * @var boolean
     *
     * @ORM\Column(name="type_price", type="boolean")
     */
    private $typePrice;

    /**
     * @var boolean
     *
     * @ORM\Column(name="type_smeta", type="boolean")
     */
    private $typeSmeta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="type_abon", type="boolean")
     */
    private $typeAbon;

    /**
     * @var string
     *
     * @ORM\Column(name="agreement", type="text", nullable=true)
     */
    private $agreement;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prolong", type="boolean", nullable=true)
     */
    private $prolong;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="findate", type="date", nullable=true)
     */
    private $findate;

    /**
     * @var string
     *
     * @ORM\Column(name="project", type="text", nullable=true)
     */
    private $project;

    /**
     * @var TaskStatus
     *
     * @ORM\ManyToOne(targetEntity="TaskStatus")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="ammount", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $ammount;

    /**
     * @var integer
     *
     * @ORM\Column(name="paytype", type="integer")
     */
    private $paytype = 1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mailonlk", type="boolean", nullable=true)
     */
    private $mailonlk;

    /**
     * @var LinkAgreementTaskType[]
     *
     * @ORM\OneToMany(targetEntity="models\LinkAgreementTaskType", mappedBy="agreement")
     */
    private $workTypes;

    /**
     * @var Agent[]
     *
     * @ORM\OneToMany(targetEntity="models\Agent", mappedBy="agreement")
     */
    private $agents;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Agreement
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set agrDate
     *
     * @param \DateTime $agrDate
     *
     * @return Agreement
     */
    public function setAgrDate($agrDate)
    {
        $this->agrDate = $agrDate;

        return $this;
    }

    /**
     * Get agrDate
     *
     * @return \DateTime
     */
    public function getAgrDate()
    {
        return $this->agrDate;
    }

    /**
     * @return ListContr
     */
    public function getPartner(): ListContr
    {
        return $this->partner;
    }

    /**
     * @param ListContr $partner
     * @return Agreement
     */
    public function setPartner(ListContr $partner): Agreement
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * Set coment
     *
     * @param string $coment
     *
     * @return Agreement
     */
    public function setComent($coment)
    {
        $this->coment = $coment;

        return $this;
    }

    /**
     * Get coment
     *
     * @return string
     */
    public function getComent()
    {
        return $this->coment;
    }

    /**
     * Set accessContact
     *
     * @param string $accessContact
     *
     * @return Agreement
     */
    public function setAccessContact($accessContact)
    {
        $this->accessContact = $accessContact;

        return $this;
    }

    /**
     * Get accessContact
     *
     * @return string
     */
    public function getAccessContact()
    {
        return $this->accessContact;
    }

    /**
     * Set tpContact
     *
     * @param string $tpContact
     *
     * @return Agreement
     */
    public function setTpContact($tpContact)
    {
        $this->tpContact = $tpContact;

        return $this;
    }

    /**
     * Get tpContact
     *
     * @return string
     */
    public function getTpContact()
    {
        return $this->tpContact;
    }

    /**
     * Set typePrice
     *
     * @param boolean $typePrice
     *
     * @return Agreement
     */
    public function setTypePrice($typePrice)
    {
        $this->typePrice = $typePrice;

        return $this;
    }

    /**
     * Get typePrice
     *
     * @return boolean
     */
    public function getTypePrice()
    {
        return $this->typePrice;
    }

    /**
     * Set typeSmeta
     *
     * @param boolean $typeSmeta
     *
     * @return Agreement
     */
    public function setTypeSmeta($typeSmeta)
    {
        $this->typeSmeta = $typeSmeta;

        return $this;
    }

    /**
     * Get typeSmeta
     *
     * @return boolean
     */
    public function getTypeSmeta()
    {
        return $this->typeSmeta;
    }

    /**
     * Set typeAbon
     *
     * @param boolean $typeAbon
     *
     * @return Agreement
     */
    public function setTypeAbon($typeAbon)
    {
        $this->typeAbon = $typeAbon;

        return $this;
    }

    /**
     * Get typeAbon
     *
     * @return boolean
     */
    public function getTypeAbon()
    {
        return $this->typeAbon;
    }

    /**
     * Set agreement
     *
     * @param string $agreement
     *
     * @return Agreement
     */
    public function setAgreement($agreement)
    {
        $this->agreement = $agreement;

        return $this;
    }

    /**
     * Get agreement
     *
     * @return string
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * Set prolong
     *
     * @param boolean $prolong
     *
     * @return Agreement
     */
    public function setProlong($prolong)
    {
        $this->prolong = $prolong;

        return $this;
    }

    /**
     * Get prolong
     *
     * @return boolean
     */
    public function getProlong()
    {
        return $this->prolong;
    }

    /**
     * Set findate
     *
     * @param \DateTime $findate
     *
     * @return Agreement
     */
    public function setFindate($findate)
    {
        $this->findate = $findate;

        return $this;
    }

    /**
     * Get findate
     *
     * @return \DateTime
     */
    public function getFindate()
    {
        return $this->findate;
    }

    /**
     * Set project
     *
     * @param string $project
     *
     * @return Agreement
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return string
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return TaskStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param TaskStatus $status
     * @return Agreement
     */
    public function setStatus(TaskStatus $status): Agreement
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Set ammount
     *
     * @param string $ammount
     *
     * @return Agreement
     */
    public function setAmmount($ammount)
    {
        $this->ammount = $ammount;

        return $this;
    }

    /**
     * Get ammount
     *
     * @return string
     */
    public function getAmmount()
    {
        return $this->ammount;
    }

    /**
     * Set paytype
     *
     * @param integer $paytype
     *
     * @return Agreement
     */
    public function setPaytype($paytype)
    {
        $this->paytype = $paytype;

        return $this;
    }

    /**
     * Get paytype
     *
     * @return integer
     */
    public function getPaytype()
    {
        return $this->paytype;
    }

    /**
     * Set mailonlk
     *
     * @param boolean $mailonlk
     *
     * @return Agreement
     */
    public function setMailonlk($mailonlk)
    {
        $this->mailonlk = $mailonlk;

        return $this;
    }

    /**
     * Get mailonlk
     *
     * @return boolean
     */
    public function getMailonlk()
    {
        return $this->mailonlk;
    }

    /**
     * @return LinkAgreementTaskType[]
     */
    public function getWorkTypes(): array
    {
        return $this->workTypes;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->number.' от '.$this->agrDate->format('Y-m-d');
    }

    /**
     * @return mixed
     */
    public function getAgents()
    {
        return $this->agents;
    }
}

