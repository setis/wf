<?php

namespace models;

use WF\Core\MeasureUnitInterface;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\ORM\Mapping as ORM;

/**
 * Description of MeasureUnit
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 *
 * @ORM\Table(name="measure_unit")
 * @ORM\Entity()
 */
class MeasureUnit implements MeasureUnitInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=20)
     * @Serializer\Groups({"default"})
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_metrical", type="boolean")
     * @Serializer\Groups({"default"})
     */
    private $isMetrical;

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function isMetrical()
    {
        return $this->isMetrical;
    }

    public function setIsMetrical($isMetrical)
    {
        $this->isMetrical = $isMetrical;

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }

}
