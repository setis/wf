<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * QcQlist
 *
 * @ORM\Table(name="qc_qlist")
 * @ORM\Entity
 */
class QcQuestion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="iorder", type="smallint")
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(name="qtext", type="text", nullable=true)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="qtextdesc", type="text", nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="q_yesno", type="smallint")
     */
    private $yesNo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="q_mbyesno", type="smallint")
     */
    private $maybeYesNo;

    /**
     * @var integer
     *
     * @ORM\Column(name="q_maxcount", type="integer")
     */
    private $maxCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="yes_count", type="integer")
     */
    private $yesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="no_count", type="integer")
     */
    private $noCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer")
     */
    private $weight;

    /**
     * @var boolean
     *
     * @ORM\Column(name="req_ammount", type="boolean")
     */
    private $isAmmountRequired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="req_comment", type="boolean")
     */
    private $isCommentRequired;

    /**
     * @var integer
     *
     * @ORM\Column(name="status_group", type="smallint")
     */
    private $statusGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="plugin_uid", type="string", length=50)
     */
    private $pluginUid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $isActive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cr_date", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $isDeleted;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $user;

    /**
     * @var \models\ListContr
     *
     * @ORM\ManyToOne(targetEntity="models\ListContr")
     * @ORM\JoinColumn(name="contr_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $partner;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getYesNo()
    {
        return $this->yesNo;
    }

    public function getMaybeYesNo()
    {
        return $this->maybeYesNo;
    }

    public function getMaxCount()
    {
        return $this->maxCount;
    }

    public function getYesCount()
    {
        return $this->yesCount;
    }

    public function getNoCount()
    {
        return $this->noCount;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function isAmmountRequired()
    {
        return $this->isAmmountRequired;
    }

    public function isCommentRequired()
    {
        return $this->isCommentRequired;
    }

    public function getStatusGroup()
    {
        return $this->statusGroup;
    }

    public function getPluginUid()
    {
        return $this->pluginUid;
    }

    public function isActive()
    {
        return $this->isActive;
    }


    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function isDeleted()
    {
        return $this->isDeleted;
    }

    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    
    public function setYesNo($yesNo)
    {
        $this->yesNo = $yesNo;
        return $this;
    }

    public function setMaybeYesNo($maybeYesNo)
    {
        $this->maybeYesNo = $maybeYesNo;
        return $this;
    }

    public function setMaxCount($maxCount)
    {
        $this->maxCount = $maxCount;
        return $this;
    }

    public function setYesCount($yesCount)
    {
        $this->yesCount = $yesCount;
        return $this;
    }

    public function setNoCount($noCount)
    {
        $this->noCount = $noCount;
        return $this;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    public function setIsAmmountRequired($isAmmountRequired)
    {
        $this->isAmmountRequired = $isAmmountRequired;
        return $this;
    }

    public function setIsCommentRequired($isCommentRequired)
    {
        $this->isCommentRequired = $isCommentRequired;
        return $this;
    }

    public function setStatusGroup($statusGroup)
    {
        $this->statusGroup = $statusGroup;
        return $this;
    }

    public function setPluginUid($pluginUid)
    {
        $this->pluginUid = $pluginUid;
        return $this;
    }

    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

    public function isInteractive()
    {
        return $this->getYesNo() === 1
            || $this->getMaybeYesNo() === 1
            || $this->getMaxCount() > 0;
    }

    /**
     * @param User $user
     * @return QcQuestion
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param ListContr $partner
     * @return QcQuestion
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * @return ListContr
     */
    public function getPartner()
    {
        return $this->partner;
    }
}

