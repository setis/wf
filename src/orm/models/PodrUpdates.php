<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * PodrUpdates
 *
 * @ORM\Table(name="podr_updates", indexes={
 *     @ORM\Index(columns={"contr_id"}), 
 *     @ORM\Index(columns={"is_processed"})
 * })
 * @ORM\Entity
 */
class PodrUpdates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_post", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $datePost;

    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $taskId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $contrId;

    /**
     * @var integer
     *
     * @ORM\Column(name="status_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $statusId;

    /**
     * @var string
     *
     * @ORM\Column(name="cmm", type="text", length=16777215, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cmm;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_processed", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isProcessed;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datePost
     *
     * @param \DateTime $datePost
     *
     * @return PodrUpdates
     */
    public function setDatePost($datePost)
    {
        $this->datePost = $datePost;

        return $this;
    }

    /**
     * Get datePost
     *
     * @return \DateTime
     */
    public function getDatePost()
    {
        return $this->datePost;
    }

    /**
     * Set taskId
     *
     * @param integer $taskId
     *
     * @return PodrUpdates
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set contrId
     *
     * @param integer $contrId
     *
     * @return PodrUpdates
     */
    public function setContrId($contrId)
    {
        $this->contrId = $contrId;

        return $this;
    }

    /**
     * Get contrId
     *
     * @return integer
     */
    public function getContrId()
    {
        return $this->contrId;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return PodrUpdates
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set cmm
     *
     * @param string $cmm
     *
     * @return PodrUpdates
     */
    public function setCmm($cmm)
    {
        $this->cmm = $cmm;

        return $this;
    }

    /**
     * Get cmm
     *
     * @return string
     */
    public function getCmm()
    {
        return $this->cmm;
    }

    /**
     * Set isProcessed
     *
     * @param boolean $isProcessed
     *
     * @return PodrUpdates
     */
    public function setIsProcessed($isProcessed)
    {
        $this->isProcessed = $isProcessed;

        return $this;
    }

    /**
     * Get isProcessed
     *
     * @return boolean
     */
    public function getIsProcessed()
    {
        return $this->isProcessed;
    }
}

