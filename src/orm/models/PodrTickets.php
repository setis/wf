<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * PodrTickets
 *
 * @ORM\Table(name="podr_tickets", indexes={
 *     @ORM\Index(columns={"is_processed"})
 * })
 * @ORM\Entity
 */
class PodrTickets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $taskId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $contrId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_processed", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isProcessed;


    /**
     * Set taskId
     *
     * @param integer $taskId
     *
     * @return PodrTickets
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set contrId
     *
     * @param integer $contrId
     *
     * @return PodrTickets
     */
    public function setContrId($contrId)
    {
        $this->contrId = $contrId;

        return $this;
    }

    /**
     * Get contrId
     *
     * @return integer
     */
    public function getContrId()
    {
        return $this->contrId;
    }

    /**
     * Set isProcessed
     *
     * @param boolean $isProcessed
     *
     * @return PodrTickets
     */
    public function setIsProcessed($isProcessed)
    {
        $this->isProcessed = $isProcessed;

        return $this;
    }

    /**
     * Get isProcessed
     *
     * @return boolean
     */
    public function getIsProcessed()
    {
        return $this->isProcessed;
    }
}

