<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * LinkScArea
 *
 * @ORM\Table(name="link_sc_area")
 * @ORM\Entity
 */
class LinkScArea
{
    /**
     * @var integer
     *
     * @ORM\Column(name="area_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $areaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sc_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $scId;


    /**
     * Set areaId
     *
     * @param integer $areaId
     *
     * @return LinkScArea
     */
    public function setAreaId($areaId)
    {
        $this->areaId = $areaId;

        return $this;
    }

    /**
     * Get areaId
     *
     * @return integer
     */
    public function getAreaId()
    {
        return $this->areaId;
    }

    /**
     * Set scId
     *
     * @param integer $scId
     *
     * @return LinkScArea
     */
    public function setScId($scId)
    {
        $this->scId = $scId;

        return $this;
    }

    /**
     * Get scId
     *
     * @return integer
     */
    public function getScId()
    {
        return $this->scId;
    }
}

