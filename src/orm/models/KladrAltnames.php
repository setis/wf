<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * KladrAltnames
 *
 * @ORM\Table(name="kladr_altnames")
 * @ORM\Entity
 */
class KladrAltnames
{
    /**
     * @var string
     *
     * @ORM\Column(name="OLDCODE", type="string", length=19, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $oldcode;

    /**
     * @var string
     *
     * @ORM\Column(name="NEWCODE", type="string", length=19, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $newcode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="LEVEL", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $level;


    /**
     * Set oldcode
     *
     * @param string $oldcode
     *
     * @return KladrAltnames
     */
    public function setOldcode($oldcode)
    {
        $this->oldcode = $oldcode;

        return $this;
    }

    /**
     * Get oldcode
     *
     * @return string
     */
    public function getOldcode()
    {
        return $this->oldcode;
    }

    /**
     * Set newcode
     *
     * @param string $newcode
     *
     * @return KladrAltnames
     */
    public function setNewcode($newcode)
    {
        $this->newcode = $newcode;

        return $this;
    }

    /**
     * Get newcode
     *
     * @return string
     */
    public function getNewcode()
    {
        return $this->newcode;
    }

    /**
     * Set level
     *
     * @param boolean $level
     *
     * @return KladrAltnames
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return boolean
     */
    public function getLevel()
    {
        return $this->level;
    }
}

