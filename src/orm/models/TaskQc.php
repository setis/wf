<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;

/**
 * TicketsQc
 *
 * @ORM\Table(name="tickets_qc", indexes={
 *     @ORM\Index(columns={"task_id","qc_status"})
 * })
 * @ORM\Entity
 */
class TaskQc
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="qc_result", type="integer", nullable=true)
     */
    private $result;

    /**
     * @var integer
     *
     * @ORM\Column(name="qc_new_result", type="integer", nullable=true)
     */
    private $newResult;

    /**
     * @var boolean
     *
     * @ORM\Column(name="qc_status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="qc_delayed", type="datetime", nullable=true)
     */
    private $delayTo;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="qc_date", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $date;

    /**
     * @var Task
     *
     * @ORM\OneToOne(targetEntity="models\Task", inversedBy="qc")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $task;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function getId()
    {
        return $this->id;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getNewResult()
    {
        return $this->newResult;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getDelayTo()
    {
        return $this->delayTo;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }

    public function setNewResult($newResult)
    {
        $this->newResult = $newResult;
        return $this;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function setDelayTo(DateTime $delayTo)
    {
        $this->delayTo = $delayTo;
        return $this;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    public function setDate(DateTime $date)
    {
        $this->date = $date;
        return $this;
    }

    public function setTask(Task $task)
    {
        $this->task = $task;
        return $this;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }
}

