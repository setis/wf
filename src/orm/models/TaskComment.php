<?php

namespace models;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use WF\Task\Model\TaskCommentInterface;

/**
 * TaskComment
 *
 * @ORM\Table(name="task_comments", indexes={
 *     @ORM\Index(columns={"datetime"}),
 *     @ORM\Index(columns={"task_id", "status_id"}),
 *     @ORM\Index(columns={"user_id", "id", "task_id"}),
 *     @ORM\Index(columns={"text"}, flags={"fulltext"}),
 *     @ORM\Index(columns={"id", "status_id", "user_id", "task_id", "datetime"})
 * })
 * @ORM\Entity
 */
class TaskComment implements TaskCommentInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $datetime;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="text", nullable=true)
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string", length=32, nullable=true)
     */
    private $externalId;

    /**
     * @var Task
     *
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="comments", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $task;

    /**
     * @var TaskStatus
     *
     * @ORM\ManyToOne(targetEntity="TaskStatus", inversedBy="comments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=true, onDelete="NO ACTION")
     * })
     */
    private $status;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="NO ACTION")
     * })
     */
    private $author;

    public function __construct()
    {
        $this->datetime = new DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param DateTime $datetime
     *
     * @return TaskComment
     */
    public function setDatetime(DateTime $datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return TaskComment
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return TaskComment
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set task
     *
     * @param Task $task
     *
     * @return TaskComment
     */
    public function setTask(Task $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param TaskStatus $status
     * @return TaskComment
     */
    public function setStatus($status = null)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return TaskStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param User $author
     * @return TaskComment
     */
    public function setAuthor($author = null)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    public function getExternalId()
    {
        return $this->externalId;
    }

    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }


}

