<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * TmcScTech
 *
 * @ORM\Table(name="tmc_sc_tech", indexes={
 *     @ORM\Index(columns={"tmc_sc_id", "tech_id", "ledit"})
 * })
 * @ORM\Entity
 */
class TmcScTech
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="sc_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $scId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tech_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $techId;

    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $count;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ledit", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $ledit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inplace", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $inplace;

    /**
     * @var boolean
     *
     * @ORM\Column(name="incoming", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $incoming;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dismantled", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dismantled;

    /**
     * @var integer
     *
     * @ORM\Column(name="tmc_ticket_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tmcTicketId;

    /**
     * @var \models\TmcSc
     *
     * @ORM\ManyToOne(targetEntity="models\TmcSc", inversedBy="tmcScTechs")
     * @ORM\JoinColumn(name="tmc_sc_id", referencedColumnName="id")
     */
    private $tmcSc;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set scId
     *
     * @param integer $scId
     *
     * @return TmcScTech
     */
    public function setScId($scId)
    {
        $this->scId = $scId;

        return $this;
    }

    /**
     * Get scId
     *
     * @return integer
     */
    public function getScId()
    {
        return $this->scId;
    }

    /**
     * Set techId
     *
     * @param integer $techId
     *
     * @return TmcScTech
     */
    public function setTechId($techId)
    {
        $this->techId = $techId;

        return $this;
    }

    /**
     * Get techId
     *
     * @return integer
     */
    public function getTechId()
    {
        return $this->techId;
    }

    /**
     * Set tmcScId
     *
     * @param integer $tmcScId
     *
     * @return TmcScTech
     */
    public function setTmcScId($tmcScId)
    {
        $this->tmcScId = $tmcScId;

        return $this;
    }

    /**
     * Get tmcScId
     *
     * @return integer
     */
    public function getTmcScId()
    {
        return $this->tmcScId;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return TmcScTech
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return TmcScTech
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set ledit
     *
     * @param \DateTime $ledit
     *
     * @return TmcScTech
     */
    public function setLedit($ledit)
    {
        $this->ledit = $ledit;

        return $this;
    }

    /**
     * Get ledit
     *
     * @return \DateTime
     */
    public function getLedit()
    {
        return $this->ledit;
    }

    /**
     * Set inplace
     *
     * @param boolean $inplace
     *
     * @return TmcScTech
     */
    public function setInplace($inplace)
    {
        $this->inplace = $inplace;

        return $this;
    }

    /**
     * Get inplace
     *
     * @return boolean
     */
    public function getInplace()
    {
        return $this->inplace;
    }

    /**
     * Set incoming
     *
     * @param boolean $incoming
     *
     * @return TmcScTech
     */
    public function setIncoming($incoming)
    {
        $this->incoming = $incoming;

        return $this;
    }

    /**
     * Get incoming
     *
     * @return boolean
     */
    public function getIncoming()
    {
        return $this->incoming;
    }

    /**
     * Set dismantled
     *
     * @param boolean $dismantled
     *
     * @return TmcScTech
     */
    public function setDismantled($dismantled)
    {
        $this->dismantled = $dismantled;

        return $this;
    }

    /**
     * Get dismantled
     *
     * @return boolean
     */
    public function getDismantled()
    {
        return $this->dismantled;
    }

    /**
     * @return TmcSc
     */
    public function getTmcSc()
    {
        return $this->tmcSc;
    }

    /**
     * @param TmcSc $tmcSc
     * @return TmcScTech
     */
    public function setTmcSc(TmcSc $tmcSc)
    {
        $this->tmcSc = $tmcSc;
        return $this;
    }


}

