<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Tmc\Model\TmcItemInterface;

/**
 * Just move composition.
 * Quantity can be only positive.
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 *
 * @ORM\Table(name="tmc_item", options={"comment":"Just move composition. Quantity can be only positive."})
 * @ORM\Entity(repositoryClass="repository\TmcItemRepository")
 */
class TmcItem implements TmcItemInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="float")
     * @Serializer\Groups({"default"})
     */
    private $quantity;

    /**
     * @var TmcMove
     *
     * @ORM\ManyToOne(targetEntity="models\TmcMove", inversedBy="items")
     * @ORM\JoinColumn(name="move_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Groups({"extended", "tmc.report"})
     */
    private $move;

    /**
     * @var TmcItemProperty
     *
     * @ORM\ManyToOne(targetEntity="models\TmcItemProperty", cascade={"persist"})
     * @ORM\JoinColumn(name="property_id", referencedColumnName="id")
     * @Serializer\Groups({"extended", "TmcItem.Property"})
     */
    private $property;

    ///////////////////////////////////////
    /******************************************************
     * Extended data
     ******************************************************/

    /**
     * @ORM\Column(name="amount", type="decimal", precision=15, scale=3)
     * @Serializer\Groups({"TmcItem.ExtendedData"})
     */
    protected $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3)
     * @Serializer\Groups({"TmcItem.ExtendedData"})
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=true)
     * @Serializer\Groups({"TmcItem.ExtendedData"})
     */
    private $serial;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_lease", type="boolean", nullable=false)
     * @Serializer\Groups({"TmcItem.ExtendedData"})
     */
    private $lease = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_uninstalled", type="boolean")
     * @Serializer\Groups({"TmcItem.ExtendedData"})
     */
    private $isUninstalled = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_defect", type="boolean")
     * @Serializer\Groups({"TmcItem.ExtendedData"})
     */
    private $isDefect = false;

    /**
     * @var mixed
     *
     * @ORM\Column(name="data", type="json_array", nullable=true)
     * @Serializer\Groups({"TmcItem.ExtendedData"})
     */
    private $data;

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public function __construct(TmcItemProperty $property)
    {
        $this->property = $property;

        $this->isUninstalled = $property->isUninstalled();
        $this->isDefect = $property->isDefect();
        $this->lease = $property->isLease();
        $this->amount = $property->getAmount();
        $this->currency = $property->getCurrency();
        $this->data = $property->getData();
        $this->serial = $property->getSerial();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }


    /**
     * @param \models\TmcMove $move
     *
     * @return TmcItem
     */
    public function setMove($move)
    {
        $this->move = $move;

        return $this;
    }

    /**
     * @return \models\TmcMove
     */
    public function getMove()
    {
        return $this->move;
    }

    /**
     * @param $quantity
     *
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @param \models\TmcItemProperty $property
     *
     * @return TmcItem
     */
    public function setProperty($property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * @return \models\TmcItemProperty
     */
    public function getProperty()
    {
        return $this->property;
    }
    /////////////////////////////////////////////////////////////////////////

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("nomenclature")
     * @Serializer\Groups({"TmcHolderItem.Nomenclature"})
     *
     * @return \models\Nomenclature
     */
    public function getNomenclature()
    {
        return $this->property->getNomenclature();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("partner")
     * @Serializer\Groups({"TmcHolderItem.Partner"})
     *
     * @return \models\ListContr
     */
    public function getPartner()
    {
        return $this->property->getPartner();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("amount")
     * @Serializer\Groups({"default"})
     *
     * @return mixed
     */
    public function getAmount()
    {
        return $this->property->getAmount();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("currency")
     * @Serializer\Groups({"default"})
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->property->getCurrency();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("serial")
     * @Serializer\Groups({"default"})
     *
     * @return string
     */
    public function getSerial()
    {
        return $this->property->getSerial();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("lease")
     * @Serializer\Groups({"default"})
     *
     * @return boolean
     */
    public function isLease()
    {
        return $this->property->isLease();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("is_uninstalled")
     * @Serializer\Groups({"default"})
     *
     * @return boolean
     */
    public function isUninstalled()
    {
        return $this->property->isUninstalled();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("is_defect")
     * @Serializer\Groups({"default"})
     *
     * @return boolean
     */
    public function isIsDefect()
    {
        return $this->property->isDefect();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("data")
     * @Serializer\Groups({"default"})
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->property->getData();
    }

}
