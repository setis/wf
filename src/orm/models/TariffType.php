<?php
namespace models;


use Doctrine\ORM\Mapping as ORM;

/**
 * TarifTypes
 *
 * @ORM\Table(name="tarif_types", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"tarif_type"})
 * })
 * @ORM\Entity
 */
class TariffType
{
    /**
     * @var string
     *
     * @ORM\Column(name="tarif_type", type="string", length=50, nullable=false)
     */
    private $tarifType;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set tarifType
     *
     * @param string $tarifType
     *
     * @return TariffType
     */
    public function setTarifType($tarifType)
    {
        $this->tarifType = $tarifType;

        return $this;
    }

    /**
     * Get tarifType
     *
     * @return string
     */
    public function getTarifType()
    {
        return $this->tarifType;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

