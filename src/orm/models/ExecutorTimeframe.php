<?php

namespace models;

use DateInterval;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gorserv\Gerp\ScheduleBundle\Model\ExecutorInterface;
use Gorserv\Gerp\ScheduleBundle\Model\SlotInterface;
use Gorserv\Gerp\ScheduleBundle\Model\TimeframeInterface;

/**
 * ExeceturTimeframe
 *
 * @ORM\Table(name="link_empl_tf", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"user_id", "sched_date"})
 * }, indexes={
 *     @ORM\Index(columns={"time_start", "time_end"})
 * })
 * @ORM\Entity(repositoryClass="repository\ExecutorTimeframeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ExecutorTimeframe implements TimeframeInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $executor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sched_date", type="date")
     */
    private $schedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="starttime", type="integer")
     */
    private $startTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="endtime", type="integer")
     */
    private $endTime;

    /**
     *
     * @var DateTime
     *
     * @ORM\Column(name="time_start", type="datetime", nullable=true)
     */
    private $timeStart;

    /**
     *
     * @var DateTime
     *
     * @ORM\Column(name="time_end", type="datetime", nullable=true)
     */
    private $timeEnd;

    /**
     * @var Gfx[]
     */
    private $slots = [];

    public function getId()
    {
        return $this->id;
    }

    public function getUserExecutor(): User
    {
        return $this->executor;
    }

    public function getTimeStart(): DateTime
    {
        return $this->timeStart;
    }

    public function getTimeEnd(): DateTime
    {
        return $this->timeEnd;
    }

    public function setUserExecutor(User $executor)
    {
        $this->executor = $executor;
        return $this;
    }

    /**
     *
     * @return DateTime
     * @deprecated
     */
    public function getSchedDate(): DateTime
    {
        return $this->schedDate;
    }

    /**
     *
     * @return int
     * @deprecated
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     *
     * @return int
     * @deprecated
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     *
     * @param DateTime $schedDate
     * @return $this
     * @deprecated
     */
    public function setSchedDate(DateTime $schedDate)
    {
        $this->schedDate = $schedDate;
        return $this;
    }

    /**
     *
     * @param int $startTime
     * @return $this
     * @deprecated
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
        return $this;
    }

    /**
     *
     * @param int $endTime
     * @return $this
     * @deprecated
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
        return $this;
    }

    public function setTimeStart(DateTime $timeStart)
    {
        $this->timeStart = $timeStart;
        return $this;
    }

    public function setTimeEnd(DateTime $timeEnd)
    {
        $this->timeEnd = $timeEnd;
        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateNewFields()
    {
        $this->timeStart = clone $this->schedDate;
        $this->timeStart->setTime(0, $this->startTime, 0);

        $this->timeEnd = clone $this->schedDate;
        $this->timeEnd->setTime(0, $this->endTime, 0);
    }

    public function getInterval(): DateInterval
    {
        return new DateInterval('PT30M');
    }

    public function getExecutor(): ExecutorInterface
    {
        return $this->executor;
    }

    public function setSlots(array $slots)
    {
        $this->slots = $slots;
    }

    /**
     * @return SlotInterface[]
     */
    public function getSlots() : array
    {
        return $this->slots;
    }
}
