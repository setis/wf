<?php

namespace models;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Description of UserStatistic
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 *
 * @ORM\Table(name="user_statistic",uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"user_id", "date"})
 * })
 * @ORM\Entity(repositoryClass="repository\UserStatisticRepository")
 */
class UserStatistic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="models\User", inversedBy="statistics")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     *
     * @var DateTime
     *
     * @ORM\Column(name="work_day_started_at", type="datetime", nullable=true)
     */
    private $workDayStartedAt;

    public function __construct(User $user, DateTime $date = null)
    {
        $this->user = $user;
        $this->date = $date ?: new DateTime();
    }

    public function getWorkDayStartedAt()
    {
        return $this->workDayStartedAt;
    }

    public function setWorkDayStartedAt(DateTime $workDayStartedAt)
    {
        $this->workDayStartedAt = $workDayStartedAt;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getUser()
    {
        return $this->user;
    }
}
