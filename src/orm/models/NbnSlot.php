<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * NbnSlots
 *
 * @ORM\Table(name="nbn_slots", indexes={
 *     @ORM\Index(columns={"date_update"})
 * })
 * @ORM\Entity
 */
class NbnSlot
{

    const ACCIDENTS_MEMO = 'WORK_PAY_REQUEST';
    const CONNECTIONS_MEMO = 'WORK_NEW_USER';

    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taskId;

    /**
     * @var string
     *
     * @ORM\Column(name="subject_key", type="string", length=100, nullable=false)
     */
    private $subjectKey;

    /**
     * @var string
     *
     * @ORM\Column(name="slot_key", type="string", length=100, nullable=false)
     */
    private $slotKey;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \models\Task
     *
     * @ORM\OneToOne(targetEntity="models\Task", cascade={"persist","remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id", unique=true, nullable=false, onDelete="CASCADE")
     * })
     */
    private $task;

    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set subjectKey
     *
     * @param string $subjectKey
     *
     * @return NbnSlot
     */
    public function setSubjectKey($subjectKey)
    {
        $this->subjectKey = $subjectKey;

        return $this;
    }

    /**
     * Get subjectKey
     *
     * @return string
     */
    public function getSubjectKey()
    {
        return $this->subjectKey;
    }

    /**
     * Set slotKey
     *
     * @param string $slotKey
     *
     * @return NbnSlot
     */
    public function setSlotKey($slotKey)
    {
        $this->slotKey = $slotKey;

        return $this;
    }

    /**
     * Get slotKey
     *
     * @return string
     */
    public function getSlotKey()
    {
        return $this->slotKey;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return NbnSlot
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * @param Task $task
     * @return NbnSlot
     */
    public function setTask($task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }
}

