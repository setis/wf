<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserPos
 *
 * @ORM\Table(name="user_pos")
 * @ORM\Entity
 */
class JobPosition
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=250)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="le", type="datetime", nullable=true)
     */
    private $le;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return JobPosition
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set le
     *
     * @param \DateTime $le
     *
     * @return JobPosition
     */
    public function setLe($le)
    {
        $this->le = $le;

        return $this;
    }

    /**
     * Get le
     *
     * @return \DateTime
     */
    public function getLe()
    {
        return $this->le;
    }

    /**
     * @param User $user
     * @return JobPosition
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}

