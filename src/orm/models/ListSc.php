<?php

namespace models;

use CrEOF\Spatial\PHP\Types\Geography\Polygon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gorserv\Gerp\AppBundle\Entity\Region;
use JMS\Serializer\Annotation as Serializer;
use WF\Tmc\Model\TmcWarehouseInterface;

/**
 * ListSc
 *
 * @ORM\Table(name="list_sc")
 * @ORM\Entity(repositoryClass="repository\ListScRepository")
 */
class ListSc implements TmcWarehouseInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default", "tmc.moves"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     * @Serializer\Groups({"default"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="`desc`", type="text", nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $desc;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="chief", type="integer", nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $chief;

    /**
     * @var Polygon
     *
     * @ORM\Column(name="area", type="polygon", nullable=true)
     * @Serializer\Groups({"default"})
     * @Serializer\Accessor(getter="getSerializedArea")
     */
    private $area;

    /**
     * @var User
     *
     * @ORM\ManyToMany(targetEntity="models\User", mappedBy="subWarehouses", cascade={"persist", "remove"})
     * @Serializer\Groups({"ListSc.mols"})
     */
    private $mols;

    /**
     * @var User
     *
     * @ORM\ManyToMany(targetEntity="models\User", mappedBy="superSubWarehouses", cascade={"persist", "remove"})
     * @Serializer\Groups({"ListSc.SuperMols"})
     */
    private $superMols;

    /**
     * TMC coordinators
     *
     * @var User
     *
     * @ORM\ManyToMany(targetEntity="models\User", mappedBy="coordinatedWarehouses", cascade={"persist", "remove"})
     * @Serializer\Groups({"ListSc.coordinators"})
     */
    private $coordinators;

    /**
     * Service center technicians
     *
     * @var User[]
     *
     * @ORM\ManyToMany(targetEntity="models\User", mappedBy="warehouses", cascade={"persist", "remove"}, indexBy="sc_id")
     * @Serializer\Groups({"ListSc.technicians"})
     */
    private $technicians;

    /**
     * Service center chiefs
     *
     * @var User
     *
     * @ORM\ManyToMany(targetEntity="models\User", mappedBy="slaveServiceCenters", cascade={"persist", "remove"})
     * @Serializer\Groups({"ListSc.chiefs"})
     */
    private $chiefs;

    /**
     * @var Region
     *
     * @ORM\ManyToOne(targetEntity="Gorserv\Gerp\AppBundle\Entity\Region", inversedBy="serviceCenters")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    private $region;

    public function __construct()
    {
        $this->technicians = new ArrayCollection();
        $this->coordinators = new ArrayCollection();
        $this->mols = new ArrayCollection();
        $this->superMols = new ArrayCollection();
        $this->chiefs = new ArrayCollection();
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ListSc
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get desc
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set desc
     *
     * @param string $desc
     *
     * @return ListSc
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get chief
     *
     * @return integer
     */
    public function getChief()
    {
        return $this->chief;
    }

    /**
     * Set chief
     *
     * @param integer $chief
     *
     * @return ListSc
     */
    public function setChief($chief)
    {
        $this->chief = $chief;

        return $this;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getCoordinators()
    {
        return $this->coordinators;
    }

    /**
     * @param User[]|ArrayCollection $coordinators
     * @return ListSc
     */
    public function setCoordinators($coordinators)
    {
        if (!$coordinators instanceof ArrayCollection)
            $coordinators = new ArrayCollection($coordinators);

        foreach ($this->coordinators as $coordinator) {
            if ($coordinators->contains($coordinator)) {
                //the product already exists do not overwrite
                $coordinators->removeElement($coordinator);
            } else {
                $coordinator->removeCoordinatedWarehouse($this);
            }
        }


        foreach ($coordinators as $coordinator) {
            $coordinator->addCoordinatedWarehouse($this);
        }

        return $this;
    }

    /**
     * @param User $coordinator
     * @return $this
     */
    public function addCoordinator(User $coordinator)
    {
        if (!$this->coordinators->contains($coordinator)) {
            $this->coordinators->add($coordinator);
            $coordinator->addCoordinatedWarehouse($this);
        }

        return $this;
    }

    /**
     * @param User $coordinator
     */
    public function removeCoordinator(User $coordinator)
    {
        if (!$this->coordinators->contains($coordinator)) {
            return $this;
        }
        $this->coordinators->removeElement($coordinator);

        return $this;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getMols()
    {
        return $this->mols;
    }

    /**
     * @param User[]|ArrayCollection $mols
     * @return ListSc
     */
    public function setMols($mols)
    {
        if (!$mols instanceof ArrayCollection)
            $mols = new ArrayCollection($mols);


        foreach ($this->mols as $mol) {
            if ($mols->contains($mol)) {
                //the product already exists do not overwrite
                $mols->removeElement($mol);
            } else {
                $mol->removeSubWarehouse($this);
            }
        }

        foreach ($mols as $mol) {
            $mol->addSubWarehouse($this);
        }

        return $this;
    }

    /**
     * @param User $mol
     * @return ListSc
     */
    public function addMol(User $mol)
    {
        if (!$this->mols->contains($mol)) {
            $this->mols->add($mol);
            $mol->addSubWarehouse($this);
        }

        return $this;
    }

    /**
     * @param User $mol
     * @return $this
     */
    public function removeMol(User $mol)
    {
        if (!$this->mols->contains($mol)) {
            return $this;
        }
        $this->mols->removeElement($mol);

        return $this;
    }


    /**
     * @return User[]|ArrayCollection
     */
    public function getSuperMols()
    {
        return $this->superMols;
    }

    /**
     * @param User[]|ArrayCollection $mols
     * @return ListSc
     */
    public function setSuperMols($mols)
    {
        if (!$mols instanceof ArrayCollection)
            $mols = new ArrayCollection($mols);


        foreach ($this->superMols as $mol) {
            if ($mols->contains($mol)) {
                //the product already exists do not overwrite
                $mols->removeElement($mol);
            } else {
                $mol->removeSuperSubWarehouse($this);
            }
        }

        foreach ($mols as $mol) {
            $mol->addSuperSubWarehouse($this);
        }

        return $this;
    }

    /**
     * @param User $mol
     * @return ListSc
     */
    public function addSuperMol(User $mol)
    {
        if (!$this->superMols->contains($mol)) {
            $this->superMols->add($mol);
            $mol->addSuperSubWarehouse($this);
        }

        return $this;
    }

    /**
     * @param User $mol
     * @return $this
     */
    public function removeSuperMol(User $mol)
    {
        if (!$this->superMols->contains($mol)) {
            return $this;
        }
        $this->superMols->removeElement($mol);

        return $this;
    }

    /**
     * @return User[]|PersistentCollection
     */
    public function getTechnicians()
    {
        return $this->technicians;
    }

    /**
     * @param User[]|ArrayCollection $technicians
     * @return ListSc
     */
    public function setTechnicians($technicians)
    {
        if (!$technicians instanceof ArrayCollection)
            $technicians = new ArrayCollection($technicians);

        foreach ($this->technicians as $technician) {
            if ($technicians->contains($technician)) {
                //the product already exists do not overwrite
                $technicians->removeElement($technician);
            } else {
                $technician->removeWarehouse($this);
            }
        }

        foreach ($technicians as $technician) {
            $technician->addWarehouse($this);
        }

        return $this;
    }

    /**
     * @param User $technician
     * @return ListSc
     */
    public function addTechnician(User $technician)
    {
        if (!$this->technicians->contains($technician))
            $this->technicians->add($technician);

        $technician->addWarehouse($this);

        return $this;
    }

    /**
     * @param User $technician
     * @return $this
     */
    public function removeTechnician(User $technician)
    {
        if (!$this->technicians->contains($technician)) {
            return $this;
        }
        $this->technicians->removeElement($technician);
        $technician->removeWarehouse($this);

        return $this;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getChiefs()
    {
        return $this->chiefs;
    }

    /**
     * @param User[]|ArrayCollection $chiefs
     * @return ListSc
     */
    public function setChiefs($chiefs)
    {
        if (!$chiefs instanceof ArrayCollection)
            $chiefs = new ArrayCollection($chiefs);

        foreach ($this->chiefs as $chief) {
            if ($chiefs->contains($chief)) {
                //the product already exists do not overwrite
                $chiefs->removeElement($chief);
            } else {
                $chief->removeSlaveServiceCenter($this);
            }
        }

        foreach ($chiefs as $chief) {
            $chief->addSlaveServiceCenter($this);
        }

        return $this;
    }

    /**
     * @param User $chief
     * @return ListSc
     */
    public function addChief(User $chief)
    {
        if (!$this->chiefs->contains($chief))
            $this->chiefs->add($chief);

        $chief->addSlaveServiceCenter($this);

        return $this;
    }

    /**
     * @param User $chief
     * @return $this
     */
    public function removeChief(User $chief)
    {
        if (!$this->chiefs->contains($chief)) {
            return $this;
        }

        $this->chiefs->removeElement($chief);

        $chief->removeSlaveServiceCenter($this);

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return ListSc
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return ListSc
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ListSc
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return array
     */
    public function getSerializedArea()
    {
        if ($this->getArea())
            return ['geometry' => ['type' => 'Polygon', 'coordinates' => $this->getArea()->toArray()]];

        return ['geometry' => ['type' => 'Polygon', 'coordinates' => []]];
    }

    /**
     * @return Polygon
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param Polygon $area
     * @return ListSc
     */
    public function setArea(Polygon $area)
    {
        $this->area = $area;
        return $this;
    }

    public function __toString()
    {
        return sprintf("%s", $this->getName());
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param Region $region
     * @return ListSc
     */
    public function setRegion(Region $region): ListSc
    {
        $this->region = $region;
        return $this;
    }

    /**
     *
     * @return string
     * @Serializer\VirtualProperty()
     * @Serializer\Groups({"default", "tmc.moves"})
     */
    public function getName()
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

}

