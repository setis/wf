<?php

namespace models;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gorserv\Gerp\ScheduleBundle\Model\JobInterface;
use JMS\Serializer\Annotation as Serializer;
use WF\Core\TaskInterface;
use WF\Task\Model\BaseTicketInterface;

/**
 * Task
 *
 * @ORM\Table(name="tasks", indexes={
 *     @ORM\Index(columns={"plugin_uid"}),
 *     @ORM\Index(columns={"author_id"}),
 *     @ORM\Index(columns={"status_id", "id"}),
 *     @ORM\Index(columns={"id", "status_id"}),
 *     @ORM\Index(columns={"task_title"}),
 *     @ORM\Index(columns={"lastcmm_date"})
 * })
 * @ORM\Entity(repositoryClass="repository\TaskRepository")
 */
class Task implements TaskInterface, JobInterface
{

    const PROJECT = 'projects';
    const BILL = 'bills';
    const SERVICE = 'services';
    const ACCIDENT = 'accidents';
    const CONNECTION = 'connections';
    const GLOBAL_PROBLEM = 'gp';
    const TELEMARKETING = 'tm_module';
    const AGREEMENT = 'agreements';

    const ACCIDENT_DURATION = 'PT1H';
    const CONNECTION_DURATION = 'PT2H';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="plugin_uid", type="string", length=20, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $pluginUid;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_reg", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     * @Serializer\Groups({"default"})
     */
    private $dateReg;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_fn", type="datetime", nullable=true)
     */
    private $delayedTo;
    /**
     * @var string
     *
     * @ORM\Column(name="task_title", type="string", length=255, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $taskTitle;
    /**
     * @var string
     *
     * @ORM\Column(name="task_comment", type="text", length=16777215, nullable=true)
     */
    private $taskComment;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="lastcmm_date", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="change", field={"$lastcmmId", "$lastcmmUserId"})
     */
    private $lastcmmDate;
    /**
     * @var integer
     *
     * @ORM\Column(name="lastcmm_user_id", type="integer", nullable=true)
     */
    private $lastcmmUserId;
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_private", type="boolean", nullable=false)
     */
    private $isPrivate;
    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_status_date", type="datetime", nullable=true)
     */
    private $lastStatusDate;
    /**
     * @var string
     *
     * @ORM\Column(name="questionnaire", type="text", length=16777215, nullable=true)
     */
    private $questionnaire;
    /**
     * @var string
     *
     * @ORM\Column(name="loyalty", type="text", length=16777215, nullable=true)
     */
    private $loyalty;
    /**
     * @var int
     *
     * @ORM\Column(name="relevance_level", type="integer", nullable=true)
     */
    private $relevanceLevel;
    /**
     * @var ListTtTechCalc[]
     * @ORM\OneToMany(targetEntity="models\ListTtTechCalc", mappedBy="task")
     * @Serializer\Groups({"task.ListTtTechCalc"})
     */
    private $listTtTechCalc;
    /**
     * @var ListTechCalc[]
     * @ORM\OneToMany(targetEntity="models\ListTechCalc", mappedBy="task")
     * @Serializer\Groups({"task.ListTechCalc"})
     */
    private $listTechCalc;
    /**
     * @var ListContrCalc[]
     * @ORM\OneToMany(targetEntity="models\ListContrCalc", mappedBy="task")
     * @Serializer\Groups({"task.ListContrCalc"})
     */
    private $listContrCalc;
    /**
     * @var ListTtContrCalc[]
     * @ORM\OneToMany(targetEntity="models\ListTtContrCalc", mappedBy="task")
     * @Serializer\Groups({"task.ListTtContrCalc"})
     */
    private $listTtContrCalc;
    /**
     * @var Ticket
     *
     * @ORM\OneToOne(targetEntity="models\Ticket", mappedBy="task")
     * @Serializer\Groups({"Task.Ticket"})
     */
    private $ticket;
    /**
     * @var Gp
     *
     * @ORM\OneToOne(targetEntity="models\Gp", mappedBy="task", cascade={"persist", "remove"})
     * @Serializer\Groups({"task.gp"})
     */
    private $gp;
    /**
     * @var Bill
     *
     * @ORM\OneToOne(targetEntity="models\Bill", mappedBy="task", cascade={"persist", "remove"})
     * @Serializer\Groups({"task.bill"})
     */
    private $bill;
    /**
     * @var TaskComment[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="models\TaskComment", mappedBy="task", cascade={"persist", "remove"})
     * @ORM\OrderBy({"datetime" = "ASC"})
     * @Serializer\Groups({"task.comments"})
     */
    private $comments;
    /**
     * @var TaskFile[]
     *
     * @ORM\OneToMany(targetEntity="models\TaskFile", mappedBy="task")
     * @Serializer\Groups({"task.files"})
     */
    private $files;
    /**
     * @var TaskPhoto[]
     *
     * @ORM\OneToMany(targetEntity="models\TaskPhoto", mappedBy="task")
     * @Serializer\Groups({"task.photos"})
     */
    private $photos;
    /**
     * @var TaskUserLink[]
     *
     * @ORM\OneToMany(targetEntity="models\TaskUserLink", mappedBy="task")
     */
    private $taskUsers;
    /**
     * @var TmcTicket[]
     *
     * @ORM\OneToMany(targetEntity="models\TmcTicket", mappedBy="task")
     */
    private $tmcTickets;
    /**
     *
     * @var TaskStatus
     *
     * @ORM\ManyToOne(targetEntity="TaskStatus")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    private $status;
    /**
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $author;
    /**
     * @var TmcMove[]
     *
     * @ORM\OneToMany(targetEntity="models\TmcMove", mappedBy="task")
     */
    private $tmcMoves;
    /**
     * @var Gfx
     *
     * @ORM\OneToOne(targetEntity="models\Gfx", mappedBy="task", cascade={"persist", "remove"})
     * @Serializer\Groups({"task.schedule"})
     */
    private $schedule;
    /**
     * @var TaskComment
     *
     * @ORM\OneToOne(targetEntity="TaskComment", cascade={"persist"})
     * @ORM\JoinColumn(name="lastcmm_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $lastComment;
    /**
     * @var TaskAddressInfo[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="TaskAddressInfo", mappedBy="task", cascade={"persist", "remove"})
     */
    private $addressInfo;
    /**
     *
     * @var Project
     *
     * @ORM\OneToOne(targetEntity="Project", mappedBy="task")
     */
    private $project;
    /**
     *
     * @var TaskQc
     *
     * @ORM\OneToOne(targetEntity="TaskQc", mappedBy="task")
     */
    private $qc;
    /**
     *
     * @var TaskQcResult[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="TaskQcResult", mappedBy="task", cascade={"persist", "remove"})
     */
    private $qcResults;
    /**
     * @var Task[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Task", mappedBy="slaveTasks")
     */
    private $masterTasks;
    /**
     * @var Task[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Task", inversedBy="masterTasks")
     * @ORM\JoinTable(name="joinedtasks",
     *      joinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="joined_task_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $slaveTasks;
    /**
     * @var ListSkpTechCalc[]
     *
     * @ORM\OneToMany(targetEntity="models\ListSkpTechCalc", mappedBy="task", cascade={"persist", "remove"})
     */
    private $skpTechCalc;

    /**
     * Task constructor.
     */
    public function __construct()
    {
        $this->taskUsers = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->photos = new ArrayCollection();
        $this->addressInfo = new ArrayCollection();
        $this->qcResults = new ArrayCollection();
        $this->masterTasks = new ArrayCollection();
        $this->slaveTasks = new ArrayCollection();
        $this->lastcmmDate = new DateTime();
        $this->relevanceLevel = 0;
    }

    public function getAuthorId()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get dateReg
     *
     * @return DateTime
     */
    public function getDateReg()
    {
        return $this->dateReg;
    }

    /**
     * Set dateReg
     *
     * @param DateTime $dateReg
     *
     * @return Task
     */
    public function setDateReg($dateReg)
    {
        $this->dateReg = $dateReg;

        return $this;
    }

    /**
     * Get delayedTo
     *
     * @return DateTime
     */
    public function getDelayedTo()
    {
        return $this->delayedTo;
    }

    /**
     * Set delayedTo
     *
     * @param DateTime $delayedTo
     *
     * @return Task
     */
    public function setDelayedTo(DateTime $delayedTo = null)
    {
        $this->delayedTo = $delayedTo;

        return $this;
    }

    /**
     *
     * @return TaskStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     *
     * @param TaskStatus $status
     * @return Task
     */
    public function setStatus(TaskStatus $status = null)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get taskTitle
     *
     * @return string
     */
    public function getTaskTitle()
    {
        return $this->taskTitle;
    }

    /**
     * Set taskTitle
     *
     * @param string $taskTitle
     *
     * @return Task
     */
    public function setTaskTitle($taskTitle)
    {
        $this->taskTitle = $taskTitle;

        return $this;
    }

    /**
     * Get taskComment
     *
     * @return string
     */
    public function getTaskComment()
    {
        return $this->taskComment;
    }

    /**
     * Set taskComment
     *
     * @param string $taskComment
     *
     * @return Task
     */
    public function setTaskComment($taskComment)
    {
        $this->taskComment = $taskComment;

        return $this;
    }

    /**
     * Get lastcmmId
     *
     * @return integer
     * @deprecated Use getLastComment instead
     */
    public function getLastcmmId()
    {
        return $this->lastComment ? $this->lastComment->getId() : null;
    }

    /**
     * Get lastcmmDate
     *
     * @return DateTime
     * @deprecated Use getLastComment instead
     */
    public function getLastcmmDate()
    {
        return $this->lastComment ? $this->lastComment->getDatetime() : null;
    }

    /**
     * @param DateTime $lastcmmDate
     * @return $this
     */
    public function setLastcmmDate(DateTime $lastcmmDate)
    {
        $this->lastcmmDate = $lastcmmDate;

        return $this;
    }

    /**
     * Get lastcmmUserId
     *
     * @return integer
     * @deprecated Use getLastComment instead
     */
    public function getLastcmmUserId()
    {
        return $this->lastComment && $this->lastComment->getAuthor() ?
            $this->lastComment->getAuthor()->getId() : null;
    }

    /**
     * Get isPrivate
     *
     * @return boolean
     */
    public function getIsPrivate()
    {
        return $this->isPrivate;
    }

    /**
     * Set isPrivate
     *
     * @param boolean $isPrivate
     *
     * @return Task
     */
    public function setIsPrivate($isPrivate)
    {
        $this->isPrivate = $isPrivate;

        return $this;
    }

    /**
     * Get lastStatusDate
     *
     * @return DateTime
     */
    public function getLastStatusDate()
    {
        return $this->lastStatusDate;
    }

    /**
     * Set lastStatusDate
     *
     * @param DateTime $lastStatusDate
     *
     * @return Task
     */
    public function setLastStatusDate($lastStatusDate)
    {
        $this->lastStatusDate = $lastStatusDate;

        return $this;
    }

    /**
     * Add comment
     *
     * @param TaskComment $comment
     *
     * @return Task
     */
    public function addComment(TaskComment $comment)
    {
        $this->comments[] = $comment;
        $this->setLastComment($comment);
        return $this;
    }

    /**
     * Remove comment
     *
     * @param TaskComment $comment
     */
    public function removeComment(TaskComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return TaskComment[]
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add file
     *
     * @param TaskFile $file
     *
     * @return Task
     */
    public function addFile(TaskFile $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param TaskFile $file
     */
    public function removeFile(TaskFile $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add user
     *
     * @param TaskUserLink $user
     *
     * @return Task
     */
    public function addTaskUser(TaskUserLink $user)
    {
        $this->taskUsers[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param TaskUserLink $user
     */
    public function removeTaskUser(TaskUserLink $user)
    {
        $this->taskUsers->removeElement($user);
    }

    /**
     * Get users
     *
     * @return Collection
     */
    public function getTaskUsers()
    {
        return $this->taskUsers;
    }

    /**
     * Add TmcTicket
     *
     * @param TmcTicket $user
     *
     * @return Task
     */
    public function addTmcTicket(TmcTicket $tmcTicket)
    {
        $this->tmcTickets[] = $tmcTicket;

        return $this;
    }

    /**
     * Remove TmcTicket
     *
     * @param TmcTicket $user
     */
    public function removeTmcTicket(TaskUserLink $tmcTicket)
    {
        $this->tmcTickets->removeElement($tmcTicket);
    }

    /**
     * Get TmcTickets
     *
     * @return Collection
     */
    public function getTmcTickets()
    {
        return $this->tmcTickets;
    }

    /**
     * @return string
     */
    public function getQuestionnaire()
    {
        return $this->questionnaire;
    }

    /**
     * @param string $questionnaire
     * @return Task
     */
    public function setQuestionnaire($questionnaire)
    {
        $this->questionnaire = $questionnaire;
        return $this;
    }

    /**
     * @param TaskPhoto $photo
     * @return Task
     */
    public function addPhoto($photo)
    {
        if (!$this->photos->contains($photo)) {
            $this->photos->add($photo);
            $photo->setTask($this);
        }

        return $this;
    }

    /**
     * @param TaskPhoto $photo
     * @return Task
     */
    public function removePhoto($photo)
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
        }

        return $this;
    }

    /**
     * @return TaskPhoto[]
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    public function getAddressInfo()
    {
        return $this->addressInfo;
    }

    /**
     *
     * @return ListAddr[]|ArrayCollection
     */
    public function getAddresses()
    {
        return $this->addressInfo->map(function (TaskAddressInfo $info) {
            return $info->getAddress();
        });
    }

    public function addAddress(ListAddr $address)
    {
        $this->addressInfo->add(new TaskAddressInfo($this, $address));
    }

    public function addAddressInfo(TaskAddressInfo $addressInfo)
    {
        $addressInfo->setTask($this);
        $this->addressInfo->add($addressInfo);
    }

    public function getLastComment()
    {
        return $this->lastComment;
    }

    public function setLastComment(TaskComment $lastComment)
    {
        $this->lastComment = $lastComment;
        $this->lastcmmDate = $lastComment->getDatetime();
        $this->lastcmmUserId = $lastComment->getAuthor() ?
            $lastComment->getAuthor()->getId() : null;
        return $this;
    }

    public function defineLastComment()
    {
        if (count($this->comments) === 0) {
            return;
        }

        $comments = $this->comments->toArray();
        usort($comments, function (TaskComment $a, TaskComment $b) {
            if ($a->getDatetime() < $b->getDatetime()) return -1;
            if ($a->getDatetime() > $b->getDatetime()) return 1;
            return 0;
        });

        $lastComment = $this->comments->last();
        if ($lastComment) {
            $this->setLastComment($lastComment);
        }
    }

    public function getQc()
    {
        return $this->qc;
    }

    public function setQc(TaskQc $qc = null)
    {
        $this->qc = $qc;
        return $this;
    }

    public function getQcResults()
    {
        return $this->qcResults;
    }

    /**
     * @return string
     */
    public function getLoyalty()
    {
        return $this->loyalty;
    }

    /**
     * @param string $loyalty
     * @return Task
     */
    public function setLoyalty($loyalty)
    {
        $this->loyalty = $loyalty;
        return $this;
    }

    /**
     * @return int
     */
    public function getRelevanceLevel()
    {
        return $this->relevanceLevel;
    }

    /**
     * @param int $relevanceLevel
     * @return Task
     */
    public function setRelevanceLevel($relevanceLevel)
    {
        $this->relevanceLevel = $relevanceLevel;
        return $this;
    }

    /**
     * @return BaseTicketInterface
     */
    public function getAnyTicket()
    {
        switch ($this->getPluginUid()) {
            case Task::BILL:
                return $this->getBill();
            break;
            case Task::PROJECT:
                return $this->getProject();
                break;
            case Task::GLOBAL_PROBLEM:
                return $this->getGp();
                break;
            case Task::ACCIDENT:
            case Task::CONNECTION:
            case Task::SERVICE:
            case Task::TELEMARKETING:
                return $this->getTicket();
                break;
            default:
                throw new \LogicException('Task type - '.$this->getPluginUid().' does not have ticket!');
        }
    }

    /**
     * Get pluginUid
     *
     * @return string
     */
    public function getPluginUid()
    {
        return $this->pluginUid;
    }

    /**
     * Set pluginUid
     *
     * @param string $pluginUid
     *
     * @return Task
     */
    public function setPluginUid($pluginUid)
    {
        $this->pluginUid = $pluginUid;

        return $this;
    }

    /**
     * @return Bill
     */
    public function getBill()
    {
        return $this->bill;
    }

    /**
     * @param Bill $bill
     * @return Task
     */
    public function setBill(Bill $bill)
    {
        $this->bill = $bill;
        return $this;
    }

    /**
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     *
     * @param Project $project
     * @return Task
     */
    public function setProject(Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return Gp
     */
    public function getGp()
    {
        return $this->gp;
    }

    /**
     * @param Gp $gp
     * @return Task
     */
    public function setGp($gp)
    {
        $this->gp = $gp;
        return $this;
    }

    /**
     * Get ticket
     *
     * @return Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Set ticket
     *
     * @param Ticket $ticket
     *
     * @return Task
     */
    public function setTicket(Ticket $ticket = null)
    {
        $this->ticket = $ticket;

        return $this;
    }

    public function addSlaveTask(Task $task)
    {
        $this->slaveTasks->add($task);

        return $this;
    }

    public function addMasterTask(Task $task)
    {
        $this->masterTasks->add($task);

        return $this;
    }

    public function getSlot()
    {
        return $this->getSchedule();
    }

    /**
     * @return Gfx
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * @param Gfx $schedule
     * @return Task
     */
    public function setSchedule(Gfx $schedule = null)
    {
        $this->schedule = $schedule;
        if (null !== $schedule) {
            $schedule->setTask($this);
        }
        return $this;
    }

    /**
     * @return ListSkpTechCalc[]
     */
    public function getSkpTechCalc()
    {
        return $this->skpTechCalc;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     * @return Task
     */
    public function setAuthor(User $author = null)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return ListTtTechCalc[]
     */
    public function getListTtTechCalc()
    {
        return $this->listTtTechCalc;
    }

    /**
     * @return ListTechCalc[]
     */
    public function getListTechCalc()
    {
        return $this->listTechCalc;
    }

    /**
     * @return ListContrCalc[]
     */
    public function getListContrCalc()
    {
        return $this->listContrCalc;
    }

    /**
     * @return ListTtContrCalc[]
     */
    public function getListTtContrCalc()
    {
        return $this->listTtContrCalc;
    }

}

