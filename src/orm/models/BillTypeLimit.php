<?php

namespace models;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * BillTypeLimit
 *
 * @ORM\Table(name="bill_type_limits", indexes={
 *     @ORM\Index(columns={"limit_at"}), 
 *     @ORM\Index(columns={"bill_type_id"})
 * })
 * @ORM\Entity(repositoryClass="repository\BillTypeLimitRepository")
 */
class BillTypeLimit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="money_limit", type="integer", nullable=false)
     */
    private $moneyLimit = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="limit_at", type="integer", nullable=false)
     */
    private $limitAt;

    /**
     * @var \models\BillType
     *
     * @ORM\ManyToOne(targetEntity="models\BillType", inversedBy="limits")
     * @ORM\JoinColumn(name="bill_type_id", referencedColumnName="id")
     */
    private $billType;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set moneyLimit
     *
     * @param integer $moneyLimit
     *
     * @return BillTypeLimit
     */
    public function setMoneyLimit($moneyLimit)
    {
        $this->moneyLimit = $moneyLimit;

        return $this;
    }

    /**
     * Get moneyLimit
     *
     * @return integer
     */
    public function getMoneyLimit()
    {
        return $this->moneyLimit;
    }

    /**
     * Set limitAt
     *
     * @param DateTime $limitAt
     *
     * @return BillTypeLimit
     */
    public function setLimitAt(DateTime $limitAt)
    {
        $this->limitAt = $limitAt->getTimestamp();

        return $this;
    }

    /**
     * Get limitAt
     *
     * @return DateTime
     */
    public function getLimitAt()
    {
        if($this->limitAt) {
            return (new DateTime())->setTimestamp($this->limitAt);
        }

        return new DateTime();
    }

    /**
     * Set billType
     *
     * @param \models\BillType $billType
     *
     * @return BillTypeLimit
     */
    public function setBillType(\models\BillType $billType = null)
    {
        $this->billType = $billType;

        return $this;
    }

    /**
     * Get billType
     *
     * @return \models\BillType
     */
    public function getBillType()
    {
        return $this->billType;
    }
}
