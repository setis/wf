<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListAgrDs
 *
 * @ORM\Table(name="list_agr_ds")
 * @ORM\Entity
 */
class ListAgrDs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="agr_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $agrId;

    /**
     * @var string
     *
     * @ORM\Column(name="simplename", type="text", length=16777215, precision=0, scale=0, nullable=false, unique=false)
     */
    private $simplename;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="text", length=16777215, precision=0, scale=0, nullable=false, unique=false)
     */
    private $subject;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sign_date", type="date", precision=0, scale=0, nullable=false, unique=false)
     */
    private $signDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin_date", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $finDate;

    /**
     * @var string
     *
     * @ORM\Column(name="ammount", type="decimal", precision=10, scale=2, nullable=true, unique=false)
     */
    private $ammount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inaction", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $inaction;

    /**
     * @var string
     *
     * @ORM\Column(name="doctoprint", type="text", length=16777215, precision=0, scale=0, nullable=false, unique=false)
     */
    private $doctoprint;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=16777215, precision=0, scale=0, nullable=false, unique=false)
     */
    private $comment;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return ListAgrDs
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set agrId
     *
     * @param integer $agrId
     *
     * @return ListAgrDs
     */
    public function setAgrId($agrId)
    {
        $this->agrId = $agrId;

        return $this;
    }

    /**
     * Get agrId
     *
     * @return integer
     */
    public function getAgrId()
    {
        return $this->agrId;
    }

    /**
     * Set simplename
     *
     * @param string $simplename
     *
     * @return ListAgrDs
     */
    public function setSimplename($simplename)
    {
        $this->simplename = $simplename;

        return $this;
    }

    /**
     * Get simplename
     *
     * @return string
     */
    public function getSimplename()
    {
        return $this->simplename;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return ListAgrDs
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set signDate
     *
     * @param \DateTime $signDate
     *
     * @return ListAgrDs
     */
    public function setSignDate($signDate)
    {
        $this->signDate = $signDate;

        return $this;
    }

    /**
     * Get signDate
     *
     * @return \DateTime
     */
    public function getSignDate()
    {
        return $this->signDate;
    }

    /**
     * Set finDate
     *
     * @param \DateTime $finDate
     *
     * @return ListAgrDs
     */
    public function setFinDate($finDate)
    {
        $this->finDate = $finDate;

        return $this;
    }

    /**
     * Get finDate
     *
     * @return \DateTime
     */
    public function getFinDate()
    {
        return $this->finDate;
    }

    /**
     * Set ammount
     *
     * @param string $ammount
     *
     * @return ListAgrDs
     */
    public function setAmmount($ammount)
    {
        $this->ammount = $ammount;

        return $this;
    }

    /**
     * Get ammount
     *
     * @return string
     */
    public function getAmmount()
    {
        return $this->ammount;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return ListAgrDs
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set inaction
     *
     * @param boolean $inaction
     *
     * @return ListAgrDs
     */
    public function setInaction($inaction)
    {
        $this->inaction = $inaction;

        return $this;
    }

    /**
     * Get inaction
     *
     * @return boolean
     */
    public function getInaction()
    {
        return $this->inaction;
    }

    /**
     * Set doctoprint
     *
     * @param string $doctoprint
     *
     * @return ListAgrDs
     */
    public function setDoctoprint($doctoprint)
    {
        $this->doctoprint = $doctoprint;

        return $this;
    }

    /**
     * Get doctoprint
     *
     * @return string
     */
    public function getDoctoprint()
    {
        return $this->doctoprint;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ListAgrDs
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
}

