<?php

namespace models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BillType
 *
 * @ORM\Table(name="bill_types")
 * @ORM\Entity(repositoryClass="repository\BillTypeRepository")
 */
class BillType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="models\BillTypeLimit", mappedBy="billType", cascade={"persist", "remove"})
     */
    private $limits;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="models\Bill", mappedBy="type", cascade={"persist","remove"})
     */
    private $bills;

    /**
     * @var \models\BusinessUnit
     *
     * @ORM\ManyToOne(targetEntity="models\BusinessUnit", inversedBy="types")
     * @ORM\JoinColumn(name="business_unit_id", referencedColumnName="id")
     */
    private $businessUnit;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="models\BillPayment", mappedBy="type", cascade={"persist"})
     */
    private $payments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->limits = new ArrayCollection();
        $this->bills = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return BillType
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BillType
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add limit
     *
     * @param BillTypeLimit $limit
     *
     * @return BillType
     */
    public function addLimit(BillTypeLimit $limit)
    {
        $this->limits[] = $limit;

        $limit->setBillType($this);

        return $this;
    }

    /**
     * Remove limit
     *
     * @param BillTypeLimit $limit
     */
    public function removeLimit(BillTypeLimit $limit)
    {
        $this->limits->removeElement($limit);
    }

    /**
     * Get limits
     *
     * @return Collection
     */
    public function getLimits()
    {
        return $this->limits;
    }

    /**
     * Add bill
     *
     * @param Bill $bill
     *
     * @return BillType
     */
    public function addBill(Bill $bill)
    {
        $this->bills[] = $bill;

        return $this;
    }

    /**
     * Remove bill
     *
     * @param Bill $bill
     */
    public function removeBill(Bill $bill)
    {
        $this->bills->removeElement($bill);
    }

    /**
     * Get bills
     *
     * @return Collection
     */
    public function getBills()
    {
        return $this->bills;
    }

    /**
     * @return BusinessUnit
     */
    public function getBusinessUnit()
    {
        return $this->businessUnit;
    }

    /**
     * @param BusinessUnit $businessUnit
     * @return BillType
     */
    public function setBusinessUnit($businessUnit)
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }
}
