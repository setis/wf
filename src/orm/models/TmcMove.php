<?php

namespace models;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use RuntimeException;
use WF\Core\TaskInterface;
use WF\Tmc\Model\TmcCarrierInterface;
use WF\Tmc\Model\TmcCoordinatorInterface;
use WF\Tmc\Model\TmcHolderInterface;
use WF\Tmc\Model\TmcMatcherInterface;
use WF\Tmc\Model\TmcMolInterface;
use WF\Tmc\Model\TmcMoveInterface;
use WF\Tmc\Model\TmcUserInterface;

/**
 * Description of TmcMove
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 *
 * @ORM\Table(name="tmc_move", indexes={
 *     @ORM\Index(columns={"move_key"}),
 *     @ORM\Index(columns={"state"})
 * })
 * @ORM\Entity(repositoryClass="repository\TmcMoveRepository")
 * @ORM\HasLifecycleCallbacks
 */
class TmcMove implements TmcMoveInterface
{

    static private $states = [
        self::STATE_MOVED => 'Перемещено',
        self::STATE_FROM_PARTNER => 'От поставщика',
        self::STATE_TO_PARTNER => 'Поставщику',
        self::STATE_WRITE_OFF => 'Списано',
        self::STATE_TRANSFERRED => 'Передача',
        self::STATE_REQUEST => 'Запрос',
        self::STATE_REQUEST_CONFIRMED => 'Запрос подтверждён',
        self::STATE_RETURN_REQUEST => 'Запрос на возврат',
        self::STATE_REQUEST_APPROVED => 'Запрос одобрен',
        self::STATE_INSTALLED => 'Инсталляция',
        self::STATE_UNINSTALLED => 'Демонтаж',
        self::STATE_MARK_DEFECT => 'Брак',
        self::STATE_TRANSPORT_REQUEST => 'Запрос транспортировки',
        self::STATE_IS_TRANSPORTED => 'Транспортировка',
        self::STATE_CANCELED => 'Отмена',
        self::STATE_ITEMS_CHANGED => 'Изменение состава',
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="move_key", type="string", length=23, options={"fixed" = true})
     * @Serializer\Groups({"default"})
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=24)
     * @Serializer\Groups({"default"})
     */
    private $state;

    /**
     *
     * @var Task
     *
     * @ORM\ManyToOne(targetEntity="models\Task", cascade={"persist"}, inversedBy="tmcMoves")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * @Serializer\Groups({"extended", "TmcMove.Task"})
     */
    private $task;

    /**
     *
     * @var TmcHolderLink
     *
     * @ORM\OneToOne(targetEntity="models\TmcHolderLink", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="source_holder_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $sourceHolderLink;

    /**
     *
     * @var TmcHolderLink
     *
     * @ORM\OneToOne(targetEntity="models\TmcHolderLink", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="target_holder_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $targetHolderLink;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="approved_by_id", referencedColumnName="id")
     * @Serializer\Groups({"extended", "tmc.moves", "TmcMove.ApprovedBy"})
     */
    private $approvedBy;

    /**
     *
     * @var TmcMove
     *
     * @ORM\OneToOne(targetEntity="models\TmcMove", cascade={"persist"})
     * @ORM\JoinColumn(name="prev_move_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Groups({"full"})
     * @Serializer\MaxDepth(1)
     */
    private $previousMove;

    /**
     *
     * @var TmcItem[]
     *
     * @ORM\OneToMany(targetEntity="models\TmcItem", mappedBy="move", cascade={"persist", "remove"})
     * @Serializer\Groups({"extended", "TmcMove.Items"})
     */
    private $items;

    /**
     * @var TmcHolderItem[]
     *
     * @ORM\OneToMany(targetEntity="models\TmcHolderItem", mappedBy="move", cascade={"persist", "remove"})
     * @Serializer\Groups({"extended", "TmcMove.MoveItems"})
     */
    private $holderItems;

    /**
     *
     * @var DateTime
     *
     * @ORM\Column(name="move_date", type="datetime")
     * @Serializer\Groups({"default", "tmc.moves"})
     */
    private $moveDate;

    /** @ORM\PrePersist */
    public function onPrePersist()
    {
        if (empty($this->previousMove)) {
            $this->key = uniqid('', true);
        }
    }

    /** @ORM\PreRemove */
    public function checkMoveState()
    {
        if ($this->state != TmcMoveInterface::STATE_INSTALLED && $this->state != TmcMoveInterface::STATE_UNINSTALLED) {
            throw new RuntimeException('Can\'t remove wrong state move!');
        }
    }

    public function __construct()
    {
        $this->holderItems = new ArrayCollection();
    }

    public static function getStateMemo($state){
        return isset(self::$states[$state]) ? self::$states[$state] : $state;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getTask()
    {
        return $this->task;
    }

    /**
     *
     * @return TmcHolderInterface
     * @Serializer\Groups({"extended", "tmc.moves", "tmc.report", "TmcMove.SourceHolder"})
     * @Serializer\VirtualProperty
     */
    public function getSourceHolder()
    {
        return $this->sourceHolderLink ?
            $this->sourceHolderLink->getHolder() :
            null;
    }

    /**
     * @return TmcHolderLink
     */
    public function getSourceHolderLink()
    {
        return $this->sourceHolderLink;
    }

    /**
     *
     * @return TmcHolderInterface
     * @Serializer\Groups({"extended", "tmc.moves", "tmc.report", "TmcMove.TargetHolder"})
     * @Serializer\VirtualProperty
     */
    public function getTargetHolder()
    {
        return $this->targetHolderLink ?
            $this->targetHolderLink->getHolder() :
            null;
    }

    /**
     * @return TmcHolderLink
     */
    public function getTargetHolderLink()
    {
        return $this->targetHolderLink;
    }

    /**
     * @return User|TmcMatcherInterface
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param TmcUserInterface $approvedBy
     *
     * @return TmcMove
     */
    public function setApprovedBy(TmcUserInterface $approvedBy)
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    public function getMoveDate()
    {
        return $this->moveDate;
    }

    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @param TaskInterface $task
     *
     * @return $this
     */
    public function setTask(TaskInterface $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * @param TmcHolderInterface|null $sourceHolderLink
     *
     * @return $this
     */
    public function setSourceHolder(TmcHolderInterface $sourceHolderLink = null)
    {
        if ($sourceHolderLink === null) {
            $this->sourceHolderLink = null;

            return $this;
        }

        if ($this->sourceHolderLink === null) {
            $this->sourceHolderLink = new TmcHolderLink();
        }

        $this->sourceHolderLink->setHolder($sourceHolderLink);

        return $this;
    }

    /**
     * @param TmcHolderInterface|null $targetHolderLink
     *
     * @return $this
     */
    public function setTargetHolder(TmcHolderInterface $targetHolderLink = null)
    {
        if ($targetHolderLink === null) {
            $this->targetHolderLink = null;

            return $this;
        }

        if ($this->targetHolderLink === null) {
            $this->targetHolderLink = new TmcHolderLink();
        }
        $this->targetHolderLink->setHolder($targetHolderLink);

        return $this;
    }

    public function setMoveDate(DateTime $moveDate)
    {
        $this->moveDate = $moveDate;

        return $this;
    }

    /**
     * @return TmcMove
     */
    public function getPreviousMove()
    {
        return $this->previousMove;
    }

    /**
     * @param TmcMove|TmcMoveInterface $previousMove
     *
     * @return TmcMove
     */
    public function setPreviousMove(TmcMoveInterface $previousMove)
    {
        $this->previousMove = $previousMove;
        $this->key = $previousMove->getKey();

        return $this;
    }

    /**
     * @param TmcItem[] $items
     *
     * @return TmcMove
     */
    public function setItems($items)
    {
        $this->items = new ArrayCollection();

        if (!empty($items)) {
            foreach ($items as $item) {
                $cloneItem = clone $item;
                $cloneItem->setMove($this);
                $this->items->add($cloneItem);
            }
        }

        return $this;
    }

    /**
     *
     * @return TmcItem[]|ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getKey()
    {
        return $this->key;
    }

    public function toState($state)
    {
        $move = new self;

        $move->setState($state)
            ->setItems($this->getItems())
            ->setPreviousMove($this)
            ->setTask($this->getTask())
            ->setSourceHolder($this->getSourceHolder())
            ->setTargetHolder($this->getTargetHolder())
            ->setMoveDate(new DateTime());

        return $move;
    }

    /**
     * Interface's methods
     */

    /**
     * @return User|TmcMatcherInterface
     */
    public function getMoveApprovedBy()
    {
        return $this->getApprovedBy();
    }

    /**
     * @param TmcUserInterface $approvedBy
     *
     * @return $this
     */
    public function setMoveApprovedBy(TmcUserInterface $approvedBy)
    {
        $this->setApprovedBy($approvedBy);

        return $this;
    }

    /**
     * @return User|TmcCoordinatorInterface
     */
    public function getConfirmedBy()
    {
        return $this->getApprovedBy();
    }

    /**
     * @param TmcCoordinatorInterface|TmcMatcherInterface|User $confirmedBy
     *
     * @return TmcMove
     */
    public function setConfirmedBy(TmcCoordinatorInterface $confirmedBy)
    {
        $this->setApprovedBy($confirmedBy);

        return $this;
    }

    /**
     * @return User|TmcMatcherInterface
     */
    public function getCanceledBy()
    {
        return $this->getApprovedBy();
    }

    /**
     * @param TmcUserInterface $canceledBy
     *
     * @return TmcMove
     */
    public function setCanceledBy(TmcUserInterface $canceledBy)
    {
        $this->setApprovedBy($canceledBy);

        return $this;
    }

    /**
     * @return User|TmcCarrierInterface
     */
    public function getShippedBy()
    {
        return $this->getApprovedBy();
    }

    /**
     * @param TmcCarrierInterface|TmcMatcherInterface|User $shippedBy
     *
     * @return TmcMove
     */
    public function setShippedBy(TmcCarrierInterface $shippedBy)
    {
        $this->setApprovedBy($shippedBy);

        return $this;
    }


    /**
     * @param TmcMolInterface|TmcMatcherInterface|User $requestApprovedBy
     *
     * @return TmcMove
     */
    public function setRequestApprovedBy(TmcMolInterface $requestApprovedBy)
    {
        $this->setApprovedBy($requestApprovedBy);

        return $this;
    }

    /**
     * @return User|TmcMatcherInterface
     */
    public function getRequestApprovedBy()
    {
        return $this->getApprovedBy();
    }

    /**
     * @return TmcHolderItem[]|ArrayCollection
     */
    public function getHolderItems()
    {
        return $this->holderItems;
    }
}
