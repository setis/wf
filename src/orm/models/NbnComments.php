<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * NbnComments
 *
 * @ORM\Table(name="nbn_comments")
 * @ORM\Entity
 */
class NbnComments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $taskId;

    /**
     * @var integer
     *
     * @ORM\Column(name="cmm_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $cmmId;


    /**
     * Set taskId
     *
     * @param integer $taskId
     *
     * @return NbnComments
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set cmmId
     *
     * @param integer $cmmId
     *
     * @return NbnComments
     */
    public function setCmmId($cmmId)
    {
        $this->cmmId = $cmmId;

        return $this;
    }

    /**
     * Get cmmId
     *
     * @return integer
     */
    public function getCmmId()
    {
        return $this->cmmId;
    }
}

