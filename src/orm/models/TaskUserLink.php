<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskUserLink
 *
 * @ORM\Table(name="task_users", uniqueConstraints={@ORM\UniqueConstraint(columns={"ispoln", "user_id", "task_id"})})
 * @ORM\Entity
 */
class TaskUserLink
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ispoln", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $ispoln;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $time;

    /**
     * @var \models\Task
     *
     * @ORM\ManyToOne(targetEntity="models\Task", inversedBy="taskUsers", cascade={"persist"})
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $task;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User", inversedBy="userTasks", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ispoln
     *
     * @param boolean $ispoln
     *
     * @return TaskUserLink
     */
    public function setIspoln($ispoln)
    {
        $this->ispoln = $ispoln;

        return $this;
    }

    /**
     * Get ispoln
     *
     * @return boolean
     */
    public function getIspoln()
    {
        return $this->ispoln;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return TaskUserLink
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }


    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param Task $task
     * @return TaskUserLink
     */
    public function setTask(Task $task)
    {
        $this->task = $task;
        $this->task->addTaskUser($this);
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return TaskUserLink
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        $this->user->addUserTask($this);
        return $this;
    }

}

