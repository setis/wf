<?php
namespace models;


use Doctrine\ORM\Mapping as ORM;

/**
 * SalesChannels
 *
 * @ORM\Table(name="sales_channels", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"channel_name"})
 * })
 * @ORM\Entity
 */
class SalesChannels
{
    /**
     * @var string
     *
     * @ORM\Column(name="channel_name", type="string", length=200, nullable=false)
     */
    private $channelName;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set channelName
     *
     * @param string $channelName
     *
     * @return SalesChannels
     */
    public function setChannelName($channelName)
    {
        $this->channelName = $channelName;

        return $this;
    }

    /**
     * Get channelName
     *
     * @return string
     */
    public function getChannelName()
    {
        return $this->channelName;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

