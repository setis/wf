<?php

namespace models;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Task\Model\TechnicalTicketInterface;

/**
 * Gp
 *
 * @ORM\Table(name="gp")
 * @ORM\Entity
 */
class Gp implements TechnicalTicketInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="operator_key", type="string", length=50, nullable=true)
     */
    private $operatorKey;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="operator_start", type="datetime", nullable=true)
     */
    private $operatorStart;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="operator_2gss", type="datetime", nullable=true)
     */
    private $operator2gss;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="close_time", type="datetime", nullable=true)
     */
    private $closeTime;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    private $author;

    /**
     * @var integer
     *
     * @ORM\Column(name="agr_id", type="integer", nullable=true)
     */
    private $agrId;

    /**
     * @var string
     *
     * @ORM\Column(name="op_city", type="string", length=50, nullable=true)
     */
    private $opCity;

    /**
     * @var string
     *
     * @ORM\Column(name="op_area", type="string", length=50, nullable=true)
     */
    private $opArea;

    /**
     * @var integer
     *
     * @ORM\Column(name="op_b2b", type="integer", nullable=true)
     */
    private $opB2b;

    /**
     * @var integer
     *
     * @ORM\Column(name="op_b2c", type="integer", nullable=true)
     */
    private $opB2c;

    /**
     * @var integer
     *
     * @ORM\Column(name="gpsubtype_id", type="integer", nullable=true)
     */
    private $gpSubTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="doc_ticket", type="integer", nullable=true)
     */
    private $docTicket;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="doc_date", type="date", nullable=true)
     */
    private $docDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="tmc_ticket", type="integer", nullable=true)
     */
    private $tmcTicket;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="tmc_date", type="date", nullable=true)
     */
    private $tmcDate;

    /**
     * @var Task
     *
     * @ORM\OneToOne(targetEntity="Task", inversedBy="gp")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id", unique=true, onDelete="CASCADE")
     * })
     * @Serializer\Groups({"gp.task"})
     */
    private $task;

    /**
     *
     * @var ListContr
     *
     * @ORM\ManyToOne(targetEntity="ListContr")
     * @ORM\JoinColumn(name="cnt_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @Serializer\Groups({"gp.partner"})
     */
    private $partner;

    /**
     *
     * @var ListSc
     *
     * @ORM\ManyToOne(targetEntity="ListSc")
     * @ORM\JoinColumn(name="sc_id", referencedColumnName="id", onDelete="NO ACTION")
     * @Serializer\Groups({"gp.serviceCenter"})
     */
    private $serviceCenter;

    /**
     * @var TaskType
     *
     * @ORM\ManyToOne(targetEntity="TaskType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $taskType;

    /**
     * @var GpType
     *
     * @ORM\ManyToOne(targetEntity="GpType")
     * @ORM\JoinColumn(name="gptype_id", referencedColumnName="id")
     */
    private $gpType;

    /**
     * @var TaskSource
     *
     * @ORM\ManyToOne(targetEntity="TaskSource")
     * @ORM\JoinColumn(name="task_from", referencedColumnName="id", onDelete="SET NULL")
     */
    private $taskSource;

    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->task->getId();
    }

    /**
     * Get operatorKey
     *
     * @return string
     */
    public function getOperatorKey()
    {
        return $this->operatorKey;
    }

    /**
     * Set operatorKey
     *
     * @param string $operatorKey
     *
     * @return Gp
     */
    public function setOperatorKey($operatorKey)
    {
        $this->operatorKey = $operatorKey;

        return $this;
    }

    /**
     * Get operatorStart
     *
     * @return DateTime
     */
    public function getOperatorStart()
    {
        return $this->operatorStart;
    }

    /**
     * Set operatorStart
     *
     * @param DateTime $operatorStart
     *
     * @return Gp
     */
    public function setOperatorStart(DateTime $operatorStart = null)
    {
        $this->operatorStart = $operatorStart;

        return $this;
    }

    /**
     * Get operator2gss
     *
     * @return DateTime
     */
    public function getOperator2gss()
    {
        return $this->operator2gss;
    }

    /**
     * Set operator2gss
     *
     * @param DateTime $operator2gss
     *
     * @return Gp
     */
    public function setOperator2gss(DateTime $operator2gss = null)
    {
        $this->operator2gss = $operator2gss;

        return $this;
    }

    /**
     * Get closeTime
     *
     * @return DateTime
     */
    public function getCloseTime()
    {
        return $this->closeTime;
    }

    /**
     * Set closeTime
     *
     * @param DateTime $closeTime
     *
     * @return Gp
     */
    public function setCloseTime($closeTime)
    {
        $this->closeTime = $closeTime;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Gp
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Gp
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Gp
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    public function getTaskSource()
    {
        return $this->taskSource;
    }

    public function setTaskSource(TaskSource $taskSource = null)
    {
        $this->taskSource = $taskSource;
        return $this;
    }

    public function getTaskType()
    {
        return $this->taskType;
    }

    public function setTaskType(TaskType $taskType)
    {
        $this->taskType = $taskType;
        return $this;
    }

    /**
     * Get typeId
     *
     * @return integer
     */
    public function getTypeId()
    {
        return $this->taskType->getId();
    }

    /**
     * Get agrId
     *
     * @return integer
     */
    public function getAgrId()
    {
        return $this->agrId;
    }

    /**
     * Set agrId
     *
     * @param integer $agrId
     *
     * @return Gp
     */
    public function setAgrId($agrId)
    {
        $this->agrId = $agrId;

        return $this;
    }

    /**
     * Get opCity
     *
     * @return string
     */
    public function getOpCity()
    {
        return $this->opCity;
    }

    /**
     * Set opCity
     *
     * @param string $opCity
     *
     * @return Gp
     */
    public function setOpCity($opCity)
    {
        $this->opCity = $opCity;

        return $this;
    }

    /**
     * Get opArea
     *
     * @return string
     */
    public function getOpArea()
    {
        return $this->opArea;
    }

    /**
     * Set opArea
     *
     * @param string $opArea
     *
     * @return Gp
     */
    public function setOpArea($opArea)
    {
        $this->opArea = $opArea;

        return $this;
    }

    /**
     * Get opB2b
     *
     * @return integer
     */
    public function getOpB2b()
    {
        return $this->opB2b;
    }

    /**
     * Set opB2b
     *
     * @param integer $opB2b
     *
     * @return Gp
     */
    public function setOpB2b($opB2b)
    {
        $this->opB2b = $opB2b;

        return $this;
    }

    /**
     * Get opB2c
     *
     * @return integer
     */
    public function getOpB2c()
    {
        return $this->opB2c;
    }

    /**
     * Set opB2c
     *
     * @param integer $opB2c
     *
     * @return Gp
     */
    public function setOpB2c($opB2c)
    {
        $this->opB2c = $opB2c;

        return $this;
    }

    /**
     * Get gpsubtypeId
     *
     * @return integer
     */
    public function getGpSubTypeId()
    {
        return $this->gpSubTypeId;
    }

    /**
     * Set gpsubtypeId
     *
     * @param integer $gpsubtypeId
     *
     * @return Gp
     */
    public function setGpSubTypeId($gpsubtypeId)
    {
        $this->gpSubTypeId = $gpsubtypeId;

        return $this;
    }

    /**
     * Get docTicket
     *
     * @return integer
     */
    public function getDocTicket()
    {
        return $this->docTicket;
    }

    /**
     * Set docTicket
     *
     * @param integer $docTicket
     *
     * @return Gp
     */
    public function setDocTicket($docTicket)
    {
        $this->docTicket = $docTicket;

        return $this;
    }

    /**
     * Get docDate
     *
     * @return DateTime
     */
    public function getDocDate()
    {
        return $this->docDate;
    }

    /**
     * Set docDate
     *
     * @param DateTime $docDate
     *
     * @return Gp
     */
    public function setDocDate($docDate)
    {
        $this->docDate = $docDate;

        return $this;
    }

    /**
     * Get tmcTicket
     *
     * @return integer
     */
    public function getTmcTicket()
    {
        return $this->tmcTicket;
    }

    /**
     * Set tmcTicket
     *
     * @param integer $tmcTicket
     *
     * @return Gp
     */
    public function setTmcTicket($tmcTicket)
    {
        $this->tmcTicket = $tmcTicket;

        return $this;
    }

    /**
     * Get tmcDate
     *
     * @return DateTime
     */
    public function getTmcDate()
    {
        return $this->tmcDate;
    }

    /**
     * Set tmcDate
     *
     * @param DateTime $tmcDate
     *
     * @return Gp
     */
    public function setTmcDate($tmcDate)
    {
        $this->tmcDate = $tmcDate;

        return $this;
    }

    /**
     * @return ListSc
     */
    public function getServiceCenter()
    {
        return $this->serviceCenter;
    }

    /**
     * @param ListSc $serviceCenter
     * @return Gp
     */
    public function setServiceCenter(ListSc $serviceCenter = null)
    {
        $this->serviceCenter = $serviceCenter;
        return $this;
    }

    /**
     * @return ListContr
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param ListContr $partner
     * @return Gp
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
        return $this;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function setTask(Task $task)
    {
        $this->task = $task;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getGpType()
    {
        return $this->gpType;
    }

    public function setGpType(GpType $gpType = null)
    {
        $this->gpType = $gpType;
        return $this;
    }

    public function getClnttnum()
    {
        return $this->operatorKey;
    }

    public function getDom()
    {
        return $this->getTask()->getAddresses()->first();
    }

}
