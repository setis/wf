<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * GpSubtypes
 *
 * @ORM\Table(name="gp_subtypes")
 * @ORM\Entity
 */
class GpSubtypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $typeId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ledit", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ledit;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeId
     *
     * @param integer $typeId
     *
     * @return GpSubtypes
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;

        return $this;
    }

    /**
     * Get typeId
     *
     * @return integer
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return GpSubtypes
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set ledit
     *
     * @param \DateTime $ledit
     *
     * @return GpSubtypes
     */
    public function setLedit($ledit)
    {
        $this->ledit = $ledit;

        return $this;
    }

    /**
     * Get ledit
     *
     * @return \DateTime
     */
    public function getLedit()
    {
        return $this->ledit;
    }
}

