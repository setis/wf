<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketsDocsProtocols
 *
 * @ORM\Table(name="tickets_docs_protocols")
 * @ORM\Entity
 */
class TicketsDocsProtocols
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="cr_usr", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $crUsr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cr_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $crDate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return TicketsDocsProtocols
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set crUsr
     *
     * @param integer $crUsr
     *
     * @return TicketsDocsProtocols
     */
    public function setCrUsr($crUsr)
    {
        $this->crUsr = $crUsr;

        return $this;
    }

    /**
     * Get crUsr
     *
     * @return integer
     */
    public function getCrUsr()
    {
        return $this->crUsr;
    }

    /**
     * Set crDate
     *
     * @param \DateTime $crDate
     *
     * @return TicketsDocsProtocols
     */
    public function setCrDate($crDate)
    {
        $this->crDate = $crDate;

        return $this;
    }

    /**
     * Get crDate
     *
     * @return \DateTime
     */
    public function getCrDate()
    {
        return $this->crDate;
    }
}

