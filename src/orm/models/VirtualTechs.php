<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * VirtualTechs
 *
 * @ORM\Table(name="virtual_techs")
 * @ORM\Entity
 */
class VirtualTechs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="virtual_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $virtualId;


    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return VirtualTechs
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set virtualId
     *
     * @param integer $virtualId
     *
     * @return VirtualTechs
     */
    public function setVirtualId($virtualId)
    {
        $this->virtualId = $virtualId;

        return $this;
    }

    /**
     * Get virtualId
     *
     * @return integer
     */
    public function getVirtualId()
    {
        return $this->virtualId;
    }
}

