<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Services
 *
 * @ORM\Table(name="services", indexes={
 *     @ORM\Index(columns={"task_type"}),
 *     @ORM\Index(columns={"cnt_id"}),
 *     @ORM\Index(columns={"sc_id"}),
 *     @ORM\Index(columns={"clnt_type"})
 * })
 * @ORM\Entity
 */
class Services
{
    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taskId;

    /**
     * @var integer
     *
     * @ORM\Column(name="cnt_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $cntId;

    /**
     * @var integer
     *
     * @ORM\Column(name="task_type", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $taskType;

    /**
     * @var integer
     *
     * @ORM\Column(name="dom_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $domId;

    /**
     * @var string
     *
     * @ORM\Column(name="pod", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $pod;

    /**
     * @var string
     *
     * @ORM\Column(name="etazh", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $etazh;

    /**
     * @var string
     *
     * @ORM\Column(name="kv", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $kv;

    /**
     * @var string
     *
     * @ORM\Column(name="domofon", type="string", length=20, precision=0, scale=0, nullable=true, unique=false)
     */
    private $domofon;

    /**
     * @var integer
     *
     * @ORM\Column(name="sc_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $scId;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=false, unique=false)
     */
    private $price;

    /**
     * @var boolean
     *
     * @ORM\Column(name="clnt_type", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $clntType;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_org_name", type="string", length=250, precision=0, scale=0, nullable=true, unique=false)
     */
    private $clntOrgName;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_fio", type="string", length=250, precision=0, scale=0, nullable=true, unique=false)
     */
    private $clntFio;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_passp_sn", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $clntPasspSn;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_passport_vid", type="string", length=250, precision=0, scale=0, nullable=true, unique=false)
     */
    private $clntPassportVid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="clnt_passport_viddate", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $clntPassportViddate;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_tel1", type="string", length=11, precision=0, scale=0, nullable=true, unique=false)
     */
    private $clntTel1;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_tel2", type="string", length=11, precision=0, scale=0, nullable=true, unique=false)
     */
    private $clntTel2;

    /**
     * @var integer
     *
     * @ORM\Column(name="reject_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $rejectId;


    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set cntId
     *
     * @param integer $cntId
     *
     * @return Services
     */
    public function setCntId($cntId)
    {
        $this->cntId = $cntId;

        return $this;
    }

    /**
     * Get cntId
     *
     * @return integer
     */
    public function getCntId()
    {
        return $this->cntId;
    }

    /**
     * Set taskType
     *
     * @param integer $taskType
     *
     * @return Services
     */
    public function setTaskType($taskType)
    {
        $this->taskType = $taskType;

        return $this;
    }

    /**
     * Get taskType
     *
     * @return integer
     */
    public function getTaskType()
    {
        return $this->taskType;
    }

    /**
     * Set domId
     *
     * @param integer $domId
     *
     * @return Services
     */
    public function setDomId($domId)
    {
        $this->domId = $domId;

        return $this;
    }

    /**
     * Get domId
     *
     * @return integer
     */
    public function getDomId()
    {
        return $this->domId;
    }

    /**
     * Set pod
     *
     * @param string $pod
     *
     * @return Services
     */
    public function setPod($pod)
    {
        $this->pod = $pod;

        return $this;
    }

    /**
     * Get pod
     *
     * @return string
     */
    public function getPod()
    {
        return $this->pod;
    }

    /**
     * Set etazh
     *
     * @param string $etazh
     *
     * @return Services
     */
    public function setEtazh($etazh)
    {
        $this->etazh = $etazh;

        return $this;
    }

    /**
     * Get etazh
     *
     * @return string
     */
    public function getEtazh()
    {
        return $this->etazh;
    }

    /**
     * Set kv
     *
     * @param string $kv
     *
     * @return Services
     */
    public function setKv($kv)
    {
        $this->kv = $kv;

        return $this;
    }

    /**
     * Get kv
     *
     * @return string
     */
    public function getKv()
    {
        return $this->kv;
    }

    /**
     * Set domofon
     *
     * @param string $domofon
     *
     * @return Services
     */
    public function setDomofon($domofon)
    {
        $this->domofon = $domofon;

        return $this;
    }

    /**
     * Get domofon
     *
     * @return string
     */
    public function getDomofon()
    {
        return $this->domofon;
    }

    /**
     * Set scId
     *
     * @param integer $scId
     *
     * @return Services
     */
    public function setScId($scId)
    {
        $this->scId = $scId;

        return $this;
    }

    /**
     * Get scId
     *
     * @return integer
     */
    public function getScId()
    {
        return $this->scId;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Services
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set clntType
     *
     * @param boolean $clntType
     *
     * @return Services
     */
    public function setClntType($clntType)
    {
        $this->clntType = $clntType;

        return $this;
    }

    /**
     * Get clntType
     *
     * @return boolean
     */
    public function getClntType()
    {
        return $this->clntType;
    }

    /**
     * Set clntOrgName
     *
     * @param string $clntOrgName
     *
     * @return Services
     */
    public function setClntOrgName($clntOrgName)
    {
        $this->clntOrgName = $clntOrgName;

        return $this;
    }

    /**
     * Get clntOrgName
     *
     * @return string
     */
    public function getClntOrgName()
    {
        return $this->clntOrgName;
    }

    /**
     * Set clntFio
     *
     * @param string $clntFio
     *
     * @return Services
     */
    public function setClntFio($clntFio)
    {
        $this->clntFio = $clntFio;

        return $this;
    }

    /**
     * Get clntFio
     *
     * @return string
     */
    public function getClntFio()
    {
        return $this->clntFio;
    }

    /**
     * Set clntPasspSn
     *
     * @param string $clntPasspSn
     *
     * @return Services
     */
    public function setClntPasspSn($clntPasspSn)
    {
        $this->clntPasspSn = $clntPasspSn;

        return $this;
    }

    /**
     * Get clntPasspSn
     *
     * @return string
     */
    public function getClntPasspSn()
    {
        return $this->clntPasspSn;
    }

    /**
     * Set clntPassportVid
     *
     * @param string $clntPassportVid
     *
     * @return Services
     */
    public function setClntPassportVid($clntPassportVid)
    {
        $this->clntPassportVid = $clntPassportVid;

        return $this;
    }

    /**
     * Get clntPassportVid
     *
     * @return string
     */
    public function getClntPassportVid()
    {
        return $this->clntPassportVid;
    }

    /**
     * Set clntPassportViddate
     *
     * @param \DateTime $clntPassportViddate
     *
     * @return Services
     */
    public function setClntPassportViddate($clntPassportViddate)
    {
        $this->clntPassportViddate = $clntPassportViddate;

        return $this;
    }

    /**
     * Get clntPassportViddate
     *
     * @return \DateTime
     */
    public function getClntPassportViddate()
    {
        return $this->clntPassportViddate;
    }

    /**
     * Set clntTel1
     *
     * @param string $clntTel1
     *
     * @return Services
     */
    public function setClntTel1($clntTel1)
    {
        $this->clntTel1 = $clntTel1;

        return $this;
    }

    /**
     * Get clntTel1
     *
     * @return string
     */
    public function getClntTel1()
    {
        return $this->clntTel1;
    }

    /**
     * Set clntTel2
     *
     * @param string $clntTel2
     *
     * @return Services
     */
    public function setClntTel2($clntTel2)
    {
        $this->clntTel2 = $clntTel2;

        return $this;
    }

    /**
     * Get clntTel2
     *
     * @return string
     */
    public function getClntTel2()
    {
        return $this->clntTel2;
    }

    /**
     * Set rejectId
     *
     * @param integer $rejectId
     *
     * @return Services
     */
    public function setRejectId($rejectId)
    {
        $this->rejectId = $rejectId;

        return $this;
    }

    /**
     * Get rejectId
     *
     * @return integer
     */
    public function getRejectId()
    {
        return $this->rejectId;
    }
}

