<?php
/**
 * mail@artemd.ru
 * 11.04.2016
 */

namespace models;

use Doctrine\ORM\Mapping as ORM;
use WF\Slack\Model\SlackMessageInterface;

/**
 * SlackMessage
 *
 * @ORM\Table(name="slack_messages")
 * @ORM\Entity
 */
class SlackMessage implements SlackMessageInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=16)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="attachments", type="text")
     */
    private $attachments;

    /**
     *
     * @var SlackChannel
     *
     * @ORM\ManyToOne(targetEntity="models\SlackChannel", inversedBy="messages")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id")
     */
    private $channel;

    public function __construct()
    {
        $this->channels = new \Doctrine\Common\Collections\ArrayCollection();
    }

     /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return SlackMessage
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return SlackMessage
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return SlackMessage
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return SlackChannel
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param SlackChannel $channel
     * @return SlackMessage
     */
    public function setChannel(SlackChannel $channel)
    {
        $this->channel = $channel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param mixed $attachments
     * @return SlackMessage
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;
        return $this;
    }
}
