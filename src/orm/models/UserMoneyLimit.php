<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListEmplMoneyLimits
 *
 * @ORM\Table(name="list_empl_money_limits")
 * @ORM\Entity
 */
class UserMoneyLimit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var User
     * 
     * @ORM\OneToOne(targetEntity="User", inversedBy="moneyLimit")
     * @ORM\JoinColumn(name="tech_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="limit1", type="decimal", precision=10, scale=2)
     */
    private $limit1;

    /**
     * @var string
     *
     * @ORM\Column(name="limit2", type="decimal", precision=10, scale=2)
     */
    private $limit2;


    public function getId()
    {
        return $this->id;
    }

    /**
     * 
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * 
     * @param User $user
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    
    /**
     * Set limit1
     *
     * @param string $limit1
     *
     * @return ListEmplMoneyLimits
     */
    public function setLimit1($limit1)
    {
        $this->limit1 = $limit1;

        return $this;
    }

    /**
     * Get limit1
     *
     * @return string
     */
    public function getLimit1()
    {
        return $this->limit1;
    }

    /**
     * Set limit2
     *
     * @param string $limit2
     *
     * @return ListEmplMoneyLimits
     */
    public function setLimit2($limit2)
    {
        $this->limit2 = $limit2;

        return $this;
    }

    /**
     * Get limit2
     *
     * @return string
     */
    public function getLimit2()
    {
        return $this->limit2;
    }
}

