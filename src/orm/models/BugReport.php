<?php
namespace models;

use Doctrine\ORM\Mapping as ORM;
use DateTime;


/**
 * BugReport
 *
 * @ORM\Table(name="bug_reports", options={"collate"="utf8_general_ci"})
 * @ORM\Entity
 */
class BugReport
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=20)
     * @ORM\Id
     *
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     *
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     * @ORM\Column(name="user_id", type="integer")
     */
    private $author;


    /**
     * @var string
     * @ORM\Column(name="executive", type="string", nullable=true)
     */
    private $executive;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="text")
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="failure", type="text")
     */
    private $failure;
    /**
     * @var string
     *
     * @ORM\Column(name="wish", type="text")
     */
    private $wish;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="done", type="datetime", nullable=true)
     */
    private $done;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=200)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=200)
     */
    private $phone;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text")
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="environment", type="json_array")
     */
    private $environment;

     /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return BugReport
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $action
     * @return BugReport
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
    /**
     * @return string
     */
    public function getFailure()
    {
        return $this->failure;
    }

    /**
     * @param string $failure
     * @return BugReport
     */
    public function setFailure($failure)
    {
        $this->failure = $failure;
        return $this;
    }

    /**
     * @return string
     */
    public function getWish()
    {
        return $this->wish;
    }

    /**
     * @param string $wish
     * @return BugReport
     */
    public function setWish($wish)
    {
        $this->wish = $wish;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * @param DateTime $done
     *
     * @return BugReport
     */
    public function setDone(DateTime $done)
    {
        $this->done = $done;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BugReport
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return BugReport
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return BugReport
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set Date
     *
     * @param DateTime $date
     *
     * @return BugReport
     */
    public function setDate(DateTime $date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return BugReport
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     * @return BugReport
     */
    public function setAuthor(User $author): BugReport
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string
     */
    public function getExecutive()
    {
        return $this->executive;
    }

    /**
     * @param string $executive
     * @return BugReport
     */
    public function setExecutive($executive)
    {
        $this->executive = $executive;
        return $this;
    }


    /**
     * @return string
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @param string $environment
     * @return BugReport
     */
    public function setEnvironment($environment)
    {
        $this->environment = $environment;
        return $this;
    }


}
