<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tarifs
 *
 * @ORM\Table(name="tarifs", indexes={
 *     @ORM\Index(columns={"tarif_type_id"})
 * })
 * @ORM\Entity
 */
class Tariff
{
    /**
     * @var string
     *
     * @ORM\Column(name="tarif_name", type="string", length=200, nullable=false)
     */
    private $tarifName;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_id", type="integer", nullable=true)
     */
    private $contrId;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var TariffType
     *
     * @ORM\ManyToOne(targetEntity="models\TariffType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tarif_type_id", referencedColumnName="id")
     * })
     */
    private $tarifType;


    /**
     * Set tarifName
     *
     * @param string $tarifName
     *
     * @return Tariff
     */
    public function setTarifName($tarifName)
    {
        $this->tarifName = $tarifName;

        return $this;
    }

    /**
     * Get tarifName
     *
     * @return string
     */
    public function getTarifName()
    {
        return $this->tarifName;
    }

    /**
     * Set contrId
     *
     * @param integer $contrId
     *
     * @return Tariff
     */
    public function setContrId($contrId)
    {
        $this->contrId = $contrId;

        return $this;
    }

    /**
     * Get contrId
     *
     * @return integer
     */
    public function getContrId()
    {
        return $this->contrId;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tarifType
     *
     * @param TariffType $tarifType
     *
     * @return Tariff
     */
    public function setTarifType(TariffType $tarifType = null)
    {
        $this->tarifType = $tarifType;

        return $this;
    }

    /**
     * Get tarifType
     *
     * @return TariffType
     */
    public function getTarifType()
    {
        return $this->tarifType;
    }
}

