<?php

namespace models;

use CrEOF\Spatial\PHP\Types\Geography\Polygon;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gorserv\Gerp\AppBundle\Entity\Money;
use Gorserv\Gerp\AppBundle\Model\UserInterface;
use Gorserv\Gerp\BillingBundle\Model\BalanceOwnerInterface;
use Gorserv\Gerp\BillingBundle\Model\MoneyInterface;
use Gorserv\Gerp\ScheduleBundle\Model\ExecutorInterface;
use Gorserv\Gerp\ScheduleBundle\Model\ScheduleOperatorInterface;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use WF\Tmc\Model\ChiefInterface;
use WF\Tmc\Model\TechnicianInterface;
use WF\Tmc\Model\TmcCarrierInterface;
use WF\Tmc\Model\TmcCoordinatorInterface;
use WF\Tmc\Model\TmcMolInterface;
use WF\Tmc\Model\TmcWarehouseInterface;
use WF\Users\Model\DispatcherInterface;
use WF\Users\Model\GenderInterface;

/**
 * User
 *
 * @ORM\Table(name="users",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(columns={"login"})
 *     },
 *     indexes={@ORM\Index(columns={"active"})}
 * )
 * @ORM\Entity(repositoryClass="repository\UserRepository")
 * @Vich\Uploadable
 * @UniqueEntity("login")
 */
class User implements BalanceOwnerInterface, AdvancedUserInterface, TmcMolInterface, TechnicianInterface, ChiefInterface, TmcCoordinatorInterface, TmcCarrierInterface, GenderInterface, DispatcherInterface, ExecutorInterface, ScheduleOperatorInterface, \Serializable, UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default", "tmc.moves"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=50, nullable=false)
     * @Serializer\Groups({"default"})
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", length=64, nullable=false)
     */
    private $pass;

    /**
     * @var string
     *
     * @ORM\Column(name="fio", type="string", length=100, nullable=false)
     * @Serializer\Groups({"default"})
     */
    private $fio;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     * @Serializer\Groups({"default"})
     * @Assert\Email()
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="otdel_id", type="integer", nullable=true)
     */
    private $otdelId;

    /**
     * @var string
     *
     * @ORM\Column(name="phones", type="string", length=250, nullable=true)
     */
    private $phones;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=16,  nullable=true, options={"default": "male"})
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=16777215, nullable=true)
     */
    private $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     * @Serializer\Groups({"default"})
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="dolzn", type="string", length=100, nullable=true)
     */
    private $dolzn;

    /**
     * Rows per page in listing
     *
     * @var integer
     *
     * @ORM\Column(name="rpp", type="integer", nullable=false)
     */
    private $rpp = 20;

    /**
     * Default design theme
     *
     * @var string
     *
     * @ORM\Column(name="tpl", type="string", length=50, nullable=false)
     */
    private $tpl = 'twozero';

    /**
     * @var boolean
     *
     * @ORM\Column(name="usenbnintegr", type="boolean", nullable=true)
     */
    private $usenbnintegr;

    /**
     * @var string
     *
     * @ORM\Column(name="nbn_crew_name", type="string", length=200, nullable=true)
     */
    private $nbnCrewName;

    /**
     * Allow access from not local network
     *
     * @var boolean
     *
     * @ORM\Column(name="access_type", type="boolean", nullable=false)
     */
    private $accessType = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_user", type="integer", nullable=true)
     */
    private $contrUser;

    /**
     * Partner integration account activity flag
     *
     * @var boolean
     *
     * @ORM\Column(name="allow_access_cu", type="boolean", nullable=false)
     */
    private $allowAccessCu = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="internalphone", type="integer", nullable=true)
     */
    private $internalphone;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="tojob", type="date", nullable=true)
     */
    private $tojob;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="outjob", type="date", nullable=true)
     */
    private $outjob;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_ext_sc", type="boolean", nullable=true)
     */
    private $allowExtSc;

    /**
     * @var Money
     *
     * @ORM\Embedded(class="Gorserv\Gerp\AppBundle\Entity\Money")
     * @ORM\Version()
     */
    private $balance;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $balanceBecameNegativeAt;

    /**
     * @var Polygon
     *
     * @ORM\Column(name="area", type="polygon", nullable=true)
     * @Serializer\Groups({"default"})
     * @Serializer\Accessor(getter="getSerializedArea")
     */
    private $area;

    /**
     * @var SlackUser
     *
     * @ORM\OneToOne(targetEntity="models\SlackUser", mappedBy="user", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @Serializer\Groups({"User.SlackUser"})
     */
    private $slackUser;

    /**
     * ONLY GETTER IMPLEMENTED
     *
     * @var NbnCrew[]
     *
     * @ORM\OneToMany(targetEntity="models\NbnCrew", mappedBy="technician", cascade={"persist", "remove"})
     * @Serializer\Groups({"User.NbnCrews"})
     */
    private $nbnCrews;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="models\TaskUserLink", mappedBy="user", cascade={"remove"})
     */
    private $userTasks;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="models\TmcTicket", mappedBy="tech")
     */
    private $tmcTechTickets;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="models\TmcTicket", mappedBy="user")
     */
    private $tmcUserTickets;

    /**
     * @var ListSc[]
     *
     * @ORM\ManyToMany(targetEntity="models\ListSc", inversedBy="chiefs")
     * @ORM\JoinTable(name="link_sc_chiefs",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="sc_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $slaveServiceCenters;

    /**
     * @var ListSc[]
     *
     * @ORM\ManyToMany(targetEntity="models\ListSc", inversedBy="technicians")
     * @ORM\JoinTable(name="link_sc_user",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="sc_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $warehouses;

    /**
     * @var ListSc[]
     *
     * @ORM\ManyToMany(targetEntity="models\ListSc", inversedBy="mols")
     * @ORM\JoinTable(name="link_warehouse_mol",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="listsc_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $subWarehouses;

    /**
     * @var ListSc[]
     *
     * @ORM\ManyToMany(targetEntity="models\ListSc", inversedBy="superMols")
     * @ORM\JoinTable(name="link_warehouse_supermol",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="listsc_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $superSubWarehouses;

    /**
     * @var ListSc[]
     *
     * @ORM\ManyToMany(targetEntity="ListSc", inversedBy="coordinators")
     * @ORM\JoinTable(name="link_warehouse_coordinator",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="listsc_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $coordinatedWarehouses;

    /**
     * @var Gfx[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="models\UserStatistic", mappedBy="user")
     */
    private $statistics;

    /**
     * @ORM\OneToMany(targetEntity="models\Gfx", mappedBy="technician", cascade={"persist"})
     */
    private $schedules;

    /**
     * @var BillPayment[]
     *
     * @ORM\OneToMany(targetEntity="models\BillPayment", mappedBy="user")
     */
    private $payments;


    /**
     * ONLY GETTER IMPLEMENTED!
     *
     * @var TaskComment[]
     *
     * @ORM\OneToMany(targetEntity="models\TaskComment", mappedBy="author")
     */
    private $comments;

    /**
     * @var Department
     *
     * @ORM\ManyToOne(targetEntity="models\Department", inversedBy="users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="otdel_id", referencedColumnName="id", onDelete="SET NULL")
     * })
     * @Serializer\Groups({"default", "User.Department"})
     */
    private $department;

    /**
     *
     * @var UserMoneyLimit
     *
     * @ORM\OneToOne(targetEntity="UserMoneyLimit", mappedBy="user", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @Serializer\Groups({"user.moneyLimit"})
     */
    private $moneyLimit;

    /**
     *
     * @var SkpTechDebts
     *
     * @ORM\OneToOne(targetEntity="SkpTechDebts", mappedBy="tech", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @Serializer\Groups({"User.skpDebt"})
     */
    private $skpDebt;

    /**
     *
     * @var JobPosition
     *
     * @ORM\ManyToOne(targetEntity="JobPosition")
     * @ORM\JoinColumn(name="pos_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $jobPosition;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="is_virtual", type="boolean", nullable=true)
     */
    private $isVirtual = false;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="virtualUsers")
     * @ORM\JoinColumn(name="real_user_id", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $realUser;

    /**
     *
     * @var User
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="realUser")
     */
    private $virtualUsers;

    /**
     *
     * @var string[]
     *
     * @ORM\Column(type="simple_array")
     */
    private $roles;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="user_photo", fileNameProperty="avatarName")
     *
     * @var File
     */
    private $avatar;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar_name", type="string", length=255, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $avatarName;

    /**
     *
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * Defines whether user is only template for credentials
     * @var boolean
     *
     * @ORM\Column(name="is_access_template", type="boolean")
     */
    private $isAccessTemplate;

    /**
     * @var ListContr[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="models\ListContr")
     * @ORM\JoinTable(name="link_user_contr",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="contr_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $partners;

    /**
     * @var TaskType[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="models\TaskType")
     * @ORM\JoinTable(name="list_empl",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="wtype_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $taskTypes;

    /**
     * @var TaskType[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="models\TaskType")
     * @ORM\JoinTable(name="list_lkuser_wtypes",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="wtype_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $lkTaskTypes;

    /**
     * @var ListSc[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="models\ListSc")
     * @ORM\JoinTable(name="user_sc_access",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="sc_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    private $availableServiceCenters;

    /**
     * @var ListContr
     *
     * @ORM\ManyToOne(targetEntity="ListContr")
     * @ORM\JoinColumn(name="contr_user", referencedColumnName="id")
     */
    private $masterPartner;

    public function __construct()
    {
        $this->warehouses = new ArrayCollection();
        $this->subWarehouses = new ArrayCollection();
        $this->superSubWarehouses = new ArrayCollection();
        $this->coordinatedWarehouses = new ArrayCollection();
        $this->slaveServiceCenters = new ArrayCollection();
        $this->statistics = new ArrayCollection();
        $this->schedules = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->virtualUsers = new ArrayCollection();
        $this->roles = ['ROLE_USER'];
        $this->isAccessTemplate = false;
        $this->partners = new ArrayCollection();
        $this->taskTypes = new ArrayCollection();
        $this->lkTaskTypes = new ArrayCollection();
        $this->availableServiceCenters = new ArrayCollection();
        $this->balance = new Money(0, 'RUB');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return User
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get pass
     *
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Set pass
     *
     * @param string $pass
     *
     * @return User
     */
    public function setPass($pass)
    {
        $this->pass = $pass;

        return $this;
    }

    /**
     * Get fio
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("name")
     * @Serializer\Groups({"default"})
     *
     * @return string
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * Set fio
     *
     * @param string $fio
     *
     * @return User
     */
    public function setFio($fio)
    {
        $this->fio = $fio;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get otdelId
     *
     * @return integer
     */
    public function getOtdelId()
    {
        return $this->otdelId;
    }

    /**
     * Set otdelId
     *
     * @param integer $otdelId
     *
     * @return User
     */
    public function setOtdelId($otdelId)
    {
        $this->otdelId = $otdelId;

        return $this;
    }

    /**
     * Get phones
     *
     * @return string
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Set phones
     *
     * @param string $phones
     *
     * @return User
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set birthday
     *
     * @param DateTime $birthday
     *
     * @return User
     */
    public function setBirthday(DateTime $birthday = null)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return User
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get dolzn
     *
     * @return string
     */
    public function getDolzn()
    {
        return $this->dolzn;
    }

    /**
     * Set dolzn
     *
     * @param string $dolzn
     *
     * @return User
     */
    public function setDolzn($dolzn)
    {
        $this->dolzn = $dolzn;

        return $this;
    }

    /**
     * Get rpp
     *
     * @return integer
     */
    public function getRpp()
    {
        return $this->rpp;
    }

    /**
     * Set rpp
     *
     * @param integer $rpp
     *
     * @return User
     */
    public function setRpp($rpp)
    {
        $this->rpp = $rpp;

        return $this;
    }

    /**
     * Get tpl
     *
     * @return string
     */
    public function getTpl()
    {
        return $this->tpl;
    }

    /**
     * Set tpl
     *
     * @param string $tpl
     *
     * @return User
     */
    public function setTpl($tpl)
    {
        $this->tpl = $tpl;

        return $this;
    }

    /**
     * Get usenbnintegr
     *
     * @return boolean
     */
    public function getUsenbnintegr()
    {
        return $this->usenbnintegr;
    }

    /**
     * Set usenbnintegr
     *
     * @param boolean $usenbnintegr
     *
     * @return User
     */
    public function setUsenbnintegr($usenbnintegr)
    {
        $this->usenbnintegr = $usenbnintegr;

        return $this;
    }

    /**
     * Get nbnCrewName
     *
     * @return string
     */
    public function getNbnCrewName()
    {
        return $this->nbnCrewName;
    }

    /**
     * Set nbnCrewName
     *
     * @param string $nbnCrewName
     *
     * @return User
     */
    public function setNbnCrewName($nbnCrewName)
    {
        $this->nbnCrewName = $nbnCrewName;

        return $this;
    }

    /**
     * Get accessType
     *
     * @return boolean
     */
    public function getAccessType()
    {
        return $this->accessType;
    }

    /**
     * Set accessType
     *
     * @param boolean $accessType
     *
     * @return User
     */
    public function setAccessType($accessType)
    {
        $this->accessType = $accessType;

        return $this;
    }

    /**
     * Get contrUser
     *
     * @return integer
     */
    public function getContrUser()
    {
        return $this->masterPartner->getId();
    }

    /**
     * Get allowAccessCu
     *
     * @return boolean
     */
    public function getAllowAccessCu()
    {
        return $this->allowAccessCu;
    }

    /**
     * Set allowAccessCu
     *
     * @param boolean $allowAccessCu
     *
     * @return User
     */
    public function setAllowAccessCu($allowAccessCu)
    {
        $this->allowAccessCu = $allowAccessCu;

        return $this;
    }

    /**
     * Get internalphone
     *
     * @return integer
     */
    public function getInternalPhone()
    {
        return $this->internalphone;
    }

    /**
     * Set internalphone
     *
     * @param integer $internalphone
     *
     * @return User
     */
    public function setInternalPhone($internalphone)
    {
        $this->internalphone = $internalphone;

        return $this;
    }

    /**
     * Get tojob
     *
     * @return DateTime
     */
    public function getToJob()
    {
        return $this->tojob;
    }

    /**
     * Set tojob
     *
     * @param DateTime $tojob
     *
     * @return User
     */
    public function setToJob(DateTime $tojob = null)
    {
        $this->tojob = $tojob;

        return $this;
    }

    /**
     * Get outjob
     *
     * @return DateTime
     */
    public function getOutJob()
    {
        return $this->outjob;
    }

    /**
     * Set outjob
     *
     * @param DateTime $outjob
     *
     * @return User
     */
    public function setOutJob(DateTime $outjob = null)
    {
        $this->outjob = $outjob;

        return $this;
    }

    /**
     * Get allowExtSc
     *
     * @return boolean
     */
    public function getAllowExtSc()
    {
        return $this->allowExtSc;
    }

    /**
     * Set allowExtSc
     *
     * @param boolean $allowExtSc
     *
     * @return User
     */
    public function setAllowExtSc($allowExtSc)
    {
        $this->allowExtSc = $allowExtSc;

        return $this;
    }

    /**
     * @return array
     */
    public function getSerializedArea()
    {
        if ($this->getArea()) {
            return ['geometry' => ['type' => 'Polygon', 'coordinates' => $this->getArea()->toArray()]];
        }

        return ['geometry' => ['type' => 'Polygon', 'coordinates' => []]];
    }

    /**
     * @return Polygon
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param Polygon $area
     * @return self
     */
    public function setArea(Polygon $area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Add task
     *
     * @param TaskUserLink $task
     *
     * @return User
     */
    public function addUserTask(TaskUserLink $task)
    {
        $this->userTasks[] = $task;

        return $this;
    }

    /**
     * Remove task
     *
     * @param TaskUserLink $task
     */
    public function removeUserTask(TaskUserLink $task)
    {
        $this->userTasks->removeElement($task);
    }

    /**
     * Get tasks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserTasks()
    {
        return $this->userTasks;
    }

    /**
     * Add TmcTicket
     *
     * @param TmcTicket $tmcTicket
     *
     * @return User
     */
    public function addTmcTechTicket(TmcTicket $tmcTicket)
    {
        $this->tmcTechTickets[] = $tmcTicket;

        return $this;
    }

    /**
     * Remove TmcTicket
     *
     * @param TmcTicket $tmcTicket
     */
    public function removeTmcTechTicket(TmcTicket $tmcTicket)
    {
        $this->tmcTechTickets->removeElement($tmcTicket);
    }

    /**
     * Get TmcTickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTmcTechTickets()
    {
        return $this->tmcTechTickets;
    }


    /**
     * Add TmcTicket
     *
     * @param TmcTicket $tmcTicket
     *
     * @return User
     */
    public function addTmcUserTicket(TmcTicket $tmcTicket)
    {
        $this->tmcUserTickets[] = $tmcTicket;

        return $this;
    }

    /**
     * Remove TmcTicket
     *
     * @param TmcTicket $tmcTicket
     */
    public function removeTmcUserTicket(TmcTicket $tmcTicket)
    {
        $this->tmcUserTickets->removeElement($tmcTicket);
    }

    /**
     * Get TmcTickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTmcUserTickets()
    {
        return $this->tmcUserTickets;
    }

    /**
     * @return SlackUser
     */
    public function getSlackUser()
    {
        return $this->slackUser;
    }

    /**
     * @param SlackUser $slackUser
     * @return User
     */
    public function setSlackUser(SlackUser $slackUser = null)
    {
        $this->slackUser = $slackUser;
        return $this;
    }

    /**
     * @param ListSc $warehouse
     * @return $this
     */
    public function addWarehouse(ListSc $warehouse)
    {
        if (!$this->warehouses->contains($warehouse))
            $this->warehouses->add($warehouse);

        return $this;
    }

    /**
     * @param ListSc $warehouse
     * @return $this
     */
    public function removeWarehouse(ListSc $warehouse)
    {
        if (!$this->warehouses->contains($warehouse)) {
            return $this;
        }
        $this->warehouses->removeElement($warehouse);

        return $this;
    }

    /**
     * @return ListSc[]|ArrayCollection
     */
    public function getSubWarehouses()
    {
        return $this->subWarehouses;
    }

    /**
     * @param ListSc $warehouse
     * @return $this
     */
    public function addSubWarehouse(ListSc $warehouse)
    {
        if (!$this->subWarehouses->contains($warehouse)) {
            $this->subWarehouses->add($warehouse);
            $warehouse->addMol($this);
        }

        return $this;
    }

    /**
     * @param ListSc $warehouse
     * @return $this
     */
    public function removeSubWarehouse(ListSc $warehouse)
    {
        if (!$this->subWarehouses->contains($warehouse)) {
            return $this;
        }
        $this->subWarehouses->removeElement($warehouse);

        return $this;
    }

    /**
     * @return ListSc[]|ArrayCollection
     */
    public function getSuperSubWarehouses()
    {
        return $this->superSubWarehouses;
    }

    /**
     * @param ListSc $warehouse
     * @return $this
     */
    public function addSuperSubWarehouse(ListSc $warehouse)
    {
        if (!$this->superSubWarehouses->contains($warehouse)) {
            $this->superSubWarehouses->add($warehouse);
            $warehouse->addMol($this);
        }

        return $this;
    }

    /**
     * @param ListSc $warehouse
     * @return $this
     */
    public function removeSuperSubWarehouse(ListSc $warehouse)
    {
        if (!$this->superSubWarehouses->contains($warehouse)) {
            return $this;
        }
        $this->superSubWarehouses->removeElement($warehouse);

        return $this;
    }

    /**
     * @param ListSc $warehouse
     * @return $this
     */
    public function addCoordinatedWarehouse(ListSc $warehouse)
    {
        if (!$this->coordinatedWarehouses->contains($warehouse)) {
            $this->coordinatedWarehouses->add($warehouse);
            $warehouse->addCoordinator($this);
        }

        return $this;
    }

    /**
     * @param ListSc $subWarehouse
     * @return $this
     */
    public function removeCoordinatedWarehouse(ListSc $subWarehouse)
    {
        if (!$this->coordinatedWarehouses->contains($subWarehouse)) {
            return $this;
        }
        $this->coordinatedWarehouses->removeElement($subWarehouse);

        return $this;
    }

    /**
     * @return ListSc[]|ArrayCollection
     */
    public function getCoordinatedWarehouses()
    {
        return $this->coordinatedWarehouses;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\Groups({"default", "tmc.moves"})
     */
    public function getName()
    {
        return $this->fio;
    }

    public function isMol()
    {
        return !$this->subWarehouses->isEmpty();
    }

    public function isSuperMol()
    {
        return !$this->superSubWarehouses->isEmpty();
    }

    public function isCoordinator()
    {
        return !$this->coordinatedWarehouses->isEmpty();
    }

    public function isMolOnWarehouse(TmcWarehouseInterface $wh)
    {
        return $this->subWarehouses->contains($wh);
    }

    public function isSuperMolOnWarehouse(TmcWarehouseInterface $wh)
    {
        return $this->superSubWarehouses->contains($wh);
    }

    public function isTechnicianOnWarehouse(TmcWarehouseInterface $wh)
    {
        return $this->warehouses->contains($wh);
    }

    public function isCoordinatorOnWarehouse(TmcWarehouseInterface $wh)
    {
        return $this->coordinatedWarehouses->contains($wh);
    }

    public function isCarrier()
    {
        return true;
    }

    public function isDispatcher()
    {
        return !$this->isTechnician() && $this->getSlaveServiceCenters()->isEmpty();
    }

    public function isTechnician()
    {
        return !$this->warehouses->isEmpty();
    }

    /**
     * @return ArrayCollection|ListSc[]
     */
    public function getSlaveServiceCenters()
    {
        return $this->slaveServiceCenters;
    }

    /**
     * @param ListSc $serviceCenter
     * @return boolean
     */
    public function isServiceCenterChief(ListSc $serviceCenter)
    {
        return $this->getSlaveServiceCenters()->contains($serviceCenter);
    }

    /**
     * @param ListSc $serviceCenter
     * @return $this
     */
    public function addSlaveServiceCenter(ListSc $serviceCenter)
    {
        if (!$this->slaveServiceCenters->contains($serviceCenter)) {
            $this->slaveServiceCenters->add($serviceCenter);
        }

        return $this;
    }

    /**
     * @param ListSc $serviceCenter
     * @return $this
     */
    public function removeSlaveServiceCenter(ListSc $serviceCenter)
    {
        if (!$this->slaveServiceCenters->contains($serviceCenter)) {
            return $this;
        }

        $this->slaveServiceCenters->removeElement($serviceCenter);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getMasterServiceCenters()
    {
        return $this->getWarehouses();
    }

    /**
     * @return ListSc[]|ArrayCollection
     */
    public function getWarehouses()
    {
        return $this->warehouses;
    }

    /**
     * @return NbnCrew[]
     */
    public function getNbnCrews()
    {
        return $this->nbnCrews;
    }

    /**
     * @return TaskComment[]
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     *
     * @return UserMoneyLimit
     */
    public function getMoneyLimit()
    {
        return $this->moneyLimit;
    }

    /**
     *
     * @param UserMoneyLimit $moneyLimit
     * @return self
     */
    public function setMoneyLimit(UserMoneyLimit $moneyLimit)
    {
        $this->moneyLimit = $moneyLimit;
        return $this;
    }

    /**
     * @return Gfx[]|ArrayCollection
     */
    public function getSchedules()
    {
        return $this->schedules;
    }

    /**
     * @param Gfx $schedule
     * @return $this
     */
    public function addSchedule(Gfx $schedule)
    {
        if (!$this->schedules->contains($schedule)) {
            $this->schedules->add($schedule);
        }

        return $this;
    }

    /**
     * @param Gfx $schedule
     * @return $this
     */
    public function removeSchedule(Gfx $schedule)
    {
        if (!$this->schedules->contains($schedule)) {
            return $this;
        }
        $this->schedules->removeElement($schedule);

        return $this;
    }

    public function getSkpDebt()
    {
        return $this->skpDebt;
    }

    public function setSkpDebt(SkpTechDebts $skpDebt)
    {
        $this->skpDebt = $skpDebt;
        return $this;
    }

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Department $department
     * @return User
     */
    public function setDepartment($department)
    {
        $this->department = $department;
        $this->department->addUser($this);
        return $this;
    }

    public function getJobPosition()
    {
        return $this->jobPosition;
    }

    public function setJobPosition(JobPosition $jobPosition)
    {
        $this->jobPosition = $jobPosition;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function isVirtual()
    {
        return $this->isVirtual;
    }

    /**
     *
     * @param boolean $isVirtual
     * @return User
     */
    public function setIsVirtual($isVirtual)
    {
        $this->isVirtual = $isVirtual;
        return $this;
    }

    /**
     *
     * @return User
     */
    public function getRealUser()
    {
        return $this->realUser;
    }

    /**
     *
     * @param User $realUser
     * @return User
     */
    public function setRealUser(User $realUser = null)
    {
        $this->realUser = $realUser;
        return $this;
    }

    /**
     *
     * @return User[]|ArrayCollection
     */
    public function getVirtualUsers()
    {
        return $this->virtualUsers;
    }

    public function __toString()
    {
        return $this->fio;
    }

    public function eraseCredentials()
    {

    }

    public function getPassword()
    {
        return $this->pass;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param \string[] $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
        return $this;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->login;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->active;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setAvatar(File $avatar = null)
    {
        $this->avatar = $avatar;
        $this->updatedAt = new DateTime();

        return $this;
    }

    public function getAvatarName()
    {
        return $this->avatarName;
    }

    public function setAvatarName($avatarName)
    {
        $this->avatarName = $avatarName;
        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->login,
            $this->pass,
            $this->active,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->login,
            $this->pass,
            $this->active,
        ) = unserialize($serialized);
    }

    public function isAccessTemplate() : bool
    {
        return $this->isAccessTemplate;
    }

    public function setIsAccessTemplate(bool $isAccessTemplate) : self
    {
        $this->isAccessTemplate = $isAccessTemplate;

        return $this;
    }

    public function getPartners()
    {
        return $this->partners;
    }

    public function getTaskTypes()
    {
        return $this->taskTypes;
    }

    /**
     * @return ArrayCollection|TaskType[]
     */
    public function getLkTaskTypes()
    {
        return $this->lkTaskTypes;
    }

    /**
     * @return ArrayCollection|ListSc[]
     */
    public function getAvailableServiceCenters()
    {
        return $this->availableServiceCenters;
    }

    /**
     * @param ListSc $sc
     * @return $this
     */
    public function addAvailableServiceCenter(ListSc $sc)
    {
        if (!$this->availableServiceCenters->contains($sc)) {
            $this->availableServiceCenters->add($sc);
        }

        return $this;
    }

    /**
     * @param ListSc $sc
     * @return $this
     */
    public function removeAvailableServiceCenters(ListSc $sc)
    {
        if (!$this->availableServiceCenters->contains($sc)) {
            return $this;
        }
        $this->availableServiceCenters->removeElement($sc);

        return $this;
    }

    /**
     * @return ListContr
     */
    public function getMasterPartner()
    {
        return $this->masterPartner;
    }

    /**
     * @param ListContr $masterPartner
     * @return User
     */
    public function setMasterPartner(ListContr $masterPartner = null): User
    {
        $this->masterPartner = $masterPartner;
        return $this;
    }

    /**
     * @return MoneyInterface|Money
     */
    public function getBalance(): MoneyInterface
    {
        return $this->balance;
    }

    /**
     * @param MoneyInterface|Money $balance
     * @return User
     */
    public function setBalance(Money $balance): User
    {
        $this->balance = $balance;
        return $this;
    }

    public function addPayment(Money $money): MoneyInterface
    {
        $this->balance = new Money($this->balance->getSum() + $money->getSum(), $this->balance->getCurrency());

        return $this->balance;
    }

    /**
     * @return DateTime
     */
    public function getBalanceBecameNegativeAt()
    {
        return $this->balanceBecameNegativeAt;
    }

    /**
     * @param DateTime $balanceBecameNegativeAt
     * @return User
     */
    public function setBalanceBecameNegativeAt($balanceBecameNegativeAt): User
    {
        $this->balanceBecameNegativeAt = $balanceBecameNegativeAt;
        return $this;
    }
}
