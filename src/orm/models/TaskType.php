<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * TaskTypes
 *
 * @ORM\Table(name="task_types", indexes={
 *     @ORM\Index(columns={"nbn_class_memo"}),
 *     @ORM\Index(columns={"nbn_slot_memo"})
 * })
 * @ORM\Entity(repositoryClass="repository\TaskTypeRepository")
 */
class TaskType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="`desc`", type="text", nullable=true)
     */
    private $desc;

    /**
     * @var string
     *
     * @ORM\Column(name="plugin_uid", type="string", length=100)
     */
    private $pluginUid;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;


    /**
     *
     * @var \DateInterval
     *
     * @ORM\Column(name="default_duration", type="dateinterval", nullable=true)
     */
    private $defaultDuration;

    /**
     * @var string
     *
     * @ORM\Column(name="nbn_class_memo", type="string", length=50, nullable=true)
     */
    private $nbnClassMemo;

    /**
     * @var string
     *
     * @ORM\Column(name="nbn_slot_memo", type="string", length=50, nullable=true)
     */
    private $nbnSlotMemo;

    /**
     * @var Ticket
     *
     * @ORM\OneToMany(targetEntity="models\Ticket", mappedBy="taskType")
     */
    private $tickets;

    /**
     * @var ListPhotoType[]
     *
     * @ORM\OneToMany(targetEntity="models\ListPhotoType", mappedBy="workType")
     */
    private $photoTypes;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return TaskType
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set desc
     *
     * @param string $desc
     *
     * @return TaskType
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set pluginUid
     *
     * @param string $pluginUid
     *
     * @return TaskType
     */
    public function setPluginUid($pluginUid)
    {
        $this->pluginUid = $pluginUid;

        return $this;
    }

    /**
     * Get pluginUid
     *
     * @return string
     */
    public function getPluginUid()
    {
        return $this->pluginUid;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return TaskType
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set nbnClassMemo
     *
     * @param string $nbnClassMemo
     *
     * @return TaskType
     */
    public function setNbnClassMemo($nbnClassMemo)
    {
        $this->nbnClassMemo = $nbnClassMemo;

        return $this;
    }

    /**
     * Get nbnClassMemo
     *
     * @return string
     */
    public function getNbnClassMemo()
    {
        return $this->nbnClassMemo;
    }

    /**
     * Set nbnSlotMemo
     *
     * @param string $nbnSlotMemo
     *
     * @return TaskType
     */
    public function setNbnSlotMemo($nbnSlotMemo)
    {
        $this->nbnSlotMemo = $nbnSlotMemo;

        return $this;
    }

    /**
     * Get nbnSlotMemo
     *
     * @return string
     */
    public function getNbnSlotMemo()
    {
        return $this->nbnSlotMemo;
    }

    /**
     * @return Ticket
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * @return ListPhotoType[]
     */
    public function getPhotoTypes()
    {
        return $this->photoTypes;
    }

    public function getDefaultDuration(): \DateInterval
    {
        return $this->defaultDuration;
    }

    public function setDefaultDuration(\DateInterval $defaultDuration)
    {
        $this->defaultDuration = $defaultDuration;
        return $this;
    }


}

