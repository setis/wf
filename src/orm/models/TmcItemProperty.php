<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Just move composition.
 * Quantity can be only positive.
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 *
 * @ORM\Table(name="tmc_item_properties", options={"comment":"Just move composition. Quantity can be only positive."}, uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"serial"})
 * })
 * @ORM\Entity(repositoryClass="repository\TmcItemPropertyRepository")
 */
class TmcItemProperty
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var Nomenclature
     *
     * @ORM\ManyToOne(targetEntity="models\Nomenclature")
     * @ORM\JoinColumn(name="nomenclature_id", referencedColumnName="id", nullable=false)
     * @Serializer\Groups({"TmcItemProperty.Nomenclature"})
     */
    private $nomenclature;

    /**
     * @var ListContr
     *
     * @ORM\ManyToOne(targetEntity="models\ListContr")
     * @ORM\JoinColumn(name="partner_id", referencedColumnName="id")
     * @Serializer\Groups({"TmcItemProperty.Partner"})
     */
    private $partner;

    /**
     * @ORM\Column(name="amount", type="decimal", precision=15, scale=3)
     * @Serializer\Groups({"default"})
     */
    protected $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3)
     * @Serializer\Groups({"default"})
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $serial;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_lease", type="boolean", nullable=false)
     * @Serializer\Groups({"default"})
     */
    private $lease = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_uninstalled", type="boolean")
     * @Serializer\Groups({"default"})
     */
    private $isUninstalled = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_defect", type="boolean")
     * @Serializer\Groups({"default"})
     */
    private $isDefect = false;

    /**
     * @var TmcMove
     *
     * @ORM\ManyToOne(targetEntity="models\TmcMove")
     * @ORM\JoinColumn(name="reserved_by_move_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * @Serializer\Groups({"TmcItemProperty.Reservation"})
     */
    private $reservation;

    /**
     * @var mixed
     *
     * @ORM\Column(name="data", type="json_array", nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $data;


    public function __construct()
    {
        $this->quantity = 0;
        $this->amount = 0;
        $this->currency = 'RUB';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \models\Nomenclature
     */
    public function getNomenclature()
    {
        return $this->nomenclature;
    }

    /**
     * @param \models\Nomenclature $nomenclature
     *
     * @return TmcItemProperty
     */
    public function setNomenclature($nomenclature)
    {
        $this->nomenclature = $nomenclature;

        return $this;
    }

    /**
     * @return \models\ListContr
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param \models\ListContr $partner
     *
     * @return TmcItemProperty
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     *
     * @return TmcItemProperty
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return TmcItemProperty
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return string
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * @param string $serial
     *
     * @return TmcItemProperty
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isLease()
    {
        return $this->lease;
    }

    /**
     * @param boolean $lease
     *
     * @return TmcItemProperty
     */
    public function setLease($lease)
    {
        $this->lease = $lease;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isUninstalled()
    {
        return $this->isUninstalled;
    }

    /**
     * @param boolean $isUninstalled
     *
     * @return TmcItemProperty
     */
    public function setIsUninstalled($isUninstalled)
    {
        $this->isUninstalled = $isUninstalled;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isDefect()
    {
        return $this->isDefect;
    }

    /**
     * @param boolean $isDefect
     *
     * @return TmcItemProperty
     */
    public function setIsDefect($isDefect)
    {
        $this->isDefect = $isDefect;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     *
     * @return TmcItemProperty
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param TmcMove $reservation
     * @return TmcItemProperty
     */
    public function setReservation(TmcMove $reservation = null)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * @return TmcMove
     */
    public function getReservation()
    {
        return $this->reservation;
    }

}
