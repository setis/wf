<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Task\Model\TicketCalcInterface;

/**
 * ListTechCalc - technician wage calc
 *
 * @ORM\Table(name="list_tech_calc", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"task_id", "work_id", "tech_id"})
 * }, indexes={
 *     @ORM\Index(columns={"price", "smeta_val"}),
 *     @ORM\Index(columns={"task_id", "work_id", "quant"}),
 *     @ORM\Index(columns={"task_id", "calc_date", "update_date"}),
 *     @ORM\Index(columns={"tech_id", "task_id"})
 * })
 * @ORM\Entity
 */
class ListTechCalc implements TicketCalcInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="smeta_val", type="decimal", precision=10, scale=2, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $smetaVal;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="work_title", type="string", length=255, nullable=false)
     * @Serializer\Groups({"default"})
     */
    private $workTitle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="calc_date", type="date", nullable=false)
     * @Serializer\Groups({"default"})
     */
    private $calcDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $updateDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="quant", type="float", nullable=false)
     * @Serializer\Groups({"default"})
     */
    private $quant;

    /**
     * @var \models\Task
     *
     * @ORM\ManyToOne(targetEntity="models\Task", inversedBy="listTechCalc")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $task;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="tech_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $technician;

    /**
     * @var \models\ListTechPrice
     *
     * @ORM\ManyToOne(targetEntity="models\ListTechPrice")
     * @ORM\JoinColumn(name="work_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $work;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set smetaVal
     *
     * @param string $smetaVal
     *
     * @return ListTechCalc
     */
    public function setSmetaVal($smetaVal)
    {
        $this->smetaVal = $smetaVal;

        return $this;
    }

    /**
     * Get smetaVal
     *
     * @return string
     */
    public function getSmetaVal()
    {
        return $this->smetaVal;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return ListTechCalc
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set workTitle
     *
     * @param string $workTitle
     *
     * @return ListTechCalc
     */
    public function setWorkTitle($workTitle)
    {
        $this->workTitle = $workTitle;

        return $this;
    }

    /**
     * Get workTitle
     *
     * @return string
     */
    public function getWorkTitle()
    {
        return $this->workTitle;
    }

    /**
     * Set calcDate
     *
     * @param \DateTime $calcDate
     *
     * @return ListTechCalc
     */
    public function setCalcDate($calcDate)
    {
        $this->calcDate = $calcDate;

        return $this;
    }

    /**
     * Get calcDate
     *
     * @return \DateTime
     */
    public function getCalcDate()
    {
        return $this->calcDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return ListTechCalc
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set quant
     *
     * @param integer $quant
     *
     * @return ListTechCalc
     */
    public function setQuant($quant)
    {
        $this->quant = $quant;

        return $this;
    }

    /**
     * Get quant
     *
     * @return integer
     */
    public function getQuant()
    {
        return $this->quant;
    }

    /**
     * @param User $technician
     * @return ListTechCalc
     */
    public function setTechnician($technician)
    {
        $this->technician = $technician;
        return $this;
    }

    /**
     * @return User
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     * @param User $user
     * @return ListTechCalc
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param Task $task
     * @return ListTechCalc
     */
    public function setTask($task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param ListTechPrice $work
     * @return ListTechCalc
     */
    public function setWork($work)
    {
        $this->work = $work;
        return $this;
    }

    /**
     * @return ListTechPrice
     */
    public function getWork()
    {
        return $this->work;
    }
}

