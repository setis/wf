<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListRejreasons
 *
 * @ORM\Table(name="list_rejreasons")
 * @ORM\Entity
 */
class ListRejreasons
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="plugin_uid", type="string", length=30, precision=0, scale=0, nullable=false, unique=false)
     */
    private $pluginUid;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=16777215, precision=0, scale=0, nullable=false, unique=false)
     */
    private $comment;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pluginUid
     *
     * @param string $pluginUid
     *
     * @return ListRejreasons
     */
    public function setPluginUid($pluginUid)
    {
        $this->pluginUid = $pluginUid;

        return $this;
    }

    /**
     * Get pluginUid
     *
     * @return string
     */
    public function getPluginUid()
    {
        return $this->pluginUid;
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return ListRejreasons
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ListRejreasons
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ListRejreasons
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
}

