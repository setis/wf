<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * TaskFile
 *
 * @ORM\Table(name="task_files", indexes={
 *     @ORM\Index(columns={"task_id"})
 * })
 * @ORM\Entity
 * @Vich\Uploadable
 */
class TaskFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;
    
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="task_file", fileNameProperty="fileName")
     * 
     * @var File
     */
    private $file;
    
    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=255, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $fileName;

    /**
     * @var string
     *
     * @ORM\Column(name="orig_name", type="string", length=255)
     * @Serializer\Groups({"default"})
     */
    private $origName;

    /**
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string", length=100)
     * @Serializer\Groups({"default"})
     */
    private $mimeType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     * @Serializer\Groups({"default"})
     */
    private $datetime;

    /**
     * @var \models\Task
     *
     * @ORM\ManyToOne(targetEntity="models\Task", inversedBy="files")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @Serializer\Groups({"extended"})
     */
    private $task;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @Serializer\Groups({"extended"})
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set origName
     *
     * @param string $origName
     *
     * @return TaskFile
     */
    public function setOrigName($origName)
    {
        $this->origName = $origName;

        return $this;
    }

    /**
     * Get origName
     *
     * @return string
     */
    public function getOrigName()
    {
        return $this->origName;
    }
    
    /**
     * @return string
     */
    public function getOrigNameUniq()
    {
        return $this->id . '_' . $this->origName;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     *
     * @return TaskFile
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return TaskFile
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set task
     *
     * @param \models\Task $task
     *
     * @return TaskFile
     */
    public function setTask(\models\Task $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \models\Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param User $user
     * @return TaskFile
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    public function getFileName()
    {
        return $this->fileName ?: $this->id;
    }
    
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        
        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile(File $file)
    {
        $this->file = $file;
        $this->datetime = new \DateTime('now');
        return $this;
    }

}
