<?php
namespace models;


use Doctrine\ORM\Mapping as ORM;

/**
 * Equipment
 *
 * @ORM\Table(name="equipment")
 * @ORM\Entity
 */
class Equipment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="contr_id", type="integer", nullable=false)
     */
    private $contrId;

    /**
     * @var string
     *
     * @ORM\Column(name="equipment_name", type="string", length=200, nullable=false)
     */
    private $equipmentName;

    /**
     * @var string
     *
     * @ORM\Column(name="equipment_type", type="string", length=200, nullable=false)
     */
    private $equipmentType;

    /**
     * @var string
     *
     * @ORM\Column(name="equipment_price", type="decimal", precision=14, scale=2, nullable=true)
     */
    private $equipmentPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="buy_conditions", type="string", length=200, nullable=true)
     */
    private $buyConditions;

    /**
     * @var string
     *
     * @ORM\Column(name="delayed_payment_price", type="decimal", precision=14, scale=2, nullable=true)
     */
    private $delayedPaymentPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="delayed_payment_conditions", type="string", length=200, nullable=true)
     */
    private $delayedPaymentConditions;

    /**
     * @var string
     *
     * @ORM\Column(name="action_name", type="string", length=200, nullable=true)
     */
    private $actionName;

    /**
     * @var string
     *
     * @ORM\Column(name="action_description", type="text", length=65535, nullable=true)
     */
    private $actionDescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="action_fd", type="datetime", nullable=true)
     */
    private $actionFd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="action_td", type="datetime", nullable=true)
     */
    private $actionTd;

    /**
     * @var string
     *
     * @ORM\Column(name="action_other", type="string", length=200, nullable=true)
     */
    private $actionOther;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set contrId
     *
     * @param integer $contrId
     *
     * @return Equipment
     */
    public function setContrId($contrId)
    {
        $this->contrId = $contrId;

        return $this;
    }

    /**
     * Get contrId
     *
     * @return integer
     */
    public function getContrId()
    {
        return $this->contrId;
    }

    /**
     * Set equipmentName
     *
     * @param string $equipmentName
     *
     * @return Equipment
     */
    public function setEquipmentName($equipmentName)
    {
        $this->equipmentName = $equipmentName;

        return $this;
    }

    /**
     * Get equipmentName
     *
     * @return string
     */
    public function getEquipmentName()
    {
        return $this->equipmentName;
    }

    /**
     * Set equipmentType
     *
     * @param string $equipmentType
     *
     * @return Equipment
     */
    public function setEquipmentType($equipmentType)
    {
        $this->equipmentType = $equipmentType;

        return $this;
    }

    /**
     * Get equipmentType
     *
     * @return string
     */
    public function getEquipmentType()
    {
        return $this->equipmentType;
    }

    /**
     * Set equipmentPrice
     *
     * @param string $equipmentPrice
     *
     * @return Equipment
     */
    public function setEquipmentPrice($equipmentPrice)
    {
        $this->equipmentPrice = $equipmentPrice;

        return $this;
    }

    /**
     * Get equipmentPrice
     *
     * @return string
     */
    public function getEquipmentPrice()
    {
        return $this->equipmentPrice;
    }

    /**
     * Set buyConditions
     *
     * @param string $buyConditions
     *
     * @return Equipment
     */
    public function setBuyConditions($buyConditions)
    {
        $this->buyConditions = $buyConditions;

        return $this;
    }

    /**
     * Get buyConditions
     *
     * @return string
     */
    public function getBuyConditions()
    {
        return $this->buyConditions;
    }

    /**
     * Set delayedPaymentPrice
     *
     * @param string $delayedPaymentPrice
     *
     * @return Equipment
     */
    public function setDelayedPaymentPrice($delayedPaymentPrice)
    {
        $this->delayedPaymentPrice = $delayedPaymentPrice;

        return $this;
    }

    /**
     * Get delayedPaymentPrice
     *
     * @return string
     */
    public function getDelayedPaymentPrice()
    {
        return $this->delayedPaymentPrice;
    }

    /**
     * Set delayedPaymentConditions
     *
     * @param string $delayedPaymentConditions
     *
     * @return Equipment
     */
    public function setDelayedPaymentConditions($delayedPaymentConditions)
    {
        $this->delayedPaymentConditions = $delayedPaymentConditions;

        return $this;
    }

    /**
     * Get delayedPaymentConditions
     *
     * @return string
     */
    public function getDelayedPaymentConditions()
    {
        return $this->delayedPaymentConditions;
    }

    /**
     * Set actionName
     *
     * @param string $actionName
     *
     * @return Equipment
     */
    public function setActionName($actionName)
    {
        $this->actionName = $actionName;

        return $this;
    }

    /**
     * Get actionName
     *
     * @return string
     */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
     * Set actionDescription
     *
     * @param string $actionDescription
     *
     * @return Equipment
     */
    public function setActionDescription($actionDescription)
    {
        $this->actionDescription = $actionDescription;

        return $this;
    }

    /**
     * Get actionDescription
     *
     * @return string
     */
    public function getActionDescription()
    {
        return $this->actionDescription;
    }

    /**
     * Set actionFd
     *
     * @param \DateTime $actionFd
     *
     * @return Equipment
     */
    public function setActionFd($actionFd)
    {
        $this->actionFd = $actionFd;

        return $this;
    }

    /**
     * Get actionFd
     *
     * @return \DateTime
     */
    public function getActionFd()
    {
        return $this->actionFd;
    }

    /**
     * Set actionTd
     *
     * @param \DateTime $actionTd
     *
     * @return Equipment
     */
    public function setActionTd($actionTd)
    {
        $this->actionTd = $actionTd;

        return $this;
    }

    /**
     * Get actionTd
     *
     * @return \DateTime
     */
    public function getActionTd()
    {
        return $this->actionTd;
    }

    /**
     * Set actionOther
     *
     * @param string $actionOther
     *
     * @return Equipment
     */
    public function setActionOther($actionOther)
    {
        $this->actionOther = $actionOther;

        return $this;
    }

    /**
     * Get actionOther
     *
     * @return string
     */
    public function getActionOther()
    {
        return $this->actionOther;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

