<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use WF\Tmc\Model\NomenclatureCategoryInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * ListMaterialCats
 *
 * @ORM\Table(name="list_material_cats")
 * @ORM\Entity(repositoryClass="repository\ListMaterialCatsRepository")
 */
class ListMaterialCats implements NomenclatureCategoryInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isdoc", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $isdoc;

    /**
     * @var MeasureUnit
     *
     * @ORM\ManyToOne(targetEntity="models\MeasureUnit")
     * @ORM\JoinColumn(name="unit_id", referencedColumnName="id")
     * @Serializer\Groups({"extended", "ListMaterialCats.Unit"})
     */
    private $unit;

    /**
     * @var Nomenclature
     *
     * @ORM\OneToMany(targetEntity="models\Nomenclature", mappedBy="category")
     * @Serializer\Groups({"ListMaterialCats.Nomenclature"})
     */
    private $nomenclature;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ListMaterialCats
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set isdoc
     *
     * @param boolean $isdoc
     *
     * @return ListMaterialCats
     */
    public function setIsdoc($isdoc)
    {
        $this->isdoc = $isdoc;

        return $this;
    }

    /**
     * Get isdoc
     *
     * @return boolean
     */
    public function getIsdoc()
    {
        return $this->isdoc;
    }

    /**
     *
     * @return string
     *
     */
    public function getCategoryName()
    {
        return $this->title;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param Nomenclature $nomenclature
     * @return ListMaterialCats
     */
    public function setNomenclature($nomenclature)
    {
        $this->nomenclature = $nomenclature;

        return $this;
    }

    /**
     * @return Nomenclature
     */
    public function getNomenclature()
    {
        return $this->nomenclature;
    }
}
