<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListAgrTpphones
 *
 * @ORM\Table(name="list_agr_tpphones")
 * @ORM\Entity
 */
class ListAgrTpphones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="agr_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $agrId;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="ext", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $ext;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cr_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $crDate;

    /**
     * @var string
     *
     * @ORM\Column(name="phonetitle", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $phonetitle;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set agrId
     *
     * @param integer $agrId
     *
     * @return ListAgrTpphones
     */
    public function setAgrId($agrId)
    {
        $this->agrId = $agrId;

        return $this;
    }

    /**
     * Get agrId
     *
     * @return integer
     */
    public function getAgrId()
    {
        return $this->agrId;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return ListAgrTpphones
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set ext
     *
     * @param string $ext
     *
     * @return ListAgrTpphones
     */
    public function setExt($ext)
    {
        $this->ext = $ext;

        return $this;
    }

    /**
     * Get ext
     *
     * @return string
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ListAgrTpphones
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set crDate
     *
     * @param \DateTime $crDate
     *
     * @return ListAgrTpphones
     */
    public function setCrDate($crDate)
    {
        $this->crDate = $crDate;

        return $this;
    }

    /**
     * Get crDate
     *
     * @return \DateTime
     */
    public function getCrDate()
    {
        return $this->crDate;
    }

    /**
     * Set phonetitle
     *
     * @param string $phonetitle
     *
     * @return ListAgrTpphones
     */
    public function setPhonetitle($phonetitle)
    {
        $this->phonetitle = $phonetitle;

        return $this;
    }

    /**
     * Get phonetitle
     *
     * @return string
     */
    public function getPhonetitle()
    {
        return $this->phonetitle;
    }
}

