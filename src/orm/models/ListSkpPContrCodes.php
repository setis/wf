<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @deprecated not used
 *
 * ListSkpPContrCodes
 *
 * @ORM\Table(name="list_skp_p_contr_codes", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"wt_id", "contr_id"})
 * })
 * @ORM\Entity
 */
class ListSkpPContrCodes
{
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $code;

    /**
     * @var integer
     *
     * @ORM\Column(name="wt_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $wtId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $contrId;

    /**
     * @var integer
     *
     * @ORM\Column(name="cr_uid", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $crUid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cr_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $crDate;


    /**
     * Set code
     *
     * @param string $code
     *
     * @return ListSkpPContrCodes
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set wtId
     *
     * @param integer $wtId
     *
     * @return ListSkpPContrCodes
     */
    public function setWtId($wtId)
    {
        $this->wtId = $wtId;

        return $this;
    }

    /**
     * Get wtId
     *
     * @return integer
     */
    public function getWtId()
    {
        return $this->wtId;
    }

    /**
     * Set contrId
     *
     * @param integer $contrId
     *
     * @return ListSkpPContrCodes
     */
    public function setContrId($contrId)
    {
        $this->contrId = $contrId;

        return $this;
    }

    /**
     * Get contrId
     *
     * @return integer
     */
    public function getContrId()
    {
        return $this->contrId;
    }

    /**
     * Set crUid
     *
     * @param integer $crUid
     *
     * @return ListSkpPContrCodes
     */
    public function setCrUid($crUid)
    {
        $this->crUid = $crUid;

        return $this;
    }

    /**
     * Get crUid
     *
     * @return integer
     */
    public function getCrUid()
    {
        return $this->crUid;
    }

    /**
     * Set crDate
     *
     * @param \DateTime $crDate
     *
     * @return ListSkpPContrCodes
     */
    public function setCrDate($crDate)
    {
        $this->crDate = $crDate;

        return $this;
    }

    /**
     * Get crDate
     *
     * @return \DateTime
     */
    public function getCrDate()
    {
        return $this->crDate;
    }
}

