<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * LinkContragentServices
 *
 * @ORM\Table(name="link_contragent_services", indexes={
 *     @ORM\Index(columns={"contragent_id"}),
 *     @ORM\Index(columns={"service_id"})
 * })
 * @ORM\Entity
 */
class LinkContragentServices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ListContr
     *
     * @ORM\ManyToOne(targetEntity="ListContr")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contragent_id", referencedColumnName="id")
     * })
     */
    private $contragent;

    /**
     * @var ServicesForSale
     *
     * @ORM\ManyToOne(targetEntity="ServicesForSale")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     * })
     */
    private $service;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contragent
     *
     * @param ListContr $contragent
     *
     * @return LinkContragentServices
     */
    public function setContragent(ListContr $contragent = null)
    {
        $this->contragent = $contragent;

        return $this;
    }

    /**
     * Get contragent
     *
     * @return ListContr
     */
    public function getContragent()
    {
        return $this->contragent;
    }

    /**
     * Set service
     *
     * @param ServicesForSale $service
     *
     * @return LinkContragentServices
     */
    public function setService(ServicesForSale $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return ServicesForSale
     */
    public function getService()
    {
        return $this->service;
    }
}

