<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskReminders
 *
 * @ORM\Table(name="task_reminders", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"user_id", "task_id"})
 * }, indexes={
 *     @ORM\Index(columns={"user_id"}), 
 *     @ORM\Index(columns={"task_id"}), 
 *     @ORM\Index(columns={"month", "day", "week"})
 * })
 * @ORM\Entity
 */
class TaskReminders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $taskId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="day", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $day;

    /**
     * @var boolean
     *
     * @ORM\Column(name="week", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $week;

    /**
     * @var boolean
     *
     * @ORM\Column(name="month", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $month;


    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return TaskReminders
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set taskId
     *
     * @param integer $taskId
     *
     * @return TaskReminders
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set day
     *
     * @param boolean $day
     *
     * @return TaskReminders
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return boolean
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set week
     *
     * @param boolean $week
     *
     * @return TaskReminders
     */
    public function setWeek($week)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return boolean
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Set month
     *
     * @param boolean $month
     *
     * @return TaskReminders
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return boolean
     */
    public function getMonth()
    {
        return $this->month;
    }
}

