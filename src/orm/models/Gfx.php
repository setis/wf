<?php

namespace models;

use DateInterval;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gorserv\Gerp\AppBundle\Schedule\Interval;
use Gorserv\Gerp\ScheduleBundle\Model\IntervalInterface;
use Gorserv\Gerp\ScheduleBundle\Model\JobInterface;
use Gorserv\Gerp\ScheduleBundle\Model\SlotInterface;
use WF\Tmc\Model\TechnicianInterface;

/**
 * Gfx
 *
 * @ORM\Table(name="gfx", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"c_date", "empl_id", "startTime"})
 * }, indexes={
 *     @ORM\Index(columns={"task_id", "c_date"}),
 *     @ORM\Index(columns={"c_date", "task_id", "empl_id"}),
 *     @ORM\Index(columns={"c_date", "empl_id", "task_id"}),
 *     @ORM\Index(columns={"time_start", "time_end"})
 * })
 * @ORM\Entity(repositoryClass="repository\GfxRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Gfx implements SlotInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="c_date", type="date")
     */
    private $date;

    /**
     * @var Task
     *
     * @ORM\OneToOne(targetEntity="Task", inversedBy="schedule")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $task;

    /**
     * @var integer
     *
     * @ORM\Column(name="startTime", type="integer")
     */
    private $startTime;

    /**
     *
     * @var DateTime
     *
     * @ORM\Column(name="time_start", type="datetime", nullable=true)
     */
    private $timeStart;

    /**
     *
     * @var DateTime
     *
     * @ORM\Column(name="time_end", type="datetime", nullable=true)
     */
    private $timeEnd;

    /**
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="schedules")
     * @ORM\JoinColumn(name="empl_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $technician;

    /**
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cDate
     *
     * @param DateTime $date
     *
     * @return Gfx
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     *
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     *
     * @param Task $task
     * @return Gfx
     */
    public function setTask(Task $task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * Set startTime
     *
     * @param integer $startTime
     *
     * @return Gfx
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return integer
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @return DateTime
     */
    public function getStartTimeAsObject()
    {
        $start = clone $this->getDate();

        $start->setTime(0, $this->getStartTime());

        return $start;
    }

    public function getEndDateTime()
    {
        if ($this->getTask() && $this->getTask()->getTicket()->getTaskType())
            return $this->getStartTimeAsObject()->add(new DateInterval('PT' . $this->getTask()->getTicket()->getTaskType()->getDuration() . 'M'));

        return null;
    }

    /**
     * @param User|TechnicianInterface $technician
     * @return Gfx
     */
    public function setTechnician(TechnicianInterface $technician)
    {
        $this->technician = $technician;
        $this->technician->addSchedule($this);
        return $this;
    }

    /**
     * @return User|TechnicianInterface
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     *
     * @return DateTime
     */
    public function getStartDateTime()
    {
        $dt = clone $this->date;
        $dt->setTime(0, $this->startTime, 0);

        return $dt;
    }

    public function getTimeStart(): DateTime
    {
        return $this->timeStart;
    }

    public function getTimeEnd(): DateTime
    {
        return $this->timeEnd;
    }

    public function setTimeStart(DateTime $timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    public function setTimeEnd(DateTime $timeEnd)
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateFields()
    {
        if (null === $this->timeStart || null === $this->timeEnd) {
            $this->timeStart = $this->getStartDateTime();

            $ticket = $this->task->getTicket();
            if (null === $ticket) {
                throw new \LogicException('Ticket is not set in task ' . $this->task->getId());
            }

            $type = $ticket->getTaskType();
            if (null === $type) {
                throw new \LogicException('Type is not set in ticket ' . $this->task->getId());
            }

            if (empty($type->getDuration())) {
                throw new \LogicException('Duration is empty in ticket ' . $this->task->getId());
            }

            $this->timeEnd = clone $this->date;
            $this->timeEnd->setTime(0, $this->startTime + $type->getDuration(), 0);

            return;
        }

        $this->date = clone $this->timeStart;
        $this->date->setTime(0, 0, 0);

        $i = $this->timeStart->diff($this->date);
        $this->startTime = $i->h * 60 + $i->i;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    public function getExecutors(): array
    {
        return [$this->technician];
    }

    public function getJob(): JobInterface
    {
        return $this->task;
    }

    public function getInterval(): IntervalInterface
    {
        return new Interval($this->timeStart, $this->timeEnd);
    }
}
