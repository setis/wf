<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListContrContacts
 *
 * @ORM\Table(name="list_contr_contacts")
 * @ORM\Entity
 */
class ListContrContacts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $contrId;

    /**
     * @var string
     *
     * @ORM\Column(name="fio", type="string", length=250, precision=0, scale=0, nullable=true, unique=false)
     */
    private $fio;

    /**
     * @var string
     *
     * @ORM\Column(name="pos", type="string", length=250, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pos;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=250, precision=0, scale=0, nullable=true, unique=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=250, precision=0, scale=0, nullable=true, unique=false)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bdate", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $bdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cr_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $crDate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contrId
     *
     * @param integer $contrId
     *
     * @return ListContrContacts
     */
    public function setContrId($contrId)
    {
        $this->contrId = $contrId;

        return $this;
    }

    /**
     * Get contrId
     *
     * @return integer
     */
    public function getContrId()
    {
        return $this->contrId;
    }

    /**
     * Set fio
     *
     * @param string $fio
     *
     * @return ListContrContacts
     */
    public function setFio($fio)
    {
        $this->fio = $fio;

        return $this;
    }

    /**
     * Get fio
     *
     * @return string
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * Set pos
     *
     * @param string $pos
     *
     * @return ListContrContacts
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return string
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return ListContrContacts
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ListContrContacts
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set bdate
     *
     * @param \DateTime $bdate
     *
     * @return ListContrContacts
     */
    public function setBdate($bdate)
    {
        $this->bdate = $bdate;

        return $this;
    }

    /**
     * Get bdate
     *
     * @return \DateTime
     */
    public function getBdate()
    {
        return $this->bdate;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ListContrContacts
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set crDate
     *
     * @param \DateTime $crDate
     *
     * @return ListContrContacts
     */
    public function setCrDate($crDate)
    {
        $this->crDate = $crDate;

        return $this;
    }

    /**
     * Get crDate
     *
     * @return \DateTime
     */
    public function getCrDate()
    {
        return $this->crDate;
    }
}

