<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListEmplUsesms
 *
 * @ORM\Table(name="list_empl_usesms", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"user_id"})
 * })
 * @ORM\Entity
 */
class ListEmplUsesms
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="use_sms", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $useSms;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;


    /**
     * Set useSms
     *
     * @param boolean $useSms
     *
     * @return ListEmplUsesms
     */
    public function setUseSms($useSms)
    {
        $this->useSms = $useSms;

        return $this;
    }

    /**
     * Get useSms
     *
     * @return boolean
     */
    public function getUseSms()
    {
        return $this->useSms;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ListEmplUsesms
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
}

