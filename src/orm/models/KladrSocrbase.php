<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * KladrSocrbase
 *
 * @ORM\Table(name="kladr_socrbase", indexes={
 *     @ORM\Index(columns={"SCNAME", "SOCRNAME"})
 * })
 * @ORM\Entity
 */
class KladrSocrbase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="LEVEL", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="SCNAME", type="string", length=10, precision=0, scale=0, nullable=false, unique=false)
     */
    private $scname;

    /**
     * @var string
     *
     * @ORM\Column(name="SOCRNAME", type="string", length=29, precision=0, scale=0, nullable=false, unique=false)
     */
    private $socrname;

    /**
     * @var integer
     *
     * @ORM\Column(name="KOD_T_ST", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $kodTSt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level
     *
     * @param boolean $level
     *
     * @return KladrSocrbase
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return boolean
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set scname
     *
     * @param string $scname
     *
     * @return KladrSocrbase
     */
    public function setScname($scname)
    {
        $this->scname = $scname;

        return $this;
    }

    /**
     * Get scname
     *
     * @return string
     */
    public function getScname()
    {
        return $this->scname;
    }

    /**
     * Set socrname
     *
     * @param string $socrname
     *
     * @return KladrSocrbase
     */
    public function setSocrname($socrname)
    {
        $this->socrname = $socrname;

        return $this;
    }

    /**
     * Get socrname
     *
     * @return string
     */
    public function getSocrname()
    {
        return $this->socrname;
    }

    /**
     * Set kodTSt
     *
     * @param integer $kodTSt
     *
     * @return KladrSocrbase
     */
    public function setKodTSt($kodTSt)
    {
        $this->kodTSt = $kodTSt;

        return $this;
    }

    /**
     * Get kodTSt
     *
     * @return integer
     */
    public function getKodTSt()
    {
        return $this->kodTSt;
    }
}

