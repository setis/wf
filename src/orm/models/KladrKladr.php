<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * KladrKladr
 *
 * @ORM\Table(name="kladr_kladr", indexes={
 *     @ORM\Index(columns={"CODE"}),
 *     @ORM\Index(columns={"NAME", "CODE"}),
 *     @ORM\Index(columns={"NAME"})})
 * @ORM\Entity
 */
class KladrKladr
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=40, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="SOCR", type="string", length=10, nullable=false, unique=false)
     */
    private $socr;

    /**
     * @var string
     *
     * @ORM\Column(name="`CODE`", type="string", length=13, nullable=false, unique=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="INDEX", type="string", length=6, nullable=true, unique=false)
     */
    private $index;

    /**
     * @var string
     *
     * @ORM\Column(name="GNINMB", type="string", length=4, nullable=true, unique=false)
     */
    private $gninmb;

    /**
     * @var string
     *
     * @ORM\Column(name="UNO", type="string", length=4, nullable=true, unique=false)
     */
    private $uno;

    /**
     * @var string
     *
     * @ORM\Column(name="OCATD", type="string", length=11, nullable=true, unique=false)
     */
    private $ocatd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="STATUS", type="boolean", nullable=false, unique=false)
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return KladrKladr
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set socr
     *
     * @param string $socr
     *
     * @return KladrKladr
     */
    public function setSocr($socr)
    {
        $this->socr = $socr;

        return $this;
    }

    /**
     * Get socr
     *
     * @return string
     */
    public function getSocr()
    {
        return $this->socr;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return KladrKladr
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set index
     *
     * @param string $index
     *
     * @return KladrKladr
     */
    public function setIndex($index)
    {
        $this->index = $index;

        return $this;
    }

    /**
     * Get index
     *
     * @return string
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Set gninmb
     *
     * @param string $gninmb
     *
     * @return KladrKladr
     */
    public function setGninmb($gninmb)
    {
        $this->gninmb = $gninmb;

        return $this;
    }

    /**
     * Get gninmb
     *
     * @return string
     */
    public function getGninmb()
    {
        return $this->gninmb;
    }

    /**
     * Set uno
     *
     * @param string $uno
     *
     * @return KladrKladr
     */
    public function setUno($uno)
    {
        $this->uno = $uno;

        return $this;
    }

    /**
     * Get uno
     *
     * @return string
     */
    public function getUno()
    {
        return $this->uno;
    }

    /**
     * Set ocatd
     *
     * @param string $ocatd
     *
     * @return KladrKladr
     */
    public function setOcatd($ocatd)
    {
        $this->ocatd = $ocatd;

        return $this;
    }

    /**
     * Get ocatd
     *
     * @return string
     */
    public function getOcatd()
    {
        return $this->ocatd;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return KladrKladr
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
}

