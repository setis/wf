<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Tmc\Model\TmcHolderInterface;
use WF\Tmc\Model\TmcMoveItemInterface;

/**
 * Rests of items on holder.
 * Quantity may be negative if item where moved from holder.
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 *
 * @ORM\Table(name="tmc_holder_items", options={"comment":"Rests of items on holder. Quantity may be negative if item where moved from holder."})
 * @ORM\Entity(repositoryClass="repository\TmcHolderItemRepository")
 */
class TmcHolderItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var TmcMove
     *
     * @ORM\ManyToOne(targetEntity="models\TmcMove", inversedBy="holderItems")
     * @ORM\JoinColumn(name="tmc_move_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Groups({"TmcHolderItem.Move"})
     */
    private $move;

    /**
     * @var TmcHolderLink
     *
     * @ORM\OneToOne(targetEntity="models\TmcHolderLink", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="holder_id", referencedColumnName="id")
     * @Serializer\Groups({"TmcHolderItem.HolderLink"})
     */
    private $holderLink;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="float")
     * @Serializer\Groups({"default"})
     */
    private $quantity;

    /**
     * @var TmcItemProperty
     *
     * @ORM\ManyToOne(targetEntity="models\TmcItemProperty", cascade={"persist"})
     * @ORM\JoinColumn(name="property_id", referencedColumnName="id")
     * @Serializer\Groups({"TmcHolderItem.Property"})
     */
    private $property;

    /**
     * TmcHolderItem constructor.
     */
    public function __construct()
    {
        $this->holderLink = new TmcHolderLink();

        $this->quantity = 0;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return TmcMove
     */
    public function getMove()
    {
        return $this->move;
    }

    /**
     * @param TmcHolderLink $holderLink
     * @return $this
     */
    public function setHolderLink(TmcHolderLink $holderLink)
    {
        $this->holderLink = $holderLink;

        return $this;
    }

    /**
     * @return TmcHolderLink
     * @Serializer\Groups({"extended"})
     * @Serializer\VirtualProperty
     */
    public function getHolderLink()
    {
        return $this->holderLink;
    }

    /**
     * @param TmcHolderInterface $holder
     * @return $this
     */
    public function setHolder(TmcHolderInterface $holder)
    {
        $this->holderLink->setHolder($holder);

        return $this;
    }

    /**
     * @return TmcHolderInterface
     * @Serializer\Groups({"TmcHolderItem.Holder"})
     * @Serializer\SerializedName("holder")
     * @Serializer\VirtualProperty
     */
    public function getHolder()
    {
        return $this->getHolderLink()->getHolder();
    }

    /**
     * @return float|int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @param TmcMove $move
     * @return TmcHolderItem
     */
    public function setMove($move)
    {
        $this->move = $move;

        return $this;
    }

    /**
     * @param \models\TmcItemProperty $property
     *
     * @return TmcHolderItem
     */
    public function setProperty($property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * @return \models\TmcItemProperty
     */
    public function getProperty()
    {
        return $this->property;
    }
    /////////////////////////////////////////////////////////////////////////

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("nomenclature")
     * @Serializer\Groups({"TmcHolderItem.Nomenclature"})
     *
     * @return \models\Nomenclature
     */
    public function getNomenclature()
    {
        return $this->property->getNomenclature();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("partner")
     * @Serializer\Groups({"TmcHolderItem.Partner"})
     *
     * @return \models\ListContr
     */
    public function getPartner()
    {
        return $this->property->getPartner();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("amount")
     * @Serializer\Groups({"default"})
     *
     * @return mixed
     */
    public function getAmount()
    {
        return $this->property->getAmount();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("currency")
     * @Serializer\Groups({"default"})
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->property->getCurrency();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("serial")
     * @Serializer\Groups({"default"})
     *
     * @return string
     */
    public function getSerial()
    {
        return $this->property->getSerial();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("lease")
     * @Serializer\Groups({"default"})
     *
     * @return boolean
     */
    public function isLease()
    {
        return $this->property->isLease();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("is_uninstalled")
     * @Serializer\Groups({"default"})
     *
     * @return boolean
     */
    public function isUninstalled()
    {
        return $this->property->isUninstalled();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("is_defect")
     * @Serializer\Groups({"default"})
     *
     * @return boolean
     */
    public function isIsDefect()
    {
        return $this->property->isDefect();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("data")
     * @Serializer\Groups({"default"})
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->property->getData();
    }

}
