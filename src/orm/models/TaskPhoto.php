<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * TaskPhotos
 *
 * @ORM\Table(name="task_photos",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="search_idx", columns={"task_id", "filetype_id"}
 *          )
 *      }
 * )
 * @ORM\Entity
 */
class TaskPhoto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=250)
     * @Serializer\Groups({"default"})
     */
    private $filename;

    /**
     * @var \models\Task
     *
     * @ORM\ManyToOne(targetEntity="models\Task", inversedBy="photos", cascade={"persist","remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Serializer\Groups({"extended"})
     * @Serializer\MaxDepth(1)
     */
    private $task;

    /**
     * @var \models\ListPhotoType
     *
     * @ORM\ManyToOne(targetEntity="models\ListPhotoType", inversedBy="photos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="filetype_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * })
     */
    private $type;

    /**
     * @var \models\TaskFile
     *
     * @ORM\OneToOne(targetEntity="models\TaskFile", cascade={"persist","remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="file_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Serializer\Groups({"default"})
     */
    private $file;

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return TaskPhoto
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param Task $task
     * @return TaskPhoto
     */
    public function setTask($task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param ListPhotoType $type
     * @return TaskPhoto
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return ListPhotoType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param TaskFile $file
     * @return TaskPhoto
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return TaskFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

