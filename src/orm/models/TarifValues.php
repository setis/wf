<?php
namespace models;


use Doctrine\ORM\Mapping as ORM;

/**
 * TarifValues
 *
 * @ORM\Table(name="tarif_values", indexes={
 *     @ORM\Index(columns={"tarif_id"}),
 *     @ORM\Index(columns={"tarif_type_property_id"})
 * })
 * @ORM\Entity
 */
class TarifValues
{
    /**
     * @var string
     *
     * @ORM\Column(name="tarif_type_property_value", type="string", length=200, nullable=false)
     */
    private $tarifTypePropertyValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var TariffTypeProperty
     *
     * @ORM\ManyToOne(targetEntity="models\TariffTypeProperty")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tarif_type_property_id", referencedColumnName="id")
     * })
     */
    private $tarifTypeProperty;

    /**
     * @var Tariff
     *
     * @ORM\ManyToOne(targetEntity="models\Tariff")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tarif_id", referencedColumnName="id")
     * })
     */
    private $tarif;


    /**
     * Set tarifTypePropertyValue
     *
     * @param string $tarifTypePropertyValue
     *
     * @return TarifValues
     */
    public function setTarifTypePropertyValue($tarifTypePropertyValue)
    {
        $this->tarifTypePropertyValue = $tarifTypePropertyValue;

        return $this;
    }

    /**
     * Get tarifTypePropertyValue
     *
     * @return string
     */
    public function getTarifTypePropertyValue()
    {
        return $this->tarifTypePropertyValue;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tarifTypeProperty
     *
     * @param TariffTypeProperty $tarifTypeProperty
     *
     * @return TarifValues
     */
    public function setTarifTypeProperty(TariffTypeProperty $tarifTypeProperty = null)
    {
        $this->tarifTypeProperty = $tarifTypeProperty;

        return $this;
    }

    /**
     * Get tarifTypeProperty
     *
     * @return TariffTypeProperty
     */
    public function getTarifTypeProperty()
    {
        return $this->tarifTypeProperty;
    }

    /**
     * Set tarif
     *
     * @param Tariff $tarif
     *
     * @return TarifValues
     */
    public function setTarif(Tariff $tarif = null)
    {
        $this->tarif = $tarif;

        return $this;
    }

    /**
     * Get tarif
     *
     * @return Tariff
     */
    public function getTarif()
    {
        return $this->tarif;
    }
}

