<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Tmc\Model\NomenclatureInterface;

/**
 * Nomenclature
 *
 * @ORM\Table(name="list_materials", indexes={
 *     @ORM\Index(columns={"cat_id"})
 * })
 * @ORM\Entity(repositoryClass="repository\NomenclatureRepository")
 */
class Nomenclature implements NomenclatureInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="bar_code", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $barCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="metric_flag", type="boolean")
     */
    private $metricFlag;

    /**
     * @var string
     *
     * @ORM\Column(name="metric_title", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $metricTitle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_num", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $isNum;

    /**
     * @var MeasureUnit
     *
     * @ORM\ManyToOne(targetEntity="models\MeasureUnit")
     * @ORM\JoinColumn(name="unit_id", referencedColumnName="id")
     * @Serializer\Groups({"default", "Nomenclature.Unit"})
     */
    private $unit;

    /**
     * @var ListMaterialCats
     *
     * @ORM\ManyToOne(targetEntity="models\ListMaterialCats", inversedBy="nomenclature")
     * @ORM\JoinColumn(name="cat_id", referencedColumnName="id")
     * @Serializer\Groups({"default", "Nomenclature.Category"})
     */
    private $category;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Nomenclature
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get catId
     *
     * @return integer
     */
    public function getCatId()
    {
        return $this->category->getId();
    }

    /**
     * Set barCode
     *
     * @param string $barCode
     *
     * @return Nomenclature
     */
    public function setBarCode($barCode)
    {
        $this->barCode = $barCode;

        return $this;
    }

    /**
     * Get barCode
     *
     * @return string
     */
    public function getBarCode()
    {
        return $this->barCode;
    }

    /**
     * Set metricFlag
     *
     * @param integer $metricFlag
     *
     * @return Nomenclature
     */
    public function setMetricFlag($metricFlag)
    {
        $this->metricFlag = $metricFlag;

        return $this;
    }

    /**
     * Get metricFlag
     *
     * @return integer
     */
    public function getMetricFlag()
    {
        return $this->metricFlag;
    }

    /**
     * Set metricTitle
     *
     * @param string $metricTitle
     *
     * @return Nomenclature
     */
    public function setMetricTitle($metricTitle)
    {
        $this->metricTitle = $metricTitle;

        return $this;
    }

    /**
     * Get metricTitle
     *
     * @return string
     */
    public function getMetricTitle()
    {
        return $this->metricTitle;
    }

    /**
     * Set isNum
     *
     * @param boolean $isNum
     *
     * @return Nomenclature
     */
    public function setIsNum($isNum)
    {
        $this->isNum = $isNum;

        return $this;
    }

    /**
     * Get isNum
     *
     * @return boolean
     */
    public function getIsNum()
    {
        return $this->isNum;
    }

    public function getCategoty()
    {
        return $this->category;
    }

    public function setCategory(ListMaterialCats $category)
    {
        $this->category = $category;
        if ($this->unit === null && null !== $unit = $category->getUnit()) {
            $this->setUnit($unit);
        }
        return $this;
    }

    public function getName()
    {
        return $this->title;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function setUnit(MeasureUnit $unit = null)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     *
     * @return boolean
     *
     * @Serializer\VirtualProperty
     * @Serializer\Groups({"default"})
     */
    public function isMetrical()
    {
        return $this->metricFlag;
    }

    public function setIsMetrical($isMetrical)
    {
        $this->metricFlag = (bool) $isMetrical;

        return $this;
    }

}

