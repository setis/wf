<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListFContrReports
 *
 * @ORM\Table(name="list_f_contr_reports")
 * @ORM\Entity
 */
class ListFContrReports
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="period", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $period;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", precision=0, scale=0, nullable=true, unique=false)
     */
    private $body;

    /**
     * @var integer
     *
     * @ORM\Column(name="cr_uid", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $crUid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cr_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $crDate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set period
     *
     * @param string $period
     *
     * @return ListFContrReports
     */
    public function setPeriod($period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return ListFContrReports
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set crUid
     *
     * @param integer $crUid
     *
     * @return ListFContrReports
     */
    public function setCrUid($crUid)
    {
        $this->crUid = $crUid;

        return $this;
    }

    /**
     * Get crUid
     *
     * @return integer
     */
    public function getCrUid()
    {
        return $this->crUid;
    }

    /**
     * Set crDate
     *
     * @param \DateTime $crDate
     *
     * @return ListFContrReports
     */
    public function setCrDate($crDate)
    {
        $this->crDate = $crDate;

        return $this;
    }

    /**
     * Get crDate
     *
     * @return \DateTime
     */
    public function getCrDate()
    {
        return $this->crDate;
    }
}

