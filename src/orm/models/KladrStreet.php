<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * KladrStreet
 *
 * @ORM\Table(name="kladr_street", indexes={
 *     @ORM\Index(columns={"NAME"}), 
 *     @ORM\Index(columns={"SOCR"}), 
 *     @ORM\Index(columns={"CODE"}), 
 *     @ORM\Index(columns={"NAME", "CODE"})
 * })
 * @ORM\Entity
 */
class KladrStreet
{
    /**
     * @var string
     *
     * @ORM\Column(name="CODE", type="string", length=17, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=40, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="SOCR", type="string", length=10, precision=0, scale=0, nullable=false, unique=false)
     */
    private $socr;

    /**
     * @var string
     *
     * @ORM\Column(name="INDEX", type="string", length=6, precision=0, scale=0, nullable=true, unique=false)
     */
    private $index;

    /**
     * @var string
     *
     * @ORM\Column(name="GNINMB", type="string", length=4, precision=0, scale=0, nullable=true, unique=false)
     */
    private $gninmb;

    /**
     * @var string
     *
     * @ORM\Column(name="UNO", type="string", length=4, precision=0, scale=0, nullable=true, unique=false)
     */
    private $uno;

    /**
     * @var string
     *
     * @ORM\Column(name="OCATD", type="string", length=11, precision=0, scale=0, nullable=true, unique=false)
     */
    private $ocatd;


    /**
     * Set code
     *
     * @param string $code
     *
     * @return KladrStreet
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return KladrStreet
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set socr
     *
     * @param string $socr
     *
     * @return KladrStreet
     */
    public function setSocr($socr)
    {
        $this->socr = $socr;

        return $this;
    }

    /**
     * Get socr
     *
     * @return string
     */
    public function getSocr()
    {
        return $this->socr;
    }

    /**
     * Set index
     *
     * @param string $index
     *
     * @return KladrStreet
     */
    public function setIndex($index)
    {
        $this->index = $index;

        return $this;
    }

    /**
     * Get index
     *
     * @return string
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Set gninmb
     *
     * @param string $gninmb
     *
     * @return KladrStreet
     */
    public function setGninmb($gninmb)
    {
        $this->gninmb = $gninmb;

        return $this;
    }

    /**
     * Get gninmb
     *
     * @return string
     */
    public function getGninmb()
    {
        return $this->gninmb;
    }

    /**
     * Set uno
     *
     * @param string $uno
     *
     * @return KladrStreet
     */
    public function setUno($uno)
    {
        $this->uno = $uno;

        return $this;
    }

    /**
     * Get uno
     *
     * @return string
     */
    public function getUno()
    {
        return $this->uno;
    }

    /**
     * Set ocatd
     *
     * @param string $ocatd
     *
     * @return KladrStreet
     */
    public function setOcatd($ocatd)
    {
        $this->ocatd = $ocatd;

        return $this;
    }

    /**
     * Get ocatd
     *
     * @return string
     */
    public function getOcatd()
    {
        return $this->ocatd;
    }
}

