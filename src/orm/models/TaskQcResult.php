<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketsQcResults
 *
 * @ORM\Table(name="tickets_qc_results", uniqueConstraints={@ORM\UniqueConstraint(columns={"task_id", "q_id"})})
 * @ORM\Entity
 */
class TaskQcResult
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var QcQuestion
     * 
     * @ORM\ManyToOne(targetEntity="QcQuestion")
     * @ORM\JoinColumn(name="q_id", referencedColumnName="id")
     */
    private $question;
    
    /**
     *
     * @var Task
     * 
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="qcResults")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $task;

    /**
     * @var integer
     *
     * @ORM\Column(name="result", type="smallint", nullable=true)
     */
    private $result;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer_mark", type="smallint", nullable=true)
     */
    private $answerMark;

    /**
     * @var integer
     *
     * @ORM\Column(name="new_answer_mark", type="smallint", nullable=true)
     */
    private $newAnswerMark;

    /**
     * @var integer
     *
     * @ORM\Column(name="mark", type="smallint", nullable=true)
     */
    private $mark;

    /**
     * @var string
     *
     * @ORM\Column(name="ammount", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $ammount;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text",)
     */
    private $comment;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getQuestion()
    {
        return $this->question;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getAnswerMark()
    {
        return $this->answerMark;
    }

    public function getNewAnswerMark()
    {
        return $this->newAnswerMark;
    }

    public function getMark()
    {
        return $this->mark;
    }

    public function getAmmount()
    {
        return $this->ammount;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function setQuestion(QcQuestion $question)
    {
        $this->question = $question;
        return $this;
    }

    public function setTask(Task $task)
    {
        $this->task = $task;
        return $this;
    }

    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }

    public function setAnswerMark($answerMark)
    {
        $this->answerMark = $answerMark;
        return $this;
    }

    public function setNewAnswerMark($newAnswerMark)
    {
        $this->newAnswerMark = $newAnswerMark;
        return $this;
    }

    public function setMark($mark)
    {
        $this->mark = $mark;
        return $this;
    }

    public function setAmmount($ammount)
    {
        $this->ammount = $ammount;
        return $this;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

}
