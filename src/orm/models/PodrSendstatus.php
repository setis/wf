<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * PodrSendstatus
 *
 * @ORM\Table(name="podr_sendstatus")
 * @ORM\Entity
 */
class PodrSendstatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="contr_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $contrId;

    /**
     * @var integer
     *
     * @ORM\Column(name="status_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $statusId;


    /**
     * Set contrId
     *
     * @param integer $contrId
     *
     * @return PodrSendstatus
     */
    public function setContrId($contrId)
    {
        $this->contrId = $contrId;

        return $this;
    }

    /**
     * Get contrId
     *
     * @return integer
     */
    public function getContrId()
    {
        return $this->contrId;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return PodrSendstatus
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }
}

