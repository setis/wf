<?php

namespace models;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Syslog
 *
 * @ORM\Table(name="syslog", indexes={
 *     @ORM\Index(columns={"actiondate"}),
 *     @ORM\Index(columns={"module"}),
 *     @ORM\Index(columns={"action"}),
 *     @ORM\Index(columns={"`get`"}, flags={"fulltext"}),
 *     @ORM\Index(columns={"`post`"}, flags={"fulltext"}),
 *     @ORM\Index(columns={"session"}, flags={"fulltext"}),
 *     @ORM\Index(columns={"content"}, flags={"fulltext"})
 * })
 * @ORM\Entity
 */
class Syslog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer", nullable=true)
     */
    private $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="userfio", type="string", length=255, nullable=true)
     */
    private $userfio;

    /**
     * @var string
     *
     * @ORM\Column(name="userip", type="string", length=50)
     */
    private $userip;

    /**
     * @var string
     *
     * @ORM\Column(name="module", type="string", length=200)
     */
    private $module;

    /**
     * @var string
     *
     * @ORM\Column(name="`action`", type="string", length=200)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="`get`", type="text", nullable=true)
     */
    private $get;

    /**
     * @var string
     *
     * @ORM\Column(name="`post`", type="text", nullable=true)
     */
    private $post;

    /**
     * @var string
     *
     * @ORM\Column(name="session", type="text", nullable=true)
     */
    private $session;


    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="result", type="string", length=50)
     */
    private $result;

    /**
     * @var string
     *
     * @ORM\Column(name="session_id", type="string", length=50)
     */
    private $sessionId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="useragent", type="text")
     */
    private $useragent;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="actiondate", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $actiondate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return Syslog
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set userfio
     *
     * @param string $userfio
     *
     * @return Syslog
     */
    public function setUserfio($userfio)
    {
        $this->userfio = $userfio;

        return $this;
    }

    /**
     * Get userfio
     *
     * @return string
     */
    public function getUserfio()
    {
        return $this->userfio;
    }

    /**
     * Set userip
     *
     * @param string $userip
     *
     * @return Syslog
     */
    public function setUserip($userip)
    {
        $this->userip = $userip;

        return $this;
    }

    /**
     * Get userip
     *
     * @return string
     */
    public function getUserip()
    {
        return $this->userip;
    }

    /**
     * Set module
     *
     * @param string $module
     *
     * @return Syslog
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return Syslog
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set get
     *
     * @param string $get
     *
     * @return Syslog
     */
    public function setGet(array $get)
    {
        $this->get = json_encode($get, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

        return $this;
    }

    /**
     * Get get
     *
     * @return string
     */
    public function getGet()
    {
        return $this->get;
    }

    /**
     * Set post
     *
     * @param string $post
     *
     * @return Syslog
     */
    public function setPost(array $post)
    {
        $this->post = json_encode($post, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

        return $this;
    }

    /**
     * Get post
     *
     * @return string
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return Syslog
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set sessionId
     *
     * @param string $sessionId
     *
     * @return Syslog
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set useragent
     *
     * @param string $useragent
     *
     * @return Syslog
     */
    public function setUseragent($useragent)
    {
        $this->useragent = $useragent;

        return $this;
    }

    /**
     * Get useragent
     *
     * @return string
     */
    public function getUseragent()
    {
        return $this->useragent;
    }

    /**
     * Set actiondate
     *
     * @param DateTime $actiondate
     *
     * @return Syslog
     */
    public function setActiondate(DateTime $actiondate)
    {
        $this->actiondate = $actiondate;

        return $this;
    }

    /**
     * Get actiondate
     *
     * @return DateTime
     */
    public function getActiondate()
    {
        return $this->actiondate;
    }

    /**
     * @param string $session
     * @return Syslog
     */
    public function setSession($session)
    {
        $this->session = json_encode($session, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return $this;
    }

    /**
     * @return string
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param string $content
     * @return Syslog
     */
    public function setContent($content)
    {
        $this->content = json_encode($content, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}

