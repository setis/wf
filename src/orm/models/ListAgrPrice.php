<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Task\Doctrine\AbstractPrice;

/**
 * ListAgrPrice
 *
 * @ORM\Table(name="list_agr_price", indexes={
 *     @ORM\Index(columns={"agr_id"})
 * })
 * @ORM\Entity(repositoryClass="repository\ListAgrPriceRepository")
 */
class ListAgrPrice extends AbstractPrice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="agr_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $agrId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="s_title", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $sTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="price_type", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $priceType;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="p_comment", type="text", length=16777215, precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $pComment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="archive", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $archive;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set agrId
     *
     * @param integer $agrId
     *
     * @return ListAgrPrice
     */
    public function setAgrId($agrId)
    {
        $this->agrId = $agrId;

        return $this;
    }

    /**
     * Get agrId
     *
     * @return integer
     */
    public function getAgrId()
    {
        return $this->agrId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ListAgrPrice
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set sTitle
     *
     * @param string $sTitle
     *
     * @return ListAgrPrice
     */
    public function setSTitle($sTitle)
    {
        $this->sTitle = $sTitle;

        return $this;
    }

    /**
     * Get sTitle
     *
     * @return string
     */
    public function getSTitle()
    {
        return $this->sTitle;
    }

    /**
     * Set priceType
     *
     * @param string $priceType
     *
     * @return ListAgrPrice
     */
    public function setPriceType($priceType)
    {
        $this->priceType = $priceType;

        return $this;
    }

    /**
     * Get priceType
     *
     * @return string
     */
    public function getPriceType()
    {
        return $this->priceType;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return ListAgrPrice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set pComment
     *
     * @param string $pComment
     *
     * @return ListAgrPrice
     */
    public function setPComment($pComment)
    {
        $this->pComment = $pComment;

        return $this;
    }

    /**
     * Get pComment
     *
     * @return string
     */
    public function getPComment()
    {
        return $this->pComment;
    }

    /**
     * Set archive
     *
     * @param boolean $archive
     *
     * @return ListAgrPrice
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;

        return $this;
    }

    /**
     * Get archive
     *
     * @return boolean
     */
    public function getArchive()
    {
        return $this->archive;
    }
}

