<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use JMS\Serializer\Annotation as Serializer;
use WF\Tmc\Model\TmcHolderInterface;
use WF\Tmc\Model\TechnicianInterface;
use WF\Tmc\Model\TmcWarehouseInterface;

/**
 * Description of TmcHolder
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 *
 * @ORM\Table(name="tmc_holder_link")
 * @ORM\Entity
 */
class TmcHolderLink
{
    const TYPE_WAREHOUSE = 1;
    const TYPE_TECHNICIAN = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Serializer\Groups({"default", "TmcHolderLink.Technician"})
     */
    private $technician;

    /**
     * @var ListSc
     *
     * @ORM\ManyToOne(targetEntity="models\ListSc")
     * @ORM\JoinColumn(name="list_sc_id", referencedColumnName="id")
     * @Serializer\Groups({"default", "TmcHolderLink.Warehouse"})
     */
    private $warehouse;

    /**
     * @var ListSc
     *
     * @ORM\Column(name="type", type="smallint", nullable=true)
     */
    private $type;

    private function setTechnician(TechnicianInterface $technician)
    {
        $this->technician = $technician;
        $this->warehouse = null;
        $this->type = self::TYPE_TECHNICIAN;
    }

    private function setWarehouse(TmcWarehouseInterface $warehouse)
    {
        $this->warehouse = $warehouse;
        $this->technician = null;
        $this->type = self::TYPE_WAREHOUSE;
    }

    /**
     *
     * @return TmcHolderInterface
     */
    public function getHolder()
    {
        switch ($this->type) {
            case self::TYPE_WAREHOUSE:
                return $this->warehouse;

            case self::TYPE_TECHNICIAN:
                return $this->technician;

            default:
                break;
        }

        return null;
    }

    /**
     *
     * @param TmcHolderInterface $holder
     * @throws InvalidArgumentException
     */
    public function setHolder(TmcHolderInterface $holder = null)
    {
        switch (true) {
            case $holder instanceof TmcWarehouseInterface:
                $this->setWarehouse($holder);
                break;

            case $holder instanceof TechnicianInterface:
                $this->setTechnician($holder);
                break;

//            default:
//                throw new InvalidArgumentException('Unsupported TmcHolderInterface implementation: ' . get_class($holder));
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
