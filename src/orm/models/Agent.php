<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Agent
 *
 * @ORM\Table(name="agents", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"user_id", "contr_id", "work_type_id", "agreement_id"})
 * })
 * @ORM\Entity
 */
class Agent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @var ListContr
     *
     * @ORM\ManyToOne(targetEntity="ListContr", inversedBy="agents")
     * @ORM\JoinColumn(name="contr_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $partner;

    /**
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var TaskType
     *
     * @ORM\ManyToOne(targetEntity="TaskType")
     * @ORM\JoinColumn(name="work_type_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $taskType;

    /**
     * @var Agreement
     *
     * @ORM\ManyToOne(targetEntity="Agreement", inversedBy="agents")
     * @ORM\JoinColumn(name="agreement_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $agreement;

    public function getId()
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return ListContr
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param ListContr $partner
     * @return Agent
     */
    public function setPartner(ListContr $partner): Agent
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * @return TaskType
     */
    public function getTaskType()
    {
        return $this->taskType;
    }

    /**
     * @param TaskType $taskType
     * @return Agent
     */
    public function setTaskType($taskType): Agent
    {
        $this->taskType = $taskType;
        return $this;
    }

    /**
     * @return Agreement
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * @param Agreement $agreement
     * @return Agent
     */
    public function setAgreement($agreement)
    {
        $this->agreement = $agreement;
        return $this;
    }

}

