<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListActsWtypes
 *
 * @ORM\Table(name="list_acts_wtypes")
 * @ORM\Entity
 */
class ListActsWtypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=250, precision=0, scale=0, nullable=false, unique=false)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="edizm", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $edizm;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ListActsWtypes
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set edizm
     *
     * @param integer $edizm
     *
     * @return ListActsWtypes
     */
    public function setEdizm($edizm)
    {
        $this->edizm = $edizm;

        return $this;
    }

    /**
     * Get edizm
     *
     * @return integer
     */
    public function getEdizm()
    {
        return $this->edizm;
    }
}

