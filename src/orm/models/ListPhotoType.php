<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ListPhotoType
 *
 * @ORM\Table(name="list_photo_types")
 * @ORM\Entity(repositoryClass="repository\ListPhotoTypeRepository")
 */
class ListPhotoType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="task_type", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $taskType;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200, precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="required", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $required;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $active;

    /**
     * @var \models\TaskType
     *
     * @ORM\ManyToOne(targetEntity="models\TaskType", inversedBy="photoTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="wtype", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * })
     */
    private $workType;

    /**
     *
     * @var ListContr
     *
     * @ORM\ManyToOne(targetEntity="models\ListContr")
     * @ORM\JoinColumn(name="contr_id", referencedColumnName="id")
     * @Serializer\Groups({"default"})
     * @Serializer\MaxDepth(1)
     */
    private $partner;


    /**
     * @var TaskPhoto[]
     *
     * @ORM\OneToMany(targetEntity="models\TaskPhoto", mappedBy="type")
     * @Serializer\Groups({"default"})
     *
     */
    private $photos;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set taskType
     *
     * @param string $taskType
     *
     * @return ListPhotoType
     */
    public function setTaskType($taskType)
    {
        $this->taskType = $taskType;

        return $this;
    }

    /**
     * Get taskType
     *
     * @return string
     */
    public function getTaskType()
    {
        return $this->taskType;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ListPhotoType
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set required
     *
     * @param boolean $required
     *
     * @return ListPhotoType
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Get required
     *
     * @return boolean
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return ListPhotoType
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param TaskType $workType
     * @return ListPhotoType
     */
    public function setWorkType($workType)
    {
        $this->workType = $workType;
        return $this;
    }

    /**
     * @return TaskType
     */
    public function getWorkType()
    {
        return $this->workType;
    }

    /**
     * @param ListContr $partner
     * @return ListPhotoType
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * @return ListContr
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @return TaskPhoto[]
     */
    public function getPhotos()
    {
        return $this->photos;
    }
}

