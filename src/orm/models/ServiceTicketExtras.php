<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceTicketExtras
 *
 * @ORM\Table(name="service_ticket_extras", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"task_id"})
 * })
 * @ORM\Entity
 */
class ServiceTicketExtras
{

    /**
     * @var string
     *
     * @ORM\Column(name="internet_ap", type="string", length=20, nullable=true)
     */
    private $internetAp;

    /**
     * @var string
     *
     * @ORM\Column(name="internet_speed", type="string", length=20, nullable=true)
     */
    private $internetSpeed;

    /**
     * @var string
     *
     * @ORM\Column(name="internet_equipment", type="string", length=200, nullable=true)
     */
    private $internetEquipment;

    /**
     * @var string
     *
     * @ORM\Column(name="ctv_ap", type="string", length=20, nullable=true)
     */
    private $ctvAp;

    /**
     * @var integer
     *
     * @ORM\Column(name="ctv_channel", type="integer", nullable=true)
     */
    private $ctvChannel;

    /**
     * @var string
     *
     * @ORM\Column(name="ctv_equipment", type="string", length=200, nullable=true)
     */
    private $ctvEquipment;

    /**
     * @var string
     *
     * @ORM\Column(name="ctv_prov", type="string", length=200, nullable=true)
     */
    private $ctvProv;

    /**
     * @var string
     *
     * @ORM\Column(name="internet_plus_tv_ap", type="string", length=20, nullable=true)
     */
    private $internetPlusTvAp;

    /**
     * @var string
     *
     * @ORM\Column(name="internet_plus_tv_speed", type="string", length=20, nullable=true)
     */
    private $internetPlusTvSpeed;

    /**
     * @var string
     *
     * @ORM\Column(name="internet_plus_tv_channel", type="string", length=20, nullable=true)
     */
    private $internetPlusTvChannel;

    /**
     * @var string
     *
     * @ORM\Column(name="internet_plus_tv_equipment1", type="string", length=250, nullable=true)
     */
    private $internetPlusTvEquipment1;

    /**
     * @var string
     *
     * @ORM\Column(name="internet_plus_tv_equipment2", type="string", length=250, nullable=true)
     */
    private $internetPlusTvEquipment2;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_ap", type="string", length=20, nullable=true)
     */
    private $phoneAp;

    /**
     * @var string
     *
     * @ORM\Column(name="vas1_name", type="string", length=20, nullable=true)
     */
    private $vas1Name;

    /**
     * @var string
     *
     * @ORM\Column(name="vas1_value", type="string", length=20, nullable=true)
     */
    private $vas1Value;

    /**
     * @var string
     *
     * @ORM\Column(name="vas2_name", type="string", length=20, nullable=true)
     */
    private $vas2Name;

    /**
     * @var string
     *
     * @ORM\Column(name="vas2_value", type="string", length=20, nullable=true)
     */
    private $vas2Value;

    /**
     * @var string
     *
     * @ORM\Column(name="vas3_name", type="string", length=20, nullable=true)
     */
    private $vas3Name;

    /**
     * @var string
     *
     * @ORM\Column(name="vas3_value", type="string", length=20, nullable=true)
     */
    private $vas3Value;

    /**
     * @var integer
     *
     * @ORM\Column(name="ispoln_id", type="integer", nullable=true)
     */
    private $ispolnId;

    /**
     * @var integer
     *
     * @ORM\Column(name="service_for_sale_id", type="integer", nullable=true)
     */
    private $serviceForSaleId;

    /**
     * @var string
     *
     * @ORM\Column(name="sold_equipment", type="string", length=200, nullable=true)
     */
    private $soldEquipment;

    /**
     * @var string
     *
     * @ORM\Column(name="tarif_info", type="string", length=200, nullable=true)
     */
    private $tarifInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="vas_info", type="string", length=200, nullable=true)
     */
    private $vasInfo;

    /**
     * @var integer
     *
     * @ORM\Column(name="subtype", type="integer", nullable=true)
     */
    private $subtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="sales_channel", type="integer", nullable=true)
     */
    private $salesChannel;

    /**
     * @var string
     *
     * @ORM\Column(name="clnttnum", type="string", length=20, nullable=true)
     */
    private $clnttnum;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_minutes", type="string", length=20, nullable=true)
     */
    private $phoneMinutes;

    /**
     * @var string
     *
     * @ORM\Column(name="current_operator", type="string", length=200, nullable=true)
     */
    private $currentOperator;

    /**
     * @var string
     *
     * @ORM\Column(name="current_services", type="string", length=200, nullable=true)
     */
    private $currentServices;

    /**
     * @var string
     *
     * @ORM\Column(name="current_ap", type="string", length=200, nullable=true)
     */
    private $currentAp;

    /**
     * @var string
     *
     * @ORM\Column(name="current_comments", type="text", length=65535, nullable=true)
     */
    private $currentComments;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Task
     *
     * @ORM\ManyToOne(targetEntity="Task")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var Tariff
     *
     * @ORM\ManyToOne(targetEntity="models\Tariff")
     * @ORM\JoinColumn(name="phone_tarif_id", nullable=true, onDelete="SET NULL")
     */
    private $phoneTariff;

    /**
     * @var Tariff
     *
     * @ORM\ManyToOne(targetEntity="models\Tariff")
     * @ORM\JoinColumn(name="internet_plus_tv_tarif_id", nullable=true, onDelete="SET NULL")
     */
    private $internetPlusTvTariff;

    /**
     * @var Tariff
     *
     * @ORM\ManyToOne(targetEntity="models\Tariff")
     * @ORM\JoinColumn(name="internet_tarif_id", nullable=true, onDelete="SET NULL")
     */
    private $internetTariff;

    /**
     * @var Tariff
     *
     * @ORM\ManyToOne(targetEntity="models\Tariff")
     * @ORM\JoinColumn(name="ctv_tarif_id", nullable=true, onDelete="SET NULL")
     */
    private $ctvTariff;

    /**
     * Set internetAp
     *
     * @param string $internetAp
     *
     * @return ServiceTicketExtras
     */
    public function setInternetAp($internetAp)
    {
        $this->internetAp = $internetAp;

        return $this;
    }

    /**
     * Get internetAp
     *
     * @return string
     */
    public function getInternetAp()
    {
        return $this->internetAp;
    }

    /**
     * Set internetSpeed
     *
     * @param string $internetSpeed
     *
     * @return ServiceTicketExtras
     */
    public function setInternetSpeed($internetSpeed)
    {
        $this->internetSpeed = $internetSpeed;

        return $this;
    }

    /**
     * Get internetSpeed
     *
     * @return string
     */
    public function getInternetSpeed()
    {
        return $this->internetSpeed;
    }

    /**
     * Set internetEquipment
     *
     * @param string $internetEquipment
     *
     * @return ServiceTicketExtras
     */
    public function setInternetEquipment($internetEquipment)
    {
        $this->internetEquipment = $internetEquipment;

        return $this;
    }

    /**
     * Get internetEquipment
     *
     * @return string
     */
    public function getInternetEquipment()
    {
        return $this->internetEquipment;
    }

    /**
     * Set ctvTarifId
     *
     * @param integer $ctvTarifId
     *
     * @return ServiceTicketExtras
     */
    public function setCtvTarifId($ctvTarifId)
    {
        $this->ctvTarifId = $ctvTarifId;

        return $this;
    }

    /**
     * Get ctvTarifId
     *
     * @return integer
     */
    public function getCtvTarifId()
    {
        return $this->ctvTarifId;
    }

    /**
     * Set ctvAp
     *
     * @param string $ctvAp
     *
     * @return ServiceTicketExtras
     */
    public function setCtvAp($ctvAp)
    {
        $this->ctvAp = $ctvAp;

        return $this;
    }

    /**
     * Get ctvAp
     *
     * @return string
     */
    public function getCtvAp()
    {
        return $this->ctvAp;
    }

    /**
     * Set ctvChannel
     *
     * @param integer $ctvChannel
     *
     * @return ServiceTicketExtras
     */
    public function setCtvChannel($ctvChannel)
    {
        $this->ctvChannel = $ctvChannel;

        return $this;
    }

    /**
     * Get ctvChannel
     *
     * @return integer
     */
    public function getCtvChannel()
    {
        return $this->ctvChannel;
    }

    /**
     * Set ctvEquipment
     *
     * @param string $ctvEquipment
     *
     * @return ServiceTicketExtras
     */
    public function setCtvEquipment($ctvEquipment)
    {
        $this->ctvEquipment = $ctvEquipment;

        return $this;
    }

    /**
     * Get ctvEquipment
     *
     * @return string
     */
    public function getCtvEquipment()
    {
        return $this->ctvEquipment;
    }

    /**
     * Set ctvProv
     *
     * @param string $ctvProv
     *
     * @return ServiceTicketExtras
     */
    public function setCtvProv($ctvProv)
    {
        $this->ctvProv = $ctvProv;

        return $this;
    }

    /**
     * Get ctvProv
     *
     * @return string
     */
    public function getCtvProv()
    {
        return $this->ctvProv;
    }

    /**
     * Set internetPlusTvTarifId
     *
     * @param integer $internetPlusTvTarifId
     *
     * @return ServiceTicketExtras
     */
    public function setInternetPlusTvTarifId($internetPlusTvTarifId)
    {
        $this->internetPlusTvTarifId = $internetPlusTvTarifId;

        return $this;
    }

    /**
     * Get internetPlusTvTarifId
     *
     * @return integer
     */
    public function getInternetPlusTvTarifId()
    {
        return $this->internetPlusTvTarifId;
    }

    /**
     * Set internetPlusTvAp
     *
     * @param string $internetPlusTvAp
     *
     * @return ServiceTicketExtras
     */
    public function setInternetPlusTvAp($internetPlusTvAp)
    {
        $this->internetPlusTvAp = $internetPlusTvAp;

        return $this;
    }

    /**
     * Get internetPlusTvAp
     *
     * @return string
     */
    public function getInternetPlusTvAp()
    {
        return $this->internetPlusTvAp;
    }

    /**
     * Set internetPlusTvSpeed
     *
     * @param string $internetPlusTvSpeed
     *
     * @return ServiceTicketExtras
     */
    public function setInternetPlusTvSpeed($internetPlusTvSpeed)
    {
        $this->internetPlusTvSpeed = $internetPlusTvSpeed;

        return $this;
    }

    /**
     * Get internetPlusTvSpeed
     *
     * @return string
     */
    public function getInternetPlusTvSpeed()
    {
        return $this->internetPlusTvSpeed;
    }

    /**
     * Set internetPlusTvChannel
     *
     * @param string $internetPlusTvChannel
     *
     * @return ServiceTicketExtras
     */
    public function setInternetPlusTvChannel($internetPlusTvChannel)
    {
        $this->internetPlusTvChannel = $internetPlusTvChannel;

        return $this;
    }

    /**
     * Get internetPlusTvChannel
     *
     * @return string
     */
    public function getInternetPlusTvChannel()
    {
        return $this->internetPlusTvChannel;
    }

    /**
     * Set internetPlusTvEquipment1
     *
     * @param string $internetPlusTvEquipment1
     *
     * @return ServiceTicketExtras
     */
    public function setInternetPlusTvEquipment1($internetPlusTvEquipment1)
    {
        $this->internetPlusTvEquipment1 = $internetPlusTvEquipment1;

        return $this;
    }

    /**
     * Get internetPlusTvEquipment1
     *
     * @return string
     */
    public function getInternetPlusTvEquipment1()
    {
        return $this->internetPlusTvEquipment1;
    }

    /**
     * Set internetPlusTvEquipment2
     *
     * @param string $internetPlusTvEquipment2
     *
     * @return ServiceTicketExtras
     */
    public function setInternetPlusTvEquipment2($internetPlusTvEquipment2)
    {
        $this->internetPlusTvEquipment2 = $internetPlusTvEquipment2;

        return $this;
    }

    /**
     * Get internetPlusTvEquipment2
     *
     * @return string
     */
    public function getInternetPlusTvEquipment2()
    {
        return $this->internetPlusTvEquipment2;
    }

    /**
     * Set phoneTarifId
     *
     * @param integer $phoneTarifId
     *
     * @return ServiceTicketExtras
     */
    public function setPhoneTarifId($phoneTarifId)
    {
        $this->phoneTarifId = $phoneTarifId;

        return $this;
    }

    /**
     * Get phoneTarifId
     *
     * @return integer
     */
    public function getPhoneTarifId()
    {
        return $this->phoneTarifId;
    }

    /**
     * Set phoneAp
     *
     * @param string $phoneAp
     *
     * @return ServiceTicketExtras
     */
    public function setPhoneAp($phoneAp)
    {
        $this->phoneAp = $phoneAp;

        return $this;
    }

    /**
     * Get phoneAp
     *
     * @return string
     */
    public function getPhoneAp()
    {
        return $this->phoneAp;
    }

    /**
     * Set vas1Name
     *
     * @param string $vas1Name
     *
     * @return ServiceTicketExtras
     */
    public function setVas1Name($vas1Name)
    {
        $this->vas1Name = $vas1Name;

        return $this;
    }

    /**
     * Get vas1Name
     *
     * @return string
     */
    public function getVas1Name()
    {
        return $this->vas1Name;
    }

    /**
     * Set vas1Value
     *
     * @param string $vas1Value
     *
     * @return ServiceTicketExtras
     */
    public function setVas1Value($vas1Value)
    {
        $this->vas1Value = $vas1Value;

        return $this;
    }

    /**
     * Get vas1Value
     *
     * @return string
     */
    public function getVas1Value()
    {
        return $this->vas1Value;
    }

    /**
     * Set vas2Name
     *
     * @param string $vas2Name
     *
     * @return ServiceTicketExtras
     */
    public function setVas2Name($vas2Name)
    {
        $this->vas2Name = $vas2Name;

        return $this;
    }

    /**
     * Get vas2Name
     *
     * @return string
     */
    public function getVas2Name()
    {
        return $this->vas2Name;
    }

    /**
     * Set vas2Value
     *
     * @param string $vas2Value
     *
     * @return ServiceTicketExtras
     */
    public function setVas2Value($vas2Value)
    {
        $this->vas2Value = $vas2Value;

        return $this;
    }

    /**
     * Get vas2Value
     *
     * @return string
     */
    public function getVas2Value()
    {
        return $this->vas2Value;
    }

    /**
     * Set vas3Name
     *
     * @param string $vas3Name
     *
     * @return ServiceTicketExtras
     */
    public function setVas3Name($vas3Name)
    {
        $this->vas3Name = $vas3Name;

        return $this;
    }

    /**
     * Get vas3Name
     *
     * @return string
     */
    public function getVas3Name()
    {
        return $this->vas3Name;
    }

    /**
     * Set vas3Value
     *
     * @param string $vas3Value
     *
     * @return ServiceTicketExtras
     */
    public function setVas3Value($vas3Value)
    {
        $this->vas3Value = $vas3Value;

        return $this;
    }

    /**
     * Get vas3Value
     *
     * @return string
     */
    public function getVas3Value()
    {
        return $this->vas3Value;
    }

    /**
     * Set ispolnId
     *
     * @param integer $ispolnId
     *
     * @return ServiceTicketExtras
     */
    public function setIspolnId($ispolnId)
    {
        $this->ispolnId = $ispolnId;

        return $this;
    }

    /**
     * Get ispolnId
     *
     * @return integer
     */
    public function getIspolnId()
    {
        return $this->ispolnId;
    }

    /**
     * Set serviceForSaleId
     *
     * @param integer $serviceForSaleId
     *
     * @return ServiceTicketExtras
     */
    public function setServiceForSaleId($serviceForSaleId)
    {
        $this->serviceForSaleId = $serviceForSaleId;

        return $this;
    }

    /**
     * Get serviceForSaleId
     *
     * @return integer
     */
    public function getServiceForSaleId()
    {
        return $this->serviceForSaleId;
    }

    /**
     * Set soldEquipment
     *
     * @param string $soldEquipment
     *
     * @return ServiceTicketExtras
     */
    public function setSoldEquipment($soldEquipment)
    {
        $this->soldEquipment = $soldEquipment;

        return $this;
    }

    /**
     * Get soldEquipment
     *
     * @return string
     */
    public function getSoldEquipment()
    {
        return $this->soldEquipment;
    }

    /**
     * Set tarifInfo
     *
     * @param string $tarifInfo
     *
     * @return ServiceTicketExtras
     */
    public function setTarifInfo($tarifInfo)
    {
        $this->tarifInfo = $tarifInfo;

        return $this;
    }

    /**
     * Get tarifInfo
     *
     * @return string
     */
    public function getTarifInfo()
    {
        return $this->tarifInfo;
    }

    /**
     * Set vasInfo
     *
     * @param string $vasInfo
     *
     * @return ServiceTicketExtras
     */
    public function setVasInfo($vasInfo)
    {
        $this->vasInfo = $vasInfo;

        return $this;
    }

    /**
     * Get vasInfo
     *
     * @return string
     */
    public function getVasInfo()
    {
        return $this->vasInfo;
    }

    /**
     * Set subtype
     *
     * @param integer $subtype
     *
     * @return ServiceTicketExtras
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;

        return $this;
    }

    /**
     * Get subtype
     *
     * @return integer
     */
    public function getSubtype()
    {
        return $this->subtype;
    }

    /**
     * Set salesChannel
     *
     * @param integer $salesChannel
     *
     * @return ServiceTicketExtras
     */
    public function setSalesChannel($salesChannel)
    {
        $this->salesChannel = $salesChannel;

        return $this;
    }

    /**
     * Get salesChannel
     *
     * @return integer
     */
    public function getSalesChannel()
    {
        return $this->salesChannel;
    }

    /**
     * Set clnttnum
     *
     * @param string $clnttnum
     *
     * @return ServiceTicketExtras
     */
    public function setClnttnum($clnttnum)
    {
        $this->clnttnum = $clnttnum;

        return $this;
    }

    /**
     * Get clnttnum
     *
     * @return string
     */
    public function getClnttnum()
    {
        return $this->clnttnum;
    }

    /**
     * Set phoneMinutes
     *
     * @param string $phoneMinutes
     *
     * @return ServiceTicketExtras
     */
    public function setPhoneMinutes($phoneMinutes)
    {
        $this->phoneMinutes = $phoneMinutes;

        return $this;
    }

    /**
     * Get phoneMinutes
     *
     * @return string
     */
    public function getPhoneMinutes()
    {
        return $this->phoneMinutes;
    }

    /**
     * Set currentOperator
     *
     * @param string $currentOperator
     *
     * @return ServiceTicketExtras
     */
    public function setCurrentOperator($currentOperator)
    {
        $this->currentOperator = $currentOperator;

        return $this;
    }

    /**
     * Get currentOperator
     *
     * @return string
     */
    public function getCurrentOperator()
    {
        return $this->currentOperator;
    }

    /**
     * Set currentServices
     *
     * @param string $currentServices
     *
     * @return ServiceTicketExtras
     */
    public function setCurrentServices($currentServices)
    {
        $this->currentServices = $currentServices;

        return $this;
    }

    /**
     * Get currentServices
     *
     * @return string
     */
    public function getCurrentServices()
    {
        return $this->currentServices;
    }

    /**
     * Set currentAp
     *
     * @param string $currentAp
     *
     * @return ServiceTicketExtras
     */
    public function setCurrentAp($currentAp)
    {
        $this->currentAp = $currentAp;

        return $this;
    }

    /**
     * Get currentAp
     *
     * @return string
     */
    public function getCurrentAp()
    {
        return $this->currentAp;
    }

    /**
     * Set currentComments
     *
     * @param string $currentComments
     *
     * @return ServiceTicketExtras
     */
    public function setCurrentComments($currentComments)
    {
        $this->currentComments = $currentComments;

        return $this;
    }

    /**
     * Get currentComments
     *
     * @return string
     */
    public function getCurrentComments()
    {
        return $this->currentComments;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set task
     *
     * @param Task $task
     *
     * @return ServiceTicketExtras
     */
    public function setTask(Task $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @return Tariff
     */
    public function getPhoneTariff()
    {
        return $this->phoneTariff;
    }

    /**
     * @param Tariff $phoneTariff
     * @return ServiceTicketExtras
     */
    public function setPhoneTariff(Tariff $phoneTariff): ServiceTicketExtras
    {
        $this->phoneTariff = $phoneTariff;
        return $this;
    }

    /**
     * @return Tariff
     */
    public function getInternetPlusTvTariff()
    {
        return $this->internetPlusTvTariff;
    }

    /**
     * @param Tariff $internetPlusTvTariff
     * @return ServiceTicketExtras
     */
    public function setInternetPlusTvTariff(Tariff $internetPlusTvTariff): ServiceTicketExtras
    {
        $this->internetPlusTvTariff = $internetPlusTvTariff;
        return $this;
    }

    /**
     * @return Tariff
     */
    public function getInternetTariff()
    {
        return $this->internetTariff;
    }

    /**
     * @param Tariff $internetTariff
     * @return ServiceTicketExtras
     */
    public function setInternetTariff(Tariff $internetTariff): ServiceTicketExtras
    {
        $this->internetTariff = $internetTariff;
        return $this;
    }

    /**
     * @return Tariff
     */
    public function getCtvTariff()
    {
        return $this->ctvTariff;
    }

    /**
     * @param Tariff $ctvTariff
     * @return ServiceTicketExtras
     */
    public function setCtvTariff(Tariff $ctvTariff): ServiceTicketExtras
    {
        $this->ctvTariff = $ctvTariff;
        return $this;
    }

    function __toString()
    {
        $result = '';

        if($this->getInternetTariff() instanceof Tariff) {
            $result .= "{$this->getInternetTariff()->getTarifName()};";
        }

        if($this->getInternetPlusTvTariff() instanceof Tariff) {
            $result .= "{$this->getInternetPlusTvTariff()->getTarifName()};";
        }

        if($this->getCtvTariff() instanceof Tariff) {
            $result .= "{$this->getCtvTariff()->getTarifName()};";
        }

        if($this->getPhoneTariff() instanceof Tariff) {
            $result .= "{$this->getPhoneTariff()->getTarifName()};";
        }

        return $result;
    }

}

