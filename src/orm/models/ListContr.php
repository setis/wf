<?php

namespace models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Core\ClientInterface;
use WF\Tmc\Model\TmcPartnerInterface;

/**
 * ListContr
 *
 * @ORM\Table(name="list_contr")
 * @ORM\Entity
 */
class ListContr implements TmcPartnerInterface, ClientInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="contr_title", type="string", length=255, nullable=false)
     * @Serializer\Groups({"default"})
     * @Serializer\SerializedName("title")
     */
    private $contrTitle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="contr_type", type="boolean", nullable=false)
     */
    private $contrType;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_type2", type="integer", nullable=true)
     */
    private $contrType2;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_type3", type="integer", nullable=true)
     */
    private $contrType3;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_type4", type="integer", nullable=true)
     */
    private $contrType4;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_type_tm", type="integer", nullable=true)
     */
    private $contrTypeTm;

    /**
     * @var string
     *
     * @ORM\Column(name="contr_desc", type="text", length=16777215, nullable=true)
     */
    private $contrDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="text", length=16777215, nullable=true)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="official_title", type="string", length=255, nullable=true)
     */
    private $officialTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="agentfee", nullable=true)
     */
    private $agentfee;

    /**
     * @var string
     *
     * @ORM\Column(name="contragentfee", nullable=true)
     */
    private $contragentfee;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_client", type="boolean", nullable=true)
     */
    private $isClient;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_supplier", type="boolean", nullable=true)
     */
    private $isSupplier;

    /**
     * @var boolean
     *
     * @ORM\Column(name="proptype_id", type="boolean", nullable=true)
     */
    private $proptypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", length=255, nullable=true)
     */
    private $site;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string", length=255, nullable=true)
     */
    private $inn;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bn", type="boolean", nullable=true)
     */
    private $bn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="it_from", type="time", nullable=true)
     */
    private $itFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="it_to", type="time", nullable=true)
     */
    private $itTo;

    /**
     * @var Agent[]
     *
     * @ORM\OneToMany(targetEntity="models\Agent", mappedBy="partner")
     */
    private $agents;

    /**
     * @var Agreement[]
     *
     * @ORM\OneToMany(targetEntity="models\Agreement", mappedBy="partner")
     */
    private $agreements;


    public function __construct()
    {
        $this->agents = new ArrayCollection();
        $this->agreements = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contrTitle
     *
     * @param string $contrTitle
     *
     * @return ListContr
     */
    public function setContrTitle($contrTitle)
    {
        $this->contrTitle = $contrTitle;

        return $this;
    }

    /**
     * Get contrTitle
     *
     * @return string
     */
    public function getContrTitle()
    {
        return $this->contrTitle;
    }

    /**
     * Set contrType
     *
     * @param boolean $contrType
     *
     * @return ListContr
     */
    public function setContrType($contrType)
    {
        $this->contrType = $contrType;

        return $this;
    }

    /**
     * Get contrType
     *
     * @return boolean
     */
    public function getContrType()
    {
        return $this->contrType;
    }

    /**
     * Set contrType2
     *
     * @param integer $contrType2
     *
     * @return ListContr
     */
    public function setContrType2($contrType2)
    {
        $this->contrType2 = $contrType2;

        return $this;
    }

    /**
     * Get contrType2
     *
     * @return integer
     */
    public function getContrType2()
    {
        return $this->contrType2;
    }

    /**
     * Set contrType3
     *
     * @param integer $contrType3
     *
     * @return ListContr
     */
    public function setContrType3($contrType3)
    {
        $this->contrType3 = $contrType3;

        return $this;
    }

    /**
     * Get contrType3
     *
     * @return integer
     */
    public function getContrType3()
    {
        return $this->contrType3;
    }

    /**
     * Set contrType4
     *
     * @param integer $contrType4
     *
     * @return ListContr
     */
    public function setContrType4($contrType4)
    {
        $this->contrType4 = $contrType4;

        return $this;
    }

    /**
     * Get contrType4
     *
     * @return integer
     */
    public function getContrType4()
    {
        return $this->contrType4;
    }

    /**
     * Set contrDesc
     *
     * @param string $contrDesc
     *
     * @return ListContr
     */
    public function setContrDesc($contrDesc)
    {
        $this->contrDesc = $contrDesc;

        return $this;
    }

    /**
     * Get contrDesc
     *
     * @return string
     */
    public function getContrDesc()
    {
        return $this->contrDesc;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return ListContr
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set officialTitle
     *
     * @param string $officialTitle
     *
     * @return ListContr
     */
    public function setOfficialTitle($officialTitle)
    {
        $this->officialTitle = $officialTitle;

        return $this;
    }

    /**
     * Get officialTitle
     *
     * @return string
     */
    public function getOfficialTitle()
    {
        return $this->officialTitle;
    }

    /**
     * Set agentfee
     *
     * @param string $agentfee
     *
     * @return ListContr
     */
    public function setAgentfee($agentfee)
    {
        $this->agentfee = $agentfee;

        return $this;
    }

    /**
     * Get agentfee
     *
     * @return string
     */
    public function getAgentfee()
    {
        return $this->agentfee;
    }

    /**
     * Set contragentfee
     *
     * @param string $contragentfee
     *
     * @return ListContr
     */
    public function setContragentfee($contragentfee)
    {
        $this->contragentfee = $contragentfee;

        return $this;
    }

    /**
     * Get contragentfee
     *
     * @return string
     */
    public function getContragentfee()
    {
        return $this->contragentfee;
    }

    /**
     * Set isClient
     *
     * @param boolean $isClient
     *
     * @return ListContr
     */
    public function setIsClient($isClient)
    {
        $this->isClient = $isClient;

        return $this;
    }

    /**
     * Get isClient
     *
     * @return boolean
     */
    public function getIsClient()
    {
        return $this->isClient;
    }

    /**
     * Set isSupplier
     *
     * @param boolean $isSupplier
     *
     * @return ListContr
     */
    public function setIsSupplier($isSupplier)
    {
        $this->isSupplier = $isSupplier;

        return $this;
    }

    /**
     * Get isSupplier
     *
     * @return boolean
     */
    public function getIsSupplier()
    {
        return $this->isSupplier;
    }

    /**
     * Set proptypeId
     *
     * @param boolean $proptypeId
     *
     * @return ListContr
     */
    public function setProptypeId($proptypeId)
    {
        $this->proptypeId = $proptypeId;

        return $this;
    }

    /**
     * Get proptypeId
     *
     * @return boolean
     */
    public function getProptypeId()
    {
        return $this->proptypeId;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return ListContr
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return ListContr
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ListContr
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set site
     *
     * @param string $site
     *
     * @return ListContr
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set inn
     *
     * @param string $inn
     *
     * @return ListContr
     */
    public function setInn($inn)
    {
        $this->inn = $inn;

        return $this;
    }

    /**
     * Get inn
     *
     * @return string
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * Set bn
     *
     * @param boolean $bn
     *
     * @return ListContr
     */
    public function setBn($bn)
    {
        $this->bn = $bn;

        return $this;
    }

    /**
     * Get bn
     *
     * @return boolean
     */
    public function getBn()
    {
        return $this->bn;
    }

    /**
     * Set itFrom
     *
     * @param \DateTime $itFrom
     *
     * @return ListContr
     */
    public function setItFrom($itFrom)
    {
        $this->itFrom = $itFrom;

        return $this;
    }

    /**
     * Get itFrom
     *
     * @return \DateTime
     */
    public function getItFrom()
    {
        return $this->itFrom;
    }

    /**
     * Set itTo
     *
     * @param \DateTime $itTo
     *
     * @return ListContr
     */
    public function setItTo($itTo)
    {
        $this->itTo = $itTo;

        return $this;
    }

    /**
     * Get itTo
     *
     * @return \DateTime
     */
    public function getItTo()
    {
        return $this->itTo;
    }

    public function getClientId()
    {
        return $this->id;
    }

    public function getClientName()
    {
        return $this->officialTitle;
    }

    public function getPartnerId()
    {
        return $this->id;
    }

    public function getPartnerName()
    {
        return $this->officialTitle;
    }

    /**
     * @param int $contrTypeTm
     * @return ListContr
     */
    public function setContrTypeTm($contrTypeTm)
    {
        $this->contrTypeTm = $contrTypeTm;
        return $this;
    }

    /**
     * @return int
     */
    public function getContrTypeTm()
    {
        return $this->contrTypeTm;
    }

    public function __toString()
    {
        return $this->contrTitle;
    }

    /**
     * @return Agreement[]
     */
    public function getAgreements(): array
    {
        return $this->agreements;
    }

    /**
     * @return Agent[]
     */
    public function getAgents(): array
    {
        return $this->agents;
    }

    public function removeAgent(Agent $agent)
    {
        if($this->agents->contains($agent)) {
            $this->agents->removeElement($agent);
        }

        return $this;
    }

    public function addAgent(Agent $agent)
    {
        if(!$this->agents->contains($agent)) {
            $this->agents->add($agent);
        }

        return $this;
    }
}

