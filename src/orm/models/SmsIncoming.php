<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * SmsIncoming
 *
 * @ORM\Table(name="sms_incoming", indexes={
 *     @ORM\Index(columns={"is_new"}), 
 *     @ORM\Index(columns={"message_id"}), 
 *     @ORM\Index(columns={"message_date"})
 * })
 * @ORM\Entity
 */
class SmsIncoming
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message_id", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $messageId;

    /**
     * @var string
     *
     * @ORM\Column(name="from", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $from;

    /**
     * @var string
     *
     * @ORM\Column(name="to", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $to;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=16777215, precision=0, scale=0, nullable=false, unique=false)
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="message_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $messageDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_recieve", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dateRecieve;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_new", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isNew;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set messageId
     *
     * @param string $messageId
     *
     * @return SmsIncoming
     */
    public function setMessageId($messageId)
    {
        $this->messageId = $messageId;

        return $this;
    }

    /**
     * Get messageId
     *
     * @return string
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * Set from
     *
     * @param string $from
     *
     * @return SmsIncoming
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param string $to
     *
     * @return SmsIncoming
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return SmsIncoming
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set messageDate
     *
     * @param \DateTime $messageDate
     *
     * @return SmsIncoming
     */
    public function setMessageDate($messageDate)
    {
        $this->messageDate = $messageDate;

        return $this;
    }

    /**
     * Get messageDate
     *
     * @return \DateTime
     */
    public function getMessageDate()
    {
        return $this->messageDate;
    }

    /**
     * Set dateRecieve
     *
     * @param \DateTime $dateRecieve
     *
     * @return SmsIncoming
     */
    public function setDateRecieve($dateRecieve)
    {
        $this->dateRecieve = $dateRecieve;

        return $this;
    }

    /**
     * Get dateRecieve
     *
     * @return \DateTime
     */
    public function getDateRecieve()
    {
        return $this->dateRecieve;
    }

    /**
     * Set isNew
     *
     * @param boolean $isNew
     *
     * @return SmsIncoming
     */
    public function setIsNew($isNew)
    {
        $this->isNew = $isNew;

        return $this;
    }

    /**
     * Get isNew
     *
     * @return boolean
     */
    public function getIsNew()
    {
        return $this->isNew;
    }
}

