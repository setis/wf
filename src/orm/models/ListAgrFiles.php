<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListAgrFiles
 *
 * @ORM\Table(name="list_agr_files")
 * @ORM\Entity
 */
class ListAgrFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="agr_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $agrId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="file_type", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $fileType;

    /**
     * @var integer
     *
     * @ORM\Column(name="act_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $actId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ds_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dsId;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $fileName;

    /**
     * @var string
     *
     * @ORM\Column(name="desc", type="text", length=16777215, precision=0, scale=0, nullable=true, unique=false)
     */
    private $desc;

    /**
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $mimeType;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="uploaddate", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $uploaddate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set agrId
     *
     * @param integer $agrId
     *
     * @return ListAgrFiles
     */
    public function setAgrId($agrId)
    {
        $this->agrId = $agrId;

        return $this;
    }

    /**
     * Get agrId
     *
     * @return integer
     */
    public function getAgrId()
    {
        return $this->agrId;
    }

    /**
     * Set fileType
     *
     * @param boolean $fileType
     *
     * @return ListAgrFiles
     */
    public function setFileType($fileType)
    {
        $this->fileType = $fileType;

        return $this;
    }

    /**
     * Get fileType
     *
     * @return boolean
     */
    public function getFileType()
    {
        return $this->fileType;
    }

    /**
     * Set actId
     *
     * @param integer $actId
     *
     * @return ListAgrFiles
     */
    public function setActId($actId)
    {
        $this->actId = $actId;

        return $this;
    }

    /**
     * Get actId
     *
     * @return integer
     */
    public function getActId()
    {
        return $this->actId;
    }

    /**
     * Set dsId
     *
     * @param integer $dsId
     *
     * @return ListAgrFiles
     */
    public function setDsId($dsId)
    {
        $this->dsId = $dsId;

        return $this;
    }

    /**
     * Get dsId
     *
     * @return integer
     */
    public function getDsId()
    {
        return $this->dsId;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     *
     * @return ListAgrFiles
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set desc
     *
     * @param string $desc
     *
     * @return ListAgrFiles
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     *
     * @return ListAgrFiles
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ListAgrFiles
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set uploaddate
     *
     * @param \DateTime $uploaddate
     *
     * @return ListAgrFiles
     */
    public function setUploaddate($uploaddate)
    {
        $this->uploaddate = $uploaddate;

        return $this;
    }

    /**
     * Get uploaddate
     *
     * @return \DateTime
     */
    public function getUploaddate()
    {
        return $this->uploaddate;
    }
}

