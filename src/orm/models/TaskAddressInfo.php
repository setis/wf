<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * GpAddr
 *
 * @ORM\Table(name="gp_addr", uniqueConstraints={@ORM\UniqueConstraint(columns={"task_id", "dom_id"})})
 * @ORM\Entity
 */
class TaskAddressInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Entrance description, usually is a number
     * 
     * @var integer
     *
     * @ORM\Column(name="pod", type="string", length=50, nullable=true)
     */
    private $entrance;

    /**
     * Intercom access code
     * 
     * @var string
     *
     * @ORM\Column(name="domofon", type="string", length=50, nullable=true)
     */
    private $intercom;
    
    /**
     * Room/office description
     * 
     * @var string
     *
     * @ORM\Column(name="room", type="string", length=50, nullable=true)
     */
    private $room;

    /**
     * @var string
     *
     * @ORM\Column(name="addr", type="text", nullable=true)
     */
    private $addressText;

    /**
     *
     * @var Task
     * 
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="addressInfo")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $task;
    
    /**
     *
     * @var ListAddr
     * 
     * @ORM\ManyToOne(targetEntity="ListAddr")
     * @ORM\JoinColumn(name="dom_id", referencedColumnName="id")
     */
    private $address;
    
    public function __construct(Task $task, ListAddr $address = null)
    {
        $this->task = $task;
        $this->address = $address;
    }

        /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function setTask(Task $task)
    {
        $this->task = $task;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress(ListAddr $address)
    {
        $this->address = $address;
        return $this;
    }
    
    public function getEntrance()
    {
        return $this->entrance;
    }

    public function getIntercom()
    {
        return $this->intercom;
    }

    public function getRoom()
    {
        return $this->room;
    }

    public function setEntrance($entrance)
    {
        $this->entrance = $entrance;
        return $this;
    }

    public function setIntercom($intercom)
    {
        $this->intercom = $intercom;
        return $this;
    }

    public function setRoom($room)
    {
        $this->room = $room;
        return $this;
    }

    public function getAddressText()
    {
        return $this->addressText;
    }

    public function setAddressText($addressText)
    {
        $this->addressText = $addressText;
        return $this;
    }
    
}
