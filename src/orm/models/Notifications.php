<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notifications
 *
 * @ORM\Table(name="notifications", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"task_id", "user_id", "transport", "blocker", "back_url"})
 * }, indexes={
 *     @ORM\Index(columns={"user_id"}), 
 *     @ORM\Index(columns={"task_id"}), 
 *     @ORM\Index(columns={"transport"})
 * })
 * @ORM\Entity
 */
class Notifications
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $taskId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="transport", type="string", length=10, precision=0, scale=0, nullable=false, unique=false)
     */
    private $transport;

    /**
     * @var boolean
     *
     * @ORM\Column(name="read", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $read;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="read_at", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $readAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="blocker", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $blocker;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=16777215, precision=0, scale=0, nullable=false, unique=false)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="back_url", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $backUrl;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set taskId
     *
     * @param integer $taskId
     *
     * @return Notifications
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Notifications
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set transport
     *
     * @param string $transport
     *
     * @return Notifications
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;

        return $this;
    }

    /**
     * Get transport
     *
     * @return string
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Set read
     *
     * @param boolean $read
     *
     * @return Notifications
     */
    public function setRead($read)
    {
        $this->read = $read;

        return $this;
    }

    /**
     * Get read
     *
     * @return boolean
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * Set createdAt
     *
     * @param integer $createdAt
     *
     * @return Notifications
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return integer
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set readAt
     *
     * @param integer $readAt
     *
     * @return Notifications
     */
    public function setReadAt($readAt)
    {
        $this->readAt = $readAt;

        return $this;
    }

    /**
     * Get readAt
     *
     * @return integer
     */
    public function getReadAt()
    {
        return $this->readAt;
    }

    /**
     * Set blocker
     *
     * @param boolean $blocker
     *
     * @return Notifications
     */
    public function setBlocker($blocker)
    {
        $this->blocker = $blocker;

        return $this;
    }

    /**
     * Get blocker
     *
     * @return boolean
     */
    public function getBlocker()
    {
        return $this->blocker;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Notifications
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set backUrl
     *
     * @param string $backUrl
     *
     * @return Notifications
     */
    public function setBackUrl($backUrl)
    {
        $this->backUrl = $backUrl;

        return $this;
    }

    /**
     * Get backUrl
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->backUrl;
    }
}

