<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListContrFiles
 *
 * @ORM\Table(name="list_contr_files")
 * @ORM\Entity
 */
class ListContrFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $contrId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="file_type", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $fileType;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $fileName;

    /**
     * @var string
     *
     * @ORM\Column(name="desc", type="text", length=16777215, precision=0, scale=0, nullable=true, unique=false)
     */
    private $desc;

    /**
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $mimeType;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="uploaddate", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $uploaddate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contrId
     *
     * @param integer $contrId
     *
     * @return ListContrFiles
     */
    public function setContrId($contrId)
    {
        $this->contrId = $contrId;

        return $this;
    }

    /**
     * Get contrId
     *
     * @return integer
     */
    public function getContrId()
    {
        return $this->contrId;
    }

    /**
     * Set fileType
     *
     * @param boolean $fileType
     *
     * @return ListContrFiles
     */
    public function setFileType($fileType)
    {
        $this->fileType = $fileType;

        return $this;
    }

    /**
     * Get fileType
     *
     * @return boolean
     */
    public function getFileType()
    {
        return $this->fileType;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     *
     * @return ListContrFiles
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set desc
     *
     * @param string $desc
     *
     * @return ListContrFiles
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     *
     * @return ListContrFiles
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ListContrFiles
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set uploaddate
     *
     * @param \DateTime $uploaddate
     *
     * @return ListContrFiles
     */
    public function setUploaddate($uploaddate)
    {
        $this->uploaddate = $uploaddate;

        return $this;
    }

    /**
     * Get uploaddate
     *
     * @return \DateTime
     */
    public function getUploaddate()
    {
        return $this->uploaddate;
    }
}

