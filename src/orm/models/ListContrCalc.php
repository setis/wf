<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Task\Model\TicketCalcInterface;

/**
 * ListContrCalc
 *
 * @ORM\Table(name="list_contr_calc", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"task_id", "work_id"})
 * })
 * @ORM\Entity
 */
class ListContrCalc implements TicketCalcInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="smeta_val", type="decimal", precision=10, scale=2, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $smetaVal;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2,  nullable=false)
     * @Serializer\Groups({"default"})
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="work_title", type="string", length=255, nullable=false)
     * @Serializer\Groups({"default"})
     */
    private $workTitle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="calc_date", type="date", nullable=false)
     * @Serializer\Groups({"default"})
     */
    private $calcDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $updateDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="quant", type="integer", nullable=false)
     * @Serializer\Groups({"default"})
     */
    private $quant;

    /**
     * @var \models\Task
     *
     * @ORM\ManyToOne(targetEntity="models\Task", inversedBy="listContrCalc")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $task;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @var \models\ListAgrPrice
     *
     * @ORM\ManyToOne(targetEntity="models\ListAgrPrice")
     * @ORM\JoinColumn(name="work_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    private $work;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set smetaVal
     *
     * @param string $smetaVal
     *
     * @return ListContrCalc
     */
    public function setSmetaVal($smetaVal)
    {
        $this->smetaVal = $smetaVal;

        return $this;
    }

    /**
     * Get smetaVal
     *
     * @return string
     */
    public function getSmetaVal()
    {
        return $this->smetaVal;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return ListContrCalc
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set workTitle
     *
     * @param string $workTitle
     *
     * @return ListContrCalc
     */
    public function setWorkTitle($workTitle)
    {
        $this->workTitle = $workTitle;

        return $this;
    }

    /**
     * Get workTitle
     *
     * @return string
     */
    public function getWorkTitle()
    {
        return $this->workTitle;
    }

    /**
     * Set calcDate
     *
     * @param \DateTime $calcDate
     *
     * @return ListContrCalc
     */
    public function setCalcDate($calcDate)
    {
        $this->calcDate = $calcDate;

        return $this;
    }

    /**
     * Get calcDate
     *
     * @return \DateTime
     */
    public function getCalcDate()
    {
        return $this->calcDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return ListContrCalc
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set quant
     *
     * @param integer $quant
     *
     * @return ListContrCalc
     */
    public function setQuant($quant)
    {
        $this->quant = $quant;

        return $this;
    }

    /**
     * Get quant
     *
     * @return integer
     */
    public function getQuant()
    {
        return $this->quant;
    }

    /**
     * @param Task $task
     * @return ListContrCalc
     */
    public function setTask($task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param User $user
     * @return ListContrCalc
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param ListAgrPrice $work
     * @return ListContrCalc
     */
    public function setWork($work)
    {
        $this->work = $work;
        return $this;
    }

    /**
     * @return ListAgrPrice
     */
    public function getWork()
    {
        return $this->work;
    }
}

