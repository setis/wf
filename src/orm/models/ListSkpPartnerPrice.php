<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ListSkpPPrice
 *
 * @ORM\Table(name="list_skp_p_price")
 * @ORM\Entity
 */
class ListSkpPartnerPrice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ptype", type="boolean", nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $ptype;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="ei_min", type="decimal", precision=10, scale=2, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $eiMin;

    /**
     * @var string
     *
     * @ORM\Column(name="contrcode", type="string", length=50, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $contrcode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cr_date", type="datetime", nullable=true)
     * @Serializer\Groups({"extended"})
     */
    private $crDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="code", type="integer", nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="intcode", type="string", length=50, nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $intcode;

    /**
     * @var \models\ListContr
     *
     * @ORM\ManyToOne(targetEntity="models\ListContr")
     * @ORM\JoinColumn(name="contr_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Groups({"default"})
     */
    private $partner;

    /**
     * @var \models\ListSkpPartnerCatTitle
     *
     * @ORM\ManyToOne(targetEntity="models\ListSkpPartnerCatTitle")
     * @ORM\JoinColumn(name="cat_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Groups({"default"})
     */
    private $category;

    /**
     * @var \models\ListSkpPartnerWorkTypes
     *
     * @ORM\ManyToOne(targetEntity="models\ListSkpPartnerWorkTypes")
     * @ORM\JoinColumn(name="wt_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Groups({"default"})
     */
    private $work;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="cr_uid", referencedColumnName="id", onDelete="SET NULL")
     * @Serializer\Groups({"extended"})
     */
    private $author;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get ptype
     *
     * @return boolean
     */
    public function getPtype()
    {
        return $this->ptype;
    }

    /**
     * Set ptype
     *
     * @param boolean $ptype
     *
     * @return ListSkpPartnerPrice
     */
    public function setPtype($ptype)
    {
        $this->ptype = $ptype;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return ListSkpPartnerPrice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get eiMin
     *
     * @return float
     */
    public function getEiMin()
    {
        return $this->eiMin;
    }

    /**
     * Set eiMin
     *
     * @param float $eiMin
     *
     * @return ListSkpPartnerPrice
     */
    public function setEiMin($eiMin)
    {
        $this->eiMin = $eiMin;

        return $this;
    }

    /**
     * Get contrcode
     *
     * @return string
     */
    public function getContrcode()
    {
        return $this->contrcode;
    }

    /**
     * Set contrcode
     *
     * @param string $contrcode
     *
     * @return ListSkpPartnerPrice
     */
    public function setContrcode($contrcode)
    {
        $this->contrcode = $contrcode;

        return $this;
    }

    /**
     * Get crDate
     *
     * @return \DateTime
     */
    public function getCrDate()
    {
        return $this->crDate;
    }

    /**
     * Set crDate
     *
     * @param \DateTime $crDate
     *
     * @return ListSkpPartnerPrice
     */
    public function setCrDate($crDate)
    {
        $this->crDate = $crDate;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set code
     *
     * @param integer $code
     *
     * @return ListSkpPartnerPrice
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get intcode
     *
     * @return string
     */
    public function getIntcode()
    {
        return $this->intcode;
    }

    /**
     * Set intcode
     *
     * @param string $intcode
     *
     * @return ListSkpPartnerPrice
     */
    public function setIntcode($intcode)
    {
        $this->intcode = $intcode;

        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     * @return ListSkpPartnerPrice
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return ListSc
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param ListSc $partner
     * @return ListSkpPartnerPrice
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * @return ListSkpPartnerCatTitle
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param ListSkpPartnerCatTitle $category
     * @return ListSkpPartnerPrice
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return ListSkpPartnerWorkTypes
     */
    public function getWork()
    {
        return $this->work;
    }

    /**
     * @param ListSkpPartnerWorkTypes $work
     * @return ListSkpPartnerPrice
     */
    public function setWork($work)
    {
        $this->work = $work;
        return $this;
    }


}

