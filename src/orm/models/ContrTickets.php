<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContrTickets
 *
 * @ORM\Table(name="contr_tickets", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"contr_id", "contr_ticket_id"})
 * })
 * @ORM\Entity
 */
class ContrTickets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taskId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contr_id", type="integer")
     */
    private $contrId;

    /**
     * @var string
     *
     * @ORM\Column(name="contr_ticket_id", type="string", length=50)
     */
    private $contrTicketId;

    /**
     * @var string
     *
     * @ORM\Column(name="contr_addr", type="string", length=250, nullable=true,)
     */
    private $contrAddr;

    /**
     * @var integer
     *
     * @ORM\Column(name="dom_id", type="integer", nullable=true,)
     */
    private $domId;


    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set contrId
     *
     * @param integer $contrId
     *
     * @return ContrTickets
     */
    public function setContrId($contrId)
    {
        $this->contrId = $contrId;

        return $this;
    }

    /**
     * Get contrId
     *
     * @return integer
     */
    public function getContrId()
    {
        return $this->contrId;
    }

    /**
     * Set contrTicketId
     *
     * @param string $contrTicketId
     *
     * @return ContrTickets
     */
    public function setContrTicketId($contrTicketId)
    {
        $this->contrTicketId = $contrTicketId;

        return $this;
    }

    /**
     * Get contrTicketId
     *
     * @return string
     */
    public function getContrTicketId()
    {
        return $this->contrTicketId;
    }

    /**
     * Set contrAddr
     *
     * @param string $contrAddr
     *
     * @return ContrTickets
     */
    public function setContrAddr($contrAddr)
    {
        $this->contrAddr = $contrAddr;

        return $this;
    }

    /**
     * Get contrAddr
     *
     * @return string
     */
    public function getContrAddr()
    {
        return $this->contrAddr;
    }

    /**
     * Set domId
     *
     * @param integer $domId
     *
     * @return ContrTickets
     */
    public function setDomId($domId)
    {
        $this->domId = $domId;

        return $this;
    }

    /**
     * Get domId
     *
     * @return integer
     */
    public function getDomId()
    {
        return $this->domId;
    }
}

