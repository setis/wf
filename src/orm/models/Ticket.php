<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Task\Model\TechnicalTicketInterface;

/**
 * Ticket
 *
 * @ORM\Table(name="tickets", indexes={
 *     @ORM\Index(columns={"task_type"}),
 *     @ORM\Index(columns={"clnt_type"}),
 *     @ORM\Index(columns={"tdataactid", "task_id"}),
 * })
 * @ORM\Entity
 */
class Ticket implements TechnicalTicketInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taskId;

    /**
     * @var string
     *
     * @ORM\Column(name="pod", type="string", length=20, nullable=true, unique=false)
     */
    private $pod;

    /**
     * @var string
     *
     * @ORM\Column(name="etazh", type="string", length=20, nullable=true, unique=false)
     */
    private $etazh;

    /**
     * @var string
     *
     * @ORM\Column(name="kv", type="string", length=20, nullable=true, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $kv;

    /**
     * @var string
     *
     * @ORM\Column(name="domofon", type="string", length=20, nullable=true, unique=false)
     */
    private $domofon;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=true, unique=false)
     */
    private $price;

    /**
     * @var boolean
     *
     * @ORM\Column(name="clnt_type", type="smallint", nullable=false, unique=false)
     */
    private $clntType;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_org_name", type="string", length=250, nullable=true, unique=false)
     */
    private $clntOrgName;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_fio", type="string", length=250, nullable=true, unique=false)
     */
    private $clntFio;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_passp_sn", type="string", length=50, nullable=true, unique=false)
     */
    private $clntPasspSn;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_passport_vid", type="string", length=250, nullable=true, unique=false)
     */
    private $clntPassportVid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="clnt_passport_viddate", type="date", nullable=true, unique=false)
     */
    private $clntPassportViddate;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_tel1", type="string", length=11, nullable=true, unique=false)
     */
    private $clntTel1;

    /**
     * @var string
     *
     * @ORM\Column(name="clnt_tel2", type="string", length=11, nullable=true, unique=false)
     */
    private $clntTel2;

    /**
     * @var integer
     *
     * @ORM\Column(name="reject_id", type="integer", nullable=true, unique=false)
     */
    private $rejectId;

    /**
     * @var string
     *
     * @ORM\Column(name="addinfo", type="text", length=16777215, nullable=true, unique=false)
     */
    private $addinfo;

    /**
     * @var string
     *
     * @ORM\Column(name="swip", type="string", length=15, nullable=true, unique=false)
     */
    private $swip;

    /**
     * @var string
     *
     * @ORM\Column(name="swport", type="string", length=255, nullable=true, unique=false)
     */
    private $swport;

    /**
     * @var string
     *
     * @ORM\Column(name="swplace", type="string", length=255, nullable=true, unique=false)
     */
    private $swplace;

    /**
     * @var string
     *
     * @ORM\Column(name="clntopagr", type="string", length=255, nullable=true, unique=false)
     */
    private $clntopagr;

    /**
     * @var string
     *
     * @ORM\Column(name="clnttnum", type="string", length=255, nullable=true, unique=false)
     */
    private $clnttnum;

    /**
     * @var boolean
     *
     * @ORM\Column(name="checked", type="boolean", nullable=false, unique=false)
     */
    private $checked = false;

    /**
     * @var string
     *
     * @ORM\Column(name="cablequant", type="string", length=15, nullable=true, unique=false)
     */
    private $cablequant;

    /**
     * @var string
     *
     * @ORM\Column(name="orient_price", type="decimal", precision=8, scale=2, nullable=true, unique=false)
     */
    private $orientPrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="task_from", type="integer", nullable=true, unique=false)
     */
    private $taskFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="slot_begin", type="datetime", nullable=true, unique=false)
     */
    private $slotBegin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="slot_end", type="datetime", nullable=true, unique=false)
     */
    private $slotEnd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="findatetech", type="datetime", nullable=true, unique=false)
     */
    private $findatetech;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="findatecontr", type="datetime", nullable=true, unique=false)
     */
    private $findatecontr;

    /**
     * @var integer
     *
     * @ORM\Column(name="tmcactid", type="integer", nullable=true, unique=false)
     */
    private $tmcactid;

    /**
     * @var integer
     *
     * @ORM\Column(name="tdataactid", type="integer", nullable=true, unique=false)
     */
    private $tdataactid;

    /**
     * @var string
     *
     * @ORM\Column(name="tickaddnumber", type="string", length=50, nullable=true, unique=false)
     */
    private $tickaddnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="tdocnum", type="string", length=50, nullable=true, unique=false)
     */
    private $tdocnum;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inmoney", type="boolean", nullable=true, unique=false)
     */
    private $inmoney;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inmoneydate", type="datetime", nullable=true, unique=false)
     */
    private $inmoneydate;

    /**
     * @var float
     *
     * @ORM\Column(name="ammount", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $ammount;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=250, nullable=true, unique=false)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=250, nullable=true, unique=false)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="conndate", type="datetime", nullable=true, unique=false)
     */
    private $conndate;

    /**
     * @var integer
     *
     * @ORM\Column(name="exttype", type="integer", nullable=true, unique=false)
     */
    private $exttype;

    /**
     * @var boolean
     *
     * @ORM\Column(name="istest", type="boolean", nullable=true, unique=false)
     */
    private $istest;

    /**
     * @var integer
     *
     * @ORM\Column(name="tttype_id", type="integer", nullable=true, unique=false)
     */
    private $tttypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ttsubtype_id", type="integer", nullable=true, unique=false)
     */
    private $ttsubtypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="skp_wtype", type="string", length=50, nullable=true, unique=false)
     */
    private $skpWtype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="skt_act_date", type="date", nullable=true, unique=false)
     */
    private $sktActDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="iscard", type="boolean", nullable=true, unique=false)
     */
    private $iscard;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="iscard_date", type="datetime", nullable=true, unique=false)
     */
    private $iscardDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="mgts_tc", type="integer", nullable=true, unique=false)
     */
    private $mgtsTc;

    /**
     * @var string
     *
     * @ORM\Column(name="mgts_ats", type="string", length=50, nullable=true, unique=false)
     */
    private $mgtsAts;

    /**
     * @var string
     *
     * @ORM\Column(name="mgts_orsh", type="string", length=50, nullable=true, unique=false)
     */
    private $mgtsOrsh;

    /**
     * @var string
     *
     * @ORM\Column(name="mgts_lines", type="string", length=50, nullable=true, unique=false)
     */
    private $mgtsLines;

    /**
     * @var string
     *
     * @ORM\Column(name="mgts_phones", type="string", length=50, nullable=true, unique=false)
     */
    private $mgtsPhones;

    /**
     * @var integer
     *
     * @ORM\Column(name="mgts_sertype", type="integer", nullable=true, unique=false)
     */
    private $mgtsSertype;

    /**
     * @var string
     *
     * @ORM\Column(name="mgts_agreement_number", type="string", length=255, nullable=true)
     */
    private $mgtsAgreementNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disallowgfx", type="boolean", nullable=true, unique=false)
     */
    private $disallowgfx;

    /**
     * @var boolean
     *
     * @ORM\Column(name="asrzerror", type="boolean", nullable=true, unique=false)
     */
    private $asrzerror;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wfmplanned", type="boolean", nullable=true, unique=false)
     */
    private $wfmplanned;

    /**
     * @var boolean
     *
     * @ORM\Column(name="noagrmgts", type="boolean", nullable=true, unique=false)
     */
    private $noagrmgts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="asrzok", type="boolean", nullable=true, unique=false)
     */
    private $asrzok;

    /**
     * @var boolean
     *
     * @ORM\Column(name="setted", type="boolean", nullable=true, unique=false)
     */
    private $setted;

    /**
     * @var string
     *
     * @ORM\Column(name="lineowner", type="string", length=250, nullable=true, unique=false)
     */
    private $lineowner;

    /**
     * @var string
     *
     * @ORM\Column(name="landlinenum", type="string", length=250, nullable=true, unique=false)
     */
    private $landlinenum;

    /**
     * @var boolean
     *
     * @ORM\Column(name="acc_contr", type="boolean", nullable=true, unique=false)
     */
    private $accContr;

    /**
     * @var boolean
     *
     * @ORM\Column(name="rt_mo", type="boolean", nullable=true, unique=false)
     */
    private $rtMo;

    /**
     * @var integer
     *
     * @ORM\Column(name="doc_ticket", type="integer", nullable=true, unique=false)
     */
    private $docTicket;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="doc_date", type="date", nullable=true, unique=false)
     */
    private $docDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="tmc_ticket", type="integer", nullable=true, unique=false)
     */
    private $tmcTicket;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tmc_date", type="date", nullable=true, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $tmcDate;

    /**
     * @var \models\TaskType
     *
     * @ORM\ManyToOne(targetEntity="models\TaskType", inversedBy="tickets")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_type", referencedColumnName="id", nullable=true)
     * })
     * @Serializer\Groups({"extended"})
     */
    private $taskType;

    /**
     * @var \models\Task
     *
     * @ORM\OneToOne(targetEntity="models\Task", inversedBy="ticket", cascade={"persist","remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id", unique=true, nullable=false, onDelete="CASCADE")
     * })
     * @Serializer\Groups({"default"})
     */
    private $task;

    /**
     * @var \models\ListAddr
     *
     * @ORM\ManyToOne(targetEntity="models\ListAddr")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dom_id", referencedColumnName="id", nullable=true)
     * })
     * @Serializer\Groups({"default"})
     */
    private $dom;

    /**
     *
     * @var ListContr
     *
     * @ORM\ManyToOne(targetEntity="models\ListContr")
     * @ORM\JoinColumn(name="cnt_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @Serializer\Groups({"default"})
     */
    private $partner;


    /**
     *
     * @var ListSc
     *
     * @ORM\ManyToOne(targetEntity="models\ListSc")
     * @ORM\JoinColumn(name="sc_id", referencedColumnName="id", onDelete="NO ACTION")
     * @Serializer\Groups({"default", "Ticket.ServiceCenter"})
     */
    private $serviceCenter;

    /**
     * @var Agreement
     *
     * @ORM\ManyToOne(targetEntity="models\Agreement")
     * @ORM\JoinColumn(name="agr_id", referencedColumnName="id")
     */
    private $agreement;


    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set taskType
     *
     * @param TaskType $taskType
     *
     * @return Ticket
     */
    public function setTaskType($taskType)
    {
        $this->taskType = $taskType;

        return $this;
    }

    /**
     * Get taskType
     *
     * @return TaskType
     */
    public function getTaskType()
    {
        return $this->taskType;
    }

    /**
     * Set pod
     *
     * @param string $pod
     *
     * @return Ticket
     */
    public function setPod($pod)
    {
        $this->pod = $pod;

        return $this;
    }

    /**
     * Get pod
     *
     * @return string
     */
    public function getPod()
    {
        return $this->pod;
    }

    /**
     * Set etazh
     *
     * @param string $etazh
     *
     * @return Ticket
     */
    public function setEtazh($etazh)
    {
        $this->etazh = $etazh;

        return $this;
    }

    /**
     * Get etazh
     *
     * @return string
     */
    public function getEtazh()
    {
        return $this->etazh;
    }

    /**
     * Set kv
     *
     * @param string $kv
     *
     * @return Ticket
     */
    public function setKv($kv)
    {
        $this->kv = $kv;

        return $this;
    }

    /**
     * Get kv
     *
     * @return string
     */
    public function getKv()
    {
        return $this->kv;
    }

    /**
     * Set domofon
     *
     * @param string $domofon
     *
     * @return Ticket
     */
    public function setDomofon($domofon)
    {
        $this->domofon = $domofon;

        return $this;
    }

    /**
     * Get domofon
     *
     * @return string
     */
    public function getDomofon()
    {
        return $this->domofon;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Ticket
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set clntType
     *
     * @param boolean $clntType
     *
     * @return Ticket
     */
    public function setClntType($clntType)
    {
        $this->clntType = $clntType;

        return $this;
    }

    /**
     * Get clntType
     *
     * @return boolean
     */
    public function getClntType()
    {
        return $this->clntType;
    }

    /**
     * Set clntOrgName
     *
     * @param string $clntOrgName
     *
     * @return Ticket
     */
    public function setClntOrgName($clntOrgName)
    {
        $this->clntOrgName = $clntOrgName;

        return $this;
    }

    /**
     * Get clntOrgName
     *
     * @return string
     */
    public function getClntOrgName()
    {
        return $this->clntOrgName;
    }

    /**
     * Set clntFio
     *
     * @param string $clntFio
     *
     * @return Ticket
     */
    public function setClntFio($clntFio)
    {
        $this->clntFio = $clntFio;

        return $this;
    }

    /**
     * Get clntFio
     *
     * @return string
     */
    public function getClntFio()
    {
        return $this->clntFio;
    }

    /**
     * Set clntPasspSn
     *
     * @param string $clntPasspSn
     *
     * @return Ticket
     */
    public function setClntPasspSn($clntPasspSn)
    {
        $this->clntPasspSn = $clntPasspSn;

        return $this;
    }

    /**
     * Get clntPasspSn
     *
     * @return string
     */
    public function getClntPasspSn()
    {
        return $this->clntPasspSn;
    }

    /**
     * Set clntPassportVid
     *
     * @param string $clntPassportVid
     *
     * @return Ticket
     */
    public function setClntPassportVid($clntPassportVid)
    {
        $this->clntPassportVid = $clntPassportVid;

        return $this;
    }

    /**
     * Get clntPassportVid
     *
     * @return string
     */
    public function getClntPassportVid()
    {
        return $this->clntPassportVid;
    }

    /**
     * Set clntPassportViddate
     *
     * @param \DateTime $clntPassportViddate
     *
     * @return Ticket
     */
    public function setClntPassportViddate($clntPassportViddate)
    {
        $this->clntPassportViddate = $clntPassportViddate;

        return $this;
    }

    /**
     * Get clntPassportViddate
     *
     * @return \DateTime
     */
    public function getClntPassportViddate()
    {
        return $this->clntPassportViddate;
    }

    /**
     * Set clntTel1
     *
     * @param string $clntTel1
     *
     * @return Ticket
     */
    public function setClntTel1($clntTel1)
    {
        $this->clntTel1 = $clntTel1;

        return $this;
    }

    /**
     * Get clntTel1
     *
     * @return string
     */
    public function getClntTel1()
    {
        return $this->clntTel1;
    }

    /**
     * Set clntTel2
     *
     * @param string $clntTel2
     *
     * @return Ticket
     */
    public function setClntTel2($clntTel2)
    {
        $this->clntTel2 = $clntTel2;

        return $this;
    }

    public function getClientPhones()
    {
        return array_filter([$this->clntTel1, $this->clntTel2]);
    }

    /**
     * Get clntTel2
     *
     * @return string
     */
    public function getClntTel2()
    {
        return $this->clntTel2;
    }

    /**
     * Set rejectId
     *
     * @param integer $rejectId
     *
     * @return Ticket
     */
    public function setRejectId($rejectId)
    {
        $this->rejectId = $rejectId;

        return $this;
    }

    /**
     * Get rejectId
     *
     * @return integer
     */
    public function getRejectId()
    {
        return $this->rejectId;
    }

    /**
     * Set addinfo
     *
     * @param string $addinfo
     *
     * @return Ticket
     */
    public function setAddinfo($addinfo)
    {
        $this->addinfo = $addinfo;

        return $this;
    }

    /**
     * Get addinfo
     *
     * @return string
     */
    public function getAddinfo()
    {
        return $this->addinfo;
    }

    /**
     * Set swip
     *
     * @param string $swip
     *
     * @return Ticket
     */
    public function setSwip($swip)
    {
        $this->swip = $swip;

        return $this;
    }

    /**
     * Get swip
     *
     * @return string
     */
    public function getSwip()
    {
        return $this->swip;
    }

    /**
     * Set swport
     *
     * @param string $swport
     *
     * @return Ticket
     */
    public function setSwport($swport)
    {
        $this->swport = $swport;

        return $this;
    }

    /**
     * Get swport
     *
     * @return string
     */
    public function getSwport()
    {
        return $this->swport;
    }

    /**
     * Set swplace
     *
     * @param string $swplace
     *
     * @return Ticket
     */
    public function setSwplace($swplace)
    {
        $this->swplace = $swplace;

        return $this;
    }

    /**
     * Get swplace
     *
     * @return string
     */
    public function getSwplace()
    {
        return $this->swplace;
    }

    /**
     * Set clntopagr
     *
     * @param string $clntopagr
     *
     * @return Ticket
     */
    public function setClntopagr($clntopagr)
    {
        $this->clntopagr = $clntopagr;

        return $this;
    }

    /**
     * Get clntopagr
     *
     * @return string
     */
    public function getClntopagr()
    {
        return $this->clntopagr;
    }

    /**
     * Set clnttnum
     *
     * @param string $clnttnum
     *
     * @return Ticket
     */
    public function setClnttnum($clnttnum)
    {
        $this->clnttnum = $clnttnum;

        return $this;
    }

    /**
     * Get clnttnum
     *
     * @return string
     */
    public function getClnttnum()
    {
        return $this->clnttnum;
    }

    /**
     * Set checked
     *
     * @param boolean $checked
     *
     * @return Ticket
     */
    public function setChecked($checked)
    {
        $this->checked = $checked;

        return $this;
    }

    /**
     * Get checked
     *
     * @return boolean
     */
    public function getChecked()
    {
        return $this->checked;
    }

    /**
     * Set cablequant
     *
     * @param string $cablequant
     *
     * @return Ticket
     */
    public function setCablequant($cablequant)
    {
        $this->cablequant = $cablequant;

        return $this;
    }

    /**
     * Get cablequant
     *
     * @return string
     */
    public function getCablequant()
    {
        return $this->cablequant;
    }

    /**
     * Set orientPrice
     *
     * @param string $orientPrice
     *
     * @return Ticket
     */
    public function setOrientPrice($orientPrice)
    {
        $this->orientPrice = $orientPrice;

        return $this;
    }

    /**
     * Get orientPrice
     *
     * @return string
     */
    public function getOrientPrice()
    {
        return $this->orientPrice;
    }

    /**
     * Set taskFrom
     *
     * @param integer $taskFrom
     *
     * @return Ticket
     */
    public function setTaskFrom($taskFrom)
    {
        $this->taskFrom = $taskFrom;

        return $this;
    }

    /**
     * Get taskFrom
     *
     * @return integer
     */
    public function getTaskFrom()
    {
        return $this->taskFrom;
    }

    /**
     * Set slotBegin
     *
     * @param \DateTime $slotBegin
     *
     * @return Ticket
     */
    public function setSlotBegin($slotBegin)
    {
        $this->slotBegin = $slotBegin;

        return $this;
    }

    /**
     * Get slotBegin
     *
     * @return \DateTime
     */
    public function getSlotBegin()
    {
        return $this->slotBegin;
    }

    /**
     * Set slotEnd
     *
     * @param \DateTime $slotEnd
     *
     * @return Ticket
     */
    public function setSlotEnd($slotEnd)
    {
        $this->slotEnd = $slotEnd;

        return $this;
    }

    /**
     * Get slotEnd
     *
     * @return \DateTime
     */
    public function getSlotEnd()
    {
        return $this->slotEnd;
    }

    /**
     * Set findatetech
     *
     * @param \DateTime $findatetech
     *
     * @return Ticket
     */
    public function setFindatetech($findatetech)
    {
        $this->findatetech = $findatetech;

        return $this;
    }

    /**
     * Get findatetech
     *
     * @return \DateTime
     */
    public function getFindatetech()
    {
        return $this->findatetech;
    }

    /**
     * Set findatecontr
     *
     * @param \DateTime $findatecontr
     *
     * @return Ticket
     */
    public function setFindatecontr($findatecontr)
    {
        $this->findatecontr = $findatecontr;

        return $this;
    }

    /**
     * Get findatecontr
     *
     * @return \DateTime
     */
    public function getFindatecontr()
    {
        return $this->findatecontr;
    }

    /**
     * Set tmcactid
     *
     * @param integer $tmcactid
     *
     * @return Ticket
     */
    public function setTmcactid($tmcactid)
    {
        $this->tmcactid = $tmcactid;

        return $this;
    }

    /**
     * Get tmcactid
     *
     * @return integer
     */
    public function getTmcactid()
    {
        return $this->tmcactid;
    }

    /**
     * Set tdataactid
     *
     * @param integer $tdataactid
     *
     * @return Ticket
     */
    public function setTdataactid($tdataactid)
    {
        $this->tdataactid = $tdataactid;

        return $this;
    }

    /**
     * Get tdataactid
     *
     * @return integer
     */
    public function getTdataactid()
    {
        return $this->tdataactid;
    }

    /**
     * Set tickaddnumber
     *
     * @param string $tickaddnumber
     *
     * @return Ticket
     */
    public function setTickaddnumber($tickaddnumber)
    {
        $this->tickaddnumber = $tickaddnumber;

        return $this;
    }

    /**
     * Get tickaddnumber
     *
     * @return string
     */
    public function getTickaddnumber()
    {
        return $this->tickaddnumber;
    }

    /**
     * Set tdocnum
     *
     * @param string $tdocnum
     *
     * @return Ticket
     */
    public function setTdocnum($tdocnum)
    {
        $this->tdocnum = $tdocnum;

        return $this;
    }

    /**
     * Get tdocnum
     *
     * @return string
     */
    public function getTdocnum()
    {
        return $this->tdocnum;
    }

    /**
     * Set inmoney
     *
     * @param boolean $inmoney
     *
     * @return Ticket
     */
    public function setInmoney($inmoney)
    {
        $this->inmoney = $inmoney;

        return $this;
    }

    /**
     * Get inmoney
     *
     * @return boolean
     */
    public function getInmoney()
    {
        return $this->inmoney;
    }

    /**
     * Set inmoneydate
     *
     * @param \DateTime $inmoneydate
     *
     * @return Ticket
     */
    public function setInmoneydate($inmoneydate)
    {
        $this->inmoneydate = $inmoneydate;

        return $this;
    }

    /**
     * Get inmoneydate
     *
     * @return \DateTime
     */
    public function getInmoneydate()
    {
        return $this->inmoneydate;
    }

    /**
     * Set ammount
     *
     * @param float $ammount
     *
     * @return Ticket
     */
    public function setAmmount($ammount)
    {
        $this->ammount = $ammount;

        return $this;
    }

    /**
     * Get ammount
     *
     * @return float
     */
    public function getAmmount()
    {
        return $this->ammount;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return Ticket
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Ticket
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set conndate
     *
     * @param \DateTime $conndate
     *
     * @return Ticket
     */
    public function setConndate($conndate)
    {
        $this->conndate = $conndate;

        return $this;
    }

    /**
     * Get conndate
     *
     * @return \DateTime
     */
    public function getConndate()
    {
        return $this->conndate;
    }

    /**
     * Set exttype
     *
     * @param integer $exttype
     *
     * @return Ticket
     */
    public function setExttype($exttype)
    {
        $this->exttype = $exttype;

        return $this;
    }

    /**
     * Get exttype
     *
     * @return integer
     */
    public function getExttype()
    {
        return $this->exttype;
    }

    /**
     * Set istest
     *
     * @param boolean $istest
     *
     * @return Ticket
     */
    public function setIstest($istest)
    {
        $this->istest = $istest;

        return $this;
    }

    /**
     * Get istest
     *
     * @return boolean
     */
    public function getIstest()
    {
        return $this->istest;
    }

    /**
     * Set tttypeId
     *
     * @param integer $tttypeId
     *
     * @return Ticket
     */
    public function setTttypeId($tttypeId)
    {
        $this->tttypeId = $tttypeId;

        return $this;
    }

    /**
     * Get tttypeId
     *
     * @return integer
     */
    public function getTttypeId()
    {
        return $this->tttypeId;
    }

    /**
     * Set ttsubtypeId
     *
     * @param integer $ttsubtypeId
     *
     * @return Ticket
     */
    public function setTtsubtypeId($ttsubtypeId)
    {
        $this->ttsubtypeId = $ttsubtypeId;

        return $this;
    }

    /**
     * Get ttsubtypeId
     *
     * @return integer
     */
    public function getTtsubtypeId()
    {
        return $this->ttsubtypeId;
    }

    /**
     * Set skpWtype
     *
     * @param string $skpWtype
     *
     * @return Ticket
     */
    public function setSkpWtype($skpWtype)
    {
        $this->skpWtype = $skpWtype;

        return $this;
    }

    /**
     * Get skpWtype
     *
     * @return string
     */
    public function getSkpWtype()
    {
        return $this->skpWtype;
    }

    /**
     * Set sktActDate
     *
     * @param \DateTime $sktActDate
     *
     * @return Ticket
     */
    public function setSktActDate($sktActDate)
    {
        $this->sktActDate = $sktActDate;

        return $this;
    }

    /**
     * Get sktActDate
     *
     * @return \DateTime
     */
    public function getSktActDate()
    {
        return $this->sktActDate;
    }

    /**
     * Set iscard
     *
     * @param boolean $iscard
     *
     * @return Ticket
     */
    public function setIscard($iscard)
    {
        $this->iscard = $iscard;

        return $this;
    }

    /**
     * Get iscard
     *
     * @return boolean
     */
    public function getIscard()
    {
        return $this->iscard;
    }

    /**
     * Set iscardDate
     *
     * @param \DateTime $iscardDate
     *
     * @return Ticket
     */
    public function setIscardDate($iscardDate)
    {
        $this->iscardDate = $iscardDate;

        return $this;
    }

    /**
     * Get iscardDate
     *
     * @return \DateTime
     */
    public function getIscardDate()
    {
        return $this->iscardDate;
    }

    /**
     * Set mgtsTc
     *
     * @param integer $mgtsTc
     *
     * @return Ticket
     */
    public function setMgtsTc($mgtsTc)
    {
        $this->mgtsTc = $mgtsTc;

        return $this;
    }

    /**
     * Get mgtsTc
     *
     * @return integer
     */
    public function getMgtsTc()
    {
        return $this->mgtsTc;
    }

    /**
     * Set mgtsAts
     *
     * @param string $mgtsAts
     *
     * @return Ticket
     */
    public function setMgtsAts($mgtsAts)
    {
        $this->mgtsAts = $mgtsAts;

        return $this;
    }

    /**
     * Get mgtsAts
     *
     * @return string
     */
    public function getMgtsAts()
    {
        return $this->mgtsAts;
    }

    /**
     * Set mgtsOrsh
     *
     * @param string $mgtsOrsh
     *
     * @return Ticket
     */
    public function setMgtsOrsh($mgtsOrsh)
    {
        $this->mgtsOrsh = $mgtsOrsh;

        return $this;
    }

    /**
     * Get mgtsOrsh
     *
     * @return string
     */
    public function getMgtsOrsh()
    {
        return $this->mgtsOrsh;
    }

    /**
     * Set mgtsLines
     *
     * @param string $mgtsLines
     *
     * @return Ticket
     */
    public function setMgtsLines($mgtsLines)
    {
        $this->mgtsLines = $mgtsLines;

        return $this;
    }

    /**
     * Get mgtsLines
     *
     * @return string
     */
    public function getMgtsLines()
    {
        return $this->mgtsLines;
    }

    /**
     * Set mgtsPhones
     *
     * @param string $mgtsPhones
     *
     * @return Ticket
     */
    public function setMgtsPhones($mgtsPhones)
    {
        $this->mgtsPhones = $mgtsPhones;

        return $this;
    }

    /**
     * Get mgtsPhones
     *
     * @return string
     */
    public function getMgtsPhones()
    {
        return $this->mgtsPhones;
    }

    /**
     * Set mgtsSertype
     *
     * @param integer $mgtsSertype
     *
     * @return Ticket
     */
    public function setMgtsSertype($mgtsSertype)
    {
        $this->mgtsSertype = $mgtsSertype;

        return $this;
    }

    /**
     * Get mgtsSertype
     *
     * @return integer
     */
    public function getMgtsSertype()
    {
        return $this->mgtsSertype;
    }

    /**
     * @return string
     */
    public function getMgtsAgreementNumber()
    {
        return $this->mgtsAgreementNumber;
    }

    /**
     * @param string $mgtsAgreementNumber
     * @return Ticket
     */
    public function setMgtsAgreementNumber($mgtsAgreementNumber): Ticket
    {
        $this->mgtsAgreementNumber = $mgtsAgreementNumber;
        return $this;
    }




    /**
     * Set disallowgfx
     *
     * @param boolean $disallowgfx
     *
     * @return Ticket
     */
    public function setDisallowgfx($disallowgfx)
    {
        $this->disallowgfx = $disallowgfx;

        return $this;
    }

    /**
     * Get disallowgfx
     *
     * @return boolean
     */
    public function getDisallowgfx()
    {
        return $this->disallowgfx;
    }

    /**
     * Set asrzerror
     *
     * @param boolean $asrzerror
     *
     * @return Ticket
     */
    public function setAsrzerror($asrzerror)
    {
        $this->asrzerror = $asrzerror;

        return $this;
    }

    /**
     * Get asrzerror
     *
     * @return boolean
     */
    public function getAsrzerror()
    {
        return $this->asrzerror;
    }

    /**
     * Set wfmplanned
     *
     * @param boolean $wfmplanned
     *
     * @return Ticket
     */
    public function setWfmplanned($wfmplanned)
    {
        $this->wfmplanned = $wfmplanned;

        return $this;
    }

    /**
     * Get wfmplanned
     *
     * @return boolean
     */
    public function getWfmplanned()
    {
        return $this->wfmplanned;
    }

    /**
     * Set noagrmgts
     *
     * @param boolean $noagrmgts
     *
     * @return Ticket
     */
    public function setNoagrmgts($noagrmgts)
    {
        $this->noagrmgts = $noagrmgts;

        return $this;
    }

    /**
     * Get noagrmgts
     *
     * @return boolean
     */
    public function getNoagrmgts()
    {
        return $this->noagrmgts;
    }

    /**
     * Set asrzok
     *
     * @param boolean $asrzok
     *
     * @return Ticket
     */
    public function setAsrzok($asrzok)
    {
        $this->asrzok = $asrzok;

        return $this;
    }

    /**
     * Get asrzok
     *
     * @return boolean
     */
    public function getAsrzok()
    {
        return $this->asrzok;
    }

    /**
     * Set setted
     *
     * @param boolean $setted
     *
     * @return Ticket
     */
    public function setSetted($setted)
    {
        $this->setted = $setted;

        return $this;
    }

    /**
     * Get setted
     *
     * @return boolean
     */
    public function getSetted()
    {
        return $this->setted;
    }

    /**
     * Set lineowner
     *
     * @param string $lineowner
     *
     * @return Ticket
     */
    public function setLineowner($lineowner)
    {
        $this->lineowner = $lineowner;

        return $this;
    }

    /**
     * Get lineowner
     *
     * @return string
     */
    public function getLineowner()
    {
        return $this->lineowner;
    }

    /**
     * Set landlinenum
     *
     * @param string $landlinenum
     *
     * @return Ticket
     */
    public function setLandlinenum($landlinenum)
    {
        $this->landlinenum = $landlinenum;

        return $this;
    }

    /**
     * Get landlinenum
     *
     * @return string
     */
    public function getLandlinenum()
    {
        return $this->landlinenum;
    }

    /**
     * Set accContr
     *
     * @param boolean $accContr
     *
     * @return Ticket
     */
    public function setAccContr($accContr)
    {
        $this->accContr = $accContr;

        return $this;
    }

    /**
     * Get accContr
     *
     * @return boolean
     */
    public function getAccContr()
    {
        return $this->accContr;
    }

    /**
     * Set rtMo
     *
     * @param boolean $rtMo
     *
     * @return Ticket
     */
    public function setRtMo($rtMo)
    {
        $this->rtMo = $rtMo;

        return $this;
    }

    /**
     * Get rtMo
     *
     * @return boolean
     */
    public function getRtMo()
    {
        return $this->rtMo;
    }

    /**
     * Set docTicket
     *
     * @param integer $docTicket
     *
     * @return Ticket
     */
    public function setDocTicket($docTicket)
    {
        $this->docTicket = $docTicket;

        return $this;
    }

    /**
     * Get docTicket
     *
     * @return integer
     */
    public function getDocTicket()
    {
        return $this->docTicket;
    }

    /**
     * Set docDate
     *
     * @param \DateTime $docDate
     *
     * @return Ticket
     */
    public function setDocDate($docDate)
    {
        $this->docDate = $docDate;

        return $this;
    }

    /**
     * Get docDate
     *
     * @return \DateTime
     */
    public function getDocDate()
    {
        return $this->docDate;
    }

    /**
     * Set tmcTicket
     *
     * @param integer $tmcTicket
     *
     * @return Ticket
     */
    public function setTmcTicket($tmcTicket)
    {
        $this->tmcTicket = $tmcTicket;

        return $this;
    }

    /**
     * Get tmcTicket
     *
     * @return integer
     */
    public function getTmcTicket()
    {
        return $this->tmcTicket;
    }

    /**
     * Set tmcDate
     *
     * @param \DateTime $tmcDate
     *
     * @return Ticket
     */
    public function setTmcDate($tmcDate)
    {
        $this->tmcDate = $tmcDate;

        return $this;
    }

    /**
     * Get tmcDate
     *
     * @return \DateTime
     */
    public function getTmcDate()
    {
        return $this->tmcDate;
    }

    /**
     * Set task
     *
     * @param \models\Task $task
     *
     * @return Ticket
     */
    public function setTask(\models\Task $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \models\Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set dom
     *
     * @param \models\ListAddr $dom
     *
     * @return Ticket
     */
    public function setDom(\models\ListAddr $dom = null)
    {
        $this->dom = $dom;

        return $this;
    }

    /**
     * Get dom
     *
     * @return \models\ListAddr
     */
    public function getDom()
    {
        return $this->dom;
    }

    /**
     * @param ListContr $partner
     * @return Ticket
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * @return ListContr
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param ListSc $serviceCenter
     * @return Ticket
     */
    public function setServiceCenter(ListSc $serviceCenter = null)
    {
        $this->serviceCenter = $serviceCenter;
        return $this;
    }

    /**
     * @return ListSc
     */
    public function getServiceCenter()
    {
        return $this->serviceCenter;
    }

    /**
     * @return Agreement
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * @param Agreement $agreement
     * @return Ticket
     */
    public function setAgreement(Agreement $agreement = null): Ticket
    {
        $this->agreement = $agreement;
        return $this;
    }


}

