<?php
/**
 * mail@artemd.ru
 * 11.04.2016
 */

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Slack\Model\SlackChannelInterface;

/**
 * SlackChannel
 *
 * @ORM\Table(name="slack_channels")
 * @ORM\Entity(repositoryClass="repository\SlackChannelRepository")
 */
class SlackChannel implements SlackChannelInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="string", length=30, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Serializer\Groups({"default", "api"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Groups({"default", "api"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16)
     * @Serializer\Groups({"default", "api"})
     */
    private $type;

    /**
     * @var SlackUser[]
     *
     * @ORM\ManyToMany(targetEntity="models\SlackUser", mappedBy="channels", cascade={"persist", "remove"})
     */
    private $users;

    /**
     * @var SlackMessage[]
     *
     * @ORM\OneToMany(targetEntity="models\SlackMessage", mappedBy="channel")
     */
    private $messages;

    /**
     * SlackChannel constructor.
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return SlackChannel
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return SlackChannel
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return SlackChannel
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|SlackUser[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     *
     * @param SlackUser $slack_user
     * @return SlackChannel
     */
    public function addUser(SlackUser $slack_user)
    {
        if (!$this->users->contains($slack_user))
            $this->users[] = $slack_user;

        $slack_user->addChannel($this);
        return $this;
    }

    /**
     *
     * @param SlackUser $slackUser
     */
    public function removeUser(SlackUser $slackUser)
    {
        if (!$this->users->contains($slackUser)) {
            return;
        }
        $this->users->removeElement($slackUser);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|SlackMessage[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param SlackMessage $message
     * @return $this
     */
    public function addMessage(SlackMessage $message)
    {
        $this->messages[] = $message;

        return $this;
    }
}
