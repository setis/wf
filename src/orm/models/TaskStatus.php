<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskStatus
 *
 * @ORM\Table(name="task_status", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"name", "plugin_uid"}),
 *     @ORM\UniqueConstraint(columns={"plugin_uid", "is_default"})
 * }, indexes={
 *     @ORM\Index(columns={"plugin_uid"}),
 *     @ORM\Index(columns={"tag", "name", "id"}),
 *     @ORM\Index(columns={"id", "name", "tag"})
 * })
 * @ORM\Entity(repositoryClass="repository\TaskStatusRepository")
 */
class TaskStatus
{
    /**
     * Status then comment send to partner via some integration...
     */
    const TAG_PARTNER_COMMENT_STATUS = 'cmmcontr';
    const TAG_TASK_NEW = 'new';
    const TAG_TASK_DONE = 'done';
    const TAG_SCHEDULED = 'grafik';
    const TAG_TASK_CLOSED = 'closed';
    const TAG_TASK_DELAYED = 'otlozh';

    const TAG_PARTNER_CANCEL = 'opotkaz';
    const TAG_CLIENT_CANCEL = 'otkaz';
    const TAG_TECH_CANCEL = 'tehotkaz';

    const STATUS_AGREEMENT_ACTIVE = 84;

    const STATUS_TELEMARKETING_NEW = 106;

    const STATUS_SERVICE_NEW = 8;
    const STATUS_SERVICE_GFX = 9;
    const STATUS_SERVICE_DONE = 12;
    const STATUS_SERVICE_REPORT = 24;
    const STATUS_SERVICE_CLOSED = 25;
    const STATUS_SERVICE_TIMEOUT = 42;
    const STATUS_SERVICE_ENGINEER_SELECTION = 44;
    const STATUS_SERVICE_SCHEDULED = 45;
    const STATUS_SERVICE_CASH_ADOPTED = 49;
    const STATUS_SERVICE_CANCELED = 94;

    const STATUS_ACCIDENT_NEW = 51;
    const STATUS_ACCIDENT_DONE = 61;
    const STATUS_ACCIDENT_REPORT = 52;
    const STATUS_ACCIDENT_CLOSED = 54;
    const STATUS_ACCIDENT_BY_PARTNER_CANCELED = 60;

    const STATUS_CONNECTION_NEW = 2;
    const STATUS_CONNECTION_DONE = 1;
    const STATUS_CONNECTION_REPORT = 23;
    const STATUS_CONNECTION_CLOSED = 26;
    const STATUS_CONNECTION_CALC_TMC = 99;

    const STATUS_GLOBAL_PROBLEM_NEW = 69;
    const STATUS_GLOBAL_PROBLEM_DONE = 71;
    const STATUS_GLOBAL_PROBLEM_CLOSED = 72;
    const STATUS_GLOBAL_PROBLEM_IN_WORK = 70;

    const STATUS_PROJECT_DELEGATED = 28;
    const STATUS_PROJECT_DONE = 29;
    const STATUS_PROJECT_COMPLETE = 30;
    const STATUS_PROJECT_DELAYED = 31;

    const STATUS_BILL_NEW = 33;
    const STATUS_BILL_APPROVED = 34;
    const STATUS_BILL_PAYED = 35;
    const STATUS_BILL_NOT_PAYED = 36;
    const STATUS_BILL_PARTLY_PAYED = 37;
    const STATUS_BILL_DELAYED = 38;
    const STATUS_BILL_COMPLETE = 39;
    const STATUS_BILL_CANCELED = 40;
    const STATUS_BILL_AGREED = 75;
    const STATUS_BILL_TO_PAYMENT = 76;
    const STATUS_BILL_CLOSING_DOCUMENT = 104;


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false, unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=7, nullable=false)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="plugin_uid", type="string", length=50, nullable=false)
     */
    private $pluginUid;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=50, nullable=true)
     */
    private $tag;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=true)
     */
    private $isDefault;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_hidden", type="boolean", nullable=false)
     */
    private $isHidden;

    /**
     * @var boolean
     *
     * @ORM\Column(name="require_comment", type="boolean", nullable=false)
     */
    private $requireComment;

    /**
     * ONLY GETTER IMPLEMENTED!
     *
     * @var TaskComment[]
     *
     * @ORM\OneToMany(targetEntity="models\TaskComment", mappedBy="status")
     */
    private $comments;

    public function __construct($id = null)
    {
        $this->id = $id;
        $this->isActive = true;
        $this->isDefault = false;
        $this->isHidden = false;
        $this->requireComment = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TaskStatus
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TaskStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return TaskStatus
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get pluginUid
     *
     * @return string
     */
    public function getPluginUid()
    {
        return $this->pluginUid;
    }

    /**
     * Set pluginUid
     *
     * @param string $pluginUid
     *
     * @return TaskStatus
     */
    public function setPluginUid($pluginUid)
    {
        $this->pluginUid = $pluginUid;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    public function getTags() : array
    {
        if (null === $this->tag) {
            return [];
        }

        return explode(',', $this->tag);
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return TaskStatus
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return TaskStatus
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     *
     * @return TaskStatus
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isHidden
     *
     * @return boolean
     */
    public function getIsHidden()
    {
        return $this->isHidden;
    }

    /**
     * Set isHidden
     *
     * @param boolean $isHidden
     *
     * @return TaskStatus
     */
    public function setIsHidden($isHidden)
    {
        $this->isHidden = $isHidden;

        return $this;
    }

    /**
     * Get requireComment
     *
     * @return boolean
     */
    public function getRequireComment()
    {
        return $this->requireComment;
    }

    /**
     * Set requireComment
     *
     * @param boolean $requireComment
     *
     * @return TaskStatus
     */
    public function setRequireComment($requireComment)
    {
        $this->requireComment = $requireComment;

        return $this;
    }

    /**
     * @return TaskComment[]
     */
    public function getComments()
    {
        return $this->comments;
    }
}

