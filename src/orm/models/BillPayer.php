<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payer
 *
 * @ORM\Table(name="bills_plat")
 * @ORM\Entity
 */
class BillPayer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \models\Bill
     *
     * @ORM\OneToMany(targetEntity="models\Bill", mappedBy="payer")
     */
    private $bills;

    /**
     * @var \models\BusinessUnit
     *
     * @ORM\ManyToOne(targetEntity="models\BusinessUnit", inversedBy="payers")
     * @ORM\JoinColumn(name="business_unit_id", referencedColumnName="id")
     */
    private $businessUnit;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return BusinessUnit
     */
    public function getBusinessUnit()
    {
        return $this->businessUnit;
    }

    /**
     * @return BillType
     */
    public function getBills()
    {
        return $this->bills;
    }

    public function __toString()
    {
        return $this->name;
    }
}

