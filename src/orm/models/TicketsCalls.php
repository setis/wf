<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketsCalls
 *
 * @ORM\Table(name="tickets_calls")
 * @ORM\Entity
 */
class TicketsCalls
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $taskId;

    /**
     * @var string
     *
     * @ORM\Column(name="plugin_uid", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pluginUid;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="callfrom", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $callfrom;

    /**
     * @var string
     *
     * @ORM\Column(name="callto", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $callto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="calldate", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $calldate;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="asteriskstatus", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $asteriskstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="asteriskdate", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $asteriskdate;

    /**
     * @var string
     *
     * @ORM\Column(name="asteriskfile", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $asteriskfile;

    /**
     * @var string
     *
     * @ORM\Column(name="asteriskuid", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $asteriskuid;

    /**
     * @var string
     *
     * @ORM\Column(name="asteriskpathtofile", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $asteriskpathtofile;

    /**
     * @var string
     *
     * @ORM\Column(name="techphone", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $techphone;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set taskId
     *
     * @param integer $taskId
     *
     * @return TicketsCalls
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set pluginUid
     *
     * @param string $pluginUid
     *
     * @return TicketsCalls
     */
    public function setPluginUid($pluginUid)
    {
        $this->pluginUid = $pluginUid;

        return $this;
    }

    /**
     * Get pluginUid
     *
     * @return string
     */
    public function getPluginUid()
    {
        return $this->pluginUid;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return TicketsCalls
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set callfrom
     *
     * @param string $callfrom
     *
     * @return TicketsCalls
     */
    public function setCallfrom($callfrom)
    {
        $this->callfrom = $callfrom;

        return $this;
    }

    /**
     * Get callfrom
     *
     * @return string
     */
    public function getCallfrom()
    {
        return $this->callfrom;
    }

    /**
     * Set callto
     *
     * @param string $callto
     *
     * @return TicketsCalls
     */
    public function setCallto($callto)
    {
        $this->callto = $callto;

        return $this;
    }

    /**
     * Get callto
     *
     * @return string
     */
    public function getCallto()
    {
        return $this->callto;
    }

    /**
     * Set calldate
     *
     * @param \DateTime $calldate
     *
     * @return TicketsCalls
     */
    public function setCalldate($calldate)
    {
        $this->calldate = $calldate;

        return $this;
    }

    /**
     * Get calldate
     *
     * @return \DateTime
     */
    public function getCalldate()
    {
        return $this->calldate;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return TicketsCalls
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set asteriskstatus
     *
     * @param string $asteriskstatus
     *
     * @return TicketsCalls
     */
    public function setAsteriskstatus($asteriskstatus)
    {
        $this->asteriskstatus = $asteriskstatus;

        return $this;
    }

    /**
     * Get asteriskstatus
     *
     * @return string
     */
    public function getAsteriskstatus()
    {
        return $this->asteriskstatus;
    }

    /**
     * Set asteriskdate
     *
     * @param \DateTime $asteriskdate
     *
     * @return TicketsCalls
     */
    public function setAsteriskdate($asteriskdate)
    {
        $this->asteriskdate = $asteriskdate;

        return $this;
    }

    /**
     * Get asteriskdate
     *
     * @return \DateTime
     */
    public function getAsteriskdate()
    {
        return $this->asteriskdate;
    }

    /**
     * Set asteriskfile
     *
     * @param string $asteriskfile
     *
     * @return TicketsCalls
     */
    public function setAsteriskFile($asteriskfile)
    {
        $this->asteriskfile = $asteriskfile;

        return $this;
    }

    /**
     * Get asteriskfile
     *
     * @return string
     */
    public function getAsteriskFile()
    {
        return $this->asteriskfile;
    }

    /**
     * Set asteriskuid
     *
     * @param string $asteriskuid
     *
     * @return TicketsCalls
     */
    public function setAsteriskuid($asteriskuid)
    {
        $this->asteriskuid = $asteriskuid;

        return $this;
    }

    /**
     * Get asteriskuid
     *
     * @return string
     */
    public function getAsteriskuid()
    {
        return $this->asteriskuid;
    }

    /**
     * Set asteriskpathtofile
     *
     * @param string $asteriskpathtofile
     *
     * @return TicketsCalls
     */
    public function setAsteriskpathtofile($asteriskpathtofile)
    {
        $this->asteriskpathtofile = $asteriskpathtofile;

        return $this;
    }

    /**
     * Get asteriskpathtofile
     *
     * @return string
     */
    public function getAsteriskpathtofile()
    {
        return $this->asteriskpathtofile;
    }

    /**
     * Set techphone
     *
     * @param string $techphone
     *
     * @return TicketsCalls
     */
    public function setTechphone($techphone)
    {
        $this->techphone = $techphone;

        return $this;
    }

    /**
     * Get techphone
     *
     * @return string
     */
    public function getTechphone()
    {
        return $this->techphone;
    }

}

