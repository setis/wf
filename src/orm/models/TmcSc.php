<?php

namespace models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TmcSc
 *
 * @ORM\Table(name="tmc_sc", indexes={
 *     @ORM\Index(columns={"sc_id", "tmc_id", "inplace", "incoming"})
 * })
 * @ORM\Entity
 */
class TmcSc
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="sc_id", type="integer", nullable=false)
     */
    private $scId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tmc_id", type="integer", nullable=false)
     */
    private $tmcId;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=true)
     */
    private $serial;

    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer", nullable=false)
     */
    private $count;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ledit", type="datetime", nullable=false)
     */
    private $ledit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inplace", type="boolean", nullable=false)
     */
    private $inplace;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dismantled", type="boolean", nullable=false)
     */
    private $dismantled;

    /**
     * @var boolean
     *
     * @ORM\Column(name="incoming", type="boolean", nullable=false)
     */
    private $incoming;

    /**
     * @var integer
     *
     * @ORM\Column(name="docnum", type="integer", nullable=true)
     */
    private $docnum;

    /**
     * @var integer
     *
     * @ORM\Column(name="from_task_id", type="integer", nullable=true)
     */
    private $fromTaskId;

    /**
     * @var integer
     *
     * @ORM\Column(name="from_tech_id", type="integer", nullable=true)
     */
    private $fromTechId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="from_date", type="datetime", nullable=true)
     */
    private $fromDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="integer", nullable=true)
     */
    private $deleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="tmc_ticket_id", type="integer", nullable=true)
     */
    private $tmcTicketId;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="models\TmcScTech", mappedBy="tmcSc")
     */
    private $tmcScTechs;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var \models\ListSc
     *
     * @ORM\ManyToOne(targetEntity="models\ListSc")
     * @ORM\JoinColumn(name="sc_id", referencedColumnName="id")
     */
    private $sc;

    public function __construct()
    {
        $this->tmcScTechs = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set scId
     *
     * @param integer $scId
     *
     * @return TmcSc
     */
    public function setScId($scId)
    {
        $this->scId = $scId;

        return $this;
    }

    /**
     * Get scId
     *
     * @return integer
     */
    public function getScId()
    {
        return $this->scId;
    }

    /**
     * Set tmcId
     *
     * @param integer $tmcId
     *
     * @return TmcSc
     */
    public function setTmcId($tmcId)
    {
        $this->tmcId = $tmcId;

        return $this;
    }

    /**
     * Get tmcId
     *
     * @return integer
     */
    public function getTmcId()
    {
        return $this->tmcId;
    }

    /**
     * Set serial
     *
     * @param string $serial
     *
     * @return TmcSc
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;

        return $this;
    }

    /**
     * Get serial
     *
     * @return string
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return TmcSc
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return TmcSc
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set ledit
     *
     * @param \DateTime $ledit
     *
     * @return TmcSc
     */
    public function setLedit($ledit)
    {
        $this->ledit = $ledit;

        return $this;
    }

    /**
     * Get ledit
     *
     * @return \DateTime
     */
    public function getLedit()
    {
        return $this->ledit;
    }

    /**
     * Set inplace
     *
     * @param boolean $inplace
     *
     * @return TmcSc
     */
    public function setInplace($inplace)
    {
        $this->inplace = $inplace;

        return $this;
    }

    /**
     * Get inplace
     *
     * @return boolean
     */
    public function getInplace()
    {
        return $this->inplace;
    }

    /**
     * Set dismantled
     *
     * @param boolean $dismantled
     *
     * @return TmcSc
     */
    public function setDismantled($dismantled)
    {
        $this->dismantled = $dismantled;

        return $this;
    }

    /**
     * Get dismantled
     *
     * @return boolean
     */
    public function getDismantled()
    {
        return $this->dismantled;
    }

    /**
     * Set incoming
     *
     * @param boolean $incoming
     *
     * @return TmcSc
     */
    public function setIncoming($incoming)
    {
        $this->incoming = $incoming;

        return $this;
    }

    /**
     * Get incoming
     *
     * @return boolean
     */
    public function getIncoming()
    {
        return $this->incoming;
    }

    /**
     * Set docnum
     *
     * @param integer $docnum
     *
     * @return TmcSc
     */
    public function setDocnum($docnum)
    {
        $this->docnum = $docnum;

        return $this;
    }

    /**
     * Get docnum
     *
     * @return integer
     */
    public function getDocnum()
    {
        return $this->docnum;
    }

    /**
     * Set fromTaskId
     *
     * @param integer $fromTaskId
     *
     * @return TmcSc
     */
    public function setFromTaskId($fromTaskId)
    {
        $this->fromTaskId = $fromTaskId;

        return $this;
    }

    /**
     * Get fromTaskId
     *
     * @return integer
     */
    public function getFromTaskId()
    {
        return $this->fromTaskId;
    }

    /**
     * Set fromTechId
     *
     * @param integer $fromTechId
     *
     * @return TmcSc
     */
    public function setFromTechId($fromTechId)
    {
        $this->fromTechId = $fromTechId;

        return $this;
    }

    /**
     * Get fromTechId
     *
     * @return integer
     */
    public function getFromTechId()
    {
        return $this->fromTechId;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     *
     * @return TmcSc
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     *
     * @return TmcSc
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set tmcTicketId
     *
     * @param integer $tmcTicketId
     *
     * @return TmcSc
     */
    public function setTmcTicketId($tmcTicketId)
    {
        $this->tmcTicketId = $tmcTicketId;

        return $this;
    }

    /**
     * Get tmcTicketId
     *
     * @return integer
     */
    public function getTmcTicketId()
    {
        return $this->tmcTicketId;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return TmcSc
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return ListSc
     */
    public function getSc()
    {
        return $this->sc;
    }

    /**
     * @param ListSc $sc
     * @return TmcSc
     */
    public function setSc(ListSc $sc)
    {
        $this->sc = $sc;
        return $this;
    }

    /**
     * @param TmcScTech $tmcScTech
     * @return TmcSc
     */
    public function addTmcScTech(TmcScTech $tmcScTech)
    {
        $this->tmcScTechs[] = $tmcScTech;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTmcScTechs()
    {
        return $this->tmcScTechs;
    }

    /**
     * @param TmcScTech $tmcScTech
     */
    public function removeTmcScTech(TmcScTech $tmcScTech)
    {
        if (!$this->tmcScTechs->contains($tmcScTech)) {
            return;
        }
        $this->tmcScTechs->removeElement($tmcScTech);
    }


}

