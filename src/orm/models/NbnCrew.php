<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * NbnCrews
 *
 * @ORM\Table(name="nbn_crews",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(columns={"empl_user_id", "nbn_department_key", "nbn_class_memo", "nbn_slot_memo"})
 *      },
 *     indexes={
 *          @ORM\Index(columns={"active"})
 *      }
 * )
 * @ORM\Entity
 */
class NbnCrew
{

    const ACCIDENTS_MEMO = 'CREW_CLIENTS_PROBLEMS';
    const CONNECTIONS_MEMO = 'CREW_NEW_USERS';

    /**
     * @var string
     *
     * @ORM\Column(name="nbn_crew_key", type="string", length=50, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $nbnCrewKey;

    /**
     * @var string
     *
     * @ORM\Column(name="nbn_department_key", type="string", length=50, nullable=false)
     */
    private $nbnDepartmentKey;

    /**
     * @var string
     *
     * @ORM\Column(name="nbn_class_memo", type="string", length=50, nullable=true)
     */
    private $nbnClassMemo;

    /**
     * @var string
     *
     * @ORM\Column(name="nbn_slot_memo", type="string", length=50, nullable=true)
     */
    private $nbnSlotMemo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="models\User", inversedBy="nbnCrews")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="empl_user_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $technician;

    /**
     * Set nbnCrewKey
     *
     * @param string $nbnCrewKey
     *
     * @return NbnCrew
     */
    public function setNbnCrewKey($nbnCrewKey)
    {
        $this->nbnCrewKey = $nbnCrewKey;

        return $this;
    }

    /**
     * Get nbnCrewKey
     *
     * @return string
     */
    public function getNbnCrewKey()
    {
        return $this->nbnCrewKey;
    }

    /**
     * Set nbnDepartmentKey
     *
     * @param string $nbnDepartmentKey
     *
     * @return NbnCrew
     */
    public function setNbnDepartmentKey($nbnDepartmentKey)
    {
        $this->nbnDepartmentKey = $nbnDepartmentKey;

        return $this;
    }

    /**
     * Get nbnDepartmentKey
     *
     * @return string
     */
    public function getNbnDepartmentKey()
    {
        return $this->nbnDepartmentKey;
    }

    /**
     * Set nbnClassMemo
     *
     * @param string $nbnClassMemo
     *
     * @return NbnCrew
     */
    public function setNbnClassMemo($nbnClassMemo)
    {
        $this->nbnClassMemo = $nbnClassMemo;

        return $this;
    }

    /**
     * Get nbnClassMemo
     *
     * @return string
     */
    public function getNbnClassMemo()
    {
        return $this->nbnClassMemo;
    }

    /**
     * Set nbnSlotMemo
     *
     * @param string $nbnSlotMemo
     *
     * @return NbnCrew
     */
    public function setNbnSlotMemo($nbnSlotMemo)
    {
        $this->nbnSlotMemo = $nbnSlotMemo;

        return $this;
    }

    /**
     * Get nbnSlotMemo
     *
     * @return string
     */
    public function getNbnSlotMemo()
    {
        return $this->nbnSlotMemo;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return NbnCrew
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param User $technician
     * @return NbnCrew
     */
    public function setTechnician(User $technician)
    {
        $this->technician = $technician;
        return $this;
    }

    /**
     * @return User
     */
    public function getTechnician()
    {
        return $this->technician;
    }
}

