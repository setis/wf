<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * LinkUserNbnArea
 *
 * @ORM\Table(name="link_user_nbn_area", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"user_id", "area_id"})
 * })
 * @ORM\Entity
 */
class LinkUserNbnArea
{
    /**
     * @var integer
     *
     * @ORM\Column(name="area_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $areaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;


    /**
     * Set areaId
     *
     * @param integer $areaId
     *
     * @return LinkUserNbnArea
     */
    public function setAreaId($areaId)
    {
        $this->areaId = $areaId;

        return $this;
    }

    /**
     * Get areaId
     *
     * @return integer
     */
    public function getAreaId()
    {
        return $this->areaId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return LinkUserNbnArea
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
}

