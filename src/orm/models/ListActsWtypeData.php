<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListActsWtypeData
 *
 * @ORM\Table(name="list_acts_wtype_data")
 * @ORM\Entity
 */
class ListActsWtypeData
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="act_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $actId;

    /**
     * @var integer
     *
     * @ORM\Column(name="wtype_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $wtypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="wtype_count", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $wtypeCount;

    /**
     * @var string
     *
     * @ORM\Column(name="wtype_ammount", type="decimal", precision=10, scale=2, nullable=false, unique=false)
     */
    private $wtypeAmmount;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actId
     *
     * @param integer $actId
     *
     * @return ListActsWtypeData
     */
    public function setActId($actId)
    {
        $this->actId = $actId;

        return $this;
    }

    /**
     * Get actId
     *
     * @return integer
     */
    public function getActId()
    {
        return $this->actId;
    }

    /**
     * Set wtypeId
     *
     * @param integer $wtypeId
     *
     * @return ListActsWtypeData
     */
    public function setWtypeId($wtypeId)
    {
        $this->wtypeId = $wtypeId;

        return $this;
    }

    /**
     * Get wtypeId
     *
     * @return integer
     */
    public function getWtypeId()
    {
        return $this->wtypeId;
    }

    /**
     * Set wtypeCount
     *
     * @param integer $wtypeCount
     *
     * @return ListActsWtypeData
     */
    public function setWtypeCount($wtypeCount)
    {
        $this->wtypeCount = $wtypeCount;

        return $this;
    }

    /**
     * Get wtypeCount
     *
     * @return integer
     */
    public function getWtypeCount()
    {
        return $this->wtypeCount;
    }

    /**
     * Set wtypeAmmount
     *
     * @param string $wtypeAmmount
     *
     * @return ListActsWtypeData
     */
    public function setWtypeAmmount($wtypeAmmount)
    {
        $this->wtypeAmmount = $wtypeAmmount;

        return $this;
    }

    /**
     * Get wtypeAmmount
     *
     * @return string
     */
    public function getWtypeAmmount()
    {
        return $this->wtypeAmmount;
    }
}

