<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * BusinessUnit
 *
 * @ORM\Table(name="business_units")
 * @ORM\Entity
 */
class BusinessUnit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=16777215, precision=0, scale=0, nullable=true, unique=false)
     */
    private $description;

    /**
     * @var BillPayer[]
     *
     * @ORM\OneToMany(targetEntity="models\BillPayer", mappedBy="businessUnit")
     */
    private $payers;

    /**
     * @var BillType[]
     *
     * @ORM\OneToMany(targetEntity="models\BillType", mappedBy="businessUnit")
     */
    private $types;

    /**
     * @param string $title
     * @return BusinessUnit
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $description
     * @return BusinessUnit
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return BillPayer[]
     */
    public function getPayers()
    {
        return $this->payers;
    }

    /**
     * @return BillType[]
     */
    public function getTypes()
    {
        return $this->types;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

}

