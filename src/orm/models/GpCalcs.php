<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * GpCalcs
 *
 * @ORM\Table(name="gp_calcs")
 * @ORM\Entity
 */
class GpCalcs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taskId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="calc_date", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $calcDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="edit_user_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $editUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edit_last_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $editLastDate;


    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return GpCalcs
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set calcDate
     *
     * @param \DateTime $calcDate
     *
     * @return GpCalcs
     */
    public function setCalcDate($calcDate)
    {
        $this->calcDate = $calcDate;

        return $this;
    }

    /**
     * Get calcDate
     *
     * @return \DateTime
     */
    public function getCalcDate()
    {
        return $this->calcDate;
    }

    /**
     * Set editUserId
     *
     * @param integer $editUserId
     *
     * @return GpCalcs
     */
    public function setEditUserId($editUserId)
    {
        $this->editUserId = $editUserId;

        return $this;
    }

    /**
     * Get editUserId
     *
     * @return integer
     */
    public function getEditUserId()
    {
        return $this->editUserId;
    }

    /**
     * Set editLastDate
     *
     * @param \DateTime $editLastDate
     *
     * @return GpCalcs
     */
    public function setEditLastDate($editLastDate)
    {
        $this->editLastDate = $editLastDate;

        return $this;
    }

    /**
     * Get editLastDate
     *
     * @return \DateTime
     */
    public function getEditLastDate()
    {
        return $this->editLastDate;
    }
}

