<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListArea
 *
 * @ORM\Table(name="list_area", indexes={
 *     @ORM\Index(columns={"nbn_department_key"})
 * })
 * @ORM\Entity(repositoryClass="repository\ListAreaRepository")
 */
class ListArea
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="`desc`", type="text", length=16777215, nullable=true)
     */
    private $desc;

    /**
     * @var integer
     *
     * @ORM\Column(name="dis_id", type="integer", nullable=true)
     */
    private $disId;

    /**
     * @var string
     *
     * @ORM\Column(name="kladr_code", type="string", length=20)
     */
    private $kladrCode;

    /**
     * @var string
     *
     * @ORM\Column(name="nbn_department_key", type="string", length=50, nullable=true)
     */
    private $nbnDepartmentKey;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ListArea
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set desc
     *
     * @param string $desc
     *
     * @return ListArea
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set disId
     *
     * @param integer $disId
     *
     * @return ListArea
     */
    public function setDisId($disId)
    {
        $this->disId = $disId;

        return $this;
    }

    /**
     * Get disId
     *
     * @return integer
     */
    public function getDisId()
    {
        return $this->disId;
    }

    /**
     * Set kladrCode
     *
     * @param string $kladrCode
     *
     * @return ListArea
     */
    public function setKladrCode($kladrCode)
    {
        $this->kladrCode = $kladrCode;

        return $this;
    }

    /**
     * Get kladrCode
     *
     * @return string
     */
    public function getKladrCode()
    {
        return $this->kladrCode;
    }

    /**
     * Set nbnDepartmentKey
     *
     * @param string $nbnDepartmentKey
     *
     * @return ListArea
     */
    public function setNbnDepartmentKey($nbnDepartmentKey)
    {
        $this->nbnDepartmentKey = $nbnDepartmentKey;

        return $this;
    }

    /**
     * Get nbnDepartmentKey
     *
     * @return string
     */
    public function getNbnDepartmentKey()
    {
        return $this->nbnDepartmentKey;
    }
}

