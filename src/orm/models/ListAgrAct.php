<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListAgrAct
 *
 * @ORM\Table(name="list_agr_act")
 * @ORM\Entity
 */
class ListAgrAct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="agr_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $agrId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ds_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dsId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sign_date", type="date", precision=0, scale=0, nullable=false, unique=false)
     */
    private $signDate;

    /**
     * @var string
     *
     * @ORM\Column(name="ammount", type="decimal", precision=10, scale=2, nullable=true, unique=false)
     */
    private $ammount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=16777215, precision=0, scale=0, nullable=false, unique=false)
     */
    private $comment;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return ListAgrAct
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set agrId
     *
     * @param integer $agrId
     *
     * @return ListAgrAct
     */
    public function setAgrId($agrId)
    {
        $this->agrId = $agrId;

        return $this;
    }

    /**
     * Get agrId
     *
     * @return integer
     */
    public function getAgrId()
    {
        return $this->agrId;
    }

    /**
     * Set dsId
     *
     * @param integer $dsId
     *
     * @return ListAgrAct
     */
    public function setDsId($dsId)
    {
        $this->dsId = $dsId;

        return $this;
    }

    /**
     * Get dsId
     *
     * @return integer
     */
    public function getDsId()
    {
        return $this->dsId;
    }

    /**
     * Set signDate
     *
     * @param \DateTime $signDate
     *
     * @return ListAgrAct
     */
    public function setSignDate($signDate)
    {
        $this->signDate = $signDate;

        return $this;
    }

    /**
     * Get signDate
     *
     * @return \DateTime
     */
    public function getSignDate()
    {
        return $this->signDate;
    }

    /**
     * Set ammount
     *
     * @param string $ammount
     *
     * @return ListAgrAct
     */
    public function setAmmount($ammount)
    {
        $this->ammount = $ammount;

        return $this;
    }

    /**
     * Get ammount
     *
     * @return string
     */
    public function getAmmount()
    {
        return $this->ammount;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return ListAgrAct
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ListAgrAct
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
}

