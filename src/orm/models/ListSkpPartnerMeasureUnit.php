<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ListSkpPEdizm
 *
 * @ORM\Table(name="list_skp_p_edizm")
 * @ORM\Entity
 */
class ListSkpPartnerMeasureUnit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="fulltitle", type="text", length=16777215, precision=0, scale=0, nullable=true, unique=false)
     * @Serializer\Groups({"extended"})
     */
    private $fulltitle;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="cr_uid", referencedColumnName="id", onDelete="SET NULL")
     * @Serializer\Groups({"extended"})
     */
    private $author;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cr_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     * @Serializer\Groups({"extended"})
     */
    private $crDate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ListSkpPartnerMeasureUnit
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set fulltitle
     *
     * @param string $fulltitle
     *
     * @return ListSkpPartnerMeasureUnit
     */
    public function setFulltitle($fulltitle)
    {
        $this->fulltitle = $fulltitle;

        return $this;
    }

    /**
     * Get fulltitle
     *
     * @return string
     */
    public function getFulltitle()
    {
        return $this->fulltitle;
    }

    /**
     * Set crDate
     *
     * @param \DateTime $crDate
     *
     * @return ListSkpPartnerMeasureUnit
     */
    public function setCrDate($crDate)
    {
        $this->crDate = $crDate;

        return $this;
    }

    /**
     * Get crDate
     *
     * @return \DateTime
     */
    public function getCrDate()
    {
        return $this->crDate;
    }

    /**
     * @param User $author
     * @return ListSkpPartnerMeasureUnit
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }
}

