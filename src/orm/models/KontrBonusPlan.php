<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * KontrBonusPlan
 *
 * @ORM\Table(name="kontr_bonus_plan")
 * @ORM\Entity
 */
class KontrBonusPlan
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="kontr_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $kontrId;

    /**
     * @var integer
     *
     * @ORM\Column(name="convval", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $convval;

    /**
     * @var float
     *
     * @ORM\Column(name="midcheck", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $midcheck;

    /**
     * @var integer
     *
     * @ORM\Column(name="bonusval", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $bonusval;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ledit", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ledit;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kontrId
     *
     * @param integer $kontrId
     *
     * @return KontrBonusPlan
     */
    public function setKontrId($kontrId)
    {
        $this->kontrId = $kontrId;

        return $this;
    }

    /**
     * Get kontrId
     *
     * @return integer
     */
    public function getKontrId()
    {
        return $this->kontrId;
    }

    /**
     * Set convval
     *
     * @param integer $convval
     *
     * @return KontrBonusPlan
     */
    public function setConvval($convval)
    {
        $this->convval = $convval;

        return $this;
    }

    /**
     * Get convval
     *
     * @return integer
     */
    public function getConvval()
    {
        return $this->convval;
    }

    /**
     * Set midcheck
     *
     * @param float $midcheck
     *
     * @return KontrBonusPlan
     */
    public function setMidcheck($midcheck)
    {
        $this->midcheck = $midcheck;

        return $this;
    }

    /**
     * Get midcheck
     *
     * @return float
     */
    public function getMidcheck()
    {
        return $this->midcheck;
    }

    /**
     * Set bonusval
     *
     * @param integer $bonusval
     *
     * @return KontrBonusPlan
     */
    public function setBonusval($bonusval)
    {
        $this->bonusval = $bonusval;

        return $this;
    }

    /**
     * Get bonusval
     *
     * @return integer
     */
    public function getBonusval()
    {
        return $this->bonusval;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return KontrBonusPlan
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set ledit
     *
     * @param \DateTime $ledit
     *
     * @return KontrBonusPlan
     */
    public function setLedit($ledit)
    {
        $this->ledit = $ledit;

        return $this;
    }

    /**
     * Get ledit
     *
     * @return \DateTime
     */
    public function getLedit()
    {
        return $this->ledit;
    }
}

