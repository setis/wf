<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * SkpMoney
 *
 * @ORM\Table(name="skp_money", indexes={
 *     @ORM\Index(columns={"task_id"})
 * })
 * @ORM\Entity
 */
class SkpMoney
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer", nullable=false, unique=false)
     */
    private $taskId;

    /**
     * @var float
     *
     * @ORM\Column(name="ammount", type="decimal", precision=10, scale=2, nullable=true, unique=false)
     */
    private $ammount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="le", type="datetime", nullable=true, unique=false)
     */
    private $le;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false, unique=false)
     */
    private $userId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="last", type="boolean", nullable=true, unique=false)
     */
    private $last;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set taskId
     *
     * @param integer $taskId
     *
     * @return SkpMoney
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set ammount
     *
     * @param float $ammount
     *
     * @return SkpMoney
     */
    public function setAmmount($ammount)
    {
        $this->ammount = $ammount;

        return $this;
    }

    /**
     * Get ammount
     *
     * @return float
     */
    public function getAmmount()
    {
        return $this->ammount;
    }

    /**
     * Set le
     *
     * @param \DateTime $le
     *
     * @return SkpMoney
     */
    public function setLe($le)
    {
        $this->le = $le;

        return $this;
    }

    /**
     * Get le
     *
     * @return \DateTime
     */
    public function getLe()
    {
        return $this->le;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return SkpMoney
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set last
     *
     * @param boolean $last
     *
     * @return SkpMoney
     */
    public function setLast($last)
    {
        $this->last = $last;

        return $this;
    }

    /**
     * Get last
     *
     * @return boolean
     */
    public function getLast()
    {
        return $this->last;
    }
}

