<?php
namespace models;


use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceSubtypes
 *
 * @ORM\Table(name="service_subtypes", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"subtype_name"})
 * })
 * @ORM\Entity
 */
class ServiceSubtypes
{
    /**
     * @var string
     *
     * @ORM\Column(name="subtype_name", type="string", length=200, nullable=false)
     */
    private $subtypeName;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set subtypeName
     *
     * @param string $subtypeName
     *
     * @return ServiceSubtypes
     */
    public function setSubtypeName($subtypeName)
    {
        $this->subtypeName = $subtypeName;

        return $this;
    }

    /**
     * Get subtypeName
     *
     * @return string
     */
    public function getSubtypeName()
    {
        return $this->subtypeName;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

