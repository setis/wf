<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ListSkpPWtypes
 *
 * @ORM\Table(name="list_skp_p_wtypes")
 * @ORM\Entity
 */
class ListSkpPartnerWorkTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $title;

    /**
     * @var \models\ListSkpPartnerCatTitle
     *
     * @ORM\ManyToOne(targetEntity="models\ListSkpPartnerCatTitle")
     * @ORM\JoinColumn(name="cat_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Groups({"default"})
     */
    private $category;

    /**
     * @var \models\ListSkpPartnerMeasureUnit
     *
     * @ORM\ManyToOne(targetEntity="models\ListSkpPartnerMeasureUnit")
     * @ORM\JoinColumn(name="ei_id", referencedColumnName="id", onDelete="SET NULL")
     * @Serializer\Groups({"default"})
     */
    private $unit;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="cr_uid", referencedColumnName="id", onDelete="SET NULL")
     * @Serializer\Groups({"extended"})
     */
    private $author;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cr_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     * @Serializer\Groups({"extended"})
     */
    private $crDate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ListSkpPartnerWorkTypes
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set eiId
     *
     * @param integer $eiId
     *
     * @return ListSkpPartnerWorkTypes
     */
    public function setEiId($eiId)
    {
        $this->eiId = $eiId;

        return $this;
    }

    /**
     * Get eiId
     *
     * @return integer
     */
    public function getEiId()
    {
        return $this->eiId;
    }

    /**
     * Set crDate
     *
     * @param \DateTime $crDate
     *
     * @return ListSkpPartnerWorkTypes
     */
    public function setCrDate($crDate)
    {
        $this->crDate = $crDate;

        return $this;
    }

    /**
     * Get crDate
     *
     * @return \DateTime
     */
    public function getCrDate()
    {
        return $this->crDate;
    }

    /**
     * @return ListSkpPartnerCatTitle
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param ListSkpPartnerCatTitle $category
     * @return ListSkpPartnerWorkTypes
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     * @return ListSkpPartnerWorkTypes
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @param ListSkpPartnerMeasureUnit $unit
     * @return ListSkpPartnerWorkTypes
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @return ListSkpPartnerMeasureUnit
     */
    public function getUnit()
    {
        return $this->unit;
    }

}

