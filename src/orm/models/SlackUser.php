<?php
/**
 * mail@artemd.ru
 * 11.04.2016
 */

namespace models;


use Doctrine\ORM\Mapping as ORM;
use models\User;
use WF\Slack\Model\SlackUserInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * SlackUser
 *
 * @ORM\Table(name="slack_users")
 * @ORM\Entity(repositoryClass="repository\SlackUserRepository")
 */
class SlackUser implements SlackUserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="string", length=30, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Groups({"default"})
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_deleted", type="boolean")
     * @Serializer\Groups({"default"})
     */
    private $deleted;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     * @Serializer\Groups({"default"})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="real_name", type="string", length=100)
     * @Serializer\Groups({"default"})
     */
    private $realName;

    /**
     *
     * @var \Doctrine\Common\Collections\Collection|User[]
     *
     * @ORM\OneToOne(targetEntity="models\User", inversedBy="slackUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * })
     */
    private $user;

    /**
     *
     * @var \Doctrine\Common\Collections\Collection|SlackChannel[]
     *
     * @ORM\ManyToMany(targetEntity="models\SlackChannel", inversedBy="users", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="slack_users_channels",
     *     joinColumns={@ORM\JoinColumn(name="slackuser_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="slackchannel_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     * @Serializer\Groups({"default"})
     */
    private $channels;

    /**
     * SlackUser constructor.
     */
    public function __construct()
    {
        $this->channels = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return SlackUser
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return SlackUser
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param boolean $deleted
     * @return SlackUser
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return SlackUser
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getRealName()
    {
        return $this->realName;
    }

    /**
     * @param string $realName
     * @return SlackUser
     */
    public function setRealName($realName)
    {
        $this->realName = $realName;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return SlackUser
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|SlackChannel[]
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * @param SlackChannel $channel
     * @return SlackUser
     */
    public function addChannel(SlackChannel $channel)
    {
        if(!$this->channels->contains($channel))
            $this->channels[] = $channel;

        return $this;
    }

    /**
     * @param SlackChannel $channel
     */
    public function removeChannel(SlackChannel $channel) {
        if(!$this->channels->contains($channel))
            return;

        $this->channels->removeElement($channel);
        $channel->removeUser($this);
    }

}
