<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmpTf
 *
 * @ORM\Table(name="emp_tf")
 * @ORM\Entity
 */
class EmpTf
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeStart", type="time", precision=0, scale=0, nullable=false, unique=false)
     */
    private $timestart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeEnd", type="time", precision=0, scale=0, nullable=false, unique=false)
     */
    private $timeend;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return EmpTf
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set timestart
     *
     * @param \DateTime $timestart
     *
     * @return EmpTf
     */
    public function setTimestart($timestart)
    {
        $this->timestart = $timestart;

        return $this;
    }

    /**
     * Get timestart
     *
     * @return \DateTime
     */
    public function getTimestart()
    {
        return $this->timestart;
    }

    /**
     * Set timeend
     *
     * @param \DateTime $timeend
     *
     * @return EmpTf
     */
    public function setTimeend($timeend)
    {
        $this->timeend = $timeend;

        return $this;
    }

    /**
     * Get timeend
     *
     * @return \DateTime
     */
    public function getTimeend()
    {
        return $this->timeend;
    }
}

