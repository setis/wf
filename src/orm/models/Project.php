<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use WF\Task\Model\BaseTicketInterface;
use WF\Task\Model\TechnicalTicketInterface;

/**
 * Projects
 *
 * @ORM\Table(name="projects")
 * @ORM\Entity
 */
class Project implements BaseTicketInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var Task
     * 
     * @ORM\OneToOne(targetEntity="Task", inversedBy="project")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $task;

    /**
     * @var ProjectType
     *
     * @ORM\ManyToOne(targetEntity="ProjectType")
     * @ORM\JoinColumn(name="type", referencedColumnName="id", onDelete="SET NULL")
     */
    private $type;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="deadline", type="datetime", nullable=true)
     */
    private $deadline;

    /**
     * @var ListContr
     *
     * @ORM\ManyToOne(targetEntity="ListContr")
     * @ORM\JoinColumn(name="zakazchik", referencedColumnName="id", onDelete="SET NULL")
     */
    private $partner;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="delayed", type="date", nullable=true)
     */
    private $delayTo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="remind", type="boolean", nullable=true)
     */
    private $isRemindEnable;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="remdate", type="date", nullable=true)
     */
    private $remindAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer", nullable=true)
     */
    private $priority;
    
    public function __construct()
    {
        $this->priority = 0;
        $this->remindEnable = false;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get task
     *
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Project
     */
    public function setType(ProjectType $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return ProjectType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set deadline
     *
     * @param DateTime $deadline
     *
     * @return Project
     */
    public function setDeadline(DateTime $deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * 
     * @return ListContr
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * 
     * @param ListContr $partner
     * @return Project
     */
    public function setPartner(ListContr $partner)
    {
        $this->partner = $partner;
        
        return $this;
    }

    /**
     * 
     * @return DateTime
     */
    public function getDelayTo()
    {
        return $this->delayTo;
    }

    /**
     * 
     * @param DateTime $delayTo
     * @return Project
     */
    public function setDelayTo(DateTime $delayTo)
    {
        $this->delayTo = $delayTo;
        
        return $this;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isRemindEnable()
    {
        return $this->isRemindEnable;
    }

    /**
     * 
     * @param boolean $isRemindEnable
     * @return Project
     */
    public function setIsRemindEnable($isRemindEnable)
    {
        $this->isRemindEnable = $isRemindEnable;
        
        return $this;
    }

    /**
     * 
     * @return DateTime
     */
    public function getRemindAt()
    {
        return $this->remindAt;
    }

    /**
     * 
     * @param DateTime $remindAt
     * @return Project
     */
    public function setRemindAt(DateTime $remindAt)
    {
        $this->remindAt = $remindAt;
        
        return $this;
    }

        

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return Projects
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return int
     */
    public function getTaskId()
    {
        return $this->getTask()->getId();
    }

    /**
     * @return ProjectType
     */
    public function getTaskType()
    {
        return $this->getType();
    }
}

