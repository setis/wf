<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAccess
 *
 * @ORM\Table(name="user_access",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(columns={"user_id", "plugin_uid"})
 *     })
 * @ORM\Entity
 */
class UserAccess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="plugin_uid", type="string", length=255)
     */
    private $pluginUid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="access", type="smallint")
     */
    private $access;

    /**
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * Set pluginUid
     *
     * @param string $pluginUid
     *
     * @return UserAccess
     */
    public function setPluginUid($pluginUid)
    {
        $this->pluginUid = $pluginUid;

        return $this;
    }

    /**
     * Get pluginUid
     *
     * @return string
     */
    public function getPluginUid()
    {
        return $this->pluginUid;
    }

    /**
     * Set access
     *
     * @param boolean $access
     *
     * @return UserAccess
     */
    public function setAccess($access)
    {
        $this->access = $access;

        return $this;
    }

    /**
     * Get access
     *
     * @return boolean
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * @param User $user
     * @return UserAccess
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
