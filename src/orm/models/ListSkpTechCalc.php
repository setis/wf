<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use WF\Task\Model\TicketCalcInterface;

/**
 * ListSkpTechCalc - skp technician wage
 *
 * @ORM\Table(name="list_skp_tech_calc")
 * @ORM\Entity
 */
class ListSkpTechCalc implements TicketCalcInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="quant", type="float", nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="minval", type="decimal", precision=10, scale=2, nullable=true, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $minval;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="calc_date", type="datetime", nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $calcDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cr_date", type="datetime", nullable=false, unique=false)
     * @Serializer\Groups({"default"})
     */
    private $crDate;

    /**
     * @var \models\ListSkpPartnerWorkTypes
     *
     * @ORM\ManyToOne(targetEntity="models\ListSkpPartnerWorkTypes")
     * @ORM\JoinColumn(name="wt_id", referencedColumnName="id", onDelete="SET NULL")
     * @Serializer\Groups({"default"})
     */
    private $work;


    /**
     * @var \models\Task
     *
     * @ORM\ManyToOne(targetEntity="models\Task", inversedBy="skpTechCalc")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Groups({"default"})
     */
    private $task;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     * @Serializer\Groups({"default"})
     */
    private $user;

    /**
     * @var \models\User
     *
     * @ORM\ManyToOne(targetEntity="models\User")
     * @ORM\JoinColumn(name="tech_id", referencedColumnName="id", onDelete="SET NULL")
     * @Serializer\Groups({"default"})
     */
    private $technician;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return ListSkpTechCalc
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get minval
     *
     * @return string
     */
    public function getMinval()
    {
        return $this->minval;
    }

    /**
     * Set minval
     *
     * @param string $minval
     *
     * @return ListSkpTechCalc
     */
    public function setMinval($minval)
    {
        $this->minval = $minval;

        return $this;
    }

    /**
     * Get calcDate
     *
     * @return \DateTime
     */
    public function getCalcDate()
    {
        return $this->calcDate;
    }

    /**
     * Set calcDate
     *
     * @param \DateTime $calcDate
     *
     * @return ListSkpTechCalc
     */
    public function setCalcDate($calcDate)
    {
        $this->calcDate = $calcDate;

        return $this;
    }

    /**
     * Get crDate
     *
     * @return \DateTime
     */
    public function getCrDate()
    {
        return $this->crDate;
    }

    /**
     * Set crDate
     *
     * @param \DateTime $crDate
     *
     * @return ListSkpTechCalc
     */
    public function setCrDate($crDate)
    {
        $this->crDate = $crDate;

        return $this;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     * @return ListSkpTechCalc
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return ListSkpPartnerWorkTypes
     */
    public function getWork()
    {
        return $this->work;
    }

    /**
     * @param ListSkpPartnerWorkTypes $work
     * @return ListSkpTechCalc
     */
    public function setWork(ListSkpPartnerWorkTypes $work)
    {
        $this->work = $work;
        return $this;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param Task $task
     * @return ListSkpTechCalc
     */
    public function setTask(Task $task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return ListSkpTechCalc
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     * @param User $technician
     * @return ListSkpTechCalc
     */
    public function setTechnician(User $technician)
    {
        $this->technician = $technician;
        return $this;
    }


}

