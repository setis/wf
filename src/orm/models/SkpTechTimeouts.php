<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * SkpTechTimeouts
 *
 * @ORM\Table(name="skp_tech_timeouts")
 * @ORM\Entity
 */
class SkpTechTimeouts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tech_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $techId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rdate", type="date", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $rdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $count;


    /**
     * Set techId
     *
     * @param integer $techId
     *
     * @return SkpTechTimeouts
     */
    public function setTechId($techId)
    {
        $this->techId = $techId;

        return $this;
    }

    /**
     * Get techId
     *
     * @return integer
     */
    public function getTechId()
    {
        return $this->techId;
    }

    /**
     * Set rdate
     *
     * @param \DateTime $rdate
     *
     * @return SkpTechTimeouts
     */
    public function setRdate($rdate)
    {
        $this->rdate = $rdate;

        return $this;
    }

    /**
     * Get rdate
     *
     * @return \DateTime
     */
    public function getRdate()
    {
        return $this->rdate;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return SkpTechTimeouts
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }
}

