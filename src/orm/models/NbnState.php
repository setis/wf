<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * NbnState
 *
 * @ORM\Table(name="nbn_state")
 * @ORM\Entity()
 */
class NbnState
{
    const TIMEOUT = 600;
    /**
     * @var integer
     *
     * @ORM\Column(name="lastcheck", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $lastcheck;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $status;


    /**
     * Set lastcheck
     *
     * @param integer $lastcheck
     *
     * @return NbnState
     */
    public function setLastcheck($lastcheck)
    {
        $this->lastcheck = $lastcheck;

        return $this;
    }

    /**
     * Get lastcheck
     *
     * @return integer
     */
    public function getLastcheck()
    {
        return $this->lastcheck;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return NbnState
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
}

