<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161207080106 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE service_ticket_extras SET phone_tarif_id = null WHERE NOT EXISTS( SELECT * from tarifs where id = phone_tarif_id);');
        $this->addSql('ALTER TABLE service_ticket_extras ADD CONSTRAINT FK_8C66B598FD3B1955 FOREIGN KEY (phone_tarif_id) REFERENCES tarifs (id) ON DELETE SET NULL');

        $this->addSql('UPDATE service_ticket_extras SET internet_plus_tv_tarif_id = null WHERE NOT EXISTS( SELECT * from tarifs where id = internet_plus_tv_tarif_id);');
        $this->addSql('ALTER TABLE service_ticket_extras ADD CONSTRAINT FK_8C66B598882045EF FOREIGN KEY (internet_plus_tv_tarif_id) REFERENCES tarifs (id) ON DELETE SET NULL');

        $this->addSql('UPDATE service_ticket_extras SET internet_tarif_id = null WHERE NOT EXISTS( SELECT * from tarifs where id = internet_tarif_id);');
        $this->addSql('ALTER TABLE service_ticket_extras ADD CONSTRAINT FK_8C66B5987E7E351F FOREIGN KEY (internet_tarif_id) REFERENCES tarifs (id) ON DELETE SET NULL');

        $this->addSql('UPDATE service_ticket_extras SET ctv_tarif_id = null WHERE NOT EXISTS( SELECT * from tarifs where id = ctv_tarif_id);');
        $this->addSql('ALTER TABLE service_ticket_extras ADD CONSTRAINT FK_8C66B5986C00F7AE FOREIGN KEY (ctv_tarif_id) REFERENCES tarifs (id) ON DELETE SET NULL');

        $this->addSql('CREATE INDEX IDX_8C66B598FD3B1955 ON service_ticket_extras (phone_tarif_id)');
        $this->addSql('CREATE INDEX IDX_8C66B598882045EF ON service_ticket_extras (internet_plus_tv_tarif_id)');
        $this->addSql('CREATE INDEX IDX_8C66B5987E7E351F ON service_ticket_extras (internet_tarif_id)');
        $this->addSql('CREATE INDEX IDX_8C66B5986C00F7AE ON service_ticket_extras (ctv_tarif_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_ticket_extras DROP FOREIGN KEY FK_8C66B598FD3B1955');
        $this->addSql('ALTER TABLE service_ticket_extras DROP FOREIGN KEY FK_8C66B598882045EF');
        $this->addSql('ALTER TABLE service_ticket_extras DROP FOREIGN KEY FK_8C66B5987E7E351F');
        $this->addSql('ALTER TABLE service_ticket_extras DROP FOREIGN KEY FK_8C66B5986C00F7AE');
        $this->addSql('DROP INDEX IDX_8C66B598FD3B1955 ON service_ticket_extras');
        $this->addSql('DROP INDEX IDX_8C66B598882045EF ON service_ticket_extras');
        $this->addSql('DROP INDEX IDX_8C66B5987E7E351F ON service_ticket_extras');
        $this->addSql('DROP INDEX IDX_8C66B5986C00F7AE ON service_ticket_extras');
    }
}
