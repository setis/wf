<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170118032914 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users DROP gcm_id');
        $this->addSql('UPDATE task_status SET is_default=null WHERE plugin_uid=\'tm_module\' and is_default=1 and tag=\'think\'');
        $this->addSql('ALTER TABLE task_status CHANGE is_default is_default TINYINT(1) DEFAULT NULL');
        $this->addSql('UPDATE task_status SET is_default = NULL WHERE is_default = 0;');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_40A9E1CF3378259FF5628617 ON task_status (plugin_uid, is_default)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users ADD gcm_id VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci');
        $this->addSql('DROP INDEX UNIQ_40A9E1CF3378259FF5628617 ON task_status');
        $this->addSql('ALTER TABLE task_status CHANGE is_default is_default TINYINT(1) NOT NULL');
    }
}
