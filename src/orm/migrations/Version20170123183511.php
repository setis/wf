<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170123183511 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE list_lkuser_wtypes DROP PRIMARY KEY');
        $this->addSql('DELETE u FROM list_lkuser_wtypes u LEFT JOIN users ON (u.user_id = users.id) WHERE users.id IS NULL');
        $this->addSql('DELETE u FROM list_lkuser_wtypes u LEFT JOIN task_types ON (u.wtype_id = task_types.id) WHERE task_types.id IS NULL');
        $this->addSql('ALTER TABLE list_lkuser_wtypes ADD CONSTRAINT FK_7EC0890EA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE list_lkuser_wtypes ADD CONSTRAINT FK_7EC0890E9B97140D FOREIGN KEY (wtype_id) REFERENCES task_types (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_7EC0890EA76ED395 ON list_lkuser_wtypes (user_id)');
        $this->addSql('CREATE INDEX IDX_7EC0890E9B97140D ON list_lkuser_wtypes (wtype_id)');
        $this->addSql('ALTER TABLE list_lkuser_wtypes ADD PRIMARY KEY (user_id, wtype_id)');
        $this->addSql('ALTER TABLE tickets CHANGE agr_id agr_id INT DEFAULT NULL');
        $this->addSql('UPDATE tickets t LEFT JOIN list_contr_agr ON (t.agr_id = list_contr_agr.id) SET t.agr_id = NULL WHERE list_contr_agr.id IS NULL');
        $this->addSql('ALTER TABLE tickets ADD CONSTRAINT FK_54469DF414EA234 FOREIGN KEY (agr_id) REFERENCES list_contr_agr (id) ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_54469DF414EA234 ON tickets (agr_id)');
        $this->addSql('UPDATE users u LEFT JOIN list_contr ON (u.contr_user = list_contr.id) SET u.contr_user = NULL WHERE list_contr.id IS NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9D7B82244 FOREIGN KEY (contr_user) REFERENCES list_contr (id)');
        $this->addSql('CREATE INDEX IDX_1483A5E9D7B82244 ON users (contr_user)');
    }

    public function down(Schema $schema)
    {
        // No down migration
    }
}
