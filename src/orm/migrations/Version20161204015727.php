<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161204015727 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agents MODIFY contr_id INT(11) NOT NULL');
        $this->addSql('ALTER TABLE agents DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE agents ADD id INT FIRST');

        $this->addSql('ALTER TABLE agents 
                CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT,
                DROP INDEX UNIQ_9596AB6EA76ED395,
                ADD UNIQUE INDEX UNIQ_9596AB6EA76ED3952022182D108734B124890B2B (user_id ASC, contr_id ASC, work_type_id ASC, agreement_id ASC),
                ADD PRIMARY KEY (id);');

        $this->addSql('DELETE a FROM agents a LEFT JOIN users u ON u.id = a.user_id WHERE u.id IS NULL;');
        $this->addSql('DELETE a FROM agents a LEFT JOIN list_contr c ON c.id = a.contr_id WHERE c.id IS NULL;');
        $this->addSql('DELETE a FROM agents a LEFT JOIN task_types t ON t.id = a.work_type_id WHERE t.id IS NULL;');
        $this->addSql('DELETE a FROM agents a LEFT JOIN list_contr_agr l ON l.id = a.agreement_id WHERE l.id IS NULL;');

        $this->addSql('ALTER TABLE agents CHANGE user_id user_id INT, CHANGE work_type_id work_type_id INT , CHANGE agreement_id agreement_id INT, CHANGE contr_id contr_id INT ');

        $this->addSql('ALTER TABLE agents ADD CONSTRAINT FK_9596AB6E2022182D FOREIGN KEY (contr_id) REFERENCES list_contr (id) ON DELETE CASCADE ');
        $this->addSql('ALTER TABLE agents ADD CONSTRAINT FK_9596AB6EA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ');

        $this->addSql('ALTER TABLE agents ADD CONSTRAINT FK_9596AB6E108734B1 FOREIGN KEY (work_type_id) REFERENCES task_types (id) ON DELETE SET NULL ');
        $this->addSql('ALTER TABLE agents ADD CONSTRAINT FK_9596AB6E24890B2B FOREIGN KEY (agreement_id) REFERENCES list_contr_agr (id) ON DELETE SET NULL ');

        $this->addSql('CREATE INDEX IDX_9596AB6E108734B1 ON agents (work_type_id)');
        $this->addSql('CREATE INDEX IDX_9596AB6E24890B2B ON agents (agreement_id)');
        $this->addSql('CREATE INDEX IDX_9596AB6E2022182D ON agents (contr_id)');

        //=============================================

        $this->addSql('ALTER TABLE link_agr_wtypes DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE link_agr_wtypes ADD id INT AUTO_INCREMENT PRIMARY KEY NOT NULL FIRST');

        $this->addSql('ALTER TABLE link_agr_wtypes CHANGE agr_id agr_id INT DEFAULT NULL , CHANGE wtype_id wtype_id INT DEFAULT NULL , CHANGE is_qc is_qc TINYINT(1) NOT NULL');

        $this->addSql('DELETE l FROM link_agr_wtypes l LEFT JOIN task_types t ON t.id = l.wtype_id WHERE t.id IS NULL;');
        $this->addSql('DELETE l FROM link_agr_wtypes l LEFT JOIN list_contr_agr a ON a.id = l.agr_id WHERE a.id IS NULL;');

        $this->addSql('ALTER TABLE link_agr_wtypes ADD CONSTRAINT FK_FA67479B97140D FOREIGN KEY (wtype_id) REFERENCES task_types (id) ON DELETE CASCADE ');
        $this->addSql('ALTER TABLE link_agr_wtypes ADD CONSTRAINT FK_FA674714EA234 FOREIGN KEY (agr_id) REFERENCES list_contr_agr (id) ON DELETE CASCADE ');

        $this->addSql('CREATE INDEX IDX_FA67479B97140D ON link_agr_wtypes (wtype_id)');
        $this->addSql('CREATE INDEX IDX_FA674714EA234 ON link_agr_wtypes (agr_id)');

        //=============================================
        $this->addSql('DELETE a FROM list_contr_agr a LEFT JOIN list_contr c ON c.id = a.contr_id WHERE c.id IS NULL;');

        $this->addSql('ALTER TABLE list_contr_agr CHANGE contr_id contr_id INT DEFAULT NULL');

        $this->addSql('ALTER TABLE list_contr_agr ADD CONSTRAINT FK_41048FE02022182D FOREIGN KEY (contr_id) REFERENCES list_contr (id) ON DELETE RESTRICT ');
        $this->addSql('ALTER TABLE list_contr_agr ADD CONSTRAINT FK_41048FE06BF700BD FOREIGN KEY (status_id) REFERENCES task_status (id) ON DELETE SET NULL ');

        $this->addSql('CREATE INDEX IDX_41048FE02022182D ON list_contr_agr (contr_id)');
        $this->addSql('CREATE INDEX IDX_41048FE06BF700BD ON list_contr_agr (status_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(true, 'This migration can\'t be safely rolled back.');

        $this->addSql('ALTER TABLE agents DROP FOREIGN KEY FK_9596AB6E2022182D');
        $this->addSql('ALTER TABLE agents DROP FOREIGN KEY FK_9596AB6EA76ED395');
        $this->addSql('ALTER TABLE agents DROP FOREIGN KEY FK_9596AB6E108734B1');
        $this->addSql('ALTER TABLE agents DROP FOREIGN KEY FK_9596AB6E24890B2B');
        $this->addSql('DROP INDEX IDX_9596AB6E108734B1 ON agents');
        $this->addSql('DROP INDEX IDX_9596AB6E24890B2B ON agents');
        $this->addSql('ALTER TABLE agents CHANGE user_id user_id INT NOT NULL, CHANGE work_type_id work_type_id INT NOT NULL, CHANGE agreement_id agreement_id INT NOT NULL');
        $this->addSql('ALTER TABLE link_agr_wtypes DROP FOREIGN KEY FK_FA67479B97140D');
        $this->addSql('ALTER TABLE link_agr_wtypes DROP FOREIGN KEY FK_FA674714EA234');
        $this->addSql('DROP INDEX IDX_FA67479B97140D ON link_agr_wtypes');
        $this->addSql('DROP INDEX IDX_FA674714EA234 ON link_agr_wtypes');
        $this->addSql('ALTER TABLE list_contr_agr DROP FOREIGN KEY FK_41048FE02022182D');
        $this->addSql('ALTER TABLE list_contr_agr DROP FOREIGN KEY FK_41048FE06BF700BD');
        $this->addSql('DROP INDEX IDX_41048FE02022182D ON list_contr_agr');
        $this->addSql('DROP INDEX IDX_41048FE06BF700BD ON list_contr_agr');
        $this->addSql('ALTER TABLE list_contr_agr CHANGE contr_id contr_id INT NOT NULL');
    }
}
