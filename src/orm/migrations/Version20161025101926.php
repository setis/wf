<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161025101926 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users ADD is_access_template TINYINT(1) NOT NULL, CHANGE otdel_id otdel_id INT DEFAULT NULL');
        $this->addSql('DELETE e FROM user_access e LEFT JOIN users on (users.id = e.user_id) WHERE users.id IS NULL');
        $this->addSql('CREATE INDEX IDX_633B3069A76ED395 ON user_access (user_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX IDX_633B3069A76ED395 ON user_access');
        $this->addSql('ALTER TABLE users DROP is_access_template, CHANGE otdel_id otdel_id INT NOT NULL');
    }
}
