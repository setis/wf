<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161013143308 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("INSERT INTO `cron_job` (`name`,`command`,`schedule`,`description`,`enabled`) 
              VALUES 
              (     'swiftmailer_spooler_send', 
                    'swiftmailer:spool:send --message-limit=10 --time-limit=5 --recover-timeout=90 --mailer=file_spooler', 
                    '* * * * *', 
                    'Sends email', 
                    1
              ),
              (     'wf:task:notify-gp-close-time-expired', 
                    'wf:task:notify-gp-close-time-expired', 
                    '*/5 * * * *', 
                    'Событие о времени закрытия ГП истекло',
                    1
              ),
              (     'wf:task:notify-gp-not-closed-in-time', 
                    'wf:task:notify-gp-not-closed-in-time', 
                    '*/5 * * * *', 
                    'Событие о том что ГП не закрыто впредпологаемое закрытие',
                    1
              ),
              (     'wf:task:notify-gp-will-be-closed', 
                    'wf:task:notify-gp-will-be-closed', 
                    '*/5 * * * *', 
                    'Событие о уведомление ауповцев о скором (60мин) закрытии ГП',
                    1
              ),
              (     'checkdelayed_qc', 
                    'wf:script:run checkdelayed_qc', 
                    '30 * * * *', 
                    'Автоматическая установка статуса обзвона Новый при наступлении даты Отложено',
                    1
              ),
              (     'skp_debt', 
                    'wf:script:run skp_debt', 
                    '0,30 * * * *', 
                    'Определение задолженности техников', 
                    1
              ),
              (     'wf:task:notify-project-deadline', 
                    'wf:task:notify-project-deadline', 
                    '15 9 * * 1-5', 
                    'Определение истекших проектов', 
                    1
              ),
              (     'proj_reminder', 
                    'wf:script:run proj_reminder', 
                    '30 9 * * 1-5', 
                    'Напоминание о проектах через виджет напоминания о проектах', 
                    1
              ),
              (     'sys_setfailed', 
                    'wf:script:run sys_setfailed', 
                    '0 2 * * 1-5', 
                    'Отслеживание Невыполненных задач', 
                    1
              ),
              (     'reminders', 
                    'wf:script:run reminders', 
                    '30 9 * * *', 
                    'Переодические напоминания по проектам', 
                    1
              ),
              (     'sys_set_to_report', 
                    'wf:script:run sys_set_to_report', 
                    '*/5 * * * *', 
                    'Установка статуса Сдача отчета для заявок на Подключение', 
                    1
              ),
              (     'slack:sync', 
                    'slack:sync', 
                    '*/30 * * * *', 
                    'Синхронизация пользователей и каналов слак', 
                    1
              ),
              (     'system:notify:tech-day-not-started', 
                    'system:notify:tech-day-not-started', 
                    '*/30 * * * *', 
                    'Отправка уведоления технику если он не начала день', 
                    1
              ),
              (     'system:notify:schedule-task-time', 
                    'system:notify:schedule-task-time', 
                    '*/30 * * * *', 
                    'Отправляет уведомление если задача начнётся в ближайщие 30 минут', 
                    1
              ),
              (     'wf:task:notify-gp-no-address -m 20', 
                    'wf:task:notify-gp-no-address -m 20', 
                    '*/30 * * * *', 
                    'Уведомление ауповцев о том что ГП без адреса (20мин)', 
                    1
              ),
              (     'wf:tech:notify-money-limits', 
                    'wf:tech:notify-money-limits', 
                    '*/30 9 * * *', 
                    'Уведомление о привыщенни денежного лимита техником', 
                    1
              ),
              (     'wf:task:notify-task-no-reaction-timeout', 
                    'wf:task:notify-task-no-reaction-timeout', 
                    '*/30 * * * *', 
                    'Уведомление о привыщенни денежного лимита техником', 
                    1
              ),
              (     'wf:task:notify-project-delay-end', 
                    'wf:task:notify-project-delay-end', 
                    '*/30 9 * * *', 
                    'Уведомление о привыщенни денежного лимита техником', 
                    1
              ),
              (     'wf:task:notify-bill-delay-end', 
                    'wf:task:notify-bill-delay-end', 
                    '*/30 9 * * *', 
                    'Уведомления о возобновлении счёт', 
                    1
              ),
              (     'wf:task:notify-bill-payment-deadline', 
                    'wf:task:notify-bill-payment-deadline', 
                    '*/30 9 * * *', 
                    'Наступила дата оплаты по счёту',
                    1
              )
              ;");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
