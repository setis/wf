<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160919200109 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tmc_item_properties ADD reserved_by_move_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tmc_item_properties ADD CONSTRAINT FK_D4A3DBFE3B715810 FOREIGN KEY (reserved_by_move_id) REFERENCES tmc_move (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_D4A3DBFE3B715810 ON tmc_item_properties (reserved_by_move_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tmc_item_properties DROP FOREIGN KEY FK_D4A3DBFE3B715810');
        $this->addSql('DROP INDEX IDX_D4A3DBFE3B715810 ON tmc_item_properties');
        $this->addSql('ALTER TABLE tmc_item_properties DROP reserved_by_move_id');
    }
}
