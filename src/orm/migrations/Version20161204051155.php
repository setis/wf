<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161204051155 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E941085FAE');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E95E8978AE');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E941085FAE FOREIGN KEY (pos_id) REFERENCES user_pos (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E95E8978AE FOREIGN KEY (otdel_id) REFERENCES user_otdels (id) ON DELETE SET NULL');

        $this->addSql('ALTER TABLE link_sc_chiefs DROP FOREIGN KEY FK_93101951A76ED395');
        $this->addSql('ALTER TABLE link_sc_chiefs DROP FOREIGN KEY FK_93101951C8C329CD');
        $this->addSql('ALTER TABLE link_sc_chiefs ADD CONSTRAINT FK_93101951A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE link_sc_chiefs ADD CONSTRAINT FK_93101951C8C329CD FOREIGN KEY (sc_id) REFERENCES list_sc (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE link_sc_user DROP FOREIGN KEY FK_A57E6386A76ED395');
        $this->addSql('ALTER TABLE link_sc_user DROP FOREIGN KEY FK_A57E6386C8C329CD');
        $this->addSql('ALTER TABLE link_sc_user ADD CONSTRAINT FK_A57E6386A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE link_sc_user ADD CONSTRAINT FK_A57E6386C8C329CD FOREIGN KEY (sc_id) REFERENCES list_sc (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE link_warehouse_mol DROP FOREIGN KEY FK_6658C0CAA76ED395');
        $this->addSql('ALTER TABLE link_warehouse_mol DROP FOREIGN KEY FK_6658C0CAD028863F');
        $this->addSql('ALTER TABLE link_warehouse_mol ADD CONSTRAINT FK_6658C0CAA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE link_warehouse_mol ADD CONSTRAINT FK_6658C0CAD028863F FOREIGN KEY (listsc_id) REFERENCES list_sc (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE link_warehouse_supermol DROP FOREIGN KEY FK_EAC3EA73A76ED395');
        $this->addSql('ALTER TABLE link_warehouse_supermol DROP FOREIGN KEY FK_EAC3EA73D028863F');
        $this->addSql('ALTER TABLE link_warehouse_supermol ADD CONSTRAINT FK_EAC3EA73A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE link_warehouse_supermol ADD CONSTRAINT FK_EAC3EA73D028863F FOREIGN KEY (listsc_id) REFERENCES list_sc (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE link_warehouse_coordinator DROP FOREIGN KEY FK_CB745A9EA76ED395');
        $this->addSql('ALTER TABLE link_warehouse_coordinator DROP FOREIGN KEY FK_CB745A9ED028863F');
        $this->addSql('ALTER TABLE link_warehouse_coordinator ADD CONSTRAINT FK_CB745A9EA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE link_warehouse_coordinator ADD CONSTRAINT FK_CB745A9ED028863F FOREIGN KEY (listsc_id) REFERENCES list_sc (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE link_user_contr DROP FOREIGN KEY FK_79AC6CB02022182D');
        $this->addSql('ALTER TABLE link_user_contr DROP FOREIGN KEY FK_79AC6CB0A76ED395');
        $this->addSql('ALTER TABLE link_user_contr ADD CONSTRAINT FK_79AC6CB02022182D FOREIGN KEY (contr_id) REFERENCES list_contr (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE link_user_contr ADD CONSTRAINT FK_79AC6CB0A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE tmc_ticket DROP FOREIGN KEY FK_1C9F196164727BFC');
        $this->addSql('ALTER TABLE tmc_ticket DROP FOREIGN KEY FK_1C9F1961A76ED395');
        $this->addSql('ALTER TABLE tmc_ticket ADD CONSTRAINT FK_1C9F196164727BFC FOREIGN KEY (tech_id) REFERENCES users (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tmc_ticket ADD CONSTRAINT FK_1C9F1961A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL');

        $this->addSql('ALTER TABLE gfx DROP FOREIGN KEY FK_DFA03198E1AA14EE');
        $this->addSql('ALTER TABLE gfx ADD CONSTRAINT FK_DFA03198E1AA14EE FOREIGN KEY (empl_id) REFERENCES users (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE user_statistic DROP FOREIGN KEY FK_647BCB78A76ED395');
        $this->addSql('ALTER TABLE user_statistic ADD CONSTRAINT FK_647BCB78A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE bills_payments DROP FOREIGN KEY FK_F29D33E4A76ED395');
        $this->addSql('ALTER TABLE bills_payments ADD CONSTRAINT FK_F29D33E4A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL');

        $this->addSql('ALTER TABLE link_empl_tf DROP FOREIGN KEY FK_8DAE432BA76ED395');
        $this->addSql('ALTER TABLE link_empl_tf ADD CONSTRAINT FK_8DAE432BA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bills_payments DROP FOREIGN KEY FK_F29D33E4A76ED395');
        $this->addSql('ALTER TABLE bills_payments ADD CONSTRAINT FK_F29D33E4A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE gfx DROP FOREIGN KEY FK_DFA03198E1AA14EE');
        $this->addSql('ALTER TABLE gfx ADD CONSTRAINT FK_DFA03198E1AA14EE FOREIGN KEY (empl_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE link_sc_chiefs DROP FOREIGN KEY FK_93101951A76ED395');
        $this->addSql('ALTER TABLE link_sc_chiefs DROP FOREIGN KEY FK_93101951C8C329CD');
        $this->addSql('ALTER TABLE link_sc_chiefs ADD CONSTRAINT FK_93101951A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE link_sc_chiefs ADD CONSTRAINT FK_93101951C8C329CD FOREIGN KEY (sc_id) REFERENCES list_sc (id)');
        $this->addSql('ALTER TABLE link_sc_user DROP FOREIGN KEY FK_A57E6386A76ED395');
        $this->addSql('ALTER TABLE link_sc_user DROP FOREIGN KEY FK_A57E6386C8C329CD');
        $this->addSql('ALTER TABLE link_sc_user ADD CONSTRAINT FK_A57E6386A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE link_sc_user ADD CONSTRAINT FK_A57E6386C8C329CD FOREIGN KEY (sc_id) REFERENCES list_sc (id)');
        $this->addSql('ALTER TABLE link_user_contr DROP FOREIGN KEY FK_79AC6CB0A76ED395');
        $this->addSql('ALTER TABLE link_user_contr DROP FOREIGN KEY FK_79AC6CB02022182D');
        $this->addSql('ALTER TABLE link_user_contr ADD CONSTRAINT FK_79AC6CB0A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE link_user_contr ADD CONSTRAINT FK_79AC6CB02022182D FOREIGN KEY (contr_id) REFERENCES list_contr (id)');
        $this->addSql('ALTER TABLE link_warehouse_coordinator DROP FOREIGN KEY FK_CB745A9EA76ED395');
        $this->addSql('ALTER TABLE link_warehouse_coordinator DROP FOREIGN KEY FK_CB745A9ED028863F');
        $this->addSql('ALTER TABLE link_warehouse_coordinator ADD CONSTRAINT FK_CB745A9EA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE link_warehouse_coordinator ADD CONSTRAINT FK_CB745A9ED028863F FOREIGN KEY (listsc_id) REFERENCES list_sc (id)');
        $this->addSql('ALTER TABLE link_warehouse_mol DROP FOREIGN KEY FK_6658C0CAA76ED395');
        $this->addSql('ALTER TABLE link_warehouse_mol DROP FOREIGN KEY FK_6658C0CAD028863F');
        $this->addSql('ALTER TABLE link_warehouse_mol ADD CONSTRAINT FK_6658C0CAA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE link_warehouse_mol ADD CONSTRAINT FK_6658C0CAD028863F FOREIGN KEY (listsc_id) REFERENCES list_sc (id)');
        $this->addSql('ALTER TABLE link_warehouse_supermol DROP FOREIGN KEY FK_EAC3EA73A76ED395');
        $this->addSql('ALTER TABLE link_warehouse_supermol DROP FOREIGN KEY FK_EAC3EA73D028863F');
        $this->addSql('ALTER TABLE link_warehouse_supermol ADD CONSTRAINT FK_EAC3EA73A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE link_warehouse_supermol ADD CONSTRAINT FK_EAC3EA73D028863F FOREIGN KEY (listsc_id) REFERENCES list_sc (id)');
        $this->addSql('ALTER TABLE tmc_ticket DROP FOREIGN KEY FK_1C9F196164727BFC');
        $this->addSql('ALTER TABLE tmc_ticket DROP FOREIGN KEY FK_1C9F1961A76ED395');
        $this->addSql('ALTER TABLE tmc_ticket ADD CONSTRAINT FK_1C9F196164727BFC FOREIGN KEY (tech_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE tmc_ticket ADD CONSTRAINT FK_1C9F1961A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE user_statistic DROP FOREIGN KEY FK_647BCB78A76ED395');
        $this->addSql('ALTER TABLE user_statistic ADD CONSTRAINT FK_647BCB78A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E95E8978AE');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E941085FAE');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E95E8978AE FOREIGN KEY (otdel_id) REFERENCES user_otdels (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E941085FAE FOREIGN KEY (pos_id) REFERENCES user_pos (id)');
        $this->addSql('ALTER TABLE link_empl_tf DROP FOREIGN KEY FK_8DAE432BA76ED395');
        $this->addSql('ALTER TABLE link_empl_tf ADD CONSTRAINT FK_8DAE432BA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
    }
}
