<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161030232913 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE region (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME DEFAULT NULL, point_price DOUBLE PRECISION NOT NULL, kladr_code VARCHAR(80) DEFAULT NULL, INDEX IDX_F62F176979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE region_director (region_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_6D57D70298260155 (region_id), INDEX IDX_6D57D702A76ED395 (user_id), PRIMARY KEY(region_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE region ADD CONSTRAINT FK_F62F176979B1AD6 FOREIGN KEY (company_id) REFERENCES list_contr (id)');
        $this->addSql('ALTER TABLE region_director ADD CONSTRAINT FK_6D57D70298260155 FOREIGN KEY (region_id) REFERENCES region (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE region_director ADD CONSTRAINT FK_6D57D702A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE list_sc ADD region_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE list_sc ADD CONSTRAINT FK_13E779F398260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('CREATE INDEX IDX_13E779F398260155 ON list_sc (region_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE list_sc DROP FOREIGN KEY FK_13E779F398260155');
        $this->addSql('ALTER TABLE region_director DROP FOREIGN KEY FK_6D57D70298260155');
        $this->addSql('DROP TABLE region');
        $this->addSql('DROP TABLE region_director');
        $this->addSql('DROP INDEX IDX_13E779F398260155 ON list_sc');
        $this->addSql('ALTER TABLE list_sc DROP region_id');
    }
}
