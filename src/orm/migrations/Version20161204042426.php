<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161204042426 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE list_empl DROP FOREIGN KEY FK_43460B329B97140D');
        $this->addSql('ALTER TABLE list_empl DROP FOREIGN KEY FK_43460B32A76ED395');
        $this->addSql('ALTER TABLE list_empl ADD CONSTRAINT FK_43460B329B97140D FOREIGN KEY (wtype_id) REFERENCES task_types (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE list_empl ADD CONSTRAINT FK_43460B32A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE list_empl DROP FOREIGN KEY FK_43460B32A76ED395');
        $this->addSql('ALTER TABLE list_empl DROP FOREIGN KEY FK_43460B329B97140D');
        $this->addSql('ALTER TABLE list_empl ADD CONSTRAINT FK_43460B32A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE list_empl ADD CONSTRAINT FK_43460B329B97140D FOREIGN KEY (wtype_id) REFERENCES task_types (id)');
    }
}
