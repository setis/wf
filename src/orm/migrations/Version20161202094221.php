<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161202094221 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bug_reports CHANGE environment environment LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\'');
        $this->addSql('DELETE e FROM list_empl e LEFT JOIN task_types ON (e.wtype_id = task_types.id) WHERE task_types.id IS NULL');
        $this->addSql('ALTER TABLE list_empl ADD CONSTRAINT FK_43460B329B97140D FOREIGN KEY (wtype_id) REFERENCES task_types (id)');
        $this->addSql('CREATE INDEX IDX_43460B329B97140D ON list_empl (wtype_id)');
        $this->addSql('ALTER TABLE tmc_move DROP FOREIGN KEY FK_5D00A0AC3109A67E');
        $this->addSql('ALTER TABLE tmc_move ADD CONSTRAINT FK_5D00A0AC3109A67E FOREIGN KEY (prev_move_id) REFERENCES tmc_move (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_8DAE432BDA0BB4F3DA7D58DB ON link_empl_tf (time_start, time_end)');
        $this->addSql('CREATE INDEX IDX_DFA03198DA0BB4F3DA7D58DB ON gfx (time_start, time_end)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bug_reports CHANGE environment environment LONGBLOB NOT NULL');
        $this->addSql('DROP INDEX IDX_DFA03198DA0BB4F3DA7D58DB ON gfx');
        $this->addSql('DROP INDEX IDX_8DAE432BDA0BB4F3DA7D58DB ON link_empl_tf');
        $this->addSql('ALTER TABLE list_empl DROP FOREIGN KEY FK_43460B329B97140D');
        $this->addSql('DROP INDEX IDX_43460B329B97140D ON list_empl');
        $this->addSql('ALTER TABLE tmc_move DROP FOREIGN KEY FK_5D00A0AC3109A67E');
        $this->addSql('ALTER TABLE tmc_move ADD CONSTRAINT FK_5D00A0AC3109A67E FOREIGN KEY (prev_move_id) REFERENCES tmc_move (id)');
    }
}
