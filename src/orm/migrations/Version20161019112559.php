<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161019112559 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE task_comments DROP FOREIGN KEY FK_1F5E7C666BF700BD;');
        $this->addSql('ALTER TABLE task_comments DROP INDEX IDX_1F5E7C666BF700BD;');
        $this->addSql('ALTER TABLE tasks DROP FOREIGN KEY FK_505865976BF700BD;');
        $this->addSql('ALTER TABLE tasks DROP INDEX IDX_505865976BF700BD;');
            $this->addSql('ALTER TABLE task_status CHANGE id id INT NOT NULL');
            $this->addSql('CREATE UNIQUE INDEX UNIQ_40A9E1CFBF396750 ON task_status (id)');
        $this->addSql('ALTER TABLE task_comments ADD CONSTRAINT FK_1F5E7C666BF700BD FOREIGN KEY (status_id) REFERENCES task_status (id) ON DELETE NO ACTION;');
        $this->addSql('CREATE INDEX IDX_1F5E7C666BF700BD ON task_comments (status_id);');
        $this->addSql('ALTER TABLE tasks ADD CONSTRAINT FK_505865976BF700BD FOREIGN KEY (status_id) REFERENCES task_status (id);');
        $this->addSql('CREATE INDEX IDX_505865976BF700BD ON tasks (status_id);');
        $this->addSql('ALTER TABLE list_contr CHANGE contr_desc contr_desc MEDIUMTEXT DEFAULT NULL, CHANGE contact contact MEDIUMTEXT DEFAULT NULL, CHANGE official_title official_title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_pos CHANGE title title VARCHAR(250) NOT NULL, CHANGE le le DATETIME NOT NULL');
        $this->addSql('UPDATE user_pos u SET user_id = NULL WHERE user_id = 0');
        $this->addSql('UPDATE user_pos t0 LEFT JOIN users t1 ON (t0.user_id = t1.id) SET t0.user_id = NULL WHERE t1.id IS NULL');
        $this->addSql('ALTER TABLE user_pos ADD CONSTRAINT FK_6BCF8727A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_6BCF8727A76ED395 ON user_pos (user_id)');
        $this->addSql('ALTER TABLE qc_qlist CHANGE user_id user_id INT DEFAULT NULL, CHANGE contr_id contr_id INT DEFAULT NULL');
        $this->addSql('UPDATE qc_qlist SET contr_id = NULL WHERE contr_id = 0');
        $this->addSql('UPDATE qc_qlist SET user_id = NULL WHERE user_id = 0');
        $this->addSql('ALTER TABLE qc_qlist ADD CONSTRAINT FK_E3604C46A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE qc_qlist ADD CONSTRAINT FK_E3604C462022182D FOREIGN KEY (contr_id) REFERENCES list_contr (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_E3604C46A76ED395 ON qc_qlist (user_id)');
        $this->addSql('CREATE INDEX IDX_E3604C462022182D ON qc_qlist (contr_id)');
        $this->addSql('ALTER TABLE list_sc CHANGE chief chief INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_otdels CHANGE `order` `order` INT DEFAULT NULL');
        $this->addSql('UPDATE user_otdels SET user_id = NULL WHERE user_id = 0');
        $this->addSql('ALTER TABLE user_otdels ADD CONSTRAINT FK_DB7C2CACA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_DB7C2CACA76ED395 ON user_otdels (user_id)');
        $this->addSql('ALTER TABLE task_types CHANGE `desc` `desc` MEDIUMTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_pos CHANGE le le DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user_access ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT FIRST, DROP PRIMARY KEY, ADD PRIMARY KEY (`id`)');

        $this->addSql('ALTER TABLE user_access CHANGE user_id user_id INT DEFAULT NULL, CHANGE access access SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE user_access ADD CONSTRAINT FK_633B3069A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_633B3069A76ED3953378259F ON user_access (user_id, plugin_uid)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE list_contr CHANGE contr_desc contr_desc MEDIUMTEXT NOT NULL COLLATE utf8_general_ci, CHANGE contact contact MEDIUMTEXT NOT NULL COLLATE utf8_general_ci, CHANGE official_title official_title VARCHAR(255) NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE list_ods CHANGE fio fio VARCHAR(255) NOT NULL COLLATE utf8_general_ci, CHANGE tel tel VARCHAR(20) NOT NULL COLLATE utf8_general_ci, CHANGE address address VARCHAR(255) NOT NULL COLLATE utf8_general_ci, CHANGE comment comment MEDIUMTEXT NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE list_sc CHANGE chief chief INT NOT NULL');
        $this->addSql('ALTER TABLE qc_qlist DROP FOREIGN KEY FK_E3604C46A76ED395');
        $this->addSql('ALTER TABLE qc_qlist DROP FOREIGN KEY FK_E3604C462022182D');
        $this->addSql('DROP INDEX IDX_E3604C46A76ED395 ON qc_qlist');
        $this->addSql('DROP INDEX IDX_E3604C462022182D ON qc_qlist');
        $this->addSql('ALTER TABLE qc_qlist CHANGE user_id user_id INT NOT NULL, CHANGE contr_id contr_id INT NOT NULL');
        $this->addSql('DROP INDEX UNIQ_40A9E1CFBF396750 ON task_status');
        $this->addSql('ALTER TABLE task_status CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE task_types CHANGE `desc` `desc` MEDIUMTEXT NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE user_otdels DROP FOREIGN KEY FK_DB7C2CACA76ED395');
        $this->addSql('DROP INDEX IDX_DB7C2CACA76ED395 ON user_otdels');
        $this->addSql('ALTER TABLE user_otdels CHANGE `order` `order` INT NOT NULL');
        $this->addSql('ALTER TABLE user_pos DROP FOREIGN KEY FK_6BCF8727A76ED395');
        $this->addSql('DROP INDEX IDX_6BCF8727A76ED395 ON user_pos');
        $this->addSql('ALTER TABLE user_pos CHANGE title title VARCHAR(250) DEFAULT NULL COLLATE utf8_general_ci, CHANGE le le DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user_pos CHANGE le le DATETIME NOT NULL');

        $this->addSql('ALTER TABLE user_access DROP FOREIGN KEY FK_633B3069A76ED395');
        $this->addSql('DROP INDEX UNIQ_633B3069A76ED3953378259F ON user_access');
        $this->addSql('ALTER TABLE user_access CHANGE user_id user_id INT NOT NULL, CHANGE access access TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE user_access MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE user_access DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE user_access DROP id');
        $this->addSql('ALTER TABLE user_access ADD PRIMARY KEY (user_id, plugin_uid)');
    }
}
