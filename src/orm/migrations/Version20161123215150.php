<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161123215150 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE link_empl_tf DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE link_empl_tf ADD COLUMN id INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);');
        $this->addSql('ALTER TABLE link_empl_tf ADD time_start DATETIME DEFAULT NULL, ADD time_end DATETIME DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE link_empl_tf ADD CONSTRAINT FK_8DAE432BA76ED395 FOREIGN KEY (user_id) REFERENCES users (id);');
        $this->addSql('CREATE INDEX IDX_8DAE432BA76ED395 ON link_empl_tf (user_id);');
        $this->addSql('UPDATE link_empl_tf SET time_start = sched_date + INTERVAL starttime MINUTE, time_end = sched_date + INTERVAL endtime MINUTE;');
        $this->addSql('ALTER TABLE gfx ADD time_start DATETIME NOT NULL, ADD time_end DATETIME NOT NULL;');
        $this->addSql('ALTER TABLE gfx CHANGE time_start time_start DATETIME DEFAULT NULL, CHANGE time_end time_end DATETIME DEFAULT NULL;');
        $this->addSql('UPDATE gfx g JOIN tickets t ON t.task_id = g.task_id JOIN task_types tt ON tt.id = t.task_type SET g.time_start = g.c_date + INTERVAL g.startTime MINUTE, g.time_end = g.c_date + INTERVAL (g.startTime + tt.duration) MINUTE;');
        $this->addSql('ALTER TABLE task_types ADD default_duration CHAR(20) DEFAULT NULL COMMENT \'(DC2Type:dateinterval)\';');
        $this->addSql('UPDATE task_types SET default_duration = CONCAT(\'PT\', duration, \'M\') WHERE duration > 0;');
        $this->addSql('ALTER TABLE gfx ADD created_by INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE gfx ADD CONSTRAINT FK_DFA03198DE12AB56 FOREIGN KEY (created_by) REFERENCES users (id);');
        $this->addSql('CREATE INDEX IDX_DFA03198DE12AB56 ON gfx (created_by);');
        $this->addSql('DELETE le FROM list_empl le LEFT JOIN users ON (le.user_id = users.id) WHERE users.id IS NULL');
        $this->addSql('ALTER TABLE list_empl ADD CONSTRAINT FK_43460B32A76ED395 FOREIGN KEY (user_id) REFERENCES users (id);');
        $this->addSql('CREATE INDEX IDX_43460B32A76ED395 ON list_empl (user_id);');
        $this->addSql('ALTER TABLE link_user_contr ADD CONSTRAINT FK_79AC6CB0A76ED395 FOREIGN KEY (user_id) REFERENCES users (id);');
        $this->addSql('CREATE INDEX IDX_79AC6CB0A76ED395 ON link_user_contr (user_id);');
        $this->addSql('DROP INDEX UNIQ_79AC6CB0A76ED3952022182D ON link_user_contr;');
        $this->addSql('ALTER TABLE link_user_contr DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE link_user_contr ADD CONSTRAINT FK_79AC6CB02022182D FOREIGN KEY (contr_id) REFERENCES list_contr (id);');
        $this->addSql('CREATE INDEX IDX_79AC6CB02022182D ON link_user_contr (contr_id);');
        $this->addSql('ALTER TABLE link_user_contr ADD PRIMARY KEY (user_id, contr_id);');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
       // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(true, 'This migration can\'t be safely rolled back.');


    }
}
