<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 26.01.17
 * Time: 13:29
 */

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20170126140000 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tasks ADD relevance_level INT DEFAULT NULL');

        $this->addSql("INSERT INTO `cron_job` (`name`,`command`,`schedule`,`description`,`enabled`) 
              VALUES 
              (     'gerp:task:update-relevance-level', 
                    'gerp:task:update-relevance-level', 
                    '*/5 * * * *', 
                    'Update tasks relevance level', 
                    1
              )");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM `cron_job` WHERE `name` = 'gerp:task:update-relevance-level'");
        $this->addSql("ALTER TABLE tasks DROP relevance_level");
    }
}