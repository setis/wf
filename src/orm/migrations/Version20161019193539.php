<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161019193539 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE jt FROM joinedtasks jt
                                LEFT JOIN
                            tasks t1 ON jt.task_id = t1.id
                                LEFT JOIN
                            tasks t2 ON jt.joined_task_id = t2.id 
                        WHERE
                            t1.id IS NULL OR t2.id IS NULL');
        $this->addSql('ALTER TABLE joinedtasks ADD CONSTRAINT FK_1020195F8DB60186 FOREIGN KEY (task_id) REFERENCES tasks (id)');
        $this->addSql('ALTER TABLE joinedtasks ADD CONSTRAINT FK_1020195FBB16E8B7 FOREIGN KEY (joined_task_id) REFERENCES tasks (id)');
        $this->addSql('CREATE INDEX IDX_1020195F8DB60186 ON joinedtasks (task_id)');
        $this->addSql('CREATE INDEX IDX_1020195FBB16E8B7 ON joinedtasks (joined_task_id)');
        $this->addSql('ALTER TABLE tickets CHANGE clnt_type clnt_type SMALLINT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE joinedtasks DROP FOREIGN KEY FK_1020195F8DB60186');
        $this->addSql('ALTER TABLE joinedtasks DROP FOREIGN KEY FK_1020195FBB16E8B7');
        $this->addSql('DROP INDEX IDX_1020195F8DB60186 ON joinedtasks');
        $this->addSql('DROP INDEX IDX_1020195FBB16E8B7 ON joinedtasks');
        $this->addSql('ALTER TABLE tickets CHANGE clnt_type clnt_type TINYINT(1) NOT NULL');
    }
}
