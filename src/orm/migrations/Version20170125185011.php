<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170125185011 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tasks CHANGE date_fn date_fn DATETIME DEFAULT NULL');
        $this->addSql('UPDATE tasks SET date_fn = null WHERE date_fn < now()');
        $this->addSql('DELETE FROM cron_job WHERE name = \'checkdelayed\'');

        $this->addSql("INSERT INTO `cron_job` (`name`,`command`,`schedule`,`description`,`enabled`) 
              VALUES 
              (     'gerp:task:check-delayed', 
                    'gerp:task:check-delayed', 
                    '*/10 * * * *', 
                    'Check delayed task and trigger event if delay time is occurred', 
                    1
              )");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM `cron_job` WHERE `name` = 'gerp:task:check-delayed'");

    }
}
