<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161125095904 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bug_reports (id VARCHAR(20) NOT NULL, user_id INT NOT NULL, executive VARCHAR(255) DEFAULT NULL, action LONGTEXT NOT NULL, failure LONGTEXT NOT NULL, wish LONGTEXT NOT NULL, done DATETIME DEFAULT NULL, name VARCHAR(200) NOT NULL, email VARCHAR(200) NOT NULL, phone VARCHAR(200) NOT NULL, date DATETIME DEFAULT NULL, url LONGTEXT NOT NULL, environment LONGBLOB NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE bug_reports');
    }
}
