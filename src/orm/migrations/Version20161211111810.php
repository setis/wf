<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161211111810 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE joinedtasks DROP FOREIGN KEY FK_1020195FBB16E8B7');
        $this->addSql('ALTER TABLE joinedtasks ADD CONSTRAINT FK_1020195FBB16E8B7 FOREIGN KEY (joined_task_id) REFERENCES tasks (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE joinedtasks DROP FOREIGN KEY FK_1020195FBB16E8B7');
        $this->addSql('ALTER TABLE joinedtasks ADD CONSTRAINT FK_1020195FBB16E8B7 FOREIGN KEY (joined_task_id) REFERENCES tasks (id)');
    }
}
