<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161005122546 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE list_ods ADD area POLYGON DEFAULT NULL COMMENT \'(DC2Type:polygon)\', CHANGE fio fio VARCHAR(255) NOT NULL, CHANGE tel tel VARCHAR(20) NOT NULL, CHANGE address address VARCHAR(255) NOT NULL, CHANGE comment comment MEDIUMTEXT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE list_ods DROP area, CHANGE fio fio VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, CHANGE tel tel VARCHAR(20) DEFAULT NULL COLLATE utf8_general_ci, CHANGE address address VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, CHANGE comment comment MEDIUMTEXT DEFAULT NULL COLLATE utf8_general_ci');
    }
}
