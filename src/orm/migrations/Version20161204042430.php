<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161204042430 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_sc_access MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE user_sc_access DROP FOREIGN KEY FK_1395D2ABA76ED395');
        $this->addSql('ALTER TABLE user_sc_access DROP FOREIGN KEY FK_1395D2ABC8C329CD');
        $this->addSql('DROP INDEX UNIQ_1395D2ABA76ED395C8C329CD ON user_sc_access');
        $this->addSql('ALTER TABLE user_sc_access DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE user_sc_access DROP id, CHANGE user_id user_id INT NOT NULL, CHANGE sc_id sc_id INT NOT NULL');
        $this->addSql('ALTER TABLE user_sc_access ADD CONSTRAINT FK_1395D2ABA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_sc_access ADD CONSTRAINT FK_1395D2ABC8C329CD FOREIGN KEY (sc_id) REFERENCES list_sc (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_sc_access ADD PRIMARY KEY (user_id, sc_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(true, 'This migration can\'t be safely rolled back.');
    }
}
