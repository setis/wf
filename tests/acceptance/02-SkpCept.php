<?php
use Step\Acceptance\Admin as AdminTester;

$I = new AdminTester($scenario);
$I->wantTo('perform actions and see result');

$I->wantToTest('Login with right credentials');
$I->loginAsAdmin();

$I->click('СКП');
$I->see('Заявки на обслуживание и ремонт');

$I->click('Добавить заявку');

$I->see('Новая заявка на обслуживание и ремонт');

$I->fillField('input[name=full_address]', 'Москва окская 21');
$I->fillField('input[name=pod]', '1');
$I->fillField('input[name=kv]', '1');
$I->fillField('input[name=clnt_phone1]', '79263333333');
$I->fillField('input[name=clnt_fio]', 'Вася пупкин');
$I->scrollTo('button[value="Сохранить заявку"]');
$I->click('button[value="Сохранить заявку"]');

$I->see('Просмотр заявки на обслуживание');
$I->seeInRepository(\models\Ticket::class, [
    'clntFio' => 'Вася пупкин'
]);
