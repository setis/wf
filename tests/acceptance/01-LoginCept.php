<?php
use Step\Acceptance\Admin as AdminTester;

$I = new AdminTester($scenario);

$I->wantTo('Try login with wrong credentials.');
$I->amOnPage('/login');
$I->fillField('_username', 'user_name');
$I->fillField('_password', 'user_password');
$I->click('button[type=submit]');
$I->see('Неверные учётные данные', 'div.alert');

$I->wantToTest('Login with right credentials');
$I->loginAsAdmin();
$I->seeInTitle('Рабочий стол');
