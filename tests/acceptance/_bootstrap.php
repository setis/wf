<?php
// Here you can initialize variables that will be available to your tests
$loader = require __DIR__.'/../../app/autoload.php';

$kernel = new AppKernel('test', true);
$kernel->boot();

wf::init();
