<?php
use Codeception\Actor;
use Codeception\Lib\Friend;
use Codeception\Util\Stub;
use Doctrine\Common\Collections\ArrayCollection;
use models\Department;
use models\Gfx;
use models\Gp;
use models\ListSc;
use models\Task;
use models\TaskStatus;
use models\Ticket;
use models\User;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
 */
class UnitTester extends Actor
{
    use _generated\UnitTesterActions;

    protected $faker;

    public function __construct(\Codeception\Scenario $scenario)
    {
        parent::__construct($scenario);
        $this->faker = \Faker\Factory::create('ru_RU');
    }

    public function createPartner($title)
    {
        return (new \models\ListContr())
            ->setContrTitle($title)
            ->setOfficialTitle($title)
            ->setContrType(true)
            ->setContrDesc('test')
            ->setContact('test');
    }

    public function createTaskType($title, $pluginUid = 'test')
    {
        return (new \models\TaskType())
            ->setTitle($title)
            ->setDesc('test')
            ->setPluginUid($pluginUid)
            ->setDuration(1);
    }

    public function createTaskSource()
    {
        $faker = \Faker\Factory::create('ru_RU');

        return (new \models\TaskSource())
            ->setTag('tag')
            ->setTitle($faker->title);
    }

    public function createTaskStatus($id, $plugin)
    {
        $faker = \Faker\Factory::create('ru_RU');

        return (new \models\TaskStatus($id))
            ->setPluginUid($plugin)
            ->setColor('#FFF')
            ->setName($faker->title);
    }

    public function createServiceCenter()
    {
        return (new \models\ListSc())
            ->setTitle($this->faker->company)
            ->setDesc($this->faker->text)
            ->setChief(0);
    }

    public function createJobPosition()
    {
        return (new \models\JobPosition())
            ->setTitle($this->faker->title);
    }

    /**
     *
     * @return User
     */
    public function createUser()
    {
        return (new User())
            ->setLogin($this->faker->userName)
            ->setFio($this->faker->name)
            ->setEmail($this->faker->email)
            ->setPass($this->faker->password)
            ->setPhones($this->faker->phoneNumber)
            ->setActive(true);
    }

    /**
     *
     * @return Department
     */
    public function createDepartment()
    {
        $faker = \Faker\Factory::create('ru_RU');

        return (new Department())
            ->setName($faker->uuid)
            ->setEmail($faker->companyEmail)
            ->setPhone($faker->phoneNumber)
            ->setOrder(0);
    }

    public function createBusinessUnit()
    {
        $faker = \Faker\Factory::create('ru_RU');

        return (new \models\BusinessUnit())
            ->setTitle($faker->title)
            ->setDescription($faker->text);
    }

    public function createBillType()
    {
        $faker = \Faker\Factory::create('ru_RU');

        return (new \models\BillType())
            ->setTitle($faker->title);
    }

    public function imagineSlackUser()
    {
        $faker = \Faker\Factory::create('ru_RU');

        return $slackUser = Stub::make(\models\SlackUser::class, [
            'id' => $faker->randomDigitNotNull,
            'name' => $faker->name,
            'email' => $faker->email,
        ]);
    }

    /**
     * @return Task
     */
    public function imagineAccidentTask()
    {
        $task = $this->imagineTask();

        $task = Stub::update($task, [
            'pluginUid' => Task::ACCIDENT,
            'ticket' => $this->imagineTicket($task->getId()),
            'status' => Stub::make(TaskStatus::class, [
                'id' => TaskStatus::STATUS_ACCIDENT_NEW, // accidents new
            ]),
        ]);

        return $task;
    }

    /**
     * @return Task
     */
    public function imagineTask()
    {
        $faker = \Faker\Factory::create('ru_RU');

        $user = $this->imagineTechnician();

        $dt = date_timestamp_set(new \DateTime(), 0);

        // disable base constructor
        $schedule = Stub::make(Gfx::class,
            [
                'id' => $faker->randomDigitNotNull,
                'getTechnician' => $user,
                'getStartDateTime' => $dt
            ]);

        $task_id = $faker->randomDigitNotNull;


        $task = Stub::make(Task::class, [
            'schedule' => $schedule,
            'id' => $task_id
        ]);

        return $task;
    }

    /**
     * @return User
     */
    public function imagineTechnician()
    {
        return Stub::update($this->imagineUser(), [
            'warehouses' => new ArrayCollection([$this->imagineServiceCenter()]),
            'slaveServiceCenters' => new ArrayCollection(),
        ]);
    }

    /**
     * @return User
     */
    public function imagineUser()
    {
        $faker = \Faker\Factory::create('ru_RU');

        return Stub::make(User::class, [
            'id' => $faker->randomDigitNotNull,
            'fio' => $faker->name,
            'email' => $faker->companyEmail,
            'login' => $faker->userName,
            'password' => $faker->password,
            'phones' => $faker->phoneNumber,
            'dolzn' => $faker->sentence($nbWords = 2, $variableNbWords = true),
            'getDepartment' => $this->imagineDepartment(),
        ]);
    }

    /**
     * @return Department
     */
    public function imagineDepartment()
    {
        $faker = \Faker\Factory::create('ru_RU');

        return Stub::make(Department::class, [
            'id' => $faker->randomDigitNotNull,
            'getName' => 'ИТ отдел',
            'getEmail' => $faker->companyEmail,
        ]);
    }

    public function imagineServiceCenter()
    {
        $faker = \Faker\Factory::create('ru_RU');

        $serviceCenter = Stub::make(ListSc::class, [
            'id' => $faker->randomDigitNotNull,
            'title' => $faker->company
        ]);

        return $serviceCenter;
    }

    public function imagineTicket($task_id)
    {
        $faker = \Faker\Factory::create('ru_RU');

        $ticket = Stub::make(Ticket::class, [
            'taskId' => $task_id,
            'getClntTel1' => $faker->phoneNumber,
            'getClntTel2' => $faker->phoneNumber,
            'serviceCenter' => $this->imagineServiceCenter(),
        ]);

        return $ticket;
    }

    /**
     * @return Task
     */
    public function imagineGlobalProblemTask()
    {
        $task = $this->imagineTask();

        $ticket = Stub::make(Gp::class, [
            'taskId' => $task->getId()
        ]);

        $task = Stub::update($task, [
            'pluginUid' => Task::GLOBAL_PROBLEM,
            'gp' => $ticket,
            'status' => Stub::make(TaskStatus::class, [
                'id' => TaskStatus::STATUS_GLOBAL_PROBLEM_NEW, // gp new
            ]),
        ]);

        return $task;
    }

    /**
     * @return Task
     */
    public function imagineConnectionTask()
    {
        $task = $this->imagineTask();

        $task = Stub::update($task, [
            'pluginUid' => Task::CONNECTION,
            'ticket' => $this->imagineTicket($task->getId()),
            'status' => Stub::make(TaskStatus::class, [
                'id' => TaskStatus::STATUS_CONNECTION_NEW, // connection new
            ]),
        ]);

        return $task;
    }

    /**
     * @return Task
     */
    public function imagineServiceTask()
    {
        $task = $this->imagineTask();

        $task = Stub::update($task, [
            'pluginUid' => Task::SERVICE,
            'ticket' => $this->imagineTicket($task->getId()),
            'status' => Stub::make(TaskStatus::class, [
                'id' => TaskStatus::STATUS_SERVICE_NEW, // accidents new
            ]),
        ]);

        return $task;
    }

    /**
     * @return User
     */
    public function imagineDispatcher()
    {
        return Stub::update($this->imagineUser(), [
            'warehouses' => new ArrayCollection(),
        ]);
    }

    /**
     * @return User
     */
    public function imagineChiefTechnician()
    {
        $faker = \Faker\Factory::create('ru_RU');

        $serviceCenter = $this->imagineServiceCenter();

        $chief = $this->imagineTechnician();
        $chief = Stub::update($chief, [
            'id' => $faker->randomDigitNotNull,
            'slaveServiceCenters' =>  new ArrayCollection([$serviceCenter]),
            'warehouses' => new ArrayCollection(),
        ]);

        Stub::update($serviceCenter, [
            'chiefs' => [$chief]
        ]);

        return $chief;
    }
}
