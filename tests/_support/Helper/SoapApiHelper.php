<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class SoapApiHelper extends \Codeception\Module
{
    /**
     * Cached WSDL files
     */
    static private $schemas = [];

    /**
     * Returns the location of the Wsdl file generated dynamically
     *
     * @param   string  $endpoint  The webservice url.
     * @param   string  $wsdlFile  Wsdl file url
     *
     * @return mixed
     */
    public function getSoapWsdlDynamically($endpoint, $wsdlFile)
    {
        // Gets cached WSDL static file from dynamic file
        if (!isset(self::$schemas[$endpoint]))
        {
            $wsdl = simplexml_load_file($wsdlFile);
            $schema = $wsdl['targetNamespace'];
            self::$schemas[$endpoint] = $schema;
        }

        return self::$schemas[$endpoint];
    }

}
