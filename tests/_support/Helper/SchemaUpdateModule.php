<?php

namespace Helper;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module as CodeceptionModule;
use Codeception\Module\Symfony;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Description of Module
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SchemaUpdateModule extends CodeceptionModule implements DependsOnModule
{
    private static $dbCleared = false;
    /**
     * @var Symfony
     */
    private $symfony;

    public function __construct(\Codeception\Lib\ModuleContainer $moduleContainer, $config = null)
    {
        parent::__construct($moduleContainer, $config);
    }

    public function _depends()
    {
        return [Symfony::class => 'Symfony module is required'];
    }

    public function _inject(Symfony $symfony = null)
    {
        $this->symfony = $symfony;
    }

    public function _initialize()
    {
        if (self::$dbCleared || null === $this->symfony->kernel) {
            return;
        }

        $app = new Application($this->symfony->kernel);
        $app->setAutoExit(false);

        if(!isset($_SERVER['CCRR'])) {
            $app->run(new StringInput('cache:clear'));
            $app->run(new StringInput('doctrine:database:drop --if-exists --force'));
            $app->run(new StringInput('doctrine:database:create --if-not-exists'));
            $app->run(new StringInput('doctrine:migrations:migrate -n'));
            $app->run(new StringInput('gerp:demo:seed'));
        }

        $process = new Process('chmod 777 -R var/');
        try {
            $process->mustRun();

            echo $process->getOutput();
        } catch (ProcessFailedException $e) {
            echo $e->getMessage();
        }

        self::$dbCleared = true;
    }

    /**
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function getContainer()
    {
        return $this->getModule('Symfony')->_getContainer();
    }

    /**
     * @param $entity
     * @param array $params
     * @param array $order
     * @return mixed
     */
    public function getFromRepository($entity, array $params = [], array $order = [])
    {
        // we need to store to database...
        $this->getModule('Symfony')->_getContainer()->get('doctrine.orm.entity_manager')->flush();
        return $this->getModule('Symfony')->_getContainer()->get('doctrine.orm.entity_manager')->getRepository($entity)
            ->findBy($params, $order);
    }
}
