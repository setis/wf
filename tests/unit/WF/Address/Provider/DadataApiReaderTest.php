<?php

namespace WF\Address\Provider;

use Doctrine\ORM\EntityManagerInterface;
use WF\Task\TaskManager;

/**
 * Description of DadataApiReaderTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class DadataApiReaderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    public function testRead()
    {
        $url = $this->tester->getContainer()->getParameter('dadata_address_suggestion_url');
        $token = $this->tester->getContainer()->getParameter('dadata_api_key');

        if (!$token || !$url) {
            $this->markTestIncomplete('Dadata url or token not defined');
        }

        $reader = new DadataApiReader($url, $token);
        $data = $reader->read('г Москва, ул Хабаровская');
        $this->assertArrayHasKey('suggestions', $data);
        $suggestions = $data['suggestions'];
        $s = $suggestions[0];
        $this->assertEquals('г Москва, ул Хабаровская', $s['value']);
        $this->assertEquals('0c5b2444-70a0-4932-980c-b4dc0d3f02b5', $s['data']['city_fias_id']);
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
    }
}
