<?php

namespace backend\WF\Address\Provider;

use Codeception\TestCase\Test;
use WF\Address\AddressElementInterface;
use WF\Address\AddressInterface;
use WF\Address\Provider\DadataAddressProvider;
use WF\Address\Provider\DadataApiReader;
use Codeception\Util\Stub;

/**
 * Description of DadataAddressProviderTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class DadataAddressProviderTest extends Test
{
    public function testFind()
    {
        $testData = json_decode(file_get_contents(__DIR__.'/fixture/dadata_response.json'), true);
        $reader = Stub::make(DadataApiReader::class, ['read' => $testData]);
        
        $provider = new DadataAddressProvider($reader);
        $result = $provider->find('test');
        
        $this->assertCount(1, $result);
        $address = $result[0];
        $this->assertInstanceOf(AddressInterface::class, $address);
        $city = $address->getCity();
        $this->assertInstanceOf(AddressElementInterface::class, $city);
        $this->assertEquals('Москва', $city->getTitle());
        $this->assertEquals('32fcb102-2a50-44c9-a00e-806420f448ea', $address->getCode('fias'));
        $this->assertEquals('77000000000713400', $address->getCode('kladr'));
        $this->assertEquals(AddressInterface::LEVEL_STREET, $address->getLevel());
    }

    public function testFind2()
    {
        $testData = json_decode(file_get_contents(__DIR__.'/fixture/dadata_response.json'), true);
        $testData['suggestions'][0]['data']['qc_geo'] = 100500;
        $reader = Stub::make(DadataApiReader::class, ['read' => $testData]);

        $provider = new DadataAddressProvider($reader);
        $result = $provider->find('test');

        $this->assertCount(1, $result);
        $address = $result[0];
        $this->assertInstanceOf(AddressInterface::class, $address);
        $city = $address->getCity();
        $this->assertInstanceOf(AddressElementInterface::class, $city);
        $this->assertEquals('Москва', $city->getTitle());
        $this->assertEquals('32fcb102-2a50-44c9-a00e-806420f448ea', $address->getCode('fias'));
        $this->assertEquals('77000000000713400', $address->getCode('kladr'));
        $this->assertEquals(PHP_INT_MAX, $address->getLevel());
    }
}
