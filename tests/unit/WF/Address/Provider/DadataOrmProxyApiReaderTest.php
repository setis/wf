<?php

namespace WF\Address\Provider;

use Codeception\Util\Stub;
use Doctrine\ORM\EntityManagerInterface;
use models\DadataAddress;
use WF\Task\TaskManager;

/**
 * Description of DadataOrmProxyApiReaderTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class DadataOrmProxyApiReaderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    public function testReadWithCoordinates()
    {
        $testData = json_decode(file_get_contents(__DIR__.'/fixture/dadata_response.json'), true);
        $stubReader = Stub::makeEmpty(DadataApiReaderInterface::class, [
            'read' => Stub::once(function() use($testData) {
                return $testData;
            })
        ]);


        $proxyReader = new DadataOrmProxyApiReader($this->em, $stubReader);

        // First time read - test that data saved to db
        $proxyReader->read('test address request');

        $this->em->clear();

        $addresses = $this->em
            ->getRepository(DadataAddress::class)
            ->findBy(['raw' => 'test address request']);

        $this->assertCount(1, $addresses);

        $this->em->clear();

        // First time read - test that data saved to db
        $data = $proxyReader->read('test address request');
        $this->assertArrayHasKey('id', $data['suggestions'][0]);
        unset($data['suggestions'][0]['id']);
        $this->assertEquals($testData, $data);
    }

    public function testReadWithoutCoordinates()
    {
        $testData = json_decode(file_get_contents(__DIR__ . '/fixture/dadata_response.json'), true);
        $stubReader = Stub::makeEmpty(DadataApiReaderInterface::class, [
            'read' => Stub::exactly(2, function () use ($testData) {
                $testData['suggestions'][0]['data']['geo_lat'] = null;
                $testData['suggestions'][0]['data']['geo_lon'] = null;
                return $testData;
            })
        ]);


        $proxyReader = new DadataOrmProxyApiReader($this->em, $stubReader);

        // First time read - test that data saved to db
        $proxyReader->read('test address request');

        $this->em->clear();

        $addresses = $this->em
            ->getRepository(DadataAddress::class)
            ->findBy(['raw' => 'test address request']);

        $this->assertCount(0, $addresses);

        $this->em->clear();

        // First time read - test that data saved to db
        $data = $proxyReader->read('test address request');
        $this->assertArrayNotHasKey('id', $data['suggestions'][0]);
        $testData['suggestions'][0]['data']['geo_lat'] = null;
        $testData['suggestions'][0]['data']['geo_lon'] = null;
        $this->assertEquals($testData, $data);
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
    }
}
