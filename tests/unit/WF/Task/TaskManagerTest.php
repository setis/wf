<?php

namespace WF\Task;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\ORM\Doctrine\Populator;
use InvalidArgumentException;
use models\Department;
use models\Task;
use models\TaskComment;
use models\TaskStatus;
use models\TaskUserLink;
use models\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class TaskManagerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var DateTime
     */
    private $dateInPast;

    public function testSetTaskBoundedUsers()
    {
        $task = (new Task())
            ->setPluginUid('test')
            ->setIsPrivate(false);

        $dept = (new Department())
            ->setName('testDept')
            ->setOrder(0);

        $user1 = $this->createUser(1, $dept);
        $user2 = $this->createUser(2, $dept);
        $user3 = $this->createUser(3, $dept);
        $taskUserLink = (new TaskUserLink())
            ->setIspoln(false)
            ->setUser($user3)
            ->setTask($task);

        $this->em->persist($dept);
        $this->em->persist($task);
        $this->em->persist($user1);
        $this->em->persist($user2);
        $this->em->persist($user3);
        $this->em->persist($taskUserLink);
        $this->em->flush();
        $dispatcher = $this->getMockForAbstractClass(EventDispatcherInterface::class);

        $sm = $this->getMockForAbstractClass(\Gorserv\Gerp\ScheduleBundle\Model\ScheduleManagerInterface::class);

        $tm = new TaskManager($this->em, $sm, $dispatcher);
        $tm->setTaskBoundedUsers($task, [$user1, $user2->getId()]);
    }

    private function createUser($number, $dept)
    {
        return (new User())
            ->setLogin('__testusr' . $number)
            ->setFio('Test' . $number)
            ->setPass('pass')
            ->setDepartment($dept)
            ->setActive(true)
            ->setRpp(20)
            ->setTpl('')
            ->setNbnCrewName('')
            ->setAccessType(0)
            ->setAllowAccessCu(true);
    }

    public function testSetTaskExecutors()
    {
        $task = (new Task())
            ->setPluginUid('test')
            ->setIsPrivate(false);

        $dept = (new Department())
            ->setName('testDept')
            ->setOrder(0);

        $user1 = $this->createUser(1, $dept);
        $user2 = $this->createUser(2, $dept);
        $user3 = $this->createUser(3, $dept);
        $taskUserLink = (new TaskUserLink())
            ->setIspoln(false)
            ->setUser($user3)
            ->setTask($task);

        $this->em->persist($dept);
        $this->em->persist($task);
        $this->em->persist($user1);
        $this->em->persist($user2);
        $this->em->persist($user3);
        $this->em->persist($taskUserLink);
        $this->em->flush();
        $dispatcher = $this->getMockForAbstractClass(EventDispatcherInterface::class);

        $sm = $this->getMockForAbstractClass(\Gorserv\Gerp\ScheduleBundle\Model\ScheduleManagerInterface::class);

        $tm = new TaskManager($this->em, $sm, $dispatcher);
        $tm->setTaskExecutors($task, [$user1, $user2->getId()]);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCreateTaskTypeException()
    {
        $dispatcher = $this->getMockForAbstractClass(EventDispatcherInterface::class);
        $sm = $this->getMockForAbstractClass(\Gorserv\Gerp\ScheduleBundle\Model\ScheduleManagerInterface::class);

        $tm = new TaskManager($this->em, $sm, $dispatcher);
        $tm->createTask('__test');
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCreateTask()
    {
        $dispatcher = $this->getMockForAbstractClass(EventDispatcherInterface::class);
        $sm = $this->getMockForAbstractClass(\Gorserv\Gerp\ScheduleBundle\Model\ScheduleManagerInterface::class);
        $tm = new TaskManager($this->em, $sm, $dispatcher);
        $task = $tm->createTask('service');
        $this->assertGreaterThan(0, $task->getId());
    }

    public function testSaveTaskEntity()
    {
        $task = $this->prepareTaskClosedInPast(Task::CONNECTION);

        $this->em->persist($task);
        $this->em->flush();
        $this->assertGreaterThan(0, $task->getId());

        $task = $this->prepareTaskNotClosed(Task::ACCIDENT);
        $this->em->persist($task);
        $this->em->flush();
        $this->assertGreaterThan(0, $task->getId());

        $task = $this->prepareTaskClosedInThisPeriod(Task::GLOBAL_PROBLEM);
        $this->em->persist($task);
        $this->em->flush();
        $this->assertGreaterThan(0, $task->getId());
    }

    private function prepareTaskClosedInPast($pluginUid)
    {
        $generator = Factory::create('ru_RU');
        $populator = new Populator($generator, $this->em);
        $status = $this->em->getRepository(TaskStatus::class)
            ->findOneByTag('done', $pluginUid);
        $populator->addEntity(Task::class, 1, [
            'pluginUid' => $pluginUid,
            'bill' => null,
            'status' => $status,
            'schedule' => null,
            'lastComment' => null,
            'project' => null,
        ]);
        $populator->addEntity(TaskComment::class, 1, [
            'datetime' => new DateTime('-3 month'),
            'status' => $status
        ]);

        $models = $populator->execute();
        /** @var Task $task */
        $task = $models[Task::class][0];

        $task->addComment($models[TaskComment::class][0]);

        return $task;
    }

    private function prepareTaskNotClosed($pluginUid)
    {
        $generator = Factory::create('ru_RU');
        $populator = new Populator($generator, $this->em);
        $status = $this->em->getRepository(TaskStatus::class)
            ->findOneBy(['isDefault'=>null, 'pluginUid'=>$pluginUid]);
        $populator->addEntity(Task::class, 1, [
            'pluginUid' => $pluginUid,
            'bill' => null,
            'status' => $status,
            'schedule' => null,
            'lastComment' => null,
            'project' => null,
        ]);
        $populator->addEntity(TaskComment::class, 1, [
            'datetime' => $this->dateInPast,
            'status' => $status
        ]);

        $models = $populator->execute();
        /** @var Task $task */
        $task = $models[Task::class][0];

        return $task->addComment($models[TaskComment::class][0]);
    }

    /**
     * @param $pluginUid
     * @return Task
     */
    private function prepareTaskClosedInThisPeriod($pluginUid)
    {
        $generator = Factory::create('ru_RU');
        $populator = new Populator($generator, $this->em);
        $status = $this->em->getRepository(TaskStatus::class)
            ->findOneByTag('done', $pluginUid);
        $populator->addEntity(Task::class, 1, [
            'pluginUid' => function () use ($pluginUid) {
                return $pluginUid;
            },
            'bill' => null,
            'status' => $status,
            'schedule' => null,
            'lastComment' => null,
            'project' => null,
        ]);
        $populator->addEntity(TaskComment::class, 1, [
            'datetime' => new DateTime(),
            'status' => $status
        ]);

        $models = $populator->execute();
        /** @var Task $task */
        return $models[Task::class][0];
    }

    /**
     * depends testSaveTaskEntity
     */
    public function testIsTaskNotClosed()
    {
        $taskNotClosed = $this->prepareTaskNotClosed(Task::CONNECTION);
        $this->assertFalse($this->tm->isTaskClosedInPeriod($taskNotClosed));
    }

    /**
     * @depends testSaveTaskEntity
     */
    public function testIsTaskClosedInPast()
    {
        $taskClosedInPast = $this->prepareTaskClosedInPast(Task::SERVICE);
        $this->assertFalse($this->tm->isTaskClosedInPeriod($taskClosedInPast));
        $this->assertTrue($this->tm->isTaskClosedInPeriod($taskClosedInPast, $this->dateInPast));
    }

    /**
     * @depends testSaveTaskEntity
     */
    public function testIsTaskClosedInThisPeriod()
    {
        $connectionClosedInThisPeriod = $this->prepareTaskClosedInThisPeriod(Task::GLOBAL_PROBLEM);
        $this->assertTrue($this->tm->isTaskClosedInPeriod($connectionClosedInThisPeriod));
        $this->assertFalse($this->tm->isTaskClosedInPeriod($connectionClosedInThisPeriod, $this->dateInPast));
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
        $this->container = $this->tester->getContainer();
        $this->dateInPast = new DateTime('-3 month');
    }
}
