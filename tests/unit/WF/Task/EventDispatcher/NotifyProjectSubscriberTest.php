<?php

namespace WF\Task\EventDispatcher;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Project;
use models\Task;
use models\TaskStatus;
use ReflectionProperty;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WF\Task\Events\ProjectDeadlineExpiredEvent;
use WF\Task\TaskManager;

/**
 * Description of NotifyProjectSubscriberTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyProjectSubscriberTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function testOnProjectDeadline()
    {
        /* @var $subscriber NotifyProjectSubscriber */
        $subscriber = $this->container->get('wf.task.notify_project_subscriber');

       $project = (new Project())
           ->setDeadline(new DateTime());

       $task = (new Task())
           ->setPluginUid(Task::PROJECT)
           ->setDateReg(new DateTime())
           ->setStatus((new TaskStatus)->setName('Test'))
           ->setProject($project);

       $idProp = new ReflectionProperty(Task::class, 'id');
       $idProp->setAccessible(true);
       $idProp->setValue($task, 1);

       $event = new ProjectDeadlineExpiredEvent($task);
       $subscriber->onProjectDeadline($event);
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
        $this->container = $this->tester->getContainer();
    }
}
