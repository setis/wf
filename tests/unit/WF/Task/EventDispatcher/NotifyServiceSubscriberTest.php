<?php

namespace WF\Task\EventDispatcher;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Gfx;
use models\Task;
use models\TaskStatus;
use models\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WF\Notification\Model\AbstractHandler;
use WF\Notification\Model\NotificationInterface;
use WF\Task\Events\TaskAddedToScheduleEvent;
use WF\Task\Events\TaskExecutorsChangedEvent;
use WF\Task\Events\TaskStatusChangedEvent;
use WF\Task\TaskManager;

/**
 * Description of NotifyServiceSubscriberTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyServiceSubscriberTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function testOnTaskToScheduleTech()
    {
        /* @var $subscriber NotifyServiceSubscriber */
        $subscriber = $this->container->get('wf.task.notify_service_subscriber');

        $subscriber->pushHandler('slack', $this->createMockHandler());
        $event = new TaskAddedToScheduleEvent($this->createTask());

        $subscriber->onTaskToScheduleTech($event);
    }

    private function createMockHandler()
    {
        $handler = $this->getMockBuilder(AbstractHandler::class)
            ->setMethods(['handle', 'isHandling'])
            ->getMockForAbstractClass();

        $handler->method('isHandling')
            ->willReturn(true);

        $handler->expects($this->once())
            ->method('handle')
            ->with($this->callback(function ($notification) {
                return $notification instanceof NotificationInterface;
            }));

        return $handler;
    }

    private function createTask()
    {
        $user = $this->createMock(User::class);
        $user->method('getFio')
            ->willReturn('Фамилия Имя Отчество');

        $schedule = $this->getMockBuilder(Gfx::class)
            ->disableOriginalConstructor()
            ->getMock();

        $schedule->method('getTechnician')
            ->willReturn($user);
        $schedule->method('getStartDateTime')
            ->willReturn(new DateTime('1970-01-01 01:01:01'));


        $task = $this->getMockBuilder(Task::class)->getMock();
        $task->method('getSchedule')
            ->willReturn($schedule);
        $task->method('getId')
            ->willReturn('1');

        $task->method('getPluginUid')
            ->willReturn(Task::SERVICE);

        return $task;
    }

    public function testOnTaskExecutorsChanged()
    {
        /* @var $subscriber NotifyServiceSubscriber */
        $subscriber = $this->container->get('wf.task.notify_service_subscriber');

        $subscriber->pushHandler('slack', $this->createMockHandler());

        $tech1 = $this->createMock(User::class);
        $tech1->method('getId')->willReturn(1);

        $tech2 = $this->createMock(User::class);
        $tech2->method('getId')->willReturn(2);

        $tech3 = $this->createMock(User::class);
        $tech3->method('getId')->willReturn(3);

        $task = $this->createMock(Task::class);
        $task->method('getPluginUid')->willReturn(Task::SERVICE);
        $task->method('getId')->willReturn(1);

        $event = new TaskExecutorsChangedEvent($task, [$tech2], [$tech1, $tech2, $tech3]);

        $subscriber->onTaskExecutorsChanged($event);
    }

    public function testOnTaskStatusChangedReportTimeout()
    {
        $tech = $this->createMock(User::class);
        $tech->method('getMasterServiceCenters')->willReturn([]);

        $schedule = $this->getMockBuilder(Gfx::class)
            ->disableOriginalConstructor()
            ->getMock();
        $schedule->method('getTechnician')->willReturn($tech);

        $task = $this->createMock(Task::class);
        $task->method('getSchedule')->willReturn($schedule);
        $task->method('getPluginUid')->willReturn(Task::SERVICE);
        $task->method('getId')->willReturn(1);

        $event =$this->getMockBuilder(TaskStatusChangedEvent::class)
            ->disableOriginalConstructor()
            ->getMock();
        $status = $this->createMock(TaskStatus::class);
        $status->method('getId')->willReturn(42);
        $event->method('getNewStatus')->willReturn($status);
        $event->method('getTask')->willReturn($task);

        $handler = $this->createMockHandler();

        /* @var $subscriber NotifyServiceSubscriber */
        $subscriber = $this->container->get('wf.task.notify_service_subscriber');

        $subscriber->pushHandler('slack', $handler);

        $subscriber->onTaskStatusChangedReportTimeout($event);
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
        $this->container = $this->tester->getContainer();
    }
}
