<?php

namespace WF\Task\EventDispatcher;

use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use models\Gfx;
use models\Task;
use models\Ticket;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WF\Task\Events\TaskScheduleTimeSoonExpireEvent;
use WF\Task\TaskManager;

/**
 * Description of NotifyAccidentConnectionSubscriberTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyAccidentConnectionSubscriberTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function testOnScheduleTimeSoonExpire()
    {
        /* @var $subscriber NotifyAccidentConnectionSubscriber */
        $subscriber = $this->container->get('wf.task.notify_accident_connection_subscriber');

       $tech = $this->tester->createUser();
       $slackUser = $this->tester->imagineSlackUser();
       $tech->setSlackUser($slackUser);
       $ticket = new Ticket();
       $task = (new Task())
           ->setPluginUid(Task::ACCIDENT)
           ->setTicket($ticket);
       $schedule = (new Gfx($task))
           ->setTechnician($tech);
       $interval = new DateInterval('PT30M');
       $event = new TaskScheduleTimeSoonExpireEvent($schedule, $interval);
       $subscriber->onScheduleTimeSoonExpire($event);
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
        $this->container = $this->tester->getContainer();
    }
}
