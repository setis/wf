<?php

namespace WF\Task\EventDispatcher;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Gfx;
use models\Task;
use models\Ticket;
use models\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WF\Task\Events\TaskAddedToScheduleEvent;
use WF\Task\TaskManager;

/**
 * Description of NotifyUserOnSkpToScheduleListenerTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyClientOnSkpToScheduleListenerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function testOnWfTaskAddedToSchedule()
    {
        /* @var $listener NotifyClientOnSkpToScheduleListener */
        $listener = $this->container->get('wf.task.notify_client_on_skp_to_schedule');

        $event = new TaskAddedToScheduleEvent($this->createTask());

        $listener->onWfTaskAddedToSchedule($event);
    }

    private function createTask()
    {
        $user = $this->createMock(User::class);
        $user->method('getFio')
            ->willReturn('Фамилия Имя Отчество');

        $schedule = $this->getMockBuilder(Gfx::class)
            ->disableOriginalConstructor()
            ->getMock();

        $schedule->method('getTechnician')
            ->willReturn($user);
        $schedule->method('getStartDateTime')
            ->willReturn(new DateTime('1970-01-01 01:01:01'));

        $ticket = $this->createMock(Ticket::class);
        $ticket->method('getClntTel1')
            ->willReturn('+79111111111');
        $ticket->method('getClntTel2')
            ->willReturn('+79222222222');

        $task = $this->getMockBuilder(Task::class)->getMock();
        $task->method('getSchedule')
            ->willReturn($schedule);
        $task->method('getTicket')
            ->willReturn($ticket);
        $task->method('getId')
            ->willReturn('1');
        $task->method('getPluginUid')
            ->willReturn(Task::SERVICE);

        return $task;
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
        $this->container = $this->tester->getContainer();
    }
}
