<?php

namespace WF\Task\EventDispatcher;

use Codeception\Util\Stub;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use models\Gp;
use models\ListSc;
use models\Task;
use models\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WF\Notification\Model\AbstractHandler;
use WF\Notification\Model\HandlerInterface;
use WF\Notification\Model\NotificationInterface;
use WF\Task\Events\GpCreatedEvent;
use WF\Task\Events\TaskExecutorsChangedEvent;
use WF\Task\TaskManager;

/**
 * Description of NotifyGpSubscriberTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyGpSubscriberTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function testOnGpCreated()
    {
        $slackHandler = Stub::makeEmpty(HandlerInterface::class, [
            'handle' => $this->once(),
        ]);
        $emailHandler = Stub::makeEmpty(HandlerInterface::class, [
            'handle' => $this->once(),
        ]);

        /* @var $subscriber NotifyServiceSubscriber */
        $subscriber = $this->container->get('wf.task.notify_gp_subscriber');

        $subscriber->pushHandler('slack', $slackHandler);
        $subscriber->pushHandler('email', $emailHandler);

        $sc = Stub::make(ListSc::class, ['getChiefs' => new ArrayCollection()]);

        $gp = Stub::make(Gp::class, [
            'getServiceCenter' => $sc
        ]);

        $task = Stub::construct(Task::class, [], [
            'getId' => 1,
            'getPluginUid' => Task::GLOBAL_PROBLEM,
            'getGp' => $gp,
            'getAddresses' => new ArrayCollection(),
            ]);

        $event = new GpCreatedEvent($task, $gp);

        // TODO: replace mock by real objects
        //$subscriber->onGpCreated($event);
    }

    public function testOnTaskExecutorsChanged()
    {
        /* @var $subscriber NotifyServiceSubscriber */
        $subscriber = $this->container->get('wf.task.notify_gp_subscriber');

        $subscriber->pushHandler('slack', $this->createMockHandler());

        $tech1 = $this->createMock(User::class);
        $tech1->method('getId')->willReturn(1);

        $tech2 = $this->createMock(User::class);
        $tech2->method('getId')->willReturn(2);

        $tech3 = $this->createMock(User::class);
        $tech3->method('getId')->willReturn(3);

        $task = $this->createMock(Task::class);
        $task->method('getPluginUid')->willReturn(Task::GLOBAL_PROBLEM);
        $task->method('getId')->willReturn(1);

        $event = new TaskExecutorsChangedEvent($task, [$tech2], [$tech1, $tech2, $tech3]);

        $subscriber->onTaskExecutorsChanged($event);
    }

    private function createMockHandler()
    {
        $handler = $this->getMockBuilder(AbstractHandler::class)
            ->setMethods(['handle', 'isHandling'])
            ->getMockForAbstractClass();

        $handler->method('isHandling')
            ->willReturn(true);

        $handler->expects($this->once())
            ->method('handle')
            ->with($this->callback(function($notification){
                return $notification instanceof NotificationInterface;
            }));

        return $handler;
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
        $this->container = $this->tester->getContainer();
    }
}
