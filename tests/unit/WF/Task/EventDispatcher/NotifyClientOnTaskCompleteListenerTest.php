<?php

namespace WF\Task\EventDispatcher;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Gfx;
use models\Task;
use models\TaskComment;
use models\TaskStatus;
use models\Ticket;
use models\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WF\Sms\Model\SmsManagerInterface;
use WF\Sms\SmsMessage;
use WF\Task\Events\TaskStatusChangedEvent;
use WF\Task\TaskManager;

/**
 * Description of NotifyClientOnTaskCompleteListenerTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyClientOnTaskCompleteListenerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function testWithContainer()
    {

        /* @var $listener NotifyClientOnTaskCompleteListener */
        $listener = $this->container->get('wf.task.notify_client_on_task_complete_listener');

        $comment = $this->createMock(TaskComment::class);
        $oldStatus = $this->createMock(TaskStatus::class);

        $event = new TaskStatusChangedEvent($this->createTask(), $oldStatus, $comment);

        $listener->onWfTaskStatusChanged($event);
    }

    private function createTask()
    {
        $user = $this->createMock(User::class);
        $user->method('getFio')
            ->willReturn('Фамилия Имя Отчество');

        $schedule = $this->getMockBuilder(Gfx::class)
            ->disableOriginalConstructor()
            ->getMock();

        $schedule->method('getTechnician')
            ->willReturn($user);
        $schedule->method('getStartDateTime')
            ->willReturn(new DateTime('1970-01-01 01:01:01'));

        $ticket = $this->createMock(Ticket::class);
        $ticket->method('getClntTel1')
            ->willReturn('+79111111111');
        $ticket->method('getClntTel2')
            ->willReturn('+79222222222');

        $task = $this->getMockBuilder(Task::class)->getMock();
        $task->method('getSchedule')
            ->willReturn($schedule);
        $task->method('getTicket')
            ->willReturn($ticket);
        $task->method('getId')
            ->willReturn('1');
        $task->method('getPluginUid')
            ->willReturn(Task::SERVICE);

       return $task;
    }

    public function testModel()
    {
        $smsManager = $this->getMockForAbstractClass(SmsManagerInterface::class);
        $smsManager->expects($this->once())
            ->method('send')
            ->with($this->callback(function ($obj) {
                return $obj instanceof SmsMessage
                    && $obj->getText() === 'Заявка №1 выполнена';
            }));

        $logger = $this->getMockForAbstractClass(LoggerInterface::class);

        $em = $this->getMockForAbstractClass(EntityManagerInterface::class);

        $listener = new NotifyClientOnTaskCompleteListener($logger, $smsManager, $em, '123456');

        $event = $this->getMockBuilder(TaskStatusChangedEvent::class)
            ->disableOriginalConstructor()
            ->getMock();
        $status = $this->createMock(TaskStatus::class);
        $status->method('getId')->willReturn(12);
        $event->method('getNewStatus')->willReturn($status);
        $event->method('getTask')->willReturn($this->createTask());

        $listener->onWfTaskStatusChanged($event);

    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
        $this->container = $this->tester->getContainer();
    }
}
