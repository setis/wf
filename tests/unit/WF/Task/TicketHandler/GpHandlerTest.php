<?php

namespace WF\Task\TicketHandler;

use Doctrine\ORM\EntityManagerInterface;
use models\Task;
use models\TaskComment;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Task\TaskManager;

/**
 * Description of GpHandlerTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class GpHandlerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function testCreateTicket()
    {
        $dispatcher = $this->getMockForAbstractClass(EventDispatcherInterface::class);
        $handler = new GpHandler($this->em, $dispatcher);

        $task = (new Task())
            ->setPluginUid('test')
            ->setIsPrivate(false);

        $partner = $this->tester->createPartner('partner1');
        $taskType = $this->tester->createTaskType('type1');
        $this->em->persist($task);
        $this->em->persist($partner);
        $this->em->persist($taskType);
        $this->em->flush();

        $handler->createTicket($task, [
            'partner' => $partner,
            'task_type' => $taskType,
            'comment' => 'Test comment'
        ]);

        $this->assertGreaterThan(0, $task->getId());
        $this->assertGreaterThan(0, $task->getGp()->getId());

        $this->assertContains('Контрагент: partner1 (type1)', $task->getTaskTitle());
        $commentExists = $task->getComments()
            ->exists(function($key, TaskComment $comment) {
                return $comment->getText() === 'Test comment';
            });
        $this->assertTrue($commentExists);
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
        $this->container = $this->tester->getContainer();
    }
}
