<?php

namespace backend\WF\Sms\Sender;

use Codeception\TestCase\Test;
use WF\Sms\Sender\WebsmsRuSender;
use WF\Sms\SmsMessage;

/**
 * Description of WebsmsRuSenderTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class WebsmsRuSenderTest extends Test
{
    /**
     * @expectedException WF\Sms\SmsException
     */
    public function testSendWrongLoginPass()
    {
        $sender = new WebsmsRuSender('http://websms.ru/xml_in5.asp', 'test', 'test', true);
        $message = new SmsMessage('Проверка', ['+79999999991', '+79999999992']);
        $sender->send($message);
    }

    public function testSend()
    {
        $sender = new WebsmsRuSender('http://websms.ru/xml_in5.asp', 'Gorserv', 'R7901870', true);
        $message = new SmsMessage('Проверка', ['+79999999991', '+79999999992']);
        $sender->send($message);
    }
}
