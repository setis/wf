<?php

namespace backend\WF\Sms;

use Codeception\TestCase\Test;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WF\Sms\Model\SmsSenderInterface;
use WF\Sms\SmsException;
use WF\Sms\SmsManager;
use WF\Sms\SmsMessage;

/**
 * Description of SmsManagerTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SmsManagerTest extends Test
{
    public function testSend()
    {
        $sender = $this->createSender();
        $logger = $this->createLogger();
        $ed = $this->createDispatcher();
        $manager = new SmsManager($sender, $logger, $ed);
        $message = new SmsMessage('Test message', ['+79999999991', '+79999999992']);
        $manager->send($message);
    }

    /**
     * @expectedException WF\Sms\SmsException
     */
    public function testSendException()
    {
        $message = new SmsMessage('Test message');
        $sender = $this->createSenderException($message);
        $logger = $this->createLogger();
        $ed = $this->createDispatcher();
        $manager = new SmsManager($sender, $logger, $ed, true);
        $manager->send($message);
    }

    private function createSender()
    {
        return $this->getMockForAbstractClass(SmsSenderInterface::class);
    }

    private function createSenderException($message)
    {
        $mock = $this->getMockForAbstractClass(SmsSenderInterface::class);
        $mock->method('send')
            ->will($this->throwException(new SmsException('Test', $message)));
        return $mock;
    }

    private function createLogger()
    {
        return $this->getMockForAbstractClass(LoggerInterface::class);
    }

    private function createDispatcher()
    {
        return $this->getMockForAbstractClass(EventDispatcherInterface::class);
    }
}
