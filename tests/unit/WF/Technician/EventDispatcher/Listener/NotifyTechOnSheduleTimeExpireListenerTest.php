<?php

namespace WF\Technician\EventDispatcher\Listener;

use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use models\Gfx;
use models\Task;
use models\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WF\Notification\Model\AbstractHandler;
use WF\Notification\Model\NotificationInterface;
use WF\Task\Events\TaskScheduleTimeSoonExpireEvent;
use WF\Task\TaskManager;

/**
 * Description of NotifyTechOnSheduleTimeExpireListenerTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class NotifyTechOnSheduleTimeExpireListenerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function testOnWfTaskScheduleTimeSoonExpire()
    {
        /* @var $listener NotifyTechOnScheduleTimeExpireListener */
        $listener = $this->container->get('wf.technician.notify_tech_on_schedule_time_expire_listener');
        $listener->pushHandler($this->createMockHandler());
        $interval = new DateInterval('PT30M');
        $schedule = $this->createSchedule();
        $event = new TaskScheduleTimeSoonExpireEvent($schedule, $interval);

        $listener->onWfTaskScheduleTimeSoonExpire($event);
    }

    private function createMockHandler()
    {
        $handler = $this->getMockBuilder(AbstractHandler::class)
            ->setMethods(['handle', 'isHandling'])
            ->getMockForAbstractClass();

        $handler->method('isHandling')
            ->willReturn(true);

        $handler->expects($this->once())
            ->method('handle')
            ->willReturn(true)
            ->with($this->callback(function ($notification) {
                return $notification instanceof NotificationInterface
                    && !empty($notification->getMessage());
            }));

        return $handler;
    }

    private function createSchedule()
    {
        $user = $this->createMock(User::class);
        $user->method('getFio')
            ->willReturn('Фамилия Имя Отчество');

        $task = $this->getMockBuilder(Task::class)->getMock();
        $task->method('getId')
            ->willReturn('1');
        $task->method('getPluginUid')
            ->willReturn(Task::SERVICE);

        $schedule = $this->getMockBuilder(Gfx::class)
            ->disableOriginalConstructor()
            ->getMock();
        $schedule->method('getTechnician')
            ->willReturn($user);
        $schedule->method('getTask')
            ->willReturn($task);


       return $schedule;
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
        $this->container = $this->tester->getContainer();
    }
}
