<?php

namespace repository;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use models\Bill;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UnitTester;
use WF\Task\TaskManager;

/**
 * Description of BillRepositoryTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class BillRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function testGetPartialSpendingsAtPeriodByType()
    {
        /* @var $repo BillRepository */
        $repo = $this->em->getRepository(Bill::class);

        $end = new DateTime();

        $start = clone $end;
        $start->modify('-1 week');

        $bu = $this->tester->createBusinessUnit();
        $billType = $this->tester->createBillType()
            ->setBusinessUnit($bu);
        $this->em->persist($bu);
        $this->em->persist($billType);
        $this->em->flush();


        $repo->getPartialSpendingsAtPeriodByType($billType, $start, $end);
    }

    public function testGetPlannedSpendingsAtPeriodByType()
    {
        /* @var $repo BillRepository */
        $repo = $this->em->getRepository(Bill::class);

        $end = new DateTime();

        $start = clone $end;
        $start->modify('-1 week');

        $bu = $this->tester->createBusinessUnit();
        $billType = $this->tester->createBillType()
            ->setBusinessUnit($bu);
        $this->em->persist($bu);
        $this->em->persist($billType);
        $this->em->flush();


        $repo->getPlannedSpendingsAtPeriodByType($billType, $end, $end);
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
        $this->container = $this->tester->getContainer();
    }
}
