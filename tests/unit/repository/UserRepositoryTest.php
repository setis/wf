<?php

namespace repository;

use Doctrine\ORM\EntityManagerInterface;
use models\User;
use UnitTester;
use WF\Task\TaskManager;

/**
 * Description of UserRepositoryTest
 *
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class UserRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TaskManager
     */
    protected $tm;

    public function testFindActiveByServiceCenterAndPositions()
    {
        $sc = $this->tester->createServiceCenter();
        $user = $this->tester->createUser();
        $dept = $this->tester->createDepartment();
        $jobPos = $this->tester->createJobPosition();
        $user->setDepartment($dept)->setJobPosition($jobPos);
        $user->getWarehouses()->add($sc);
        $sc->getTechnicians()->add($user);

        $this->em->persist($sc);
        $this->em->persist($dept);
        $this->em->persist($user);
        $this->em->persist($jobPos);
        $this->em->flush();

        $users = $this->em->getRepository(User::class)
            ->findActiveByServiceCenterAndPositions($sc, [$jobPos, -1]);

        $this->assertCount(1, $users);
        $this->assertContains($user, $users);

        $user2 = $this->tester->createUser();
        $jobPos2 = $this->tester->createJobPosition();
        $user2->setDepartment($dept)->setJobPosition($jobPos2);
        $user2->getWarehouses()->add($sc);
        $this->em->persist($user2);
        $this->em->persist($jobPos2);
        $this->em->flush();

        $users2 = $this->em->getRepository(User::class)
            ->findActiveByServiceCenterAndPositions($sc, [$jobPos, $jobPos2]);

        $this->assertCount(2, $users2);
        $this->assertContains($user, $users2);
        $this->assertContains($user2, $users2);
    }

    public function testFindActiveByDepartments()
    {
        $user = $this->tester->createUser();
        $dept = $this->tester->createDepartment();
        $user->setDepartment($dept);
        $this->em->persist($dept);
        $this->em->persist($user);

        $user2 = $this->tester->createUser();
        $dept2 = $this->tester->createDepartment();
        $user2->setDepartment($dept2);
        $this->em->persist($dept2);
        $this->em->persist($user2);

        $this->em->flush();

        $users = $this->em->getRepository(User::class)
            ->findActiveByDepartments([$dept, $dept2->getId()]);

        $this->assertCount(2, $users);
        $this->assertContains($user, $users);
        $this->assertContains($user2, $users);
    }

    protected function _before()
    {
        parent::_before();
        $this->tm = $this->tester->getContainer()->get('wf.task.task_manager');
        $this->em = $this->tester->getContainer()->get('doctrine.orm.entity_manager');
    }
}
