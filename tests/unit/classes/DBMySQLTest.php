<?php

namespace classes;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Statement;
use Faker\Factory;
use Faker\ORM\Doctrine\Populator;
use models\Department;
use models\User;

class DBMySQLTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     *
     * @var DBMySQL
     */
    private $db;

    /**
     *
     * @var Connection
     */
    private $connection;


    public function _before()
    {
        parent::_before();
        $this->connection = $this->tester->getContainer()->get('doctrine.dbal.default_connection');
        $this->db = new DBMySQL($this->connection);

        $generator = Factory::create('ru_RU');
        $populator = new Populator($generator, $this->tester->getContainer()->get('doctrine.orm.entity_manager'));
        $populator->addEntity(Department::class, 1, [
            'users' => new ArrayCollection(),
        ]);
        $populator->addEntity(User::class, 18, [
            'roles' => ['ROLE_USER'],
            'moneyLimit' => null,
            'skpDebt' => null,
            'jobPosition' => null,
            'login' => function() use ($generator){ return $generator->uuid; },
        ]);


        $populator->execute();
    }

    public function testConnection()
    {
        $this->assertNotEmpty($this->db);

        $this->assertEquals(0, $this->db->errno());
    }

    /**
     * @depends testConnection
     */
    public function testQueryResultType()
    {
        $good_query = 'SELECT * FROM users LIMIT 10';
        $result = $this->db->query($good_query);
        $this->assertInstanceOf(Statement::class, $result,
            'db->query result has wrong instance type');

        $result->fetch();
    }

    /**
     * @depends testConnection
     */
    public function testGetAll()
    {
        $good_query = 'SELECT * FROM users';
        $result = $this->db->getAll($good_query);
        $this->assertTrue(is_array($result));
        $this->assertEquals(2 /* created by feeder */, count($result));

        $good_query = 'SELECT * FROM users LIMIT 10';
        $result = $this->db->getAll($good_query);
        $this->assertTrue(is_array($result));
        $this->assertEquals(2, count($result));
    }

    public function testTableAndT()
    {
        $tableName = 'users';
        $this->assertEquals('`' . addslashes($tableName) . '`', $this->db->table($tableName));
        $this->assertEquals('`' . addslashes($tableName) . '`', $this->db->T($tableName));
        $this->assertEquals(addslashes($tableName), $this->db->table($tableName, false));
    }

    /**
     * @depends testConnection
     */
    public function testTableFieldAndTF()
    {
        $tableName = 'users';
        $tableField = 'login';

        $this->assertEquals(addslashes($tableName . '.' . $tableField), $this->db->tableField($tableName, $tableField));
        $this->assertEquals(addslashes($tableName . '.' . $tableField), $this->db->TF($tableName, $tableField));
        $this->assertEquals(addslashes($tableName . '.id'), $this->db->TF($tableName));
    }

    /**
     * @depends testConnection
     */
    public function testFieldAndNullfieldAndF()
    {
        $value = uniqid('', true); // strlen 23

        $this->assertEquals("'$value'", $this->db->field($value));
        $this->assertEquals(10 + 2, strlen($this->db->field($value, 10)));
        $this->assertEquals("'1'", $this->db->field(true));

        $this->assertEquals("NULL", $this->db->nullfield(null));
        $this->assertEquals("'0'", $this->db->nullfield('0'));
        $this->assertEquals("''", $this->db->nullfield(false));
        $this->assertEquals("'1'", $this->db->nullfield(true));
        $this->assertEquals("'0'", $this->db->nullfield(0));
        $this->assertEquals("'12'", $this->db->nullfield(12));
        $this->assertEquals("'teststring'", $this->db->nullfield('teststring'));

        $this->assertEquals("'123'", $this->db->F('123', true));
        $this->assertEquals("'123'", $this->db->F('123', false));

        $this->assertEquals("'0'", $this->db->F(0, true));
        $this->assertEquals("'0'", $this->db->F(0, false));

        $this->assertEquals("NULL", $this->db->F(null, true));
        $this->assertEquals("''", $this->db->F(null, false));

        $this->assertEquals("''", $this->db->F(false, true));
        $this->assertEquals("''", $this->db->F(false, false));

        $this->assertEquals("'1'", $this->db->F(true, true));
        $this->assertEquals("'1'", $this->db->F(true, false));

    }

    public function testGetField()
    {
        $query = 'SELECT * FROM users LIMIT 1';
        $result = $this->db->getField($query);

        $all_result = $this->db->getAll($query, false)[0][0];

        $this->assertEquals($all_result, $result);
    }

    /**
     * @depends testConnection
     */
    public function testGetRow()
    {
        $query = 'SELECT * FROM users LIMIT 1';
        $result = $this->db->getRow($query);
        $all_result = $this->db->getAll($query, false)[0];
        $this->assertEquals($all_result, $result);

        $result = $this->db->getRow($query, true);
        $all_result = $this->db->getAll($query)[0];
        $this->assertEquals($all_result, $result);
    }

    /**
     * @depends testConnection
     */
    public function testGetCell()
    {
        $query = 'SELECT * FROM users LIMIT 10';
        $result = $this->db->getCell($query);

        $tmp_result = $this->db->getAll($query);
        $all_result = array();
        foreach ($tmp_result as $row) {
            $values = array_values($row);
            $all_result[] = reset($values);
        }

        $this->assertEquals($result, $all_result);
    }

    /**
     * @expectedException Doctrine\DBAL\Exception\TableNotFoundException
     */
    public function testWrongQuery()
    {
        $sql = 'SELECT * FROM _some_unexisted_Table';
        $this->db->query($sql);
    }

}
