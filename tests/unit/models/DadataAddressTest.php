<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 27.07.16
 * Time: 20:13
 */

namespace models;

use Doctrine\ORM\EntityManagerInterface;
use UnitTester;

class DadataAddressTest extends \Codeception\Test\Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testCreateFromIncompleteDadataResponse()
    {
        $testData = json_decode(file_get_contents(__DIR__.'/../WF/Address/Provider/fixture/dadata_response.json'), true);
        $testData1 = $testData;
        unset($testData1['suggestions'][0]['settlement_fias_id']);
        unset($testData1['suggestions'][0]['settlement_kladr_id']);
        unset($testData1['suggestions'][0]['settlement_with_type']);
        unset($testData1['suggestions'][0]['settlement_type']);
        unset($testData1['suggestions'][0]['settlement_type_full']);
        unset($testData1['suggestions'][0]['settlement']);

        $address1 = DadataAddress::createFromDadata($testData1['suggestions'][0]);
        $address1->setRaw('123');

        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getModule('Doctrine2')->em;

        $entityManager->persist($address1);
        $entityManager->flush();

        $testData2 = $testData;
        unset($testData2['suggestions'][0]['new_fucking_element']);
        $address2 = DadataAddress::createFromDadata($testData['suggestions'][0]);
        $address2->setRaw('123');

        $entityManager->persist($address2);
        $entityManager->flush();

        $addresses = $entityManager
            ->getRepository(DadataAddress::class)
            ->findBy(['raw' => '123']);

        $this->assertCount(2, $addresses);
    }
}
