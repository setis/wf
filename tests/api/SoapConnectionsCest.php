<?php
use Codeception\Util\Xml;
use models\Task;

class SoapConnectionsCest
{
    public function _before(ApiTester $I, Helper\SoapModule $soapModule)
    {
        $endpoint = 'http://localhost:8081/soap/connections.php';
        $schema = $I->getSoapWsdlDynamically($endpoint, $endpoint . '?wsdl');

        $soapModule->configure(
            $endpoint,
            $schema
        );
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function tryToCreateTicketWithoutTicketData(ApiTester $I)
    {
        $I->sendSoapRequest('CreateConnectionTicket',
            Xml::build()
                ->login->val('admin')->parent()
                ->password->val('admin')->parent()
        );
        $I->seeSoapResponseContainsStructure('<message></message>');
        $I->seeSoapResponseIncludes('<code>0</code>');
    }

    public function tryToCreateTicketWithAddress(ApiTester $I)
    {
        /**
         * Minimal data for creating ticket
         */
        $I->sendSoapRequest('CreateConnectionTicket',
            Xml::build()
                ->login->val('admin')->parent()
                ->password->val('admin')->parent()
                ->addr_dom->val(30)->parent()
                ->addr_ul->val('Красная сосна')->parent()
                ->addr_gor->val('Москва')->parent()
        );
        $I->seeSoapResponseContainsStructure('<message></message>');
        $I->seeSoapResponseIncludes('<code>0</code>');
    }

    public function tryToCancelConnectionTicket(ApiTester $I)
    {
        /** @var Task[] $tasks */
        $tasks = $I->getFromRepository(Task::class, [
            'pluginUid' => Task::CONNECTION,
        ], [
            'id' => 'DESC'
        ]);

        $task = $tasks[0];

        $I->sendSoapRequest('CancelConnectionTicket',
            Xml::build()
                ->login->val('admin')->parent()
                ->password->val('admin')->parent()
                ->ticket_id->val($task->getId())->parent()
                ->comment->val('Наши поезда самые поездатые поезда в мире!')
            );
        $I->seeSoapResponseContainsStructure('<message></message>');
        $I->seeSoapResponseIncludes('<code>0</code>');
    }

    public function tryToGetTaskTypes(ApiTester $I)
    {
        $I->sendSoapRequest('getAvailableTypes',
            Xml::build()
                ->login->val('admin')->parent()
                ->password->val('admin')->parent()
        );
        $I->seeSoapResponseContainsStructure('<message></message>');
        $I->seeSoapResponseIncludes('<code>0</code>');
    }
}
