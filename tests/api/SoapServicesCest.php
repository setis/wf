<?php
use Codeception\Util\Xml;
use models\Task;

class SoapServicesCest
{
    public function _before(ApiTester $I, Helper\SoapModule $soapModule)
    {
        $endpoint = 'http://localhost:8081/soap/services.php';
        $schema = $I->getSoapWsdlDynamically($endpoint, $endpoint . '?wsdl');

        $soapModule->configure(
            $endpoint,
            $schema
        );
    }

    public function _after(ApiTester $I)
    {
    }

    public function tryToTestWrongCredentialsAction(ApiTester $I)
    {
        $I->sendSoapRequest('CheckStatus',
            Xml::build()
                ->login->val('123')->parent()
                ->password->val('123')->parent()
                ->ticket_id->val(666)
        );
        $I->seeSoapResponseContainsStructure('<message></message>');
        $I->seeSoapResponseIncludes('<code>1</code>');
        $I->cantSeeSoapResponseContainsStructure("<ticket_id></ticket_id>");
    }

    // tests

    public function tryToCreateTicketWithoutTicketData(ApiTester $I)
    {
        $I->sendSoapRequest('CreateTicket',
            Xml::build()
                ->login->val('admin')->parent()
                ->password->val('admin')->parent()
        );
        $I->seeSoapResponseContainsStructure('<message></message>');
        $I->seeSoapResponseIncludes('<code>0</code>');
    }

    public function tryToCreateTicketWithAddress(ApiTester $I)
    {
        $I->sendSoapRequest('CreateTicket',
            Xml::build()
                ->login->val('admin')->parent()
                ->password->val('admin')->parent()
                ->addr_dom->val(30)->parent()
                ->addr_ul->val('Красная сосна')->parent()
                ->addr_gor->val('Москва')->parent()
                ->lsnum->val(666)->parent()
                ->start_date->val((new \DateTime())->modify('+1 day')->setTime(9, 0, 0)->format(DATE_ISO8601))->parent()
                ->end_date->val((new \DateTime())->modify('+1 day')->setTime(12, 0, 0)->format(DATE_ISO8601))->parent()

        );
        $I->seeSoapResponseContainsStructure('<message></message>');
        $I->seeSoapResponseIncludes('<code>0</code>');
    }

    public function tryToUpdateTicket(ApiTester $I)
    {
        $I->sendSoapRequest('UpdateTicket',
            Xml::build()
                ->login->val('admin')->parent()
                ->password->val('admin')->parent()
                ->lsnum->val(666)->parent()
                ->addr_dom->val(13)->parent()
                ->addr_ul->val('Окская')->parent()
                ->addr_gor->val('Москва')->parent()
                ->addr_pod->val(1)->parent()
                ->addr_etazh->val(2)->parent()
                ->addr_kv->val(3)->parent()
                ->client_type->val(1)->parent()
                ->fio->val('Владимир Морозов')->parent()
                ->phone->val('+79263333333')->parent()
                ->comment->val('Lorem ipsum dolor set amet')->parent()
                ->start_date->val((new \DateTime())->setTime(15,0,0)->format(DATE_ATOM))->parent()
                ->end_date->val((new \DateTime())->setTime(18,0,0)->format(DATE_ATOM))->parent()

        );
        $I->seeSoapResponseContainsStructure('<message></message>');
        $I->seeSoapResponseIncludes('<code>0</code>');
    }

    public function tryToAppendComment(ApiTester $I)
    {
        $I->sendSoapRequest('AppendComment',
            Xml::build()
                ->login->val('admin')->parent()
                ->password->val('admin')->parent()
                ->ticket_id->val($this->_getLastTask($I)->getId())->parent()
                ->message->val('Lorem ipsum dolor sit amet')
        );

        $I->seeSoapResponseContainsStructure('<message></message>');
        $I->seeSoapResponseIncludes('<code>0</code>');
    }

    public function _getLastTask(ApiTester $I)
    {
        /** @var Task[] $tasks */
        $tasks = $I->getFromRepository(Task::class, [
            'pluginUid' => Task::SERVICE,
        ], [
            'id' => 'DESC'
        ]);

        return $task = $tasks[0];
    }

    public function tryToCancelServiceTicket(ApiTester $I)
    {
        $I->sendSoapRequest('CancelTicket',
            Xml::build()
                ->login->val('admin')->parent()
                ->password->val('admin')->parent()
                ->ticket_id->val($this->_getLastTask($I)->getId())->parent()
                ->comment->val('Наши поезда самые поездатые поезда в мире!')
        );
        $I->seeSoapResponseContainsStructure('<message></message>');
        $I->seeSoapResponseIncludes('<code>0</code>');
    }

    public function tryToGetTaskTypes(ApiTester $I)
    {
        $I->sendSoapRequest('getAvailableTypes',
            Xml::build()
                ->login->val('admin')->parent()
                ->password->val('admin')->parent()
        );
        $I->seeSoapResponseContainsStructure('<message></message>');
        $I->seeSoapResponseIncludes('<code>0</code>');
    }

}
