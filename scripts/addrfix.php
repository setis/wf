<?php
use classes\UserSession;

set_time_limit(600);

require_once __DIR__ .'/../bootstrap.php';

$DB = wf::$container->get('wf.mysql_db');

$sql = "SELECT
        la.id,
        la.sc_id,
        la.area_id,
        (
            select
                `title`
            from
                `list_area`
            where
                `id`=la.area_id
        ) as areaname,
        (
            select
                `title`
            from
                `list_sc`
            where
                `id`=la.sc_id
        ) as sc_name
    FROM
        `list_addr` as la
    WHERE
        sc_id in (9,14,8)
    ;";
$DB->query($sql);
echo "Started: ".date("H:i d-m-Y")."\r\n";
echo "Total records: ".$DB->num_rows()."\r\n";
$i = 0;
while ($r = $DB->fetch(true)) {
    $sql = "SELECT
        lsa.sc_id,
        (
            select
                `title`
            from
                `list_sc`
            where
                `id`=lsa.sc_id
        ) as sc_name
    FROM
        `link_sc_area` as lsa
    WHERE
        lsa.area_id=".$DB->F($r["area_id"])." AND
        lsa.sc_id NOT IN (9,14,8)
    ;";
    $new = $DB->getRow($sql, true);
    //echo "<tr><td>".$r['id']. "</td><td>".$r["sc_id"]."</td><td>". $r["area_id"]."</td><td>". $r["areaname"]."</td><td>". $r["sc_name"] ."</td><td>". $new["sc_name"] . "</td><td>".$new["sc_id"]."</td><td>";
    if ($new["sc_id"]!= "") {
        $sql = "UPDATE
            `list_addr`
        SET
            `sc_id`=".$DB->F($new["sc_id"])."
        WHERE
            `id`=".$DB->F($r["id"])."
        ;";
        $res = $DB->query($sql);
        //if ($DB->errno()) echo "DB ERROR"; else echo "UPDATED_1<br />";
        $DB->free($res);
        $sql2 = "UPDATE
            `tickets`
        SET
            `sc_id`=".$new["sc_id"]."
        WHERE
            `dom_id`=".$r["id"]." AND
            `task_type` IN (25)
        ;";
        //$sql2 = "UPDATE `tickets` SET `sc_id`=".$new["sc_id"]." WHERE `dom_id`=".$r["id"]." AND `task_type` IN (25,1,13,14,23,16,17,18,19,20,24,21,31);";
        $res = $DB->query($sql2);
        //if ($DB->errno()) echo "DB ERROR"; else echo "UPDATED_2<br />";
        $DB->free($res);
        $i+=1;
        //echo $sql."<br />".$sql2;
    }
    //echo "</td></tr>";
}
echo "Updated: $i \r\n-----------------------------\r\n";
$DB->free();
