<?php
use classes\tickets\ServiceTicket;

set_time_limit(600);
error_reporting(E_ALL);
require_once __DIR__ .'/../bootstrap.php';

die('deprecated');
/**
* очистка телефонного номера от всего, кроме цифр, подходящих для отправки СМС
* @param string $phone
*
* @return string phone || false
*/
if(!function_exists('smsCleanPhone')) {
    function smsCleanPhone($phone)
    {
        $phone_clean = preg_replace("/[^0-9]/", "", $phone);

        if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
        {
            $phone_clean = $phone_clean;
        } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
        {
            $phone_clean = "7" . substr($phone_clean, 1);
        } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
        {
            $phone_clean = "7" . $phone_clean;
        } else {
            $phone_clean = false;
        }

        return $phone_clean;
    }
}

/**
* получить из любой строки список телефонов для отправки СМС
* @param string $str
* @param bool $clean_only - возвращать только очищенные значения
*
* @return array(phone_clean, phone_clean, ...) || array(phone_clean => phone_str, phone_clean => phone_str, ...) || false
*/
if(!function_exists('smsPhonesFromString')) {
    function smsPhonesFromString($str, $clean_only = false)
    {
        $ret = array();
        $arr = preg_split("/[^0-9\(\)\-\+]+/", $str, -1, PREG_SPLIT_NO_EMPTY);
        foreach ($arr as $phone) {
            if ($phone_clean = smsCleanPhone($phone)) {
                if ($clean_only) $ret[] = $phone_clean;
                else $ret[$phone_clean] = $phone;
            }
        } // foreach

        if (sizeof($ret)) return $ret;
        else             return false;
    }
}

/**
* отправка СМС
* @param mixed $to - string || array номеров получателей
* @param string $text
* @param mixed $result - string || array резултатов отправки в зависимости от количества получателей
*
* @return bool. если несколько получателей, то всегда true, для анализа нужно разбирать $result
*/
if(!function_exists('sendSMS')) {
    function sendSMS($to, $text, &$result)
    {
        if (!is_array($to)) $to = smsPhonesFromString($to, true);
        else foreach ($to as $i => $phone) if ($phone = smsCleanPhone($phone)) $to[$i] = $phone;

        if (!sizeof($to)) {
            trigger_error("No phones were found from \$to", E_USER_WARNING);
            return false;
        }

        $xml = new SimpleXMLElement("<message></message>");
        $servce = $xml->addChild("service");
        $servce->addAttribute('id', sizeof($to) > 1 ? 'bulk' : 'single');
        $servce->addAttribute('login', getcfg('sms_login'));
        $servce->addAttribute('password', getcfg('sms_pass'));
        //$servce->addAttribute('test', class_exists('Debugger') && Debugger::getDebug() ? 1 : 0);

        foreach ($to as $t) $xml->addChild("to", $t);

        $body = $xml->addChild("body", $text);

        /*header("content-type: text/plain");
        print_r($xml->asXML());*/

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, getcfg('sms_xml_url'));
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml->asXML());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-type: text/xml"
        ));

        $data = curl_exec($ch);
        $ret = array();

        if ($data === false) {
            trigger_error('cURL error: ' . curl_error($ch), E_USER_WARNING);
            return false;
        } else {
            try {
                $status = new SimpleXMLElement($data);
                if (sizeof($status->state) > 1) {
                    $result = array();
                    $i = 0;
                    foreach ($status->state as $state) {
                        if ($errcode = (int)$state['errcode']) {
                            $error = (string)$state['error'];
                            trigger_error("Send SMS $i error $errcode: $error", E_USER_WARNING);
                            $result[$i] = 0;
                        } else {
                            $result[$i] = (string)$status->id[$i];
                        }
                        $i++;
                    }
                } else {
                    if ($errcode = (int)$status->state['errcode']) {
                        $error = (string)$status->state['error'];
                        trigger_error("Send SMS error $errcode: $error", E_USER_WARNING);
                        return false;
                    } else {
                        $result = (string)$status['id'];
                    }
                }
            } catch (Exception $e) {
                trigger_error('Failed to get result XML: ' . $e->getMessage());
                return false;
            }
        }
        curl_close($ch);

        return true;
    }
}

$DB = wf::$container->get('wf.mysql_db');
$notrecognized = 0;
echo "-------------------------------------------------------------------------------\r\n";
echo "Обработка входящих смс начата: ".date("H:i:s d.m.Y")."\r\n";
$sql = "SELECT * FROM `sms_incoming` WHERE `is_new`=1;";
$DB->query($sql);
if ($DB->errno()) {
    echo "Ошибка БД: ".$DB->error()."\r\n";
    die();
}
if ($DB->num_rows()) {
    echo "Всего новых смс: ".$DB->num_rows()." \r\n";
    while ($r = $DB->fetch(true)) {
        // разбор вида смс
        $smsList[] = $r["id"];
        $r["text"] = preg_replace("/(\r?\n)|(\s{2,})/", " ", $r["text"]);
        $t = $r["text"];
        switch($t) {

            // fun
            /*case (!!preg_match("/тест/i", $t)):
                if (sendSMS(smsCleanPhone($r["from"]), "sms has been bazzzzed")) {
                    echo "Recognized. Reply sended.\r\n";
                }
                break;
            */
            // принятие заявки техником - формат [НОМЕР ЗАЯВКИ] [1 или 0]
            case (!!preg_match("/^\d+\s[0,1]{1}$/", $t)):
                    $taskparam = explode(" ", $t);
                    $sql = "SELECT t.id FROM `tasks` as t WHERE `id`=".$DB->F($taskparam[0])." AND (`status_id`=44 OR `status_id`=45) AND `plugin_uid`='services';";
                    $targetTask = $DB->getField($sql);
                    if ($targetTask) {
                        $task = new ServiceTicket($taskparam[0]);
                        $ispolnArr = $task->getIspolnPhones();
                        foreach($ispolnArr as $key => $item) {
                            $ispolnArr[$key] = smsCleanPhone($item);
                        }
                        if (!in_array($r["from"], $ispolnArr, true)) {
                            if (sendSMS($r["from"], "Вы не можете управлять заявкой с ID: ".$taskparam[0])) {
                                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Попытка захвата чужой заявки. Уведомление отправлено.");
                            } else {
                                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Попытка захвата чужой заявки. Ошибка отправки СМС.");
                            }
                            break;
                        }
                        //$task = new ServiceTicket($taskparam[0]);
                        //die($taskparam[1]);
                        // определение автора заявки
                        $ticketAuthor = !!$task->getAuthorId() ? $task->getAuthorId() : $DB->getField("SELECT `user_id` FROM `task_comments` WHERE `task_id`=".$DB->F($taskparam[0])." AND `status_id`=44 AND `id` ORDER BY `datetime` DESC;");
                        if (!!$ticketAuthor) {
                            // определение и проверка номера отправителя

                            if ($taskparam[1] == "1") {
                                //  инженер принял заявку
                                $text = "";
                                $phone = $r["from"];
                                $setCStatus = true;
                                if (smsCleanPhone($phone)) {
                                    $smsbody = "Подтверждение заявки №: ".trim($task->getId()).".\r";
                                    $smsbody .= "Контрагент: ".trim($task->getContrName()).".\r";
                                    $smsbody .= "Адрес: ".trim(implode(",",$addr = $task->getAddr1($task->getDomId())))." под. " . trim($task->getPod()) .", этаж " . trim($task->getEtazh()) .", кв. " . trim($task->getKv()) . ".\r";
                                    $smsbody .= "ФИО: ".trim($task->getFio()).".\r";
                                    $smsbody .= "Телефон: ".trim($task->getPhones(" ")).".\r\r";
                                    $smsbody .= "У Вас есть 120 мин, чтобы поставить заявку в график.\r";
                                    if (sendSMS(smsCleanPhone($phone), $smsbody)) {
                                        $comment = "СМС-уведомление отправлено. Ожидается постановка в график.";
                                    } else {
                                        $comment = "ОШИКА ОТПРАВКИ СМС!";
                                    }
                                } else {
                                    $comment = "ОШИБКА отправки СМС. Нераспознан формат номера телефона. ";
                                }
                                $comment = "Заявка принята инженером по СМС. Тел:".$r["from"]. " " .$r["date_recieve"]." ".$comment;
                                // отправить смс инженеру с данными по заявке с предупреждением о таймауте постановки в график
                                $task->addComment($comment, "Заявка принята инженером.", 45);
                                sms_log($r['id'], $r["message_id"], $r["from"], $r["text"], "Заявка #".$taskparam[0]. ($taskparam[1] == 0 ? " не принята" : " принята")." инженером");

                            } else {
                                //  инженер не принял заявку
                                $phone = $r["from"];
                                //$u = $DB->getRow("SELECT * FROM `users` WHERE `fio`='Оператор ГСС';", true);
                                //adm_users_plugin::getUser($ticketAuthor);
                                sendSMS(smsCleanPhone($phone), "Отказ от заявки с ID: ".$task->getId()." зафиксирован.");
                                //if ($u["id"]) {
                                //    $task->setIspoln($u["id"]);
                                //}
                                $task->addComment("Отказ получен по СМС. Тел:".$r["from"]. " ".$r["date_recieve"].". Назначен ответственный ".$u["fio"] ." (UID: ".$u["id"].")", "Заявка не принята инженером.", 43);
                                sms_log($r['id'], $r["message_id"], $r["from"], $r["text"], "Заявка #".$taskparam[0]. (" не принята" )." инженером. Назначен ответственный USERID: ".$ticketAuthor);

                            }
                        } else {
                            sms_log($r['id'], $r["message_id"], $r["from"], $r["text"], "ошибка определения автора заявки или диспетчера. смс пропущена");
                            array_pop($smsList);

                        }
                    } else {
                        sms_log($r['id'], $r["message_id"], $r["from"], $r["text"], "не принят номер или статус заявки не соответствует правилам");
                    }
                break;
                // постановка-перенос в график
            case (!!preg_match("/^(\d+)\s(\d{8})\s(\d{4})(\s(.*))*$/", trim($t))):
                preg_match("/^(\d+)\s(\d{8})\s(\d{4})(\s(.*))*$/", $t, $a);
                array_shift($a);
                $task_id = $a[0];
                $date = $a[1];
                $time = $a[2];
                $comment = $a[3];
                // проверка заявки
                $task = new ServiceTicket($task_id);
                if ($task->getStatusId() != 45 && $task->getStatusId()!= 9) {
                    if (sendSMS($r["from"], "Заявку с ID: $task_id нельзя поставить/перенести в график.")) {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "График: Некорректный статус заявки. Уведомление отправлено.");
                    } else {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "График: Некорректный статус заявки. Ошибка отправки СМС.");
                    }
                    break;
                }
                // проверка даты, времени и наличия расписания у инженера
                $targetDate = substr($date, 4, 4)."-".substr($date, 2, 2)."-".substr($date, 0, 2);

                if (intval(substr($date, 0, 2))>31 || intval(substr($date, 2, 2)>12) || intval(substr($date, 4, 4))<2014) {
                    if (sendSMS($r["from"], "Неправильная дата. Формат даты - 20052014, где 20 - день, 05 - месяц, 2014 - год. Проверьте дату!")) {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Некорректная дата. Уведомление отправлено.");
                    } else {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Некорректная дата. Ошибка отправки СМС.");
                    }
                    break;
                }
                if ((intval(substr($time, 0,2))>=24) || (intval(substr($time, 2,2))>=60)) {
                    if (sendSMS($r["from"], "Неправильное время! Формат времени - 1234, где 12 - час, 34 - минуты. Минуты будут округлены до 30 мин: 0-29 - 00 мин, 31-59 - 30 мин.")) {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Время указано неправильно!. Уведомление отправлено.");
                    } else {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Время указано неправильно!. Ошибка отправки СМС.");
                    }
                    break;
                }
                $targetMin = (intval(substr($time, 2, 2)) >= 0 && intval(substr($time, 2, 2)) < 30) ? "00" : "30";
                $targetTime = substr($time, 0,2).":".$targetMin;

                if (date("Y-m-d", strtotime($targetDate)) < date("Y-m-d") || (date("Y-m-d", strtotime($targetDate)) == date("Y-m-d")) && date("H:i", strtotime($targetTime))<date("H:i")) {
                    if (sendSMS($r["from"], "Нельзя установить заявку в график в прошлое. Проверьте дату!")) {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Время в прошлом. Уведомление отправлено.");
                    } else {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Время в прошлом. Ошибка отправки СМС.");
                    }
                    break;
                }
                // поиск инженера по телефону
                $phone = "(".substr($r["from"], 1, 3).") ".substr($r["from"], 4, 3)."-".substr($r["from"], 7, 2)."-".substr($r["from"], 9, 2);
                $sql = "SELECT * FROM `users` WHERE `phones` LIKE  ".$DB->F("%".$phone."%").";";
                $DB->query($sql);
                if ($DB->num_rows()==1) {
                    $targetTech = $DB->fetch(true);
                    $ispolnArr = $task->getIspolnPhones();
                        foreach($ispolnArr as $key => $item) {
                            $ispolnArr[$key] = smsCleanPhone($item);
                        }
                        if (!in_array($r["from"], $ispolnArr, true)) {
                            if (sendSMS($r["from"], "Вы не можете управлять заявкой с ID: ".$task->getId())) {
                                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Попытка захвата чужой заявки ID: ".$task->getId().". Уведомление отправлено.");
                            } else {
                                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Попытка захвата чужой заявки ID: ".$task->getId().". Ошибка отправки СМС.");
                            }
                            break;
                        }
                    // поиск расписания инженерв
                    $sql = "SELECT `duration` FROM `task_types` WHERE `id`=".$DB->F(services_plugin::getTypeID($task_id));
                    $duration = $DB->getField($sql);
                    if (!$duration) {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "ошибка получения длительности работ по виду заявки\r\n Item: ".implode(",\r\n Item: ", $a)."\r\n");
                        break;
                    }
                    $sql = "SELECT `starttime`, `endtime` FROM `link_empl_tf` WHERE `user_id`=".$DB->F($targetTech["id"])." AND `sched_date`=".$DB->F($targetDate)." AND `starttime`<=".$DB->F(intval(substr($time, 0,2))*60+intval($targetMin))." AND `endtime`>=".$DB->F(intval(substr($time, 0,2))*60+intval($targetMin)+intval($duration)).";";
                    $tf = $DB->getRow($sql, true);
                    if (!$tf) {
                        if (sendSMS($r["from"], "У Вас отсутствует расписание на выбранную дату или нет свободного времени или длительность выполнения заявки превышает доступное время. Обратитесь к диспетчеру!")) {
                            sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Отсутствует расписание у инженера на выбранную дату . Уведомление отправлено.".$sql);
                        } else {
                            sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Отсутствует расписание у инженера на выбранную дату. Ошибка отправки СМС.".$sql);
                        }
                        break;
                    }
                    $dayTF = array();
                    $s = $tf["starttime"];
                    while ($s<=$tf["endtime"]) {
                        $dayTF[$s] = ($tf["endtime"]-$s)/30;
                        $s += 30;
                    }
                    $maxCells = $dayTF[$tf["starttime"]];
                    $sql = "SELECT * FROM `gfx` WHERE `c_date`=".$DB->F($targetDate)." AND `empl_id`=".$targetTech["id"].";";
                    $DB->query($sql);
                    if ($DB->num_rows()) {
                        while ($res = $DB->fetch(true)) {
                            if ($res["task_id"] != $task_id) {
                                $sql = "SELECT `duration` FROM `task_types` WHERE `id`=".$DB->F(services_plugin::getTypeID($t["task_id"]));
                                $gfxtaskdur = $DB->getField($sql);
                                $gfxtaskdur = intval($gfxtaskdur)/30;
                                $gfxtaskstart = $res["startTime"];
                                for ($i=0; $i<$gfxtaskdur; $i+=1) {
                                    $dayTF[$res["startTime"]+$i*30] = -1;
                                }
                            }
                        }
                    }
                    $DB->free();
                    foreach ($dayTF as $key => $item) {
                        if ($item == -1) {
                            $walkback = true;
                            $k = 1;
                            while ($walkback) {
                                if ($dayTF[$key-30*$k] != -1 && ($key-30*$k)>=$tf["starttime"]) {
                                    $dayTF[$key-30*$k] = $k;
                                    $k+=1;
                                } else
                                    $walkback = false;

                            }
                        }
                    }
                    $avTimes = array();
                    foreach($dayTF as $key => $value) {
                        if (intval($value) >= ($task->getTypeDuration()/30)) {
                            $avTimes[] = floor($key/60).":".(strlen($key % 60) == 1 ? "0".($key % 60) : ($key % 60));
                        }
                    }
                    if (!array_key_exists(intval(substr($targetTime, 0, 2))*60+$targetMin, $dayTF) || $dayTF[intval(substr($targetTime, 0, 2))*60+$targetMin] < $task->getTypeDuration()/30) {
                        if (count($avTimes)) {
                            $addtoSms = "Доступное время для заявки: ".implode(", ", $avTimes);
                        }
                        if (sendSMS($r["from"], "Невозможно поставить заявку на указанное время. $addtoSms")) {
                            sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Невозможно поставить заявку на указанное время. Уведомление отправлено. $addtoSms");
                        } else {
                            sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Невозможно поставить заявку на указанное время. Ошибка отправки СМС. $addtoSms");
                        }
                        break;
                    }

                    $sql = "DELETE FROM `gfx` WHERE `task_id`=".$DB->F($task->getId())." LIMIT 10;";
                    $DB->query($sql);
                    if ($DB->errno()) {
                        sendSMS($r["from"], "Ошибка установки заявки в график. Обратитесь к диспетчеру.");
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Ошибка удаление заявки из графика. MySQL Error: ".$DB->error()."\r\n");
                        break;
                    }
                    $DB->free();

                    $executor = wf::$em->getReference(\models\User::class, $targetTech["id"]);
                    $slot = (new models\Gfx(wf::$em->find(\models\Task::class, $task->getId())))
                        ->setDate(new DateTime($targetDate))
                        ->setStartTime(intval(substr($targetTime, 0, 2)) * 60 + $targetMin);
                    $slot->updateFields();

                    /* @var $shm Gorserv\Gerp\ScheduleBundle\ScheduleManager */
                    $shm = wf::$container->get('schedule_manager');
                    try {
                        $shm->createSlot($slot->getJob(), $slot->getTimeStart(), $slot->getTimeEnd(), [$executor]);
                    } catch(\Exception $e) {

                    }

                    if ($DB->errno()) {
                        sendSMS($r["from"], "Ошибка установки заявки в график. Обратитесь к диспетчеру.");
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Ошибка удаление заявки из графика. MySQL Error: ".$DB->error()."\r\n");
                        break;
                    }
                    $DB->free();
                    if (sendSMS($r["from"], "Заявка ID: ".$task->getId(). " назначена в график на ".$targetDate." ".$targetTime)) {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "постановка/перенос в график. Уведомление отправлено.\r\n");
                    } else {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "постановка/перенос в график. Ошибка отправки уведомления.\r\n");
                    }
                    $task->addComment("Заявка назначена/перенесена в график на ".$targetDate." ".$targetTime." c номера ".$r["from"]." СМС:".$comment, "Назначение в график через СМС", 9);
                    //echo $sql."\r\n";
                    $smsbody = "Заявка №: ".trim($task->getId()).".\r";
                    $smsbody .= "Назначена на : ".$targetDate." ".$targetTime.".\r";
                    $smsbody .= "Ваш инженер: ".$targetTech["fio"]."\r";
                    $smsbody .= "Телефон диспетчера: ".getcfg('company_phone');

                    $clnt = $task->getPhones();
                    //foreach($clnt as $key => $item) {
                        if (!sendSMS(smsCleanPhone($clnt[0]), $smsbody)) {
                            $err[] = "Ошибка отправки смс на номер клиента: $item";
                        }
                    //}
                    $ispolnArr = $task->getIspolnPhones();
                    foreach($ispolnArr as $key => $item) {
                        if (!sendSMS(smsCleanPhone($item), $smsbody)) {
                            $err[] = "Ошибка отправки смс на номер техника: $item";
                        }
                    }
                    if (sizeof($err)) {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], implode(",\r\n Error: ", $err)."\r\n");

                    }



                } else {
                    sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "ошибка поиска номера отправителя по БД сотрудников\r\n Item: ".implode(",\r\n Item: ", $a)."\r\n");
                }
                $DB->free();


                break;
                // отказ (техотказ)
            case (!!preg_match("/^(\d+)\s(0{2})\s(.*)$/", trim($t))):
                preg_match("/^(\d+)\s(0{2})\s(.*)$/", $t, $a);
                array_shift($a);
                $task_id = $a[0];
                $comment = $a[2];
                // проверка заявки
                $task = new ServiceTicket($task_id);
                $ispolnArr = $task->getIspolnPhones();
                        foreach($ispolnArr as $key => $item) {
                            $ispolnArr[$key] = smsCleanPhone($item);
                        }
                        if (!in_array($r["from"], $ispolnArr, true)) {
                            if (sendSMS($r["from"], "Вы не можете управлять заявкой с ID: ".$task->getId())) {
                                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Попытка захвата чужой заявки ID: ".$task->getId().". Уведомление отправлено.");
                            } else {
                                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Попытка захвата чужой заявки ID: ".$task->getId().". Ошибка отправки СМС.");
                            }
                            break;
                        }
                if ($task->getStatusId() != 45 && $task->getStatusId()!= 9) {
                    if (sendSMS($r["from"], "Заявке с ID: $task_id нельзя назначить статус отказ.")) {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Некорректный статус заявки (отказ). Уведомление отправлено.");
                    } else {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Некорректный статус заявки (отказ). Ошибка отправки СМС.");
                    }
                    break;
                }
                //$u = $DB->getRow("SELECT * FROM `users` WHERE `fio`='Оператор ГСС';", true);
                                //adm_users_plugin::getUser($ticketAuthor);
                                //sendSMS(smsCleanPhone($phone), "Отказ от заявки с ID: ".$task->getId()." зафиксирован.");
                //if ($u["id"]) {
                //    $task->setIspoln($u["id"]);
                //}
                $task->addComment("Комментарий инженера: $comment", "Инженер установил статус Отказ через СМС", 46);
                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "техотказ ID: $task_id " . $comment . "\r\n");
                sendSMS($r["from"], "Заявке с ID: $task_id установлен статус Отказ.");
                break;
                // прибыл на заявку
            case (!!preg_match("/^(\d+)\s(11)$/", trim($t))):
                preg_match("/^(\d+)\s(11)$/", $t, $a);
                array_shift($a);
                $task_id = $a[0];
                $comment = $a[2];
                // проверка заявки
                $task = new ServiceTicket($task_id);
                $ispolnArr = $task->getIspolnPhones();
                        foreach($ispolnArr as $key => $item) {
                            $ispolnArr[$key] = smsCleanPhone($item);
                        }
                        if (!in_array($r["from"], $ispolnArr, true)) {
                            if (sendSMS($r["from"], "Вы не можете управлять заявкой с ID: ".$task->getId())) {
                                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Попытка захвата чужой заявки ID: ".$task->getId().". Уведомление отправлено.");
                            } else {
                                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Попытка захвата чужой заявки ID: ".$task->getId().". Ошибка отправки СМС.");
                            }
                            break;
                        }
                if ($task->getStatusId() != 45 && $task->getStatusId()!= 9) {
                    if (sendSMS($r["from"], "Заявке с ID: $task_id нельзя назначить статус Выполнение.")) {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Некорректный статус заявки (Выполнение). Уведомление отправлено.");
                    } else {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Некорректный статус заявки (Выполнение). Ошибка отправки СМС.");
                    }
                    break;
                }
                $task->addComment("Выполнение заявки", "Инженер прибыл на заявку", 47);
                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Выполнение заявки ID: $task_id " . $comment . "\r\n");
                sendSMS($r["from"], "Заявке с ID: $task_id установлен статус Выполнение.");
                break;
                // расчет заявки
            case (!!preg_match("/^(\d+)\s(\d+)=(\d+)(\s(.*))*$/", trim($t))):
                preg_match("/^(\d+)\s(\d+)=(\d+)(\s(.*))*$/", $t, $a);
                array_shift($a);
                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "расчет заявки\r\n Item: ".implode(",\r\n Item: ", $a)."\r\n");
                $task_id = $a[0];
                $check = $a[1];
                $price = $a[2];
                $comment = $a[3];
                // проверка заявки
                $task = new ServiceTicket($task_id);
                $ispolnArr = $task->getIspolnPhones();
                foreach($ispolnArr as $key => $item) {
                    $ispolnArr[$key] = smsCleanPhone($item);
                }
                if (!in_array($r["from"], $ispolnArr, true)) {
                    if (sendSMS($r["from"], "Вы не можете управлять заявкой с ID: ".$task->getId())) {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Попытка захвата чужой заявки ID: ".$task->getId().". Уведомление отправлено.");
                    } else {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Попытка захвата чужой заявки ID: ".$task->getId().". Ошибка отправки СМС.");
                    }
                    break;
                }
                if ($task->getStatusId() != 47) {
                    if (sendSMS($r["from"], "Заявке с ID: $task_id нельзя назначить статус Выполнена.")) {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Некорректный статус заявки (Выполнение). Уведомление отправлено.");
                    } else {
                        sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Некорректный статус заявки (Выполнение). Ошибка отправки СМС.");
                    }
                    break;
                }
                $task->setPrice($price);
                $task->setOrientPrice($price);
                $task->setTicketDocNum($check);
                $task->updateCommit();

                $task->addComment("Сдача отчета по заявке. Номер квитанции: $check, сумма: $price, комментарий инженера: $comment", "Инженер выполнил заявку", 12);
                if (sendSMS($r["from"], "Ваш отчет по заявке ID: $task_id принят.")) {
                    sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Отчет принят. Уведомление отправлено.");
                } else {
                    sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "Отчет принят. Ошибка отправки СМС.");
                }
                $smsbody = "Ваш заказ №: ".trim($task->getId())." выполнен!\r";
                $smsbody .= "Для скидки в 5% к следующему заказу сообщите промо-код: gorserv нашему диспетчеру по тел.:\r";
                $smsbody .= getcfg('company_phone');
                $clnt = $task->getPhones();
                //foreach($clnt as $key => $item) {
                    if (!sendSMS(smsCleanPhone($clnt[0]), $smsbody)) {
                        $err[] = "Ошибка отправки смс на номер клиента: $item";
                    }
                //}
                //$u = $DB->getRow("SELECT * FROM `users` WHERE `fio`='Оператор ГСС';", true);
                                //adm_users_plugin::getUser($ticketAuthor);
                                //sendSMS(smsCleanPhone($phone), "Отказ от заявки с ID: ".$task->getId()." зафиксирован.");
                //if ($u["id"]) {
                //    $task->setIspoln($u["id"]);
                //}
                if (sizeof($err)) {
                    sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], implode(",\r\n Error: ", $err)."\r\n");
                }
                break;

            default:
                $notrecognized+=1;
                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], "неопознанное смс");
                break;
        }
    }
    $sql = "UPDATE `sms_incoming` SET `is_new`=0 WHERE `id` IN (".preg_replace("/,$/", "", implode(", ", $smsList)).");";
    $DB->query($sql);
    if ($DB->errno()) {
        echo "Ошибка БД: ".$DB->error()."\r\n";
        die();
    }
    $DB->free();
    echo "Нераспознанных смс: ".$notrecognized."\r\n";

} else {
    echo "Нет новых входящих смс.\r\n";
}
$DB->free();
echo "Обработка входящих смс завершена: ".date("H:i:s d.m.Y")."\r\n";
// - таймаут30 - назначение на инженера, таймаут120 - назначение в график, график - прибытие на заявку, график +50% времени на заявку - отчет по заявке
echo "Обработка статусов заявок СКП ".date("H:i:s d.m.Y")."\r\n";
// тайм-аут ожидания принятие заявки инженером
echo "Тайм-аут выбора инженера: "; //1800
//$sql = "SELECT tc.* FROM `task_comments` AS tc LEFT JOIN tasks AS t ON tc.task_id=t.id WHERE DATE_FORMAT(tc.datetime, '%Y-%m-%d')<=".$DB->F(date("Y-m-d"))." AND (UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(tc.datetime)>".getcfg('ticket_accept_timeout').") AND t.status_id=44 AND tc.status_id=44 GROUP BY tc.task_id";
$sql = "SELECT t.id FROM tasks AS t WHERE (UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(t.last_status_date)>".$DB->F(getcfg('ticket_accept_timeout')).") AND t.status_id=44;";
//echo $sql."\r\n";
$DB->query($sql);
echo intval($DB->num_rows())." заявок\r\n";
if ($DB->num_rows()) {
    while($r = $DB->fetch(true)) {
        $task = new ServiceTicket($r["id"]);
        /*$u = $DB->getRow("SELECT * FROM `users` WHERE `fio`='Оператор ГСС';", true);
        if ($u["id"]) {
            $task->setIspoln($u["id"]);
        } else {
            $task->addComment("Не возможно установить диспетчера задачи", "Ошибка");
        }*/
        $task->addComment("Таймаут подтверждения приема заявки инженером", "Таймаут", 42);
        // определение и проверка ном
    }
}
$DB->free();
//echo $sql."\r\n\r\n";
echo "Тайм-аут назначения в график: "; //7200
//$sql = "SELECT tc.* FROM `task_comments` AS tc LEFT JOIN tasks AS t ON tc.task_id=t.id WHERE DATE_FORMAT(tc.datetime, '%Y-%m-%d')<=".$DB->F(date("Y-m-d"))." AND (UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(tc.datetime)>".getcfg('ticket_gfx_timeout').") AND t.status_id=45 AND tc.status_id=45 GROUP BY tc.task_id";
$sql = "SELECT t.id FROM tasks AS t WHERE (UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(t.last_status_date)>".$DB->F(getcfg('ticket_gfx_timeout')).") AND t.status_id=45;";
$DB->query($sql);
echo intval($DB->num_rows())." заявок\r\n";
if ($DB->num_rows()) {
    while($r = $DB->fetch(true)) {
        $task = new ServiceTicket($r["id"]);
        /*$u = $DB->getRow("SELECT * FROM `users` WHERE `fio`='Оператор ГСС';", true);
        if ($u["id"]) {
            $task->setIspoln($u["id"]);
        } else {
            $task->addComment("Не возможно установить диспетчера задачи", "Ошибка");
        }*/
        $task->addComment("Таймаут назначения заявки в график инженером", "Таймаут", 42);

                            // определение и проверка ном
    }
}
$DB->free();
//echo $sql."\r\n\r\n";
//echo "Предупреждение инженеров о заявках за 30 мин до начала: ";
//$time = date("Hi");
//$targetTimeStart = intval(substr($time, 0, 2))*60+((intval(substr($time, 2, 2)) >= 0 && intval(substr($time, 2, 2)) < 30) ? 0 : 30);
//
//$sql = "SELECT g.task_id, g.empl_id, g.startTime FROM `gfx` AS g LEFT JOIN `tasks` AS t ON t.id=g.task_id WHERE g.c_date=".$DB->F(date("Y-m-d"))." AND (g.startTime-$targetTimeStart)<=30 AND (g.startTime-$targetTimeStart)>=20 AND t.plugin_uid='services' AND t.status_id=9;";
////echo "\r\n".$sql."\r\n";
//$DB->query($sql);
//echo intval($DB->num_rows())." заявок\r\n";
//if ($DB->num_rows()) {
//    $reminds = 0;
//    while($r = $DB->fetch(true)) {
//        $err = array();
//        $sql = "SELECT COUNT(id) FROM `task_comments` WHERE `task_id`=".$DB->F($r["task_id"])." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')=".$DB->F(date("Y-m-d"))." AND `text` LIKE '%Отправлено напоминание%';";
//        $r1 = $DB->getField($sql);
//        if ($r1==0) {
//            $reminds += 1;
//            $task = new ServiceTicket($r["task_id"]);
//            $ispolnArr = $task->getIspolnPhones();
//            $targetTime = floor(intval($r["startTime"])/60).":".(strlen(intval($r["startTime"])%60) ==1 ? "00" : intval($r["startTime"])%60);
//
//            $smsbody = "Заявка ".$task->getId()." на ".date("d-m")." в $targetTime. При переносе сообщите!";
//            echo $smsbody."\r\n";
//            foreach($ispolnArr as $key => $item) {
//                if (!sendSMS(smsCleanPhone($item), $smsbody)) {
//                    $err[] = "Ошибка отправки смс на номер техника: $item";
//                }
//            }
//            if (sizeof($err)) {
//                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], implode(",\r\n Error: ", $err)."\r\n");
//                $task->addComment(implode(", ", $err), "Ошибка отправки напоминания по СМС");
//
//            } else
//                $task->addComment("Отправлено напоминание технику", "Напоминание");
//        }
//                            // определение и проверка ном
//    }
//    echo " --- Отправлено напоминаний: ".$reminds."\r\n";
//}
//$DB->free();


/*echo "Тайм-аут по отчету: ";
$time = date("Hi");
$targetTimeStart = intval(substr($time, 0, 2))*60+intval(substr($time, 2, 2));

//$sql = "SELECT g.task_id, g.empl_id, g.startTime FROM `gfx` AS g LEFT JOIN `tasks` AS t ON t.id=g.task_id WHERE g.c_date=".$DB->F(date("Y-m-d"))." AND g.startTime<$targetTimeStart AND t.plugin_uid='services' AND t.status_id=9;";
$sql = "SELECT g.task_id, g.empl_id, g.startTime FROM `gfx` AS g LEFT JOIN `tasks` AS t ON t.id=g.task_id WHERE g.c_date=".$DB->F(date("Y-m-d"))." AND g.startTime<$targetTimeStart AND t.plugin_uid='services' AND t.status_id=9;";

$DB->query($sql);
if ($DB->num_rows()) {
    $reminds = 0;
    while($r = $DB->fetch(true)) {
        $task = new ServiceTicket($r["task_id"]);
        $targetTime = floor($task->getTypeDuration()) + $task->getTypeDuration()/2 + $r["startTime"];
        if ($targetTimeStart>$targetTime && $task->getTypeName() == "СКП") {
            $reminds+=1;
            $smsbody = "Вы не отчиталиcь по заявке №".$task->getId()." Срочно свяжитеь с диспетчером!!.";
            $ispolnArr = $task->getIspolnPhones();
            foreach($ispolnArr as $key => $item) {
                if (adm_empl_plugin::usesSms($key)) {
                    if (!sendSMS(smsCleanPhone($item), $smsbody)) {
                        $err[] = "Ошибка отправки смс на номер техника: $item";
                    }
                } else {
                    $err[] = "Техник не использует СМС";
                }
            }
            if (sizeof($err)) {
                sms_log($r["id"], $r["message_id"], $r["from"], $r["text"], implode(",\r\n Error: ", $err)."\r\n");
            }
            /*$u = $DB->getRow("SELECT * FROM `users` WHERE `fio`='Оператор ГСС';", true);
            if ($u["id"]) {
                $task->setIspoln($u["id"]);
            } else {
                $task->addComment("Не возможно установить диспетчера задачи", "Ошибка");
            }*/

           /* $task->addComment("Отчет техника не получен", "Тайм-аут", 42);
        }
    }

}
echo intval($reminds)." заявок\r\n";

$DB->free();*/

echo "Тайм-аут по текущим суткам: \r\n";
$sql = "SELECT g.task_id, g.empl_id, g.c_date FROM `gfx` AS g LEFT JOIN `tasks` AS t ON t.id=g.task_id WHERE ((g.c_date<".$DB->F(date("Y-m-d"))." AND (t.status_id=9 OR t.status_id=47)) OR (t.status_id=50 AND t.date_fn<=".$DB->F(date("Y-m-d", strtotime(date("Y-m-d")." - 1 day"))).")) AND t.plugin_uid='services' ;";
echo $sql."\r\n";
$DB->query($sql);
if ($DB->num_rows()) {
    $reminds = 0;
    while($r = $DB->fetch(true)) {
        $task = new ServiceTicket($r["task_id"]);
            if ($task->getTypeName() == "СКП") {
                $sql = "SELECT `count` FROM `skp_tech_timeouts` WHERE `rdate`=".$DB->F(date("Y-m-d"))." AND `tech_id`=".$DB->F($r["empl_id"]).";";
                $m = $DB->query($sql);
                if ($DB->num_rows()) {
                    $cc = $DB->fetch(true);
                    $sql = "UPDATE `skp_tech_timeouts` SET `count`=".$DB->F($cc["count"]+1)." WHERE `rdate`=".$DB->F(date("Y-m-d"))." AND `tech_id`=".$DB->F($r["empl_id"]).";";
                } else {
                    $sql = "INSERT INTO `skp_tech_timeouts` (`tech_id`, `rdate`, `count`) VALUES (".$DB->F($r["empl_id"]).", ".$DB->F(date("Y-m-d")).", ".$DB->F(1).");";
                }
                $DB->free($m);
                $m = $DB->query($sql);
                $DB->free($m);
                $task->addComment("Суточный тайм-аут", "Тайм-аут", 42);
            }
    }

}
echo intval($DB->num_rows())." заявок\r\n";

$DB->free();


echo "Обработка статусов заявок СКП завершена: ".date("H:i:s d.m.Y")."\r\n";
echo "-------------------------------------------------------------------------------\r\n";

function sms_log($id, $hash, $from, $text, $msg) {
    echo "--- MSG_ID: ".$id." ID: ".$hash." RESULT: --- ".$msg." --- Sender: ".$from." SMS: ".$text."\r\n";
}
