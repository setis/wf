<?php
use classes\UserSession;

set_time_limit(600);
error_reporting(E_ALL);

require_once __DIR__ .'/../bootstrap.php';

$DB = wf::$container->get('wf.mysql_db');

$notrecognized = 0;
echo "-------------------------------------------------------------------------------\r\n";
echo "Поиск задолженностей по заявкам СКП (нал): ".date("H:i:s d.m.Y")."\r\n";
$doneStatus = adm_statuses_plugin::getStatusByTag("done", "services");
$countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
$completeStatus = adm_statuses_plugin::getStatusByTag("closed", "services");
$otkaz = adm_statuses_plugin::getStatusByTag("otkaz", "services");
$opotkaz = adm_statuses_plugin::getStatusByTag("opotkaz", "services");
$accepted = adm_statuses_plugin::getStatusByTag("acc_accepted", "services");
$add_filter = "AND tick.cnt_id IN (SELECT DISTINCT(`contr_id`) FROM `list_contr_agr` WHERE `paytype`!='2' AND `id` IN (SELECT `agr_id` FROM `link_agr_wtypes` WHERE `wtype_id`='10'))";            



$sql = "SELECT u.id FROM `users` as u WHERE u.active ;";
$techList = $DB->getCell($sql);
$sql = "TRUNCATE TABLE `skp_tech_debts`;";
$m = $DB->query($sql);
$DB->free($m);
foreach($techList as $item) {
    $sql_notpaid = "SELECT SUM(tick.orient_price) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                            WHERE 
                                
                                t.id NOT IN (SELECT `task_id` FROM `skp_money`) AND  t.id IN (SELECT g.task_id FROM `gfx` AS g WHERE g.empl_id = ".$DB->F($item)." GROUP BY g.task_id) 
                                AND t.plugin_uid='services' AND tick.task_type='10'  AND 
                                (t.status_id=".$DB->F($doneStatus["id"])." OR 
                                t.status_id=".$DB->F($countStatus["id"])." OR 
                                t.status_id=".$DB->F($completeStatus["id"])." OR 
                                t.status_id=".$DB->F($accepted["id"]).")
                                AND t.id IN (SELECT task_id FROM `task_comments` WHERE 
                                (`status_id`=".$DB->F($doneStatus["id"])." )
                                AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime("2014-08-01")))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d")).")".$add_filter.";";
    //echo $sql_notpaid;
    $ammount = $DB->getField($sql_notpaid);
    if (intval($ammount) >= 0) {
        $sql = "DELETE FROM `skp_tech_debts` WHERE `tech_id`=".$DB->F($item).";";
        $DB->query($sql);
        echo "Tech ID: ".$item." Ammount: ".$ammount."\r\n";
        $sql = "INSERT INTO `skp_tech_debts` (`tech_id`, `ammount`) VALUES (".$DB->F($item).", ".$DB->F($ammount).");";
        $m = $DB->query($sql);
        $DB->free($m);
    } 
    
}
echo "-------------------------------------------------------------------------------\r\n";
