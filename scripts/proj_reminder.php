<?php
use classes\Task;
use classes\UserSession;

set_time_limit(600);
error_reporting(E_ALL);
require_once __DIR__ .'/../bootstrap.php';

$DB = wf::$container->get('wf.mysql_db');
echo "-------------------------------------------------\r\n";
echo "Напоминания о проектах ".date("d.m.Y")."\r\n";
$sql = "SELECT * FROM `projects` WHERE `remind` AND DATE_FORMAT(`remdate`, '%Y-%m-%d')=".$DB->F(date("Y-m-d")).";";
//echo $sql."\r\n";
$DB->query($sql);
if ($DB->num_rows()) {
    while ($r = $DB->fetch(true)) {
        $t = new Task($r["task_id"]);
        echo date("H:i:s d.m.Y"). " Отправлено напоминание всем связанным лицам. Проект: ".$t->getId();
        error_reporting(E_ALL);
        if ($t->toMail("Напоминание о проекте! ")) {
            echo " Успешно!";
        } else {
            echo " Ошибка!";
        }
        echo "\r\n";
    }
    $DB->free();
    $DB->query("UPDATE `projects` SET `remind`=0, `remdate`='0000-00-00' WHERE `remind` AND DATE_FORMAT(`remdate`, '%Y-%m-%d')=".$DB->F(date("Y-m-d")).";");
    if ($DB->errno()) echo $DB->error()."\r\n";
    else 
        echo "Напоминания уничтожены!\r\n";
} else {
    echo "Нет проектов для напоминания!\r\n";
}


echo "-------------------------------------------------\r\n";
echo "Отложенные счета ".date("d.m.Y")."\r\n";
$sql = "SELECT * FROM `bills` WHERE `delayed` AND DATE_FORMAT(`delayed`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d")).";";
//echo $sql."\r\n";
$DB->query($sql);
if ($DB->num_rows()) {
    while ($r = $DB->fetch(true)) {
        $t = new Task($r["task_id"]);
        if ($t->getStatusId() == 38) {
            $count +=1;
            //$sql = "SELECT `status_id` FROM `task_comments` WHERE `task_id`=".$DB->F($r["task_id"])." AND status_id NOT IN (35,38,39,40) AND `datetime` IN (SELECT `datetime` FROM `task_comments` WHERE 1 ORDER BY `datetime` DESC) ORDER BY `datetime` DESC LIMIT 1;";
            
            //$target = $DB->getField($sql);
            //if (!$target) $target = 33;
            $t->addComment("","Наступила дата, до которой этот счет был отложен. Автоматический возврат к статусу Новый.", 33);
            $t->toMail();
            echo $r["task_id"]."->status:". $target." ";
        }
    }
    if (!$count)
        echo "Отсутствуют отложенные до ".date("d.m.Y")." счета!\r\n";
    else echo "\r\n";
    $DB->free();
    
} else {
    echo "Отсутствуют отложенные до ".date("d.m.Y")." проекты!\r\n";
}
