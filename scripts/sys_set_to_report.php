<?php
use classes\tickets\ConnectionTicket;
use classes\UserSession;

header("Content-type: text/plain");
set_time_limit(600);

require_once __DIR__ .'/../bootstrap.php';

$DB = wf::$container->get('wf.mysql_db');
$connection = new ConnectionTicket();
$connCount = $connection->setToReportTickets();
if ($connCount !== FALSE) {
    
    echo date("d-m-Y H:i:s") . " Статус \"Сдача отчета\" - Заявки на Подключение: ".$connCount."\r\n";
} else {
    echo date("d-m-Y H:i:s") . " Статус \"Сдача отчета\" - Заявки на Подключение: ОШИБКА!\r\n";
    
}
echo "--------------------------------\r\n";
