<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 *  mail@artemd.ru
 * 03.12.2015
 */
use classes\mailbox\IncomingMail;
use classes\tickets\ServiceTicket;
use classes\UserSession;

set_time_limit(600);

require_once __DIR__ .'/../bootstrap.php';

$DB = wf::$container->get('wf.mysql_db');

function parseMail(IncomingMail $message){
    if(!empty($message->plain)) {
        $text = $message->plain;
    } else {
        $text = strip_tags($message->html);
    }

    if(preg_match('/Номер заявки\: (.+)Имя клиента\: (.+)Телефон клиента: (.+)Город: (.+)Район: (.+)/i', $text, $result) !== false) {
        array_shift($result);
        return $result;
    }

    return false;
}


$counterparty_id = 498; // ВымпелКом ПАО
$task_type = 10; // СКП
$task_from = 3; // От партнеров (операторов связи)

$mailbox = new \classes\mailbox\Mailbox(array(
    'mailbox'=>'{imap.yandex.ru:993/imap/ssl}INBOX',
    'username'=>"beeline-skp@gorserv.ru",
    'password'=>"BeeGSS$$2020"
));

$messages = $mailbox->getMailList();
foreach($messages as $message) {
    /** @var IncomingMail $message */

    $message->attachments = array();
    list($external_ticket_number, $client_name, $client_phone, $client_city, $client_district) = parseMail($message);

    if(empty($external_ticket_number)  || ServiceTicket::hasExternalIdExists( $external_ticket_number, $counterparty_id )) {
        echo "[warning] $message->id Task with external ID $external_ticket_number in counterparty $counterparty_id already exists. Skipping...\n";
        continue;
    }

    $task_comment = "Автоматическое заведение заявки от контрагента\r\n" .
        "№ заявки в БД контрагента: ".$external_ticket_number."\r\n" .
        "Дата заведения завки у контрагента: ".date("d.m.Y H:i:s", $message->date)."\r\n" .
        "Адрес клиента: $client_city $client_district\r\n";

    /*
     * $cnt_id,
     * $type_id,
     * $dom_id,
     * $pod,
     * $etazh,
     * $kv,
     * $domofon,
     * $clnt_type,
     * $org_name,
     * $fio,
     * $passp_sn,
     * $passp_vidan,
     * $passp_date,
     * $phone1,
     * $phone2,
     * $ispoln_ids,
     * $cmm,
     * $addinfo,
     * $agr_id,
     * $swip,
     * $swport,
     * $swplace,
     * $clntopagr,
     * $clnttnum,
     * $sc_id = "0",
     * $orient,
     * $task_from,
     * $extwtype,
     * $istest
     */
    $task_id = \classes\tickets\ServiceTicket::createTicket(
        $counterparty_id, /*cnt_id*/
        $task_type, /*type_id*/
        0, /*dom_id*/
        '', /*pod*/
        '', /*etazh*/
        '', /*kv*/
        '', /*domofon*/
        1, /*clnt_type*/
        '', /*org_name*/
        $client_name, /*fio*/
        '', /*passp_sn*/
        '', /*passp_vidan*/
        '', /*passp_date*/
        $client_phone, /*phone1*/
        '', /*phone2*/
        0, /*ispoln_ids*/
        $task_comment, /*cmm*/
        '', /*addinfo*/
        0, /*agr_id*/
        '', /*swip*/
        '', /*swport*/
        '', /*swplace*/
        '', /*clntopagr*/
        $external_ticket_number, /*clnttnum*/
        '', /*sc_id*/
        '', /*orient*/
        $task_from, /*task_from*/
        '', /*extwtype*/
        false /*istest*/
    );

    if($task_id) {
        d($task_id);
    }

}
