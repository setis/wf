<?php

use classes\tickets\ServiceTicket;
use classes\UserSession;

set_time_limit(600);

require_once __DIR__ . '/../bootstrap.php';

error_reporting(E_ALL);
ini_set("display_errors", 1);

function findAddress($addr_city, $addr_street, $addr_building)
{
    $data = [
        'city' => $addr_city,
        'street' => $addr_street,
        'house' => $addr_building,
    ];

    try {
        $listAddr = \wf::$em->getRepository(\models\ListAddr::class)
            ->setAddressProvider(\wf::$container->get('wf.address.provider'))
            ->findOrCreateByAddressArray($data, [
                'count' => 1
            ]);
    } catch (Exception $e) {
        $listAddr = null;
    }

    if(null !== $listAddr && $listAddr->isNotRecognized()) {
        print_r($data);
        echo $listAddr->getFullAddress()."\n";
    }

    return $listAddr;
}

$DB = wf::$container->get('wf.mysql_db');
/* @var $logger \Psr\Log\LoggerInterface */
$logger = wf::$container->get('monolog.logger.console');

$nbn_processed = array();

try {
    $client = new SoapClient(getcfg('nbn_soap_url'));

    echo date("Y-m-d H:i:s") . " Executing getNewTickets...\r\n";
    $data = new stdClass();
    $data->login = getcfg('nbn_soap_login');
    $data->password = getcfg('nbn_soap_pass');

    $result = $client->getNewTickets($data);
    $logger->info(print_r($result, true));
    if ($result->code != 200) {
        echo "Got error code {$result->code}: " . $result->message . "\r\n";
        exit;
    }
    //print_r($result); exit;
    echo "Got " . sizeof($result->tickets) . " new tickets.\r\n";
    if (is_array($result->tickets) && count($result->tickets) > 0) {
        foreach ($result->tickets as $nbn_ticket) {
            $sql = "SELECT `task_id` FROM `contr_tickets` WHERE `contr_id`=" . $DB->F(getcfg('nbn_contr_id')) . " AND `contr_ticket_id`=" . $DB->F($nbn_ticket->ticket_id);
            if ($task_id = $DB->getField($sql, false)) {
                echo "Ticket {$nbn_ticket->ticket_id} already exists (task_id $task_id)!\r\n";
                $nbn_processed[] = $nbn_ticket->ticket_id;
            } else {
                $contr_addr = $nbn_ticket->address;
                $a = explode(",", trim($contr_addr));
                $bname = explode(" ", trim($a[3]));

                $listAddr = findAddress(trim($a[0]), trim($a[2]), trim($bname[0]));
                $sc = [];
                if (null !== $listAddr && null !== $listAddr->getCoordinates()) {
                    $sc = \wf::$em->getRepository(\models\ListSc::class)->getServiceCentersByPoint($listAddr->getCoordinates(), [
                        'task_type' => wf::$em->getReference(\models\TaskType::class, getcfg('nbn_type_id'))
                    ]);
                }

                $cmm = "Автоматическое заведение заявки от контрагента\r\n" .
                    "№ заявки в БД контрагента: {$nbn_ticket->ticket_id}\r\n" .
                    "Дата заведения завки у контрагента: " . date("d.m.Y H:i:s", strtotime($nbn_ticket->created_at)) . "\r\n" .
                    "Адрес клиента: $contr_addr\r\n" .
                    "Комментарий клиента:\r\n\r\n" . $nbn_ticket->comment . "\r\n";

                $task_id = ServiceTicket::createTicket(
                    getcfg('nbn_contr_id'),
                    getcfg('nbn_type_id'),
                    null === $listAddr? null : $listAddr->getId(),
                    '',
                    '',
                    '',
                    '',
                    1,
                    '',
                    $nbn_ticket->name,
                    '',
                    '',
                    '',
                    '7' . ltrim(preg_replace("/[^0-9]/", '', $nbn_ticket->phone), '78'),
                    '',
                    0,
                    $cmm,
                    $nbn_ticket->comment,
                    getcfg('nbn_agr_id'),
                    '',
                    '',
                    '',
                    '',
                    $nbn_ticket->ticket_id,
                    !empty($sc)?reset($sc)->getId():null
                );
                if ($task_id) {
                    $sql = "INSERT INTO `contr_tickets` (`task_id`, `contr_id`, `contr_ticket_id`, `contr_addr`, `dom_id`) VALUES(" . $DB->F($task_id) . ", " . $DB->F(getcfg('nbn_contr_id')) . ", " . $DB->F($nbn_ticket->ticket_id) . ", " . $DB->F($contr_addr, true) . ", " . $DB->F(null === $listAddr ? null : $listAddr->getId(), true) . ")";
                    $DB->query($sql);
                    if ($DB->errno())
                        echo "Failed to add {$nbn_ticket->ticket_id} (task_id $task_id) to contr_tickets!\r\n";
                    else {
                        $nbn_processed[] = $nbn_ticket->ticket_id;
                        echo "Successfully added {$nbn_ticket->ticket_id} to task $task_id\r\n";
                    }
                } else
                    echo "Failed to create ServiceTicket for {$nbn_ticket->ticket_id}\r\n";
            }
        }
    }

    //print_r($nbn_processed); exit;
    if (!sizeof($nbn_processed))
        die("Array of NBN processed tickets is empty, exiting.\r\n");
    echo "Executing markProcessedTickets...\r\n";
    $data = new stdClass();
    $data->login = getcfg('nbn_soap_login');
    $data->password = getcfg('nbn_soap_pass');
    $data->tickets_id = $nbn_processed;
    $result = $client->markProcessedTickets($data);
    if ($result->code != 200) {
        echo "Got error code {$result->code}: " . $result->message . "\r\n";
        //exit;
    }
    echo date("Y-m-d H:i:s") . " Done.\r\n";
} catch (SoapFault $e) {
    echo date("Y-m-d H:i:s") . " Exeption! " . $e->getMessage() . "\r\n";
}
