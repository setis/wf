<?php
/**
 * Ручная загрузка какого то локального файла с заявками Ростелек
 * 
 * 
 * 
 */


use classes\tickets\AccidentTicket;
use classes\User;

require_once __DIR__ . '/../bootstrap.php';

$DB = wf::$container->get('wf.mysql_db');
error_reporting(E_ALL);

echo "Started at " . date("Y-m-d H:i:s") . "\r\n";
echo print_r($DB,true);
#exit;

###### Город Москва
$row = 0;

$seen_addresses = array();
$uniq_clients = array();
$cmms = array();

if (1 == 0) {
    if (($handle = fopen("../../m.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {
            #$num = count($data);
            #echo "<p> $num полей в строке $row: <br /></p>\n";
            #for ($c=0; $c < $num; $c++) {
            #    #echo $data[$c] . "\n";
            #}
            $phones = array();
            if (($row > 0) && ($row > 0)) { ### пропускаем строку с заголовком файла
                $ls = $data[0];
                $address = $data[1];
                $phones[0] = $data[2];
                $phones[1] = $data[3];
                $sn = $data[4];
                $name = $data[5];

                #$obl = "Московская обл";
                $city = "г. Москва";
                $street = "";
                $houese = "";
                $korp = "";
                $str = "";
                $dom = "";
                $kv = "";
                $sc_id = 8;

                $matches = array();
                if (preg_match("/^(.*?),/",$address,$matches)) {
                    $street = $matches[1];
                }
                if (preg_match("/д\.(.*?),/",$address,$matches)) {
                    $dom = $matches[1];
                }
                if (preg_match("/кв\.(.*?)$/",$address,$matches)) {
                    $kv = trim($matches[1]);
                }
                if (preg_match("/корп\.(.*?)(,|$)/",$address,$matches)) {
                    $korp = $matches[1];
                    $dom .="к".$korp;
                }
                if (preg_match("/стр\.(.*?)(,|$)/",$address,$matches)) {
                    $str = $matches[1];
                    $dom .="стр".$str;
                }

                $cmm = $name." серийный номер ".$sn;

                $seen_addresses[$address]++;
                $uniq_clients[$address] = array ($phones[0], $phones[1], 'г. Москва', $street, $dom, $kv, $ls );
                if (!in_array($cmm,$cmms[$address])) {
                    $cmms[$address][] = $cmm;
                }
                #### addinfo: Примечание. Сюда сохраняем информацию о дополнительном оборудовании на этом же адресе.
            }
            $row++;
        }
        fclose($handle);

        #print_r($seen_addresses);
        #print_r($uniq_clients);
        #print_r($cmms);
        #exit;

        foreach ($seen_addresses as $key => $val) {
            $phones[0] = $uniq_clients[$key][0];
            $phones[1] = $uniq_clients[$key][1];
            $city_name = $uniq_clients[$key][2];
            $street = $uniq_clients[$key][3];
            $dom = $uniq_clients[$key][4];
            $kv = $uniq_clients[$key][5];
            $ls = $uniq_clients[$key][6];
            $additional_info = join("\n",$cmms[$key]);
            $cmm = $key."\n\n".$additional_info;
            $sc_id = 8;
            #echo $city_name."\n";
            $dom_id = 0;
            #echo "DOM = [".$dom."]\n";
            #echo "Before creating ticket(1): ".$ls.", ".$street.", д. ".$dom.", кв. ".$kv.", [".$dom_id."]\n";
            $dom_id = addr_interface_plugin::getKLADRCodeByAddressNBN(array(
                'city_name' => $city_name,
                'street_name' => $street,
                'building_name' => $dom
            ));
            #echo "Before creating ticket(2): ".$ls.", ".$street.", д. ".$dom.", кв. ".$kv.", [".$dom_id."]\n";
            $task_id = AccidentTicket::createTicket(
                70, ### Ростелеком
                39, ### Тип работ 39, демонтаж оборудования
                $dom_id,
                '',
                '',
                $kv,
                '',
                User::CLIENT_TYPE_PHYS,
                '',
                '', //// $a['fio']
                $phones[1],
                $phones[0],
                0,
                $cmm,
                $additional_info,
                289, ## номер договора
                '',
                '',
                '',
                '',
                $ls,
                $sc_id,
                17, ### откуда узнали - РТ.
                date('Y-m-d'),
                date('Y-m-d'),
                '',
                ''
            );
            echo $ls.", ".$street.", д. ".$dom.", кв. ".$kv.", [".$dom_id."] => ".$task_id."\n";
            #print_r($cmms[$key]);
            #exit;
        }
    }
} # if (1 == 1)

###### Московская область
if (1 == 1) {
    $row = 0;
    $seen_addresses = array();
    $uniq_clients = array();
    $cmms = array();
    if (($handle = fopen("../../mo.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {
            #$num = count($data);
            #echo "<p> $num полей в строке $row: <br /></p>\n";
            #for ($c=0; $c < $num; $c++) {
            #    #echo $data[$c] . "\n";
            #}
            $phones = array();
            if (($row > 0) && ($row > 0)) { ### пропускаем строку с заголовком файла
                $ls = $data[0];
                $address = $data[1];
                $sn = $data[3];
                $name = $data[2];
                $fio = $data[4];

                $obl = "Московская обл";
                $city = "";
                $street = "";
                $real_street = $street;
                $houese = "";
                $korp = "";
                $str = "";
                $dom = "";
                $rn = "";
                $kv = "";
                $sc_id = 8;

                $matches = array();
                if (preg_match("/^(.*?)\s+ОБЛ\.,/",$address,$matches)) {
                    $obl = $matches[1]." область";
                }
                if (preg_match("/^(.*?)\s+Р-Н\.,/",$address,$matches)) {
                    $rn = $matches[1]. " район";
                }
                if (preg_match("/П\.(.*?)\s+/",$address,$matches)) {
                    $city = "поселок ".$matches[1];
                }
                if (preg_match("/Г\.(.*?)\s+/",$address,$matches)) {
                    $city = "город ".$matches[1];
                }
                if (preg_match("/ПГТ\.(.*?)\s+/",$address,$matches)) {
                    $city = "пгт ".$matches[1];
                }
                if (preg_match("/С\.(.*?)\s+/",$address,$matches)) {
                    $city = "село ".$matches[1];
                }
                if (preg_match("/ПР-КТ.\.(.*?)\s+\S+\s+корп\./",$address,$matches)) {
                    $street = "проспект ".$matches[1];
                    $real_street = $matches[1];
                }
                if (preg_match("/УЛ\.(.*?)\s+\S+\s+корп\./",$address,$matches)) {
                    $street = "улица ".$matches[1];
                    $real_street = $matches[1];
                    #echo "Нашел улицу ".$matches[1]."\n";
                }
                if (preg_match("/Ш\.(.*?)\s+\S+\s+корп\./",$address,$matches)) {
                    $street = "шоссе ".$matches[1];
                    $real_street = $matches[1];
                }
                if (preg_match("/ПЕР\.(.*?)\s+\S+\s+корп\./",$address,$matches)) {
                    $street = "переулок ".$matches[1];
                    $real_street = $matches[1];
                }
                if (preg_match("/МКР\.(.*?)\s+\S+\s+корп\./",$address,$matches)) {
                    $street = "микрорайон ".$matches[1];
                    $real_street = $matches[1];
                }
                if (preg_match("/Б-Р\.(.*?)\s+\S+\s+корп\./",$address,$matches)) {
                    $street = "бульвар ".$matches[1];
                    $real_street = $matches[1];
                }

                ## вычисления номера дома
                $pattern = "/".$real_street."\s+(.*?)\s+корп\./";
                #echo "Паттерн для поиска дома = ".$pattern."\n";
                if (preg_match($pattern,$address,$matches)) {
                    #$dom = "дом ".$matches[1];
                    $dom = $matches[1];
                    #echo "Нашел ДОМ! ".$matches[1]."\n";
                }            

                if (preg_match("/корп\.(.*?)\s+кв\.$/",$address,$matches)) {
                    $korp = trim($matches[1]);
                    if (strlen($korp) > 0) {
                        $dom .= "к".$korp;
                    }
                }
                if (preg_match("/кв\.(.*?)$/",$address,$matches)) {
                    $kv = trim($matches[1]);
                }

                $phones[0] = "";
                $phones[1] = "";

                if (preg_match("/(\d+),\s+(\d+)/",$data[5],$matches)) {
                    $phones[0] = $matches[1];
                    $phones[1] = $matches[2];
                }

                if (preg_match("/(\d+)/",$data[5],$matches)) {
                    $phones[0] = $matches[1];
                }

                $cmm = $name." серийный номер ".$sn;
                $seen_addresses[$address]++;
                $uniq_clients[$address] = array ($phones[0], $phones[1], $city, $street, $dom, $kv, $ls, $obl, $rn, $fio );
                if (!in_array($cmm,$cmms[$address])) {
                    $cmms[$address][] = $cmm;
                }
            }
            $row++;
        }
        fclose($handle);
        #print_r($seen_addresses);
        #print_r($uniq_clients);
        #print_r($cmms);
        #exit;

        foreach ($seen_addresses as $key => $val) {
            $phones[0] = $uniq_clients[$key][0];
            $phones[1] = $uniq_clients[$key][1];
            $city_name = $uniq_clients[$key][2];
            $street = $uniq_clients[$key][3];
            $dom = $uniq_clients[$key][4];
            $kv = $uniq_clients[$key][5];
            $ls = $uniq_clients[$key][6];
            $obl = $uniq_clients[$key][7];
            $rn = $uniq_clients[$key][8];
            $fio = $uniq_clients[$key][9];
            $additional_info = join("\n",$cmms[$key]);
            $cmm = $additional_info;
            $sc_id = 8;
            #echo $city_name."\n";
            $dom_id = 0;
            $dom_id = addr_interface_plugin::getKLADRCodeByAddressNBN(array(
                    'area_name' => $obl,
                    'region_name' => $rn,
                    'city_name' => $city_name,
                    'street_name' => $street,
                    'building_name' => $dom
                ));


            $task_id = AccidentTicket::createTicket(
                70, ### Ростелеком
                39, ### Тип работ 39, демонтаж оборудования
                $dom_id,
                '',
                '',
                $kv,
                '',
                User::CLIENT_TYPE_PHYS,
                '',
                $fio, //// $a['fio']
                $phones[1],
                $phones[0],
                0,
                $key."\n".$additional_info,
                $additional_info,
                289, ## номер договора
                '',
                '',
                '',
                '',
                $ls,
                $sc_id,
                17, ### откуда узнали - РТ.
                date('Y-m-d'),
                date('Y-m-d'),
                '',
                ''
            );
            echo $ls.", ".$obl.", ".$rn.", ".$city_name.", ".$street.", д. ".$dom.", кв. ".$kv.", [".$dom_id."] => ".$task_id."\n";
        }
    }
} # if (1 == 0)
