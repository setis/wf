<?php
use classes\tickets\AccidentTicket;
use classes\tickets\ConnectionTicket;
use classes\tickets\ServiceTicket;
use classes\UserSession;

header("Content-type: text/plain");
set_time_limit(600);

require_once __DIR__ .'/../bootstrap.php';

$DB = wf::$container->get('wf.mysql_db');

$connCount = ConnectionTicket::setFailedTickets();
$serviceCount = ServiceTicket::setFailedTickets();
$accidents = AccidentTicket::setFailedTickets();

if ($connCount !== FALSE) {
    echo date("d-m-Y H:i:s") . " Статус \"Не выполнена\" - Заявки на Подключение: ".$connCount."\r\n";
} else {
    echo date("d-m-Y H:i:s") . " Статус \"Не выполнена\" - Заявки на Подключение: ОШИБКА!\r\n";

}
if ($serviceCount !== FALSE) {
    echo date("d-m-Y H:i:s") . " Статус \"Не выполнена\" - Заявки на Сервисное обслуживание и Ремонт: ".$serviceCount."\r\n";
} else {
    echo date("d-m-Y H:i:s") . " Статус \"Не выполнена\" - Заявки на Сервисное обслуживание и Ремонт: ОШИБКА!\r\n";

}
if ($accidents !== FALSE) {
    echo date("d-m-Y H:i:s") . " Статус \"Не выполнена\" - Заявки на Аварии: ".$accidents."\r\n";
} else {
    echo date("d-m-Y H:i:s") . " Статус \"Не выполнена\" - Заявки на Аварии: ОШИБКА!\r\n";

}
echo "--------------------------------\r\n";
