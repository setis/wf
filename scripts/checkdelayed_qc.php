<?php

use classes\Task;
use classes\UserSession;

set_time_limit(600);

require_once __DIR__ .'/../bootstrap.php';

$DB = wf::$container->get('wf.mysql_db');

# EF: Исправляю запрос, чтобы сегодняшние заявки (на будущее время) не обнулялись, а оставались в статусе 3
#$sql = "SELECT `id`, `task_id` FROM `tickets_qc` WHERE DATE_FORMAT(`qc_delayed`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d"))." AND `qc_status`='3';";
$sql = "SELECT `id`, `task_id` FROM `tickets_qc` WHERE qc_delayed < now() AND `qc_status`=3;";
$DB->query($sql);
echo "Started: ".date("H:i d-m-Y")."\r\n";
echo "Total records: ".$DB->num_rows()."\r\n";
$i = 0;
while ($r = $DB->fetch(true)) {
    $sql = "UPDATE `tickets_qc` SET `qc_status`='0' WHERE `id`=".$DB->F($r["id"]).";";
    $DB->query($sql);
    $t = new Task($r["task_id"]);
    if ($t) {
        if ($t->addComment("", "Автоматическая установка статуса обзвона Новый при наступлении даты Отложено")) {
            echo "Status set: ".$r["id"]."\r\n";
            $i+=1;
        } 
    
    } else {
        echo "Error creating task: ".$r["id"]."\r\n";
    } 
    $DB->free();
}
echo "Updated: $i \r\n-----------------------------\r\n";
$DB->free();

