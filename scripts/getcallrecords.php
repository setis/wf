<?php
use classes\UserSession;

set_time_limit(600);

require_once __DIR__ .'/../bootstrap.php';

$DB = wf::$container->get('wf.mysql_db');
/* @var $fs \League\Flysystem\FilesystemInterface */
$fs = wf::$container->get('oneup_flysystem.call_records_filesystem');

echo "Started at ".date("Y-m-d H:i:s")."...\r\n";
$sql = "SELECT * FROM `tickets_calls` WHERE `status`=0 OR `status`=2;";
$DB->query($sql);
if ($DB->num_rows()) {
    while ($r = $DB->fetch(true)) {
        //print_r($r);
        $r["calldate"] = implode(",",explode(" ", $r["calldate"]));
        if ($r["techphone"]!= "")
            $url = getcfg('asterisk_host')."aster_db.php?from=4957390940&to=".$r["techphone"]."&cdate=".$r["calldate"].'&key='.$r["id"];
        else
            $url = getcfg('asterisk_host')."aster_db.php?from=".$r["callfrom"]."&to=".$r["callto"]."&cdate=".$r["calldate"].'&key='.$r["id"];
        echo "URL: ".$url."\r\n";
        ob_start();
        $cURL = curl_init();

        curl_setopt($cURL, CURLOPT_URL, $url);
        curl_setopt($cURL, CURLOPT_HTTPGET, true);
        curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Accept: application/json'
        ));
        $result = @curl_exec($cURL);
        @curl_close($cURL);
        $r1 = ob_get_contents();
        ob_end_clean();
        $r1 = json_decode($r1, true);
        //echo $r1['sql'].'\r\n';
        if ($r1["ok"] == "ok" && $r1["file"] == "ok" && $r1["rc"] == "1" && $r1["pathtofile"] != "") {
            $sql = "UPDATE `tickets_calls` SET
                    `asteriskstatus`=".$DB->F($r1["disposition"]).",
                    `asteriskdate`=".$DB->F($r1["calldate"]).",
                    `asteriskfile`=".$DB->F($r1["recordingfile"]).",
                    `asteriskuid`=".$DB->F($r1["uniqueid"]).",
                    `asteriskpathtofile`=".$DB->F($r1["pathtofile"])."
                    WHERE `id`=".$DB->F($r["id"]).";";
            $DB->query($sql);
            echo "Updating CID: ". $r["id"]."...";
            if ($DB->errno()) {
                echo "FAIL - ".$DB->error()."\r\n";
            } else  {
                echo "OK - FILE: ".$r1["recordingfile"];
                $targetLink = getcfg('asterisk_host')."aster_get.php?targetpath=".base64_encode($r1["pathtofile"])."&target=".base64_encode($r1["recordingfile"]);

                $file = file_get_contents($targetLink);
                if ($fs->put($r1["recordingfile"], $file)) {
                    echo "...DOWNLOAD OK\r\n";
                    if ($r["techphone"]!="") {
                        if ($r["status"] == "2") {
                            $sql = "UPDATE `tickets_calls` SET `status`=".$DB->F(1)." WHERE `id`=".$DB->F($r["id"]).";";
                        } else {
                            $sql = "UPDATE `tickets_calls` SET `status`=".$DB->F(2)." WHERE `id`=".$DB->F($r["id"]).";";
                        }
                    } else {
                        $sql = "UPDATE `tickets_calls` SET `status`=".$DB->F(1)." WHERE `id`=".$DB->F($r["id"]).";";
                    }
                    $DB->query($sql);
                    if ($DB->errno()) echo "MySQL-Error: ".$DB->error()."\r\n";
                    $DB->free();
                } else {
                    echo "...DOWNLOAD FAIL\r\n";
                }
            }
            $DB->free();
        } else {
            echo "RECORD_ID: ".$r["id"]." marked as ";
            if ($r["status"] == "2") {
                echo "FAILED\r\n";
                $sql = "UPDATE `tickets_calls` SET `status`=".$DB->F(3)." WHERE `id`=".$DB->F($r["id"]).";";
            } else {
                echo "RETEST\r\n";
                $sql = "UPDATE `tickets_calls` SET `status`=".$DB->F(2)." WHERE `id`=".$DB->F($r["id"]).";";
            }
            $DB->query($sql);
            if ($DB->errno()) echo "MySQL-Error: ".$DB->error()."\r\n";
            $DB->free();
        }
    }
} else {
    echo "No new callrecords\r\n";
}
$DB->free();
echo "Finished at ".date("Y-m-d H:i:s");
