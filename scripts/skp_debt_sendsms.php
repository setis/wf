<?php
use classes\UserSession;

set_time_limit(600);
error_reporting(E_ALL);
require_once __DIR__ .'/../bootstrap.php';

/**
* очистка телефонного номера от всего, кроме цифр, подходящих для отправки СМС
* @param string $phone
* 
* @return string phone || false
*/
if(!function_exists('smsCleanPhone')) {
	function smsCleanPhone($phone)
	{
		$phone_clean = preg_replace("/[^0-9]/", "", $phone);

		if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
		{
			$phone_clean = $phone_clean;
		} elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
		{
			$phone_clean = "7" . substr($phone_clean, 1);
		} elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
		{
			$phone_clean = "7" . $phone_clean;
		} else {
			$phone_clean = false;
		}

		return $phone_clean;
	}
}

/**
* получить из любой строки список телефонов для отправки СМС
* @param string $str
* @param bool $clean_only - возвращать только очищенные значения
* 
* @return array(phone_clean, phone_clean, ...) || array(phone_clean => phone_str, phone_clean => phone_str, ...) || false
*/
if(!function_exists('smsPhonesFromString')) {
	function smsPhonesFromString($str, $clean_only = false)
	{
		$ret = array();
		$arr = preg_split("/[^0-9\(\)\-\+]+/", $str, -1, PREG_SPLIT_NO_EMPTY);
		foreach ($arr as $phone) {
			if ($phone_clean = smsCleanPhone($phone)) {
				if ($clean_only) $ret[] = $phone_clean;
				else $ret[$phone_clean] = $phone;
			}
		} // foreach

		if (sizeof($ret)) return $ret;
		else             return false;
	}
}

/**
* отправка СМС
* @param mixed $to - string || array номеров получателей
* @param string $text 
* @param mixed $result - string || array резултатов отправки в зависимости от количества получателей
* 
* @return bool. если несколько получателей, то всегда true, для анализа нужно разбирать $result
*/
if(!function_exists('sendSMS')) {
	function sendSMS($to, $text, &$result)
	{
		return true;
		if (!is_array($to)) $to = smsPhonesFromString($to, true);
		else foreach ($to as $i => $phone) if ($phone = smsCleanPhone($phone)) $to[$i] = $phone;

		if (!sizeof($to)) {
			trigger_error("No phones were found from \$to", E_USER_WARNING);
			return false;
		}

		$xml = new SimpleXMLElement("<message></message>");
		$servce = $xml->addChild("service");
		$servce->addAttribute('id', sizeof($to) > 1 ? 'bulk' : 'single');
		$servce->addAttribute('login', getcfg('sms_login'));
		$servce->addAttribute('password', getcfg('sms_pass'));
		//$servce->addAttribute('test', class_exists('Debugger') && Debugger::getDebug() ? 1 : 0);

		foreach ($to as $t) $xml->addChild("to", $t);

		$body = $xml->addChild("body", $text);

		/*header("content-type: text/plain");
        print_r($xml->asXML());*/

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getcfg('sms_xml_url'));
		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml->asXML());
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Content-type: text/xml"
		));

		$data = curl_exec($ch);
		$ret = array();

		if ($data === false) {
			trigger_error('cURL error: ' . curl_error($ch), E_USER_WARNING);
			return false;
		} else {
			try {
				$status = new SimpleXMLElement($data);
				if (sizeof($status->state) > 1) {
					$result = array();
					$i = 0;
					foreach ($status->state as $state) {
						if ($errcode = (int)$state['errcode']) {
							$error = (string)$state['error'];
							trigger_error("Send SMS $i error $errcode: $error", E_USER_WARNING);
							$result[$i] = 0;
						} else {
							$result[$i] = (string)$status->id[$i];
							trigger_error("Send SMS " . (string)$status->id[$i], E_USER_WARNING);
						}
						$i++;
					}
				} else {
					if ($errcode = (int)$status->state['errcode']) {
						$error = (string)$status->state['error'];
						trigger_error("Send SMS error $errcode: $error", E_USER_WARNING);
						return false;
					} else {
						$result = (string)$status['id'];
					}
				}
			} catch (Exception $e) {
				trigger_error('Failed to get result XML: ' . $e->getMessage());
				return false;
			}
		}
		curl_close($ch);

		return true;
	}
}

$DB = wf::$container->get('wf.mysql_db');
$notrecognized = 0;
echo "-------------------------------------------------------------------------------\r\n";
echo "Отправка уведомлений о задолженности техникам: ".date("H:i:s d.m.Y")."\r\n";
$sql = "SELECT * FROM `skp_tech_debts` WHERE 1";
$DB->query($sql);
if ($DB->num_rows()) {
    $parsed = 0;
    while ($r = $DB->fetch(true)) {
        $limits = $DB->getRow("SELECT * FROM `list_empl_money_limits` WHERE `tech_id`=".$DB->F($r["tech_id"]).";", true);
        if ($limits) {
            $lim1 = $limits["limit1"];
            $lim2 = $limits["limit2"];
        } else {
            $lim1 = 10000;
            $lim2 = 15000;
        }
        /*if (intval($r["ammount"])>=$lim1 && intval($r["ammount"])<=$lim2) {
            $parsed += 1;
            $smsbody = "Ваш долг составляет ".$r["ammount"]." руб. Просьба сдать в кассу деньги, иначе при достижении порога в $lim2 руб. будет прекращена выдача новых заявок. Горсвязь.";
            $phone = $DB->getField("SELECT `phones` FROM `users` WHERE `id`=".$DB->F($r["tech_id"]).";");
            $phone = smsCleanPhone($phone);    
            if ($phone) {
                if (sendSMS(smsCleanPhone($phone), $smsbody)) {
                    echo "Отправлено СМС технику ID:".$r["tech_id"].", номер: ".$phone.". Долг: ".$r["ammount"]."\r\n";
                } else {
                    echo "ОШИБКА отправки СМС технику ID:".$r["tech_id"].". Долг: ".$r["ammount"]."\r\n";
                }
            } else {
                echo "Нет телефона для отправки СМС технику ID:".$r["tech_id"].". Долг: ".$r["ammount"]."\r\n";
                
            }
        } else {*/
            if (intval($r["ammount"])>=$lim2) {
                $parsed += 1;
                $smsbody = "Ваш долг в ". $r["ammount"] ." превысил допустимый порог. Получение новых заявок блокировано, просьба оперативно сдать в кассу деньги. Горсвязь.";
                $phone = $DB->getField("SELECT `phones` FROM `users` WHERE `id`=".$DB->F($r["tech_id"]).";");
                $phone = smsCleanPhone($phone);    
                if ($phone) {
                    if (sendSMS(smsCleanPhone($phone), $smsbody)) {
                        echo "Отправлено СМС технику ID:".$r["tech_id"].", номер: ".$phone.". Долг: ".$r["ammount"]." - блокировка\r\n";
                    } else {
                        echo "ОШИБКА отправки СМС технику ID:".$r["tech_id"].". Долг: ".$r["ammount"]."\r\n";
                    }
                } else {
                    echo "Нет телефона для отправки СМС технику ID:".$r["tech_id"].". Долг: ".$r["ammount"]." - блокировка\r\n";
                    
                }
            }
        //}
    }
    if ($parsed) {
        echo "Обработано $parsed записей.\r\n";
    }
} else {
    echo "нет записей\r\n";
}

echo "-------------------------------------------------------------------------------\r\n";

function sms_log($id, $hash, $from, $text, $msg) {
    echo "--- MSG_ID: ".$id." ID: ".$hash." RESULT: --- ".$msg." --- Sender: ".$from." SMS: ".$text."\r\n";
}
