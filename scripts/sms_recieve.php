<?php
use classes\UserSession;

set_time_limit(100);

require_once __DIR__ .'/../bootstrap.php';

$DB = wf::$container->get('wf.mysql_db');

$sql = "SELECT DATE_FORMAT(IFNULL(MAX(`message_date`), DATE_SUB(NOW(), INTERVAL 30 DAY)), '%d.%m.%Y %H:%i:%s') FROM `sms_incoming`";

$params = array(
	'http_username' => getcfg('sms_login'),
	'http_password' => getcfg('sms_pass'),	
	'startdate' => $startdate = $DB->getField($sql),
	'format' => 'xml'
);

$nmess = 0;

echo date("[Y-m-d H:i:s]")." Recieving messages from $startdate...\r\n";

$url = getcfg('sms_recieve_url') . '?' . http_build_query($params, '', '&');;
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
//curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
//curl_setopt($ch, CURLOPT_POSTFIELDS,  $xml->asXML());
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
/*
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	"Content-type: text/xml"
));*/

$data = curl_exec($ch);
$data = str_replace("}/&=<$", "", $data);
if($data === false){
    $error = curl_error($ch);
    die("CURL error: $error\r\n");
}elseif($xml = simplexml_load_string($data)) {
    
	if($xml->error && ($errnum = (int)$xml->error['error_num'])) {
		echo "Got error num $errnum: ".(string)$xml->error['error_message']."\r\n";
	} else foreach($xml->message as $message) {		
		$text = (string)$message;
		$len = strlen($text);
		//echo (string)$message['sms_date'];
		$date = date("Y-m-d H:i:s", strtotime((string)$message['sms_date']));
		$from = (string)$message['telnum'];
		$to = (string)$message['rentnum'];
		$message_id = md5($from.$date);
		
		echo "Message $message_id from $from to $to at $date ($len chars) ... ";
		
		$sql = "SELECT COUNT(*) FROM `sms_incoming` WHERE `message_id`=".$DB->F($message_id);
		if($DB->getField($sql)) {
			echo "ALREADY EXISTS ";
		} else {
			$sql = "INSERT INTO `sms_incoming` (`message_id`, `from`, `to`, `text`, `message_date`, `date_recieve`, `is_new`)
					VALUES(".$DB->F($message_id).", ".$DB->F($from).", ".$DB->F($to).", ".$DB->F($text).", ".$DB->F($date).", NOW(), 1)";
			$DB->query($sql);
			if($DB->errno()) {
				echo "ERROR ".$DB->error()." ";
			} else {
				$nmess++;
				echo "OK ";
			}
		}
		echo "\r\n";
	}
} else {
	die("Failed to parse XML!\r\n".$data."\r\n");
}
curl_close($ch);
echo date("[Y-m-d H:i:s]")." Finished. $nmess messages added.\r\n";
