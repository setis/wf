<?php
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;

header("Content-type: text/plain");
set_time_limit(600);
error_reporting(E_ALL);
require_once __DIR__ . '/../bootstrap.php';

$DB = wf::$container->get('wf.mysql_db');
// 0 9 * * * run


$tasksFinishStatuses = [
    /* projects */
    30, 31,
    /* bills */
    39, 40,
    /* agreements */

    /* agr_acts */
    89,
    /* accidents */
    54, 60, 63, 65, 9,
    /* connections */
    5, 6, 13, 26, 95,
    /* gp */
    72, 73, 74, 97,
    /* services */
    10, 12, 17, 25, 50, 94

];
$sql = 'select
    *
from
    task_reminders
    left join
    tasks on (task_reminders.task_id = tasks.id)
where tasks.status_id not in ( ' . implode(', ', $tasksFinishStatuses) . ')
;';
$DB->query($sql);

/**
 * @param $t
 * @param $targetUserList
 * @param $DB
 * @return array
 */
function sendNotification($t, $targetUserList, $DB)
{
    $tpl = new HTML_Template_IT(path2('templates'));
    $tpl->loadTemplatefile('task_mail.tmpl.html');
    $tpl->setVariable('TASK_TITLE', "Проект №" . $t->getId() . " - " . $t->getTitle());
    $tpl->setVariable('TASK_COMMENT', $t->getComment());
    $tpl->setVariable("LAST_COMMENT", " <br /><strong style='color: #FF0000;'>Вы связаны с данным проектом. Наступила дата напоминания.</strong><br /><br />");
    $tpl->setVariable('SYS_NAME', getcfg('system.name'));
    $tpl->setVariable('PLUGIN_TITLE', Plugin::getNameStatic($t->getPluginUid()));
    $tpl->setVariable('TASK_ID', $t->getId());
    $tpl->setVariable('TASK_HREF', link2("/projects/viewtask?task_id=" . $t->getId()));
    $tpl->setVariable('DATE_REG', $t->getDateReg());
    $tpl->setVariable('STATUS_NAME', $t->getStatusName());
    $tpl->setVariable('STATUS_COLOR', '#F5F50E');
    $tpl->setVariable('AUTH_FIO', $t->getAuthorFio());
    $tpl->setVariable('ISPOLN_FIO', $t->getIspoln(', '));

    $email = wf::$container->get('wf.deprecated_mail');
    $email->setSubject('Напоминание о проекте №' . $t->getId() . ' - ' . $t->getTitle() . " - " . $t->getStatusName());
    $sql = "SELECT us.email, us.fio FROM `users` AS us WHERE us.email IS NOT NULL AND us.active AND us.id IN ($targetUserList)";
    $to = array();
    foreach ($DB->getCell2($sql) as $eml => $fio) {
        if ($eml != "") {
            $to[] = "$fio <$eml>";
        }
    }
    $email->setTo(implode(", ", $to));
    $email->setHtml($tpl->get());
    $email->send();
    return $to;
}

while ($row = $DB->fetch(true)) {
    $t = new Task($row['task_id']);
    $targetUserList = $row['user_id'];

    if (!empty($row['day'])) {
        // send notify

        $result = sendNotification($t, $targetUserList, $DB);
        //die($tpl->get());
        if (!$result) {
            echo "Ошибка отправки почты адресатам с требованием комментария о проекте " . $t->getId() . " - " . implode(",", $to) . "\r\n";
        } else {
            $count += 1;
            echo "Успешно отправлено сообщение с требованием комментария о проекте " . $t->getId() . " - " . implode(",", $to) . "\r\n";
        }

    } elseif (!empty($row['week']) && date('w') == 1) {
        $t = new Task($row['task_id']);

        $result = sendNotification($t, $targetUserList, $DB);
        //die($tpl->get());
        if (!$result) {
            echo "Ошибка отправки почты адресатам с требованием комментария о проекте " . $t->getId() . " - " . implode(",", $to) . "\r\n";
        } else {
            $count += 1;
            echo "Успешно отправлено сообщение с требованием комментария о проекте " . $t->getId() . " - " . implode(",", $to) . "\r\n";
        }

    } elseif (!empty($row['month']) && date('d') == 1) {
        $t = new Task($row['task_id']);

        $result = sendNotification($t, $targetUserList, $DB);
        //die($tpl->get());
        if (!$result) {
            echo "Ошибка отправки почты адресатам с требованием комментария о проекте " . $t->getId() . " - " . implode(",", $to) . "\r\n";
        } else {
            $count += 1;
            echo "Успешно отправлено сообщение с требованием комментария о проекте " . $t->getId() . " - " . implode(",", $to) . "\r\n";
        }

    }
}
