<?php

/**
 * User Interface library
 *
 * @author Gorserv - kblp
 * @copyright 2011
 */

use classes\HTML_Template_IT;
use classes\IpAddress;
use classes\Plugin;
use classes\User;

$ACCESS = false;

function array2options($array, $selected = 0, $escape = true, $all = false)
{
    $opt = '';
    if (is_array($selected)) {
        foreach ($selected as $key => $item) {
            $selected[$key] = (string)$item;
        }
    }
    foreach ($array as $key => $value) {
        if (!$all) {
            $sel = is_array($selected) ?
                (in_array((string)$key, $selected, true) ? 'selected selected="selected"' : '')
                : (($key == $selected) ? 'selected selected="selected"' : '');
        } else {
            $sel = 'selected selected="selected"';
        }
        $opt .= "<option value='$key' $sel>" . ($escape ? htmlspecialchars($value) : $value) . "</option>\r\n";
    }

    return $opt;
}

function buildContextMenu($uid, $menu, $level = 0, $parent)
{
    global $ACCESS;
    if (!$uid || !$menu) {
        //die();
        return false;
    }
    $tpl = new HTML_Template_IT(path2('templates'));
    $tpl->loadTemplatefile("leftmenu_r.tmpl.htm");
    //$access = wf::$db->getCell2("SELECT `plugin_uid`, `access` FROM `user_access` WHERE `user_id`=".wf::$db->F($USER->getId()).";");
    //die();
    //print_r($menu);
    foreach ($menu as $key => $value) {
        //if ($value["name"] == "Отчеты") {
        /*print_r($value);
        echo "\r\n";
        echo $key;
        *///die();
        //}

        ///echo $level."<br />";
        //if ($ACCESS)

        if (isset($value["parent"]) && $value["parent"] == $parent) {

            if ($value["link"]) {
                $accessComp = $value["href"];
            } else {
                $accessComp = $uid;
            }
            if ($value["type"] == "read") {
                $accessLevel = 1;
            } else {
                $accessLevel = 2;
            }
            /*echo $value["href"]."<br />";
            echo "ACCL: ";
            print_r($ACCESS["reports"]);
            echo "<br />";
            echo "A: ".$accessLevel;
            echo "<br />";
            */
            $disTech = ((User::isTech(wf::$user->getId()) && !User::isChiefTech(wf::$user->getId())) && $value["disallowtech"]);
            if ((($ACCESS[$accessComp] >= $accessLevel) && !$disTech) || $value["type"] == "spacer") {
                $tpl->setVariable('LEVEL_ID', $level);
                $tpl->setCurrentBlock('lm-item');
                if ($value["type"] == "spacer") {
                    $tpl->setCurrentBlock("nolink");
                    $tpl->setVariable('LMTITLE', htmlspecialchars($value['name']));
                    $tpl->parse("nolink");
                } else {
                    $tpl->setVariable("MOD_UID", $uid);
                    $tpl->setCurrentBlock("islink");
                    if ($value["link"]) {
                        //echo "/".$value["href"]." ". $_SERVER["REQUEST_URI"]."<br />";
                        if (preg_replace("/(\/+)$/", "", "/" . $value["href"]) == preg_replace("/(\/+)$/", "", $_SERVER["REQUEST_URI"])) {
                            $tpl->setVariable("SELECTED_ITEM", "selected_lm");
                        }
                        $link = Plugin::getLinkStatic($value["href"]);
                    } else {
                        //echo "/".$uid."/".$value["href"]." ". $_SERVER["REQUEST_URI"]."<br />";
                        if (preg_replace("/(\/+)$/", "", "/" . $uid . "/" . $value["href"]) == preg_replace("/(\/+)$/", "", $_SERVER["REQUEST_URI"])) {
                            $tpl->setVariable("SELECTED_ITEM", "selected_lm");
                        }
                        $link = link2($uid . "/" . $value["href"]);
                    }
                    $tpl->setVariable('LM_LINK1', $link);
                    //die($_SERVER["REQUEST_URI"]." ".$link);
                    $tpl->setVariable('LM_TITLE1', htmlspecialchars($value['name']));
                    $tpl->parse("islink");
                }

                if ($value["type"] == "spacer" && $key != $parent) {
                    $tpl->setVariable("LM_CHILDREN", buildContextMenu($uid, $menu, $level++, $key));
                } else {
                    //echo $uid;
                }
                $tpl->parse('lm-item');
            }
        }
    }

    return $tpl->get();

}

/**
 * @param HTML_Template_IT $tpl
 */
function UIHeader($tpl)
{
    global $PLUGINS, $plugin, $plugin_event, $ACCESS, $LM;

    $tpl->setVariable('ADDR_URL', '/address_finder/');
    $tpl->setVariable('ADDR_TOKEN', \wf::$container->getParameter('dadata_api_key'));

    if (wf::$container->getParameter('maintenance')) {
        $tpl->setCurrentBlock("maintenance");
        if (wf::$container->getParameter('maintenance') === true)
            $message = 'WF на обслуживании.';
        else
            $message = wf::$container->getParameter('maintenance');
        $tpl->setVariable('MAINTENANCE_MESSAGE', $message);
        $tpl->parseCurrentBlock();
    }

    $atpl = 'footer.inc';
    $tpl->loadTemplatefile($atpl);
    $tpl->setVariable('FACILITY', wf::getServerName());
    $tpl->setVariable('DEVELOPER_EMAIL', wf::getDeveloperEmail());
    $tpl->setVariable('CLIENT_IP', IpAddress::getIp());

    if ($plugin instanceof Plugin) {
        $tpl->setVariable('PLUGIN_UID', $plugin->getUID());
        $tpl->setVariable('PLUGIN_NAME', $plugin->getName());
        $tpl->setVariable('EVENT_TITLE', getEventTitle($plugin->getUID(), $plugin_event));

        $current_plugin = $plugin->getUID();
    } else {
        $current_plugin = '';
    }
    $intphone = false;
    if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPod')) {
        $tpl->setVariable("METAVIEWPORT_ADD", ", user-scalable=no");
        $intphone = wf::$user->getId();
    } else {
        if (strstr($_SERVER['HTTP_USER_AGENT'], 'Android')) {
            $intphone = wf::$user->getId();
            $tpl->setVariable("METAVIEWPORT_ADD", ", user-scalable=no, maximum-scale=0.7");
        }
    }

    $ACCESS = wf::$db->getCell2("SELECT `plugin_uid`, `access` FROM `user_access` WHERE `user_id`=" . wf::$db->F(wf::$user->getId()) . ";");

    $addTpl = "seluser_single.tmpl.htm";

    $atpl = new HTML_Template_IT(path2('templates'));
    $atpl->loadTemplatefile($addTpl);
    $atpl->setVariable("USER_TEMPLATE", wf::$user->getTemplate());
    $atpl->setVariable("PODR_LIST", "<option value=''>-- все --</option>" . adm_users_plugin::getOtdelOptions());
    $atpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScListByAccess());
    $addtpltext = '';
    $addtpltext .= $atpl->get();

    $addTpl = "seluser_multiple.tmpl.htm";

    $atpl = new HTML_Template_IT(path2('templates'));
    $atpl->loadTemplatefile($addTpl);
    $atpl->setVariable("USER_TEMPLATE", wf::$user->getTemplate());
    $atpl->setVariable("PODR_LIST", "<option value=''>-- все --</option>" . adm_users_plugin::getOtdelOptions());
    $addtpltext .= $atpl->get();

    $addTpl = "selskpwtypes.tmpl.htm";

    $atpl = new HTML_Template_IT(path2('templates'));
    $atpl->loadTemplatefile($addTpl);
    $atpl->setVariable("USER_TEMPLATE", wf::$user->getTemplate());
    $atpl->setVariable("FILTER_PRICESECTION", "<option value=''>-- все --</option>" . adm_skp_catnames_plugin::getOptList());
    $atpl->setVariable("FILTER_CONTRLIST_OPTIONS", kontr_plugin::getClientList());
    $addtpltext .= $atpl->get();

    if (User::isTech(wf::$user->getId()) && wf::$user->getIntPhone()) {
        $tpl->setCurrentBlock("g_calltogssdisp");
        $tpl->setVariable("G_CALLFROMPHONE", $intphone ? $intphone : wf::$user->getIntPhone());
        $tpl->setVariable("G_CALLTODISP", getcfg('dispgroupnumber'));
        $tpl->parse("g_calltogssdisp");
    }

    /* @var $security \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface */
    $security = wf::$container->get('security.authorization_checker');

    if ($security->isGranted('ROLE_CAN_TO_BE')) {
        $addStr = "
        <div id='loginat' class='modal fade' role='dialog'>
            <div class='modal-dialog'>
                <div class='modal-content'>
                <form role='form' action='/adm_users/setfa' method='POST'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <h4 class='modal-title'>Воплотиться в...</h4>
                    </div>
                    <div class='modal-body'>

                            <div class='form-group'>
                                <select id='impersonate_select' class='form-control' name='targetuser_id' style='width: 100%'>";

        $addStr .= ($security->isGranted('ROLE_CAN_TO_BE_ANY_USER') ? adm_users_plugin::getUserActiveList() : adm_users_plugin::getUserActiveTechList());

        $addStr .= "</select>
                            </div>
                    </div>
                    <div class='modal-footer'>
                        <button type='submit' value='Переключить' class='btn btn-primary'>Воплотиться</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>";
    } else
        $addStr = "";

    $tpl->setVariable("SELECTOR_DIALOGS", $addtpltext . $addStr);
    $tpl->setVariable('HTTP_BASE', getcfg('http_base'));

    $tpl->setVariable('SYS_NAME', getcfg('system.name', true));
    $tpl->setVariable("SWITCHER", $addStr);
    if (wf::$user->checkAccess("adm_users")) {
        $tpl->setCurrentBlock("userlink");
        $tpl->setVariable("USERLINK", "/adm_users/user_view?user_id=" . wf::$user->getId());
        $tpl->parse("userlink");
        $tpl->touchBlock("ulend");
        $tpl->setCurrentBlock("userlink_im");
        $tpl->setVariable("USERLINK_IM", "/adm_users/user_view?user_id=" . wf::$user->getId());
        $tpl->parse("userlink_im");
        $tpl->touchBlock("ulend_im");
    } else {
        $tpl->setCurrentBlock("userlink");
        $tpl->setVariable("USERLINK", "/");
        $tpl->parse("userlink");
        $tpl->touchBlock("ulend");
        $tpl->setCurrentBlock("userlink_im");
        $tpl->setVariable("USERLINK_IM", "/");
        $tpl->parse("userlink_im");
        $tpl->touchBlock("ulend_im");
    }
    $tpl->setVariable("USERLKLINK", "/adm_users/user_view?user_id=" . wf::$user->getId());
    $tpl->setVariable("USER_TEMPLATE", wf::$user->getTemplate() ? wf::$user->getTemplate() : "default");
    $userName = explode(" ", wf::$user->getFio());

    $userName1 = $userName[0] . " " . (mb_substr($userName[1], 0, 1) != "" ? mb_substr($userName[1], 0, 1) . ". " : "") . " " . (mb_substr($userName[2], 0, 1) != "" ? mb_substr($userName[2], 0, 1) . "." : "");
    /** @var \models\User $user */
//    $user = wf::$container->get('security.token_storage')->getToken()->getUser();
//    if ($user->isTechnician()) {
//        $tpl->setCurrentBlock('balance');
//        $tpl->setVariable('BALANCE', wf::$container->get('gerp.core_bundle.twig.user_extension')->balance($user));
//        $tpl->parseCurrentBlock();
//    }
    if (wf::$user->isLoggedIn()) {
        $tpl->setVariable('USER_FIO', $userName1);
    }

    if ($security->isGranted('ROLE_PREVIOUS_ADMIN')) {
        $tpl->setVariable('LOGOUT_TITLE', 'Вернуться...');
        $tpl->setVariable('LOGOUT_URL', '/?_to_be=_exit');
    } else {
        $tpl->setVariable('LOGOUT_TITLE', 'Выход');
        $tpl->setVariable('LOGOUT_URL', '/logout');
    }

    if (wf::$user->getTemplate() == "twozero") {
        @$tpl->setCurrentBlock("lmbody");
        @$tpl->setCurrentBlock("lm");
        $tpl->setVariable("LEVEL_ID", 0);
        $a["desktop"] = ["hidden" => false, "name" => "Рабочий стол"];
        if (!empty($PLUGINS)) {
            $LKUser = adm_users_plugin::isLKUser(wf::$user->getId());
            foreach ($PLUGINS as $uid => $p) {
                if ($LKUser) {
                    if ($uid != "services" && $uid != "connections" && $uid != "desktop" && $uid != "accidents" && $uid != "gp") continue;
                }

                $accl = wf::$user->checkAccess($uid);
                if (!$p['hidden'] && $accl > 0) {
                    if ($uid != "desktop")
                        $a[$uid] = ["hidden" => $p["hidden"], "name" => $p["name"]];
                    if ($uid != "desktop") {
                        //print_r($rtpl);
                        @$tpl->setCurrentBlock("lm-item");
                        @$tpl->setCurrentBlock("no-link");
                        $tpl->setVariable("LMROOTID", $uid);
                        $tpl->setVariable("LM_NLTITLE", htmlspecialchars($p['name']));
                        $tpl->setVariable("MOD_UID", $uid);

                        @$tpl->parse("no-link");
                        ////echo $uid."<br />";
                        if ($LM["lm"][$uid]) {
                            //print_r($PLUGINS["lm"][$uid]);
                            $tpl->setVariable("LM_CHILDREN", buildContextMenu($uid, $LM["lm"][$uid], 1, 0));
                        }
                        //$rtpl->get());

                        @$tpl->parse("lm-item");
                    }
                }


            }
        }

        @$tpl->parse("lmbody");

        $tpl->parse("lm");
        $showsettings = false;
        foreach ($a as $uid => $p) {

            $tpl->setCurrentBlock('main_menu_item');

            $tpl->setVariable('MM_HREF', Plugin::getLinkStatic($uid));
            $tpl->setVariable('MM_NAME', htmlspecialchars($p['name']));
            if ($p['name'] == "Отчеты") {
                $tpl->setVariable("CALLLM", "id=\"lm_reports\"");
            }
            if ($uid == "desktop") {
                $tpl->setVariable("MM_NAME", "<span title=\"Рабочий стол\" class=\"glyphicon glyphicon-home hidden-sm hidden-xs\" ></span><span class='visible-xs visible-sm'>Рабочий стол</span>");
            }
            if ($uid == "adm_log") {
                $tpl->setVariable("MM_NAME", "<span title=\"Системный журнал\" class=\"glyphicon glyphicon-exclamation-sign hidden-sm hidden-xs\"></span><span class='visible-xs visible-sm'>Системный журнал</span>");
            }
            if ($uid == "adm_interface") {
                $showsettings = true;
                continue;
            }

            if ($uid == "agreements") {
                $tpl->setVariable("MM_NAME", "
                    <span title=\"Договоры\" class=\"fa fa-file-text-o hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl'>Договоры</span>
                ");
            }
            if ($uid == "addr_interface") {
                $tpl->setVariable("MM_NAME", "
                    <span title=\"Адреса\" class=\"fa fa-building hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl'>Адреса</span>
                ");
            }
            if ($uid == "adm_ods") {
                $tpl->setVariable("MM_NAME", "
                    <span title=\"ОДС\" class=\"fa fa-key hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl'>ОДС</span>
                ");
            }
            if ($uid == "tmc") {
                $tpl->setVariable("MM_NAME", "
                    <span title=\"ТМЦ\" class=\"fa fa-archive hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl'>ТМЦ</span>
                ");
            }
            if ($uid == "tmc2") {
                $tpl->setVariable("MM_NAME", "
                    <span title=\"ТМЦv2\" class=\"fa fa-archive hidden-xs hidden-sm visible-md hidden-lg hidden-xl hidden-xxl\"><sup> 2</sup></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xl visible-xxl'>ТМЦv2</span>
                ");
            }
            if ($uid == "reports") {
                $tpl->setVariable("MM_NAME", "
                    <span title=\"Отчеты\" class=\"fa fa-bar-chart hidden-xs hidden-sm visible-md hidden-lg visible-xl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xxl'>Отчеты</span>
                ");
            }
            if ($uid == "callviewer") {
                $tpl->setVariable("MM_NAME", "
                    <span title=\"Записи телефонных звонков\" class=\"fa fa-play-circle hidden-xs hidden-sm visible-md hidden-lg visible-xl\"></span>
                    <span class='visible-xs visible-sm hidden-md hidden-lg visible-xxl'>Записи звонков</span>
                ");
            }
            if ($uid == "qc") {
                $tpl->setVariable("MM_NAME", "
                    <span title=\"Контроль качества (отзывы)\" class=\"fa fa-thumbs-up hidden-xs hidden-sm visible-md hidden-lg visible-xl\"></span>
                    <span class='visible-sm visible-xs hidden-md hidden-lg visible-xxl'>Отзывы</span>
                ");
            }

//            echo "$uid\r\n";

            if (($current_plugin == $uid) || (isset($PLUGINS[$current_plugin]) && $PLUGINS[$current_plugin]['hidden'] && isset($PLUGINS[$current_plugin]['menuparent']) && $PLUGINS[$current_plugin]['menuparent'] == $uid)) {
                $tpl->setVariable('MM_SEL', 'active');
            }
            $tpl->parseCurrentBlock();

        }

//                die();

        if ($showsettings) {
            $tpl->setCurrentBlock('main_menu_item');
            $tpl->setVariable("MM_NAME", "<span title=\"Настройки\" class=\"fa fa-cog hidden-xs hidden-sm\"> </span></span><span class='visible-sm visible-xs'> Настройки</span>");
            $tpl->setVariable('MM_HREF', Plugin::getLinkStatic("adm_interface"));
            $tpl->parseCurrentBlock();
        }
    } else {
        if (!empty($PLUGINS)) {
            foreach ($PLUGINS as $uid => $p) if (!$p['hidden'] && wf::$user->checkAccess($uid)) {
                $tpl->setCurrentBlock('main_menu_item');
                $tpl->setVariable('MM_HREF', Plugin::getLinkStatic($uid));
                $tpl->setVariable('MM_NAME', htmlspecialchars($p['name']));
                if (($current_plugin == $uid) || ($PLUGINS[$current_plugin]['hidden'] && $PLUGINS[$current_plugin]['menuparent'] == $uid)) {
                    $tpl->setVariable('MM_SEL', 'active');
                }
                $tpl->parseCurrentBlock();
            }
            //$tpl->setVariable("LMDISABLED", "style=\"display: none !important;\"");
        }
    }

    if (wf::$user->getTemplate() == "twozero") {
        $tpl->touchBlock("twozerocopy");
        $tpl->setVariable("IP", IpAddress::getIp());
        $tpl->parseCurrentBlock();
        $tpl->setVariable("PRELOADER", "<td style='padding-left: 20px;'><img style='display: none;' id='ajaxloader' src='/templates/twozero/images/preload.gif' /></td>");
        $tpl->setVariable("PRELOGO", "<td><img class='gsslogo' width='20' src='/templates/images/small-logo.gif' /></td>");
    }

    $tpl->setVariable('USER_EMAIL', wf::$user->getEmail());
    $tpl->setVariable('USER_PHONE', wf::$user->getPhones());
    $tpl->setVariable('SERVER', implode('; ', $_SERVER));

}

/**
 * @param $message
 * @param int $code
 */
function UIErrorWrapped($message, $code = 404)
{
    header($message, true, $code);
    UIError($message);
}

/**
 * UIError()
 * вывести сообщение об ошибке и закончить выполнение если не AJAX
 *
 * @param mixed $err - массив или строка ошибки
 * @param string $title - заголовок сообщения
 * @param bool $header - показывать заголовок страницы (меню и тп)
 * @param string $actionTitle - название дополнительной кнопки
 * @param string $actionURL - альтернативный url для перехода по нажатию кнопки
 * @return void || string если AJAX
 *
 * @see templates/error.tmpl.htm
 */
function UIError($err, $title = 'Ошибка', $header = true, $actionTitle = false, $actionURL = false, $asHtml = false, $addHeader = false, $hideback = false)
{
    if (!is_array($err)) {
        $err = [$err];
    }

    throw new \Exception(implode("\n", $err));

    $header = $header && wf::$user->Alive() && !defined('AJAX');

    $tpl = new HTML_Template_IT(path2('templates'));
    $tpl->loadTemplatefile('error.tmpl.htm');
    if ($header) {
        $tpl->touchBlock('header');
        UIHeader($tpl);
    } else {
        if ($addHeader) {
            $tpl->touchBlock("dialogheader");
        }
        $tpl->setVariable('HTTP_BASE', getcfg('http_base'));
        $tpl->setVariable('SYS_NAME', getcfg('sys_name', true));
    }
    $tpl->setVariable('TITLE', htmlspecialchars($title));
    if (sizeof($_POST)) foreach ($_POST as $key => $value) {
        $tpl->setCurrentBlock('post');
        $tpl->setVariable('POST_NAME', htmlspecialchars($key));
        $tpl->setVariable('POST_VALUE', htmlspecialchars($value));
        $tpl->parseCurrentBlock();
    }
    if (!is_array($err)) $err = [$err];
    foreach ($err as $error) {
        $tpl->setCurrentBlock('err');
        if (!$asHtml)
            $tpl->setVariable('ERROR', htmlspecialchars($error));
        else
            $tpl->setVariable("ERROR", $error);
        $tpl->parseCurrentBlock();
    }
    if ($actionURL) {
        $tpl->setVariable('BACK_URL', $actionURL);
    } else {
        $ref = getenv('HTTP_REFERER');
        if ($ref && ($ref != link2($_SERVER['REQUEST_URI']))) {
            $tpl->setVariable('BACK_URL', $ref);
            if (!$hideback) $tpl->touchBlock("back");
        }
    }
    if ($actionTitle) {
        $tpl->setCurrentBlock("action");
        $tpl->setVariable("ACTIONVAL", $actionTitle);
        $tpl->parse("action");
    }
    if (defined('AJAX')) {
        echo preg_replace(["/^\s*\n$/m", "/&[#0-9a-z]+;/i", "/\s{2,}/"], ["\n", '', ' '], str_replace("\r", '', trim(strip_tags($tpl->get()))));
    } else {
        $tpl->show();
    }
}

/**
 * pages() создает UL со списком страниц
 * удобно для вывода текцщего куска данных, полученных SQL запросом  ...LIMIT $start,$limit
 * выводит UL с классом 'pages', в котором:
 * LI со ссылками на страницы
 * LI со span для '...' и прочих не нажимаемых
 * LI с классом 'selected' и span c номером текущей страницы
 * LI с классом 'prev' и 'next' со ссылками на предыдущюю и следующую страницы
 * пример вывода:
 * <ul class="pages">
 *  <li class="prev"><a ...></li>
 *  <li class="selected"><span>1</span></li>
 *  <li><a ...></li>
 *  <li><span>...</span></li>
 *  <li class="next"><a ...></li>
 * </ul>
 *
 * @param int $start - отступ от начала
 * @param int $limit - кол-во элементов на страницу (0 = getcfg('rows_per_page'))
 * @param int $total - всего элементов
 * @param string $href формат ссылки на страницу, %s заменяется на отступ от начала, %n - на номер страницы
 * @return HTML список страниц
 */
function pages($start, $limit, $total, $href)
{
    if (!$limit)
        $limit = wf::$container->getParameter('rows_per_page');

    $num_pages = ceil($total / $limit);
    $cur_page = $start / $limit;

    $arr = [];
    $arr1 = [];
    $search = ['%s', '%n'];

    if ($num_pages > 10) {
        $p_start = $cur_page - 5;
        $p_end = $cur_page + 5;
        if ($p_start < 1) {
            $p_start = 0;
            $p_end = $p_start + 10;
        } else {
            $phref = str_replace($search, [0, 1], $href);
            $arr[] = "<li><a href=\"$phref\" rel=\"first\">1</a></li>";
            if ($p_start == 2) {
                $p_start = $p_start - 1;
            } elseif ($p_start > 2 && $num_pages != 11) {
                $arr[] = "<li><span>&hellip;</span></li>";
            }
        }

        if ($p_end >= $num_pages) {
            $p_start = $num_pages - 10;
            $p_end = $num_pages;
        } else {
            $phref = str_replace($search, [($num_pages - 1) * $limit, $num_pages], $href);
            $arr1[] = "<li><a href=\"$phref\" rel=\"last\">$num_pages</a></li>";
            if ($num_pages - $p_end == 2) {
                $p_end = $p_end + 1;
            } elseif ($num_pages - $p_end > 2) {
                array_unshift($arr1, "<li><span>&hellip;</span></li>");
            }
        }
    } else {
        $p_start = 0;
        $p_end = $num_pages;
    }

    for ($i = $p_start; $i < $p_end; $i++) {
        $i_start = $i * $limit;
        $i_page = $i + 1;
        $phref = str_replace($search, [$i_start, $i_page], $href);
        $arr[] = $i_start == $start ? "<li class=\"selected\"><span>$i_page</span></li>" : "<li><a href=\"$phref\">$i_page</a></li>";
    }

    if ($cur_page > 0) {
        $p_start = ($cur_page - 1) * $limit;
        $prev_page_num = round($cur_page, 0);
        $phref = str_replace($search, [$p_start, $prev_page_num], $href);
        array_unshift($arr, "<li class=\"prev\"><a href=\"$phref\" rel=\"prev\">&#9668;</a></li>");
    }

    if ($cur_page < $num_pages - 1) {
        $p_start = ($cur_page + 1) * $limit;
        $next_page_num = round($cur_page + 2, 0);
        $phref = str_replace($search, [$p_start, $next_page_num], $href);
        $arr1[] = "<li class=\"next\"><a href=\"$phref\" rel=\"next\">&#9658;</a></li>";
    }

    return "<ul class=\"pages pagination pagination-sm\">\r\n" . join("\r\n", array_merge($arr, $arr1)) . "</ul>\r\n";
}

function pages_ajax($start, $limit, $total, $href)
{
    if (!$limit)
        $limit = wf::$container->getParameter('rows_per_page');
    $num_pages = ceil($total / $limit);
    $cur_page = $start / $limit;

    $arr = [];
    $arr1 = [];
    $search = ['%s', '%n'];

    if ($num_pages > 10) {
        $p_start = $cur_page - 5;
        $p_end = $cur_page + 5;
        if ($p_start < 1) {
            $p_start = 0;
            $p_end = $p_start + 10;
        } else {
            $phref = str_replace($search, [0, 1], $href);
            $arr[] = "<li><a class=\"pages_ajax\" href=\"$phref\" rel=\"first\">1</a></li>";
            if ($p_start == 2) {
                $p_start = $p_start - 1;
            } elseif ($p_start > 2 && $num_pages != 11) {
                $arr[] = "<li><span>&hellip;</span></li>";
            }
        }

        if ($p_end >= $num_pages) {
            $p_start = $num_pages - 10;
            $p_end = $num_pages;
        } else {
            $phref = str_replace($search, [($num_pages - 1) * $limit, $num_pages], $href);
            $arr1[] = "<li><a class=\"pages_ajax\" href=\"$phref\" rel=\"last\">$num_pages</a></li>";
            if ($num_pages - $p_end == 2) {
                $p_end = $p_end + 1;
            } elseif ($num_pages - $p_end > 2) {
                array_unshift($arr1, "<li><span>&hellip;</span></li>");
            }
        }
    } else {
        $p_start = 0;
        $p_end = $num_pages;
    }

    for ($i = $p_start; $i < $p_end; $i++) {
        $i_start = $i * $limit;
        $i_page = $i + 1;
        $phref = str_replace($search, [$i_start, $i_page], $href);
        $arr[] = $i_start == $start ? "<li class=\"selected\"><span>$i_page</span></li>" : "<li><a class=\"pages_ajax\" href=\"$phref\">$i_page</a></li>";
    }

    if ($cur_page > 0) {
        $p_start = ($cur_page - 1) * $limit;
        $prev_page_num = round($cur_page, 0);
        $phref = str_replace($search, [$p_start, $prev_page_num], $href);
        array_unshift($arr, "<li class=\"prev\"><a class=\"pages_ajax\" href=\"$phref\" rel=\"prev\">&#9668;</a></li>");
    }

    if ($cur_page < $num_pages - 1) {
        $p_start = ($cur_page + 1) * $limit;
        $next_page_num = round($cur_page + 2, 0);
        $phref = str_replace($search, [$p_start, $next_page_num], $href);
        $arr1[] = "<li class=\"next\"><a class=\"pages_ajax\" href=\"$phref\" rel=\"next\">&#9658;</a></li>";
    }

    return "<ul class=\"pages_ajax pagination pagination-sm\">\r\n" . join("\r\n", array_merge($arr, $arr1)) . "</ul>\r\n";
}
