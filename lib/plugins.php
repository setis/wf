<?php

function getDefaultPlugin($plugins = null)
{
    global $PLUGINS, $USER;
    if (!$plugins) $plugins = $PLUGINS;

    reset($plugins);
    //return key($plugins);
    while ($p = key($plugins)) {
        if ($USER->getAccess($p)) return $p;
        next($plugins);
    }
    return false;
}

function getDefaultEvent($plugin_uid, $plugins = null)
{
    global $PLUGINS;
    if (!$plugins) $plugins = $PLUGINS;

    if (!is_array($plugins[$plugin_uid]['events'])) return false;
    reset($plugins[$plugin_uid]['events']);
    return key($plugins[$plugin_uid]['events']);
}

function getEventTitle($plugin_uid, $event = '', $escape = true, $plugins = null)
{
    global $PLUGINS;
    if (!$plugins) $plugins = $PLUGINS;

    if (!isset($plugins[$plugin_uid]['events'])) return false;
    if (!$event) $event = getDefaultEvent($plugin_uid, $plugins);
    if (!$event) return false;
    return $escape ? htmlspecialchars($plugins[$plugin_uid]['events'][$event]) : $plugins[$plugin_uid]['events'][$event];
}

function eventExists($plugin_uid, $event = '', $plugins = null)
{
    global $PLUGINS;
    if (!$plugins) $plugins = $PLUGINS;

    if (!isset($plugins[$plugin_uid]['events'])) return false;
    if (!$event) $event = getDefaultEvent($plugin_uid, $plugins);
    if (!$event) return false;

    return isset($plugins[$plugin_uid]['events'][$event]);
}

function oldAutoload($class_name)
{
    if (!preg_match("/^(.+)_plugin$/", $class_name, $arr)) {
        return false;
    }

    $class_file = path2("plugins/{$arr[1]}/{$arr[1]}.php");
    if (!file_exists($class_file)) {
        throw new ErrorException("Plugin file not found '$class_file' (".$class_name.")");
    }

    include_once($class_file);

    if (!class_exists($class_name, false)) {
        throw new ErrorException("Class '$class_name' does not exists in '$class_file'");
    }
    return true;
}
