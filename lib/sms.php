<?php
/**
 * очистка телефонного номера от всего, кроме цифр, подходящих для отправки СМС
 * @param string $phone
 *
 * @return string phone || false
 */
if (!function_exists('smsCleanPhone')) {
    function smsCleanPhone($phone)
    {
        $phone_clean = '7' . ltrim(preg_replace('/[^0-9]/', '', $phone), '78');

        if (strlen($phone_clean) != 11) {
            $phone_clean = false;
        }

        return $phone_clean;
    }
}

/**
 * получить из любой строки список телефонов для отправки СМС
 * @param string $str
 * @param bool $clean_only - возвращать только очищенные значения
 *
 * @return array(phone_clean, phone_clean, ...) || array(phone_clean => phone_str, phone_clean => phone_str, ...) || false
 */
if (!function_exists('smsPhonesFromString')) {
    function smsPhonesFromString($str, $clean_only = false)
    {
        $ret = array();
        $arr = preg_split("/[^0-9\(\)\-\+]+/", $str, -1, PREG_SPLIT_NO_EMPTY);
        foreach ($arr as $phone) {
            if ($phone_clean = smsCleanPhone($phone)) {
                if ($clean_only) $ret[] = $phone_clean;
                else $ret[$phone_clean] = $phone;
            }
        } // foreach

        if (sizeof($ret)) return $ret;
        else             return false;
    }
}

/**
 * отправка СМС
 * @param mixed $to - string || array номеров получателей
 * @param string $text
 * @param mixed $result - string || array резултатов отправки в зависимости от количества получателей
 *
 * @return bool. если несколько получателей, то всегда true, для анализа нужно разбирать $result
 */
if (!function_exists('sendSMS')) {
    function sendSMS($to, $text, &$result)
    {
        if (!is_array($to)) $to = smsPhonesFromString($to, true);
        else foreach ($to as $i => $phone) if ($phone = smsCleanPhone($phone)) $to[$i] = $phone;

        if (!sizeof($to)) {
            trigger_error("No phones were found from \$to", E_USER_WARNING);
            return false;
        }

        $xml = new SimpleXMLElement("<message></message>");
        $servce = $xml->addChild("service");
        $servce->addAttribute('id', sizeof($to) > 1 ? 'bulk' : 'single');
        $servce->addAttribute('login', getcfg('sms_login'));
        $servce->addAttribute('password', getcfg('sms_pass'));
        //$servce->addAttribute('test', class_exists('Debugger') && Debugger::getDebug() ? 1 : 0);

        foreach ($to as $t) $xml->addChild("to", $t);

        $body = $xml->addChild("body", $text);

        /*header("content-type: text/plain");
        print_r($xml->asXML());*/

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, getcfg('sms_xml_url'));
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml->asXML());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-type: text/xml"
        ));

        $data = curl_exec($ch);
        $ret = array();

        if ($data === false) {
            trigger_error('cURL error: ' . curl_error($ch), E_USER_WARNING);
            return false;
        } else {
            try {
                $status = new SimpleXMLElement($data);
                if (sizeof($status->state) > 1) {
                    $result = array();
                    $i = 0;
                    foreach ($status->state as $state) {
                        if ($errcode = (int)$state['errcode']) {
                            $error = (string)$state['error'];
                            trigger_error("Send SMS $i error $errcode: $error", E_USER_WARNING);
                            $result[$i] = 0;
                        } else {
                            $result[$i] = (string)$status->id[$i];
                        }
                        $i++;
                    }
                } else {
                    if ($errcode = (int)$status->state['errcode']) {
                        $error = (string)$status->state['error'];
                        trigger_error("Send SMS error $errcode: $error", E_USER_WARNING);
                        return false;
                    } else {
                        $result = (string)$status['id'];
                    }
                }
            } catch (Exception $e) {
                trigger_error('Failed to get result XML: ' . $e->getMessage());
                return false;
            }
        }
        curl_close($ch);

        return true;
    }
}