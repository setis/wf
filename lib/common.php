<?php

/**
 * @author kblp
 * @copyright 2011
 */

use classes\tickets\ConnectionTicket;

function from1251toUtf($str)
{
    return iconv('windows-1251', 'utf-8', $str);
}

function redirect($url, $message = '', $return = false)
{
    $mes = '';

    if (ltrim($_SERVER['REQUEST_URI'], "/") == ltrim($url, "/")) {
        trigger_error('You are redirecting to Bobruisk!', E_USER_WARNING);
        $mes = "<p class='redirect-message'>Ошибка! Обнаружено бесконечное перенаправление на <a href='$url'>$url</a>.</p>\r\n";
        if (!$return) echo $mes;
        return $return ? $mes : false;
    }

    $message = addslashes($message);
    $mes = <<<REDIRECT
<script type="text/javascript">
    var redirect_message = '$message';
    if(redirect_message.length) alert(redirect_message);
    window.location.href = '$url';
</script>\r\n
REDIRECT;


    $message = htmlspecialchars(stripslashes($message));
    $mes .= <<<REDIRECT
<p class="redirect-message">$message</p>
<p class="redirect-message">Для перехода на нужную страницу нажмите <a href="$url">здесь</a>.</p>\r\n
REDIRECT;

    if (!$return)
        echo $mes;
    return $return ? $mes : true;
}

function path2($path)
{
    return realpath(getcfg('root_dir') . '/' . ltrim($path, "/\\"));
}

function link2($url, $withBase = true)
{
    $url = ltrim($url, "/\\");
    if($withBase)
        $url = trim(getcfg('http_base'), '/') .'/'. ltrim($url, '/');
    else {
        $url = '/'.trim($url, '/');
    }
    return $url;
}

function link3($url, $params = '')
{
    if (is_array($params))
        $params = http_build_query($params);
    if ($params)
        $url .= (strpos($url, '?') === false ? '?' : '&') . $params;
    return link2($url, false);
}


/**
 * rudate() аналог date(), но с русскими названиями месяцев и дней недели
 *
 * @param string $format - The format of the outputted date string.
 * F Полное наименование месяца, например Января или Марта от Января до Декабря
 * M Сокращенное наименование месяца, 3 символа От Янв до Дек
 * l (строчная 'L') Полное наименование дня недели От Воскресенье до Суббота
 * D Сокращенное наименование дня недели, 2 символа от Вс до Сб
 * остальные варианты форматирования см. функцию date() в мануале.
 * @param mixed $timestamp is optional and defaults to the value of time()
 * если в $timestamp не цифра, то функция пытается получить $timestamp при помощи strtotime($timestamp)
 * @param bool $nominative_month - Полное наименование месяца (F) в именительном падеже, влияет только если в $format присутствует 'F'
 * если $nominative_month истина, то: F Полное наименование месяца, например Январь или Март от Январь до Декабрь
 * если $nominative_month ложь,   то: F Полное наименование месяца, например Января или Марта от Января до Декабря
 * @return a string formatted according to the given format string using the given integer/string timestamp or the current time if no timestamp is given.
 */
function rudate($format, $timestamp = 0, $nominative_month = false)
{
    if (!$timestamp) $timestamp = time();
    elseif (!preg_match("/^[0-9]+$/", $timestamp)) $timestamp = strtotime($timestamp);

    $F = $nominative_month ? array(1 => "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь") : array(1 => "Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря");
    $M = array(1 => "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек");
    $l = array("Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота");
    $D = array("Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб");

    $format = str_replace("F", $F[date("n", $timestamp)], $format);
    $format = str_replace("M", $M[date("n", $timestamp)], $format);
    $format = str_replace("l", $l[date("w", $timestamp)], $format);
    $format = str_replace("D", $D[date("w", $timestamp)], $format);

    return date($format, $timestamp);
}

function report_left_menu($tpl)
{
    global $USER;
    if (!$USER->checkAccess("reports_contr")) {
        $tpl->setVariable("DIS_USERS_1", "disabled");
    }
    if (!$USER->checkAccess("reports_contr_rejects")) {
        $tpl->setVariable("DIS_USERS_2", "disabled");
    }
    if (!$USER->checkAccess("reports_task_types")) {
        $tpl->setVariable("DIS_USERS_3", "disabled");
    }
    if (!$USER->checkAccess("reports_ser_contr")) {
        $tpl->setVariable("DIS_USERS_4", "disabled");
    }
    if (!$USER->checkAccess("reports_ser_contr_rejects")) {
        $tpl->setVariable("DIS_USERS_5", "disabled");
    }
    if (!$USER->checkAccess("reports_ser_task_types")) {
        $tpl->setVariable("DIS_USERS_6", "disabled");
    }

    if (!$USER->checkAccess("reports_f_contr")) {
        $tpl->setVariable("DIS_USERS_7", "disabled");
    }
    if (!$USER->checkAccess("reports_f_tech")) {
        $tpl->setVariable("DIS_USERS_8", "disabled");
    }
    if (!$USER->checkAccess("reports_f_sc")) {
        $tpl->setVariable("DIS_USERS_9", "disabled");
    }
    if (!$USER->checkAccess("acts_ticket")) {
        $tpl->setVariable("DIS_USERS_10", "disabled");
    }
    if (!$USER->checkAccess("acts_tmc")) {
        $tpl->setVariable("DIS_USERS_11", "disabled");
    }
    if (!$USER->checkAccess("reports_f_skp_avg_price")) {
        $tpl->setVariable("DIS_USERS_12", "disabled");
    }
    if (!$USER->checkAccess("reports_f_total")) {
        $tpl->setVariable("DIS_USERS_13", "disabled");
    }
    if (!$USER->checkAccess("reports_tmc_sc_move")) {
        $tpl->setVariable("DIS_USERS_14", "disabled");
    }
    if (!$USER->checkAccess("reports_tmc_sc_total")) {
        $tpl->setVariable("DIS_USERS_15", "disabled");
    }
    if (!$USER->checkAccess("reports_tmc_tech_move")) {
        $tpl->setVariable("DIS_USERS_16", "disabled");
    }
    if (!$USER->checkAccess("reports_tmc_tech_total")) {
        $tpl->setVariable("DIS_USERS_17", "disabled");
    }
    if (!$USER->checkAccess("reports_f_skp_mr")) {
        $tpl->setVariable("DIS_USERS_18", "disabled");
    }
    if (!$USER->checkAccess("reports_f_skp_total")) {
        $tpl->setVariable("DIS_USERS_19", "disabled");
    }
    if (!$USER->checkAccess("reports_f_skp_agents")) {
        $tpl->setVariable("DIS_USERS_20", "disabled");
    }
    if (!$USER->checkAccess("reports_op_conn")) {
        $tpl->setVariable("DIS_USERS_21", "disabled");
    }


}


// ------------------------------------------------- ИНТЕГРАЦИЯ С НБН  --------------------------------------------------------
// -------------------------- ДЛЯ РАБОТЫ ФУНКЦИЙ ТРЕБУЕТСЯ АКТИВНОЕ СОЕДИНЕНИЕ С БД НБН И ДЕЙСТВУЮЩИЙ КЛАСС $DB ---------------
/**
 * переводит строку дата-время в массив array('date', 'time', 'minutes_int')
 * @param string $datestr
 *
 * @return array || bool
 */
function nbnDt2array1($datestr)
{
    // 2014-07-23 16:00:00+04
    if (!preg_match("/^(\d{4}-\d{2}-\d{2}) ((\d{2}):(\d{2}):(\d{2}))/", trim($datestr), $matches)) return false;
    /* array (
      0 => '2014-07-23 16:00:00',
      1 => '2014-07-23',
      2 => '16:00:00',
      3 => '16',
      4 => '00',
      5 => '00',
    )*/
    return array(
        'date' => $matches[1],
        'time' => $matches[2],
        'minutes_int' => (int)$matches[3] * 60 + (int)$matches[4] + round("0.{$matches[5]}", 0)
    );
}

/**
 * поставить заявку из слота в наш график
 * @param integer task_id
 * @param array $slot
 * @param integer $empl_user_id
 * @param bool $echo
 * @deprecated
 * @return bool
 */
function nbnAppendSlot2gfx1($task_id, $slot, $empl_user_id, $echo = true)
{
    trigger_error(__METHOD__, E_USER_DEPRECATED);
    global $DB;
    $sql = "SELECT `fio` FROM `users` WHERE `id`=" . $DB->F($empl_user_id);
    $empl_user_fio = $DB->getField($sql);
    $ticket = new ConnectionTicket($task_id);
    if (!$ticket->getId()) {
        if ($echo) echo "Failed to create ConnectionTicket for $task_id\r\n";
        return false;
    }
    $dt = nbnDt2array($slot['bdate']);
    $busyGFX = $DB->getField("SELECT COUNT(task_id) FROM `gfx` WHERE `startTime`>=" . $DB->F($dt["minutes_int"]) . " AND `startTime`<=" . $DB->F($dt["minutes_int"] + 120) . " AND `c_date`=" . $DB->F($dt["date"]) . " AND `empl_id`=" . $DB->F($empl_user_id) . ";");
    if (!$busyGFX) {
        $sql = "INSERT INTO `gfx` (`c_date`, `task_id`, `empl_id`, `startTime`)
    			VALUES (" . $DB->F($dt['date']) . ", " . $DB->F($task_id) . ", " . $DB->F($empl_user_id) . ", " . $DB->F($dt['minutes_int']) . ")";
        $DB->query($sql);

        $sql = "SELECT status.id FROM `task_status` AS status
    			WHERE status.plugin_uid IN (SELECT task.plugin_uid FROM `tasks` AS task WHERE task.id=" . $DB->F($task_id) . ")
    			AND status.tag LIKE 'grafik%';";
        $status_id = $DB->getField($sql);

        $ticket->addComment(
            "Поставлена в График Работ.",
            "Добавлено в график на {$dt['date']} {$dt['time']}, Исполнитель: $empl_user_fio",
            $status_id);
        if ($echo) echo "Successfully added ticket $task_id in gfx for empl_user_id $empl_user_id\r\n";
    } else {
        $sql = "SELECT status.id FROM `task_status` AS status
    			WHERE status.plugin_uid IN (SELECT task.plugin_uid FROM `tasks` AS task WHERE task.id=" . $DB->F($task_id) . ")
    			AND status.tag LIKE 'gfxconflict%';";
        $status_id = $DB->getField($sql);
        $ticket->addComment(
            "Конфликт Графика Работ.",
            "Нельзя поставить в график на {$dt['date']} {$dt['time']}, Исполнитель: $empl_user_fio. Нет свободного времени",
            $status_id);
        if ($echo) echo "Successfully added ticket $task_id in gfx for empl_user_id $empl_user_id\r\n";
    }


    unset($ticket);


    return true;
}

/**
 * закрыть параллельные слоты
 * @param array $slot
 * @param integer $empl_user_id
 *
 * @return bool
 */
function nbnCloseParallelSlots1($slot, $empl_user_id, $echo = true)
{
    global $DB;

    if ($echo) echo "Closing parallel slots for {$slot['key']} and empl_user_id $empl_user_id\r\n";

    $dt = nbnDt2array($slot['bdate']);

    $c = 0;
    $sql = "SELECT `nbn_crew_key`, `nbn_department_key` FROM `nbn_crews`
			WHERE `active` AND `nbn_crew_key`!=" . $DB->F($slot['crew_key']) . " AND `empl_user_id`=" . $DB->F($empl_user_id);
    $crews = $DB->getCell2($sql);
    foreach ($crews as $crew_key => $department_key) {
        $sql = "SELECT * FROM slots.get_department_slots(" . $DB->F($department_key) . ", " . $DB->F($dt['date']) . ")
				WHERE sobject_key IS NULL AND crew_key=" . $DB->F($crew_key) . "
				AND (
					bdate >= " . $DB->F($slot['bdate']) . " AND edate <= " . $DB->F($slot['edate']) . "
					OR bdate < " . $DB->F($slot['bdate']) . " AND edate > " . $DB->F($slot['bdate']) . "
					OR bdate < " . $DB->F($slot['edate']) . " AND edate > " . $DB->F($slot['edate']) . "
				);";
        wf::$logger->debug('nbn_pgquery', [
            'file' => __FILE__,
            'line' => __LINE__,
            'query' => $sql,
            'stack' => debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10)
        ]);

        $res = pg_query($sql);

        while ($b = pg_fetch_assoc($res)) {
            if ($echo) echo "Closing slot " . $b['key'] . " ...";
            $sql = "SELECT * FROM slots.change_slot_reserved(" . $DB->F($b['key']) . ", True);";
            wf::$logger->debug('nbn_pgquery', [
                'file' => __FILE__,
                'line' => __LINE__,
                'query' => $sql,
                'stack' => debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10)
            ]);

            $result = pg_query($sql);
            $a = pg_fetch_assoc($result);
            if ($echo) echo $a['text'] . "\r\n";
            pg_free_result($result);
            $c++;
        }
        pg_free_result($res);
    }
    if ($echo) echo $c . " slots processed.\r\n";
    return true;
}

/**
 * открыть слоты по освободившемуся месту в графике
 * @param ConnectionTicket $task
 * @param array $gfx
 * @param bool $echo
 *
 * @return bool
 */
function nbnOpenSlots1($task, $gfx, $echo = true)
{
    global $DB;
    $duration = $task->getTypeDuration();
    $tfEnd = intval($duration) + intval($tfStart);
    $timeStart = (strlen(floor($tfStart / 60)) == 1 ? "0" . floor($tfStart / 60) : floor($tfStart / 60)) . ":" . (strlen($tfStart % 60) == 1 ? "0" . $tfStart % 60 : $tfStart % 60) . ":00";
    $timeEnd = (strlen(floor($tfEnd / 60)) == 1 ? "0" . floor($tfEnd / 60) : floor($tfEnd / 60)) . ":" . (strlen($tfEnd % 60) == 1 ? "0" . $tfEnd % 60 : $tfEnd % 60) . ":00";
    if ($echo) echo "Opening slots for " . $timeStart . " - " . $timeEnd . " empl_id " . $gfx['empl_id'] . "\r\n";

    $bdate = $gfx['c_date'] . " " . $timeStart;
    $edate = $gfx['c_date'] . " " . $timeEnd;

    $c = 0;
    $sql = "SELECT `nbn_crew_key`, `nbn_department_key` FROM `nbn_crews`
			WHERE `active` AND `empl_user_id`=" . $DB->F($gfx['empl_id']);
    $crews = $DB->getCell2($sql);
    foreach ($crews as $crew_key => $department_key) {
        //TODO: вот тут хз как открывтаь слоты, либо только те, что внутри интервала в графике, лиюо шире но не понятн как
        $sql = "SELECT * FROM slots.get_department_slots(" . $DB->F($department_key) . ", " . $DB->F($gfx['c_date']) . ")
				WHERE sobject_key IS NULL AND crew_key=" . $DB->F($crew_key) . "
				AND bdate >= " . $DB->F($bdate) . " AND edate <= " . $DB->F($edate) . ";";
        wf::$logger->debug('nbn_pgquery', [
            'file' => __FILE__,
            'line' => __LINE__,
            'query' => $sql,
            'stack' => debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10)
        ]);

        $res = pg_query($sql);

        while ($b = pg_fetch_assoc($res)) {
            if ($echo) echo "Opening slot " . $b['key'] . " ...";
            $sql = "SELECT * FROM slots.change_slot_reserved(" . $DB->F($b['key']) . ", False);";
            wf::$logger->debug('nbn_pgquery', [
                'file' => __FILE__,
                'line' => __LINE__,
                'query' => $sql,
                'stack' => debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10)
            ]);

            $result = pg_query($sql);
            $a = pg_fetch_assoc($result);
            if ($echo) echo $a['text'] . "\r\n";
            pg_free_result($result);
            $c++;
        }
        pg_free_result($res);
    }
    if ($echo) echo $c . " slots processed.\r\n";
    return true;
}

// -------------------------------------------------------------------------------------------------------------------------------------

/**
 * @param bool|false $var
 * @param bool|false $showHtml
 * @param bool|true $showFrom
 */
function d($var = false, $showHtml = false, $showFrom = true, $return = false)
{
    $result = '';
    $result .= '<div class="debug">';
    if ($showFrom) {
        $calledFrom = debug_backtrace();
        $result .= '<strong>' . $calledFrom[0]['file'] . '</strong>';
        $result .= ' (line <strong>' . $calledFrom[0]['line'] . '</strong>)';
    }
    $result .= "\n<pre class=\"debug\">\n";

    $var = print_r($var, true);
    if ($showHtml) {
        $var = str_replace('<', '&lt;', str_replace('>', '&gt;', $var));
    }
    $result .= $var . "\n</pre></div>\n\n";

    if($return)
        return $result;
    else
        echo $result;
}

function rub($number, $showZero = true)
{
    if (!$showZero && ($number == 0 || empty($number)))
        return '';
    $num = number_format($number, 2, ',', ' ');

    return $num;
}


function rubsign($number)
{
    return rub($number, true) . ' <i class="fa fa-rub"></i>';
}

function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

function getcfg($param, $html = false)
{
    if (wf::$container === null) {
        return 'dummy';
    }

    $value = \wf::$container->hasParameter($param) ?
            ($html ?
                \classes\web\StringHelper::encode(\wf::$container->getParameter($param))
                : \wf::$container->getParameter($param))
            :'dummy';

    return $value;
}
