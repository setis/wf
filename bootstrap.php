<?php

if (!defined('WF_ENV')) {
    define('WF_ENV', getenv('WF_ENV') ?: 'prod');
}

if (!defined('WF_DEBUG')) {
    define('WF_DEBUG', getenv('WF_DEBUG') ?: false);
}

if (!defined('WF_MAINTENANCE')) {
    define('WF_MAINTENANCE', getenv('WF_MAINTENANCE') ?: false);
}

$loader = require __DIR__.'/app/autoload.php';
$kernel = new AppKernel(WF_ENV, WF_DEBUG);
$kernel->boot();
