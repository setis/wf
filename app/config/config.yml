imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }
    - { resource: local_services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: ru
    router.request_context.host: '%hostname%'
    router.request_context.scheme: '%http_schema%'
    graylog.handler.server: '%graylog_server%'
    graylog.handler.port: 12201
    graylog.handler.chunk_size: 8154

framework:
    #esi:             ~
    translator:      { fallbacks: [en] }
    secret:          '%secret%'
    router:
        resource: '%kernel.root_dir%/config/routing.yml'
        strict_requirements: ~
    form:            ~
    csrf_protection: true
    validation:      { enable_annotations: true }
    #serializer:      { enable_annotations: true }
    templating:
        engines: ['twig', 'php']
    session:
        handler_id: ~
    default_locale:  '%locale%'
    trusted_hosts:   ~
    trusted_proxies: ~
    fragments:       ~
    http_method_override: true
    assets: ~

# Twig Configuration
twig:
    debug:            '%kernel.debug%'
    strict_variables: '%kernel.debug%'
    globals:
        maintenance: '%maintenance%'
        system_name: '%name%'
        system_developer_email: '%developer_email%'
        float_button: '@wf.widget.float_button'
        impersonate: '@wf.widget.impersonate'
    form_themes:
        - bootstrap_3_layout.html.twig
        - LexikFormFilterBundle:Form:form_div_layout.html.twig

# Assetic Configuration
assetic:
    debug:          '%kernel.debug%'
    use_controller: '%kernel.debug%'
    filters:
        cssrewrite: ~

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_mysql
        host:     '%database_host%'
        port:     '%database_port%'
        dbname:   '%database_name%'
        user:     '%database_user%'
        password: '%database_password%'
        charset:  UTF8
        types:
            geometry:   CrEOF\Spatial\DBAL\Types\GeometryType
            point:      CrEOF\Spatial\DBAL\Types\Geometry\PointType
            polygon:    CrEOF\Spatial\DBAL\Types\Geometry\PolygonType
            linestring: CrEOF\Spatial\DBAL\Types\Geometry\LineStringType
            dateinterval: Gorserv\Gerp\CoreBundle\Doctrine\DBAL\Types\DateIntervalType
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: "%kernel.root_dir%/data/data.db3"
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #     path:     "%database_path%"

    orm:
        dql:
            numeric_functions:
                st_contains:     CrEOF\Spatial\ORM\Query\AST\Functions\MySql\STContains
                contains:        CrEOF\Spatial\ORM\Query\AST\Functions\MySql\Contains
                st_area:         CrEOF\Spatial\ORM\Query\AST\Functions\MySql\Area
                st_geomfromtext: WF\Doctrine\STGeomFromText
                st_intersects:   CrEOF\Spatial\ORM\Query\AST\Functions\MySql\STIntersects
                st_buffer:       CrEOF\Spatial\ORM\Query\AST\Functions\MySql\STBuffer
                point:           CrEOF\Spatial\ORM\Query\AST\Functions\MySql\Point
                rand:            WF\Doctrine\RandFunction
                timestampdiff:   Oro\ORM\Query\AST\Functions\Numeric\TimestampDiff
                dayofyear:       Oro\ORM\Query\AST\Functions\SimpleFunction
                dayofmonth:      Oro\ORM\Query\AST\Functions\SimpleFunction
                dayofweek:       Oro\ORM\Query\AST\Functions\SimpleFunction
                week:            Oro\ORM\Query\AST\Functions\SimpleFunction
                day:             Oro\ORM\Query\AST\Functions\SimpleFunction
                hour:            Oro\ORM\Query\AST\Functions\SimpleFunction
                minute:          Oro\ORM\Query\AST\Functions\SimpleFunction
                month:           Oro\ORM\Query\AST\Functions\SimpleFunction
                quarter:         Oro\ORM\Query\AST\Functions\SimpleFunction
                second:          Oro\ORM\Query\AST\Functions\SimpleFunction
                year:            Oro\ORM\Query\AST\Functions\SimpleFunction
                round:           Oro\ORM\Query\AST\Functions\SimpleFunction
                sum_of_subquery: Gorserv\Gerp\ReportsBundle\Doctrine\SumOfSubqueryFunction
                sign:            Oro\ORM\Query\AST\Functions\Numeric\Sign
                pow:             Oro\ORM\Query\AST\Functions\Numeric\Pow
            datetime_functions:
                date:            Oro\ORM\Query\AST\Functions\SimpleFunction
                time:            Oro\ORM\Query\AST\Functions\SimpleFunction
                timestamp:       Oro\ORM\Query\AST\Functions\SimpleFunction
                convert_tz:      Oro\ORM\Query\AST\Functions\DateTime\ConvertTz
            string_functions:
                DATE_FORMAT:     DoctrineExtensions\Query\Mysql\DateFormat
                md5:             Oro\ORM\Query\AST\Functions\SimpleFunction
                group_concat:    Oro\ORM\Query\AST\Functions\String\GroupConcat
                concat_ws:       Oro\ORM\Query\AST\Functions\String\ConcatWs
                cast:            Oro\ORM\Query\AST\Functions\Cast
                replace:         Oro\ORM\Query\AST\Functions\String\Replace


        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true
        mappings:
            Models:
                type: annotation
                dir: '%kernel.root_dir%/../src/orm'
                is_bundle: false
                prefix: models
                alias: Gerp
    # An array of filters
        filters:
            user.active:
                class: Gorserv\Gerp\AppBundle\Doctrine\Filter\UserActiveFilter
                enabled: false

doctrine_migrations:
    dir_name: '%kernel.root_dir%/../src/orm/migrations'
    namespace: DoctrineMigrations
    table_name: doctrine_migration_versions
    name: GERP Migrations

stof_doctrine_extensions:
    default_locale: en_US
    orm:
        default:
            timestampable: true

monolog:
    use_microseconds: false
    channels:
        - debug
        - soap
        - console
        - user_security # for logging user actions
# Swiftmailer Configuration
swiftmailer:
    default_mailer: file_spooler
    mailers:
        file_spooler:
            transport: '%mailer_transport%'
            host:      '%mailer_host%'
            username:  '%mailer_user%'
            password:  '%mailer_password%'
            port:      '%mailer_port%'
            encryption: '%mailer_encryption%'
            auth_mode: '%mailer_auth_mode%'
            spool:     { type: file }
        memory_spooler:
            transport: '%mailer_transport%'
            host:      '%mailer_host%'
            username:  '%mailer_user%'
            password:  '%mailer_password%'
            port:      '%mailer_port%'
            encryption: '%mailer_encryption%'
            auth_mode: '%mailer_auth_mode%'
            spool:     { type: memory }


oneup_flysystem:
    adapters:
        task_adapter:
            local:
                directory: '%kernel.root_dir%/../web/gss_files'
        user_photo_adapter:
            local:
                directory: '%kernel.root_dir%/../web/gss_files/photos'
        call_records_adapter:
            local:
                directory: '%kernel.root_dir%/../web/gss_files/callrecords'

    filesystems:
        task_fs:
            adapter: task_adapter
            mount: task_fs
        user_photo:
            adapter: user_photo_adapter
            mount: user_photo
        call_records:
            adapter: call_records_adapter
            mount: call_records

liip_imagine:
    filter_sets:
        user_photo_filter:
            filters:
                auto_rotate: ~
                downscale:
                    max: [700, 500]
    loaders:
        user_photo:
            flysystem:
                filesystem_service: oneup_flysystem.user_photo_filesystem
    data_loader: user_photo

vich_uploader:
    db_driver: orm
    storage: flysystem

    mappings:
        task_file:
            uri_prefix: /
            upload_destination: task_fs
            namer: vich_uploader.namer_uniqid
        user_photo:
            uri_prefix: /
            upload_destination: user_photo
            namer: vich_uploader.namer_uniqid

fos_rest:
    exception: ~
    routing_loader:
        include_format: false
    view:
        view_response_listener: 'force'
        formats:
            xml: true
            json: true
    body_converter:
        enabled: true
    format_listener:
        rules:
            - { path: '^/qiwi', priorities: ['xml'], fallback_format: ~, prefer_extension: false }
            - { path: '^/', priorities: ['html'], fallback_format: 'html' }

lexik_form_filter:
    listeners:
        doctrine_orm: true

knp_paginator:
    page_range: 10                      # default page range used in pagination control
    default_options:
        page_name: page                # page query parameter name
        sort_field_name: sort          # sort field query parameter name
        sort_direction_name: direction # sort direction query parameter name
        distinct: true                 # ensure distinct results, useful when ORM queries are using GROUP BY statements
    template:
        pagination: KnpPaginatorBundle:Pagination:twitter_bootstrap_v3_pagination.html.twig     # sliding pagination controls template
        sortable: KnpPaginatorBundle:Pagination:sortable_link.html.twig # sort link template

knp_menu:
    # use "twig: false" to disable the Twig extension and the TwigRenderer
    twig:
        template: KnpMenuBundle::menu.html.twig
    #  if true, enables the helper for PHP templates
    templating: false
    # the renderer to use, list is also available by default
    default_renderer: twig

gorserv_gerp_schedule:
    slot_manager: app.schedule.slot_manager
    timeframe_manager: app.schedule.timeframe_manager

jms_payment_core:
    secret: 18209b2471d88407412752fdf4013f58

chewbacco_payment_qiwi_wallet:
    login: your username
    password: your password
