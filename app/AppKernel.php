<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{

    public function registerBundles()
    {
        $bundles = [
            // Out of box bundles
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            // Other useful bundles
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Oneup\FlysystemBundle\OneupFlysystemBundle(),
            new Vich\UploaderBundle\VichUploaderBundle(),
            new Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle(),
            new Cron\CronBundle\CronCronBundle(),

            // Knp bundles
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),

            //  Easy extended bundles
            new Application\Cron\CronBundle\ApplicationCronCronBundle(),

            //  Gorserv bundles
            new Gorserv\Gerp\CoreBundle\GorservGerpCoreBundle(),
            new Gorserv\Gerp\AddressBundle\GorservGerpAddressBundle(),
            new Gorserv\Gerp\IntegrationBundle\GorservGerpIntegrationBundle(),
            new Gorserv\Gerp\DemoBundle\GorservGerpDemoBundle(),
            new Gorserv\Gerp\DadataBundle\GorservGerpDadataBundle(),
            new Gorserv\Gerp\ScheduleBundle\GorservGerpScheduleBundle(),
            new Gorserv\Gerp\AppBundle\GorservGerpAppBundle(),

            new Liip\ImagineBundle\LiipImagineBundle(),
            new Gorserv\Gerp\ReportsBundle\GorservGerpReportsBundle(),
            new JMS\Payment\CoreBundle\JMSPaymentCoreBundle(),
            new Chewbacco\Payment\QiwiWalletBundle\ChewbaccoPaymentQiwiWalletBundle(),
            new Gorserv\Gerp\BillingBundle\GorservGerpBillingBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle();
        }

        return $bundles;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__) . '/var/cache/' . $this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__) . '/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir() . '/config/config_' . $this->getEnvironment() . '.yml');
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    protected function initializeContainer()
    {
        parent::initializeContainer();

        spl_autoload_register('oldAutoload');
        if (!defined('ENV')) {
            define('ENV', $this->container->getParameter('kernel.environment'));
        }

        wf::getInstance($this->container);
    }

}
