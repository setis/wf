dbserver_packages:
  debconf.set:
    - name: mariadb-server-10.1
    - data:
        'mysql-server/root_password': { 'type': 'password', 'value': "{{ pillar['mysql_root_password'] }}" }
        'mysql-server/root_password_again': { 'type': 'password', 'value': "{{ pillar['mysql_root_password'] }}" }
  pkgrepo.managed:
    - name: 'deb [arch=amd64,i386,ppc64el] http://mirror.timeweb.ru/mariadb/repo/10.1/ubuntu trusty main'
    - humanname: MariaDB Repo
    - dist: trusty
    - keyserver: keyserver.ubuntu.com
    - keyid: 1BB943DB
    - refresh_db: true
  pkg.installed:
    - skip_verify: True
    - pkgs:
      - mariadb-server
      - mariadb-client
      - python-mysqldb

/etc/mysql/my.cnf:
  file.managed:
    - source: salt://db/my.cnf
    - user: root
    - group: root
    - mode: 644
    - required:
      - pkg: dbserver_packages
    - watch_in:
      - service: mysql

mysql:
  service.running:
    - required:
      - pkg: dbserver_packages
    - watch:
      - file: /etc/mysql/my.cnf
  mysql_query.run:
    - name: SQL Mode - PostgreSQL
    - database: mysql
    - query: "SET GLOBAL sql_mode = 'POSTGRESQL';"
    - connection_user: root
    - connection_pass: {{ pillar['mysql_root_password'] }}
    - required:
      - service: mysql

allow_remote_root:
  mysql_user.present:
    - name: root
    - host: "%"
    - password: {{ pillar['mysql_root_password'] }}
    - connection_user: root
    - connection_pass: {{ pillar['mysql_root_password'] }}
    - connection_host: localhost
    - require:
      - service: mysql
  mysql_grants.present:
    - user: root
    - grant: "all"
    - database: "*.*"
    - host: "%"
    - connection_user: root
    - connection_pass: {{ pillar['mysql_root_password'] }}
    - require:
      - service: mysql
