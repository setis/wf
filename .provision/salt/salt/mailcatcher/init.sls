ruby_repo:
  pkgrepo.managed:
    - ppa: brightbox/ruby-ng

mailcatcher_packages:
  pkg.installed:
    - pkgs:
      - build-essential
      - software-properties-common
      - libsqlite3-dev
      - ruby2.3
      - ruby2.3-dev

mailcatcher:
  gem.installed: []
  file.managed:
    - name: /etc/init.d/mailcatcher
    - source: salt://mailcatcher/mailcatcher
    - mode: 755
    - user: root
    - group: root
    - require:
      - gem: mailcatcher
  service.running:
    - enable: true
    - require:
      - file: mailcatcher
