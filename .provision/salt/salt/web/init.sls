php_repo:
  pkgrepo.managed:
    - name: deb http://ppa.launchpad.net/ondrej/php/ubuntu trusty main
    - humanname: PHP Ondrey Repo
    - dist: trusty
    - keyserver: keyserver.ubuntu.com
    - keyid: E5267A6C
    - refresh_db: true

webserver_packages:
  pkg.installed:
    - pkgs:
      - nginx
      - php7.0
      - php7.0-mysql
      - php7.0-fpm
      - php7.0-intl
      - php7.0-imap
      - php7.0-mbstring
      - php7.0-soap
      - php7.0-zip
      - php7.0-pgsql
      - php7.0-curl
      - php7.0-xml
      - php7.0-gd
      - php7.0-sqlite
      - php7.0-opcache
      - php-memcached
      - php-xdebug
    - required: php_repo

nodejs:
  cmd.run:
    - name: "wget -O - https://deb.nodesource.com/setup_4.x | sudo -E bash -"
    - unless: test -L /usr/bin/node
  pkg.installed:
    - pkgs:
      - nodejs
    - required:
      - cmd: nodejs
  file.symlink:
    - name: /usr/bin/node
    - target: /usr/bin/nodejs
    - required:
      - pkg: nodejs

bower:
  npm.installed: []
  require:
    - pkg:
      - nodejs

php-fpm:
  file.replace:
    - name: /etc/php/7.0/fpm/php.ini
    - pattern: ^(;cgi.fix_pathinfo.*)$
    - repl: cgi.fix_pathinfo=0
  service.running:
    - name: php7.0-fpm
    - enable: True

/var/log/php7.0-fpm.log:
  file.managed:
    - contents: ''
    - mode: 666

/var/log/php-slow.log:
  file.managed:
    - contents: ''
    - mode: 666

/var/log/php-error.log:
  file.managed:
    - contents: ''
    - mode: 666

/var/log/php-access.log:
  file.managed:
    - contents: ''
    - mode: 666

#php-fpm7.0 -y /etc/php/7.0/fpm/php-fpm.conf # for debug config
/etc/php/7.0/fpm/pool.d/www.conf:
  file.blockreplace:
    - marker_start: "; BLOCK TOP : salt managed zone : local services : please do not edit"
    - marker_end: "; BLOCK BOTTOM : end of salt managed zone --"
    - content: |
        user = vagrant
        group = vagrant
        listen = /run/php/php7.0-fpm.sock
        listen.owner = www-data
        listen.group = www-data
        pm = dynamic
        pm.max_children = 5
        pm.start_servers = 2
        pm.min_spare_servers = 1
        pm.max_spare_servers = 3
        access.log = /var/log/php-access.log
        slowlog = /var/log/php-slow.log
        request_slowlog_timeout = 10
        catch_workers_output = yes
        clear_env = true
        php_flag[display_errors] = no
        php_admin_value[error_log] = /var/log/php-error.log
        php_admin_value[log_errors] = on
        php_admin_value[memory_limit] = 128m
        php_admin_value[sendmail_path] = /usr/bin/env catchmail -f gss@local
    - show_changes: True
    - append_if_not_found: True
    - watch_in:
      - service: php-fpm
    - require:
      - file: /var/log/php-access.log
      - file: /var/log/php-error.log
      - file: /var/log/php-slow.log

get-composer:
  cmd.run:
    - name: 'CURL=`which curl`; $CURL -sS https://getcomposer.org/installer | php'
    - unless: test -f /usr/bin/composer
    - cwd: /tmp
    - watch:
      - pkg: webserver_packages

install-composer:
  cmd.wait:
    - name: mv /tmp/composer.phar /usr/bin/composer
    - cwd: /tmp
    - watch:
      - cmd: get-composer
      - file: nodejs

nginx:
  file.directory:
    - name: /var/www
  pkg.installed: []
  service.running: []

