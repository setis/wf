date > /tmp/salt-run:
  cmd.run

salt-minion:
  service.dead:
    - enable: False

set timezone:
  cmd.run:
    - name: 'TDCTL=`which timedatectl`; $TDCTL set-timezone {{ pillar['timezone'] }}'
    - runas: root

common_packages:
  pkg.installed:
    - pkgs:
      - git
      - vim-nox
      - ntp
      - curl
      - bash
      - tmux
      - build-essential
      - htop
      - python-software-properties
      - dos2unix
