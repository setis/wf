{% set mysql_root_password = pillar['mysql_root_password'] %}
{% set db_user_name = pillar['gerp']['mysql_wf_db'] %}
{% set env = pillar['gerp']['env'] %}
{% set db_name = pillar['gerp']['mysql_wf_db'] %}

# database
"{{db_name}}":
  mysql_database.present:
    - character_set: utf8
    - connection_user: root
    - connection_pass: {{mysql_root_password}}
    - connection_host: localhost
    - require:
      - service: mysql

vendor_libs:
  environ.setenv:
    - name: WF_ENV
    - value: "{{env}}"
    - update_minion: True
  composer.installed:
    - name: {{ pillar['gerp']['approot'] }}
    - require:
      - cmd: install-composer

migrations:
  cmd.run:
    - name: {{ pillar['gerp']['approot'] }}/bin/console doctrine:migrations:migrate --env dev -n
    - require:
      - composer: vendor_libs

# webserver
gerp.fpm-log:
  file.managed:
    - name: /var/log/fpm-php.www.log
    - user: www-data
    - group: www-data
    - mode: 660

gerp_php-fpm_xdebug:
  file.append:
    - name: /etc/php/7.0/mods-available/xdebug.ini
    - text:
      - xdebug.remote_enable = on
      - xdebug.remote_connect_back = on
      - xdebug.idekey = PHPSTORM
    - watch_in:
      - service: php-fpm

gerp_nginx:
  file.managed:
    - name: /etc/nginx/sites-available/default
    - source: salt://gerp/gerp.conf
    - watch_in:
      - service: nginx

php-fpm_debugging:
  file.blockreplace:
    - name: /etc/php/7.0/fpm/pool.d/www.conf
    - marker_start: "; BEGIN project debug variables"
    - marker_end: "; END project debug variables --"
    - content: |
        env[WF_ENV] = {{ pillar['gerp']['env'] }}
        ;env[WF_DEBUG] = true
    - show_changes: True
    - append_if_not_found: True
    - watch_in:
      - service: php-fpm

/etc/php/7.0/fpm/php.ini:
  file.blockreplace:
    - marker_start: "; BEGIN project tunning"
    - marker_end: "; END project tunning --"
    - content: |
        upload_max_filesize = 256M
        post_max_siz = 256M
        max_input_vars = 10000
    - show_changes: true
    - append_if_not_found: true
    - watch_in:
      - service: php-fpm
