gerp:
  env: dev
  approot: /var/www/html
  git_repo: ssh://git@git.gorserv.ru:42874/gorserv/workflow.git
  git_key: /srv/salt/deploy_keys/id_rsa
  mysql_wf_db: gerp
  mysql_wf_host: localhost
  mysql_wf_port: 3306
