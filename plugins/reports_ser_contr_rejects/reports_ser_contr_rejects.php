<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;



 
class reports_ser_contr_rejects_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT_SER_REJECT", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListSer(@$_POST["cnt_id"]));
            } 
            if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getOptList")) {
                $st = new adm_rejects_plugin();
                $tpl->setVariable("FILTER_REJECT_OPTIONS", $st->getOptList(@$_POST["reject_id"], 'services'));
            } 
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || isset($_POST["createxlsversion"])) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
            $rtpl->setVariable("DATETO", $_POST['dateto']);
            if ($_POST['reject_id'] != 0) {
                if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getByID")) {
                    $st = new adm_rejects_plugin();
                    if ($state = $st->getByID($_POST['reject_id'])) {
                        $rtpl->setCurrentBlock("taskstatetitle");
                            $rtpl->setVariable("TASKSTATETITLE", $state);
                            $rtpl->parse("taskstatetitle");     
                    } else {
                        UIError("Отсутствует Причина отказа с указанным ID.".$_POST['reject_id']);
                    }
                } else {
                    UIError("Отсутствует модуль Причины отказов.");
                }
            } else {
                if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getList")) {
                    $st = new adm_rejects_plugin();
                    $sList = $st->getList('services');
                    $rtpl->setVariable("STATECOUNT", count($sList));
                    if ($sList) {
                        foreach($sList as $item) {
                            $rtpl->setCurrentBlock("taskstatetitle");
                            $rtpl->setVariable("TASKSTATETITLE", $item);
                            $rtpl->parse("taskstatetitle");                        
                        }
                    } else {
                        UIError("Отсутствует словарь Причин отказа.");
                    }
                } else {
                    UIError("Отсутствует модуль Причины отказа.");
                }
            }
            if ($_POST['cnt_id']!=0) {
                if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getByID")) {
                    $ct = new kontr_plugin();
                    if ($kontr = $ct->getByID($_POST['cnt_id'])) {
                        $rtpl->setCurrentBlock("rep_row");
                        $rtpl->setVariable("CTRG_NAME", $kontr);
                        if ($_POST['reject_id'] != 0) {
                            $sql = "SELECT COUNT(t.id) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id WHERE tick.cnt_id=".$DB->F($_POST['cnt_id'])." AND tick.reject_id=".$DB->F($_POST['reject_id'])." AND DATE_FORMAT(t.date_reg, '%Y-%m-%d')>=".$DB->F(substr($_POST['datefrom'], 6,4)."-".substr($_POST['datefrom'], 3,2)."-".substr($_POST['datefrom'], 0,2))." AND DATE_FORMAT(t.date_reg, '%Y-%m-%d')<=".$DB->F(substr($_POST['dateto'], 6,4)."-".substr($_POST['dateto'], 3,2)."-".substr($_POST['dateto'], 0,2)).";";
                            $tCount = $DB->getField($sql);
                            $rtpl->setCurrentBlock("ctscv");
                            $rtpl->setVariable("STATEVALCOUNT", intval($tCount) != 0 ? "<strong>".intval($tCount)."</strong>" : "0");
                            $rtpl->parse("ctscv");
                        } else {
                            $sList = $st->getList('services');
                            foreach($sList as $key => $item) {
                                $sql = "SELECT COUNT(t.id) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id WHERE tick.cnt_id=".$DB->F($_POST['cnt_id'])." AND tick.reject_id=".$DB->F($key)." AND DATE_FORMAT(t.date_reg, '%Y-%m-%d')>=".$DB->F(substr($_POST['datefrom'], 6,4)."-".substr($_POST['datefrom'], 3,2)."-".substr($_POST['datefrom'], 0,2))." AND DATE_FORMAT(t.date_reg, '%Y-%m-%d')<=".$DB->F(substr($_POST['dateto'], 6,4)."-".substr($_POST['dateto'], 3,2)."-".substr($_POST['dateto'], 0,2)).";";
                                $tCount = $DB->getField($sql);
                                $rtpl->setCurrentBlock("ctscv");
                                $rtpl->setVariable("STATEVALCOUNT", intval($tCount) != 0 ? "<strong>".intval($tCount)."</strong>" : "0");
                                $rtpl->parse("ctscv");
                            }
                            
                        }
                        $rtpl->parse("rep_row");     
                    } else {
                        UIError("Отсутствует Контрагент с указанным ID.");
                    }
                } else {
                    UIError("Отсутствует модуль Контрагенты.");
                }
            } else {
                if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getListSer")) {
                    $ct = new kontr_plugin();
                    if ($kontr = $ct->getListSer()) {
                        foreach($kontr as $key => $item) {
                            $rtpl->setCurrentBlock("rep_row");
                            $rtpl->setVariable("CTRG_NAME", $item);
                            if ($_POST['reject_id']!=0) {
                                $sql = "SELECT COUNT(t.id) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id WHERE tick.cnt_id=".$DB->F($key)." AND tick.reject_id=".$DB->F($_POST['reject_id'])." AND DATE_FORMAT(t.date_reg, '%Y-%m-%d')>=".$DB->F(substr($_POST['datefrom'], 6,4)."-".substr($_POST['datefrom'], 3,2)."-".substr($_POST['datefrom'], 0,2))." AND DATE_FORMAT(t.date_reg, '%Y-%m-%d')<=".$DB->F(substr($_POST['dateto'], 6,4)."-".substr($_POST['dateto'], 3,2)."-".substr($_POST['dateto'], 0,2)).";";
                                $tCount = $DB->getField($sql);
                                $rtpl->setCurrentBlock("ctscv");
                                $rtpl->setVariable("STATEVALCOUNT", intval($tCount) != 0 ? "<strong>".intval($tCount)."</strong>" : "0");
                                $rtpl->parse("ctscv");
                            } else {
                                $sList = $st->getList('services');
                                foreach($sList as $k => $item) {
                                    $sql = "SELECT COUNT(t.id) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id WHERE tick.cnt_id=".$DB->F($key)." AND tick.reject_id=".$DB->F($k)." AND DATE_FORMAT(t.date_reg, '%Y-%m-%d')>=".$DB->F(substr($_POST['datefrom'], 6,4)."-".substr($_POST['datefrom'], 3,2)."-".substr($_POST['datefrom'], 0,2))." AND DATE_FORMAT(t.date_reg, '%Y-%m-%d')<=".$DB->F(substr($_POST['dateto'], 6,4)."-".substr($_POST['dateto'], 3,2)."-".substr($_POST['dateto'], 0,2)).";";
                                    $tCount = $DB->getField($sql);
                                    $rtpl->setCurrentBlock("ctscv");
                                    $rtpl->setVariable("STATEVALCOUNT", intval($tCount) != 0 ? "<strong>".intval($tCount)."</strong>" : "0");
                                    $rtpl->parse("ctscv");
                                }
                            }
                            $rtpl->parse("rep_row");                                   
                        }
                    } else {
                        UIError("Отсутствует Контрагент с указанным ID.");
                    }
                } else {
                    UIError("Отсутствует модуль Контрагенты.");
                }
            }
            
            
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_rejects_".$_POST['datefrom']."-".$_POST['dateto'].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 
    
}
?>