<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2011
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\User;
use WF\Bills\Controller\BillTypesController;
use WF\Slack\Controller\SlackController;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
class adm_interface_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function menu()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));

        $tpl->loadTemplatefile($USER->getTemplate() . "/admin.tmpl.htm");

        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $fail = true;
        if (!$USER->checkAccess('adm_skp_edizm')) $tpl->setVariable('DIS_SKPEDIZM', 'disabled'); else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }

        if ($USER->checkAccess(SlackController::class)) {
            $tpl->setVariable("VISIBLE_SLACK", "visible");
            $fail = false;
        } else {
            $tpl->setVariable("VISIBLE_SLACK", "hidden");
        }

        if (!$USER->checkAccess('adm_skp_catnames')) $tpl->setVariable('DIS_SKPCATNAMES', 'disabled'); else {
            $tpl->setVariable("VISIBLE9", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_skp_wtypes')) $tpl->setVariable('DIS_SKPWTYPES', 'disabled'); else {
            $tpl->setVariable("VISIBLE9", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_skp_price')) $tpl->setVariable('DIS_SKPPRICE', 'disabled'); else {
            $tpl->setVariable("VISIBLE10", "visible");
            $fail = false;
        }
        if(!$USER->checkAccess('adm_users', User::ACCESS_WRITE)){
            $tpl->setVariable('DIS_ROLES', 'disabled');
        }
        if (!$USER->checkAccess('adm_users')) {
            $tpl->setVariable('DIS_USERS', 'disabled');
        } else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('adm_statuses'))
            $tpl->setVariable('DIS_STATUSES', 'disabled');
        else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('adm_sc')) $tpl->setVariable('DIS_SC', 'disabled'); else {
            $tpl->setVariable("VISIBLE2", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_empl')) $tpl->setVariable('DIS_EMPL', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_emplsched')) $tpl->setVariable('DIS_EMPLSCHED', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_ticktypes')) $tpl->setVariable('DIS_TICKTYPES', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_techprice')) $tpl->setVariable('DIS_TECHPRICE', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }


        if (!$USER->checkAccess('adm_ticket_rtsubtypes')) $tpl->setVariable('DIS_RTCONST', 'disabled'); else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }


        if (!$USER->checkAccess('adm_ticket_sources')) $tpl->setVariable('DIS_SOURCES', 'disabled'); else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('adm_mgts_tc')) $tpl->setVariable('DIS_MGTS_TC', 'disabled'); else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('adm_rejects')) $tpl->setVariable('DIS_REJREASONS', 'disabled'); else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('adm_ticket_gptypes')) $tpl->setVariable('DIS_GPTYPES', 'disabled'); else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('adm_ticket_tttypes')) $tpl->setVariable('DIS_TTTYPES', 'disabled'); else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_gp_reason')) $tpl->setVariable('DIS_GPREASON', 'disabled'); else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_gp_comment')) $tpl->setVariable('DIS_GPCOMMENT', 'disabled'); else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }

        if ($USER->checkAccess('adm_clearbase', User::ACCESS_WRITE)) {
            $tpl->touchBlock("clear");
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_areas')) $tpl->setVariable('DIS_AREA', 'disabled'); else {
            $tpl->setVariable("VISIBLE5", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_districts')) $tpl->setVariable('DIS_DISTR', 'disabled'); else {
            $tpl->setVariable("VISIBLE5", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('adm_material_cats')) $tpl->setVariable('DIS_CATTMC', 'disabled'); else {
            $tpl->setVariable("VISIBLE6", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_materials')) $tpl->setVariable('DIS_TMC', 'disabled'); else {
            $tpl->setVariable("VISIBLE6", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('project_types')) $tpl->setVariable('DIS_PTYPES', 'disabled'); else {
            $tpl->setVariable("VISIBLE7", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess(BillTypesController::class)) {
            $tpl->setVariable('DIS_BTYPES', 'disabled');
        } else {
            $tpl->setVariable("VISIBLE8", "visible");
            /** @var \Symfony\Component\Routing\Router $router */
            $router = $this->getContainer()->get('router');
            $tpl->setVariable('LINK_BTYPES', $router->generate('bill_types'));
            $fail = false;
        }

        if (!$USER->checkAccess('kontr_proptypes')) $tpl->setVariable('DIS_PROPTYPES', 'disabled'); else {
            $tpl->setVariable("VISIBLE12", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('adm_acts_wtypes')) $tpl->setVariable('DIS_ACTTYPES', 'disabled'); else {
            $tpl->setVariable("VISIBLE12", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('bill_payers')) $tpl->setVariable('DIS_PAYERS', 'disabled'); else {
            $tpl->setVariable("VISIBLE12", "visible");
            $fail = false;
        }


        if (!$USER->checkAccess('user_positions')) $tpl->setVariable('DIS_USERPOS', 'disabled'); else {
            $fail = false;
            $tpl->setVariable("VISIBLE1", "visible");
        }

        if (!$USER->checkAccess('q_list')) $tpl->setVariable('DIS_QLIST', 'disabled'); else {
            $fail = false;
            $tpl->setVariable("VISIBLE13", "visible");
        }

        if (!$USER->checkAccess('import_conn_akado')) $tpl->setVariable('DIS_AKADO', 'disabled'); else {
            $fail = false;
            $tpl->setVariable("VISIBLE14", "visible");
        }

        if (!$USER->checkAccess('load_price_mgts')) $tpl->setVariable('DIS_MGTS', 'disabled'); else {
            $fail = false;
            $tpl->setVariable("VISIBLE14", "visible");
        }

        if (!$USER->checkAccess('adm_task_photo')) $tpl->setVariable('DIS_PHOTOS', 'disabled'); else {
            $fail = false;
            $tpl->setVariable("VISIBLE4", "visible");
        }


        $tpl->setVariable('SMS_NUMBER', getcfg('sms_number'));
        if ($fail) {
            UIError("У Вас нет доступа ни к одному из модулей Настроек системы. Обратитесь к Вашему руководителю!", "Недостаточно прав доступа");
        }
        UIHeader($tpl);
        $tpl->show();
    }
}

?>
