<?php

/**
 * Plugin Header
 *
 * @author kblp
 * @copyright 2011
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Настройки";
$PLUGINS[$plugin_uid]['hidden'] = false;

if (wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['menu'] = "Настройки системы";
}
