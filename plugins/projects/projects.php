<?php
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use classes\User;
use Symfony\Component\HttpFoundation\Request;

class projects_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function viewlist(Request $request)
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/list.tmpl.htm");
        else
            $tpl->loadTemplatefile("list.tmpl.htm");
        if ($USER->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }
        $sort_ids = [
            3 => "ID проекта",
            2 => "Дата создания",
            1 => "Дата изменения",
            4 => "Срок выполнения",
            5 => "Тип проекта",
            6 => "Автор",
            7 => "Контрагент",
            8 => "Приоритет",
        ];
        $sort_sql = [
            3 => "TASK_ID",
            2 => "ts.date_reg",
            1 => "ts.lastcmm_date",
            4 => "deadline",
            5 => "ptype",
            6 => "AUTHOR_FIO",
            7 => "zakazchik",
            8 => "PRIORITY",
        ];

        $sort_order = [
            'DESC' => "Обратный",
            'ASC' => "Прямой",
        ];

        $rpp = [
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250",
        ];

        $priority = [
            0 => "-- все",
            1 => "1",
            2 => "2",
            3 => "3",
            4 => "4",
            5 => "5",
        ];

        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_status_id'] = array_filter(
            $this->getCookie('filter_status_id', $request->query->has('filter_status_id') ? $request->get('filter_status_id') : null),
            function ($e) {
                return !empty($e) && -1 != $e;
            }
        );
        $_REQUEST['filter_author_ids'] = $this->getCookie('filter_author_ids', $_REQUEST['filter_author_ids']);

        $_REQUEST['filter_author_fio'] = $this->getCookie('filter_author_fio', $_REQUEST['filter_author_fio']);
        $_REQUEST['filter_ispoln_ids'] = $this->getCookie('filter_ispoln_ids', $_REQUEST['filter_ispoln_ids']);
        $_REQUEST['filter_ispoln_fio'] = $this->getCookie('filter_ispoln_fio', $_REQUEST['filter_ispoln_fio']);
        $_REQUEST["filter_ptype_id"] = $this->getCookie('filter_ptype_id', $_REQUEST['filter_ptype_id']);

        $_REQUEST["filter_zakaz_id"] = $this->getCookie('filter_zakaz_id', $_REQUEST['filter_zakaz_id']);
        $_REQUEST["task_contents"] = $this->getCookie('task_contents', $_REQUEST['task_contents']);
        $_REQUEST["filter_priority"] = $this->getCookie('filter_priority', $_REQUEST['filter_priority']);
        $_REQUEST["filter_pc_date_from"] = $this->getCookie('filter_pc_date_from', $_REQUEST['filter_pc_date_from']);

        $_REQUEST["filter_pc_date_to"] = $this->getCookie('filter_pc_date_to', $_REQUEST['filter_pc_date_to']);
        $_REQUEST["filter_pd_date_from"] = $this->getCookie('filter_pd_date_from', $_REQUEST['filter_pd_date_from']);
        $_REQUEST["filter_pd_date_to"] = $this->getCookie('filter_pd_date_to', $_REQUEST['filter_pd_date_to']);
        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', $_REQUEST['filter_sort_id']);

        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', $_REQUEST['filter_sort_order']);
        $_REQUEST['filter_rpp'] = $this->getCookie('filter_rpp', $_REQUEST['filter_rpp']);

        $q = $request->query;
        if (!$_REQUEST['filter_sort_id']) $_REQUEST['filter_sort_id'] = 1;
        if (!$_REQUEST['filter_sort_order']) $_REQUEST['filter_sort_order'] = "DESC";
        if (!$_REQUEST['filter_rpp']) $_REQUEST['filter_rpp'] = 20;
        if ($_REQUEST['filter_priority'] == "") $_REQUEST['filter_priority'] = 0;


        $tpl->setVariable('FILTER_ID', htmlspecialchars($_REQUEST['filter_id']));
        $status = wf::$em->getRepository(\models\TaskStatus::class)->findBy(['pluginUid' => \models\Task::PROJECT]);
        $statuses = array_map(function (\models\TaskStatus $item) {
            return [
                'id' => $item->getId(),
                'text' => $item->getName()
            ];
        }, $status);
        $statusStyles = implode("\n", array_map(function (\models\TaskStatus $item) {
            return ".status_{$item->getId()}{
                background:{$item->getColor()};
            }";
        }, $status));
        $statuses[] = [
            'id' => 1000,
            'text' => 'Активные задачи'
        ];
        $statusStyles .= "\n" . '.status_1000{  background:#99cc33 }';
        $tpl->setVariable('STATUS_STYLE', $statusStyles);
        $tpl->setVariable('FILTER_STATUS_OPTIONS', json_encode($statuses));
        $tpl->setVariable('FILTER_STATUS_OPTIONS_SET', (empty($_REQUEST['filter_status_id'])) ? '""' : json_encode($_REQUEST['filter_status_id']));
        $tpl->setVariable('FILTER_AUTHOR_IDS', htmlspecialchars($_REQUEST['filter_author_ids']));
        $tpl->setVariable('FILTER_AUTHOR_FIO', htmlspecialchars_decode($_REQUEST['filter_author_fio']));

        $tpl->setVariable('FILTER_ISPOLN_IDS', htmlspecialchars($_REQUEST['filter_ispoln_ids']));
        $tpl->setVariable('FILTER_ISPOLN_FIO', htmlspecialchars_decode($_REQUEST['filter_ispoln_fio']));
        $tpl->setVariable("FILTER_ZAKAZ_OPTIONS", kontr_plugin::getClientList(intval($_REQUEST["filter_zakaz_id"])));
        $tpl->setVariable("FILTER_PTYPE_OPTIONS", project_types_plugin::getOptions(intval($_REQUEST["filter_ptype_id"])));

        $tpl->setVariable("CONTENTS", $_REQUEST["task_contents"]);
        $tpl->setVariable("FILTER_PC_DATE_FROM", $_REQUEST["filter_pc_date_from"]);
        $tpl->setVariable("FILTER_PC_DATE_TO", $_REQUEST["filter_pc_date_to"]);
        $tpl->setVariable("FILTER_PD_DATE_FROM", $_REQUEST["filter_pd_date_from"]);

        $tpl->setVariable("FILTER_PD_DATE_TO", $_REQUEST["filter_pd_date_to"]);
        $tpl->setVariable('FILTER_PRIORITY', array2options($priority, $_REQUEST['filter_priority']));
        $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
        $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));

        $tpl->setVariable('FILTER_ROWS_PER_PAGE', array2options($rpp, $_REQUEST['filter_rpp']));

        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];
        $f_ptype_id = $q->getInt('filter_ptype_id');
        $f_zakaz_id = $q->getInt('filter_zakaz_id');

        $filter = "ts.plugin_uid = " . $DB->F($this->getUID()) . " AND tsu.user_id=" . $DB->F($USER->getId());
        $filter .= $_REQUEST["filter_ptype_id"] > 0 ? " AND p.type=" . $DB->F($_REQUEST["filter_ptype_id"]) . " " : "";
        $filter .= $_REQUEST["filter_zakaz_id"] > 0 ? " AND p.zakazchik=" . $DB->F($_REQUEST["filter_zakaz_id"]) . " " : "";
        $filter .= $_REQUEST["filter_priority"] > 0 ? " AND p.priority=" . $DB->F($_REQUEST["filter_priority"]) . " " : "";

        if ($_REQUEST["filter_pc_date_from"]) {
            $_REQUEST["filter_pc_date_from"] = date("Y-m-d", strtotime($_REQUEST["filter_pc_date_from"]));
            $filter .= " AND DATE_FORMAT(ts.date_reg, '%d.%m.%Y')>=" . $DB->F($_REQUEST["filter_pc_date_from"]);
        }

        if ($_REQUEST["filter_pc_date_to"]) {
            $_REQUEST["filter_pc_date_to"] = date("Y-m-d", strtotime($_REQUEST["filter_pc_date_to"]));
            $filter .= " AND DATE_FORMAT(ts.date_reg, '%d.%m.%Y')<=" . $DB->F($_REQUEST["filter_pc_date_to"]);
        }

        if ($_REQUEST["filter_pd_date_from"]) {
            $_REQUEST["filter_pd_date_from"] = date("Y-m-d", strtotime($_REQUEST["filter_pd_date_from"]));
            $filter .= " AND DATE_FORMAT(p.deadline, '%Y-%m-%d')>=" . $DB->F($_REQUEST["filter_pd_date_from"]);
        }

        if ($_REQUEST["filter_pd_date_to"]) {
            $_REQUEST["filter_pd_date_to"] = date("Y-m-d", strtotime($_REQUEST["filter_pd_date_to"]));
            $filter .= " AND DATE_FORMAT(p.deadline, '%Y-%m-%d')<=" . $DB->F($_REQUEST["filter_pd_date_to"]);
        }

        if ($_REQUEST['filter_id']) $filter .= " AND ts.id=" . $DB->F($_REQUEST['filter_id']);

        if ($_REQUEST['task_contents']) $filter .= " AND (ts.task_title LIKE " . $DB->F("%" . $_REQUEST['task_contents'] . "%") . "OR ts.task_comment LIKE " . $DB->F("%" . $_REQUEST['task_contents'] . "%") . ")";

        if (count($_REQUEST['filter_status_id']) > 0) {
            if (in_array(1000, $_REQUEST['filter_status_id'])) {
                $filter .= " AND (st.is_active OR ts.status_id=29) ";
            } else {
                $filter .= " AND ts.status_id in (" . implode(',', array_map($DB->F, $_REQUEST['filter_status_id'])) . ')';
            }

        }

        $statusList = adm_statuses_plugin::getFullList("projects");
        if ($statusList) {
            $tpl->setCurrentBlock("sl-items");
            $statusList[] = ["id" => "1000", "name" => "Активные", "color" => "#99cc33", "plugin_uid" => "projects", "tag" => "prall"];
            foreach ($statusList as $st_item) {
                $tpl->setCurrentBlock("statuslist");
                if (in_array($st_item["id"], $_REQUEST['filter_status_id'])) {
                    $tpl->setVariable("FF_SELECTED", 'ffstisel');
                }
                $tpl->setVariable("FF_STATUS_ID", $st_item["id"]);
                $tpl->setVariable("FF_STATUS", $st_item["name"]);
                $tpl->setVariable("FF_STATUS_COLOR", $st_item["color"]);
                $tpl->parse("statuslist");
            }
            if (count($_REQUEST['filter_status_id']) === 0)
                $tpl->setVariable("FF_ALL_SELECTED", "ffstisel");
            $tpl->parse("sl-items");

        }

        if ($_REQUEST['filter_author_ids'] && ($ids = array_map('intval', explode(',', $_REQUEST['filter_author_ids']))) && sizeof($ids)) $filter .= " AND ts.author_id IN (" . implode(',', $ids) . ")";
        if ($_REQUEST['filter_ispoln_ids'] && ($ids = array_map('intval', explode(',', $_REQUEST['filter_ispoln_ids']))) && sizeof($ids)) $filter .= " AND tsu2.user_id IN (" . implode(',', $ids) . ") AND tsu.ispoln";

        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    DISTINCT(ts.id) AS TASK_ID, 
					ts.task_title AS TASK_TITLE,
					ts.task_comment AS TASK_COMMENT,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,                     
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR, 
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    ts.lastcmm_user_id AS lastuser,
					us.fio AS AUTHOR_FIO,
                    (SELECT `title` FROM `project_types` WHERE `id`=p.type) AS ptype,
                    (SELECT `contr_title` FROM `list_contr` WHERE `id`=p.zakazchik) AS zakazchik,
                    (SELECT `id` FROM `list_contr` WHERE `id`=p.zakazchik) AS zakazchik_id,
                    DATE_FORMAT(p.deadline, '%d.%m.%Y') as deadline, 
                    p.`delayed` as delay1,          
                    p.priority as PRIORITY,    
                    (SELECT CASE WHEN (p.deadline!='0000-00-00 00:00:00' AND p.deadline<=" . $DB->F(date("Y-m-d") . " 00:00:00") . ") THEN 1 ELSE -1 END) AS o1,
                    (SELECT CASE WHEN (delay1!='0000-00-00' AND delay1<=" . $DB->F(date("Y-m-d")) . ") THEN 1 ELSE -1 END) AS o2
                FROM `task_users` AS tsu
                JOIN `tasks` AS ts ON tsu.task_id=ts.id
                join users as u on u.id = tsu.user_id and u.active = 1
                LEFT JOIN `projects` AS p ON p.task_id=ts.id
                LEFT JOIN `task_status` AS st ON ts.status_id=st.id
				LEFT JOIN `users` AS us ON ts.author_id=us.id
				LEFT JOIN `task_users` AS tsu2 ON tsu2.task_id=ts.id
                WHERE $filter  
                ORDER BY $order
                LIMIT {$_REQUEST['filter_start']},{$_REQUEST["filter_rpp"]};";

        $DB->query($sql);
        $this->getLogger('debug')->debug($sql);
        if ($DB->errno())
            UIError($DB->error() . " " . $sql);
        $total = $DB->getFoundRows();
        while ($a = $DB->fetch(true)) {
            $lastuser = adm_users_plugin::getUser($a["lastuser"]);
            $a["LASTUSER"] = !empty($lastuser["fio"]) ? $lastuser['fio'] : 'СИСТЕМА';
            $a['HREF'] = $this->getLink('viewtask', "task_id={$a['TASK_ID']}");
            $a['ISPOLN_FIO'] = Task::getTaskIspoln1($a['TASK_ID']);
            $a["ZAKAZ_NAME"] = $a["zakazchik"] ? $a["zakazchik"] : "&mdash;";
            $a["PTYPE"] = $a["ptype"] ? $a["ptype"] : "&mdash;";
            $task = new Task($a['TASK_ID']);

            $a["LASTCOMMENTVALUE"] = strip_tags($task->getLastComment(true));

            $description = new \classes\comments\TextObject($a['TASK_COMMENT']);
            $a["TASK_COMMENT1"] = strip_tags($description->getText());

            $title = new \classes\comments\TextObject($task->getTitle());
            $truncDecorator = new \classes\comments\TextTruncateDecorator($title);
            $truncDecorator->length = 150;
            $truncDecorator->afterCut = '&hellip;';
            $truncDecorator->process();
            $a["TASK_TITLE"] = $title->getText();
            $title->resetText();
            if ($title != $description) {
                $truncDecorator = new \classes\comments\TextTruncateDecorator($description);
                $truncDecorator->length = 250;
                $truncDecorator->afterCut = '&hellip;';
                $truncDecorator->process();
                $a["TASK_DESCRIPTION"] = $description->getText();
            }


            $status = $task->getStatusArray();
            if (strpos($status['tag'], 'otlozh') !== false) {

                $dd = $DB->getField("SELECT DATE_FORMAT(`delayed`, '%d.%m.%Y') FROM `projects` WHERE `task_id`=" . $DB->F($task->getId()) . ";");
                if ($dd) {
                    $a["STATUS_NAME"] = $task->getStatusName() . "<br/ > до " . $dd;
                    if ($a["delay1"] <= date('Y-m-d')) {
                        $a["STATUS_COLOR"] = "#FFFCA3";
                    }
                }
            }
            $a["DEADLINE"] = ($a["deadline"] && $a["deadline"] != "01.01.1970" && $a["deadline"] != "00.00.0000") ? $a["deadline"] : false;
            if ($task->getStatusId() != 30) {
                if (strtotime($a["DEADLINE"]) && strtotime($a["DEADLINE"]) < strtotime(date("d.m.Y"))) {
                    $tpl->setVariable("OUTDATED", "outdated");
                }
            }
            $tpl->setCurrentBlock('row');
            $tpl->setVariable($a);
            $tpl->parse('row');
        }
        $DB->free();

        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"], $total, "#start-%s"));

        UIHeader($tpl);
        $tpl->show();
    }

    function viewtask_print()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("task.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/task_print.tmpl.htm");
        else
            $tpl->loadTemplatefile("task_print.tmpl.htm");
        if (!$_REQUEST["task_id"]) UIError("Не указан идентификатор проекта.");
        $err = [];
        $task = new Task($_REQUEST['task_id']);
        if (!$task->getId()) UIError("Проект с таким номером не найден! Возможно, он был удален.");
        $task_id = $task->getId();
        if (!$task_id) $err[] = "Нет такой задачи";
        elseif ($task->getPluginUid() != $this->getUID()) $err[] = "Задача $task_id не является проектом";
        elseif (!$task->isAccessable()) $err[] = "Вас нет в списке связанных лиц, обратитесь к автору проекта";
        if (sizeof($err)) UIError($err);
        $p_name = $DB->getField("SELECT title FROM project_types WHERE `id` IN (SELECT type FROM projects WHERE task_id=" . $DB->F($task_id) . ")");
        list($type, $deadline, $zakaz, $remind, $remdate, $priority) = $DB->getRow("SELECT `type`, DATE_FORMAT(`deadline`, '%d.%m.%Y') AS deadline, `zakazchik`, `remind`,  DATE_FORMAT(`remdate`, '%d.%m.%Y') AS remdate, `priority` FROM projects WHERE task_id=" . $DB->F($task_id) . ";");
        $tpl->setVariable("CURRENT_DATE", date("d.m.Y"));
        $tpl->setVariable('TASK_ID', $task_id);
        $tpl->setVariable("PRIORITYVAL", $priority);

        $title = new \classes\comments\TextObject($task->getTitle());
        $a["TASK_TITLE"] = $title->getText();

        $description = new \classes\comments\TextObject($task->getComment(false));
        $context = new \classes\comments\CommentsProcessor();
        $context->purify($description);
        $a["TASK_COMMENT"] = $description->getText();


        $a = $DB->getRow("SELECT us.id, us.fio, us.email, us.phones FROM `users` AS us WHERE `id`=" . $DB->F($task->getAuthorId()), true);
        $at = "<small class='ispuser'><strong>" . $a["fio"] . "</strong><div class='userinfo user" . $a["id"] . "'>&nbsp;&nbsp;&nbsp;E-Mail: " . $a["email"] . "<br />&nbsp;&nbsp;&nbsp;Телефон: " . $a["phones"] . "</div></small>";
        $tpl->setVariable('AUTHOR_FIO', $at);
        $DB->query("SELECT us.id, us.fio, us.email, us.phones FROM `task_users` AS tu JOIN `users` AS us ON tu.user_id=us.id WHERE tu.task_id=" . $DB->F($task_id) . " AND tu.ispoln ORDER BY us.fio");
        while ($r = $DB->fetch(true)) {
            $t .= "<small class='ispuser'><strong>" . $r["fio"] . "</strong><div class='userinfo user" . $r["id"] . "'>&nbsp;&nbsp;&nbsp;E-Mail: " . $r["email"] . "<br />&nbsp;&nbsp;&nbsp;Телефон: " . $r["phones"] . "</div></small>";
        }
        $tpl->setVariable('ISPOLN_FIO', $t);
        if ($remind && $remdate && $remdate != "0000-00-00") {
            $tpl->setVariable("REMINDACTIVE", "checked='checked'");
            $tpl->setVariable("REMIND_AT", $remdate);
        }
        $users = $task->getUsersExt();
        if ($users) {
            $user_id = [];
            foreach ($users as $userParams) {
                $user_id[] = $userParams["id"];
                $tpl->setCurrentBlock("attended-people");
                $tpl->setVariable("USERS_FIO", "<small class='ispuser'><strong>" . $userParams["fio"] . "</strong>");
                //$tpl->setVariable("LASTVISIT", $userParams["lastvisit"] ? $userParams["lastvisit"] : "никогда не был");
                $at = "<div class='userinfo user" . $userParams["id"] . "'>&nbsp;&nbsp;&nbsp;E-Mail: " . $userParams["email"] . "<br />&nbsp;&nbsp;&nbsp;Телефон: " . $userParams["phones"] . "</div></small>";

                $tpl->setVariable("CONTACTS", $at);
                $tpl->parse("attended-people");
            }
            $tpl->setVariable("INVUSERS", implode(",", $user_id));
        } else {
            UIError("Отсутствуют связанные лица!");
        }
        $bills = false;
        $projects = false;
        $sql = "(SELECT jt.joined_task_id, t.plugin_uid, t.task_title, DATE_FORMAT(t.date_reg, '%d.%m.%Y %H:%i') as date_reg, t.status_id FROM `joinedtasks` AS jt LEFT JOIN `tasks` AS t ON jt.joined_task_id=t.id
                WHERE jt.task_id=" . $DB->F($task_id) . ") UNION (SELECT jt.task_id, t.plugin_uid, t.task_title, DATE_FORMAT(t.date_reg, '%d.%m.%Y %H:%i') as date_reg, t.status_id FROM `joinedtasks` AS jt LEFT JOIN `tasks` AS t ON jt.task_id=t.id
                WHERE jt.joined_task_id=" . $DB->F($task_id) . ")";
        //$sql = "SELECT jt.joined_task_id, t.plugin_uid, t.task_title, DATE_FORMAT(t.date_reg, '%d.%m.%Y %H:%i') as date_reg, t.status_id FROM `joinedtasks` AS jt LEFT JOIN `tasks` AS t ON jt.joined_task_id=t.id WHERE jt.task_id=".$DB->F($task_id).";"
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            $projects = true;
            while ($r = $DB->fetch(true)) {
                if ($r["plugin_uid"] == "projects") {
                    $lp[] = $r["joined_task_id"];
                }
                $tpl->setCurrentBlock("lpb-list");
                $tpl->setVariable("ITEMTYPE", $r["plugin_uid"] == "projects" ? "Проект" : "Счет");
                $tt = new Task($r["joined_task_id"]);
                $tpl->setVariable("LSTATE", $tt->getStatusName());
                $tpl->setVariable("ITEMLINK", "projects/viewtask?task_id=" . $r["joined_task_id"]);
                $tpl->setVariable("ITEMID", $r["joined_task_id"]);
                $tpl->setVariable("ITEMTITLE", $r["task_title"]);
                $tpl->setVariable("ITEMDATE", $r["date_reg"]);
                $tpl->parse("lpb-list");
            }
            $tpl->setVariable("LINKED_PROJECTS", implode(",", $lp));
        }
        $sql = "SELECT DATE_FORMAT(b.date_srok, '%d.%m.%Y') AS date_srok, b.plat_id, b.poluch, b.sum, b.num, jt.joined_bill_id, t.plugin_uid, t.task_title, DATE_FORMAT(t.date_reg, '%d.%m.%Y %H:%i') AS date_reg, t.status_id FROM `joinedbills` AS jt
                LEFT JOIN `tasks` AS t ON jt.joined_bill_id=t.id
                LEFT JOIN `bills` AS b ON jt.joined_bill_id=b.task_id
                WHERE jt.task_id=" . $DB->F($task_id) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            $bills = true;
            while ($r = $DB->fetch(true)) {
                if ($r["plugin_uid"] == "bills") {
                    $lb[] = $r["joined_bill_id"];
                }
                $tpl->setCurrentBlock("lpb-list");
                $tpl->setVariable("ITEMTYPE", $r["plugin_uid"] == "projects" ? "Проект" : "Счет");
                if (preg_match("/^\d+$/", $r["poluch"])) {
                    $contr = $DB->getField("SELECT `contr_title` FROM `list_contr` WHERE `id`=" . $DB->F($r["poluch"]) . ";");
                } else {
                    $contr = $r["poluch"];
                }
                $platname = $r["plat_id"] == 1 ? "ГорСвязьСервис" : "ГорСвязьМастер";
                $tpl->setVariable("ITEMTAGTITLE", "Контрагент: " . $contr . " \r\nСчет № " . $r["num"] . " \r\nСумма: " . number_format($r["sum"], 2, ".", " ") . " \r\nСрок оплаты: " . $r['date_srok'] . " \r\nПлательщик: " . $platname);
                $tpl->setVariable("ITEMID", $r["joined_bill_id"]);
                $tt = new Task($r["joined_bill_id"]);
                $tpl->setVariable("LSTATE", $tt->getStatusName());
                $tpl->setVariable("ITEMLINK", "bills/viewbill?task_id=" . $r["joined_bill_id"]);
                $tpl->setVariable("ITEMTITLE", $r["task_title"]);
                $tpl->setVariable("ITEMDATE", $r["date_reg"]);
                $tpl->parse("lpb-list");
            }
            $tpl->setVariable("LINKED_BILLS", implode(",", $lb));
        }
        if (!$bills && !$projects) {
            $tpl->touchBlock("nojoinedbillsorprojects");
        }
        $status = $task->getStatusArray();
        if (strpos($status['tag'], 'otlozh') !== false) {
            $dd = $DB->getField("SELECT DATE_FORMAT(`delayed`, '%d.%m.%Y') FROM `projects` WHERE `task_id`=" . $DB->F($task_id) . ";");
            if ($dd)
                $tpl->setVariable('STATUS_NAME', $task->getStatusName() . " до " . $dd);

        } else {
            $tpl->setVariable('STATUS_NAME', $task->getStatusName());
        }
        $tpl->setVariable('STATUS_COLOR', $task->getStatusColor());
        $tpl->setVariable('DATE_REG', $task->getDateReg("d.m.Y H:i"));
        $tpl->setVariable("DEADLINE1", $deadline && $deadline != "01.01.1970" && $deadline != "00.00.0000" ? $deadline : "");
        $tpl->setVariable("DEADLINE", $deadline && $deadline != "01.01.1970" && $deadline != "00.00.0000" ? $deadline : "&mdash;");
        $tpl->setVariable("ZAKAZCHIK", kontr_plugin::getByID(intval($zakaz)) ? kontr_plugin::getByID(intval($zakaz)) : "&mdash;");
        $tpl->setVariable("PROJECT_TYPE", project_types_plugin::getById(intval($type)) ? project_types_plugin::getById(intval($type)) : "&mdash;");
        $tpl->setVariable('TASK_COMMENTS', $task->getComments(false));
        //die($task->getComments());
        if ($USER->getId() == $task->getAuthorId()) {
            $tpl->touchBlock("newdeadline");
        }
        UIHeader($tpl);
        $tpl->show();
    }

    function viewtask(Request $request)
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("task.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/task.tmpl.htm");
        else
            $tpl->loadTemplatefile("task.tmpl.htm");
        if (!$_REQUEST["task_id"]) UIError("Не указан идентификатор проекта.");
        $err = [];
        $task = new Task($_REQUEST['task_id']);
        if (!$task->getId()) UIError("Проект с таким номером не найден! Возможно, он был удален.");
        $task_id = $task->getId();
        if (!$task_id) $err[] = "Нет такой задачи";
        elseif ($task->getPluginUid() != $this->getUID()) $err[] = "Задача $task_id не является проектом";
        elseif (!$task->isAccessable()) $err[] = "Вас нет в списке связанных лиц, обратитесь к автору проекта: <a href='mailto:" . $DB->getField("SELECT `email` FROM `users` WHERE `id`=" . $task->getAuthorId()) . "'>" . $task->getAuthorFio() . " (" . $DB->getField("SELECT `email` FROM `users` WHERE `id`=" . $task->getAuthorId()) . ")</a>";
        if (sizeof($err)) UIError($err, "Ошибка", true, false, false, true);
        $p_name = $DB->getField("SELECT title FROM project_types WHERE `id` IN (SELECT type FROM projects WHERE task_id=" . $DB->F($task_id) . ")");
        list($type, $deadline, $zakaz, $remind, $remdate, $priority) = $DB->getRow("SELECT `type`, DATE_FORMAT(`deadline`, '%d.%m.%Y') AS deadline, `zakazchik`, `remind`,  DATE_FORMAT(`remdate`, '%d.%m.%Y') AS remdate, `priority` FROM projects WHERE task_id=" . $DB->F($task_id) . ";");
        $tpl->setVariable("CURRENT_DATE", date("d.m.Y"));
        $tpl->setVariable('TASK_ID', $task_id);

        $tpl->setVariable("PRIORITYVAL", $priority);
        $tpl->setVariable('TASK_TITLE', $task->getTitle());

        $description = new \classes\comments\TextObject($task->getComment(false));
        $context = new \classes\comments\CommentsProcessor();
        $context->purify($description);
        $tpl->setVariable('TASK_COMMENT', $description->getText());

        $a = $DB->getRow("SELECT us.id, us.fio, us.email, us.phones FROM `users` AS us WHERE `id`=" . $DB->F($task->getAuthorId()), true);
        $at = "<span class='ispuser'><strong id='author_name'>" . $a["fio"] . "</strong><div class='userinfo user" . $a["id"] . "'>&nbsp;&nbsp;&nbsp;E-Mail: " . $a["email"] . "<br />&nbsp;&nbsp;&nbsp;Телефон: " . $a["phones"] . "</div></span><br />";
        $tpl->setVariable('AUTHOR_FIO', $at);
        if (!$USER->checkAccess("projects_editauthor", User::ACCESS_WRITE)) {
            $tpl->setVariable("NORIGHTS", "hidden");
        }
        $DB->query("SELECT us.id, us.fio, us.email, us.phones FROM `task_users` AS tu JOIN `users` AS us ON tu.user_id=us.id WHERE tu.task_id=" . $DB->F($task_id) . " AND tu.ispoln ORDER BY us.fio");
        $t = '';
        while ($r = $DB->fetch(true)) {
            $t .= "<span class='ispuser'><strong>" . $r["fio"] . "</strong><div class='userinfo user" . $r["id"] . "'>&nbsp;&nbsp;&nbsp;E-Mail: " . $r["email"] . "<br />&nbsp;&nbsp;&nbsp;Телефон: " . $r["phones"] . "</div></span><br />";
        }
        $tpl->setVariable('ISPOLN_FIO', $t);
        if ($remind && $remdate && $remdate != "0000-00-00") {
            $tpl->setVariable("REMINDACTIVE", "checked='checked'");
            $tpl->setVariable("REMIND_AT", $remdate);
        }

        $periodical_reminders = $DB->getRow('SELECT day, week, month FROM task_reminders WHERE user_id = ' . $DB->F($USER->getId()) . ' AND task_id = ' . $DB->F($task_id) . ' AND (`day` > 0 OR `week`>0 OR `month`>0) ', true);
        if ($periodical_reminders) {
            if ($periodical_reminders['day'])
                $tpl->setVariable('PERIODICALY_REMINDER_DAY', 'selected');
            if ($periodical_reminders['week'])
                $tpl->setVariable('PERIODICALY_REMINDER_WEEK', 'selected');
            if ($periodical_reminders['month'])
                $tpl->setVariable('PERIODICALY_REMINDER_MONTH', 'selected');
        }

        $users = $task->getUsersExt();
        if ($users) {
            $user_id = [];
            foreach ($users as $userParams) {
                $user_id[] = $userParams["id"];
                $tpl->setCurrentBlock("attended-people");
                $tpl->setVariable("USERS_FIO", "<span class='ispuser'><strong>" . $userParams["fio"] . "</strong>");
                $tpl->setVariable("LASTVISIT", $userParams["lastvisit"] ? $userParams["lastvisit"] : "никогда не был");
                $at = "<div class='userinfo user" . $userParams["id"] . "'>&nbsp;&nbsp;&nbsp;E-Mail: " . $userParams["email"] . "<br />&nbsp;&nbsp;&nbsp;Телефон: " . $userParams["phones"] . "</div></span>";

                $tpl->setVariable("CONTACTS", $at);
                $tpl->parse("attended-people");
            }
            $tpl->setVariable("INVUSERS", implode(",", $user_id));
        } else {
            UIError("Отсутствуют связанные лица!");
        }
        $bills = false;
        $projects = false;
        $sql = "(SELECT jt.joined_task_id, t.plugin_uid, t.task_title, DATE_FORMAT(t.date_reg, '%d.%m.%Y %H:%i') as date_reg, t.status_id FROM `joinedtasks` AS jt LEFT JOIN `tasks` AS t ON jt.joined_task_id=t.id 
                WHERE jt.task_id=" . $DB->F($task_id) . ") UNION (SELECT jt.task_id, t.plugin_uid, t.task_title, DATE_FORMAT(t.date_reg, '%d.%m.%Y %H:%i') as date_reg, t.status_id FROM `joinedtasks` AS jt LEFT JOIN `tasks` AS t ON jt.task_id=t.id
                WHERE jt.joined_task_id=" . $DB->F($task_id) . ")";
        //$sql = "SELECT jt.joined_task_id, t.plugin_uid, t.task_title, DATE_FORMAT(t.date_reg, '%d.%m.%Y %H:%i') as date_reg, t.status_id FROM `joinedtasks` AS jt LEFT JOIN `tasks` AS t ON jt.joined_task_id=t.id WHERE jt.task_id=".$DB->F($task_id).";"
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            $projects = true;
            while ($r = $DB->fetch(true)) {
                if ($r["plugin_uid"] == "projects") {
                    $lp[] = $r["joined_task_id"];
                }
                $tpl->setCurrentBlock("lpb-list");
                $tpl->setVariable("ITEMTYPE", $r["plugin_uid"] == "projects" ? "Проект" : "Счет");
                $tt = new Task($r["joined_task_id"]);
                $tpl->setVariable("LSTATE", $tt->getStatusName());
                $tpl->setVariable("ITEMLINK", "projects/viewtask?task_id=" . $r["joined_task_id"]);
                $tpl->setVariable("ITEMID", $r["joined_task_id"]);
                $tpl->setVariable("ITEMTITLE", $r["task_title"]);
                $tpl->setVariable("ITEMDATE", $r["date_reg"]);
                $tpl->parse("lpb-list");
            }
            $tpl->setVariable("LINKED_PROJECTS", implode(",", (array)$lp));
        }
        $sql = "SELECT DATE_FORMAT(b.date_srok, '%d.%m.%Y') AS date_srok, b.plat_id, b.poluch, b.sum, b.num, jt.joined_bill_id, t.plugin_uid, t.task_title, DATE_FORMAT(t.date_reg, '%d.%m.%Y %H:%i') AS date_reg, t.status_id FROM `joinedbills` AS jt
                LEFT JOIN `tasks` AS t ON jt.joined_bill_id=t.id 
                LEFT JOIN `bills` AS b ON jt.joined_bill_id=b.task_id 
                WHERE jt.task_id=" . $DB->F($task_id) . ";";
        $DB->query($sql);
        if ($DB->errno())
            UIError($DB->error());
        if ($DB->num_rows()) {
            $bills = true;
            while ($r = $DB->fetch(true)) {
                if ($r["plugin_uid"] == "bills") {
                    $lb[] = $r["joined_bill_id"];
                }
                $tpl->setCurrentBlock("lpb-list");
                $tpl->setVariable("ITEMTYPE", $r["plugin_uid"] == "projects" ? "Проект" : "Счет");
                if (preg_match("/^\d+$/", $r["poluch"])) {
                    $contr = $DB->getField("SELECT `contr_title` FROM `list_contr` WHERE `id`=" . $DB->F($r["poluch"]) . ";");
                } else {
                    $contr = $r["poluch"];
                }
                $platname = $r["plat_id"] == 1 ? "ГорСвязьСервис" : "ГорСвязьМастер";
                $tpl->setVariable("ITEMTAGTITLE", "Контрагент: " . $contr . " \r\nСчет № " . $r["num"] . " \r\nСумма: " . number_format($r["sum"], 2, ".", " ") . " \r\nСрок оплаты: " . $r['date_srok'] . " \r\nПлательщик: " . $platname);
                $tpl->setVariable("ITEMID", $r["joined_bill_id"]);
                $tt = new Task($r["joined_bill_id"]);
                $tpl->setVariable("LSTATE", $tt->getStatusName());
                $tpl->setVariable("ITEMLINK", "bills/viewbill?task_id=" . $r["joined_bill_id"]);
                $tpl->setVariable("ITEMTITLE", $r["task_title"]);
                $tpl->setVariable("ITEMDATE", $r["date_reg"]);
                $tpl->parse("lpb-list");
            }
            $tpl->setVariable("LINKED_BILLS", implode(",", $lb));
        }
        if (!$bills && !$projects) {
            $tpl->touchBlock("nojoinedbillsorprojects");
        }
        $status = $task->getStatusArray();
        if (strpos($status['tag'], 'otlozh') !== false) {
            $dd = $DB->getField("SELECT DATE_FORMAT(`delayed`, '%d.%m.%Y') FROM `projects` WHERE `task_id`=" . $DB->F($task_id) . ";");
            if ($dd)
                $tpl->setVariable('STATUS_NAME', $task->getStatusName() . " до " . $dd);

        } else {
            $tpl->setVariable('STATUS_NAME', $task->getStatusName());
        }
        $tpl->setVariable('STATUS_COLOR', $task->getStatusColor());
        $tpl->setVariable('DATE_REG', $task->getDateReg("d.m.Y H:i"));
        $tpl->setVariable("DEADLINE1", $deadline && $deadline != "01.01.1970" && $deadline != "00.00.0000" ? $deadline : "");
        $tpl->setVariable("DEADLINE", $deadline && $deadline != "01.01.1970" && $deadline != "00.00.0000" ? $deadline : "&mdash;");
        $tpl->setVariable("ZAKAZCHIK", kontr_plugin::getByID(intval($zakaz)) ? kontr_plugin::getByID(intval($zakaz)) : "&mdash;");
        $tpl->setVariable("PROJECT_TYPE", project_types_plugin::getById(intval($type)) ? project_types_plugin::getById(intval($type)) : "&mdash;");
        $tpl->setVariable('TASK_COMMENTS', $task->getComments());
        //die($task->getComments());
        if ($USER->getId() == $task->getAuthorId()) {
            $tpl->touchBlock("newdeadline");
        }
        UIHeader($tpl);
        $tpl->show();
    }

    public function reminderChangeAjax()
    {
        global $DB, $USER;

        $task = new Task($_POST['task']);
        $value = $_POST["val"] ? $_POST["val"] : null;
        switch ($value) {
            case 1: //day
                $sql_part = '`day` = 1, `week` = 0, `month` = 0 ';
                break;
            case 2: //week
                $sql_part = '`day` = 0, `week` = 1, `month` = 0 ';
                break;
            case 3: //month
                $sql_part = '`day` = 0, `week` = 0, `month` = 1';
                break;
            default:
                $sql_part = 'day = 0, week = 0, month = 0';
        }

        $sql = 'SELECT count(task_id) FROM task_reminders WHERE `user_id` = ' . $DB->F($USER->getId()) . ' AND `task_id` = ' . $DB->F($task->getId()) . ';';
        $data = $DB->getField($sql);
        if ($data > 0) {

            $sql = '
                UPDATE task_reminders SET ' . $sql_part . ' WHERE
                user_id = ' . $DB->F($USER->getId()) . ' AND task_id = ' . $DB->F($task->getId()) . ';
            ';
        } else {
            $sql = '
                INSERT INTO task_reminders SET `task_id`=' . $DB->F($task->getId()) . ', `user_id`=' . $DB->F($USER->getId()) . ', ' . $sql_part . ';';
        }
        $DB->query($sql);
        $result = ['status' => 200, 'data' => ''];
        if ($DB->errno()) {
            $result = ['status' => 500, 'data' => ''];
        }
        $DB->free();

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    function editask()
    {
        global $DB, $USER;
        $prioritywoall = [
            1 => "1",
            2 => "2",
            3 => "3",
            4 => "4",
            5 => "5",
        ];
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("editor.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/editor.tmpl.htm");
        else
            $tpl->loadTemplatefile("editor.tmpl.htm");
        $err = [];
        $task = new Task($_REQUEST['task_id']);
        $tpl->setVariable("ZAKAZLIST", kontr_plugin::getClientList());
        $tpl->setVariable("PTLIST", project_types_plugin::getOptions());
        $tpl->setVariable("CURRENT_DATE", date("d.m.Y"));
        if ($task_id = $task->getId()) {
            if ($task->getPluginUid() != $this->getUID()) $err[] = "Задача $task_id не является проектом";
            if ($task->getAuthorId() != $USER->getId() && !$USER->checkAccess("projects_editall", User::ACCESS_WRITE)) $err[] = "Только автор может отредактировать проект";
            if (sizeof($err)) UIError($err);
            list($type, $deadline, $zakaz, $priority) = $DB->getRow("SELECT `type`, DATE_FORMAT(`deadline`, '%d.%m.%Y') AS deadline, `zakazchik`, `priority` FROM projects WHERE task_id=" . $DB->F($task_id) . ";");
            $tpl->setVariable('TASK_ID', $task_id);
            $tpl->setVariable('ID_TEXT', $task_id);
            $tpl->setVariable('TASK_TITLE', $task->getTitle());
            $tpl->setVariable('TASK_COMMENT', $task->getComment(false));
            $tpl->setVariable('ISPOLN_FIO', $task->getIspoln('<br>'));
            $tpl->setVariable('ISPOLN_IDS', implode(',', array_keys($task->getIspoln())));
            $tpl->setVariable('USERS_FIO', $task->getUsers('<br>'));
            $tpl->setVariable('USERS_IDS', implode(',', array_keys($task->getUsers())));
            if (!$USER->checkAccess("projects_editauthor", User::ACCESS_WRITE)) {
                $tpl->setVariable("NORIGHTS", "hidden");
            }
            $tpl->setVariable("AUTHORUSER_FIO", $task->getAuthorFio());
            $tpl->setVariable("ZAKAZLIST", kontr_plugin::getClientList(intval($zakaz)));
            $tpl->setVariable("PTLIST", project_types_plugin::getOptions(intval($type)));
            if ($USER->checkAccess("projects_edit_priorities", User::ACCESS_WRITE)) {
                $tpl->setCurrentBlock("editpriority");
                $tpl->setVariable("PRIORITIES_LIST", array2options($prioritywoall, $priority));
                $tpl->parse("editpriority");
            } else {
                $tpl->setVariable("PRIORITYVAL", "<strong>" . $priority . "</strong>");
            }
            $tpl->setVariable("DEADLINE", ($deadline && $deadline != "01.01.1970" && $deadline != "00.00.0000") ? $deadline : "");
        } else {
            $tpl->setVariable('ID_TEXT', 'новый');
            $tpl->setVariable("NORIGHTS", "hidden");
            $tpl->setVariable("NORIGHTS1", "hidden");

            $tpl->touchBlock('files');
        }

        UIHeader($tpl);
        $tpl->show();
    }

    function savetask(Request $request)
    {
        //Debugger::dump($_POST, true);
        /*
        [task_id] =>
        [title] =>
        [comment] =>
        [ispoln_ids] =>
        [user_ids] =>
        */

        global $USER, $DB;

        $err = [];
        $task = new Task($_POST['task_id']);

        if (!$_POST['title'] && !$_POST['comment']) $err[] = "Не указан заголовок и содержание";
        if (!$_POST['ispoln_ids']) $_POST['ispoln_ids'] = $USER->getId();

        if (sizeof($err)) UIError($err);
        if ($_POST["deadline"]) {
            $_POST["deadline"] = date("Y-m-d", strtotime($_POST["deadline"]));
            if (strtotime($_POST["deadline"]) < strtotime(date("d.m.Y"))) {
                UIError("Дата выполнения не может быть меньше текущей.");
            }
        }
        $deadline = $_POST["deadline"] ? $_POST["deadline"] : "";
        if ($task_id = $task->getId()) {
            if ($task->getPluginUid() != $this->getUID()) $err[] = "Задача $task_id не является проектом";
            if ($task->getAuthorId() != $USER->getId() && !$USER->checkAccess("projects_editall", User::ACCESS_WRITE)) $err[] = "Только автор может отредактировать проект";
            if (sizeof($err)) UIError($err);

            $task->setTitle($_POST['title']);
            $task->setComment($_POST['comment']);
            $task->setExecutors(explode(',', $_POST["ispoln_ids"]));
            $task->setBoundedUsers(explode(',', $_POST['user_ids'] . ',' . $task->getAuthorId()));
            $deadline = $_POST["deadline"] ? $_POST["deadline"] : "";
            $c = $DB->getField("SELECT count(task_id) FROM projects WHERE `task_id`=" . $DB->F($task_id) . ";");
            if ($c)
                $sql = "UPDATE projects SET `priority`=" . $DB->F($_REQUEST["priority"]) . ", `type`=" . $DB->F(($_POST["project_type"] > 0 ? $_POST["project_type"] : null), true) . ", `deadline`=" . $DB->F($deadline, true) . ", `zakazchik`=" . $DB->F(($_POST["zakazchik_id"] > 0 ? $_POST["zakazchik_id"] : null), true) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            else
                $sql = "INSERT INTO projects (task_id, type, deadline, zakazchik) VALUES (" . $DB->F($task_id) . ", " . $DB->F(($_POST["project_type"] > 0 ? $_POST["project_type"] : null), true) . ", " . $DB->F($deadline, true) . ", " . $DB->F(($_POST["zakazchik_id"] > 0 ? $_POST["zakazchik_id"] : null), true) . ");";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());

        } else {
            $task_id = Task::createTask($this->getUID(), $_POST['title'], $_POST['comment'], 1, 0, $_POST['ispoln_ids'], $_POST['user_ids']);

            $task = new Task($task_id);
            $task->toMail();
            $sql = "INSERT INTO projects (task_id, type, deadline, zakazchik) VALUES (" . $DB->F($task_id) . ", " . $DB->F(($_POST["project_type"] > 0 ? $_POST["project_type"] : null), true) . ", " . $DB->F($deadline, true) . ", " . $DB->F(($_POST["zakazchik_id"] > 0 ? $_POST["zakazchik_id"] : null), true) . ");";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());

            foreach ($_FILES['userfile']['error'] as $i => $error) {
                if ($error == UPLOAD_ERR_OK) {
                    if ($task->addFile($_FILES['userfile']['tmp_name'][$i], $_FILES['userfile']['name'][$i], $_FILES['userfile']['type'][$i])) {
                        /* all ok */
                    } else $err[] = "Не удалось добавить файл '{$_FILES['userfile']['name'][$i]}' к задаче";
                } elseif ($error != UPLOAD_ERR_NO_FILE) {
                    $err[] = "Не удалось загрузить файл '{$_FILES['userfile']['name'][$i]}' на сервер";
                }
            }
            if (sizeof($err)) {
                UIError($err, "Не удалось обработать все файлы", true, "Продолжить", $this->getLink('viewtask', "task_id=$task_id"));
            }
        }

        redirect($this->getLink('viewtask', "task_id=$task_id"));
    }

    static function statusCallbackGet($task_id, $status)
    {
        global $DB, $USER;

        $err = [];
        $plugin_uid = basename(__FILE__, '.php');
        if ((false !== strpos($status['tag'], 'otlozh'))) {
            $r = $DB->getField("SELECT DATE_FORMAT(`delayed`, '%d.%m.%Y') AS dd FROM `projects` WHERE `task_id`=" . $DB->F($task_id) . ";");

            return $status['name'] . "</label> до <input type='text' name='otlozh_date' class='date_input' values='" . $r . "'> <label>";
        }

        $sql = "SELECT `author_id` FROM `tasks` WHERE `id`=" . $DB->F($task_id);
        if ((false !== strpos($status['tag'], 'author')) && ($USER->getId() != $DB->getField($sql))) {
            if (!$USER->checkAccess("projects_close", User::ACCESS_WRITE))
                return false;
        }

        return $status['name'];
        //return sizeof($err) ? $err : $status['name'];
    }

    static function statusCallbackSet($task_id, &$status, &$text, &$tag)
    {
        global $DB;

        $err = [];
        $plugin_uid = basename(__FILE__, '.php');
        if ((false !== strpos($status['tag'], 'otlozh'))) {
            $_REQUEST["otlozh_date"] = $_REQUEST['otlozh_date'] ? date("Y-m-d", strtotime($_REQUEST["otlozh_date"])) : false;
            if ($_REQUEST['otlozh_date']) {
                $r = $DB->getField("SELECT count(task_id) FROM `projects` WHERE `task_id`=" . $DB->F($task_id) . ";");
                if ($r > 0)
                    $DB->query("UPDATE `projects` SET `delayed`=" . $DB->F($_REQUEST["otlozh_date"]) . " WHERE `task_id`=" . $DB->F($task_id) . ";");
                else
                    $DB->query("INSERT INTO `projects` (`task_id`, `type`, `zakazchik`, `delayed`) VALUES (" . $DB->F($task_id) . ", 0, 0, " . $DB->F($_REQUEST['otlozh_date']) . ");");
                if ($DB->errno()) {
                    $err[] = $DB->error();
                } else {
                    $_REQUEST["otlozh_date"] = date("d.m.Y", strtotime($_REQUEST["otlozh_date"]));
                    $tag .= " до {$_REQUEST['otlozh_date']}";
                }
            }
        }

        return sizeof($err) ? $err : true;
    }


    function setusers()
    {
        global $DB, $USER;
        $id = $_REQUEST["project_id"];
        if (!$id) {
            $ret["error"] = "Не указан проект!";
            echo json_encode($ret);

            return false;
        }
        $task = new Task($id);
        $svList = explode(",", $_REQUEST["users"]);
        $author_id = $task->getAuthorId();
        $ispList = array_keys($task->getIspoln());
        $target = $ispList;
        $target[] = $author_id;
        $old_list = [];
        $sql = "SELECT `user_id` FROM `task_users` WHERE (`task_id` = $id);";
        $result = $DB->query($sql);
        //debug_mes(mysql_errno(), mysql_error(), __FILE__, __LINE__, $sql);
        while (list($old_id) = $DB->fetch()) $old_list[] = $old_id;
        $DB->free();
        $add_list = array_diff($svList, $old_list);
        $del_list = array_diff($old_list, $svList);
        $target = array_unique(array_merge($target, $svList));
        $sql = "DELETE FROM `task_users` WHERE `task_id`=" . $DB->F($id) . " AND `user_id` NOT IN (" . preg_replace("/,$/", "", implode(",", $target)) . ") AND !ispoln;";
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = $DB->error() . " " . $sql;
            echo json_encode($ret);

            return false;
        }

        foreach ($svList as $item) {
            $sql_add[] = "(" . $DB->F($id) . ", " . $DB->F($item) . ", 0)";
        }
        if (count($sql_add)) {
            $sql = "INSERT IGNORE INTO `task_users` (`task_id`, `user_id`, `ispoln`) VALUES " . implode(", ", $sql_add);
            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
                echo json_encode($ret);

                return false;
            }
        }
        $sql = "SELECT `fio` FROM users WHERE `id` IN (" . implode(",", $target) . ");";
        $newlist = $DB->getCell($sql);

        $sql_list = [];
        foreach ($add_list as $a) if ($a) $sql_list[] = " `id`=$a ";
        foreach ($del_list as $d) if ($d) $sql_list[] = " `id`=$d ";

        $cmm = false;
        $sql = "SELECT `id`, `fio` FROM `users` WHERE (" . implode("OR", $sql_list) . ") ORDER BY `fio`;";

        $result = $DB->query($sql);
        while (list($id, $fio) = $DB->fetch()) {
            if (in_array($id, $add_list) && $id != $author_id && !in_array($id, $ispList)) {
                $cmma .= "Добавлен: " . $fio . "\r\n";
            }
            if (in_array($id, $del_list) && $id != $author_id && !in_array($id, $ispList)) {
                $cmmd .= "Удален: " . $fio . "\r\n";
            }
        }
        $cmm = $cmma . $cmmd;

        $DB->free();
        if (count($newlist))
            $task->addComment($cmm, "Новый список связанных лиц.\r\n" . $cmm);

        $ret["ok"] = "ok";
        echo json_encode($ret);

        return false;
    }

    function joinprojtoproject()
    {
        global $DB, $USER;
        $id = $_REQUEST["project_id"];
        if (!$id) {
            $ret["error"] = "Не указан проект!";
            echo json_encode($ret);

            return false;
        }
        $task = new Task($id);
        $sql = "DELETE FROM joinedtasks WHERE `task_id`=" . $DB->F($id) . ";";
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);

            return false;
        }
        if ($_REQUEST["projects"]) {
            $np = explode(",", $_REQUEST["projects"]);
            foreach ($np as $item) {
                if ($id != $item)
                    $sql_add[] = "(" . $DB->F($id) . ", " . $DB->F($item) . ")";
            }
            if (count($sql_add)) {
                $sql = "INSERT IGNORE INTO `joinedtasks` (`task_id`, `joined_task_id`) VALUES " . implode(", ", $sql_add);
                $DB->query($sql);
                if ($DB->errno()) {
                    $ret["error"] = $DB->error();
                    echo json_encode($ret);

                    return false;
                }

            }
        }
        if ($_REQUEST["projects"] && count($sql_add)) {
            foreach ($np as $item) {
                $t[] = $item;
            }
            $task->addComment("", "Новый список: " . implode(", ", $t));
        } else {
            $task->addComment("", "Новый список: отсутствует");
        }
        $ret["ok"] = "ok";
        echo json_encode($ret);

        return false;


    }

    function joinbilltoproject()
    {
        global $DB, $USER;
        $id = $_REQUEST["project_id"];
        if (!$id) {
            $ret["error"] = "Не указан проект!";
            echo json_encode($ret);

            return false;
        }
        $task = new Task($id);
        $sql = "DELETE FROM joinedbills WHERE `task_id`=" . $DB->F($id) . ";";
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);

            return false;
        }
        if ($_REQUEST["bills"]) {
            $np = explode(",", $_REQUEST["bills"]);
            foreach ($np as $item) {
                if ($id != $item)
                    $sql_add[] = "(" . $DB->F($id) . ", " . $DB->F($item) . ")";
            }
            if (count($sql_add)) {
                $sql = "INSERT IGNORE INTO `joinedbills` (`task_id`, `joined_bill_id`) VALUES " . implode(", ", $sql_add);
                $DB->query($sql);
                if ($DB->errno()) {
                    $ret["error"] = $DB->error();
                    echo json_encode($ret);

                    return false;
                }

            }
        }
        if ($_REQUEST["bills"] && count($sql_add)) {
            foreach ($np as $item) {
                $t[] = $item;
            }
            $task->addComment("", "Новый список связанных счетов: " . implode(", ", $t));
        } else {
            $task->addComment("", "Новый список связанных счетов: отсутствует");
        }
        $ret["ok"] = "ok";
        echo json_encode($ret);

        return false;

    }

    function setactiveremind_ajax()
    {
        global $DB, $USER;
        $id = $_REQUEST["project_id"];
        if (!$id) {
            $ret["error"] = "Не указан проект!";
            echo json_encode($ret);

            return false;
        }
        $task = new Task($id);
        if ($_REQUEST["remind_at"] == "") {
            $nd = false;
        } else {
            $nd = date("Y-m-d", strtotime($_REQUEST["remind_at"]));
            if ($nd <= date("Y-m-d")) $nd = date("Y-m-d", strtotime(date("Y-m-d") . " + 1 day"));
        }
        if ($nd) {
            $sql = "UPDATE `projects` SET `remind`=1, remdate=" . $DB->F($nd) . " WHERE `task_id`=" . $DB->F($id) . ";";
            $ret["msg"] = "Напоминание о проекте включено!";
        } else {
            $sql = "UPDATE `projects` SET `remind`=0, remdate=NULL WHERE `task_id`=" . $DB->F($id) . ";";
            $ret["msg"] = "Напоминание о проекте выключено!";
        }
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);

            return false;

        }
        $ret["ok"] = "ok";
        echo json_encode($ret);

        return false;
    }

    function setnewdeadline_ajax()
    {
        global $DB, $USER;
        $id = $_REQUEST["project_id"];
        $deadline = $_REQUEST['deadline'];
        if ($deadline != "") {
            $deadline = date("Y-m-d", strtotime($deadline));
            if (strtotime($deadline) < strtotime(date("d.m.Y"))) {
                $ret["error"] = "Дата выполнения не может быть меньше текущей.";
                echo json_encode($ret);

                return false;
            }
        } else {
            if (!$_REQUEST["reset"]) {
                $ret["error"] = "Дата выполнения не может иметь пустое значение.";
                echo json_encode($ret);

                return false;
            }
        }
        if (!$id) {
            $ret["error"] = "Не указан проект!";
            echo json_encode($ret);

            return false;
        }
        $task = new Task($id);
        if ($_REQUEST["reset"]) $deadline = "NULL"; else $deadline = $DB->F($deadline);
        $sql = "UPDATE `projects` SET `deadline`=" . $deadline . " WHERE `task_id`=" . $DB->F($id) . ";";
        if ($_REQUEST["reset"])
            $ret["msg"] = "Срок выполнения сброшен.";
        else
            $ret["msg"] = "Установлен новый срок выполнения - " . $_REQUEST['deadline'];
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);

            return false;
        }
        $task->addComment("", "Срок выполнения " . $ret["msg"]);

        $ret["ok"] = "ok";
        echo json_encode($ret);

        return false;

    }

    function getcomment_ajax()
    {
        global $DB, $USER;
        $id = $_REQUEST["comment_id"];
        $comment = $_REQUEST['comment'];
        if (!$id) {
            $ret["error"] = "Не указан комментарий!";
            echo json_encode($ret);

            return false;
        }
        $sql = "SELECT `text` FROM `task_comments` WHERE `id`=" . $DB->F($id) . ";";
        $cmm = $DB->getField($sql);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);

            return false;
        }
        $ret["cmm"] = htmlspecialchars_decode($cmm);
        $ret["ok"] = "ok";
        echo json_encode($ret);

        return false;

    }


    function editcomment_ajax()
    {
        global $DB, $USER;
        $id = $_REQUEST["comment_id"];
        $comment = new \classes\comments\TextObject($_REQUEST['comment']);
        if (!$id) {
            $ret["error"] = "Не указан комментарий!";
            echo json_encode($ret);

            return false;
        }
        $sql = "UPDATE `task_comments` SET `text`=" . $DB->F($comment->getText()) . " WHERE `id`=" . $DB->F($id) . ";";
        $ret["msg"] = "Комментарий обновлен";
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);

            return false;
        }
        $ret["ok"] = "ok";

        $strategy = new \classes\comments\CommentsProcessor();
        $strategy->purify($comment);

        $ret["cmm"] = $comment->getText();
        echo json_encode($ret);

        return false;

    }

    function setauthor_ajax()
    {
        global $USER, $DB;
        $task_id = $_REQUEST["task_id"];
        $user_id = $_REQUEST["user_id"];
        if ($USER->checkAccess("projects_editauthor", User::ACCESS_WRITE)) {
            if ($task_id && $user_id) {
                $project = new Task($task_id);
                if ($project) {
                    $sql = "UPDATE `tasks` SET `author_id`=" . $DB->F($user_id) . " WHERE `id`=" . $DB->F($task_id) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) {
                        $ret["error"] = $DB->error();
                    } else {
                        $ret["ok"] = "ok";
                        $project->addComment("", "Изменен автор проекта на " . $DB->getField("SELECT `fio` FROM `users` WHERE `id`=" . $DB->F($user_id) . ";"));
                        $ret['tpl'] = $project->getLastComment();
                    }
                } else {
                    $ret["error"] = "Внутренняя ошибка. Возможно, данная задача не является проектом!";
                }
            } else {
                $ret["error"] = "Недостаточно данных для выполнения операции!";
            }
        } else {
            $ret["error"] = "Нет прав доступа для выполнения данной операции!";
        }
        echo json_encode($ret);

        return false;
    }

}

?>
