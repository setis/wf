<?php 
/**
 * Plugin Header
 * @author kblp
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Проекты";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Проекты";
    $PLUGINS[$plugin_uid]['events']['viewtask'] = "Просмотр проекта";
    $PLUGINS[$plugin_uid]['events']['editcomment_ajax'] = "Редактирование комментария";  
    $PLUGINS[$plugin_uid]['events']['getcomment_ajax'] = "Получение комментария";  
    $PLUGINS[$plugin_uid]['events']['viewtask_print'] = "Проект";  
    $PLUGINS[$plugin_uid]['events']['reminderChangeAjax'] = "Переодическое напоминание";

}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['editask'] = "Создание/Редактирование проекта";
    $PLUGINS[$plugin_uid]['events']['savetask'] = "Сохранение проекта";  
    $PLUGINS[$plugin_uid]['events']['setusers'] = "Назначение связанных лиц проекта";  
    $PLUGINS[$plugin_uid]['events']['joinprojtoproject'] = "Присоединение проектов к проекту";  
    $PLUGINS[$plugin_uid]['events']['joinbilltoproject'] = "Присоединение счетов к проекту";  
    $PLUGINS[$plugin_uid]['events']['setactiveremind_ajax'] = "Установка напоминания о проекте";  
    $PLUGINS[$plugin_uid]['events']['setnewdeadline_ajax'] = "Установка нового строка исполнения";     
    $PLUGINS[$plugin_uid]['events']['setauthor_ajax'] = "Установка нового автора проекта";     

}

$plugin_uid = basename(__FILE__, ".h.php")."_edit_priorities";

$PLUGINS[$plugin_uid]['name'] = "Проекты: Изменение приоритетов проектов";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = basename(__FILE__, ".h.php")."_close";

$PLUGINS[$plugin_uid]['name'] = "Проекты: Закрытие проектов";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = basename(__FILE__, ".h.php")."_editall";

$PLUGINS[$plugin_uid]['name'] = "Проекты: Редактирование проектов в любых статусах";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = basename(__FILE__, ".h.php")."_editauthor";

$PLUGINS[$plugin_uid]['name'] = "Проекты: Изменение авторов проектов";
$PLUGINS[$plugin_uid]['hidden'] = true;


?>