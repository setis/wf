<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;


require_once(dirname(__FILE__)."/../connections/connections.php");

 
class reports_tmc_tech_total_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;

        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $dateFrom = @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date("d.m.Y") . " - 1 month"));
        $dateTo = @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y");    
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__TMC_TECH_TOTAL", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            //$tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            //$tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            if (strtotime($dateFrom)>strtotime($dateTo)) {
                UIError("Некорректный период!");
            }
            $tpl->setVariable("DATE_FROM", $dateFrom);
            $tpl->setVariable("DATE_TO", $dateTo);
            
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList(@$_POST["sc_id"]));
            $firstSC = $DB->getField("SELECT `id` FROM `list_sc` ORDER BY `title` LIMIT 1;");
            if (!$firstSC) UIError("Отсутствуют Сервисные Центры");
            $tpl->setVariable("FILTER_SC_TECH", adm_empl_plugin::getSCTechsAll($firstSC, $_REQUEST["tech_id"]));
            $techName = adm_empl_plugin::getTechFIO($_REQUEST["tech_id"]);
            
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"] || @$_POST["setflag"]) {
                
                $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
                if ($USER->getTemplate() != "default") 
                    $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
                else 
                    $rtpl->loadTemplatefile("report.tmpl.htm");
                if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                    $rtpl->setCurrentBlock("print_head");
                    $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                    $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                    $rtpl->parse("print_head");
                }
                $currentSC = adm_sc_plugin::getSC($_POST["sc_id"]); 
                $rtpl->setVariable("SCTITLE", $currentSC["title"]);
                $rtpl->setVariable("TECHNAME", $techName);
                $rtpl->setVariable("DATE_FROM", $dateFrom);
                $rtpl->setVariable("DATE_TO", $dateTo);
                $catList = adm_material_cats_plugin::getList();
                if (!$catList) {    
                    UIError("Отсутствуют категори товара!");
                }
                $dateFrom = substr($dateFrom, 6, 4)."-".substr($dateFrom, 3, 2). "-".substr($dateFrom, 0,2);
                $dateTo = substr($dateTo, 6, 4)."-".substr($dateTo, 3, 2). "-".substr($dateTo, 0,2);
                $rows = false;
                foreach($catList as $cat) {
                    $tmcList = adm_materials_plugin::getTMCListByCat($cat["id"]);
                    foreach($tmcList as $item) {
                        $sql = "SELECT
                            *
                        FROM
                            `tmc_sc`
                        WHERE
                            `tmc_id`=".$DB->F($item["id"])." AND
                            `sc_id`=".$DB->F($_POST["sc_id"])." AND
                            id IN (
                                    SELECT
                                        `tmc_sc_id`
                                    FROM
                                        tmc_sc_tech
                                    WHERE
                                        `tech_id`=".$DB->F($_REQUEST["tech_id"])."
                            )
                        GROUP BY `tmc_id`
                        ;";
                        $DB->query($sql);
                        if ($DB->num_rows()) {
                            $rows = true;
                            $rtpl->setCurrentBlock("rep_row");
                            $rtpl->setVariable("ROW_PERIOD", $dateFrom." - ".$dateTo);
                            $rtpl->setVariable("ROW_TMC_CAT", $cat["title"]);
                            $rtpl->setVariable("ROW_TMC_NAME", $item["title"]);
                            if ($item["metric_flag"]) {
                                $in = $DB->getField("SELECT SUM(count) FROM `tmc_sc_tech` WHERE `tmc_sc_id` IN (SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=".$DB->F($item["id"]).") AND `tech_id`=".$DB->F($_REQUEST["tech_id"])." AND `sc_id`=".$DB->F($_POST["sc_id"])." AND DATE_FORMAT(`ledit`, '%Y-%m-%d')<=".$DB->F($dateFrom)." AND incoming=1 AND count>0;");
                                $out = $DB->getField("SELECT SUM(count) FROM `tmc_sc_tech` WHERE `tmc_sc_id` IN (SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=".$DB->F($item["id"]).") AND `tech_id`=".$DB->F($_REQUEST["tech_id"])." AND `sc_id`=".$DB->F($_POST["sc_id"])." AND DATE_FORMAT(`ledit`, '%Y-%m-%d')<=".$DB->F($dateFrom)." AND (!inplace OR inplace IS NULL) AND (!incoming OR incoming IS NULL);");
                                $rtpl->setVariable("ROW_TMC_COUNT_WAS", ($in-$out) >= 0 ? $in-$out . " ".$item["metric_title"] : "<span style='margin:0; padding:0;color:#FF0000;'>".($in-$out). " ".$item["metric_title"]."</span>");
                                $incoming = $DB->getField("SELECT SUM(count) FROM `tmc_sc_tech` WHERE `tmc_sc_id` IN (SELECT id FROM tmc_sc WHERE `tmc_id`=".$DB->F($item["id"]).") AND `tech_id`=".$DB->F($_REQUEST["tech_id"])." AND `sc_id`=".$DB->F($_POST["sc_id"])." AND DATE_FORMAT(`ledit`, '%Y-%m-%d')>=".$DB->F($dateFrom)." AND DATE_FORMAT(`ledit`, '%Y-%m-%d')<=".$DB->F($dateTo)." AND incoming=1;");
                                $rtpl->setVariable("ROW_TMC_COUNT_INC", intval($incoming). " ".$item["metric_title"]);
                                $gone = $DB->getField("SELECT SUM(count) FROM `tmc_sc_tech` WHERE `tmc_sc_id` IN (SELECT id FROM tmc_sc WHERE `tmc_id`=".$DB->F($item["id"]).") AND `tech_id`=".$DB->F($_REQUEST["tech_id"])." AND `sc_id`=".$DB->F($_POST["sc_id"])." AND DATE_FORMAT(`ledit`, '%Y-%m-%d')>=".$DB->F($dateFrom)." AND DATE_FORMAT(`ledit`, '%Y-%m-%d')<=".$DB->F($dateTo)." AND (!incoming OR incoming IS NULL) AND (!inplace OR inplace IS NULL);");
                                $rtpl->setVariable("ROW_TMC_COUNT_GONE", intval($gone). " ".$item["metric_title"]);
                                $inplace = (intval($in)-intval($out)) + intval($incoming) - intval($gone);
                                $rtpl->setVariable("ROW_TMC_COUNT_INPLACE", intval($inplace) > 0 ? intval($inplace). " ".$item["metric_title"] : "<span style='margin:0; padding:0;color:#FF0000;'>".$inplace. " ".$item["metric_title"]."</span>");
                            } else {
                                //$in = $DB->getField("SELECT COUNT(id) FROM `tmc_sc` WHERE `tmc_id`=".$DB->F($item["id"])." AND `sc_id`=".$DB->F($_POST["sc_id"])." AND ledit<".$DB->F($dateFrom)." AND incoming=1;");
                                //$out = $DB->getField("SELECT COUNT(id) FROM `tmc_sc` WHERE `tmc_id`=".$DB->F($item["id"])." AND `sc_id`=".$DB->F($_POST["sc_id"])." AND ledit<".$DB->F($dateFrom)." AND (!inplace OR inplace IS NULL) AND (!incoming OR incoming IS NULL);");
                                $now = $DB->getField("SELECT COUNT(id) FROM `tmc_sc_tech` WHERE `tmc_sc_id` IN (SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=".$DB->F($item["id"]).") AND `tech_id`=".$DB->F($_REQUEST["tech_id"])." AND `sc_id`=".$DB->F($_POST["sc_id"])." AND DATE_FORMAT(`ledit`, '%Y-%m-%d')<=".$DB->F($dateFrom)." AND inplace=1;");
                                $rtpl->setVariable("ROW_TMC_COUNT_WAS", (intval($now)) >= 0 ? $now. " ".$item["metric_title"] : "<span style='margin:0; padding:0;color:#FF0000;'>".($in-$out). " ".$item["metric_title"]."</span>");
                                $incoming = $DB->getField("SELECT COUNT(id) FROM `tmc_sc_tech` WHERE `tmc_sc_id` IN (SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=".$DB->F($item["id"]).") AND `tech_id`=".$DB->F($_REQUEST["tech_id"])." AND `sc_id`=".$DB->F($_POST["sc_id"])." AND `ledit`>=".$DB->F($dateFrom)." AND DATE_FORMAT(`ledit`, '%Y-%m-%d')<=".$DB->F($dateTo)." AND incoming=1;");
                                $rtpl->setVariable("ROW_TMC_COUNT_INC", intval($incoming). " ".$item["metric_title"]);
                                $gone = $DB->getField("SELECT COUNT(id) FROM `tmc_sc_tech` WHERE `tmc_sc_id` IN (SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=".$DB->F($item["id"]).") AND `tech_id`=".$DB->F($_REQUEST["tech_id"])." AND `sc_id`=".$DB->F($_POST["sc_id"])." AND DATE_FORMAT(`ledit`, '%Y-%m-%d')>=".$DB->F($dateFrom)." AND DATE_FORMAT(`ledit`, '%Y-%m-%d')<=".$DB->F($dateTo)." AND (!incoming OR incoming IS NULL) AND (!inplace OR inplace IS NULL);");
                                $rtpl->setVariable("ROW_TMC_COUNT_GONE", intval($gone). " ".$item["metric_title"]);
                                $inplace = (intval($now)) + intval($incoming) - intval($gone);
                                $now = $DB->getField("SELECT COUNT(id) FROM `tmc_sc_tech` WHERE `tmc_sc_id` IN (SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=".$DB->F($item["id"]).") AND `tech_id`=".$DB->F($_REQUEST["tech_id"])." AND `sc_id`=".$DB->F($_POST["sc_id"])." AND inplace=1;");
                                $rtpl->setVariable("ROW_TMC_COUNT_INPLACE", $now. " ".$item["metric_title"] ." (" .(intval($inplace) >= 0 ? intval($inplace) : "<span style='margin:0; padding:0;color:#FF0000;'>".$inplace."</span>").")");
                            }
                            $rtpl->parse("rep_row");
                        }
                        $DB->free();
                    }
                }
                if (!$rows) $rtpl->touchBlock("no-rows");
                $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
                $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
                if (isset($_POST["createxlsversion"])) {
                    header("Content-type: application/vnd.ms-excel");  
                    header("Content-disposition: attachment; filename=report_tmc_tech_total_".$dateFrom."-".$dateTo.".xls");  
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    if (isset($_POST['createprintversion'])) {
                        $rtpl->touchBlock("print_footer");
                        $rtpl->show();
                        
                        return;
                    } else {
                        $tpl->setCurrentBlock("reportval");
                        $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                        $tpl->parse("reportval");
                    }
                }
            } else {
                $tpl->touchBlock("notcreated");
                $tpl->setVariable("DISABLED", "disabled='disabled'");
            }
        
        
        $tpl->show();
        
    }

}
?>