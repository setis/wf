<?php
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WF\Users\Model\GenderInterface;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
class adm_users_plugin extends Plugin
{
    private static $isLkUsers = [];

    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function addtocontr_ajax()
    {
        //Добавление пользователя в группу Контрагенты

        $id = $_REQUEST["target_user"];
        $contr_id = $_REQUEST["target_contr"];
        if (!$id) {
            $ret["error"] = "Не указан идентификатор пользователя";
            echo json_encode($ret);
            exit();
        }
        $sql = "UPDATE `users` SET `contr_user`=" . wf::$db->F($contr_id) . " WHERE `id`=" . wf::$db->F($id) . ";";
        $res = wf::$db->query($sql);
        wf::$db->free($res);
        $tpl = new HTML_Template_IT(path2("plugins/adm_users"));
        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/contrusers.tmpl.htm");
        else
            $tpl->loadTemplatefile("contrusers.tmpl.htm");
        wf::$db->query("SELECT * FROM `users` WHERE `contr_user`=" . wf::$db->F($contr_id) . ";");
        if (wf::$db->num_rows()) {
            $tpl->setVariable("USER_NO_RECORDS", "style='display:none;'");
            while ($r = wf::$db->fetch(true)) {
                $tpl->setCurrentBlock("userrow");
                if ($r["allow_access_cu"]) {
                    $tpl->setVariable("IS_ACTIVE", "checked='checked'");
                    $tpl->setVariable("STATUS_COLOR", "#99ff99");
                } else {
                    //$tpl->setVariable("STATUS_COLOR", "#d5d5d5");
                }
                $tpl->setVariable("USER_ID", $r["id"]);
                $tpl->setVariable("USER_FIO", $r["fio"]);
                $tpl->setVariable("USER_LOGIN", $r["login"]);
                $tpl->setVariable("USER_EMAIL", $r["email"]);
                $tpl->setVariable("USER_PHONE", $r["phones"]);
                $tpl->parse("userrow");
            }
        } else {
            $tpl->setVariable("USER_NO_RECORDS", " ");
        }
        wf::$db->free();
        $ret["ok"] = "ok";
        $ret["tpl"] = $tpl->get();
        echo json_encode($ret);
        exit();
    }

    static function removefromcontr_ajax()
    {
        //Удаление пользователя из группы Контрагенты

        $id = $_REQUEST["target_user"];
        if (!$id) $ret["error"] = "Не указан идентификатор пользователя";
        wf::$db->free($m);
        $contr_id = wf::$db->getField("SELECT `contr_user` FROM `users` WHERE `id`=" . wf::$db->F($id) . ";");
        $sql = "UPDATE `users` SET `contr_user` = NULL, `allow_access_cu`='0' WHERE `id`=" . wf::$db->F($id) . ";";
        wf::$db->query($sql);
        if (wf::$db->errno()) $ret["error"] = wf::$db->error(); else $ret["ok"] = "ok";
        if ($ret["error"]) {
            echo json_encode($ret);
            exit();
        }
        $tpl = new HTML_Template_IT(path2("plugins/adm_users"));
        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/contrusers.tmpl.htm");
        else
            $tpl->loadTemplatefile("contrusers.tmpl.htm");
        wf::$db->query("SELECT * FROM `users` WHERE `contr_user`=" . wf::$db->F($contr_id) . ";");
        if (wf::$db->num_rows()) {
            $tpl->setVariable("USER_NO_RECORDS", "style='display:none;'");
            while ($r = wf::$db->fetch(true)) {
                $tpl->setCurrentBlock("userrow");
                if ($r["allow_access_cu"]) {
                    $tpl->setVariable("IS_ACTIVE", "checked='checked'");
                    $tpl->setVariable("STATUS_COLOR", "#99ff99");
                } else {
                    //$tpl->setVariable("STATUS_COLOR", "#d5d5d5");
                }
                $tpl->setVariable("USER_ID", $r["id"]);
                $tpl->setVariable("USER_FIO", $r["fio"]);
                $tpl->setVariable("USER_LOGIN", $r["login"]);
                $tpl->setVariable("USER_EMAIL", $r["email"]);
                $tpl->setVariable("USER_PHONE", $r["phones"]);
                $tpl->parse("userrow");
            }
        } else {
            $tpl->setVariable("USER_NO_RECORDS", " ");
        }
        wf::$db->free();

        $ret["tpl"] = $tpl->get();
        echo json_encode($ret);
        exit();
    }

    static function togglecontractive_ajax()
    {
        //Установка-снятие флага активности от Контрагента

        $id = $_REQUEST["target_user"];
        if (!$id) $ret["error"] = "Не указан идентификатор пользователя";
        else {
            $state = wf::$db->getField("SELECT `allow_access_cu` FROM `users` WHERE `id`=" . wf::$db->F($id) . ";");
            if ($state) $target = "0"; else $target = "1";
            $sql = "UPDATE `users` SET `allow_access_cu`=" . wf::$db->F($target) . " WHERE `id`=" . wf::$db->F($id) . ";";
            wf::$db->query($sql);
            if (wf::$db->errno()) $ret["error"] = wf::$db->error(); else $ret["ok"] = "ok";
        }
        if ($ret["error"]) {
            echo json_encode($ret);
            exit();
        }
        $contr_id = wf::$db->getField("SELECT `contr_user` FROM `users` WHERE `id`=" . wf::$db->F($id) . ";");
        $tpl = new HTML_Template_IT(path2("plugins/adm_users"));
        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/contrusers.tmpl.htm");
        else
            $tpl->loadTemplatefile("contrusers.tmpl.htm");
        wf::$db->query("SELECT * FROM `users` WHERE `contr_user`=" . wf::$db->F($contr_id) . ";");
        if (wf::$db->num_rows()) {
            $tpl->setVariable("USER_NO_RECORDS", "style='display:none;'");
            while ($r = wf::$db->fetch(true)) {
                $tpl->setCurrentBlock("userrow");
                if ($r["allow_access_cu"]) {
                    $tpl->setVariable("IS_ACTIVE", "checked='checked'");
                    $tpl->setVariable("STATUS_COLOR", "#99ff99");
                } else {
                    //$tpl->setVariable("STATUS_COLOR", "#d5d5d5");
                }
                $tpl->setVariable("USER_ID", $r["id"]);
                $tpl->setVariable("USER_FIO", $r["fio"]);
                $tpl->setVariable("USER_LOGIN", $r["login"]);
                $tpl->setVariable("USER_EMAIL", $r["email"]);
                $tpl->setVariable("USER_PHONE", $r["phones"]);
                $tpl->parse("userrow");
            }
        } else {
            $tpl->setVariable("USER_NO_RECORDS", " ");
        }
        wf::$db->free();

        $ret["tpl"] = $tpl->get();
        echo json_encode($ret);
        exit();
    }

    static function getContrUsers()
    {

        $contr = $_REQUEST["contr_id"];
        if (!$contr) {
            $ret["error"] = "Не указан идентификатор контрагента!";
            echo json_encode($ret);
            exit();
        } else {
            $ret = wf::$db->query("SELECT * FROM `users` WHERE `contr_user`=" . wf::$db->F($contr));
        }
        echo json_encode($ret);
        exit();
    }

    static function getDepartment($id)
    {


        return wf::$db->getRow("SELECT * FROM `user_otdels` WHERE `id`=" . wf::$db->F($id), true);
    }

    static function getDepartmentUsers($department_id, $roles = null)
    {

        if (is_array($roles)) {
            $roles = implode(', ', array_map('intval', $roles));
        } else {
            $roles = intval($roles);
        }

        if (empty($roles))
            $roles = '0';

        $query = "SELECT * FROM users AS u WHERE
                (u.pos_id IN (" . $roles . ") OR 1 = " . (int)!$roles . ") AND u.active
                AND otdel_id = " . wf::$db->F($department_id);

        return wf::$db->getAll($query);
    }

    static function getOtdelEmail($id)
    {

        if (!$id)
            return false;
        return wf::$db->getField("SELECT `email` FROM `user_otdels` WHERE `id`=" . wf::$db->F($id));
    }

    /**
     * @param $id
     * @return array
     */
    static function getUser($id)
    {

        $sql = "SELECT `id`, `fio`, `pos_id` FROM `users` WHERE `id`=" . wf::$db->F($id) . ";";
        wf::$db->query($sql);
        if (wf::$db->errno() || !wf::$db->num_rows()) {
            wf::$db->free();
            return false;
        }
        $result = wf::$db->fetch(true);
        wf::$db->free();
        return $result;
    }

    static function getUserPhone($id)
    {

        $sql = "SELECT `phones` FROM `users` WHERE `id`=" . wf::$db->F($id) . ";";
        $result = wf::$db->getField($sql);
        return $result;
    }

    static function getUserActiveList()
    {

        $sql = "SELECT `id`, `fio` FROM `users` WHERE `active` ORDER BY `fio`;";
        $result = wf::$db->getCell2($sql);
        return $result ? array2options($result) : false;
    }

    static function getUserActiveTechList()
    {

        $sql = "SELECT `id`, `fio` FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `list_empl`) GROUP BY `id` ORDER BY `fio`;";
        $result = wf::$db->getCell2($sql);
        return $result ? array2options($result) : false;
    }

    static function isUserUsingNBN($id)
    {

        $sql = "SELECT `usenbnintegr` FROM `users` WHERE `id`=" . wf::$db->F($id) . ";";
        $result = wf::$db->getField($sql);
        if (wf::$db->errno()) {
            return false;
        }
        return $result > 0 ? true : false;
    }

    static function isLKUser($id)
    {
        if (!isset(self::$isLkUsers[$id])) {
            $sql = "SELECT `allow_access_cu` FROM `users` WHERE `id`=" . wf::$db->F($id) . ";";
            $result = wf::$db->getField($sql);
            if (wf::$db->errno()) {
                self::$isLkUsers[$id] = false;
            }
            self::$isLkUsers[$id] = $result > 0 ? true : false;
        }

        return self::$isLkUsers[$id];


    }

    static function getLKUserContrID($id)
    {

        $sql = "SELECT `contr_user` FROM `users` WHERE `id`=" . wf::$db->F($id) . ";";
        $result = wf::$db->getField($sql);
        if (wf::$db->errno()) {
            return false;
        }
        return $result > 0 ? $result : false;
    }

    static function getLKWtypeList($user_id)
    {
        if (!$user_id)
            return false;
        return wf::$db->getCell2("SELECT `wtype_id`, (SELECT `title` FROM `task_types` WHERE `id`=`wtype_id`) AS wtype_name FROM `list_lkuser_wtypes` WHERE `user_id`=" . wf::$db->F($user_id));
    }

    static function getLKWtypeIDList($user_id)
    {
        if (!$user_id) return false;
        return wf::$db->getCell("SELECT `wtype_id` FROM `list_lkuser_wtypes` WHERE `user_id`=" . wf::$db->F($user_id));
    }

    static function addLKWtypeID_ajax()
    {
        $user_id = $_REQUEST["user_id"];
        $wtype_id = $_REQUEST["wtype_id"];

        if (!$user_id || !$wtype_id) {
            $ret["error"] = "Недостаточно данных для выполнения операции";
            echo json_encode($ret);
            return false;
        }
        $sql = "DELETE FROM `list_lkuser_wtypes` WHERE `wtype_id`=" . wf::$db->F($wtype_id) . " AND `user_id`=" . wf::$db->F($user_id) . ";";
        $m = wf::$db->query($sql);
        if (wf::$db->errno()) {
            $ret["error"] = wf::$db->error();
            echo json_encode($ret);
            return false;
        }
        wf::$db->free($m);
        $sql = "INSERT INTO `list_lkuser_wtypes` (`user_id`, `wtype_id`) VALUES (" . wf::$db->F($user_id) . ", " . wf::$db->F($wtype_id) . ")";
        $m = wf::$db->query($sql);
        if (wf::$db->errno()) {
            $ret["error"] = wf::$db->error();
            echo json_encode($ret);
            return false;
        } else {
            wf::$db->free($m);
            $wtype_name = wf::$db->getField("SELECT `title` FROM `task_types` WHERE `id`=" . wf::$db->F($wtype_id) . ";");
            $ret["tpl"] = "<div>" . $wtype_name . "  <span user_id=\"" . $user_id . "\" id=\"" . $wtype_id . "\" style=\"cursor: pointer; color: #FF0000;\" class=\"removeuserlkwtype pull-right glyphicon glyphicon-remove\"></span></div>";
            $ret["ok"] = "ok";
            echo json_encode($ret);
            return false;
        }

    }

    static function deleteLKWtypeID_ajax()
    {

        $user_id = $_REQUEST["user_id"];
        $wtype_id = $_REQUEST["wtype_id"];

        if (!$user_id || !$wtype_id) {
            $ret["error"] = "Недостаточно данных для выполнения операции";
            echo json_encode($ret);
            return false;
        }
        $sql = "DELETE FROM `list_lkuser_wtypes` WHERE `wtype_id`=" . wf::$db->F($wtype_id) . " AND `user_id`=" . wf::$db->F($user_id) . ";";
        $m = wf::$db->query($sql);
        if (wf::$db->errno()) {
            $ret["error"] = wf::$db->error();
            echo json_encode($ret);
            return false;
        }
        wf::$db->free($m);
        $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;

    }

    static function getExtSCState($user_id)
    {

        $id = false;
        if ($user_id) $id = $user_id;
        else {
            if ($_REQUEST["target_user"])
                $id = $_REQUEST["target_user"];
        }
        if (!$id)
            UIError("Не указан идентификатор техника!");
        else
            return wf::$db->getField("SELECT `allow_ext_sc` FROM `users` WHERE `id`=" . wf::$db->F($id) . ";");
    }

    static function toggletechextsc_ajax()
    {


        $id = $_REQUEST["target_user"];
        if (!$id) $ret["error"] = "Не указан идентификатор пользователя";
        else {
            $state = wf::$db->getField("SELECT `allow_ext_sc` FROM `users` WHERE `id`=" . wf::$db->F($id) . ";");
            $ret["prevstate"] = $state;
            if ($state) $target = "0"; else $target = "1";
            $sql = "UPDATE `users` SET `allow_ext_sc`=" . wf::$db->F($target) . " WHERE `id`=" . wf::$db->F($id) . ";";
            wf::$db->query($sql);
            if (wf::$db->errno()) $ret["error"] = wf::$db->error(); else $ret["ok"] = "ok";
            $ret["state"] = $target;
            if ($state) {
                $currentSC = wf::$db->getField("SELECT `sc_id` FROM `link_sc_user` WHERE `user_id`=" . wf::$db->F($id) . ";");
                if (!$currentSC) {
                    $ret["error"] = "Ошибка получения текущего СЦ техника: " . $id;
                } else {
                    $areaList = wf::$db->getCell("SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" . wf::$db->F($currentSC));
                    $currentTechAreaList = wf::$db->getCell("SELECT `area_id` FROM `link_user_area` WHERE `user_id`=" . wf::$db->F($id));
                    foreach ($currentTechAreaList as $item) {
                        if (!in_array($item, $areaList)) {
                            wf::$db->query("DELETE FROM `link_user_area` WHERE `area_id`=" . wf::$db->F($item) . " AND `user_id`=" . wf::$db->F($id) . " LIMIT 1");
                            if (wf::$db->errno()) {
                                $ret["error"] = wf::$db->error();
                                echo json_encode($ret);
                                exit();
                            }
                        }
                    }
                }
            }

        }
        if ($ret["error"]) {
            echo json_encode($ret);
            exit();
        }

        echo json_encode($ret);
        exit();
    }

    public function roles(Request $request)
    {
        $utype = array(
            1 => "Пользователь",
            2 => "Техник",
            3 => "Руководитель СЦ"
        );
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/roles.tmpl.htm");
        else
            $tpl->loadTemplatefile("roles.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        if (wf::$user->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }

        $hideLogin = true;
        if(wf::$user->checkAccess($this->getUID(), User::ACCESS_WRITE)) {
            $hideLogin = false;
        }

        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        //$_REQUEST["filter_rpp"] = getcfg('rows_per_page');

        $_REQUEST['fio'] = $this->getCookie('fio', $_REQUEST['fio']);
        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_podr_id'] = $this->getCookie('filter_podr_id', $_REQUEST['filter_podr_id']);
        $_REQUEST['filter_utype_id'] = $this->getCookie('filter_utype_id', $_REQUEST['filter_utype_id']);


        //$_REQUEST['showinactive'] = $this->getCookie('showinactive', $_REQUEST['showinactive']);
        $tpl->setVariable('FIO', $_REQUEST['fio']);
        $tpl->setVariable('FILTER_ID', $_REQUEST['filter_id']);
        $tpl->setVariable('FILTER_PODR_OPTIONS', array2options(wf::$db->getCell2("SELECT `id`, `name` FROM `user_otdels` ORDER BY `order`, `name`"), $_REQUEST['filter_podr_id']));
        $tpl->setVariable('FILTER_UTYPE_OPTIONS', array2options($utype, $_REQUEST['filter_utype_id']));
        $tpl->setVariable('SHOWINACTIVE', $_REQUEST['showinactive'] == "on" ? "checked='checked'" : "");


        if ($_REQUEST['filter_podr_id']) {
            $pfilter = " WHERE `id`=" . wf::$db->F($_REQUEST['filter_podr_id']);
        }
        if ($_REQUEST["showinactive"] != "on") {
            $ufilter = " AND `active` ";
        }
        if ($_REQUEST["fio"]) {
            $ufilter .= " AND (`login` LIKE " . wf::$db->F("%" . $_REQUEST["fio"] . "%") . "
                            OR `email` LIKE " . wf::$db->F("%" . $_REQUEST["fio"] . "%") . "
                            OR `dolzn` LIKE " . wf::$db->F("%" . $_REQUEST["fio"] . "%") . "
                            OR `fio` LIKE " . wf::$db->F("%" . $_REQUEST["fio"] . "%") . ") ";
        }
        if ($_REQUEST["filter_id"]) {
            $ufilter .= " AND `id`=" . wf::$db->F($_REQUEST["filter_id"]) . " ";
        }

        if ($_REQUEST['filter_utype_id']) {
            switch ($_REQUEST['filter_utype_id']) {
                case "1":
                    $ufilter .= " AND `id` NOT IN (SELECT DISTINCT(user_id) FROM `link_sc_user`) ";
                    break;

                case "2":
                    $ufilter .= " AND `id` IN (SELECT DISTINCT(user_id) FROM `link_sc_user`) AND `id` NOT IN (SELECT DISTINCT(user_id) FROM `link_sc_chiefs`)";
                    break;

                case "3":
                    $ufilter .= " AND `id` IN (SELECT DISTINCT(user_id) FROM `link_sc_chiefs`) ";
                    break;

                default :

                    break;
            }
        }


            $otdel_id = $otdels['id'];
            $otdel_name = $otdels['name'];

            $tpl->setCurrentBlock('ou');
            $tpl->setVariable('OTDEL_ID', $otdel_id);
            $tpl->setVariable('OTDEL_NAME', $otdel_name);
            $tpl->setVariable('OTDEL_EMAIL', $otdels['email']);

            $sql = "SELECT `id`, `fio`, `login`, `email`, `phones`, `dolzn`, `active`, `internalphone`, `pos_id` FROM `users` WHERE is_access_template = 1";
            wf::$db->query($sql);
            if (wf::$db->num_rows()) {
                while (list($user_id, $user_fio, $user_login, $user_email, $user_phones, $user_dolzn1, $user_active, $intphone, $user_pos1) = wf::$db->fetch()) {
                    $tpl->setCurrentBlock('user');
                    $tpl->setVariable('USER_ID', $user_id);
                    $tpl->setVariable('USER_FIO', $user_fio);
                    if ($hideLogin) {
                        $tpl->setVariable('USER_LOGIN', '*****');
                    } else {
                        $tpl->setVariable('USER_LOGIN', $user_login);
                    }
                    $tpl->setVariable('USER_EMAIL', $user_email);
                    $tpl->setVariable('USER_PHONES', $user_phones);
                    $tpl->setVariable("USER_INTPHONE", $intphone > 0 ? $intphone : "&mdash;");
                    $user_pos = user_positions_plugin::getById($user_pos1);
                    //$tpl->setVariable('USER_DOLZN1', $user_dolzn1);
                    $tpl->setVariable('USER_DOLZN', $user_pos ? $user_pos : "");
                    if ($user_active) {
                        //$tpl->touchBlock('user_active');
                    } else {
                        //$tpl->touchBlock('user_inactive');
                        $tpl->setVariable('USER_CLASS', "inactive");
                    }
                    $tpl->parse('user');
                }
            } else {
                if ($_REQUEST['showinactive'] == "")
                    $tpl->setVariable("OTDEL_HIDE", "style='display:none;'");
            }
            wf::$db->free();

            $tpl->parse('ou');



        UIHeader($tpl);
        $tpl->show();
    }

    public function role_edit(Request $request)
    {
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile(wf::$user->getTemplate() . "/role.tmpl.htm");

        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        if ($user_id = $_REQUEST['user_id']) {
            $user = new User($user_id);
            $tpl->setVariable('ID_TEXT', $user->getId());
            $tpl->setVariable('ID', $user->getId());
            $tpl->setVariable('FIO', $user->getFio());
        } else {
            $tpl->setVariable('ID_TEXT', 'Новый');
            $user = null;
        }
        UIHeader($tpl);
        $tpl->show();
    }

    public function role_del(Request $request)
    {
        $user_id = $request->get('user_id');

        if (null === $user = $this->getEm()->find(models\User::class, $user_id)) {
            UIError(500, 'Нет такой роли!');
        }

        $this->getEm()->remove($user);
        $this->getEm()->flush();

        redirect($this->getLink('roles'));
    }

    public function role_save(Request $request)
    {
        $user_id = $request->get('user_id');

        $fio = $request->get('user_fio');
        if (empty($fio)) {
            UIError("Не заполнен заголовок");
        }

        if ($user_id) {
            $user = $this->getEm()->find(models\User::class, $user_id);
        }
        else {
            $user = new models\User();
            /* @var $encoder Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface */
            $encoder = wf::$container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, md5(uniqid(null, true)));
            $user->setPass($encoded)
                ->setLogin(md5(uniqid(null, true)))
                ->setActive(false)
                ->setIsAccessTemplate(true)
                ;
            $this->getEm()->persist($user);
        }

        $user->setFio($fio)->setComment($request->get('cmm'));
        $this->getEm()->flush();

        redirect($this->getLink('user_view', "user_id={$user->getId()}"), "Учетная запись сохранена");
    }

    function struct(Request $request)
    {
        $utype = array(
            1 => "Пользователь",
            2 => "Техник",
            3 => "Руководитель СЦ"
        );
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/struct.tmpl.htm");
        else
            $tpl->loadTemplatefile("struct.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        if (wf::$user->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }

        $hideLogin = true;
        if(wf::$user->checkAccess($this->getUID(), User::ACCESS_WRITE)) {
            $hideLogin = false;
        }

        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        //$_REQUEST["filter_rpp"] = getcfg('rows_per_page');

        $_REQUEST['fio'] = $this->getCookie('fio', $_REQUEST['fio']);
        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_podr_id'] = $this->getCookie('filter_podr_id', $_REQUEST['filter_podr_id']);
        $_REQUEST['filter_utype_id'] = $this->getCookie('filter_utype_id', $_REQUEST['filter_utype_id']);


        //$_REQUEST['showinactive'] = $this->getCookie('showinactive', $_REQUEST['showinactive']);
        $tpl->setVariable('FIO', $_REQUEST['fio']);
        $tpl->setVariable('FILTER_ID', $_REQUEST['filter_id']);
        $tpl->setVariable('FILTER_PODR_OPTIONS', array2options(wf::$db->getCell2("SELECT `id`, `name` FROM `user_otdels` ORDER BY `order`, `name`"), $_REQUEST['filter_podr_id']));
        $tpl->setVariable('FILTER_UTYPE_OPTIONS', array2options($utype, $_REQUEST['filter_utype_id']));
        $tpl->setVariable('SHOWINACTIVE', $_REQUEST['showinactive'] == "on" ? "checked='checked'" : "");


        if ($_REQUEST['filter_podr_id']) {
            $pfilter = " WHERE `id`=" . wf::$db->F($_REQUEST['filter_podr_id']);
        }
        if ($_REQUEST["showinactive"] != "on") {
            $ufilter = " AND `active` ";
        }
        if ($_REQUEST["fio"]) {
            $ufilter .= " AND (`login` LIKE " . wf::$db->F("%" . $_REQUEST["fio"] . "%") . "
                            OR `email` LIKE " . wf::$db->F("%" . $_REQUEST["fio"] . "%") . "
                            OR `dolzn` LIKE " . wf::$db->F("%" . $_REQUEST["fio"] . "%") . "
                            OR `fio` LIKE " . wf::$db->F("%" . $_REQUEST["fio"] . "%") . ") ";
        }
        if ($_REQUEST["filter_id"]) {
            $ufilter .= " AND `id`=" . wf::$db->F($_REQUEST["filter_id"]) . " ";
        }

        if ($_REQUEST['filter_utype_id']) {
            switch ($_REQUEST['filter_utype_id']) {
                case "1":
                    $ufilter .= " AND `id` NOT IN (SELECT DISTINCT(user_id) FROM `link_sc_user`) ";
                    break;

                case "2":
                    $ufilter .= " AND `id` IN (SELECT DISTINCT(user_id) FROM `link_sc_user`) AND `id` NOT IN (SELECT DISTINCT(user_id) FROM `link_sc_chiefs`)";
                    break;

                case "3":
                    $ufilter .= " AND `id` IN (SELECT DISTINCT(user_id) FROM `link_sc_chiefs`) ";
                    break;

                default :

                    break;
            }
        }
        $sql = "SELECT `id`, `name`, `email` FROM `user_otdels`" . $pfilter . " ORDER BY `order`, `name`";
        //die($sql);
        wf::$db->query($sql);
        while ($otdels = wf::$db->fetch($sql, true)) {
            $otdel_id = $otdels['id'];
            $otdel_name = $otdels['name'];

            $tpl->setCurrentBlock('ou');
            $tpl->setVariable('OTDEL_ID', $otdel_id);
            $tpl->setVariable('OTDEL_NAME', $otdel_name);
            $tpl->setVariable('OTDEL_EMAIL', $otdels['email']);

            $sql = "SELECT `id`, `fio`, `login`, `email`, `phones`, `dolzn`, `active`, `internalphone`, `pos_id` FROM `users` WHERE `otdel_id`=" . wf::$db->F($otdel_id) . " " . $ufilter . " ORDER BY `fio`";
            wf::$db->query($sql);
            if (wf::$db->num_rows()) {
                while (list($user_id, $user_fio, $user_login, $user_email, $user_phones, $user_dolzn1, $user_active, $intphone, $user_pos1) = wf::$db->fetch()) {
                    $tpl->setCurrentBlock('user');
                    $tpl->setVariable('USER_ID', $user_id);
                    $tpl->setVariable('USER_FIO', $user_fio);
                    if ($hideLogin) {
                        $tpl->setVariable('USER_LOGIN', '*****');
                    } else {
                        $tpl->setVariable('USER_LOGIN', $user_login);
                    }
                    $tpl->setVariable('USER_EMAIL', $user_email);
                    $tpl->setVariable('USER_PHONES', $user_phones);
                    $tpl->setVariable("USER_INTPHONE", $intphone > 0 ? $intphone : "&mdash;");
                    $user_pos = user_positions_plugin::getById($user_pos1);
                    //$tpl->setVariable('USER_DOLZN1', $user_dolzn1);
                    $tpl->setVariable('USER_DOLZN', $user_pos ? $user_pos : "");
                    if ($user_active) {
                        //$tpl->touchBlock('user_active');
                    } else {
                        //$tpl->touchBlock('user_inactive');
                        $tpl->setVariable('USER_CLASS', "inactive");
                    }
                    $tpl->parse('user');
                }
            } else {
                if ($_REQUEST['showinactive'] == "")
                    $tpl->setVariable("OTDEL_HIDE", "style='display:none;'");
            }
            wf::$db->free();

            $tpl->parse('ou');
        }


        UIHeader($tpl);
        $tpl->show();
    }

    function browse()
    {
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("browse.tmpl.htm");
        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/browse.tmpl.htm");
        else
            $tpl->loadTemplatefile("browse.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        $checked = explode(',', $_REQUEST['user_ids']);
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseUsers');

        $sql = "SELECT `id`, `name` FROM `user_otdels` ORDER BY `order`, `name`";
        foreach ($otdels = wf::$db->getCell2($sql) as $otdel_id => $otdel_name) {
            $tpl->setCurrentBlock('ou');
            $tpl->setVariable('OTDEL_ID', $otdel_id);
            $tpl->setVariable('OTDEL_NAME', $otdel_name);

            $sql = "SELECT `id`, `fio`, `dolzn` FROM `users` WHERE `otdel_id`=" . wf::$db->F($otdel_id) . " AND `active` ORDER BY `fio`";
            foreach ($users = wf::$db->getCell2($sql) as $user_id => $user_fio) {
                $tpl->setCurrentBlock('user');
                $tpl->setVariable('USER_ID', $user_id);
                $tpl->setVariable('USER_FIO', $user_fio);
                $tpl->setVariable('USER_CHK', in_array($user_id, $checked) ? 'checked' : '');
                $tpl->parse('user');
            }
            wf::$db->free();

            $tpl->parse('ou');
        }


        UIHeader($tpl);
        $tpl->show();
    }

    function browse2()
    {


        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("browse2.tmpl.htm");
        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/browse2.tmpl.htm");
        else
            $tpl->loadTemplatefile("browse2.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        $checked = $_REQUEST['user_id'];
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseUser');

        $sql = "SELECT `id`, `name` FROM `user_otdels` ORDER BY `order`, `name`";
        foreach ($otdels = wf::$db->getCell2($sql) as $otdel_id => $otdel_name) {
            $tpl->setCurrentBlock('ou');
            $tpl->setVariable('OTDEL_ID', $otdel_id);
            $tpl->setVariable('OTDEL_NAME', $otdel_name);

            $sql = "SELECT `id`, `fio` FROM `users` WHERE `otdel_id`=" . wf::$db->F($otdel_id) . " AND `active` ORDER BY `fio`";
            foreach ($users = wf::$db->getCell2($sql) as $user_id => $user_fio) {
                $tpl->setCurrentBlock('user');
                $tpl->setVariable('USER_ID', $user_id);
                $tpl->setVariable('USER_FIO', $user_fio);
                $tpl->setVariable('USER_CHK', $user_id == $checked ? 'checked' : '');
                $tpl->parse('user');
            }
            wf::$db->free();

            $tpl->parse('ou');
        }


        UIHeader($tpl);
        $tpl->show();
    }

    function otdel_edit()
    {


        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("otdel.tmpl.htm");
        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/otdel.tmpl.htm");
        else
            $tpl->loadTemplatefile("otdel.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        if ($otdel_id = $_REQUEST['otdel_id']) {
            $otdel = wf::$db->getRow("SELECT * FROM `user_otdels` WHERE `id`=" . wf::$db->F($otdel_id), true);
            $tpl->setVariable('ID', $otdel['id']);
            $tpl->setVariable('ID_TEXT', $otdel['id']);
            $tpl->setVariable('NAME', $otdel['name']);
            $tpl->setVariable('EMAIL', $otdel['email']);
            $tpl->setVariable('ORDER', $otdel['order']);
        } else {
            $tpl->setVariable('ID_TEXT', "Новый");
        }

        UIHeader($tpl);
        $tpl->show();
    }

    function otdel_del()
    {
        global $USER;

        $this->getLogger('security')->info($USER->getFio() . ' удалил(а) отдел ' . date('Y-m-d H:i:s'), ['POST' => json_encode($_POST, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)]);

        $err = array();

        if (!($id = $_REQUEST['otdel_id'])) $err[] = "Не указано подразделение";

        $sql = "SELECT COUNT(*) FROM `users` WHERE `otdel_id`=" . wf::$db->F($id);
        if ($c = wf::$db->getField($sql)) $err[] = "Нельзя удалить подразделение, есть $c сотрудников!";

        if (sizeof($err)) UIError($err);

        $sql = "DELETE FROM `user_otdels` WHERE `id`=" . wf::$db->F($id);
        wf::$db->query($sql);
        redirect($this->getLink());
    }

    function user_edit()
    {
        global $PLUGINS;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("user.tmpl.htm");
        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/user.tmpl.htm");
        else
            $tpl->loadTemplatefile("user.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        if ($user_id = $_REQUEST['user_id']) {
            $user = new User($user_id);
            $tpl->setVariable('ID_TEXT', $user->getId());
            $tpl->setVariable('ID', $user->getId());
            $tpl->setVariable('FIO', $user->getFio());
            $tpl->setVariable('LOGIN', $user->getLogin());
            $tpl->setVariable('EMAIL', $user->getEmail());
            //$tpl->setVariable('DOLZN', $user->getDolzn());
            $tpl->setVariable('PHONES', $user->getPhones());
            $tpl->setVariable('BIRTHDAY', $user->getBirthday());
            $tpl->setVariable('GENDERS', $this->getGenderOptions($user->getGender()));
            $tpl->setVariable("POS_OPTIONS", user_positions_plugin::getOptions($user->getPosId()));
            $tpl->setVariable("INTPHONES", $user->getIntPhone() > 0 ? $user->getIntPhone() : "");
            $tpl->setVariable("USERTOJOB", $user->getToJobDate() ? date("d.m.Y", strtotime($user->getToJobDate())) : "");
            $tpl->setVariable("USEROUTJOB", $user->getOutJobDate() ? date("d.m.Y", strtotime($user->getOutJobDate())) : "");
            $tpl->setVariable('CMM', $user->getComment(false));
            if ($user->getAccessType()) {
                $tpl->setVariable("FROMOFFICE", "selected='selected'");
            } else {
                $tpl->setVariable("ANYWHERE", "selected='selected'");
            }
            $tpl->setVariable('OTDEL_OPTIONS', $this->getOtdelOptions($user->getOtdelId()));
            $tpl->setVariable('ACTVE_CHK', $user->isActive() ? 'checked' : '');
        } else {
            $tpl->setVariable('ID_TEXT', 'Новый');
            $tpl->setVariable("POS_OPTIONS", user_positions_plugin::getOptions());
            $tpl->setVariable('OTDEL_OPTIONS', $this->getOtdelOptions($_REQUEST['otdel_id']));
            $tpl->setVariable('ACTVE_CHK', 'checked');
            $tpl->setVariable('NO_ID_DIS', 'disabled');
            $tpl->setVariable('GENDERS', $this->getGenderOptions());
            $user = null;
        }
        foreach ($PLUGINS as $uid => $a) {
            $tpl->setCurrentBlock('row');
            $tpl->setVariable('MODULE_UID', $uid);
            $tpl->setVariable('MODULE_NAME', $a['name']);
            if ($user)
                $tpl->setVariable('CHK' . intval($user->getAccess($uid)), 'checked');
            else
                $tpl->setVariable('CHK0', 'checked');
            $tpl->parse("row");
        }
        $uList = $this->getUserListByPos($user_id);
        if ($uList) {
            $tpl->setCurrentBlock("usercopyrights");
            $tpl->setVariable("SAMEPOSUSERS", $uList);
            $tpl->setVariable("USERID", $user_id);

            $tpl->parse("usercopyrights");
        } else {
            $tpl->touchBlock("nouserpos");
        }
        UIHeader($tpl);
        $tpl->show();
    }

    static function getGenderOptions($gender = null)
    {
        return array2options(GenderInterface::GENDERS, $gender);
    }

    static function getOtdelOptions($sel_id = 0)
    {
        return array2options(wf::$db->getCell2("SELECT `id`, `name` FROM `user_otdels` ORDER BY `order`, `name`"), $sel_id);
    }

    function getUserListByPos($uid)
    {

        $sql = "SELECT `id`, `fio` FROM `users` WHERE `active` AND `id`!=" . wf::$db->F($uid) . " ORDER BY `fio`";

        return array2options(wf::$db->getCell2($sql));
    }

    function user_view(Request $request)
    {
        global $PLUGINS, $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));

        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/viewuser.tmpl.htm");
        else
            $tpl->loadTemplatefile("viewuser.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        $hideLogin = true;
        if(wf::$user->checkAccess($this->getUID(), User::ACCESS_WRITE)) {
            $hideLogin = false;
        }

        $edited_user = new User($request->get('user_id'));
        if (!$edited_user->getId())
            UIError("Не найден пользователь с указанным идентификатором!");

        $tpl->setVariable('USER_TEMPLATE', wf::$user->getTemplate());
        $tpl->setVariable('ID_TEXT', $edited_user->getId());
        $tpl->setVariable('ID', $edited_user->getId());
        $tpl->setVariable('USERPHOTO', $edited_user->getPhoto());
        $tpl->setVariable('FIO', $edited_user->getFio());
        if ($hideLogin) {
            $tpl->setVariable('LOGIN', '*****');
        } else {
            $tpl->setVariable('LOGIN', $edited_user->getLogin());
        }
        $tpl->setVariable('EMAIL', $edited_user->getEmail());
        $tpl->setVariable('GENDER', $edited_user->getGenderMemo());
        $tpl->setVariable("DOLZN", adm_users_plugin::getPos($edited_user->getPosId()) ? adm_users_plugin::getPos($edited_user->getPosId()) : "не указана");
        $tpl->setVariable('BIRTHDAY', $edited_user->getBirthday());

        if (wf::$user->checkAccess('adm_users.private_call')) {
            $tpl->setVariable('CALL_FROM_INTERNAL', wf::$user->getIntPhone());
            $tpl->setVariable('INT_PHONE', $edited_user->getIntPhone());
            $tpl->setVariable('EXT_PHONE', $edited_user->getPhones());
        } else {
            $tpl->setVariable('PHONES', $edited_user->getPhones());
            $tpl->setVariable("INTPHONES", $edited_user->getIntPhone() ? $edited_user->getIntPhone() : "&mdash;");
        }

        if (wf::$user->checkAccess('adm_users.private_message')) {
            $tpl->touchBlock('allow_1_private_message');

            $tpl->setVariable('ID1', $edited_user->getId());
            /** @var \WF\Notification\NotificationManager $notifications */
            $notifications = $this->getContainer()->get('notifications');
            $transports = $notifications->getHandlers();
            $tpl->setCurrentBlock('private_message_transports');
            foreach ($transports as $k => $handler) {
                if(!$handler->isPublic())
                    continue;
                $tpl->setVariable('TRANSPORT_KEY', $k);
                $tpl->setVariable('TRANSPORT_NAME', $handler->getName());
                $tpl->parseCurrentBlock();
            }
            $tpl->parseCurrentBlock();

        } else {
            $tpl->touchBlock('private_message_disallow');
        }

        $tpl->setVariable("USERTOJOB", $edited_user->getToJobDate() ? date("d.m.Y", strtotime($edited_user->getToJobDate())) : "&mdash;");
        $tpl->setVariable("USEROUTJOB", $edited_user->getOutJobDate() ? date("d.m.Y", strtotime($edited_user->getOutJobDate())) : "&mdash;");
        $tpl->setVariable('CMM', $edited_user->getComment(false));
        if ($edited_user->getAccessType()) {
            $tpl->setVariable("ACCESSLEVEL", "Только из офиса");
        } else {
            $tpl->setVariable("ACCESSLEVEL", "Любая подсеть");
        }
        $tpl->setVariable('OTDEL', $this->getOtdel($edited_user->getOtdelId()));
        $tpl->setVariable('ACTVE_CHK', $edited_user->isActive() ? 'да' : 'нет');


        if (wf::$user->checkAccess('adm_users', User::ACCESS_WRITE)) {
            $tpl->touchBlock('allow_1_user_access_edit');

            $tpl->setVariable('ID2', $edited_user->getId());

            $tpl->setVariable('ID3', $edited_user->getId());

            ## Доступы на СЦ
            if ($edited_user->getId() > 0) {
                #$tpl->setVariable("SC_USER_ID", $user_id);
                $sql = "select id,title,
    (select count(*) from user_sc_access uoa where uoa.sc_id = lo.id and uoa.user_id = ".$edited_user->getId().") has_access
    from list_sc lo
    order by lower(title),id";
                #echo $sql."<br/>\n";
                #exit;
                $DB->query($sql);
                while ($res = $DB->fetch(true)) {
                        $tpl->setCurrentBlock("sc_rr");
                        $tpl->setVariable("SC_ID", $res['id']);
                        $tpl->setVariable("SC_NAME", $res['title']);
                        $tpl->setVariable("SC_CHECKED", ($res['has_access'] > 0 ? "checked" : "") );
                        $tpl->parse("sc_rr");
                }
                $tpl->parseCurrentBlock();
            }

            $tpl->setCurrentBlock('row1');
            foreach ($PLUGINS as $uid => $a) {
                $tpl->setVariable('MODULE_UID', $uid);
                $tpl->setVariable('MODULE_NAME', $a['name']);
                if ($edited_user) $tpl->setVariable('CHK' . intval($edited_user->getAccess($uid)), " <strong style='font-size: 12pt;'>+</strong> ");
                else $tpl->setVariable('CHK0', '');
                $tpl->parseCurrentBlock();
            }
        } else {
            $tpl->touchBlock('disallow_1_user_access_edit');
        }

        UIHeader($tpl);
        $tpl->show();
    }

    static function getPos($id = 0)
    {

        if (!$id) return false;
        return wf::$db->getField("SELECT `title` FROM `user_pos` WHERE `id`=" . wf::$db->F($id) . "");
    }

    static function getOtdel($id = 0)
    {

        if (!$id) return false;
        return wf::$db->getField("SELECT `name` FROM `user_otdels` WHERE `id`=" . wf::$db->F($id));
    }

    public function access_edit(Request $request)
    {
        global $PLUGINS, $USER, $DB;

        if (User::ACCESS_WRITE != $USER->getAccess(USER_COPY_EDIT_SAVE_ACCESS)) {
            throw new \Symfony\Component\Security\Core\Exception\AccessDeniedException();
        }

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));

        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/access_edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("access_edit.tmpl.htm");

        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        if ($user_id = $_REQUEST['user_id']) {
            $edited_user = new User($user_id);
            if (!$edited_user->getId()) UIError("Не найден пользователь с указанным идентификатором!");
            $tpl->setVariable('ID_TEXT', $edited_user->getId());
            $tpl->setVariable('ID', $edited_user->getId());
            $tpl->setVariable('USERPHOTO', $edited_user->getPhoto());
            $tpl->setVariable('FIO', $edited_user->getFio());
            $tpl->setVariable('LOGIN', $edited_user->getLogin());
            $tpl->setVariable('EMAIL', $edited_user->getEmail());
            $tpl->setVariable('GENDER', $edited_user->getGenderMemo());
            //$tpl->setVariable('DOLZN', $edited_user->getDolzn());
            $tpl->setVariable("DOLZN", adm_users_plugin::getPos($edited_user->getPosId()) ? adm_users_plugin::getPos($edited_user->getPosId()) : "не указана");
            $tpl->setVariable('BIRTHDAY', $edited_user->getBirthday());

            $tpl->setVariable("USERTOJOB", $edited_user->getToJobDate() ? date("d.m.Y", strtotime($edited_user->getToJobDate())) : "&mdash;");
            $tpl->setVariable("USEROUTJOB", $edited_user->getOutJobDate() ? date("d.m.Y", strtotime($edited_user->getOutJobDate())) : "&mdash;");
            $tpl->setVariable('CMM', $edited_user->getComment(false));
            if ($edited_user->getAccessType()) {
                $tpl->setVariable("ACCESSLEVEL", "Только из офиса");
            } else {
                $tpl->setVariable("ACCESSLEVEL", "Любая подсеть");
            }
            $tpl->setVariable('OTDEL', $this->getOtdel($edited_user->getOtdelId()));
            $tpl->setVariable('ACTVE_CHK', $edited_user->isActive() ? 'да' : 'нет');
        } else {
            $tpl->setVariable('ID_TEXT', 'Новый');

            $tpl->setVariable('OTDEL_OPTIONS', $this->getOtdelOptions($_REQUEST['otdel_id']));
            $tpl->setVariable('ACTVE_CHK', 'checked');
            $tpl->setVariable('NO_ID_DIS', 'disabled');
            $edited_user = null;
        }

        if ($USER->checkAccess('adm_users', User::ACCESS_WRITE)) {
            $tpl->touchBlock('user_access_allow_1');
            $tpl->touchBlock('user_access_allow_2');

            $tpl->setCurrentBlock('user_access_allow_3');
            //$tpl->setCurrentBlock('row');
            foreach ($PLUGINS as $uid => $a) {
                $tpl->setCurrentBlock('row');
                $tpl->setVariable('MODULE_UID', $uid);
                $tpl->setVariable('MODULE_NAME', $a['name']);
                if ($edited_user)
                    $tpl->setVariable('CHK' . intval($edited_user->getAccess($uid)), 'checked');
                else
                    $tpl->setVariable('CHK0', 'checked');
                $tpl->parse("row");
            }
            $tpl->parseCurrentBlock();

        } else {
            $tpl->touchBlock('user_access_disallow');
        }

        $uList = $this->getUserListByPos($user_id);
        if ($uList) {
            $tpl->setCurrentBlock("usercopyrights");
            $tpl->setVariable("SAMEPOSUSERS", $uList);
            $tpl->setVariable("USERID", $user_id);

            $tpl->parse("usercopyrights");
        } else {
            $tpl->touchBlock("nouserpos");
        }

        ## Доступы на СЦ
        if ($user_id > 0) {
            $tpl->setVariable("SC_USER_ID", $user_id);
            $sql = "select id,title,
(select count(*) from user_sc_access uoa where uoa.sc_id = lo.id and uoa.user_id = ".$user_id.") has_access,
(select count(*) from link_sc_user lsu where lsu.sc_id = lo.id and lsu.user_id = ".$user_id.") default_access,
(select count(*) from link_sc_chiefs lsc where lsc.sc_id = lo.id and lsc.user_id = ".$user_id.") default_access2
from list_sc lo
order by lower(title),id";
            $DB->query($sql);
            while ($res = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("sc_rr");
                    $tpl->setVariable("SC_ID", $res['id']);
                    $tpl->setVariable("SC_NAME", $res['title']);
                    if ( ($res['default_access'] > 0) || ($res['default_access2'] > 0) ) {
                        $tpl->setVariable("SC_EXTRA", "<input type=hidden name='sc[".$res['id']."]' value='".$res['id']."' />");
                        $tpl->setVariable("SC_DISABLED", 'disabled="disabled"');
                    } else {
                         $tpl->setVariable("SC_DISABLED", '');
                         $tpl->setVariable("SC_EXTRA", "");
                    }

                    $tpl->setVariable("SC_CHECKED", ($res['has_access'] > 0 ? "checked" : "") );
                    $tpl->parse("sc_rr");
            }
            $tpl->parseCurrentBlock();
            #echo "<pre>\n";
            #print_r($res);
            #echo "</pre>\n";
        }

        UIHeader($tpl);
        $tpl->show();
    }

    public function save_sc_access_ajax() {
        global $DB;
        $user_id = intval($_REQUEST['user_id']);

        $sc = $_REQUEST['sc'];
        $sql = "start transaction; delete from user_sc_access where user_id = ".$user_id."; ";
        foreach ($sc as $key => $val) {
            $sql.= "insert into user_sc_access (user_id, sc_id) values (".$user_id.",".intval($val)."); ";
        }
        $sql .= " commit; ";

        $DB->query($sql);
        redirect("/adm_users/access_edit?user_id=".$user_id,"Доступы на СЦ изменены");
    }

    public function sendNotification(Request $request)
    {
        /** @var \WF\Notification\NotificationManager $notifications */
        $notifications = $this->getContainer()->get('notifications');
        $transports = $notifications->getHandlers();

        if ($request->request->has('handler') && $request->get('handler') !== null) {
            $transport = $transports[$request->get('handler')];
        } else {
            $transport = array_pop($transports);
        }

        /** @var \repository\UserRepository $repo */
        $repo = $this->getEm()->getRepository(\models\User::class);

        $notification = new \WF\Notification\Model\Notification();
        $notification
            ->setTo($repo->find($request->get('user_id')))
            ->setFrom($repo->find($this->getCurrentUser()))
            ->setMessage($request->get('message'))
            ->setSubject('Приватное сообщение');

        echo $transport->handle($notification) ? 'ok' : 'failed';
    }

    public function user_avatar(Request $request)
    {
        $id = $request->get('id');
        if (empty($id)) {
            throw new BadRequestHttpException('Undefined user id');
        }

        /* @var $user \models\User */
        $user = $this->getEm()->find(\models\User::class, $id);
        if (null === $user) {
            throw new NotFoundHttpException("User with id $id not found");
        }

        /* @var $fs \League\Flysystem\FilesystemInterface */
        $fs = $this->getContainer()->get('oneup_flysystem.user_photo_filesystem');
        $stream = null;

        // Try to get avatar image stream
        if (null !== $user->getAvatarName()) {
            if ($fs->has($user->getAvatarName())) {
                $stream = $fs->readStream($user->getAvatarName());
            }
        }

        //  If stream is null try to get no-photo-image stream
        if (null === $stream) {
            if ($fs->has('nophoto.jpg')) {
                $stream = $fs->readStream('nophoto.jpg');
            }
            else {
                throw new NotFoundHttpException('Cant get image');
            }
        }

        $response = new StreamedResponse(function () use ($stream) {
            stream_copy_to_stream($stream, fopen('php://output', 'w'));
        });
        $response->headers->add([
            'Content-Type' => 'image/jpeg',
        ]);
        return $response;

    }

    public function user_save(Request $request)
    {
        $user_id = $request->get('user_id');

        $err = [];

        $fio = $request->get('user_fio');
        if (!$fio) {
            $err[] = "Не заполнено ФИО";
        }

        $login = $request->get('user_login');
        if (!$login) {
            $err[] = "Не заполнен Логин";
        }

        $pass1 = $request->get('pass1');
        $pass2 = $request->get('pass2');

        // If pass was entered - check that it equals to confirm
        if ($pass1 && $pass1 !== $pass2) {
            $err[] = "Пароль и подтверждение не совпадают";
        }

        if (sizeof($err)) {
            UIError($err);
        }

        /* @var $encoder Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface */
        $encoder = wf::$container->get('security.password_encoder');

        if ($user_id) {
            $user = $this->getEm()->find(models\User::class, $user_id);
            if (null === $user) {
                throw new NotFoundHttpException("User with id $user_id not found");
            }

            // Change password if defined
            if ($pass1) {
                $encoded = $encoder->encodePassword($user, $pass1);
                $user->setPass($encoded);
            }
        }
        else {
            // Password required for new user
            if (!$pass1) {
                UIError("Введите пароль для нового пользователя");
            }

            $user = new models\User();
            $encoded = $encoder->encodePassword($user, $pass1);
            $user->setPass($encoded);
        }

        $internalPhone = (int)$request->get('user_intphone');

        $avatar = $request->files->get('userphoto');
        if(null !== $avatar) {
            $image = new \Liip\ImagineBundle\Model\FileBinary($avatar->getRealPath(), $avatar->getMimeType(), 'jpeg');

            $filtered = $this->getContainer()->get('liip_imagine.filter.manager')
                ->applyFilter($image, 'user_photo_filter');

            $avatar->openFile('w')->fwrite($filtered->getContent());
        }

        $user->setLogin($login)
            ->setFio($fio)
            ->setPhones($request->get('user_phones'))
            ->setComment($request->get('cmm'))
            ->setInternalPhone($internalPhone ?: null)
            ->setAccessType($request->get('access_type'))
            ->setEmail($request->get('user_email'))
            ->setActive($request->get('active', false))
            ->setGender($request->get('gender'))
            ->setAvatar($avatar);

        $positionId = $request->get('user_dolzn');
        if ($positionId) {
            $position = $this->getEm()->getReference(models\JobPosition::class, $positionId);
            $user->setJobPosition($position);
        }

        $departmentId = $request->get('otdel_id');
        if ($departmentId) {
            $department = $this->getEm()->getReference(models\Department::class, $departmentId);
            $user->setDepartment($department);
        }

        $toJob = $request->get('user_tojob');
        if ($toJob) {
            $user->setToJob(new DateTime($toJob));
        } else {
            $user->setToJob(null);
        }

        $outJob = $request->get('user_outjob');
        if ($outJob) {
            $user->setOutJob(new DateTime($outJob));
        } else {
            $user->setOutJob(null);
        }

        $birthday = $request->get('user_birthday');
        if ($birthday) {
            $user->setBirthday(new DateTime($birthday));
        } else {
            $user->setBirthday(null);
        }

        $addtojob = ($_POST["user_tojob"] != "" ? "`tojob`=" . wf::$db->F(date("Y-m-d", strtotime($_POST["user_tojob"]))) . "," : "`tojob`=NULL,");
        if (!$_POST['active'] && wf::$user->isActive()) {
            $addoutjob = "`outjob`=" . wf::$db->F(date("Y-m-d")) . ",";
        }
        else {
            $addoutjob = ($_POST["user_outjob"] != "" ? "`outjob`=" . wf::$db->F(date("Y-m-d", strtotime($_POST["user_outjob"]))) . "," : "`outjob`=NULL,");
        }

        $validator = $this->getContainer()->get('validator');
        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            UIError('Такой логин уже есть в системе!');
        }

        $this->getEm()->persist($user);
        $this->getEm()->flush();

        if (sizeof($err)) {
            UIError($err);
        }

        $edited_user = new User($user_id);
        $this->getLogger('security')->info($this->getUserModel()->getFio() . ' отредактировал(а) профиль пользователю ' . $user->getFio() . ' (' . $user->getLogin() . ')', ['POST' => json_encode($_POST, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)]);

        redirect($this->getLink('user_view', "user_id={$user->getId()}"), "Учетная запись сохранена");
    }

    function user_del()
    {
        global $USER;

        $this->getLogger('security')->info($USER->getFio() . ' удалил(а) пользователя ' . date('Y-m-d H:i:s'), ['POST' => json_encode($_POST, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)]);

        $err = array();

        if (!($id = $_REQUEST['user_id'])) $err[] = "Не указан пользователь";

        $sql = "SELECT COUNT(*) FROM `task_users` WHERE `user_id`=" . wf::$db->F($id);
        if ($c = wf::$db->getField($sql)) $err[] = "Нельзя удалить пользоваетля, он участвует в $c задачах!";

        if (sizeof($err)) UIError($err);

        $sql = "DELETE FROM `list_empl_money_limits` WHERE `tech_id`=" . wf::$db->F($id);
        wf::$db->query($sql);

        $sql = "DELETE FROM `user_sc_access` WHERE `user_id`=" . wf::$db->F($id);
        wf::$db->query($sql);

        $sql = "DELETE FROM `user_access` WHERE `user_id`=" . wf::$db->F($id);
        wf::$db->query($sql);

        $sql = "DELETE FROM `users` WHERE `id`=" . wf::$db->F($id);
        wf::$db->query($sql);
        redirect($this->getLink());
    }

    function access_save(Request $request)
    {
        global $USER;

        if (User::ACCESS_WRITE != $USER->getAccess(USER_COPY_EDIT_SAVE_ACCESS)) {
            throw new \Symfony\Component\Security\Core\Exception\AccessDeniedException();
        }

        if ($user_id = $_POST['user_id']) {
            $sql = "DELETE FROM `user_access` WHERE `user_id`=" . wf::$db->F($user_id);
            wf::$db->query($sql);

            foreach ($_POST['access'] as $uid => $level) {
                $sql = "INSERT INTO `user_access` (`user_id`, `plugin_uid`, `access`) VALUES(" . wf::$db->F($user_id) . ", " . wf::$db->F($uid) . ", " . wf::$db->F($level) . ")";
                wf::$db->query($sql);
            }
        }

        $edited_user = new User($user_id);

        $t = $this;

        $access_map = [
            User::ACCESS_NONE => 'Нет доступа',
            User::ACCESS_READ => 'Чтение',
            User::ACCESS_WRITE => 'Запись',
        ];
        $new_rules = array_diff_assoc($request->get('access', []), $USER->getAccess());
        $new_rules_tmp = [];
        array_walk($new_rules, function ($v, &$k) use ($t, &$new_rules_tmp, $access_map) {
            $new_rules_tmp [$t->getPluginNameByAlias($k) . ' (' . $k . ')'] = $access_map[$v];
        });
        $new_rules = $new_rules_tmp;

        $this->getLogger('user_security')->info($USER->getFio() . ' изменил(а) права пользователю ' . $edited_user->getFio() . ' (' . $edited_user->getLogin() . ')', [
            'Изменения' => $new_rules,
        ]);

        redirect($this->getLink('user_view', "user_id={$_POST['user_id']}"), "Настройки доступа сохранены");
    }

    function otdel_save()
    {
        global $USER;

        $this->getLogger('security')->info($USER->getFio() . ' отредактировал(а) отдел ' . date('Y-m-d H:i:s'), ['POST' => json_encode($_POST, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)]);

        $err = array();

        if (!$_POST['otdel_name']) $err[] = "Не заполнено название";

        if (sizeof($err)) UIError($err);

        if ($_POST['otdel_id']) $sql = "UPDATE `user_otdels` SET `name`=" . wf::$db->F($_POST['otdel_name']) . ", `order`=" . wf::$db->F($_POST['otdel_order']) . ", `parent_id`=" . wf::$db->F($_POST['parent_id']) . ", `email`=" . wf::$db->F($_POST['otdel_email']) . " WHERE `id`=" . wf::$db->F($_POST['otdel_id']);
        else {
            $sql = "INSERT INTO `user_otdels` (`name`, `order`, `email`, `parent_id`) VALUES(" . wf::$db->F($_POST['otdel_name']) . ", " . wf::$db->F($_POST['otdel_order']) . ", " . wf::$db->F($_POST['otdel_email']) . ", " . wf::$db->F($_POST['parent_id']) . ")";
        }

        wf::$db->query($sql);
        if (wf::$db->errno()) UIError(wf::$db->error());

        if (!$_POST['otdel_id']) {
            $_POST['otdel_id'] = wf::$db->insert_id();
        }

        redirect($this->getLink(''), "Изменения сохранены");
    }

    function access_copy()
    {
        global $USER;

        if (User::ACCESS_WRITE != $USER->getAccess(USER_COPY_EDIT_SAVE_ACCESS)) {
            throw new \Symfony\Component\Security\Core\Exception\AccessDeniedException();
        }

        $source = $_POST["source_user_id"];
        $target = $_POST["user_id"];
        if (!$source || !$target) {
            UIError("Ошибка копирования прав доступа!");
            return false;
        }
        $s_rights = wf::$db->getCell2("SELECT `plugin_uid`, `access` FROM `user_access` WHERE `user_id`=" . wf::$db->F($source) . ";");
        if (wf::$db->errno()) UIError(wf::$db->error());
        if (count($s_rights)) {
            wf::$db->query("DELETE FROM `user_access` WHERE `user_id`=" . wf::$db->F($target) . ";");
            if (wf::$db->errno()) UIError(wf::$db->error());
            $sql = "INSERT INTO `user_access` (`user_id`, `plugin_uid`, `access`) VALUES ";
            $sql_add = false;
            foreach ($s_rights as $plug => $access) {
                $sql_add[] = "(" . wf::$db->F($target) . ", " . wf::$db->F($plug) . ", " . wf::$db->F($access) . ")";
            }
            if (count($sql_add)) {
                $sql .= implode(", ", $sql_add);
                wf::$db->query($sql);
                if (wf::$db->errno()) UIError(wf::$db->error());
                redirect($this->getLink('user_view', "user_id={$_POST['user_id']}"), "Копирование прав доступа завершено успешно!");

            } else {
                UIError("Ошибка копирования прав доступа! Обратитесь к разработчикам!");
            }
        } else {
            redirect($this->getLink('user_view', "user_id={$_POST['user_id']}"), "У выбранного пользователя отсутствует список прав. Копирование прав доступа отменено!");
        }
    }

    function setfa(Request $request)
    {
        $user = wf::$em->find(models\User::class, $request->get("targetuser_id"));
        if (null === $user) {
            throw new Exception('Cant find user to impersonate');
        }

        return new RedirectResponse('/?_to_be=' . $user->getUsername());
    }

}
