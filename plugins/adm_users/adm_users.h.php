<?php

/**
 * Plugin Header
 *
 * @author Gorserv - kblp
 * @copyright 2011
 */

use classes\User;

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Пользователи";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['struct'] = "Структура пользователей и подразделений";
    $PLUGINS[$plugin_uid]['events']['user_view'] = "Просмотр пользователя";
    $PLUGINS[$plugin_uid]['events']['sendNotification'] = "Пользователи: Отправка личных сообщений из профиля (sendNotification)";
    $PLUGINS[$plugin_uid]['events']['getLKWtypeList'] = "Список  видов работ по заявкам для отображения в ЛК";
    $PLUGINS[$plugin_uid]['events']['addLKWtypeID_ajax'] = "Добавление вида работ в ЛК";
    $PLUGINS[$plugin_uid]['events']['deleteLKWtypeID_ajax'] = "Удаление вида работ в ЛК";
    $PLUGINS[$plugin_uid]['events']['setfa'] = "Impersonate callback";
    $PLUGINS[$plugin_uid]['events']['roles'] = "Роли";
}

if(wf::$user->checkAccess($plugin_uid, User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['user_edit'] = "Редактирование пользователя";
    $PLUGINS[$plugin_uid]['events']['user_save'] = "Сохранение пользователя";
    $PLUGINS[$plugin_uid]['events']['user_del'] = "Удаление пользователя";
    $PLUGINS[$plugin_uid]['events']['user_del'] = "Удаление пользователя";
    $PLUGINS[$plugin_uid]['events']['otdel_edit'] = "Редактирование подразделения";
    $PLUGINS[$plugin_uid]['events']['otdel_save'] = "Сохранение подразделения";
    $PLUGINS[$plugin_uid]['events']['otdel_del'] = "Удаление подразделения";
    $PLUGINS[$plugin_uid]['events']['addtocontr_ajax'] = "Добавление пользователя в группу Контрагенты";
    $PLUGINS[$plugin_uid]['events']['removefromcontr_ajax'] = "Удаление пользователя из группы Контрагенты";
    $PLUGINS[$plugin_uid]['events']['togglecontractive_ajax'] = "Установка-снятие флага активности от Контрагента";
    $PLUGINS[$plugin_uid]['events']['toggletechextsc_ajax'] = "Установка-снятие флага выхода за пределы СЦ (по районам)";
    $PLUGINS[$plugin_uid]['events']['role_edit'] = "Редактирование роли";
    $PLUGINS[$plugin_uid]['events']['role_save'] = "Сохранение роли";
    $PLUGINS[$plugin_uid]['events']['role_del'] = "Удаление роли";

    $PLUGINS[$plugin_uid]['events']['save_sc_access_ajax'] = "Установка-снятие доступа на СЦ";

    $PLUGINS[$plugin_uid]['events']['access_save'] = "Сохранение прав доступа пользователя";
    $PLUGINS[$plugin_uid]['events']['access_edit'] = "Редактирование прав доступа пользователя";
    $PLUGINS[$plugin_uid]['events']['access_copy'] = "Копирование прав доступа пользователя";
}

define('USER_COPY_EDIT_SAVE_ACCESS', 'user_copy_edit_save_access');
$PLUGINS[USER_COPY_EDIT_SAVE_ACCESS]['name'] = "Пользователи: редактирование прав (редактирование, сохарнение, копирование)";
$PLUGINS[USER_COPY_EDIT_SAVE_ACCESS]['hidden'] = true;

$PLUGINS['lkaccess2allcomments']['name'] = "ЛК Контрагента: Просмотр комментариев по заявкам";
$PLUGINS['lkaccess2allcomments']['hidden'] = true;

$PLUGINS[$plugin_uid]['events']['user_avatar'] = 'Аватарка пользователя';

$plugin_uid = "loginatuser";
$PLUGINS[$plugin_uid]['name'] = "Доступ в активные учетные записи техников и рук-лей СЦ";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "adm_users.private_call";
$PLUGINS[$plugin_uid]['name'] = "Пользователи: Callback-звонок из профиля";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "adm_users.private_message";
$PLUGINS[$plugin_uid]['name'] = "Пользователи: Отправка личных сообщений из профиля";
$PLUGINS[$plugin_uid]['hidden'] = true;
