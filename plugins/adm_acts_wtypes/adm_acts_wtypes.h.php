<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2014
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Виды работ для актов по договорам";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Виды работ для актов по договорам";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактор вида работ";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление вида работ";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение вида работ";
    
    
}
?>