<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class adm_acts_wtypes_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getOptions($id = 0)
    {
        global $DB;
        $sql = "SELECT id, title FROM `list_acts_wtypes` WHERE 1 ORDER BY `title`;";
        $res = $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());
        return array2options($res, $id);
    }

    static function getById($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_acts_wtypes` WHERE `id`=" . $DB->F($id) . ";";
        $res = $DB->getField($sql);
        if ($DB->errno()) UIError($DB->error());
        return $res ? $res : false;
    }

    function viewlist()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        $sql = "SELECT w.id, w.title, (SELECT `title` FROM `list_skp_p_edizm` WHERE `id`=w.edizm) as edizmtitle FROM `list_acts_wtypes` as w WHERE 1;";
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());
        while (list($id, $title, $edizm) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->setVariable("VLI_EDIZM", $edizm);
            $tpl->parse("vl_item");
        }
        $DB->free();
        UIHeader($tpl);
        $tpl->show();
    }

    function edit() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        if ($id = $_REQUEST['id']) {
            $sql = "SELECT * FROM `list_acts_wtypes` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Запись с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);
            $tpl->setVariable("ID", $result["id"]);
            $tpl->setVariable("TITLE", $result["title"]);
        //    $tpl->setVariable("OPTIONS_EDIZM", adm_skp_edizm_plugin::getOptList($result["edizm"]));
        }
        $tpl->setVariable("OPTIONS_EDIZM", adm_skp_edizm_plugin::getOptList($result["edizm"]));

        UIHeader($tpl);
        $tpl->show();

    }

    function save() {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = array();

        if(!$_POST['title']) $err[] = "Не заполнено название типа проекта";

        if(sizeof($err)) UIError($err);
        if ($_POST["id"]) {
            $sql = "UPDATE `list_acts_wtypes` SET `edizm`=".$DB->F($_POST["edizm"]).", `title`=".$DB->F($_POST['title'])." WHERE `id`=".$DB->F($_POST["id"]).";";
        } else {
            $sql = "INSERT INTO `list_acts_wtypes` (`edizm`, `title`) VALUES(".$DB->F($_POST['edizm']).",".$DB->F($_POST['title']).")";
        }
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());

        if(!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Запись сохранена. ID: ".$_POST['id'] );
    }

    function delete() {
        global $DB;
        if (!($id = $_REQUEST['id'])) $err[] = "Не указан идентификатор записи!";
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($id)) UIError("Выбранная запись связана с другими записями в системе. Удаление невозможно.");
        $DB->query("DELETE FROM `list_acts_wtypes` WHERE `id`='".$_REQUEST['id']."';");
        redirect($this->getLink(),"Запись успешно удалена.");

    }

    function isBusy($id)
    {
        global $DB;
        $m = $DB->getField("SELECT count(`act_id`) FROM `list_acts_wtype_data` WHERE `wtype_id`=" . $DB->F($id) . ";");
        if ($m > 0) return true; else return false;
    }


}
?>
