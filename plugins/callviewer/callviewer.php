<?php

use classes\HTML_Template_IT;
use classes\Plugin;
use League\Flysystem\FilesystemInterface;
use models\TicketsCalls;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class callviewer_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__file__, '.php'));
    }


    function viewlist()
    {
        global $DB, $USER;
        if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
            UIError("В доступе отказано!");
        }
        $sort_ids = array(
            1 => "Дата звонка",
            2 => "ID заявки",
            3 => "ID записи",
            4 => "Тип заявки",
            5 => "Результат звонка"
        );
        $sort_sql = array(
            1 => "cr.calldate",
            2 => "cr.task_id",
            3 => "cr.id",
            4 => "cr.plugin_uid",
            5 => "cr.asteriskstatus"
        );

        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );

        $rpp = array(
            20 => "20",
            50 => "50",
        );

        $tasktypes = array("" => "-- все --", "services" => "СКП", "connections" => "Подключение", "accidents" => "ТТ");
        $callstatus = array("" => "-- все --", "ANSWERED" => "Ответ", "NO ANSWER" => "Нет ответа", "BUSY" => "Занято");
        $callstatusColors = array("ANSWERED" => "#a8c716", "NO ANSWER" => "#FF5500", "BUSY" => "#FFFCA3");
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");

        if ($USER->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }
        if (!isset($_REQUEST["filter_rpp"]))
            $_REQUEST["filter_rpp"] = 20;
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ? $_REQUEST['filter_sort_order'] : key($sort_order));
        if (!isset($_REQUEST["filter_rpp"])) $_REQUEST["filter_rpp"] = $USER->getRpp();

        $_REQUEST["filter_id"] = $this->getCookie('filter_id', $_REQUEST['filter_id']);

        $_REQUEST["filter_task_id"] = $this->getCookie('filter_task_id', $_REQUEST['filter_task_id']);
        $_REQUEST["filter_source"] = $this->getCookie('filter_source', $_REQUEST['filter_source']);
        $_REQUEST["filter_dest"] = $this->getCookie('filter_dest', $_REQUEST['filter_dest']);
        $_REQUEST["filter_callresult"] = $this->getCookie('filter_callresult', $_REQUEST['filter_callresult']);
        $_REQUEST["techname"] = $this->getCookie('techname', $_REQUEST['techname']);
        $_REQUEST["filter_empl_id"] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);
        $_REQUEST["filter_tasktype_id"] = $this->getCookie('filter_tasktype_id', $_REQUEST['filter_tasktype_id']);
        $_REQUEST["filter_contr_id"] = $this->getCookie('filter_contr_id', $_REQUEST['filter_contr_id']);
        $_REQUEST['filter_ti_date_from'] = $this->getCookie('filter_ti_date_from', $_REQUEST['filter_ti_date_from']);
        $_REQUEST['filter_ti_date_to'] = $this->getCookie('filter_ti_date_to', $_REQUEST['filter_ti_date_to']);

        if (!$_REQUEST['filter_ti_date_from']) $_REQUEST['filter_ti_date_from'] = date("d.m.Y");
        if (!$_REQUEST['filter_ti_date_to']) $_REQUEST['filter_ti_date_to'] = date("d.m.Y");

        $tpl->setVariable("FILTER_ID", $_REQUEST["filter_id"]);
        $tpl->setVariable('FILTER_TASK_ID', $_REQUEST['filter_task_id']);
        $tpl->setVariable('FILTER_SOURCE', $_REQUEST['filter_source']);
        $tpl->setVariable('FILTER_DEST', $_REQUEST['filter_dest']);
        $tpl->setVariable('FILTER_TASKTYPE_OPTIONS', array2options($tasktypes, $_REQUEST['filter_tasktype_id']));
        $tpl->setVariable('FILTER_CALLRESULT_OPTIONS', array2options($callstatus, $_REQUEST['filter_callresult']));
        $tpl->setVariable('FILTER_EMPL_NAME', $_REQUEST['techname']);
        $tpl->setVariable('FILTER_EMPL_ID', $_REQUEST['filter_empl_id']);
        $tpl->setVariable("FILTER_CONTR_OPTIONS", kontr_plugin::getOptList($_REQUEST["filter_contr_id"]));
        $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
        $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
        $tpl->setVariable('FILTER_ROWS_PER_PAGE', array2options($rpp, $_REQUEST['filter_rpp']));
        $tpl->setVariable("FILTER_TI_DATE_FROM", $_REQUEST['filter_ti_date_from'] ? $_REQUEST['filter_ti_date_from'] : "");
        $tpl->setVariable("FILTER_TI_DATE_TO", $_REQUEST['filter_ti_date_to'] ? $_REQUEST['filter_ti_date_to'] : "");

        //$filter[] = "cr.status=" . $DB->F(1) . "";
        if ($_REQUEST["filter_id"] != "") $filter[] = "cr.id=" . $DB->F($_REQUEST["filter_id"]);
        if ($_REQUEST["filter_task_id"] != "") $filter[] = "cr.task_id=" . $DB->F($_REQUEST["filter_task_id"]);
        if ($_REQUEST["filter_source"] != "") $filter[] = "cr.callfrom LIKE " . $DB->F("%" . $_REQUEST["filter_source"] . "%");
        if ($_REQUEST["filter_dest"] != "") $filter[] = "cr.callto LIKE " . $DB->F("%" . $_REQUEST["filter_dest"] . "%");
        if ($_REQUEST["filter_tasktype_id"] != "") $filter[] = "cr.plugin_uid=" . $DB->F($_REQUEST["filter_tasktype_id"]);
        if ($_REQUEST["filter_callresult"] != "") $filter[] = "cr.asteriskstatus=" . $DB->F($_REQUEST["filter_callresult"]);
        if ($_REQUEST["filter_empl_id"] != "") $filter[] = "cr.user_id=" . $DB->F($_REQUEST["filter_empl_id"]);

        $filter[] = "cr.calldate between \"" . (new DateTIme($_REQUEST['filter_ti_date_from']))->format('Y-m-d 00:00:00') . "\" and \"" . (new DateTime($_REQUEST['filter_ti_date_to']))->format('Y-m-d 23:59:59') . "\"";

        $filter = implode(" AND ", $filter);
        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];
        if ($_REQUEST["filter_contr_id"] > 0) {
            $sql = "SELECT SQL_CALC_FOUND_ROWS
                    cr.id, cr.*, t.cnt_id as contr FROM `tickets_calls` AS cr JOIN `tickets` AS t ON (t.task_id=cr.task_id AND t.cnt_id=" . $DB->F($_REQUEST["filter_contr_id"]) . ")
                    WHERE $filter
                    and case when ifnull(t.sc_id,0) = 0 then 1
                           when exists (select * from user_sc_access usa where usa.user_id = " . $USER->getId() . " and usa.sc_id = t.sc_id) then 1
                           else 0
                       end = 1
                    ORDER BY $order
                    LIMIT {$_REQUEST['filter_start']}," . intval($_REQUEST["filter_rpp"]);

        } else {
            $sql = "SELECT SQL_CALC_FOUND_ROWS
                    cr.id, cr.* FROM `tickets_calls` AS cr JOIN `tickets` AS t ON (t.task_id=cr.task_id )
                    WHERE $filter
                    and case when ifnull(t.sc_id,0) = 0 then 1
                           when exists (select * from user_sc_access usa where usa.user_id = " . $USER->getId() . " and usa.sc_id = t.sc_id) then 1
                           else 0
                       end = 1
                    ORDER BY $order
                    LIMIT {$_REQUEST['filter_start']}," . intval($_REQUEST["filter_rpp"]);

        }
        $DB->query($sql);
        $total = $DB->getFoundRows();
        if ($DB->errno())
            UIError($DB->error() . $sql);
        if (!$DB->num_rows()) {
            $tpl->touchBlock("norecords");
        }
        while ($calls = $DB->fetch(true)) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $calls["id"]);
            $tpl->setVariable("VLI_DATETIME", date("d.m.Y H:i:s", strtotime($calls["calldate"])));
            $tpl->setVariable("VLI_TASKID", $calls["task_id"]);
            if ($calls["task_id"] == "0") {
                $tpl->setVariable("HIDETASK_ID", "hidden");
            }
            $tpl->setVariable("VLI_TASKTYPE", $tasktypes[$calls["plugin_uid"]]);
            $tpl->setVariable("VLI_SOURCE", $calls["callfrom"]);
            $tpl->setVariable("VLI_DEST", $calls["callto"]);
            $tpl->setVariable("VLI_TASKHREF", link2($calls["plugin_uid"] . "/viewticket?task_id=" . $calls["task_id"], false));
            $tpl->setVariable("VLI_STATUS", $callstatus[$calls["asteriskstatus"]] ? $callstatus[$calls["asteriskstatus"]] : " не определен ");
            if ($callstatusColors[$calls["asteriskstatus"]])
                $tpl->setVariable("VLI_STATUSCOLOR", " style='background-color:" . $callstatusColors[$calls["asteriskstatus"]] . "' ");
            //$tpl->setVariable("VLI_AUDIO", "<audio src=\"/callviewer/listenfile?id=".$calls["id"]."\"></audio>");
            $user_src = adm_users_plugin::getUser($calls["user_id"]);
            $tpl->setVariable("VLI_USER", "<a target=\"_blank\" href=\"/adm_users/user_view/?user_id=" . $user_src["id"] . "\">" . $user_src["fio"] . "</a>");
            if (preg_match('/Trident/i', $_SERVER['HTTP_USER_AGENT']) || preg_match('/msie/i', $_SERVER['HTTP_USER_AGENT'])) {
                $tpl->setCurrentBlock("is-audio-ie");
                $tpl->setVariable("AUDIO__ID", $calls["asteriskfile"]);
                $tpl->setVariable("AUDIO__SRC", "/callviewer/get_record/?id=" . $calls["id"]);
                $tpl->parse("is-audio-ie");
            } else {
                $tpl->setCurrentBlock("is-audio");
                $tpl->setVariable("AUDIO_ID", $calls["asteriskfile"]);
                $tpl->setVariable("AUDIO_SRC", "/callviewer/get_record/?id=" . $calls["id"]);
                $tpl->parse("is-audio");
            }
            //$head = array_change_key_case(get_headers("https://work.gorserv.ru/gss_files/callrecords/".$calls["asteriskfile"], TRUE));
            $filesize = number_format(filesize(getcfg('files_path') . "/callrecords/" . $calls["asteriskfile"]) / 1024, 2, ".", " ");

            $tpl->setVariable("VLI_FILESIZE", $filesize);
            $tpl->setVariable("AUDIO___SRC", "/callviewer/get_record/?download=1&id=" . $calls["id"]);
            $tpl->parse("vl_item");
        }
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"],
            $total, "#start-%s"));
        UIHeader($tpl);
        $tpl->show();

    }

    static function refresh_ticketcalls_ajax()
    {
        global $DB, $USER;
        $task_id = $_REQUEST['task_id'];
        if (!$task_id) {
            $ret["error"] = "Не указа ID заявки";
            echo json_encode($ret);
            return false;
        }
        /*$sql = "UPDATE `tickets_calls` SET `status`='0' WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);
            return false;
        }
        $DB->free();
        */
        // refreshing ....
        $sql = "SELECT * FROM `tickets_calls` WHERE `task_id`=" . $DB->F($task_id) . " AND `status`!=1;";
        $DB->query($sql);
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                //print_r($r);
                $r["calldate"] = implode(",", explode(" ", $r["calldate"]));
                if ($r["techphone"] != "")
                    $url = getcfg('asterisk_host') . "aster_db.php?from=4957390940&to=" . $r["techphone"] . "&cdate=" . $r["calldate"] . '&key=' . $r['id'];
                else
                    $url = getcfg('asterisk_host') . "aster_db.php?from=" . $r["callfrom"] . "&to=" . $r["callto"] . "&cdate=" . $r["calldate"] . '&key=' . $r['id'];
                //echo "URL: ".$url."\r\n";
                ob_start();
                $cURL = curl_init();

                curl_setopt($cURL, CURLOPT_URL, $url);
                curl_setopt($cURL, CURLOPT_HTTPGET, true);
                curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Accept: application/json'
                ));
                $result = @curl_exec($cURL);
                @curl_close($cURL);
                $r1 = ob_get_contents();
                ob_end_clean();
                $r1 = json_decode($r1, true);
                if ($r1["ok"] == "ok" && $r1["file"] == "ok" && $r1["pathtofile"] != "") {
                    $sql = "UPDATE `tickets_calls` SET
                            `asteriskstatus`=" . $DB->F($r1["disposition"]) . ",
                            `asteriskdate`=" . $DB->F($r1["calldate"]) . ",
                            `asteriskfile`=" . $DB->F($r1["recordingfile"]) . ",
                            `asteriskuid`=" . $DB->F($r1["uniqueid"]) . ",
                            `asteriskpathtofile`=" . $DB->F($r1["pathtofile"]) . "
                            WHERE `id`=" . $DB->F($r["id"]) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) {
                    } else {
                        $targetLink = getcfg('asterisk_host') . "aster_get.php?targetpath=" . base64_encode($r1["pathtofile"]) . "&target=" . base64_encode($r1["recordingfile"]);
                        $file = file_get_contents($targetLink);
                        if (false !== file_put_contents(getcfg('files_path') . "/callrecords/" . $r1["recordingfile"], $file)) {
                            $sql = "UPDATE `tickets_calls` SET `status`=" . $DB->F(1) . " WHERE `id`=" . $DB->F($r["id"]) . ";";
                            $DB->query($sql);
                            $DB->free();
                        }

                    }
                    $DB->free();
                }
            }
        }
        // end of refreshing...
        $DB->free();
        $callstatus = array("" => "-- все --", "ANSWERED" => "Ответ", "NO ANSWER" => "Нет ответа", "BUSY" => "Занято");
        $callstatusColors = array("ANSWERED" => "#a8c716", "NO ANSWER" => "#FF5500", "BUSY" => "#FFFCA3");

        $filter[] = "cr.status=" . $DB->F(1) . "";
        $filter[] = "cr.task_id=" . $DB->F($task_id);
        $filter = implode(" AND ", $filter);

        $sql = "SELECT cr.* FROM `tickets_calls` AS cr WHERE $filter";
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = "MySQL: " . $DB->error();
            echo json_encode($ret);
            return false;
        }
        $tpl = new HTML_Template_IT(path2("plugins/callviewer"));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/callrec_task.tmpl.htm");
        else
            $tpl->loadTemplatefile("callrec_task.tmpl.htm");

        if ($DB->num_rows()) {

            while ($calls = $DB->fetch(true)) {
                $tpl->setCurrentBlock("vl_item");
                $tpl->setVariable("VLI_ID", $calls["id"]);
                $tpl->setVariable("VLI_TASKID", $calls["id"]);
                $tpl->setVariable("VLI_DATETIME", date("d.m.Y H:i:s", strtotime($calls["calldate"])));
                $tpl->setVariable("VLI_TASKID", $calls["task_id"]);
                $tpl->setVariable("VLI_TASKTYPE", $tasktypes[$calls["plugin_uid"]]);
                $tpl->setVariable("VLI_SOURCE", $calls["callfrom"]);
                $tpl->setVariable("VLI_DEST", $calls["callto"]);
                $tpl->setVariable("VLI_STATUS", $callstatus[$calls["asteriskstatus"]] ? $callstatus[$calls["asteriskstatus"]] : " не определен ");
                if ($callstatusColors[$calls["asteriskstatus"]])
                    $tpl->setVariable("VLI_STATUSCOLOR", " style='background-color:" . $callstatusColors[$calls["asteriskstatus"]] . "' ");

                $user_src = adm_users_plugin::getUser($calls["user_id"]);
                $tpl->setVariable("VLI_USER", "<a target=\"_blank\" href=\"/adm_users/user_view/?user_id=" . $user_src["id"] . "\">" . $user_src["fio"] . "</a>");
                if (preg_match('/Trident/i', $_SERVER['HTTP_USER_AGENT']) || preg_match('/msie/i', $_SERVER['HTTP_USER_AGENT'])) {
                    $tpl->setCurrentBlock("is-audio-ie");
                    $tpl->setVariable("AUDIO__ID", $calls["asteriskfile"]);
                    $tpl->setVariable("AUDIO__SRC", "/callviewer/get_record/?id=" . $calls["id"]);
                    $tpl->parse("is-audio-ie");
                } else {
                    $tpl->setCurrentBlock("is-audio");
                    $tpl->setVariable("AUDIO_ID", $calls["asteriskfile"]);
                    $tpl->setVariable("AUDIO_SRC", "/callviewer/get_record/?id=" . $calls["id"]);
                    $tpl->parse("is-audio");
                }
                $filesize = number_format(filesize(getcfg('files_path') . "/callrecords/" . $calls["asteriskfile"]) / 1024, 2, ".", " ");

                $tpl->setVariable("VLI_FILESIZE", $filesize);
                $tpl->setVariable("AUDIO___SRC", "/callviewer/get_record/?download=1&id=" . $calls["id"]);
                $tpl->parse("vl_item");
            }
        } else {
            $tpl->setCurrentBlock("no-items");
            $tpl->setVariable("NR", "&nbsp;");
            $tpl->parse("no-items");
        }
        $ret["tpl"] = $tpl->get();
        $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;
    }

    static function getrecordsbytask_id()
    {
        global $DB, $USER;
        $callstatus = array("" => "-- все --", "ANSWERED" => "Ответ", "NO ANSWER" => "Нет ответа", "BUSY" => "Занято");
        $callstatusColors = array("ANSWERED" => "#a8c716", "NO ANSWER" => "#FF5500", "BUSY" => "#FFFCA3");

        $task_id = $_REQUEST['task_id'];
        if (!$task_id) {
            $ret["error"] = "Не указан ИД заявки";
            echo json_encode($ret);
            return false;
        }
        $filter[] = "cr.status=" . $DB->F(1) . "";
        $filter[] = "cr.task_id=" . $DB->F($task_id);
        $filter = implode(" AND ", $filter);

        $sql = "SELECT cr.* FROM `tickets_calls` AS cr WHERE $filter";
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = "MySQL: " . $DB->error();
            echo json_encode($ret);
            return false;
        }
        $tpl = new HTML_Template_IT(path2("plugins/callviewer"));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/callrec_task.tmpl.htm");
        else
            $tpl->loadTemplatefile("callrec_task.tmpl.htm");

        if ($DB->num_rows()) {

            while ($calls = $DB->fetch(true)) {
                $tpl->setCurrentBlock("vl_item");
                $tpl->setVariable("VLI_ID", $calls["id"]);
                $tpl->setVariable("VLI_TASKID", $calls["id"]);
                $tpl->setVariable("VLI_DATETIME", date("d.m.Y H:i:s", strtotime($calls["calldate"])));
                $tpl->setVariable("VLI_TASKID", $calls["task_id"]);
                $tpl->setVariable("VLI_TASKTYPE", $tasktypes[$calls["plugin_uid"]]);
                $tpl->setVariable("VLI_SOURCE", $calls["callfrom"]);
                $tpl->setVariable("VLI_DEST", $calls["callto"]);
                $tpl->setVariable("VLI_STATUS", $callstatus[$calls["asteriskstatus"]] ? $callstatus[$calls["asteriskstatus"]] : " не определен ");
                if ($callstatusColors[$calls["asteriskstatus"]])
                    $tpl->setVariable("VLI_STATUSCOLOR", " style='background-color:" . $callstatusColors[$calls["asteriskstatus"]] . "' ");
                //$tpl->setVariable("VLI_AUDIO", "<audio src=\"/callviewer/listenfile?id=".$calls["id"]."\"></audio>");
                $user_src = adm_users_plugin::getUser($calls["user_id"]);
                $tpl->setVariable("VLI_USER", "<a target=\"_blank\" href=\"/adm_users/user_view/?user_id=" . $user_src["id"] . "\">" . $user_src["fio"] . "</a>");
                if (preg_match('/Trident/i', $_SERVER['HTTP_USER_AGENT']) || preg_match('/msie/i', $_SERVER['HTTP_USER_AGENT'])) {
                    $tpl->setCurrentBlock("is-audio-ie");
                    $tpl->setVariable("AUDIO__ID", $calls["asteriskfile"]);
                    $tpl->setVariable("AUDIO__SRC", "/callviewer/get_record/?id=" . $calls["id"]);
                    $tpl->parse("is-audio-ie");
                } else {
                    $tpl->setCurrentBlock("is-audio");
                    $tpl->setVariable("AUDIO_ID", $calls["asteriskfile"]);
                    $tpl->setVariable("AUDIO_SRC", "/callviewer/get_record/?id=" . $calls["id"]);
                    $tpl->parse("is-audio");
                }
                //$head = array_change_key_case(get_headers("https://work.gorserv.ru/gss_files/callrecords/".$calls["asteriskfile"], TRUE));
                $filesize = number_format(filesize(getcfg('files_path') . "/callrecords/" . $calls["asteriskfile"]) / 1024, 2, ".", " ");

                $tpl->setVariable("VLI_FILESIZE", $filesize);
                $tpl->setVariable("AUDIO__SRC", "/callviewer/get_record/?id=" . $calls["id"]);
                $tpl->setVariable("AUDIO___SRC", "/callviewer/get_record/?download=1&id=" . $calls["id"]);
                $tpl->parse("vl_item");
            }
        } else {
            $tpl->setCurrentBlock("no-items");
            $tpl->setVariable("NR", "&nbsp;");
            $tpl->parse("no-items");
        }


        $ret["tpl"] = $tpl->get();
        $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;
    }

    public function get_record(Request $request)
    {
        $id = $request->get('id');
        if (empty($id)) {
            throw new BadRequestHttpException('Undefined record id');
        }

        /* @var $record TicketsCalls */
        $record = $this->getEm()->find(TicketsCalls::class, $id);
        if (null === $record) {
            throw new NotFoundHttpException("Call with id $id not found");
        }

        /* @var $fs FilesystemInterface */
        $fs = $this->getContainer()->get('oneup_flysystem.call_records_filesystem');
        $filename = $record->getAsteriskFile();

        if (!$fs->has($filename)) {
            throw new NotFoundHttpException("File not found for call $id");
        }
        $size = $fs->getSize($filename);
        $stream = $fs->readStream($filename);

        $response = new StreamedResponse(function () use ($stream) {
            stream_copy_to_stream($stream, fopen('php://output', 'w'));
        });
        $response->headers->add([
            'Content-Type' => 'audio/x-wav',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $size,
        ]);

        if ($request->get('download')) {
            $disposition = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $filename
            );

            $response->headers->set('Content-Disposition', $disposition);
        }

        return $response;
    }

}
