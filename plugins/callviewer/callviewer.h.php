<?php

/**
 *
 * Plugin Header
 *
 * @author kblp
 * @copyright 2013
 *
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Записи звонков";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Журнал записей телефонных переговоров";
    $PLUGINS[$plugin_uid]['events']['getrecordsbytask_id'] = "Записи переговоров по заявке";
    $PLUGINS[$plugin_uid]['events']['refresh_ticketcalls_ajax'] = "Принудительное получение записей по заявке";
    $PLUGINS[$plugin_uid]['events']['get_record'] = "Чтение файла";
}

/**
 * Костыль для давания Пользователю Партнёрского ЛК доступ к записям звонков по своим заявкам
 */
if(wf::$user->checkAccess('plk.connection.view.calls')) {
    $PLUGINS[$plugin_uid]['events']['getrecordsbytask_id'] = "Записи переговоров по заявке";
    $PLUGINS[$plugin_uid]['events']['get_record'] = "Чтение файла";
}
