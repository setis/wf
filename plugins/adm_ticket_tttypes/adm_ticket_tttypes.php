<?php

/**
 * Plugin Implementation
 * 
 * @author kblp
 * @copyright 2015
 */

use classes\HTML_Template_IT;
use classes\Plugin;



/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 * 
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
 
class adm_ticket_tttypes_plugin extends Plugin
{    
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    static function getTypeOptions($sel_id = 0)
    {
        global $DB;
        return "<option value=''>-- выберите значение --</option>".array2options($DB->getCell2("SELECT `id`, `title` FROM `tt_types` ORDER BY `title`"), $sel_id);
    }
    
    static function getTypeOptions1($sel_id = 0)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`, `title` FROM `tt_types` WHERE `title`!='Не выбирать' ORDER BY `title`"), $sel_id);
    }
    
    
    static function getSubTypeOptions($type = 0, $sel_id = 0)
    {
        global $DB;
        if ($type) {
            $sql_add = " WHERE `type_id`=".$DB->F($type)." ";
        } else {
            return "<option value=''>-- выберите значение --</option>";
        }
        return "<option value=''>-- выберите значение --</option>".array2options($DB->getCell2("SELECT `id`, `title` FROM `tt_subtypes` $sql_add ORDER BY `title`"), $sel_id);
    }
    
    static function getSubTypeOptions1($type = 0, $sel_id = 0)
    {
        global $DB;
        if ($type) {
            $sql_add = " WHERE `type_id`=".$DB->F($type)." ";
        } else {
            return "";
        }
        return "".array2options($DB->getCell2("SELECT `id`, `title` FROM `tt_subtypes` $sql_add AND `title`!='Не выбирать' ORDER BY `title`"), $sel_id);
    }
    
    static function getTypeOptions_ajax()
    {
        global $DB;
        $ret["tpl"] = "<option value=''>-- выберите значение --</option>".array2options($DB->getCell2("SELECT `id`, `title` FROM `tt_types` WHERE `title`!='Не выбирать' ORDER BY `title`"), $sel_id);
        echo json_encode($ret, true);
        die();
    }
    
    static function getSubTypeOptions_ajax()
    {
        global $DB;
        $type = $_REQUEST["type_id"];
        if ($type) {
            $sql_add = " WHERE `type_id`=".$DB->F($type)." ";
            $ret["tpl"] = "<option value=''>-- выберите значение --</option>".array2options($DB->getCell2("SELECT `id`, `title` FROM `tt_subtypes` $sql_add ORDER BY `title`"), $sel_id);
        
        } else {
            $ret["tpl"] = "<option value=''>-- выберите значение --</option>";
        }
        echo json_encode($ret, true);
        die();
    }
    
    static function getTypeOptions1_ajax()
    {
        global $DB;
        $ret["tpl"] = array2options($DB->getCell2("SELECT `id`, `title` FROM `tt_types` WHERE `title`!='Не выбирать' ORDER BY `title`"), $sel_id);
        echo json_encode($ret, true);
        die();
    }
    
    static function getSubTypeOptions1_ajax()
    {
        global $DB;
        $type = $_REQUEST["type_id"];
        if ($type) {
            $sql_add = " WHERE `type_id`=".$DB->F($type)." ";
            $ret["tpl"] = array2options($DB->getCell2("SELECT `id`, `title` FROM `tt_subtypes` $sql_add ORDER BY `title`"), $sel_id);
        
        } else {
            $ret["tpl"] = "<option value=''>-- выберите значение --</option>";
        }
        echo json_encode($ret, true);
        die();
    }
    
    static function getType($id) {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT `title` FROM `tt_types` WHERE `id`=".$DB->F($id).";";
        return $DB->getField($sql);
    }

    static function getSubType($id) {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT `title` FROM `tt_subtypes` WHERE `id`=".$DB->F($id).";";
        return $DB->getField($sql);
    }
    
    static function getTypeId($id) {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT `type_id` FROM `tt_subtypes` WHERE `id`=".$DB->F($id).";";
        return $DB->getField($sql);
    }

    function struct()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        if ($USER->getTemplate() != "default") 
            $tpl->loadTemplatefile($USER->getTemplate()."/struct.tmpl.htm");
        else 
            $tpl->loadTemplatefile("struct.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $sql = "SELECT `id`, `title` FROM `tt_types`".$pfilter." ORDER BY `title`";
        //die($sql);
        $isRec = false;
        foreach($types = $DB->getCell2($sql) as $type_id=>$type_name) {
            $isRec = true;
            $tpl->setCurrentBlock('type');
            $tpl->setVariable('TYPE_ID', $type_id);
            $tpl->setVariable('TYPE_NAME', $type_name);
            $sql = "SELECT `id`, `title` FROM `tt_subtypes` WHERE `type_id`=".$DB->F($type_id)." ORDER BY `title`";
            $DB->query($sql);
            if ($DB->num_rows()) {
                while(list($subtype_id, $subtype_name) = $DB->fetch()) {
                    $tpl->setCurrentBlock('subtype');
                    $tpl->setVariable('SUBTYPE_ID', $subtype_id);
                    $tpl->setVariable('SUBTYPE_TITLE', $subtype_name);
                    $tpl->parse('subtype');
                }
            } else {
                //$tpl->setVariable("SUBTYPES_HIDE", "style='display:none;'");
            }
            $DB->free();
            
            $tpl->parse('type');
        }
        if (!$isRec) {
            $tpl->touchBlock("norecords");
        }
        
        UIHeader($tpl);
        $tpl->show();
    }
    
    function type_save()
    {
        global $DB, $USER;
        
        //Debugger::dump($_POST, true);
        
        $err = array();
        
        if(!$_POST['type_name']) $err[] = "Не заполнено название";
       
        if(sizeof($err)) UIError($err);
        
        if($_POST['type_id']) $sql = "UPDATE `tt_types` SET `title`=".$DB->F($_POST['type_name'])." WHERE `id`=".$DB->F($_POST['type_id']);
        else {
            $sql = "INSERT INTO `tt_types` (`title`) VALUES (".$DB->F($_POST['type_name']).");";
        }
        
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());
        
        if(!$_POST['type_id']) {
            $_POST['type_id'] = $DB->insert_id();
        }
        
        redirect($this->getLink(''), "Изменения сохранены");
    }
    
    
    function type_edit()
    {
        global $DB, $USER;
        
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("otdel.tmpl.htm");
        if ($USER->getTemplate() != "default") 
            $tpl->loadTemplatefile($USER->getTemplate()."/type.tmpl.htm");
        else 
            $tpl->loadTemplatefile("type.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        
        if($type_id = $_REQUEST['type_id']) {
            $type = $DB->getRow("SELECT * FROM `tt_types` WHERE `id`=".$DB->F($type_id), true);
            $tpl->setVariable('ID', $type['id']);
            $tpl->setVariable('TITLE', $type['title']);
        }
        UIHeader($tpl);
        $tpl->show();
    }
    
    function subtype_edit()
    {
        global $DB, $USER, $PLUGINS;
        
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("user.tmpl.htm");
        if ($USER->getTemplate() != "default") 
            $tpl->loadTemplatefile($USER->getTemplate()."/subtype.tmpl.htm");
        else 
            $tpl->loadTemplatefile("subtype.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        if($subtype_id = $_REQUEST['subtype_id']) {
            $tpl->setVariable('ID', $subtype_id);
            $tpl->setVariable('SUBTYPE_TITLE', self::getSubType($subtype_id));
            $tpl->setVariable('TYPENAME', self::getType(self::getTypeId($subtype_id)));
            $tpl->setVariable('TYPE_ID', self::getTypeId($subtype_id));
        } else {
            if ($type_id = $_GET["type_id"]) {
                $tpl->setVariable('TYPE_ID', $type_id);               
                $tpl->setVariable('TYPENAME', self::getType($type_id));
                 
            } else {
                UIError("Не указан идентификатор типа заявок ТТ");
            }
        }        
        
        UIHeader($tpl);
        $tpl->show();
    }
    
    function subtype_save()
    {
        global $DB, $USER;
        
        //Debugger::dump($_POST, true);
        
        $err = array();
        
        if(!$_POST['subtype_title']) $err[] = "Не заполнено название подтипа";
        if(!$_POST['type_id']) $err[] = "Не указан идентификатор типа заявки";
        
        if(sizeof($err)) UIError($err);
        
        if($_POST['subtype_id']) $sql = "UPDATE `tt_subtypes` SET `title`=".$DB->F($_POST['subtype_title'])." WHERE `id`=".$DB->F($_POST['subtype_id']);
        else {
            $sql = "INSERT INTO `tt_subtypes` (`type_id`, `title`) VALUES(".$DB->F($_POST['type_id']).", ".$DB->F($_POST['subtype_title']).")";
        }
        
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());
        
         
        redirect($this->getLink('struct', "Запись сохранена"));
    }
    
    function subtype_del()
    {
        global $DB, $USER;
        //Debugger::dump($_POST, true);
        $err = array();
        if(!($id = $_REQUEST['subtype_id'])) $err[] = "Не указан ИД подтипа заявки";        
        $sql = "SELECT COUNT(`task_id`) FROM `tickets` WHERE `ttsubtype_id`=".$DB->F($id);
        if($c = $DB->getField($sql)) $err[] = "Нельзя удалить подтип заявки ТТ, он участвует в $c задачах!";
                
        if(sizeof($err)) UIError($err);
        
        $sql = "DELETE FROM `tt_subtypes` WHERE `id`=".$DB->F($id);      
        $DB->query($sql);   
        redirect($this->getLink(), "Подтип успешно удален!");
    }
    
    function type_del()
    {
        global $DB, $USER;
        //Debugger::dump($_POST, true);
        $err = array();
        if(!($id = $_REQUEST['type_id'])) $err[] = "Не указан ИД типа заявки";        
        $sql = "SELECT COUNT(`task_id`) FROM `tickets` WHERE `tttype_id`=".$DB->F($id);
        if($c = $DB->getField($sql)) $err[] = "Нельзя удалить тип заявки ТТ, он участвует в $c задачах!";
        $sql = "SELECT COUNT(`id`) FROM `tt_subtypes` WHERE `type_id`=".$DB->F($id);
        if($c = $DB->getField($sql)) $err[] = "Нельзя удалить тип заявки ГП, он участвует в $c подтипах!";
        
        if(sizeof($err)) UIError($err);
        
        $sql = "DELETE FROM `tt_types` WHERE `id`=".$DB->F($id);      
        $DB->query($sql);   
        redirect($this->getLink(), "Тип успешно удален!");
    }
    
}

?>