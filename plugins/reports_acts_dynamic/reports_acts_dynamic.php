<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;



 
class reports_acts_dynamic_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/reports_acts_dynamic.tmpl.htm");
            else 
                $tpl->loadTemplatefile("reports_contr.tmpl.htm");
            $tpl->setVariable("REPORT_CONTR", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptList")) {
                $cntr = new kontr_plugin();
                
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptList(@$_POST["cnt_id"]));
            } 
            
        }
        
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
        
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //die("asdf");
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
            $rtpl->setVariable("DATETO", $_POST['dateto']);
            $signed = adm_statuses_plugin::getStatusByTag("signed", "agr_acts");
            $sql_wtypes = "SELECT lawd.wtype_id, (SELECT `contr_id` FROM `list_contr_agr` WHERE `id` IN (SELECT `agr_id` FROM `list_agr_act` WHERE `id`=lawd.act_id)) as contr_id FROM `list_acts_wtype_data` as lawd WHERE lawd.act_id IN 
                    (SELECT `id` FROM `list_agr_act` 
                        WHERE `sign_date`>=".$DB->F(date("Y-m-d", strtotime($_POST["datefrom"])))." AND `sign_date`<=".$DB->F(date("Y-m-d", strtotime($_POST["dateto"])))." AND `status`=".$DB->F($signed["id"]).") ORDER BY contr_id, wtype_id";
            $m = $DB->getCell2($sql_wtypes);
            if (!$m) {
                $rtpl->touchBlock("no-rows");
            } else {
                $wtype_list = false;
                foreach($m as $key => $val) {
                    if ($_POST["cnt_id"]>0) {
                        if ($_POST["cnt_id"] == $val) {
                            $wtype_list[] = $key;
                        }
                    } else {
                        $wtype_list[] = $key;                        
                    }
                }
                $wtypeNamesList = $DB->getCell2("SELECT `id`, `title` FROM `list_acts_wtypes` WHERE `id` IN (".implode(", ", $wtype_list).")");
                foreach($wtypeNamesList as $key=>$title) {
                    $rtpl->setCurrentBlock("wtypeth");
                    $rtpl->setVariable("WTYPETHN", $title);
                    $rtpl->parse("wtypeth");
                }
                
                $sql = "SELECT  lawd.act_id, 
                        (SELECT `ammount` FROM `list_agr_act` WHERE `id`=lawd.act_id) as amm, 
                        (SELECT `number` FROM `list_agr_act` WHERE `id`=lawd.act_id) as act, 
                        (SELECT DATE_FORMAT(`sign_date`, '%d.%m.%Y') FROM `list_agr_act` WHERE `id`=lawd.act_id) as act_date, 
                        (SELECT CONCAT(`number`, ' от ', DATE_FORMAT(`agr_date`, '%d.%m.%Y')) as agr FROM `list_contr_agr` WHERE `id` IN (SELECT `agr_id` FROM `list_agr_act` WHERE `id`=lawd.act_id)) as agr, 
                        (SELECT `contr_id` FROM `list_contr_agr` WHERE `id` IN (SELECT `agr_id` FROM `list_agr_act` WHERE `id`=lawd.act_id)) as contr_id FROM `list_acts_wtype_data` as lawd WHERE lawd.act_id IN 
                        (SELECT `id` FROM `list_agr_act` 
                            WHERE `sign_date`>=".$DB->F(date("Y-m-d", strtotime($_POST["datefrom"])))." AND `sign_date`<=".$DB->F(date("Y-m-d", strtotime($_POST["dateto"])))." AND `status`=".$DB->F($signed["id"]).") GROUP BY lawd.act_id ORDER BY contr_id, wtype_id";
                //die($sql);
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                if ($DB->num_rows()) {
                    $i = 1;
                    $total_amm = 0;
                    while ($r = $DB->fetch(true)) {
                        if ($_POST["cnt_id"]>0) {
                            if ($r["contr_id"] != $_POST["cnt_id"]) continue;
                        }
                        $rtpl->setCurrentBlock("rep_row");
                        $rtpl->setVariable("RR_ID", $i);
                        $rtpl->setVariable("RR_CONTR_NAME", $cntr);
                        $rtpl->setVariable("RR_AGR_NUM", $r["agr"] ? $r["agr"] : "<center>&mdash;</center>");
                        $rtpl->setVariable("RR_ACT_NUM", $r["act"] ? $r["act"] : "<center>&mdash;</center>");
                        $rtpl->setVariable("RR_ACT_DATE", $r["act_date"] ? $r["act_date"] : "<center>&mdash;</center>");
                        $contr_contact = $DB->getField("SELECT `contact` FROM `list_contr` WHERE `id`=".$DB->F($r["contr_id"]));
                        $contr_data = $DB->getField("SELECT `official_title` FROM `list_contr` WHERE `id`=".$DB->F($r["contr_id"]));
                        
                        $rtpl->setVariable("RR_CONTR_TITLE", $contr_data ? $contr_data : "<center>&mdash;</center>");
                        $rtpl->setVariable("RR_CONTR_CONTACT", $contr_contact ? $contr_contact : "<center>&mdash;</center>");
                        foreach($wtypeNamesList as $key => $i1) {
                            $sql = "SELECT `wtype_count`, (SELECT `title` FROM `list_skp_p_edizm` WHERE `id` IN (SELECT `edizm` FROM `list_acts_wtypes` WHERE `id`=".$DB->F($key).")) AS edizm FROM `list_acts_wtype_data` WHERE `act_id`=".$DB->F($r["act_id"])." AND `wtype_id`=".$DB->F($key).";";
                            $wed = $DB->getRow($sql, true);
                            $rtpl->setCurrentBlock("wtypetd");
                            if ($wed) 
                                $rtpl->setVariable("WTYPETDN", $wed["wtype_count"]);
                            else 
                                $rtpl->setVariable("WTYPETDN", "");
                            $rtpl->parse("wtypetd");
                            
                            
                        }
                        
                        
                        $rtpl->setVariable("RR_TOTALAMMOUNT", $r["amm"] ? number_format($r["amm"], 2, ",", " ") : "&mdash;");
                        $total_amm += floatval($r["amm"]);
                        $rtpl->parse("rep_row");
                        $i+=1;
                    }
                    if ($i==1) {
                        $rtpl->touchBlock("no-rows");
                    } else {
                        $rtpl->setCurrentBlock("tfoot");
                        $rtpl->setVariable("FOOT_COLSPAN", 5+count($wtypeNamesList));
                        $rtpl->setVariable("FOOT_TOTAL",  number_format($total_amm, 2, ",", " "));
                        $rtpl->parse("tfoot");
                    }
                } else {
                    $rtpl->touchBlock("no-rows");
                }
            
            }
                $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
                $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            
                if (isset($_POST["createxlsversion"])) {
                    header("Content-type: application/vnd.ms-excel");  
                    header("Content-disposition: attachment; filename=report_contr_".$_POST['datefrom']."-".$_POST['dateto'].".xls");  
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    if (isset($_POST['createprintversion'])) {
                        $rtpl->touchBlock("print_footer");
                        $rtpl->show();
                        
                        return;
                    } else {
                        $tpl->setCurrentBlock("reportval");
                        $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                        $tpl->parse("reportval");
                    }
                }
            
            
        } else {
            $tpl->touchBlock("notcreated");
        }
         
        $tpl->show();
        
    }
 

}
?>