<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\User;

/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class agreements_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__file__, '.php'));
    }


    function isBusy($id)
    {
        return false;
    }

    function viewlist()
    {
        global $DB, $USER;
        $sort_ids = array(
            1 => "ID",
            2 => "Контрагент",
            3 => "Дата подписания");
        $sort_sql = array(
            1 => "`id`",
            2 => "contr_name",
            3 => "lca.agr_date");

        $sort_order = array('DESC' => "Обратный", 'ASC' => "Прямой");

        $rpp = array(
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250");
        $prolong = array(
            0 => "-- все",
            1 => "Необходима пролонгация",
            2 => "Автоматическая пролонгация",
            3 => "Нет пролонгации");

        $type = array(
            0 => "-- все",
            1 => "Прайс",
            2 => "Смета",
            3 => "Абонентская плата");
        $contr_types = array(1=>"Поставщик", 2=>"Заказчик");

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() .
                ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->checkAccess("showfilter"))
        {
            $tpl->touchBlock("showfilter");
        } else
        {
            $tpl->touchBlock("hidefilter");
        }
        if ($USER->checkAccess("agreements", User::ACCESS_WRITE)) {
            $tpl->touchBlock("export");
        }
        if (!isset($_REQUEST["filter_rpp"]))
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        if ($_REQUEST["filter_rpp"] != $USER->getRpp())
        {
            $sql = "UPDATE `users` SET `rpp`=" . $_REQUEST["filter_rpp"] . " WHERE `id`=" .
                $DB->F($USER->getId()) . ";";
            $DB->query($sql);
            if ($DB->errno())
                UIError($DB->error());
        } else
        {
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        }
        $_REQUEST["filter_contrtype_id"] = $this->getCookie('filter_contrtype_id', $_REQUEST['filter_contrtype_id']);
        $_REQUEST["filter_id"] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ?
            $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ?
            $_REQUEST['filter_sort_order'] : key($sort_order));
        $_REQUEST["filter_search"] = $this->getCookie('filter_search', $_REQUEST['filter_search']);
        $_REQUEST["filter_title"] = $this->getCookie('filter_title', $_REQUEST['filter_title']);
        $_REQUEST["filter_type_id"] = $this->getCookie('filter_type_id', isset($type[$_REQUEST['filter_type_id']]) ?
            $_REQUEST['filter_type_id'] : key($type));
        $_REQUEST["filter_worktype_id"] = $this->getCookie('filter_worktype_id', $_REQUEST['filter_worktype_id']);
        $_REQUEST['filter_prolong_id'] = $this->getCookie('filter_prolong_id', isset($prolong[$_REQUEST['filter_prolong_id']]) ?
            $_REQUEST['filter_prolong_id'] : key($prolong));
        $_REQUEST["filter_contr_id"] = $this->getCookie('filter_contr_id', $_REQUEST['filter_contr_id']);
        $_REQUEST["filter_status_id"] = $this->getCookie('filter_status_id', $_REQUEST['filter_status_id']);

        $tpl->setVariable("FILTER_ID", $_REQUEST["filter_id"]);
        $tpl->setVariable("FILTER_CONTRTYPE_OPTIONS", array2options($contr_types, $_REQUEST["filter_contrtype_id"]));
        $tpl->setVariable("FILTER_TITLE", $_REQUEST["filter_title"]);
        $tpl->setVariable("FILTER_TYPE_OPTIONS", array2options($type, $_REQUEST["filter_type_id"]));
        $tpl->setVariable("FILTER_WORKTYPE_OPTIONS", adm_ticktypes_plugin::getOptList($_REQUEST["filter_worktype_id"]));
        $tpl->setVariable("FILTER_PROLONG_OPTIONS", array2options($prolong, $_REQUEST["filter_prolong_id"]));
        $tpl->setVariable("FILTER_CONTR_OPTIONS", kontr_plugin::getOptList($_REQUEST["filter_contr_id"]));
        $tpl->setVariable("FILTER_STATUS_OPTIONS", adm_statuses_plugin::getOptList($_REQUEST["filter_status_id"],
            "agreements"));
        $tpl->setVariable("FILTER_SORT_OPTIONS", array2options($sort_ids, $_REQUEST["filter_sort_id"]));
        $tpl->setVariable("FILTER_ORDER_OPTIONS", array2options($sort_order, $_REQUEST["filter_sort_order"]));
        $tpl->setVariable("FILTER_ROWS_PER_PAGE", array2options($rpp, $_REQUEST["filter_rpp"]));
        $tpl->setVariable('FILTER_SEARCH', $_REQUEST["filter_search"]);
        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];


        if ($_REQUEST["filter_id"])
            $filter[] = "lca.id=" . $DB->F($_REQUEST["filter_id"]) . " ";
        if ($_REQUEST["filter_title"])
            $filter[] = " lca.number=" . $DB->F($_REQUEST["filter_title"]) . " ";
        if ($_REQUEST["filter_type_id"])
        {
            if ($_REQUEST["filter_type_id"] == 1)
                $filter[] = " lca.type_price ";
            if ($_REQUEST["filter_type_id"] == 2)
                $filter[] = " lca.type_smeta ";
            if ($_REQUEST["filter_type_id"] == 3)
                $filter[] = " lca.type_abon ";
        }
        if ($_REQUEST["filter_worktype_id"])
            $filter[] = " lca.id IN (SELECT `agr_id` FROM `link_agr_wtypes` WHERE `wtype_id`=" .
                $DB->F($_REQUEST["filter_worktype_id"]) . ") ";
        if ($_REQUEST['filter_prolong_id'])
            $filter[] = " lca.prolong=" . $DB->F($_REQUEST['filter_prolong_id']) . " ";
        if ($_REQUEST["filter_contr_id"])
            $filter[] = " lca.contr_id=" . $DB->F($_REQUEST["filter_contr_id"]) . " ";
        if ($_REQUEST["filter_status_id"])
            $filter[] = " lca.status_id=" . $DB->F($_REQUEST["filter_status_id"]) . " ";
        if ($_REQUEST["filter_search"])
            $filter[] = " (lca.coment LIKE " . $DB->F("%" . $_REQUEST["filter_search"] . "%") .
                " OR lca.project LIKE " . $DB->F("%" . $_REQUEST["filter_search"] . "%") .
                " OR lca.agreement LIKE " . $DB->F("%" . $_REQUEST["filter_search"] . "%") .
                ") ";
        if ($_REQUEST["filter_contrtype_id"]>0) {
            if ($_REQUEST["filter_contrtype_id"] == "1")
                $filter[] = " lc.is_supplier ";
            else
                $filter[] = " lc.is_client ";
        }
        if (!sizeof($filter))
            $filter = " 1 ";
        else
        {
            $filter = implode(" AND ", $filter);
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS lca.*, 
                       (SELECT REPLACE(`contr_title`, '\"', '') as contr_title FROM `list_contr` WHERE `id`=lca.contr_id) as contr_name 
                FROM `list_contr_agr` AS lca $sql_add
                LEFT JOIN `list_contr` AS lc ON (lc.id=lca.contr_id)
                WHERE $filter                    
                ORDER BY
                    $order
                LIMIT {$_REQUEST['filter_start']}," . intval($_REQUEST["filter_rpp"]);

        //die($sql);
        $DB->query($sql);
        $total = $DB->getFoundRows();
        if ($DB->errno())
            UIError($DB->error() . $sql);
        ///if (!$DB->num_rows()) UIError("Контрагенты отсутствуют!", "Справочник Контрагентов", true, "Создать Контрагента", "adm_kontr/edit");
        while ($agreement = $DB->fetch(true))
        {
            $wtype = array();
            if ($agreement["type_price"])
                $wtype[] = "Прайс";
            if ($agreement["type_smeta"])
                $wtype[] = "Смета";
            if ($agreement["type_abon"])
                $wtype[] = "Абонентсая плата";
            if (sizeof($wtype))
            {
                $wt = implode("<br />", $wtype);
            } else
                $wt = "&mdash;";
            $sql = "SELECT `title` FROM `task_types` WHERE `id` IN (SELECT `wtype_id` FROM `link_agr_wtypes` WHERE `agr_id`=" .
                $DB->F($agreement["id"]) . ");";
            $wtypes = $DB->getCell($sql);
            if (sizeof($wtypes))
                $wtl = implode("<br />", $wtypes);
            else
                $wtl = "&mdash;";
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $agreement["id"]);
            $tpl->setVariable("VLI_TITLE", $agreement["number"]);
            $tpl->setVariable("VLI_CONTR", $agreement["contr_name"]);
            $tpl->setVariable("VLI_PROJECTTITLE", $agreement["project"] ? $agreement["project"] :
                "&mdash;");
            $tpl->setVariable("VLI_WTYPE", $wtl);
            $tpl->setVariable("VLI_AGRTPE", $wt);
            $tpl->setVariable("VLI_SIGNDATE", ($agreement["agr_date"] && $agreement["agr_date"] != "1970-01-01") ? date("d.m.Y",
                strtotime($agreement["agr_date"])) : "&mdash;");
            $tpl->setVariable("VLI_FINDATE", ($agreement["findate"] && $agreement["findate"]!="1970-01-01" ) ? date("d.m.Y", strtotime
                ($agreement["findate"])) : "&mdash;");
            $tpl->setVariable("VLI_PROLONG", $agreement["prolong"] ? $prolong[$agreement["prolong"]] :
                "не указно");
            $tpl->setVariable("VLI_STATUS", $agreement["status_id"] ? adm_statuses_plugin::
                getByID($agreement["status_id"]) : "не указан");
            $tpl->parse("vl_item");
        }
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"],
            $total, "#start-%s"));
        UIHeader($tpl);
        $tpl->show();

    }


    function viewlist_export()
    {
        header("Content-type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=agreements_export_".date("H_i_d-M-Y").".xls");

        global $DB, $USER;
        $sort_ids = array(
            1 => "ID",
            2 => "Контрагент",
            3 => "Дата подписания");
        $sort_sql = array(
            1 => "`id`",
            2 => "contr_name",
            3 => "lca.agr_date");

        $sort_order = array('DESC' => "Обратный", 'ASC' => "Прямой");

        $rpp = array(
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250");
        $prolong = array(
            0 => "-- все",
            1 => "Необходима пролонгация",
            2 => "Автоматическая пролонгация",
            3 => "Нет пролонгации");

        $type = array(
            0 => "-- все",
            1 => "Прайс",
            2 => "Смета",
            3 => "Абонентская плата");
        $contr_types = array(1=>"Поставщик", 2=>"Заказчик");

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/export.tmpl.htm");
        else
            $tpl->loadTemplatefile("export.tmpl.htm");
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if (!isset($_REQUEST["filter_rpp"]))
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        if ($_REQUEST["filter_rpp"] != $USER->getRpp())
        {
            $sql = "UPDATE `users` SET `rpp`=" . $_REQUEST["filter_rpp"] . " WHERE `id`=" .
                $DB->F($USER->getId()) . ";";
            $DB->query($sql);
            if ($DB->errno())
                UIError($DB->error());
        } else
        {
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        }

        $_REQUEST["filter_contrtype_id"] = $this->getCookie('filter_contrtype_id', $_REQUEST['filter_contrtype_id']);
        $_REQUEST["filter_id"] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ?
            $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ?
            $_REQUEST['filter_sort_order'] : key($sort_order));
        $_REQUEST["filter_search"] = $this->getCookie('filter_search', $_REQUEST['filter_search']);
        $_REQUEST["filter_title"] = $this->getCookie('filter_title', $_REQUEST['filter_title']);
        $_REQUEST["filter_type_id"] = $this->getCookie('filter_type_id', isset($type[$_REQUEST['filter_type_id']]) ?
            $_REQUEST['filter_type_id'] : key($type));
        $_REQUEST["filter_worktype_id"] = $this->getCookie('filter_worktype_id', $_REQUEST['filter_worktype_id']);
        $_REQUEST['filter_prolong_id'] = $this->getCookie('filter_prolong_id', isset($prolong[$_REQUEST['filter_prolong_id']]) ?
            $_REQUEST['filter_prolong_id'] : key($prolong));
        $_REQUEST["filter_contr_id"] = $this->getCookie('filter_contr_id', $_REQUEST['filter_contr_id']);
        $_REQUEST["filter_status_id"] = $this->getCookie('filter_status_id', $_REQUEST['filter_status_id']);

        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];


        if ($_REQUEST["filter_id"])
            $filter[] = "lca.id=" . $DB->F($_REQUEST["filter_id"]) . " ";
        if ($_REQUEST["filter_title"])
            $filter[] = " lca.number=" . $DB->F($_REQUEST["filter_title"]) . " ";
        if ($_REQUEST["filter_type_id"])
        {
            if ($_REQUEST["filter_type_id"] == 1)
                $filter[] = " lca.type_price ";
            if ($_REQUEST["filter_type_id"] == 2)
                $filter[] = " lca.type_smeta ";
            if ($_REQUEST["filter_type_id"] == 3)
                $filter[] = " lca.type_abon ";
        }
        if ($_REQUEST["filter_contrtype_id"]>0) {
            if ($_REQUEST["filter_contrtype_id"] == "1")
                $filter[] = " lc.is_supplier ";
            else
                $filter[] = " lc.is_client ";
        }
        if ($_REQUEST["filter_worktype_id"])
            $filter[] = " lca.id IN (SELECT `agr_id` FROM `link_agr_wtypes` WHERE `wtype_id`=" .
                $DB->F($_REQUEST["filter_worktype_id"]) . ") ";
        if ($_REQUEST['filter_prolong_id'])
            $filter[] = " lca.prolong=" . $DB->F($_REQUEST['filter_prolong_id']) . " ";
        if ($_REQUEST["filter_contr_id"])
            $filter[] = " lca.contr_id=" . $DB->F($_REQUEST["filter_contr_id"]) . " ";
        if ($_REQUEST["filter_status_id"])
            $filter[] = " lca.status_id=" . $DB->F($_REQUEST["filter_status_id"]) . " ";
        if ($_REQUEST["filter_search"])
            $filter[] = " (lca.coment LIKE " . $DB->F("%" . $_REQUEST["filter_search"] . "%") .
                " OR lca.project LIKE " . $DB->F("%" . $_REQUEST["filter_search"] . "%") .
                " OR lca.agreement LIKE " . $DB->F("%" . $_REQUEST["filter_search"] . "%") .
                ") ";
        if (!sizeof($filter))
            $filter = " 1 ";
        else
        {
            $filter = implode(" AND ", $filter);
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS lca.*, 
                (SELECT REPLACE(`official_title`, '\"', '') as contr_title FROM `list_contr` WHERE `id`=lca.contr_id) as contr_name 
                FROM `list_contr_agr` AS lca $sql_add
                LEFT JOIN `list_contr` AS lc ON (lc.id=lca.contr_id)
                
                WHERE $filter                    
                ORDER BY
                    $order;";

        //die($sql);
        $DB->query($sql);
        $total = $DB->getFoundRows();
        if ($DB->errno())
            UIError($DB->error() . $sql);
        ///if (!$DB->num_rows()) UIError("Контрагенты отсутствуют!", "Справочник Контрагентов", true, "Создать Контрагента", "adm_kontr/edit");
        while ($agreement = $DB->fetch(true))
        {
            $wtype = array();
            if ($agreement["type_price"])
                $wtype[] = "Прайс";
            if ($agreement["type_smeta"])
                $wtype[] = "Смета";
            if ($agreement["type_abon"])
                $wtype[] = "Абонентсая плата";
            if (sizeof($wtype))
            {
                $wt = implode("<br />", $wtype);
            } else
                $wt = "&mdash;";
            $sql = "SELECT `title` FROM `task_types` WHERE `id` IN (SELECT `wtype_id` FROM `link_agr_wtypes` WHERE `agr_id`=" .
                $DB->F($agreement["id"]) . ");";
            $wtypes = $DB->getCell($sql);
            if (sizeof($wtypes))
                $wtl = implode("<br />", $wtypes);
            else
                $wtl = "&mdash;";
            $tpl->setCurrentBlock("vl_item");

            $tpl->setVariable("VLI_SUBJECT", $agreement["agreement"] ? $agreement["agreement"] : "&mdash;");
            $tpl->setVariable("VLI_AMMOUNT", $agreement["ammount"] ? number_format($agreement["ammount"], 2, ",", " "):"&mdash;");
            $tpl->setVariable("VLI_ID", $agreement["id"]);
            $tpl->setVariable("VLI_TITLE", $agreement["number"]);
            $tpl->setVariable("VLI_CONTR", $agreement["contr_name"]);
            $tpl->setVariable("VLI_PROJECTTITLE", $agreement["project"] ? $agreement["project"] :
                "&mdash;");
            $tpl->setVariable("VLI_WTYPE", $wtl);
            $tpl->setVariable("VLI_AGRTPE", $wt);
            $tpl->setVariable("VLI_SIGNDATE", ($agreement["agr_date"] && $agreement["agr_date"] != "1970-01-01") ? date("d.m.Y",
                strtotime($agreement["agr_date"])) : "&mdash;");
            $tpl->setVariable("VLI_FINDATE", ($agreement["findate"] && $agreement["findate"]!="1970-01-01" ) ? date("d.m.Y", strtotime
                ($agreement["findate"])) : "&mdash;");
            $tpl->setVariable("VLI_PROLONG", $agreement["prolong"] ? $prolong[$agreement["prolong"]] :
                "не указно");
            $tpl->setVariable("VLI_STATUS", $agreement["status_id"] ? adm_statuses_plugin::
                getByID($agreement["status_id"]) : "не указан");
            $tpl->parse("vl_item");
        }
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable("CURRENT_USER", $USER->getFio());
        $tpl->setVariable("CURRENT_DATE", rudate("d M Y H:i:s"));
        UIHeader($tpl);
        $tpl->show();
        die();
    }


    function addfile_ajax()
    {
        global $DB, $USER;
        $files = "";
        $type = $_REQUEST["filetype"];
        $contr_id = $_REQUEST["agr_id"];
        $desc = $_REQUEST["desc"];
        $act_id = $_REQUEST["act_id"] ? $_REQUEST["act_id"] : 0;
        $ds_id = $_REQUEST["ds_id"] ? $_REQUEST["ds_id"] : 0;
        foreach ($_FILES['userfile']['error'] as $i => $error)
        {
            if ($error == UPLOAD_ERR_OK)
            {
                $sql = "INSERT INTO `list_agr_files` (`agr_id`, `file_type`, `act_id`, `ds_id`, `file_name`, `desc`, `mime_type`, `user_id`) 
                VALUES (" . $DB->F($contr_id) . ", " . $DB->F($type) . ", " . $DB->F($act_id, true) . ", " . $DB->F($ds_id, true) . ", " . $DB->F($_FILES['userfile']['name'][$i]) .", " . $DB->F($desc) . ", " . $DB->F($_FILES['userfile']['type'][$i]) . ", " . $DB->F($USER->getId()) . ");";
                $DB->query($sql);
                if (!($file_id = $DB->insert_id()))
                    $err[] = "Ошибка загрузки файла";

                $new_name = getcfg('files_path') . "/agr_$file_id.dat";
                if (!copy($_FILES['userfile']['tmp_name'][$i], $new_name))
                {
                    trigger_error("Failed to copy '$path' to '$new_name'");

                    $sql = "DELETE FROM `list_agr_files` WHERE `id`=" . $DB->F($file_id);
                    $DB->query($sql);

                    $err[] = "Ошибка копирования файла";
                }
                if (!$file_id)
                    $err[] = "Не удалось добавить файл '{$_FILES['userfile']['name'][$i]}' к договору. $sql";
                //$files .= "<a href=\"".getcfg('http_base')."kontr/getfile?id=".$file_id."&name=".urlencode($_FILES['userfile']['name'][$i]).">".$_FILES['userfile']['name'][$i]."</a><br />";
                //} else
            } elseif ($error != UPLOAD_ERR_NO_FILE)
            {
                $err[] = "Не удалось загрузить файл '{$_FILES['userfile']['name'][$i]}' на сервер";
            }
        }
        if (sizeof($err))
        {
            $ret["error"] = implode("\r\n", $err);
        } else
        {
            $ret["tpl"] = $files;
            $ret["ok"] = "ok";
            $ret["target"] = $type;
        }
        echo json_encode($ret);
        exit();
    }

    function delete_file_ajax()
    {
        global $USER, $DB;
        $id = $_REQUEST["id"];
        if ($id)
        {
            $sql = "DELETE FROM `list_agr_files` WHERE `id`=" . $DB->F($id) . " LIMIT 1;";
            $DB->query($sql);
            if ($DB->errno())
                $ret["error"] = $DB->error();
            else
                $ret["ok"] = "ok";
        } else
        {
            $ret["error"] = "Не указан идентификатор файла";
        }
        echo json_encode($ret);
        exit();
    }

    function getfile()
    {
        global $DB, $USER;

        $sql = "SELECT * FROM `list_agr_files` WHERE `id`=" . $DB->F($_REQUEST['id']);
        $f = $DB->getRow($sql, true);
        if (!$f['id'])
            UIError('Нет такого файла');

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Description: File Transfer");
        header("Content-type: {$f['mime_type']}");
        header("Content-Length: " . filesize(getcfg('files_path') . "/agr_{$f['id']}.dat"));
        header("Content-Disposition: attachment; filename=\"{$f['file_name']}\"");
        readfile(getcfg('files_path') . "/agr_{$f['id']}.dat");
        exit;
    }

    function files_ajax()
    {
        global $DB, $USER;
        $target = $_REQUEST["target"];
        $agr_id = $_REQUEST["agr_id"];
        if ($target && $agr_id)
        {
            $sql = "SELECT * FROM `list_agr_files` WHERE `agr_id`=" . $DB->F($agr_id) .
                " AND `file_type`=" . $DB->F($target) . ";";
            $DB->query($sql);
            if ($DB->num_rows())
            {

                while ($r = $DB->fetch(true))
                {
                    $add = "";
                    if ($r["act_id"])
                        $add = "<br /><small>Акт № " . $DB->getField("SELECT `number` FROM `list_agr_act` WHERE `id`=" .
                            $DB->F($r["act_id"]) . ";") . "</small>";
                    if ($r["ds_id"])
                        $add .= "<br /><small>ДС № " . $DB->getField("SELECT `number` FROM `list_agr_ds` WHERE `id`=" .
                            $DB->F($r["ds_id"]) . ";") . "</small>";
                    $files .= "<tr><td width='30%'><a href=/agreements/getfile?id=" . $r["id"] . "&name=" . urlencode($r["file_name"]) .
                        ">" . $r["file_name"] . "</a>" . $add . "</td><td>" . ($r["desc"] ? $r["desc"] :
                        "-- отсутствует --") .
                        "</td><td style='width:16px !important;'><a style='cursor:pointer;' class='deletefile' target_type='" .
                        $r["file_type"] . "' id='" . $r["id"] .
                        "'><img src='/templates/images/cross.png' /></a></td></tr>";
                }
                $ret["tpl"] = "<table class='t1' width='100%'><tr><th>Имя файла</th><th>Описание</th><th></th></tr>" .
                    $files . "</table>";
            } else
            {
                $ret["tpl"] = "-- файлы не загружены --";
            }
            $ret['ok'] = "ok";
        } else
        {
            $ret["error"] = "Не указан идентификатор договора и/или тип файлов";
        }
        $DB->free();
        echo json_encode($ret);
        exit();
    }


    function saveagreement()
    {

        global $DB;
        $contr_id = @$_POST["new_contr_id"] ? @$_POST["new_contr_id"] : @$_POST["contr_id"];
        $agr_id = @$_POST["agr_id"];
        $agr_num = @$_POST["agr_num"];
        $agr_date = @$_POST["agr_date"];
        $wtypes = @$_POST["wtypes_id"];
        $qcs = @$_POST["wtypes_qc"];
        $comment = @$_POST["agr_comment"];
        $access = @$_POST["access_contact"];
        $type_price = @$_POST["type_price"] == "on" ? "1" : "0";
        $type_smeta = @$_POST["type_smeta"] == "on" ? "1" : "0";
        $type_abon = @$_POST["type_abon"] == "on" ? "1" : "0";
        $tp = @$_POST["tp_contact"];
        $mailonlk = @$_POST["mailonnewticket"] == "on" ? "1" : "0";
        //print_r($_REQUEST);
        //die();
        if (!$contr_id || !$agr_date || !$agr_num)
        {
            UIError("Для сохранения договора недостаточно данных! Пожалуйста, заполните форму целиком!");
            return;
        }
        $aDate = explode(".", $agr_date);
        if ($agr_id)
        {
            $sql = "DELETE FROM `link_agr_wtypes` WHERE `agr_id`=" . $DB->F($agr_id) . ";";
            $DB->query($sql);
            if ($DB->errno())
                UIError($DB->error() . $sql);
            //$sql = "DELETE FROM `list_agr_price` WHERE `agr_id`=".$DB->F($agr_id).";";
            //$DB->query($sql);
            //if ($DB->errno()) UIError($DB->error().$sql);
            if ($_POST["agr_findate"]!="") {
                $agrfindate = "`findate`=".$DB->F(date("Y-m-d", strtotime($_POST["agr_findate"])), true).",";
            } else {
                //$DB->query("UPDATE `list_contr_agr` SET `findate`=".$DB->F('', true).";");
                //$DB->free();
                $agrfindate = "`findate`=".$DB->F('', true).",";
            }

            $sql = "UPDATE `list_contr_agr` SET `number`=" . $DB->F($agr_num) . ", 
                    `agr_date`=" . $DB->F($aDate[2] . "-" . $aDate[1] . "-" . $aDate[0]) .
                ", 
                    `coment`=" . $DB->F($comment) . ", `access_contact`=" . $DB->
                F($access) . ", 
                    `tp_contact`=" . $DB->F($tp) . ", `type_price`=" . $DB->F($type_price) .
                ", 
                    `type_smeta`=" . $DB->F($type_smeta) . ", `type_abon`=" . $DB->
                F($type_abon) . ",
                `agreement`=".$DB->F($_POST["subject"]).",
                `prolong`=".$DB->F($_POST["prolong"]).",
                {$agrfindate}
                `project`=".$DB->F($_POST["simplename"]).",
                `status_id`=".$DB->F($_POST["status"]).",
                `ammount`=".$DB->F($_POST["price"] ? $_POST["price"] : 0).",
                `paytype`=".$DB->F($_POST["paytype"]).", `mailonlk`=".$DB->F($mailonlk)."
                 WHERE `id`=" . $DB->F($agr_id) . ";";
            $DB->query($sql);
            if ($DB->errno())
                UIError($DB->error() . $sql);

        } else
        {
            $sql = "INSERT INTO `list_contr_agr` (`number`, `agr_date`, `contr_id`, `coment`, `access_contact`, `tp_contact`, `type_price`, `type_smeta`, `type_abon`, 
            `agreement`, `prolong`, `findate`, `project`, `status_id`, `ammount`, `paytype`, `mailonlk`) 
                    VALUES (" . $DB->F($agr_num) . ", " . $DB->F($aDate[2] . "-" .
                $aDate[1] . "-" . $aDate[0]) . ", " . $DB->F($contr_id) . ", " . $DB->F($comment) .
                ", " . $DB->F($access) . ",
                    " . $DB->F($tp) . ", " . $DB->F($type_price) . ", " . $DB->
                F($type_smeta) . ", " . $DB->F($type_abon) . ", ".$DB->F($_POST["subject"]).", ".$DB->F($_POST["prolong"]).", ".$DB->F(date("Y-m-d", strtotime($_POST["agr_findate"]))).", ".$DB->F($_POST["simplename"]).", ".$DB->F($_POST["status"]).", ".$DB->F($_POST["price"] ? $_POST["price"] : 0).", ".$DB->F($_POST["paytype"]).", ".$DB->F($mailonlk).");";
            $DB->query($sql);
            if ($DB->errno())
                UIError($DB->error());
            $agr_id = $DB->insert_id();
        }
        $sql = "INSERT IGNORE INTO `link_agr_wtypes` (`agr_id`, `wtype_id`, `is_qc`) VALUES ";
        $wt = explode(",", $wtypes);
        $qcs = explode(",", $qcs);
        $sql_add = false;
        foreach ($wt as $key=>$item)
        {
            $sql_add[] = "(" . $DB->F($agr_id) . ", " . $DB->F($item) . "," . ($qcs[$key] > 0 ? $qcs[$key] : "0"). ")";
        }
        $sql = $sql . implode(",", $sql_add) . ";";
        #error_log($sql."\n".print_r($_REQUEST,true)."\n".print_r($qcs,true)."\n",3,"/ramdisk/sql.log");
        $DB->query($sql);
        if ($DB->errno())
            UIError($DB->error());
        redirect($this->getLink('viewagreement') . "?contr_id=" . $contr_id . "&agr_id=" . $agr_id,
            "Договор успешно сохранен. ID: " . $agr_id);

    }

    function saveds_ajax()
    {
        global $DB, $USER;
        $ds_id = $_REQUEST["ds_id"];
        $agr_id = $_REQUEST["agr_id"];
        if (!$agr_id)
        {
            $ret["error"] = "Не указан идентификатор договора!";
            echo json_encode($ret);
            exit();
        }
        if ($ds_id)
        {
            $sql = "UPDATE `list_agr_ds` SET 
            `number`=" . $DB->F($_REQUEST["number"]) . ", 
            `simplename`=" . $DB->F($_REQUEST["simplename"]) . ", 
            `subject`=" . $DB->F($_REQUEST["subject"]) . ", 
            `sign_date`=" . $DB->F(date("Y-m-d", strtotime($_REQUEST["signdate"]))) . ", 
            `fin_date`=" . $DB->F($_REQUEST["findate"] ? date("Y-m-d", strtotime($_REQUEST["findate"])): "", true) . ", 
            `ammount`=" . $DB->F($_REQUEST["ammount"] ? $_REQUEST["ammount"] : 0) . ", 
            `status`=" . $DB->F($_REQUEST["status"]) . ", 
            `inaction`=" . $DB->F($_REQUEST["inaction"]) . ", 
            `doctoprint`=" . $DB->F($_REQUEST["doctoprint"]) . ", 
            `comment`=" . $DB->F($_REQUEST["comment"]) . "
            WHERE `id`=" . $DB->F($ds_id) . ";";
        } else
        {
            $sql = "INSERT INTO `list_agr_ds` 
            (`number`, 
            `agr_id`, 
            `simplename`, 
            `subject`, 
            `sign_date`, 
            `fin_date`, 
            `ammount`, 
            `status`, 
            `inaction`, 
            `doctoprint`, 
            `comment`) 
            VALUES 
            (" . $DB->F($_REQUEST["number"]) . ", 
            " . $DB->F($agr_id) . ", 
            " . $DB->F($_REQUEST["simplename"]) . ", 
            " . $DB->F($_REQUEST["subject"]) . ", 
            " . $DB->F(date("Y-m-d", strtotime($_REQUEST["signdate"]))) . ", 
            " . $DB->F($_REQUEST["findate"] ? date("Y-m-d", strtotime($_REQUEST["findate"])):"", true) . ", 
            " . $DB->F($_REQUEST["ammount"] ? $_REQUEST["ammount"] : 0) . ", 
            " . $DB->F($_REQUEST["status"]) . ", 
            " . $DB->F($_REQUEST["inaction"]) . ", 
            " . $DB->F($_REQUEST["doctoprint"]) . ", 
            " . $DB->F($_REQUEST["comment"]) . ");";
        }
        $DB->query($sql);
        if ($DB->errno())
        {
            $ret["error"] = $DB->error();
            echo json_encode($ret);
            exit();
        } else
            $ret["ok"] = "ok";
        if (!$ds_id)
            $last = $DB->insert_id();
        $DB->free();
        if (!$ds_id)
        {

            $files = "";
            $type = "3";
            $contr_id = $_REQUEST["agr_id"];
            $desc = "Загружено при создании";
            foreach ($_FILES['userfile']['error'] as $i => $error)
            {
                if ($error == UPLOAD_ERR_OK)
                {
                    $sql = "INSERT INTO `list_agr_files` (`agr_id`, `file_type`, `act_id`, `ds_id`, `file_name`, `desc`, `mime_type`, `user_id`) 
                    VALUES(" . $DB->F($contr_id) . ", " . $DB->F($type) . ", " .
                        $DB->F(0, true) . ", " . $DB->F($last, true) . ", " . $DB->F($_FILES['userfile']['name'][$i]) .
                        ", " . $DB->F($desc) . ", " . $DB->F($_FILES['userfile']['type'][$i]) . ", " . $DB->
                        F($USER->getId()) . ");";
                    $DB->query($sql);
                    if (!($file_id = $DB->insert_id()))
                        $err[] = "Ошибка загрузки файла";
                    $DB->free();
                    $new_name = getcfg('files_path') . "/agr_$file_id.dat";
                    if (!copy($_FILES['userfile']['tmp_name'][$i], $new_name))
                    {
                        trigger_error("Failed to copy '$path' to '$new_name'");

                        $sql = "DELETE FROM `list_agr_files` WHERE `id`=" . $DB->F($file_id);
                        $DB->query($sql);
                        $DB->free();
                        $err[] = "Ошибка копирования файла";
                    }
                    if (!$file_id)
                        $err[] = "Не удалось добавить файл '{$_FILES['userfile']['name'][$i]}' к договору. $sql";
                    //$files .= "<a href=\"".getcfg('http_base')."kontr/getfile?id=".$file_id."&name=".urlencode($_FILES['userfile']['name'][$i]).">".$_FILES['userfile']['name'][$i]."</a><br />";
                    //} else
                } elseif ($error != UPLOAD_ERR_NO_FILE)
                {
                    $err[] = "Не удалось загрузить файл '{$_FILES['userfile']['name'][$i]}' на сервер";
                }
            }

        }
        if (sizeof($err))
            $ret["error"] = implode("\r\n", $err);
        echo json_encode($ret);
        exit();
    }

    function deleteds_ajax() {
        global $DB, $USER;
        $id = $_REQUEST["id"];
        if ($id) {
            if ($DB->getField("SELECT COUNT(`id`) FROM `list_agr_act` WHERE `ds_id`=".$DB->F($id).";")) {
                $ret["error"] = "Выбранная запись занята системой. Удаление невозможно!";
            } else {
                $sql = "DELETE FROM `list_agr_ds` WHERE `id`=".$DB->F($id).";";
                $DB->query($sql);
                if ($DB->errno()) $ret["error"] = $DB->error();
                else $ret["ok"] = "ok";
            }
        } else {
            $ret["error"] = "Не указан идентификатор ДС";
        }
        echo json_encode($ret);
        exit();
    }

    function deleteact_ajax() {
        global $DB, $USER;
        $id = $_REQUEST["id"];
        if ($id) {
            $sql = "DELETE FROM `list_agr_act` WHERE `id`=".$DB->F($id).";";
            $DB->query($sql);
            if ($DB->errno()) $ret["error"] = $DB->error();
            $sql = "DELETE FROM `list_acts_wtype_data` WHERE `act_id`=".$DB->F($id).";";
            $DB->query($sql);
            if ($DB->errno()) $ret["error"] = $DB->error();
            if (!$ret["error"]) {
                $ret["ok"] = "ok";
            }
        } else {
            $ret["error"] = "Не указан идентификатор Акта";
        }
        echo json_encode($ret);
        exit();

    }

    function saveact_ajax()
    {
        global $DB, $USER;
        $act_id = $_REQUEST["act_id"];
        $agr_id = $_REQUEST["agr_id"];
        if (!$agr_id)
        {
            $ret["error"] = "Не указан идентификатор договора!";
            echo json_encode($ret);
            exit();
        }
        if ($act_id)
        {
            $sql = "UPDATE `list_agr_act` SET 
            `number`=" . $DB->F($_REQUEST["number"]) . ", 
            `ds_id`=" . $DB->F($_REQUEST["ds1_id"]) . ", 
            `sign_date`=" . $DB->F(date("Y-m-d", strtotime($_REQUEST["signdate"]))) . ", 
            `ammount`=" . $DB->F($_REQUEST["ammount"] ? $_REQUEST["ammount"] : 0) . ", 
            `status`=" . $DB->F($_REQUEST["status"]) . ", 
            `comment`=" . $DB->F($_REQUEST["comment"]) . "
            WHERE `id`=" . $DB->F($act_id) . ";";
        } else
        {
            $sql = "INSERT INTO `list_agr_act` 
            (`number`, 
            `agr_id`, 
            `ds_id`, 
            `sign_date`, 
            `ammount`, 
            `status`, 
            `comment`) 
            VALUES 
            (" . $DB->F($_REQUEST["number"]) . ", 
            " . $DB->F($agr_id) . ", 
            " . $DB->F($_REQUEST["ds1_id"]) . ", 
            " . $DB->F(date("Y-m-d", strtotime($_REQUEST["signdate"]))) . ", 
            " . $DB->F($_REQUEST["ammount"] ? $_REQUEST["ammount"] : 0) . ", 
            " . $DB->F($_REQUEST["status"]) . ", 
            " . $DB->F($_REQUEST["comment"]) . ");";
        }
        $DB->query($sql);
        if ($DB->errno())
        {
            $ret["error"] = $DB->error();
            echo json_encode($ret);
            exit();
        } else
            $ret["ok"] = "ok";
        if (!$act_id)
            $last = $DB->insert_id();
        $DB->free();
        if (!$act_id)
        {
            $files = "";
            $type = "2";
            $contr_id = $_REQUEST["agr_id"];
            $desc = "Загружено при создании";
            foreach ($_FILES['userfile']['error'] as $i => $error)
            {
                if ($error == UPLOAD_ERR_OK)
                {
                    $sql = "INSERT INTO `list_agr_files` (`agr_id`, `file_type`, `act_id`, `ds_id`, `file_name`, `desc`, `mime_type`, `user_id`) 
                    VALUES(" . $DB->F($contr_id) . ", " . $DB->F($type) . ", " .
                        $DB->F($last, true) . ", " . $DB->F(0, true) . ", " . $DB->F($_FILES['userfile']['name'][$i]) .
                        ", " . $DB->F($desc) . ", " . $DB->F($_FILES['userfile']['type'][$i]) . ", " . $DB->
                        F($USER->getId()) . ");";
                    $DB->query($sql);
                    if (!($file_id = $DB->insert_id()))
                        $err[] = "Ошибка загрузки файла";
                    $DB->free();
                    $new_name = getcfg('files_path') . "/agr_$file_id.dat";
                    if (!copy($_FILES['userfile']['tmp_name'][$i], $new_name))
                    {
                        trigger_error("Failed to copy '$path' to '$new_name'");

                        $sql = "DELETE FROM `list_agr_files` WHERE `id`=" . $DB->F($file_id);
                        $DB->query($sql);
                        $DB->free();
                        $err[] = "Ошибка копирования файла";
                    }
                    if (!$file_id)
                        $err[] = "Не удалось добавить файл '{$_FILES['userfile']['name'][$i]}' к договору. $sql";
                    //$files .= "<a href=\"".getcfg('http_base')."kontr/getfile?id=".$file_id."&name=".urlencode($_FILES['userfile']['name'][$i]).">".$_FILES['userfile']['name'][$i]."</a><br />";
                    //} else
                } elseif ($error != UPLOAD_ERR_NO_FILE)
                {
                    $err[] = "Не удалось загрузить файл '{$_FILES['userfile']['name'][$i]}' на сервер";
                }
            }

        }
        if (sizeof($err))
            $ret["error"] = implode("\r\n", $err);
        echo json_encode($ret);
        exit();
    }

    function getds_ajax()
    {
        global $DB, $USER;
        $agr_id = $_REQUEST["agr_id"];
        if (!$agr_id)
        {
            $ret["error"] = "Не указан идентификатор договора!";
            echo json_encode($ret);
            exit();
        }
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("viewagr.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/ds.tmpl.htm");
        else
            $tpl->loadTemplatefile("ds.tmpl.htm");

        $sql = "SELECT * FROM `list_agr_ds` WHERE `agr_id`=" . $DB->F($agr_id) . ";";
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);
            exit();
        }
        if ($DB->num_rows())
        {
            $tpl->setVariable("DS_NO_RECORDS", "style='display: none;'");
            while ($ds = $DB->fetch(true))
            {
                $ds_arr[$ds["id"]] = $ds["number"]." от ".$ds["sign_date"];
                $tpl->setCurrentBlock("ds-list");
                $tpl->setVariable("DSID", $ds["id"]);
                $tpl->setVariable("DSNAME", $ds["number"] . " " . ($ds["sign_date"] ? " от " .
                    rudate("d M Y", strtotime($ds["sign_date"])) : ""));
                $tpl->parse("ds-list");
                $tpl->setCurrentBlock("ds1-list");
                $tpl->setVariable("DS1ID", $ds["id"]);
                $tpl->setVariable("DS1NAME", $ds["number"] . " " . ($ds["sign_date"] ? " от " .
                    rudate("d M Y", strtotime($ds["sign_date"])) : ""));
                $tpl->parse("ds1-list");
                $tpl->setCurrentBlock("ds-row");
                $tpl->setVariable("DS_ID", $ds["id"]);
                $tpl->setVariable("DS_NUM", $ds["number"]);
                $tpl->setVariable("DS_SIMPLENAME", $ds["simplename"] ? $ds["simplename"] :
                    "&mdash;");
                $tpl->setVariable("DS_SUBJECT", $ds["subject"] ? $ds["subject"] : "&mdash;");
                $tpl->setVariable("DS_SIGNDATE", $ds["sign_date"] ? rudate("d M Y", strtotime($ds["sign_date"])) :
                    "&mdash;");
                $tpl->setVariable("DS_FINDATE", $ds["fin_date"]!='' ? rudate("d M Y", strtotime($ds["fin_date"])) :
                    "");
                $tpl->setVariable("DS_PRICE", $ds["ammount"] ? number_format($ds["ammount"], 2,
                    ".", " ") : "&mdash;");
                $tpl->setVariable("DS_STSTE", $ds["status"] == "0" ? "Активен" : "Завершен");
                $tpl->setVariable("DS_TOTALAMMOUNT", number_format($DB->getField("SELECT SUM(`ammount`) FROM `list_agr_act` WHERE `status`='89' AND `ds_id`=" .
                    $DB->F($ds["id"]) . ";"), 2, ".", " "));
                $sql = "SELECT * FROM `list_agr_files` WHERE `agr_id`=" . $DB->F($agr_id) .
                    " AND `file_type`='3' AND `ds_id`=" . $DB->F($ds["id"]) . ";";
                $DB->query($sql);
                $files = "";
                if ($DB->num_rows())
                {
                    while ($r = $DB->fetch(true))
                    {
                        $files .= "<a href=/agreements/getfile?id=" . $r["id"] .
                            "&name=" . urlencode($r["file_name"]) . ">" . $r["file_name"] . "</a><br />";
                    }
                } else
                {
                    $files = "-- файлы не загружены --";
                }
                $DB->free();
                $tpl->setVariable("DS_FILES", $files);
                $tpl->setVariable("DS_COMMENT", $ds["comment"] ? $ds["comment"] : "&mdash;");
                $tpl->parse("ds-row");
            }
        } else $tpl->setVariable("DS_NO_RECORDS", " ");
        $DB->free();
        $ret["tpl"] = $tpl->get();
        $ret["ds_arr"] = sizeof($ds_arr) ? array2options($ds_arr) : "";
        $ret["ok"] = "ok";
        echo json_encode($ret);
        exit();

    }

    function getact_ajax()
    {
        global $DB, $USER;
        $agr_id = $_REQUEST["agr_id"];
        if (!$agr_id)
        {
            $ret["error"] = "Не указан идентификатор договора!";
            echo json_encode($ret);
            exit();
        }
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("viewagr.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/act.tmpl.htm");
        else
            $tpl->loadTemplatefile("act.tmpl.htm");


        $sql = "SELECT * FROM `list_agr_act` WHERE `agr_id`=" . $DB->F($agr_id) . ";";
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);
            exit();
        }
        if ($DB->num_rows())
        {
            $tpl->setVariable("ACT_NO_RECORDS", "style='display: none;'");
            while ($act = $DB->fetch(true))
            {
                $act_arr[$act["id"]] = $act["number"]." от ".$act["sign_date"];
                $tpl->setCurrentBlock("act-list");
                $tpl->setVariable("ACTID", $act["id"]);
                $tpl->setVariable("ACTNAME", $act["number"] . " " . ($act["sign_date"] ? " от " .
                    rudate("d M Y", strtotime($act["sign_date"])) : ""));
                $tpl->parse("act-list");
                $tpl->setCurrentBlock("actrow");

                $tpl->setVariable("STATUS_COLOR", adm_statuses_plugin::getStatusColorByID($act["status"]));
                $tpl->setVariable("ACT_ID", $act["id"]);
                $tpl->setVariable("ACT_NUM", $act["number"]);
                $ds = $DB->getField("SELECT `number` FROM `list_agr_ds` WHERE `id`=" . $DB->F($act["ds_id"]) .
                    ";");
                $tpl->setVariable("DS", $ds ? $ds : "&mdash;");
                $tpl->setVariable("ACT_DATE", $act["sign_date"] ? rudate("d M Y", strtotime($act["sign_date"])) :
                    "");
                $tpl->setVariable("ACT_SUMM", number_format($act["ammount"], 2, ".", " "));
                $sql = "SELECT * FROM `list_agr_files` WHERE `agr_id`=" . $DB->F($agr_id) .
                    " AND `file_type`='2' AND `act_id`=" . $DB->F($act["id"]) . ";";

                $DB->query($sql);
                $files = "";
                if ($DB->num_rows())
                {
                    while ($r = $DB->fetch(true))
                    {
                        $files .= "<a href=/agreements/getfile?id=" . $r["id"] .
                            "&name=" . urlencode($r["file_name"]) . ">" . $r["file_name"] . "</a><br />";
                    }
                } else
                {
                    $files = "-- файлы не загружены --";
                }
                $DB->free();
                $signed = adm_statuses_plugin::getStatusByTag("signed", "agr_acts");
                if ($act["status"] != $signed["id"]) {
                    $tpl->setVariable("SHOW_AWD", "style='display: none;'");
                }
                $tpl->setVariable("ACT_SUMM_NF", $act["ammount"]);

                $tpl->setVariable("ACT_FILES", $files);
                $tpl->setVariable("ACT_STATUS", adm_statuses_plugin::getByID($act["status"]));
                $tpl->setVariable("ACT_COMMENT", $act["comment"] ? $act["comment"] : "&mdash;");
                $tpl->parse("actrow");
            }
        } else $tpl->setVariable("ACT_NO_RECORDS", " ");
        $DB->free();
        $closedsumm =  $DB->getField("SELECT SUM(`ammount`) FROM `list_agr_act` WHERE `status`='89' AND `agr_id`=" .$DB->F($agr_id) . ";");
        $closedsumm = $closedsumm ? number_format($closedsumm, 2, ".", " ") : "&mdash;";
        $ret["agr_total"] = $closedsumm;
        $ret["tpl"] = $tpl->get();
        $ret["act_arr"] = sizeof($act_arr) ? array2options($act_arr) : "";
        $ret["ok"] = "ok";
        echo json_encode($ret);
        exit();
    }


    function getactbyid_ajax()
    {
        global $DB, $USER;
        $act_id = $_REQUEST["act_id"];
        if (!$act_id)
        {
            $ret["error"] = "Не указан идентификатор акта!";
            echo json_encode($ret);
            exit();
        }
        $sql = "SELECT * FROM `list_agr_act` WHERE `id`=" . $DB->F($act_id) . ";";
        $act = $DB->getRow($sql, true);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);
            exit();
        }
        if ($act) {
            $act["sign_date"] = date("d.m.Y", strtotime($act["sign_date"]));
            $ret = $act;
            $ret["ok"] = "ok";
        } else $ret["error"] = "Не найден акт";
        echo json_encode($ret);
        exit();
    }


    function getdsbyid_ajax()
    {
        global $DB, $USER;
        $ds_id = $_REQUEST["ds_id"];
        if (!$ds_id)
        {
            $ret["error"] = "Не указан идентификатор ДС!";
            echo json_encode($ret);
            exit();
        }
        $sql = "SELECT * FROM `list_agr_ds` WHERE `id`=" . $DB->F($ds_id) . ";";
        $ds = $DB->getRow($sql, true);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);
            exit();
        }
        if ($ds) {
            $ds["sign_date"] = date("d.m.Y", strtotime($ds["sign_date"]));
            $ds["fin_date"] = $ds["fin_date"] ? date("d.m.Y", strtotime($ds["fin_date"])):'';

            $ret = $ds;
            $ret["ok"] = "ok";
        } else $ret["error"] = "Не найден акт";
        echo json_encode($ret);
        exit();

    }

    static function getwtypeactbyid_ajax() {
        global $DB, $USER;
        $id = $_REQUEST["id"];
        if (!$id) {
            $ret["error"] = "Не указан идентификатор акта";
            echo json_encode($ret);
            return false;
        }
        $sql = "SELECT awd.id, (SELECT `title` FROM `list_acts_wtypes` WHERE `id`=awd.wtype_id) as wtype_name, (SELECT `edizm` FROM `list_acts_wtypes` WHERE `id`=awd.wtype_id) as wtype_edizm_id, awd.wtype_count, awd.wtype_ammount FROM `list_acts_wtype_data` as awd WHERE awd.act_id=".$DB->F($id).";";
        $DB->query($sql);
        $tpl = new HTML_Template_IT(path2("plugins/agreements"));
        $tpl->loadTemplatefile($USER->getTemplate()."/awd.tmpl.htm");

        if (!$DB->num_rows()) {
            $tpl->touchBlock("no-awd");
        } else {
            while ($r = $DB->fetch(true)) {
                $tpl->setCurrentBlock("awd");
                $tpl->setVariable("AWD_ACT_ID", $id);
                $tpl->setVariable("AWD_ID", $r["id"]);
                $tpl->setVariable("AWD_NAME", $r["wtype_name"]);
                $tpl->setVariable("AWD_QUANT", $r["wtype_count"]." ". $DB->getField("SELECT `title` FROM `list_skp_p_edizm` WHERE `id`=".$DB->F($r["wtype_edizm_id"]).""));
                $tpl->setVariable("AWD_AMMOUNT", $r["wtype_ammount"]);
                $tpl->parse("awd");
            }
        }
        $ret["ok"] = "ok";
        $ret["tpl"] = $tpl->get();
        echo json_encode($ret);
        return false;
    }

    function viewagreement()
    {
        global $DB, $USER;
        $prolong_ids = array(
            1 => "Необходима пролонгация",
            2 => "Автоматическая пролонгация",
            3 => "Нет пролонгации");
        $bn = array(
            0 => "-- не указан --",
            1 => "Наличный расчет",
            2 => "Безналичный расчет"
        );
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("viewagr.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/view.tmpl.htm");
        else
            $tpl->loadTemplatefile("view.tmpl.htm");

        $cType = false;
        $agr_id = $_REQUEST['agr_id'];
        $agreement = $DB->getRow("SELECT * FROM `list_contr_agr` WHERE `id`=" . $DB->F($agr_id), true);
        //print_r($agreement);
        //die();
        $color = adm_statuses_plugin::getStatusColorByID($agreement["status_id"]);
        $tpl->setVariable("STATUS_BCOLOR", $color);
        if ($contr_id = $agreement["contr_id"])
        {
            $sql = "SELECT `contr_title`, `official_title`, `contr_type`, `contr_type2`, `contr_type3`, `contr_type4`, contr_type_tm FROM `list_contr` WHERE `id`=" .
                $DB->F($contr_id) . ";";
            $r = $DB->getRow($sql, true);
            //print_r($r);
            //die();
            if ($r)
            {
                $tpl->setVariable("AGR_CONTR_TITLE", $r['official_title'] . " (" . $r['contr_title'] . ")");
                $tpl->setVariable("CONTR_ID", $contr_id);

            } else
            {
                UIError("Ошибка определения Контрагента для добавления договора!");
            }
            if ($r["contr_type"]>0) $cType[] = 1;
            if ($r["contr_type2"]>0) $cType[] = 2;
            if ($r["contr_type3"]>0) $cType[] = 3;
            if ($r["contr_type4"]>0) $cType[] = 4;
            if ($r["contr_type_tm"]>0) $cType[] = 5;

            if (sizeof($cType))
                $tpl->setVariable("CTYPE", $cType);
            else
                $tpl->setVariable("CTYPE", '0');

            if ($agr_id = $_REQUEST['agr_id'])
            {
                $tpl->setVariable("AGR_ID", $agr_id);
                $tpPhones = $this->getTPPhones($agr_id);
                if ($tpPhones) {
                    foreach($tpPhones as $item) {
                        $phonestr .= ($item["phonetitle"] ? $item["phonetitle"]." - " : "").$item["phone"]. ($item["ext"] ? " доб. ".$item["ext"] : "") . "<br />";
                    }
                } else $phonestr = "&mdash;";
                $tpl->setVariable("TPTELNUM", $phonestr);
                $actst = adm_statuses_plugin::getList("agr_acts");
                $tpl->setVariable("ACTSTATUSNAMELIST", array2options($actst));
                if ($agreement)
                {
                    $tpl->setVariable("MAILONLKIFSET", $agreement["mailonlk"]>0 ? "Да" : "Нет");
                    $agreement["paytype"] = $agreement["paytype"] ? $agreement["paytype"] : 0;
                    $tpl->setVariable("AGR_NUM", $agreement["number"]);
                    $rdate = explode("-", $agreement["agr_date"]);
                    $tpl->setVariable("AGR_DATE", $rdate[2] . "." . $rdate[1] . "." . $rdate[0]);
                    $tpl->setVariable("AGR_COMMENT", $agreement["coment"] ? $agreement["coment"] :
                        "&mdash;");
                    $sql = "SELECT tt.id, tt.title FROM `link_agr_wtypes` AS lwt LEFT JOIN `task_types` AS tt ON lwt.wtype_id=tt.id WHERE lwt.agr_id=" .
                        $DB->F($agr_id) . ";";
                    $res = $DB->getCell2($sql);
                    $ids = false;
                    $titles = false;
                    foreach ($res as $key => $val)
                    {
                        $ids[] = $key;
                        $titles[] = $val;
                    }
                    $idsStr = implode(", ", $ids);
                    $titlesStr = implode(", ", $titles);
                    $tpl->setVariable("MAILTARGETS", implode(", ", getcfg('mailtoonnewticket')));
                    $tpl->setVariable("AGR_WTYPE_IDS", $idsStr);
                    $tpl->setVariable("AGR_WTYPE_VALS", $titlesStr);
                    $tpl->setVariable("PAYTYPE", $bn[$agreement["paytype"]]);
                    $tpl->setVariable("AGR_TP", $agreement["tp_contact"] ? $agreement["tp_contact"] :
                        "&mdash;");
                    $tpl->setVariable("AGR_STATUS", adm_statuses_plugin::getByID($agreement["status_id"]) ?
                        adm_statuses_plugin::getByID($agreement["status_id"]) : "&mdash;");
                    $tpl->setVariable("AGR_CONTR_TITLE", $r['contr_title']);
                    $tpl->setVariable("AGR_TITLE", $agreement["project"] ? $agreement["project"] :
                        "&mdash;");
                    $tpl->setVariable("AGR_SUBJECT", $agreement["agreement"] ? $agreement["agreement"] :
                        "&mdash;");
                    $tpl->setVariable("AGR_ACCESS", $agreement["access_contact"] ? $agreement["access_contact"] :
                        "&mdash;");
                    $tpl->setVariable("AGR_TYPE", $agreement["tp_contact"]);
                    $rdate = explode("-", $agreement["findate"]);
                    $tpl->setVariable("AGR_FINDATE", $agreement["findate"] ? $rdate[2] . "." . $rdate[1] .
                        "." . $rdate[0] : "&mdash;");
                    $tpl->setVariable("AGR_PROLONG", $agreement["prolong"] ? $prolong_ids[$agreement["prolong"]] :
                        "&mdash;");
                    $tpl->setVariable("AGR_SUMM", $agreement["ammount"] ? number_format($agreement["ammount"],
                        2, ".", " ") : "&mdash;");

                    $closedsumm =  $DB->getField("SELECT SUM(`ammount`) FROM `list_agr_act` WHERE `status`='89' AND `agr_id`=" .
                    $DB->F($agr_id) . ";");
                    $closedsumm = $closedsumm ? number_format($closedsumm, 2, ".", " ") : "&mdash;";

                    $tpl->setVariable("AGR_CLOSEDSUM", $closedsumm);

                    $sql = "SELECT * FROM `list_agr_ds` WHERE `agr_id`=" . $DB->F($agr_id) . ";";
                    $DB->query($sql);
                    if ($DB->errno())
                        UIError($DB->error());
                    if ($DB->num_rows())
                    {
                        $tpl->setVariable("DS_NO_RECORDS", "style='display: none;'");
                        while ($ds = $DB->fetch(true))
                        {
                            $tpl->setCurrentBlock("ds-list");
                            $tpl->setVariable("DSID", $ds["id"]);
                            $tpl->setVariable("DSNAME", $ds["number"] . " " . ($ds["sign_date"] ? " от " .
                                rudate("d M Y", strtotime($ds["sign_date"])) : ""));
                            $tpl->parse("ds-list");
                            $tpl->setCurrentBlock("ds1-list");
                            $tpl->setVariable("DS1ID", $ds["id"]);
                            $tpl->setVariable("DS1NAME", $ds["number"] . " " . ($ds["sign_date"] ? " от " .
                                rudate("d M Y", strtotime($ds["sign_date"])) : ""));
                            $tpl->parse("ds1-list");
                            $tpl->setCurrentBlock("ds-row");
                            $tpl->setVariable("DS_ID", $ds["id"]);
                            $tpl->setVariable("DS_NUM", $ds["number"]);
                            $tpl->setVariable("DS_SIMPLENAME", $ds["simplename"] ? $ds["simplename"] :
                                "&mdash;");
                            $tpl->setVariable("DS_SUBJECT", $ds["subject"] ? $ds["subject"] : "&mdash;");
                            $tpl->setVariable("DS_SIGNDATE", $ds["sign_date"] ? rudate("d M Y", strtotime($ds["sign_date"])) :
                                "&mdash;");
                            $tpl->setVariable("DS_FINDATE", $ds["fin_date"] ? rudate("d M Y", strtotime($ds["fin_date"])) :
                                "&mdash;");
                            $tpl->setVariable("DS_PRICE", $ds["ammount"] ? number_format($ds["ammount"], 2,
                                ".", " ") : "&mdash;");
                            $tpl->setVariable("DS_STSTE", $ds["status"] == "0" ? "Активен" : "Завершен");
                            $tot = $DB->getField("SELECT SUM(`ammount`) FROM `list_agr_act` WHERE `status`='89' AND `ds_id`=" . $DB->F($ds["id"]) . ";");
                            $tpl->setVariable("DS_TOTALAMMOUNT", number_format( (float)$tot, 2, ".", " "));
                            $sql = "SELECT * FROM `list_agr_files` WHERE `agr_id`=" . $DB->F($agr_id) .
                                " AND `file_type`='3' AND `ds_id`=" . $DB->F($ds["id"]) . ";";
                            $DB->query($sql);
                            $files = "";
                            if ($DB->num_rows())
                            {
                                while ($r = $DB->fetch(true))
                                {
                                    $files .= "<a href=/agreements/getfile?id=" . $r["id"] .
                                        "&name=" . urlencode($r["file_name"]) . ">" . $r["file_name"] . "</a><br />";
                                }
                            } else
                            {
                                $files = "-- файлы не загружены --";
                            }
                            $DB->free();
                            $tpl->setVariable("DS_FILES", $files);
                            $tpl->setVariable("DS_COMMENT", $ds["comment"] ? $ds["comment"] : "&mdash;");
                            $tpl->parse("ds-row");
                        }
                    }
                    $DB->free();

                    $tpl->setVariable("AWD_OPTION_LIST", adm_acts_wtypes_plugin::getOptions());

                    $sql = "SELECT * FROM `list_agr_act` WHERE `agr_id`=" . $DB->F($agr_id) . ";";
                    $DB->query($sql);
                    if ($DB->errno())
                        UIError($DB->error());
                    if ($DB->num_rows())
                    {
                        $tpl->setVariable("ACT_NO_RECORDS", "style='display: none;'");
                        while ($act = $DB->fetch(true))
                        {
                            $tpl->setCurrentBlock("act-list");
                            $tpl->setVariable("ACTID", $act["id"]);
                            $tpl->setVariable("ACTNAME", $act["number"] . " " . ($act["sign_date"] ? " от " .
                                rudate("d M Y", strtotime($act["sign_date"])) : ""));
                            $tpl->parse("act-list");
                            $tpl->setCurrentBlock("actrow");
                            $tpl->setVariable("STATUS_COLOR", adm_statuses_plugin::getStatusColorByID($act["status"]));
                            $tpl->setVariable("ACT_ID", $act["id"]);
                            $signed = adm_statuses_plugin::getStatusByTag("signed", "agr_acts");
                            if ($act["status"] != $signed["id"]) {
                                $tpl->setVariable("SHOW_AWD", "style='display: none;'");
                            }
                            $tpl->setVariable("ACT_SUMM_NF", $act["ammount"]);

                            $tpl->setVariable("ACT_NUM", $act["number"]);
                            $ds = $DB->getField("SELECT `number` FROM `list_agr_ds` WHERE `id`=" . $DB->F($act["ds_id"]) .
                                ";");
                            $tpl->setVariable("DS", $ds ? $ds : "&mdash;");
                            $tpl->setVariable("ACT_DATE", $act["sign_date"] ? rudate("d M Y", strtotime($act["sign_date"])) :
                                "&mdash;");
                            $tpl->setVariable("ACT_SUMM", number_format($act["ammount"], 2, ".", " "));
                            $sql = "SELECT * FROM `list_agr_files` WHERE `agr_id`=" . $DB->F($agr_id) .
                                " AND `file_type`='2' AND `act_id`=" . $DB->F($act["id"]) . ";";

                            $DB->query($sql);
                            $files = "";
                            if ($DB->num_rows())
                            {
                                while ($r = $DB->fetch(true))
                                {
                                    $files .= "<a href=/agreements/getfile?id=" . $r["id"] .
                                        "&name=" . urlencode($r["file_name"]) . ">" . $r["file_name"] . "</a><br />";
                                }
                            } else
                            {
                                $files = "-- файлы не загружены --";
                            }
                            $DB->free();
                            $tpl->setVariable("ACT_FILES", $files);
                            $tpl->setVariable("ACT_STATUS", adm_statuses_plugin::getByID($act["status"]));
                            $tpl->setVariable("ACT_COMMENT", $act["comment"] ? $act["comment"] : "&mdash;");
                            $tpl->parse("actrow");
                        }
                    }
                    $DB->free();
                    $type = false;
                    if ($agreement['type_price'])
                        $type[] = "Прайс";
                    if ($agreement['type_smeta'])
                        $type[] = "Смета";
                    if ($agreement['type_abon'])
                        $type[] = "Абонентская плата";
                    if ($type)
                    {
                        $tpl->setVariable("AGR_TYPE", implode("<br />", $type));
                    } else
                    {
                        $tpl->setVariable("AGR_TYPE", "<strong>&mdash;</strong>");
                    }

                    $sql = "SELECT * FROM `list_agr_price` WHERE `agr_id`=" . $agreement['id'] .
                        " ORDER BY `title`;";
                    $DB->query($sql);
                    if ($DB->errno())
                        UIError($DB->error());
                    if ($DB->num_rows())
                    {
                        while (list($id, $agr_id, $title, $stitle, $price_type, $price_val, $p_comment, $archive) =
                            $DB->fetch())
                        {
                            $tpl->setCurrentBlock("row");
                            $tpl->setVariable("JT_ID", $id);
                            $tpl->setVariable("JT_TITLE", $title);
                            $tpl->setVariable("JT_SHORT_TITLE", $stitle);
                            $tpl->setVariable("JT_PRICETYPE", $price_type);
                            $tpl->setVariable("JT_TAG", $price_type == "smeta" ? "Смета" : "Прайс");
                            $tpl->setVariable("JT_PRICE", $price_type != "price" ? "100%" : $price_val);
                            $tpl->setVariable("JT_COMMENT", $p_comment);
                            $tpl->setVariable("JT_IS_ARCHIVE", $archive);
                            $tpl->setVariable("JT_ISARCHIVE", $archive>0 ? "Да" : "");
                            $tpl->parse("row");
                        }
                    } else
                    {
                        $tpl->touchBlock("no-rec");
                    }
                    $DB->free();

                } else
                {
                    UIError("Договор с указанным ID не найден!");
                }
            } else
            {

            }
            $tpl->setVariable("AGRLINKTOCONTR", "view?contr_id=" . $contr_id);

        } else
        {
            UIError("Не указан ID контрагента для добавления договора!");
        }

        UIHeader($tpl);
        $tpl->show();


    }

    static function saveawd_ajax() {
        global $DB, $USER;
        $act_id = $_REQUEST["awd-act_id"];
        $quant = $_REQUEST["awd-quant"];
        $amm = $_REQUEST["awd-ammount"];
        $awdname = $_REQUEST["awd-names"];
        if (!$act_id || !$quant || $amm=='' || !$awdname) {
            $ret["error"]= "Недостаточно данных для выполнения операции.";
        } else {
            $sql = "SELECT * FROM `list_acts_wtype_data` WHERE `wtype_id`=".$DB->F($awdname)." AND `act_id`=".$DB->F($act_id).";";
            $r = $DB->getRow($sql, true);
            if ($r) {
                $sql = "UPDATE `list_acts_wtype_data` SET `wtype_count`=".$DB->F($quant + $r["wtype_count"]).", `wtype_ammount`=".$DB->F($amm + $r["wtype_ammount"])." WHERE `id`=".$DB->F($r["id"])."";
            } else {
                $sql = "INSERT INTO `list_acts_wtype_data` (`act_id`, `wtype_id`, `wtype_count`, `wtype_ammount`) 
                    VALUES (".$DB->F($act_id).", ".$DB->F($awdname).", ".$DB->F($quant).", ".$DB->F($amm).");";
            }
            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
            } else {
                $sql = "SELECT awd.id, (SELECT `title` FROM `list_acts_wtypes` WHERE `id`=awd.wtype_id) as wtype_name, (SELECT `edizm` FROM `list_acts_wtypes` WHERE `id`=awd.wtype_id) as wtype_edizm_id, awd.wtype_count, awd.wtype_ammount FROM `list_acts_wtype_data` as awd WHERE awd.act_id=".$DB->F($act_id).";";
                $DB->query($sql);
                $tpl = new HTML_Template_IT(path2("plugins/agreements"));
                $tpl->loadTemplatefile($USER->getTemplate()."/awd.tmpl.htm");

                if (!$DB->num_rows()) {
                    $tpl->touchBlock("no-awd");
                } else {
                    while ($r = $DB->fetch(true)) {
                        $tpl->setCurrentBlock("awd");
                        $tpl->setVariable("AWD_ACT_ID", $act_id);
                        $tpl->setVariable("AWD_ID", $r["id"]);
                        $tpl->setVariable("AWD_NAME", $r["wtype_name"]);
                        $tpl->setVariable("AWD_QUANT", $r["wtype_count"]." ". $DB->getField("SELECT `title` FROM `list_skp_p_edizm` WHERE `id`=".$DB->F($r["wtype_edizm_id"]).""));
                        $tpl->setVariable("AWD_AMMOUNT", $r["wtype_ammount"]);
                        $tpl->parse("awd");
                    }
                }
                $ret["ok"] = "ok";
                $ret["tpl"] = $tpl->get();
            }
        }
        echo json_encode($ret);
        return false;
    }

    static function deleteawd_ajax() {
        global $DB, $USER;
        $id = $_REQUEST["target_id"];
        $act_id = $_REQUEST["act_id"];
        if (!$id || !$act_id) {
            $ret["error"] = "Недостаточно данных для выполнения операции.";
        } else {
            $sql = "DELETE FROM `list_acts_wtype_data` WHERE `id`=".$DB->F($id).";";
            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
            } else {
                $sql = "SELECT awd.id, (SELECT `title` FROM `list_acts_wtypes` WHERE `id`=awd.wtype_id) as wtype_name, (SELECT `edizm` FROM `list_acts_wtypes` WHERE `id`=awd.wtype_id) as wtype_edizm_id, awd.wtype_count, awd.wtype_ammount FROM `list_acts_wtype_data` as awd WHERE awd.act_id=".$DB->F($act_id).";";
                $DB->query($sql);
                $tpl = new HTML_Template_IT(path2("plugins/agreements"));
                $tpl->loadTemplatefile($USER->getTemplate()."/awd.tmpl.htm");

                if (!$DB->num_rows()) {
                    $tpl->touchBlock("no-awd");
                } else {
                    while ($r = $DB->fetch(true)) {
                        $tpl->setCurrentBlock("awd");
                        $tpl->setVariable("AWD_ACT_ID", $act_id);
                        $tpl->setVariable("AWD_ID", $r["id"]);
                        $tpl->setVariable("AWD_NAME", $r["wtype_name"]);
                        $tpl->setVariable("AWD_QUANT", $r["wtype_count"]." ". $DB->getField("SELECT `title` FROM `list_skp_p_edizm` WHERE `id`=".$DB->F($r["wtype_edizm_id"]).""));
                        $tpl->setVariable("AWD_AMMOUNT", $r["wtype_ammount"]);
                        $tpl->parse("awd");
                    }
                }
                $ret["ok"] = "ok";
                $ret["tpl"] = $tpl->get();
            }
        }
        echo json_encode($ret);
        return false;
    }


    function editagreement()
    {
        global $DB, $USER;
        $prolong = array(
            0 => "-- все",
            1 => "Необходима пролонгация",
            2 => "Автоматическая пролонгация",
            3 => "Нет пролонгации");
        $prolong1 = array(

            1 => "Необходима пролонгация",
            2 => "Автоматическая пролонгация",
            3 => "Нет пролонгации");
        $bn = array(
            2 => "Безналичный расчет",
            1 => "Наличный расчет",
        );
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("editagr.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/editagr.tmpl.htm");
        else
            $tpl->loadTemplatefile("editagr.tmpl.htm");


        $cType = false;
        if ($contr_id = $_REQUEST['contr_id'])
        {
            $sql = "SELECT `contr_title`, `official_title`, `contr_type`, `contr_type2`, `is_client`, `is_supplier`, `contr_type3`, `contr_type4`, contr_type_tm FROM `list_contr` WHERE `id`=" .
                $DB->F($contr_id) . ";";
            $r = $DB->getRow($sql, true);
            if ($r)
            {
                $tpl->setVariable("AGR_CONTR_TITLE", $r['official_title'] . " (" . $r['contr_title'] . ")");
                $tpl->setVariable("CONTR_ID", $contr_id);
                if ($r['is_client'])
                    $tpl->setVariable('CONTR_TYPE_CLIENT', "checked='checked'");
                if ($r['is_supplier'])
                    $tpl->setVariable('CONTR_TYPE_SUPPLIER', "checked='checked'");
            } else
            {
                UIError("Ошибка определения Контрагента для добавления договора!");
            }
            if ($r['contr_type'] == 0 && $r['contr_type2'] == 0 && !$r['contr_type3'] == 0 && !$r['contr_type4'] == 0 && !$r['contr_type_tm'])
            {
                UIError("Не определен тип Контрагента!!  Выберите тип Контрагента в его редакторе, а затем добавляйте договор!");
            }

            if ($r["contr_type"]>0) $cType[] = 1;
            if ($r["contr_type2"]>0) $cType[] = 2;
            if ($r["contr_type3"]>0) $cType[] = 3;
            if ($r["contr_type4"]>0) $cType[] = 4;
            if ($r["contr_type_tm"]>0) $cType[] = 5;

            if (sizeof($cType))
                $tpl->setVariable("CTYPE", implode("", (array)$cType));
            else
                $tpl->setVariable("CTYPE", '0');
            $tpl->setVariable("PROLONGLIST", array2options($prolong1));
            $tpl->setVariable("STATUSLIST", adm_statuses_plugin::getOptList($cAgr["status_id"], "agreements"));
            $agr_id = $_REQUEST['id'];
            if ($agr_id)
            {

                $tpl->setVariable("AGR_ID", $agr_id);
                $cAgr = $DB->getRow("SELECT * FROM `list_contr_agr` WHERE `id`=".$DB->F($agr_id).";", true);
                if ($cAgr)
                {
                    $tpl->setVariable("MAILTARGETS", implode(", ", getcfg('mailtoonnewticket')));
                    $tpl->setVariable("AGR_NUM", $cAgr["number"]);
                    $rdate = explode("-", $cAgr["agr_date"]);
                    $tpl->setVariable("AGR_DATE", $rdate[2] . "." . $rdate[1] . "." . $rdate[0]);
                    $tpl->setVariable("AGR_COMMENT", $cAgr["coment"]);
                    $sql = "SELECT tt.id as keyval, tt.title, lwt.is_qc FROM `link_agr_wtypes` AS lwt LEFT JOIN `task_types` AS tt ON lwt.wtype_id=tt.id WHERE lwt.agr_id=" .
                        $DB->F($agr_id) . ";";
                    //// EF: добавляем новую птицу - включать в обзвон или нет
                    #$res = $DB->getCell2($sql);
                    #$ids = false;
                    #$titles = false;
                    #foreach ($res as $key => $val)
                    #{
                    #    $ids[] = $key;
                    #    $titles[] = $val;
                    #}

                    $res = $DB->getCell3($sql);
                    $ids = false;
                    $titles = false;
                    $is_qcs = false;
                    foreach ($res as $key => $val) {
                        $ids[] = $key;
                        $titles[] = $val[0]['title'];
                        $is_qcs[] = $val[0]['is_qc'];
                    }

                    #echo "<pre>\n";
                    #print_r($res);
                    #print_r($ids);
                    #print_r($titles);
                    #print_r($is_qcs);
                    #echo "</pre>\n";
                    #exit();

                    $idsStr = implode(", ", $ids);
                    $titlesStr = implode(", ", $titles);
                    $is_qcStr = implode(", ", $is_qcs);
                    $tpl->setVariable("AGR_WTYPE_IDS", $idsStr);
                    $tpl->setVariable("AGR_WTYPE_VALS", $titlesStr);
                    $tpl->setVariable("AGR_WTYPE_QCS", $is_qcStr);
                    $tpl->setVariable("AGR_ACCESS", $cAgr["access_contact"]);
                    $tpl->setVariable("AGR_TP", $cAgr["tp_contact"]);
                    if ($cAgr['type_price'])
                        $tpl->setVariable("TYPE_PRICE", "checked='checked'");
                    if ($cAgr['type_smeta'])
                        $tpl->setVariable("TYPE_SMETA", "checked='checked'");
                    if ($cAgr['type_abon'])
                        $tpl->setVariable("TYPE_ABON", "checked='checked'");
                    $tpl->setVariable("PROLONGLIST", array2options($prolong, $cAgr["prolong"]));
                    $tpl->setVariable("AGR_FINDATE", $cAgr["findate"] ? date("d.m.Y", strtotime($cAgr["findate"])) : "");
                    $tpl->setVariable("STATUSLIST", adm_statuses_plugin::getOptList($cAgr["status_id"], "agreements"));
                    $tpl->setVariable("AGR_PRICE", $cAgr["ammount"]);
                    $tpl->setVariable("AGR_SUBJECT", $cAgr["agreement"]);
                    $tpl->setVariable("AGR_SIMPLENAME", $cAgr["project"]);
                    if ($cAgr["mailonlk"]) {
                        $tpl->setVariable("MAILONLK", "checked='checked'");
                    }
                    $tpl->setVariable("MAILONLK_ADDR", implode(',', (array)getcfg('mailtoonnewticket')));
                    //$tpl->setVariable("", $cAgr[""]);
                    $DB->query("SELECT * FROM `list_agr_tpphones` WHERE `agr_id`=".$DB->F($agr_id).";");
                    $tpl->setCurrentBlock("tpnum");
                    if ($DB->num_rows()) {
                        $tpl->setVariable("TPNUMNOITEMSHIDE", "style='display:none;'");
                        while ($r = $DB->fetch(true)) {
                            $tpl->setCurrentBlock("tpnumslist");
                            $tpl->setVariable("TPNUMID", $r["id"]);
                            $tpl->setVariable("TPNUMNUM", $r["phone"]);
                            $tpl->setVariable("TPNUMEXT", $r["ext"]);
                            $tpl->setVariable("TPNUMTITLE", $r["phonetitle"]);
                            $tpl->parse("tpnumslist");
                        }
                    } else {
                        $tpl->setVariable("TPNUMNOITEMSHIDE", " ");
                    }
                    $tpl->parse("tpnum");


                } else
                {
                    UIError("Договор с указанным ID не найден!");
                }
            } else
            {

            }

            $tpl->setVariable("AGRLINKTOCONTR", "view?contr_id=" . $contr_id);

        } else
        {
            $tpl->setVariable("MAILTARGETS", implode(", ", getcfg('mailtoonnewticket')));
            $tpl->setVariable("NEWAGR", "style='display:none;'");
            $tpl->setVariable("PROLONGLIST", array2options($prolong1, $cAgr["prolong"]));
            $tpl->setVariable("STATUSLIST", adm_statuses_plugin::getOptList($cAgr["status_id"], "agreements"));
            $tpl->setCurrentBlock("contr-list");
            $tpl->setVariable("FILTER_CONTR_OPTIONS", kontr_plugin::getOptListForAgr());
            $tpl->parse("contr-list");
        }
        $tpl->setVariable("AGR_PAYTYPE", array2options($bn, $cAgr["paytype"]));
        UIHeader($tpl);
        $tpl->show();
    }

    function savetpnum_ajax() {
        global $DB, $USER;
        if ($_REQUEST["tpnumid"]) {
            $sql = "UPDATE `list_agr_tpphones` SET `agr_id`=".$DB->F($_REQUEST["agr_id"]).",
            `phone`=".$DB->F($_REQUEST["phonenum"]).", `ext`=".$DB->F($_REQUEST["phoneext"]).", `phonetitle`=".$DB->F($_REQUEST["phonetitle"])." WHERE `id`=".$DB->F($_REQUEST["tpnumid"]).";";
        } else {
            $sql = "INSERT INTO `list_agr_tpphones` (`agr_id`, `phone`, `ext`, `user_id`, `phonetitle`) 
            VALUES (".$DB->F($_REQUEST["agr_id"]).", ".$DB->F($_REQUEST["phonenum"]).", ".$DB->F($_REQUEST["phoneext"]).", ".$DB->F($USER->getId()).", ".$DB->F($_REQUEST["phonetitle"]).");";
        }
        $DB->query($sql);
        if ($_REQUEST["tpnumid"]) {
            $item_id = $_REQUEST["tpnumid"];
            $ret["item_id"] = $item_id;
        } else {
            $item_id = $DB->insert_id();
        }
        if ($DB->errno()) $ret["error"] = $DB->error()." ".$sql; else {
            $ret['ok'] = "ok";
            $ret["tpl"] = "<tr class=\"tpnrow row{$item_id}\">
                <td width=\"5%\"><a href=\"#\" class=\"edittpnum\" id=\"{$item_id}\" ext=\"{$_REQUEST["phoneext"]}\" phone=\"{$_REQUEST["phonenum"]}\" phonetitle=\"".$_REQUEST["phonetitle"]."\">{$item_id}</a></td>
                <td widht=\"30%\" nowrap='nowrap'><a href=\"#\" class=\"edittpnum\" id=\"{$item_id}\" ext=\"{$_REQUEST["phoneext"]}\" phone=\"{$_REQUEST["phonenum"]}\" phonetitle=\"".$_REQUEST["phonetitle"]."\">{$_REQUEST["phonenum"]}</a></td>
                <td width=\"30%\">{$_REQUEST["phoneext"]}</td>
                <td width=\"30%\">".$_REQUEST["phonetitle"]."</td>
                <td><a class=\"removetpnum\" href=\"#\" id=\"{$item_id}\"><img src=\"templates/images/cross.png\" /></a></td></tr>";
        }
        echo json_encode($ret);
        die();
    }

    function deletetpnum_ajax() {
        global $DB, $USER;
        if ($_REQUEST["tpnumid"]) {
            $sql = "DELETE FROM `list_agr_tpphones` WHERE `id`=".$DB->F($_REQUEST["tpnumid"]).";";
            $DB->query($sql);
            if ($DB->errno()) $ret["error"] = $DB->error(); else $ret['ok'] = "ok";
        } else $ret["error"] = "Не указан идентификатор записи";
        echo json_encode($ret);
        die();
    }

    static function getTPPhones($agr_id) {
        global $DB, $USER;
        if (!$agr_id) return false;
        $sql = "SELECT `id`, `phone`, `ext`, `phonetitle` FROM `list_agr_tpphones` WHERE `agr_id`=".$DB->F($agr_id).";";
        $rc = $DB->query($sql);
        if ($DB->num_rows()) {
            while ($item = $DB->fetch(true)) {
                $ret[] = $item;
            }
            return $ret;
        } else {
            return false;
        }
    }

    static function requireMailOnNewTicket($agr_id) {
        global $USER, $DB;
        if (!$agr_id) return false;
        return $DB->getField("SELECT `mailonlk` FROM `list_contr_agr` WHERE `id`=".$DB->F($agr_id).";");
    }

    function deleteagreement()
    {
        global $DB;

        $agr_id = $_REQUEST["agr_id"];
        $contr_id = $_REQUEST["contr_id"];
        $sql = "SELECT COUNT(`task_id`) FROM `tickets` WHERE `agr_id`=" . $DB->F($agr_id) .
            ";";
        $fCnt = $DB->getField($sql);
        if (!$fCnt)
        {
            if ($agr_id && $contr_id)
            {
                $sql = "DELETE FROM `link_agr_wtypes` WHERE `agr_id`=" . $DB->F($agr_id) . ";";
                $DB->query($sql);
                if ($DB->errno())
                    UIError($DB->error() . $sql);
                $sql = "DELETE FROM `list_contr_agr` WHERE `id`=" . $DB->F($agr_id) . ";";
                $DB->query($sql);
                if ($DB->errno())
                    UIError($DB->error() . $sql);
                redirect($this->getLink('view') . "?contr_id=" . $contr_id,
                    "Договор успешно удален. ID: " . $agr_id);
            } else
            {
                UIError("Не указан ID договора для удаления!");

            }
        } else
        {
            UIError("Договор привязан к действующим заявкам. Удаление невозможно.");
        }

    }


}

?>
