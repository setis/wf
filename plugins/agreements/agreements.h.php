<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Договоры";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Договоры";
    $PLUGINS[$plugin_uid]['events']['viewagreement'] = "Просмотр Договора";
    $PLUGINS[$plugin_uid]['events']['getds_ajax'] = "Загрузка ДС";
    $PLUGINS[$plugin_uid]['events']['getact_ajax'] = "Загрузка Актов";
    $PLUGINS[$plugin_uid]['events']['getdsbyid_ajax'] = "Загрузка ДС по ID";
    $PLUGINS[$plugin_uid]['events']['getactbyid_ajax'] = "Загрузка Акта по ID";    
    $PLUGINS[$plugin_uid]['events']['getwtypeactbyid_ajax'] = "Загрузка видов работ акта по ID";    
}

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['saveagreement'] = "Сохранить Договор";
    $PLUGINS[$plugin_uid]['events']['editagreement'] = "Редактор Договора";
    $PLUGINS[$plugin_uid]['events']['deleteagreement'] = "Удалить Договор";
    $PLUGINS[$plugin_uid]['events']['jobtype_edit'] = "Редактирование элемента прайса контрагентов (вида работ)";
    $PLUGINS[$plugin_uid]['events']['jobtype_save'] = "Сохранение элемента прайса контрагентов (вида работ)";
    $PLUGINS[$plugin_uid]['events']['jobtype_del'] = "Удаление элемента прайса контрагентов (вида работ)";
    $PLUGINS[$plugin_uid]['events']['saveplanitem_ajax'] = "Сохранение эл-та плана премий";
    $PLUGINS[$plugin_uid]['events']['getplanitem_ajax'] = "Получение эл-та плана премий";
    $PLUGINS[$plugin_uid]['events']['deleteplanitem_ajax'] = "Удаление эл-та плана премий";
    $PLUGINS[$plugin_uid]['events']['getAgrPlan'] = "Получение массива плана расчета премии по договору";
    $PLUGINS[$plugin_uid]['events']['addfile_ajax'] = "Загрузка файлов";
    $PLUGINS[$plugin_uid]['events']['files_ajax'] = "Список файлов";
    $PLUGINS[$plugin_uid]['events']['delete_file_ajax'] = "Удаление файлов";
    $PLUGINS[$plugin_uid]['events']['getfile'] = "Удаление файлов";
    $PLUGINS[$plugin_uid]['events']['saveds_ajax'] = "Сохранение ДС";
    $PLUGINS[$plugin_uid]['events']['saveact_ajax'] = "Сохранение Акта";
    $PLUGINS[$plugin_uid]['events']['deleteds_ajax'] = "Удаление ДС";
    $PLUGINS[$plugin_uid]['events']['deleteact_ajax'] = "Удаление Акта";
    $PLUGINS[$plugin_uid]['events']['deletetpnum_ajax'] = "Удаление Телефона ТП";
    $PLUGINS[$plugin_uid]['events']['savetpnum_ajax'] = "Сохранение Телефона ТП";
    $PLUGINS[$plugin_uid]['events']['saveawd_ajax'] = "Сохранение вида работ с суммой по Акту";
    $PLUGINS[$plugin_uid]['events']['deleteawd_ajax'] = "Удаление вида работ с суммой по Акту";
    $PLUGINS[$plugin_uid]['events']['viewlist_export'] = "Выгрузка в Excel";
    
}


$plugin_uid = "agr_acts";

$PLUGINS[$plugin_uid]['name'] = "Акты к договорам";
$PLUGINS[$plugin_uid]['hidden'] = true;

?>