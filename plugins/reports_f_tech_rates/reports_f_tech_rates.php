<?php
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\tickets\AccidentTicket;
use classes\tickets\ServiceTicket;
use classes\tickets\ConnectionTicket;

function aksort(&$array, $valrev = false, $keyrev = false)
{
    if ($valrev) {
        arsort($array);
    } else {
        asort($array);
    }
    $vals = array_count_values($array);
    $i = 0;
    foreach ($vals AS $val => $num) {
        $first = array_splice($array, 0, $i);
        $tmp = array_splice($array, 0, $num);
        if ($keyrev) {
            krsort($tmp);
        } else {
            ksort($tmp);
        }
        $array = array_merge($first, $tmp, $array);
        unset($tmp);
        $i = $num;
    }
}

class reports_f_tech_rates_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function main()
    {
        global $DB, $USER;
        $fintypes = array("connections" => "Подключение", "services" => "СКП", "accidents" => "ТТ");
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01' => "Январь", '02' => "Февраль", '03' => "Март", '04' => "Апрель", '05' => "Май", '06' => "Июнь", '07' => "Июль", '08' => "Август", '09' => "Сентябрь", '10' => "Октябрь", '11' => "Ноябрь", '12' => "Декабрь");

        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
            else
                $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_TECH", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            //$tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            //$tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id"]));
            $tpl->setVariable("FILTER_CONTR_OPTIONS", kontr_plugin::getOptListTasks($_REQUEST["contr_id"]));
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date('Y-m-d') . " - 1 month")));
            $tpl->setVariable("DATE_TO", @$_POST['dateto'] ? @$_POST['dateto'] : date("d.m.Y"));

            UIHeader($tpl);
        }

        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default")
                $rtpl->loadTemplatefile($USER->getTemplate() . "/report.tmpl.htm");
            else
                $rtpl->loadTemplatefile("report.tmpl.htm");

            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $datefrom = @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date('Y-m-d') . " - 1 month"));
            $dateto = @$_POST['dateto'] ? @$_POST['dateto'] : date("d.m.Y");
            $rtpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date('Y-m-d') . " - 1 month")));
            $rtpl->setVariable("DATE_TO", @$_POST['dateto'] ? @$_POST['dateto'] : date("d.m.Y"));
            if ($sc_id) {
                $sc = adm_sc_plugin::getSC($sc_id);
                $rtpl->setVariable("REP_SCLIST", $sc["title"]);
            } else {
                $rtpl->setVariable("REP_SCLIST", "Все");
            }

            if ($datefrom > $dateto) {
                $rtpl->touchBlock("no-rows");
            } else {
                $sc_id = $_REQUEST["sc_id"];
                $contr = $_REQUEST["contr_id"];

//                    $empl_list = adm_empl_plugin::getEmplListRowsBySCID($scid);
                if ($sc_id > 0) $add = " AND `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($sc_id) . ") ";
                $query = "SELECT
                        `id`,
                        `fio`
                    FROM
                        `users`
                    WHERE
                        id NOT IN (
                            SELECT `virtual_id` FROM `virtual_techs`
                        ) AND
                        `active` AND
                        `id` IN (
                            SELECT `user_id` FROM `list_empl`
                        )
                        $add
                    ORDER BY
                        `fio`";
                $empl_list = $DB->getCell2($query);
                if ($empl_list) {
                    if ($contr)
                        $sql_add = " AND tick.cnt_id=" . $DB->F($contr) . " ";
                    $used[] = array();
                    foreach ($empl_list as $techid => $fio) {
                        if (!in_array($techid, $used)) {
                            $qc_techList = array();
                            $used[] = $techid;
                            $qc_techList[] = $techid;
                            $virtAlias = adm_empl_plugin::isVirtual($techid);
                            if ($virtAlias) {
                                $qc_techList[] = $virtAlias;
                                $used[] = $virtAlias;
                            }
                            $sql = "SELECT
                                        SUM(case when tqc.qc_new_result>0 and t.plugin_uid='accidents' then tqc.qc_new_result else 0 end) as sum_tt,
                                        SUM(case when tqc.qc_new_result>0 and t.plugin_uid='accidents' then 1 else 0 end) as count_tt,
                                        SUM(case when tqc.qc_new_result>0 and t.plugin_uid='services' then tqc.qc_new_result else 0 end) as sum_skp,
                                        SUM(case when tqc.qc_new_result>0 and t.plugin_uid='services' then 1 else 0 end) as count_skp,
                                        SUM(case when tqc.qc_new_result>0 and t.plugin_uid='connections' then tqc.qc_new_result else 0 end) as sum_conn,
                                        SUM(case when tqc.qc_new_result>0 and t.plugin_uid='connections' then 1 else 0 end) as count_conn,
                                        SUM(case when tqc.qc_new_result>0 then tqc.qc_new_result else 0 end) as sum_total,
                                        SUM(case when tqc.qc_new_result>0 then 1 else 0 end) as count_total,
                                        IF(SUM(case when tqc.qc_new_result>0 then 1 else 0 end)>0, round(SUM(case when tqc.qc_new_result>0 then tqc.qc_new_result else 0 end)/SUM(case when tqc.qc_new_result>0 then 1 else 0 end)), 3) as avg_total,
                                        '$techid' as empl,
                                        '$fio' as empl_name,
                                        tick.sc_id,
                                        (SELECT `title` FROM `list_sc` WHERE `id`=tick.sc_id) as sc_name
                                    FROM `tickets_qc` AS tqc 
                                        LEFT JOIN `tasks` AS t ON (t.id=tqc.task_id) 
                                        LEFT JOIN `tickets` AS tick ON (tick.task_id=tqc.task_id) 
                                        LEFT JOIN `gfx` AS gfx ON gfx.task_id=t.id
                                     WHERE 
                                         DATE_FORMAT(tqc.qc_date, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($datefrom))) . " AND DATE_FORMAT(tqc.qc_date, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($dateto))) . "
                                         AND tqc.task_id IN (SELECT `task_id` FROM `gfx` WHERE `empl_id` IN (" . implode(",", $qc_techList) . "))
                                         AND tqc.qc_status=1
                                         $sql_add 
                                         AND DATE_FORMAT(tqc.qc_date, '%Y-%m-%d')>='2015-07-09'
                                     GROUP BY empl
                                     ORDER BY avg_total DESC";
                            $m = $DB->getRow($sql, true);
                            if ($m) {
                                $sc_res[$m["empl_name"]] = $m;
                                $rating[$m["empl_name"]] = $m["avg_total"];

                            }
                            $DB->free();
                        }

                    }
                    if ($sc_res) {
                        aksort($rating, true);
                        foreach ($rating as $techid => $item) {

                            $rtpl->setCurrentBlock("rep_row");
                            $rtpl->setVariable("RR_FIOORSC", $sc_res[$techid]["empl_name"]);
                            $rtpl->setVariable("RR_SC", $sc_res[$techid]["sc_name"]);
                            $rtpl->setVariable("RR_TOTALCOUNT", $sc_res[$techid]["count_tt"] + $sc_res[$techid]["count_skp"] + $sc_res[$techid]["count_conn"]);
                            $sc_total_count += ($sc_res[$techid]["count_tt"] + $sc_res[$techid]["count_skp"] + $sc_res[$techid]["count_conn"]);
                            $midres_tt = '';
                            $midres_skp = '';
                            $midres_conn = '';
                            $mrtt = 0;
                            $mrskp = 0;
                            $mrconn = 0;
                            $mrconncount = 0;
                            $mrskpcount = 0;
                            $mrttcount = 0;
                            $divider = 0;
                            if ($sc_res[$techid]["count_tt"]) {
                                $midres_tt = round($sc_res[$techid]["sum_tt"] / $sc_res[$techid]["count_tt"], 3);
                                $mrtt += $sc_res[$techid]["sum_tt"];
                                $mrttcount += $sc_res[$techid]["count_tt"];
                                $sc_mid_tt += $midres_tt;
                                $sc_mid_tt_count += 1;
                                $divider += 1;
                            }
                            if ($sc_res[$techid]["count_skp"]) {
                                $midres_skp = round($sc_res[$techid]["sum_skp"] / $sc_res[$techid]["count_skp"], 3);
                                $mrskp += $sc_res[$techid]["sum_skp"];
                                $mrskpcount += $sc_res[$techid]["count_skp"];
                                $sc_mid_skp += $midres_skp;
                                $sc_mid_skp_count += 1;
                                $divider += 1;
                            }
                            if ($sc_res[$techid]["count_conn"]) {
                                $midres_conn = round($sc_res[$techid]["sum_conn"] / $sc_res[$techid]["count_conn"], 3);
                                $sc_mid_conn += $midres_conn;
                                $mrconn += $sc_res[$techid]["sum_conn"];
                                $mrconncount += $sc_res[$techid]["count_conn"];
                                $sc_mid_conn_count += 1;
                                $divider += 1;
                            }
                            $rtpl->setVariable("RR_SKP", $midres_skp);
                            if ($midres_skp != "" && $midres_skp > 0) {
                                if ($midres_skp == 100) {
                                    $rtpl->setVariable("SKP_STYLE", "style='background: #B0FFB7; font-weight: bold; text-align:center;' title='Отлично'");
                                } else {
                                    if ($midres_skp >= 90 && $midres_skp <= 99.99) {
                                        $rtpl->setVariable("SKP_STYLE", "style='background: #00ffd2; font-weight: bold; text-align:center;' title='Удовлетворительно'");
                                    } else {
                                        if ($midres_skp >= 86 && $midres_skp <= 89.99) {
                                            $rtpl->setVariable("SKP_STYLE", "style='background: #fff600; font-weight: bold; text-align:center;' title='Плохо'");
                                        } else {

                                            if ($midres_skp < 86)
                                                $rtpl->setVariable("SKP_STYLE", "style='background: #FF0000; color: #FFFFFF; font-weight: bold; text-align:center;'  title='Очень плохо'");

                                        }
                                    }
                                }
                            }
                            $rtpl->setVariable("RR_CONN", $midres_conn);
                            if ($midres_conn != "" && $midres_conn > 0) {
                                if ($midres_conn == 100) {
                                    $rtpl->setVariable("CONN_STYLE", "style='background: #B0FFB7; font-weight: bold; text-align:center;' title='Отлично'");
                                } else {
                                    if ($midres_conn >= 90 && $midres_conn <= 99.99) {
                                        $rtpl->setVariable("CONN_STYLE", "style='background: #00ffd2; font-weight: bold; text-align:center;' title='Удовлетворительно'");
                                    } else {
                                        if ($midres_conn >= 86 && $midres_conn <= 89.99) {
                                            $rtpl->setVariable("CONN_STYLE", "style='background: #fff600; font-weight: bold; text-align:center;' title='Плохо'");
                                        } else {

                                            if ($midres_conn < 86)
                                                $rtpl->setVariable("CONN_STYLE", "style='background: #FF0000; color: #FFFFFF; font-weight: bold; text-align:center;'  title='Очень плохо'");

                                        }
                                    }
                                }
                            }
                            $rtpl->setVariable("RR_TT", $midres_tt);
                            if ($midres_tt != "" && $midres_tt > 0) {
                                if ($midres_tt == 100) {
                                    $rtpl->setVariable("TT_STYLE", "style='background: #B0FFB7; font-weight: bold; text-align:center;' title='Отлично'");
                                } else {
                                    if ($midres_tt >= 90 && $midres_tt <= 99.99) {
                                        $rtpl->setVariable("TT_STYLE", "style='background: #00ffd2; font-weight: bold; text-align:center;' title='Удовлетворительно'");
                                    } else {
                                        if ($midres_tt >= 86 && $midres_tt <= 89.99) {
                                            $rtpl->setVariable("TT_STYLE", "style='background: #fff600; font-weight: bold; text-align:center;' title='Плохо'");
                                        } else {

                                            if ($midres_tt < 86)
                                                $rtpl->setVariable("TT_STYLE", "style='background: #FF0000; color: #FFFFFF; font-weight: bold; text-align:center;'  title='Очень плохо'");

                                        }
                                    }
                                }
                            }
                            $midval = $divider > 0 ? round(($mrconn + $mrskp + $mrtt) / ($mrconncount + $mrskpcount + $mrttcount), 3) : "";
                            $sc_midval += ($divider > 0 ? $midval : 0);
                            $sc_midval_count += ($divider > 0 ? 1 : 0);
                            $rtpl->setVariable("RR_TOTALMID", $midval);
                            if ($midval != "" && $midval > 0) {
                                if ($midval == 100) {
                                    $rtpl->setVariable("TOTAL_STYLE", "style='background: #B0FFB7; font-weight: bold; text-align:center;' title='Отлично'");
                                } else {
                                    if ($midval >= 90 && $midval <= 99.99) {
                                        $rtpl->setVariable("TOTAL_STYLE", "style='background: #00ffd2; font-weight: bold; text-align:center;' title='Удовлетворительно'");
                                    } else {
                                        if ($midval >= 86 && $midval <= 89.99) {
                                            $rtpl->setVariable("TOTAL_STYLE", "style='background: #fff600; font-weight: bold; text-align:center;' title='Плохо'");
                                        } else {
                                            if ($midval < 86)
                                                $rtpl->setVariable("TOTAL_STYLE", "style='background: #FF0000; color: #FFFFFF; font-weight: bold; text-align:center;'  title='Очень плохо'");

                                        }
                                    }
                                }
                            }
                            $rtpl->parse("rep_row");
                            //}

                            //}

                        }
                        $total_tt = round($sc_mid_tt / $sc_mid_tt_count, 3);
                        $divider = 0;
                        if ($total_tt) {
                            $divider += 1;
                        }
                        $total_skp = round($sc_mid_skp / $sc_mid_skp_count, 3);
                        if ($total_skp) {
                            $divider += 1;
                        }
                        $total_conn = round($sc_mid_conn / $sc_mid_conn_count, 3);
                        if ($total_conn) {
                            $divider += 1;
                        }
                        //$total_count =  //+= ($sc_mid_conn_count+$sc_mid_skp_count+$sc_mid_tt_count);


                        if ($divider > 0)
                            $final = round(($total_tt + $total_skp + $total_conn) / $divider, 3);
                        $rtpl->setCurrentBlock("rep_footer");
                        $rtpl->setVariable("TOTAL_SKP", $total_skp ? $total_skp : "");
                        if ($total_skp != "" && $total_skp > 0) {
                            if ($total_skp == 100) {
                                $rtpl->setVariable("TOTAL_SKP_STYLE", "style='background: #B0FFB7; font-weight: bold; text-align:center;' title='Отлично'");
                            } else {
                                if ($total_skp >= 90 && $total_skp <= 99.99) {
                                    $rtpl->setVariable("TOTAL_SKP_STYLE", "style='background: #00ffd2; font-weight: bold; text-align:center;' title='Удовлетворительно'");
                                } else {
                                    if ($total_skp >= 86 && $total_skp <= 89.99) {
                                        $rtpl->setVariable("TOTAL_SKP_STYLE", "style='background: #fff600; font-weight: bold; text-align:center;' title='Плохо'");
                                    } else {

                                        if ($total_skp < 86)
                                            $rtpl->setVariable("TOTAL_SKP_STYLE", "style='background: #FF0000; color: #FFFFFF; font-weight: bold; text-align:center;'  title='Очень плохо'");

                                    }
                                }
                            }
                        }
                        $rtpl->setVariable("TOTAL_CONN", $total_conn ? $total_conn : "");
                        if ($total_conn != "" && $total_conn > 0) {
                            if ($total_conn == 100) {
                                $rtpl->setVariable("TOTAL_CONN_STYLE", "style='background: #B0FFB7; font-weight: bold; text-align:center;' title='Отлично'");
                            } else {
                                if ($total_conn >= 90 && $total_conn <= 99.99) {
                                    $rtpl->setVariable("TOTAL_CONN_STYLE", "style='background: #00ffd2; font-weight: bold; text-align:center;' title='Удовлетворительно'");
                                } else {
                                    if ($total_conn >= 86 && $total_conn <= 89.99) {
                                        $rtpl->setVariable("TOTAL_CONN_STYLE", "style='background: #fff600; font-weight: bold; text-align:center;' title='Плохо'");
                                    } else {

                                        if ($total_conn < 86)
                                            $rtpl->setVariable("TOTAL_CONN_STYLE", "style='background: #FF0000; color: #FFFFFF; font-weight: bold; text-align:center;'  title='Очень плохо'");

                                    }
                                }
                            }
                        }
                        $rtpl->setVariable("TOTAL_TT", $total_tt ? $total_tt : "");
                        if ($total_tt != "" && $total_tt > 0) {
                            if ($total_tt == 100) {
                                $rtpl->setVariable("TOTAL_TT_STYLE", "style='background: #B0FFB7; font-weight: bold; text-align:center;' title='Отлично'");
                            } else {
                                if ($total_tt >= 90 && $total_tt <= 99.99) {
                                    $rtpl->setVariable("TOTAL_TT_STYLE", "style='background: #00ffd2; font-weight: bold; text-align:center;' title='Удовлетворительно'");
                                } else {
                                    if ($total_tt >= 86 && $total_tt <= 89.99) {
                                        $rtpl->setVariable("TOTAL_TT_STYLE", "style='background: #fff600; font-weight: bold; text-align:center;' title='Плохо'");
                                    } else {

                                        if ($total_tt < 86)
                                            $rtpl->setVariable("TOTAL_TT_STYLE", "style='background: #FF0000; color: #FFFFFF; font-weight: bold; text-align:center;'  title='Очень плохо'");

                                    }
                                }
                            }
                        }
                        $rtpl->setVariable("TOTAL_MID", $final ? $final : "");
                        if ($final != "" && $final > 0) {
                            if ($final == 100) {
                                $rtpl->setVariable("TOTAL_TOTAL_STYLE", "style='background: #B0FFB7; font-weight: bold; text-align:center;' title='Отлично'");
                            } else {
                                if ($final >= 90 && $final <= 99.99) {
                                    $rtpl->setVariable("TOTAL_TOTAL_STYLE", "style='background: #00ffd2; font-weight: bold; text-align:center;' title='Удовлетворительно'");
                                } else {
                                    if ($final >= 86 && $final <= 89.99) {
                                        $rtpl->setVariable("TOTAL_TOTAL_STYLE", "style='background: #fff600; font-weight: bold; text-align:center;' title='Плохо'");
                                    } else {
                                        if ($final < 86)
                                            $rtpl->setVariable("TOTAL_TOTAL_STYLE", "style='background: #FF0000; color: #FFFFFF; font-weight: bold; text-align:center;'  title='Очень плохо'");
                                    }
                                }
                            }
                        }
                        $rtpl->setVariable("TOTAL_COUNT", $sc_total_count);
                        $rtpl->parse("rep_footer");
                    } else {
                        $rtpl->touchBlock("no-rows");
                    }
                } else {
                    UIError("Не найдены СЦ");
                }
                //$rtpl->setVariable("REP_SCLIST", implode(", ", $rep_sclist));
            }
            $rtpl->setVariable("REP_CONTRLIST", $contr > 0 ? $DB->getField("SELECT `contr_title` FROM `list_contr` WHERE `id`=" . $DB->F($contr) . ";") : "Все");
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");
                header("Content-disposition: attachment; filename=report_tech_rating__" . $_POST['datefrom'] . "-" . $_POST['dateto'] . ".xls");
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();

    }


}

?>