<?php
/**
 * Created for work.gorserv.ru
 * in extweb.org with love!
 *  mail@artemd.ru
 * 25.12.2015
 */

/** @var $report \classes\reports42\ReportTechPeriod */
/** @var $start integer */


function drawSumCell($data, $cols, $class, $openTag = '<td>', $closeTag = '</td>')
{
    if (count($cols))
        foreach ($cols as $k => $v) {
            echo str_replace('>', ' class="' . $class . '">', $openTag) . rub($data[$k], false) . $closeTag;
        }
    elseif (count($cols) > 0)
        foreach ($cols as $k => $v) {
            echo str_replace('>', ' class="' . $class . '">', $openTag) . $closeTag;
        }
    else
        echo str_replace('>', ' class="' . $class . '">', $openTag) . $closeTag;
}

$tech = $report->getTech();

if (!empty($alert))
    echo $alert;
?>
<div class="container-fluid" style="padding-top: 40px;">
<style>
    body {
        background: #FFFFFF !important;
        font-family: 'Lucida Grande', Verdana, Arial, sans-serif;
    }

    .report_body {
        border: 1px solid #808080 !important;
        font-size: 9pt;
    }

    .report_body th {
        border: 1px solid #808080 !important;
        border-collapse: collapse !important;
        font-size: 9pt;
    }

    .report_body td {
        border: 1px solid #808080 !important;
        border-collapse: collapse !important;
        font-size: 9pt;
    }

    .report_body .nowrap {
        white-space: nowrap;
    }

    .conn {
        background: #badbad;
    }

    .tt {
        background: #afe1f0;
    }

    .skp_cash {
        background: #ffff99;
    }

    .skp_cashless {
        background: #fcd975;
    }

    .sc_totals {
        background: #646464;
        font-weight: bold;
        text-align: left;
        color: white;
    }
</style>

<table width="100%" cellpadding="10" bgcolor="white">
    <tr>
        <td colspan="2">
            <h2 style="margin-bottom: 0;">Расчетный лист Техника: <?php echo $tech['fio']; ?> (заявки на
                Подключение/ТТ/СКП)</h2>
            Период: <?php echo $report->getPeriod(); ?> г.<br/>

            <p><br/></p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="report_body table table-responsive table-condensed " border="1" cellspacing="0" cellpadding="5" width="100%">
                <thead>
                <tr>
                    <th rowspan="2">№</th>
                    <th rowspan="2">Дата поступления</th>
                    <th rowspan="2">График</th>
                    <th rowspan="2">Дата выполнения</th>
                    <th rowspan="2">Контрагент</th>
                    <th rowspan="2">ПИН</th>
                    <th rowspan="2">Номер заявки Горсвязь</th>
                    <th rowspan="2">Номер заявки КА</th>
                    <th rowspan="2">Адрес</th>
                    <th rowspan="2">Кв.(Оф.)</th>
                    <th class="skp_cash" rowspan="2">СКП нал.</th>
                    <th class="skp_cashless" rowspan="2">СКП безнал.</th>
                    <th colspan="<?php echo count($report->getWorks()); ?>">Вид услуг</th>
                    <th rowspan="2">Баллов</th>
                </tr>
                <tr>
                    <?php foreach ($report->getWorks() as $work_title => $work): ?>
                        <th><?php echo $work_title; ?></th>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody>
                <?php
                /** @var \classes\reports42\ReportTask[] $items */
                $items = $report->getItems();
                if (!empty($items))
                    foreach ($items as $i => $task):
                        $cols = 12;
                        ?>
                        <tr>
                            <td><?php echo 1 + $i; ?></td>
                            <td>
                                <?php echo $task->reg_date; ?>
                            </td>
                            <td>
                                <?php echo $task->getGfx(); ?>
                            </td>
                            <td>
                                <?php echo $task->getDoneDate(); ?>
                            </td>
                            <td>
                                <?php echo $task->counterparty['contr_title']; ?>
                            </td>
                            <td>
                                <?php echo $task->ticket['clntopagr']; ?>
                            </td>
                            <td>
                                <?php echo $task->id; ?>
                            </td>
                            <td>
                                <?php echo $task->ticket['clnttnum']; ?>
                            </td>
                            <td>
                                <?php echo $task->address['address']; ?>
                            </td>
                            <td><?php echo $task->address['kv']; ?></td>

                            <td class="skp_cash nowrap">
                                <?php echo rub($task->getSkpCashProfit(), false); ?>
                            </td>

                            <td class="skp_cashless nowrap">
                                <?php echo rub($task->getSkpCashlessProfit(), false); ?>
                            </td>

                            <?php
                            drawSumCell($task->getProfitByWork(), $report->getWorks(), 'nowrap');
                            if (count($report->getWorks()) == 0) {
                                $cols += 1;
                            } else
                                $cols += count($report->getWorks());
                            ?>
                            <td class="nowrap">
                                <?php echo
                                rub($task->getProfit() / $report->getPointPrice(), false);
                                ?>
                            </td>
                        </tr>
                        <?php
                    endforeach; ?>
                <!-- END rep_row -->
                <tr class="sc_totals">
                    <th colspan="<?php echo $cols - 1; ?>" class="text-right" align="right">Штрафы в отчётном периоде, б.:</th>
                    <th><?php echo implode('; ', array_map(function ($n) {
                            return '<a href="/open?object_id=' . $n['task_id'] . '" target="_blank" style="color: white; text-decoration: underline;">' . $n['task_id'] . '</a> (' . $n['price'] . ')';
                        }, $report->getPenaltiesList())); ?></th>
                    <th class="nowrap">
                        <?php echo rub($report->getPenalties() / $report->getPointPrice()); ?>
                    </th>
                </tr>
                <tr class="sc_totals">
                    <th colspan="<?php echo $cols; ?>" class="text-right" align="right">Сумма за все виды работ, б.:</th>
                    <th class="nowrap">
                        <?php echo rub($report->getProfit() / $report->getPointPrice()); ?>
                    </th>
                </tr>

                <tr class="sc_totals">
                    <th colspan="<?php echo $cols; ?>" class="text-right" align="right">KPI1 Конвертация заявок:</th>
                    <th class="nowrap">
                        <?php echo sprintf('%01.2g%% (%01.2f%%)',
                            \classes\reports42\ReportTechPeriod::KPI1FAC * $report->getP1kpi(),
                            $report->getKPI1() * 100
                        ); ?>
                    </th>
                </tr>
                <tr class="sc_totals">
                    <th colspan="<?php echo $cols; ?>" class="text-right" align="right">KPI2 Обзвон:</th>
                    <th class="nowrap">
                        <?php echo sprintf('%01.2f%% (%01.2f%%)',
                            \classes\reports42\ReportTechPeriod::KPI2FAC * $report->getP2kpi(),
                            $report->getKPI2()
                        ); ?>
                    </th>
                </tr>
                <tr class="sc_totals">
                    <th colspan="<?php echo $cols; ?>" class="text-right" align="right">KPI3 Средний чек:</th>
                    <th class="nowrap">
                        <?php echo sprintf('%01.2g%% (%01.2f баллов)',
                            \classes\reports42\ReportTechPeriod::KPI3FAC * $report->getP3kpi(),
                            $report->getKPI3()
                        ); ?>
                    </th>
                </tr>
                <tr class="sc_totals">
                    <th colspan="<?php echo $cols; ?>" class="text-right" align="right">KPI4 Количество набранных
                        баллов:
                    </th>
                    <th class="nowrap">
                        <?php echo sprintf('%01.2f%% (%01.2f баллов)',
                            \classes\reports42\ReportTechPeriod::KPI4FAC * $report->getP4kpi(),
                            $report->getKPI4()
                        ); ?>
                    </th>
                </tr>
                <tr class="sc_totals">
                    <th colspan="<?php echo $cols - 1; ?>"></th>
                    <th class="text-right" align="right">Итого:</th>
                    <th class="nowrap">
                        <?php echo sprintf('%01.2f%%',
                            $report->getKpiSum()
                        ); ?>
                    </th>
                </tr>
                <tr class="sc_totals">
                    <th colspan="<?php echo $cols; ?>" class="text-right" align="right">Итого, б.:</th>
                    <th class="nowrap">
                        <?php echo sprintf('%01.2f',
                            $report->getKpiSum() * (($report->getProfit() + $report->getPenalties()) / $report->getPointPrice()) / 100
                        ); ?>
                    </th>
                </tr>
                <tr class="sc_totals">
                    <th colspan="<?php echo $cols; ?>" class="text-right" align="right">Начислено, р.:</th>
                    <th class="nowrap">
                        <?php echo sprintf('%01.2f',
                            $report->getKpiSum() * ($report->getProfit() + $report->getPenalties()) / $report->getPointPrice()
                        ); ?>
                    </th>
                </tr>
                <?php /* if ($report->getAssessedProfit() > 0): ?>
                    <tr class="sc_totals">
                        <th colspan="<?php echo $cols - 3; ?>" class="text-right" align="right">Доначисления (тестовый режим, в расчёте не участвует), б.:</th>
                        <th colspan="3">
                            По заявкам: <?php
                            $tmp = [];
                            foreach ($report->getAssessedItem() as $task)
                                $tmp[] = $task->id . ' (' . $task->getProfit() . ')';
                            echo implode(', ', $tmp);
                            ?>
                        </th>
                        <th>
                            <?php echo rub($report->getAssessedProfit()); ?>
                        </th>
                    </tr>
                <?php endif; */ ?>
                <tr class="sc_totals">
                    <th colspan="<?php echo $cols; ?>" class="text-right" align="right">К выплате, р.:</th>
                    <th>
                        <b><?php echo rub(round($report->getSalary() - $report->getSkpCashProfit())); ?></b>
                    </th>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<p><?php echo date('Y-m-d H:i:s'); ?></p>
<p>
    <small><?php echo time() - $start; ?> s</small>
</p>
</div>
