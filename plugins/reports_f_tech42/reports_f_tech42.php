<?php
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\reports42\ReportTechPeriod;

set_time_limit(0);
ini_set('max_execution_time', 0);

class reports_f_tech42_plugin extends Plugin
{

    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function main()
    {
        global $USER;

        $return = '';
        $fintypes = array("0" => "Все", "1" => "Рассчитанные (Проведенные)", "2" => "Не рассчитанные (Не проведенные)");
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01' => "Январь", '02' => "Февраль", '03' => "Март", '04' => "Апрель", '05' => "Май", '06' => "Июнь", '07' => "Июль", '08' => "Август", '09' => "Сентябрь", '10' => "Октябрь", '11' => "Ноябрь", '12' => "Декабрь");

        $tpl = $this->showForm($month, $year, $fintypes);

        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = $this->showReport($month, $year);

            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());

            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");
                header("Content-disposition: attachment; filename=report_tech_" . $_POST["tech_id"] . "_" . date("d") . "_" . $_POST['month'] . "-" . $_POST['year'] . ".xls");

                $html = $this->compileReport();
                $this->layout = 'pure';
                $return = $this->render('twozero/wrap', [
                    'content'=>$html
                ]);
            } else {
                $html = $this->compileReport();
                $this->layout = 'fullscreen';
                $return = $this->render('twozero/wrap', [
                    'content'=>$html
                ]);
            }
        } else {
            $tpl->touchBlock("notcreated");
            $tpl->show();
        }

        echo $return;

    }

    /**
     * Prints report form
     * @param $month
     * @param $year
     * @param $fintypes
     * @return mixed
     */
    private function showForm($month, $year, $fintypes)
    {
        global $USER, $DB;
        $tpl = $this->getTpl(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
        $tpl->setVariable("REPORT__FIN_TECH", "buttonsel1");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m', strtotime('-1 month'))));
        $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y", strtotime('-1 month'))));
        UIHeader($tpl);
        report_left_menu($tpl);
        if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
            $tpl->setVariable("FILTER_TECH_OPTIONS", "<option value='" . $USER->getId() . "'>" . $DB->getField("SELECT `fio` FROM `users` WHERE `id`=" . $DB->F($USER->getId())) . "</option>");
            $tpl->setVariable("FILTER_TASK_TYPE", "<option value='0'>-- все --</option>");
        } else {
            $tpl->setVariable("FILTER_TECH_OPTIONS", adm_empl_plugin::getTechList($_POST["tech_id"]));
            $tpl->setVariable("FILTER_TASK_TYPE", array2options($fintypes, $_POST["type_id"]));
        }
        $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
        $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
        return $tpl;
    }

    /**
     * Shows report
     * @param $month
     * @param $year
     * @return HTML_Template_IT
     */
    private function showReport($month, $year)
    {
        global $USER;
        $rtpl = $this->getTpl(path2("plugins/" . $this->getUID()));
        if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
            if ($USER->getId() != $_POST["tech_id"]) {
                UIError("Вы пытаетесь получить доступ к чужому отчету!");
                die();
            }
        }
        if ($USER->getTemplate() != "default")
            $rtpl->loadTemplatefile($USER->getTemplate() . "/report.tmpl.htm");
        else
            $rtpl->loadTemplatefile("report.tmpl.htm");

        if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
            $rtpl->setCurrentBlock("print_head");
            $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
            $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
            $rtpl->parse("print_head");
        }
        $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
        $rtpl->setVariable("DATETO", $_POST['dateto']);
        $rtpl->setVariable("MONTH", $month[$_POST['month']]);
        $rtpl->setVariable("YEAR", $year[$_POST['year']]);
        return $rtpl;
    }

    private function compileReport()
    {
        $this->layout = null;

        $start = time();
        $period = $_POST["year"] . "-" . $_POST["month"];

        if (!isset($_POST["tech_id"])) {
            UIError("Не указан Техник");
            return;
        }

        $pointPrice = $this->getEm()->find(\models\User::class, $_POST["tech_id"])
            ->getMasterServiceCenters()[0]->getRegion()->getPointPrice();

        $usr = adm_users_plugin::getUser((int)$_POST["tech_id"]);
        $report = new ReportTechPeriod((int)$_POST["tech_id"], $period, $pointPrice);

        $value = $this->render('twozero/report', array(
            'report' => $report,
            'tech' => $usr,
            'period' => $period,
            'start' => $start,
        ));


        return $value;
    }
}
