<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2015
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Акты Заявок (СКП)";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Акты Заявок (СКП)";
    $PLUGINS[$plugin_uid]['events']['actarchive'] = "Архив актов Заявок (СКП)";
    $PLUGINS[$plugin_uid]['events']['viewact'] = "Просмотр акта Заявок (СКП)";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['createact'] = "Создание акта Заявок";
    $PLUGINS[$plugin_uid]['events']['deleteact'] = "Удаление акта Заявок (СКП)";
    $PLUGINS[$plugin_uid]['events']['setsent'] = "Изменение статуса актов Заявок (СКП)";
    $PLUGINS[$plugin_uid]['events']['removefromact'] = "Удаление заявок из актов (СКП)";
    $PLUGINS[$plugin_uid]['events']['savenewwtype_ajax'] = "Редактирование доп. вида работ (СКП)";
    $PLUGINS[$plugin_uid]['events']['saveacttitle_ajax'] = "Редактирование названия акта (СКП)";
    /*$PLUGINS[$plugin_uid]['events']['savenewcnumval_ajax'] = "Редактирование номера заявки в 1С (СКП)";
    */
}
?>