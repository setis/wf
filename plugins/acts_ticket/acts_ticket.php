<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\tickets\ConnectionTicket;



class acts_ticket_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function createact() {
        global $DB, $USER;
        $ticks = $_POST["task"];
        if (isset($_POST["setflagtoex"])) {
            $act_id = $_POST['exact_id'];
            if (count($ticks) && $act_id) {
                $sql = "UPDATE `tickets` SET `tdataactid`=".$DB->F($act_id)." WHERE `task_id` IN (" . implode(",", $ticks) . ");";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                redirect($this->getLink('actarchive'), "Акт обновлен. ID: ".$act_id);
            } else {
                UIError("Не указаны заявки ");
            }
        } else {
            if (count($ticks)) {
                $ct = new ConnectionTicket($ticks[0]);
                $sql = "INSERT INTO `list_tdata_reports` (`contr_id`, `status`, `cr_uid`, `cr_date`) VALUES
                    (".$DB->F($ct->getContrId()).", 0, ".$DB->F($USER->getId()).", now())";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                $lastrec = $DB->insert_id();
                $sql = "UPDATE `tickets` SET `tdataactid`=".$DB->F($lastrec)." WHERE `task_id` IN (" . implode(",", $ticks) . ");";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                redirect($this->getLink('actarchive'), "Акт сформирован. ID: ".$lastrec);
            } else {
                UIError("Не указаны заявки ");
            }
        }           
    }
    
    static public function saveacttitle_ajax() {
         global $DB, $USER;
         $act_id = $_REQUEST["act_id"];
         $title = $_REQUEST["title"];
         if (!$act_id || !$title) {
            $ret["error"] = "Недостаточно данных для выполнения операции";
            echo json_encode($ret);
            return false;
         }
         $sql = "UPDATE `list_tdata_reports` SET `title`=".$DB->F($title)." WHERE `id`=".$DB->F($act_id).";";
         $DB->query($sql);
         if ($DB->errno()) {
            $ret["error"] = $DB->error()." ".$sql;
            echo json_encode($ret);
            return false;            
         }
         $DB->free();
         $ret["ok"] = "ok";
         echo json_encode($ret);
         return false;
    }
    
    function actarchive() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("actarchive.tmpl.htm");
        if ($USER->getTemplate() != "default") 
            $tpl->loadTemplatefile($USER->getTemplate()."/actarchive.tmpl.htm");
        else 
            $tpl->loadTemplatefile("actarchive.tmpl.htm");
        $tpl->setVariable("REPORT__TICKET_ACT", "buttonsel1");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        UIHeader($tpl);
        report_left_menu($tpl);
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
         $sql = "SELECT count(ltr.id)
                FROM `list_tdata_reports` AS ltr 
                WHERE 1 = 1
                  and not exists ( select * from tickets ti, tasks ta where ta.id = ti.task_id and ta.plugin_uid in ('services','accidents','gp') and ti.tdataactid = ltr.id )
                ORDER BY `status` ASC, `cr_date` DESC";
        $total = $DB->getField($sql);
        $sql = "SELECT ltr.*, DATE_FORMAT(`cr_date`, '%H:%i:%s %d.%m.%Y') as created 
                FROM `list_tdata_reports` AS ltr 
                WHERE 1 = 1
                  and not exists ( select * from tickets ti, tasks ta where ta.id = ti.task_id and ta.plugin_uid in ('services','accidents','gp') and ti.tdataactid = ltr.id )
                ORDER BY `status` ASC, `cr_date` DESC 
                LIMIT ".$_REQUEST['filter_start'].",".getcfg('rows_per_page');

        #echo "<pre>\n".$sql."</pre>\n";

        $DB->query($sql);

        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            while ($res = $DB->fetch(true)) {
                $uid = adm_users_plugin::getUser($res["cr_uid"]);
                $tpl->setCurrentBlock("actrow");
                $tpl->setVariable("RR_ID", $res["id"]);
                $tpl->setVariable("RR_CONTR", kontr_plugin::getByID($res["contr_id"]));
                $tpl->setVariable("RR_CONTR_ID", $res["contr_id"]);
                $rCount = $DB->getField("SELECT COUNT(task_id) FROM `tickets` WHERE `task_id` IN (SELECT `id` FROM `tasks` WHERE `plugin_uid`='connections') AND `tdataactid`=".$DB->F($res["id"]).";");
                $tpl->setVariable("RR_TICKCOUNT", $rCount ? $rCount : "&mdash;");
                $tpl->setVariable("RR_TITLE", $res["title"]);
                $tpl->setVariable("RR_CRDATE", $res["created"]);
                $tpl->setVariable("RR_AUTHOR", $uid["fio"]);
                $tpl->setVariable("RR_STATUS", $res["status"] ? "Отправлен" : "<strong>Новый</strong>");
                $tpl->setVariable("RR_SENTDATE", $res["status"] && $res["send_date"] ? $res["send_date"] : "&mdash;");
                $tpl->parse("actrow");   
            }
            $DB->free();
        } else {
            $tpl->touchBlock("norows");
        }
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], getcfg('rows_per_page'), intval($total), "acts_tmc/actarchive#start-%s"));
        $tpl->show();    
    }
    
    function deleteact() {
        global $DB;
        if ($id = $_GET["id"]) {
            $sql = "UPDATE `tickets` SET `tdataactid`=0 WHERE `task_id` IN (SELECT `id` FROM `tasks` WHERE `plugin_uid`='connections') AND `tdataactid`=".$DB->F($id).";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            $sql = "DELETE FROM `list_tdata_reports` WHERE `id`=".$DB->F($id).";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            redirect($this->getLink('actarchive'), "Акт успешно удален. ID: ".$id);
        } else {
            UIError("Не указан индентификатор акта для удадения!");
        }
    } 
    
    function viewact() {
        global $DB, $USER;
        
        $act_id = $_REQUEST["id"];
        $contr_id = $_REQUEST["contr_id"];
        if ($act_id && $contr_id) {            
            if (isset($_POST["setsent"])) {
                $sql = "UPDATE list_tdata_reports SET `status`=1, `send_date`=".$DB->F(date("Y-m-d H:i:s"))." WHERE `id`=".$DB->F($act_id).";";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                redirect($this->getLink('actarchive'), "Статус 'Отправлен' установлен для акта №".$contr_id.". Дата отправки: ".date("H:i:s d.m.Y"));
            }
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile("viewact.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/viewact.tmpl.htm");
            else 
                $tpl->loadTemplatefile("viewact.tmpl.htm");
            $tpl->setVariable("REPORT__TICKET_ACT", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable("SELECTED_ACT", $act_id);
            $tpl->setVariable("SELECTED_CONTR", $contr_id);
            $contrInfo = kontr_plugin::getByID($contr_id);
            $actInfo = self::getActInfo($act_id);
            $userInfo = adm_users_plugin::getUser($actInfo["cr_uid"]);
            UIHeader($tpl);
            report_left_menu($tpl);
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("viewreport.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/viewreport.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("viewreport.tmpl.htm");
            
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
            
                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("REP_CONTR", $contrInfo);
            $rtpl->setVariable("ACTNUN_VAL", $act_id);
            $rtpl->setVariable("ACT_DATE", $actInfo["created"]);
            $rtpl->setVariable("REP_CDATE", $actInfo["created"]);
            $rtpl->setVariable("REP_AUTHOR", $userInfo["fio"]);
            if ($actInfo["status"]!=1 && !isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
                $rtpl->touchBlock("rmfa");
            }
            
            $isAdsl = false;
            $base_sql = "SELECT count(task_id) FROM `tickets` WHERE `cnt_id`='36' AND `task_type`='37' AND `task_id` IN (SELECT `id` FROM `tasks` WHERE `plugin_uid`='connections') AND `tdataactid`=".$DB->F($act_id);
            $isAdsl = $DB->getField($base_sql);
            if ($isAdsl) {
                $rtpl->touchBlock("mgtsadslth");
                $clsp = 2;
            } else {
                $clsp = 0;
            }
            
            $base_sql = "SELECT task_id FROM `tickets` WHERE `task_id` IN (SELECT `id` FROM `tasks` WHERE `plugin_uid`='connections') AND `tdataactid`=".$DB->F($act_id);
            $sql = "SELECT agr_id FROM `tickets` WHERE `tdataactid`=".$DB->F($act_id)." AND `agr_id`!=0;";
            $agr_id = $DB->getField($sql);
            $i = 1;
            $total = 0;
            $total_nds = 0;
            $total_wnds = 0;
            $wtList = kontr_plugin::getPItemsByAgrAndTaskFilter($agr_id, $base_sql);
            foreach($wtList as $item) {
                $rtpl->setCurrentBlock("wt_col");
                $rtpl->setVariable("WT_SERVICENAME", $item["s_title"]);
                $rtpl->parse("wt_col");
            }
            $DB->query($base_sql);
            $rtpl->setVariable("WT_COUNT", count($wtList));
                                
            if ($DB->errno()) UIError($DB->error(). " " . $base_sql);
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
            while ($res1 = $DB->fetch(true)) {
                $ticket = new ConnectionTicket($res1["task_id"]);
                $rtpl->setCurrentBlock("rep_row");
                if ($ticket->getFindateContr()) {
                    $rtpl->setVariable("ALREADYCOUNTED", " counted");
                }
                $rtpl->setVariable("RR_ID", $i);
                $rtpl->setVariable("RR_T_ID", $ticket->getId());
                if ($doneStatus) {
                    $sql = "SELECT DATE_FORMAT(`datetime`, '%d.%m.%Y') as doneDate FROM `task_comments` WHERE `task_id`=".$DB->F($res1["task_id"])." AND `status_id`=".$DB->F($doneStatus["id"]).";";
                    $doneDate = $DB->getField($sql);
                    if ($doneDate) {
                        $rtpl->setVariable("RR_DATE", $doneDate);
                    } else {
                        $rtpl->setVariable("RR_DATE", "&mdash;");                                     
                    }
                } else {
                    $rtpl->setVariable("RR_DATE", "&mdash;");                 
                }
                if (!$ticket->getTICKActId()) {
                    $rtpl->setCurrentBlock("inact");
                    $rtpl->setVariable("RR_TASK_ID", $ticket->getId());
                    $rtpl->parse("inact");
                }
                if ($actInfo["status"]!=1 && !isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
                    $rtpl->setCurrentBlock("rmfa_row");
                    $rtpl->setVariable("RR_ID1", $ticket->getId());
                    $rtpl->parse("rmfa_row");
                }
                
                if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
                    $rtpl->setCurrentBlock("has-link");
                    $rtpl->setVariable("TICKETOPENLINK", "/connections/viewticket?task_id=".$ticket->getId());
                    $rtpl->parse("has-link");
                    $rtpl->touchBlock("has-link1");
                    $rtpl->setCurrentBlock("ep");
                    $rtpl->setVariable("RR_TSK1_ID", $ticket->getId());
                    $rtpl->parse("ep");
                    $rtpl->setCurrentBlock("ep1");
                    
                }
                $rtpl->setVariable("RR_TASK_TYPENAME", $ticket->getTypeName());
                $rtpl->setVariable("RR_TASK_ID_INLIST", $ticket->getId());
                $rtpl->setVariable("RR_OPNUM", $ticket->getClntTNum());
                
                
                
                $rtpl->setVariable("RR_PIN", $ticket->getClntOpAgr() ? (isset($_POST["createxlsversion"]) ?  "# ".$ticket->getClntOpAgr() : $ticket->getClntOpAgr()) : "&mdash;");
                $rtpl->setVariable("RR_CR_DATE", $ticket->getDateReg());
                $rtpl->setVariable("RR_FIO", $ticket->getClientType() == "1" ? $ticket->getFio() : $ticket->getOrgName());
                $rtpl->setVariable("RR_GSSNUM", $ticket->getId());
                $rtpl->setVariable("RR_AREA", $ticket->getArea());
                $rtpl->setVariable("RR_OPGSS", $ticket->getId());
                $rtpl->setVariable("RR_CLNTTYPE", $ticket->getClientType() == "1" ? "Физ. лицо" : "Юр. лицо");
                $sum = 0;
                $rowspan = 1;
                $addr = $ticket->getAddr($ticket->getDomId());
                if (isset($addr["nokladr"])) {
                    $rtpl->setCurrentBlock("no_kladr");
                    $rtpl->setVariable("TMCCOUNT2", $rowspan);
                    $rtpl->parse("no_kladr");
                } else {
                    $rtpl->setCurrentBlock("kladr_addr");
                    $rtpl->setVariable("TMCCOUNT3", $rowspan);
                    $rtpl->setVariable("RR_ADDR_CITY", $addr["city"]);
                    $rtpl->setVariable("RR_ADDR_STREET", $addr["street"]);
                    $rtpl->setVariable("RR_ADDR_HOUSE", "д.".$addr["house"]);
                    $rtpl->setVariable("RR_ADDR_FLAT", $ticket->getKv());
                    $rtpl->parse("kladr_addr");
                }
                foreach($wtList as $item) {
                    $price = connections_plugin::getCWTypeID($ticket->getId(),$item["id"]);
                    if ($price) {
                        $rtpl->setCurrentBlock("wt_val");
                        $rtpl->setVariable("TMCCOUNT1", $rowspan);
                        $sum += $price[0]["price_type"] != "smeta" ? $price[0]["price"]*$price[0]["quant"] : $price[0]["smeta_val"];
                        $rtpl->setVariable("WT_RR_PRICE", $price[0]["price_type"] != "smeta" ? $price[0]["price"]*$price[0]["quant"] : $price[0]["smeta_val"]);
                        $rtpl->parse("wt_val");   
                    } else {
                        $rtpl->setCurrentBlock("wt_val");
                        $rtpl->setVariable("TMCCOUNT1", $rowspan);
                        $rtpl->setVariable("WT_RR_PRICE", "0");
                        $rtpl->parse("wt_val");   
                    }  
                }
                $nds = round($sum*18/100, 2);
                $total += $sum;
                $total_nds += $nds;
                $total_wnds += $sum + $nds;
                $rtpl->setVariable("RR_PRICE", number_format($sum , 2, ',', ' '));
                $rtpl->setVariable("RR_TAX", number_format($nds , 2, ',', ' '));
                $rtpl->setVariable("RR_WTAX", number_format($nds + $sum, 2, ',', ' '));
                $cableCount = 0;
                
                $sql = "SELECT tmc_tech.id, tmc_tech.count, tmc_sc.serial, tmc_sc.tmc_id, tmc_list.title, tickdata.isrent, tmc_list.metric_flag, tmc_list.cat_id FROM `tmc_ticket` AS tickdata 
                                            LEFT JOIN `tmc_sc_tech` AS tmc_tech ON tmc_tech.id=tickdata.tmc_tech_sc_id 
                                            LEFT JOIN `tmc_sc` AS tmc_sc ON tmc_sc.id=tmc_tech.tmc_sc_id 
                                            LEFT JOIN `list_materials` AS tmc_list ON tmc_list.id=tmc_sc.tmc_id 
                                            WHERE tickdata.task_id=".$DB->F($res1["task_id"]).";";
                                    $DB->query($sql);
                                    if ($DB->errno()) { echo "error ".$sql." ".$DB->error(); trigger_error($DB->error()); return false; }
                                    $rowspan = 0;
                                    
                                    $rC = $DB->num_rows();
                                    if ($rC>1) {
                                        $first = true;
                                        $rtpl->setVariable("TMCCOUNT", $rC);
                                        $rowspan = $rC;
                                        while ($res = $DB->fetch(true)) {
                                            if ($first) {
                                                if ($res["metric_flag"]) {
                                                    $cableCount += $res["count"];
                                                } 
                                                $first = false;
                                                
                                            } else {
                                                if ($res["metric_flag"]) {
                                                    $cableCount += $res["count"];
                                                }
                                            }
                                        }
                                    } else {
                                        $res = $DB->fetch(true);
                                        $rtpl->setVariable("TMCCOUNT", "1");
                                        $rowspan = 1;
                                        if ($DB->num_rows()) {
                                            if ($res["metric_flag"]) {
                                                $cableCount += $res["count"];
                                            }
                                        }
                                    }
                                    $DB->free();
                
                
                
                
                $rtpl->setVariable("RR_CABLE", intval($cableCount) ? $cableCount." м." : "&mdash;");  
                if ($isAdsl) {
                    $rtpl->setCurrentBlock("mgtsadsltd");
                    $rtpl->setVariable("RR_ATS", $ticket->getMGTS_ATS() ? $ticket->getMGTS_ATS() : "&mdash;");
                    $rtpl->setVariable("RR_LANDLINE", $ticket->getMGTS_LandlineNum() ? $ticket->getMGTS_LandlineNum() : "&mdash;");
                    $rtpl->parse("mgtsadsltd");
                    
                }              
                $rtpl->parse("rep_row");
                $i+=1;
            }
            $rtpl->setVariable("TOTALCOLSPAN", 14+intval(count($wtList))+ $clsp);
            $rtpl->setVariable("TOTAL_WONDS", number_format($total, 2, ',', ' '));
            $rtpl->setVariable("TOTAL_NDS", number_format($total_nds, 2, ',', ' '));
            $rtpl->setVariable("TOTAL_WNDS", number_format($total_wnds, 2, ',', ' '));
            if (isset($_POST["createxlsversion"])) {
                    header("Content-type: application/vnd.ms-excel");  
                    header("Content-disposition: attachment; filename=tmc_act__".$_REQUEST['id'].".xls");  
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    if (isset($_POST['createprintversion'])) {
                        $rtpl->touchBlock("print_footer");
                        $rtpl->show();
                        
                        return;
                    } else {
                        $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());  
                    }
            }
            $tpl->show();
        } else {
            UIError("Не указан идентификатор акта!");
        }
    }
    
    function removefromact() {
        global $DB, $USER;
        if ($id = $_REQUEST["ticket_id"]) {
            $sql = "UPDATE `tickets` SET `tdataactid`='0' WHERE `task_id`=".$DB->F($id).";";
            $DB->query($sql);
            if ($DB->error()) {
                echo "error";
            } else {
                echo "ok";                
            }
        } else {
            echo "error";
        }
        return false;
    }
    
    
    function main() {
        global $DB, $USER;
        
        $sort_ids = array(
            1 => "Дата выполнения",
            2 => "Дата регистрации",
            3 => "Номер заявки оператора",
            4 => "ПИН",
            5 => "Номер заявки",
            6 => "Район",
            7 => "Адрес",
        );
        $sort_sql = array(
            1 => "tc1.datetime",
            2 => "t.date_reg",
            3 => "tick.clnttnum",
            4 => "tick.clntopagr",
            5 => "t.id",
			6 => "lar.title",
            7 => "kstreet.NAME",
        );
        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $fintypes = array("0"=>"Все", "1"=>"Учтенные", "2"=>"Не учтенные");
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/acts_ticket.tmpl.htm");
            else 
                $tpl->loadTemplatefile("acts_ticket.tmpl.htm");
            
            $tpl->setVariable("REPORT__TICKET_ACT", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
            $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
            UIHeader($tpl);
            report_left_menu($tpl);
            $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            $tpl->setVariable("SELECTED_AGR", @$_POST["agr_id"]);
            $tpl->setVariable("SELECTED_AREA", @$_POST["area_id"]);
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            $tpl->setVariable("FILTER_TASK_TYPE", array2options($fintypes, $_POST["type_id"]));
            $tpl->setVariable("FILTER_WTYPE_OPTIONS", "<option value='0'> -- все -- </option>".adm_ticktypes_plugin::getOptList(@$_POST["wtype_id"], "connections"));
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListConn")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListConn(@$_POST["cnt_id"]));
            } 
            if (class_exists("adm_statuses_plugin", true) && method_exists("adm_statuses_plugin", "getOptList")) {
                $st = new adm_statuses_plugin();
                $tpl->setVariable("FILTER_STATUS_OPTIONS", $st->getOptList(@$_POST["status_id"], 'connections'));
            } 
            $tpl->setVariable("FILTER_SC_OPTIONS", "<option value=0>-- все --</option>".adm_sc_plugin::getScList(@$_POST["sc_id"]));
            
            
            $actList = self::getActList(0);
            
            if ($actList) {
                $tpl->setCurrentBlock("newactlist");
                $tpl->setVariable("EXACT_OPTIONS", $actList);
                $tpl->parse("newactlist");
                $tpl->setCurrentBlock("newactlistbtm");
                $tpl->setVariable("EXACT_OPTIONS_BTM", $actList);
                $tpl->parse("newactlistbtm");
            }
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"] || @$_POST["setflag"]) {
                
                $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
                //$rtpl->loadTemplatefile("report.tmpl.htm");
                if ($USER->getTemplate() != "default") 
                    $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
                else 
                    $rtpl->loadTemplatefile("report.tmpl.htm");
                if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                    $rtpl->setCurrentBlock("print_head");
                    $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                    $rtpl->parse("print_head");
                }
                $rtpl->setVariable("MONTH", $month[$_POST['month']]);
                $rtpl->setVariable("YEAR", $year[$_POST['year']]);
                if ($_POST['cnt_id']!=0 && $_POST["agr_id"]) {
                    $rtpl->setVariable("AGR_VAL", kontr_plugin::getAgrByIdT($_POST["agr_id"]));
                    if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getByID")) {
                        $ct = new kontr_plugin();
                        if ($kontr = $ct->getByID($_POST['cnt_id'])) {
                            $rtpl->setVariable("REP_CONTR", $kontr);
                            $status = adm_statuses_plugin::getStatusByTag("closed", "connections");
                            if (count($status)) {
                                $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
                                $filter ="";
                                if ($_POST["sc_id"] && $_POST["area_id"]) {
                                    $filter = "AND tick.dom_id IN (SELECT `id` FROM `list_addr` WHERE `area_id`=".$DB->F($_POST["area_id"]).")";
                                } else {
                                    if ($_POST["sc_id"]) {
                                        $filter = " AND tick.sc_id=".$DB->F($_POST["sc_id"]);
                                    } 
                                }
                                if ($_POST["wtype_id"]>0) {
                                    $filter .=" AND tick.task_type=".$DB->F($_POST["wtype_id"])." ";
                                }
                                $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];           
                                $filter .= "AND (tick.tdataactid IS NULL OR tick.tdataactid=0)";
                                $base_sql = "SELECT tc1.task_id, DATE_FORMAT(tc1.datetime, '%d.%m.%Y') AS datetime FROM `task_comments` AS tc1 
                                        LEFT JOIN `tasks` AS t ON tc1.task_id=t.id
                                        LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                                        LEFT JOIN `list_addr` AS la ON la.id=tick.dom_id
                                        LEFT JOIN `list_area` AS lar ON lar.id=la.area_id
                                        LEFT JOIN `kladr_street` AS kstreet ON CONCAT(SUBSTR(la.kladrcode, 1, 15), '00')=kstreet.CODE
                                        WHERE tc1.status_id=".$DB->F($doneStatus["id"])." 
                                            AND DATE_FORMAT(tc1.datetime, '%Y-%m')=".$DB->F($_POST["year"]."-".$_POST["month"])."
                                            AND tick.agr_id=".$DB->F($_POST["agr_id"])."
                                            AND t.status_id=".$DB->F($status["id"])." 
                                            ". $filter. " 
                                        GROUP BY tc1.task_id    
                                        ORDER BY " .$order;
                                $sql = "SELECT tc1.task_id FROM `task_comments` AS tc1 
                                        LEFT JOIN `tasks` AS t ON tc1.task_id=t.id
                                        LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                                        LEFT JOIN `list_addr` AS la ON la.id=tick.dom_id
                                        LEFT JOIN `list_area` AS lar ON lar.id=la.area_id
                                        LEFT JOIN `kladr_street` AS kstreet ON CONCAT(SUBSTR(la.kladrcode, 1, 15), '00')=kstreet.CODE
                                        WHERE tc1.status_id=".$DB->F($doneStatus["id"])." 
                                            AND DATE_FORMAT(tc1.datetime, '%Y-%m')=".$DB->F($_POST["year"]."-".$_POST["month"])."
                                            AND tick.agr_id=".$DB->F($_POST["agr_id"])."
                                            AND t.status_id=".$DB->F($status["id"])." 
                                            ". $filter. "
                                        GROUP BY tc1.task_id    
                                        ORDER BY " .$order;
                                //die($sql);
                                $i = 1;
                                $total = 0;
                                $total_nds = 0;
                                $total_wnds = 0;
                                
                                if ($_POST["wtype_id"] == "37" && $_POST["cnt_id"] == "36") {
                                    $rtpl->touchBlock("mgtsadslth");
                                    $mgts_intpl = true;
                                    $addcl = 3;
                                } else {
                                    $mgts_intpl = false;
                                    $addcl = 1;
                                }
                                
                                $wtList = kontr_plugin::getPItemsByAgrAndTaskFilter($_POST['agr_id'], $sql);
                                if (!count($wtList)) {
                                    $DB->free();
                                    UIError("Отсутствует прайс для контрагента по выбранному договору");
                                }
                                
                                foreach($wtList as $item) {
                                    $rtpl->setCurrentBlock("wt_col");
                                    $rtpl->setVariable("WT_SERVICENAME", $item["s_title"]);
                                    $rtpl->parse("wt_col");
                                }

                                $rtpl->setVariable("WT_COUNT", count($wtList));
                                $DB->query($base_sql);
                                if ($DB->errno()) UIError($DB->error(). " " . $base_sql);
                                while ($res1 = $DB->fetch(true)) {
                                    $ticket = new ConnectionTicket($res1["task_id"]);
                                    $rtpl->setCurrentBlock("rep_row");
                                    if (!$_POST["createprintversion"] && !$_POST["createxlsversion"]) {
                                        $rtpl->setCurrentBlock("ep1");
                                        $rtpl->setVariable("RR_TSK_ID1", $ticket->getId());
                                        $rtpl->parse("ep1");
                                        $rtpl->setCurrentBlock("ep3");
                                        $rtpl->setVariable("RR_TSK_ID2", $ticket->getId());
                                        $rtpl->parse("ep3");
                                        $rtpl->touchBlock("ep2");
                                        $rtpl->touchBlock("ep4");
                                        $rtpl->touchBlock("el1");
                                        $rtpl->setCurrentBlock("el");
                                        $rtpl->setVariable("RR_TASK1_ID", $ticket->getId());
                                        $rtpl->parse("el");
                                    }
                                    if ($ticket->getFindateContr()) {
                                        $rtpl->setVariable("ALREADYCOUNTED", " counted");
                                    }
                                    $rtpl->setVariable("RR_ID", $i);
                                    $rtpl->setVariable("RR_CONTRID", $ticket->getClntTNum());
                                    if ($doneStatus) {
                                        if ($res1["datetime"]) {
                                            $rtpl->setVariable("RR_DATE", $res1["datetime"]);
                                        } else {
                                            $rtpl->setVariable("RR_DATE", "&mdash;");                                     
                                        }
                                    } else {
                                        $rtpl->setVariable("RR_DATE", "&mdash;");                 
                                    }
                                    if (!$ticket->getTICKActId()) {
                                        $rtpl->setCurrentBlock("inact");
                                        $rtpl->setVariable("RR_TASK_ID", $ticket->getId());
                                        $rtpl->parse("inact");
                                    }
                                    $rtpl->setVariable("RR_TASK_ID_INLIST", $ticket->getId());
                                    $rtpl->setVariable("RR_OPNUM", $ticket->getClntTNum());
                                    $rtpl->setVariable("RR_PIN", $ticket->getClntOpAgr() ? isset($_POST["createxlsversion"]) ?  "# ".$ticket->getClntOpAgr() : $ticket->getClntOpAgr() : "&mdash;");
                                    $rtpl->setVariable("RR_CTASK", $ticket->getAddNumber() ? $ticket->getAddNumber() : "&mdash;");
                                    $rtpl->setVariable("RR_CR_DATE", $ticket->getDateReg());
                                    $rtpl->setVariable("RR_FIO", $ticket->getClientType() == "1" ? $ticket->getFio() : $ticket->getOrgName());
                                    $rtpl->setVariable("RR_GSSNUM", $ticket->getId());
                                    $rtpl->setVariable("RR_AREA", $ticket->getArea());
                                    $sum = 0;
                                    $rowspan = 1;
                                    $addr = $ticket->getAddr($ticket->getDomId());
                                    if (isset($addr["nokladr"])) {
                                        $rtpl->setCurrentBlock("no_kladr");
                                        $rtpl->setVariable("TMCCOUNT2", $rowspan);
                                        $rtpl->parse("no_kladr");
                                    } else {
                                        $rtpl->setCurrentBlock("kladr_addr");
                                        $rtpl->setVariable("TMCCOUNT3", $rowspan);
                                        $rtpl->setVariable("RR_ADDR_CITY", $addr["city"]);
                                        $rtpl->setVariable("RR_ADDR_STREET", $addr["street"]);
                                        $rtpl->setVariable("RR_ADDR_HOUSE", "д.".$addr["house"]);
                                        $rtpl->setVariable("RR_ADDR_FLAT", $ticket->getKv());
                                        $rtpl->parse("kladr_addr");
                                    }
                                    foreach($wtList as $item) {
                                        $price = connections_plugin::getCWTypeID($ticket->getId(),$item["id"]);
                                        
                                            if ($price) {
                                                $rtpl->setCurrentBlock("wt_val");
                                                $rtpl->setVariable("TMCCOUNT1", $rowspan);
                                                $sum += $price[0]["price_type"] != "smeta" ? ($price[0]["price"]*$price[0]["quant"]) : $price[0]["smeta_val"];
                                                $rtpl->setVariable("WT_RR_PRICE", $price[0]["price_type"] != "smeta" ? ($price[0]["price"]*$price[0]["quant"]) : $price[0]["smeta_val"]);
                                                $rtpl->parse("wt_val");   
        
                                            } else {
                                                $rtpl->setCurrentBlock("wt_val");
                                                $rtpl->setVariable("TMCCOUNT1", $rowspan);
                                                $rtpl->setVariable("WT_RR_PRICE", "0");
                                                $rtpl->parse("wt_val");   
                                                
                                            }                                                                         
                                    }
                                    $nds = round($sum*18/100, 2);
                                    $total += $sum;
                                    $total_nds += $nds;
                                    $total_wnds += $sum + $nds;
                                    $rtpl->setVariable("RR_PRICE", number_format($sum + $nds, 2, ',', ' '));
                                    
                                    $cableCount = 0;
                
                                    $sql = "SELECT tmc_tech.id, tmc_tech.count, tmc_sc.serial, tmc_sc.tmc_id, tmc_list.title, tickdata.isrent, tmc_list.metric_flag, tmc_list.cat_id FROM `tmc_ticket` AS tickdata 
                                                                LEFT JOIN `tmc_sc_tech` AS tmc_tech ON tmc_tech.id=tickdata.tmc_tech_sc_id 
                                                                LEFT JOIN `tmc_sc` AS tmc_sc ON tmc_sc.id=tmc_tech.tmc_sc_id 
                                                                LEFT JOIN `list_materials` AS tmc_list ON tmc_list.id=tmc_sc.tmc_id 
                                                                WHERE tickdata.task_id=".$DB->F($res1["task_id"]).";";
                                                        $DB->query($sql);
                                                        if ($DB->errno()) { echo "error ".$sql." ".$DB->error(); trigger_error($DB->error()); return false; }
                                                        $rowspan = 0;
                                                        
                                                        $rC = $DB->num_rows();
                                                        if ($rC>1) {
                                                            $first = true;
                                                            $rtpl->setVariable("TMCCOUNT", $rC);
                                                            $rowspan = $rC;
                                                            while ($res = $DB->fetch(true)) {
                                                                if ($first) {
                                                                    if ($res["metric_flag"]) {
                                                                        $cableCount += $res["count"];
                                                                    } 
                                                                    $first = false;
                                                                    
                                                                } else {
                                                                    if ($res["metric_flag"]) {
                                                                        $cableCount += $res["count"];
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            $res = $DB->fetch(true);
                                                            $rtpl->setVariable("TMCCOUNT", "1");
                                                            $rowspan = 1;
                                                            if ($DB->num_rows()) {
                                                                if ($res["metric_flag"]) {
                                                                    $cableCount += $res["count"];
                                                                }
                                                            }
                                                        }
                                                        $DB->free();
                                    
                                    
                                    
                                    
                                    $rtpl->setVariable("RR_CABLE", intval($cableCount) ? $cableCount." м." : "&mdash;");             
                                    
                                    if ($mgts_intpl) {
                                        $rtpl->setCurrentBlock("mgtsadsltd");
                                        $rtpl->setVariable("RR_ATS", $ticket->getMGTS_ATS() ? $ticket->getMGTS_ATS() : "&mdash;");
                                        $rtpl->setVariable("RR_LANDLINE", $ticket->getMGTS_LandlineNum() ? $ticket->getMGTS_LandlineNum() : "&mdash;");
                                        //$rtpl->setVariable("TMCCOUNT1", $rowspan);
                                        $rtpl->parse("mgtsadsltd");   
                                    }
                                    $rtpl->parse("rep_row");
                                    $i+=1;
                                }
                                $rtpl->setVariable("TOTALCOLSPAN", 13+intval(count($wtList)) + $addcl);
                                $rtpl->setVariable("TOTAL_WONDS", number_format($total, 2, ',', ' '));
                                $rtpl->setVariable("TOTAL_NDS", number_format($total_nds, 2, ',', ' '));
                                $rtpl->setVariable("TOTAL_WNDS", number_format($total_wnds, 2, ',', ' '));
                                $DB->free();
                            } else {
                                UIError("Отсутствует статус с тэгом closed");
                            }
                        } else {
                            UIError("Отсутствует Контрагент с указанным ID.");
                        }
                    } else {
                        UIError("Отсутствует модуль Контрагенты.");
                    }
                } else UIError("Не указан контрагент и/или договор!"); 
                
                $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
                $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
                if (isset($_POST["createxlsversion"])) {
                    header("Content-type: application/vnd.ms-excel");  
                    header("Content-disposition: attachment; filename=report_contr_".$_POST['datefrom']."-".$_POST['dateto'].".xls");  
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    if (isset($_POST['createprintversion'])) {
                        $rtpl->touchBlock("print_footer");
                        $rtpl->show();
                        
                        return;
                    } else {
                        $tpl->setCurrentBlock("reportval");
                        $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                        $tpl->parse("reportval");
                    }
                }
            } else {
                $tpl->touchBlock("notcreated");
                $tpl->setVariable("DISABLED", "disabled='disabled'");
            }
        
        
        $tpl->show();
        
    }
    
    static function getActInfo($id) {
        global $DB, $USER;
        if (!$id) return false;
        $sql = "SELECT ltr.*, DATE_FORMAT(`cr_date`, '%H:%i  %d.%m.%Y') as created FROM `list_tdata_reports` AS ltr WHERE `id`=".$DB->F($id)." ORDER BY `cr_date` DESC, `status` ASC;";
        $result = $DB->getRow($sql, true); 
        if ($DB->errno()) UIError($DB->error());
        return $result;
    }
    static function getActList($status=0) {
        global $DB, $USER;
        $sql = "SELECT ltr.id, CONCAT(if(ltr.title!='', ltr.title, ''), ' № ', ltr.id, ' от ', DATE_FORMAT(`cr_date`, '%H:%i  %d.%m.%Y')) as created FROM `list_tdata_reports` AS ltr WHERE `status`=".$DB->F($status)." ORDER BY `cr_date` DESC, `status` ASC;";
        $result = array2options($DB->getCell2($sql, true)); 
        if ($DB->errno()) UIError($DB->error());
        return $result;
    }
    
    function savenewpin_ajax() {
        global $DB, $USER;
        $task_id = $_REQUEST["task_id"];
        $pinval = $_REQUEST["val"];
        $sql = "UPDATE `tickets` SET `clntopagr`=".$DB->F($pinval)." WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        if ($DB->errno()) {
            echo "error";
            return false;
        } else {
            echo "ok";
        }
        return false;
    }
    
    function savenewcnumval_ajax() {
        global $DB, $USER;
        $task_id = $_REQUEST["task_id"];
        $cval = $_REQUEST["val"];
        $sql = "UPDATE `tickets` SET `tickaddnumber`=".$DB->F($cval)." WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        if ($DB->errno()) {
            echo "error";
            return false;
        } else {
            echo "ok";
        }
        return false;        
    }
}
?>
