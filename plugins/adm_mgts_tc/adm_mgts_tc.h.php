<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Справочник МГТС: ТЦ";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник МГТС: ТЦ";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование ТЦ";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление ТЦ";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение ТЦ";
    
    
}
$PLUGINS[$plugin_uid]['events']['getByATS_ajax'] = "Получение ТЦ по идентификатору АТС";

?>