<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class adm_mgts_tc_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getOptions($id = 0)
    {
        global $DB;
        $sql = "SELECT id, title FROM `mgts_tc` WHERE `active`;";
        $res = $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());
        return array2options($res, $id);
    }

    static function getById($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `mgts_tc` WHERE `id`=" . $DB->F($id) . ";";
        $res = $DB->getField($sql);
        if ($DB->errno()) UIError($DB->error());
        return $res ? $res : false;
    }

    static function getByATS_ajax()
    {
        global $DB, $USER;
        $ats = $_REQUEST["ats"];
        if (!$ats) {
            $ret["error"] = "Не указан идентификатор АТС для подбора.";
        } else {
            $sql = "SELECT `id`, title FROM `mgts_tc` WHERE `desc` LIKE " . $DB->F("$ats,%") . " OR `desc` LIKE " . $DB->F("%,$ats,%") . " OR `desc` LIKE " . $DB->F("%,$ats") . ";";
            $m = $DB->getRow($sql, true);
            if ($m) {
                $ret["ok"] = "ok";
                $ret["id"] = $m['id'];
                $ret["title"] = $m['title'];

            } else {
                $ret["error"] = "Не найден ТЦ МГТС!";
            }
            $ret["sql"] = $sql;

        }
        echo json_encode($ret);
        return false;

    }

    function viewlist()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/adm_mgts_tc.tmpl.htm");
            else
                $tpl->loadTemplatefile("adm_mgts_tc.tmpl.htm");
        $sql = "SELECT * FROM `mgts_tc` WHERE `active`;";
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());
        while (list($id, $title, $desc) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->setVariable("VLI_DESC", $desc);
            $tpl->parse("vl_item");
        }
        $DB->free();
        UIHeader($tpl);
        $tpl->show();
    }

    function edit() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
            else
                $tpl->loadTemplatefile("edit.tmpl.htm");
        if($id = $_REQUEST['id']) {
            $sql = "SELECT * FROM `mgts_tc` WHERE `id`=".$DB->F($id).";";
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("ТЦ с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);

            $tpl->setVariable("ID", $result["id"]);
            $tpl->setVariable("TITLE", $result["title"]);
            $tpl->setVariable("DESC", $result["desc"]);

        }
        UIHeader($tpl);
        $tpl->show();

    }

    function save() {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = array();

        if(!$_POST['title']) $err[] = "Не заполнено название ТЦ";
        if(!$_POST['desc']) $err[] = "Не заполнены номера АТС";

        if(sizeof($err)) UIError($err);

        if($_POST['id']) $sql = "UPDATE `mgts_tc` SET `desc`=".$DB->F($_POST['desc']).", `title`=".$DB->F($_POST['title'])." WHERE `id`=".$DB->F($_POST['id']);
        else {
            $sql = "INSERT INTO `mgts_tc` (`title`, `desc`) VALUES(".$DB->F($_POST['title']).",".$DB->F($_POST['desc']).")";
        }
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());

        if(!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "ТЦ сохранен. ID: ".$_POST['id'] );
    }

    function delete() {
        global $DB;
        if(!($id = $_REQUEST['id'])) $err[] = "Не указан идентификатор записи!";
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($id)) UIError("Выбранная запись связана с заявками. Удаление невозможно.");
        $DB->query("UPDATE `mgts_tc` SET `active`=0 WHERE `id`='".$_REQUEST['id']."';");
        redirect($this->getLink(),"Запись успешно удалена.");

    }

    function isBusy($id)
    {
        global $DB;
        $DB->query("SELECT `task_id` FROM `tickets` WHERE `mgts_tc`=" . $DB->F($id) . ";");
        //die("SELECT `task_id` FROM `tickets` WHERE `task_from`=".$DB->F($id).";");
        if ($DB->num_rows()) $err = true; else $err = false;
        $DB->free();
        return $err;
    }

}
?>
