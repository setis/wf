<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;


require_once(dirname(__FILE__)."/../connections/connections.php");
require_once(dirname(__FILE__)."/../accidents/accidents.php");
require_once(dirname(__FILE__)."/../services/services.php");

 
class reports_f_tech_qc_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        $fintypes = array("connections"=>"Подключение", "services"=>"СКП", "accidents"=>"ТТ");
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_TECH", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            UIHeader($tpl);
            report_left_menu($tpl);
            if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
                $tpl->setVariable("FILTER_TECH_OPTIONS", "<option value='".$USER->getId()."'>".$DB->getField("SELECT `fio` FROM `users` WHERE `id`=".$DB->F($USER->getId())."")."</option>");
                $tpl->setVariable("FILTER_TASK_TYPE", "<option value='0'>-- все --</option>");
            } else {
                $tpl->setVariable("FILTER_TECH_OPTIONS", adm_empl_plugin::getTechList($_POST["tech_id"]));
                $tpl->setVariable("FILTER_TASK_TYPE", array2options($fintypes, $_POST["type_id"]));
            }
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            
        }
        
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
                if ($USER->getId() != $_POST["tech_id"]) {
                    UIError("Вы пытаетесь получить доступ к чужому отчету!");
                    die();
                }
            }
            if ($USER->getTemplate() != "default") 
                    $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
                else 
                    $rtpl->loadTemplatefile("report.tmpl.htm");
            
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
            $rtpl->setVariable("DATETO", $_POST['dateto']);
            $rtpl->setVariable("MONTH", $month[$_POST['month']]);
            $rtpl->setVariable("YEAR", $year[$_POST['year']]);
            $rtpl->setVariable("TASK_TYPES", $fintypes[$_POST["type_id"]]);
            if ($_POST["tech_id"]!=0 && $usr = adm_users_plugin::getUser($_POST["tech_id"])) {
                $qc_techList[]=$_POST["tech_id"];
                $virtAlias = adm_empl_plugin::isVirtual($_POST["tech_id"]);
                if ($virtAlias) $qc_techList[] = $virtAlias;                        
                
                
                
                
                 $rtpl->setVariable("REP_TECH", $usr["fio"]);
                 $sql = "SELECT tqc.*, (SELECT `title` FROM `task_types` WHERE `id`=tick.task_type) as ttname FROM `tickets_qc` AS tqc 
                    LEFT JOIN `tasks` AS t ON (t.id=tqc.task_id) 
                    LEFT JOIN `tickets` AS tick ON (tick.task_id=tqc.task_id) 
                 WHERE 
                 DATE_FORMAT(tqc.qc_date, '%Y-%m')=".$DB->F($_POST['year']."-".$_POST['month'])."
                 AND tqc.task_id IN (SELECT `task_id` FROM `gfx` WHERE `empl_id` IN (".implode(",",$qc_techList)."))
                 AND tqc.qc_status=1
                 AND t.plugin_uid=".$DB->F($_POST["type_id"])." 
                 AND DATE_FORMAT(tqc.qc_date, '%Y-%m-%d')>='2015-07-09'
                 ORDER BY tqc.qc_date";
                 //die( $sql );
                 
                 $DB->query($sql);
                 if ($DB->errno()) {
                    UIError($DB->error());
                 }
                 
                 if ($DB->num_rows()) {
                    $count = 0;
                    $totalMark = 0;
                    $amarks = array();
                    while($r = $DB->fetch(true)) {
                        
                        $sql = "SELECT * FROM `tickets_qc_results` AS t LEFT JOIN `qc_qlist` AS q ON q.id=t.q_id WHERE q.weight>0 AND t.task_id=".$DB->F($r["task_id"])." ORDER BY q.iorder;";
                        $DB->query($sql);
                        if (!$DB->num_rows()) {
                            $DB->free();   
                            continue;
                        }
                        $rtpl->setCurrentBlock("rep_row");
                        while ($m = $DB->fetch(true)) {
                            $amarks[$m["q_id"]] += $m["new_answer_mark"];
                            $rtpl->setCurrentBlock("qlist_val");
                            $rtpl->setVariable("RR_PIN", $m["new_answer_mark"]);
                            $rtpl->parse("qlist_val");   
                        }
                        $DB->free();
                        $rtpl->setVariable("RR_ID", $r["task_id"]);
                        $rtpl->setVariable("RR_LINK", link2($_POST["type_id"]."/viewticket?task_id=".$r["task_id"], false));
                        $rtpl->setVariable("RR_TYPE", $fintypes[$_POST["type_id"]]." - ".$r["ttname"]);
                        $rtpl->setVariable("RR_CALLDATE", date("H:i:s d.m.Y", strtotime($r["qc_date"])));
                        $rtpl->setVariable("RR_MARK", $r["qc_new_result"]!="" ? $r["qc_new_result"] : ($r["qc_result"]?"Старая оценка: ".$r["qc_result"]:"&mdash;"));
                        
                        if ($r["qc_new_result"]!="" ) {
                            if ($r["qc_new_result"]== 100) {
                                $rtpl->setVariable("RR_CALLRESULT_STYLE", "style='background: #B0FFB7; font-weight: bold;' title='Отлично'");
                            } else {
                                if ($r["qc_new_result"]>=90 && $r["qc_new_result"]<=99.99) {
                                    $rtpl->setVariable("RR_CALLRESULT_STYLE", "style='background: #00ffd2; font-weight: bold;' title='Удовлетворительно'");
                                } else {
                                    if ($r["qc_new_result"]>=86 && $r["qc_new_result"]<=89.99) {
                                        $rtpl->setVariable("RR_CALLRESULT_STYLE", "style='background: #fff600; font-weight: bold;' title='Плохо'");
                                    } else {
                                        if ($r["qc_new_result"]==0 && ($r['qc_status']=="2" || $r['qc_status']=="3")) {
                                            $rtpl->setVariable("RR_CALLRESULT_STYLE", "style='background: inherit; color: #000000; font-weight: bold;'  title='Отказ от опроса'");
                                        } else {
                                            if ($r["qc_new_result"]<86)   
                                                $rtpl->setVariable("RR_CALLRESULT_STYLE", "style='background: #FF0000; color: #FFFFFF; font-weight: bold;'  title='Очень плохо'");                
                                        }
                                    } 
                                }
                            }
                        }
                        
                        $rtpl->parse("rep_row");
                        $totalMark +=$r["qc_new_result"];
                        $count += 1;
                    }   
                    $fcnt = count($amarks);
                    if ($fcnt) {
                        foreach($amarks as $key => $i) {
                            $rtpl->setCurrentBlock("qlist");
                            $rtpl->setVariable("QLINK", link2("q_list/editq?id=".$key, false));
                            $rtpl->setVariable("Q_ID", $key);
                            $rtpl->parse("qlist");
                        
                        
                        }
                    }
                    $rtpl->setCurrentBlock("rep_footer");
                    $rtpl->setVariable("QC_COUNT", $count);
                    $rtpl->setVariable("MID_RESULT", round($totalMark/$count,3));
                    $midres = round($totalMark/$count,3);
                    if ($midres!="" ) {
                            if ($midres== 100) {
                                $rtpl->setVariable("MIDCALLRESULT_STYLE", "style='background: #B0FFB7; font-weight: bold;' title='Отлично'");
                            } else {
                                if ($midres>=90 && $midres<=99.99) {
                                    $rtpl->setVariable("MIDCALLRESULT_STYLE", "style='background: #00ffd2; font-weight: bold;' title='Удовлетворительно'");
                                } else {
                                    if ($midres>=86 && $midres<=89.99) {
                                        $rtpl->setVariable("MIDCALLRESULT_STYLE", "style='background: #fff600; font-weight: bold;' title='Плохо'");
                                    } else {
                                        if ($midres==0 && ($r['qc_status']=="2" || $r['qc_status']=="3")) {
                                            $rtpl->setVariable("MIDCALLRESULT_STYLE", "style='background: inherit; color: #000000; font-weight: bold;'  title='Отказ от опроса'");
                                        } else {
                                            if ($midres<86)   
                                                $rtpl->setVariable("MIDCALLRESULT_STYLE", "style='background: #FF0000; color: #FFFFFF; font-weight: bold;'  title='Очень плохо'");                
                                        }
                                    } 
                                }
                            }
                        }
                        
                    
                    
                    
                    
                    foreach($amarks as $key => $i) {
                        $rtpl->setCurrentBlock("qlist_val_footer");
                        $rtpl->setVariable("MID_QMARK", round($i/$count,3));
                        $rtpl->parse("qlist_val_footer");
                    }
                    $rtpl->parse("rep_footer");
                 } else {
                    $rtpl->touchBlock("no-rows");
                 }
                 
                 
            } else UIError("Не указан Техник"); 
            
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_tech_".$_POST["tech_id"]."_".date("d")."_".$_POST['month']."-".$_POST['year'].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 

}
?>