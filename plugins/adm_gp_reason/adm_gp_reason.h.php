<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Справочник причин списания ТМЦ с заявки";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';


if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник причин списания ТМЦ с заявки";
    
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование причины списания ТМЦ с заявки";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление причины списания ТМЦ с заявки";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение причины списания ТМЦ с заявки";
}

?>