<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2011
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Комментарии (добавление, удаление, операции с файлами)";
$PLUGINS[$plugin_uid]['hidden'] = true;

//if(wf::$user->checkAccess($plugin_uid))
//{ 
    $PLUGINS[$plugin_uid]['events']['addcmm'] = "Добавление комментария";
    $PLUGINS[$plugin_uid]['events']['addcmm_ajax'] = "Добавление комментария";    
    $PLUGINS[$plugin_uid]['events']['upload'] = "Загрузка файла";
    $PLUGINS[$plugin_uid]['events']['upload_ajax'] = "Загрузка файла";
    $PLUGINS[$plugin_uid]['events']['task_files_ajax'] = "Загрузка файла";
    $PLUGINS[$plugin_uid]['events']['getfile'] = "Скачивание файла";
    $PLUGINS[$plugin_uid]['events']['delete_file_ajax'] = "Удаление файла";
    $PLUGINS[$plugin_uid]['events']['delete_allfiles_ajax'] = "Удаление всех файлов";
    $PLUGINS[$plugin_uid]['events']['download_all'] = "Скачать все файлы";

//}

$plugin_uid = "allowfiledelete";
$PLUGINS[$plugin_uid]['name'] = "Удаление файлов, прикрепленных к задачам, проектам, счетам";
$PLUGINS[$plugin_uid]['hidden'] = true;
