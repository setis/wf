<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2011
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use classes\tickets\ConnectionTicket;
use classes\tickets\ServiceTicket;
use classes\tickets\Ticket;
use classes\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
class comments_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function addcmm($return = false, &$err = null)
    {
        global $USER;

        //Debugger::dump($_POST, true);
        if (!$err) $err = array();
        $skpmail = false;
        $isSKP = false;

        //// EF: исправление бага с tinymce, иногда не передается содержимое поля cmm_text
        if (($_POST['cmm_text'] === '') && (strlen($_POST['cmm_text_src']) > 0)) {
            $_POST['cmm_text'] = $_POST['cmm_text_src'];
        }

        if (!$_POST['task_id']) $err[] = "Нет ID задачи";
        if (!$_POST['cmm_text'] && !$_POST['status_id'] && !$_POST['poruch_user_ids'] && empty($_FILES['userfile']['error'])) {
            $err[] = "Нет параметров комментария";
        }

        if (sizeof($err)) {
            if ($return) return false;
            else UIError($err, "Ошибка добавления комментария", defined('AJAX') ? false : true);
        }
        $is_enc = isset($_POST["is_enc"]) ? true : false;
        $tag = '';
        $task = new Task($_POST['task_id']);
        if ($task->getPluginUid() == "connections" && (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()))) {
            $ct = new ConnectionTicket($task->getId());
            if ($_POST["lk_pin"]) {
                $ct->setClntOpAgr($_POST["lk_pin"]);
                $ct->updateCommit();
                if ($_POST["lk_pin"]) {
                    $tag .= "ПИН: <b>" . $_POST["lk_pin"] . "</b>";
                }
            }
        }


        if ($task->getPluginUid() == "services") {
            $st = new ServiceTicket($task->getId());
            if ($st->getTypeName() == "СКП") {
                $isSKP = true;
                if ($_POST["allowstatus"] == "ok") {
                    $st->setPrice($_POST["lk_price"]);
                    $st->setOrientPrice($_POST["lk_price"]);
                    $st->setTicketDocNum($_POST["lk_docnum"]);
                    $st->updateCommit();
                }
            }
        }
        if ($_POST["allowstatus"] != "ok") {
            if ($_POST['status_id'] && ($status_name = $task->getStatusNameById($_POST['status_id']))) {
                $tag .= ($tag ? '; ' : '') . "Статус <b>$status_name</b>" . (isset($_POST["or_price"]) && $task->getPluginUid() == "services" ? "; Предварительная стоимость: <b>" . $_POST["or_price"] . "</b>" : "");
                if ($_POST["or_price"]) {
                    $st = new ServiceTicket($_POST["task_id"]);
                    $st->setOrientPrice($_POST["or_price"], true);
                }
            } else {
                $_POST['status_id'] = 0;
            }
        } else {
            $tag .= ($tag ? '; ' : '') . "Сдача отчета техника. Стоимость: " . $_POST["lk_price"] . ". № квитанции: " . $_POST["lk_docnum"] . ". Комментарий техника: " . $_POST["lk_comment"];

        }
        $targetStatus = null;
        if ($_POST['poruch_user_ids']) {
            $executorsTag = $task->setExecutors(explode(',', $_POST['poruch_user_ids']));
            if ($_POST['poruch_user_ids'] && $executorsTag !== false) {
                $tag .= ($tag ? '; ' : '') . "Задача перепоручена <b>" . $task->getIspoln(', ') . "</b>";
                if ($isSKP) {
                    $skpmail = true;
                    $targetStatus = 44;
                }
            }
        }
        $tag = $tag != '' ? $tag : @$_POST['tag'];
        $cb_err = array();
        $setCStatus = false;
        $user_ids = explode(',', $_POST['poruch_user_ids']);
        if ($targetStatus == 44) { // отправка СМС о СКП отвественным
            if ($setCStatus) {
                $_POST["status_id"] = 44;
                $tag = "Статус: <b>Выбор инженера</b> " . $tag;
            }
        }


        $files = "";
        if (isset($_FILES['userfile']) && isset($_FILES['userfile']['error']) && is_array($_FILES['userfile']['error']) && count($_FILES['userfile']['error']) > 0) {
            foreach ($_FILES['userfile']['error'] as $i => $error) {
                if ($error == UPLOAD_ERR_OK) {
                    if ($files_id = $task->addFile($_FILES['userfile']['tmp_name'][$i], $_FILES['userfile']['name'][$i], $_FILES['userfile']['type'][$i])) {

                        if (strpos($_FILES['userfile']['type'][$i], 'image') === 0) {
                            $files .= "<a href='/comments/getfile?id=" . $files_id . "&name=" . urlencode($_FILES['userfile']['name'][$i]) . "' data-lightbox='" . $files_id . "'>" . $_FILES['userfile']['name'][$i] . "</a>, ";
                        } else {
                            $files .= "<a href='/comments/getfile?id=" . $files_id . "&name=" . urlencode($_FILES['userfile']['name'][$i]) . "'>" . $_FILES['userfile']['name'][$i] . "</a>, ";
                        }
                    } else {
                        $err[] = "Не удалось добавить файл '{$_FILES['userfile']['name'][$i]}' к задаче";
                    }
                } elseif ($error != UPLOAD_ERR_NO_FILE) {
                    $err[] = "Не удалось загрузить файл '{$_FILES['userfile']['name'][$i]}' на сервер";
                }
            }
        }

        if ($files) {
            $_POST["cmm_text"] .= " ";
            $tag .= " Добавлен файл(ы): " . preg_replace("/, $/", "", $files);
        }

        if (!$_POST['cmm_text']) {
            $_POST['cmm_text'] = " ";
        }

        if ($task->addComment($_POST['cmm_text'], $tag, $_POST['status_id'], $cb_err)) {

            if (!$isSKP) {
                $task->toMail();
            } else {
                if ($skpmail) {
                    $task->toMail();
                }
            }

            if ($return) {
                return true;
            }

            redirect(getenv("HTTP_REFERER"));
        } else {
            if (sizeof($cb_err)) {
                $err = array_merge($err, $cb_err);
            } else {
                $err[] = "Не удалось добавить комментарий" . $r;
            }

            if ($return) {
                return false;
            }

            UIError($err, "Ошибка добавления комментария", defined('AJAX') ? false : true);
        }
    }

    function addcmm_ajax()
    {
        $err = array();

        $cmm = $_POST["cmm_text"];
        $is_enc = isset($_POST["is_enc"]) ? true : false;
        $cmm = $is_enc ? ($is_enc ? urldecode($cmm) : $cmm) : $cmm;

        $_POST["cmm_text"] = $cmm;

        if ($this->addcmm(true, $err)) {
            $task = new Task($_POST['task_id']);
            return $task->getLastComment();
        } else {
            UIError($err);
        }
    }

    function upload($return = false, &$err = null)
    {
        if (!$err) $err = array();

        if (!$_POST['task_id']) $err[] = "Нет ID задачи";
        //if($_FILES['userfile']['error'] != UPLOAD_ERR_OK) $err[] = "Ошибка загрузки файла код '{$_FILES['userfile']['error']}'";
        ///
        if (sizeof($err)) {
            if ($return) return false;
            else UIError($err, "Ошибка добавления файла", defined('AJAX') ? false : true);
        }

        $task = new Task($_POST['task_id']);
        /*
        if($file_id = $task->addFile($_FILES['userfile']['tmp_name'], $_FILES['userfile']['name'], $_FILES['userfile']['type'])) {
            $task->addComment("Добавлен файл", "Загружен файл <b>{$_FILES['userfile']['name']}</b>");
            if($return) return true;
            else redirect(getenv("HTTP_REFERER"));
        } else {
            $err[] = "Не удалось добавить файл";
            if($return) return false;
            else UIError($err, "Ошибка добавления файла", defined('AJAX') ? false : true);
        }
		*/
        $cmm = '';
        foreach ($_FILES['userfile']['error'] as $i => $error) {
            if ($error == UPLOAD_ERR_OK) {
                if ($task->addFile($_FILES['userfile']['tmp_name'][$i], $_FILES['userfile']['name'][$i], $_FILES['userfile']['type'][$i])) {
                    $cmm .= "{$_FILES['userfile']['name'][$i]}\r\n";
                } else $err[] = "Не удалось добавить файл '{$_FILES['userfile']['name'][$i]}' к задаче";
            } elseif ($error != UPLOAD_ERR_NO_FILE) {
                $err[] = "Не удалось загрузить файл '{$_FILES['userfile']['name'][$i]}' на сервер";
            }
        }
        if ($cmm) {
            $task->addComment($cmm, "Добавлен файл(ы)");
        }
        if ($return) {
            return sizeof($err) ? false : true;
        }
        if (sizeof($err)) {
            UIError($err, "Не все файлы были загружены", defined('AJAX') ? false : true);
        } else {
            redirect(getenv("HTTP_REFERER"));
        }
    }

    function upload_ajax()
    {
        $err = array();
        if ($this->addcmm(true, $err)) {
            $task = new Task($_POST['task_id']);
            die($task->getLastComment());
        } else {
            UIError($err);
        }
    }

    function task_files_ajax()
    {
        global $DB, $USER;

        $err = array();
        if (!$_REQUEST['task_id']) $err[] = "Нет ID задачи";
        if (sizeof($err)) UIError($err);

        $tpl = new HTML_Template_IT(path2("templates"));
        $tpl->loadTemplatefile("task_files.tmpl.html");

        $task = new Task($_REQUEST['task_id']);
        $flist = $task->getFiles();
        $fs = $this->getContainer()->get('oneup_flysystem.task_fs_filesystem');

        if (count($flist) < 1) {
            $tpl->touchBlock("no-files");
            return new Response($tpl->get());
        }

        $tpl->setCurrentBlock("file-list");
        foreach ($flist as $item) {
            if (!$fs->has($item->getFileName())) {
                continue;
            }

            $isfile = true;
            $tpl->setCurrentBlock('row');

            $tpl->setVariable('ID', $item->getId());
            if ($USER->getId() != $task->getAuthorId() && !$USER->checkAccess("allowfiledelete", User::ACCESS_WRITE)) {
                $tpl->setVariable("NOACCESSTODELETEFILE", "hidden");
            }
            $tpl->setVariable('NAME', $item->getOrigName());

            $tpl->setVariable("AUTHOR", $item->getUser() ? $item->getUser()->getFio() : "(noname)" );
            $tpl->setVariable("DATE", $item->getDatetime()->format('Y-m-d H:i'));
            $tpl->setVariable("TASK_ID", $_REQUEST['task_id']);
            if (strpos($item->getMimeType(), 'image') === 0) {
                $tpl->touchBlock("file-with-preview");
            } else {
                $tpl->touchBlock("file-without-preview");
            }

            $tpl->parseCurrentBlock();

        }
        $tpl->parse("file-list");

        return new Response($tpl->get());

    }

    function getfile(Request $request)
    {
        /* @var $fileEntity models\TaskFile */
        $fileEntity = $this->getEm()->find(models\TaskFile::class, $request->get('id'));

        if (null === $fileEntity) {
            UIError('Нет такого файла');
        }

        /* @var $storage Vich\UploaderBundle\Storage\StorageInterface */
        $storage = $this->getContainer()->get('vich_uploader.storage');
        $stream = $storage->resolveStream($fileEntity, 'file');
        $response = new Symfony\Component\HttpFoundation\StreamedResponse(function () use ($stream) {
            stream_copy_to_stream($stream, fopen('php://output', 'w'));
        });
        $response->headers->add([
            'Content-Type' => $fileEntity->getMimeType(),
            'Content-Disposition' => 'attachment; filename="' . $fileEntity->getOrigName() . '"',
        ]);

        if ($request->get('download')) {
            $response->headers->set('Content-Type', 'application/octet-stream');
        }

        return $response;
    }

    function download_all(Request $request)
    {
        $task_id = $request->get('task_id');

        /* @var $task \models\Task */
        $task = $this->getEm()
            ->find(\models\Task::class, $task_id);

        if (null === $task) {
            throw new Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Task not found');
        }

        $ticket = new Ticket($task->getId());

        $zip = new ZipArchive();

        $zip_name = sys_get_temp_dir() . DIRECTORY_SEPARATOR . implode('_', array_filter(array('task', $task_id, $ticket->getClntTNum(), date('Y-m-d-His') . ".zip"))); // Zip name

        if (($result = $zip->open($zip_name, ZipArchive::CREATE)) !== true) {
            throw new \Exception($result);
        }

        /* @var $storage Vich\UploaderBundle\Storage\StorageInterface */
        $storage = $this->getContainer()->get('vich_uploader.storage');

        foreach ($task->getFiles() as $file) {
            $stream = $storage->resolveStream($file, 'file');
            if (false === $stream) {
                continue;
            }

            $zip->addFromString(iconv('utf-8', 'CP866//TRANSLIT//IGNORE', $file->getOrigNameUniq()), stream_get_contents($stream));
        }

        $zip->close();

        $response = new Symfony\Component\HttpFoundation\BinaryFileResponse($zip_name);
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($zip_name) . '";');
        $response->sendHeaders();
        $response->deleteFileAfterSend(true);

        return $response;
    }


    function delete_file_ajax()
    {
        global $DB, $USER;
        if (!$_REQUEST['task_id']) $ret["error"] = "Нет ID задачи";
        if (!$_REQUEST['file_id']) $ret["error"] = "Нет ID файла";
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $task = new Task($_REQUEST['task_id']);
        if ($USER->getId() != $task->getAuthorId() && !$USER->checkAccess("allowfiledelete", User::ACCESS_WRITE)) {
            $ret["error"] = "В доступе отказано!";
            return json_encode($ret);
            return false;
        }
        $res = $task->deleteFile($_REQUEST["file_id"], $_REQUEST["task_id"]);
        if ($res !== FALSE) {
            $_POST["cmm_text"] = "Удален файл: " . $res;
            $_POST["tag"] = "Удаление файла: " . $res;
            if ($this->addcmm(true, $err)) {
                $ret["ok"] = "ok";
                $ret["tpl"] = $task->getLastComment();
            } else {
                $ret["error"] = "Ошибка добавления коммпентария";
            }
            unset($_POST);
            $_POST = array();
            //$task->addComment("Удален файл: ".$res, "Удаление файла: ".$res);
            //$ret["tpl"] = $task->getLastComment();
        } else {
            $ret["error"] = "Ошибка удаления файла.";
        }
        echo json_encode($ret);
    }

    function delete_allfiles_ajax()
    {
        global $DB, $USER;
        if (!$_REQUEST['task_id']) $ret["error"] = "Нет ID задачи";
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $task = new Task($_REQUEST['task_id']);
        if ($USER->getId() != $task->getAuthorId() && !$USER->checkAccess("allowfiledelete", User::ACCESS_WRITE)) {
            $ret["error"] = "В доступе отказано!";
            echo json_encode($ret);
            return false;
        }
        $flist = $task->getFiles();
        $err = array();
        if ($flist) {
            foreach ($flist as $item) {
                $res = $task->deleteFile($item->getId(), $task->getId());
                if ($res === FALSE) {
                    $err[] = $item->getId();
                }
            }
            if (sizeof($err)) {
                $ret["error"] = "Ошибка удаления файла(ов): " . implode(",", $err);
                echo json_encode($ret);
                return false;
            } else {
                $ret["ok"] = "ok";
            }
        } else {
            $ret["error"] = "Нет прикрепленных файлов!";
            echo json_encode($ret);
            return false;
        }
        $_POST["cmm_text"] = "Удалены все файлы!";
        $_POST["tag"] = "Удалены все файлы!";
        if ($this->addcmm(true, $err)) {
            $ret["ok"] = "ok";
            $ret["tpl"] = $task->getLastComment();
        } else {
            $ret["error"] = "Ошибка добавления коммпентария";
        }
        unset($_POST);
        $_POST = array();
        echo json_encode($ret);
        return false;
    }
}
