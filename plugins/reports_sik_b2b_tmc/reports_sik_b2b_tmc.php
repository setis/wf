<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\tickets\ConnectionTicket;


require_once(dirname(__FILE__)."/../connections/connections.php");

 
class reports_sik_b2b_tmc_plugin extends Plugin
{

    const PARTNER_ID = 791;

    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    
    
    function main() {
        global $DB, $USER;
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/reports_mgts_b2b_tmc.tmpl.htm");
            else 
                $tpl->loadTemplatefile("reports_contr.tmpl.htm");
            $tpl->setVariable("REPORT_CONTR", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date('Y-m-d') ." - 1 month")));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            $tpl->setVariable("WTYPE_OPTIONS", array2options($DB->getCell2("SELECT `id`, `title` FROM `mgts_services`"), $_POST["wtype"]));
            
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
            $rtpl->setVariable("DATETO", $_POST['dateto']);
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
            $closedStatus = adm_statuses_plugin::getStatusByTag("closed", "connections");
            $reportStatus = adm_statuses_plugin::getStatusByTag("report", "connections");
            $reporttmcStatus = adm_statuses_plugin::getStatusByTag("clctmc", "connections");
            // убрать хардкод для id контрагента и id вида работ заявки (в config.php)
            $filter = $_POST["wtype"]>0 ? " AND tick.mgts_sertype=".$DB->F($_POST["wtype"])." " : "";                                    
            $sql = "SELECT 
                        tick.task_id, 
                        if((select `empl_id` from `gfx` where `task_id`=tick.task_id limit 1)>0, (select `fio` from `users` where id=(select `empl_id` from `gfx` where `task_id`=tick.task_id limit 1)), 0) as tech, 
                        (SELECT SUM(case when ltc.smeta_val>0 then ltc.smeta_val*ltc.price/100 else ltc.price*ltc.quant end) FROM `list_contr_calc` AS ltc WHERE ltc.task_id=tick.task_id) as sum, 
                        (SELECT `title` FROM `mgts_services` WHERE `id`=tick.mgts_sertype) as sertype
                    FROM `tickets` AS tick 
                        LEFT JOIN `tasks` AS t ON t.id=tick.task_id
                        LEFT JOIN `task_comments` AS tc ON tc.task_id=t.id
                    WHERE 
                        t.status_id IN (".$doneStatus["id"].", ".$closedStatus["id"].", ".$reportStatus["id"].", ".$reporttmcStatus["id"].") 
                        AND tick.cnt_id='".self::PARTNER_ID."' 
                        AND (tick.task_type='20')
                        AND t.plugin_uid='connections' 
                        AND (DATE_FORMAT(tc.datetime, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST["datefrom"])))." 
                            AND DATE_FORMAT(tc.datetime, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST["dateto"])))." 
                            AND tc.status_id=".$DB->F($closedStatus["id"]).") $filter 
                        GROUP BY t.id ORDER BY sertype, t.status_id, tc.datetime;";
            //die($sql);
            $m = $DB->getCell($sql);
            $tmcsql = "SELECT tmc.tmc_id, (SELECT `title` FROM `list_materials` WHERE `id`=tmc.tmc_id) as tmc_name FROM `tmc_sc` AS tmc LEFT JOIN `tmc_sc_tech` AS tst ON tst.tmc_sc_id=tmc.id LEFT JOIN `tmc_ticket` AS tt ON tt.tmc_tech_sc_id=tst.id WHERE tt.task_id IN (".implode(",",$m).") GROUP BY tmc.tmc_id";
            $tmc_h = $DB->getCell2($tmcsql);
            if ($tmc_h) {
                $thcount = count($tmc_h);
                foreach($tmc_h as $item) {
                    $rtpl->setCurrentBlock("devth");
                    $rtpl->setVariable("DEVNAME", $item);
                    $rtpl->parse("devth");
                }
            } else {
                $thcount = 0;
            }
            //die($sql);
            $DB->query($sql);
            $total = 0;
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $rtpl->setCurrentBlock("rep_row");
                    $t = new ConnectionTicket($r["task_id"]);
                    $rtpl->setVariable("MGTS_ID", $t->getClntTNum());
                    $rtpl->setVariable("MGTS_HREF", link2("connections/viewticket?task_id=".$r["task_id"], false));
                    $tc = $DB->getField("SELECT `title` FROM `mgts_tc` WHERE `id`=".$DB->F($t->getMGTS_TC()).";");
                    $rtpl->setVariable("MGTS_WTYPE", $r["sertype"] ? $r["sertype"] : "не указан");
                    $rtpl->setVariable("MGTS_TC", $tc ? $tc : "не указан");
                    $rtpl->setVariable("MGTS_ATS", $t->getMGTS_ATS() ? $t->getMGTS_ATS() : "не указан");
                    $rtpl->setVariable("MGTS_ORSH", $t->getMGTS_ORSH() ? $t->getMGTS_ORSH() : "не указан");
                    $rtpl->setVariable("MGTS_STATUS_NAME", $t->getStatusName());
                    $rtpl->setVariable("MGTS_COLOR", $t->getStatusColor());
                    $rtpl->setVariable("MGTS_TECH", $r["tech"] ? $r["tech"] : "&mdash;");
                    $addr = $t->getAddr($t->getDomId());
                    if ($addr) 
                        $rtpl->setVariable("MGTS_ADDR", $addr["city"]." ".$addr["street"]." ".$addr["house"]." ".$t->getKv());
                    else 
                        $rtpl->setVariable("MGTS_ADDR", "Некорректный формат адреса");
                    $rtpl->setVariable("MGTS_CLNT", $t->getOrgName() ? $t->getOrgName() : $t->getFio());
                    $total += $r["sum"];
                    $rtpl->setVariable("MGTS_PRICE", number_format($r["sum"], 2, ","," "));
                    foreach($tmc_h as $key => $item) {
                        $rtpl->setCurrentBlock("devlist");
                        $tmc = "SELECT tt.count, tmc.serial FROM `tmc_sc` AS tmc LEFT JOIN `tmc_sc_tech` AS tst ON tst.tmc_sc_id=tmc.id LEFT JOIN `tmc_ticket` AS tt ON tt.tmc_tech_sc_id=tst.id WHERE tt.task_id=".$DB->F($r["task_id"])." AND tmc.tmc_id=".$DB->F($key).";";
                        $tmcr = $DB->getRow($tmc, true);
                        $rtpl->setVariable("MGTS_DLITEM", ($tmcr["count"] ? "Кол-во: ".$tmcr["count"] : "&mdash;"). ($tmcr["serial"] ? "<br />"."Сер.Ном.:".$tmcr["serial"] : ""));
                        $rtpl->parse("devlist");
                    }
                    $rtpl->parse("rep_row");
                }
                $rtpl->setCurrentBlock("rep_row_total");
                $rtpl->setVariable("TOTAL_PRICE",  number_format($total, 2, ","," "));
                $rtpl->setVariable("DEVCOLCOUNT", $thcount);
                if (!$thcount) {
                    $rtpl->setVariable("DEVCOLFOOTER", "class='hidden'");
                }
                $rtpl->parse("rep_row_total");
            } else {
                $rtpl->touchBlock("no-rows");
            }
            
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_contr_".$_POST['datefrom']."-".$_POST['dateto'].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 

}
?>
