<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\tickets\ServiceTicket;


require_once(dirname(__FILE__)."/../services/services.php");

class reports_f_skp_accn_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $_REQUEST["sc_id"] = $this->getCookie("sc_id", (isset($_REQUEST["sc_id"]) ? $_REQUEST["sc_id"] : 8));
        $_REQUEST["createreport"] = $this->getCookie("createreport", $_REQUEST["createreport"]);
        $_REQUEST["month"] = $this->getCookie('month', $_REQUEST["month"] ? $_REQUEST["month"] : date("m"));
        $_REQUEST["year"] = $this->getCookie('year', $_REQUEST["year"] ? $_REQUEST["year"] : date("Y"));
        $dateParams = array(0 => "Дата создания", 1 => "Дата выполнения", 2=>"Дата изменения", 3=>"Дата принятия ДС");
        if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
            $cntr = new kontr_plugin();
        }   
        $contrCount = $cntr->getListSer();                    
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_SKP_TOTAL", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            $tpl->setVariable("MONTH", array2options($month, $_REQUEST["month"]));
            $tpl->setVariable("YEAR", array2options($year, $_REQUEST["year"]));
            
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id"]));
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListSer(@$_POST["cnt_id"]));
            } 
            
            
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_REQUEST['createprintversion']) || @$_REQUEST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $period = $_REQUEST["year"]."-".$_REQUEST["month"];
            $rtpl->setVariable('CURRENT_PERIOD', $month[$_REQUEST["month"]]." ".$year[$_REQUEST["year"]]);
            $sc = adm_sc_plugin::getSC($_REQUEST["sc_id"]);
            $rtpl->setVariable("SC_NAME", $_REQUEST["sc_id"] ? $sc['title'] : "Все");
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "services");
            $countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "services");
            $acceptedStatus = adm_statuses_plugin::getStatusByTag("acc_accepted", "services");
            $otkaz = adm_statuses_plugin::getStatusByTag("otkaz", "services");
            $opotkaz = adm_statuses_plugin::getStatusByTag("opotkaz", "services");
            
            /*$sql = "SELECT t.id FROM `tasks` AS t 
                            LEFT JOIN `task_users` AS tu ON tu.task_id=t.id 
                            LEFT JOIN tickets AS tick ON tick.task_id=t.id 
                            WHERE t.plugin_uid='services' AND
                                tu.user_id IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`= ".$DB->F($_REQUEST["sc_id"]).")
                                GROUP BY t.id ORDER BY 1;"; 
            *///die($sql);
            if ($_REQUEST["sc_id"]>0) 
                $sql_user = "SELECT `id` FROM `users` WHERE `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`= ".$DB->F($_REQUEST["sc_id"]).") AND `active` ORDER BY `fio`";
            else
                $sql_user = "SELECT `id` FROM `users` WHERE `active` ORDER BY `fio`";
            if ($_REQUEST["cnt_id"]>0) {
                $add_sql = " AND pr.cnt_id=".$DB->F($_REQUEST["cnt_id"]);
            }
            $base = "SELECT 
                        pr.cnt_id, 
                        pr.contr_title, 
                        pr.empl_id, 
                        pr.user, 
                        SUM(if(pr.donedate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"]).",1,0)) as completed,
                        SUM(case when pr.donedate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])." and pr.status_id in (".$doneStatus["id"].", ".$countStatus["id"].", ".$acceptedStatus["id"].") then pr.orient_price else 0 end) as ticksumm,
                        SUM(case when pr.donedate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])." and pr.status_id in (".$doneStatus["id"].", ".$countStatus["id"].", ".$acceptedStatus["id"].") then pr.orient_price*0.3 else 0 end) as techpay,
                        SUM(case when pr.donedate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])." and pr.status_id in (".$doneStatus["id"].", ".$countStatus["id"].", ".$acceptedStatus["id"].") then pr.orient_price*pr.contr_fee*0.01 else 0 end) as agent_fee,
                        SUM(case when pr.reportdate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])." or pr.accdate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])." then pr.orient_price else 0 end) as virpost,
                        SUM(case when pr.reportdate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])." or pr.accdate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])." then pr.orient_price*0.3 else 0 end) as techpayfull,
                        SUM(case when pr.donedate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])." and pr.status_id in (".$doneStatus["id"].") then pr.orient_price else 0 end) as dz,
                        SUM(case when pr.donedate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])." and pr.status_id in (".$doneStatus["id"].") then pr.orient_price*0.3 else 0 end) as kz
                        
                        
                    FROM 
                        (SELECT 
                            gfx.empl_id, 
                            t.id, 
                            tick.cnt_id, 
                            (SELECT `contr_title` FROM `list_contr` WHERE `id`=tick.cnt_id) as contr_title, 
                            (SELECT `contragentfee` FROM `list_contr` WHERE `id`=tick.cnt_id) as contr_fee, 
                            t.status_id, 
                            tick.orient_price, 
                            (SELECT `fio` FROM `users` WHERE `id`=gfx.empl_id) as user,
                            (SELECT DATE_FORMAT(MAX(`datetime`), '%Y-%m') FROM `task_comments` WHERE `task_id`=t.id AND `status_id`=".$doneStatus["id"].") as donedate,
                            (SELECT DATE_FORMAT(MAX(`datetime`), '%Y-%m') FROM `task_comments` WHERE `task_id`=t.id AND `status_id`=".$countStatus["id"].") as reportdate,
                            (SELECT DATE_FORMAT(MAX(`datetime`), '%Y-%m') FROM `task_comments` WHERE `task_id`=t.id AND `status_id`=".$acceptedStatus["id"].") as accdate,
                            agr.paytype
                        FROM `tasks` AS t 
                            LEFT JOIN `tickets` AS tick ON tick.task_id=t.id 
                            LEFT JOIN `list_contr_agr` AS agr ON agr.id=tick.agr_id
                            LEFT JOIN `gfx` AS gfx ON gfx.task_id=t.id 
                        WHERE 
                            t.plugin_uid='services' 
                            AND t.status_id IN (".$doneStatus["id"].",".$countStatus["id"].",".$acceptedStatus["id"].",".$completeStatus["id"].")
                            AND gfx.empl_id IN ($sql_user)
                            AND agr.paytype!=2) 
                    AS pr 
                    WHERE 
                        (pr.donedate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])." 
                    OR 
                        pr.reportdate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])." 
                    OR 
                        pr.accdate=".$DB->F($_REQUEST["year"]."-".$_REQUEST["month"])."
                    ) $add_sql GROUP BY pr.cnt_id, pr.user";
            $DB->query($base); 
            if (!$DB->num_rows()) {
                $rtpl->touchBlock("no-rows");
            } else {
                while($m = $DB->fetch(true)) {
                    $i+=1;
                    $r[$m["contr_title"]][] = $m;   
                }
                if (count($r)) {
                    $total_amm = 0;
                    $total_techpay = 0;
                    $total_fot = 0;
                    $total_agent = 0;
                    $total_vir = 0;
                    $total_tpf = 0;
                    $total_dz = 0;
                    $total_kz = 0;
                    foreach($r as $key => $row) {
                        $rtpl->setCurrentBlock("ctgrb");
                        $rtpl->setVariable("CTRGTITLE", $key);
                        $ptamm = 0;
                        $techpay = 0;
                        $fot = 0;
                        $agent = 0;
                        $vir = 0;
                        $tpf = 0;
                        $dz = 0;
                        $kz = 0;
                        foreach($row as $cti) {
                            $rtpl->setCurrentBlock("rep_row");
                            $rtpl->setVariable("CTGR", $key);
                            $rtpl->setVariable("PERIOD", rudate("M Y", strtotime($_POST["year"]."-".$_POST["month"])));
                            $rtpl->setVariable("QUANT", $cti["completed"]);
                            $rtpl->setVariable("FIO", $cti["user"]);
                            $rtpl->setVariable("AMMOUNT", number_format($cti["ticksumm"],2,","," "));
                            $rtpl->setVariable("FOT_NACH", number_format($cti["techpay"],2,","," "));
                            $rtpl->setVariable("PREM", "&mdash;");
                            $rtpl->setVariable("PENALT", "&mdash;");
                            $rtpl->setVariable("AGENT", number_format($cti["agent_fee"],2,","," "));
                            $rtpl->setVariable("INCEARN", number_format($cti["virpost"],2,","," "));
                            $rtpl->setVariable("FOT", number_format($cti["techpayfull"],2,","," "));
                            $rtpl->setVariable("DZ", number_format($cti["dz"],2,","," "));
                            $rtpl->setVariable("KZ", number_format($cti["kz"],2,","," "));
                            $rtpl->parse("rep_row");
                            $ptamm += $cti["ticksumm"];
                            $total_amm += $cti["ticksumm"];
                            $techpay += $cti["techpay"];
                            $total_techpay += $cti["techpay"];
                            $fot += $cti["techpay"];
                            $total_fot += $cti["techpay"];
                            $agent += $cti["agent_fee"];
                            $total_agent += $cti["agent_fee"];
                            $vir += $cti["virpost"];
                            $total_vir += $cti["virpost"];
                            $tpf += $cti["techpayfull"];
                            $total_tpf += $cti["techpayfull"];
                            $dz += $cti["dz"];
                            $total_dz += $cti["dz"];
                            $kz += $cti["kz"];
                            $total_kz += $cti["kz"];
                        }
                        $rtpl->setCurrentBlock("ptotal");
                            $rtpl->setVariable("PT_AMMOUNT", number_format($ptamm, 2, ",", " "));
                            $rtpl->setVariable("PT_FOT_NACH", number_format($fot, 2, ",", " "));
                            $rtpl->setVariable("PT_AGENT", number_format($agent, 2, ",", " "));
                            $rtpl->setVariable("PT_INCEARN", number_format($vir, 2, ",", " "));
                            $rtpl->setVariable("PT_FOT", number_format($tpf, 2, ",", " "));
                            $rtpl->setVariable("PT_DZ", number_format($dz, 2, ",", " "));
                            $rtpl->setVariable("PT_KZ", number_format($kz, 2, ",", " "));
                            $rtpl->setVariable("PT_PREM", "&mdash;");
                            $rtpl->setVariable("PT_PENALT", "&mdash;");
                        $rtpl->parse("ptotal");
                        $rtpl->parse("ctgrb");
                    }
                    $rtpl->setCurrentBlock("rep-total");
                    $rtpl->setVariable("TOTAL_AMMOUNT", number_format($total_amm, 2, ",", " "));
                    $rtpl->setVariable("TOTAL_FOT_NACH", number_format($total_fot, 2, ",", " "));
                    $rtpl->setVariable("TOTAL_AGENT", number_format($total_agent, 2, ",", " "));
                    $rtpl->setVariable("TOTAL_INCEARN", number_format($total_vir, 2, ",", " "));
                    $rtpl->setVariable("TOTAL_FOT", number_format($total_tpf, 2, ",", " "));
                    $rtpl->setVariable("TOTAL_DZ", number_format($total_dz, 2, ",", " "));
                    $rtpl->setVariable("TOTAL_KZ", number_format($total_kz, 2, ",", " "));
                    $rtpl->setVariable("TOTAL_PREM", "&mdash;");
                    $rtpl->setVariable("TOTAL_PENALT", "&mdash;");
                    $rtpl->parse("rep-total");
                }
            }
            $DB->free();
            $rtpl->setVariable("REP_CDATE", date("d.m.Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            
            
            if (isset($_REQUEST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_skp_total__".$_REQUEST["month"]."-".$_REQUEST["year"].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_REQUEST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 
    function acceptMoney() {
        global $DB, $USER;
        $task_id = $_REQUEST["amtask_id"];
        $docnum = $_REQUEST["tdnum"];
        $ammount = $_REQUEST["ammount"];
                   
        if (!$task_id || !$docnum || !!$ammount) {
            $res["error"] = "Ошибка: форма заполнена не полностью";
            
        }
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        
        $sql = "UPDATE `tickets` SET `inmoney`=1 WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] = $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $sql = "UPDATE `tickets` SET `tdocnum`=".$DB->F($docnum)." WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $sql = "UPDATE `tickets` SET `inmoneydate`=".$DB->F(date("Y-m-d H:i:s"))." WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $sql = "UPDATE `skp_money` SET `last`=0 WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $dd = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `skp_money` (`task_id`, `ammount`, `le`, `user_id`, `last`) VALUES (".$DB->F($task_id).", ".$DB->F(implode("", explode(" ", $ammount))).", ".$DB->F($dd).", ".$DB->F($USER->getId()).", 1);";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
        $sql = "SELECT `status_id` FROM `tasks` WHERE `id`=".$DB->F($task_id).";";
        $r = $DB->getField($sql);
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $t = new ServiceTicket($task_id);
        if ($r != $countStatus["id"]) {
            $ret["newstatus"] = $countStatus["name"];
            $t->addComment("Прием денег. Сумма: ".number_format($ammount, 2, '.', ' '), "Автоматическое обновление статуса на Сдача отчета при приеме денег от техника", $countStatus["id"]);        
        } else
            $t->addComment("Прием денег. Сумма: ".number_format($ammount, 2, '.', ' '), "Обновление принятой от техника суммы", 0);
        $ret["ok"] = "ok";
        $ret["am"] = number_format($ammount, 2, '.', ' ');
        $ret["dn"] = $docnum;
        $ret["dd"] = $dd;
        echo json_encode($ret);
        return false;
    }
}
?>