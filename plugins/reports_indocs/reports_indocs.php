<?php
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use classes\tickets\AccidentTicket;
use classes\tickets\ConnectionTicket;
use classes\tickets\ServiceTicket;

class reports_indocs_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function main()
    {
        global $DB, $USER;
        $types = ["connections" => "Подключения", "services" => "СКП", "gp" => "ГП", "accidents" => "ТТ"];
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/reports_indocs.tmpl.htm");
        else
            $tpl->loadTemplatefile("reports_contr.tmpl.htm");
        $tpl->setVariable("REPORT_CONTR", "buttonsel1");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        UIHeader($tpl);
        $protocol = $this->getSession()->get("protocol");
        if (sizeof($protocol)) {
            $tpl->setVariable("TODAY", $protocol['date']);
            $tpl->setVariable("USER_NAME", $protocol['author']);
        }
        $protocol['author'] = $USER->getFio();
        $protocol['date'] = rudate("d F Y", 0, false);
        $this->getSession()->set('protocol', $protocol);
        if (count($protocol["items"])) {
            foreach ($protocol["items"] as $key => $item) {
                $tpl->setCurrentBlock("rep_row");
                $tpl->setVariable("PROT_ITEMS", $item);
                $tpl->parse("rep_row");
            }
            $tpl->setVariable("NO_ROWS", "hidden");
        }
        $tpl->setVariable("TODAY", $protocol['date']);
        $tpl->setVariable("USER_NAME", $protocol['author']);
        $tpl->show();
    }

    function protocol_view()
    {
        global $DB, $USER;
        $types = ["connections" => "Подключения", "services" => "СКП", "gp" => "ГП", "accidents" => "ТТ"];
        $docrtypes = ["0" => "Акт", "1" => "Акт ТМЦ"];

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/protocolview.tmpl.htm");
        else
            $tpl->loadTemplatefile("protocolview.tmpl.htm");
        $tpl->setVariable("REPORT_CONTR", "buttonsel1");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $proto = $_REQUEST["id"];
        UIHeader($tpl);
        $sql = "SELECT pl.*, (SELECT COUNT(`task_id`) FROM `tickets` WHERE `doc_ticket`=pl.id OR `tmc_ticket`=pl.id) AS doc_count, (SELECT `fio` FROM `users` WHERE `id`=pl.cr_usr) AS author,(SELECT `fio` FROM `users` WHERE `id`=pl.user_id) AS user FROM `tickets_docs_protocols` pl WHERE `id`=" . $DB->F($proto) . ";";
        $mm = $DB->getRow($sql, true);
        if ($DB->errno()) UIError($DB->error());
        if (!$mm) {
            redirect(link2("reports_indocs", false), "Протокол не найден в системе");

            return false;
        }
        $tpl->setVariable("CDATE", rudate("d F Y", strtotime($mm[""])));
        $tpl->setVariable("PR_AUTHOR", $mm["author"]);
        $tpl->setVariable("PR_FROM", $mm["user"]);
        $tpl->setVariable("PRINT_LINK", "reports_indocs/print_protocol?id=" . $proto);
        $sql = "SELECT `task_id`, `doc_ticket`, `tmc_ticket` FROM `tickets` WHERE `tmc_ticket`=" . $DB->F($proto) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            $tpl->setVariable("NO_ROWS", "hidden");
            $i = 0;
            while ($m = $DB->fetch(true)) {
                $tpl->setCurrentBlock("rep_row");
                $tpl->setVariable("ROW_ID", ++$i);
                $tpl->setVariable("PROTO_ID", $proto);
                $dt = "Акт TMC";
                $ticket_code = 1;
                $t = new Task($m["task_id"]);
                if ($t) {
                    $task_id = $t->getId();
                    switch ($t->getPluginUid()) {
                        case "connections":
                            $task = new ConnectionTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("connections/viewticket?task_id=" . $task->getId(), false));
                            $addr = $task->getAddr($task->getDomId());
                            if ($addr["nokladr"]) {
                                $tpl->setVariable('ROW_TASK_ADDR', "<font color='#FF0000'>Адрес не в формате КЛАДР</font>");
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr["city"] . " " . $addr["street"] . " д." . ($addr["house"] ? $addr["house"] : $addr["althouse"]) . " " . $task->getKv());
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "connections");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "services":
                            $task = new ServiceTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $addr = $task->getAddr($task->getDomId());
                            $tpl->setVariable('ROW_TASK_ADDR', $addr . " " . $task->getKv());
                            $tpl->setVariable("TASK_LINK", link2("services/viewticket?task_id=" . $task->getId(), false));

                            $ds = adm_statuses_plugin::getStatusByTag("done", "services");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "gp":
                            $task_gp = new GP($task_id);

                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task_gp->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task_gp->getContrNumber());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task_gp->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task_gp->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("gp/viewticket?task_id=" . $task_gp->getId(), false));

                            $addr = $DB->getField("SELECT la.full_address FROM list_addr la JOIN gp_addr ga ON ga.dom_id = la.id AND ga.task_id = " . $DB->F($task_gp->getId()) . " LIMIT 1;");
                            $addrCount = $DB->getField("SELECT COUNT(*) FROM `gp_addr` WHERE `task_id`=" . $DB->F($task_gp->getId()) . ";");
                            if ($addr) {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr . (($addrCount - 1) > 0 ? " <br /><small><strong>и еще " . ($addrCount - 1) . " шт.</strong></small>" : ""));
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', "Адреса не распознаны. " . ($addrCount ? $addrCount . " шт." : ""));
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "gp");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task_gp->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "accidents":
                            $task = new AccidentTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("accidents/viewticket?task_id=" . $task->getId(), false));

                            $addr = $task->getAddr($task->getDomId());
                            if ($addr["nokladr"]) {
                                $tpl->setVariable('ROW_TASK_ADDR', "<font color='#FF0000'>Адрес не в формате КЛАДР</font>");
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr["city"] . " " . $addr["street"] . " д." . ($addr["house"] ? $addr["house"] : $addr["althouse"]) . " " . $task->getKv());
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "accidents");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        default:
                            UIError("Тип заявки неопределен" . $task_id);
                            break;
                    }

                } else {
                    UIError("Не найдена заявка с ID:" . $task_id);
                }
                $tpl->parse("rep_row");
            }
        }

        $sql = "SELECT `task_id`, `doc_ticket`, `tmc_ticket` FROM `tickets` WHERE `doc_ticket`=" . $DB->F($proto) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            $tpl->setVariable("NO_ROWS", "hidden");
            $i = 0;
            while ($m = $DB->fetch(true)) {
                $tpl->setCurrentBlock("rep_row");
                $tpl->setVariable("ROW_ID", ++$i);
                $tpl->setVariable("PROTO_ID", $proto);
                $dt = "Акт";
                $ticket_code = 0;
                $t = new Task($m["task_id"]);
                if ($t) {
                    $task_id = $t->getId();
                    switch ($t->getPluginUid()) {
                        case "connections":
                            $task = new ConnectionTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("connections/viewticket?task_id=" . $task->getId(), false));
                            $addr = $task->getAddr($task->getDomId());
                            if ($addr["nokladr"]) {
                                $tpl->setVariable('ROW_TASK_ADDR', "<font color='#FF0000'>Адрес не в формате КЛАДР</font>");
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr["city"] . " " . $addr["street"] . " д." . ($addr["house"] ? $addr["house"] : $addr["althouse"]) . " " . $task->getKv());
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "connections");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "services":
                            $task = new ServiceTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $addr = $task->getAddr($task->getDomId());
                            $tpl->setVariable('ROW_TASK_ADDR', $addr . " " . $task->getKv());
                            $tpl->setVariable("TASK_LINK", link2("services/viewticket?task_id=" . $task->getId(), false));

                            $ds = adm_statuses_plugin::getStatusByTag("done", "services");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "gp":
                            $task_gp = new GP($task_id);

                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task_gp->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task_gp->getContrNumber());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task_gp->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task_gp->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("gp/viewticket?task_id=" . $task_gp->getId(), false));

                            $addr = $DB->getField("SELECT la.full_address FROM list_addr la JOIN gp_addr ga ON ga.dom_id = la.id AND ga.task_id = " . $DB->F($task_gp->getId()) . " LIMIT 1;");
                            $addrCount = $DB->getField("SELECT COUNT(*) FROM `gp_addr` WHERE `task_id`=" . $DB->F($task_gp->getId()) . ";");
                            if ($addr) {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr . (($addrCount - 1) > 0 ? " <br /><small><strong>и еще " . ($addrCount - 1) . " шт.</strong></small>" : ""));
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', "Адреса не распознаны. " . ($addrCount ? $addrCount . " шт." : ""));
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "gp");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task_gp->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "accidents":
                            $task = new AccidentTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("accidents/viewticket?task_id=" . $task->getId(), false));

                            $addr = $task->getAddr($task->getDomId());
                            if ($addr["nokladr"]) {
                                $tpl->setVariable('ROW_TASK_ADDR', "<font color='#FF0000'>Адрес не в формате КЛАДР</font>");
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr["city"] . " " . $addr["street"] . " д." . ($addr["house"] ? $addr["house"] : $addr["althouse"]) . " " . $task->getKv());
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "accidents");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        default:
                            UIError("Тип заявки неопределен" . $task_id);
                            break;
                    }

                } else {
                    UIError("Не найдена заявка с ID:" . $task_id);
                }
                $tpl->parse("rep_row");
            }
        }


        $tpl->show();
    }

    function clear_protocol()
    {
        global $DB, $USER;
        $this->getSession()->remove('protocol');
        redirect(link2("reports_indocs", false), "Протокол очишен!");

        return false;
    }

    function addticket_ajax()
    {
        global $DB, $USER;
        $task_id = false;
        $search = false;
        $types = ["connections" => "Подключения", "services" => "СКП", "gp" => "ГП", "accidents" => "ТТ"];
        $docrtypes = ["0" => "Акт", "1" => "Акт ТМЦ"];
        $code = $_REQUEST["ticket_code"];
        $protocol = $this->getSession()->get('protocol');
        if ($protocol['items'][$_REQUEST["ticket_code"]]) {
            $ret["error"] = "Данный документ уже добавлен в протокол";
            echo json_encode($ret);

            return false;
        }
        if (preg_match("/-/", $code)) {
            $code = explode("-", $code);
            if (preg_match("/\d+/", $code[0]) && ($code[1] == "1" || $code[1] == "0")) {
                $task_id = trim($code[0]);
                $type = trim($code[1]);
            }
        } else {
            $search = trim($code);
        }
        // добавить поиск по произвольному коду
        if (!$task_id) {
            $ret["error"] = "Некорректный штрих-код";
            echo json_encode($ret);

            return false;
        } else {
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate() . "/task_row.tmpl.htm");
            else
                $tpl->loadTemplatefile("task_row.tmpl.htm");
            if ($task_id) {
                $t = new Task($task_id);
                $field = $type > 0 ? "tmc_ticket" : "doc_ticket";
                $checkIfNotAcceptedYet = $DB->getField("SELECT count(`task_id`) FROM `tickets` WHERE `task_id`=" . $DB->F($task_id) . " AND `$field`;");
                if ($t && !$checkIfNotAcceptedYet) {
                    switch ($t->getPluginUid()) {
                        case "connections":
                            $task = new ConnectionTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $_REQUEST["ticket_code"]);
                            $tpl->setVariable("ROW_ID", count($protocol["items"]) > 0 ? count($protocol["items"]) + 1 : "1");
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $docrtypes[$type] ? $docrtypes[$type] : "Прочие документы");
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("connections/viewticket?task_id=" . $task->getId(), false));
                            $addr = $task->getAddr($task->getDomId());
                            if ($addr["nokladr"]) {
                                $tpl->setVariable('ROW_TASK_ADDR', "<font color='#FF0000'>Адрес не в формате КЛАДР</font>");
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr["city"] . " " . $addr["street"] . " д." . ($addr["house"] ? $addr["house"] : $addr["althouse"]) . " " . $task->getKv());
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "connections");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());

                            $ret["ok"] = "ok";
                            $ret["tpl"] = $tpl->get();
                            $protocol['items'][$_REQUEST["ticket_code"]] = $tpl->get();
                            $protocol["tasks"][$_REQUEST["ticket_code"]] = ["task_id" => $task->getId(), "plugin_uid" => $task->getPluginUid(), "date" => date("Y-m-d H:i:s"), "doc_type" => $type];

                            break;

                        case "services":
                            $task = new ServiceTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $_REQUEST["ticket_code"]);
                            $tpl->setVariable("ROW_ID", count($protocol["items"]) > 0 ? count($protocol["items"]) + 1 : "1");
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $docrtypes[$type] ? $docrtypes[$type] : "Прочие документы");
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $addr = $task->getAddr($task->getDomId());
                            $tpl->setVariable('ROW_TASK_ADDR', $addr . " " . $task->getKv());
                            $tpl->setVariable("TASK_LINK", link2("services/viewticket?task_id=" . $task->getId(), false));

                            $ds = adm_statuses_plugin::getStatusByTag("done", "services");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());

                            $ret["ok"] = "ok";
                            $ret["tpl"] = $tpl->get();
                            $protocol['items'][$_REQUEST["ticket_code"]] = $tpl->get();
                            $protocol["tasks"][$_REQUEST["ticket_code"]] = ["task_id" => $task->getId(), "plugin_uid" => $task->getPluginUid(), "date" => date("Y-m-d H:i:s"), "doc_type" => $type];
                            break;

                        case "gp":
                            $task_gp = new GP($task_id);

                            $tpl->setVariable("ITEM_CODE", $_REQUEST["ticket_code"]);
                            $tpl->setVariable("ROW_ID", count($protocol["items"]) > 0 ? count($protocol["items"]) + 1 : "1");
                            $tpl->setVariable("ROW_DOC_TYPE", $docrtypes[$type] ? $docrtypes[$type] : "Прочие документы");
                            $tpl->setVariable("ROW_TASK_ID", $task_gp->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task_gp->getContrNumber());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task_gp->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task_gp->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("gp/viewticket?task_id=" . $task_gp->getId(), false));

                            $addr = $DB->getField("SELECT la.full_address FROM list_addr la JOIN gp_addr ga ON ga.dom_id = la.id AND ga.task_id = " . $DB->F($task_gp->getId()) . " LIMIT 1;");
                            $addrCount = $DB->getField("SELECT COUNT(*) FROM `gp_addr` WHERE `task_id`=" . $DB->F($task_gp->getId()) . ";");
                            if ($addr) {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr . (($addrCount - 1) > 0 ? " <br /><small><strong>и еще " . ($addrCount - 1) . " шт.</strong></small>" : ""));
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', "Адреса не распознаны. " . ($addrCount ? $addrCount . " шт." : ""));
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "gp");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task_gp->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());

                            $ret["ok"] = "ok";
                            $ret["tpl"] = $tpl->get();
                            $protocol['items'][$_REQUEST["ticket_code"]] = $tpl->get();
                            $protocol["tasks"][$_REQUEST["ticket_code"]] = ["task_id" => $task_gp->getId(), "plugin_uid" => $task_gp->getPluginUid(), "date" => date("Y-m-d H:i:s"), "doc_type" => $type];

                            break;

                        case "accidents":
                            $task = new AccidentTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $_REQUEST["ticket_code"]);
                            $tpl->setVariable("ROW_ID", count($protocol["items"]) > 0 ? count($protocol["items"]) + 1 : "1");
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $docrtypes[$type] ? $docrtypes[$type] : "Прочие документы");
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("accidents/viewticket?task_id=" . $task->getId(), false));

                            $addr = $task->getAddr($task->getDomId());
                            if ($addr["nokladr"]) {
                                $tpl->setVariable('ROW_TASK_ADDR', "<font color='#FF0000'>Адрес не в формате КЛАДР</font>");
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr["city"] . " " . $addr["street"] . " д." . ($addr["house"] ? $addr["house"] : $addr["althouse"]) . " " . $task->getKv());
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "accidents");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());

                            $ret["ok"] = "ok";
                            $ret["tpl"] = $tpl->get();
                            $protocol['items'][$_REQUEST["ticket_code"]] = $tpl->get();
                            $protocol["tasks"][$_REQUEST["ticket_code"]] = ["task_id" => $task->getId(), "plugin_uid" => $task->getPluginUid(), "date" => date("Y-m-d H:i:s"), "doc_type" => $type];
                            break;

                        default:
                            $ret["error"] = "Тип заявки неопределен" . $task_id;
                            break;

                    }
                    $this->getSession()->set('protocol', $protocol);
                    echo json_encode($ret);

                    return false;
                } else {
                    $ret["error"] = "Заявка не найдена или, возможно, присутствует в другом протоколе приема документов.";
                    echo json_encode($ret);

                    return false;
                }
            } else {
                // $search сюда (произвольный код)
            }
        }

        echo json_encode($ret);

        return false;
    }

    function remove_item()
    {
        global $DB, $USER;
        $item = $_REQUEST["item_code"];
        if ($item) {
            $protocol = $this->getSession()->get('protocol');
            $protocol['items'][$item] = "";
            unset($protocol['items'][$item]);
            $protocol['tasks'][$item] = "";
            unset($protocol['tasks'][$item]);
            $this->getSession()->set('protocol', $protocol);
            $msg = "Запись удалена";
        } else $msg = "Не указана запись для удаления из протокола";
        redirect(link2("reports_indocs", false), $msg);

        return false;
    }

    function protocol_list()
    {
        global $DB, $USER;

        $types = ["connections" => "Подключения", "services" => "СКП", "gp" => "ГП", "accidents" => "ТТ"];
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/protocollist.tmpl.htm");
        else
            $tpl->loadTemplatefile("protocollist.tmpl.htm");
        $tpl->setVariable("REPORT_CONTR", "buttonsel1");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        UIHeader($tpl);
        $sql = "SELECT 
            pl.*, 
            (SELECT COUNT(`task_id`) FROM `tickets` WHERE `tmc_ticket`=pl.id) AS tmc_count, 
            (SELECT COUNT(`task_id`) FROM `tickets` WHERE `doc_ticket`=pl.id) AS doc_count, 
            (SELECT `fio` FROM `users` WHERE `id`=pl.cr_usr) AS author,
            (SELECT `fio` FROM `users` WHERE `id`=pl.user_id) AS user 
        FROM 
          `tickets_docs_protocols` pl WHERE 1;";

        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            $tpl->setVariable("NO_ROWS", "hidden");
            while ($m = $DB->fetch(true)) {
                $tpl->setCurrentBlock("rep_row");
                $tpl->setVariable("P_NUM", $m["id"]);
                $tpl->setVariable("P_DATE", !$m["cr_date"] ? '-' : rudate("d F Y", strtotime($m["cr_date"])));
                $tpl->setVariable("P_AUTHOR", $m["author"]);
                $tpl->setVariable("P_DC", $m["doc_count"] + $m["tmc_count"]);
                $tpl->parse("rep_row");
            }
        }
        $DB->free();
        $tpl->show();

        return false;
    }

    function remove_protocol()
    {
        global $DB, $USER;
        $proto = $_REQUEST["id"];
        if (!$proto) {
            redirect(link2("reports_indocs/protocol_view?id=" . $_REQUEST["id"], false), "Не указан протокол для удаления.");

            return false;

        } else {
            $sql = "UPDATE `tickets` SET `tmc_ticket`=0, `tmc_date`=" . $DB->F(null, true) . " WHERE `tmc_ticket`=" . $DB->F($proto) . ";";
            $DB->query($sql);
            if ($DB->errno()) {
                redirect(link2("reports_indocs/protocol_list", false), "Ошибка: " . $DB->error());

                return false;

            }
            $DB->free();
            $sql = "UPDATE `tickets` SET `doc_ticket`=0, `doc_date`=" . $DB->F(null, true) . " WHERE `doc_ticket`=" . $DB->F($proto) . ";";
            $DB->query($sql);
            if ($DB->errno()) {
                redirect(link2("reports_indocs/protocol_list", false), "Ошибка: " . $DB->error());

                return false;
            }
            $DB->free();
            $sql = "DELETE FROM `tickets_docs_protocols` WHERE `id`=" . $DB->F($proto) . ";";
            $DB->query($sql);
            if ($DB->errno()) {
                redirect(link2("reports_indocs/protocol_list", false), "Ошибка: " . $DB->error());

                return false;
            }
            $DB->free();
            redirect(link2("reports_indocs/protocol_list", false), "Протокол удален");

            return false;
        }
    }

    function remove_task()
    {
        global $DB, $USER;
        $task_id = $_REQUEST["task_id"];
        $type = $_REQUEST["type_id"];
        if (!$task_id) {
            redirect(link2("reports_indocs/protocol_view?id=" . $_REQUEST["id"], false), "Не выбрана заявка и тип документа");

            return false;
        }
        if ($type > 0)
            $sql = "UPDATE `tickets` SET `tmc_ticket`=0, `tmc_date`=" . $DB->F(null, true) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
        else
            $sql = "UPDATE `tickets` SET `doc_ticket`=0, `doc_date`=" . $DB->F(null, true) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
        $DB->query($sql);
        if ($DB->errno()) {
            redirect(link2("reports_indocs/protocol_view?id=" . $_REQUEST["id"], false), "Ошибка: " . $DB->error());

            return false;
        }
        $sql = "SELECT count(`task_id`) FROM `tickets` WHERE `doc_ticket`=" . $DB->F($_REQUEST["id"]) . " OR `tmc_ticket`=" . $DB->F($_REQUEST["id"]) . ";";
        $m = $DB->getField($sql);
        if ($m == 0) {
            $sql = "DELETE FROM `tickets_docs_protocols` WHERE `id`=" . $DB->F($_REQUEST["id"]) . ";";
            $DB->query($sql);
            if (!$DB->errno()) {
                redirect(link2("reports_indocs/protocol_list", false), "Прием документов по заявке " . $task_id . " отменен. Протокол удален.");

                return false;
            } else {
                redirect(link2("reports_indocs/protocol_list", false), "Ошибка: " . $DB->error());

                return false;
            }
        }
        redirect(link2("reports_indocs/protocol_view?id=" . $_REQUEST["id"], false), "Прием документов по заявке " . $task_id . " отменен.");

        return false;
    }

    function create_protocol()
    {
        global $DB, $USER;
        if ($_POST["filter_empl_id"]) {
            $protocol = $this->getSession()->get('protocol');
            if (count($protocol["tasks"])) {
                $sql = "INSERT INTO  `tickets_docs_protocols` (`user_id`, `cr_usr`, cr_date) VALUES (" . $DB->F($_POST["filter_empl_id"]) . ", " . $DB->F($USER->getId()) . ", now());";
                $DB->query($sql);
                if ($DB->errno()) {
                    redirect(link2("reports_indocs", false), "Ошибка создания протокола! " . $DB->error());

                    return false;
                }
                $m = $DB->insert_id();
                $DB->free();
                if (!$m) {
                    redirect(link2("reports_indocs", false), "Ошибка создания протокола!");

                    return false;
                }
                $cdate = date("Y-m-d");
                foreach ($protocol["tasks"] as $code => $item) {
                    //print_r($item);
                    switch ($item["plugin_uid"]) {
                        case "connections":
                            $t = new ConnectionTicket($item["task_id"]);
                            if ($t) {
                                if ($item["doc_type"] == 0) {
                                    $t->setDocID($m);
                                    $t->setDocDate($cdate);
                                    $t->updateCommit();
                                    $t->addComment("Принят акт по заявке.", "Прием документов");
                                } else {
                                    $t->setTMCDocID($m);
                                    $t->setTMCDocDate($cdate);
                                    $t->updateCommit();
                                    $t->addComment("Принят акт ТМЦ по заявке.", "Прием документов");
                                }
                            } else {
                                redirect(link2("reports_indocs", false), "Ошибка определения типа заявки: " . $item['task_id']);

                                return false;

                                break;
                            }
                            break;
                        case "services":
                            $t = new ServiceTicket($item["task_id"]);
                            if ($t) {
                                if ($item["doc_type"] == 0) {
                                    $t->setDocID($m);
                                    $t->setDocDate($cdate);
                                    $t->updateCommit();
                                    $t->addComment("Принят акт по заявке.", "Прием документов");
                                } else {
                                    $t->setTMCDocID($m);
                                    $t->setTMCDocDate($cdate);
                                    $t->updateCommit();
                                    $t->addComment("Принят акт ТМЦ по заявке.", "Прием документов");
                                }
                            } else {
                                redirect(link2("reports_indocs", false), "Ошибка определения типа заявки: " . $item['task_id']);

                                return false;
                                break;
                            }
                            break;
                        case "gp":
                            $t = new GP($item["task_id"]);
                            if ($t) {
                                if ($item["doc_type"] == 0) {
                                    $t->setDocID($m);
                                    $t->setDocDate($cdate);
                                    $t->updateCommit();
                                    $t->addComment("Принят акт по заявке.", "Прием документов");
                                } else {
                                    $t->setTMCDocID($m);
                                    $t->setTMCDocDate($cdate);
                                    $t->updateCommit();
                                    $t->addComment("Принят акт ТМЦ по заявке.", "Прием документов");
                                }
                            } else {
                                redirect(link2("reports_indocs", false), "Ошибка определения типа заявки: " . $item['task_id']);

                                return false;
                                break;
                            }
                            break;
                        case "accidents":
                            $t = new AccidentTicket($item["task_id"]);
                            if ($t) {
                                if ($item["doc_type"] == 0) {
                                    $t->setDocID($m);
                                    $t->setDocDate($cdate);
                                    $t->updateCommit();
                                    $t->addComment("Принят акт по заявке.", "Прием документов");
                                } else {
                                    $t->setTMCDocID($m);
                                    $t->setTMCDocDate($cdate);
                                    $t->updateCommit();
                                    $t->addComment("Принят акт ТМЦ по заявке.", "Прием документов");
                                }
                            } else {
                                redirect(link2("reports_indocs", false), "Ошибка определения типа заявки: " . $item['task_id']);

                                return false;
                                break;
                            }
                            break;


                        default:
                            redirect(link2("reports_indocs", false), "Ошибка определения типа заявки: " . $item['task_id']);

                            return false;
                            break;
                    }


                }
                $protocol["tasks"] = [];
                unset($protocol["tasks"]);
                $protocol["items"] = [];
                unset($protocol["items"]);

                $this->getSession()->set('protocol', $protocol);
                redirect(link2("reports_indocs", false), "Протокол создан успешно!");

                return false;
            }
        } else {
            redirect(link2("reports_indocs", false), "Необходимо выбрать сотрудника, сдавшего документы!");

            return false;
        }

        return false;

    }

    function print_protocol()
    {
        global $DB, $USER;
        $types = ["connections" => "Подключения", "services" => "СКП", "gp" => "ГП", "accidents" => "ТТ"];
        $docrtypes = ["0" => "Акт", "1" => "Акт ТМЦ"];

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/protocolview_print.tmpl.htm");
        else
            $tpl->loadTemplatefile("protocolview_print.tmpl.htm");
        $tpl->setVariable("REPORT_CONTR", "buttonsel1");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $proto = $_REQUEST["id"];
        //UIHeader($tpl);
        $sql = "SELECT pl.*, (SELECT COUNT(`task_id`) FROM `tickets` WHERE `doc_ticket`=pl.id OR `tmc_ticket`=pl.id) AS doc_count, (SELECT `fio` FROM `users` WHERE `id`=pl.cr_usr) AS author,(SELECT `fio` FROM `users` WHERE `id`=pl.user_id) AS user FROM `tickets_docs_protocols` pl WHERE `id`=" . $DB->F($proto) . ";";
        $mm = $DB->getRow($sql, true);
        if ($DB->errno()) UIError($DB->error());
        if (!$mm) {
            redirect(link2("reports_indocs", false), "Протокол не найден в системе");

            return false;
        }
        $tpl->setVariable("CDATE", rudate("d F Y", strtotime($mm[""])));
        $tpl->setVariable("PR_AUTHOR", $mm["author"]);
        $tpl->setVariable("PR_FROM", $mm["user"]);

        $sql = "SELECT `task_id`, `doc_ticket`, `tmc_ticket` FROM `tickets` WHERE `tmc_ticket`=" . $DB->F($proto) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            $tpl->setVariable("NO_ROWS", "hidden");
            $i = 0;
            while ($m = $DB->fetch(true)) {
                $tpl->setCurrentBlock("rep_row");
                $tpl->setVariable("ROW_ID", ++$i);
                $tpl->setVariable("PROTO_ID", $proto);
                $dt = "Акт TMC";
                $ticket_code = 1;
                $t = new Task($m["task_id"]);
                if ($t) {
                    $task_id = $t->getId();
                    switch ($t->getPluginUid()) {
                        case "connections":
                            $task = new ConnectionTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("connections/viewticket?task_id=" . $task->getId(), false));
                            $addr = $task->getAddr($task->getDomId());
                            if ($addr["nokladr"]) {
                                $tpl->setVariable('ROW_TASK_ADDR', "<font color='#FF0000'>Адрес не в формате КЛАДР</font>");
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr["city"] . " " . $addr["street"] . " д." . ($addr["house"] ? $addr["house"] : $addr["althouse"]) . " " . $task->getKv());
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "connections");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "services":
                            $task = new ServiceTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $addr = $task->getAddr($task->getDomId());
                            $tpl->setVariable('ROW_TASK_ADDR', $addr . " " . $task->getKv());
                            $tpl->setVariable("TASK_LINK", link2("services/viewticket?task_id=" . $task->getId(), false));

                            $ds = adm_statuses_plugin::getStatusByTag("done", "services");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "gp":
                            $task_gp = new GP($task_id);

                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task_gp->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task_gp->getContrNumber());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task_gp->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task_gp->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("gp/viewticket?task_id=" . $task_gp->getId(), false));

                            $addr = $DB->getField("SELECT la.full_address FROM list_addr la JOIN gp_addr ga ON ga.dom_id = la.id AND ga.task_id = " . $DB->F($task_gp->getId()) . " LIMIT 1;");
                            $addrCount = $DB->getField("SELECT COUNT(*) FROM `gp_addr` WHERE `task_id`=" . $DB->F($task_gp->getId()) . ";");
                            if ($addr) {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr . (($addrCount - 1) > 0 ? " <br /><small><strong>и еще " . ($addrCount - 1) . " шт.</strong></small>" : ""));
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', "Адреса не распознаны. " . ($addrCount ? $addrCount . " шт." : ""));
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "gp");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task_gp->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "accidents":
                            $task = new AccidentTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("accidents/viewticket?task_id=" . $task->getId()));

                            $addr = $task->getAddr($task->getDomId());
                            if ($addr["nokladr"]) {
                                $tpl->setVariable('ROW_TASK_ADDR', "<font color='#FF0000'>Адрес не в формате КЛАДР</font>");
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr["city"] . " " . $addr["street"] . " д." . ($addr["house"] ? $addr["house"] : $addr["althouse"]) . " " . $task->getKv());
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "accidents");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        default:
                            UIError("Тип заявки неопределен" . $task_id);
                            break;
                    }

                } else {
                    UIError("Не найдена заявка с ID:" . $task_id);
                }
                $tpl->parse("rep_row");
            }
        }

        $sql = "SELECT `task_id`, `doc_ticket`, `tmc_ticket` FROM `tickets` WHERE `doc_ticket`=" . $DB->F($proto) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            $tpl->setVariable("NO_ROWS", "hidden");
            $i = 0;
            while ($m = $DB->fetch(true)) {
                $tpl->setCurrentBlock("rep_row");
                $tpl->setVariable("ROW_ID", ++$i);
                $tpl->setVariable("PROTO_ID", $proto);
                $dt = "Акт";
                $ticket_code = 0;
                $t = new Task($m["task_id"]);
                if ($t) {
                    $task_id = $t->getId();
                    switch ($t->getPluginUid()) {
                        case "connections":
                            $task = new ConnectionTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("connections/viewticket?task_id=" . $task->getId()));
                            $addr = $task->getAddr($task->getDomId());
                            if ($addr["nokladr"]) {
                                $tpl->setVariable('ROW_TASK_ADDR', "<font color='#FF0000'>Адрес не в формате КЛАДР</font>");
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr["city"] . " " . $addr["street"] . " д." . ($addr["house"] ? $addr["house"] : $addr["althouse"]) . " " . $task->getKv());
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "connections");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "services":
                            $task = new ServiceTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $addr = $task->getAddr($task->getDomId());
                            $tpl->setVariable('ROW_TASK_ADDR', $addr . " " . $task->getKv());
                            $tpl->setVariable("TASK_LINK", link2("services/viewticket?task_id=" . $task->getId()));

                            $ds = adm_statuses_plugin::getStatusByTag("done", "services");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "gp":
                            $task_gp = new GP($task_id);

                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task_gp->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task_gp->getContrNumber());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task_gp->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task_gp->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("gp/viewticket?task_id=" . $task_gp->getId()));

                            $addr = $DB->getField("SELECT la.full_address FROM list_addr la JOIN gp_addr ga ON ga.dom_id = la.id AND ga.task_id = " . $DB->F($task_gp->getId()) . " LIMIT 1;");
                            $addrCount = $DB->getField("SELECT COUNT(*) FROM `gp_addr` WHERE `task_id`=" . $DB->F($task_gp->getId()) . ";");
                            if ($addr) {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr . (($addrCount - 1) > 0 ? " <br /><small><strong>и еще " . ($addrCount - 1) . " шт.</strong></small>" : ""));
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', "Адреса не распознаны. " . ($addrCount ? $addrCount . " шт." : ""));
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "gp");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task_gp->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        case "accidents":
                            $task = new AccidentTicket($task_id);
                            $tpl->setVariable("ITEM_CODE", $ticket_code);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_DOC_TYPE", $dt);
                            $tpl->setVariable("ROW_TASK_ID", $task->getId());
                            $tpl->setVariable("ROW_TASK_CONTR_ID", $task->getClntTNum());
                            $tpl->setVariable("ROW_TASK_TYPE", $types[$task->getPluginUid()]);
                            $tpl->setVariable("ROW_TASK_WTYPE", $task->getTypeName());
                            $tpl->setVariable("TASK_LINK", link2("accidents/viewticket?task_id=" . $task->getId()));

                            $addr = $task->getAddr($task->getDomId());
                            if ($addr["nokladr"]) {
                                $tpl->setVariable('ROW_TASK_ADDR', "<font color='#FF0000'>Адрес не в формате КЛАДР</font>");
                            } else {
                                $tpl->setVariable('ROW_TASK_ADDR', $addr["city"] . " " . $addr["street"] . " д." . ($addr["house"] ? $addr["house"] : $addr["althouse"]) . " " . $task->getKv());
                            }
                            $ds = adm_statuses_plugin::getStatusByTag("done", "accidents");
                            if ($ds["id"]) {
                                $sql = "SELECT MAX(DATE_FORMAT(`datetime`,'%d.%m.%Y')) AS ddate FROM `task_comments` WHERE `status_id`=" . $DB->F($ds["id"]) . " AND `task_id`=" . $DB->F($task->getId()) . ";";
                                $ddate = $DB->getField($sql);
                                if ($ddate) {
                                    $tpl->setVariable("ROW_TASK_DD", $ddate);
                                } else {
                                    $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                                }
                            } else {
                                $tpl->setVariable("ROW_TASK_DD", "&mdash;");
                            }
                            $tpl->setVariable("STATUS_NAME", $task->getStatusName());
                            $tpl->setVariable("STATUS_COLOR", $task->getStatusColor());


                            break;

                        default:
                            UIError("Тип заявки неопределен" . $task_id);
                            break;
                    }

                } else {
                    UIError("Не найдена заявка с ID:" . $task_id);
                }
                $tpl->parse("rep_row");
            }
        }


        $tpl->show();

    }

}
