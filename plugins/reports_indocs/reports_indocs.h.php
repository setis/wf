<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Протоколы приема документов";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Протоколы приема документов";
    $PLUGINS[$plugin_uid]['events']['protocol_list'] = "Список протоколов приема документов";
    $PLUGINS[$plugin_uid]['events']['protocol_view'] = "Протокол приема документов";
    $PLUGINS[$plugin_uid]['events']['addticket_ajax'] = "Добавление заявки в протокол";
    $PLUGINS[$plugin_uid]['events']['clear_protocol'] = "Очистка протокола";
    $PLUGINS[$plugin_uid]['events']['remove_item'] = "Удаление заявки из протокола приема документов";
    $PLUGINS[$plugin_uid]['events']['print_protocol'] = "Печать протокола приема документов";
    
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['create_protocol'] = "Создание протокола приема документов";
    $PLUGINS[$plugin_uid]['events']['remove_protocol'] = "Удаление протокола приема документов";
    $PLUGINS[$plugin_uid]['events']['remove_task'] = "Удаление заявки из протокола приема документов";
    
}
?>