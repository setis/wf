<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;



 
class reports_acts_static_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/reports_acts_static.tmpl.htm");
            else 
                $tpl->loadTemplatefile("reports_contr.tmpl.htm");
            $tpl->setVariable("REPORT_CONTR", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptList")) {
                $cntr = new kontr_plugin();
                
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptList(@$_POST["cnt_id"]));
            } 
            $tpl->setVariable("FILTER_WTYPELIST", adm_acts_wtypes_plugin::getOptions($_REQUEST["filter_wtype_id"]));
            
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            if ($_REQUEST["filter_wtype_id"]) $filter = " AND lawd.wtype_id=".$DB->F($_REQUEST["filter_wtype_id"])." ";
            $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
            $rtpl->setVariable("DATETO", $_POST['dateto']);
            $signed = adm_statuses_plugin::getStatusByTag("signed", "agr_acts");
            $sql = "SELECT lawd.*, 
                        (SELECT `edizm` FROM `list_acts_wtypes` WHERE `id`=lawd.wtype_id) as wtype_edizm, 
                        (SELECT `contr_id` FROM `list_contr_agr` WHERE `id` IN (SELECT `agr_id` FROM `list_agr_act` WHERE `id`=lawd.act_id)) as contr_id 
                    FROM `list_acts_wtype_data` as lawd WHERE lawd.act_id IN 
                        (SELECT `id` FROM `list_agr_act` 
                        WHERE `sign_date`>=".$DB->F(date("Y-m-d", strtotime($_POST["datefrom"])))." AND `sign_date`<=".$DB->F(date("Y-m-d", strtotime($_POST["dateto"])))." AND `status`=".$DB->F($signed["id"]).") $filter 
                    ORDER BY contr_id, wtype_id";
            //die($sql);
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            if ($DB->num_rows()) {
                $i = 1;
                $total_amm = 0;
                while ($r = $DB->fetch(true)) {
                    if ($_POST["cnt_id"]>0) {
                        if ($r["contr_id"] != $_POST["cnt_id"]) continue;
                    }
                    $rtpl->setCurrentBlock("rep_row");
                    $rtpl->setVariable("RR_ID", $i);
                    $cntr = kontr_plugin::getByID($r["contr_id"]);
                    $rtpl->setVariable("RR_CONTR_NAME", $cntr);
                    $rtpl->setVariable("RR_WTYPE", adm_acts_wtypes_plugin::getById($r["wtype_id"]));
                    $rtpl->setVariable("RR_QUANT", $r["wtype_count"]);
                    $rtpl->setVariable("RR_AMMOUNT", number_format($r["wtype_ammount"], 2, ",", " "));                    
                    $total_amm += $r["wtype_ammount"];
                    $rtpl->parse("rep_row");
                    $i+=1;
                }
                if ($i==1) {
                    $rtpl->touchBlock("no-rows");
                } else {
                    $rtpl->setCurrentBlock("tfoot");
                    $rtpl->setVariable("RR_TOTAL",  number_format($total_amm, 2, ",", " "));
                    $rtpl->parse("tfoot");
                }
            } else {
                $rtpl->touchBlock("no-rows");
            }
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_contr_".$_POST['datefrom']."-".$_POST['dateto'].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
         
        $tpl->show();
        
    }
 

}
?>