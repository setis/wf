<?php

/**
 * Plugin Implementation
 *
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use classes\tickets\GlobalProblemTask;
use classes\User;
use models\Task as ModelTask;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use WF\Task\Events\TaskAddressAddedEvent;
use WF\Task\Events\TaskMovedToRmsEvent;

class gp_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function viewlist(Request $request)
    {
        global $DB, $USER;
        $g_user_id = $this->getUserId();
        $LKUser = adm_users_plugin::isLKUser($USER->getId());

        if ($LKUser) {
            $LKContrID = adm_users_plugin::getLKUserContrID($USER->getId());

            $tpl_add = "_lk";
        } else {
            $tpl_add = "";
        }
        $sort_ids = array(
            1 => "Дата изменения",
            2 => "Дата создания",
            3 => "Дата передачи в Горсвязь",
            5 => "Дата до которой отложена",
            6 => "ID заявки",
            7 => "ID заявки контрагента",
            8 => "Тип",
            9 => "Контрагент",
            10 => "Вид работ",
            20 => "Приоритет"/*,
            11 => "СЦ",
            12 => "Район",
            13 => "Статус",
            14 => "Адрес",
            15 => "Код аварии"*/
        );
        $sort_sql = array(
            1 => "ts.lastcmm_date",
            2 => "ts.date_reg",
            3 => "1",
            5 => "is_otlozh_date DESC, ts.date_fn",
            6 => "ts.id",
            7 => "gp.operator_key",
            8 => "gp.type_id",
            9 => "ca.contr_title",
            10 => "typ.title",
            /*11 => "sc.title",
            12 => "area.title",
            13 => "st.name",
            14 => "kstreet.NAME",
			15 => "clntopagr",*/
            20 => "PRIORITY"
        );

        $sort_ti_ids = array(
            1 => "Дата изменения",
            2 => "Дата создания",

        );

        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );

        $rpp = array(
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250"
        );
        $report_status = array("" => "-- выберите значение-- ", "1" => "выполнен", "2" => "не выполнен");
        $status_otlozh = $this->getStatusesByTag('otlozh');
        $status_otlozh = $status_otlozh[0];

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/list$tpl_add.tmpl.htm");
        else
            $tpl->loadTemplatefile("list$tpl_add.tmpl.htm");
        if ($USER->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }
        //if ($LKUser) {
        if (!$USER->isTech($USER->getId()) || $USER->isChiefTech($USER->getId())) {
            $tpl->setCurrentBlock("notistech");
            $tpl->setVariable("PLUGIN1_UID", $this->getUID());
            $tpl->parse("notistech");
        }
        //}
        $filter = "ts.plugin_uid=" . $DB->F($this->getUID());
        $isTech = User::isTech($g_user_id);
        $isChiefTech = User::isChiefTech($g_user_id);
        $slaveServiceCenters = [];
        # EF: Устарело после ввода доступа на СЦ. Пока оставлю здесь для возможности быстро откатиться назад.
        #if ($isChiefTech > 0) {
        #    $slaveServiceCenters = $DB->getCell('select sc_id from link_sc_chiefs where user_id =' . $DB->F($g_user_id));
        #}
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        if (!$LKUser) {

            $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $_REQUEST['filter_sc_id']);
            $tpl->setVariable("FILTER_SC_VAL", $_REQUEST['filter_sc_id']);
            $_REQUEST['filter_reported'] = $this->getCookie('filter_reported', $_REQUEST['filter_reported']);
            $tpl->setVariable("FILTER_REPORT_OPTIONS", array2options($report_status, $_REQUEST['filter_reported']));
            $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);
            if ($_REQUEST['filter_empl_id']) {
                //die("sadf");
                $filter .= " AND tu.user_id=" . $DB->F($_REQUEST['filter_empl_id']) . " AND tu.ispoln";
            }
            /*if($_REQUEST['filter_sc_id'] == "-1")
                $filter .= " AND gp.sc_id=0";
            elseif ($_REQUEST['filter_sc_id']) {
                $filter .= " AND gp.sc_id=".$DB->F($_REQUEST['filter_sc_id']);
            }*/
            $tpl->setCurrentBlock("sclist");
            $tpl->setVariable('FILTER_SC_OPTIONS', $this->getSCOptions($_REQUEST['filter_sc_id']));
            $tpl->parse("sclist");
            $tpl->setCurrentBlock("techlist");
            if ($_REQUEST['filter_empl_id']) {
                $tpl->setVariable("FILTER_EMPL_ID", $_REQUEST['filter_empl_id']);
                $usrName = adm_users_plugin::getUser($_REQUEST["filter_empl_id"]);
                $tpl->setVariable("FILTER_EMPL_NAME", $usrName["fio"]);
            }
            $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
            $tpl->parse("techlist");


            //print_r($_REQUEST);

            $this->getSession()->set("gp_filter_type", $_REQUEST['filter_type'] ? $_REQUEST['filter_type'] : $this->getSession()->get("gp_filter_type"));
            $_REQUEST['filter_type'] = $this->getSession()->get("gp_filter_type");
            $this->getSession()->set("filter_area_id", $_REQUEST['filter_area_id'] ? $_REQUEST['filter_area_id'] : $this->getSession()->get("gp_filter_area_id"));
            $_REQUEST['filter_area_id'] = $this->getSession()->get("gp_filter_area_id");

            $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
            $_REQUEST['filter_cnt_id'] = $this->getCookie('filter_cnt_id', $_REQUEST['filter_cnt_id']);
            $_REQUEST['filter_pin_id'] = $this->getCookie('filter_pin_id', $_REQUEST['filter_pin_id']);
            $_REQUEST['filter_type'] = $this->getCookie('filter_type', $_REQUEST['filter_type']);
            $_REQUEST['filter_clnt_type'] = $this->getCookie('filter_clnt_type', $_REQUEST['filter_clnt_type']);
            $_REQUEST['filter_status_id'] = array_filter(
                $this->getCookie('filter_status_id', $request->request->has('filter_status_id') ? $request->get('filter_status_id') : null),
                function($e) { return !empty($e) && -1 != $e; }
            );
            $_REQUEST['filter_ods_id'] = $this->getCookie('filter_ods_id', $_REQUEST['filter_ods_id']);
            $_REQUEST['filter_area_id'] = $this->getCookie('filter_area_id', $_REQUEST['filter_area_id']);
            $_REQUEST['filter_ctgrnum_id'] = $this->getCookie('filter_ctgrnum_id', $_REQUEST['filter_ctgrnum_id']);
            $_REQUEST['filter_ti_sort_id'] = $this->getCookie('filter_ti_sort_id', $_REQUEST['filter_ti_sort_id']);
            $_REQUEST['filter_ti_date_from'] = $this->getCookie('filter_ti_date_from', $_REQUEST['filter_ti_date_from']);
            $_REQUEST['filter_ti_date_to'] = $this->getCookie('filter_ti_date_to', $_REQUEST['filter_ti_date_to']);
            $_REQUEST['filter_addr'] = $this->getCookie('filter_addr', $_REQUEST['filter_addr']);
            $_REQUEST['filter_date_from'] = $this->getCookie('filter_date_from', $_REQUEST['filter_date_from']);
            $_REQUEST['filter_time_from'] = $this->getCookie('filter_time_from', $_REQUEST['filter_time_from']);
            $_REQUEST['filter_date_to'] = $this->getCookie('filter_date_to', $_REQUEST['filter_date_to']);
            $_REQUEST['filter_time_to'] = $this->getCookie('filter_time_to', $_REQUEST['filter_time_to']);

            $_REQUEST['filter_gssdate_from'] = $this->getCookie('filter_gssdate_from', $_REQUEST['filter_gssdate_from']);
            $_REQUEST['filter_gsstime_from'] = $this->getCookie('filter_gsstime_from', $_REQUEST['filter_gsstime_from']);
            $_REQUEST['filter_gssdate_to'] = $this->getCookie('filter_gssdate_to', $_REQUEST['filter_gssdate_to']);
            $_REQUEST['filter_gsstime_to'] = $this->getCookie('filter_gsstime_to', $_REQUEST['filter_gsstime_to']);
            $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $_REQUEST['filter_sc_id']);
            $_REQUEST['filter_date_from'] = $this->getCookie('filter_date_from', $_REQUEST['filter_date_from']);
            $_REQUEST['filter_time_from'] = $this->getCookie('filter_time_from', $_REQUEST['filter_time_from']);

            $_REQUEST['filter_date_to'] = $this->getCookie('filter_date_to', $_REQUEST['filter_date_to']);
            $_REQUEST['filter_time_to'] = $this->getCookie('filter_time_to', $_REQUEST['filter_time_to']);
            if ($_REQUEST['filter_date_from']) {
                if ($_REQUEST['filter_time_from']) {
                    $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                    $filter .= " AND gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                    if ($_REQUEST['filter_date_to']) {
                        if ($_REQUEST['filter_time_to']) {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter .= " AND gp.operator_start<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);

                        } else {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter .= " AND DATE_FORMAT(gp.operator_start, '%Y-%m-%d')<=" . $DB->F($dateTo);
                        }
                    }
                } else {

                    $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                    $filter .= " AND DATE_FORMAT(gp.operator_start, '%Y-%m-%d') >=" . $DB->F($dateFrom);

                    if ($_REQUEST['filter_date_to']) {
                        if ($_REQUEST['filter_time_to']) {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter .= " AND gp.operator_start<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);

                        } else {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter .= " AND DATE_FORMAT(gp.operator_start, '%Y-%m-%d')<=" . $DB->F($dateTo);
                        }
                    }
                }
            }
            if ($_REQUEST['filter_date_to']) {
                if ($_REQUEST['filter_time_to']) {
                    $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                    $filter .= " AND gp.operator_start<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);
                    if ($_REQUEST['filter_date_from']) {
                        if ($_REQUEST['filter_time_from']) {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter .= " AND gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                        } else {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter .= " AND DATE_FORMAT(gp.operator_start, '%Y-%m-%d') >=" . $DB->F($dateFrom);
                        }
                    }
                } else {
                    $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                    $filter .= " AND DATE_FORMAT(gp.operator_start, '%Y-%m-%d')<=" . $DB->F($dateTo);
                    if ($_REQUEST['filter_date_from']) {
                        if ($_REQUEST['filter_time_from']) {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter .= " AND gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                        } else {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter .= " AND DATE_FORMAT(gp.operator_start, '%Y-%m-%d') >=" . $DB->F($dateFrom);
                        }
                    }
                }
            }

            if ($_REQUEST['filter_gssdate_from']) {
                if ($_REQUEST['filter_gsstime_from']) {
                    $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                    $filter .= " AND gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_gsstime_from"]);
                    if ($_REQUEST['filter_gssdate_to']) {
                        if ($_REQUEST['filter_gsstime_to']) {
                            $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                            $filter .= " AND gp.operator_start<=" . $DB->F($dateTo . " " . $_REQUEST["filter_gsstime_to"]);

                        } else {
                            $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                            $filter .= " AND DATE_FORMAT(gp.operator_start, '%Y-%m-%d')<=" . $DB->F($dateTo);
                        }
                    }
                } else {

                    $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                    $filter .= " AND DATE_FORMAT(gp.operator_start, '%Y-%m-%d') >=" . $DB->F($dateFrom);

                    if ($_REQUEST['filter_gssdate_to']) {
                        if ($_REQUEST['filter_gsstime_to']) {
                            $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                            $filter .= " AND gp.operator_start<=" . $DB->F($dateTo . " " . $_REQUEST["filter_gsstime_to"]);

                        } else {
                            $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                            $filter .= " AND  gp.operator_start <=" . $DB->F($dateTo);
                        }
                    }
                }
            }
            if ($_REQUEST['filter_gssdate_to']) {
                if ($_REQUEST['filter_gsstime_to']) {
                    $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                    $filter .= " AND gp.operator_start <=" . $DB->F($dateTo . " " . $_REQUEST["filter_gsstime_to"]);
                    if ($_REQUEST['filter_gssdate_from']) {
                        if ($_REQUEST['filter_gsstime_from']) {
                            $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                            $filter .= " AND gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                        } else {
                            $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                            $filter .= " AND gp.operator_start >=" . $DB->F($dateFrom);
                        }
                    }
                } else {
                    $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                    $filter .= " AND DATE_FORMAT(gp.operator_start, '%Y-%m-%d')<=" . $DB->F($dateTo);
                    if ($_REQUEST['filter_gssdate_from']) {
                        if ($_REQUEST['filter_gsstime_from']) {
                            $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                            $filter .= " AND gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_gsstime_from"]);
                        } else {
                            $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                            $filter .= " AND gp.operator_start >=" . $DB->F($dateFrom);
                        }
                    }
                }
            }

            if ($request->get('filter_sc_id', -1) > 0) {
                $filter .= " AND gp.sc_id=" . $request->request->getInt('filter_sc_id');
            } elseif (0 == $request->get('filter_sc_id', -1)) {
                $filter .= " AND (gp.sc_id IS NULL OR gp.sc_id = 0)";
            }

            if ($_REQUEST['filter_id']) $filter .= " AND ts.id=" . $DB->F($_REQUEST['filter_id']);
            if ($_REQUEST['filter_cnt_id']) $filter .= " AND gp.cnt_id=" . $DB->F($_REQUEST['filter_cnt_id']);
            if (count($_REQUEST['filter_type']) > 0) {
                $type = $_REQUEST['filter_type'][0];
                if ("-1" === $type) {
                    $filter .= " AND gp.type_id IS NULL";
                } elseif (!empty ($type)) {
                    $filter .= " AND gp.type_id = $type";
                }
            }

            if ($_REQUEST['filter_pin_id']) $filter .= " AND lower(gp.operator_key) LIKE " . $DB->F("%" . strtolower($_REQUEST['filter_pin_id']) . "%");
            if ($_REQUEST['filter_addr']) {
                $filter .= " AND ts.id IN (SELECT task_addr.task_id FROM gp_addr task_addr JOIN list_addr la ON la.id = task_addr.dom_id AND la.full_address LIKE " . $DB->F('%' . $_REQUEST["filter_addr"] . '%') . ")";
            }
            if ($_REQUEST['filter_reported'] == "1") {
                $filter .= " AND ts.id IN (SELECT `task_id` FROM `gp_calcs`) ";
            }

            if ($_REQUEST['filter_reported'] == "2") {
                $filter .= " AND ts.id NOT IN (SELECT `task_id` FROM `gp_calcs`) ";

            }

            $sql_add = "";
            if ($_REQUEST['filter_ti_date_from'] || $_REQUEST['filter_ti_date_to']) {
                switch ($_REQUEST['filter_ti_sort_id']) {
                    case "1":
                        if ($_REQUEST['filter_ti_date_from'])
                            $filter .= " AND ts.lastcmm_date>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                        if ($_REQUEST['filter_ti_date_to'])
                            $filter .= " AND ts.lastcmm_date<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));
                        break;

                    case "2":
                        if ($_REQUEST['filter_ti_date_from'])
                            $filter .= " AND ts.date_reg>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                        if ($_REQUEST['filter_ti_date_to'])
                            $filter .= " AND ts.date_reg<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));

                        break;


                    case "5":
                        $doneStatus = adm_statuses_plugin::getStatusByTag("donegp", "gp");
                        if ($_REQUEST['filter_ti_date_from']) {
                            $dateFrom = substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2);
                            $dateTo = substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2);
                            if ($_REQUEST['filter_ti_date_to']) {
                                $filter .= " AND ts.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id=" . $DB->F($doneStatus["id"]) . " AND
                                            ( tc.datetime >=" . $DB->F($dateFrom) . " AND tc.datetime<=" . $DB->F($dateTo) . ")) ";
                            } else {
                                $filter .= " AND ts.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id=" . $DB->F($doneStatus["id"]) . " AND
                                             tc.datetimeт >=" . $DB->F($dateFrom) . ") ";
                            }
                        }
                        break;
                }
            }

        } else {
            $filter .= " AND gp.cnt_id=" . $DB->F($LKContrID);
        }
        if (!isset($_REQUEST["filter_rpp"])) $_REQUEST["filter_rpp"] = $USER->getRpp();

        if ($_REQUEST["filter_rpp"] != $USER->getRpp()) {
            $sql = "UPDATE `users` SET `rpp`=" . $_REQUEST["filter_rpp"] . " WHERE `id`=" . $DB->F($USER->getId()) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
        } else {
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        }
        if ($LKUser) {
            $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ti_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ti_ids));
        } else
            $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ? $_REQUEST['filter_sort_order'] : key($sort_order));

        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];

        if (!empty($_REQUEST['filter_status_id'])) {
            $filter .= " AND ts.status_id in(" . implode(',', array_map($DB->F, $_REQUEST['filter_status_id'])) . ')';
        }
        if (in_array($status_otlozh['id'], $_REQUEST['filter_status_id'])) {
            $_REQUEST['filter_sort_order'] = 'ASC';
        }
        if (!$LKUser) {
            $tpl->setVariable("FILTER_GSSDATE_FROM", $_REQUEST['filter_gssdate_from'] ? $_REQUEST['filter_gssdate_from'] : "");
            $tpl->setVariable("FILTER_GSSTIME_FROM", $_REQUEST['filter_gsstime_from'] ? $_REQUEST['filter_gsstime_from'] : "");
            $tpl->setVariable("FILTER_GSSDATE_TO", $_REQUEST['filter_gssdate_to'] ? $_REQUEST['filter_gssdate_to'] : "");
            $tpl->setVariable("FILTER_GSSTIME_TO", $_REQUEST['filter_gsstime_to'] ? $_REQUEST['filter_gsstime_to'] : "");

            $tpl->setVariable('FILTER_ID', htmlspecialchars($_REQUEST['filter_id']));

            $tpl->setVariable("PIN_ID", $_REQUEST['filter_pin_id'] ? $_REQUEST['filter_pin_id'] : "");
            $tpl->setVariable("PIN_ID", $_REQUEST['filter_pin_id'] ? $_REQUEST['filter_pin_id'] : "");
            $tpl->setVariable("ADDRTEXT", $_REQUEST['filter_addr']);
            $tpl->setVariable('FILTER_CNT_OPTIONS', $this->getContrOptions($_REQUEST['filter_cnt_id']));
            $tpl->setVariable('FILTER_TYPE_OPTIONS', $this->getTypeOptions($_REQUEST['filter_type']));
            $tpl->setVariable('FILTER_SC_OPTIONS', $this->getSCOptions($_REQUEST['filter_sc_id']));
            $tpl->setVariable('FILTER_CLNT_TYPE_SEL_' . $_REQUEST['filter_clnt_type'], 'selected');
            $tpl->setVariable('FILTER_ODS_OPTIONS', $this->getODSOptions($_REQUEST['filter_ods_id']));
            $tpl->setVariable("FILTER_DATE_FROM", $_REQUEST['filter_date_from'] ? $_REQUEST['filter_date_from'] : "");
            $tpl->setVariable("FILTER_TIME_FROM", $_REQUEST['filter_time_from'] ? $_REQUEST['filter_time_from'] : "");
            $tpl->setVariable("FILTER_DATE_TO", $_REQUEST['filter_date_to'] ? $_REQUEST['filter_date_to'] : "");
            $tpl->setVariable("FILTER_TIME_TO", $_REQUEST['filter_time_to'] ? $_REQUEST['filter_time_to'] : "");
            $statusList = adm_statuses_plugin::getFullList("gp");
            if ($statusList) {
                $tpl->setCurrentBlock("sl-items");
                foreach ($statusList as $st_item) {
                    $tpl->setCurrentBlock("statuslist");
                    if (in_array($st_item["id"], $_REQUEST['filter_status_id'])) {
                        $tpl->setVariable("FF_SELECTED", 'ffstisel');
                    }
                    $tpl->setVariable("FF_STATUS_ID", $st_item["id"]);
                    $tpl->setVariable("FF_STATUS", $st_item["name"]);
                    $tpl->setVariable("FF_STATUS_COLOR", $st_item["color"]);
                    $tpl->parse("statuslist");
                }
                if (in_array('-1', $_REQUEST['filter_status_id'])) {
                    $tpl->setVariable("FF_ALL_SELECTED", "ffstisel");
                }
                $tpl->parse("sl-items");

            }

            //type_id
            $tpl->setVariable("FILTER_TI_DATE_FROM", $_REQUEST['filter_ti_date_from'] ? $_REQUEST['filter_ti_date_from'] : "");
            $tpl->setVariable("FILTER_TI_DATE_TO", $_REQUEST['filter_ti_date_to'] ? $_REQUEST['filter_ti_date_to'] : "");
            $tpl->setVariable("ENABLEALLAREAS", "firstItemChecksAll: true,");

            $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));

        }
        $status = wf::$em->getRepository(\models\TaskStatus::class)->findBy(['pluginUid' => \models\Task::GLOBAL_PROBLEM]);
        $statuses = array_map(function (\models\TaskStatus $item) {
            return [
                'id' => $item->getId(),
                'text' => $item->getName()
            ];
        }, $status);
        $statusStyles = implode("\n", array_map(function (\models\TaskStatus $item) {
            return ".status_{$item->getId()}{
                background:{$item->getColor()};
            }";
        }, $status));
        $tpl->setVariable('STATUS_STYLE', $statusStyles);
        $tpl->setVariable('FILTER_STATUS_OPTIONS', json_encode($statuses));
        $tpl->setVariable('FILTER_STATUS_OPTIONS_SET', (empty($_REQUEST['filter_status_id'])) ? '""' : json_encode($_REQUEST['filter_status_id']));
        $tpl->setVariable('FILTER_ROWS_PER_PAGE', array2options($rpp, $_REQUEST['filter_rpp']));
        $tpl->setVariable('FILTER_TI_SORT_OPTIONS', array2options($sort_ti_ids, $_REQUEST['filter_ti_sort_id']));
        $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
        if (!$LKUser) {
            $areaData1 = [];
            if (($isChiefTech || $isTech) && !$USER->checkAccess("scgpfullaccess")) {
                $sc_id = $isChiefTech ? $isChiefTech : User::getScIdByUserId($g_user_id);
                $areaData["no"] = "-- не указан";
                $tpl->setVariable('FILTER_AREA_OPTIONS', array2options(($areaData + $DB->getCell2("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `title`;")), $_REQUEST['filter_area_id'], false, in_array("0", (array)$_REQUEST['filter_area_id'], true)));
            } else {
                if ($_REQUEST['filter_sc_id']) {
                    $areaData1 = $DB->getCell2("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT area_id FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($_REQUEST["filter_sc_id"]) . ") ORDER BY `title`;");
                } else {
                    $areaData1 = $DB->getCell2("SELECT `id`,`title` FROM `list_area` ORDER BY `title`;");
                }
                $areaData["no"] = "-- не указан";
                $tpl->setVariable('FILTER_AREA_OPTIONS', array2options(($areaData + $areaData1), $_REQUEST['filter_area_id'], false, in_array("0", (array)$_REQUEST['filter_area_id'], true)));
            }
            $finalAreas = $areaData + $areaData1;
            if (isset($_REQUEST['filter_area_id'])) {
                if (is_array($_REQUEST['filter_area_id'])) {
                    if (count($_REQUEST['filter_area_id']) != count(($finalAreas))) {
                        $arr = str_replace("no", "0", implode(",", $_REQUEST['filter_area_id']));
                        $filter .= " AND addr.area_id IN (" . $arr . ")";
                    } else {
                        $filter .= "";
                    }
                }
            }
            $tpl->setVariable('CTGRNUM_ID', $_REQUEST['filter_ctgrnum_id']);
        } else {
            $wtype_list = adm_users_plugin::getLKWtypeIDList($USER->getId());
            if ($wtype_list) {
                $filter .= " AND (gp.type_id IN (" . implode(",", $wtype_list) . ")) ";
            }
        }
        if ($USER->checkAccess("deletegp", User::ACCESS_WRITE)) {
            $tpl->touchBlock("delticket-head");
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
                    DATE_FORMAT(gp.operator_start, '%d.%m.%Y %H:%i') AS STARTDATE,
                    DATE_FORMAT(gp.operator_2gss, '%d.%m.%Y %H:%i') AS GSSDATE,
                    ca.contr_title AS CONTR,
                    typ.title AS TYPE,
                    gp.*,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    user.fio, st.tag, gp.operator_key AS CONTR_ID,
					ts.date_fn,
					(ts.date_fn IS NOT NULL AND ts.date_fn <= NOW()) AS is_otlozh_date,
                    (gp.op_b2b + gp.op_b2c) AS PRIORITY
                FROM
                    `tasks` AS ts
                    LEFT JOIN `gp` AS gp ON ts.id=gp.task_id
                    LEFT JOIN `list_contr` AS ca ON gp.cnt_id=ca.id
                    LEFT JOIN `task_types` AS typ ON gp.type_id=typ.id
                    LEFT JOIN `task_status` AS st ON ts.status_id=st.id
                    LEFT JOIN `task_users` AS tu ON gp.task_id=tu.task_id
                    LEFT JOIN `users` AS user ON user.id=tu.user_id
                     $sql_add
                WHERE $filter
                  and case when gp.sc_id is null then 1
                           when exists (select * from user_sc_access usa where usa.user_id = " . $USER->getId() . " and usa.sc_id = gp.sc_id) then 1
                           else 0
                       end = 1
                GROUP BY ts.id
                ORDER BY
                    $order
                LIMIT " . intval($_REQUEST['filter_start']) . ", " . intval($_REQUEST["filter_rpp"]);

        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        $total = $DB->getFoundRows();
        while ($a = $DB->fetch(true)) {
            $resp = $DB->getCell("SELECT user.fio FROM `task_users` AS tu LEFT JOIN `users` AS user ON user.id=tu.user_id WHERE tu.task_id=" . $DB->F($a['TASK_ID']) . " AND tu.ispoln ;");
            if (!$resp) {
                $resp_user = "<i>" . $a['fio'] . "</i>";
            } else {
                $r = false;
                foreach ($resp as $item) {
                    $r[] = "$item";
                }
                $resp_user = implode(", <br/ >", $r);
            }
            $a["HREF_ORDER"] = $this->getLink('vieworderdoc', "id={$a['TASK_ID']}");
            $a['HREF'] = $this->getLink('viewticket', "task_id={$a['TASK_ID']}");
            $t = new GlobalProblemTask($a["TASK_ID"]);
            $currentStatus = $t->getStatusId();
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "gp");
            $countStatus = adm_statuses_plugin::getStatusByTag("report", "gp");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "gp");
            $targetStatus = false;
            //if ($currentStatus == $doneStatus["id"] || $currentStatus == $countStatus["id"] || $currentStatus == $completeStatus["id"]) $targetStatus = true;
            $addrs = $DB->getRow("SELECT la.full_address, ga.addr FROM list_addr la JOIN gp_addr ga ON ga.dom_id = la.id AND ga.task_id = {$DB->F($a['TASK_ID'])} ORDER BY la.full_address DESC, ga.addr DESC LIMIT 1");
            $addr = $addrs[0] ?: $addrs[1];
            $addrCount = $DB->getField("SELECT COUNT(*) FROM gp_addr WHERE task_id = {$DB->F($a['TASK_ID'])}");
            if ($addr) {
                $a["ADDRLIST"] = $addr . (($addrCount - 1) > 0 ? " <br /><small><strong>и еще " . ($addrCount - 1) . " шт.</strong></small>" : "");
            } else {
                $a["ADDRLIST"] = "Адреса не распознаны. " . ($addrCount ? $addrCount . " шт." : "");
            }
            $a["LASTCOMMENTVALUE"] = strip_tags($t->getLastComment(true));
            if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getSC")) {
                $scO = new adm_sc_plugin();
                $sc = $scO->getSC($t->getSCId());
            }
            $a['SC'] = $sc['title'] ? $sc['title'] : "<center>&mdash;</center>";
            $a['CLOSE_TIME'] = $a["close_time"] ? date("d.m.Y H:i", strtotime($a["close_time"])) : "&mdash;";
            $a['USERFIO'] = Task::getTaskIspoln2($a['TASK_ID']);
            $a['CONTR_ID'] = $a['CONTR_ID'] ? $a['CONTR_ID'] : "<center>&mdash;</center>";
            $a['FIZJUR'] = ($a['clnt_type'] == 1 ? "Физ" : "Юр");
            $a['PINVAL'] = $a['clntopagr'] ? $a['clntopagr'] : "<center>&mdash;</center>";
            $a['OP_B2B'] = $a["op_b2b"];
            $a['OP_B2C'] = $a["op_b2c"];
            $sc_name = adm_sc_plugin::getSC($a["sc_id"]);
            $a['SC_NAME'] = $sc_name["title"] ? $sc_name["title"] : "&mdash;";
            $tpl->setCurrentBlock('row');
            $tpl->setVariable($a);
            if ($USER->checkAccess("deletegp", User::ACCESS_WRITE) && !$readonly) {
                $tpl->setCurrentBlock("deleteticket");
                $tpl->setVariable("DELETEHREF", $this->getLink('deleteticket', "task_id={$a['TASK_ID']}"));
                $tpl->parse("deleteticket");
            }
            $tpl->parse('row');
        }
        /*$mem->set(md5($sql),$cache, true, 60);
        $DB->free();

    }*/
        /*$total = $mem->get(md5($sql." count"));
        if (!$total[0]) {*/
        //$DB->query($sql);

        //$in[] = $total;
        //$mem->set(md5($sql." count"), $in, true, 60);
        //}
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"], $total, "#start-%s"));
        //$mem->close();
        UIHeader($tpl);
        $tpl->show();
    }

    function viewlist_export(Request $request)
    {
        header("Content-type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=gp_export_" . date("H_i_d-M-Y") . ".xls");

        global $DB, $USER;
        $g_user_id = $this->getUserId();
        $LKUser = adm_users_plugin::isLKUser($USER->getId());

        if ($LKUser) {
            $LKContrID = adm_users_plugin::getLKUserContrID($USER->getId());

            $tpl_add = "_lk";
        } else {
            $tpl_add = "";
        }
        $sort_ids = array(
            1 => "Дата изменения",
            2 => "Дата создания",
            3 => "Дата передачи в Горсвязь",
            5 => "Дата до которой отложена",
            6 => "ID заявки",
            7 => "ID заявки контрагента",
            8 => "Тип",
            9 => "Контрагент",
            10 => "Вид работ",
            20 => "Приоритет"/*,
            11 => "СЦ",
            12 => "Район",
            13 => "Статус",
            14 => "Адрес",
            15 => "Код аварии"*/
        );
        $sort_sql = array(
            1 => "ts.lastcmm_date",
            2 => "ts.date_reg",
            3 => "1",
            5 => "is_otlozh_date DESC, ts.date_fn",
            6 => "ts.id",
            7 => "gp.operator_key",
            8 => "gp.type_id",
            9 => "ca.contr_title",
            10 => "typ.title",
            /*11 => "sc.title",
            12 => "area.title",
            13 => "st.name",
            14 => "kstreet.NAME",
			15 => "clntopagr",*/
            20 => "PRIORITY"
        );

        $sort_ti_ids = array(
            1 => "Дата изменения",
            2 => "Дата создания",

        );

        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );

        $rpp = array(
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250"
        );
        $status_otlozh = $this->getStatusesByTag('otlozh');
        $status_otlozh = $status_otlozh[0];
        $report_status = array("" => "-- выберите значение-- ", "1" => "выполнен", "2" => "не выполнен");

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/export.tmpl.htm");


        $_REQUEST['filter_start'] = 0;
        if (!$LKUser) {
            $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $_REQUEST['filter_sc_id']);

            $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);
            if ($_REQUEST['filter_empl_id']) {
                //die("sadf");
                $filter[] = "  tu.user_id=" . $DB->F($_REQUEST['filter_empl_id']) . " AND tu.ispoln";
            }
            /*if($_REQUEST['filter_sc_id'] == "-1")
                $filter[] = " AND gp.sc_id=0";
            elseif ($_REQUEST['filter_sc_id']) {
                $filter[] = " AND gp.sc_id=".$DB->F($_REQUEST['filter_sc_id']);
            }*/

            $_REQUEST['filter_reported'] = $this->getCookie('filter_reported', $_REQUEST['filter_reported']);


            //print_r($_REQUEST);

            $this->getSession()->set("gp_filter_type", $_REQUEST['filter_type'] ? $_REQUEST['filter_type'] : $this->getSession()->get("gp_filter_type"));
            $_REQUEST['filter_type'] = $this->getSession()->get("gp_filter_type");
            $this->getSession()->set("filter_area_id", $_REQUEST['filter_area_id'] ? $_REQUEST['filter_area_id'] : $this->getSession()->get("gp_filter_area_id"));
            $_REQUEST['filter_area_id'] = $this->getSession()->get("gp_filter_area_id");

            $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
            $_REQUEST['filter_cnt_id'] = $this->getCookie('filter_cnt_id', $_REQUEST['filter_cnt_id']);
            $_REQUEST['filter_pin_id'] = $this->getCookie('filter_pin_id', $_REQUEST['filter_pin_id']);
            $_REQUEST['filter_type'] = $this->getCookie('filter_type', $_REQUEST['filter_type']);
            $_REQUEST['filter_clnt_type'] = $this->getCookie('filter_clnt_type', $_REQUEST['filter_clnt_type']);
            $_REQUEST['filter_status_id'] = array_filter(
                $this->getCookie('filter_status_id', $request->request->has('filter_status_id') ? $request->get('filter_status_id') : null),
                function($e) { return !empty($e) && -1 != $e; }
            );
            $_REQUEST['filter_ods_id'] = $this->getCookie('filter_ods_id', $_REQUEST['filter_ods_id']);
            $_REQUEST['filter_area_id'] = $this->getCookie('filter_area_id', $_REQUEST['filter_area_id']);
            $_REQUEST['filter_ctgrnum_id'] = $this->getCookie('filter_ctgrnum_id', $_REQUEST['filter_ctgrnum_id']);
            $_REQUEST['filter_ti_sort_id'] = $this->getCookie('filter_ti_sort_id', $_REQUEST['filter_ti_sort_id']);
            $_REQUEST['filter_ti_date_from'] = $this->getCookie('filter_ti_date_from', $_REQUEST['filter_ti_date_from']);
            $_REQUEST['filter_ti_date_to'] = $this->getCookie('filter_ti_date_to', $_REQUEST['filter_ti_date_to']);
            $_REQUEST['filter_addr'] = $this->getCookie('filter_addr', $_REQUEST['filter_addr']);
            $_REQUEST['filter_date_from'] = $this->getCookie('filter_date_from', $_REQUEST['filter_date_from']);
            $_REQUEST['filter_time_from'] = $this->getCookie('filter_time_from', $_REQUEST['filter_time_from']);
            $_REQUEST['filter_date_to'] = $this->getCookie('filter_date_to', $_REQUEST['filter_date_to']);
            $_REQUEST['filter_time_to'] = $this->getCookie('filter_time_to', $_REQUEST['filter_time_to']);

            $_REQUEST['filter_gssdate_from'] = $this->getCookie('filter_gssdate_from', $_REQUEST['filter_gssdate_from']);
            $_REQUEST['filter_gsstime_from'] = $this->getCookie('filter_gsstime_from', $_REQUEST['filter_gsstime_from']);
            $_REQUEST['filter_gssdate_to'] = $this->getCookie('filter_gssdate_to', $_REQUEST['filter_gssdate_to']);
            $_REQUEST['filter_gsstime_to'] = $this->getCookie('filter_gsstime_to', $_REQUEST['filter_gsstime_to']);
            $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $_REQUEST['filter_sc_id']);
            $_REQUEST['filter_date_from'] = $this->getCookie('filter_date_from', $_REQUEST['filter_date_from']);
            $_REQUEST['filter_time_from'] = $this->getCookie('filter_time_from', $_REQUEST['filter_time_from']);

            $_REQUEST['filter_date_to'] = $this->getCookie('filter_date_to', $_REQUEST['filter_date_to']);
            $_REQUEST['filter_time_to'] = $this->getCookie('filter_time_to', $_REQUEST['filter_time_to']);
            if ($_REQUEST['filter_date_from']) {
                if ($_REQUEST['filter_time_from']) {
                    $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                    $filter[] = "  gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                    if ($_REQUEST['filter_date_to']) {
                        if ($_REQUEST['filter_time_to']) {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter[] = "  gp.operator_start<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);

                        } else {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter[] = "  DATE_FORMAT(gp.operator_start, '%Y-%m-%d')<=" . $DB->F($dateTo);
                        }
                    }
                } else {

                    $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                    $filter[] = "  DATE_FORMAT(gp.operator_start, '%Y-%m-%d') >=" . $DB->F($dateFrom);

                    if ($_REQUEST['filter_date_to']) {
                        if ($_REQUEST['filter_time_to']) {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter[] = "  gp.operator_start<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);

                        } else {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter[] = "  DATE_FORMAT(gp.operator_start, '%Y-%m-%d')<=" . $DB->F($dateTo);
                        }
                    }
                }
            }
            if ($_REQUEST['filter_date_to']) {
                if ($_REQUEST['filter_time_to']) {
                    $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                    $filter[] = "  gp.operator_start<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);
                    if ($_REQUEST['filter_date_from']) {
                        if ($_REQUEST['filter_time_from']) {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter[] = "  gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                        } else {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter[] = "  gp.operator_start >=" . $DB->F($dateFrom);
                        }
                    }
                } else {
                    $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                    $filter[] = "  gp.operator_start<=" . $DB->F($dateTo);
                    if ($_REQUEST['filter_date_from']) {
                        if ($_REQUEST['filter_time_from']) {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter[] = "  gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                        } else {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter[] = "  gp.operator_start >=" . $DB->F($dateFrom);
                        }
                    }
                }
            }

            if ($_REQUEST['filter_gssdate_from']) {
                if ($_REQUEST['filter_gsstime_from']) {
                    $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                    $filter[] = "  gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_gsstime_from"]);
                    if ($_REQUEST['filter_gssdate_to']) {
                        if ($_REQUEST['filter_gsstime_to']) {
                            $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                            $filter[] = "  gp.operator_start<=" . $DB->F($dateTo . " " . $_REQUEST["filter_gsstime_to"]);

                        } else {
                            $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                            $filter[] = "  DATE_FORMAT(gp.operator_start, '%Y-%m-%d')<=" . $DB->F($dateTo);
                        }
                    }
                } else {

                    $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                    $filter[] = "  gp.operator_start >=" . $DB->F($dateFrom);

                    if ($_REQUEST['filter_gssdate_to']) {
                        if ($_REQUEST['filter_gsstime_to']) {
                            $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                            $filter[] = "  gp.operator_start<=" . $DB->F($dateTo . " " . $_REQUEST["filter_gsstime_to"]);

                        } else {
                            $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                            $filter[] = "  gp.operator_start <=" . $DB->F($dateTo);
                        }
                    }
                }
            }
            if ($_REQUEST['filter_gssdate_to']) {
                if ($_REQUEST['filter_gsstime_to']) {
                    $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                    $filter[] = "  gp.operator_start<=" . $DB->F($dateTo . " " . $_REQUEST["filter_gsstime_to"]);
                    if ($_REQUEST['filter_gssdate_from']) {
                        if ($_REQUEST['filter_gsstime_from']) {
                            $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                            $filter[] = "  gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                        } else {
                            $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                            $filter[] = "  gp.operator_start >=" . $DB->F($dateFrom);
                        }
                    }
                } else {
                    $dateTo = substr($_REQUEST['filter_gssdate_to'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_to'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_to'], 0, 2);
                    $filter[] = "  DATE_FORMAT(gp.operator_start, '%Y-%m-%d')<=" . $DB->F($dateTo);
                    if ($_REQUEST['filter_gssdate_from']) {
                        if ($_REQUEST['filter_gsstime_from']) {
                            $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                            $filter[] = "  gp.operator_start>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_gsstime_from"]);
                        } else {
                            $dateFrom = substr($_REQUEST['filter_gssdate_from'], 6, 4) . "-" . substr($_REQUEST['filter_gssdate_from'], 3, 2) . "-" . substr($_REQUEST['filter_gssdate_from'], 0, 2);
                            $filter[] = "  gp.operator_start >=" . $DB->F($dateFrom);
                        }
                    }
                }
            }

            if ($_REQUEST['filter_sc_id']) $filter[] = "  gp.sc_id=" . $DB->F($_REQUEST['filter_sc_id']);
            if ($_REQUEST['filter_id']) $filter[] = "  ts.id=" . $DB->F($_REQUEST['filter_id']);
            if ($_REQUEST['filter_cnt_id']) $filter[] = "  gp.cnt_id=" . $DB->F($_REQUEST['filter_cnt_id']);
            if (count($_REQUEST['filter_type']) > 0) {
                $type = $_REQUEST['filter_type'][0];
                if ("-1" === $type) {
                    $filter .= " AND gp.type_id IS NULL";
                } elseif (!empty ($type)) {
                    $filter .= " AND gp.type_id = $type";
                }
            }
            if ($_REQUEST['filter_pin_id']) $filter[] = "  lower(gp.operator_key) LIKE " . $DB->F("%" . strtolower($_REQUEST['filter_pin_id']) . "%");
            if ($_REQUEST['filter_addr']) $filter[] = " ts.id IN (SELECT task_addr.task_id FROM gp_addr task_addr JOIN list_addr la ON la.id = task_addr.dom_id AND la.full_address LIKE " . $DB->F("%" . strtolower($_REQUEST["filter_addr"]) . "%") . ")";

            if ($_REQUEST['filter_reported'] == "1") {
                $filter[] = " ts.id IN (SELECT `task_id` FROM `gp_calcs`) ";
            }

            if ($_REQUEST['filter_reported'] == "2") {
                $filter[] = " ts.id NOT IN (SELECT `task_id` FROM `gp_calcs`) ";

            }

            $sql_add = "";
            if ($_REQUEST['filter_ti_date_from'] || $_REQUEST['filter_ti_date_to']) {
                switch ($_REQUEST['filter_ti_sort_id']) {
                    case "1":
                        if ($_REQUEST['filter_ti_date_from'])
                            $filter[] = "  ts.lastcmm_date>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                        if ($_REQUEST['filter_ti_date_to'])
                            $filter[] = "  ts.lastcmm_date<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));
                        break;

                    case "2":
                        if ($_REQUEST['filter_ti_date_from'])
                            $filter[] = "  ts.date_reg>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                        if ($_REQUEST['filter_ti_date_to'])
                            $filter[] = "  ts.date_reg<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));

                        break;


                    case "5":
                        $doneStatus = adm_statuses_plugin::getStatusByTag("donegp", "gp");
                        if ($_REQUEST['filter_ti_date_from']) {
                            $dateFrom = substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2);
                            $dateTo = substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2);
                            if ($_REQUEST['filter_ti_date_to']) {
                                $filter[] = "  ts.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id=" . $DB->F($doneStatus["id"]) . " AND
                                            (tc.datetime>=" . $DB->F($dateFrom) . " AND tc.datetime<=" . $DB->F($dateTo) . ")) ";
                            } else {
                                $filter[] = "  ts.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id=" . $DB->F($doneStatus["id"]) . " AND
                                            (tc.datetime>=" . $DB->F($dateFrom) . ")) ";
                            }
                        }
                        break;
                }
            }

        } else {
            $filter[] = "  gp.cnt_id=" . $DB->F($LKContrID);
        }


        if ($LKUser) {
            $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ti_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ti_ids));
        } else
            $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ids));

        //$_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ?  $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ? $_REQUEST['filter_sort_order'] : key($sort_order));

        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];

        if (!empty($_REQUEST['filter_status_id'])) {
            $filter .= " AND ts.status_id in(" . implode(',', array_map($DB->F, $_REQUEST['filter_status_id'])) . ')';
        }
        if (in_array($status_otlozh['id'], $_REQUEST['filter_status_id'])) {
            $_REQUEST['filter_sort_order'] = 'ASC';
        }

        if (!$LKUser) {
            $areaData1 = [];
            if (($isChiefTech || $isTech) && !$USER->checkAccess("scgpfullaccess")) {
                $sc_id = $isChiefTech ? $isChiefTech : User::getScIdByUserId($g_user_id);
                $areaData["no"] = "-- не указан";
            } else {
                if ($_REQUEST['filter_sc_id']) {
                    $areaData1 = $DB->getCell2("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT area_id FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($_REQUEST["filter_sc_id"]) . ") ORDER BY `title`;");
                } else {
                    $areaData1 = $DB->getCell2("SELECT `id`,`title` FROM `list_area` ORDER BY `title`;");
                }
                $areaData["no"] = "-- не указан";
            }
            $finalAreas = $areaData + $areaData1;
            if (isset($_REQUEST['filter_area_id'])) {
                if (is_array($_REQUEST['filter_area_id'])) {
                    if (count($_REQUEST['filter_area_id']) != count(($finalAreas))) {
                        $arr = str_replace("no", "0", implode(",", $_REQUEST['filter_area_id']));
                        $filter[] = " addr.area_id IN (" . $arr . ")";
                    } else {
                        $filter[] = "";
                    }
                }
            }
        }

        if (!isset($_REQUEST["filter_rpp"])) $_REQUEST["filter_rpp"] = $USER->getRpp();

        if ($_REQUEST["filter_rpp"] != $USER->getRpp()) {
            $sql = "UPDATE `users` SET `rpp`=" . $_REQUEST["filter_rpp"] . " WHERE `id`=" . $DB->F($USER->getId()) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
        } else {
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        }

        $filter[] = " ts.plugin_uid='gp' ";
        $filter = implode(" AND ", $filter);
        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
                    DATE_FORMAT(gp.operator_start, '%d.%m.%Y %H:%i') AS STARTDATE,
                    DATE_FORMAT(gp.operator_2gss, '%d.%m.%Y %H:%i') AS GSSDATE,
                    ca.contr_title AS CONTR,
                    typ.title AS TYPE,
                    gp.*,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    user.fio, st.tag, gp.operator_key AS CONTR_ID,
					ts.date_fn,
					(ts.date_fn IS NOT NULL AND ts.date_fn <= NOW()) AS is_otlozh_date,
                    (gp.op_b2b + gp.op_b2c) AS PRIORITY, (SELECT `title` FROM `gp_types` WHERE `id`=gp.gptype_id) AS gptype_name, (SELECT `title` FROM `gp_subtypes` WHERE `id`=gp.gpsubtype_id) AS gpsubtype_name
                FROM
                    `tasks` AS ts
                    LEFT JOIN `gp` AS gp ON ts.id=gp.task_id
                    LEFT JOIN `list_contr` AS ca ON gp.cnt_id=ca.id
                    LEFT JOIN `task_types` AS typ ON gp.type_id=typ.id
                    LEFT JOIN `task_status` AS st ON ts.status_id=st.id
                    LEFT JOIN `task_users` AS tu ON gp.task_id=tu.task_id
                    LEFT JOIN `users` AS user ON user.id=tu.user_id
                     $sql_add
                WHERE $filter
                  and case when gp.sc_id = 0 then 1
                           when exists (select * from user_sc_access usa where usa.user_id = " . $USER->getId() . " and usa.sc_id = gp.sc_id) then 1
                           else 0
                       end = 1
                GROUP BY ts.id
                ORDER BY
                    $order 
                LIMIT " . intval($_REQUEST['filter_start']) . ", " . intval($_REQUEST["filter_rpp"]);

        //die($sql);
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        $total = $DB->getFoundRows();
        while ($a = $DB->fetch(true)) {
            $t = new GlobalProblemTask($a["TASK_ID"]);
            $a['CLOSE_TIME'] = $a["close_time"] ? date("d.m.Y H:i", strtotime($a["close_time"])) : "&mdash;";
            $a["DATETOGSS"] = $a["operator_2gss"];
            $a["GPTYPE_NAME"] = $a["gptype_name"] ? $a["gptype_name"] : "&mdash;";
            $a["GPSUBTYPE_NAME"] = $a["gpsubtype_name"] ? $a["gpsubtype_name"] : "&mdash;";
            $addr = $DB->getCell("SELECT la.full_address FROM list_addr la JOIN gp_addr ga ON ga.dom_id = la.id AND ga.task_id =" . $DB->F($a["TASK_ID"]) . ";");
            if ($addr) {
                $a["ADDRLIST"] = implode("\r\n", $addr);
            } else {
                $a["ADDRLIST"] = "Адреса не распознаны.";
            }
            $tpl->setCurrentBlock('row');
            $tpl->setVariable($a);
            $tpl->parse('row');
        }
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"], $total, "#start-%s"));
        $tpl->setVariable("CURRENT_USER", $USER->getFio());
        $tpl->setVariable("CURRENT_DATE", rudate("H:i d M Y"));
        $tpl->show();

    }

    function delete_returned_tmc_from_ticket()
    {
        global $USER, $DB;
        $row_id = $_REQUEST["row_id"];
        $task_id = $_REQUEST["task_id"];
        $and_tmc = $_REQUEST['and_tmc'];
        if ($row_id && $task_id) {
            $tmc_task = $DB->getRow("SELECT * FROM `tmc_ticket` WHERE `id`=" . $DB->F($row_id) . ";", true);

            if ($tmc_task) {
                $isontask = $DB->getRow("SELECT * FROM `tmc_ticket` WHERE `tmc_tech_sc_id`=" . $DB->F($tmc_task["tmc_tech_sc_id"]) . " AND !`isreturn`", true);
                if (!$isontask) {
                    $sql = "DELETE FROM `tmc_ticket` WHERE `id`=" . $DB->F($tmc_task["id"]) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) $err[] = $DB->error();
                    $tmc_sc_task = $DB->getRow("SELECT * FROM `tmc_sc_tech` WHERE `id`=" . $DB->F($tmc_task["tmc_tech_sc_id"]) . " AND `tmc_sc_id`>0 LIMIT 1;", true);
                    if ($tmc_sc_task) {
                        $DB->query("DELETE FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $tmc_sc_task["tmc_sc_id"] . " LIMIT 2;");
                        $r_tmc_sc_serial = $DB->getField("SELECT `serial` FROM `tmc_sc` WHERE `id`=" . $DB->F($tmc_sc_task["tmc_sc_id"]) . ";");
                        $r_tmc_id = $DB->getField("SELECT `tmc_id` FROM `tmc_sc` WHERE `id`=" . $DB->F($tmc_sc_task["tmc_sc_id"]) . ";");
                        $tmctitle = $DB->getField("SELECT `title` FROM `list_materials` WHERE `id`=" . $DB->F($r_tmc_id) . ";");
                        if ($r_tmc_sc_serial) {
                            $DB->query("DELETE FROM `tmc_sc` WHERE `serial`=" . $DB->F($r_tmc_sc_serial) . " LIMIT 2;");
                            $tag = "<strong>Удаление ТМЦ с заявки. </strong>";
                            if ($tmctitle) $tag .= " Тип: " . $tmctitle . " ";
                            $tag .= "S/N: " . $r_tmc_sc_serial;
                            if ($r_tmc_id && $and_tmc) {
                                $DB->query("DELETE FROM `list_materials` WHERE `id`=" . $DB->F($r_tmc_id) . " LIMIT 1;");
                                $tag .= " <strong>Удален тип оборудования: </strong> " . $tmctitle;
                            }
                            if (sizeof($err)) {
                                $ret["error"] = implode('\r\n', $err);
                            } else {
                                $ret["ok"] = "ok";
                                $t = new Task($task_id);
                                $t->addComment("", $tag);
                                $ret["comment"] = $t->getLastComment();
                            }
                        } else {
                            $ret["error"] = "Отсутствует запись о выдаче на склад СЦ!";
                        }
                    } else {
                        $ret["error"] = "Отсутствует запись о выдаче на техника!";
                    }
                } else {
                    $ret["error"] = "Вы пытаетесь удалить принятое по данной заявке ТМЦ, предварительно списав его на эту же заявку! Удалить такое ТМЦ нельзя. Сначала откатите списание на заявку, сохраните отчет, а после, - повторите попытку.";
                }
            } else {
                $ret["error"] = "Отсутствует запись о списании на заявку!";
            }
        } else {
            $ret["error"] = "Недостаточно данных для выполнения операции";
        }
        echo json_encode($ret);
        return false;
    }


    function return_tmc_from_ticktet()
    {
        global $DB, $USER;

        $task_id = $_REQUEST["task_id"];
        $tech_id = $_REQUEST["tech_id"];
        $tmccat = $_REQUEST["r_tmccat_inform"];
        $tmc_name_id = $_REQUEST["r_tmc_name_id"];
        $tmc_name = $_REQUEST["new_tmc_name"];
        $barcode = $_REQUEST["new_tmc_barcode"];
        $serial = $_REQUEST["new_tmc_serial"];
        $desc = $_REQUEST["reason"];
        $cmm = $_REQUEST["comment"];
        ob_start();
        print_r($_REQUEST);
        $ret["addinfo"] = ob_get_contents();
        ob_end_clean();
        //$ret["addinfo"] = $desc . " " . $cmm;
        if (!$tmc_name_id) {
            $sql = "INSERT INTO `list_materials` (`title`, `cat_id`, `bar_code`, `metric_flag`, `metric_title`) VALUES (" . $DB->F($tmc_name) . ", " . $DB->F($tmccat) . ", " . $DB->F($barcode) . ", " . $DB->F(0) . ", " . $DB->F("шт.") . ");";
            $DB->query($sql);
            $tmc_name_id = $DB->insert_id();
            $wasnew = true;
            $tag = "<strong>Добавлен тип ТМЦ: </strong>" . $tmc_name;
            $DB->free();
        } else {
            $wasnew = false;
            $tmc_name = $DB->getField("SELECT `title` FROM `list_materials` WHERE `id`=" . $DB->F($tmc_name_id) . ";");
        }

        $sql = "SELECT * FROM `tmc_sc` WHERE `serial`=" . $DB->F($serial) . " ORDER BY ledit DESC LIMIT 1;";
        $row = $DB->getRow($sql, true);
        $istmc = isset($row['id']) ? $row['id'] : null;
        if (!$istmc || ($row['incoming'] == 0 && $row['inplace'] == 0)) {
            $sc_id = $DB->getField("SELECT `sc_id` FROM `link_sc_user` WHERE `user_id`=" . $DB->F($tech_id) . ";");
            $dismantled = true; // демонтаж любого оборудования

            // демонтаж оборудования костыль для заведения SN в базе ТМЦ на СЦ
            $sql = 'INSERT INTO tmc_sc SET
                      sc_id = ' . $DB->F($sc_id) . ',
                      tmc_id=' . $DB->F($tmc_name_id) . ',
                      `serial` = ' . $DB->F($serial) . ',
                      `count` = ' . $DB->F(1) . ',
                      user_id = ' . $DB->F($USER->getId()) . ',
                      inplace=' . $DB->F(0) . ',
                      dismantled = ' . $DB->F($dismantled) . ',
                      incoming = ' . $DB->F(1) . ', # оприходовано
                      from_task_id=' . $DB->F($task_id) . ',
                      from_tech_id=' . $DB->F($tech_id) . ';';
            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
                echo json_encode($ret);
                return false;
            }
            $tmc_sc_id_f = $DB->insert_id();
            $DB->free();

            $sql = 'INSERT INTO tmc_sc SET
                      sc_id = ' . $DB->F($sc_id) . ',
                      tmc_id=' . $DB->F($tmc_name_id) . ',
                      `serial` = ' . $DB->F($serial) . ',
                      `count` = ' . $DB->F(1) . ',
                      user_id = ' . $DB->F($USER->getId()) . ',
                      inplace=' . $DB->F(0) . ',
                      dismantled = ' . $DB->F($dismantled) . ',
                      incoming = ' . $DB->F(0) . ', # списано
                      from_task_id=' . $DB->F($task_id) . ',
                      from_tech_id=' . $DB->F($tech_id) . ';';

            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
                echo json_encode($ret);
                return false;
            }
            $tmc_sc_id = $DB->insert_id();
            $DB->free();

            $sql = 'INSERT INTO tmc_sc_tech SET
                      sc_id = ' . $DB->F($sc_id) . ',
                      tech_id=' . $DB->F($tech_id) . ',
                      tmc_sc_id = ' . $DB->F($tmc_sc_id) . ',
                      `count` = ' . $DB->F(1) . ',
                      user_id = ' . $DB->F($USER->getId()) . ',
                      inplace=' . $DB->F(0) . ',
                      dismantled = ' . $DB->F($dismantled) . ',
                      ledit=now(),
                      incoming = ' . $DB->F(1) . ' # оприходовано
                      ;';

            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
                echo json_encode($ret);
                return false;
            }
            $DB->free();

            $sql = 'INSERT INTO tmc_sc_tech SET
                      sc_id = ' . $DB->F($sc_id) . ',
                      tech_id=' . $DB->F($tech_id) . ',
                      tmc_sc_id = ' . $DB->F($tmc_sc_id) . ',
                      `count` = ' . $DB->F(1) . ',
                      user_id = ' . $DB->F($USER->getId()) . ',
                      ledit=now(),
                      inplace=' . $DB->F(1) . ', # на руках
                      dismantled = ' . $DB->F($dismantled) . ',
                      incoming = ' . $DB->F(0) . ' # оприходовано
                      ;';
            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
                echo json_encode($ret);
                return false;
            }
            $tmc_tech_sc_id = $DB->insert_id();
            $DB->free();

            $sql = "INSERT INTO `tmc_ticket` SET
                      `task_id`=" . $DB->F($task_id) . ",
                      `tmc_tech_sc_id` = " . $DB->F($tmc_tech_sc_id) . ",
                      `count` = " . $DB->F(1) . ",
                      `tech_id` = " . $DB->F($tech_id) . ",
                      `user_id` = " . $DB->F($USER->getId()) . ",
                      `isreturn` = " . $DB->F(1) . ",
                      `reason_id` = " . $DB->F($desc) . ",
                      `comment_id` = " . $DB->F($cmm) . ",
                      ledit= now()
                      ;";
            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
                echo json_encode($ret);
                return false;
            }
            $tmc_task_id = $DB->insert_id();
            $DB->free();
            $DB->query("UPDATE `tmc_sc` SET `tmc_ticket_id`=" . $DB->F($tmc_task_id) . " WHERE `id`=" . $DB->F($tmc_sc_id_f) . ";");
            $DB->free();
            $DB->query("UPDATE `tmc_sc` SET `tmc_ticket_id`=" . $DB->F($tmc_task_id) . " WHERE `id`=" . $DB->F($tmc_sc_id) . ";");
            $DB->free();
            if ($tmc_task_id) {
                $user = $DB->getField("SELECT `fio` FROM `users` WHERE `id`=" . $DB->F($tech_id) . ";");
                if ($wasnew) $add = "wasnew='1'";
                $ret["tpl"] = "<tr id=\"$tmc_task_id\" class='r_tmc_rows rtmc_$tmc_task_id newrtmcid newtmc_id_$tmc_task_id'><td align='center'>" . $tmc_name . "</td><td align='center'>" . $serial . "</td><td>" . $user . "</td><td><a $add id='$tmc_task_id' task_id='$task_id' class='removertmc' href='#'><img src='/templates/images/cross.png' /></a></td></tr>";
                $ret["ok"] = "ok";
                $t = new Task($task_id);
                $t->addComment("", "<strong>Прием ТМЦ с заявки: </strong>" . $tmc_name . ", S/N: " . $serial . " " . $tag);
                $ret["comment"] = $t->getLastComment();
            }
        } else {
            $ret["error"] = "Эта ТМЦ уже находится на нашем складке!";
        }
        echo json_encode($ret);
        return false;
    }

    function calcticket()
    {
        global $DB, $USER;
        //print_r($_POST);
        //die();
        $task_id = $_POST["calc_tsk_id"];
        $cs_items = false;
        $ts_items = false;
        if (!$task_id) {
            echo "notfilled";
            return false;
        }

        $task = $task_id;

        $currentCalc = $DB->getRow("SELECT * FROM `gp_calcs` WHERE `task_id`=" . $DB->F($task_id) . ";", true);
        if ($currentCalc) {
            $DB->query("UPDATE `gp_calcs` SET `edit_user_id`=" . $DB->F($USER->getId()) . " WHERE `task_id`=" . $DB->F($task_id) . ";");
            $DB->free();
        } else {
            $DB->query("INSERT INTO `gp_calcs` (`task_id`, `user_id`, `calc_date`, `edit_user_id`) VALUES (" . $DB->F($task_id) . ", " . $DB->F($USER->getId()) . ", " . $DB->F(date("Y-m-d")) . ", " . $DB->F($USER->getId()) . ");");
            $DB->free();
        }

        /**
         * УДаление предыдущего расчёта ТМЦ
         */
        $sql = "SELECT tmc_tech.id FROM `tmc_ticket` AS tickdata
                    LEFT JOIN `tmc_sc_tech` AS tmc_tech ON tmc_tech.id=tickdata.tmc_tech_sc_id
                    WHERE tickdata.isreturn=0 AND tickdata.task_id=" . $DB->F($task) . ";";
        //die($sql);
        $DB->query($sql);
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $res[] = $r;
                if ($r["metric_flag"] != 1) {
                    $sql = "UPDATE `tmc_sc_tech` SET `inplace`=1, ledit=now() WHERE `inplace`=0 AND `incoming`=0 AND `id`=" . $DB->F($r["id"]) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql;
                        trigger_error($DB->error());
                        return false;
                    }
                    $DB->free();
                } else {
                    $sql = "DELETE FROM `tmc_sc_tech` WHERE `id`=" . $DB->F($r["id"]) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql;
                        trigger_error($DB->error());
                        return false;
                    }
                    $DB->free();

                }

            }
        }
        $DB->free();

        $sql = "DELETE FROM `tmc_ticket` WHERE `task_id`=" . $DB->F($task) . " AND `isreturn`=0;";
        $DB->query($sql);
        if ($DB->errno()) {
            echo "error";
            trigger_error($DB->error());
            return false;
        }
        /* \\\\\ УДаление предыдущего расчёта ТМЦ */

        if (count($_REQUEST["writeoff_tmc"])) {
            foreach ($_REQUEST["writeoff_tmc"] as $key => $item) {
                $serial = isset($_REQUEST["writeoff_tmc"][$key]['sn']) ? $_REQUEST["writeoff_tmc"][$key]['sn'] : false;
                $target_sc_id = $_REQUEST["target_sc_id"];
                $tech_id = $item['tech_id'];
                $calc_tsk_id = $_REQUEST["calc_tsk_id"];
                $isrent = $_REQUEST["writeoff_tmc"][$key]['rent'] == "on" ? "1" : "0";

                if ($serial)
                    $sql = "SELECT
                        tmc_sc.id
                    FROM
                        `tmc_sc`
                        LEFT JOIN
                        tmc_sc_tech ON (tmc_sc_tech.tmc_sc_id = tmc_sc.id)
                    WHERE
                        tmc_sc.`sc_id` = " . $DB->F($target_sc_id) . " AND
                        tmc_sc.`tmc_id` = " . $DB->F($item['tmc_id']) . " AND
                        tmc_sc_tech.tech_id = " . $DB->F($tech_id) . " AND
                        `serial`=" . $DB->F($serial) . " AND
                        /* списано с СЦ */
                        (tmc_sc.`incoming` = 0 OR tmc_sc.`incoming` IS NULL) AND
                        (tmc_sc.`inplace` = 0 OR tmc_sc.`inplace` IS NULL)
                    ;";
                else
                    $sql = "SELECT
                        tmc_sc.id
                    FROM
                        `tmc_sc`
                        LEFT JOIN
                        tmc_sc_tech ON (tmc_sc_tech.tmc_sc_id = tmc_sc.id)
                    WHERE
                        tmc_sc.`sc_id` = " . $DB->F($target_sc_id) . " AND
                        tmc_sc.`tmc_id` = " . $DB->F($item['tmc_id']) . " AND
                        tmc_sc_tech.tech_id = " . $DB->F($tech_id) . " AND
                        /* списано с СЦ */
                        (tmc_sc.`incoming` = 0 OR tmc_sc.`incoming` IS NULL) AND
                        (tmc_sc.`inplace` = 0 OR tmc_sc.`inplace` IS NULL)
                    ;";
                $scRec = $DB->getField($sql);
                if (empty($scRec)) {
                    echo $err = "error Оборудование не было верно списано с Сервисного Центра! Обратитесь к разработчику.";
                    print_r($item);
                    trigger_error($err);
                    return false;
                }

                if ($DB->errno()) {
                    echo "error" . $DB->error() . $sql;
                    trigger_error($DB->error());
                    return false;
                }

                $sql1 = false;
                if ($serial && $item['count'] == 1) {
                    $sql = "UPDATE /* списание ТМЦ с техника */
                        `tmc_sc_tech`
                    SET
                        `inplace`=0,
                        ledit = now()
                    WHERE
                        `tmc_sc_id`=" . $DB->F($scRec) . " AND
                        /* на руках у техника */
                        `inplace`=1 AND (`incoming`=0 OR `incoming` IS NULL)
                    ;";
                    $sql1 = "SELECT /* id записи ТМЦ на руках у техника */
                        `id`
                    FROM
                        `tmc_sc_tech`
                    WHERE
                        `tmc_sc_id`=" . $DB->F($scRec) . " AND
                         /* на руках у техника */
                        `inplace`=1 AND (`incoming`=0 OR `incoming` IS NULL);";
                    $outRec = $DB->getField($sql1);
                } else {
                    $sql = "INSERT INTO
                        `tmc_sc_tech`
                    SET
                        `sc_id` = " . $DB->F($target_sc_id) . ",
                        `tech_id` = " . $DB->F($tech_id) . ",
                        `tmc_sc_id` = " . $DB->F($scRec) . ",
                        `count` = " . $DB->F($item['count']) . ",
                        `user_id` = " . $DB->F($USER->getId()) . ",
                        ledit=now(),
                        `inplace` = 0,
                        `incoming` = 0
                    ;";
                }
                $DB->query($sql);

                if ($DB->errno()) {
                    echo "error" . $DB->error() . $sql;
                    trigger_error($DB->error());
                    return false;
                }
                if (!$sql1)
                    $outRec = $DB->insert_id();

                $DB->free();

                if ($outRec) {
                    $sql = "INSERT INTO
                        `tmc_ticket`
                    SET
                        `task_id` = " . $DB->F($calc_tsk_id) . ",
                        `tmc_tech_sc_id` = " . $DB->F($outRec) . ",
                        `count` = " . $DB->F($item['count']) . ",
                        `tech_id` = " . $DB->F($tech_id) . ",
                        `isrent` = " . $DB->F($isrent) . ",
                        `user_id` = " . $DB->F($USER->getId()) . ",
                        ledit = now()
                    ;";
                    $DB->query($sql);

                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql;
                        trigger_error($DB->error());
                        return false;
                    }
                    $DB->free();
                } else {
                    echo $err = "error Оборудование не было верно записано на техника! обратитесь к разработчику.";
                    print_r($item);
                    //trigger_error($err);
                    return false;
                }

            }

        } else {
            echo "notfilled";
            return false;
        }


        echo "ok";
        return false;

    }

    function movetosmr()
    {
        global $DB, $USER;
        $task_id = $_REQUEST["task_id"];
        $t = new GlobalProblemTask($task_id);
        if (!$task_id || !$t) {
            redirect(link2("gp", false), "Не указан идентификатор заявки для передачи в РМС!");
            exit();
        }
        $target_status = 70;
        $target_sc = 9;
        $responsibleTechs = array(3076, 3528, 3389);
        $target_sc_name = $DB->getField("SELECT `title` FROM `list_sc` WHERE `id`=" . $DB->F($target_sc) . ";");
        $target_sc_name = $target_sc_name ? $target_sc_name : "не определен";
        foreach ($responsibleTechs as $item) {
            $user_item = adm_users_plugin::getUser($item);
            if ($user_item)
                $responsibleTechFIOs[] = $user_item["fio"];
        }
        if (!$t->setIspoln($responsibleTechs)) {
            $err[] = "Ошибка перепоручения заявки";
        }
        if (!$t->setSCId($target_sc, true)) {
            $err[] = "Ошибка установки СЦ";
        }

        if (!$t->addComment("Заявка передана в РМС. Перепоручена сотрудникам: " . implode(", ", $responsibleTechFIOs) . ".Установлен СЦ: " . $target_sc_name, "Передача в РМС", $target_status)) {
            $err[] = "Ошибка добавления комментария";
        }
        if (sizeof($err)) {
            $msg = "Ошибки: " . implode("\r\n", $err);
        } else {
            $task = $this->getEm()->find(\models\Task::class, $task_id);
            $this->getDispatcher()->dispatch(TaskMovedToRmsEvent::NAME, new TaskMovedToRmsEvent($task));
            $msg = "Заявка успешно передана в РМС";
        }
        redirect("viewticket?task_id=" . $task_id, $msg);
        exit();
    }


    function deleteticket()
    {
        global $DB, $USER;
        if ($USER->checkAccess("deletegp", User::ACCESS_WRITE)) {
            if ($id = $_REQUEST["task_id"]) {
                $tmc = $DB->getField("SELECT count(`id`) FROM `tmc_ticket` WHERE `task_id`=" . $DB->F($id) . ";");
                if ($tmc) {
                    UIError("Удаление заявки невозможно, т.к. по ней есть списанные ТМЦ. Перед удалением заявки необходимо откатить списание ТМЦ!");
                }

                $err = array();
                $sql = "DELETE FROM `tasks` WHERE `id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_files` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_comments` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_users` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `gp` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                if (!sizeof($err))
                    redirect($this->getLink('viewlist'), "Заявка с ID $id успешно удалена.");
                else
                    redirect($this->getLink('viewlist'), "Удаление заявки с ID $id завершено с ошибками! Обратитесь к разработчикам! " . implode("\r\n", $err));

            } else {
                redirect($this->getLink('viewlist'), "Не указан идентификатор заявки для удаления.");

            }
        } else {
            redirect($this->getLink('viewlist'), "Нет доступа к удалению заявок.");
        }

    }

    function checkTaskStatusA()
    {
        global $DB;
        if ($task_id = $_POST['task_id']) {
            $task = new GlobalProblemTask($task_id);
            $currentStatus = $task->getStatusId();
            $doneStatus = adm_statuses_plugin::getStatusByTag("donegp", "gp");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closedgp", "gp");
            if ($currentStatus == $doneStatus["id"] || $currentStatus == $completeStatus["id"]) {
                $ret = "ok";
            } else {
                $ret = "nostatus";
            }
        } else {
            $ret = "error";
        }
        echo $ret;
        return false;
    }

    function getCurrentCalcA()
    {
        global $DB, $USER;


        if ($task_id = $_POST["task_id"]) {

            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $task = new GlobalProblemTask($task_id);
            $currentStatus = $task->getStatusId();
            if (!$task->getSCId()) {
                echo "<p class='redmessage error'>Перед выполнением расчета заявки, пожалуйста, укажите СЦ!</p>";
                return false;
            }
            $tpl->loadTemplatefile($USER->getTemplate() . "/calc.tmpl.htm");

            $tpl->setVariable("TMC_SC_ID", $task->getSCId());

            // добавить дату расчета
            $currentCalc = $DB->getRow("SELECT * FROM `gp_calcs` WHERE `task_id`=" . $DB->F($task_id) . ";", true);

            if ($currentCalc) {
                $u = adm_users_plugin::getUser($currentCalc["user_id"]);
                $tpl->setVariable('CALC_TECH_NAME', $u["fio"]);
                $tpl->setVariable("CALC_DATE", date("d.m.Y", strtotime($currentCalc["calc_date"])));
            } else {
                $tpl->setVariable('CALC_TECH_NAME', $USER->getFio());
                $tpl->setVariable("CALC_DATE", date("d.m.Y"));
            }

            /// ^^^^^^ добавить выполнившего расчет


            // TMC
            $sql = "SELECT
                tmc_tech.id,
                tmc_tech.count,
                tmc_sc.serial,
                tmc_sc.tmc_id,
                tmc_list.title,
                tickdata.isrent,
                tmc_list.metric_flag,
                tickdata.tech_id
            FROM
            `tmc_ticket` AS tickdata
            LEFT JOIN
            `tmc_sc_tech` AS tmc_tech ON tmc_tech.id=tickdata.tmc_tech_sc_id
            LEFT JOIN
            `tmc_sc` AS tmc_sc ON tmc_sc.id=tmc_tech.tmc_sc_id
            LEFT JOIN
            `list_materials` AS tmc_list ON tmc_list.id=tmc_sc.tmc_id
            WHERE
                !tickdata.isreturn AND
                tickdata.task_id=" . $DB->F($task_id) . " AND tmc_tech.id IS NOT NULL
            ;";
            $DB->query($sql);
            if ($DB->errno()) {
                echo "error " . $sql . " " . $DB->error();
                trigger_error($DB->error());
                return false;
            }
            if ($DB->num_rows()) {
                while ($res = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("tmc_row");
                    $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                    $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                    if ($readonly) $tpl->setVariable("AFTERDEADLINE_HIDDENTMC", 'hidden');
                    if ($res["metric_flag"]) {
                        $tpl->setVariable("NO_SERIAL", "&mdash;");
                        $tpl->setVariable("NO_ISRENT", "&mdash;");
                        $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                    } else {
                        $tpl->setCurrentBlock("hasserial");
                        $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                        $tpl->setVariable("TMC_S_ID", $res["id"]);
                        $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                        $tpl->parse("hasserial");
                        $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                        $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                        $tpl->setCurrentBlock("isrent");
                        $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                        $tpl->setVariable("IS_RENT_CHECKED", $res["isrent"] == '1' ? "checked=\"checked\"" : "");
                        $tpl->setVariable("TMC_IR_ID", $res["id"]);
                        $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                        $tpl->parse("isrent");
                        $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                        $tpl->setVariable("TMC_COUNT", "1");
                        $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                    }
                    $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                    $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                    $tpl->setVariable("TMC_TECH_ID", $res["tech_id"]);
                    $tpl->setVariable("TMC_ID", $res["tmc_id"]);
                    $tpl->setVariable("TMC_TITLE", $res["title"]);
                    $tpl->setVariable("TMC_COUNT", $res["count"]);

                    $tpl->parse("tmc_row");
                }
                $DB->free();
                $tpl->setVariable("NO_TMC_ROWS", "display: none;");

            } else {
                $tpl->setVariable("NO_TMC_ROWS", "");
            }

            /*$sql = "SELECT tmc_tech.id, tickdata.id as tid, tmc_tech.count, tmc_sc.serial, tmc_sc.tmc_id, tmc_list.title, tickdata.isrent, tmc_list.metric_flag, tickdata.tech_id FROM `tmc_ticket` AS tickdata
                     LEFT JOIN `tmc_sc_tech` AS tmc_tech ON tmc_tech.id=tickdata.tmc_tech_sc_id
                     LEFT JOIN `tmc_sc` AS tmc_sc ON tmc_sc.id=tmc_tech.tmc_sc_id
                     LEFT JOIN `list_materials` AS tmc_list ON tmc_list.id=tmc_sc.tmc_id
                     WHERE tickdata.isreturn AND tickdata.task_id=".$DB->F($task_id).";";*/
            $sql = "SELECT
                tmc_sc_tech.id,
                tmc_ticket.id AS tid,
                tmc_sc_tech.count,
                tmc_sc.serial,
                tmc_sc.tmc_id,
                list_materials.title,
                tmc_ticket.isrent,
                list_materials.metric_flag,
                tmc_sc.from_tech_id,
                tmc_ticket.isreturn
            FROM
                `tmc_ticket`
                    LEFT JOIN
                tmc_sc_tech ON (tmc_sc_tech.id = tmc_ticket.tmc_tech_sc_id)
                    LEFT JOIN
                tmc_sc ON (tmc_sc.id = tmc_sc_tech.tmc_sc_id)
                    LEFT JOIN
                list_materials ON (tmc_sc.tmc_id = list_materials.id)
            WHERE
                tmc_ticket.isreturn AND task_id = " . $DB->F($task_id) . "
            ;";

            $DB->query($sql);
//            d($sql);die;
            if ($DB->errno()) {
                echo "error " . $sql . " " . $DB->error();
                trigger_error($DB->error());
                return false;
            }
            if ($DB->num_rows()) {
                while ($res = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("r_tmc_row");
                    $tpl->setVariable("R_TMC_ROW_ID", $res["tid"]);
                    $tpl->setVariable("R_TMC_TECH_ID", $res["tech_id"]);
                    $tpl->setVariable("R_TMC_S_ID", $res["id"]);
                    $tmc_row = $DB->getRow("SELECT `serial`,`from_tech_id`, `tmc_id` FROM `tmc_sc` WHERE `from_task_id`=" . $DB->F($task_id) . " AND `tmc_ticket_id`=" . $DB->F($res["tid"]) . ";", true);
                    $tpl->setVariable("R_TMC_SERIAL", $tmc_row["serial"]);
                    $tpl->setVariable("R_TMC_COUNT", "1");
                    $userName = $DB->getField("SELECT `fio` FROM `users` WHERE `id`=" . $DB->F($tmc_row["from_tech_id"]) . ";");
                    if ($userName) $tpl->setVariable("R_TMC_TECHFIO", $userName);
                    else $tpl->setVariable("R_TMC_TECHFIO", "<center>&mdash;</center>");

                    $tmc_name = $DB->getField("SELECT tmc_list.title FROM `list_materials` AS tmc_list WHERE tmc_list.id=" . $DB->F($tmc_row["tmc_id"]) . ";");
                    $tpl->setVariable("R_TMC_ID", $tmc_row["tmc_id"]);
                    $tpl->setVariable("R_TMC_TITLE", $tmc_name);

                    $tpl->setVariable("RTMCTASK_ID", $task_id);
                    if (!$res["id"]) {
                        $tpl->setVariable("AFTERDEADLINE_HIDDEN_R_TMC", "style='display:none;'");
                    }
                    $tpl->parse("r_tmc_row");
                }
                $DB->free();
                $tpl->setVariable("NO_R_TMC_ROWS", "display: none;");

            } else {
                $tpl->setVariable("NO_R_TMC_ROWS", "");
            }

            $tpl->setVariable("TMC_SC_ID", $task->getSCId());

            $tpl->show();

        } else {
            echo "error";
        }
        return;
    }


    function viewticket()
    {
        global $DB, $USER;

        $id = (int)($_GET['task_id'] ? $_GET['task_id'] : ($_GET['id'] ? $_GET['id'] : "0"));
        if (!$id) UIError("Не указан идентификатор заявки");

        if ($id > 0) {
            $sql = "SELECT max(sc.title) title
FROM list_sc sc,
gp tt,
tasks t
WHERE tt.task_id = " . $id . "
  AND t.id = tt.task_id
  AND sc.id = tt.sc_id
  AND t.plugin_uid = '" . $this->getUID() . "'
  AND CASE WHEN tt.sc_id = 0 THEN 0
           WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = tt.sc_id) THEN 0
           ELSE 1
      END = 1";
            $sc_with_no_access = $DB->getField($sql);
            if (strlen($sc_with_no_access) > 0) {
                #$sc_with_no_access .= $sql;
                UIError("У Вас нет доступа на СЦ " . $sc_with_no_access . ", просмотр заявки невозможен.");
            }
        }

        $ticket = new GlobalProblemTask($id);

        if (!$ticket->getId()) {
            UIError("Заявка с таким номером не найдена! Возможно, она была удалена.");
        }
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $currentStatus = $ticket->getStatusId();
        $doneStatus = adm_statuses_plugin::getStatusByTag("donegp", "gp");
        $completeStatus = adm_statuses_plugin::getStatusByTag("closedgp", "gp");
        if (!$USER->checkAccess("rooteditgpticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock('is_contr_fields');
        }
        if ($currentStatus == $doneStatus["id"] || $currentStatus == $completeStatus["id"]) $targetStatus = true;
        $readonly = false;
        $LKUser = adm_users_plugin::isLKUser($USER->getId());
        $modelTask = $this->getEm()->find(ModelTask::class, $id);
        if ($LKUser) {
            $tpl_add = "_lk";
        } else {
            if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
                $tpl_add = "_tlk";
            } else {
                $tpl_add = "";
            }
        }

        /**
         * check lk user access to task
         */
        if (null !== $this->getUserModel()->getMasterPartner() &&
            (
                !$this->getUserModel()->getAllowAccessCu() || $this->getUserModel()->getMasterPartner() !== $modelTask->getAnyTicket()->getPartner()
            )
        ) {
            throw new AccessDeniedException('У вас нет доступа к этой заявке!');
        }

        if ($readonly) {
            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate() . "/ticket_readonly.tmpl.htm");
            else
                $tpl->loadTemplatefile("ticket_readonly.tmpl.htm");
        } else {
            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate() . "/ticket$tpl_add.tmpl.htm");
            else
                $tpl->loadTemplatefile("ticket$tpl_add.tmpl.htm");
        }
        if (class_exists("adm_areas_plugin", true) && method_exists("adm_areas_plugin", "getAreaList")) {
            $area = new adm_areas_plugin();
            $tpl->setVariable("AREA_OPTIONS", $area->getAreaList());
        }
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKRegionList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_REGION", $addr->getKRegionList(getcfg('default_region')));
        }
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKAreaList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_AREA", $addr->getKAreaList(getcfg('default_region')));
        }
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKCityList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_CITY", $addr->getKCityList(getcfg('default_region')));
        }
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKNPList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_NP", $addr->getKNPList(getcfg('default_region')));
        }
        if (class_exists("adm_ods_plugin", true) && method_exists("adm_ods_plugin", "getOptList")) {
            $addr = new adm_ods_plugin();
            $tpl->setVariable("ODS_OPTIONS", $addr->getOptList());
        }
        $tpl->setVariable("REASON_LIST", adm_gp_reason_plugin::getOptions());
        $tpl->setVariable("COMMENT_LIST", adm_gp_comment_plugin::getOptions());

        if (!$USER->checkAccess("rooteditgpticket", User::ACCESS_WRITE)) {
            $sql = "SELECT COUNT(*) FROM `contr_tickets` WHERE `task_id`=" . $DB->F($ticket->getId());
            if ($DB->getField($sql) && $ticket->getClntOpAgr() != "") {
                $tpl->setVariable('IS_CONTR_READONLY', 'readonly');
                $tpl->touchBlock('is_contr_fields');
            }
        }
        if (!$USER->checkAccess("calc_ticket_gp", User::ACCESS_WRITE)) {
            $tpl->touchBlock("calcinactive");
        } else {
            $tpl->setVariable("TMC_CAT_LIST_INFORM", adm_material_cats_plugin::getOptions());
            $tpl->setVariable("R_TMC_CAT_LIST_INFORM", adm_material_cats_plugin::getOptions_nonmetric());
        }

        $ispolnListTech = $DB->getCell2("SELECT us.id, us.fio FROM `task_users` AS tu JOIN `users` AS us ON tu.user_id=us.id WHERE tu.task_id=" . $DB->F($id) . " AND tu.ispoln AND us.id IN (SELECT `user_id` FROM `link_sc_user` WHERE 1) ORDER BY us.id");
        if (!$ispolnListTech) {
            $tpl->setVariable("TECH_LIST_INFORM_TECH", "<option value=''>-- нет исполнителей --</option>");
        } else {
            $tpl->setVariable("TECH_LIST_INFORM_TECH", array2options($ispolnListTech));
        }
        $ispolnList = $ispolnListTech;
        if (!$ispolnList) {
            $tpl->setVariable("TECH_LIST_INFORM", "<option value=''>-- нет исполнителей --</option>");
        } else {
            $tpl->setVariable("TECH_LIST_INFORM", array2options($ispolnList));
        }

        $currentStatus = $ticket->getStatusId();
        if ($currentStatus != 69 && $currentStatus != 70 && $currentStatus != 73 && $currentStatus != 77 && $currentStatus != 78) {
            $tpl->setVariable("MOVETOSMRHIDDEN", "class='hidden'");
        }

        $doneStatus = adm_statuses_plugin::getStatusByTag("donegp", "gp");
        $completeStatus = adm_statuses_plugin::getStatusByTag("closedgp", "gp");

        if ($USER->checkAccess("deletegp", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("deleteticket");
            $tpl->setVariable("DEL_TASK_ID", $_GET["task_id"]);
            $tpl->setVariable("PLUG_UID", $this->getUID());

            $tpl->parse("deleteticket");
        }
        $tpl->setVariable('CONTR', $ticket->getContrName());
        $tpl->setVariable('TASK_TYPE', $ticket->getTypeName());

        if ($LKUser) {
            $tpl->setVariable("TASK_FROM_TITLE", adm_ticket_sources_plugin::getById($ticket->getTaskFrom()));
            $sc_name = adm_sc_plugin::getSC($ticket->getSCId());
            $tpl->setVariable("SC_NAME", $sc_name["title"] ? $sc_name["title"] : "&mdash;");
            $tpl->setVariable("SC_LK_ID", $ticket->getSCId());
        } else {
            $tpl->setVariable("SC_LIST", $this->getSCOptions($ticket->getSCId()));
            $sc_name = adm_sc_plugin::getSC($ticket->getSCId());
            $tpl->setVariable("SC_NAME", $sc_name["title"] ? $sc_name["title"] : "&mdash;");
        }
        $tpl->setVariable("OPTITLE", $ticket->getTaskTitle());
        $tpl->setVariable("ADDINFO", $ticket->getAddInfo());
        $tpl->setVariable("CLNTTNUM", $ticket->getTaskOpNum());
        $tpl->setVariable("FILTER_DATE_TO", $ticket->getGssDate());
        $tpl->setVariable("FILTER_TIME_TO", $ticket->getGssTime());
        $tpl->setVariable("FILTER_DATE_FROM", $ticket->getOpDate());
        $tpl->setVariable("FILTER_TIME_FROM", $ticket->getOpTime());
        $tpl->setVariable("FILTER_DATE_PLAN", $ticket->getPlanDate());
        $tpl->setVariable("FILTER_TIME_PLAN", $ticket->getPlanTime());
        $tpl->setVariable("OP_B2B", $ticket->getB2b());
        $tpl->setVariable("OP_B2C", $ticket->getB2c());
        $tpl->setVariable("OP_USER", $ticket->getTaskAuthor());
        $pri = $DB->getField("SELECT COUNT(`id`) FROM `gp_addr` WHERE `task_id`=" . $DB->F($ticket->getId()) . ";");
        $tpl->setVariable("TASK_PRIORITY", $pri);
        $rejTitle = '';
        $rej_id = $ticket->getRejectReasonId(false);
        if ($rej_id) {
            if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejReasonTitle")) {
                $rej = new adm_rejects_plugin();
                $rejTitle = " (" . $rej->getRejReasonTitle($rej_id) . ")";
            }
        }
        $agr_id = $ticket->getAgrId(false);
        if ($agr_id) {
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getAgrById")) {
                $kontr = new kontr_plugin();
                $agr = $kontr->getAgrById($agr_id);
                if ($agr) {
                    $tpl->setVariable("TASK_AGR", $agr["number"] . " от " . $agr["agr_date"]);
                } else {
                    $tpl->setVariable("TASK_AGR", "<strong>ошибка</strong>");
                }
                $agr_contacts = $kontr->getAgrById($agr_id);
                if ($agr_contacts) {
                    $tpl->setVariable("AGR_ACCESS", $agr_contacts['access_contact'] ? nl2br($agr_contacts['access_contact']) : "&mdash;");
                    $tpl->setVariable("AGR_TP", $agr_contacts['tp_contact'] ? nl2br($agr_contacts['tp_contact']) : "&mdash;");

                } else {
                    $tpl->setVariable("AGR_ACCESS", "<strong>ошибка</strong>");
                    $tpl->setVariable("AGR_TP", "<strong>ошибка</strong>");
                }
            }
            $tpl->setVariable("AGR_ID", $agr_id);
        } else {
            $tpl->setVariable("TASK_AGR", "&mdash;");
            $tpl->setVariable("AGR_ACCESS", "&mdash;");
            $tpl->setVariable("AGR_TP", "&mdash;");
        }
        $tpl->setVariable('STATUS_NAME', $ticket->getStatusName() . " " . $rejTitle);

        $tpl->setVariable('STATUS_COLOR', $ticket->getStatusColor());
        $tpl->setVariable('DATE_REG', $ticket->getDateReg("d.m.Y H:i"));

        $tpl->setVariable('TASK_COMMENTS', $ticket->getComments());
        $tpl->setVariable('TASK_ID', $ticket->getId());
        $addrItems = $ticket->getAddrList();

        if ($addrItems) {
            $tpl->setVariable("ROWSEXIST", "style='display:none;'");
            $rtpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $rtpl->loadTemplatefile($USER->getTemplate() . "/addritem.tmpl.htm");
            foreach ($addrItems as $item) {

                if ($item["dom_id"]) {
                    $addrList[] = $item["dom_id"];
                    $rtpl->setCurrentBlock("row");
                    $rtpl->setVariable("ROW_ID", $item["id"]);
                    $rtpl->setVariable("AREA", $item["area"]);

                    $rtpl->setVariable("ORDER_ID", $item["order_id"]);
                    $rtpl->setVariable("GP_ADDR_ID", $item["id"]);

                    if ($item["is_not_recognized"] == 0)
                        $rtpl->setVariable("FULL_ADDRESS", $item["op_string"]);
                    else
                        $rtpl->setVariable("FULL_ADDRESS", '<span style="color: red;">' . $item["op_string"] . '</span>');

                    $rtpl->setVariable("POD", $item["pod"]);
                    $rtpl->setVariable("HREF", link2("addr_interface/edit?id=" . $item["dom_id"], false));
                    if ($item["sc_id"]) {
                        $rtpl->setCurrentBlock("issc");
                        $rtpl->setVariable("SC", $item["sc"]);
                        $rtpl->setVariable("SC_ID", $item["sc_id"]);
                        $rtpl->parse("issc");
                    } else {
                        $rtpl->setVariable("NOSC", "&mdash;");
                    }
                    if ($item["ods_id"]) {
                        $rtpl->setCurrentBlock("isods");
                        $rtpl->setVariable("ODS", $item["ods"]);
                        $rtpl->setVariable("ODS_ID", $item["ods_id"]);
                        $rtpl->parse("isods");
                    } else {
                        $rtpl->setVariable("NOODS", "&mdash;");
                    }

                    if ($USER->checkAccess($this->getUID(), User::ACCESS_WRITE)) {
                        $rtpl->setCurrentBlock("deleteaddritem");
                        $rtpl->setVariable("ADDRID", $item["id"]);
                        $rtpl->parse("deleteaddritem");
                    }
                    $rtpl->parse("row");

                } else {
                    $rtpl->setCurrentBlock("row-orig");
                    $rtpl->setVariable("ORIGNAME", $item["op_string"]);
                    if ($USER->checkAccess($this->getUID(), User::ACCESS_WRITE)) {
                        $rtpl->setCurrentBlock("deleteaddritem1");
                        $rtpl->setVariable("ADDRID1", $item["id"]);
                        $rtpl->parse("deleteaddritem1");
                    }
                    $rtpl->parse("row-orig");
                }

            }
            $tpl->setVariable("ADDRLISTBODY", $rtpl->get());
            // tt list
            if (count($addrList)) {
                $activeConn = $DB->getCell("SELECT `id` FROM `task_status` WHERE `is_active` AND `plugin_uid`='connections';");
                $activeConn = preg_replace("/,$/", "", implode(",", $activeConn));
                $activeSkp = $DB->getCell("SELECT `id` FROM `task_status` WHERE `is_active` AND `plugin_uid`='services';");
                $activeSkp = preg_replace("/,$/", "", implode(",", $activeSkp));
                $activeTT = $DB->getCell("SELECT `id` FROM `task_status` WHERE `is_active` AND `plugin_uid`='accidents';");
                $activeTT = preg_replace("/,$/", "", implode(",", $activeTT));
                $addrList = preg_replace("/,$/", "", implode(",", $addrList));
                $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
                    ca.contr_title AS CONTR,
                    typ.title AS TYPE,
                    tt.dom_id,
                    tt.clnt_org_name,
                    tt.clnt_fio,
                    tt.clnt_type,
					tt.clntopagr,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    addr.name, addr.kladrcode, addr.althouse, gfx.c_date, gfx.startTime, user.fio, tt.clnttnum, tt.kv, st.tag, tt.clnttnum AS CONTR_ID,
					ts.date_fn,
					(ts.date_fn IS NOT NULL AND ts.date_fn <= NOW()) AS is_otlozh_date
                FROM
                    `tasks` AS ts
                    LEFT JOIN `tickets` AS tt ON ts.id=tt.task_id
                    LEFT JOIN `list_contr` AS ca ON tt.cnt_id=ca.id
                    LEFT JOIN `task_types` AS typ ON tt.task_type=typ.id
                    LEFT JOIN `task_status` AS st ON ts.status_id=st.id
                    LEFT JOIN `list_addr` AS addr ON addr.id=tt.dom_id
                    LEFT JOIN `gfx` AS gfx ON gfx.task_id=ts.id
                    LEFT JOIN `users` AS user ON user.id=gfx.empl_id
                    LEFT JOIN `list_ods` AS ods ON ods.id=addr.ods_id
                    LEFT JOIN `list_sc` AS sc ON sc.id=addr.sc_id
                    LEFT JOIN `list_area` AS area ON area.id=addr.area_id
                    LEFT JOIN `kladr_street` AS kstreet ON CONCAT(SUBSTR(addr.kladrcode, 1, 15), '00')=kstreet.CODE
                WHERE ts.plugin_uid='accidents' AND ts.status_id IN ($activeTT) AND tt.cnt_id=" . $DB->F($this->getContrID($ticket->getId())) . " AND tt.dom_id IN ($addrList)
                ORDER BY
                    ts.date_reg";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                if ($DB->num_rows()) {
                    while ($t = $DB->fetch(true)) {
                        $tpl->setVariable("TTHIDDEN", "style='display: none;'");
                        $tpl->setCurrentBlock("rowtt");
                        $tpl->setVariable("RTID", $t["TASK_ID"]);
                        $tpl->setVariable("RTCRDATE", $t["DATE"]);
                        $tpl->setVariable("RTWTYPE", $t["TYPE"]);

                        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "decodeFullKLADR")) {
                            $addr = new addr_interface_plugin();
                            $addrString = $addr->decodeFullKLADRPartial($t['kladrcode']);
                        }
                        if (count($addrString)) {
                            $tpl->setCurrentBlock("ttkladr_addr");
                            $tpl->setVariable("RTCITY", $addrString["city"]);
                            $tpl->setVariable("RTSTREET", $addrString["street"]);
                            $tpl->setVariable("RTHOUSE", $t["name"] ? $t["name"] : $t["althouse"]);
                            $tpl->setVariable("RTFLAT", $t["kv"]);

                            $tpl->parse("ttkladr_addr");
                        } else {
                            $tpl->setCurrentBlock("ttno_kladr");
                            $tpl->setVariable("TTNOKLADR", "<center><font color=#FF0000>Адрес не в формате КЛАДР</font></center>");
                            $tpl->parse("ttno_kladr");
                        }
                        $tpl->setVariable("RTSTATUS", $t["STATUS_NAME"]);
                        $tpl->setVariable("RTUSER", $t["fio"]);
                        $tpl->setVariable("TTSTATUSCOLOR", $t["STATUS_COLOR"]);

                        $tpl->parse("rowtt");
                    }
                }
                $DB->free();
                // connections
                $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
                    ca.contr_title AS CONTR,
                    typ.title AS TYPE,
                    tt.dom_id,
                    tt.clnt_org_name,
                    tt.clnt_fio,
                    tt.clnt_type,
					tt.clntopagr,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    addr.name, addr.kladrcode, addr.althouse, gfx.c_date, gfx.startTime, user.fio, tt.clnttnum, tt.kv, st.tag, tt.clnttnum AS CONTR_ID,
					ts.date_fn,
					(ts.date_fn IS NOT NULL AND ts.date_fn <= NOW()) AS is_otlozh_date
                FROM
                    `tasks` AS ts
                    LEFT JOIN `tickets` AS tt ON ts.id=tt.task_id
                    LEFT JOIN `list_contr` AS ca ON tt.cnt_id=ca.id
                    LEFT JOIN `task_types` AS typ ON tt.task_type=typ.id
                    LEFT JOIN `task_status` AS st ON ts.status_id=st.id
                    LEFT JOIN `list_addr` AS addr ON addr.id=tt.dom_id
                    LEFT JOIN `gfx` AS gfx ON gfx.task_id=ts.id
                    LEFT JOIN `users` AS user ON user.id=gfx.empl_id
                    LEFT JOIN `list_ods` AS ods ON ods.id=addr.ods_id
                    LEFT JOIN `list_sc` AS sc ON sc.id=addr.sc_id
                    LEFT JOIN `list_area` AS area ON area.id=addr.area_id
                    LEFT JOIN `kladr_street` AS kstreet ON CONCAT(SUBSTR(addr.kladrcode, 1, 15), '00')=kstreet.CODE
                WHERE ts.plugin_uid='connections' AND ts.status_id IN ($activeConn) AND tt.cnt_id=" . $DB->F($this->getContrID($ticket->getId())) . " AND tt.dom_id IN ($addrList)
                ORDER BY
                    ts.date_reg";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                if ($DB->num_rows()) {
                    while ($t = $DB->fetch(true)) {
                        $tpl->setVariable("CONHIDDEN", "style='display: none;'");
                        $tpl->setCurrentBlock("row");
                        $tpl->setVariable("RID", $t["TASK_ID"]);
                        $tpl->setVariable("RCRDATE", $t["DATE"]);
                        $tpl->setVariable("RWTYPE", $t["TYPE"]);

                        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "decodeFullKLADR")) {
                            $addr = new addr_interface_plugin();
                            $addrString = $addr->decodeFullKLADRPartial($t['kladrcode']);
                        }
                        if (count($addrString)) {
                            $tpl->setCurrentBlock("kladr_addr");
                            $tpl->setVariable("RCITY", $addrString["city"]);
                            $tpl->setVariable("RSTREET", $addrString["street"]);
                            $tpl->setVariable("RHOUSE", $t["name"] ? $t["name"] : $t["althouse"]);
                            $tpl->setVariable("RFLAT", $t["kv"]);

                            $tpl->parse("kladr_addr");
                        } else {
                            $tpl->setCurrentBlock("no_kladr");
                            $tpl->setVariable("NOKLADR", "<center><font color=#FF0000>Адрес не в формате КЛАДР</font></center>");
                            $tpl->parse("no_kladr");
                        }
                        $tpl->setVariable("RSTATUS", $t["STATUS_NAME"]);
                        $tpl->setVariable("RUSER", $t["fio"]);
                        $tpl->setVariable("STATUSCOLOR", $t["STATUS_COLOR"]);

                        $tpl->parse("row");
                    }
                }
                $DB->free();
                // services list
                $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
                    ca.contr_title AS CONTR,
                    typ.title AS TYPE,
                    tt.dom_id,
                    tt.clnt_org_name,
                    tt.clnt_fio,
                    tt.clnt_type,
					tt.clntopagr,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    addr.name, addr.kladrcode, addr.althouse, gfx.c_date, gfx.startTime, user.fio, tt.clnttnum, tt.kv, st.tag, tt.clnttnum AS CONTR_ID,
					ts.date_fn,
					(ts.date_fn IS NOT NULL AND ts.date_fn <= NOW()) AS is_otlozh_date
                FROM
                    `tasks` AS ts
                    LEFT JOIN `tickets` AS tt ON ts.id=tt.task_id
                    LEFT JOIN `list_contr` AS ca ON tt.cnt_id=ca.id
                    LEFT JOIN `task_types` AS typ ON tt.task_type=typ.id
                    LEFT JOIN `task_status` AS st ON ts.status_id=st.id
                    LEFT JOIN `list_addr` AS addr ON addr.id=tt.dom_id
                    LEFT JOIN `gfx` AS gfx ON gfx.task_id=ts.id
                    LEFT JOIN `users` AS user ON user.id=gfx.empl_id
                    LEFT JOIN `list_ods` AS ods ON ods.id=addr.ods_id
                    LEFT JOIN `list_sc` AS sc ON sc.id=addr.sc_id
                    LEFT JOIN `list_area` AS area ON area.id=addr.area_id
                    LEFT JOIN `kladr_street` AS kstreet ON CONCAT(SUBSTR(addr.kladrcode, 1, 15), '00')=kstreet.CODE
                WHERE ts.plugin_uid='services' AND ts.status_id IN ($activeSkp) AND tt.cnt_id=" . $DB->F($this->getContrID($ticket->getId())) . " AND tt.dom_id IN ($addrList)
                ORDER BY
                    ts.date_reg";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                if ($DB->num_rows()) {
                    while ($t = $DB->fetch(true)) {
                        $tpl->setVariable("SKPHIDDEN", "style='display: none;'");
                        $tpl->setCurrentBlock("rowskp");
                        $tpl->setVariable("RSID", $t["TASK_ID"]);
                        $tpl->setVariable("RSCRDATE", $t["DATE"]);
                        $tpl->setVariable("RSWTYPE", $t["TYPE"]);

                        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "decodeFullKLADR")) {
                            $addr = new addr_interface_plugin();
                            $addrString = $addr->decodeFullKLADRPartial($t['kladrcode']);
                        }
                        if (count($addrString)) {
                            $tpl->setCurrentBlock("skladr_addr");
                            $tpl->setVariable("RSCITY", $addrString["city"]);
                            $tpl->setVariable("RSSTREET", $addrString["street"]);
                            $tpl->setVariable("RSHOUSE", $t["name"] ? $t["name"] : $t["althouse"]);
                            $tpl->setVariable("RSFLAT", $t["kv"]);

                            $tpl->parse("skladr_addr");
                        } else {
                            $tpl->setCurrentBlock("sno_kladr");
                            $tpl->setVariable("SNOKLADR", "<center><font color=#FF0000>Адрес не в формате КЛАДР</font></center>");
                            $tpl->parse("sno_kladr");
                        }
                        $tpl->setVariable("RSSTATUS", $t["STATUS_NAME"]);
                        $tpl->setVariable("RSUSER", $t["fio"]);
                        $tpl->setVariable("RSSTATUSCOLOR", $t["STATUS_COLOR"]);

                        $tpl->parse("rowskp");
                    }
                }
            }

        } else {

        }


        //$ticket->getAddrList();
        $tpl->setVariable('TYPE_OPTIONS', $this->getTypeOptions($this->getTypeID($ticket->getId())));
        $tpl->setVariable('CNT_OPTIONS', $this->getContrOptions($this->getContrID($ticket->getId())));
        $complete_status = adm_statuses_plugin::getStatusByTag("done", "gp");
        $check_status = adm_statuses_plugin::getStatusByTag("check", "gp");
        $currentStatus = $ticket->getStatusId();
        if (!$USER->checkAccess("rooteditgpticket", User::ACCESS_WRITE)) {
            if (($currentStatus == $complete_status["id"] || $currentStatus == $check_status["id"])) {
                $tpl->touchBlock("removeinactive");
                $tpl->touchBlock("resetinactive");
                $tpl->setVariable("ISTECH_DISABLED", "disabled=\"disabled\"");
            }

        } else {
            $tpl->touchBlock("changecontr");
        }
        $new_status = adm_statuses_plugin::getStatusByTag("newgp", "gp");
        $report_status = adm_statuses_plugin::getStatusByTag("inprogressgp", "gp");

        if (($currentStatus == $new_status["id"] || $currentStatus == $report_status["id"]) && $USER->checkAccess("edit_contr_gp_ticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("changecontr");
        }
        if ($ticket->getDocID() && $ticket->getDocDate()) {
            $tpl->setVariable("DOC_TICKET_PARAMS", $ticket->getDocID() . " от " . $ticket->getDocDate());
        } else {
            $tpl->setVariable("DOC_TICKET_PARAMS", "&mdash;");
        }
        if ($ticket->getTMCDocID() && $ticket->getTMCDocDate()) {
            $tpl->setVariable("DOC_TMC_PARAMS", $ticket->getTMCDocID() . " от " . $ticket->getTMCDocDate());
        } else {
            $tpl->setVariable("DOC_TMC_PARAMS", "&mdash;");
        }

        UIHeader($tpl);
        $tpl->show();
    }

    function updateContrDataA()
    {
        global $DB;
        $task_id = $_POST["task_id"];
        $type_id = $_POST["type_id"];
        $agr_id = $_POST["agr_id"];
        $err = array();
        if ($task_id && $type_id && $agr_id) {
            $sql = "UPDATE `gp` SET `type_id`=" . $DB->F($type_id) . ", `agr_id`=" . $DB->F($agr_id) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) $err[] = "error";
        } else {
            $err[] = "error";
        }
        echo sizeof($err) ? "error" : "ok";
        return false;

    }

    function queryStatus()
    {
        global $DB;
        if ($task_id = $_POST['task_id']) {
            $sql = "SELECT s.tag FROM `tasks` AS t LEFT JOIN `task_status` AS s ON t.status_id=s.id WHERE t.id=" . $DB->F($task_id) . ";";
            $status_tag = $DB->getField($sql);
            $ret = $DB->errno() ? "error" : ($status_tag ? $status_tag : "none");
        } else {
            $ret = "error";
        }
        echo $ret;
        return false;
    }

    function vieworderdoc()
    {
        global $DB, $USER;
        if ($id = $_REQUEST["id"]) {
            $ticket = new GlobalProblemTask($id);
            $sql = "SELECT gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.task_id=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->num_rows()) {
                list($c_date, $startTime, $user) = $DB->fetch();
                $DB->free();
            }
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $tpl->loadTemplatefile($USER->getTemplate() . "/orderdoc.tmpl.htm");
            $tpl->setVariable("TASK_ID", $id);
            $tpl->setVariable("OD_NUM", $id . " / " . $ticket->getTaskOpNum());
            $tpl->setVariable("OD_CONTRAGENT", $ticket->getContrName());
            $tpl->setVariable("OD_WTYPE", $ticket->getTypeName());
            //$phnes = $ticket->getPhones();
            $tpl->setVariable("OD_CLNT_TEL", "&mdash;");
            $scname = adm_sc_plugin::getSC($ticket->getSCId());
            $tpl->setVariable("OD_SC", $scname["title"]);
            $tpl->setVariable("OD_AREA", "&mdash;");
            //$tpl->setVariable("OD_EMPL", @$user ? $user : "---");
            //$ods = $this->getOds($ticket->getDomId());
            $tpl->setVariable("OD_ODS", "&mdash;");
            $tpl->setVariable("OD_CREATEDATE", rudate("d M Y H:i"));
            $tpl->setVariable("OD_ADDINFO", $ticket->getAddInfo());
            $tpl->setVariable("OD_EMPL", $ticket->getUsers("<br />"));
//            $addrList = $ticket->getAddrList();
//            foreach ($addrList as $item) {
//                $addr .= $DB->getField("SELECT `op_string` FROM `gp_addr` WHERE `id`=" . $DB->F($item["id"]) . ";") . "<br />";
//            }
            $addrList = '';
            $rows = $this->getConn()
                ->executeQuery('SELECT la.full_address FROM list_addr la JOIN gp_addr ga ON ga.dom_id = la.id AND ga.task_id = :id', ['id' => $id])
                ->fetchAll();
            foreach ($rows as $row) {
                $addrList .= $row['full_address'] . "<br />";
            }
            $tpl->setVariable("OD_ADDR", $addrList);
            $tpl->setVariable("OD_INFO", $ticket->getTaskTitle());
            $agr_id = $ticket->getAgrId(false);
            if ($agr_id) {
                if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getAgrById")) {
                    $kontr = new kontr_plugin();
                    $agr_contacts = $kontr->getAgrById($agr_id);
                    if ($agr_contacts) {
                        $tpl->setVariable("OD_CONTR_ACCESS", $agr_contacts['access_contact'] ? $agr_contacts['access_contact'] : "<strong>-</strong>");
                        $tpl->setVariable("OD_CONTR_TP", $agr_contacts['tp_contact'] ? $agr_contacts['tp_contact'] : "<strong>-</strong>");
                    } else {
                        $tpl->setVariable("OD_CONTR_ACCESS", "<strong>ошибка</strong>");
                        $tpl->setVariable("OD_CONTR_TP", "<strong>ошибка</strong>");
                    }
                    $tpl->setVariable("OD_CNLT_AGRNUM", $agr_contacts["number"] . " от " . rudate("d M Y", $agr_contacts["agr_date"]));
                }
            } else {
                $tpl->setVariable("OD_CONTR_ACCESS", "<strong>-</strong>");
                $tpl->setVariable("OD_CONTR_TP", "<strong>-</strong>");
            }
            if (isset($_REQUEST['printcomments'])) {
                $tpl->setCurrentBlock("print-comments");
                //$tpl->setVariable("C_OD_NUM", $id." / ".$ticket->getClntTNum());
                $tpl->setVariable("C_COMMENTS_LIST", $ticket->getCommentsPrint());
                $tpl->parse("print-comments");
            }
            UIHeader($tpl);
            $tpl->show();
        } else {
            UIError("Ошибка! Не указан идентификатор заявки для печати Заказ-наряда!");
        }
    }

    function getwtypelist()
    {
        global $DB;
        $contr_id = $_POST['contr_id'];
        $type = $_POST['type'];
        if ($contr_id === FALSE || $type === FALSE) {
            echo "error";
            return;
        }
        $sql = "SELECT wt.id, wt.title FROM `list_contr_agr` AS ca
                LEFT JOIN `link_agr_wtypes` AS law ON law.agr_id=ca.id
                LEFT JOIN `task_types` AS wt ON wt.id=law.wtype_id WHERE ca.contr_id=" . $DB->F($contr_id) . " AND wt.title IS NOT NULL AND wt.plugin_uid=" . $DB->F($type) . " ORDER BY wt.title;";

        if (array2options($DB->getCell2($sql))) echo array2options($DB->getCell2($sql), $_POST['sel_id']); else echo "<option value=\"\">-- нет видов работ --</option>";

    }

    function getagrlist()
    {
        global $DB;
        $contr_id = $_POST['contr_id'];
        $wtype_id = $_POST['wtype_id'];
        if ($contr_id === FALSE || $wtype_id === FALSE) {
            echo "error";
            return;
        }
        $sql = "SELECT `id`, CONCAT(`number`, ' от ', `agr_date`) AS agr FROM `list_contr_agr` WHERE `contr_id`=" . $DB->F($contr_id) . " AND `id` IN (SELECT `agr_id` FROM `link_agr_wtypes` WHERE `wtype_id`=" . $DB->F($wtype_id) . ");";

        if (array2options($DB->getCell2($sql))) echo array2options($DB->getCell2($sql), $_POST['sel_id']); else echo "<option value=\"\">-- нет договоров --</option>";
    }


    function newticket()
    {
        global $DB, $USER;
        if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
            UIError("Вам запрещено создавать заявки!", "Отказано в доступе!");
        }
        $LKUser = adm_users_plugin::isLKUser($USER->getId());

        if ($LKUser) {
            $LKContrID = adm_users_plugin::getLKUserContrID($USER->getId());
            $contr_id = $LKContrID;
            $type = "gp";

            $sql = "SELECT wt.id, wt.title FROM `list_contr_agr` AS ca
                    LEFT JOIN `link_agr_wtypes` AS law ON law.agr_id=ca.id
                    LEFT JOIN `task_types` AS wt ON wt.id=law.wtype_id WHERE ca.contr_id=" . $DB->F($contr_id) . " AND wt.title IS NOT NULL AND wt.plugin_uid=" . $DB->F($type) . " ORDER BY wt.title;";
            $r = $DB->getCell($sql);
            if (!$r) UIError("У Вас нет активных договоров в WF для создания заявок такого типа!");

            $tpl_add = "_lk";
        } else {
            $tpl_add = "";
        }
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("new.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/new$tpl_add.tmpl.htm");
        else
            $tpl->loadTemplatefile("new$tpl_add.tmpl.htm");
        $tpl->setVariable("LK_CONTR_ID", $LKContrID);
        $tpl->setVariable("LK_CONTR_NAME", kontr_plugin::getByID($LKContrID));
        $tpl->setVariable('TYPE_OPTIONS', $this->getTypeOptions());
        $tpl->setVariable('CNT_OPTIONS', $this->getContrOptions());
        /*if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getStreetList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_STREET", $addr->getStreetList(getcfg('default_region')));
        } */
        $sc_list = adm_sc_plugin::getScList();
        $tpl->setVariable("SC_OPTIONS", $sc_list);
        //$tpl->setVariable("FILTER_K_REGION", $this->getKRegionList($currentCode));
        //$tpl->setVariable("FILTER_K_AREA", $this->getKAreaList($currentCode));
        //$tpl->setVariable("FILTER_K_CITY", $this->getKCityList($currentCode));
        //$tpl->setVariable("FILTER_K_NP", $this->getKNPList($currentCode));
        //$tpl->setVariable("FILTER_K_STREET", $this->getStreetList($currentCodeStreet));

        UIHeader($tpl);
        $tpl->show();
    }

    function saveticket(Symfony\Component\HttpFoundation\Request $request)
    {
        $err = array();
        if (!$_POST['cnt_id']) {
            $err[] = "Не выбран контрагент";
        }

        if (!$_POST['type_id']) {
            $err[] = "Не выбран вид работ";
        }

        if (sizeof($err)) {
            UIError($err);
        }
        $r = $request->request;
        $contragent = $r->getInt('cnt_id', null);
        $type_id = $r->getInt('type_id', null);
        $agr_id = $r->getInt('agr_id', null);
        $clttnum = $request->get('clnttnum');
        $cr_datetime = $_REQUEST["date_from"] ? date("Y-m-d H:i", strtotime($_REQUEST["date_from"] . " " . $_REQUEST["time_from"])) : null;
        $togss_datetime = $_REQUEST["date_to"] ? date("Y-m-d H:i", strtotime($_REQUEST["date_to"] . " " . $_REQUEST["time_to"])) : null;
        $contr_user = $request->get('clnt_user_name');
        $gp_title = $request->get('gp_title');
        $gp_body = $request->get('addinfo');
        $fcomment = $request->get('cmm');
        $sc_id = $r->getInt('sc_id', null) ?: null;

        /* @var $taskManager \WF\Task\TaskManager */
        $taskManager = $this->getContainer()->get('wf.task.task_manager');

        $comments = [];
        if ($fcomment) {
            $comments[] = (new models\TaskComment())
                ->setAuthor($this->getCurrentUser())
                ->setText($fcomment);
        }

        $task = $taskManager->createTask(ModelTask::GLOBAL_PROBLEM, [
            'comments' => $comments,
            'author_id' => $this->getUserId(),
        ]);

        $taskManager->createTicket($task, [
            'partner_id' => $contragent,
            'task_type_id' => $type_id,
            'agreement_id' => $agr_id,
            'operator_key' => $clttnum,
            'operator_start_dt' => $cr_datetime,
            'operator_gss_dt' => $togss_datetime,
            'author_name' => $contr_user,
            'title' => $gp_title,
            'body' => $gp_body,
            'service_center_id' => $sc_id,
        ]);

        if (!empty($_FILES)) {
            $cm = new comments_plugin();
            $_POST['task_id'] = $task->getId();
            $cm->addcmm(true);
        }
        redirect($this->getLink('viewticket', "task_id=" . $task->getId()));
    }

    function updateticket()
    {
        //Debugger::dump($_POST, 1);
        $ticket = new GlobalProblemTask($_REQUEST['task_id']);
        $err = array();

        if (sizeof($err)) UIError($err);

        $comment = "";
        /*if($ticket->getSCId() != $_POST['sc_id'])
            $comment .= "Сервисный центр: ".$this->getSCName($ticket->getSCId())." -> ".$this->getSCName($_POST['sc_id'])."\r\n";
        /*if($ticket->getPod(false) != $_POST['pod'])
            $comment .= "Подъезд: ".$ticket->getPod(false)." -> ".$_POST['pod']."\r\n";
        if($ticket->getEtazh(false) != $_POST['etazh'])
            $comment .= "Этаж: ".$ticket->getEtazh(false)." -> ".$_POST['etazh']."\r\n";
        if($ticket->getKv(false) != $_POST['kv'])
            $comment .= "Квартира: ".$ticket->getKv(false)." -> ".$_POST['kv']."\r\n";
        if($ticket->getDomofon(false) != $_POST['domofon'])
            $comment .= "Код домофона: ".$ticket->getDomofon(false)." -> ".$_POST['domofon']."\r\n";*/
        /*if (isset($_POST["clnttype"])) {
            if($ticket->getClientType() != $_POST["clnttype"]) {
                $comment .= "Тип клиента: ".($ticket->getClientType() == 1 ? "Физ. лицо" : "Юр. лицо")." -> ".($_POST["clnttype"] == 1 ? "Физ. лицо" : "Юр. лицо")."\r\n";
            }
        }
        if($ticket->getClientType()==\classes\User::CLIENT_TYPE_YUR && $ticket->getOrgName(false) != $_POST['org_name'])
            $comment .= "Название организации: ".$ticket->getOrgName(false)." -> ".$_POST['org_name']."\r\n";
        if($ticket->getFio(false) != $_POST['fio'])
            $comment .= "ФИО: ".$ticket->getFio(false)." -> ".$_POST['fio']."\r\n";
        $phones = $ticket->getPhones();
        if($phones[0] != preg_replace("/[^0-9]/", "", $_POST['phone1']))
            $comment .= "Телефон (1): {$phones[0]} -> {$_POST['phone1']}\r\n";
        if($phones[1] != preg_replace("/[^0-9]/", "", $_POST['phone2']))
            $comment .= "Телефон (2): {$phones[1]} -> {$_POST['phone2']}\r\n";
        if($ticket->getSwIp(false) != $_POST['swip'])
            $comment .= "IP-адрес: ".$ticket->getSwIp(false)." -> ".$_POST['swip']."\r\n";
        if($ticket->getSwPort(false) != $_POST['swport'])
            $comment .= "Порт: ".$ticket->getSwPort(false)." -> ".$_POST['swport']."\r\n";
        if($ticket->getSwPlace(false) != $_POST['swplace'])
            $comment .= "Расположение свитча: ".$ticket->getSwPlace(false)." -> ".$_POST['swplace']."\r\n";
        */

        if ($ticket->getOpDate(false) != $_POST['date_from'])
            $comment .= "Дата открытия у оператора: " . $ticket->getOpDate(false) . " -> " . $_POST['date_from'] . "\r\n";
        if ($ticket->getOpTime(false) != $_POST['time_from'])
            $comment .= "Время открытия у оператора: " . $ticket->getOpTime(false) . " -> " . $_POST['time_from'] . "\r\n";
        if ($ticket->getGssDate(false) != $_POST['date_to'])
            $comment .= "Дата передачи в Горсвязь: " . $ticket->getGssDate(false) . " -> " . $_POST['date_to'] . "\r\n";
        if ($ticket->getGssTime(false) != $_POST['time_to'])
            $comment .= "Время передачи в Горсвязь: " . $ticket->getGssTime(false) . " -> " . $_POST['time_to'] . "\r\n";

        if ($ticket->getPlanDate(false) != $_POST['date_plan'])
            $comment .= "Планируемая дата закрытия: " . $ticket->getPlanDate(false) . " -> " . $_POST['date_plan'] . "\r\n";
        if ($ticket->getPlanTime(false) != $_POST['time_plan'])
            $comment .= "Планируемое время закрытия: " . $ticket->getPlanTime(false) . " -> " . $_POST['time_plan'] . "\r\n";


        if ($ticket->getContrNumber(false) != $_POST['clnttnum'])
            $comment .= "Номер заявки клиента в базе оператора: " . $ticket->getContrNumber(false) . " -> " . $_POST['clnttnum'] . "\r\n";
        if ($ticket->getTaskTitle() != $_POST["gp_title"]) {
            $comment .= "Название аварии: " . $ticket->getTaskTitle() . " -> " . $_POST['gp_title'] . "\r\n";
        }
        if ($ticket->getAddInfo() != $_POST["addinfo"]) {
            $comment .= "Описание аварии: " . $ticket->getAddInfo() . " -> " . $_POST['addinfo'] . "\r\n";
        }
        if ($ticket->getTaskAuthor() != $_POST["clnt_user_name"]) {
            $comment .= "Автор аварии: " . $ticket->getTaskAuthor() . " -> " . $_POST['clnt_user_name'] . "\r\n";
        }
        if ($ticket->getB2b() != $_POST["b2b"]) {
            $comment .= "Поле приоритета b2b: " . $ticket->getB2b() . " -> " . $_POST['b2b'] . "\r\n";
        }
        if ($ticket->getB2c() != $_POST["b2c"]) {
            $comment .= "Поле приоритета b2c: " . $ticket->getB2c() . " -> " . $_POST['b2c'] . "\r\n";
        }

        if ($ticket->getSCId() != $_POST["sc_id"]) {
            $oldSC = adm_sc_plugin::getSC($ticket->getSCId());
            $newSC = adm_sc_plugin::getSC($_POST["sc_id"]);
            $comment .= "СЦ: " . $oldSC["title"] . " -> " . $newSC["title"] . "\r\n";
        }

        /*if (isset($_POST["donedate"]) && preg_match("/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/", $_POST["donedate"])) {
            if ($ticket->getDoneDate() != $_POST["donedate"]) {
                $comment .= "ОБНОВЛЕНА ДАТА ВЫПОЛНЕНИЯ: ".$ticket->getDoneDate()." -> ".$_POST['donedate']."\r\n";
                if (!$ticket->setDoneDate($_POST["donedate"])) UIError("Ошибка обновления ДАТЫ ВЫПОЛНЕНИЯ!");
            }
        } */

        $ticket->setAddInfo($_POST["addinfo"]);
        $ticket->setTaskTitle($_POST["gp_title"]);
        $ticket->setTaskAuthor($_POST["clnt_user_name"]);
        $ticket->setB2b($_POST["b2b"]);
        $ticket->setB2c($_POST["b2c"]);
        $ticket->setSCId($_POST["sc_id"]);
        /*$ticket->setSwIp($_POST["swip"]);
        $ticket->setSwPort($_POST["swport"]);
        $ticket->setSwPlace($_POST["swplace"]);
        $ticket->setClntOpAgr($_POST["clntopagr"]);
        */
        $ticket->setContrNumber($_POST["clnttnum"]);
        $ticket->setOperatorDate(date("Y-m-d", strtotime($_POST["date_from"])) . " " . $_POST["time_from"]);
        if ($_POST["date_plan"] != "" && $_POST["time_plan"] != "")
            $ticket->setPlanDate(date("Y-m-d", strtotime($_POST["date_plan"])) . " " . $_POST["time_plan"]);
        $ticket->setGssDate(date("Y-m-d", strtotime($_POST["date_to"])) . " " . $_POST["time_to"]);
        if ($ticket->updateCommit()) {
            if ($comment) $ticket->addComment($comment, "Обновлены данные");
        }

        global $DB;
        // передача комментария контрагенту начало
        /*if(isset($_POST['cmm_to_contr']) && $_POST['cmm_contr'] && ($contr_id = $ticket->getContrId())) {
            $sql = "SELECT `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=".$DB->F($ticket->getId())." AND `contr_id`=".$DB->F($contr_id);
            if($contr_ticket_id = $DB->getField($sql)) {
                if($contr_id == getcfg('2kom_contr_id')) {
                    try {
                        $client = new SoapClient(getcfg('2kom_soap_url'));
                        $result = $client->UpdateOrder(getcfg('2kom_soap_login'), getcfg('2kom_soap_pass'), $contr_ticket_id, 0, '', $_POST['cmm_contr']);
                        trigger_error("2KOM SOAP Result code {$result['retcode']}: ".$result['retmessage']);
                        if($result['retcode'] != 0) {
                            $err[] = "Ошибка обновления даннх в 2KOM: ".$result['retmessage'];
                        } else {
                            $ticket->addComment($_POST['cmm_contr'], "Отправлен комментарий контрагенту");
                        }
                    } catch(SoapFault $e) {
                        trigger_error("2KOM SOAP Exeption! ".$e->getMessage(), E_USER_WARNING);
                        $err[] = "Ошибка связи с 2KOM: ".$e->getMessage();
                    }
                }
            }
        }
        // передача комментария контрагенту конец
        */
        if (sizeof($err)) UIError($err);

        redirect($this->getLink('viewticket', "task_id=" . $ticket->getId()));
    }

    function createaddr_ajax(Request $request)
    {
        global $DB, $USER;

        $task_id = $_POST["task_id"] ? $_POST["task_id"] : null;
        $addr_id = $_POST["addr_id"] ? $_POST["addr_id"] : null;
        $pod = $_POST["pod"] ? $_POST["pod"] : null;
        $domofon = $_POST["domofon"] ? $_POST["domofon"] : null;
        $order_id = $_POST["n_order_id"] ? $_POST["n_order_id"] : 0;

        $sql = "SELECT count(id) FROM `gp_addr` WHERE `task_id`=" . $DB->F($task_id) . " AND `dom_id`=" . $DB->F($addr_id) . " AND `pod`=" . $DB->F($pod) . ";";
        $cnt = $DB->getField($sql);
        if ($DB->errno()) {
            echo "error";
            return false;
        } else {
            $addr = $DB->getRow("SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($addr_id) . ";", true);

            $sql = "INSERT INTO `gp_addr` (`task_id`, `dom_id`, `pod`, `domofon`, `addr`) VALUES
                 (" . $DB->F($task_id) . ", " . $DB->F($addr_id) . ", " . $DB->F($pod, true) . ", " . $DB->F($domofon) . ",
                 " . $DB->F($addr['full_address']) . ")";


            $DB->query($sql);
            if ($DB->errno()) {
                echo "error";
                return false;
            } else {
                $address = $this->getEm()->find(models\ListAddr::class, $addr['id']);
                $task = $this->getEm()->find(\models\Task::class, $task_id);
                $event = new TaskAddressAddedEvent($task, $address);
                $this->getDispatcher()->dispatch(TaskAddressAddedEvent::NAME, $event);
                $id = $DB->insert_id();
                if ($id) {
                    $rtpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
                    $rtpl->loadTemplatefile($USER->getTemplate() . "/addritem.tmpl.htm");
                    $ires = $DB->getRow("SELECT gpa.id AS gpa, gpa.*, la.* FROM `gp_addr` AS gpa LEFT JOIN `list_addr` AS la ON gpa.dom_id=la.id WHERE gpa.id=" . $DB->F($id) . ";", true);
                    if ($DB->errno()) {
                        echo "error";
                        return false;
                    }
                    if ($ires) {
                        $r1["id"] = $ires["gpa"];
                        $r1["dom_id"] = $ires["dom_id"];
                        $r1["pod"] = $ires["pod"];
                        $r1["domofon"] = $ires["domofon"];
                        $r1["op_string"] = $ires["full_address"];
                        $r1["sc_id"] = $ires["sc_id"];
                        $r1["dom"] = $ires["name"];
                        $r1["area_id"] = $ires["area_id"];
                        $r1["ods_id"] = intval($ires["ods_id"]);
                        //echo $r["kladrcode"];
                        $r2 = addr_interface_plugin::decodeFullKLADRPartial($ires["kladrcode"]);
                        $r1["city"] = $r2["city"];
                        $r1["street"] = $r2["street"];
                        $r1["area"] = $ires["area_id"] ? adm_areas_plugin::getArea($ires["area_id"]) : "&mdash;";
                        $sc = $ires["sc_id"] ? adm_sc_plugin::getSC($ires["sc_id"]) : false;
                        $r1["sc"] = $sc ? $sc["title"] : "&mdash;";
                        $ods = $r1["ods_id"] ? adm_ods_plugin::getODSById($r1["ods_id"]) : false;
                        $r1["ods"] = $ods ? $ods["title"] : "&mdash;";
                        $rq["order_id"] = $ires["order_id"];
                        $item = $r1;
                    }
                    if ($item) {
                        if ($item["dom_id"]) {
                            $rtpl->setCurrentBlock("row");
                            $rtpl->setVariable("ROW_ID", $item["id"]);
                            $rtpl->setVariable("AREA", $item["area"]);
                            $rtpl->setVariable("FULL_ADDRESS", $addr['full_address']);
                            $rtpl->setVariable("ORDER_ID", $order_id);
                            $rtpl->setVariable("POD", $item["pod"]);
                            $rtpl->setVariable("HREF", link2("addr_interface/edit?id=" . $item["dom_id"], false));
                            if ($item["sc_id"]) {
                                $rtpl->setCurrentBlock("issc");
                                $rtpl->setVariable("SC", $item["sc"]);
                                $rtpl->setVariable("SC_ID", $item["sc_id"]);
                                $rtpl->parse("issc");
                            } else {
                                $rtpl->setVariable("NOSC", "&mdash;");
                            }
                            if ($item["ods_id"]) {
                                $rtpl->setCurrentBlock("isods");
                                $rtpl->setVariable("ODS", $item["ods"]);
                                $rtpl->setVariable("ODS_ID", $item["ods_id"]);
                                $rtpl->parse("isods");
                            } else {
                                $rtpl->setVariable("NOODS", "&mdash;");
                            }
                            if ($USER->checkAccess($this->getUID(), User::ACCESS_WRITE)) {
                                $rtpl->setCurrentBlock("deleteaddritem");
                                $rtpl->setVariable("ADDRID", $item["id"]);
                                $rtpl->parse("deleteaddritem");
                            }
                            $rtpl->parse("row");

                        } else {
                            $rtpl->setCurrentBlock("row-orig");
                            $rtpl->setVariable("ORIGNAME", $item["op_string"]);
                            if ($USER->checkAccess($this->getUID(), User::ACCESS_WRITE)) {
                                $rtpl->setCurrentBlock("deleteaddritem1");
                                $rtpl->setVariable("ADDRID1", $item["id"]);
                                $rtpl->parse("deleteaddritem1");
                            }
                            $rtpl->parse("row-orig");
                        }
                        echo $rtpl->get();
                    } else {
                        echo "error";
                    }
                } else {
                    echo "error";
                    return false;
                }
            }
        }
        return false;
    }

    function deleteaddr_ajax()
    {
        global $DB;
        $id = $_POST["id"];
        if (!$id) {
            echo "error";
            return false;
        }
        $sql = "DELETE FROM `gp_addr` WHERE `id`=" . $DB->F($id) . " LIMIT 1;";
        $DB->query($sql);
        if ($DB->errno()) {
            echo "error";
            return false;
        } else {
            echo "ok";
            return false;
        }

    }

    static function getContrOptions($sel_id = 0)
    {
        global $DB, $USER;
        $addsql = "";
        if (adm_users_plugin::isLKUser($USER->getId())) {
            $cnt_id = adm_users_plugin::getLKUserContrID($USER->getId());
            $addsql = "AND `id`=" . $DB->F($cnt_id);
        }
        $sql = "SELECT `id`, concat(`contr_title`,' (',`official_title`,')') as val  FROM `list_contr` WHERE `contr_type4` $addsql ORDER BY `contr_title` ";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getSCOptions($sel_id = 0)
    {
        global $DB, $USER;
        $sql = "SELECT
            s.id, s.title
            FROM list_sc s,
            user_sc_access usa
            WHERE s.id = usa.sc_id
              AND usa.user_id = " . $USER->getId() . "
            ORDER BY s.title";
        $result = $DB->getCell2($sql);
        $result[-1] = '-- Все сервисные центры --';
        $result[0] = '-- Не указан --';

        ksort($result);

        return array2options($result, $sel_id, false);
    }

    static function getODSOptions($sel_id = 0)
    {
        global $DB;
        $sql = "SELECT `id`, `title` FROM `list_ods` ORDER BY `title`";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getEmplOptions($sel_id = 0)
    {
        global $DB;
        $sql_add = "";
        if ($isChiefTech = User::isChiefTech(\wf::getUserId())) {
            $sql_add = " AND user.id IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($isChiefTech) . ")";
        }

        $sql = "SELECT user.id, user.fio FROM `list_empl` AS empl LEFT JOIN `users` AS user ON user.id=empl.user_id WHERE user.active='1' " . $sql_add . " ORDER BY `user`.fio";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getContrName($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT concat(`contr_title`,' (',`official_title`,')') AS val  FROM `list_contr` WHERE `id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function getContrID($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT `cnt_id` FROM `gp` WHERE `task_id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function getTypeID($id, $escape = true)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `type_id` FROM `gp` WHERE `task_id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }


    static function getTypeOptions($sel_id = 0)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`='gp' ORDER BY `title`";
        return array2options(['' => '- Все -', '-1' => 'He выбран'] + $DB->getCell2($sql), $sel_id, false);
    }

    static function getTypeName($id, $escape = true)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `title` FROM `task_types` WHERE `plugin_uid`=" . $DB->F($plugin_uid) . " AND `id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }


    static function statusCallbackGet($task_id, $status)
    {
        global $DB, $USER;
        //die("asdf");
        if ((false !== strpos($status['tag'], 'cmmcontr'))) {
            $t = new GlobalProblemTask($task_id);
            //$sql = "SELECT `contr_id`, `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=".$DB->F($task_id);
            //list($contr_id, $contr_ticket_id) = $DB->getRow($sql);
            $contr_ticket_id = $t->getContrId();
            $contr_id = getcfg('nbn_contr_id');
            if ($contr_id == $contr_ticket_id) {
                if ($USER->checkAccess('gp2contr', User::ACCESS_WRITE) && $contr_ticket_id) return $status['name'];
                else return false;
            }
        }
        if ((false !== strpos($status['tag'], 'otlozh'))) {
            return $status['name'] . "</label> до <input type='text' name='otlozh_date' class='datetime_input'><label>";
        }
        if ((false !== strpos($status['tag'], 'done'))) {
            $tlist = adm_ticket_gptypes_plugin::getTypeOptions();
            $stlist = adm_ticket_gptypes_plugin::getSubTypeOptions();
            if ($tlist && $stlist) {
                return $status['name'] . "</label> - тип: <select style='width:150px;' name='gp_type'>" . $tlist . "</select> подтип: <select style='width:150px;' name='gp_subtype'>" . $stlist . "</select>";
            } else {
                $err[] = "Пустой справочник типов или подтипов для ГП";

            }
        }
        $err = array();
        $plugin_uid = basename(__FILE__, '.php');
        if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejectReasonsList")) {
            $rej = new adm_rejects_plugin();
            if ($status['tag'] != '') {
                $rejReasons = $rej->getRejectReasonsList(false, $plugin_uid, $status['tag']);
                if ($rejReasons !== FALSE) {
                    return $status['name'] . "</label> <select style=\"margin-left: 10px;width:150px;\" name='reject_" . $status["tag"] . "_id'>" . $rejReasons . "</select><label>";
                }
            }
        } else {
            $err[] = 'Отсутствует класс Причин отказа!';

        }
        return sizeof($err) ? $err : $status['name'];
    }

    static function statusCallbackSet($task_id, &$status, &$text, &$tag)
    {

        //die("asdfasdf");
        global $DB, $USER;

        //die($status['tag']);
        //die("asdfasdf");
        if (false !== strpos($status['tag'], 'cmmcontr')) {

            //$sql = "SELECT `contr_id`, `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=".$DB->F($task_id);
            //list($contr_id, $contr_ticket_id) = $DB->getRow($sql);
            $gp = new GlobalProblemTask($task_id);
            $contr_id = $gp->getContrId();
            if ($contr_id == getcfg('nbn_slots_contr_id')) {

                $domtree = new DOMDocument('1.0', 'UTF-8');

                /* create the root element of the xml tree */
                $xmlRoot = $domtree->createElement("xml");
                /* append it to the document created */
                $xmlRoot = $domtree->appendChild($xmlRoot);

                $currentTrack = $domtree->createElement("comment");
                $currentTrack = $xmlRoot->appendChild($currentTrack);

                /* you should enclose the following two lines in a cicle */
                $currentTrack->appendChild($domtree->createElement('key', $gp->getContrNumber()));
                $currentTrack->appendChild($domtree->createElement('message', $text));

                $currentTrack->appendChild($domtree->createElement('creator', $USER->getFio()));

                /* get the xml printed */

                $xml = $domtree->saveXML();
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, getcfg('nbn_gondor_url'));
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "xml=" . $xml);
                $content = curl_exec($ch);
                curl_close($ch);

                $xml = simplexml_load_string($content);

                if (@$xml !== FALSE && @$xml->id != '') {
                    $tag = "Комментарий успешно отправлен контрагенту";
                } else {
                    $tag = "Отправка комментария завершена с ошибкой. ";
                }
            }
            return true;
        }

        if (false !== strpos($status['tag'], 'otlozh')) {
            if (!$_POST['otlozh_date']) $err[] = "Нет даты, до которой отложить";
            else {
                $task = new GlobalProblemTask($task_id);
                if ($task->setDateFn($_POST['otlozh_date'])) $tag .= " до {$_POST['otlozh_date']}";
            }
        }

        if (false !== strpos($status['tag'], 'donegp')) {
            if (!$_POST['gp_type'] || !$_POST['gp_subtype']) $err[] = "Не указан тип/подтип заявки";
            else {
                $sql = "UPDATE `gp` SET `gptype_id`=" . $DB->F($_POST["gp_type"]) . ", `gpsubtype_id`=" . $DB->F($_POST["gp_subtype"]) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
                $r = $DB->query($sql);
                $DB->free($r);
                $tag .= " Тип: <strong>" . adm_ticket_gptypes_plugin::getType($_POST["gp_type"]) . "</strong>, подтип: <strong>" . adm_ticket_gptypes_plugin::getSubType($_POST["gp_subtype"]) . "</strong>";
            }
        }

        $reject_id = $_POST["reject_" . $status["tag"] . "_id"] ? $_POST["reject_" . $status["tag"] . "_id"] : false;
        if ($reject_id) {
            $sql = "UPDATE `tickets` SET `reject_id`=" . $DB->F($reject_id) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) return false;
            if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejReasonTitle")) {
                $rej = new adm_rejects_plugin();
                $tag .= " - " . $rej->getRejReasonTitle($reject_id);
                //return true;
            } else return false;
        } else {
            $sql = "UPDATE `tickets` SET `reject_id`=" . $DB->F("0") . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) return false;
            //return true;
        }
        if ($_SERVER["HTTP_HOST"] != "gss.local") {
            // передача статусов контрагенту начало
            $sql = "SELECT `contr_id`, `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=" . $DB->F($task_id);
            list($contr_id, $contr_ticket_id) = $DB->getRow($sql);
            trigger_error("Task_id='$task_id', contr_id='$contr_id', contr_ticket_id='$contr_ticket_id'");
            if ($contr_id == getcfg('2kom_contr_id') && preg_match("/2kom/", $status['tag'])) { // заявка от 2KOM + статус для них
                try {
                    $client = new SoapClient(getcfg('2kom_soap_url'));
                    $result = $client->UpdateOrder(getcfg('2kom_soap_login'), getcfg('2kom_soap_pass'), $contr_ticket_id, $status['id'], $status['name'], "Изменен статус");
                    trigger_error("2KOM SOAP Result code {$result['retcode']}: " . $result['retmessage']);
                    if ($result['retcode'] != 0) {
                        $err[] = "Ошибка обновления даннх в 2KOM: " . $result['retmessage'];
                    } else {
                        $tag .= "; статус отправлен контрагенту";
                    }
                } catch (SoapFault $e) {
                    trigger_error("2KOM SOAP Exeption! " . $e->getMessage(), E_USER_WARNING);
                    $err[] = "Ошибка связи с 2KOM: " . $e->getMessage();
                }
            }
        }
        if (sizeof($err)) {
            $text .= "\r\n\r\nВНИМАНИЕ:\r\n" . implode("\r\n", $err);
            $err = array();
        }
        // передача статусов контрагенту конец

        return sizeof($err) ? $err : true;
    }


}
