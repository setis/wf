<?php 
/**
 * Plugin Header
 * @author kblp
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "ГП";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "ГП (Линейные Аварии)";
    $PLUGINS[$plugin_uid]['events']['viewticket'] = "Просмотр ГП";
    $PLUGINS[$plugin_uid]['events']['vieworderdoc'] = "Печать заказ-наряда";
    $PLUGINS[$plugin_uid]['events']['viewlist_export'] = "Линейные Аварии. Выгрузка.";
    $PLUGINS[$plugin_uid]['events']['queryStatus'] = "Запрос текущего статуса ГП";
    $PLUGINS[$plugin_uid]['events']['getwtypelist'] = "Список видов работ";
    $PLUGINS[$plugin_uid]['events']['getagrlist'] = "Список договоров";
    $PLUGINS[$plugin_uid]['events']['checkTaskStatusA'] = "Проверка статуса заявки";
    $PLUGINS[$plugin_uid]['events']['getCurrentCalcA'] = "Текущий расчет ГП";
    
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['newticket'] = "Новая ГП (Линейная авария)";
    $PLUGINS[$plugin_uid]['events']['saveticket'] = "Сохранение ГП";
    $PLUGINS[$plugin_uid]['events']['updateticket'] = "Обновление ГП";
    $PLUGINS[$plugin_uid]['events']['deleteticket'] = "Удаление ГП";
    $PLUGINS[$plugin_uid]['events']['updateContrDataA'] = "Изменение Контрагента, договора и вида работ";
    $PLUGINS[$plugin_uid]['events']['createaddr_ajax'] = "Добавление адреса";
    $PLUGINS[$plugin_uid]['events']['deleteaddr_ajax'] = "Удаление адреса";
    $PLUGINS[$plugin_uid]['events']['movetosmr'] = "Передача в СМР";
    $PLUGINS[$plugin_uid]['events']['calcticket'] = "Сохранение списания ТМЦ по ГП";
    $PLUGINS[$plugin_uid]['events']['return_tmc_from_ticktet'] = "Возврат ТМЦ по ГП";
    $PLUGINS[$plugin_uid]['events']['delete_returned_tmc_from_ticket'] = "Откат (отмена списания) ТМЦ по ГП";
}


$plugin_uid = "calc_ticket_gp";
$PLUGINS[$plugin_uid]['name'] = "ГП. Расчет заявок";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "edit_contr_gp_ticket";
$PLUGINS[$plugin_uid]['name'] = "ГП. Изменение вида работ и договора";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['editcontr'] = "Редактор контрагента";
}

$plugin_uid = 'gp2contr';
$PLUGINS[$plugin_uid]['name'] = "ГП. Комментарии контрагенту";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'rooteditgpticket';
$PLUGINS[$plugin_uid]['name'] = "ГП. Редактирование данных в заявках в любых статусах";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'scgpfullaccess';
$PLUGINS[$plugin_uid]['name'] = "ГП. Полный доступ ко всем СЦ";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'deletegp';
$PLUGINS[$plugin_uid]['name'] = "ГП. Удаление заявки";
$PLUGINS[$plugin_uid]['hidden'] = true;


?>