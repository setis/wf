<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use classes\tickets\ServiceTicket;
use classes\User;


require_once(dirname(__FILE__)."/../services/services.php");

class reports_f_skp_mr_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function getCurrentTNumAndAmmount_ajax() {
        global $DB, $USER;
        $ticket = $_REQUEST["id"];
        if (!$ticket) $ret["error"] = "error";
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;            
        }
        $t = new ServiceTicket($ticket);
        $ret["ammount"] = $t->getOrientPrice();
        $ret["docnum"] = $t->getTicketDocNum();
        $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;
    }
    
    function main() {
        global $DB, $USER;
        $fintypes = array("1"=>"Рассчитанные (Деньги приняты)", "2"=>"Не рассчитанные (Деньги не приняты)");
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $_REQUEST["skp_mr_datefrom"] = $this->getCookie("skp_mr_datefrom", @$_REQUEST["skp_mr_datefrom"]);
        $_REQUEST["skp_mr_dateto"] = $this->getCookie("skp_mr_dateto", @$_REQUEST["skp_mr_dateto"]);
        $sc_id = $USER->isChiefTech($USER->getId()) ? $USER->isChiefTech($USER->getId()) : false;
        if (!$sc_id && !User::isTech($USER->getId())) {
            $_REQUEST["scortech"] = $this->getCookie("scortech", $_REQUEST["scortech"]);
            if ($_REQUEST["scortech"] == "tech") {
                $_REQUEST["tech_id"] = $this->getCookie("tech_id", $_REQUEST["tech_id"]);            
            } else {
                $_REQUEST["sc_id"] = $this->getCookie("sc_id", $_REQUEST["sc_id"]);
                
            }
        } else {
            $_REQUEST["tech_id"] = $this->getCookie("tech_id", $_REQUEST["tech_id"]);            
        }
        $_REQUEST["filter_status_id"] = $this->getCookie('filter_status_id', $_REQUEST["filter_status_id"]);
        $_REQUEST["date_type"] = $this->getCookie("date_type", $_REQUEST["date_type"]);
        $_REQUEST["filter_wtype_id"] = $this->getCookie("filter_wtype_id", $_REQUEST["filter_wtype_id"]);
        $_REQUEST["createreport"] = $this->getCookie("createreport", $_REQUEST["createreport"]);
        $_REQUEST["cnt_id"] = $this->getCookie("cnt_id", $_REQUEST["cnt_id"]);
        
        //$_REQUEST["onlyinreport"] = $this->getCookie("onlyinreport", $_REQUEST["onlyinreport"]);
        
         if (isset($_POST["createreport"]) || isset($_POST["createprintversion"]) || isset($_POST["createxlsversion"])) {
            $_REQUEST["onlyinreport"] = $_REQUEST["onlyinreport"] == "on" ? "on" : "off";
            $_REQUEST["onlydsoncard"] = $_REQUEST["onlydsoncard"] == "on" ? "on" : "off";
        } else {
            $_REQUEST["onlyinreport"] = $this->getCookie("onlyinreport", $_REQUEST["onlyinreport"]);
            $_REQUEST["onlydsoncard"] = $this->getCookie("onlydsoncard", $_REQUEST["onlydsoncard"]);
            
        }
        
        //die($_REQUEST["onlyinreport"]);
        $dateParams = array(0 => "Дата создания", 1 => "Дата выполнения", 2=>"Дата изменения", 3=>"Дата принятия ДС");
        if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
            $cntr = new kontr_plugin();
        }
        $contrCount = $cntr->getListSer();
                    
        if (!isset($_REQUEST['createprintversion']) && !isset($_REQUEST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_SKP_MONEY", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListSer(@$_REQUEST["cnt_id"]));
            }
            $tpl->setVariable("DATE_FROM", @$_REQUEST['skp_mr_datefrom'] ? @$_REQUEST['skp_mr_datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_REQUEST['skp_mr_dateto'] ? @$_REQUEST['skp_mr_dateto'] : date("d.m.Y"));

            if ($USER->isChiefTech($USER->getId())){
                $sc_id = $USER->isChiefTech($USER->getId()) ? $USER->isChiefTechAllReturn($USER->getId()) : false;
                $tech_list = adm_empl_plugin::getTechListSKP_MultipleSC($_REQUEST["tech_id"], $sc_id);
            }
            else {
                $tech_list = adm_empl_plugin::getTechListSKP($_REQUEST["tech_id"], false);
            }

            if (!$sc_id && !User::isTech($USER->getId())) {
                $tpl->setCurrentBlock("show_sot");
                $tpl->setVariable("SOT_TECH", $_REQUEST["scortech"] == "tech" ? "checked='checked'": ($_REQUEST["scortech"] == "" ? "checked='checked'" : " "));
                $tpl->parse("show_sot");
                $tpl->setCurrentBlock("notscchief");
                $tpl->setVariable("SOT_SC", $_REQUEST["scortech"] == "sc" ? "checked='checked'":" ");
                $tpl->setVariable("FILTER_SC_OPTIONS", array2options(adm_sc_plugin::getSCList2Col(), $_REQUEST["sc_id"]));
                $tpl->parse("notscchief");
            }
            $tech_list = $tech_list ? $tech_list : '<option>-- нет --</option>';
            $tpl->setVariable("FILTER_TECH_OPTIONS", $tech_list);
            $tpl->setVariable('FILTER_STATUS_OPTIONS', array2options($fintypes, $_REQUEST['filter_status_id']));
            $tpl->setVariable("FILTER_DATE_PARAMS", array2options($dateParams, $_REQUEST["date_type"]));
            $tpl->setVariable("FILTER_WTYPE_OPTIONS", array2options(adm_ticktypes_plugin::getList("services"), $_REQUEST["filter_wtype_id"]));
            
            $tpl->setVariable('SHOWONLYINREPORTS', $_REQUEST["onlyinreport"] == "on" ? "checked='checked'" : "");
            $tpl->setVariable('SHOWONLYDSONCARD', $_REQUEST["onlydsoncard"] == "on" ? "checked='checked'" : "");
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $showAccAccept = (@$_REQUEST["createreport"] && !(@$_REQUEST["createprintversion"] && @$_REQUEST["createxlsversion"]) && $_REQUEST['onlyinreport'] == "on") ? true : false;
            $showAccAcceptinQuery = $_REQUEST['onlyinreport'] == "on" ? true : false;
            $showds = $_REQUEST['onlydsoncard'] == "on" ? true : false;
            
            if ($showAccAccept) {
                $rtpl->touchBlock("checkcol-th");
            }
            $showcard = $USER->checkAccess("allow-card", User::ACCESS_WRITE);
            if ($showcard) {
                
                //$rtpl->touchBlock("allow-cards");
            }
            if ($showcard) {
                $rtpl->touchBlock("cc-th");
            }
            if ($showAccAccept) {
                if ($showcard) {
                    $cs = 7;
                    $rtpl->setVariable("COLSPAN_DEF", 7);
                } else {
                    $rtpl->setVariable("COLSPAN_DEF", 6); 
                    $cs = 6;
                }
            } else {
                if ($showcard) {
                    $cs = 6;
                    $rtpl->setVariable("COLSPAN_DEF", 6);
                } else {
                    $rtpl->setVariable("COLSPAN_DEF", 5);                 
                    $cs = 5;
                }
            }
            if ($showAccAcceptinQuery) {
                $rtpl->setCurrentBlock("footer-accsel");
                $rtpl->setVariable("COLSPAN_DEF1", $cs);  
                if ($showcard) {
                    //die("asf");
                    $rtpl->touchBlock("allowcrd");
                    
                }  
                if ($USER->checkAccess("allow-acc-accept", User::ACCESS_WRITE) && $showAccAcceptinQuery) {
                    $rtpl->touchBlock("allow-acc-accept");
                }
                $rtpl->parse("footer-accsel");
            }
            
            
            $rtpl->setVariable("SORTTYPE", $dateParams[$_REQUEST["date_type"]]);
            $rtpl->setVariable("DATEFROM", $_REQUEST['skp_mr_datefrom']);
            $rtpl->setVariable("DATETO", $_REQUEST['skp_mr_dateto']);
            //if ($_REQUEST["tech_id"]!=0 && $usr = adm_users_plugin::getUser($_REQUEST["tech_id"])) {
            $tech = adm_users_plugin::getUser($_REQUEST["tech_id"]);
            if ($_REQUEST["scortech"] == "sc") {
                $rtpl->setVariable("TECHHIDE", 'hidden');
                $sc = adm_sc_plugin::getSC($_REQUEST["sc_id"]);
                $rtpl->setVariable("SCNAME", $sc["title"]);
            } else {
                $rtpl->setVariable("TECHNAME", $tech['fio']);
                $rtpl->setVariable("SCHIDE", 'hidden');
            }
            $techfio = $tech["fio"];
            //$rtpl->setVariable("REP_TECH", $usr['fio']);
            $datefrom = substr($_REQUEST['skp_mr_datefrom'], 6,4)."-".substr($_REQUEST['skp_mr_datefrom'], 3,2)."-".substr($_REQUEST['skp_mr_datefrom'], 0,2);
            $dateto = substr($_REQUEST['skp_mr_dateto'], 6,4)."-".substr($_REQUEST['skp_mr_dateto'], 3,2)."-".substr($_REQUEST['skp_mr_dateto'], 0,2);
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "services");
            $countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
            $acceptedStatus = adm_statuses_plugin::getStatusByTag("acc_accepted", "services");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "services");
            if ($_REQUEST["filter_status_id"]!=0) {
                // filter payed tickets here
                if ($_REQUEST["filter_status_id"] == 1) 
                    $filter .= " AND tick.inmoney > 0 ";
                else 
                    $filter .= " AND (tick.inmoney is null or tick.inmoney = 0)";
            }
            if ($_REQUEST["date_type"] == 1) {
                $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=".$DB->F($doneStatus["id"])." AND `datetime` >=".$DB->F(date("Y-m-d", strtotime($_REQUEST['skp_mr_datefrom'])))." AND `datetime` <=".$DB->F(date("Y-m-d", strtotime($_REQUEST['skp_mr_dateto'])))."))";
            } else {
                if ($_REQUEST["date_type"] == 2) {
                    $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE (`datetime` >=".$DB->F(date("Y-m-d", strtotime($_REQUEST['skp_mr_datefrom'])))." AND `datetime` <=".$DB->F(date("Y-m-d", strtotime($_REQUEST['skp_mr_dateto'])))."))";
                } else {
                    if ($_REQUEST["date_type"] == 3) {
                        $filter .= " AND tick.inmoneydate >=".$DB->F(date("Y-m-d", strtotime($_REQUEST['skp_mr_datefrom']))) . " AND tick.inmoneydate <=".$DB->F(date("Y-m-d", strtotime($_REQUEST['skp_mr_dateto'])));
                    } else {
                        $filter .= " AND t.date_reg >=".$DB->F(date("Y-m-d", strtotime($_REQUEST['skp_mr_datefrom']))) . " AND t.date_reg <=".$DB->F(date("Y-m-d", strtotime($_REQUEST['skp_mr_dateto'])));
                    }
                }
            }
             
             
            if ($_REQUEST["tech_id"] > 0) {
                $filter .= " AND g.empl_id=".$DB->F($_REQUEST["tech_id"])." ";
            } else {
                
                if ($_REQUEST["sc_id"])
                    $filter .= " AND g.empl_id IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=".$DB->F($_REQUEST["sc_id"]).") ";
                
            }
            if ($_REQUEST['filter_wtype_id']) {                
                $filter .= " AND tick.task_type=".$DB->F($_REQUEST["filter_wtype_id"])." ";
            }
            
            
            if (count($_REQUEST['cnt_id']) != count($contrCount) && count($_REQUEST['cnt_id'])>0) {
                $filter .= " AND tick.cnt_id IN (".preg_replace("/,$/", "", implode(",", $_REQUEST["cnt_id"])).") ";
            }
            
            /*if ($_REQUEST['cnt_id']!=0) {
                $filter .= " AND tick.cnt_id=".$DB->F($_REQUEST["cnt_id"])." ";
            }*/
            if ($showAccAcceptinQuery) {
                if ($showds) {
                    $filter .= " AND tick.iscard AND t.status_id = ".$DB->F($countStatus["id"])." ";                                    
                } else {
                    $filter .= " AND t.status_id = ".$DB->F($countStatus["id"])." ";
                }
            } else {
                if ($showds) {
                    $filter .= " AND tick.iscard ";
                }
                $filter .= " AND (t.status_id = ".$DB->F($acceptedStatus["id"])." OR t.status_id = ".$DB->F($doneStatus["id"])." OR t.status_id = ".$DB->F($countStatus["id"])." OR t.status_id = ".$DB->F($completeStatus["id"]).")";
                
            }
            
            $order = " ORDER BY g.empl_id DESC, t.date_reg";
            $sql = "SELECT
                t.id
            FROM
                `tasks` AS t
                LEFT JOIN
                `gfx` AS g ON g.task_id=t.id
                LEFT JOIN
                tickets AS tick ON tick.task_id=t.id
            WHERE
                t.plugin_uid='services'
                $filter
            GROUP BY
                t.id $order;";

//            d($sql);
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error()." ".$sql);
            $i = 1;
            $totalTICKET = 0;
            $totalTECH = 0;
            $rejectHead = false;
            $predtotalTICKET = 0;
            $predtotalTECH = 0;
            while ($res = $DB->fetch(true)) {
                $ticket = new ServiceTicket($res["id"]);
                $rtpl->setCurrentBlock("rep_row");
                if ($showcard) {
                    $rtpl->setCurrentBlock("checkcol-card-td");
                    $rtpl->setVariable("CC_TASK_ID", $res["id"]);
                    $m = $DB->getField("SELECT `iscard` FROM `tickets` WHERE `task_id`=".$DB->F($res["id"]).";");
                    if ($m) {
                        $rtpl->setVariable("CC_CARDS", "checked='checked'");
                    } else {
                        $rtpl->setVariable("CC_CARDS", "");                        
                    }
                    $rtpl->parse("checkcol-card-td");
                } else {
                    $rtpl->setCurrentBlock("checkcol-card-td");
                    $rtpl->setVariable("CC_CARDS", "style='display:none;'");
                    $rtpl->parse("checkcol-card-td");
                    
                }
                
                if ($showAccAccept) {
                    $rtpl->setCurrentBlock("checkcol-td");
                    $rtpl->setVariable("PP_TASK_ID", $res["id"]);
                    $rtpl->parse("checkcol-td");
                }
                if ($doneStatus) {
                    $sql = "SELECT DATE_FORMAT(`datetime`, '%d.%m.%Y') as doneDate FROM `task_comments` WHERE `task_id`=".$DB->F($res["id"])." AND `status_id`=".$DB->F($doneStatus["id"])." ORDER BY doneDate ASC LIMIT 1;";
                    $doneDate = $DB->getField($sql);
                    if ($doneDate) {
                        $rtpl->setVariable("RR_EXDATE", $doneDate);
                    } else {
                        if ($failStatus || $failOpStatus) {
                            $sql = "SELECT DATE_FORMAT(`datetime`, '%d.%m.%Y') as doneDate FROM `task_comments` WHERE `task_id`=".$DB->F($res["id"])." AND `status_id`=".$DB->F($failStatus["id"])." ORDER BY doneDate ASC LIMIT 1;";
                            $failDate = $DB->getField($sql);
                            if ($failDate) {
                                $rtpl->setVariable("RR_EXDATE", "<p style=\"color:#FF0000;\">".$failDate."</p>");
                            } else {
                                $sql = "SELECT DATE_FORMAT(`datetime`, ".$DB->F("%d.%m.%Y").") as doneDate FROM `task_comments` WHERE `task_id`=".$DB->F($res["id"])." AND `status_id`=".$DB->F($failOpStatus["id"])." ORDER BY doneDate ASC LIMIT 1;";
                                $failDate = $DB->getField($sql);
                                if ($failDate) {
                                    $rtpl->setVariable("RR_EXDATE", "<p style=\"color:#FF0000;\">".$failDate."</p>");
                                } else {
                                    $rtpl->setVariable("RR_EXDATE", "&mdash;");                                     
                                }
                            }
                        } else {
                            $rtpl->setVariable("RR_EXDATE", "&mdash;");
                        }                                                         
                    }
                } else {
                    $rtpl->setVariable("RR_EXDATE", "&mdash;");
                }
                $rtpl->setVariable("RR_AVG_PRICE", $ticket->getOrientPrice() ? $ticket->getOrientPrice() : 0);
                $rtpl->setVariable("RR_PRICE", $ticket->getPrice() ? $ticket->getPrice() : 0);
                $totalAVG += $ticket->getOrientPrice();
                $totalREAL += $ticket->getPrice();

                $rtpl->setVariable("PARTNER_NAME", $ticket->getContrName() ? $ticket->getContrName() : "&mdash;");
                $rtpl->setVariable("RR_WTYPENAME", $ticket->getTypeName() ? $ticket->getTypeName() : "&mdash;");
                $rtpl->setVariable("RR_DOCNUM", $ticket->getTicketDocNum() ? $ticket->getTicketDocNum() : "&mdash;");
                if ( @$_REQUEST["createprintversion"] || @$_REQUEST["createxlsversion"]) {
                    $rtpl->setCurrentBlock("non-print-version");
                    $rtpl->setVariable("PV_RRR_ID", $i);
                    $rtpl->setVariable("PV_RR_INTID", $ticket->getId());
                    $rtpl->setVariable("PV_RR_GSSID", $ticket->getClntTNum());
                    $rtpl->parse("non-print-version");
                } else {
                    $rtpl->setCurrentBlock("print-version");
                    $rtpl->setVariable("RR_ID", $ticket->getId());
                    $rtpl->setVariable("RRR_ID", $i);
                    $rtpl->setVariable("RR_INTID", $ticket->getId());
                    $rtpl->setVariable("RR_GSSID", $ticket->getClntTNum());
                    $rtpl->parse("print-version");
                }
                $rtpl->setVariable("RRNPV_ID", $ticket->getId());
                $rtpl->setVariable("RR_STATUSNAME", $ticket->getStatusName());
                $statusData = $ticket->getStatusArray();
                
                $rtpl->setVariable("RR_AREA", $ticket->getArea());
                $rtpl->setVariable("RR_CRDATE", $ticket->getDateReg());
                $tech = $ticket->getTaskIspoln($ticket->getId());
                $tech = array_values($tech);
                $rtpl->setVariable("RR_TECHNAME", $tech[0]);
                $addr = $ticket->getAddrN();
                if (isset($addr["nokladr"])) {
                    $rtpl->setCurrentBlock("no_kladr");
                    $rtpl->setVariable("TMCCOUNT2", $rowspan);
                    $rtpl->parse("no_kladr");
                } else {
                    $rtpl->setCurrentBlock("kladr_addr");
                    $rtpl->setVariable("TMCCOUNT3", $rowspan);
                    $rtpl->setVariable("RR_ADDR_CITY", $addr["city"]);
                    $rtpl->setVariable("RR_ADDR_STREET", $addr["street"]);
                    $rtpl->setVariable("RR_ADDR_HOUSE", "д.".$addr["house"]);
                    $rtpl->setVariable("RR_ADDR_FLAT", $ticket->getKv() ? $ticket->getKv() : "&mdash;");
                    $rtpl->parse("kladr_addr");
                }
                $rtpl->setVariable("RR_DOCDATE", $ticket->getIsInMoney() ? $ticket->getTicketMoneyDate() : "&mdash;");
                $allowaccept = false;
                if ($USER->checkAccess($this->getUID(), User::ACCESS_WRITE)) {
                    $allowaccept = true;
                }
                if ( @$_REQUEST["createprintversion"] || @$_REQUEST["createxlsversion"]) {
                    if ($ticket->getIsInMoney()) {
                        $rtpl->setVariable("ALREADYCOUNTED", "selected_row");
                        $sql = "SELECT * FROM skp_money WHERE `task_id`=".$DB->F($ticket->getId())." AND `last`;";
                        $r = $DB->getRow($sql, true);
                        if ($DB->errno()) UIError($DB->error());
                        if ($DB->num_rows()) {
                            $rtpl->setVariable("RR_AMMOUNT", $r["ammount"]);
                            $rtpl->setVariable("RR_TECHTOPAY", round($r["ammount"]*getcfg('tech_skp_pay_coef'), 2));
                            
                            $totalTICKET += $r["ammount"];
                            $totalTECH += round($r["ammount"]*getcfg('tech_skp_pay_coef'), 2);
                            $rtpl->setVariable("RR_TECHDOCNUM", $ticket->getTicketDocNum());
                        }
                        $rtpl->touchBlock("accepted-noedit");
                    } else {
                        $amm = $ticket->getOrientPrice();
                        $predtotalTICKET += intval($amm);
                        $predtotalTECH += round(intval($amm)*getcfg('tech_skp_pay_coef'), 2);
                        
                        $rtpl->setVariable("RR_AMMOUNT", "-");
                        $rtpl->setVariable("RR_TECHDOCNUM", $ticket->getTicketDocNum() ? $ticket->getTicketDocNum() : "-");
                        $rtpl->touchBlock("notaccepted-noedit");       
                    }
                } else {
                    if ($ticket->getIsInMoney()) {
                        $rtpl->setVariable("ALREADYCOUNTED", "selected_row");
                        $sql = "SELECT * FROM skp_money WHERE `task_id`=".$DB->F($ticket->getId())." AND `last`;";
                        $r = $DB->getRow($sql, true);
                        if ($DB->errno()) UIError($DB->error());
                        if ($DB->num_rows()) {
                            
                            $rtpl->setVariable("RR_AMMOUNT", $r["ammount"]);
                            $rtpl->setVariable("RR_TECHTOPAY", round($r["ammount"]*getcfg('tech_skp_pay_coef'), 2));
                            
                            $totalTICKET += $r["ammount"];
                            $totalTECH += round($r["ammount"]*getcfg('tech_skp_pay_coef'), 2);
                        
                            
                            $rtpl->setVariable("RR_AMMOUNT", $r["ammount"]);
                            $rtpl->setVariable("RR_TECHDOCNUM", $ticket->getTicketDocNum());
                        }
                        if ($allowaccept) {
                            $rtpl->setCurrentBlock("accept-accepted");
                            $rtpl->setVariable("AATASK_ID", $ticket->getId());
                            $rtpl->parse("accept-accepted"); 
                        } else {
                            $rtpl->touchBlock("accepted-noedit");                           
                        }
                    } else {
                        $amm = $ticket->getOrientPrice();
                        $predtotalTICKET += intval($amm);
                        $predtotalTECH += round(intval($amm)*getcfg('tech_skp_pay_coef'), 2);
                        
                        $rtpl->setVariable("RR_AMMOUNT", "-");
                        $rtpl->setVariable("RR_TECHDOCNUM", $ticket->getTicketDocNum() ? $ticket->getTicketDocNum() : "-");
                        if ($allowaccept) {
                            $rtpl->setCurrentBlock("accept");
                            $rtpl->setVariable("ATASK_ID", $ticket->getId());
                            $rtpl->parse("accept"); 
                        } else {
                            $rtpl->touchBlock("notaccepted-noedit");   
                        }
                        
                    }
                }
                $rtpl->setVariable("RR_SUMMWONDS", $sum);
                $rtpl->parse("rep_row");
                $i+=1;
            }
            $DB->free();
            $rtpl->setVariable("PREDTOTALTICKETS", number_format($predtotalTICKET, 2, '.', ' '));
            $rtpl->setVariable("PREDTOTALTECH", number_format($predtotalTECH, 2, '.', ' '));
            $rtpl->setVariable("TOTALTICKETS", number_format($totalTICKET, 2, '.', ' '));
            $rtpl->setVariable("TOTALTECH", number_format($totalTECH, 2, '.', ' '));
            $rtpl->setVariable("TOTALRECIEVED", number_format($totalTICKET - $totalTECH, 2, ".", " "));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("USERFIO", $USER->getFio());
            $rtpl->setVariable("TECHFIO", str_replace(" СКП", "", $techfio));
            if (isset($_REQUEST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_skp_mr__".$_REQUEST["tech_id"]."_".$datefrom."-".$dateto.".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_REQUEST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
    
    function setcard() {
        global $DB, $USER;
        if (!$USER->checkAccess("allow-card", User::ACCESS_WRITE)) {
            $ret["error"] = "В доступе отказано";
            echo json_encode($ret);
            return false;
        }
        $task_id = $_REQUEST["id"];
        $set = $_REQUEST["set"];
        if (!$task_id || !$set) {
            $res["error"] = "Ошибка: Недостаточно данных для выполнения операции.";
            echo json_encode($ret);
            return false;
        }
        if ($set == "set") {
            $sql = "UPDATE `tickets` SET `iscard`=1, `iscard_date`=".$DB->F(date("Y-m-d"))." WHERE `task_id`=".$DB->F($task_id).";";
            $t = new ServiceTicket($task_id);
            if ($t) {
                $t->addComment("Установлен флаг 'ДС на Счете (или карте)'", "Установлен флаг 'ДС на Счете (или карте)'");
            }
        } else {
            $sql = "UPDATE `tickets` SET `iscard`=0, `iscard_date`=".$DB->F(date("Y-m-d"))." WHERE `task_id`=".$DB->F($task_id).";";            
            $t = new ServiceTicket($task_id);
            if ($t) {
                $t->addComment("Снят флаг 'ДС на Счете (или карте)'", "Установлен флаг 'ДС на Счете (или карте)'");
            }
        }     
        $DB->query($sql);
        if ($DB->errno()) $ret["error"] = $DB->error();
        else $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;

    }
    
    function setcard_mult() {
        global $DB, $USER;
        if (!$USER->checkAccess("allow-card", User::ACCESS_WRITE)) {
            $ret["error"] = "В доступе отказано";
            echo json_encode($ret);
            return false;
        }

        //print_r($_REQUEST);
        if (sizeof($_REQUEST["seltask"])) {
            foreach($_REQUEST["seltask"] as $item) {
                $sql = "UPDATE `tickets` SET `iscard`=1, `iscard_date`=".$DB->F(date("Y-m-d"))." WHERE `task_id`=".$DB->F($item).";";
                $t = new ServiceTicket($item);
                if ($t) {
                    $t->addComment("Установлен флаг 'ДС на Счете (или карте)'", "Установлен флаг 'ДС на Счете (или карте)'");
                }

                $DB->query($sql);
                if ($DB->errno()) $ret["error"] = $DB->error();
            }
            if (!$ret["error"]) $ret["ok"] = "ok";
        } else {
            $ret["error"] = "Нет данных";
        }
        echo json_encode($ret);
        return false;
        
    }    
 
    function acceptMoney() {
        global $DB, $USER;
        $task_id = $_REQUEST["amtask_id"];
        $docnum = $_REQUEST["tdnum"];
        $ammount = $_REQUEST["ammount"];
                   
        if (!$task_id || !$docnum || !!$ammount) {
            $res["error"] = "Ошибка: форма заполнена не полностью";
            
        }
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        
        $sql = "UPDATE `tickets` SET `inmoney`=1 WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] = $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $sql = "UPDATE `tickets` SET `tdocnum`=".$DB->F($docnum)." WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $sql = "UPDATE `tickets` SET `inmoneydate`=".$DB->F(date("Y-m-d H:i:s"))." WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $sql = "UPDATE `skp_money` SET `last`=0 WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $dd = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `skp_money` (`task_id`, `ammount`, `le`, `user_id`, `last`) VALUES (".$DB->F($task_id).", ".$DB->F(implode("", explode(" ", $ammount))).", ".$DB->F($dd).", ".$DB->F($USER->getId()).", 1);";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
        $sql = "SELECT `status_id` FROM `tasks` WHERE `id`=".$DB->F($task_id).";";
        $r = $DB->getField($sql);
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $t = new ServiceTicket($task_id);
        if ($r != $countStatus["id"]) {
            $ret["newstatus"] = $countStatus["name"];
            $t->addComment("Прием денег. Сумма: ".number_format($ammount, 2, '.', ' '), "Автоматическое обновление статуса на Сдача отчета при приеме денег от техника", $countStatus["id"]);        
        } else {
            $t->addComment("Прием денег. Сумма: ".number_format($ammount, 2, '.', ' '), "Обновление принятой от техника суммы", 0);
        }
        $ret["ok"] = "ok";
        $ret["am"] = number_format($ammount, 2, '.', ' ');
        $ret["dn"] = $docnum;
        $ret["dd"] = $dd;
        echo json_encode($ret);
        return false;
    }
    
    function setaccepted() {
        global $DB, $USER;
        //print_r($_REQUEST);
        if (sizeof($_REQUEST["seltask"])) {
            foreach($_REQUEST["seltask"] as $item) {
                $sql = "UPDATE `tickets` SET `iscard`=0 WHERE `task_id`=".$DB->F($item).";";
                $DB->query($sql);
                if ($DB->error()) UIError($DB->error());
                $DB->free();
                $task = new Task($item);
                if ($task->getId()) {
                    if ($task->getStatusId() == 24) {
                        $task->addComment(" ", "Статус: <strong>Принято бухгалтерией</strong>", 49);
                    } else {
                        $err_task[] = $task->getId();
                    }
                } else {
                    redirect(link2("reports_f_skp_mr", false), "ID не соответствует реальной заявке!");            
                }
            }
            if (sizeof($err_task)) {
                redirect(link2("reports_f_skp_mr", false), "Ошибка изменения статуса заявок: ".implode(", ", $err_task));
            } else {
                redirect(link2("reports_f_skp_mr", false), "Заявки успешно приняты бухгалтерией.");
                
            }
        } else {
            redirect(link2("reports_f_skp_mr", false), "Не выбраны заявки для изменения статуса!");
        }
    }
}
?>
