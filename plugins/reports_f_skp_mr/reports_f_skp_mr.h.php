<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2014
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Прием денег СКП (СКП)";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Прием денег (СКП)";
    $PLUGINS[$plugin_uid]['events']['getCurrentTNumAndAmmount_ajax'] = "Получение текущих параметров заявки (СКП)";
    
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['acceptMoney'] = "Прием денег по заявке от техника";
    $PLUGINS[$plugin_uid]['events']['setaccepted'] = "Установка статуса Принято бухгалтерией";
    $PLUGINS[$plugin_uid]['events']['setcard'] = "Установка статуса ДС на счете";
    $PLUGINS[$plugin_uid]['events']['setcard_mult'] = "Установка статуса ДС на счете (несколько заявок)";
    
}

$plugin_uid = "allow-acc-accept";
$PLUGINS[$plugin_uid]['name'] = "Присвоение статуса Принято бухгалтерией СКП";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "allow-card";
$PLUGINS[$plugin_uid]['name'] = "Присвоение статуса ДС на счете";
$PLUGINS[$plugin_uid]['hidden'] = true;

?>