<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class adm_rejects_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getByID($id)
    {
        global $DB;
        if (!$id) return false;
        return $DB->getField("SELECT `title` FROM `list_rejreasons` WHERE `id`=" . $DB->F($id) . ";");
    }

    function getRejReasonTitle($id) {
        global $DB;
        $sql = "SELECT `title` FROM `list_rejreasons` WHERE `id`=" . $DB->F($id) . ";";
        return $DB->getField($sql);

    }

    function getOptList($sel_id = false, $tType =false) {
        global $DB;
        if ($tType)
            return array2options($DB->getCell2("SELECT `id`, `title` FROM `list_rejreasons` WHERE `plugin_uid`=" . $DB->F($tType) . " ORDER BY `title`"), $sel_id);
        else
            return array2options($DB->getCell2("SELECT `id`, `title` FROM `list_rejreasons` ORDER BY `title`"), $sel_id);

    }

    function getList($tType = false) {
        global $DB;
        if ($tType)
            return $DB->getCell2("SELECT `id`, `title` FROM `list_rejreasons` WHERE `plugin_uid`=" . $DB->F($tType) . " ORDER BY `title`");
        else
            return $DB->getCell2("SELECT `id`, `title` FROM `list_rejreasons` ORDER BY `title`");

    }

    function viewlist()
    {
        global $DB, $USER, $PLUGINS;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/adm_rejects.tmpl.htm");
        else
                $tpl->loadTemplatefile("adm_rejects.tmpl.htm");
        $sql = "SELECT * FROM `list_rejreasons` ORDER BY `plugin_uid`;";
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());
        while (list($id, $type, $tag, $title, $desc) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->setVariable("VLI_TYPE", $PLUGINS[$type]["name"]);
            $tpl->setVariable("VLI_DESC", $desc);
            $tpl->setVariable("VLI_TAG", $tag ? $tag: "&mdash;");
            $tpl->parse("vl_item");
        }
        UIHeader($tpl);
        $tpl->show();
    }

    function edit() {
        global $DB, $PLUGINS, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
        else
                $tpl->loadTemplatefile("edit.tmpl.htm");
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($id = $_REQUEST['id']) {
            $sql = "SELECT * FROM `list_rejreasons` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Запись с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);

            $tpl->setVariable("TYPE_ID", $result["id"]);
            $tpl->setVariable("TITLE", $result["title"]);
            $tpl->setVariable("DESC", $result["comment"]);
            $tpl->setVariable("TAG", $result["tag"]);
        }
        $ret = "<select name='plugin_uid'>";
        foreach($PLUGINS as $uid => $params) {
            $ret.="<option value='$uid' " . (@$result["plugin_uid"]==$uid ? "selected" : "" ) . ">".$params["name"]."</option>";
        }
        $ret.="</select>";
        $tpl->setVariable("PLUGIN_LIST", $ret);
        UIHeader($tpl);
        $tpl->show();

    }

    function save() {
        global $DB, $USER;
        $err = array();
        if(!$_POST['title']) $err[] = "Не заполнено поле Название причины отказа";
        if(sizeof($err)) UIError($err);
        if($_POST['id']) $sql = "UPDATE `list_rejreasons` SET `title`=".$DB->F($_POST['title']).", `comment`=".$DB->F($_POST['desc']).", `plugin_uid`=".$DB->F($_POST['plugin_uid']).", `tag`=".$DB->F($_POST["tag"])." WHERE `id`=".$DB->F($_POST['id']).";";
        else {
            $sql = "INSERT INTO `list_rejreasons` (`title`, `comment`, `plugin_uid`, `tag`) VALUES(".$DB->F($_POST['title']).", ".$DB->F($_POST['desc']).", ".$DB->F($_POST['plugin_uid']).", ".$DB->F($_POST['tag']).")";
        }
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error(). "<br />".$sql);

        if(!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Причина отказа сохранена. ID: ".$_POST['id']);
    }

    function delete() {
        global $DB;
        if (!($id = $_REQUEST['id'])) $err[] = "Не указан Вид работ!";
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST["id"])) UIError("Выбранная запись связана с активными Заявками. Удаление невозможно.");
        $DB->query("DELETE FROM `list_rejreasons` WHERE `id`='".$_REQUEST['id']."';");
        redirect($this->getLink(), "Причина отказа успешно удалена.");
    }

    function isBusy($id)
    {
        global $DB;
        $cnt = $DB->getField("SELECT COUNT(task_id) FROM `tickets` WHERE `reject_id`=" . $DB->F($id));
        if ($cnt != 0) {
            return true;
        } else return false;
    }

    function getRejectReasonsList($selected = false, $plugin_uid=false, $tag = false) {
        global $DB;
        $sql_add = false;
        if (!$plugin_uid || !$tag) {
            return false;
        }
        $tagList = explode(",", $tag);
        if (count($tagList)>=2) {
            foreach($tagList as $item) {
                $s[] = "`tag`=".$DB->F($item);
            }
            $sql_add = "AND (". implode(" OR ", $s) .")";
        } else {
            $sql_add = "AND `tag`=".$DB->F($tag);
        }
        if (!$tag) {
            $sql_add = "";
        }
        $sql = "SELECT * FROM `list_rejreasons` WHERE `plugin_uid`=" . $DB->F($plugin_uid) . " $sql_add ORDER BY `title`;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        $ret = false;
        if ($DB->num_rows()) {
            while (list($id, $mod, $tag, $title)=$DB->fetch()) {
                if ($id == $selected)
                    $ret .="<option selected='selected' value='$id' >$title</option>";
                else
                    $ret .="<option value='$id'>$title</option>";
            }
        }
        $DB->free();
        return $ret;
    }
}
?>
