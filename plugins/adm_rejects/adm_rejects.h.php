<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Справочник причин отказа по заявкам";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Причины отказа";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование причины отказа";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранить причину отказа";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление причины отказа";
    
}
?>