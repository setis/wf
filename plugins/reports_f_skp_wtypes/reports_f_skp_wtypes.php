<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\User;


require_once(dirname(__FILE__)."/../services/services.php");

class reports_f_skp_wtypes_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
     
    function main() {
        global $DB, $USER;
        $fintypes = array("1"=>"Рассчитанные (Деньги приняты)", "2"=>"Не рассчитанные (Деньги не приняты)");
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $_REQUEST["month"] = $this->getCookie('month', $_REQUEST["month"] ? $_REQUEST["month"] : date("m"));
        $_REQUEST["year"] = $this->getCookie('year', $_REQUEST["year"] ? $_REQUEST["year"] : date("Y"));
        
        
        $sc_id = $USER->isChiefTech($USER->getId()) ? $USER->isChiefTech($USER->getId()) : false;
        if (!$sc_id && !User::isTech($USER->getId())) {
            $_REQUEST["scortech"] = $this->getCookie("scortech", $_REQUEST["scortech"]);
            if ($_REQUEST["scortech"] == "tech") {
                $_REQUEST["tech_id"] = $this->getCookie("tech_id", $_REQUEST["tech_id"]);            
            } else {
                $_REQUEST["sc_id"] = $this->getCookie("sc_id", $_REQUEST["sc_id"]);
                
            }
        } else {
            $_REQUEST["tech_id"] = $this->getCookie("tech_id", $_REQUEST["tech_id"]);            
        }
        $_REQUEST["createreport"] = $this->getCookie("createreport", $_REQUEST["createreport"]);
        $_REQUEST["cnt_id"] = $this->getCookie("cnt_id", $_REQUEST["cnt_id"]);
        
        //$_REQUEST["onlyinreport"] = $this->getCookie("onlyinreport", $_REQUEST["onlyinreport"]);
        
         
        
        //die($_REQUEST["onlyinreport"]);
        $dateParams = array(0 => "Дата создания", 1 => "Дата выполнения", 2=>"Дата изменения", 3=>"Дата принятия ДС");
        if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
            $cntr = new kontr_plugin();
        }
        $contrCount = $cntr->getListSer();
                    
        if (!isset($_REQUEST['createprintversion']) && !isset($_REQUEST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListSer(@$_REQUEST["cnt_id"]));
            }
            $tpl->setVariable("MONTH", array2options($month, $_REQUEST["month"]));
            $tpl->setVariable("YEAR", array2options($year, $_REQUEST["year"]));
            
            $sc_id = $USER->isChiefTech($USER->getId()) ? $USER->isChiefTech($USER->getId()) : false;
            if (!$sc_id && !User::isTech($USER->getId())) {
                $tpl->setCurrentBlock("show_sot");
                $tpl->setVariable("SOT_TECH", $_REQUEST["scortech"] == "tech" ? "checked='checked'": ($_REQUEST["scortech"] == "" ? "checked='checked'" : " "));
                $tpl->parse("show_sot");
                $tpl->setCurrentBlock("notscchief");
                $tpl->setVariable("SOT_SC", $_REQUEST["scortech"] == "sc" ? "checked='checked'":" ");
                $tpl->setVariable("FILTER_SC_OPTIONS", array2options(adm_sc_plugin::getSCList2Col(), $_REQUEST["sc_id"]));
                $tpl->parse("notscchief");
            }
            $tpl->setVariable("FILTER_TECH_OPTIONS", adm_empl_plugin::getTechListSKP($_REQUEST["tech_id"], $sc_id));
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $showAccAccept = (@$_REQUEST["createreport"] && !(@$_REQUEST["createprintversion"] && @$_REQUEST["createxlsversion"]) && $_REQUEST['onlyinreport'] == "on") ? true : false;
            $showAccAcceptinQuery = $_REQUEST['onlyinreport'] == "on" ? true : false;
            $period = $_REQUEST["year"]."-".$_REQUEST["month"];
            
            
            $rtpl->setVariable("DATEFROM", rudate("d F Y", strtotime("01-".$_REQUEST["month"]."-".$_REQUEST["year"])));
            $rtpl->setVariable("DATETO", rudate("d F Y", strtotime($period ." + 1 month - 1 day")));
            //if ($_REQUEST["tech_id"]!=0 && $usr = adm_users_plugin::getUser($_REQUEST["tech_id"])) {
            $tech = adm_users_plugin::getUser($_REQUEST["tech_id"]);
            if ($_REQUEST["scortech"] == "sc") {
                $rtpl->setVariable("TECHHIDE", 'hidden');
                $sc = adm_sc_plugin::getSC($_REQUEST["sc_id"]);
                $rtpl->setVariable("SCNAME", $sc["title"]);
            } else {
                $rtpl->setVariable("TECHNAME", $tech['fio']);
                $rtpl->setVariable("SCHIDE", 'hidden');
            }
            $techfio = $tech["fio"];
            //$rtpl->setVariable("REP_TECH", $usr['fio']);
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "services");
            $countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
            $acceptedStatus = adm_statuses_plugin::getStatusByTag("acc_accepted", "services");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "services");

            $filter = '';
            if ($_REQUEST["tech_id"] > 0 && $_REQUEST["scortech"] != "sc") {
                $filter .= " AND g.empl_id=".$DB->F($_REQUEST["tech_id"])." ";
            } else {
                if ($_REQUEST["sc_id"])
                    $filter .= " AND g.empl_id IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=".$DB->F($_REQUEST["sc_id"]).") ";  
            }
            if ($_REQUEST['cnt_id']>0) {
                $sel = "(SELECT `price` FROM `list_skp_p_price` WHERE `wt_id`=ltc.wt_id AND `contr_id`=".$DB->F($_REQUEST['cnt_id']).") as price1, ";
                $filter .= " AND t.cnt_id=".$DB->F($_REQUEST["cnt_id"])." ";
            }
            $order = " ORDER BY g.empl_id DESC, t.date_reg";
            

            $sql ="SELECT ltc.wt_id,
                        (SELECT `ei_min` FROM `list_skp_p_price` WHERE `wt_id`=ltc.wt_id limit 1) as ei_min, 
                        $sel
                        (SELECT `title` FROM `list_skp_p_wtypes` WHERE `id`=ltc.wt_id) as name, 
                        (SELECT (select `title` from `list_skp_p_edizm` where `id`=`ei_id`) FROM `list_skp_p_wtypes` WHERE `id`=ltc.wt_id) as ei_name,  
                        sum(ltc.quant) as cnt,
                        sum(ltc.quant*ltc.price) as price,
                        ltc.minval 
                   FROM `list_skp_tech_calc` AS ltc 
                        LEFT JOIN `tickets` AS t ON t.task_id=ltc.task_id
                        LEFT JOIN `gfx` AS g ON g.task_id=t.task_id
                   WHERE DATE_FORMAT(ltc.calc_date, '%Y-%m')=".$DB->F($period)." 
                          $filter 
                   GROUP BY ltc.wt_id;";

//            d($sql);

            $sql ="SELECT ltc.wt_id,
                        (SELECT `ei_min` FROM `list_skp_p_price` WHERE `wt_id`=ltc.wt_id limit 1) as ei_min,
                        $sel
                        (SELECT `title` FROM `list_skp_p_wtypes` WHERE `id`=ltc.wt_id) as name,
                        (SELECT (select `title` from `list_skp_p_edizm` where `id`=`ei_id`) FROM `list_skp_p_wtypes` WHERE `id`=ltc.wt_id) as ei_name,
                        sum(ltc.quant) as cnt,
                        sum(ltc.quant*ltc.price) as price,
                        ltc.minval
                   FROM `list_skp_tech_calc` AS ltc
                        LEFT JOIN `tickets` AS t ON t.task_id=ltc.task_id
                        left join `task_comments` tc on t.task_id = tc.task_id
                        LEFT JOIN `gfx` AS g ON g.task_id=t.task_id
                   WHERE DATE_FORMAT(tc.datetime, '%Y-%m')=".$DB->F($period)." and tc.status_id = ".$doneStatus['id']."
                          $filter
                   GROUP BY ltc.wt_id;";
//            d($sql);

//            die;
            
            //die($sql);
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error()." ".$sql);
            $i = 1;
            if ($DB->num_rows()) {
                while ($res = $DB->fetch(true)) {
                    $rtpl->setCurrentBlock("rep_row");
                    $rtpl->setVariable("RR_ID", $res["wt_id"]);
                    $rtpl->setVariable("ROW_NUM", $i++);
                    $rtpl->setVariable("ROW_TITLE", $res["name"]);
                    $rtpl->setVariable("ROW_EDIZM", $res["ei_name"]);
                    $rtpl->setVariable("ROW_PRICEONE", $_REQUEST["cnt_id"]==0? "&mdash;" : number_format($res["price1"], 2, ",", " "));
                    $rtpl->setVariable("ROW_MIN", round($res["ei_min"],2));
                    $rtpl->setVariable("ROW_SUMM", number_format($res["price"], 2, ",", " "));
                    $rtpl->setVariable("ROW_QUANT", $res["cnt"]);
                    $rtpl->parse("rep_row");
                }
            } else {
                $rtpl->touchBlock("no-rows");
            }
            $DB->free();
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            if (isset($_REQUEST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_skp_wtype__"."_".$_REQUEST["month"]."-".$_REQUEST["year"].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_REQUEST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
    
     
     
}
?>