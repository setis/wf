<?php

/**
 * Plugin Implementation
 *
 * @author Ed
 */
use classes\Plugin;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use classes\Task;
use models\Task as ModelTask;
use models\Ticket as ModelTicket;
use models\TaskStatus;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class clone_ticket_plugin extends Plugin
{

    public function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    /**
     * Возвращает json массива плагинов, в которые может быть преобразована исходная заявка
     *
     * @param Request $request
     * @return array
     */
    static function getOptions($src_plugin)
    {
        $available_plugins = [
            ModelTask::ACCIDENT,
            ModelTask::SERVICE,
            ModelTask::CONNECTION,
            ModelTask::TELEMARKETING,
        ];

        $opts = array();
        foreach ($available_plugins as $val) {
            if (wf::$user->checkAccess($val, \classes\User::ACCESS_WRITE)) {
                $opts[] = $val;
            }
        }

        return $opts;
    }

    /**
     * Clone one task to another
     *
     * @param Request $request
     */
    public function cloneTicket(Request $request)
    {
        $src_task_id = (int) $request->get('src_task_id');
        $target_plugin = $request->get('target_plugin');
        $src_plugin = $request->get('src_plugin');

        if (!in_array($target_plugin, $this->getOptions($src_plugin))) {
            throw new AccessDeniedHttpException('У Вас нет доступа на создание заявок выбранного типа.');
        }

        $em = $this->getEm();

        /* @var $old_ticket ModelTicket */
        $old_ticket = $em->getRepository(ModelTicket::class)->find($src_task_id);

        if (null === $old_ticket) {
            throw new NotFoundHttpException("Заявка №$src_task_id не найдена");
        }

        $tm = $this->getContainer()->get('wf.task.task_manager');

        $em->beginTransaction();
        try {
            $task = $tm->createTask($target_plugin, [
                'author_id' => $this->getUserId(),
                'status' => $em->find(TaskStatus::class, (int) Task::getDefaultStatusId($target_plugin)),
                'status_id' => (int) Task::getDefaultStatusId($target_plugin)
            ]);
            $em->persist($task);

            $ticket = $tm->createTicket($task, [
                "partner" => $old_ticket->getPartner(),
                "checked" => 0,
                "client_full_name" => $old_ticket->getClntFio(),
                "client_org_name" => $old_ticket->getClntOrgName(),
                "client_phone1" => $old_ticket->getClntTel1(),
                "client_phone2" => $old_ticket->getClntTel2(),
                "client_type" => $old_ticket->getClntType(),
                "address" => $old_ticket->getDom(),
                "intercom" => $old_ticket->getDomofon(),
                "floor" => $old_ticket->getEtazh(),
                "apartment" => $old_ticket->getKv(),
                "entrance" => $old_ticket->getPod(),
                "service_center" => $old_ticket->getServiceCenter(),
                "agreement" => $old_ticket->getAgreement()
            ]);
            $em->persist($ticket);

            $old_ticket->getTask()->addSlaveTask($task);
            $em->flush();
            $em->commit();
            return new RedirectResponse("/" . $target_plugin . "/viewticket?task_id=" . $task->getId());
        }
        catch (Exception $ex) {
            $em->rollback();
            throw $ex;//new Exception('Ошибка копирования тикета');
        }
    }

}
