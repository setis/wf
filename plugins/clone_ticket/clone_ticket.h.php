<?php 

$plugin_uid = basename(__FILE__, ".h.php");
$PLUGINS[$plugin_uid]['name'] = "Создание заявок нового типа из уже существующих";
$PLUGINS[$plugin_uid]['hidden'] = true;

$PLUGINS['joinedtasks_access']['name'] = "Просмотр связанных заявок в Подключениях, СКП, ТТ, ТМ";
$PLUGINS['joinedtasks_access']['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['getOptions'] = "Доступные виды заявок";
    $PLUGINS[$plugin_uid]['events']['cloneTicket'] = "Клонирование заявки";
}

?>