<?php
use classes\User;

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Управление Техниками";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник Техников";
    $PLUGINS[$plugin_uid]['events']['getSCTechsAll_ajax'] = "Получение списка Техников";
}

if(wf::$user->checkAccess($plugin_uid, User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование Техника";
    $PLUGINS[$plugin_uid]['events']['addmultiple'] = "Пакетное Добавление Техников";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление Техника";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение параметров Техника";
    $PLUGINS[$plugin_uid]['events']['savealias_ajax'] = "Сохранение виртуального алиаса Техника";
    $PLUGINS[$plugin_uid]['events']['deletealias_ajax'] = "Удаление виртуального алиаса Техника";
    $PLUGINS[$plugin_uid]['events']['setTechnicianCoordinates'] = "Сохранение координат";
    $PLUGINS[$plugin_uid]['events']['getTechnicianCoordinates'] = "Получение координат";
    $PLUGINS[$plugin_uid]['events']['getAllTechniciansCoordinates'] = "Получение координат";
    $PLUGINS[$plugin_uid]['events']['getServiceCenterCoordinates'] = "Получение координат СЦ";
}
