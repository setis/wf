<?php
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class adm_empl_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function usesSms($id)
    {
        global $DB;
        if (!$id) return false;
        return $DB->getField("SELECT `use_sms` FROM `list_empl_usesms` WHERE `user_id`=" . $DB->F($id) . ";");
    }

    static function isReal($id)
    {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT COUNT(`user_id`) FROM `virtual_techs` WHERE `virtual_id`=" . $DB->F($id) . ";";
        $vid = $DB->getField($sql);
        return $vid > 0 ? false : true;
    }

    static function getReal($id)
    {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT `user_id` FROM `virtual_techs` WHERE `virtual_id`=" . $DB->F($id) . ";";
        $vid = $DB->getField($sql);
        return $vid;
    }

    /**
     * Get list of virtual employee of \$user
     * @param $user
     * @return mixed
     */
    static function getalias($user)
    {
        global $DB;
        if (!$user) return false;
        $vid = $DB->getField("SELECT `virtual_id` FROM `virtual_techs` WHERE `user_id`=" . $DB->F($user) . ";");
        //die("SELECT `virtual_id` FROM `virtual_techs` WHERE `user_id`=".$DB->F($user).";");
        if ($DB->errno()) {
            UIError($DB->error());
        }
        return $vid;
    }

    static function getChiefListBySC($sc_id)
    {
        global $DB;
        $DB->query("SELECT `id`, `fio` FROM `users` WHERE `active` AND  `id` IN (SELECT `user_id` FROM `link_sc_chiefs` WHERE  `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `fio`;");
        if ($DB->errno()) {
            $DB->free();
            return false;
        }
        if (!$DB->num_rows()) {
            $DB->free();
            return false;
        }
        while (list($id, $fio) = $DB->fetch()) {
            $res[$id] = $fio;
        }
        $DB->free();
        return $res;
    }

    static function getSCTechs($sc_id)
    {
        global $DB;
        $DB->query("SELECT `id`, `fio` FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `fio`;");
        //AND `id` NOT IN (SELECT `user_id` FROM `link_sc_chiefs` WHERE  `sc_id`=".$DB->F($sc_id).")
        if ($DB->errno()) {
            $DB->free();
            return false;
        }
        if (!$DB->num_rows()) {
            $DB->free();
            return false;
        }
        while (list($id, $fio) = $DB->fetch()) {
            $res[$id] = $fio;
        }
        $DB->free();
        return $res;
    }

    static function getSCTechs1($sc_id)
    {
        global $DB;
        $DB->query("SELECT u.id, u.fio, u.pos_id, (SELECT `title` FROM `user_pos` WHERE `id`=u.pos_id) AS posname FROM `users` AS u WHERE u.active AND u.id IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `fio`;");
        //AND `id` NOT IN (SELECT `user_id` FROM `link_sc_chiefs` WHERE  `sc_id`=".$DB->F($sc_id).")
        if ($DB->errno()) {
            $DB->free();
            return false;
        }
        if (!$DB->num_rows()) {
            $DB->free();
            return false;
        }
        while (list($id, $fio, $pos_id, $posname) = $DB->fetch()) {
            $res[$id] = array($fio, $pos_id, $posname);

        }
        $DB->free();
        return $res;
    }

    /**
     * Return NOT virtual tech of from SC by $sc_id
     * @param $sc_id
     * @return []|bool
     */
    static function getSCTechs2($sc_id)
    {
        global $DB;
        $DB->query("
        SELECT
          u.id,
          u.fio,
          u.pos_id,
          (
            SELECT
              `title`
            FROM
              `user_pos`
            WHERE
              `id`=u.pos_id
            ) AS posname /* наименование должности */
        FROM
          `users` AS u
        WHERE
          u.active AND
          u.id NOT IN (SELECT `virtual_id` FROM `virtual_techs`) AND
          u.id IN (
            SELECT
              `user_id`
            FROM
              `link_sc_user`
            WHERE
              `sc_id`=" . $DB->F($sc_id) . "
          )
        ORDER BY `fio`;
        ");
        //AND `id` NOT IN (SELECT `user_id` FROM `link_sc_chiefs` WHERE  `sc_id`=".$DB->F($sc_id).")
        if ($DB->errno()) {
            $DB->free();
            return false;
        }
        if (!$DB->num_rows()) {
            $DB->free();
            return false;
        }
        while (list($id, $fio, $pos_id, $posname) = $DB->fetch()) {
            $res[$id] = array($fio, $pos_id, $posname);

        }
        $DB->free();
        return $res;
    }

    static function getSCTechsAll_ajax(Request $request)
    {
        global $DB, $USER;
        $sc_id = $request->get("sc_id");
        $tech_id = $request->get("tech_id");
        if (!empty($tech_id) && $USER->isTech($tech_id)) {
            $sql = "SELECT `id`, `fio` FROM `users` WHERE `active` AND `id`=" . $DB->F($tech_id) . ";";
        } else
            $sql = "SELECT `id`, `fio` FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `fio`;";

        $result = array2options($DB->getCell2($sql), $tech_id);

        echo $result;
    }

    static function getSCTechsAll($sc_id, $tech_id = 0)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`, `fio` FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `fio`;"), $tech_id);
    }

    static function getTechList($uid)
    {
        global $DB;
        $result = $DB->getCell2("SELECT `id`, CONCAT(fio, ' (', `id`,')') FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `list_empl`) ORDER BY `fio`;");
        if ($DB->error()) return false;
        return array2options($result, $uid);
    }

    static function getTechListSKP($uid, $sc_id = 0)
    {
        global $DB;
        if ($sc_id) $sc_sql = "AND `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F(sc_id) . ")";
        $result = $DB->getCell2("(SELECT `id`, CONCAT(fio, ' (', `id`,')') as fioid FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `list_empl`) $sc_sql AND `fio` LIKE '%СКП%' ORDER BY `fioid`);");
        if ($DB->error()) return false;
        $tmp = array2options($result, $uid);
        $result = $DB->getCell2("(SELECT `id`, CONCAT(fio, ' (', `id`,')') as fioid FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `list_empl`) $sc_sql AND `fio` NOT LIKE '%СКП%' ORDER BY `fioid`);");
        $tmp .= array2options($result, $uid);
        if ($DB->error()) return false;
        return $tmp;
    }

    static function getTechListSKP_MultipleSC($uid, $sc_id = 0)
    {
        global $DB;
        $tmp = '';
        foreach ($sc_id as $item) {
            if ($sc_id) $sc_sql = "AND `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($item['sc_id']) . ")";
            $result = $DB->getCell2("(SELECT `id`, CONCAT(fio, ' (', `id`,')') as fioid FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `list_empl`) $sc_sql AND `fio` LIKE '%СКП%' ORDER BY `fioid`);");
            if ($DB->error()) return false;
            $tmp .= array2options($result, $uid);
            $result = $DB->getCell2("(SELECT `id`, CONCAT(fio, ' (', `id`,')') as fioid FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `list_empl`) $sc_sql AND `fio` NOT LIKE '%СКП%' ORDER BY `fioid`);");
            $tmp .= array2options($result, $uid);
        }
        if ($DB->error()) return false;
        return $tmp;
    }

    static function getTechListSKP_MR($uid, $sc_id)
    {
        global $DB;
        $tmp = "";
        if ($sc_id) {
            $sc_sql = "AND `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($sc_id) . ")";
            $tmp = "<option" . (!$uid ? " selected='selected' " : "") . " value='0'>-- все --</option>";
        }
        $result = $DB->getCell2("(SELECT `id`, CONCAT(fio, ' (', `id`,')') as fioid FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `list_empl`) $sc_sql AND `fio` LIKE '%СКП%' ORDER BY `fioid`);");
        //print_r($result);
        //echo "(SELECT `id`, CONCAT(fio, ' (', `id`,')') as fioid FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `list_empl`) $sc_sql AND `fio` LIKE '%СКП%' ORDER BY `fioid`)";
        //die();
        if ($DB->error()) return false;
        $tmp .= array2options($result, $uid);
        $result = $DB->getCell2("(SELECT `id`, CONCAT(fio, ' (', `id`,')') as fioid FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `list_empl`) $sc_sql AND `fio` NOT LIKE '%СКП%' ORDER BY `fioid`);");
        $tmp .= array2options($result, $uid);
        if ($DB->error()) return false;
        return $tmp;
    }

    static function getTechFIO($uid)
    {
        global $DB;
        $result = $DB->getField("SELECT `fio` FROM `users` WHERE `active` AND `id`=" . $DB->F($uid) . ";");
        if ($DB->error()) return false;
        return $result;
    }

    static function getPItemsByWorkAndTaskFilter($sql)
    {
        global $DB;
        if ($sql == '') return false;
        $sql1 = "SELECT lp.* FROM `list_tech_price` AS lp WHERE (lp.id IN (SELECT lcc.work_id FROM `list_tech_calc` AS lcc WHERE lcc.task_id IN (" . $sql . ")) OR lp.id IN (SELECT lcc.work_id FROM `list_tt_tech_calc` AS lcc WHERE lcc.task_id IN (" . $sql . "))) GROUP BY lp.id;";

        $DB->query($sql1);
        if ($DB->errno()) UIError($DB->error() . " " . $sql1);
        while ($res = $DB->fetch(true)) {
            $ret[] = $res;
        }

        $DB->free();
        return $ret;
    }

    /**
     * Example usage
     *
     * adm_empl_plugin::revokeAllTechsAccess('reports_f_tech');
     *
     * adm_empl_plugin::grantAllTechsAccess('reports_f_tech41');
     * adm_empl_plugin::grantAllChiefTechsAccess('reports_f_sc41');
     *
     * @param $uid
     * @param int $access
     */
    static public function grantAllTechsAccess($uid, $access = User::ACCESS_READ)
    {
        $users = self::getAllTechs();

        foreach ($users as $usr) {
            $user = new User($usr['id']);
//            if(!adm_empl_plugin::isVirtual($usr['id'])) {
            if ($user->isTech($usr['id'])) {
                $user->grantAccess($uid, $access);
            }
//            }
        }
    }

    /**
     * @return array
     */
    static public function getAllTechs()
    {
        global $DB;

        $query = "SELECT
            `id`,
            `fio`
        FROM
            `users`
        WHERE
            `active` AND
            `id` IN (
                SELECT
                    `user_id`
                FROM
                    `link_sc_user`
                WHERE
                    sc_id > 0
            )
        ORDER BY
            `fio`
        ;";

        $users = $DB->getAll($query, true, true, -1);

        return $users;
    }

    static public function grantAllChiefTechsAccess($uid, $access = User::ACCESS_READ)
    {
        $users = self::getAllChiefTechs();

        foreach ($users as $usr) {
            $user = new User($usr['id']);
//            if(!adm_empl_plugin::isVirtual($usr['id'])) {
            if ($user->isTech($usr['id'])) {
                $user->grantAccess($uid, $access);
            }
//            }
        }
    }

    static public function getAllChiefTechs()
    {
        global $DB;
        $query = "SELECT
            `id`,
            `fio`
        FROM
            `users` LEFT JOIN
            link_sc_chiefs ON (link_sc_chiefs.user_id = users.id)
        WHERE
            `active` AND
            link_sc_chiefs.user_id IS NOT NULL
        ORDER BY
            `fio`
        ;";

        $users = $DB->getAll($query, true, true, -1);

        return $users;
    }

    /**
     * @param $uid
     */
    static public function revokeAllTechsAccess($uid)
    {
        $users = self::getAllTechs();

        foreach ($users as $usr) {
            $user = new User($usr['id']);
            if ($user->isTech($usr['id'])) {
                $user->revokeAccess($uid);
            }
        }
    }

    function viewlist(Request $request)
    {
        global $DB, $USER, $PLUGINS;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));

        if ('POST' === $request->getMethod() &&
            ($request->request->has('addwrite') || $request->request->has('addread') || $request->request->has('remove') || $request->request->has('copyuser') )
        ) {
            $this->group_operations($request);
        }

        $_REQUEST["filter_id"] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST["filter_search"] = $this->getCookie('filter_search', $_REQUEST['filter_search']);
        $_REQUEST["filter_sc_id"] = $this->getCookie('filter_sc_id', $_REQUEST['filter_sc_id']);
        $_REQUEST["filter_area_id"] = $this->getCookie('filter_area_id', $_REQUEST['filter_area_id']);
        $_REQUEST["filter_wtype_id"] = $this->getCookie('filter_wtype_id', $_REQUEST['filter_wtype_id']);
        $_REQUEST["filter_pos_id"] = $this->getCookie('filter_pos_id', $_REQUEST['filter_pos_id']);

        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");


        if ($USER->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }
        $tpl->setVariable("FILTER_ID", $_REQUEST["filter_id"]);
        $tpl->setVariable("FILTER_SEARCH", $_REQUEST["filter_search"]);
        $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["filter_sc_id"], false));
        $tpl->setVariable("FILTER_WTYPE_OPTIONS", array2options(adm_ticktypes_plugin::getList(), $_REQUEST["filter_wtype_id"]));
        $tpl->setVariable("FILTER_POS_OPTIONS", user_positions_plugin::getOptionsFilter($_REQUEST["filter_pos_id"]));

        $sql_add = [];
        if ($_REQUEST["filter_id"]) {
            $sql_add[] = "usr.id=" . $DB->F($_REQUEST["filter_id"]);
        }
        if ($_REQUEST["filter_search"]) {
            $sql_add[] = "usr.fio LIKE " . $DB->F("%" . $_REQUEST["filter_search"] . "%");
        }
        if ($_REQUEST["filter_sc_id"] > 0) {
            $sql_add[] = "sc.sc_id=" . $DB->F($_REQUEST["filter_sc_id"]);
        }
        if ($_REQUEST["filter_wtype_id"] > 0) {
            $sql_add[] = "usr.id IN (SELECT `user_id` FROM `list_empl` WHERE `wtype_id`=" . $DB->F($_REQUEST["filter_wtype_id"]) . ")";
        }
        if ($_REQUEST["filter_pos_id"] > 0) {
            $sql_add[] = "usr.pos_id=" . $DB->F($_REQUEST["filter_pos_id"]);
        }
        foreach ($PLUGINS as $key => $item) {
            if ($key != "desktop") {
                $plarr[$key] = $item["name"];
            }
        }
        $tpl->setVariable("PLUGINLIST", array2options($plarr));
        $sql_add = implode(" AND ", $sql_add);
        if ($sql_add) $sql_add = " AND " . $sql_add;

        # EF: Ограничение к списку по новым доступам
        $sql_add .= " and (usr.id in (SELECT distinct lsu.user_id FROM link_sc_user lsu, user_sc_access usa where usa.sc_id = lsu.sc_id and usa.user_id = " . $USER->getId() . ") "
            . "or (sc.user_id is null and exists ( select * from list_empl le where le.user_id = usr.id )) )";

        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $sql = "SELECT usr.id, usr.fio, usr.pos_id, sc.sc_id FROM users AS usr
                LEFT JOIN link_sc_user AS sc ON usr.id=sc.user_id
                WHERE `active` AND usr.id IN (SELECT DISTINCT user_id FROM `list_empl`) $sql_add GROUP BY usr.id ORDER BY `fio`";
        //die($sql);
        $DB->query($sql);
        //die($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $tpl->touchBlock("notechrecords");
        } //UIError("Техники отсутствуют!", "Справочник Техников", true, "Добавить Техника", "adm_empl/addmultiple");
        while (list($id, $fio, $pos, $sc_id) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_FIO", $fio);
            if ($wtypes = $this->getWTypes($id)) {
                $tpl->setVariable("VLI_WTYPES", $wtypes);
            }
            $tpl->setVariable("VLI_POS", adm_users_plugin::getPos($pos));
            if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getSc")) {
                $scC = new adm_sc_plugin();
                $sc = $scC->getSC($sc_id);
                if ($sc)
                    $tpl->setVariable("VLI_SC", $sc["title"]);
                else
                    $tpl->setVariable("VLI_SC", " - ");
            } else {
                $tpl->setVariable("VLI_SC", " - ");
            }
            $sql = "SELECT `title` FROM `list_area` WHERE `id` IN (SELECT `area_id` FROM `link_user_area` WHERE `user_id`=" . $DB->F($id) . ");";
            $areaList = $DB->getCell($sql);
            if ($DB->errno()) UIError($DB->error());
            if ($areaList) {
                $tpl->setVariable("VLI_AREAS", implode("<br />", $areaList));
            } else {
                $tpl->setVariable("VLI_AREAS", " - ");
            }
            $tpl->parse("vl_item");
        }
        $DB->free();
        UIHeader($tpl);
        $tpl->show();

    }

    /**
     * TODO
     */
    private function group_operations(Request $request)
    {
        global $USER, $DB, $PLUGINS;

        if (!User::ACCESS_WRITE == $USER->checkAccess(USER_COPY_EDIT_SAVE_ACCESS)) {
            throw new \Symfony\Component\Security\Core\Exception\AccessDeniedException();
        }

        if (isset($_POST["addwrite"])) {
            if (
                is_array($_POST["seltechid"]) && count($_POST["seltechid"]) && $_POST["plugin_uid"] != ""
            ) {
                foreach ($_POST["seltechid"] as $user_id) {
                    $sql = "DELETE FROM `user_access` WHERE plugin_uid=" . $DB->F($_POST["plugin_uid"]) . " AND `user_id`=" . $DB->F($user_id);
                    $DB->query($sql);
                    if ($DB->errno()) $err[] = $DB->error();
                    $sql = "INSERT INTO `user_access` (`user_id`, `plugin_uid`, `access`) VALUES(" . $DB->F($user_id) . ", " . $DB->F($_POST["plugin_uid"]) . ", " . $DB->F(2) . ")";
                    $DB->query($sql);
                    if ($DB->errno()) $err[] = $DB->error() . " " . $sql;
                }
                if (sizeof($err)) {
                    UIError("Ошибки во время выполнения пакетной обработки прав: \r\n" . implode("\r\n\r\n", $err), "Система безопасности");

                }
            } else {
                UIError("Недостаточно данных для пакетной обработки прав!", "Система безопасности");
            }
            redirect(link2("/adm_empl", false), "Право на запись в модуль " . $PLUGINS[$_POST["plugin_uid"]]["name"] . " успешно добавлено группе");
        }

        if (isset($_POST["addread"])) {
            if (
                is_array($_POST["seltechid"]) && count($_POST["seltechid"]) && $_POST["plugin_uid"] != ""
            ) {
                foreach ($_POST["seltechid"] as $user_id) {
                    $sql = "DELETE FROM `user_access` WHERE plugin_uid=" . $DB->F($_POST["plugin_uid"]) . " AND `user_id`=" . $DB->F($user_id);
                    $DB->query($sql);
                    if ($DB->errno()) $err[] = $DB->error();
                    $sql = "INSERT INTO `user_access` (`user_id`, `plugin_uid`, `access`) VALUES(" . $DB->F($user_id) . ", " . $DB->F($_POST["plugin_uid"]) . ", " . $DB->F(1) . ")";
                    $DB->query($sql);
                    if ($DB->errno()) $err[] = $DB->error() . " " . $sql;
                }
                if (sizeof($err)) {
                    UIError("Ошибки во время выполнения пакетной обработки прав: \r\n" . implode("\r\n\r\n", $err), "Система безопасности");

                }
            } else {
                UIError("Недостаточно данных для пакетной обработки прав!", "Система безопасности");
            }
            redirect(link2("/adm_empl", false), "Право на чтение в модуль " . $PLUGINS[$_POST["plugin_uid"]]["name"] . " успешно добавлено группе");
        }

        if (isset($_POST["remove"])) {
            if (
                is_array($_POST["seltechid"]) && count($_POST["seltechid"]) && $_POST["plugin_uid"] != ""
            ) {
                foreach ($_POST["seltechid"] as $user_id) {
                    $sql = "DELETE FROM `user_access` WHERE plugin_uid=" . $DB->F($_POST["plugin_uid"]) . " AND `user_id`=" . $DB->F($user_id);
                    $DB->query($sql);
                    if ($DB->errno()) $err[] = $DB->error();
                    $sql = "INSERT INTO `user_access` (`user_id`, `plugin_uid`, `access`) VALUES(" . $DB->F($user_id) . ", " . $DB->F($_POST["plugin_uid"]) . ", " . $DB->F(0) . ")";
                    $DB->query($sql);
                    if ($DB->errno()) $err[] = $DB->error() . " " . $sql;
                }
                if (sizeof($err)) {
                    UIError("Ошибки во время выполнения пакетной обработки прав: \r\n" . implode("\r\n\r\n", $err), "Система безопасности");

                }
            } else {
                UIError("Недостаточно данных для пакетной обработки прав!", "Система безопасности");
            }
            redirect(link2("/adm_empl", false), "Доступ в модуль " . $PLUGINS[$_POST["plugin_uid"]]["name"] . " закрыт успешно!");
        }

        if (isset($_POST["copyuser"])) {
            $source = $_POST["filter_empl_id"];
            if (
                is_array($_POST["seltechid"]) && count($_POST["seltechid"]) && $source
            ) {
                $s_rights = $DB->getCell2("SELECT `plugin_uid`, `access` FROM `user_access` WHERE `user_id`=" . $DB->F($source) . ";");
                if ($DB->errno()) UIError($DB->error());
                if (count($s_rights)) {
                    foreach ($_POST["seltechid"] as $target) {
                        if ($target == $source) continue;
                        $DB->query("DELETE FROM `user_access` WHERE `user_id`=" . $DB->F($target) . ";");
                        if ($DB->errno()) UIError($DB->error());
                        $sql = "INSERT INTO `user_access` (`user_id`, `plugin_uid`, `access`) VALUES ";
                        $sql_add = false;
                        foreach ($s_rights as $plug => $access) {
                            $sql_add[] = "(" . $DB->F($target) . ", " . $DB->F($plug) . ", " . $DB->F($access) . ")";
                        }
                        if (count($sql_add)) {
                            $sql .= implode(", ", $sql_add);
                            $DB->query($sql);
                            if ($DB->errno()) UIError($DB->error());

                            //redirect($this->getLink('adm_empl'), "Копирование прав доступа завершено успешно!");

                        } else {
                            UIError("Ошибка копирования прав доступа! Обратитесь к разработчикам!");
                        }
                    }
                    redirect(link2("/adm_empl", false), "Копирование прав доступа завершено успешно!");

                } else {
                    redirect(link2("/adm_empl", false), "У выбранного пользователя отсутствует список прав. Копирование прав доступа отменено!");
                }

            } else {
                UIError("Недостаточно данных для пакетной обработки прав!", "Система безопасности");
            }
        }
    }

    function getWTypes($usr)
    {
        global $DB;
        $sql = "SELECT `title` FROM `task_types` WHERE `id` IN  (SELECT `wtype_id` FROM `list_empl` WHERE `user_id`=" . $DB->F($usr) . ");";
        $DB->query($sql);
        $ret = "";
        while (list($title) = $DB->fetch()) {
            $ret .= $title . ", ";
        }
        $ret = preg_replace("/, $/", "", $ret);
        $DB->free();
        return $ret;
    }

    public function addmultiple(Request $request)
    {
        global $DB, $USER;

        if ($request->get('add')) {

            $userIds = $request->get('users');
            if (empty($userIds)) {
                UIError("Не выбран ни один пользователь!", "Ошибка");
            }

            $typeIdsRaw = $request->get('wtypes_id');
            if (empty($typeIdsRaw)) {
                UIError("Не выбран тип работ!", "Ошибка");
            }
            $typeIds = explode(',', $typeIdsRaw);

            $em = $this->getEm();

            $types = $em->getRepository(\models\TaskType::class)
                ->findById($typeIds);

            /** @var \models\User[] $users */
            $users = $em->getRepository(\models\User::class)
                ->findById($userIds);

            foreach ($users as $user) {
                foreach ($types as $type) {
                    $user->getTaskTypes()->add($type);
                }
            }
            $em->flush();

            redirect($this->getLink('viewlist'), "Добавлено " . count($users) . " записей.");
        }

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/addmultiple.tmpl.htm");
        else
            $tpl->loadTemplatefile("addmultiple.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        $sql = "SELECT `id`, `name` FROM `user_otdels` ORDER BY `order`, `name`";
        foreach ($otdels = $DB->getCell2($sql) as $otdel_id => $otdel_name) {
            $tpl->setCurrentBlock('ou');
            $tpl->setVariable('OTDEL_ID', $otdel_id);
            $tpl->setVariable('OTDEL_NAME', $otdel_name);

            $sql = "SELECT 
                id, fio, pos_id 
            FROM users 
            WHERE 
                otdel_id=" . $DB->F($otdel_id) . " 
                AND active 
                AND NOT EXISTS (SELECT * FROM list_empl WHERE list_empl.user_id = users.id) 
                AND NOT EXISTS (SELECT * FROM list_sc WHERE list_sc.chief = users.id) 
            ORDER BY fio";
            $DB->query($sql);
            $islist = false;
            while (list($user_id, $user_fio, $pos) = $DB->fetch()) {
                $islist = true;
                $posTitle = '';
                if ($pos && $postTemp = adm_users_plugin::getPos($pos)) {
                    $posTitle = '(' . $postTemp . ')';
                }
                $tpl->setCurrentBlock('user');
                $tpl->setVariable('USER_ID', $user_id);
                $tpl->setVariable('USER_FIO', $user_fio);
                $tpl->setVariable('USER_POS', $posTitle);
                $tpl->parse('user');
            }
            if (!$islist) {
                $tpl->setVariable("NOLIST", "hidden");
            }

            $tpl->parse('ou');
        }
        $DB->free();
        UIHeader($tpl);
        $tpl->show();
    }

    function edit()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        UIHeader($tpl);

        if ($id = $_REQUEST["id"]) {
            $id = str_replace("/", "", $id);
            $sc_id = $DB->getField("SELECT `sc_id` FROM `link_sc_user` WHERE `user_id`=" . $DB->F($id) . ";");
            if (class_exists("adm_users_plugin", true) && method_exists("adm_users_plugin", "getUser")) {
                $usr = new adm_users_plugin();
                if ($usrData = $usr->getUser($id)) {
                    $tpl->setVariable("USER_ID", $id);
                    $tpl->setVariable("SC_ID", $sc_id);
                    $tpl->setVariable("MUSER_FIO", $usrData["fio"]);
                    $tpl->setVariable("MUSER_POS", adm_users_plugin::getPos($usrData["pos_id"]));
                } else UIError("Ошибка БД");
            } else UIError("Не найден модуль &laquo;Пользователи системы&raquo;!");
            if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getSelList")) {
                $sc = new adm_sc_plugin();
                if ($data = $sc->getSelList($sc_id)) {
                    $tpl->setVariable("SC_LIST", $data);
                } else UIError("Ошибка БД");
            } else UIError("Не найден модуль &laquo;Сервисные Центры&raquo;!");
            $sql = "SELECT `id`,`title` FROM `task_types` WHERE `id` IN (SELECT `wtype_id` FROM `list_empl` WHERE `user_id`=" . $DB->F($id) . ");";
            $res = $DB->getCell2($sql);
            if ($res) {
                foreach ($res as $key => $val) {
                    $ids[] = $key;
                    $titles[] = $val;
                }
                $id_str = implode(",", $ids);
                $title_str = implode(", ", $titles);
                $tpl->setVariable("WTYPE_IDS", $id_str);
                $tpl->setVariable("WTYPE_VALS", $title_str);
            }

            $sql = "SELECT * FROM `virtual_techs` WHERE `virtual_id`=" . $DB->F($id) . " OR `user_id`=" . $DB->F($id) . ";";
            $vres = $DB->getRow($sql, true);
            if ($vres) {
                if ($vres["user_id"] == $id) {
                    $tpl->setVariable("TECHTYPE", "Виртуальный техник");

                    $vuser = adm_users_plugin::getUser($vres["virtual_id"]);
                    $tpl->setCurrentBlock("virtualtech");
                    $tpl->setVariable("VIRTUAL_EMPL_NAME", $vuser["fio"]);
                    $tpl->setVariable("VIRTUAL_EMPL_ID", $vuser["id"]);
                    $tpl->parse("virtualtech");
                } else {
                    $tpl->setVariable("TECHTYPE", "Основной техник");
                    $tpl->setCurrentBlock("alreadyvirtual");
                    $vuser = adm_users_plugin::getUser($vres["user_id"]);
                    $tpl->setVariable("REALUSER", "<a target='_blank' href='/adm_empl/edit?id=" . $vuser["id"] . "'>" . $vuser["fio"] . " (UID: " . $vuser["id"] . ")</a>");
                    $tpl->parse("alreadyvirtual");
                    $tpl->touchBlock("virttechbutton");
                }
            } else {
                $tpl->setVariable("TECHTYPE", "Виртуальный техник");
                $tpl->touchBlock("virtualtech-tb");
            }


            $sql = "SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT `area_id` FROM `link_user_area` WHERE `user_id`=" . $DB->F($id) . ");";
            $res = $DB->getCell2($sql);
            $ids = false;
            $titles = false;
            if ($res) {
                foreach ($res as $key => $val) {
                    $areas[$key] = $val;
                    $ids[] = $key;
                    $titles[] = trim($val);
                }
                $id_str = implode(",", $ids);
                $title_str = implode(", ", $titles);
                $tpl->setVariable("AREA_IDS", $id_str);
                $tpl->setVariable("AREA_VALS", $title_str);
            }
            $sql = "SELECT * FROM `list_empl_money_limits` WHERE `tech_id`=" . $DB->F($id) . ";";
            $moneyLimits = $DB->getRow($sql, true);
            if ($moneyLimits) {
                $tpl->setVariable("LIMIT2", $moneyLimits['limit2']);
            } else {
                $tpl->setVariable("LIMIT2", "15000.00");
            }
            $usesms = $DB->getField("SELECT `use_sms` FROM `list_empl_usesms` WHERE `user_id`=" . $DB->F($id) . ";");
            if ($usesms == "1") {
                $tpl->setVariable("USESMSBOT", "checked='checked'");
            }

            $locked = false;
            if ($locked) {
                $m = $DB->query("UPDATE `users` SET `allow_ext_sc`=0 WHERE `id`=" . $DB->F($id) . ";");
                $DB->free($m);
                $tpl->setVariable("ALLOWEXTSC", "disabled='disabled'");
            } else
                $currentExtSCState = adm_users_plugin::getExtSCState($id);

            if ($currentExtSCState) {
                $tpl->setVariable("ALLOWEXTSC", "checked='checked'");
            }

            $contrList = $DB->getCell("SELECT `contr_id` FROM `link_user_contr` WHERE `user_id`=" . $DB->F($id) . ";");
            $contrListTitle = $DB->getCell("SELECT `contr_title` FROM `list_contr` WHERE `id` IN (SELECT `contr_id` FROM `link_user_contr` WHERE `user_id`=" . $DB->F($id) . ");");
            if ($contrList) {
                $tpl->setVariable("CONTR_IDS", implode(",", $contrList));
                $tpl->setVariable("CONTR_VALS", implode(", ", $contrListTitle));
            }

        } else UIError("Не указан идентификатор Техника!");

        $DB->free();
        $tpl->show();
    }

    public function setTechnicianCoordinates(Request $request)
    {
        $area = $request->get('area');
        $userId = $request->get('user_id');
        /** @var \models\User $user */
        $user = $this->getEm()->find(\models\User::class, $userId);
        if (null === $user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User not found by id ' . $userId);
        }

        $polygon = new \CrEOF\Spatial\PHP\Types\Geography\Polygon($area['geometry']['coordinates']);
        $user->setArea($polygon);
        $this->getEm()->persist($user);
        $this->getEm()->flush();

        return new \Symfony\Component\HttpFoundation\Response('OK');
    }

    public function getTechnicianCoordinates(Request $request)
    {
        $userId = $request->get('user_id');
        /** @var \models\User $user */
        $user = $this->getEm()->find(\models\User::class, $userId);
        if (null === $user) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User not found by id ' . $userId);
        }

        return new \Symfony\Component\HttpFoundation\JsonResponse($user->getSerializedArea());
    }

    public function getServiceCenterCoordinates(Request $request)
    {
        $sc = $this->getEm()->find(\models\ListSc::class, $request->get('sc_id'));

        return new \Symfony\Component\HttpFoundation\JsonResponse($sc->getSerializedArea());
    }

    public function getAllTechniciansCoordinates(Request $request)
    {
        $criteria = [];
        if ($request->query->has('user_id')) {
            $criteria['serviceCenters'] = $this->getEm()
                ->find(\models\User::class, $request->get('user_id'))
                ->getMasterServiceCenters();
        }

        $users = $this->getEm()->getRepository(\models\User::class)
            ->findTechnicians($criteria);

        return new \Symfony\Component\HttpFoundation\JsonResponse(array_map(function (\models\User $user) {
            return [
                'id' => $user->getId(),
                'fio' => $user->getFio(),
                'area' => $user->getSerializedArea(),
            ];
        }, $users));
    }

    function savealias_ajax()
    {
        global $DB;
        $user = $_REQUEST['user_id'];
        $virtual = $_REQUEST['virtual_id'];
        if ($user != $virtual) {
            $DB->query("DELETE FROM `virtual_techs` WHERE `user_id`=" . $DB->F($user) . " OR `virtual_id`=" . $DB->F($user) . ";");
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
            } else {
                $sql = "INSERT INTO `virtual_techs` VALUES (" . $DB->F($user) . ", " . $DB->F($virtual) . ");";
                $DB->query($sql);
                if ($DB->errno()) {
                    $ret["error"] = $DB->error();
                } else {
                    $ret["ok"] = "ok";

                    /** @var \models\User $userEntity */
                    $userEntity = $this->getEm()->find(\models\User::class, $virtual);
                    $userEntity->setIsVirtual(true)
                        ->setRealUser($this->getEm()->getReference(\models\User::class, $user));

                    $this->getEm()->flush();

                }
            }
        } else {
            $ret["error"] = "Виртуальный техник не может иметь ID реального техника.";
        }
        echo json_encode($ret);
        exit();
    }

    function deletealias_ajax()
    {
        global $DB;
        $user = $_REQUEST['user_id'];
        $DB->query("DELETE FROM `virtual_techs` WHERE `user_id`=" . $DB->F($user) . ";");
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
        } else {
            $ret['ok'] = "ok";

            /** @var \models\User $userEntity */
            $userEntity = $this->getEm()->find(\models\User::class, $user);
            $virtuals = $userEntity->getVirtualUsers();
            foreach ($virtuals as $virtual)
                $virtual->setRealUser(null);

            $this->getEm()->flush();
        }
        echo json_encode($ret);
        exit();
    }

    function save()
    {
        global $DB, $USER;

        $err = array();
        $m = false;
        if ($_POST["sc"] == "none")
            UIError("Выберите Сервисный Центр!", "Ошибка", true, "Назад", $this->getLink() . "/edit/?id=" . $_POST["id"]);


        if ($id = preg_replace("/\//", "", $_POST["id"])) {
            $DB->query("DELETE FROM `link_user_contr` WHERE `user_id`=" . $DB->F($_POST["id"]) . ";");
            if ($DB->errno()) UIError($DB->error());
            if ($_POST["contrs_id"]) {
                $cArr = explode(",", $_POST["contrs_id"]);
                if ($cArr) {
                    foreach ($cArr as $cid) {
                        $sql = "INSERT INTO `link_user_contr` (`user_id`, `contr_id`) VALUES (" . $DB->F($_POST["id"]) . ", " . $DB->F($cid) . ");";
                        $DB->query($sql);
                        if ($DB->errno()) UIError($DB->error());
                    }
                }
            }

            $DB->query("DELETE FROM `list_empl` WHERE `user_id`=" . $DB->F($_POST["id"]) . ";");
            if ($DB->errno()) {
                UIError($DB->error());
            }
            $DB->query("DELETE FROM `link_user_area` WHERE `user_id`=" . $DB->F($_POST["id"]) . ";");
            if ($DB->errno()) {
                UIError($DB->error());
            }

            if (!$_POST["wtypes_id"]) {
                $err[] = "Не выбраны виды работ!";
            }

            if ($_POST["wtypes_id"] != "") {
                $wtypes = explode(",", $_POST["wtypes_id"]);

                foreach ($wtypes as $item) {
                    $sql_vals[] = "(" . $DB->F($id) . ", " . $DB->F($item) . ")";
                }
                if ($sql_vals) {
                    $sql = "INSERT INTO `list_empl` (`user_id`, `wtype_id`) VALUES " . implode(", ", $sql_vals);
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error() . $sql);
                }
            }
            $areas = explode(",", $_POST["areas_id"]);
            $sql_vals = false;
            foreach ($areas as $item) {
                $sql_vals[] = "(" . $DB->F($id) . ", " . $DB->F($item) . ")";
            }
            if ($sql_vals) {
                $sql = "INSERT INTO `link_user_area` (`user_id`, `area_id`) VALUES " . implode(", ", $sql_vals);
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error() . $sql);
            }

            $moneyLimits = $DB->getRow("SELECT * FROM `list_empl_money_limits` WHERE `tech_id`=" . $DB->F($_POST["id"]) . ";", true);
            if ($moneyLimits) {
                if (!$_REQUEST["limit2"]) $_REQUEST["limit2"] = "15000.00";
                $sql = "UPDATE `list_empl_money_limits` SET `limit2`=" . $_REQUEST["limit2"] . " WHERE `tech_id`=" . $DB->F($_POST["id"]) . ";";
                $m = $DB->query($sql);
                if ($DB->errno()) UIError($DB->error() . $sql);
                $DB->free($m);
            } else {
                if (!$_REQUEST["limit2"]) $_REQUEST["limit2"] = "15000.00";
                $sql = "INSERT INTO `list_empl_money_limits` (`tech_id`, `limit2`) VALUES (" . $DB->F($_POST["id"]) . ", " . $_REQUEST["limit2"] . ");";
                $m = $DB->query($sql);
                if ($DB->errno()) UIError($DB->error() . $sql);
                $DB->free($m);
            }

            if (sizeof($err)) UIError($err);
            if (isset($_POST['sc'])) {
                $sql = "SELECT `sc_id` FROM `link_sc_user` WHERE `user_id`=" . $DB->F($_POST["id"]) . ";";
                $sc = $DB->getField("SELECT `sc_id` FROM `link_sc_user` WHERE `user_id`=" . $DB->F($_POST["id"]) . ";");
                if (!$sc) {
                    # EF: Убираем доступ со старого СЦ, если он был
                    $sql = "INSERT INTO `link_sc_user` (`user_id`, `sc_id`) VALUES (" . $DB->F($_POST["id"]) . ", " . $DB->F($_POST["sc"]) . ");";
                } else {
                    $sql = "DELETE FROM user_sc_access WHERE user_id = " . $DB->F($_POST["id"]) . " AND sc_id = " . $DB->F($sc);
                    $DB->query($sql);
                    $sql = "REPLACE INTO user_sc_access (user_id, sc_id) VALUES (" . $DB->F($_POST["id"]) . ", " . $DB->F($_POST["sc"]) . " )";
                    $DB->query($sql);
                    $sql = "UPDATE `link_sc_user` SET `sc_id`=" . $DB->F($_POST["sc"]) . " WHERE `user_id`=" . $DB->F($_POST["id"]) . ";";
                }
                // die($sc."<br />".$sql);
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error() . $sql);
            }
            $DB->query("DELETE FROM `list_empl_usesms` WHERE `user_id`=" . $DB->F($id) . ";");
            if ($_POST["usesmsbot"] == "on") {
                $DB->query("INSERT INTO `list_empl_usesms` VALUES (" . $DB->F($id) . ", 1);");
            }


            if ($_REQUEST['virtual_empl_id'] != "") {
                $DB->query("DELETE FROM `virtual_techs` WHERE `user_id`=" . $DB->F($id) . " OR `virtual_id`=" . $DB->F($id) . ";");
                $DB->free();
                $sql = "INSERT INTO `virtual_techs` VALUES (" . $DB->F($id) . ", " . $DB->F($_REQUEST['virtual_empl_id']) . ");";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error() . $sql);
                $DB->free();
            }
        } else {
            UIError("Не указан идентификатор Техника");
        }
        $DB->free();
        redirect(Plugin::getLinkStatic("adm_empl/edit?id=" . $id), "Изменения сохранены. "/*.implode("\r\n", $err)*/);
    }

    function getEmplListBySCID($sc_id)
    {
        global $DB;
        $DB->query("SELECT `id`, `fio` FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($sc_id) . ");");
        if ($DB->errno()) {
            $DB->free();
            return "<strong style='color:#FF0000;'>Ошибка БД</strong>";
        }
        if (!$DB->num_rows()) {
            $DB->free();
            return " - ";
        }
        $res = "";
        while (list($id, $fio) = $DB->fetch()) {
            $res .= "<a target='_target' href=" . $this->getLink() . "/edit/?id=" . $id . ">" . $fio . "</a>, ";
        }
        $res = preg_replace("/, $/", "", $res);
        $DB->free();
        return $res;
    }

    function getEmplListRowsBySCID($sc_id)
    {
        global $DB;
        return $DB->getCell2("SELECT `id`, `fio` FROM `users` WHERE `active` AND `id` NOT IN (SELECT `virtual_id` FROM `virtual_techs`) AND `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `fio`;");
    }

    function getChiefListBySCID($sc_id)
    {
        global $DB;
        $DB->query("SELECT `id`, `fio` FROM `users` WHERE `active` AND  `id` IN (SELECT `user_id` FROM `link_sc_chiefs` WHERE  `sc_id`=" . $DB->F($sc_id) . ");");
        if ($DB->errno()) {
            $DB->free();
            return "<strong style='color:#FF0000;'>Ошибка БД</strong>";
        }
        if (!$DB->num_rows()) {
            $DB->free();
            return " - ";
        }
        $res = "";
        while (list($id, $fio) = $DB->fetch()) {
            $res .= "<a target='_target' href=" . $this->getLink() . "/edit/?id=" . $id . ">" . $fio . "</a><br />";
        }
        $DB->free();
        return $res;
    }

    function getInvAndFreeEmpl($sc_id)
    {
        global $DB;
        $DB->query("SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($sc_id) . " ORDER BY `user_id`;");
        if ($DB->errno()) return "<strong style='color:#FF0000;'>Ошибка БД</strong>";
        $selID = false;
        if ($DB->num_rows()) {
            while (list($id) = $DB->fetch()) {
                $selID[] = $id;
            }

        }
        $DB->free();
        $DB->query("SELECT `id`, `fio`, `pod_id` FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `list_empl`) AND `id` NOT IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`!=" . $DB->F($sc_id) . ") ORDER BY `id`;");
        if ($DB->errno()) return "<strong style='color:#FF0000;'>Ошибка БД</strong>";
        if ($DB->num_rows()) {
            $res = "<ul>";
            while (list($id, $fio) = $DB->fetch()) {
                if (in_array($id, $selID)) {
                    $res .= "<li><input type='checkbox' name='empl-$id' checked='checked' id='empl-$id' /><label style='width:300px;' for='empl-$id'>" . $fio . "</label></li>";
                } else {
                    $res .= "<li><input type='checkbox' name='empl-$id' id='empl-$id' /><label style='width:300px;' for='empl-$id'>" . $fio . "</label></li>";
                }
            }
            $res .= "</ul>";
        } else {
            return " - ";
        }
        $DB->free();

        return $res;
    }

    function getInvEmpl($sc_id, $wtype_list = false)
    {
        global $DB, $USER;
        $user_id = $USER->getId();
        if (!$user_id) {
            return false;
        }

        $sql_add = " and id in (SELECT distinct lsu.user_id FROM link_sc_user lsu, user_sc_access usa where usa.sc_id = lsu.sc_id and usa.user_id = " . $user_id . ")";
        //die($sql_add);

        $sql_is_virt_attr = 'SELECT count(`virtual_id`) FROM `virtual_techs` WHERE `virtual_id`=users.id';
        $isTech = User::isTech($user_id);
        $isChefTech = User::isChiefTech($user_id);
        if ($isChefTech) {
            $sc_filter = "";
            if ($sc_id > 0) {
                $sc_filter = " where link_sc_user.sc_id = " . $DB->F($sc_id);
            }
            if (!$wtype_list)
                $DB->query("SELECT `id`, `fio`, `pos_id`, `phones`, if( ($sql_is_virt_attr) > 0, 1, 0) as is_virtual  FROM `users` WHERE `active` AND  `id` IN (SELECT `user_id` FROM `link_sc_user` $sc_filter) $sql_add ORDER BY `fio`;");
            else {
                if (is_array($wtype_list))
                    $wtype_list = preg_replace("/,$/", "", implode(",", $wtype_list));
                $DB->query("SELECT `id`, `fio`, `pos_id`, `phones`, if( ($sql_is_virt_attr) > 0, 1, 0) as is_virtual  FROM `users` WHERE `active` AND  `id` IN (SELECT `user_id` FROM `link_sc_user` $sc_filter) AND `id` IN (SELECT `user_id` FROM `list_empl` WHERE `wtype_id` IN (" . $wtype_list . ")) $sql_add ORDER BY `fio`;");

            }
        } else {
            if ($isTech) {
                $DB->query("SELECT `id`, `fio`, `pos_id`, `phones`, if( ($sql_is_virt_attr) > 0, 1, 0) as is_virtual  FROM `users` WHERE `active` AND `id` =" . $DB->F($user_id) . " $sql_add ORDER BY `fio`;");
            } else {
                if (!$sc_id) {
                    if ($wtype_list) {
                        if (is_array($wtype_list))
                            $wtype_list = preg_replace("/,$/", "", implode(",", $wtype_list));

                        $DB->query("SELECT `id`, `fio`, `pos_id`, `phones`, if( ($sql_is_virt_attr) > 0, 1, 0) as is_virtual  FROM `users` WHERE `active` AND `id` IN (SELECT DISTINCT `user_id` FROM `link_sc_user`)  AND `id` IN (SELECT `user_id` FROM `list_empl` WHERE `wtype_id` IN (" . $wtype_list . ")) $sql_add ORDER BY `fio`;");
                    } else {
                        $sql = "SELECT `id`, `fio`, `pos_id`, `phones`, if( ($sql_is_virt_attr) > 0, 1, 0) as is_virtual  FROM `users` WHERE `active` AND `id` IN (SELECT DISTINCT `user_id` FROM `link_sc_user`) $sql_add ORDER BY `fio`;";
                        $DB->query($sql);
                    }
                } else {
                    if ($wtype_list) {
                        if (is_array($wtype_list))
                            $wtype_list = preg_replace("/,$/", "", implode(",", $wtype_list));
                        $DB->query("SELECT `id`, `fio`, `pos_id`, `phones`, if( ($sql_is_virt_attr) > 0, 1, 0) as is_virtual  FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($sc_id) . ") AND `id` IN (SELECT `user_id` FROM `list_empl` WHERE `wtype_id` IN (" . $wtype_list . ")) $sql_add ORDER BY `fio`;");
                    } else {
                        $DB->query("SELECT `id`, `fio`, `pos_id`, `phones`, if( ($sql_is_virt_attr) > 0, 1, 0) as is_virtual FROM `users` WHERE `active` AND `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($sc_id) . ") $sql_add ORDER BY `fio`;");
                    }
                }
            }
        }
        if ($DB->errno()) return [];
        if (!$DB->num_rows()) return [];

        $uList = [];
        while ($row = $DB->fetch(true)) {
            $uList[] = $row;
        }

        if ($isTech) {
            $virtTechID = adm_empl_plugin::isVirtual($user_id);
            if ($virtTechID) {
                $vUser = adm_users_plugin::getUser($virtTechID);
                $uList[] = [
                    'id' => $vUser["id"],
                    'fio' => $vUser["fio"],
                    'pos_id' => $vUser["pos_id"],
                    'phones' => null,
                    'is_virtual' => false
                ];
            }
        }
        $DB->free();

        return $uList;
    }

    static function isVirtual($id)
    {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT `virtual_id` FROM `virtual_techs` WHERE `user_id`=" . $DB->F($id) . ";";
        $vid = $DB->getField($sql);
        return $vid;
    }

    public function delete(Request $request)
    {
        if (!$request->query->has('id')) {
            UIError("Не указан идентификатор Техника");
        }

        if ($this->isBusy($request->get('id'))) {
            UIError("Выбранный Техник связан с активными заявками / СЦ. Удаление невозможно.");
        }

        /** @var \models\User $user */
        $user = $this->getEm()->find(\models\User::class, $request->get('id'));

        $this->getEm()->transactional(function (EntityManagerInterface $em) use ($user, $listEmpls) {
            $user->getTaskTypes()->clear();
            $user->getWarehouses()->clear();
            $em->flush();
        });

        redirect($this->getLink(), "Техник успешно удален.");
    }

    function isBusy($id)
    {
        global $DB;
        $r = $DB->getField("SELECT COUNT(*) FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($id) . " AND DATE_FORMAT(`sched_date`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d")) . ";");
        if ($r > 0) return true;
        return false;
    }

}
