<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;



 
class reports_tt_gp_types_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $typelist = array("accidents"=> "ТТ", "gp"=>"ГП");
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date("d.m.Y")." - 1 day")));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            $tpl->setVariable("FILTER_TYPE_OPTIONS", array2options($typelist, $_REQUEST["type_id"]));
            $tpl->setVariable("USENONZERO", $_POST["usenonzerotype"]!="" ? "checked='checked'" : "");
             
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || isset($_POST["createxlsversion"])) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("TASK_TYPE", $typelist[$_REQUEST['type_id']]); 
            //$rtpl->setVariable("MONTH", $month[$_POST["month"]]);
            //$rtpl->setVariable("YEAR", $_POST["year"]);
            $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
            $rtpl->setVariable("DATETO", $_POST['dateto']);
            
            if ($_POST["type_id"] == "accidents") {
                if ($_POST["usenonzerotype"]) {
                    $add_sql = " AND t.tttype_id AND t.ttsubtype_id ";
                } else $add_sql = "";
                $sql = "SELECT count(distinct(t.task_id)) as total FROM `tickets` as t LEFT JOIN `tasks` AS tsk ON (tsk.plugin_uid='accidents' AND tsk.id=t.task_id) WHERE 
                tsk.id IN ((SELECT task_id FROM `task_comments` WHERE `status_id` IN (61) 
                                            AND (DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_REQUEST['datefrom'])))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_REQUEST['dateto']))).")))".$add_sql;
                
                //die($sql);
                $totalTT = $DB->getField($sql);
                $sql = "SELECT type.id, type.title, subtype.id as sub_id, subtype.title as subtitle,
                (SELECT count(t.task_id) FROM `tickets` as t LEFT JOIN `tasks` AS tsk ON (tsk.plugin_uid='accidents' AND tsk.id=t.task_id) WHERE t.tttype_id=type.id AND t.ttsubtype_id=subtype.id AND 
                tsk.id IN ((SELECT task_id FROM `task_comments` WHERE `status_id` IN (61) 
                                            AND (DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_REQUEST['datefrom'])))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_REQUEST['dateto']))).")))) as count
                FROM `tt_types` AS type LEFT JOIN `tt_subtypes` AS subtype ON subtype.type_id=type.id ORDER BY `count` DESC;";
                $DB->query($sql);
                if ($DB->num_rows()) {
                    while($r = $DB->fetch(true)) {
                        if ($r['title']!= "Не выбирать") {
                            $rtpl->setCurrentBlock("rep_row");
                            $rtpl->setVariable("TYPE", $r["title"]);
                            $rtpl->setVariable("SUBTYPE", $r["subtitle"]);
                            $rtpl->setVariable("TOTAL", $r["count"]);
                            $rtpl->setVariable("PERCENTTOTAL", $totalTT>0 ? (round($r["count"]/$totalTT, 3)*100)."%" : 0);
                            $rtpl->parse("rep_row");
                        }
                    }
                } else {
                    $rtpl->touchBlock("empty");
                }
            
            
            
            } else {
                //$sql = "SELECT count(t.task_id) as total FROM `gp` as t LEFT JOIN `tasks` AS tsk ON (tsk.plugin_uid='gp' AND tsk.id=t.task_id)  WHERE DATE_FORMAT(tsk.date_reg, '%Y-%m')=".$DB->F($_POST['year']."-".$_POST["month"]);
                //$totalGP = $DB->getField($sql);
                //$sql = "SELECT type.id, type.title, subtype.id as sub_id, subtype.title as subtitle FROM `gp_types` AS type LEFT JOIN `gp_subtypes` AS subtype ON subtype.type_id=type.id;";            
                    
            
                if ($_POST["usenonzerotype"]) {
                    $add_sql = " AND t.gptype_id AND t.gpsubtype_id ";
                } else $add_sql = "";
                $sql = "SELECT count(distinct(t.task_id)) as total FROM `gp` as t LEFT JOIN `tasks` AS tsk ON (tsk.plugin_uid='gp' AND tsk.id=t.task_id) WHERE 
                tsk.id IN ((SELECT task_id FROM `task_comments` WHERE `status_id` IN (71) 
                                            AND (DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_REQUEST['datefrom'])))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_REQUEST['dateto']))).")))".$add_sql;
                $totalGP  = $DB->getField($sql);
                $sql = "SELECT type.id, type.title, subtype.id as sub_id, subtype.title as subtitle,
                (SELECT count(t.task_id) FROM `gp` as t LEFT JOIN `tasks` AS tsk ON (tsk.plugin_uid='gp' AND tsk.id=t.task_id) WHERE t.gptype_id=type.id AND t.gpsubtype_id=subtype.id AND 
                tsk.id IN ((SELECT task_id FROM `task_comments` WHERE `status_id` IN (71) 
                                            AND (DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_REQUEST['datefrom'])))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_REQUEST['dateto']))).")))) as count
                FROM `gp_types` AS type LEFT JOIN `gp_subtypes` AS subtype ON subtype.type_id=type.id ORDER BY `count` DESC;";
                $DB->query($sql);
                if ($DB->num_rows()) {
                    while($r = $DB->fetch(true)) {
                        if ($r['title']!= "Не выбирать") {
                            $rtpl->setCurrentBlock("rep_row");
                            $rtpl->setVariable("TYPE", $r["title"]);
                            $rtpl->setVariable("SUBTYPE", $r["subtitle"]);
                            $rtpl->setVariable("TOTAL", $r["count"]);
                            $rtpl->setVariable("PERCENTTOTAL", $totalGP>0 ? (round($r["count"]/$totalGP, 3)*100)."%" : 0);
                            $rtpl->parse("rep_row");
                        }
                    }
                } else {
                    $rtpl->touchBlock("empty");
                }
            
            }
            
            
            
            
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_tt_gp_types_".$_POST['datefrom']."-".$_POST['dateto'].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 
    
}
?>