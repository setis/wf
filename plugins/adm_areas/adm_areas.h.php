<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Справочник Районов";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник Районов";
    $PLUGINS[$plugin_uid]['events']['getAreaListA'] = "Выборка района";
    $PLUGINS[$plugin_uid]['events']['getAreaList_ajax'] = "Выборка района";
    $PLUGINS[$plugin_uid]['events']['getAreaListA1'] = "Выборка района";
    $PLUGINS[$plugin_uid]['events']['getAreaList1_ajax'] = "Выборка района";
    $PLUGINS[$plugin_uid]['events']['getAreaListA2'] = "Выборка района";


}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование Района";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление Района";
    $PLUGINS[$plugin_uid]['events']['save'] = "Редактирование Района";
    $PLUGINS[$plugin_uid]['events']['getregion'] = "Выборка региона";
    $PLUGINS[$plugin_uid]['events']['getareak'] = "Выборка района";
    $PLUGINS[$plugin_uid]['events']['getcity'] = "Выборка города регионального подчинения";
    $PLUGINS[$plugin_uid]['events']['gettown'] = "Выборка города административного подчинения";
    $PLUGINS[$plugin_uid]['events']['ckeckKCode'] = "Проверка КЛАДР-кода на наличие улиц или домов для присвоения району";
}
?>