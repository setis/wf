<?php
use classes\HTML_Template_IT;
use classes\Plugin;
use Symfony\Component\HttpFoundation\Request;

class adm_areas_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getArea($id)
    {
        global $DB;

        return $DB->getField("SELECT `title` FROM `list_area` WHERE `id`=" . $DB->F($id) . ";");
        $DB->free();
    }

    static function getArea1List($sc_id)
    {
        global $DB;
        if (!$sc_id) return false;
        $DB->query("SET group_concat_max_len = 2048");
        //$sql = "SELECT `nbn_department_key`, ";
        $sql = "SELECT `nbn_department_key`, GROUP_CONCAT(title SEPARATOR ', '), IF(`title` LIKE '%Ремонт%', 1, 0) as ord FROM `list_area` WHERE `nbn_department_key`!='' AND `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($sc_id) . ") GROUP BY `nbn_department_key` ORDER BY `ord`,`title`;";
        $sql1 = "SELECT `nbn_department_key`, GROUP_CONCAT(title SEPARATOR ', '), IF(`title` LIKE '%Ремонт%', 1, 0) as ord FROM `list_area` WHERE `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($sc_id) . ") GROUP BY `nbn_department_key` ORDER BY `ord`,`title`;";
        //$sql = "SELECT `id`,`title`, IF(`title` LIKE '%Ремонт%', 1, 0) as ord FROM `list_area` WHERE `nbn_department_key`!='' AND `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=".$DB->F($sc_id).") ORDER BY `ord`,`title`;";
        $m = $DB->getCell2($sql);
        if ($m) return $m;
        else return $DB->getCell2($sql1);
    }

    static function getArea2List($sc_id)
    {
        global $DB;
        if (!$sc_id) return false;
        $DB->query("SET group_concat_max_len = 2048");
        //$sql = "SELECT `nbn_department_key`, ";
        //$sql = "SELECT `nbn_department_key`, GROUP_CONCAT(title SEPARATOR ', '), IF(`title` LIKE '%Ремонт%', 1, 0) as ord FROM `list_area` WHERE `nbn_department_key`!='' AND `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=".$DB->F($sc_id).") GROUP BY `nbn_department_key` ORDER BY `ord`,`title`;";
        $sql = "SELECT `id`,`title`, IF(`title` LIKE '%Ремонт%', 1, 0) as ord FROM `list_area` WHERE `nbn_department_key`!='' AND `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `ord`,`title`;";

        return $DB->getCell2($sql);
    }

    function getFullAreaList($sel = false, $cId)
    {
        global $DB;

        $res = "<table>";
        $res .= "<tr><td><br /><strong>Без округов</strong>";
        $DB->query("SELECT `id`, `title` FROM `list_area` WHERE `dis_id`='0' AND `id` NOT IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`!='$cId');");
        if ($DB->errno()) return "<strong style='color:#FF0000'>Ошибка БД</strong>";
        $res .= "<ul style='list-style:none;'>";
        while (list($id, $title) = $DB->fetch()) {
            if (in_array($id, $sel)) {
                $res .= "<li style='float:left; height:30px;'><input type='checkbox' name='area-" . $id . "' id='area-" . $id . "' checked='checked' /><label for='area-" . $id . "'>" . $title . "</label></li>";
            } else {
                $res .= "<li style='float:left; height:30px;'><input type='checkbox' name='area-" . $id . "' id='area-" . $id . "' /><label for='area-" . $id . "'>" . $title . "</label></li>";
            }
        }

        $res .= "</td></tr>";
        $DB->free();
        $sql = "SELECT `id`, `title` FROM `list_district` ORDER BY `title`";
        foreach ($dis = $DB->getCell2($sql) as $dis_id => $dis_name) {
            $res .= "<tr><td><br /><strong>$dis_name</strong>";
            $DB->query("SELECT `id`, `title` FROM `list_area` WHERE `dis_id`='$dis_id' AND `id` NOT IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`!='$cId');");
            if ($DB->errno()) return "<strong style='color:#FF0000'>Ошибка БД</strong>";
            $res .= "<ul style='list-style:none;'>";
            while (list($id, $title) = $DB->fetch()) {
                if (in_array($id, $sel)) {
                    $res .= "<li style='float:left; height:30px;'><input type='checkbox' name='area-" . $id . "' id='area-" . $id . "' checked='checked' /><label for='area-" . $id . "'>" . $title . "</label></li>";
                } else {
                    $res .= "<li style='float:left; height:30px;'><input type='checkbox' name='area-" . $id . "' id='area-" . $id . "' /><label for='area-" . $id . "'>" . $title . "</label></li>";
                }
            }
            $DB->free();
            $res .= "</td></tr>";
        }
        $DB->free();

        $res .= "</table>";

        return $res;

    }

    function getAreaListBySCID($sc_id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_area` WHERE `id` IN  (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($sc_id) . ");";
        $DB->query($sql);
        $ret = "";
        while (list($title) = $DB->fetch()) {
            $ret .= $title . ", ";
        }
        $DB->free();
        $ret = preg_replace("/, $/", "", $ret);

        return $ret;
    }

    function viewlist()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
        $sql = "SELECT area.id, area.title, area.desc, dis.title, area.kladr_code, area.nbn_department_key  FROM `list_area` AS `area` LEFT JOIN list_district AS dis ON dis.id=area.dis_id ORDER BY area.kladr_code, dis.title;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        while (list($id, $title, $desc, $district, $kladr, $nbn_department_key) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->setVariable("VLI_DESC", $desc);
            $tpl->setVariable("VLI_DISTRICT", $district ? $district : '--');
            $tpl->setVariable("VLI_KLADR_CODE", $kladr ? $kladr : '--');
            $tpl->setVariable("VLI_NBN_DEPARTMENT_KEY", $nbn_department_key ? $nbn_department_key : '--');

            $tpl->parse("vl_item");
        }
        UIHeader($tpl);
        $DB->free();

        $tpl->show();
    }

    function getAreaListA()
    {
        global $DB;

        $sc_id = $_POST["sc_id"];
        if ($sc_id) {
            echo "<option value='0'>-- все --</option>" . array2options($DB->getCell2("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `title`;"));
        } else {
            echo "<option value='0'>-- все --</option>";
        }
    }

    function getAreaList_ajax(Request $request)
    {
        global $DB;

        echo "<option value='0'>-- все --</option>" . array2options($DB->getCell2("SELECT `id`,`title` FROM `list_area` ORDER BY `title`;"));
    }

    function getAreaListA1()
    {
        global $DB;

        $sc_id = $_POST["sc_id"];
        if ($sc_id)
            echo array2options($DB->getCell2("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `title`;"));
        else
            echo "";
    }

    function getAreaListA2()
    {
        global $DB;

        $sc_id = $_POST["sc_id"];
        if ($sc_id) {
            echo array2options($DB->getCell2("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id` IN (" . preg_replace("/,$/", "", implode(",", $sc_id)) . ")) ORDER BY `title`;"));
        } else
            echo "";
    }

    function getAreaList1_ajax($sel = false)
    {
        global $DB;

        echo array2options($DB->getCell2("SELECT `id`,`title` FROM `list_area` ORDER BY `title`;"), $sel);

    }

    function getAreaList($sel = false)
    {
        global $DB;
        $res = '<option value="0">-- не указан</option>'."\r\n";
        $res .= array2options($DB->getCell2("SELECT `id`,`title` FROM `list_area` ORDER BY `title`;"), $sel);
        return $res;
    }

    function selAreasList()
    {
        global $DB;
        $sql = "SELECT * FROM `list_area` WHERE 1;";
        $DB->query($sql);
        if ($DB->errno()) return "Ошибка.";
        if (!$DB->num_rows()) return "Записи отсутствуют.";
        $res = "<select name='area_id'><option value='0'>-- выберите Район --</option>";
        while (list($id, $title, $desc) = $DB->fetch()) {
            $res .= "<option value='$id'>$title ($desc)</option>";
        }
        $res .= "</select>";
        $DB->free();

        return $res;
    }

    function edit()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        if ($contr_id = $_REQUEST['area_id']) {
            $sql = "SELECT * FROM `list_area` WHERE `id`=" . $DB->F($contr_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Район с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);
            $kladr = $result["kladr_code"];
            $tpl->setVariable("KLADRCODE", $kladr);
            $tpl->setVariable("AREA_ID", $result["id"]);
            $tpl->setVariable("AREA_TITLE", $result["title"]);
            $tpl->setVariable("AREA_DESC", $result["desc"]);
            $tpl->setVariable("NBN_DEPARTMENT_KEY", $result["nbn_department_key"]);
            $regionKladr = substr($kladr, 0, 2) . "00000000000";
            $regionTitle = $DB->getRow("SELECT k.name, sb.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS sb ON sb.scname=k.socr WHERE k.code=" . $DB->F($regionKladr) . ";", true);
            $tpl->setVariable("REGION_KLADR", $regionKladr);
            $tpl->setVariable("REGION_TITLE", implode(' ', $regionTitle));
            $DB->free();
            if (substr($kladr, 2, 3) != "000") {
                $areaKladr = substr($kladr, 0, 5) . "00000000";
                $areaTitle = $DB->getRow("SELECT k.name, sb.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS sb ON sb.scname=k.socr WHERE k.code=" . $DB->F($areaKladr) . ";", true);
                $tpl->setVariable("KAREA_KLADR", $areaKladr);
                $tpl->setVariable("KAREA_TITLE", implode(' ', $areaTitle));
                $DB->free();
            }
            if (substr($kladr, 5, 3) != "000") {
                $cityKladr = substr($kladr, 0, 8) . "00000";
                $cityTitle = $DB->getRow("SELECT k.name, sb.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS sb ON sb.scname=k.socr WHERE k.code=" . $DB->F($cityKladr) . ";", true);
                $tpl->setVariable("CITY_KLADR", $cityKladr);
                $tpl->setVariable("CITY_TITLE", implode(' ', $cityTitle));
                $DB->free();
            }
            if (substr($kladr, 8, 3) != "000") {
                $townKladr = substr($kladr, 0, 11) . "00";
                $townTitle = $DB->getRow("SELECT k.name, sb.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS sb ON sb.scname=k.socr WHERE k.code=" . $DB->F($townKladr) . ";", true);
                $tpl->setVariable("TOWN_KLADR", $townKladr);
                $tpl->setVariable("TOWN_TITLE", implode(' ', $townTitle));
                $DB->free();
            }
        }

        UIHeader($tpl);
        $tpl->setVariable("AREA_DIST", $this->selDisList($result["dis_id"]));
        $tpl->show();
        $DB->free();

    }

    function selDisList($dis_id)
    {
        global $DB;
        $sql = "SELECT * FROM `list_district` WHERE 1;";
        $DB->query($sql);
        if ($DB->errno()) return "Ошибка.";
        if (!$DB->num_rows()) return "Записи отсутствуют.";
        $res = "<select name='dis_id'><option value='0'>-- выберите Округ --</option>";
        while (list($id, $title, $desc) = $DB->fetch()) {
            if ($dis_id && $dis_id == $id)
                $res .= "<option selected='selected' value='$id'>$title ($desc)</option>";
            else
                $res .= "<option value='$id'>$title ($desc)</option>";
        }
        $res .= "</select>";
        $DB->free();

        return $res;
    }

    function save()
    {
        global $DB, $USER;
        $err = [];
        if (!$_POST['title']) $err[] = "Не заполнено Название Района";
        if (sizeof($err)) UIError($err);
        if ($_POST['area_id']) $sql = "UPDATE `list_area` SET `title`=" . $DB->F($_POST['title']) . ", `desc`=" . $DB->F($_POST['desc']) . ", `dis_id`=" . $DB->F($_POST['dis_id']) . ", `kladr_code`=" . $DB->F($_POST['kladr_code']) . ", `nbn_department_key`=" . $DB->F($_POST['nbn_department_key'], true) . " WHERE `id`=" . $DB->F($_POST['area_id']);
        else {
            $sql = "INSERT INTO `list_area` (`title`, `desc`, `dis_id`, `kladr_code`, `nbn_department_key`) VALUES(" . $DB->F($_POST['title']) . ", " . $DB->F($_POST['desc']) . ", " . $DB->F($_POST['dis_id']) . ", " . $DB->F($_POST["kladr_code"]) . ", " . $DB->F($_POST['nbn_department_key'], true) . ")";
        }
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());

        if (!$_POST['area_id']) {
            $_POST['area_id'] = $DB->insert_id();
        }
        $DB->free();
        redirect($this->getLink('viewlist'), "Район сохранен. ID: " . $_POST["area_id"]);
    }

    function delete()
    {
        global $DB;
        if (!($id = $_REQUEST['area_id'])) $err[] = "Не указан Район";
        if (sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST["area_id"])) UIError("Выбранный Район связан с действующими СЦ! Удаление невозможно.");
        $DB->query("DELETE FROM `list_area` WHERE `id`='" . $_REQUEST['area_id'] . "';");
        redirect($this->getLink(), "Район успешно удален.");

    }

    function isBusy($id)
    {
        global $DB;
        if ($cnt = $DB->getField("SELECT COUNT(*) FROM `link_sc_area` WHERE `area_id`=" . $DB->F($id) . ";")) return true;
        else
            return false;
    }

    function getregion()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/suggest.tmpl.htm");
        $_POST['q'];

        $sql = "SELECT k.name, s.socrname, k.code FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON k.socr=s.scname WHERE k.code REGEXP '[0-9]{2}[0]{9}[0]{1}[0]{1}' AND k.name LIKE " . $DB->F(preg_replace("/ {1,}/", "", $_POST["q"]) . "%") . " GROUP BY k.code ORDER BY k.name;";
        $DB->query($sql);
        if (!$DB->errno() && $DB->num_rows()) {
            while (list($name, $socr, $code) = $DB->fetch()) {
                $tpl->setCurrentBlock("row");
                $tpl->setVariable("NAME_REAL", $socr == "Город" ? "<strong>" . $name . "</strong>" : $name);
                $tpl->setVariable("NAME", $name);
                $tpl->setVariable("ID", $code);
                $tpl->setVariable("SOCR", $socr);
                $tpl->parse("row");
            }
        } else {
            $tpl->touchBlock("norows");
        }
        $tpl->show();
        $DB->free();
    }

    function getareak()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/suggest.tmpl.htm");
        if (@$_POST['parent_id'] != '') {
            $_POST['q'];
            $region = substr($_POST['parent_id'], 0, 2);
            $sql = "SELECT k.name, s.socrname, k.code FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON k.socr=s.scname WHERE k.code REGEXP '" . $region . "[0-9]{3}[0]{6}[0]{1}[0]{1}' AND k.code!=" . $DB->F($_POST['parent_id']) . " AND k.name LIKE " . $DB->F(preg_replace("/ {1,}/", "", $_POST["q"]) . "%") . " GROUP BY k.code ORDER BY k.name;";
            $DB->query($sql);
            if (!$DB->errno() && $DB->num_rows()) {
                while (list($name, $socr, $code) = $DB->fetch()) {
                    $tpl->setCurrentBlock("row");
                    $tpl->setVariable("NAME_REAL", $socr == "Город" ? "<strong>" . $name . "</strong>" : $name);
                    $tpl->setVariable("NAME", $name);
                    $tpl->setVariable("ID", $code);
                    $tpl->setVariable("SOCR", $socr);
                    $tpl->parse("row");
                }
            } else {
                $tpl->touchBlock("norows");
            }
        } else {
            $tpl->touchBlock("noparentselected");
        }
        $tpl->show();
        $DB->free();
    }

    function getcity()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/suggest.tmpl.htm");
        $_POST['q'];
        if (@$_POST['parent_id'] != '') {
            $region = substr($_POST['parent_id'], 0, 2);
            $area = substr($_POST['parent_id'], 2, 3);
            $area = ($area == "000" ? "[0-9]{3}" : $area);
            $sql = "SELECT k.name, s.socrname, k.code FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON k.socr=s.scname WHERE k.code REGEXP '" . $region . $area . "(([1-9]{1}[0-9]{1}[0-9]{1})|([0-9]{1}[1-9]{1}[0-9]{1})|([0-9]{1}[0-9]{1}[1-9]{1}))000[0]{1}[0]{1}' AND k.code!=" . $DB->F($_POST['parent_id']) . " AND k.name LIKE " . $DB->F(preg_replace("/ {1,}/", "", $_POST["q"]) . "%") . " GROUP BY k.code ORDER BY k.name;";
            $DB->query($sql);
            if (!$DB->errno() && $DB->num_rows()) {
                while (list($name, $socr, $code) = $DB->fetch()) {
                    $tpl->setCurrentBlock("row");
                    $tpl->setVariable("NAME_REAL", $socr == "Город" ? "<strong>" . $name . "</strong>" : $name);
                    $tpl->setVariable("NAME", $name);
                    $tpl->setVariable("ADDINFO", " (" . $code . ")");
                    $tpl->setVariable("ID", $code);
                    $tpl->setVariable("SOCR", $socr);
                    $tpl->parse("row");
                }
            } else {
                $tpl->touchBlock("norows");
            }
        } else {
            $tpl->touchBlock("noparentselected");
        }
        //echo $sql;
        $tpl->show();
        $DB->free();
    }


    function gettown()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/suggest.tmpl.htm");
        $_POST['q'];
        if (@$_POST['parent_id'] != '') {
            $region = substr($_POST['parent_id'], 0, 2);
            $area = substr($_POST['parent_id'], 2, 3);
            $area = $area == "000" ? "[0-9]{3}" : $area;
            $city = substr($_POST['parent_id'], 5, 3);
            $city = $city == "000" ? "[0-9]{3}" : $city;
            //if ($area != "[0-9]{3}" && $city !="[0-9]{3}") {
            $sql = "SELECT k.name, s.socrname, k.code FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON k.socr=s.scname WHERE k.code REGEXP '" . $region . $area . $city . "(([1-9]{1}[0-9]{1}[0-9]{1})|([0-9]{1}[1-9]{1}[0-9]{1})|([0-9]{1}[0-9]{1}[1-9]{1}))[0]{1}[0]{1}' AND k.code!=" . $DB->F($_POST['parent_id']) . " AND k.name LIKE " . $DB->F(preg_replace("/ {1,}/", "", $_POST["q"]) . "%") . " GROUP BY k.code ORDER BY k.name;";
            $DB->query($sql);
            if (!$DB->errno() && $DB->num_rows()) {
                $addInfo = " <small>(";
                while (list($name, $socr, $code) = $DB->fetch()) {

                    $addInfo .= ")</small>";
                    $tpl->setCurrentBlock("row");
                    $tpl->setVariable("NAME_REAL", $socr == "Город" ? "<strong>" . $name . "</strong>" : $name);
                    $tpl->setVariable("NAME", $name);
                    $tpl->setVariable("ADDINFO", " (" . $code . ")");
                    $tpl->setVariable("ID", $code);
                    $tpl->setVariable("SOCR", $socr);
                    $tpl->parse("row");
                }
                $DB->free();
            } else {
                $tpl->touchBlock("norows");
            }
            /*} else {
                $tpl->touchBlock("noparentselected");
            } */
        } else {
            $tpl->touchBlock("noparentselected");
        }

        $tpl->show();

    }


    function getstreet()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/suggest.tmpl.htm");
        $_POST['q'];
        if (@$_POST['parent_id'] != '') {
            $region = substr($_POST['parent_id'], 0, 2);
            $area = substr($_POST['parent_id'], 2, 3);
            $area = $area == "000" ? "[0-9]{3}" : $area;
            $city = substr($_POST['parent_id'], 5, 3);
            $city = $city == "000" ? "[0-9]{3}" : $city;
            //if ($area != "[0-9]{3}" && $city !="[0-9]{3}") {
            $sql = "SELECT k.name, s.socrname, k.code FROM `kladr_street` AS k LEFT JOIN `kladr_socrbase` AS s ON k.socr=s.scname WHERE k.code REGEXP '" . $region . $area . $city . "(([1-9]{1}[0-9]{1}[0-9]{1})|([0-9]{1}[1-9]{1}[0-9]{1})|([0-9]{1}[0-9]{1}[1-9]{1}))[0]{1}[0]{1}' AND k.code!=" . $DB->F($_POST['parent_id']) . " AND k.name LIKE " . $DB->F(preg_replace("/ {1,}/", "", $_POST["q"]) . "%") . " GROUP BY k.code ORDER BY k.name;";
            $DB->query($sql);
            if (!$DB->errno() && $DB->num_rows()) {
                $addInfo = " <small>(";
                while (list($name, $socr, $code) = $DB->fetch()) {

                    $addInfo .= ")</small>";
                    $tpl->setCurrentBlock("row");
                    $tpl->setVariable("NAME_REAL", $socr == "Город" ? "<strong>" . $name . "</strong>" : $name);
                    $tpl->setVariable("NAME", $name);
                    $tpl->setVariable("ADDINFO", " (" . $code . ")");
                    $tpl->setVariable("ID", $code);
                    $tpl->setVariable("SOCR", $socr);
                    $tpl->parse("row");
                }
                $DB->free();
            } else {
                $tpl->touchBlock("norows");
            }
            /*} else {
                $tpl->touchBlock("noparentselected");
            } */
        } else {
            $tpl->touchBlock("noparentselected");
        }

        $tpl->show();

    }


    function ckeckKCode()
    {
        global $DB;
        if (@$_POST['code'] != '') {
            $code = substr($_POST["code"], 0, 11) . "[0-9]{4}[0]{1}[0]{1}";
            $sql = "SELECT COUNT(code) FROM `kladr_street` WHERE `code` REGEXP " . $DB->F($code) . ";";
            if ($streetCount = $DB->getField($sql)) {
                $DB->free();
                echo "1";
            } else {
                echo "0";
            }

            return;
            $code = substr($_POST["code"], 0, 11) . "0000[0-9]{4}";
            $sql = "SELECT COUNT(code) FROM `kladr_doma` WHERE `code` REGEXP " . $DB->F($code) . ";";
            if ($houseCount = $DB->getField($sql)) {
                $DB->free();
                echo "1";
            } else {
                echo "0";
            }
        } else {
            echo "0";
        }

        return false;
    }


}
