<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Акты ТМЦ (Подключение)";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Акты ТМЦ (Подключение)";
    $PLUGINS[$plugin_uid]['events']['actarchive'] = "Архив актов ТМЦ (Подключение)";
    $PLUGINS[$plugin_uid]['events']['viewact'] = "Просмотр акта ТМЦ (Подключение)";

}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['createact'] = "Создание акта ТМЦ";
    $PLUGINS[$plugin_uid]['events']['deleteact'] = "Удаление акта ТМЦ (Подключение)";
    $PLUGINS[$plugin_uid]['events']['setsent'] = "Изменение статуса актов ТМЦ (Подключение)";
    $PLUGINS[$plugin_uid]['events']['removefromact'] = "Удаление заявок из актов ТМЦ (Подключение)";
    $PLUGINS[$plugin_uid]['events']['savenewpin_ajax'] = "Редактирование ПИН-Кода (Подключение)";
    $PLUGINS[$plugin_uid]['events']['savenewcnumval_ajax'] = "Редактирование номера заявки в 1С (Подключение)";
    $PLUGINS[$plugin_uid]['events']['saveacttitle_ajax'] = "Редактирование названия акта ТМЦ";

}
?>