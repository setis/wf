<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;


require_once(dirname(__FILE__)."/../services/services.php");

 
class reports_f_skp_timeouts_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        $fintypes = array("0"=>"Все", "1"=>"Рассчитанные (Проведенные)", "2"=>"Не рассчитанные (Не проведенные)");
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_TECH_TICKET", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            UIHeader($tpl);
            report_left_menu($tpl);
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            $tpl->setVariable("FILTER_TECH_OPTIONS", adm_empl_plugin::getTechListSKP($_POST["tech_id"]));
            
        }
        
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
            $rtpl->setVariable("DATETO", $_POST['dateto']);
            
            if ($_POST["tech_id"]) {
                $sql_add = " AND r.tech_id=".$DB->F($_POST["tech_id"])." ";
            }

            $sql = "SELECT 
                        SUM(r.count) as count, (SELECT `fio` FROM `users` WHERE `id`=r.tech_id) as user_fio 
                    FROM 
                        `skp_tech_timeouts` AS r 
                    WHERE 
                        r.rdate>=".$DB->F(date("Y-m-d", strtotime($_POST["datefrom"])))." AND r.rdate<=".$DB->F(date("Y-m-d", strtotime($_POST["dateto"])))." $sql_add 
                    GROUP BY user_fio    
                    ORDER BY user_fio ASC, r.rdate ASC";
            //die($sql);
            $DB->query($sql);
            if ($DB->num_rows()) {
                $rtpl->setCurrentBlock("report");
                while ($m = $DB->fetch(true)) {
                    $rtpl->setCurrentBlock("rep_row");
                    $rtpl->setVariable("RR_FIO", $m["user_fio"]);
                    //$rtpl->setVariable("RR_RDATE", $m["rdate"]);
                    $rtpl->setVariable("RR_QUANT", $m["count"]);
                    $total += $m["count"];
                    $rtpl->parse("rep_row");
                }
                $rtpl->setVariable("TOTALTIMEOUTS", $total);
                $rtpl->parse("report");
            } else {
                $rtpl->touchBlock("no-rows");
            }
            
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_avg_price__".$datefrom."-".$dateto.".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 

}
?>