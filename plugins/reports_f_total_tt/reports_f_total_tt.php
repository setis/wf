<?php

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\tickets\AccidentTicket;
use classes\User;

class reports_f_total_tt_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function getPItemsByAgrAndTaskFilter($agr_id, $sql)
    {
        global $DB;
        if (!$agr_id) return false;
        $sql = "SELECT lp.* FROM `list_agr_price` AS lp WHERE (lp.agr_id=" . $DB->F($agr_id) . " AND lp.id IN (SELECT lcc.work_id FROM `list_tt_contr_calc` AS lcc WHERE lcc.task_id IN (" . $sql . "))) GROUP BY lp.id;";

        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error() . " " . $sql);
        if (!$DB->num_rows()) {
            $ret = false;
        } else {
            while ($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }

    function main() {
        global $DB, $USER;

        $sort_ids = array(
            1 => "Дата выполнения",
            2 => "Дата регистрации",
            3 => "Номер заявки оператора",
            4 => "ПИН",
            5 => "Номер заявки",
            6 => "Район",
            7 => "Адрес",
        );
        $sort_sql = array(
            1 => "tc1.datetime",
            2 => "t.date_reg",
            3 => "tick.clnttnum",
            4 => "tick.clntopagr",
            5 => "t.id",
			6 => "lar.title",
            7 => "kstreet.NAME",
        );
        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $fintypes = array("0"=>"Все", "1"=>"Учтенные", "2"=>"Не учтенные");

        if ($_REQUEST['opc_filter_sc']) {
                $areaData1 = $DB->getCell2("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT area_id FROM `link_sc_area` WHERE `sc_id` IN (".preg_replace("/,$/", "",implode(",",array_map([$DB,'F'],(array)$_REQUEST["opc_filter_sc"]))).")) GROUP BY `id` ORDER BY `title`;");
        } else {
                $areaData1 = $DB->getCell2("SELECT `id`,`title` FROM `list_area` ORDER BY `title`;");
        }
        $areaData["no"] = "-- не указан";

        $finalAreas = $areaData1;

        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN__TOTAL", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
            $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));

            UIHeader($tpl);
            report_left_menu($tpl);
            $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            $tpl->setVariable("SELECTED_AGR", @$_POST["agr_id"]);
            $tpl->setVariable("SELECTED_AREA", @$_POST["area_id"]);
            $tpl->setVariable('AREALIST', array2options($finalAreas, $_REQUEST['opc_filter_area'], false, ($_REQUEST["opc_filter_area"] == "" || count($finalAreas) == count($_REQUEST['opc_filter_area']))));
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            $tpl->setVariable("FILTER_TASK_TYPE", array2options($fintypes, $_POST["type_id"]));
            $tpl->setVariable('FILTER_SC_OPTIONS', adm_sc_plugin::getScList($_REQUEST["sc_id"]));
            $wtypeList = $DB->getCell2("SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`=".$DB->F("accidents")." ORDER BY `title`");
            $wtypeList[0] = "-- все --";
            ksort($wtypeList);
            $tpl->setVariable('FILTER_WTYPE_OPTIONS', array2options($wtypeList, $_POST["wtype_id"]));
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListConn")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListConn(@$_POST["cnt_id"]));
            }
            if (class_exists("adm_statuses_plugin", true) && method_exists("adm_statuses_plugin", "getOptList")) {
                $st = new adm_statuses_plugin();
                $tpl->setVariable("FILTER_STATUS_OPTIONS", $st->getOptList(@$_POST["status_id"], 'accidents'));
            }
            $this->getSession()->set("sc_id", $_REQUEST['sc_id'] ? $_REQUEST['sc_id'] : $this->getSession()->get("sc_id"));
            $_REQUEST['sc_id'] = $this->getSession()->get("sc_id");
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList(@$_REQUEST["sc_id"]));

            $actList = self::getActList(0);
            if ($actList) {
                $tpl->setCurrentBlock("newactlist");
                $tpl->setVariable("EXACT_OPTIONS", $actList);
                $tpl->parse("newactlist");
                $tpl->setCurrentBlock("newactlist_btm");
                $tpl->setVariable("EXACT_OPTIONS_BTM", $actList);
                $tpl->parse("newactlist_btm");
            }
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"] || @$_POST["setflag"]) {
                if (strtotime($_POST["year"]."-".$_POST["month"]) < strtotime("2014-02")) {
                    UIError("Текущая версия отчета \"Полный отчет - ТТ\" не совместима с данными БД ранее февраля 2014 года. Для предотвращения выдачи некорректных данных, генерация отчета прекращена. Пожалуйста, используйте Архивы данного отчета.");
                }

                $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
                //$rtpl->loadTemplatefile("report.tmpl.htm");
                if ($USER->getTemplate() != "default")
                    $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
                else
                    $rtpl->loadTemplatefile("report.tmpl.htm");
                if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                    $rtpl->setCurrentBlock("print_head");
                    $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                    $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                    $rtpl->parse("print_head");
                }
                if ($_POST["month"] == "12") {
                    $dc_param = "connfinreport_dc_january";
                } else {
                    $dc_param = "connfinreport_dc";
                }
                $next = $_POST["month"] == "12" ? "connfinreport_dc_january" : "connfinreport_dc";
                $rtpl->setVariable("MONTH", $month[$_POST['month']]);
                $rtpl->setVariable("YEAR", $year[$_POST['year']]);
                if ($_POST['cnt_id']!=0 && $_POST["agr_id"]) {
                    $rtpl->setVariable("AGR_VAL", kontr_plugin::getAgrByIdT($_POST["agr_id"]));
                    if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getByID")) {
                        $ct = new kontr_plugin();
                        if ($kontr = $ct->getByID($_POST['cnt_id'])) {
                            $rtpl->setVariable("REP_CONTR", $kontr);
                            $status = adm_statuses_plugin::getStatusByTag("closed", "accidents");
                            if (count($status)) {
                                $doneStatus = adm_statuses_plugin::getStatusByTag("done", "accidents");
                                $filter ="";
                                $filter .= " AND (tick.tdataactid OR tick.tmcactid) ";
                                $filter .= count($_REQUEST["sc_id"]) > 0 ? " AND tick.sc_id IN (".preg_replace("/,$/", "", implode(", ",array_map([$DB,'F'],$_REQUEST['sc_id']))).") " : "";
                                if ($_POST["wtype_id"]) {
                                    $filter .= " AND tick.task_type=".$DB->F($_POST["wtype_id"])." ";
                                }
                                $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];

                                if (isset($_REQUEST['opc_filter_area'])) {
                                    if (is_array($_REQUEST['opc_filter_area'])) {
                                        if (count($_REQUEST['opc_filter_area']) != count(($finalAreas))) {
                                            $arr = str_replace("no", "0", preg_replace("/,$/", "", implode("," ,array_map([$DB,'F'],$_REQUEST['opc_filter_area']))));
                                            $filter .= " AND la.area_id IN (".$arr.")";
                                        } else {
                                            $filter .= "";
                                        }
                                    }
                                }

                                $sql = "SELECT tc1.task_id FROM `task_comments` AS tc1
                                        LEFT JOIN `tasks` AS t ON tc1.task_id=t.id
                                        LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                                        LEFT JOIN `list_addr` AS la ON la.id=tick.dom_id
                                        LEFT JOIN `list_area` AS lar ON lar.id=la.area_id
                                        WHERE
                                            ((tc1.status_id=".$DB->F($doneStatus["id"])." AND (DATE_FORMAT(tc1.datetime, '%Y-%m')<".$DB->F($_POST["year"]."-".$_POST["month"])." AND tc1.task_id IN (SELECT task_id FROM `list_tt_contr_calc` WHERE DATE_FORMAT(ifnull(update_date,calc_date), '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]." + ".getcfg($dc_param)." days")))." AND DATE_FORMAT(ifnull(update_date,calc_date), '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]." + 1 month + ".getcfg($dc_param)." days")))." )))
                                            OR
                                            (tc1.status_id=".$DB->F($doneStatus["id"])." AND (DATE_FORMAT(tc1.datetime, '%Y-%m')=".$DB->F($_POST["year"]."-".$_POST["month"])." AND tc1.task_id IN (SELECT task_id FROM `list_tt_contr_calc` WHERE DATE_FORMAT(ifnull(update_date,calc_date), '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"])))." AND DATE_FORMAT(ifnull(update_date,calc_date), '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]." + 1 month + ".getcfg($dc_param)." days")))." ))))
                                            AND tick.agr_id=".$DB->F($_POST["agr_id"])."
                                            AND t.status_id=".$DB->F($status["id"])." ".$filter."
                                        GROUP BY tc1.task_id
                                        ORDER BY " .$order;

                                $base_sql = "SELECT 
                                            tc1.task_id, 
                                            DATE_FORMAT(tc1.datetime, '%d.%m.%Y') AS datetime,
                                             la.*,
                                             dadata_address.*,
                                             dadata_address.id as ADDRESS_ID 
                                    FROM `task_comments` AS tc1
                                        LEFT JOIN `tasks` AS t ON tc1.task_id=t.id
                                        LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                                        LEFT JOIN `list_addr` AS la ON la.id=tick.dom_id
                                        LEFT JOIN `list_area` AS lar ON lar.id=la.area_id
                                        LEFT JOIN `dadata_address` ON (dadata_address.kladr_id=la.kladrcode COLLATE utf8_unicode_ci)
                                        WHERE
                                            ((tc1.status_id=".$DB->F($doneStatus["id"])." AND (DATE_FORMAT(tc1.datetime, '%Y-%m')<".$DB->F($_POST["year"]."-".$_POST["month"])." AND tc1.task_id IN (SELECT task_id FROM `list_tt_contr_calc` WHERE DATE_FORMAT(ifnull(update_date,calc_date), '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]. "+ ".getcfg($dc_param)." days")))." AND DATE_FORMAT(ifnull(update_date,calc_date), '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]." + 1 month + ".getcfg($dc_param)." days")))." )))
                                            OR
                                            (tc1.status_id=".$DB->F($doneStatus["id"])." AND (DATE_FORMAT(tc1.datetime, '%Y-%m')=".$DB->F($_POST["year"]."-".$_POST["month"])." AND tc1.task_id IN (SELECT task_id FROM `list_tt_contr_calc` WHERE DATE_FORMAT(ifnull(update_date,calc_date), '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"])))." AND DATE_FORMAT(ifnull(update_date,calc_date), '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]." + 1 month + ".getcfg($dc_param)." days")))."  ))))
                                            AND tick.agr_id=".$DB->F($_POST["agr_id"])."
                                            AND t.status_id=".$DB->F($status["id"])." ".$filter."

                                        GROUP BY tc1.task_id
                                        ORDER BY " .$order;
                                #echo $base_sql."<br>\n";
                                #exit;
                                $DB->query($base_sql);
                                if ($_POST["wtype_id"] == "37" && $_POST["cnt_id"] == "36") {
                                    $rtpl->touchBlock("mgtsadslth");
                                    $mgts_intpl = true;
                                    $clsp = 2;
                                } else {
                                    $mgts_intpl = false;
                                    $clsp = 0;
                                }
                                if ($DB->errno()) UIError($DB->error(). " " . $base_sql);
                                if ($DB->num_rows()) {
                                $i = 1;
                                $total = 0;
                                $total_nds = 0;
                                $total_wnds = 0;
                                $wtList = $this->getPItemsByAgrAndTaskFilter($_POST['agr_id'], $sql);
                                if (!count($wtList)) {
                                    $DB->free();
                                    UIError("Отсутствует прайс для контрагента по выбранному договору");
                                }
                                foreach($wtList as $item) {
                                    $rtpl->setCurrentBlock("wt_col");
                                    $rtpl->setVariable("WT_SERVICENAME", $item["s_title"]);
                                    $rtpl->parse("wt_col");
                                }
                                $period = $_POST["year"]."-".$_POST["month"];
                                $included = $DB->getCell("SELECT DISTINCT(`task_id`) FROM `list_tt_contr_calc` AS lltc WHERE
                                                            DATE_FORMAT(ifnull(lltc.update_date,lltc.calc_date), '%Y-%m-%d')>".$DB->F(date("Y-m-d", strtotime($period." + 1 month + ".getcfg($dc_param)." days"))));

                                $rtpl->setVariable("WT_COUNT", count($wtList) == 0 ? 1 : count($wtList) );
                                $tmc = false;
                                $wt_totals = array();
                                $cableTotalCount = 0;
                                while ($res1 = $DB->fetch(true)) {
                                    $ticket = new AccidentTicket($res1["task_id"]);
                                    $rtpl->setCurrentBlock("rep_row");
                                    if (!$_POST["createprintversion"] && !$_POST["createxlsversion"]) {
                                        $rtpl->setCurrentBlock("ep1");
                                        $rtpl->setVariable("RR_TSK_ID1", $ticket->getId());
                                        $rtpl->parse("ep1");
                                        $rtpl->setCurrentBlock("ep3");
                                        $rtpl->setVariable("RR_TSK_ID2", $ticket->getId());
                                        $rtpl->parse("ep3");
                                        $rtpl->touchBlock("ep2");
                                        $rtpl->touchBlock("ep4");
                                        $rtpl->touchBlock("el1");
                                        $rtpl->setCurrentBlock("el");
                                        $rtpl->setVariable("RR_TASK1_ID", $ticket->getId());
                                        $rtpl->parse("el");
                                    }

                                    if (in_array($res1["task_id"], $included, true)) {
                                        $rtpl->setVariable("ALREADYCOUNTED", " countedandincluded ");
                                    } else {
                                        if ($ticket->getFindateContr()) {
                                            $rtpl->setVariable("ALREADYCOUNTED", " counted");
                                        }
                                    }
                                    $rtpl->setVariable("RR_ID", $i);
                                    if ($doneStatus) {
                                        if ($res1["datetime"]) {
                                            $rtpl->setVariable("RR_DATE", $res1["datetime"]);
                                        } else {
                                            $rtpl->setVariable("RR_DATE", "&mdash;");
                                        }
                                    } else {
                                        $rtpl->setVariable("RR_DATE", "&mdash;");
                                    }
                                    if (!$ticket->getTICKActId()) {
                                        $rtpl->setCurrentBlock("inact");
                                        $rtpl->setVariable("RR_TASK_ID", $ticket->getId());
                                        $rtpl->parse("inact");
                                    }
                                    $rtpl->setVariable("RR_CRDATE", date("d.m.Y", strtotime($ticket->getDateReg())));
                                    $rtpl->setVariable("RR_CONTR_ID", $ticket->getClntTNum());
                                    if ($ticket->getClientType() == User::CLIENT_TYPE_PHYS) {
                                        $rtpl->setVariable("RR_CONTR_FIO", $ticket->getFio());
                                    } else {
                                        $rtpl->setVariable("RR_CONTR_FIO", $ticket->getOrgName());
                                    }
                                    $rtpl->setVariable("RR_TASK_ID_INLIST", $ticket->getId());
                                    $rtpl->setVariable("RR_OPNUM", $ticket->getClntTNum());
                                    $rtpl->setVariable("RR_PIN", $ticket->getClntOpAgr() ? isset($_POST["createxlsversion"]) ?  "# ".$ticket->getClntOpAgr() : $ticket->getClntOpAgr() : "&mdash;");
                                    $rtpl->setVariable("RR_CTASK", $ticket->getAddNumber() ? $ticket->getAddNumber() : "&mdash;");
                                    $rtpl->setVariable("RR_CR_DATE", $ticket->getDateReg());
                                    $rtpl->setVariable("RR_FIO", $ticket->getFio());
                                    $rtpl->setVariable("RR_GSSNUM", $ticket->getId());
                                    $rtpl->setVariable("RR_AREA", $ticket->getArea());
                                    $ticketAct = acts_ticket_plugin::getActInfo($ticket->getTICKActId());

                                    $ticketActValue = ($ticketAct["id"] != "" && $ticketAct["created"] != "") ? $ticketAct["id"] . " от " . $ticketAct["created"] : "&mdash;";
                                    $rtpl->setVariable("RR_ACTS", $ticketActValue);
                                    $tmcAct = acts_tmc_plugin::getActInfo($ticket->getTMCActId());

                                    $tmcActValue = ($tmcAct["id"] != "" && $tmcAct["created"] != "") ? $tmcAct["id"] . " от " . $tmcAct["created"] : "&mdash;";
                                    $rtpl->setVariable("RR_TMC_ACTS", $tmcActValue);


                                    $sum = 0;
                                    $rowspan = 1;

                                    $rtpl->setCurrentBlock("kladr_addr");
                                    $rtpl->setVariable("TMCCOUNT3", $rowspan);
                                    if($res1['ADDRESS_ID']) {
                                        $rtpl->setVariable("RR_ADDR_CITY", $res1['city_type_full'] . ' ' . $res1['city'] . ($res1['settlement_with_type'] ? ', ' . $res1['settlement_with_type'] : ''));
                                        $rtpl->setVariable("RR_ADDR_STREET", $res1['street_with_type']);
                                        $rtpl->setVariable("RR_ADDR_HOUSE", "д." . $res1["house"]);
                                    } else {
                                        $rtpl->setVariable("RR_ADDR_STREET", $res1['full_address']. ' <sup>*</sup>');
                                    }
                                    $rtpl->setVariable("RR_ADDR_FLAT", "кв. ".$ticket->getKv());
                                    $rtpl->parse("kladr_addr");

                                    $rtpl->setVariable("TECH_NAME", $DB->getField("SELECT `fio` FROM `users` WHERE `id`=(SELECT `tech_id` FROM `list_tt_tech_calc` WHERE `task_id`=".$DB->F($ticket->getId())." LIMIT 1);"));
                                    foreach($wtList as $item) {
                                        $priceItem = accidents_plugin::getCWTypeID1($ticket->getId(),$item["id"], $_POST["year"]."-".$_POST["month"]);
                                        #error_log("\n".print_r($priceItem,true)."\n",3,"/ramdisk/priceItem.log");
                                            if ($priceItem) {
                                                $rtpl->setCurrentBlock("wt_val");
                                                $rtpl->setVariable("TMCCOUNT1", $rowspan);
                                                $price = $priceItem[0]["price_type"] == "price" ? $item["price"]*$priceItem[0]["quant"] : $priceItem[0]["smeta_val"];
                                                $price = $price + round($price*18/100, 2);
                                                $sum += $priceItem[0]["price_type"] == "price" ? $item["price"]*$priceItem[0]["quant"] : $priceItem[0]["smeta_val"];
                                                $wt_totals[$item["id"]] += $price;
                                                $rtpl->setVariable("WT_RR_PRICE", number_format($price, 2, ",", ""));
                                                $rtpl->parse("wt_val");

                                            } else {
                                                $rtpl->setCurrentBlock("wt_val");
                                                $rtpl->setVariable("TMCCOUNT1", $rowspan);
                                                $rtpl->setVariable("WT_RR_PRICE", "0");
                                                $rtpl->parse("wt_val");

                                            }
                                    }
                                    $nds = round($sum*18/100, 2);
                                    $total += $sum;
                                    $total_nds += $nds;
                                    $total_wnds += $sum + $nds;
                                    $rtpl->setVariable("RR_PRICE", number_format($sum + $nds, 2, ',', ''));

                                    $cableCount = 0;

                                    $sql = "SELECT tmc_tech.id,
                                        tmc_tech.count,
                                        tmc_sc.serial,
                                        tmc_sc.tmc_id,
                                        tmc_list.title,
                                        tickdata.isrent,
                                        tmc_list.metric_flag,
                                        tmc_list.cat_id,
                                        tmc_tech.inplace,
                                        tmc_tech.incoming,
                                        ifnull(tmc_tech.dismantled,0) dismantled
                                        FROM `tmc_ticket` AS tickdata
                                                                LEFT JOIN `tmc_sc_tech` AS tmc_tech ON tmc_tech.id=tickdata.tmc_tech_sc_id
                                                                LEFT JOIN `tmc_sc` AS tmc_sc ON tmc_sc.id=tmc_tech.tmc_sc_id
                                                                LEFT JOIN `list_materials` AS tmc_list ON tmc_list.id=tmc_sc.tmc_id
                                                                WHERE tickdata.task_id=".$DB->F($res1["task_id"]).";";
                                    $DB->query($sql);
                                    if ($DB->errno()) { echo "error ".$sql." ".$DB->error(); trigger_error($DB->error()); return false; }
                                    $rowspan = 0;
                                    $rC = $DB->num_rows();
                                    $tmcTitle = array();
                                    $tmcSerial = array();
                                    $tmcCount = array();
                                    $tmcDismantledTitle = array();
                                    $tmcDismantledSerial = array();
                                    $tmcDismantledCount = array();
                                    if ($rC>1) {
                                        $first = true;
                                        $rtpl->setVariable("TMCCOUNT", $rC);
                                        $rowspan = $rC;

                                        while ($res = $DB->fetch(true)) {
                                            if ($first) {
                                                if ($res["metric_flag"]) {
                                                    if ($res["dismantled"] == 0) {
                                                        $cableCount += $res["count"];
                                                        $tmcTitle[] = $res["title"];
                                                        $tmcSerial[] = "-";
                                                        $tmcCount[] = $res["count"];
                                                    }
                                                    else {
                                                        $tmcDismantledTitle[] = $res["title"];
                                                        $tmcDismantledSerial[] = "-";
                                                        $tmcDismantledCount[] = $res["count"];
                                                    }
                                                } else {
                                                    if ($res["dismantled"] == 0) {
                                                        $tmcTitle[] = $res["title"];
                                                        $tmcSerial[] = $res["serial"];
                                                        $tmcCount[] = 1;
                                                    } else {
                                                        $tmcDismantledTitle[] = $res["title"];
                                                        $tmcDismantledSerial[] = $res["serial"];
                                                        $tmcDismantledCount[] = 1;
                                                    }
                                                }
                                                $first = false;
                                            } else {
                                                if ($res["metric_flag"]) {
                                                    $cableCount += $res["count"];
                                                    $tmcTitle[] = $res["title"];
                                                    $tmcSerial[] = "-";
                                                    $tmcCount[] = $res["count"];
                                                } else {
                                                    $tmcTitle[] = $res["title"];
                                                    $tmcSerial[] = $res["serial"];
                                                    $tmcCount[] = 1;

                                                }
                                            }
                                        }
                                    } else {
                                        $res = $DB->fetch(true);
                                        $rtpl->setVariable("TMCCOUNT", "1");
                                        $rowspan = 1;
                                        if ($DB->num_rows()) {
                                            if ($res["metric_flag"]) {
                                                if ($res["dismantled"] == 0) {
                                                    $cableCount += $res["count"];
                                                    $tmcTitle[] = $res["title"];
                                                    $tmcSerial[] = "-";
                                                    $tmcCount[] = $res["count"];
                                                } else {
                                                    $tmcDismantledTitle[] = $res["title"];
                                                    $tmcDismantledSerial[] = "-";
                                                    $tmcDismantledCount[] = $res["count"];
                                                }
                                            } else {
                                                if ($res["dismantled"] == 0) {
                                                    $tmcTitle[] = $res["title"];
                                                    $tmcSerial[] = $res["serial"];
                                                    $tmcCount[] = 1;
                                                } else {
                                                    $tmcDismantledTitle[] = $res["title"];
                                                    $tmcDismantledSerial[] = $res["serial"];
                                                    $tmcDismantledCount[] = 1;
                                                }
                                            }
                                        }
                                    }
                                    $cableTotalCount += $cableCount;
                                    $DB->free();
                                    $rtpl->setVariable("RR_CABLE", intval($cableCount) ? $cableCount : "0");
                                    if (count($tmcTitle)) $tmcs =  implode("<br />", $tmcTitle); else $tmcs = "<center>&mdash;</center>";
                                    if (count($tmcSerial)) $tmcserials =  implode("<br />", $tmcSerial); else $tmcserials = "<center>&mdash;</center>";
                                    if (count($tmcCount)) $tmcCounts =  implode("<br />", $tmcCount); else $tmcCounts = "<center>&mdash;</center>";
                                    if (count($tmcDismantledTitle)) $tmcsDismantled =  implode("<br />", $tmcDismantledTitle); else $tmcsDismantled = "<center>&mdash;</center>";
                                    if (count($tmcDismantledSerial)) $tmcserialsDismantled =  implode("<br />", $tmcDismantledSerial); else $tmcserialsDismantled = "<center>&mdash;</center>";
                                    if (count($tmcDismantledCount)) $tmcCountsDismantled =  implode("<br />", $tmcDismantledCount); else $tmcCountsDismantled = "<center>&mdash;</center>";
                                    $rtpl->setVariable("RR_TMCTITLE", $tmcs);
                                    $rtpl->setVariable("RR_TMCSERIAL", $tmcserials);
                                    $rtpl->setVariable("RR_TMCTITLEDISMANTLED", $tmcsDismantled);
                                    $rtpl->setVariable("RR_TMCSERIALDISMANTLED", $tmcserialsDismantled);
                                    $rtpl->setVariable("RR_TMCCOUNT", $tmcCounts);

                                    if ($mgts_intpl) {
                                        $rtpl->setCurrentBlock("mgtsadsltd");
                                        $rtpl->setVariable("RR_ATS", $ticket->getMGTS_ATS() ? $ticket->getMGTS_ATS() : "&mdash;");
                                        $rtpl->setVariable("RR_LANDLINE", $ticket->getMGTS_LandlineNum() ? $ticket->getMGTS_LandlineNum() : "&mdash;");
                                        //$rtpl->setVariable("TMCCOUNT1", $rowspan);
                                        $rtpl->parse("mgtsadsltd");
                                    }
                                    $rtpl->parse("rep_row");
                                    $i+=1;
                                }
                                $rtpl->setCurrentBlock("totalfooter");
                                $rtpl->setVariable("TFCOLSPAN", 11+$clsp);
                                foreach($wtList as $item) {
                                    $rtpl->setCurrentBlock("wt_price");
                                    $rtpl->setVariable("WT_RR_PRICE_TOTAL", number_format($wt_totals[$item["id"]], 2, ',', ''));
                                    $rtpl->parse("wt_price");
                                }
                                $rtpl->setVariable("CABLE_TOTAL", $cableTotalCount);
                                $rtpl->setVariable("TOTALCOLSPAN", 12+intval(count($wtList))+$clsp);
                                $rtpl->setVariable("TOTAL_WONDS", number_format($total, 2, ',', ''));
                                $rtpl->setVariable("TOTAL_NDS", number_format($total_nds, 2, ',', ''));
                                $rtpl->setVariable("TOTAL_WNDS", number_format($total_wnds, 2, ',', ''));
                                $rtpl->parse("totalfooter");
                                } else {
                                    $rtpl->touchBlock("empty");
                                }
                                $DB->free();
                            } else {
                                UIError("Отсутствует статус с тэгом closed");
                            }
                        } else {
                            UIError("Отсутствует Контрагент с указанным ID.");
                        }
                    } else {
                        UIError("Отсутствует модуль Контрагенты.");
                    }
                } else UIError("Не указан контрагент и/или договор!");

                $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
                $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
                if (isset($_POST["createxlsversion"])) {
                    header("Content-type: application/vnd.ms-excel");
                    header("Content-disposition: attachment; filename=report_accidents_total_".$_POST['month']."-".$year[$_POST['year']].".xls");
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    if (isset($_POST['createprintversion'])) {
                        $rtpl->touchBlock("print_footer");
                        $rtpl->show();

                        return;
                    } else {
                        $tpl->setCurrentBlock("reportval");
                        $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                        $tpl->parse("reportval");
                    }
                }
            } else {
                $tpl->touchBlock("notcreated");
                $tpl->setVariable("DISABLED", "disabled='disabled'");
            }


        $tpl->show();

    }

    static function getActInfo($id) {
        global $DB, $USER;
        if (!$id) return false;
        $sql = "SELECT ltr.*, DATE_FORMAT(`cr_date`, '%H:%i  %d.%m.%Y') as created FROM `list_tdata_reports` AS ltr WHERE `id`=".$DB->F($id)." ORDER BY `cr_date` DESC, `status` ASC;";
        $result = $DB->getRow($sql, true);
        if ($DB->errno()) UIError($DB->error());
        return $result;
    }
    static function getActList($status=0) {
        global $DB, $USER;
        $sql = "SELECT ltr.id, CONCAT('№ ', ltr.id, ' от ', DATE_FORMAT(`cr_date`, '%H:%i  %d.%m.%Y')) as created FROM `list_tdata_reports` AS ltr WHERE `status`=".$DB->F($status)." ORDER BY `cr_date` DESC, `status` ASC;";
        $result = array2options($DB->getCell2($sql, true));
        if ($DB->errno()) UIError($DB->error());
        return $result;
    }

    function savenewpin_ajax() {
        global $DB, $USER;
        $task_id = $_REQUEST["task_id"];
        $pinval = $_REQUEST["val"];
        $sql = "UPDATE `tickets` SET `clntopagr`=".$DB->F($pinval)." WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        if ($DB->errno()) {
            echo "error";
            return false;
        } else {
            echo "ok";
        }
        return false;
    }

    function savenewcnumval_ajax() {
        global $DB, $USER;
        $task_id = $_REQUEST["task_id"];
        $cval = $_REQUEST["val"];
        $sql = "UPDATE `tickets` SET `tickaddnumber`=".$DB->F($cval)." WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        if ($DB->errno()) {
            echo "error";
            return false;
        } else {
            echo "ok";
        }
        return false;
    }
}
