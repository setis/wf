<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class adm_materials_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getById($id)
    {
        global $DB;
        if (!$id) UIError("Не указан ТМЦ");
        $result = $DB->getField("SELECT `title` FROM `list_materials` WHERE `id`='" . $id . "';");
        return $result;
    }

    static function getTMCListByCat($id)
    {
        global $DB;
        if (!$id) UIError("Не указана категория товара!");
        $sql = "SELECT * FROM `list_materials` WHERE `cat_id`=" . $DB->F($id) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        $res = array();
        if ($DB->num_rows()) {
            while ($fetch = $DB->fetch(true)) {
                $res[] = $fetch;
            }
        } else {
            $res = false;
        }
        $DB->free();
        return $res;
    }

    function viewlist()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        $sql = "SELECT * FROM `list_materials` WHERE 1;";
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());
        while (list($id, $title, $cat_id, $bar_code, $metric_flag, $metric_title) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->setVariable("VLI_CAT", adm_material_cats_plugin::getById($cat_id));
            $tpl->setVariable("VLI_BARCODE", $bar_code);
            $tpl->setVariable("VLI_IZM", $metric_flag ? "Да" : "Нет");
            $tpl->setVariable("VLI_EDIZM", $metric_title);

            $tpl->parse("vl_item");
        }
        UIHeader($tpl);
        $tpl->show();
    }

    function edit() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        if($tmc_id = $_REQUEST['tmc_id']) {
            $sql = "SELECT * FROM `list_materials` WHERE `id`=".$DB->F($tmc_id).";";
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("ТМЦ с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);

            $DB->free();
            $tpl->setVariable("TMC_ID", $result["id"]);
            $tpl->setVariable("TMC_TITLE", $result["title"]);
            $tpl->setVariable("TMC_CAT", adm_material_cats_plugin::getOptions($result["cat_id"]));
            $tpl->setVariable("TMC_BAR_CODE", $result["bar_code"]);
            if ($result["metric_flag"])
                $tpl->setVariable("IZM_YES", "selected='selected'");
            else
                $tpl->setVariable("IZM_NO", "selected='selected'");
            $tpl->setVariable("TMC_METRIC_TITLE", $result["metric_title"]);
        } else {
            $tpl->setVariable("TMC_CAT", adm_material_cats_plugin::getOptions());

        }
        UIHeader($tpl);
        $tpl->show();

    }

    function save() {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = array();

        if(!$_POST['title']) $err[] = "Не заполнено Название ТМЦ";

        if(sizeof($err)) UIError($err);

        if($_POST['tmc_id'])
                $sql = "UPDATE `list_materials` SET `title`=".$DB->F($_POST['title']).", `cat_id`=".$DB->F($_POST["cat_id"]).",
                        `bar_code`=".$DB->F($_POST["barcode"]).", `metric_flag`=".$DB->F($_POST["izm"]).", `metric_title`=".$DB->F($_POST["metric_title"])." WHERE `id`=".$DB->F($_POST['tmc_id']);
        else {
            $sql = "INSERT INTO `list_materials` (`title`, `cat_id`, `bar_code`, `metric_flag`, `metric_title`) 
                    VALUES(".$DB->F($_POST['title']).", ".$DB->F($_POST["cat_id"]).", ".$DB->F($_POST["barcode"]).", ".$DB->F($_POST["izm"]).", ".$DB->F($_POST["metric_title"]).");";
        }
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error()." ".$sql);

        if(!$_POST['tmc_id']) {
            $_POST['tmc_id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "ТМЦ сохранена. ID: ".$_POST['tmc_id'] );
    }

    function delete() {
        global $DB;
        if(!($id = $_REQUEST['tmc_id'])) $err[] = "Не указан ТМЦ";
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST["tmc_id"])) UIError("Выбранная ТМЦ занята! Удаление невозможно.");
        $DB->query("DELETE FROM `list_materials` WHERE `id`='".$_REQUEST['tmc_id']."';");
        redirect($this->getLink(),"ТМЦ успешно удалена.");

    }

    function isBusy($id)
    {
        global $DB;
        $DB->query("SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=" . $DB->F($id) . ";");
        if ($DB->num_rows()) $res = true; else $res = false;
        $DB->free();
        return $res;
    }

    function getListByCat()
    {

        global $DB, $USER;
        $id = $_REQUEST["cat_id"];
        $tech_id = $_REQUEST["tech_id"];
        $sc_id = $DB->getField("SELECT `sc_id` FROM `link_sc_user` WHERE `user_id`=".$DB->F($tech_id).";");
        $sql = "SELECT * FROM `list_materials` AS lm WHERE lm.cat_id=".$DB->F($id).";";
        $DB->query($sql);
        if($DB->errno()) echo "error";
        $res = false;
        $count = 0;
        if ($DB->num_rows()) {
            while ($fetch = $DB->fetch(true)) {
                if ($fetch["metric_flag"] == 1) {
                    /**
                     * всякие провода
                     */

                    $tmcCountIn = $DB->getField("SELECT 
                        SUM(sct.count) 
                    FROM 
                        tmc_sc_tech AS sct 
                        LEFT JOIN 
                        `tmc_sc` AS sc ON sct.tmc_sc_id=sc.id 
                    WHERE 
                        sct.tech_id=".$DB->F($tech_id)." AND
                        sct.sc_id=".$DB->F($sc_id)." AND  
                        sct.incoming=1 AND 
                        sct.tmc_sc_id IN (SELECT 
                                `id` 
                            FROM 
                                `tmc_sc` 
                            WHERE 
                                `tmc_id`=".$DB->F($fetch["id"])."
                        )
                    ;");
                    $tmcCountOut = $DB->getField("SELECT 
                        SUM(sct.count) 
                    FROM 
                        tmc_sc_tech AS sct 
                        LEFT JOIN 
                        `tmc_sc` AS sc ON sct.tmc_sc_id=sc.id 
                    WHERE 
                        sct.tech_id=".$tech_id." AND 
                        sct.sc_id=".$DB->F($sc_id)." AND  
                        (sct.inplace < 1 OR sct.inplace IS NULL) AND 
                        sct.tmc_sc_id IN (SELECT 
                                `id` 
                            FROM 
                                `tmc_sc` 
                            WHERE 
                                `tmc_id`=".$DB->F($fetch["id"])."
                        ) AND
                        sct.count>0 AND
                        (!sct.incoming OR sct.incoming IS NULL) AND 
                        (!sct.inplace OR sct.inplace IS NULL)
                    ;");
                    $a = intval($tmcCountIn)-intval($tmcCountOut);
                    if (($a)>0) {
                        $count++;
                        $sql4 = "SELECT
                            tst.id
                        FROM
                            tmc_sc_tech AS tst
                            LEFT JOIN
                            tmc_sc AS tsc ON tsc.id=tst.tmc_sc_id
                        WHERE
                            tst.tech_id=".$DB->F($tech_id)." AND
                            tsc.tmc_id=".$DB->F($fetch["id"])." AND
                            tst.inplace=1 AND
                            (
                                tst.incoming=0 OR
                                tst.incoming IS NULL
                            )
                        ;";
                        $tst_id = $DB->getField($sql4);
                        $res.="<tr><td><a tst_id=\"$tst_id\" id=\"$a\" href=\"#\" class=\"metric tmc tmcid_".$fetch["id"]."\">".$fetch["id"]."</a></td><td><a tst_id=\"$tst_id\" id=\"$a\" href=\"#\" class=\"metric tmc tmctitle_".$fetch["id"]."\">".$fetch["title"]."</a></td><td>".$fetch["bar_code"]."</td><td>&mdash;</td><td>".$a." ".$fetch["metric_title"]."</td></tr>";
                    }
                } else {
                    /*
                     * всякое с серийниками
                     */
                    $sql1 = "SELECT
                        tst.id,
                        tsc.serial
                    FROM
                        tmc_sc_tech AS tst
                        LEFT JOIN tmc_sc AS tsc ON tsc.id=tst.tmc_sc_id
                    WHERE
                        tst.tech_id=".$DB->F($tech_id)." AND
                        tsc.tmc_id=".$DB->F($fetch["id"])." AND
                        tst.inplace=1 AND
                        (
                            tst.incoming=0 OR
                            tst.incoming IS NULL
                        );";
                    $DB->query($sql1);
                    if ($DB->num_rows()) {
                        while ($r = $DB->fetch(true)) {
                            $count++;
                            $a = 1;
                            $res.="<tr>
                                <td>
                                    <a id=\"".$r["serial"]."\" tst_id=\"".$r["id"]."\" href=\"#\" class=\"tmc tmcid_".$fetch["id"]."\">".$fetch["id"]."</a>
                                </td>
                                <td>
                                    <a id=\"".$r["serial"]."\" tst_id=\"".$r["id"]."\" href=\"#\" class=\"tmc tmctitle_".$fetch["id"]."\">".$fetch["title"]."</a>
                                </td>
                                <td>".$fetch["bar_code"]."</td>
                                <td>".$r["serial"]."</td>
                                <td>".$a." ".$fetch["metric_title"]."</td>
                            </tr>";
                        }
                    }
                    $DB->free();

                }
            }
            if ($count < 1) {
                $res = "<tr id='tmc_norows'><td colspan='5'><p align='center' style='color: #FF0000;'><strong>Список пуст..</strong></p></td></tr>";

            }
        } else {
            $res = "<tr id='tmc_norows'><td colspan='5'><p align='center' style='color: #FF0000;'><strong>Список пуст!</strong></p></td></tr>";
        }
        $DB->free();
        echo $res;
    }

    function getOptionListByCat_ajax()
    {

        global $DB, $USER;
        $id = $_POST["cat_id"];
        $sql = "SELECT * FROM `list_materials` WHERE `cat_id`=".$DB->F($id).";";
        $DB->query($sql);
        if($DB->errno()) echo "error";
        $res = false;
        $res = "<option value=\"\">-- выберите наименование --</option>";
        if ($DB->num_rows()) {
            while ($fetch = $DB->fetch(true)) {
                $res .= "<option value=\"".$fetch["id"]."\">".$fetch["title"]."</option>";
            }

        }

        echo $res;
    }

    function ismetric_ajax() {
        global $DB;
        $metric = $DB->getField("SELECT `metric_flag` FROM `list_materials` WHERE `id`=".$DB->F($_POST["tmc_id"]).";");
        if ($DB->errno()) {
            echo "error";
            return false;
        }
        echo $metric;
    }

    function ismetric1_ajax() {
        global $DB;
        $metric = $DB->getRow("SELECT `metric_flag`, `is_num` FROM `list_materials` WHERE `id`=".$DB->F($_POST["tmc_id"]).";", true);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
        } else {
            $ret["ismetric"] = $metric["metric_flag"];
            $ret["is_num"] = $metric["is_num"];
            if ($metric['is_num']) {
                $maxnum = $DB->getField("SELECT MAX(`docnum`) FROM `tmc_sc` WHERE `tmc_id`=".$DB->F($_POST["tmc_id"]).";");
                $ret["nextnum"] = $maxnum + 1;
            }
        }
        echo json_encode($ret);
        return false;
    }




}
?>
