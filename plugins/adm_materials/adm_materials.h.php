<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Справочник ТМЦ";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';
$PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник ТМЦ";
$PLUGINS[$plugin_uid]['events']['getListByCat'] = "Справочник ТМЦ по категории (ajax)";
$PLUGINS[$plugin_uid]['events']['getOptionListByCat_ajax'] = "Справочник ТМЦ по категории (ajax)";
    
if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник ТМЦ";
    $PLUGINS[$plugin_uid]['events']['getListByCat'] = "Справочник ТМЦ по категории (ajax)";
    $PLUGINS[$plugin_uid]['events']['ismetric_ajax'] = "Проверка типа ТМЦ";
    $PLUGINS[$plugin_uid]['events']['ismetric1_ajax'] = "Проверка типа ТМЦ + документ"; 
    
}

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование ТМЦ";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление ТМЦ";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение ТМЦ";
}

?>