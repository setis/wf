<?php

/**
 * Plugin Implementation
 *
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use classes\tickets\ConnectionTicket;
use classes\User;
use models\ListAddr;
use models\Task as TaskEntity;
use models\TaskStatus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class connections_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getCWTypeID1($task_id, $id, $period)
    {
        global $DB;
        if (!$task_id || !$id) UIError("Не указан идентификатор заявки!");
        $DB->query("SELECT lap.id, lap.s_title, lap.title, lap.price_type, lap.price, lap.p_comment, (SELECT `smeta_val` FROM `list_contr_calc` WHERE `work_id`=lap.id AND `task_id`=" . $DB->F($task_id) . ") AS smeta_val, (SELECT quant FROM `list_contr_calc` WHERE `work_id`=lap.id AND `task_id`=" . $DB->F($task_id) . ") AS quant FROM `list_agr_price` AS lap
                    WHERE lap.id IN
                        (SELECT `work_id` FROM `list_contr_calc` WHERE `task_id`=" . $DB->F($task_id) . "
                            AND ((DATE_FORMAT(`update_date`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($period))) . "
                                AND DATE_FORMAT(`update_date`, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($period . " + 1 month + " . getcfg('connfinreport_dc') . " days"))) . ")

                            OR (DATE_FORMAT(`calc_date`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($period))) . "
                                AND DATE_FORMAT(`calc_date`, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($period . " + 1 month + " . getcfg('connfinreport_dc') . " days"))) . ")))

                        AND lap.id=" . $DB->F($id) . ";");
        if ($DB->errno()) UIError($DB->error());
        $ret = false;
        if ($DB->num_rows()) {
            while ($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        } else {
            $ret = false;
        }
        $DB->free();

        return $ret;
    }

    static function getCWTypeID($task_id, $id)
    {
        global $DB;
        if (!$task_id || !$id) UIError("Не указан идентификатор заявки!");
        $DB->query("SELECT lap.id, lap.s_title, lap.title, lap.price_type, lap.price, lap.p_comment, (SELECT `smeta_val` FROM `list_contr_calc` WHERE `work_id`=lap.id AND `task_id`=" . $DB->F($task_id) . ") AS smeta_val, (SELECT `quant` FROM `list_contr_calc` WHERE `work_id`=lap.id AND `task_id`=" . $DB->F($task_id) . ") AS quant  FROM `list_agr_price` AS lap WHERE lap.id IN (SELECT `work_id` FROM `list_contr_calc` WHERE `task_id`=" . $DB->F($task_id) . ") AND lap.id=" . $DB->F($id) . ";");
        if ($DB->errno()) UIError($DB->error());
        $ret = false;
        if ($DB->num_rows()) {
            while ($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        } else {
            $ret = false;
        }
        $DB->free();

        return $ret;
    }

    static function getTWTypeID($task_id, $id, $period)
    {
        global $DB;

        $targetDate = date("m", strtotime($period));
        if ($targetDate == 12) {
            $dc = "connfinreport_dc_january";
        } else {
            $dc = "connfinreport_dc";
        }
        if (!$task_id) UIError("Не указан идентификатор заявки!");
        $DB->query("SELECT ltp.id, ltp.short_title, ltp.title, ltp.price_type, ltp.price, ltp.p_comment,
                (SELECT `smeta_val` FROM `list_tech_calc` WHERE `work_id`=ltp.id AND `task_id`=" . $DB->F($task_id) . " LIMIT 1) AS smeta_val, (SELECT `quant` FROM `list_tech_calc` WHERE `work_id`=ltp.id AND `task_id`=" . $DB->F($task_id) . " LIMIT 1) AS quant
                    FROM `list_tech_price` AS ltp
                    WHERE ltp.id IN (SELECT `work_id` FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($task_id) . "
                            AND ((DATE_FORMAT(`update_date`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($period))) . "
                                AND DATE_FORMAT(`update_date`, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($period . " + 1 month + " . getcfg($dc) . " days"))) . ")

                            OR (DATE_FORMAT(`calc_date`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($period))) . "
                                AND DATE_FORMAT(`calc_date`, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($period . " + 1 month + " . getcfg($dc) . " days"))) . ")))
                        AND ltp.id=" . $DB->F($id) . ";");
        if ($DB->errno()) UIError($DB->error());
        $ret = false;
        if ($DB->num_rows()) {
            while ($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        } else {
            $ret = false;
        }
        $DB->free();

        return $ret;
    }

    static function getContrName($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT concat(`contr_title`,' (',`official_title`,')') AS val FROM `list_contr` WHERE `id`=" . $DB->F($id);

        return $DB->getField($sql, $escape);
    }

    static function getTypeName($id, $escape = true)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `title` FROM `task_types` WHERE `plugin_uid`=" . $DB->F($plugin_uid) . " AND `id`=" . $DB->F($id);

        return $DB->getField($sql, $escape);
    }

    static function getTypeDuration($id, $escape = true)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `duration` FROM `task_types` WHERE `plugin_uid`=" . $DB->F($plugin_uid) . " AND `id`=" . $DB->F($id);

        return $DB->getField($sql, $escape);
    }

    static function statusCallbackGet($task_id, $status)
    {
        global $DB, $USER;

        if (false !== strpos($status["tag"], 'pir')) {
            //die();
            if ($USER->checkAccess("allowpir", User::ACCESS_WRITE)) {
                return $status['name'];
            } else {
                return false;
            }
        }
        if (false !== strpos($status["tag"], 'smr')) {
            //die();
            if ($USER->checkAccess("allowsmr", User::ACCESS_WRITE)) {
                return $status['name'];
            } else {
                return false;
            }
        }


        if ((false !== strpos($status['tag'], 'cmmcontr'))) {

            $sql = "SELECT `contr_id`, `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=" . $DB->F($task_id);
            list($contr_id, $contr_ticket_id) = $DB->getRow($sql);

            if ($USER->checkAccess('connections2contr', User::ACCESS_WRITE) && $contr_ticket_id) return $status['name'];
            else return false;
        }
        if ((false !== strpos($status['tag'], 'otlozh'))) {
            return $status['name'] . "</label> до <input type='text' name='otlozh_date' class='datetime_input'><label>";
        }

        $err = [];
        $plugin_uid = basename(__FILE__, '.php');
        if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejectReasonsList")) {
            $rej = new adm_rejects_plugin();
            if ($status['tag'] != '') {
                $rejReasons = $rej->getRejectReasonsList(false, $plugin_uid, $status['tag']);
                if ($rejReasons !== false) {
                    return $status['name'] . "</label> <select style=\"margin-left: 10px;width:150px;\" name='reject_" . $status["tag"] . "_id'>" . $rejReasons . "</select> <label>";
                }
            }
        } else {
            $err[] = 'Отсутствует класс Причин отказа!';
        }

        return sizeof($err) ? $err : $status['name'];
    }

    static function statusCallbackSet($task_id, &$status, &$text, &$tag)
    {
        global $DB;
        $err = [];
        if (false !== strpos($status['tag'], 'cmmcontr')) {

            $sql = "SELECT `contr_id`, `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=" . $DB->F($task_id);
            list($contr_id, $contr_ticket_id) = $DB->getRow($sql);

            if ($text && $contr_id && $contr_ticket_id) {
                if ($contr_id == getcfg('2kom_contr_id')) { // заявка от 2KOM
                    try {
                        $client = new SoapClient(getcfg('2kom_soap_url'));
                        $result = $client->UpdateOrder(getcfg('2kom_soap_login'), getcfg('2kom_soap_pass'), $contr_ticket_id, 0, '', $text);
                        trigger_error("2KOM SOAP Result code {$result['retcode']}: " . $result['retmessage']);
                        if ($result['retcode'] != 0) {
                            $err[] = "Ошибка обновления даннх в 2KOM: " . $result['retmessage'];
                        } else {
                            //$tag = "Отправлен комментарий контрагенту";
                            /* OK */
                        }
                    } catch (SoapFault $e) {
                        trigger_error("2KOM SOAP Exeption! " . $e->getMessage(), E_USER_WARNING);
                        $err[] = "Ошибка связи с 2KOM: " . $e->getMessage();
                    }
                }
            }

            $status = [];
            $tag = "Отправлен комментарий контрагенту";
            //return sizeof($err) ? $err : true;
            if (sizeof($err)) $text .= "\r\n\r\nВНИМАНИЕ:\r\n" . implode("\r\n", $err);

            return true;
        }

        if (false !== strpos($status['tag'], 'otlozh')) {
            if (!$_POST['otlozh_date']) $err[] = "Нет даты, до которой отложить";
            else {
                $task = new ConnectionTicket($task_id);
                if ($task->setDateFn($_POST['otlozh_date'])) $tag .= " до {$_POST['otlozh_date']}";
            }
        }

        $rejectKey = "reject_" . $status["tag"] . "_id";
        $reject_id = (isset($_POST[$rejectKey]) && $_POST[$rejectKey]) ? $_POST[$rejectKey] : false;
        if ($reject_id) {
            $sql = "UPDATE `tickets` SET `reject_id`=" . $DB->F($reject_id) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) return false;
            if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejReasonTitle")) {
                $rej = new adm_rejects_plugin();
                $tag .= " - " . $rej->getRejReasonTitle($reject_id);
                //return true;
            } else return false;
        } else {
            $sql = "UPDATE `tickets` SET `reject_id`=" . $DB->F("0") . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) return false;
            //return true;
        }
        if (isset($_SERVER["HTTP_HOST"]) && $_SERVER["HTTP_HOST"] != "gss.local") {
            // передача статусов контрагенту начало
            $sql = "SELECT `contr_id`, `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=" . $DB->F($task_id);
            list($contr_id, $contr_ticket_id) = $DB->getRow($sql);
            trigger_error("Task_id='$task_id', contr_id='$contr_id', contr_ticket_id='$contr_ticket_id'");
            if ($contr_id == getcfg('2kom_contr_id') && preg_match("/2kom/", $status['tag'])) { // заявка от 2KOM + статус для них
                try {
                    $client = new SoapClient(getcfg('2kom_soap_url'));
                    $result = $client->UpdateOrder(getcfg('2kom_soap_login'), getcfg('2kom_soap_pass'), $contr_ticket_id, $status['id'], $status['name'], "Изменен статус");
                    trigger_error("2KOM SOAP Result code {$result['retcode']}: " . $result['retmessage']);
                    if ($result['retcode'] != 0) {
                        $err[] = "Ошибка обновления даннх в 2KOM: " . $result['retmessage'];
                    } else {
                        $tag .= "; статус отправлен контрагенту";
                    }
                } catch (SoapFault $e) {
                    trigger_error("2KOM SOAP Exeption! " . $e->getMessage(), E_USER_WARNING);
                    $err[] = "Ошибка связи с 2KOM: " . $e->getMessage();
                }
            }
        }

        if ($status['id'] == 1) // выполнена
        {
            $hadStatuses = [81, 100, 101, 102]; // [ПИР, ПИР ППО, ПИР РП, ПИР СОГЛ]
            $task = new ConnectionTicket($task_id);
            if ($task->isHadStatus($hadStatuses)) {
                // notify Project Department id=116
                $department_id = 116;
                $department = adm_users_plugin::getDepartment($department_id);
                $task->addComment('Уведомление на подготовку ИД отправлено в отдел "' . $department['name'] . '" (' . $department['email'] . ').');

                $tpl = new HTML_Template_IT(path2('templates'));
                $tpl->loadTemplatefile('task_mail.tmpl.html');

                $vt = "viewticket";
                $tpl->setVariable('TASK_HREF', "https://work.gorserv.ru/" . $task->getPluginUid() . "/" . $vt . "?task_id=" . $task->getId());
                $tpl->setVariable('DATE_REG', $task->getDateReg());
                $tpl->setVariable('STATUS_NAME', $task->getStatusName());
                $tpl->setVariable('STATUS_COLOR', $task->getStatusColor());
                $tpl->setVariable('AUTH_FIO', $task->getAuthorFio());
                $tpl->setVariable('ISPOLN_FIO', $task->getIspoln(', '));
                $tpl->setVariable('TASK_TITLE', $task->getTitle());
                $tpl->setVariable('TASK_COMMENT', $task->getComment());
                $tpl->setVariable('SYS_NAME', getcfg('system.name'));
                $tpl->setVariable('PLUGIN_TITLE', Plugin::getNameStatic($task->getPluginUid()));
                $tpl->setVariable('TASK_ID', $task->getId());
                $tpl->setVariable('SYS_HOST', getcfg('http_base'));
                if ($task->getCommentsCount()) {
                    $tpl->setVariable('LAST_COMMENT', $task->getLastComment());
                }

                $email = wf::$container->get('wf.deprecated_mail');
                $email->setSubject('Требуется ИД ' . Plugin::getNameStatic($task->getPluginUid()) . ' ' . $task->getId() . ' - ' . getcfg('sys_name'));
                $email->setTo($department['name'] . ' <' . $department['email'] . '>');
                $email->setHtml($tpl->get());

                $sql = "INSERT INTO syslog SET `actiondate`=now(), `userid`='-', `userfio`='-', `userip`='-', `module`=" . $DB->F($task->getPluginUid()) . ", `action`='email', `result`=" . $DB->F($tpl->get()) . ";";

                $m3 = $DB->query($sql);
                $DB->free($m3);

                if (!$email->send())
                    $err[] = "Ошибка отправки";
            }
        }


        if (sizeof($err)) {
            $text .= "\r\n\r\nВНИМАНИЕ:\r\n" . implode("\r\n", $err);
            $err = [];
        }

        // передача статусов контрагенту конец

        return sizeof($err) ? $err : true;
    }

    function viewlist(Request $request)
    {
        global $DB, $USER;
        $g_user_id = $this->getUserId();

        $sort_ids = [
            1 => "Дата изменения",
            2 => "Дата создания",
            3 => "Дата в графике",
            4 => "Техник",
            6 => "ID заявки",
            7 => "ID заявки контрагнета",
            8 => "Тип",
            9 => "Контрагент",
            10 => "Вид работ",
            11 => "СЦ",
            12 => "Район",
            13 => "Статус",
            14 => "Адрес",
            15 => "ПИН",
        ];
        $sort_sql = [
            1 => "ts.lastcmm_date",
            2 => "ts.date_reg",
            3 => "1",
            4 => "1",
            5 => "is_otlozh_date DESC, ts.date_fn",
            6 => "ts.id",
            7 => "tt.clnttnum",
            8 => "tt.clnt_type",
            9 => "ca.contr_title",
            10 => "typ.title",
            11 => "sc.title",
            13 => "st.name",
            14 => "kstreet.NAME",
            15 => "clntopagr",
        ];

        $sort_ti_ids = [
            1 => "Дата изменения",
            2 => "Дата создания",
            3 => "Дата в графике",
            4 => "Дата расчета",
            5 => "Дата выполнения",
        ];

        $sort_tilk_ids = [
            1 => "Дата изменения",
            2 => "Дата создания",
        ];

        $sort_order = [
            'DESC' => "Обратный",
            'ASC' => "Прямой",
        ];

        $rpp = [
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250",
            500 => "500",
        ];

        //notistech
        $status_otlozh = $this->getStatusesByTag('otlozh');
        $status_otlozh = $status_otlozh[0];
        $LKUser = adm_users_plugin::isLKUser($USER->getId());
        $LKContrID = 0;
        if ($LKUser) {
            $LKContrID = adm_users_plugin::getLKUserContrID($USER->getId());
            $tpl_add = "_lk";
        } else {
            $tpl_add = "";
        }
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/list$tpl_add.tmpl.htm");
        else
            $tpl->loadTemplatefile("list$tpl_add.tmpl.htm");
        if ($USER->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }
        if (!$USER->isTech($USER->getId()) || $USER->isChiefTech($USER->getId())) {
            $tpl->setCurrentBlock("notistech");
            $tpl->setVariable("PLUGIN1_UID", $this->getUID());
            $tpl->parse("notistech");
        } else {
            if ($USER->checkAccess("allowcreatetickets", User::ACCESS_WRITE)) {
                $tpl->setCurrentBlock("notistech");
                $tpl->setVariable("PLUGIN1_UID", $this->getUID());
                $tpl->parse("notistech");
            }
        }
        $filter = "ts.plugin_uid=" . $DB->F($this->getUID());
        $isTech = User::isTech($g_user_id);
        $isChiefTech = User::isChiefTech($g_user_id);

        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $_REQUEST['filter_ti_sort_id'] = $this->getCookie('filter_ti_sort_id', $_REQUEST['filter_ti_sort_id']);
        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ? $_REQUEST['filter_sort_order'] : key($sort_order));
        if (!isset($_REQUEST["filter_rpp"])) $_REQUEST["filter_rpp"] = $USER->getRpp();

        if ($_REQUEST["filter_rpp"] != $USER->getRpp()) {
            $sql = "UPDATE `users` SET `rpp`=" . $_REQUEST["filter_rpp"] . " WHERE `id`=" . $DB->F($USER->getId()) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
        } else {
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        }
        if (($isChiefTech && $isTech) || !$isTech) {
            $_REQUEST['filter_client_phone'] = $this->getCookie('filter_client_phone', $_REQUEST['filter_client_phone']);
            $tpl->setCurrentBlock("is_clnt_phone");
            $tpl->setVariable("FILTER_CLIENT_PHONE", $_REQUEST['filter_client_phone'] ? $_REQUEST['filter_client_phone'] : "");
            $tpl->parse("is_clnt_phone");
            if ($_REQUEST['filter_client_phone']) {
                $phone = preg_replace("/(\D+)/", "", $_REQUEST["filter_client_phone"]);
                $filter .= " AND (tt.clnt_tel1 LIKE '%" . $phone . "%' OR tt.clnt_tel2 LIKE '%" . $phone . "%') ";
            }
        } else {
            $_REQUEST['filter_client_phone'] = false;
        }
        if (!$LKUser) {
            if (($isTech || $isChiefTech) && !$USER->checkAccess("scfullaccess")) {
                if ($isChiefTech) {
                    $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);
                    $tpl->setVariable("FILTER_SC_VAL", $_REQUEST['filter_sc_id']);
                    if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']) . ")";
                    $tpl->setCurrentBlock("techlist");
                    if ($_REQUEST['filter_empl_id']) {
                        $tpl->setVariable("FILTER_EMPL_ID", $_REQUEST['filter_empl_id']);
                        $usrName = adm_users_plugin::getUser($_REQUEST["filter_empl_id"]);
                        $tpl->setVariable("FILTER_EMPL_NAME", $usrName["fio"]);
                    }
                    $tpl->setVariable('FILTER_SC_OPTIONS', $this->getSCOptions($_REQUEST['filter_sc_id']));
                    $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
                    $tpl->parse("techlist");
                } elseif ($isTech) {
                    $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $g_user_id);
                    //if($_REQUEST['filter_empl_id']) $filter .= " AND gfx.empl_id=".$DB->F($g_user_id);
                    $virtAlias = adm_empl_plugin::isVirtual($g_user_id);
                    if ($virtAlias) {
                        if ($_REQUEST['filter_empl_id']) $filter .= " AND ((gfx.empl_id=" . $DB->F($g_user_id) . " OR gfx.empl_id=" . $DB->F($virtAlias) . ") OR ts.id IN (SELECT `task_id` FROM `task_users` WHERE `user_id`=" . $DB->F($_REQUEST["filter_empl_id"]) . " AND `ispoln`))";

                    } else {
                        if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($g_user_id) . " OR ts.id IN (SELECT `task_id` FROM `task_users` WHERE `user_id`=" . $DB->F($_REQUEST["filter_empl_id"]) . " AND `ispoln`))";
                    }
                }
            } else {
                $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $request->get('filter_sc_id', -1));
                $tpl->setVariable("FILTER_SC_VAL", $_REQUEST['filter_sc_id']);
                $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);
                if ($request->get('filter_sc_id', -1) > 0) {
                    $filter .= " AND tt.sc_id=" . $request->request->getInt('filter_sc_id');
                } elseif (0 == $request->get('filter_sc_id', -1)) {
                    $filter .= " AND (tt.sc_id IS NULL OR tt.sc_id = 0)";
                }
                if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']) . ")";
                $tpl->setCurrentBlock("sclist");
                $tpl->setVariable('FILTER_SC_OPTIONS', $this->getSCOptions($_REQUEST['filter_sc_id']));
                $tpl->parse("sclist");
                $tpl->setCurrentBlock("techlist");
                if ($_REQUEST['filter_empl_id']) {
                    $tpl->setVariable("FILTER_EMPL_ID", $_REQUEST['filter_empl_id']);
                    $usrName = adm_users_plugin::getUser($_REQUEST["filter_empl_id"]);
                    $tpl->setVariable("FILTER_EMPL_NAME", $usrName["fio"]);
                }
                $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
                $tpl->parse("techlist");
            }

            //print_r($_REQUEST);
            $this->getSession()->set("filter_type", $_REQUEST['filter_type'] ? $_REQUEST['filter_type'] : $this->getSession()->get("filter_type"));
            $_REQUEST['filter_type'] = $this->getSession()->get("filter_type");
            $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
            $_REQUEST['filter_cnt_id'] = $this->getCookie('filter_cnt_id', $_REQUEST['filter_cnt_id']);
            $_REQUEST['filter_pin_id'] = $this->getCookie('filter_pin_id', $_REQUEST['filter_pin_id']);
            $_REQUEST['filter_addr_flat'] = $this->getCookie('filter_addr_flat', $_REQUEST['filter_addr_flat']);

            $_REQUEST['filter_clnt_type'] = $this->getCookie('filter_clnt_type', $_REQUEST['filter_clnt_type']);
            $_REQUEST['filter_status_id'] = array_filter(
                $this->getCookie('filter_status_id', $request->request->has('filter_status_id') ? $request->get('filter_status_id') : null),
                function($e) { return !empty($e) && -1 != $e; }
            );
            $_REQUEST['filter_ods_id'] = $this->getCookie('filter_ods_id', $_REQUEST['filter_ods_id']);
            $_REQUEST['filter_ctgrnum_id'] = $this->getCookie('filter_ctgrnum_id', $_REQUEST['filter_ctgrnum_id']);
            $_REQUEST['filter_addr'] = $this->getCookie('filter_addr', $_REQUEST['filter_addr']);

            $_REQUEST['filter_ti_date_from'] = $this->getCookie('filter_ti_date_from', $_REQUEST['filter_ti_date_from']);
            $_REQUEST['filter_ti_date_to'] = $this->getCookie('filter_ti_date_to', $_REQUEST['filter_ti_date_to']);


            $_REQUEST['filter_date_from'] = $this->getCookie('filter_date_from', $_REQUEST['filter_date_from']);
            $_REQUEST['filter_time_from'] = $this->getCookie('filter_time_from', $_REQUEST['filter_time_from']);

            $_REQUEST['filter_date_to'] = $this->getCookie('filter_date_to', $_REQUEST['filter_date_to']);
            $_REQUEST['filter_time_to'] = $this->getCookie('filter_time_to', $_REQUEST['filter_time_to']);
            $_REQUEST["date_gfx"] = $this->getCookie('date_gfx', $_REQUEST['date_gfx']);


            if ($_REQUEST['filter_date_from']) {
                if ($_REQUEST['filter_time_from']) {
                    $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                    $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d %H:%i')>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                    if ($_REQUEST['filter_date_to']) {
                        if ($_REQUEST['filter_time_to']) {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d %H:%i')<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);

                        } else {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d')<=" . $DB->F($dateTo);
                        }
                    }
                } else {

                    $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                    $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d') >=" . $DB->F($dateFrom);

                    if ($_REQUEST['filter_date_to']) {
                        if ($_REQUEST['filter_time_to']) {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d %H:%i')<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);

                        } else {
                            $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                            $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d')<=" . $DB->F($dateTo);
                        }
                    }
                }
            }
            if ($_REQUEST['filter_date_to']) {
                if ($_REQUEST['filter_time_to']) {
                    $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                    $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d %H:%i')<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);
                    if ($_REQUEST['filter_date_from']) {
                        if ($_REQUEST['filter_time_from']) {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d %H:%i')>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                        } else {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d') >=" . $DB->F($dateFrom);
                        }
                    }
                } else {
                    $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                    $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d')<=" . $DB->F($dateTo);
                    if ($_REQUEST['filter_date_from']) {
                        if ($_REQUEST['filter_time_from']) {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d %H:%i')>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                        } else {
                            $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                            $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d') >=" . $DB->F($dateFrom);
                        }
                    }
                }
            }

            if ($_REQUEST['date_gfx']) $filter .= " AND gfx.c_date=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['date_gfx'])));

            if ($_REQUEST['filter_id']) $filter .= " AND ts.id=" . $DB->F($_REQUEST['filter_id']);
            if ($_REQUEST['filter_cnt_id']) $filter .= " AND tt.cnt_id=" . $DB->F($_REQUEST['filter_cnt_id']);
            if (count($_REQUEST['filter_type']) > 0) {
                $type = $_REQUEST['filter_type'][0];
                if ("-1" === $type) {
                    $filter .= " AND tt.task_type IS NULL";
                } elseif (!empty ($type)) {
                    $filter .= " AND tt.task_type = $type";
                }
            }
            if ($_REQUEST['filter_clnt_type']) $filter .= " AND tt.clnt_type=" . $DB->F($_REQUEST['filter_clnt_type']);
            if ($_REQUEST['filter_ods_id']) $filter .= " AND ods.id=" . $DB->F($_REQUEST['filter_ods_id']);

            if ($_REQUEST['filter_ctgrnum_id']) $filter .= " AND lower(tt.clnttnum) LIKE " . $DB->F("%" . strtolower($_REQUEST['filter_ctgrnum_id']) . "%");

            //JN Full address search
            if ($_REQUEST['filter_addr']) {
                $addr = '%' . preg_replace('/[^a-z0-9а-я-]+/iu', '%', $_REQUEST['filter_addr']) . '%';
                $filter .= " AND tt.dom_id IN (select id from list_addr where list_addr.full_address LIKE " . $DB->F($addr) . ")";
            }

            if ($_REQUEST["filter_addr_flat"]) $filter .= " AND tt.kv LIKE " . $DB->F(strtolower($_REQUEST['filter_addr_flat']) . "%");
            if ($_REQUEST["filter_pin_id"]) $filter .= " AND tt.clntopagr LIKE " . $DB->F("%" . strtolower($_REQUEST['filter_pin_id']) . "%");

            $sql_add = "";
            if ($_REQUEST['filter_ti_date_from'] || $_REQUEST['filter_ti_date_to']) {
                switch ($_REQUEST['filter_ti_sort_id']) {
                    case "1":
                        if ($_REQUEST['filter_ti_date_from'])
                            $filter .= " AND DATE_FORMAT(ts.lastcmm_date, '%Y-%m-%d')>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                        if ($_REQUEST['filter_ti_date_to'])
                            $filter .= " AND DATE_FORMAT(ts.lastcmm_date, '%Y-%m-%d')<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));
                        break;

                    case "2":
                        if ($_REQUEST['filter_ti_date_from'])
                            $filter .= " AND DATE_FORMAT(ts.date_reg, '%Y-%m-%d')>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                        if ($_REQUEST['filter_ti_date_to'])
                            $filter .= " AND DATE_FORMAT(ts.date_reg, '%Y-%m-%d')<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));

                        break;

                    case "3":
                        if ($_REQUEST['filter_ti_date_from'])
                            $filter .= " AND DATE_FORMAT(gfx.c_date, '%Y-%m-%d')>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                        if ($_REQUEST['filter_ti_date_to'])
                            $filter .= " AND DATE_FORMAT(gfx.c_date, '%Y-%m-%d')<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));
                        break;

                    case "4":
                        $status = adm_statuses_plugin::getStatusByTag("closed", "connections");
                        $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
                        if ($_REQUEST['filter_ti_date_from'])
                            $filter .= "AND contr_calc.id!='' AND DATE_FORMAT(ts.lastcmm_date, '%Y-%m-%d')>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                        if ($_REQUEST['filter_ti_date_to'])
                            $filter .= "AND contr_calc.id!='' AND DATE_FORMAT(ts.lastcmm_date, '%Y-%m-%d')<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));

                        $sql_add = " LEFT JOIN `list_contr_calc` AS contr_calc ON contr_calc.task_id=tt.task_id ";
                        break;
                    case "5":
                        $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
                        if ($_REQUEST['filter_ti_date_from']) {
                            $dateFrom = substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2);
                            $dateTo = substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2);
                            if ($_REQUEST['filter_ti_date_to']) {
                                $filter .= " AND ts.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id=" . $DB->F($doneStatus["id"]) . " AND
                                            (DATE_FORMAT(tc.datetime,'%Y-%m-%d')>=" . $DB->F($dateFrom) . " AND DATE_FORMAT(tc.datetime, '%Y-%m-%d')<=" . $DB->F($dateTo) . ")) ";
                            } else {
                                $filter .= " AND ts.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id=" . $DB->F($doneStatus["id"]) . " AND
                                            (DATE_FORMAT(tc.datetime,'%Y-%m-%d')>=" . $DB->F($dateFrom) . ")) ";
                            }
                        }
                        break;
                }
            }
            $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];
        } else {
            $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];
        }

        if (!empty($_REQUEST['filter_status_id'])) {
            $filter .= " AND ts.status_id in (" . implode(',', array_map($DB->F, $_REQUEST['filter_status_id'])) . ') ';
        }
        if (in_array($status_otlozh['id'], $_REQUEST['filter_status_id'])) {
            $_REQUEST['filter_sort_order'] = 'ASC';
            //$_REQUEST['filter_sort_id'] = 5;
        }

        $tpl->setVariable('FILTER_ID', htmlspecialchars($_REQUEST['filter_id']));
        $tpl->setVariable("FILTER_ADDR", $_REQUEST['filter_addr'] ? $_REQUEST['filter_addr'] : "");
        $tpl->setVariable("FILTER_DATE_GFX", $_REQUEST['date_gfx'] ? $_REQUEST['date_gfx'] : "");

        $tpl->setVariable("PIN_ID", $_REQUEST['filter_pin_id'] ? $_REQUEST['filter_pin_id'] : "");
        $tpl->setVariable("FILTER_ADDR_FLAT", $_REQUEST['filter_addr_flat'] ? $_REQUEST['filter_addr_flat'] : "");
        $tpl->setVariable('FILTER_CNT_OPTIONS', $this->getContrOptions($_REQUEST['filter_cnt_id']));
        $tpl->setVariable('FILTER_TYPE_OPTIONS', $this->getTypeOptions($_REQUEST['filter_type']));
        $tpl->setVariable('FILTER_CLNT_TYPE_SEL_' . $_REQUEST['filter_clnt_type'], 'selected');

        $status = wf::$em->getRepository(\models\TaskStatus::class)->findBy(['pluginUid' => TaskEntity::CONNECTION]);
        $statuses = array_map(function (\models\TaskStatus $item) {
            return [
                'id' => $item->getId(),
                'text' => $item->getName()
            ];
        }, $status);
        $statusStyles = implode("\n", array_map(function (\models\TaskStatus $item) {
            return ".status_{$item->getId()}{
                background:{$item->getColor()};
            }";
        }, $status));

        $tpl->setVariable('STATUS_STYLE', $statusStyles);
        $tpl->setVariable('FILTER_STATUS_OPTIONS', json_encode($statuses));
        $tpl->setVariable('FILTER_STATUS_OPTIONS_SET', (empty($_REQUEST['filter_status_id'])) ? '""' : json_encode($_REQUEST['filter_status_id']));
        $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
        $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
        $tpl->setVariable('FILTER_ODS_OPTIONS', $this->getODSOptions($_REQUEST['filter_ods_id']));
        $tpl->setVariable("FILTER_DATE_FROM", $_REQUEST['filter_date_from'] ? $_REQUEST['filter_date_from'] : "");
        $tpl->setVariable("FILTER_TIME_FROM", $_REQUEST['filter_time_from'] ? $_REQUEST['filter_time_from'] : "");
        $tpl->setVariable("FILTER_DATE_TO", $_REQUEST['filter_date_to'] ? $_REQUEST['filter_date_to'] : "");
        $tpl->setVariable("FILTER_TIME_TO", $_REQUEST['filter_time_to'] ? $_REQUEST['filter_time_to'] : "");
        $statusList = adm_statuses_plugin::getFullList("connections");
        if ($statusList) {
            $tpl->setCurrentBlock("sl-items");
            foreach ($statusList as $st_item) {
                $tpl->setCurrentBlock("statuslist");
                if (in_array($st_item["id"], $_REQUEST['filter_status_id'])) {
                    $tpl->setVariable("FF_SELECTED", 'ffstisel');
                }
                $tpl->setVariable("FF_STATUS_ID", $st_item["id"]);
                $tpl->setVariable("FF_STATUS", $st_item["name"]);
                $tpl->setVariable("FF_STATUS_COLOR", $st_item["color"]);
                $tpl->parse("statuslist");
            }
            if (in_array('-1', $_REQUEST['filter_status_id'])) {
                $tpl->setVariable("FF_ALL_SELECTED", "ffstisel");
            }
            $tpl->parse("sl-items");

        }


        $tpl->setVariable('FILTER_ROWS_PER_PAGE', array2options($rpp, $_REQUEST['filter_rpp']));
        if ($LKUser) {
            $tpl->setVariable('FILTER_SORT_TI_LK_OPTIONS', array2options($sort_tilk_ids, $_REQUEST['filter_sort_id']));
        } else {
            $tpl->setVariable('FILTER_TI_SORT_OPTIONS', array2options($sort_ti_ids, $_REQUEST['filter_ti_sort_id']));
        }
        $tpl->setVariable("FILTER_TI_DATE_FROM", $_REQUEST['filter_ti_date_from'] ? $_REQUEST['filter_ti_date_from'] : "");
        $tpl->setVariable("FILTER_TI_DATE_TO", $_REQUEST['filter_ti_date_to'] ? $_REQUEST['filter_ti_date_to'] : "");

        if (!$LKUser) {
            $tpl->setVariable('CTGRNUM_ID', $_REQUEST['filter_ctgrnum_id']);
        } else {
            $filter .= " AND tt.cnt_id=" . $DB->F($LKContrID) . " ";
        }
        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("delticket-head");
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
                    ca.contr_title AS CONTR,
                    typ.title AS TYPE,
                    tt.dom_id,
                    tt.clnt_org_name,
                    tt.clnt_fio,
                    tt.clnt_type,
					tt.clntopagr,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    addr.name,
                    addr.full_address,
                    addr.is_not_recognized,
                    gfx.c_date,
                    gfx.startTime,
                    user.fio,
                    tt.clnttnum,
                    tt.kv,
                    st.tag,
                    tt.clnttnum AS CONTR_ID,
					ts.date_fn,
					(ts.date_fn IS NOT NULL AND ts.date_fn <= NOW()) AS is_otlozh_date
                FROM
                    `tasks` AS ts
                    LEFT JOIN `tickets` AS tt ON ts.id=tt.task_id
                    LEFT JOIN `list_contr` AS ca ON tt.cnt_id=ca.id
                    LEFT JOIN `task_types` AS typ ON tt.task_type=typ.id
                    LEFT JOIN `task_status` AS st ON ts.status_id=st.id
                    LEFT JOIN `list_addr` AS addr ON addr.id=tt.dom_id
                    LEFT JOIN `gfx` AS gfx ON gfx.task_id=ts.id
                    LEFT JOIN `users` AS user ON user.id=gfx.empl_id
                    LEFT JOIN `list_ods` AS ods ON ods.id=addr.ods_id
                    LEFT JOIN `list_sc` AS sc ON sc.id=addr.sc_id
                     $sql_add
                WHERE $filter
                  and case when ifnull(tt.sc_id,0) = 0 then 1
                           when exists (select * from user_sc_access usa where usa.user_id = " . $USER->getId() . " and usa.sc_id = tt.sc_id) then 1
                           else 0
                       end = 1
                GROUP BY ts.id, gfx.c_date, gfx.startTime, user.fio
                ORDER BY
                    $order
                LIMIT " . intval($_REQUEST['filter_start']) . ", " . intval($_REQUEST["filter_rpp"]);

        $DB->query($sql);
        $total = $DB->getFoundRows();
        while ($a = $DB->fetch(true)) {
            if ($a['clnt_type'] == User::CLIENT_TYPE_PHYS) {
                $a['CLIENT'] = $a['clnt_fio'];
            } elseif ($a['clnt_type'] == User::CLIENT_TYPE_YUR) {
                $a['CLIENT'] = $a['clnt_org_name'];
            } else {
                $a['CLIENT'] = $a['clnt_org_name'] . ', ' . $a['clnt_fio'];
            }

            $a["HREF_ORDER"] = $this->getLink('vieworderdoc', "id={$a['TASK_ID']}");
            $a['HREF'] = $this->getLink('viewticket', "task_id={$a['TASK_ID']}");

            //JN Address by one row
            if (!empty($a['full_address'])) {
                if ($a['is_not_recognized']) {
                    $a['FULL_ADDRESS'] = $a['full_address'] . ' <sup>*</sup>';
                } else {
                    $a['FULL_ADDRESS'] = $a['full_address'];
                }

                $a['ADDR_FLAT'] = $a["kv"];
                $tpl->parse("kladr_addr");
            } else {
                $tpl->setCurrentBlock("no_kladr");
                $a['ADDR_NO_KLADR'] = "<font color=#FF0000>Адрес не задан</font>";
                $tpl->parse("no_kladr");
            }

            if ($a['c_date'] && $a['startTime']) {
                $startHour = (strlen(floor($a['startTime'] / 60)) == 1) ? "0" . (floor($a['startTime'] / 60)) : floor($a['startTime'] / 60);
                $startMin = (strlen(floor($a['startTime'] % 60)) == 1) ? "0" . (floor($a['startTime'] % 60)) : floor($a['startTime'] % 60);
                if (preg_match("/grafik/", $a['tag'])) {
                    $a['GFXTIME'] = rudate("d M Y", strtotime($a['c_date'])) . " " . $startHour . ":" . $startMin;
                }
            } elseif (preg_match("/otlozh/", $a['tag']) && $a['date_fn']) {
                $a['GFXTIME'] = rudate("d M Y", strtotime($a['date_fn']));
                $a['STATUS_NAME'] .= " до";
                if ($a['is_otlozh_date'] && (date("Y-m-d", strtotime($a['date_fn'])) < date("Y-m-d"))) $a['BLINK'] = "blink";
            }
            $t = new ConnectionTicket($a["TASK_ID"]);
            $currentStatus = $t->getStatusId();
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
            $countStatus = adm_statuses_plugin::getStatusByTag("report", "connections");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "connections");
            $targetStatus = false;
            if ($currentStatus == $doneStatus["id"] || $currentStatus == $countStatus["id"] || $currentStatus == $completeStatus["id"]) $targetStatus = true;


            $sql = "SELECT DATE_FORMAT(`datetime`, '%Y-%m-%d') AS doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($t->getId()) . " AND `status_id`=" . $DB->F($doneStatus["id"]) . " ORDER BY donedate DESC;";
            $doneDate = $DB->getField($sql);
            if (date("m") == "01") {
                $df = date("Y-m-d", strtotime(date("Y-m") . "+" . getcfg('connfinreport_dc_january') . "days"));
            } else $df = date("Y-m-d", strtotime(date("Y-m") . "+" . getcfg('connfinreport_dc') . "days"));
            $cd = date("Y-m-d");
            $readonly = false;
            if (!$USER->checkAccess("calcticketafterdeadline", User::ACCESS_WRITE) && $targetStatus) {
                if ($cd > $df) {
                    if (date("Y-m", strtotime($doneDate)) <= date("Y-m", strtotime(date("Y-m") . " - 1 month"))) $readonly = true; else $readonly = false;
                } else {
                    if (date("Y-m", strtotime($doneDate)) <= date("Y-m", strtotime(date("Y-m") . " - 2 month"))) $readonly = true; else $readonly = false;
                }
            }

            $a["LASTCOMMENTVALUE"] = preg_replace("/<|>/", "", strip_tags($t->getLastComment(true)));

            if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getSC")) {
                $scO = new adm_sc_plugin();
                $sc = $scO->getSC($t->getSCId());
            }
            $a['SC'] = $sc['title'] ? $sc['title'] : "<center>&mdash;</center>";
            $a['USERFIO'] = Task::getTaskIspoln2($a['TASK_ID']);
            $a['CONTR_ID'] = $a['CONTR_ID'] ? $a['CONTR_ID'] : "<center>&mdash;</center>";
            $a['FIZJUR'] = ($a['clnt_type'] == 1 ? "Физ" : "Юр");
            $a['PINVAL'] = $a['clntopagr'] ? $a['clntopagr'] : "<center>&mdash;</center>";
            $tpl->setCurrentBlock('row');
            $tpl->setVariable($a);
            if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE) && !$readonly) {
                $tpl->setCurrentBlock("deleteticket");
                $tpl->setVariable("DELETEHREF", $this->getLink('deleteticket', "task_id={$a['TASK_ID']}"));
                $tpl->parse("deleteticket");
            }
            $tpl->parse('row');
        }

        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"], $total, "#start-%s"));
        UIHeader($tpl);
        $tpl->show();
    }

    static function getSCOptions($sel_id = 0)
    {
        global $DB, $USER;
        $sql = "SELECT
            s.id, s.title
            FROM list_sc s,
            user_sc_access usa
            WHERE s.id = usa.sc_id
              AND usa.user_id = " . $USER->getId() . "
            ORDER BY s.title";
        $result = $DB->getCell2($sql);
        $result[-1] = "-- Все СЦ --";
        $result[0] = "-- Не указан --";

        ksort($result);
        return array2options($result, $sel_id, false);
    }

    static function getEmplOptions($sel_id = 0)
    {
        global $DB;
        $sql_add = "";
        if ($isChiefTech = User::isChiefTech(\wf::getUserId())) {
            $sql_add = " AND user.id IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($isChiefTech) . ")";
        }

        $sql = "SELECT user.id, user.fio FROM `list_empl` AS empl LEFT JOIN `users` AS user ON user.id=empl.user_id WHERE user.active='1' " . $sql_add . " ORDER BY `user`.fio";

        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getContrOptions($sel_id = 0)
    {
        global $DB;
        $sql = "SELECT `id`, concat(`contr_title`,' (',`official_title`,')') AS val FROM `list_contr` WHERE `contr_type` ORDER BY `contr_title`";

        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getTypeOptions($sel_id = 0)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`=" . $DB->F($plugin_uid) . " ORDER BY `title`";

        return array2options(['' => '- Все -', '-1' => 'He выбран'] + $DB->getCell2($sql), $sel_id, false);
    }

    static function getODSOptions($sel_id = 0)
    {
        global $DB;
        $sql = "SELECT `id`, `title` FROM `list_ods` ORDER BY `title`";

        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    function getwtypelist()
    {
        global $DB;
        $contr_id = $_POST['contr_id'];
        $type = $_POST['type'];
        if ($contr_id === false || $type === false) {
            echo "error";

            return;
        }
        $sql = "SELECT wt.id, wt.title FROM `list_contr_agr` AS ca
                LEFT JOIN `link_agr_wtypes` AS law ON law.agr_id=ca.id
                LEFT JOIN `task_types` AS wt ON wt.id=law.wtype_id WHERE ca.contr_id=" . $DB->F($contr_id) . " AND wt.title IS NOT NULL AND wt.plugin_uid=" . $DB->F($type) . " ORDER BY wt.title;";

        if (array2options($DB->getCell2($sql))) echo array2options($DB->getCell2($sql), $_POST['sel_id']); else echo "<option value=\"\">-- нет видов работ --</option>";

    }

    function getagrlist()
    {
        global $DB;
        $contr_id = $_POST['contr_id'];
        $wtype_id = $_POST['wtype_id'];
        if ($contr_id === false || $wtype_id === false) {
            echo "error";

            return;
        }
        $sql = "SELECT `id`, CONCAT(`number`, ' от ', `agr_date`) AS agr FROM `list_contr_agr` WHERE `contr_id`=" . $DB->F($contr_id) . " AND `id` IN (SELECT `agr_id` FROM `link_agr_wtypes` WHERE `wtype_id`=" . $DB->F($wtype_id) . ");";

        if (array2options($DB->getCell2($sql))) echo array2options($DB->getCell2($sql), $_POST['sel_id']); else echo "<option value=\"\">-- нет договоров --</option>";
    }

    function viewlist_export(Request $request)
    {
        header("Content-type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=connections_export_" . date("H_i_d-M-Y") . ".xls");

        global $DB, $USER;
        $g_user_id = $this->getUserId();

        $sort_ids = [
            1 => "Дата изменения",
            2 => "Дата создания",
            3 => "Дата в графике",
            4 => "Техник",
            6 => "ID заявки",
            7 => "ID заявки контрагнета",
            8 => "Тип",
            9 => "Контрагент",
            10 => "Вид работ",
            11 => "СЦ",
            12 => "Район",
            13 => "Статус",
            14 => "Адрес",
            15 => "ПИН",
        ];
        $sort_sql = [
            1 => "ts.lastcmm_date",
            2 => "ts.date_reg",
            3 => "1",
            4 => "1",
            5 => "is_otlozh_date DESC, ts.date_fn",
            6 => "ts.id",
            7 => "tt.clnttnum",
            8 => "tt.clnt_type",
            9 => "ca.contr_title",
            10 => "typ.title",
            11 => "sc.title",
            13 => "st.name",
            14 => "kstreet.NAME",
            15 => "clntopagr",
        ];

        $sort_ti_ids = [
            1 => "Дата изменения",
            2 => "Дата создания",
            3 => "Дата в графике",
            4 => "Дата расчета",
            5 => "Дата выполнения",
        ];

        $sort_order = [
            'DESC' => "Обратный",
            'ASC' => "Прямой",
        ];

        $rpp = [
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250",
        ];
        $status_otlozh = $this->getStatusesByTag('otlozh');
        $status_otlozh = $status_otlozh[0];

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/export.tmpl.htm");
        $filter = "ts.plugin_uid=" . $DB->F($this->getUID());
        $isTech = User::isTech($g_user_id);
        $isChiefTech = User::isChiefTech($g_user_id);
        if (($isTech || $isChiefTech) && !$USER->checkAccess("scfullaccess")) {
            if ($isChiefTech) {
                $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);
                $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $isChiefTech);
                $tpl->setVariable("FILTER_SC_VAL", $_REQUEST['filter_sc_id']);
                if ($_REQUEST['filter_sc_id']) $filter .= " AND tt.sc_id=" . $DB->F($_REQUEST['filter_sc_id']);
                if ($_REQUEST['filter_empl_id']) $filter .= " AND gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']);
                $tpl->setCurrentBlock("techlist");
                $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
                $tpl->parse("techlist");
            } elseif ($isTech) {
                $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $g_user_id);
                if ($_REQUEST['filter_empl_id']) $filter .= " AND gfx.empl_id=" . $DB->F($g_user_id);
            }
        } else {
            $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $_REQUEST['filter_sc_id']);
            $tpl->setVariable("FILTER_SC_VAL", $_REQUEST['filter_sc_id']);
            $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);
            if ($request->get('filter_sc_id', -1) > 0) {
                $filter .= " AND tt.sc_id=" . $request->request->getInt('filter_sc_id');
            } elseif (0 === $request->get('filter_sc_id', -1)) {
                $filter .= " AND (tt.sc_id IS NULL OR tt.sc_id = 0)";
            }
            if ($_REQUEST['filter_empl_id']) $filter .= " AND gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']);
            $tpl->setCurrentBlock("sclist");
            $tpl->setVariable('FILTER_SC_OPTIONS', $this->getSCOptions($_REQUEST['filter_sc_id']));
            $tpl->parse("sclist");
            $tpl->setCurrentBlock("techlist");
            $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
            $tpl->parse("techlist");
        }
        if (($isChiefTech && $isTech) || !$isTech) {
            $_REQUEST['filter_client_phone'] = $this->getCookie('filter_client_phone', $_REQUEST['filter_client_phone']);
            $tpl->setCurrentBlock("is_clnt_phone");
            $tpl->setVariable("FILTER_CLIENT_PHONE", $_REQUEST['filter_client_phone'] ? $_REQUEST['filter_client_phone'] : "");
            $tpl->parse("is_clnt_phone");
            if ($_REQUEST['filter_client_phone']) {
                $phone = preg_replace("/(\D+)/", "", $_REQUEST["filter_client_phone"]);
                $filter .= " AND (tt.clnt_tel1 LIKE '%" . $phone . "%' OR tt.clnt_tel2 LIKE '%" . $phone . "%') ";
            }
        } else {
            $_REQUEST['filter_client_phone'] = false;
        }
        //print_r($_REQUEST);
        $this->getSession()->set("filter_type", $_REQUEST['filter_type'] ? $_REQUEST['filter_type'] : $this->getSession()->get("filter_type"));
        $_REQUEST['filter_type'] = $this->getSession()->get("filter_type");
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_cnt_id'] = $this->getCookie('filter_cnt_id', $_REQUEST['filter_cnt_id']);
        $_REQUEST['filter_pin_id'] = $this->getCookie('filter_pin_id', $_REQUEST['filter_pin_id']);
        $_REQUEST['filter_addr_flat'] = $this->getCookie('filter_addr_flat', $_REQUEST['filter_addr_flat']);
        //$_REQUEST['filter_type'] = $this->getCookie('filter_type', $_REQUEST['filter_type']);
        $_REQUEST['filter_clnt_type'] = $this->getCookie('filter_clnt_type', $_REQUEST['filter_clnt_type']);
        $_REQUEST['filter_status_id'] = array_filter(
            $this->getCookie('filter_status_id', $request->request->has('filter_status_id') ? $request->get('filter_status_id') : null),
            function($e) { return !empty($e) && -1 != $e; }
        );
        $_REQUEST['filter_ods_id'] = $this->getCookie('filter_ods_id', $_REQUEST['filter_ods_id']);
        $_REQUEST['filter_ctgrnum_id'] = $this->getCookie('filter_ctgrnum_id', $_REQUEST['filter_ctgrnum_id']);
        $_REQUEST['filter_addr'] = $this->getCookie('filter_addr', $_REQUEST['filter_addr']);
        $_REQUEST['filter_ti_sort_id'] = $this->getCookie('filter_ti_sort_id', $_REQUEST['filter_ti_sort_id']);
        $_REQUEST['filter_ti_date_from'] = $this->getCookie('filter_ti_date_from', $_REQUEST['filter_ti_date_from']);
        $_REQUEST['filter_ti_date_to'] = $this->getCookie('filter_ti_date_to', $_REQUEST['filter_ti_date_to']);
        $_REQUEST["date_gfx"] = $this->getCookie('date_gfx', $_REQUEST['date_gfx']);


        if (!isset($_REQUEST["filter_rpp"])) $_REQUEST["filter_rpp"] = $USER->getRpp();

        if ($_REQUEST["filter_rpp"] != $USER->getRpp()) {
            $sql = "UPDATE `users` SET `rpp`=" . $_REQUEST["filter_rpp"] . " WHERE `id`=" . $DB->F($USER->getId()) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
        } else {
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        }

        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ? $_REQUEST['filter_sort_order'] : key($sort_order));

        $_REQUEST['filter_date_from'] = $this->getCookie('filter_date_from', $_REQUEST['filter_date_from']);
        $_REQUEST['filter_time_from'] = $this->getCookie('filter_time_from', $_REQUEST['filter_time_from']);

        $_REQUEST['filter_date_to'] = $this->getCookie('filter_date_to', $_REQUEST['filter_date_to']);
        $_REQUEST['filter_time_to'] = $this->getCookie('filter_time_to', $_REQUEST['filter_time_to']);

        if ($_REQUEST['filter_date_from']) {
            if ($_REQUEST['filter_time_from']) {
                $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d %H:%i')>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                if ($_REQUEST['filter_date_to']) {
                    if ($_REQUEST['filter_time_to']) {
                        $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                        $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d %H:%i')<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);

                    } else {
                        $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                        $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d')<=" . $DB->F($dateTo);
                    }
                }
            } else {

                $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d') >=" . $DB->F($dateFrom);

                if ($_REQUEST['filter_date_to']) {
                    if ($_REQUEST['filter_time_to']) {
                        $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                        $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d %H:%i')<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);

                    } else {
                        $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                        $filter .= " AND DATE_FORMAT(tt.slot_begin, '%Y-%m-%d')<=" . $DB->F($dateTo);
                    }
                }
            }
        }
        if ($_REQUEST['filter_date_to']) {
            if ($_REQUEST['filter_time_to']) {
                $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d %H:%i')<=" . $DB->F($dateTo . " " . $_REQUEST["filter_time_to"]);
                if ($_REQUEST['filter_date_from']) {
                    if ($_REQUEST['filter_time_from']) {
                        $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                        $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d %H:%i')>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                    } else {
                        $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                        $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d') >=" . $DB->F($dateFrom);
                    }
                }
            } else {
                $dateTo = substr($_REQUEST['filter_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_date_to'], 0, 2);
                $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d')<=" . $DB->F($dateTo);
                if ($_REQUEST['filter_date_from']) {
                    if ($_REQUEST['filter_time_from']) {
                        $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                        $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d %H:%i')>=" . $DB->F($dateFrom . " " . $_REQUEST["filter_time_from"]);
                    } else {
                        $dateFrom = substr($_REQUEST['filter_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_date_from'], 0, 2);
                        $filter .= " AND DATE_FORMAT(tt.slot_end, '%Y-%m-%d') >=" . $DB->F($dateFrom);
                    }
                }
            }
        }

        if (!empty($_REQUEST['filter_status_id'])) {
            $filter .= " AND ts.status_id in(" . implode(',', array_map($DB->F, $_REQUEST['filter_status_id'])) . ')';
        }
        if (in_array($status_otlozh['id'], $_REQUEST['filter_status_id'])) {
            $_REQUEST['filter_sort_order'] = 'ASC';
        }

        if ($_REQUEST['filter_id']) $filter .= " AND ts.id=" . $DB->F($_REQUEST['filter_id']);
        if ($_REQUEST['filter_cnt_id']) $filter .= " AND tt.cnt_id=" . $DB->F($_REQUEST['filter_cnt_id']);
        if (count($_REQUEST['filter_type']) > 0) {
            $type = $_REQUEST['filter_type'][0];
            if ("-1" === $type) {
                $filter .= " AND tt.task_type IS NULL";
            } elseif (!empty ($type)) {
                $filter .= " AND tt.task_type = $type";
            }
        }
        if ($_REQUEST['filter_clnt_type']) $filter .= " AND tt.clnt_type=" . $DB->F($_REQUEST['filter_clnt_type']);
        if ($_REQUEST['filter_ods_id']) $filter .= " AND ods.id=" . $DB->F($_REQUEST['filter_ods_id']);

        if ($_REQUEST['date_gfx']) $filter .= " AND gfx.c_date=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['date_gfx'])));

        if ($_REQUEST['filter_ctgrnum_id']) $filter .= " AND lower(tt.clnttnum) LIKE " . $DB->F("%" . strtolower($_REQUEST['filter_ctgrnum_id']) . "%");
        if ($_REQUEST['filter_addr']) $filter .= "AND `dom_id` IN (select la.id from
                                                (select LEFT(ks.code, 15) as code
                                                from `kladr_street` AS ks
                                                WHERE ks.name LIKE " . $DB->F($_REQUEST['filter_addr'] . "%") . ") as kss
                                                join `list_addr` AS la on kss.code=LEFT(la.kladrcode, 15))";

        if ($_REQUEST["filter_addr_flat"]) $filter .= " AND tt.kv LIKE " . $DB->F(strtolower($_REQUEST['filter_addr_flat']) . "%");
        if ($_REQUEST["filter_pin_id"]) $filter .= " AND tt.clntopagr LIKE " . $DB->F("%" . strtolower($_REQUEST['filter_pin_id']) . "%");

        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];

        $sql_add = "";
        if ($_REQUEST['filter_ti_date_from'] || $_REQUEST['filter_ti_date_to']) {
            switch ($_REQUEST['filter_ti_sort_id']) {
                case "1":
                    if ($_REQUEST['filter_ti_date_from'])
                        $filter .= " AND DATE_FORMAT(ts.lastcmm_date, '%Y-%m-%d')>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                    if ($_REQUEST['filter_ti_date_to'])
                        $filter .= " AND DATE_FORMAT(ts.lastcmm_date, '%Y-%m-%d')<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));
                    break;

                case "2":
                    if ($_REQUEST['filter_ti_date_from'])
                        $filter .= " AND DATE_FORMAT(ts.date_reg, '%Y-%m-%d')>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                    if ($_REQUEST['filter_ti_date_to'])
                        $filter .= " AND DATE_FORMAT(ts.date_reg, '%Y-%m-%d')<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));

                    break;

                case "3":
                    if ($_REQUEST['filter_ti_date_from'])
                        $filter .= " AND DATE_FORMAT(gfx.c_date, '%Y-%m-%d')>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                    if ($_REQUEST['filter_ti_date_to'])
                        $filter .= " AND DATE_FORMAT(gfx.c_date, '%Y-%m-%d')<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));
                    break;

                case "4":
                    $status = adm_statuses_plugin::getStatusByTag("closed", "connections");
                    $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
                    if ($_REQUEST['filter_ti_date_from'])
                        $filter .= "AND contr_calc.id!='' AND DATE_FORMAT(ts.lastcmm_date, '%Y-%m-%d')>=" . $DB->F(substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2));
                    if ($_REQUEST['filter_ti_date_to'])
                        $filter .= "AND contr_calc.id!='' AND DATE_FORMAT(ts.lastcmm_date, '%Y-%m-%d')<=" . $DB->F(substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2));

                    $sql_add = " LEFT JOIN `list_contr_calc` AS contr_calc ON contr_calc.task_id=tt.task_id ";
                    break;
                case "5":
                    $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
                    if ($_REQUEST['filter_ti_date_from']) {
                        $dateFrom = substr($_REQUEST['filter_ti_date_from'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_from'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_from'], 0, 2);
                        $dateTo = substr($_REQUEST['filter_ti_date_to'], 6, 4) . "-" . substr($_REQUEST['filter_ti_date_to'], 3, 2) . "-" . substr($_REQUEST['filter_ti_date_to'], 0, 2);
                        if ($_REQUEST['filter_ti_date_to']) {
                            $filter .= " AND ts.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id=" . $DB->F($doneStatus["id"]) . " AND
                                        (DATE_FORMAT(tc.datetime,'%Y-%m-%d')>=" . $DB->F($dateFrom) . " AND DATE_FORMAT(tc.datetime, '%Y-%m-%d')<=" . $DB->F($dateTo) . ")) ";
                        } else {
                            $filter .= " AND ts.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id=" . $DB->F($doneStatus["id"]) . " AND
                                        (DATE_FORMAT(tc.datetime,'%Y-%m-%d')>=" . $DB->F($dateFrom) . ")) ";
                        }
                    }
                    break;
            }
        }


        if (($isChiefTech || $isTech) && !$USER->checkAccess("scfullaccess")) {
            $sc_id = $isChiefTech ? $isChiefTech : User::getScIdByUserId($g_user_id);
        }
        $tpl->setVariable('CTGRNUM_ID', $_REQUEST['filter_ctgrnum_id']);

        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("delticket-head");
        }

        $sc_id_filter = (int)$_REQUEST['filter_sc_id'];
        if ($sc_id_filter > 0) {
            $filter .= " and tt.sc_id = " . $sc_id_filter;
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y') AS DATE,
                    ca.contr_title AS CONTR,
                    typ.title AS TYPE,
                    tt.dom_id,
                    tt.clnt_org_name,
                    tt.clnt_fio,
                    tt.clnt_type,tt.clntopagr,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    addr.name,
                    addr.full_address,
                    addr.is_not_recognized,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    addr.name, addr.kladrcode, addr.althouse, gfx.c_date, gfx.startTime, user.fio, tt.clnttnum, tt.kv, st.tag, tt.clnttnum AS CONTR_ID,
					ts.date_fn,
					(ts.date_fn IS NOT NULL AND ts.date_fn <= NOW()) AS is_otlozh_date
                FROM
                    `tasks` AS ts
                    LEFT JOIN `tickets` AS tt ON ts.id=tt.task_id
                    LEFT JOIN `list_contr` AS ca ON tt.cnt_id=ca.id
                    LEFT JOIN `task_types` AS typ ON tt.task_type=typ.id
                    LEFT JOIN `task_status` AS st ON ts.status_id=st.id
                    LEFT JOIN `list_addr` AS addr ON addr.id=tt.dom_id
                    LEFT JOIN `gfx` AS gfx ON gfx.task_id=ts.id
                    LEFT JOIN `users` AS user ON user.id=gfx.empl_id
                    LEFT JOIN `list_ods` AS ods ON ods.id=addr.ods_id
                    LEFT JOIN `list_sc` AS sc ON sc.id=addr.sc_id
                    LEFT JOIN `kladr_street` AS kstreet ON CONCAT(SUBSTR(addr.kladrcode, 1, 15), '00')=kstreet.CODE
                     $sql_add
                WHERE $filter
                  and case when ifnull(tt.sc_id,0) = 0 then 1
                           when exists (select * from user_sc_access usa where usa.user_id = " . $USER->getId() . " and usa.sc_id = tt.sc_id) then 1
                           else 0
                       end = 1
                ORDER BY
                    $order
                LIMIT " . intval($_REQUEST['filter_start']) . ", " . intval($_REQUEST["filter_rpp"]);

        $DB->query($sql);
        while ($a = $DB->fetch(true)) {
            //$cache[] = $a;
            $resp = $DB->getCell("SELECT user.fio FROM `task_users` AS tu LEFT JOIN `users` AS user ON user.id=tu.user_id WHERE tu.task_id=" . $DB->F($a['TASK_ID']) . " AND tu.ispoln ;");
            if (!$resp) {
                $resp_user = "<i>" . $a['fio'] . "</i>";
            } else {
                $r = false;
                foreach ($resp as $item) {
                    $r[] = "$item";
                }
                $resp_user = implode(", <br/ >", $r);
            }

            if ($a['clnt_type'] == User::CLIENT_TYPE_PHYS) $a['CLIENT'] = $a['clnt_fio'];
            elseif ($a['clnt_type'] == User::CLIENT_TYPE_YUR) $a['CLIENT'] = $a['clnt_org_name'];
            else $a['CLIENT'] = $a['clnt_org_name'] . ', ' . $a['clnt_fio'];

            //JN Address by one row
            if (!empty($a['full_address'])) {
                if ($a['is_not_recognized']) {
                    $a['FULL_ADDRESS'] = $a['full_address'] . ' <sup>*</sup>';
                } else {
                    $a['FULL_ADDRESS'] = $a['full_address'];
                }

                $a['ADDR_FLAT'] = $a["kv"];
                $tpl->parse("kladr_addr");
            } else {
                $tpl->setCurrentBlock("no_kladr");
                $a['ADDR_NO_KLADR'] = "<font color=#FF0000>Адрес не распознан</font>";
                $tpl->parse("no_kladr");
            }

            if ($a['c_date'] && $a['startTime']) {
                $startHour = (strlen(floor($a['startTime'] / 60)) == 1) ? "0" . (floor($a['startTime'] / 60)) : floor($a['startTime'] / 60);
                $startMin = (strlen(floor($a['startTime'] % 60)) == 1) ? "0" . (floor($a['startTime'] % 60)) : floor($a['startTime'] % 60);
                if (preg_match("/grafik/", $a['tag'])) {
                    $a['GFXTIME'] = rudate("d M Y", strtotime($a['c_date'])) . " " . $startHour . ":" . $startMin;
                }
            } elseif (preg_match("/otlozh/", $a['tag']) && $a['date_fn']) {
                $a['GFXTIME'] = rudate("d M Y", strtotime($a['date_fn']));
                $a['STATUS_NAME'] .= " до";
                if ($_REQUEST['filter_sort_id'] == 5 && $a['is_otlozh_date']) $a['BLINK'] = "blink";
            }
            $t = new ConnectionTicket($a["TASK_ID"]);
            if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getSC")) {
                $scO = new adm_sc_plugin();
                $sc = $scO->getSC($t->getSCId());
            }
            $a['SC'] = $sc['title'] ? $sc['title'] : "<center>&mdash;</center>";
            $a['PINVAL'] = $a['clntopagr'] ? $a['clntopagr'] : "<center>&mdash;</center>";
            $a['USERFIO'] = $resp_user ? $resp_user : "<center>&mdash;</center>";
            $a['CONTR_ID'] = $a['CONTR_ID'] ? $a['CONTR_ID'] : "<center>&mdash;</center>";
            $a['FIZJUR'] = ($a['clnt_type'] == 1 ? "Физ" : "Юр");

            $tpl->setCurrentBlock('row');
            $tpl->setVariable($a);
            if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
                $tpl->setCurrentBlock("deleteticket");
                $tpl->setVariable("DELETEHREF", $this->getLink('deleteticket', "task_id={$a['TASK_ID']}"));
                $tpl->parse("deleteticket");
            }
            $tpl->parse('row');
        }

        $DB->query($sql);
        $total = $DB->getFoundRows();
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"], $total, "#start-%s"));
        $tpl->setVariable("CURRENT_USER", $USER->getFio());
        $tpl->setVariable("CURRENT_DATE", rudate("H:i d M Y"));


        $tpl->show();

    }

    /*
     * Расчет по заявке который делает техник!!!!!
     *
     * это тоже самое что и в методе calcticket только для техника!!! АААААААААААААААААААААА!!А!А!А1
     */

    function clearreport()
    {
        global $DB, $USER;
        if (!$USER->checkAccess("erasetaskreport", User::ACCESS_WRITE)) {
            UIError("В доступе отказано!");

            return false;
        }
        if (!$_GET["task_id"]) {
            UIError("Не указан идентификатор заявки!");

            return false;
        }
        $csql = "DELETE FROM `list_contr_calc` WHERE `task_id`=" . $DB->F($_GET["task_id"]) . ";";
        $DB->query($csql);
        if ($DB->errno()) UIError($DB->error());
        $tsql = "DELETE FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($_GET["task_id"]) . ";";
        $DB->query($tsql);
        if ($DB->errno()) UIError($DB->error());
        $tmcsql = "SELECT `tmc_tech_sc_id` FROM `tmc_ticket` WHERE `task_id`=" . $DB->F($_GET["task_id"]) . ";";
        $rec = $DB->getCell($tmcsql);
        if ($DB->errno()) UIError($DB->error());
        if ($rec) {
            foreach ($rec as $i) {
                $sql = "UPDATE `tmc_sc_tech` SET `inplace`=1, ledit=now() WHERE `id`=" . $DB->F($i) . ";";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
            }
            $sql = "DELETE FROM `tmc_ticket` WHERE `task_id`=" . $DB->F($_GET["task_id"]) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
        }
        redirect("viewticket?task_id=" . $_GET["task_id"], "Расчет успешно очищен!");
    }

    function deleteticket()
    {
        global $DB, $USER;
        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            if ($id = $_REQUEST["task_id"]) {
                $tmc = $DB->getField("SELECT count(`id`) FROM `tmc_ticket` WHERE `task_id`=" . $DB->F($id) . ";");
                if ($tmc) {
                    UIError("Удаление заявки невозможно, т.к. по ней есть списанные ТМЦ!");
                }
                $cnt = $DB->getField("SELECT count(`id`) FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($id) . ";");
                if ($cnt) {
                    UIError("Удаление заявки невозможно, т.к. по ней выполнен расчет!");
                }

                $err = [];
                $sql = "DELETE FROM `tasks` WHERE `id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_files` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_comments` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_users` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `tickets` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `gfx` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                $sql = "DELETE FROM `joinedtasks` WHERE (`task_id`=" . $DB->F($id) . " OR joined_task_id = " . $DB->F($id) . " );";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                if (!sizeof($err))
                    redirect($this->getLink('viewlist'), "Заявка с ID $id успешно удалена.");
                else
                    redirect($this->getLink('viewlist'), "Удаление заявки с ID $id завершено с ошибками! Обратитесь к разработчикам! " . implode("\r\n", $err));

            } else {
                redirect($this->getLink('viewlist'), "Не указан идентификатор заявки для удаления.");

            }
        } else {
            redirect($this->getLink('viewlist'), "Нет доступа к удалению заявок.");
        }

    }

    function setChecked($id)
    {
        global $DB;
        if (!$id) {
            return false;
        }
        $sql = "UPDATE `tickets` SET `checked`=" . $DB->F("1") . ";";
        $DB->query($sql);
        if ($DB->errno()) {
            return false;
        } else {
            return true;
        }
    }

    function viewticket(Request $request)
    {
        global $DB, $USER;
        $rejectList = [10, 46, 43, 5, 65];
        $doneList = [61, 54, 52, 1, 26, 23, 12, 25, 24, 49, 99];
        $clnttype = [
            1 => "Физ. лицо",
            2 => "Юр. лицо",
        ];
        $id = (int)($_GET['task_id'] ? $_GET['task_id'] : ($_GET['id'] ? $_GET['id'] : "0"));
        $LKUser = adm_users_plugin::isLKUser($USER->getId());

        if ($id > 0) {
            $sql = "SELECT max(sc.title) title
                    FROM list_sc sc,
                    tickets tt,
                    tasks t
                    WHERE tt.task_id = " . $id . "
                      AND t.id = tt.task_id
                      AND sc.id = tt.sc_id
                      AND t.plugin_uid = '" . $this->getUID() . "'
                      AND CASE WHEN ifnull(tt.sc_id,0) = 0 THEN 0
                               WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = tt.sc_id) THEN 0
                               ELSE 1
                          END = 1";
            $sc_with_no_access = $DB->getField($sql);
            if (strlen($sc_with_no_access) > 0) {
                #$sc_with_no_access .= $sql;
                UIError("У Вас нет доступа на СЦ " . $sc_with_no_access . ", просмотр заявки невозможен.");
            }
        }

        if (!$id) {
            UIError("Не указан идентификатор заявки");
        }

        $ticket = new ConnectionTicket($id);
        if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
            $tech_id = $DB->getField("SELECT `empl_id` FROM `gfx` WHERE `task_id`=" . $DB->F($ticket->getId()) . ";");
            $vTech = adm_empl_plugin::isVirtual($USER->getId());
            if (array_key_exists($USER->getId(), $ticket->getIspoln()) || array_key_exists($vTech, $ticket->getIspoln()) || $tech_id == $USER->getId()) {
            } else {
                UIError("В доступе отказано!");
            }
        }
        if (!$ticket->getId()) UIError("Заявка с таким номером не найдена! Возможно, она была удалена.");
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $currentStatus = $ticket->getStatusId();

        $modelTask = $this->getEm()->find(TaskEntity::class, $id);
        if (in_array($currentStatus, [TaskStatus::STATUS_CONNECTION_DONE, TaskStatus::STATUS_CONNECTION_REPORT, TaskStatus::STATUS_CONNECTION_CLOSED])
            && !$this->getContainer()->get('wf.task.task_manager')->isTaskClosedInPeriod($modelTask, new DateTime())
            && !$USER->checkAccess("calcticketafterdeadline", User::ACCESS_WRITE)
        ) {
            $tpl->loadTemplatefile($USER->getTemplate() . "/ticket_readonly.tmpl.htm");
        } elseif (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
            $tpl->loadTemplatefile($USER->getTemplate() . "/ticket_tlk.tmpl.htm");
        } elseif ($LKUser) {
            $tpl->loadTemplatefile($USER->getTemplate() . "/ticket_lk.tmpl.htm");
        } else {
            $tpl->loadTemplatefile($USER->getTemplate() . "/ticket.tmpl.htm");
        }

        /**
         * check lk user access to task
         */
        if (null !== $this->getUserModel()->getMasterPartner() &&
            (
                !$this->getUserModel()->getAllowAccessCu() || $this->getUserModel()->getMasterPartner() !== $modelTask->getAnyTicket()->getPartner()
            )
        ) {
            throw new AccessDeniedException('У вас нет доступа к этой заявке!');
        }


        if (!$USER->checkAccess("rooteditticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock('is_contr_fields');
        }

        if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
            $tpl->touchBlock("islktech");
        } else {
            $tpl->touchBlock("isnolktech");
        }

        if ($USER->checkAccess("calc_tmc_ticket", User::ACCESS_WRITE) && !$USER->checkAccess("calc_ticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("tmccalc");
        }

        # EF: Доступ на кнопку с анкетой лояльности и опросником
        if ($USER->checkAccess("connections_questionnarie_access", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("questionnarie");
            $tpl->setVariable("QU_TASK_ID", $_GET["task_id"]);
            $tpl->parse("questionnarie");
        }

        if ($USER->checkAccess("erasetaskreport")) {
            $tpl->setCurrentBlock("erasereport");
            $tpl->setVariable("TASK__ID", $id);
            $tpl->parse("erasereport");
        }
        if (!$USER->checkAccess("rooteditticket", User::ACCESS_WRITE)) {
            $sql = "SELECT COUNT(*) FROM `contr_tickets` WHERE `task_id`=" . $DB->F($ticket->getId());
            if ($DB->getField($sql) && $ticket->getClntOpAgr() != "") {
                $tpl->setVariable('IS_CONTR_READONLY', 'readonly');
                $tpl->touchBlock('is_contr_fields');
            }
        }
        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("deleteticket");
            $tpl->setVariable("DEL_TASK_ID", $_GET["task_id"]);
            $tpl->setVariable("PLUG_UID", $this->getUID());
            $tpl->parse("deleteticket");
        }

        /**
         * Отрисовка кнопки и блока связанных заявок
         */
        \WF\Legacy\TicketViewModificator::renderAll(new Task($id), $tpl);

        $wtype_id = $DB->getField("SELECT `task_type` FROM `tickets` WHERE `task_id`=" . $DB->F($ticket->getId()) . ";");
        $cnt_id = $DB->getField("SELECT `cnt_id` FROM `tickets` WHERE `task_id`=" . $DB->F($ticket->getId()) . ";");
        $mgts_tc = $DB->getCell2("SELECT `id`, `title` FROM `mgts_tc` WHERE `active`;");
        $tpl->setVariable("MGTS_TC_LIST", array2options($mgts_tc, $ticket->getMGTS_TC()));
        $mgts_serv = $DB->getCell2("SELECT `id`, `title` FROM `mgts_services` WHERE 1;");
        $tpl->setVariable("MGTS_SERVICE_LIST", array2options($mgts_serv, $ticket->getMGTS_SER()));
        $tpl->setVariable("MGTS_ATS", $ticket->getMGTS_ATS());
        $tpl->setVariable("MGTS_ORSH", $ticket->getMGTS_ORSH());
        $tpl->setVariable("MGTS_CHAN", $ticket->getMGTS_CHAN());
        $tpl->setVariable("MGTS_NUMS", $ticket->getMGTS_PH());
        $tpl->setVariable("MGTS_ADSL_ATS", $ticket->getMGTS_ATS());
        $tpl->setVariable("MGTS_ADSL_LANDLINE", $ticket->getMGTS_LandlineNum());
        $tpl->setVariable("MGTS_AGR_NUM", $ticket->getMGTS_AgrNum());

        if ($wtype_id == "20" && ($cnt_id == "36" || $cnt_id == "791")) {
            $tpl->setVariable("MGTS_REQUIRED", " required=\"required\" ");
            $tpl->setVariable("NOTMGTSB2B_ADSL", "hidden");
        } else {
            if ($wtype_id == "37" && ($cnt_id == "36" || $cnt_id == "791")) {
                $tpl->setVariable("MGTS_REQUIRED_ADSL", " required=\"required\" ");
            } else {
                $tpl->setVariable("NOTMGTSB2B_ADSL", "hidden");
            }
            $tpl->setVariable("NOTMGTSB2B", "hidden");
        }
        if ($USER->checkAccess("callviewer") || $USER->checkAccess("plk.connection.view.calls")) {
            $tpl->touchBlock("callticket");
        }
        if ($USER->checkAccess("qc", User::ACCESS_WRITE)) {
            if (in_array($currentStatus, $rejectList) || in_array($currentStatus, $doneList)) {
                $tpl->setCurrentBlock("opros");
                $tpl->setVariable("OPROS_TASK_ID", $id);
                $tpl->setVariable("PL_UID", $ticket->getPluginUid());
                $tpl->setVariable("TASK_STATUS_ID", $currentStatus);
                $tpl->parse("opros");
            }
        }
        $tpl->setVariable("REASON_LIST", adm_gp_reason_plugin::getOptions());
        $tpl->setVariable("COMMENT_LIST", adm_gp_comment_plugin::getOptions());
        $tpl->setVariable('CONTR', $ticket->getContrName());
        $tpl->setVariable('TASK_TYPE', $ticket->getTypeName());
        if ($ticket->getDomId() && $ticket->getDomId() != "0") {
            $tpl->setCurrentBlock("kladr");

            $not_recognised_status = $DB->getField("SELECT `is_not_recognized` FROM list_addr WHERE `id` =" . $ticket->getDomId() . ";");
            $ticketEntity = $modelTask->getAnyTicket();

            if (!$ticketEntity instanceof \WF\Task\Model\TechnicalTicketInterface)
                throw new Exception('Wrong ticket type!');

            if ($not_recognised_status) {
                $tpl->setVariable('ADDR', "<i style='color:#FF0000' title='Адрес не распознан'>" . $ticketEntity->getDom()->getFullAddress() . "</i>");
            } else {
                $tpl->setVariable('ADDR', $ticketEntity->getDom()->getFullAddress());
            }

            $tpl->setVariable('LINK_TO_ADDR', "addr_interface/edit?id=" . $ticket->getDomId());
            $tpl->parse("kladr");
            if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getById")) {
                $addr = new addr_interface_plugin();
                $address = $addr->getById($ticket->getDomId());
                $tpl->setVariable("CURRENT_KLADR_CODE", $address["althouse"] != "" ? $address["kladrcode"] : $address["kladrcode"] . "_" . $address["name"]);
                $tpl->setVariable("CURRENT_HOUSE", $address["althouse"] != "" ? $address["kladrcode"] : $address["kladrcode"] . "_" . $address["name"]);
                $tpl->setVariable("CURRENT_SKC", substr($address["kladrcode"], 0, 11) . "00");
                $tpl->setVariable("CURRENT_REGION_CODE", substr($address["kladrcode"], 0, 11) . "00");
                $currentDefaultRegion = substr($address["kladrcode"], 0, 11) . "00";
                $tpl->setVariable("ALT_HOUSE", $address["althouse"]);
                //$tpl->setVariable("CURRENT_HOUSE", $address["name"]);

            }
        } else {
            $tpl->setCurrentBlock("no_kladr");
            $tpl->setVariable('NO_ADDR', "<font color='#FF0000'>Адрес не распознан</font>");
            $tpl->parse("no_kladr");
            $tpl->setVariable("CURRENT_REGION_CODE", getcfg('default_region'));
            $tpl->setVariable("CURRENT_KLADR_CODE", getcfg('default_region'));
        }
        if ($LKUser) {
            $tpl->setVariable("TASK_FROM_TITLE", adm_ticket_sources_plugin::getById($ticket->getTaskFrom()));
            $sc_name = adm_sc_plugin::getSC($ticket->getSCId());
            $tpl->setVariable("SC_NAME", $sc_name["title"] ? $sc_name["title"] : "&mdash;");
            $tpl->setVariable("SC_LK_ID", $ticket->getSCId());
        } else {
            $tpl->setVariable("SC_LIST", adm_sc_plugin::getScList($ticket->getSCId()));
            $tpl->setVariable("TASK_FROM", adm_ticket_sources_plugin::getOptions($ticket->getTaskFrom()));
        }


        //$tpl->setVariable("TASK_FROM_TITLE", adm_ticket_sources_plugin::getById($ticket->getTaskFrom()));
        //$tpl->setVariable("TASK_FROM", adm_ticket_sources_plugin::getOptions($ticket->getTaskFrom()));
        $tpl->setVariable("DOM_ENTR", $address["entr_count"] ? $address["entr_count"] : " - ");
        $tpl->setVariable("DOM_FLOORS", $address["floorcount"] ? $address["floorcount"] : " - ");
        $tpl->setVariable("DOM_FLATS", $address["flatcount"] ? $address["flatcount"] : " - ");
        $tpl->setVariable("DOM_OPLIST", "<strong> - </strong>");
        if ($USER->checkAccess("virt_editticket", User::ACCESS_WRITE) || $USER->checkAccess("allowodsandaddrparams", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("edit-addr-params");
            if (!$ticket->getDomId()) {

                $tpl->setVariable("ADDR_ENABLED", "disabled=\"disabled\"");
            } else {
                $tpl->setVariable("ADDR_ENABLED", "");

            }
            $tpl->parse("edit-addr-param");
        }
        $tpl->setVariable('DOM_ID', $ticket->getDomId());
        if ($USER->checkAccess("virt_editticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("edit-addr");
        }
        if ($USER->checkAccess("changectype", User::ACCESS_WRITE)) {

            if ($ticket->getClientType() == 1) {
                $tpl->touchBlock("client_type_1");
            } else {
                $tpl->touchBlock("client_type_2");
            }
            $tpl->setCurrentBlock("changectype");
            $tpl->setVariable("CLNTTYPE_LIST", array2options($clnttype, $ticket->getClientType()));
            $tpl->parse("changectype");
            if ($ticket->getClientType() != User::CLIENT_TYPE_PHYS) {
                $tpl->setVariable('ORG_NAME', $ticket->getOrgName());
            } else {
                $tpl->setVariable("JUR_HIDDEN", "hidden");
                $tpl->setVariable("JUR_DISABLED", "disabled='disabled'");
            }
        } else {
            $tpl->touchBlock('client_type_' . $ticket->getClientType());
            if ($ticket->getClientType() != User::CLIENT_TYPE_PHYS) {
                $tpl->setVariable('ORG_NAME', $ticket->getOrgName());
            } else {
                $tpl->setVariable("JUR_HIDDEN", "hidden");
                $tpl->setVariable("JUR_DISABLED", "disabled='disabled'");
            }
        }
        $tpl->setVariable("SC_NAME", $this->getSCName($ticket->getSCId()));

        $tpl->setVariable('SC_OPTIONS', $this->getSCOptions($ticket->getSCId()));
        $tpl->setVariable('POD', $ticket->getPod() ? $ticket->getPod() : " - ");
        $tpl->setVariable('ETAZH', $ticket->getEtazh() ? $ticket->getEtazh() : " - ");
        $tpl->setVariable('KV', $ticket->getKv() ? $ticket->getKv() : " - ");
        $tpl->setVariable('DOMOFON', $ticket->getDomofon() ? $ticket->getDomofon() : " - ");
        $tpl->setVariable('ADDINFO', $ticket->getAddInfo());
        $tpl->setVariable('SWIP', $ticket->getSwIp());
        $tpl->setVariable('SWPORT', $ticket->getSwPort());
        $tpl->setVariable('SWPLACE', $ticket->getSwPlace());
        $tpl->setVariable('CLNTOPAGR', $ticket->getClntOpAgr());
        $tpl->setVariable('SW_CABLEQUANT', $ticket->getCableQuant());
        $tpl->setVariable("REP_CONTR_DATETIME", $ticket->getFindateContr() ? $ticket->getFindateContr() : "&mdash;");
        $tpl->setVariable("REP_TECH_DATETIME", $ticket->getFindateTech() ? $ticket->getFindateTech() : "&mdash;");
        $tpl->setVariable("CLNTADDTNUM", $ticket->getAddNumber());
        $tpl->setVariable("OPERATOR_LOGIN", $ticket->getOpLogin() ? $ticket->getOpLogin() : "&mdash;");
        $tpl->setVariable("OPERATOR_PASSWORD", $ticket->getOpPassword() ? $ticket->getOpPassword() : "&mdash;");
        if ($ticket->getConndate()) {
            $conndate = explode(" ", $ticket->getConndate());
            if ($conndate[1] == "00:00:00")
                $tpl->setVariable("CONNECTION_DATE", $conndate[0]);
            else
                $tpl->setVariable("CONNECTION_DATE", $conndate[0] . " " . $conndate[1]);
        } else {
            $tpl->setVariable("CONNECTION_DATE", "&mdash;");
        }
        $tmc_act_id = $ticket->getTMCActId();
        $tmc_act_date = $DB->getField("SELECT DATE_FORMAT(`cr_date`, '%H:%i %d.%m.%Y') FROM `list_tmc_reports` WHERE `id`=" . $DB->F($tmc_act_id) . ";");
        if ($tmc_act_id && $tmc_act_date) {
            $tpl->setVariable("ACT_TMC_PARAMS", "<strong>№ " . $tmc_act_id . " от " . $tmc_act_date . "</strong>");
        } else {
            $tpl->setVariable("ACT_TMC_PARAMS", "<strong>&mdash;</strong>");
        }
        $ticket_act_id = $ticket->getTICKActId();
        $ticket_act_date = $DB->getField("SELECT DATE_FORMAT(`cr_date`, '%H:%i %d.%m.%Y') FROM `list_tdata_reports` WHERE `id`=" . $DB->F($ticket_act_id) . ";");
        if ($ticket_act_id && $ticket_act_date) {
            $tpl->setVariable("ACT_TICKET_PARAMS", "<strong>№ " . $ticket_act_id . " от " . $ticket_act_date . "</strong>");
        } else {
            $tpl->setVariable("ACT_TICKET_PARAMS", "<strong>&mdash;</strong>");
        }
        $tpl->setVariable('CLNTTNUM', $ticket->getClntTNum() ? $ticket->getClntTNum() : $DB->getField("SELECT `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=" . $DB->F($_GET["task_id"]) . ";"));
        $ods = $this->getOds($ticket->getDomId());
        if ($USER->checkAccess("virt_editticket", User::ACCESS_WRITE) || $USER->checkAccess("allowodsandaddrparams", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("edit-ods");
            if (!$ods["id"]) {
                $tpl->setVariable("ODS_ENABLED", "disabled=\"disabled\"");
            } else {
                $tpl->setVariable("ODS_ENABLED", "");
            }
            $tpl->parse("edit-ods");
        }

        if ($USER->checkAccess("changedonedate", User::ACCESS_WRITE)) {
            $donedate = $ticket->getDoneDate();
            if ($donedate && $donedate != "00.00.0000") {
                $tpl->setCurrentBlock("editdonedate");
                $tpl->setVariable("DONEDATE", $donedate);
                $tpl->parse("editdonedate");
            }
        }
        if ($ods['id']) {
            $tpl->setVariable("ODS_ID", $ods["id"]);
        }
        $tpl->setVariable("ODS_ID", $ods["id"] ? $ods["id"] : "");
        $tpl->setVariable("ODS_NAME", $ods["title"] ? $ods["title"] : "не указана");
        $tpl->setVariable("ODS_ADDR", $ods["address"] ? $ods["address"] : "-");
        $tpl->setVariable("ODS_TEL", $ods["tel"] ? $ods["tel"] : "-");
        $tpl->setVariable("ODS_CONTACTS", $ods["fio"] ? $ods["fio"] : "-");
        $tpl->setVariable("ODS_COMMENT", $ods["comment"] ? $ods["comment"] : "-");

        $tpl->setVariable('FIO', $ticket->getFio());
        $phnes = $ticket->getPhones();
        if ($USER->checkAccess("hidetelnum")) {
            $tpl->setVariable('PHONE1', "");
        } else {
            $tpl->setVariable('PHONE1', ($phnes[0] ? $phnes[0] : '+7'));
        }
        if ($phnes[0]) {
            if ($USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId()))) && $USER->getIntPhone()) {
                if ($phnes[0] != "70000000000") {
                    $tpl->setCurrentBlock("calltoclient1");
                    $tpl->setVariable('CLNT1PHONE', $phnes[0]);
                    $tpl->setVariable("EXTNUM1", $USER->getIntPhone());
                    $tpl->parse("calltoclient1");
                }
            } else {
                if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()) && $USER->getIntPhone()) {
                    if ($phnes[0] != "70000000000") {
                        $tpl->setCurrentBlock("calltoclient1tech");
                        $tpl->setVariable('CLNT1PHONETECH', $phnes[0]);
                        $tpl->setVariable("EXTNUM1TECH", $USER->getIntPhone());
                        $tpl->parse("calltoclient1tech");
                    }
                }
            }
        }
        if ($USER->checkAccess("hidetelnum")) {
            $tpl->setVariable('PHONE2', "");
        } else {
            $tpl->setVariable('PHONE2', ($phnes[1] ? $phnes[1] : '+7'));
        }
        if ($phnes[1]) {
            if ($USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId()))) && $USER->getIntPhone()) {
                if ($phnes[1] != "70000000000") {
                    $tpl->setCurrentBlock("calltoclient2");
                    $tpl->setVariable('CLNT2PHONE', $phnes[1]);
                    $tpl->setVariable("EXTNUM2", $USER->getIntPhone());
                    $tpl->parse("calltoclient2");
                }
            } else {
                if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()) && $USER->getIntPhone()) {
                    if ($phnes[1] != "70000000000") {
                        $tpl->setCurrentBlock("calltoclient2tech");
                        $tpl->setVariable('CLNT2PHONETECH', $phnes[1]);
                        $tpl->setVariable("EXTNUM2TECH", $USER->getIntPhone());
                        $tpl->parse("calltoclient2tech");
                    }
                }
            }
        }
        $rejTitle = '';
        $rej_id = $ticket->getRejectReasonId(false);
        if ($rej_id) {
            if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejReasonTitle")) {
                $rej = new adm_rejects_plugin();
                $rejTitle = " (" . $rej->getRejReasonTitle($rej_id) . ")";
            }
        }
        if (User::isTech($USER->getId()) && $USER->getIntPhone()) {
            $tpl->setCurrentBlock("calltogssdisp");
            $tpl->setVariable("DISPGROUPTECHPHONE", getcfg('dispgroupnumber'));
            $tpl->setVariable("TPGSSEXTNUM1TECH", $USER->getIntPhone());
            $tpl->setVariable("TPGSSTECHPHONE", getcfg('dispgroupnumber'));
            $tpl->parse("calltogssdisp");
        }
        $agr_id = $ticket->getAgrId(false);
        if ($agr_id) {
            $phoneList = agreements_plugin::getTPPhones($agr_id);
            if ($phoneList) {
                $tpl->setCurrentBlock("tpnumlist");
                if (User::isTech($USER->getId())) {
                    foreach ($phoneList as $tpi) {
                        $tpl->setCurrentBlock("tech_tnl");
                        $tpl->setVariable("TPTECHPHONE", $tpi["phone"]);
                        if ($tpi["ext"]) {
                            $tpl->setVariable("TPTECHDOB", $tpi["ext"]);
                            $tpl->setVariable("TPTECHDOBTITLE", " доб. " . $tpi["ext"]);
                        }
                        if ($tpi["phonetitle"]) {
                            $tpl->setVariable("BUTTONTITLE", $tpi["phonetitle"]);
                        } else {
                            $tpl->setVariable("BUTTONTITLE", "Позвонить в ТП");
                        }
                        $tpl->setVariable("TPEXTNUM1TECH", $USER->getIntPhone());
                        $tpl->parse("tech_tnl");
                    }
                } else {
                    if ($USER->checkAccess("callout")) {
                        foreach ($phoneList as $tpi) {
                            $tpl->setCurrentBlock("disp_tnl");
                            $tpl->setVariable("TPDISPPHONE", $tpi["phone"]);
                            if ($tpi["ext"]) {
                                $tpl->setVariable("TPDISPDOB", $tpi["ext"]);
                                $tpl->setVariable("TPDISPDOBTITLE", " доб. " . $tpi["ext"]);
                            }
                            if ($tpi["phonetitle"]) {
                                $tpl->setVariable("BUTTONTITLE", $tpi["phonetitle"]);
                            } else {
                                $tpl->setVariable("BUTTONTITLE", "Позвонить в ТП");
                            }
                            $tpl->setVariable("TPDISPEXT", $USER->getIntPhone());
                            $tpl->parse("disp_tnl");
                        }

                    }
                }
                $tpl->parse("tpnumlist");
            }
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getAgrById")) {
                $kontr = new kontr_plugin();
                $agr = $kontr->getAgrById($agr_id);
                if ($agr) {
                    $tpl->setVariable("TASK_AGR", $agr["number"] . " от " . $agr["agr_date"]);
                } else {
                    $tpl->setVariable("TASK_AGR", "<strong>ошибка</strong>");
                }
                $agr_contacts = $kontr->getAgrById($agr_id);
                if ($agr_contacts) {
                    $tpl->setVariable("AGR_ACCESS", $agr_contacts['access_contact'] ? nl2br($agr_contacts['access_contact']) : "&mdash;");
                    $tpl->setVariable("AGR_TP", $agr_contacts['tp_contact'] ? nl2br($agr_contacts['tp_contact']) : "&mdash;");

                } else {
                    $tpl->setVariable("AGR_ACCESS", "<strong>ошибка</strong>");
                    $tpl->setVariable("AGR_TP", "<strong>ошибка</strong>");
                }
            }
            $tpl->setVariable("AGR_ID", $agr_id);
        } else {
            $tpl->setVariable("TASK_AGR", "&mdash;");
            $tpl->setVariable("AGR_ACCESS", "&mdash;");
            $tpl->setVariable("AGR_TP", "&mdash;");
        }
        $tpl->setVariable('STATUS_NAME', $ticket->getStatusName() . " " . $rejTitle);

        $tpl->setVariable('STATUS_COLOR', $ticket->getStatusColor());
        $tpl->setVariable('DATE_REG', $ticket->getDateReg());
        $tpl->setVariable("USER_REG", $ticket->getAuthorFio() ? $ticket->getAuthorFio() : 'Система');
        $tpl->setVariable('TASK_COMMENTS', $ticket->getComments());
        $tpl->setVariable('TASK_ID', $ticket->getId());
        $sql = "SELECT gfx.c_date, gfx.startTime, user.fio, user.id FROM `gfx` AS gfx LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.task_id=" . $DB->F($_GET['task_id']) . ";";
        $result = $DB->getRow($sql);
        if ($result) {

            $startHour = (strlen(floor($result[1] / 60)) == 1) ? "0" . (floor($result[1] / 60)) : floor($result[1] / 60);
            $startMin = (strlen(floor($result[1] % 60)) == 1) ? "0" . (floor($result[1] % 60)) : floor($result[1] % 60);
            $tpl->setVariable("GFX_DATETIME", rudate("d M Y", strtotime($result[0])) . " " . $startHour . ":" . $startMin);
            $tpl->setVariable("GFX_EMPL", $result[2]);
            $emplPhone = adm_users_plugin::getUserPhone($result[3]);
            $emplPhone = explode(";", $emplPhone);
            if (count($emplPhone) > 1) {
                $phone1 = $emplPhone[0];
                $phone2 = $emplPhone[1];
                if ($phone1) {
                    $phone_clean = preg_replace("/[^0-9]/", "", $phone1);

                    if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
                    {
                        $phone_clean = $phone_clean;
                    } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
                    {
                        $phone_clean = substr($phone_clean, 1);
                    } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
                    {
                        $phone_clean = $phone_clean;
                    } else {
                        $phone_clean = false;
                    }
                    $phone1 = $phone_clean;
                    if ($phone1 && $USER->getIntPhone() && $USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId())))) {
                        $tpl->setVariable("calltotech");
                        $tpl->setVariable("TECHPHONE", $phone1);
                        $tpl->setVariable("EXT3NUM", $USER->getIntPhone());

                        $ctc_tech = new User($result[3]);
                        if ($ctc_tech) {
                            $ctc_phone = $ctc_tech->getIntPhone();

                            $targetPhone = false;

                            if ($ctc_phone && $phnes[0] && $phnes[0] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[0]);
                                $tpl->parse("ctc_button");
                            }
                            if ($ctc_phone && $phnes[1] && $phnes[1] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[1]);
                                $tpl->parse("ctc_button");
                            }
                        }
                        $tpl->parse("calltotech");
                    }
                }
                if ($phone2) {
                    $phone_clean = preg_replace("/[^0-9]/", "", $phone2);

                    if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
                    {
                        $phone_clean = $phone_clean;
                    } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
                    {
                        $phone_clean = substr($phone_clean, 1);
                    } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
                    {
                        $phone_clean = $phone_clean;
                    } else {
                        $phone_clean = false;
                    }
                    $phone2 = $phone_clean;
                    if ($phone2 && $USER->getIntPhone() && $USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId())))) {
                        $tpl->setVariable("calltotech");
                        $tpl->setVariable("TECHPHONE", $phone2);
                        $tpl->setVariable("EXT3NUM", $USER->getIntPhone());
                        $ctc_tech = new User($result[3]);
                        if ($ctc_tech) {
                            $ctc_phone = $ctc_tech->getIntPhone();

                            $targetPhone = false;

                            if ($ctc_phone && $phnes[0] && $phnes[0] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[0]);
                                $tpl->parse("ctc_button");
                            }
                            if ($ctc_phone && $phnes[1] && $phnes[1] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[1]);
                                $tpl->parse("ctc_button");
                            }
                        }
                        $tpl->parse("calltotech");
                    }
                }
            } else {
                if ($emplPhone[0]) {
                    $phone_clean = preg_replace("/[^0-9]/", "", $emplPhone[0]);

                    if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
                    {
                        $phone_cslean = $phone_clean;
                    } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
                    {
                        $phone_clean = substr($phone_clean, 1);
                    } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
                    {
                        $phone_clean = $phone_clean;
                    } else {
                        $phone_clean = false;
                    }
                    $phone = $phone_clean;
                    if ($phone && $USER->getIntPhone() && $USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId())))) {
                        $tpl->setVariable("calltotech");
                        $tpl->setVariable("TECHPHONE", $phone);
                        $tpl->setVariable("EXT3NUM", $USER->getIntPhone());
                        $ctc_tech = new User($result[3]);
                        if ($ctc_tech) {
                            $ctc_phone = $ctc_tech->getIntPhone();

                            $targetPhone = false;

                            if ($ctc_phone && $phnes[0] && $phnes[0] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[0]);
                                $tpl->parse("ctc_button");
                            }

                            if ($ctc_phone && $phnes[1] && $phnes[1] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[1]);
                                $tpl->parse("ctc_button");
                            }
                        }
                        $tpl->parse("calltotech");
                    }
                }
            }
            $tpl->touchBlock("insertinactive");
        } else {
            $tpl->setVariable("GFX_DATETIME", "НЕ НАЗНАЧЕН");
            $tpl->setVariable("GFX_EMPL", "НЕ НАЗНАЧЕН");
            $tpl->touchBlock("removeinactive");
            $tpl->touchBlock("resetinactive");
        }
        $tpl->setVariable('TYPE_OPTIONS', $this->getTypeOptions($this->getTypeID($ticket->getId())));
        $tpl->setVariable('CNT_OPTIONS', $this->getContrOptions($this->getContrID($ticket->getId())));

        if (class_exists("adm_ods_plugin", true) && method_exists("adm_ods_plugin", "getOptList")) {
            $addr = new adm_ods_plugin();
            $tpl->setVariable("ODS_OPTIONS", $addr->getOptList());
        }
        if ($USER->isTech($this->getUserId()) && !$USER->isChiefTech($this->getUserId())) {
            $tpl->touchBlock("removeinactive");
            $tpl->touchBlock("resetinactive");
            $tpl->setVariable("ISTECH_DISABLED", "disabled=\"disabled\"");
        }
        if (!$USER->checkAccess("calc_ticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("calcinactive");
            $calc_uid = $DB->getField("SELECT `tech_id` FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($ticket->getId()) . ";");
            $uid = $calc_uid ? $calc_uid : gfx_plugin::getCurrentTechIdAdGfx($ticket->getId());
            if ($uid) {
                $tpl->setVariable("CALC_TECH_LIST", adm_empl_plugin::getTechList($uid));
            }
            $tpl->setVariable("CALC_DATE", date("d.m.Y"));

            $tpl->setVariable("TMC_CAT_LIST_INFORM", adm_material_cats_plugin::getOptions());
        } else {
            $cwtList = $this->getCWType($ticket->getId());
            if ($cwtList) {
                foreach ($cwtList as $item) {
                    $tpl->setCurrentBlock("cw_row");
                    $tpl->setVariable("CW_TYPE_TITLE", "<strong>" . $item["s_title"] . "</strong><br /><small>" . $item['p_comment'] . "</small>");
                    $tpl->setVariable("CW_SMETA", $item["price_type"] == "smeta" ? $item["smeta_val"] : "&mdash;");
                    $tpl->setVariable("CW_ID", $item["id"]);
                    $tpl->setVariable("CW_ID_VAL", $item["price_type"] == "smeta" ? $item["smeta_val"] : $item["id"]);
                    $tpl->setVariable("CW_TYPE_T", $item["price_type"] == "smeta" ? "cs" . $item["id"] : "cp[]");
                    $tpl->setVariable("CW_TYPE", $item["price_type"] == "smeta" ? "cs" : "cp");
                    $tpl->parse("cw_row");
                }
            } else {
                $tpl->touchBlock("cw_norows");
            }
            $twtList = $this->getTWType($ticket->getId());
            if ($twtList) {
                foreach ($twtList as $item) {
                    $tpl->setCurrentBlock("tw_row");
                    $tpl->setVariable("TW_TYPE_TITLE", "<strong>" . $item["short_title"] . "</strong><br /><small>" . $item['p_comment'] . "</small>");
                    $tpl->setVariable("TW_SMETA", $item["price_type"] == "smeta" ? $item["smeta_val"] : "&mdash;");
                    $tpl->setVariable("TW_ID", $item["id"]);
                    $tpl->setVariable("TW_ID_VAL", $item["price_type"] == "smeta" ? $item["smeta_val"] : $item["id"]);
                    $tpl->setVariable("TW_TYPE_T", $item["price_type"] == "smeta" ? "ts" . $item["id"] : "tp[]");
                    $tpl->setVariable("TW_TYPE", $item["price_type"] == "smeta" ? "ts" : "tp");
                    $tpl->parse("tw_row");
                }
            } else {
                $tpl->touchBlock("tw_norows");
            }
            $tp = new adm_techprice_plugin();
            if ($tpList = $tp->getPItemsByType1("price")) {
                $tpl->setCurrentBlock("tech_wt_price_list");
                foreach ($tpList as $item) {
                    $tpl->setCurrentBlock("tech_price_row");
                    $tpl->setVariable("TPR_ID", $item["id"]);
                    $tpl->setVariable("TPR_TITLE", $item["short_title"]);
                    $tpl->setVariable("TPR_DESC", $item["p_comment"]);
                    $tpl->setVariable("TPR_PRICEVAL", $item["price"]);
                    $tpl->parse("tech_price_row");
                }
                $tpl->parse("tech_wt_price_list");
            } else {
                $tpl->touchBlock("tech_price_norows");
            }
            if ($tpList = $tp->getPItemsShtrafList1()) {
                $tpl->setCurrentBlock("techsh_wt_price_list");
                foreach ($tpList as $item) {
                    $tpl->setCurrentBlock("techsh_price_row");
                    $tpl->setVariable("TPRSH_ID", $item["id"]);
                    $tpl->setVariable("TPRSH_TITLE", $item["short_title"]);
                    $tpl->setVariable("TPRSH_DESC", $item["p_comment"]);
                    $tpl->setVariable("TPRSH_PRICEVAL", $item["price"]);
                    $tpl->parse("techsh_price_row");
                }
                $tpl->parse("techsh_wt_price_list");
            } else {
                $tpl->touchBlock("techsh_price_norows");
            }

            if ($tpList = $tp->getPItemsByType1("smeta")) {
                $tpl->setCurrentBlock("tech_wt_smeta_list");
                foreach ($tpList as $item) {
                    $tpl->setCurrentBlock("tech_smeta_row");
                    $tpl->setVariable("TSR_ID", $item["id"]);
                    $tpl->setVariable("TSR_TITLE", $item["short_title"]);
                    $tpl->setVariable("TSR_DESC", $item["p_comment"]);
                    $tpl->setVariable("TSR_VAL", $item["price"]);
                    $tpl->parse("tech_smeta_row");
                }
                $tpl->parse("tech_wt_smeta_list");
            } else {
                $tpl->touchBlock("tech_smeta_norows");
            }

            $cp = new kontr_plugin();
            if ($cpList = $cp->getPItemsByType1("price", $agr_id)) {
                $tpl->setCurrentBlock("kontr_wt_price_list");
                foreach ($cpList as $item) {
                    $tpl->setCurrentBlock("kontr_price_row");
                    $tpl->setVariable("CPR_ID", $item["id"]);
                    $tpl->setVariable("CPR_TITLE", $item["s_title"]);
                    $tpl->setVariable("CPR_DESC", $item["p_comment"]);
                    $tpl->setVariable("CPR_PRICEVAL", $item["price"]);
                    $tpl->parse("contr_price_row");
                }
                $tpl->parse("contr_wt_price_list");
            } else {
                $tpl->touchBlock("contr_price_norows");
            }

            if ($cpList = $cp->getPItemsByType1("smeta", $agr_id)) {
                $tpl->setCurrentBlock("contr_wt_smeta_list");
                foreach ($cpList as $item) {
                    $tpl->setCurrentBlock("contr_smeta_row");
                    $tpl->setVariable("CSR_ID", $item["id"]);
                    $tpl->setVariable("CSR_TITLE", $item["s_title"]);
                    $tpl->setVariable("CSR_DESC", $item["p_comment"]);
                    $tpl->parse("contr_smeta_row");
                }
                $tpl->parse("contr_wt_smeta_list");
            } else {
                $tpl->touchBlock("contr_smeta_norows");
            }

            $tpl->setVariable("ODS_OPTIONS", $addr->getOptList());
            $calc_uid = $DB->getField("SELECT `tech_id` FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($ticket->getId()) . ";");
            $uid = $calc_uid ? $calc_uid : gfx_plugin::getCurrentTechIdAdGfx($ticket->getId());
            if ($uid) {
                $tpl->setVariable("CALC_TECH_LIST", adm_empl_plugin::getTechList($uid));
            }
            $tpl->setVariable("CALC_DATE", date("d.m.Y"));

            $tpl->setVariable("TMC_CAT_LIST_INFORM", adm_material_cats_plugin::getOptions());
        }

        $report_status = adm_statuses_plugin::getStatusByTag("report", "connections");
        if ($report_status) {
            $tpl->setVariable("REPORT_STATUS_ID", $report_status["id"]);
            $tpl->setVariable("REPORT_STATUS_COLOR", $report_status["color"]);

        }

        $closed_status = adm_statuses_plugin::getStatusByTag("closed", "connections");
        if ($closed_status) {
            $tpl->setVariable("CLOSED_STATUS_ID", $closed_status["id"]);
            $tpl->setVariable("CLOSED_STATUS_COLOR", $closed_status["color"]);
        }
        $complete_status = adm_statuses_plugin::getStatusByTag("done", "connections");
        $check_status = adm_statuses_plugin::getStatusByTag("check", "connections");
        $currentStatus = $ticket->getStatusId();
        if (!$USER->checkAccess("rooteditticket", User::ACCESS_WRITE)) {

            if (($currentStatus == $closed_status["id"] || $currentStatus == $report_status["id"] || $currentStatus == $complete_status["id"] || $currentStatus == $check_status["id"])) {
                $tpl->touchBlock("removeinactive");
                $tpl->touchBlock("resetinactive");
                $tpl->setVariable("ISTECH_DISABLED", "disabled=\"disabled\"");
            }
        }
        $new_status = adm_statuses_plugin::getStatusByTag("new", "connections");
        $report_status = adm_statuses_plugin::getStatusByTag("report", "connections");
        $conflict = adm_statuses_plugin::getStatusByTag("gfxconflict", "connections");


        if (/*($currentStatus == $conflict["id"] || $currentStatus == $new_status["id"] || $currentStatus == $report_status["id"]) &&*/
        $USER->checkAccess("edit_contr_ticket", User::ACCESS_WRITE)
        ) {
            $tpl->touchBlock("changecontr");
        }

        $tpl->setVariable("OP_DATE_FROM", $ticket->getSlotDateBegin() != "00.00.0000" ? $ticket->getSlotDateBegin() : "");
        $tpl->setVariable("OP_DATE_TO", $ticket->getSlotDateEnd() != "00.00.0000" ? $ticket->getSlotDateEnd() : "");
        $tpl->setVariable("OP_TIME_FROM", $ticket->getSlotTimeBegin() != "00:00" ? $ticket->getSlotTimeBegin() : "");
        $tpl->setVariable("OP_TIME_TO", $ticket->getSlotTimeEnd() != "00:00" ? $ticket->getSlotTimeEnd() : "");
        if ($ticket->getDocID() && $ticket->getDocDate()) {
            $tpl->setVariable("DOC_TICKET_PARAMS", $ticket->getDocID() . " от " . $ticket->getDocDate());
        } else {
            $tpl->setVariable("DOC_TICKET_PARAMS", "&mdash;");
        }
        if ($ticket->getTMCDocID() && $ticket->getTMCDocDate()) {
            $tpl->setVariable("DOC_TMC_PARAMS", $ticket->getTMCDocID() . " от " . $ticket->getTMCDocDate());
        } else {
            $tpl->setVariable("DOC_TMC_PARAMS", "&mdash;");
        }


        UIHeader($tpl);
        $tpl->show();
    }

    static function getSCName($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_sc` WHERE `id`=" . $DB->F($id);

        return $DB->getField($sql, $escape);
    }

    static function getOds($dom_id)
    {
        global $DB;
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getODSId")) {
            $addr = new addr_interface_plugin();
            $ods_id = $addr->getODSId($dom_id);
        } else return false;
        $sql = "SELECT * FROM `list_ods` WHERE `id`=" . $DB->F($ods_id) . ";";

        return $DB->getRow($sql, true);
    }

    static function getTypeID($id, $escape = true)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `task_type` FROM `tickets` WHERE `task_id`=" . $DB->F($id);

        return $DB->getField($sql, $escape);
    }

    static function getContrID($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT `cnt_id` FROM `tickets` WHERE `task_id`=" . $DB->F($id);

        return $DB->getField($sql, $escape);
    }

    static function getCWType($task_id)
    {
        global $DB;
        if (!$task_id) UIError("Не указан идентификатор заявки!");
        $DB->query("SELECT lap.id, lap.s_title, lap.title, lap.price_type, lap.price, lap.p_comment, (SELECT `smeta_val` FROM `list_contr_calc` WHERE `work_id`=lap.id AND `task_id`=" . $DB->F($task_id) . ") AS smeta_val, (SELECT quant FROM `list_contr_calc` WHERE `work_id`=lap.id AND `task_id`=" . $DB->F($task_id) . ") AS quant  FROM `list_agr_price` AS lap WHERE lap.id IN (SELECT `work_id` FROM `list_contr_calc` WHERE `task_id`=" . $DB->F($task_id) . ");");
        if ($DB->errno()) UIError($DB->error());
        $ret = false;
        if ($DB->num_rows()) {
            while ($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        } else {
            $ret = false;
        }
        $DB->free();

        return $ret;
    }

    static function getTWType($task_id)
    {
        global $DB;
        if (!$task_id) UIError("Не указан идентификатор заявки!");
        $DB->query("SELECT ltp.id, ltp.short_title, ltp.title, ltp.price_type, ltp.price, ltp.p_comment,(SELECT ltc.id AS recid FROM `list_tech_calc` AS ltc WHERE ltc.work_id=ltp.id AND ltc.task_id=" . $DB->F($task_id) . " LIMIT 1) AS recid,  (SELECT ltc.smeta_val FROM `list_tech_calc` AS ltc WHERE ltc.work_id=ltp.id AND ltc.task_id=" . $DB->F($task_id) . " LIMIT 1) AS smeta_val, (SELECT ltc.quant FROM `list_tech_calc` AS ltc WHERE ltc.work_id=ltp.id AND ltc.task_id=" . $DB->F($task_id) . " LIMIT 1) AS quant FROM `list_tech_price` AS ltp WHERE ltp.id IN (SELECT `work_id` FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($task_id) . ");");
        if ($DB->errno()) UIError($DB->error());
        $ret = false;
        if ($DB->num_rows()) {
            while ($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        } else {
            $ret = false;
        }
        $DB->free();

        return $ret;
    }

    function updateContrDataA()
    {
        global $DB;
        $task_id = $_POST["task_id"];
        $contr_id = $_POST["contr_id"];
        $type_id = $_POST["type_id"];
        $agr_id = $_POST["agr_id"];
        $err = [];
        if ($task_id && $contr_id && $type_id && $agr_id) {
            $sql = "UPDATE `tickets` SET `cnt_id`=" . $DB->F($contr_id) . ", `task_type`=" . $DB->F($type_id) . ", `agr_id`=" . $DB->F($agr_id) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) $err[] = "error";
            $sql = "DELETE FROM `list_contr_calc` WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) $err[] = "error";
        } else {
            $err[] = "error";
        }
        echo sizeof($err) ? "error" : "ok";

        return false;

    }

    function checkTaskStatusA()
    {
        global $DB;
        if ($task_id = $_POST['task_id']) {
            $task = new ConnectionTicket($task_id);
            $currentStatus = $task->getStatusId();
            $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("connections") . " AND `tag`=" . $DB->F("report") . ";";
            $status = $DB->getField($sql);
            if ($status == $currentStatus) {
                $ret = "ok";
            } else {
                $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("connections") . " AND `tag` LIKE " . $DB->F("%done%") . ";";
                $status = $DB->getField($sql);
                if ($status == $currentStatus) {
                    $ret = "change";
                } else {
                    $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("connections") . " AND `tag`=" . $DB->F("closed") . ";";
                    $status = $DB->getField($sql);
                    if ($status == $currentStatus) {
                        $ret = "ok";
                    } else {
                        $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("connections") . " AND `tag`=" . $DB->F("clctmc") . ";";
                        $status = $DB->getField($sql);
                        if ($status == $currentStatus) {
                            $ret = "ok";
                        } else {
                            $ret = "nostatus";
                        }

                    }

                }
            }
        } else {
            $ret = "error";
        }
        echo $ret;

        return false;
    }

    function getCurrentTMCCalcA()
    {
        global $DB, $USER;

        if ($task_id = $_POST["task_id"]) {

            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $task = new ConnectionTicket($task_id);
            $currentStatus = $task->getStatusId();
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
            $countStatus = adm_statuses_plugin::getStatusByTag("report", "connections");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "connections");
            $targetStatus = false;
            if ($currentStatus == $doneStatus["id"] || $currentStatus == $countStatus["id"] || $currentStatus == $completeStatus["id"]) $targetStatus = true;
            $sc_id = $task->getSCId();
            if (!$sc_id) {
                echo "<p class=\"redmessage\">Пожалуйста, укажите <strong>Сервисный Центр</strong>!<br /><small>При этом - СЦ должен совпадать с СЦ техника, который выполнял заявку.</small></p>";

                return false;
            }
            $sql = "SELECT DATE_FORMAT(`datetime`, '%Y-%m-%d') AS doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($task_id) . " AND `status_id`=" . $DB->F($doneStatus["id"]) . " ORDER BY donedate DESC;";
            $doneDate = $DB->getField($sql);
            if (date("m") == "01") {
                $df = date("Y-m-d", strtotime(date("Y-m") . "+" . getcfg('connfinreport_dc_january') . "days"));
            } else $df = date("Y-m-d", strtotime(date("Y-m") . "+" . getcfg('connfinreport_dc') . "days"));
            $cd = date("Y-m-d");
            $readonly = false;
            if (!$USER->checkAccess("calcticketafterdeadline", User::ACCESS_WRITE) && $targetStatus) {
                if ($cd > $df) {
                    if (date("Y-m", strtotime($doneDate)) <= date("Y-m", strtotime(date("Y-m") . " - 1 month"))) $readonly = true; else $readonly = false;
                } else {
                    if (date("Y-m", strtotime($doneDate)) <= date("Y-m", strtotime(date("Y-m") . " - 2 month"))) $readonly = true; else $readonly = false;
                }
            }
            //$readonly = true;
            if ($readonly)
                $tpl->loadTemplatefile($USER->getTemplate() . "/calctmcreadonly.tmpl.htm");
            else
                $tpl->loadTemplatefile($USER->getTemplate() . "/calctmc.tmpl.htm");


            $calc_uid = $DB->getField("SELECT `tech_id` FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($task_id) . ";");
            $uid = $calc_uid ? $calc_uid : gfx_plugin::getCurrentTechIdAdGfx($task_id);

            if ($uid) {
                $tpl->setVariable("CALC_TECH_LIST", $uid);
                $u = adm_users_plugin::getUser($uid);
                $tpl->setVariable('CALC_TECH_NAME', $u["fio"]);
            } else {
                echo "<p class=\"redmessage\">Не указан техник, выполнявший заявку! Для списания ТМЦ заявка должна была стоять в графике у техника-исполнителя.</p>";

                return false;
            }
            if ($readonly) {
                $tpl->setVariable("AFTERDEADLINE", "disabled='disabled'");
            }


            // TMC
            $sql = "SELECT
                tmc_tech.id,
                tmc_tech.count,
                tmc_sc.serial,
                tmc_sc.tmc_id,
                tmc_list.title,
                tickdata.isrent,
                tmc_list.metric_flag
            FROM
                `tmc_ticket` AS tickdata
                LEFT JOIN
                `tmc_sc_tech` AS tmc_tech ON tmc_tech.id=tickdata.tmc_tech_sc_id
                LEFT JOIN
                `tmc_sc` AS tmc_sc ON tmc_sc.id=tmc_tech.tmc_sc_id
                LEFT JOIN
                `list_materials` AS tmc_list ON tmc_list.id=tmc_sc.tmc_id
            WHERE
                tickdata.task_id=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) {
                echo "error " . $sql . " " . $DB->error();
                trigger_error($DB->error());

                return false;
            }
            if ($DB->num_rows()) {
                while ($res = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("tmc_row");


                    if ($readonly) $tpl->setVariable("AFTERDEADLINE_HIDDENTMC", 'hidden');

                    if ($res["metric_flag"]) {
                        $tpl->setVariable("NO_SERIAL", "&mdash;");
                        $tpl->setVariable("NO_ISRENT", "&mdash;");
                    } else {
                        $tpl->setCurrentBlock("hasserial");
                        $tpl->setVariable("TMC_S_ID", $res["id"]);
                        $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                        $tpl->parse("hasserial");
                        $tpl->setCurrentBlock("isrent");
                        $tpl->setVariable("IS_RENT_CHECKED", $res["isrent"] == '1' ? "checked=\"checked\"" : "");
                        $tpl->setVariable("IS_RENT", $res["isrent"]);
                        $tpl->setVariable("TMC_IR_ID", $res["id"]);
                        $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                        $tpl->parse("isrent");
                        $tpl->setVariable("TMC_COUNT", "1");
                    }

                    $tpl->setVariable("TMC_ID", $res["tmc_id"]);
                    $tpl->setVariable("TMC_TITLE", $res["title"]);
                    $tpl->setVariable("TMC_COUNT", $res["count"]);
                    $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                    $tpl->setVariable("TMC_TECH_ID", $uid);
                    $tpl->setVariable("TMC_ROW_ID", $res["id"]);

                    $tpl->parse("tmc_row");
                }
                $DB->free();
                $tpl->setVariable("NO_TMC_ROWS", "display: none;");

            } else {
                $tpl->setVariable("NO_TMC_ROWS", "");
            }

            $tpl->setVariable("TMC_SC_ID", $task->getSCId());
            $addr_params = $task->getAddrRecord();
            $currentODS = $addr_params && $addr_params['ods_id'] ? adm_ods_plugin::getODSById($addr_params['ods_id']) : false;
            if ($currentODS) {
                $tpl->setVariable("ODS_ID", $currentODS["id"]);
                $tpl->setVariable("ODS_NAME", $currentODS["title"]);
                $tpl->setVariable("ODS_ADDR", $currentODS["address"]);
                $tpl->setVariable("ODS_TEL", $currentODS["tel"]);
                $tpl->setVariable("ODS_CONTACTS", $currentODS["fio"] ? $currentODS["fio"] : "&mdash;");
                $tpl->setVariable("ODS_COMMENT", $currentODS["comment"] ? $currentODS["comment"] : "&mdash;");
            } else {
                $tpl->setVariable("ODS_NAME", "не указана");
                $tpl->setVariable("ODS_ADDR", "&mdash;");
                $tpl->setVariable("ODS_TEL", "&mdash;");
                $tpl->setVariable("ODS_CONTACTS", "&mdash;");
                $tpl->setVariable("ODS_COMMENT", "&mdash;");
            }

            // get photolist
            $contr = $task->getContrId();
            $wtype = $DB->getField("SELECT `task_type` FROM `tickets` WHERE `task_id`=" . $DB->F($task->getId()) . ";");
            if ($contr && $wtype) {
                $DB->query("SELECT * FROM `list_photo_types` WHERE `active` AND `wtype`=" . $DB->F($wtype) . " AND `task_type`='connections' AND `contr_id`=" . $DB->F($contr) . ";");
                if ($DB->num_rows()) {
                    $tpl->setCurrentBlock("photoreport");
                    while ($m = $DB->fetch(true)) {
                        $tpl->setCurrentBlock("filelist");
                        $tpl->setVariable("FILENAME", $m["title"] . ($m["required"] ? " <span class='red' style='font-size: 14pt'>*</span>" : ""));
                        $tpl->setVariable("FILE_ID", $m["id"]);
                        $tpl->setVariable("FILEREQUIRED", $m["required"] ? " required='required' " : "");
                        $tpl->setVariable("FILE_TASK_ID", $task->getId());
                        $filename = $DB->getRow("SELECT * FROM `task_photos` WHERE `task_id`=" . $DB->F($task->getId()) . " AND `filetype_id`=" . $DB->F($m["id"]) . ";", true);
                        if ($filename) {
                            $tpl->setVariable("FILENAME_VAL", "<a href='/comments/getfile?id=" . $filename["file_id"] . "&name=" . $filename["filename"] . "'>Скачать файл</a>");
                            $tpl->setVariable("FILEREQUIRED", "style='display:none;'");

                        } else {
                            $tpl->setVariable("FILENOTLOADED", "style='display: none;'");
                        }
                        $tpl->parse("filelist");
                    }
                    $tpl->parse("photoreport");
                }
            }
            // --------------
            $tpl->show();

        } else {
            echo "error";
        }

        return;
    }

    function deletephotofile(Request $request)
    {
        global $DB, $USER;
        $task_id = $request->get('task_id');
        $filetype_id = $request->get('filetype_id');
        if ($task_id && $filetype_id) {
            $t = new ConnectionTicket($task_id);
            $sql = "DELETE FROM `task_photos` WHERE `filetype_id`=" . $DB->F($filetype_id) . " AND `task_id`=" . $DB->F($task_id) . ";";
            $dif = $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
            } else {
                $ret["ok"] = "ok";
                $typeName = $DB->getField("SELECT `title` FROM `list_photo_types` WHERE `id`=" . $DB->F($filetype_id) . ";");
                $t->addComment("Файл удален из списка фотоотчета: " . $typeName . ". Оригинал сохранен в файлах заявки.");
                $ret["cmm"] = $t->getLastComment();
            }
            $DB->free($dif);
        } else {
            $ret["error"] = "Недостаточно данных для выполнения операции";
        }
        echo json_encode($ret);

        return false;
    }

    function addphoto(Request $request)
    {
        global $DB, $USER;

        $task = $request->get("task_id");
        $filetype = $request->get("file_type_id");
        if ($task && $filetype) {
            $t = new ConnectionTicket($task);
            if ($t) {
                $typeName = $DB->getField("SELECT `title` FROM `list_photo_types` WHERE `id`=" . $DB->F($filetype) . ";");
                $file_id = $t->addFile($_FILES["pict"]["tmp_name"], $typeName . "_" . $_FILES["pict"]["name"], $_FILES["pict"]["type"]);
                if ($file_id) {
                    $sql = "DELETE FROM `task_photos` WHERE `task_id`=" . $DB->F($task) . " AND `filetype_id`=" . $DB->F($filetype) . ";";
                    $dif = $DB->query($sql);
                    $DB->free($dif);
                    $sql = "INSERT INTO `task_photos` (`task_id`, `filetype_id`, `file_id`, `filename`)
                            VALUES (" . $DB->F($task) . ", " . $DB->F($filetype) . ", " . $DB->F($file_id) . ", " . $DB->F($typeName . "_" . $_FILES["pict"]["name"]) . ");";
                    $dif = $DB->query($sql);
                    $DB->free($dif);
                    $t->addComment("Фотоотчет по заявке", "Загрузка файла -> <a href='/comments/getfile?id=" . $file_id . "&name=" . $typeName . "_" . $_FILES["pict"]["name"] . "'>" . $typeName . "_" . $_FILES["pict"]["name"] . "</a>");
                    $ret["ok"] = "ok";
                    $ret["cmm"] = $t->getLastComment();
                    $ret["filelink"] = "<a href='/comments/getfile?id=" . $file_id . "&name=" . $typeName . "_" . $_FILES["pict"]["name"] . "'>Скачать ({$_FILES["pict"]["size"]} байт)</a>";
                } else {
                    $ret["error"] = "Ошибка присоединения файла к заявке";
                }
            } else {
                $ret["error"] = "Ошибка обработки заявки";
            }
        } else {
            $code = $_FILES["pict"]["error"] ? $_FILES["pict"]["error"] : 0;
            $ret["error"] = "Ошибка загрузки файла. " . $code;
        }
        echo json_encode($ret);

        return false;
    }

    function getCurrentCalcA()
    {
        global $DB, $USER;


        if ($task_id = $_POST["task_id"]) {

            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $task = new ConnectionTicket($task_id);
            $currentStatus = $task->getStatusId();
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
            $countStatus = adm_statuses_plugin::getStatusByTag("report", "connections");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "connections");
            $targetStatus = false;
            if ($currentStatus == $doneStatus["id"] || $currentStatus == $countStatus["id"] || $currentStatus == $completeStatus["id"]) $targetStatus = true;
            $sc_id = $task->getSCId();
            if (!$sc_id) {
                echo "<p class=\"redmessage\">Пожалуйста, укажите <strong>Сервисный Центр</strong>!<br /><small>При этом - СЦ должен совпадать с СЦ техника, который выполнял заявку.</small></p>";

                return false;
            }
            $sql = "SELECT DATE_FORMAT(`datetime`, '%Y-%m-%d') AS doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($task_id) . " AND `status_id`=" . $DB->F($doneStatus["id"]) . " ORDER BY donedate DESC;";
            $doneDate = $DB->getField($sql);
            if (date("m") == "01") {
                $df = date("Y-m-d", strtotime(date("Y-m") . "+" . getcfg('connfinreport_dc_january') . "days"));
            } else $df = date("Y-m-d", strtotime(date("Y-m") . "+" . getcfg('connfinreport_dc') . "days"));
            $cd = date("Y-m-d");
            $readonly = false;
            if (!$USER->checkAccess("calcticketafterdeadline", User::ACCESS_WRITE) && $targetStatus) {
                if ($cd > $df) {
                    if (date("Y-m", strtotime($doneDate)) <= date("Y-m", strtotime(date("Y-m") . " - 1 month"))) $readonly = true; else $readonly = false;
                } else {
                    if (date("Y-m", strtotime($doneDate)) <= date("Y-m", strtotime(date("Y-m") . " - 2 month"))) $readonly = true; else $readonly = false;
                }
            }
            if ($readonly)
                $tpl->loadTemplatefile($USER->getTemplate() . "/calcreadonly.tmpl.htm");
            else
                $tpl->loadTemplatefile($USER->getTemplate() . "/calc.tmpl.htm");

            $addconvert = false;
            $addtimeframe = false;
            if (!$this->isCurrentCalc($task_id) && !$readonly) {
                // calc convert
                $conv_rec = $DB->getRow("SELECT * FROM `list_tech_price` WHERE `convert_bonus`;", true);


                $gfx = $DB->getRow("SELECT * FROM `gfx` WHERE `task_id`=" . $DB->F($task->getId()) . ";", true);
                $sql = "SELECT `sc_id` FROM `link_sc_user` WHERE `user_id`=" . $DB->F($gfx["empl_id"]) . ";";
                $currentUserSC = $DB->getField($sql);
                if ($currentUserSC && $currentUserSC != $sc_id) {
                    echo "<p class=\"redmessage\"><strong>СЦ</strong> исполнителя не соответствует <strong>СЦ</strong> заявки. Для продолжения, - необходимо устранить это несоответствие.</p>";

                    return false;
                }
                //$exeptstatus = array("")
                $totalperday = $DB->getCell("SELECT `task_id` FROM `gfx` WHERE `c_date`=" . $DB->F($gfx["c_date"]) . " AND `empl_id`=" . $DB->F($gfx["empl_id"]) . ";", true);
                if ($totalperday) {
                    //$totalperdayCount = count($totalperday);
                    $exeptstatus = ["65", "60", "5", "13", "10", "17"];
                    $statusList = ["61", "54", "57", "52", "1", "26", "20", "23", "12", "25", "49", "21", "24"];
                    $totalperdayCount = $DB->getField("SELECT count(`id`) FROM `tasks` WHERE `id` IN (" . implode(",", $totalperday) . ") AND  `status_id` NOT IN (" . implode(",", $exeptstatus) . ");");
                    $sql = "SELECT count(`id`) FROM `tasks` WHERE `id` IN (" . implode(",", $totalperday) . ") AND `status_id` IN (" . implode(",", $statusList) . ");";
                    $totatCompletedCount = $DB->getField($sql);
                    $convert = round($totatCompletedCount / $totalperdayCount, 2);
                    if ($convert >= 0.81) {
                        $addconvert = true;
                    }
                }
                // calc timeframe
                $calc_rec = $DB->getRow("SELECT * FROM `list_tech_price` WHERE `timeslot_bonus`;", true);
                $ds = adm_statuses_plugin::getStatusesByTagStatic("done", "connections");
                if (!$ds) $addtimeframe = false;
                else {
                    $ds = $ds[0];
                    $r = $DB->query("SELECT `datetime` FROM `task_comments` WHERE `task_id`=" . $DB->F($task->getId()) . " AND `status_id`=" . $DB->F($ds["id"]) . " ORDER BY `id` ASC;");
                    if ($DB->num_rows()) {
                        $rst = $DB->fetch(true);
                        if ($task->getSlotDateBegin() && $task->getSlotDateBegin() != "0000-00-00" && $task->getSlotTimeBegin() && $task->getSlotTimeBegin() != "00:00" && $task->getSlotDateEnd() && $task->getSlotDateEnd() != "0000-00-00" && $task->getSlotTimeEnd() && $task->getSlotTimeEnd() != "00:00") {
                            $slotBegin = $task->getSlotDateBegin() . " " . $task->getSlotTimeBegin();
                            $slotEnd = $task->getSlotDateEnd() . " " . $task->getSlotTimeEnd();
                            $cDate = strtotime($rst["datetime"]);
                            $slotBegin = strtotime($slotBegin);
                            $slotEnd = strtotime($slotEnd);
                            if ($cDate >= $slotBegin && $cDate <= $slotEnd) {
                                $addtimeframe = true;

                            }

                        }
                    } else {
                        $addtimeframe = false;
                    }
                    $DB->free($r);
                }


                //convertion
                if ($conv_rec) {
                    $convertion = "<tr class='tp" . $conv_rec["id"] . "'><td><input type='hidden' name='tp[]' value='" . $conv_rec["id"] . "' /><strong>"
                        . $conv_rec['short_title'] . "</strong><br /><small>" . $conv_rec["p_comment"] . "</small></td><td><input class='onlynum' type='text' value='1' style='text-align: center; width: 30px' name='tq" . $conv_rec["id"] . "' /></td>
                     <td align='center'>" . $conv_rec["price"] . "</td><td><center>&mdash;</center></td><td><a class='tpremove remove_" . $conv_rec["id"] . " ' style='cursor:pointer;'>
                     <img border='0' src='/templates/images/cross.png' /></a></td></tr>";
                }
                //timeframe
                if ($calc_rec) {
                    $timeframe = "<tr class='tp" . $calc_rec["id"] . "'><td><input type='hidden' name='tp[]' value='" . $calc_rec["id"] . "' /><strong>"
                        . $calc_rec['short_title'] . "</strong><br /><small>" . $calc_rec["p_comment"] . "</small></td><td><input class='onlynum' type='text' value='1' style='text-align: center; width: 30px' name='tq" . $calc_rec["id"] . "' /></td>
                     <td align='center'>" . $calc_rec["price"] . "</td><td><center>&mdash;</center></td><td><a class='tpremove remove_" . $calc_rec["id"] . " ' style='cursor:pointer;'>
                     <img border='0' src='/templates/images/cross.png' /></a></td></tr>";
                } else {
                    $addtimeframe = false;
                }
            }

            $calc_uid = $DB->getField("SELECT `tech_id` FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($task_id) . ";");
            $uid = $calc_uid ? $calc_uid : gfx_plugin::getCurrentTechIdAdGfx($task_id);

            if ($uid) {
                $tpl->setVariable("CALC_TECH_LIST", $uid);
                $u = adm_users_plugin::getUser($uid);
                $tpl->setVariable('CALC_TECH_NAME', $u["fio"]);
            }
            if ($readonly) {
                $tpl->setVariable("AFTERDEADLINE", "disabled='disabled'");
            }
            $cwtList = connections_plugin::getCWType($task_id);
            if ($cwtList) {
                foreach ($cwtList as $item) {
                    $sql = "SELECT * FROM `list_contr_calc` WHERE `task_id`=" . $DB->F($task_id) . " AND `work_id`=" . $DB->F($item["id"]) . " AND `price`!=" . $DB->F($item["price"]) . ";";
                    $currentPIVal = $DB->getRow($sql, true);
                    $sql = "SELECT * FROM `list_contr_calc` WHERE `task_id`=" . $DB->F($task_id) . " AND `work_id`=" . $DB->F($item["id"]) . ";";
                    $cPIVal = $DB->getRow($sql, true);
                    if ($currentPIVal) {
                        $tpl->touchBlock("cpriceupdated");
                    }
                    $tpl->setCurrentBlock("cw_row");
                    if ($readonly) $tpl->setVariable("AFTERDEADLINE_HIDDENCONTR", 'hidden');
                    $tpl->setVariable("CW_TYPE_TITLE", $currentPIVal ? "<strong style=\"color:#FF0000;\">" . $item["s_title"] . "</strong><br /><small style=\"color:#FF0000;\">" . $item['p_comment'] . "</small>" : "<strong>" . $item["s_title"] . "</strong><br /><small>" . $item['p_comment'] . "</small>");
                    $tpl->setVariable("CW_SMETA", $item["price_type"] == "smeta" ? $item["smeta_val"] : "&mdash;");
                    $tpl->setVariable("CW_ID", $item["id"]);
                    $tpl->setVariable("CWR_ID", $cPIVal["id"]);
                    $tpl->setVariable("CW_QUANT", $cPIVal["quant"]);
                    $tpl->setVariable("CW_ID_VAL_CVAL", $currentPIVal ? $cPIVal["price"] : "");//val
                    $tpl->setVariable("CW_TYPE_T_VAL", $currentPIVal ? "dsval" . $item["id"] : "");//name


                    $tpl->setVariable("CW_ID_VAL", $item["price_type"] == "smeta" ? $item["smeta_val"] : $item["id"]);
                    $tpl->setVariable("CW_TYPE_T", $item["price_type"] == "smeta" ? "cs" . $item["id"] : "cp[]");
                    $tpl->setVariable("CW_TYPE", $item["price_type"] == "smeta" ? "cs" : "cp");
                    $tpl->setVariable("CW_PRICE", $item["price_type"] == "smeta" ? "&mdash;" : $item["price"]);
                    $tpl->parse("cw_row");
                }
            } else {
                $tpl->touchBlock("cw_norows");
            }
            $cDateC = $DB->getField("SELECT `calc_date` FROM `list_contr_calc` WHERE `task_id`=" . $DB->F($task_id) . ";");
            $cDateW = $DB->getField("SELECT `calc_date` FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($task_id) . ";");
            $tpl->setVariable("TMC_SC_ID", $task->getSCId());
            $cDate = $cDateW ? $cDateW : ($cDateС ? $cDateC : false);
            if ($cDate) {
                if ($cDate == "0000-00-00")
                    $tpl->setVariable("CALC_DATE", date('d.m.Y'));
                else
                    $tpl->setVariable("CALC_DATE_DISABLED", "disabled='disabled'");
                $tpl->setVariable("CALC_DATE", substr($cDate, 8, 2) . "." . substr($cDate, 5, 2) . "." . substr($cDate, 0, 4));
            } else {
                if ($doneStatus) {
                    $sql = "SELECT DATE_FORMAT(`datetime`, " . $DB->F("%d.%m.%Y") . ") as doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($task_id) . " AND `status_id`=" . $DB->F($doneStatus["id"]) . " ORDER BY donedate DESC;";
                    $doneDate = $DB->getField($sql);
                    if ($doneDate) {
                        $tpl->setVariable("CALC_DATE", $doneDate);
                    } else {
                        $tpl->setVariable("CALC_DATE", date('d.m.Y'));
                    }
                } else {
                    $tpl->setVariable("CALC_DATE", date('d.m.Y'));

                }
            }
            $twtList = connections_plugin::getTWType($task_id);
            if ($twtList) {
                foreach ($twtList as $item) {
                    $sql = "SELECT * FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($task_id) . " AND `work_id`=" . $DB->F($item["id"]) . " AND `price`!=" . $DB->F($item["price"]) . ";";
                    $currentPIVal = $DB->getRow($sql, true);
                    $sql = "SELECT * FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($task_id) . " AND `work_id`=" . $DB->F($item["id"]) . ";";
                    $cPIVal = $DB->getRow($sql, true);
                    if ($currentPIVal) {
                        $tpl->touchBlock("priceupdated");
                    }
                    $tpl->setCurrentBlock("tw_row");
                    if ($currentPIVal) {
                        $tpl->setVariable("TW_OLDPRICETITLE", "title=\"ВНИМАНИЕ! Прайс был обновлен. Расчет выполняется по старой цене, указанной в прайсе на дату расчета. Новая цена выделена красным цветом.\"");
                    }
                    if ($readonly) $tpl->setVariable("AFTERDEADLINE_HIDDENZP", 'hidden');
                    $tpl->setVariable("TW_TYPE_TITLE", ($currentPIVal || $cPIVal["price"] < 0) ? "<strong style=\"color:#FF0000;\">" . $item["short_title"] . "</strong><br /><small style=\"color:#FF0000;\">" . $item['p_comment'] . "</small>" : "<strong>" . $item["short_title"] . "</strong><br /><small>" . $item['p_comment'] . "</small>");
                    $tpl->setVariable("TW_SMETA", $item["price_type"] == "smeta" ? ($currentPIVal ? "<strong style=\"color:#FF0000;\">" . $item["price"] . "%</strong><br /><small><strike>" . $cPIVal["smeta_val"] . "(" . $cPIVal["price"] . "%)</strike></small>" : $cPIVal["smeta_val"] . "<small> (" . $cPIVal["price"] . "%)</small>") : "&mdash;");
                    $tpl->setVariable("TW_ID", $item["id"]);
                    $tpl->setVariable("TWR_ID", $item["recid"]);
                    $tpl->setVariable("TW_ID_VAL_CVAL", ($currentPIVal) ? $cPIVal["price"] : "");//val
                    $tpl->setVariable("TW_TYPE_T_VAL", ($currentPIVal && $item["price_type"] == "price") ? "tp_val" . $item["id"] : (($currentPIVal && $item["price_type"] == "smeta") ? "ts_val_id" . $item["id"] : ""));//name
                    $tpl->setVariable("TW_QUANT", $cPIVal["quant"]);

                    $tpl->setVariable("TW_ID_VAL_CVAL2", ($currentPIVal && $item["price_type"] == "smeta") ? $cPIVal["smeta_val"] : "");
                    $tpl->setVariable("TW_TYPE_T_VAL2", ($currentPIVal && $item["price_type"] == "smeta") ? "ts_val_val" . $item["id"] : "");

                    $tpl->setVariable("TW_ID_VAL", $currentPIVal ? ($item["price_type"] == "smeta" ? $cPIVal["price"] : $item["id"]) : ($item["price_type"] == "smeta" ? $item["smeta_val"] : $item["id"]));
                    $tpl->setVariable("TW_TYPE_T", $item["price_type"] == "smeta" ? "ts" . $item["id"] : "tp[]");
                    $tpl->setVariable("TW_TYPE", $item["price_type"] == "smeta" ? "ts" : "tp");

                    $tpl->setVariable("TW_PRICE", $item["price_type"] == "smeta" ? ($currentPIVal ? "<strong style=\"color:#FF0000;\">" . round(($item["price"] * $item["smeta_val"]) / 100, 2) . "</strong><br /><strike>" . round(($cPIVal["price"] * $cPIVal["smeta_val"]) / 100, 2) . "</strike>" : round(($cPIVal["price"] * $cPIVal["smeta_val"]) / 100, 2)) : ($currentPIVal ? "<strong style=\"color:#FF0000;\">" . $item["price"] . "</strong><br /><strike>" . $cPIVal["price"] . "</strike>" : $item["price"]));

                    $tpl->parse("tw_row");
                }
            } else {
                $addtpl = false;
                if ($addconvert) {
                    $addmsg[] = "конверсию";
                    $addtpl = $convertion;
                }
                if ($addtimeframe) {
                    $addmsg[] = "выполнение заявки в заявленное оператором время";
                    $addtpl .= $timeframe;
                }
                //print_r($addtpl);
                //die();
                if ($addtpl) {
                    $addmsg = implode(" и ", $addmsg);
                    $addtpl .= "<script type='text/javascript'>alert('Внимание! Автоматически начислена премия за $addmsg!');</script>";
                    $tpl->setVariable("BONUSES", $addtpl);
                } else
                    $tpl->touchBlock("tw_norows");
            }
            $tpl->setVariable("SW_CABLEQUANT", $task->getCableQuant());

            $currentODS = $this->getOds($task->getDomId());
            if ($currentODS) {
                $tpl->setVariable("ODS_ID", $currentODS["id"]);
                $tpl->setVariable("ODS_NAME", $currentODS["title"]);
                $tpl->setVariable("ODS_ADDR", $currentODS["address"]);
                $tpl->setVariable("ODS_TEL", $currentODS["tel"]);
                $tpl->setVariable("ODS_CONTACTS", $currentODS["fio"] ? $currentODS["fio"] : "&mdash;");
                $tpl->setVariable("ODS_COMMENT", $currentODS["comment"] ? $currentODS["comment"] : "&mdash;");
            } else {
                $tpl->setVariable("ODS_NAME", "не указана");
                $tpl->setVariable("ODS_ADDR", "&mdash;");
                $tpl->setVariable("ODS_TEL", "&mdash;");
                $tpl->setVariable("ODS_CONTACTS", "&mdash;");
                $tpl->setVariable("ODS_COMMENT", "&mdash;");
            }

            // TMC
            $sql = "SELECT
                tmc_sc_tech.id,
                tmc_ticket.count,
                tmc_sc.serial,
                tmc_sc.tmc_id,
                list_materials.title,
                tmc_ticket.isrent,
                list_materials.metric_flag,
                tmc_ticket.tech_id
            FROM
                `tmc_ticket`
                LEFT JOIN
                `tmc_sc_tech` ON tmc_sc_tech.id=tmc_ticket.tmc_tech_sc_id
                LEFT JOIN
                `tmc_sc` ON tmc_sc.id=tmc_sc_tech.tmc_sc_id
                LEFT JOIN
                `list_materials` ON list_materials.id=tmc_sc.tmc_id
            WHERE
                tmc_ticket.task_id=" . $DB->F($task_id) . ";";
            $DB->query($sql);
//            d($sql);die;
            if ($DB->errno()) {
                echo "error " . $sql . " " . $DB->error();
                trigger_error($DB->error());

                return false;
            }
            if ($DB->num_rows()) {
                while ($res = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("tmc_row");

                    if ($readonly)
                        $tpl->setVariable("AFTERDEADLINE_HIDDENTMC", 'hidden');

                    if ($res["metric_flag"]) {
                        $tpl->setVariable("NO_SERIAL", "&mdash;");
                        $tpl->setVariable("NO_ISRENT", "&mdash;");
                    } else {
                        $tpl->setCurrentBlock("hasserial");
                        $tpl->setVariable("TMC_S_ID", $res["id"]);
                        $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                        $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                        $tpl->parse("hasserial");
                        $tpl->setCurrentBlock("isrent");
                        $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                        $tpl->setVariable("IS_RENT_CHECKED", $res["isrent"] == '1' ? "checked=\"checked\"" : "");
                        $tpl->setVariable("TMC_IR_ID", $res["id"]);
                        $tpl->parse("isrent");

                        $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                        $tpl->setVariable("TMC_COUNT", "1");
                        $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                    }

                    $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                    $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                    $tpl->setVariable("TMC_ID", $res["tmc_id"]);
                    $tpl->setVariable("TMC_TITLE", $res["title"]);
                    $tpl->setVariable("TMC_COUNT", $res["count"]);
                    $tpl->setVariable("TMC_TECH_ID", $res["tech_id"]);

                    $tpl->parse("tmc_row");
                }
                $DB->free();
                $tpl->setVariable("NO_TMC_ROWS", "display: none;");

            } else {
                $tpl->setVariable("NO_TMC_ROWS", "");
            }

            $tpl->setVariable("TMC_SC_ID", $task->getSCId());

            $tpl->show();

        } else {
            echo "error";
        }

        return;
    }

    function isCurrentCalc($task_id)
    {
        global $DB;
        if (!$task_id) {
            return true;
        } else {
            $tc = $DB->getField("SELECT count(`id`) FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($task_id) . ";");
            $tsc = $DB->getField("SELECT count(`id`) FROM `tmc_ticket` WHERE `task_id`=" . $DB->F($task_id) . ";");
            if (!$tc && !$cc && !$tsc) {
                return false;
            } else {
                return true;
            }
        }
    }

    function queryStatus()
    {
        global $DB;
        if ($task_id = $_POST['task_id']) {
            $sql = "SELECT s.tag FROM `tasks` AS t LEFT JOIN `task_status` AS s ON t.status_id=s.id WHERE t.id=" . $DB->F($task_id) . ";";
            $status_tag = $DB->getField($sql);
            $ret = $DB->errno() ? "error" : ($status_tag ? $status_tag : "none");
        } else {
            $ret = "error";
        }
        echo $ret;

        return false;
    }

    function calcticket(Request $request)
    {
        global $DB, $USER;
        //print_r($_POST);
        //die();
        $task_id = $_POST["calc_tsk_id"];
        $tech_id = $_POST["tech_id"];
        $cp_items = $_POST["cp"] ? $_POST["cp"] : false;
        $tp_items = $_POST["tp"] ? $_POST["tp"] : false;
        $calc_date = $_POST["calc_date"] ? substr($_POST["calc_date"], 6, 4) . "-" . substr($_POST["calc_date"], 3, 2) . "-" . substr($_POST["calc_date"], 0, 2) : date("Y-m-d");
        $cs_items = false;
        $ts_items = false;
        if (!$task_id || !$tech_id) {
            echo "notfilled";

            return false;
        }
        $nodelete = implode(",", $_POST["rowo_ids"]);
        if (count($nodelete))
            $filter = " AND `id` NOT IN (" . $nodelete . ");";
        else $filter = ";";
        $sql = "DELETE FROM `list_contr_calc` WHERE `task_id`=" . $DB->F($task_id) . $filter . ";";
        $DB->query($sql);
        if ($DB->error()) {
            echo "error11" . $DB->error() . $sql;

            return false;
        }
        $nodelete = implode(",", $_POST["row_ids"]);
        if (count($nodelete))
            $filter = " AND `id` NOT IN (" . $nodelete . ");";
        else $filter = ";";
        $sql = "DELETE FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($task_id) . $filter;

        if ($DB->error()) {
            echo "error" . $DB->error() . $sql;

            return false;
        }
        $DB->query($sql);
        foreach ($_POST as $key => $item) {
            if (preg_match("/^cs/", $key)) {
                $cs_items[] = preg_replace("/cs/", "", $key);
            }
            if (preg_match("/^ts/", $key)) {
                $ts_items[] = preg_replace("/ts/", "", $key);
            }

        }
        $sql_add = false;
        if (is_array($cp_items) && count($cp_items) > 0) {
            foreach ($cp_items as $item) {
                $wt_params = kontr_plugin::getPItemsById($item);
                if (!$_POST["cq" . $item] || $_POST["cq" . $item] == "") $_POST["cq" . $item] = "1";
                $sql_add[] = "(" . $DB->F($task_id) . ", " . $DB->F($item) . ", " . $DB->F('0') . ", " . (isset($_POST["dsval" . $item]) ? $DB->F($_POST["dsval" . $item]) : $DB->F($wt_params["price"])) . ", " . $DB->F($wt_params["title"]) . ", " . $DB->F($calc_date) . ", " . $DB->F($USER->getId()) . ", " . $DB->F($_POST["cq" . $item]) . ", now())";
            }
        }
        if (is_array($cs_items) && count($cs_items) > 0) {
            foreach ($cs_items as $item) {
                $wt_params = kontr_plugin::getPItemsById($item);
                $sql_add[] = "(" . $DB->F($task_id) . ", " . $DB->F($item) . ", " . $DB->F($_POST["cs" . $item]) . ", " . $DB->F($wt_params["price"]) . ", " . $DB->F($wt_params["title"]) . ", " . $DB->F($calc_date) . ", " . $DB->F($USER->getId()) . ", 1, now())";
            }
        }
        if ($sql_add) {
            $sql = "INSERT IGNORE INTO `list_contr_calc` (`task_id`, `work_id`, `smeta_val`, `price`, `work_title`, `calc_date`, `user_id`, `quant`, `update_date`)
                    VALUES " . implode(",", $sql_add) . ";";
            $DB->query($sql);
            if ($DB->errno()) {
                echo "error" . $DB->error() . $sql;

                return false;
            }
        }
        $sql_add1 = false;
        if (is_array($tp_items) && count($tp_items) > 0) {
            foreach ($tp_items as $item) {
                $wt_params = adm_techprice_plugin::getPItemsById($item);
                if (!$_POST["tq" . $item] || $_POST["tq" . $item] == "") $_POST["tq" . $item] = "1";
                $sql_add1[] = "(" . $DB->F($task_id) . ", " . $DB->F($item) . ", " . $DB->F($tech_id) . ", " . $DB->F('0') . ", " . (isset($_POST["tp_val" . $item]) ? $DB->F($_POST["tp_val" . $item]) : $DB->F($wt_params["price"])) . ", " . $DB->F($wt_params["title"]) . ", " . $DB->F($calc_date) . ", " . $DB->F($USER->getId()) . ", " . $DB->F($_POST["tq" . $item]) . ", now())";
            }
        }
        if (is_array($ts_items) && count($ts_items) > 0) {
            foreach ($ts_items as $item) {
                $wt_params = adm_techprice_plugin::getPItemsById($item);
                $sql_add1[] = "(" . $DB->F($task_id) . ", " . $DB->F($item) . ", " . $DB->F($tech_id) . ", " . (isset($_POST["ts_val_val" . $item["id"]]) ? $_POST["ts_val_val" . $item["id"]] : $DB->F($_POST["ts" . $item])) . ", " . (isset($_POST["ts_val_id" . $item["id"]]) ? $_POST["ts_val_id" . $item["id"]] : $DB->F($wt_params["price"])) . ", " . $DB->F($wt_params["title"]) . ", " . $DB->F($calc_date) . ", " . $DB->F($USER->getId()) . ", 1, now())";
            }
        }
        if ($sql_add1) {
            $sql = "INSERT IGNORE INTO `list_tech_calc` (`task_id`, `work_id`, `tech_id`, `smeta_val`, `price`, `work_title`, `calc_date`, `user_id`, `quant`, `update_date`) VALUES " . implode(",", $sql_add1) . ";";
            $DB->query($sql);
            if ($DB->errno()) {
                echo "error" . $DB->error() . $sql;

                return false;
            }
        }

        /**
         * Расчёт ТМЦ
         * // Списано
         * (tmc_sc_tech.inplace=0 OR tmc_sc_tech.inplace IS NULL) AND
         * (tmc_sc_tech.incoming=0 OR tmc_sc_tech.incoming IS NULL)
         * // Приход
         * (tmc_sc_tech.incoming=1 AND
         * (tmc_sc_tech.inplace=0 OR tmc_sc_tech.inplace IS NULL))
         * // На руках
         * (tmc_sc_tech.inplace=1)
         ***********************************************************************
         * // Списано с СЦ
         * (tmc_sc.inplace <= 0 OR tmc_sc.inplace IS NULL) OR tmc_sc.deleted>0
         * // Только демонтированное находиться на СЦ
         * (tmc_sc.inplace=1) AND (tmc_sc.deleted = 0 OR tmc_sc.deleted IS NULL) AND tmc_sc.dismantled = 1
         * // на складе на СЦ
         * (tmc_sc.inplace=1) AND (tmc_sc.deleted = 0 OR tmc_sc.deleted IS NULL)
         */

        /* УДаление предыдущего расчёта ТМЦ */
        $sql = "SELECT /* списаное на заявку ТМЦ */
                tmc_sc_tech.id, list_materials.metric_flag
            FROM
                `tmc_ticket`
                LEFT JOIN
                `tmc_sc_tech` ON tmc_sc_tech.id=tmc_ticket.tmc_tech_sc_id
                LEFT JOIN
                tmc_sc ON (tmc_sc.id = tmc_sc_tech.tmc_sc_id)
                LEFT JOIN
                list_materials ON (tmc_sc.tmc_id = list_materials.id)
            WHERE
                tmc_ticket.task_id=" . $DB->F($task_id) . " AND
                (tmc_ticket.isreturn = 0 OR tmc_ticket.isreturn IS NULL)
            ;";

        $DB->query($sql);
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $res[] = $r;
                if ($r["metric_flag"] != 1 && $r["metric_flag"] != null) {
                    /* возврат оборудования технику */
                    $sql = "UPDATE `tmc_sc_tech` SET `inplace`=1, ledit=now() WHERE (`inplace`=0 OR `inplace` IS NULL) AND (`incoming`=0 OR `incoming` IS NULL) AND `id`=" . $DB->F($r["id"]) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql;
                        trigger_error($DB->error());

                        return false;
                    }
                    $DB->free();
                } else {
                    /* возврат оборудования технику */
                    $sql = "DELETE FROM `tmc_sc_tech` WHERE `id`=" . $DB->F($r["id"]) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql;
                        trigger_error($DB->error());

                        return false;
                    }
                    $DB->free();
                }
            }
        }
//
        $sql = "DELETE FROM /* удаление тикета на ТМЦ */
                `tmc_ticket`

            WHERE
                `task_id`=" . $DB->F($task_id) . " AND
                (tmc_ticket.isreturn = 0 OR tmc_ticket.isreturn IS NULL)
            ;";
        $DB->query($sql);
        if ($DB->errno()) {
            echo "error";
            trigger_error($DB->error());

            return false;
        }
        /**/

        /* Внесениние нового списания ТМЦ на заявку */
        if (count($_REQUEST["writeoff_tmc"])) {
            foreach ($_REQUEST["writeoff_tmc"] as $key => $item) {
                $serial = isset($_REQUEST["writeoff_tmc"][$key]['sn']) ? $_REQUEST["writeoff_tmc"][$key]['sn'] : false;
                $target_sc_id = $_REQUEST["target_sc_id"];
                $tech_id = $_POST['tech_id'];
                $calc_tsk_id = $_REQUEST["calc_tsk_id"];
                $isrent = $_REQUEST["writeoff_tmc"][$key]['rent'] == "on" ? "1" : "0";

                if ($serial)
                    $sql = "SELECT
                        tmc_sc.id
                    FROM
                        `tmc_sc`
                        LEFT JOIN
                        tmc_sc_tech ON (tmc_sc_tech.tmc_sc_id = tmc_sc.id)
                    WHERE
                        tmc_sc.`sc_id` = " . $DB->F($target_sc_id) . " AND
                        tmc_sc.`tmc_id` = " . $DB->F($item['tmc_id']) . " AND
                        tmc_sc_tech.tech_id = " . $DB->F($tech_id) . " AND
                        `serial`=" . $DB->F($serial) . " AND
                        /* списано с СЦ */
                        (tmc_sc.`incoming` = 0 OR tmc_sc.`incoming` IS NULL) AND
                        (tmc_sc.`inplace` = 0 OR tmc_sc.`inplace` IS NULL)
                    ;";
                else
                    $sql = "SELECT
                        tmc_sc.id
                    FROM
                        `tmc_sc`
                        LEFT JOIN
                        tmc_sc_tech ON (tmc_sc_tech.tmc_sc_id = tmc_sc.id)
                    WHERE
                        tmc_sc.`sc_id` = " . $DB->F($target_sc_id) . " AND
                        tmc_sc.`tmc_id` = " . $DB->F($item['tmc_id']) . " AND
                        tmc_sc_tech.tech_id = " . $DB->F($tech_id) . " AND
                        /* списано с СЦ */
                        (tmc_sc.`incoming` = 0 OR tmc_sc.`incoming` IS NULL) AND
                        (tmc_sc.`inplace` = 0 OR tmc_sc.`inplace` IS NULL)
                    ;";
                $scRec = $DB->getField($sql);
                if (empty($scRec)) {
                    echo $err = "error Оборудование не было верно списано с Сервисного Центра! Обратитесь к разработчику.";
                    print_r($item);
                    trigger_error($err);

                    return false;
                }

                if ($DB->errno()) {
                    echo "error" . $DB->error() . $sql;
                    trigger_error($DB->error());

                    return false;
                }

                $sql1 = false;
                if ($serial && $item['count'] == 1) {
                    $sql = "UPDATE /* списание ТМЦ с техника */
                        `tmc_sc_tech`
                    SET
                        `inplace`=0,
                        ledit=now()
                    WHERE
                        `tmc_sc_id`=" . $DB->F($scRec) . " AND
                        /* на руках у техника */
                        `inplace`=1 AND (`incoming`=0 OR `incoming` IS NULL)
                    ;";
                    $sql1 = "SELECT /* id записи ТМЦ на руках у техника */
                        `id`
                    FROM
                        `tmc_sc_tech`
                    WHERE
                        `tmc_sc_id`=" . $DB->F($scRec) . " AND
                         /* на руках у техника */
                        `inplace`=1 AND (`incoming`=0 OR `incoming` IS NULL);";
                    $outRec = $DB->getField($sql1);
                } else {
                    $sql = "INSERT INTO
                        `tmc_sc_tech`
                    SET
                        `sc_id` = " . $DB->F($target_sc_id) . ",
                        `tech_id` = " . $DB->F($tech_id) . ",
                        `tmc_sc_id` = " . $DB->F($scRec) . ",
                        `count` = " . $DB->F($item['count']) . ",
                        `user_id` = " . $DB->F($USER->getId()) . ",
                        `inplace` = 0,
                        ledit=now(),
                        `incoming` = 0
                    ;";
                }
                $DB->query($sql);

                if ($DB->errno()) {
                    echo "error" . $DB->error() . $sql;
                    trigger_error($DB->error());

                    return false;
                }
                if (!$sql1)
                    $outRec = $DB->insert_id();

                $DB->free();

                if ($outRec) {
                    $sql = "INSERT INTO
                        `tmc_ticket`
                    SET
                        `task_id` = " . $DB->F($calc_tsk_id) . ",
                        `tmc_tech_sc_id` = " . $DB->F($outRec) . ",
                        `count` = " . $DB->F($item['count']) . ",
                        `tech_id` = " . $DB->F($tech_id) . ",
                        `isrent` = " . $DB->F($isrent) . ",
                        `user_id` = " . $DB->F($USER->getId()) . ",
                        ledit = now()
                    ;";
                    $DB->query($sql);

                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql;
                        trigger_error($DB->error());

                        return false;
                    }
                    $DB->free();
                } else {
                    echo $err = "error Оборудование не было верно записано на техника! обратитесь к разработчику.";
                    print_r($item);

                    //trigger_error($err);
                    return false;
                }
            }
        } else {
            echo "notfilled";

            return false;
        }

        if (!$sql_add && !$sql_add1)
            echo "notfilled";
        else
            echo "ok";

        return false;
    }

    function calc_tmc_ticket()
    {
        global $DB, $USER;
        $task_id = $_POST["calc_tsk_id"];
        $tech_id = $_POST["tech_id"];
        $calc_date = $_POST["calc_date"] ? substr($_POST["calc_date"], 6, 4) . "-" . substr($_POST["calc_date"], 3, 2) . "-" . substr($_POST["calc_date"], 0, 2) : date("Y-m-d");
        if (!$task_id || !$tech_id) {
            echo "notfilled";

            return false;
        }
        // !!! очистка текущего расчета ТМЦ
        /**
         * Расчёт ТМЦ
         * // Списано
         * (tmc_sc_tech.inplace=0 OR tmc_sc_tech.inplace IS NULL) AND
         * (tmc_sc_tech.incoming=0 OR tmc_sc_tech.incoming IS NULL)
         * // Приход
         * (tmc_sc_tech.incoming=1 AND
         * (tmc_sc_tech.inplace=0 OR tmc_sc_tech.inplace IS NULL))
         * // На руках
         * (tmc_sc_tech.inplace=1)
         ***********************************************************************
         * // Списано с СЦ
         * (tmc_sc.inplace <= 0 OR tmc_sc.inplace IS NULL) OR tmc_sc.deleted>0
         * // Только демонтированное находиться на СЦ
         * (tmc_sc.inplace=1) AND (tmc_sc.deleted = 0 OR tmc_sc.deleted IS NULL) AND tmc_sc.dismantled = 1
         * // на складе на СЦ
         * (tmc_sc.inplace=1) AND (tmc_sc.deleted = 0 OR tmc_sc.deleted IS NULL)
         */

        /* УДаление предыдущего расчёта ТМЦ */
        $sql = "SELECT /* списаное на заявку ТМЦ */
                    tmc_sc_tech.id, list_materials.metric_flag
                FROM
                    `tmc_ticket`
                    LEFT JOIN
                    `tmc_sc_tech` ON tmc_sc_tech.id=tmc_ticket.tmc_tech_sc_id
                    LEFT JOIN
                    tmc_sc ON (tmc_sc.id = tmc_sc_tech.tmc_sc_id)
                    LEFT JOIN
                    list_materials ON (tmc_sc.tmc_id = list_materials.id)
                WHERE
                    tmc_ticket.task_id=" . $DB->F($task_id) . " AND
                    (tmc_ticket.isreturn = 0 OR tmc_ticket.isreturn IS NULL)
                ;";

        $DB->query($sql);
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $res[] = $r;
                if ($r["metric_flag"] != 1 && $r["metric_flag"] != null) {
                    /* возврат оборудования технику */
                    $sql = "UPDATE `tmc_sc_tech` SET `inplace`=1, ledit=now() WHERE (`inplace`=0 OR `inplace` IS NULL) AND (`incoming`=0 OR `incoming` IS NULL) AND `id`=" . $DB->F($r["id"]) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql;
                        trigger_error($DB->error());

                        return false;
                    }
                    $DB->free();
                } else {
                    /* возврат оборудования технику */
                    $sql = "DELETE FROM `tmc_sc_tech` WHERE `id`=" . $DB->F($r["id"]) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql;
                        trigger_error($DB->error());

                        return false;
                    }
                    $DB->free();
                }
            }
        }
//
        $sql = "DELETE FROM /* удаление тикета на ТМЦ */
                  `tmc_ticket`
                WHERE
                    `task_id`=" . $DB->F($task_id) . " AND
                    (tmc_ticket.isreturn = 0 OR tmc_ticket.isreturn IS NULL)
                ;";
        $DB->query($sql);
        if ($DB->errno()) {
            echo "error";
            trigger_error($DB->error());

            return false;
        }
        /* END УДаление предыдущего расчёта ТМЦ */

        if (count($_REQUEST["writeoff_tmc"])) {
            foreach ($_REQUEST["writeoff_tmc"] as $key => $item) {
                $serial = isset($_REQUEST["writeoff_tmc"][$key]['sn']) ? $_REQUEST["writeoff_tmc"][$key]['sn'] : false;
                $target_sc_id = $_REQUEST["target_sc_id"];
                $tech_id = $_POST['tech_id'];
                $calc_tsk_id = $_REQUEST["calc_tsk_id"];
                $isrent = $_REQUEST["writeoff_tmc"][$key]['rent'] == "on" ? "1" : "0";

                if ($serial)
                    $sql = "SELECT
                        tmc_sc.id
                    FROM
                        `tmc_sc`
                        LEFT JOIN
                        tmc_sc_tech ON (tmc_sc_tech.tmc_sc_id = tmc_sc.id)
                    WHERE
                        tmc_sc.`sc_id` = " . $DB->F($target_sc_id) . " AND
                        tmc_sc.`tmc_id` = " . $DB->F($item['tmc_id']) . " AND
                        tmc_sc_tech.tech_id = " . $DB->F($tech_id) . " AND
                        `serial`=" . $DB->F($serial) . " AND
                        /* списано с СЦ */
                        (tmc_sc.`incoming` = 0 OR tmc_sc.`incoming` IS NULL) AND
                        (tmc_sc.`inplace` = 0 OR tmc_sc.`inplace` IS NULL)
                    ;";
                else
                    $sql = "SELECT
                        tmc_sc.id
                    FROM
                        `tmc_sc`
                        LEFT JOIN
                        tmc_sc_tech ON (tmc_sc_tech.tmc_sc_id = tmc_sc.id)
                    WHERE
                        tmc_sc.`sc_id` = " . $DB->F($target_sc_id) . " AND
                        tmc_sc.`tmc_id` = " . $DB->F($item['tmc_id']) . " AND
                        tmc_sc_tech.tech_id = " . $DB->F($tech_id) . " AND
                        /* списано с СЦ */
                        (tmc_sc.`incoming` = 0 OR tmc_sc.`incoming` IS NULL) AND
                        (tmc_sc.`inplace` = 0 OR tmc_sc.`inplace` IS NULL)
                    ;";
                $scRec = $DB->getField($sql);
                if (empty($scRec)) {
                    echo $err = "error Оборудование не было верно списано с Сервисного Центра! Обратитесь к разработчику.";
                    print_r($item);
                    trigger_error($err);

                    return false;
                }

                if ($DB->errno()) {
                    echo "error" . $DB->error() . $sql;
                    trigger_error($DB->error());

                    return false;
                }

                $sql1 = false;
                if ($serial && $item['count'] == 1) {
                    $sql = "UPDATE /* списание ТМЦ с техника */
                        `tmc_sc_tech`
                    SET
                        `inplace`=0,
                        ledit=now()
                    WHERE
                        `tmc_sc_id`=" . $DB->F($scRec) . " AND
                        /* на руках у техника */
                        `inplace`=1 AND (`incoming`=0 OR `incoming` IS NULL)
                    ;";
                    $sql1 = "SELECT /* id записи ТМЦ на руках у техника */
                        `id`
                    FROM
                        `tmc_sc_tech`
                    WHERE
                        `tmc_sc_id`=" . $DB->F($scRec) . " AND
                         /* на руках у техника */
                        `inplace`=1 AND (`incoming`=0 OR `incoming` IS NULL);";
                    $outRec = $DB->getField($sql1);
                } else {
                    $sql = "INSERT INTO
                        `tmc_sc_tech`
                    SET
                        `sc_id` = " . $DB->F($target_sc_id) . ",
                        `tech_id` = " . $DB->F($tech_id) . ",
                        `tmc_sc_id` = " . $DB->F($scRec) . ",
                        `count` = " . $DB->F($item['count']) . ",
                        `user_id` = " . $DB->F($USER->getId()) . ",
                        `inplace` = 0,
                        ledit=now(),
                        `incoming` = 0
                    ;";
                }
                $DB->query($sql);

                if ($DB->errno()) {
                    echo "error" . $DB->error() . $sql;
                    trigger_error($DB->error());

                    return false;
                }
                if (!$sql1)
                    $outRec = $DB->insert_id();

                $DB->free();

                if ($outRec) {
                    $sql = "INSERT INTO
                        `tmc_ticket`
                    SET
                        `task_id` = " . $DB->F($calc_tsk_id) . ",
                        `tmc_tech_sc_id` = " . $DB->F($outRec) . ",
                        `count` = " . $DB->F($item['count']) . ",
                        `tech_id` = " . $DB->F($tech_id) . ",
                        `isrent` = " . $DB->F($isrent) . ",
                        `user_id` = " . $DB->F($USER->getId()) . ",
                        ledit = now()
                    ;";
                    $DB->query($sql);

                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql;
                        trigger_error($DB->error());

                        return false;
                    }
                    $DB->free();
                } else {
                    echo $err = "error Оборудование не было верно записано на техника! обратитесь к разработчику.";
                    print_r($item);

                    //trigger_error($err);
                    return false;
                }
            }
        } else {
            echo "notfilled";

            return false;
        }

        echo "ok";

        return false;

    }

    function vieworderdoc()
    {
        global $DB, $USER;
        if ($id = $_REQUEST["id"]) {
            $ticket = new ConnectionTicket($id);
            $sql = "SELECT gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.task_id=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->num_rows()) {
                list($c_date, $startTime, $user) = $DB->fetch();
                $DB->free();
            }
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $tpl->loadTemplatefile($USER->getTemplate() . "/orderdoc.tmpl.htm");
            $tpl->setVariable("TASK_ID", $id);
            $tpl->setVariable("OD_NUM", $id . " / " . $ticket->getClntTNum());
            $tpl->touchBlock('client_type_' . $ticket->getClientType());
            if ($ticket->getClientType() != User::CLIENT_TYPE_PHYS) {
                $tpl->setVariable("OD_CLNT_INFO", $ticket->getOrgName() . "<br />" . $ticket->getFio());
            } else {
                $tpl->setVariable("OD_CLNT_INFO", $ticket->getFio());
            }
            $tpl->setVariable("OD_CONTRAGENT", $ticket->getContrName());
            $tpl->setVariable("OD_WTYPE", $ticket->getTypeName());
            $phnes = $ticket->getPhones();
            if ($USER->checkAccess("hidetelnum")) {
                $tpl->setVariable("OD_CLNT_TEL", "---");
            } else {
                $tpl->setVariable("OD_CLNT_TEL", ($phnes[0] ? $phnes[0] : '+7') . "<br />" . ($phnes[1] ? $phnes[1] : '+7'));
            }
            $tpl->setVariable("OD_SC", $this->getSc($ticket->getSCId()));
            $tpl->setVariable("OD_EMPL", @$user ? $user : "---");
            $ods = $this->getOds($ticket->getDomId());
            $tpl->setVariable("OD_ODS", $ods ? $ods["title"] . " " . $ods["tel"] : "---");
            $tpl->setVariable("OD_CREATEDATE", rudate("d M Y H:i"));
            $addrval = $ticket->getAddr($ticket->getDomId());
            $tpl->setVariable("OD_ADDR", $addrval["city"] . " " . $addrval["street"] . " " . $addrval["house"]);
            if (@$c_date && @$startTime) {
                $startHour = (strlen(floor($startTime / 60)) == 1) ? "0" . (floor($startTime / 60)) : floor($startTime / 60);
                $startMin = (strlen(floor($startTime % 60)) == 1) ? "0" . (floor($startTime % 60)) : floor($startTime % 60);
                $tpl->setVariable("OD_GFXTIME", rudate("d M Y", strtotime($c_date)) . " " . $startHour . ":" . $startMin);
            } else $tpl->setVariable("OD_GFXTIME", "---");
            $tpl->setVariable("OD_ADDR_ENTR", $ticket->getPod());
            $tpl->setVariable("OD_CNLT_AGRNUM", "<strong>" . $ticket->getClntOpAgr() . "</strong><span style=\"font-weight:normal;\">, Заявка №" . $ticket->getClntTNum() . "</span>");
            $tpl->setVariable("OD_ADDR_DOMOFON", $ticket->getDomofon());
            $tpl->setVariable("OD_TASK_DEVPLACE", $ticket->getSwPlace());
            $tpl->setVariable("OD_ADDR_FLOOR", $ticket->getEtazh());
            $tpl->setVariable("OD_TASK_IP", $ticket->getSwIp());
            $tpl->setVariable("OD_ADDR_FLAT", $ticket->getKv());
            $tpl->setVariable("OD_TASK_PORT", $ticket->getSwPort());
            $tpl->setVariable("OD_ADDINFO", $ticket->getAddInfo());

            $agr_id = $ticket->getAgrId(false);
            if ($agr_id) {
                if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getAgrById")) {
                    $kontr = new kontr_plugin();
                    $agr_contacts = $kontr->getAgrById($agr_id);
                    if ($agr_contacts) {
                        $tpl->setVariable("OD_CONTR_ACCESS", $agr_contacts['access_contact'] ? $agr_contacts['access_contact'] : "<strong>-</strong>");
                        $tpl->setVariable("OD_CONTR_TP", $agr_contacts['tp_contact'] ? $agr_contacts['tp_contact'] : "<strong>-</strong>");

                    } else {
                        $tpl->setVariable("OD_CONTR_ACCESS", "<strong>ошибка</strong>");
                        $tpl->setVariable("OD_CONTR_TP", "<strong>ошибка</strong>");
                    }
                }
            } else {

                $tpl->setVariable("OD_CONTR_ACCESS", "<strong>-</strong>");
                $tpl->setVariable("OD_CONTR_TP", "<strong>-</strong>");
            }

            if (isset($_REQUEST['printcomments'])) {
                $tpl->setCurrentBlock("print-comments");
                $tpl->setVariable("C_OD_NUM", $id . " / " . $ticket->getClntTNum());
                $tpl->setVariable("C_COMMENTS_LIST", $ticket->getCommentsPrint());
                $tpl->parse("print-comments");
            }

            UIHeader($tpl);
            $tpl->show();
        } else {
            UIError("Ошибка! Не указан идентификатор заявки для печати Заказ-наряда!");
        }

    }

    static function getSC($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_sc` WHERE `id`=" . $DB->F($id) . ";";

        return $DB->getField($sql);
    }

    function newticket(Request $request)
    {
        global $DB, $USER;
        if ($USER->checkAccess("allowcreatetickets", User::ACCESS_WRITE)) {

        } else {
            if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
                UIError("Вам запрещено создавать заявки!", "Отказано в доступе!");
            }
        }
        $LKUser = adm_users_plugin::isLKUser($USER->getId());

        if ($LKUser) {
            $LKContrID = adm_users_plugin::getLKUserContrID($USER->getId());
            $contr_id = $LKContrID;
            $type = "connections";

            $sql = "SELECT wt.id, wt.title FROM `list_contr_agr` AS ca
                    LEFT JOIN `link_agr_wtypes` AS law ON law.agr_id=ca.id
                    LEFT JOIN `task_types` AS wt ON wt.id=law.wtype_id WHERE ca.contr_id=" . $DB->F($contr_id) . " AND wt.title IS NOT NULL AND wt.plugin_uid=" . $DB->F($type) . " ORDER BY wt.title;";
            $r = $DB->getCell($sql);
            if (!$r) UIError("У Вас нет активных договоров в WF для создания заявок такого типа!");

            $tpl_add = "_lk";
        } else {
            $tpl_add = "";
        }

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("new.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/new$tpl_add.tmpl.htm");
        else
            $tpl->loadTemplatefile("new$tpl_add.tmpl.htm");
        $tpl->setVariable("LK_CONTR_ID", $LKContrID);
        $tpl->setVariable("LK_CONTR_NAME", kontr_plugin::getByID($LKContrID));
        $tpl->setVariable('TYPE_OPTIONS', $this->getTypeOptions());
        $tpl->setVariable('CNT_OPTIONS', $this->getContrOptions());

        $mgts_tc = $DB->getCell2("SELECT `id`, `title` FROM `mgts_tc` WHERE `active`;");
        $tpl->setVariable("MGTS_TC_LIST", array2options($mgts_tc));
        $mgts_serv = $DB->getCell2("SELECT `id`, `title` FROM `mgts_services` WHERE 1;");
        $tpl->setVariable("MGTS_SERVICE_LIST", array2options($mgts_serv));
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKRegionList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_REGION", $addr->getKRegionList(getcfg('default_region')));
        }
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKCityList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_CITY", $addr->getKCityList(getcfg('default_region')));
        }
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKNPList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_NP", $addr->getKNPList(getcfg('default_region')));
        }
        if (class_exists("adm_ods_plugin", true) && method_exists("adm_ods_plugin", "getOptList")) {
            $addr = new adm_ods_plugin();
            $tpl->setVariable("ODS_OPTIONS", $addr->getOptList());
        }
        if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getScList")) {
            $addr = new adm_sc_plugin();
            $tpl->setVariable("SC_OPTIONS", $addr->getScList());
        }

        $tpl->setVariable("TASK_FROM", adm_ticket_sources_plugin::getOptions());

        UIHeader($tpl);
        $tpl->show();
    }

    public function saveticket(Request $request)
    {
        $em = $this->getEm();

        $err = [];
        $partnerId = $request->get('cnt_id');
        if (!$partnerId) {
            $err[] = "Не выбран контрагент";
        } else {
            $partner = $em->getReference(\models\ListContr::class, $partnerId);
        }

        $typeId = $request->get('type_id');
        if (!$typeId) {
            $err[] = "Не выбран вид работ";
        } else {
            $type = $em->getReference(\models\TaskType::class, $typeId);
        }

        $clientType = (int)$request->get('clnt_type');
        if (!$clientType) {
            $err[] = "Не выбран тип клиента";
        }

        $clientOrgName = $request->get('clnt_org_name');
        $clientFio = $request->get('clnt_fio');

        if ($clientType == \classes\User::CLIENT_TYPE_YUR && !$clientOrgName) {
            $err[] = "Не заполнено название организации";
        } elseif (!$clientFio) {
            $err[] = "Не заполнено ФИО клиента";
        }

        if (sizeof($err)) {
            UIError($err);
        }

        $executorIds = $request->get('ispoln_id');
        $executorIds = empty($executorIds) ? [] : explode(',', $executorIds);

        $address = $this->getEm()->getRepository(ListAddr::class)
            ->setAddressProvider($this->getContainer()->get('wf.address.provider'))
            ->findOrCreateByAddressArray([
                'city' => $request->get('full_address')
            ], [
                'count' => 1
            ]);

        $serviceCenterId = $request->get('sc_id');
        $serviceCenter = $serviceCenterId > 0 ? $em->getReference(\models\ListSc::class, $serviceCenterId) : null;

        $dateFrom = $request->get('date_from');
        $dateTo = $request->get('date_to');
        $slotBegin = null;
        $slotEnd = null;
        if ($dateFrom && $dateTo) {
            $slotBegin = new DateTime($dateFrom);
            $slotEnd = new DateTime($dateTo);
        }
        $tm = $this->getContainer()->get('wf.task.task_manager');

        $em->beginTransaction();
        try {

            $commentText = $request->get('cmm');
            $comments = [];
            if ($commentText) {
                $comments[] = (new \models\TaskComment())
                    ->setText($commentText)
                    ->setAuthor($this->getUserModel());
            }

            $task = $tm->createTask(TaskEntity::CONNECTION, [
                'author_id' => $this->getUserModel()->getId(),
                'title' => 'Заявка на подключение',
                'addresses' => [$address],
                'executor_ids' => $executorIds,
                'comments' => $comments,
            ]);

            $tm->createTicket($task, [
                'task_type' => $type,
                'partner' => $partner,
                'agreement_id' => (int)$request->get('agr_id'),
                'external_id' => $request->get('clnttnum'),
                'service_center' => $serviceCenter,
                'address' => $address,
                'entrance' => $request->get('pod'),
                'floor' => $request->get('etazh'),
                'apartment' => $request->get('kv'),
                'intercom' => $request->get('domofon'),
                'client_type' => $clientType,
                'client_operator_agreement_number' => $request->get('clntopagr'),
                'client_org_name' => $clientOrgName,
                'client_full_name' => $clientFio,
                'client_phone1' => $request->get('clnt_phone1'),
                'client_phone2' => $request->get('clnt_phone2'),
                'landline_phone' => $request->get('mgts_phone_adsl'),
                'additional_info' => $request->get('addinfo'),
                'slot_begin' => $slotBegin,
                'slot_end' => $slotEnd,
                'switch_ip' => $request->get('swip'),
                'switch_port' => $request->get('swport'),
                'switch_location' => $request->get('swplace'),

                'mgts_tech_center_id' => $request->get('tc_id', 0),
                'mgts_ats' => $request->get('mgts_ats') ?? $request->get('mgts_ats_adsl'),
                'mgts_orsh' => $request->get('mgts_orsh'),
                'mgts_channel_count' => $request->get('mgts_chan'),
                'mgts_phone_count' => $request->get('mgts_phones'),
                'mgts_service_type_id' => $request->get('mgts_service_id', 0),
                'mgts_agreement_number' => $request->get('mgts_agr_num'),
            ]);

            $em->commit();
        } catch (Exception $ex) {
            $em->rollback();
            throw $ex;
        }

        redirect($this->getLink('viewticket', "task_id={$task->getId()}"));
    }

    function updateticket(Request $request)
    {
        global $DB;
        $ticket = new ConnectionTicket($_REQUEST['task_id']);
        $err = [];
        if (!$_POST['sc_id']) {
            $err[] = "Не выбран сервисный центр";
        }
        if ($ticket->getClientType() == \classes\User::CLIENT_TYPE_YUR && !$_POST['org_name']) {
            $err[] = "Не заполнено название организации";
        } elseif (!$_POST['fio']) {
            $err[] = "Не заполнено ФИО клиента";
        }

        if (sizeof($err)) {
            UIError($err);
        }

        $comment = "";
        if ($ticket->getSCId() != $_POST['sc_id']) {
            $comment .= "Сервисный центр: " . $this->getSCName($ticket->getSCId()) . " -> " . $this->getSCName($_POST['sc_id']) . "\r\n";
        }

        if (isset($_POST["clnttype"])) {
            if ($ticket->getClientType() != $_POST["clnttype"]) {
                $comment .= "Тип клиента: " . ($ticket->getClientType() == 1 ? "Физ. лицо" : "Юр. лицо") . " -> " . ($_POST["clnttype"] == 1 ? "Физ. лицо" : "Юр. лицо") . "\r\n";
            }
        }
        if ($ticket->getClientType() == \classes\User::CLIENT_TYPE_YUR && $ticket->getOrgName(false) != $_POST['org_name']) {
            $comment .= "Название организации: " . $ticket->getOrgName(false) . " -> " . $_POST['org_name'] . "\r\n";
        }
        if ($ticket->getFio(false) != $_POST['fio'])
            $comment .= "ФИО: " . $ticket->getFio(false) . " -> " . $_POST['fio'] . "\r\n";
        $phones = $ticket->getPhones();
        if ($phones[0] != preg_replace("/[^0-9]/", "", $_POST['phone1']))
            $comment .= "Телефон (1): {$phones[0]} -> {$_POST['phone1']}\r\n";
        if ($phones[1] != preg_replace("/[^0-9]/", "", $_POST['phone2']))
            $comment .= "Телефон (2): {$phones[1]} -> {$_POST['phone2']}\r\n";
        if ($ticket->getSwIp(false) != $_POST['swip'])
            $comment .= "IP-адрес: " . $ticket->getSwIp(false) . " -> " . $_POST['swip'] . "\r\n";
        if ($ticket->getSwPort(false) != $_POST['swport'])
            $comment .= "Порт: " . $ticket->getSwPort(false) . " -> " . $_POST['swport'] . "\r\n";
        if ($ticket->getSwPlace(false) != $_POST['swplace'])
            $comment .= "Расположение свитча: " . $ticket->getSwPlace(false) . " -> " . $_POST['swplace'] . "\r\n";
        if ($ticket->getClntOpAgr(false) != $_POST['clntopagr'])
            $comment .= "Номер договора оператора с клиентом: " . $ticket->getClntOpAgr(false) . " -> " . $_POST['clntopagr'] . "\r\n";
        if ($ticket->getClntTNum(false) != $_POST['clnttnum'])
            $comment .= "Номер заявки клиента в базе оператора: " . $ticket->getClntTNum(false) . " -> " . $_POST['clnttnum'] . "\r\n";
        if ($ticket->getCableQuant(false) != $_POST['cablequant'])
            $comment .= "Количество использованного кабеля: " . $ticket->getCableQuant(false) . " -> " . $_POST['cablequant'] . "\r\n";

        if ($ticket->getAddNumber() != $_POST["clntaddtnum"]) {
            $comment .= "Доп номер (1С): " . $ticket->getAddNumber() . " -> " . $_POST['clntaddtnum'] . "\r\n";
        }

        if ($ticket->getContrId() == getcfg('mgts_contr_id') && $ticket->getType() == getcfg('mgts_b2b_id')) {
            if ($ticket->getMGTS_TC() != $_POST["tc_id"]) {
                $comment .= " Тех. центр МГТС: " . $DB->getField("SELECT `title` FROM `mgts_tc` WHERE `id`=" . $ticket->getMGTS_TC() . ";") . " -> " . $DB->getField("SELECT `title` FROM `mgts_tc` WHERE `id`=" . $_POST["tc_id"] . ";") . "\r\n";
            }
            if ($ticket->getMGTS_SER() != $_POST["mgts_service_id"]) {
                $comment .= "Услуги МГТС: " . $DB->getField("SELECT `title` FROM `mgts_services` WHERE `id`=" . $ticket->getMGTS_SER() . ";") . " -> " . $DB->getField("SELECT `title` FROM `mgts_services` WHERE `id`=" . $_POST["mgts_service_id"] . ";") . "\r\n";
            }
            if ($ticket->getMGTS_ATS() != $_POST["mgts_ats"]) {
                $comment .= "АТС МГТС: " . $ticket->getMGTS_ATS() . " -> " . $_POST["mgts_ats"] . "\r\n";
            }
            if ($ticket->getMGTS_ORSH() != $_POST["mgts_orsh"]) {
                $comment .= "ОРШ МГТС: " . $ticket->getMGTS_ORSH() . " -> " . $_POST["mgts_orsh"] . "\r\n";
            }
            if ($ticket->getMGTS_CHAN() != $_POST["mgts_chan"]) {
                $comment .= "Кол-во каналов МГТС: " . $ticket->getMGTS_CHAN() . " -> " . $_POST["mgts_chan"] . "\r\n";
            }
            if ($ticket->getMGTS_PH() != $_POST["mgts_phones"]) {
                $comment .= "Кол-во номеров МГТС: " . $ticket->getMGTS_PH() . " -> " . $_POST["mgts_phones"] . "\r\n";
            }
            if ($ticket->getMGTS_AgrNum() != $_POST["mgts_agr_num"]) {
                $comment .= "Номер договора МГТС: " . $ticket->getMGTS_AgrNum() . " -> " . $_POST["mgts_agr_num"] . "\r\n";
            }
            $ticket->setMGTS_ATS($_POST["mgts_ats"]);
        } else {
            if ($ticket->getContrId() == getcfg('mgts_contr_id') && $ticket->getType() == "37") {
                if ($ticket->getMGTS_ATS() != $_POST["mgts_ats_adsl"]) {
                    $comment .= "АТС МГТС: " . $ticket->getMGTS_ATS() . " -> " . $_POST["mgts_ats_adsl"] . "\r\n";
                }
                if ($ticket->getMGTS_LandlineNum() != $_POST["mgts_phone_adsl"]) {
                    $comment .= "Городской номер МГТС: " . $ticket->getMGTS_LandlineNum() . " -> " . $_POST["mgts_phone_adsl"] . "\r\n";
                }
                $ticket->setMGTS_ATS($_POST["mgts_ats_adsl"]);
                $ticket->setMGTS_LandlineNum($_POST["mgts_phone_adsl"]);
            }
        }

        if ($ticket->getSlotDateBegin() != $_POST["date_from"]) {
            $comment .= "График оператора с (дата): " . $ticket->getSlotDateBegin() . " -> " . $_POST['date_from'] . "\r\n";
        }

        if ($ticket->getSlotDateEnd() != $_POST["date_to"]) {
            $comment .= "График оператора по (дата): " . $ticket->getSlotDateEnd() . " -> " . $_POST['date_to'] . "\r\n";
        }

        if ($ticket->getSlotTimeBegin() != $_POST["time_from"]) {
            $comment .= "График оператора с (время): " . $ticket->getSlotTimeBegin() . " -> " . $_POST['time_from'] . "\r\n";
        }

        if ($ticket->getSlotTimeEnd() != $_POST["time_to"]) {
            $comment .= "График оператора по (время): " . $ticket->getSlotTimeEnd() . " -> " . $_POST['time_to'] . "\r\n";
        }

        if (isset($_POST["donedate"]) && preg_match("/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/", $_POST["donedate"])) {
            if ($ticket->getDoneDate() != $_POST["donedate"]) {
                $comment .= "ОБНОВЛЕНА ДАТА ВЫПОЛНЕНИЯ: " . $ticket->getDoneDate() . " -> " . $_POST['donedate'] . "\r\n";
                if (!$ticket->setDoneDate($_POST["donedate"]))
                    UIError("Ошибка обновления ДАТЫ ВЫПОЛНЕНИЯ!");
            }
        }
        if ($ticket->getTaskFrom(false) != $_POST['task_from'])
            $comment .= "Откуда узнали: " . adm_ticket_sources_plugin::getById($ticket->getTaskFrom(false)) . " -> " . adm_ticket_sources_plugin::getById($_POST['task_from']) . "\r\n";


        $sc_id = ($_POST['sc_id'] > 0) ? $_POST['sc_id'] : null;

        $ticket->setSCId($sc_id);

        if (isset($_POST["clnttype"])) {
            $ticket->setClientType($_POST["clnttype"]);
        }
        $ticket->setOrgName($_POST['org_name']);
        $ticket->setFio($_POST['fio']);
        $ticket->setPhones($_POST['phone1'], $_POST['phone2']);
        $ticket->setAddInfo($_POST["addinfo"]);
        $ticket->setSwIp($_POST["swip"]);
        $ticket->setSwPort($_POST["swport"]);
        $ticket->setSwPlace($_POST["swplace"]);
        $ticket->setClntOpAgr($_POST["clntopagr"]);
        $ticket->setClntTNum($_POST["clnttnum"]);
        $ticket->setCableQuant($_POST["cablequant"]);
        $ticket->setTaskFrom($_POST["task_from"]);
        $ticket->setAddNumber($_POST["clntaddtnum"]);
        $ticket->setMGTS_CHAN($_POST["mgts_chan"]);
        $ticket->setMGTS_ORSH($_POST["mgts_orsh"]);
        $ticket->setMGTS_PH($_POST["mgts_phones"]);
        $ticket->setMGTS_SER($_POST["mgts_service_id"]);
        $ticket->setMGTS_TC($_POST["tc_id"]);
        $ticket->setMGTS_AgrNum($_POST["mgts_agr_num"]);

        $slotBegin = $_POST["date_from"] ? substr($_POST["date_from"], 6, 4) . "-" . substr($_POST["date_from"], 3, 2) . "-" . substr($_POST["date_from"], 0, 2) : "";
        $slotBegin .= ($_POST["time_from"] ? " " . $_POST["time_from"] . ":00" : " 00:00:00");
        $slotEnd = $_POST["date_to"] ? substr($_POST["date_to"], 6, 4) . "-" . substr($_POST["date_to"], 3, 2) . "-" . substr($_POST["date_to"], 0, 2) : "";
        $slotEnd .= ($_POST["time_to"] ? " " . $_POST["time_to"] . ":00" : " 00:00:00");

        $ticket->setSlotBegin($slotBegin);
        $ticket->setSlotEnd($slotEnd);
        //die($slotBegin." ".$slotEnd);
        if ($ticket->updateCommit()) {
            if ($comment) $ticket->addComment($comment, "Обновлены данные");
        }

        // передача комментария контрагенту начало
        if (isset($_POST['cmm_to_contr']) && $_POST['cmm_contr'] && ($contr_id = $ticket->getContrId())) {
            $sql = "SELECT `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=" . $DB->F($ticket->getId()) . " AND `contr_id`=" . $DB->F($contr_id);
            if ($contr_ticket_id = $DB->getField($sql)) {
                if ($contr_id == getcfg('2kom_contr_id')) {
                    try {
                        $client = new SoapClient(getcfg('2kom_soap_url'));
                        $result = $client->UpdateOrder(getcfg('2kom_soap_login'), getcfg('2kom_soap_pass'), $contr_ticket_id, 0, '', $_POST['cmm_contr']);
                        trigger_error("2KOM SOAP Result code {$result['retcode']}: " . $result['retmessage']);
                        if ($result['retcode'] != 0) {
                            $err[] = "Ошибка обновления даннх в 2KOM: " . $result['retmessage'];
                        } else {
                            $ticket->addComment($_POST['cmm_contr'], "Отправлен комментарий контрагенту");
                        }
                    } catch (SoapFault $e) {
                        trigger_error("2KOM SOAP Exeption! " . $e->getMessage(), E_USER_WARNING);
                        $err[] = "Ошибка связи с 2KOM: " . $e->getMessage();
                    }
                }
            }
        }
        // передача комментария контрагенту конец

        if (sizeof($err)) UIError($err);
        redirect($this->getLink('viewticket', "task_id=" . $ticket->getId()));
    }


}
