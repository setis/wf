<?php 
/**
 * Plugin Header
 * @author kblp
 */

use classes\User;

$plugin_uid = "lkaccess2alltickets";

$PLUGINS[$plugin_uid]['name'] = "ЛК Контрагента: доступ к заявкам (автор и контрагент)";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Подключение";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Заявки на подключение";
    $PLUGINS[$plugin_uid]['events']['viewticket'] = "Просмотр заявки";
    $PLUGINS[$plugin_uid]['events']['vieworderdoc'] = "Печать заказ-наряда";
    $PLUGINS[$plugin_uid]['events']['viewlist_export'] = "Заявки на подключение. Выгрузка.";
    $PLUGINS[$plugin_uid]['events']['queryStatus'] = "Запрос текущего статуса Заявки";
    $PLUGINS[$plugin_uid]['events']['getwtypelist'] = "Список видов работ";
    $PLUGINS[$plugin_uid]['events']['getagrlist'] = "Список договоров";
    
}
if(wf::$user->checkAccess($plugin_uid, User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['newticket'] = "Новая заявка на подключение";
    $PLUGINS[$plugin_uid]['events']['saveticket'] = "Сохранение зявки";
    $PLUGINS[$plugin_uid]['events']['updateticket'] = "Обновление зявки";
    $PLUGINS[$plugin_uid]['events']['removefromgfx'] = "Удаление из графика";
    $PLUGINS[$plugin_uid]['events']['deleteticket'] = "Удаление заявки";
    $PLUGINS[$plugin_uid]['events']['calcticket'] = "Расчет заявки";
    $PLUGINS[$plugin_uid]['events']['calc_tmc_ticket'] = "Расчет заявки";
    $PLUGINS[$plugin_uid]['events']['getCurrentCalcA'] = "Загрузка расчета заявки";
    $PLUGINS[$plugin_uid]['events']['getCurrentTMCCalcA'] = "Загрузка расчета заявки по ТМЦ";
    $PLUGINS[$plugin_uid]['events']['checkTaskStatusA'] = "Проверка статуса заявки для перевода в статус Сдача отчета";
    $PLUGINS[$plugin_uid]['events']['updateContrDataA'] = "Изменение Контрагента, договора и вида работ";
    $PLUGINS[$plugin_uid]['events']['clearreport'] = "Удаление (обнуление) расчета по заявке";
    $PLUGINS[$plugin_uid]['events']['addphoto'] = "Добавление фотоотчета";
    $PLUGINS[$plugin_uid]['events']['deletephotofile'] = "Удаление записи фотоотчета";
    
}

define('CONNECTIONS_ALLOW_CANCELING', 'connections.allow_canceling');
$plugin_uid = CONNECTIONS_ALLOW_CANCELING;
$PLUGINS[$plugin_uid]['name'] = "Подключения: Отказные статусы";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "connections_questionnarie_access";
$PLUGINS[$plugin_uid]['name'] = "Подключения: Доступ к анкете";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "calc_ticket";

$PLUGINS[$plugin_uid]['name'] = "Подключение: Расчет Заявок";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['deleteticket'] = "Расчет";
}

$plugin_uid = "calc_tmc_ticket";

$PLUGINS[$plugin_uid]['name'] = "Подключение: Расчет ТМЦ по заявкам";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['deletetmcticket'] = "Расчет ТМЦ по заявкам";
}

$plugin_uid = "edit_contr_ticket";

$PLUGINS[$plugin_uid]['name'] = "Подключение: Изменение контрагента";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['editcontr'] = "Редактор контрагента";
}

$plugin_uid = 'connections2contr';

$PLUGINS[$plugin_uid]['name'] = "Подключение: комментарии контрагенту";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'changectype';

$PLUGINS[$plugin_uid]['name'] = "Подключение: Изменение типа клиента по заявке";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'rooteditticket';

$PLUGINS[$plugin_uid]['name'] = "Подключение: Редактирование данных в заявках в любых статусах";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'changedonedate';

$PLUGINS[$plugin_uid]['name'] = "Подключение: Изменение даты выполнения";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'scfullaccess';
$PLUGINS[$plugin_uid]['name'] = "Подключение: Полный доступ ко всем СЦ";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'calcticketafterdeadline';
$PLUGINS[$plugin_uid]['name'] = "Подключение: Редактирование заявки после отчетного периода";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'showfilter';
$PLUGINS[$plugin_uid]['name'] = "Отображение фильтра в раскрытом виде (ВСЕ)";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'erasetaskreport';
$PLUGINS[$plugin_uid]['name'] = "Подключение: Удаление (обнуление) отчета по заявке";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "allowcreatetickets";
$PLUGINS[$plugin_uid]['name'] = "Создание заявок на подключение техником";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "allowpir";
$PLUGINS[$plugin_uid]['name'] = "Подключение: Статус П.И.Р.";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "allowsmr";
$PLUGINS[$plugin_uid]['name'] = "Подключение: Статус С.М.Р.";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "allowodsandaddrparams";
$PLUGINS[$plugin_uid]['name'] = "Подключение: Редактор параметров адреса и выбор ОДС";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "plk.connection.view.calls";
$PLUGINS[$plugin_uid]['name'] = "Подключение: Просмотр звонков по заявке (ЛК Партнёра)";
$PLUGINS[$plugin_uid]['hidden'] = true;
