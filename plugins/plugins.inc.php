<?php
global $PLUGINS;

/**
 * TODO Artem make necessary rules to user_access table to allow this action without auth
 * Костыли, что бы можно было размещать в меню ссылки на новые модули.
 * @see \WF\HttpKernel\ControllerResolver::getController()
 */
$root_dir = wf::$container->getParameter('root_dir');

require_once($root_dir.'/src/WF/Slack/config/acl.php');
require_once($root_dir.'/src/WF/Bills/config/acl.php');
require_once($root_dir.'/src/WF/Task/config/acl.php');
require_once($root_dir.'/src/WF/Users/config/acl.php');
require_once($root_dir.'/src/WF/BugReport/config/acl.php');
require_once($root_dir.'/src/WF/Telemarketing/config/acl.php');
require_once($root_dir.'/src/WF/ServiceCenter/config/acl.php');

require_once(path2('/plugins/clone_ticket/clone_ticket.h.php'));
require_once(path2('/plugins/projects/projects.h.php'));
require_once(path2('/plugins/project_types/project_types.h.php'));
require_once(path2('/plugins/bills/bills.h.php'));
//require_once(path2('/plugins/bill_types/bill_types.h.php'));

require_once(path2('/plugins/gfx/gfx.h.php'));
require_once(path2('/plugins/connections/connections.h.php'));

require_once(path2('/plugins/services/services.h.php'));
require_once(path2('/plugins/tm_module/tm_module.h.php'));
require_once(path2('/plugins/tm_module_dict/tm_module_dict.h.php'));
require_once(path2('/plugins/accidents/accidents.h.php'));
require_once(path2('/plugins/adm_ticket_tttypes/adm_ticket_tttypes.h.php'));
require_once(path2('/plugins/gp/gp.h.php'));
require_once(path2('/plugins/adm_ticket_gptypes/adm_ticket_gptypes.h.php'));

require_once(path2('/plugins/adm_skp_catnames/adm_skp_catnames.h.php'));
require_once(path2('/plugins/adm_skp_edizm/adm_skp_edizm.h.php'));
require_once(path2('/plugins/adm_skp_price/adm_skp_price.h.php'));
require_once(path2('/plugins/adm_skp_wtypes/adm_skp_wtypes.h.php'));

require_once(path2('/plugins/kontr/kontr.h.php'));
require_once(path2('/plugins/kontr_proptypes/kontr_proptypes.h.php'));
require_once(path2('/plugins/agreements/agreements.h.php'));
require_once(path2('/plugins/adm_acts_wtypes/adm_acts_wtypes.h.php'));

require_once(path2('/plugins/addr_interface/addr_interface.h.php'));
require_once(path2('/plugins/adm_ods/adm_ods.h.php'));

require_once(path2('/plugins/tmc/tmc.h.php'));
require_once($root_dir.'/src/WF/Tmc/config/acl.php');

require_once(path2('/plugins/reports/reports.h.php'));
require_once(path2('/plugins/reports_contr/reports_contr.h.php'));
require_once(path2('/plugins/reports_f_contr/reports_f_contr.h.php'));
require_once(path2('/plugins/reports_f_tech42/reports_f_tech42.h.php'));
require_once(path2('/plugins/reports_f_tech_qc/reports_f_tech_qc.h.php'));
require_once(path2('/plugins/reports_f_qc_calls_stats/reports_f_qc_calls_stats.h.php'));
require_once(path2('/plugins/acts_tt/acts_tt.h.php'));
require_once(path2('/plugins/reports_f_tech_rates/reports_f_tech_rates.h.php'));

require_once(path2('/plugins/reports_f_sc42/reports_f_sc42.h.php'));

require_once(path2('/plugins/reports_f_total/reports_f_total.h.php'));
require_once(path2('/plugins/reports_f_total_v2/reports_f_total_v2.h.php'));

require_once(path2('/plugins/reports_f_total_tt/reports_f_total_tt.h.php'));
require_once(path2('/plugins/reports_f_total_tt_v2/reports_f_total_tt_v2.h.php'));

require_once(path2('/plugins/reports_f_skp_total2/reports_f_skp_total2.h.php'));
require_once(path2('/plugins/reports_f_skp_total_mod/reports_f_skp_total_mod.h.php'));
require_once(path2('/plugins/reports_f_skp_avg_price/reports_f_skp_avg_price.h.php'));
require_once(path2('/plugins/reports_f_gp_conn/reports_f_gp_conn.h.php'));
require_once(path2('/plugins/acts_ticket/acts_ticket.h.php'));
require_once(path2('/plugins/acts_ticket_services/acts_ticket_services.h.php'));

require_once(path2('/plugins/acts_tmc/acts_tmc.h.php'));
require_once(path2('/plugins/acts_tmc_v2/acts_tmc_v2.h.php'));

require_once(path2('/plugins/adm_ticket_rtsubtypes/adm_ticket_rtsubtypes.h.php'));

require_once(path2('/plugins/reports_contr_rejects/reports_contr_rejects.h.php'));
require_once(path2('/plugins/reports_task_types/reports_task_types.h.php'));
require_once(path2('/plugins/reports_ser_contr/reports_ser_contr.h.php'));
require_once(path2('/plugins/reports_tt_gp_types/reports_tt_gp_types.h.php'));
require_once(path2('/plugins/reports_tt_gp/reports_tt_gp.h.php'));

require_once(path2('/plugins/reports_tmc_ttgp_totask/reports_tmc_ttgp_totask.h.php'));
require_once(path2('/plugins/reports_tmc_ttgp_fromtask/reports_tmc_ttgp_fromtask.h.php'));


require_once(path2('/plugins/reports_ser_contr_rejects/reports_ser_contr_rejects.h.php'));
require_once(path2('/plugins/reports_ser_task_types/reports_ser_task_types.h.php'));
require_once(path2('/plugins/reports_op_conn/reports_op_conn.h.php'));
require_once(path2('/plugins/reports_b_pays/reports_b_pays.h.php'));

require_once(path2('/plugins/reports_f_skp_mr/reports_f_skp_mr.h.php'));
require_once(path2('/plugins/reports_f_skp_total/reports_f_skp_total.h.php'));
require_once(path2('/plugins/reports_f_skp_agents/reports_f_skp_agents.h.php'));
require_once(path2('/plugins/reports_f_skp_cons/reports_f_skp_cons.h.php'));
require_once(path2('/plugins/reports_f_skp_dsnal/reports_f_skp_dsnal.h.php'));

require_once(path2('/plugins/reports_tmc_sc_move/reports_tmc_sc_move.h.php'));
require_once(path2('/plugins/reports_tmc_sc_total/reports_tmc_sc_total.h.php'));
require_once(path2('/plugins/reports_tmc_tech_move/reports_tmc_tech_move.h.php'));
require_once(path2('/plugins/reports_tmc_tech_total/reports_tmc_tech_total.h.php'));

require_once(path2('/plugins/reports_f_skp_card/reports_f_skp_card.h.php'));

require_once(path2('/plugins/reports_f_skp_accn/reports_f_skp_accn.h.php'));
require_once(path2('/plugins/reports_f_skp_accbn/reports_f_skp_accbn.h.php'));
require_once(path2('/plugins/reports_f_skp_timeouts/reports_f_skp_timeouts.h.php'));

require_once(path2('/plugins/reports_acts_dynamic/reports_acts_dynamic.h.php'));
require_once(path2('/plugins/reports_acts_static/reports_acts_static.h.php'));

require_once(path2('/plugins/reports_mgts_b2b/reports_mgts_b2b.h.php'));

require_once(path2('/plugins/reports_mgts_b2b_tmc/reports_mgts_b2b_tmc.h.php'));

require_once(path2('/plugins/reports_sik_b2b/reports_sik_b2b.h.php'));
require_once(path2('/plugins/reports_sik_b2b_tmc/reports_sik_b2b_tmc.h.php'));

require_once(path2('/plugins/comments/comments.h.php'));
require_once(path2('/plugins/browse/browse.h.php'));
require_once(path2('/plugins/adm_interface/adm_interface.h.php'));
require_once(path2('/plugins/adm_areas/adm_areas.h.php'));
require_once(path2('/plugins/adm_districts/adm_districts.h.php'));
require_once(path2('/plugins/adm_users/adm_users.h.php'));
require_once(path2('/plugins/user_positions/user_positions.h.php'));
require_once(path2('/plugins/adm_sc/adm_sc.h.php'));
require_once(path2('/plugins/adm_empl/adm_empl.h.php'));
require_once(path2('/plugins/adm_ticktypes/adm_ticktypes.h.php'));
require_once(path2('/plugins/adm_statuses/adm_statuses.h.php'));
require_once(path2('/plugins/adm_emplsched/adm_emplsched.h.php'));
require_once(path2('/plugins/adm_rejects/adm_rejects.h.php'));
require_once(path2('/plugins/adm_clearbase/adm_clearbase.h.php'));
require_once(path2('/plugins/adm_techprice/adm_techprice.h.php'));
require_once(path2('/plugins/virt_delticket/virt_delticket.h.php'));
require_once(path2('/plugins/virt_editticket/virt_editticket.h.php'));
require_once(path2('/plugins/virt_deladdr/virt_deladdr.h.php'));
require_once(path2('/plugins/adm_material_cats/adm_material_cats.h.php'));
require_once(path2('/plugins/adm_materials/adm_materials.h.php'));
require_once(path2('/plugins/adm_ticket_sources/adm_ticket_sources.h.php'));
require_once(path2('/plugins/callout/callout.h.php'));
require_once(path2('/plugins/callviewer/callviewer.h.php'));
require_once(path2('/plugins/qc/qc.h.php'));
require_once(path2('/plugins/q_list/q_list.h.php'));
require_once(path2('/plugins/adm_gp_reason/adm_gp_reason.h.php'));
require_once(path2('/plugins/adm_gp_comment/adm_gp_comment.h.php'));

require_once(path2('/plugins/import_conn_akado/import_conn_akado.h.php'));
require_once(path2('/plugins/load_price_mgts/load_price_mgts.h.php'));
require_once(path2('/plugins/adm_task_photo/adm_task_photo.h.php'));
require_once(path2('/plugins/adm_mgts_tc/adm_mgts_tc.h.php'));
require_once(path2('/plugins/reports_f_skp_wtypes/reports_f_skp_wtypes.h.php'));
require_once(path2('/plugins/barcode/barcode.h.php'));
require_once(path2('/plugins/reports_indocs/reports_indocs.h.php'));

/****/
$plugin_uid = \Gorserv\Gerp\AdminBundle\Controller\SyslogController::class;
$PLUGINS[$plugin_uid]['name'] = "Syslog";
$PLUGINS[$plugin_uid]['hidden'] = true;
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Журнал событий";
}
/***/
