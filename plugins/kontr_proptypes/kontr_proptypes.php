<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class kontr_proptypes_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getOptions($id = 0)
    {
        global $DB;
        $sql = "SELECT * FROM `kontr_proptypes` WHERE 1;";
        $res = $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());
        return array2options($res, $id);
    }

    static function getList()
    {
        global $DB;
        $sql = "SELECT * FROM `kontr_proptypes` WHERE 1;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        $res = array();
        if ($DB->num_rows()) {
            while ($fetch = $DB->fetch(true)) {
                $res[] = $fetch;
            }
        } else {
            $res = false;
        }
        $DB->free();
        return $res;
    }

    static function getById($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `kontr_proptypes` WHERE `id`=" . $DB->F($id) . ";";
        $res = $DB->getField($sql);
        if ($DB->errno()) UIError($DB->error());
        return $res ? $res : false;
    }

    static function getOptionsA()
    {
        global $DB;
        $sql = "SELECT * FROM `kontr_proptypes` WHERE 1;";
        $res = $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());
        echo array2options($res);
    }

    function viewlist()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        $sql = "SELECT * FROM `kontr_proptypes` WHERE 1;";
        $DB->query($sql);
        if ($DB->errno()) {
            UIError($DB->error());
        }
        while (list($id, $title, $desc, $areacnt) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->parse("vl_item");
        }
        $DB->free();
        UIHeader($tpl);
        $tpl->show();
    }

    function edit() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        if ($cat_id = $_REQUEST['id']) {
            $sql = "SELECT * FROM `kontr_proptypes` WHERE `id`=" . $DB->F($cat_id) . ";";
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Форма собственности с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);

            $tpl->setVariable("CAT_ID", $result["id"]);
            $tpl->setVariable("CAT_TITLE", $result["title"]);

        }
        UIHeader($tpl);
        $tpl->show();

    }

    function save() {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = array();

        if(!$_POST['title']) $err[] = "Не заполнено Название Формы собственности";

        if(sizeof($err)) UIError($err);

        if($_POST['id']) $sql = "UPDATE `kontr_proptypes` SET `title`=".$DB->F($_POST['title'])." WHERE `id`=".$DB->F($_POST['id']);
        else {
            $sql = "INSERT INTO `kontr_proptypes` (`title`) VALUES(".$DB->F($_POST['title']).")";
        }
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());

        if(!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Форма собственности сохранена. ID: ".$_POST['id'] );
    }

    function delete() {
        global $DB;
        if (!($id = $_REQUEST['id'])) $err[] = "Не указан Категория ТМЦ";
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($id)) UIError("Форма собственности связана с Контрагентом. Удаление невозможно.");
        $DB->query("DELETE FROM `kontr_proptypes` WHERE `id`='".$_REQUEST['id']."';");
        redirect($this->getLink(),"Форма собственности успешно удалена.");

    }

    function isBusy($id)
    {
        global $DB;
        $DB->query("SELECT `id` FROM `list_contr` WHERE `proptype_id`=" . $DB->F($id) . ";");
        if ($DB->num_rows()) $err = true; else $err = false;
        $DB->free();
        return $err;
    }


}
?>
