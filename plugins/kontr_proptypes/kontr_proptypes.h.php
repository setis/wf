<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Формы собственности ЮЛ";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'kontr_proptypes';

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Формы собственности ЮЛ";
    $PLUGINS[$plugin_uid]['events']['getOptionsA'] = "Формы собственности ЮЛ (optionList ajax)";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование Формы собственности ЮЛ";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление Формы собственности ЮЛ";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение Формы собственности ЮЛ";
    
    
}
?>