<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;



 
class reports_b_pays_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
     
    function main() {
        global $DB, $USER;
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/reports_b_pays.tmpl.htm");
            else 
                $tpl->loadTemplatefile("reports_b_pays.tmpl.htm");
            $tpl->setVariable("REPORT_CONTR", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date("d.m.Y") ." - 1 month")));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            $tpl->setVariable("FILTER_CNT_OPTIONS", kontr_plugin::getOptList(@$_POST["cnt_id"]));
            $tpl->setVariable("PLAT_LIST", bills_plugin::getPlatOptions($_REQUEST['plat_id']));
            $tpl->setVariable("FILTER_CAT_OPTIONS", bill_types_plugin::getOptions(@$_POST["cat_id"]));
            
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
            $rtpl->setVariable("DATETO", $_POST['dateto']);
            $contr_name = kontr_plugin::getByID($_POST["cnt_id"]);
            $rtpl->setVariable("CTGR_NAME", $contr_name ? $contr_name : "&mdash;");
            if ($_POST["plat_id"]) {
                $sqladd[] = " (`plat_id`=".$DB->F($_POST["plat_id"]).") ";
            }
            if ($_POST["cnt_id"]) {
                $sqladd[] = " (`poluch`=".$DB->F($_POST["cnt_id"])." OR `poluch`=".$DB->F($contr_name).") ";
            }
            if ($_POST["cat_id"]) 
            $sqladd[] = " `exp_type_id`=".$DB->F($_POST["cat_id"]);
            if (sizeof($sqladd)) {
                $sql_add = " WHERE ".implode(" AND ", $sqladd);
            }
            
            $sql  = "SELECT b.bill_id as billid, (SELECT `title` FROM `bill_types` WHERE `id`=bb.exp_type_id) as paycat, bb.sum as total, b.ammount as paid, DATE_FORMAT(b.pay_date, '%d %m %Y') as paydate, bb.poluch, bb.plat_id 
                    FROM `bills_payments` as b LEFT JOIN `bills` AS bb ON bb.task_id=b.bill_id WHERE b.bill_id IN (SELECT `task_id` FROM `bills` $sql_add) AND
                     b.pay_date>=".$DB->F(date("Y-m-d", strtotime($_POST['datefrom'])))." AND b.pay_date<=".$DB->F(date("Y-m-d", strtotime($_POST['dateto'])));
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error()." ".$sql);
            if ($DB->num_rows()) {
                $total = 0;
                $totalpaid = 0;
                $prev_bill = 0;
                while ($r = $DB->fetch(true)) {
                    $rtpl->setCurrentBlock("rep_row");
                    $rtpl->setVariable("PAY_DATE", $r["paydate"]);
                    $rtpl->setVariable("BILL_ID", $r["billid"]);
                    if (preg_match("/^\d+$/", $r["poluch"])) {
                        $contr = kontr_plugin::getByID($r["poluch"]);
                    } else {
                        $contr = $r["poluch"];
                    }
                    
                    $rtpl->setVariable("CTGR", $contr);
                    $rtpl->setVariable("PAY_CAT", $r["paycat"]);
                    if (!$total) {
                        $total = $r["total"];
                        $prev_bill = $r["billid"];
                    }
                    if ($prev_bill != $r["billid"]) {
                        $total += $r["total"];
                        $prev_bill = $r["billid"];
                    }
                    //$total = $r["total"];
                    $totalpaid += $r["paid"];
                    $rtpl->setVariable("SUM", number_format($r["total"], 2, ".", " "));
                    $rtpl->setVariable("PAID", number_format($r["paid"], 2, ".", " "));
                    $rtpl->setVariable("PLATNAME", bills_plugin::getPlatTitle($r["plat_id"]));
                    
                    $rtpl->parse("rep_row");       
                }   
                $rtpl->setCurrentBlock("footer");
                $rtpl->setVariable("TOTALSUM", number_format($total, 2, ".", " "));
                $rtpl->setVariable("TOTALPAID", number_format($totalpaid, 2, ".", " "));
                $rtpl->parse("footer");
            } else {
                $rtpl->touchBlock("noreprow");
            }
            $DB->free();
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_paid_bills_".$_POST['datefrom']."-".$_POST['dateto'].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 

}
?>