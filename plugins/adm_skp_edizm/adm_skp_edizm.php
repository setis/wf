<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class adm_skp_edizm_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getOptList($sel_id = false)
    {
        global $DB;

        return array2options($DB->getCell2("SELECT `id`, `title` FROM `list_skp_p_edizm` ORDER BY `title`"), $sel_id);

    }

    static function getByID($id)
    {
        global $DB;
        if (!$id) return false;
        return $DB->getField("SELECT `title` FROM `list_skp_p_edizm` WHERE `id`=" . $DB->F($id) . ";");
    }

    function getEdIzmTitle($id) {
        global $DB;
        $sql = "SELECT `title` FROM `list_skp_p_edizm` WHERE `id`=" . $DB->F($id) . ";";
        return $DB->getField($sql);

    }

    function getList($tType = false) {
        global $DB;

        return $DB->getCell2("SELECT `id`, `title` FROM `list_skp_p_edizm` ORDER BY `title`");

    }

    function viewlist()
    {
        global $DB, $USER, $PLUGINS;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/adm_skp_edizm.tmpl.htm");
        else
                $tpl->loadTemplatefile("adm_skp_edizm.tmpl.htm");
        $sql = "SELECT l.*, (SELECT `fio` FROM `users` WHERE `id`=l.cr_uid) FROM `list_skp_p_edizm` AS l ORDER BY `id`;";
        $DB->query($sql);
        if ($DB->errno()) {
            UIError($DB->error());
        }
        while (list($id, $title, $fulltitle, $cr_uid, $cr_date, $author) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->setVariable("VLI_FTITLE", $fulltitle);
            $tpl->setVariable("VLI_AUTHOR", $author);
            $tpl->setVariable("VLI_DATE", rudate("H:i:s d M Y", $cr_date));
            $tpl->parse("vl_item");
        }
        UIHeader($tpl);
        $tpl->show();
    }

    function edit() {
        global $DB, $PLUGINS, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
        else
                $tpl->loadTemplatefile("edit.tmpl.htm");
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($id = $_REQUEST['id']) {
            $sql = "SELECT * FROM `list_skp_p_edizm` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Запись с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);

            $tpl->setVariable("TYPE_ID", $result["id"]);
            $tpl->setVariable("TITLE", $result["title"]);
            $tpl->setVariable("FTITLE", $result["fulltitle"]);

        }

        UIHeader($tpl);
        $tpl->show();

    }

    function save() {
        global $DB, $USER;
        $err = array();
        if(!$_POST['title']) $err[] = "Не заполнено поле Название!";
        if(sizeof($err)) UIError($err);
        if($_POST['id']) $sql = "UPDATE `list_skp_p_edizm` SET `title`=".$DB->F($_POST['title']).", `fulltitle`=".$DB->F($_POST['fulltitle'])." WHERE `id`=".$DB->F($_POST['id']).";";
        else {
            $sql = "INSERT INTO `list_skp_p_edizm` (`title`, `fulltitle`, `cr_uid`) VALUES (".$DB->F($_POST['title']).", ".$DB->F($_POST['fulltitle']).", ".$DB->F($USER->getId()).");";
        }
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error(). "<br />".$sql);

        if(!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink(), "Единица измерения сохранена. ID: ".$_POST['id']);
    }

    function delete() {
        global $DB;
        if (!($id = $_REQUEST['id'])) $err[] = "Не указан ИД!";
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST["id"])) UIError("Выбранная запись занята. Удаление невозможно.");
        $DB->query("DELETE FROM `list_skp_p_edizm` WHERE `id`='".$_REQUEST['id']."';");
        redirect($this->getLink(), "Единица измерения успешно удалена.");
    }

    function isBusy($id)
    {
        global $DB;
        $cnt = $DB->getField("SELECT COUNT(id) FROM `list_skp_p_wtypes` WHERE `ei_id`=" . $DB->F($id));
        $cnt1 = $DB->getField("SELECT COUNT(id) FROM `list_acts_wtypes` WHERE `edizm`=" . $DB->F($id));
        if ($cnt != 0 || $cnt1 != 0) {
            return true;
        } else return false;
    }

    function getEdIzmList($selected = false, $plugin_uid=false, $tag = false) {
        global $DB;
        $sql = "SELECT * FROM `list_skp_p_edizm` WHERE 1 ORDER BY `id`;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        $ret = false;
        if ($DB->num_rows()) {
            while (list($id, $title, $author, $c_date)=$DB->fetch()) {
                if ($id == $selected)
                    $ret .="<option selected='selected' value='$id' >$title</option>";
                else
                    $ret .="<option value='$id'>$title</option>";
            }
        }
        $DB->free();

        return $ret;
    }
}
?>
