<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2015
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Отчет по установленному оборудованию";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Отчет по установленному оборудованию";
    $PLUGINS[$plugin_uid]['events']['getcontrlist_ajax'] = "Список контрагентов";
}
?>
