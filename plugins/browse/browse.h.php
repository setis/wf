<?php

/**
 * Plugin Header
 * 
 * @author Gorserv - kblp
 * @copyright 2011
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Обзор пользвателей";
$PLUGINS[$plugin_uid]['hidden'] = true;

$PLUGINS[$plugin_uid]['events']['users'] = "Обзор пользователей";
$PLUGINS[$plugin_uid]['events']['users_ajax'] = "Обзор пользователей";
$PLUGINS[$plugin_uid]['events']['users_by_task_ajax'] = "Обзор пользователей по заявке";
$PLUGINS[$plugin_uid]['events']['all_users_by_task_ajax'] = "Обзор всех пользователей по заявке";

$PLUGINS[$plugin_uid]['events']['users2'] = "Обзор пользователей";
$PLUGINS[$plugin_uid]['events']['users3'] = "Обзор пользователей ЛК";
$PLUGINS[$plugin_uid]['events']['wtypes'] = "Обзор видов работ";
$PLUGINS[$plugin_uid]['events']['areasBySC'] = "Обзор районов";
$PLUGINS[$plugin_uid]['events']['emplListBySC'] = "Обзор техников по СЦ";
$PLUGINS[$plugin_uid]['events']['areaList'] = "Обзор районов по округам";
$PLUGINS[$plugin_uid]['events']['contragents'] = "Обзор контрагентов (заявки)";
$PLUGINS[$plugin_uid]['events']['chiefsBySC'] = "Обзор начальников СЦ";

$PLUGINS[$plugin_uid]['events']['projects'] = "Обзор проектов";
$PLUGINS[$plugin_uid]['events']['bills'] = "Обзор счетов";
