<?php

/**
 * Plugin Implementation
 *
 * @author Gorserv - kblp
 * @copyright 2011
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;



/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class browse_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function users_ajax() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        if ($_REQUEST["small"] != "")
        $tpl->loadTemplatefile("users_ajax_small.tmpl.htm");
        else
        $tpl->loadTemplatefile("users_ajax.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $_REQUEST["filter_start"] = intval($_REQUEST["filter_start"]);
        $_REQUEST["su_archive"] = $_REQUEST["su_archive"] == "on" ? true : false;

        if (empty($_REQUEST['with_roles'])) {
            $filter[] = " u.is_access_template = 0 ";
            $filter[] = $_REQUEST["su_archive"] ? " u.active = 0 " : " u.active = 1 ";
        }
        else {
            if ($_REQUEST["su_archive"]) {
                $filter[] = " u.is_access_template = 1 ";
                $filter[] = " u.active = 0 ";
            }
            else {
                $filter[] = " ((u.is_access_template = 1 AND u.active = 0) OR u.active = 1) ";
            }
        }


//        $filter[] = empty($_REQUEST['with_roles']) ? " u.is_access_template = 0 " : " u.is_access_template = 1 ";
//
//        if ($_REQUEST["su_archive"]) $filter[] = " !u.active "; else $filter[] = " u.active ";


        if ($_REQUEST["su_username"]) $filter[] = " u.fio like ".$DB->F("%".$_REQUEST["su_username"]."%")." ";
        if (!isset($_REQUEST['su_type_id'])) { $_REQUEST["su_type_id"] = $_REQUEST["su_type_id1"];}
        if ($_REQUEST["su_sc_id"] || $_REQUEST["su_sc_id1"]) {
            $sc = $_REQUEST["su_sc_id1"] ? $_REQUEST["su_sc_id1"] : $_REQUEST["su_sc_id"];
            $filter[] = " u.id IN (SELECT DISTINCT(`user_id`) FROM `link_sc_user` WHERE `sc_id`=".$DB->F($sc).") ";
            $_REQUEST["su_type_id"] = 3;
        }
        switch($_REQUEST["su_type_id"] ?: $_REQUEST["su_type_id1"]) {
            case "1":
                $filter[] = " !`contr_user` AND u.id NOT IN (SELECT DISTINCT(`user_id`) FROM `link_sc_chiefs`) AND u.id NOT IN (SELECT DISTINCT(`user_id`) FROM `list_empl`) ";
                break;

            case "2":
                $filter[] = " `contr_user`>0 ";
                break;

            case "3":
                # EF: Новая система доступов на СЦ
                #$filter[] = " (u.id IN (SELECT DISTINCT(`user_id`) FROM `list_empl`)) ";
                $filter[] = "( exists (select *
  from link_sc_user lsc,
       user_sc_access usa
 where usa.user_id = ".$USER->getId()."
   and lsc.user_id = u.id
   and usa.sc_id = lsc.sc_id) )";
                break;

            case "4":
                $filter[] = " u.id IN (SELECT DISTINCT(`user_id`) FROM `link_sc_chiefs`) ";
                break;


            default:
                break;
        }

        if ($_REQUEST["su_podr_id"]) $filter[] = " u.otdel_id=".$_REQUEST["su_podr_id"]." ";
        $order = " u.fio ASC";
        if (sizeof($filter)) {
            $filter_sql = " WHERE ".implode("AND", $filter);
        }
        $sql = "SELECT DISTINCT SQL_CALC_FOUND_ROWS u.*, (select `name` from `user_otdels` where `id`=u.otdel_id) as otdel FROM `users` AS u $filter_sql
        ORDER BY $order LIMIT {$_REQUEST['filter_start']},7";

        $DB->query($sql);
        $total = $DB->getFoundRows();
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $tpl->setCurrentBlock("urow");
                $tpl->setVariable("UID", $r["id"]);
                $tpl->setVariable("FIO", $r["fio"]);
                $tpl->setVariable("POSITION", adm_users_plugin::getPos($r["pos_id"]));
                $tpl->setVariable("PODR", $r["otdel"]);
                $tpl->setVariable("EMAIL", $r["email"]);
                $tpl->parse("urow");
            }
        } else {
            $tpl->touchBlock("norec");
        }
        $ret["sql"] = $sql;
        $ret["tpl"] = $tpl->get();
        $ret["ok"] = "ok";
        $ret["total"] = $total;
        $ret["pages"] = pages_ajax($_REQUEST['filter_start'], 7, $total, "#start-%s");

        return json_encode($ret);
    }

    function users_by_task_ajax()
    {
        global $DB, $USER;
        if ($_REQUEST["task_id"]!= "") {
            $t = new Task($_REQUEST["task_id"]);
            $sql = "SELECT u.user_id, CONCAT((SELECT `fio` FROM `users` WHERE `id`=u.user_id) , ' (' , (SELECT `title` FROM `user_pos` WHERE `id` IN (SELECT `pos_id` FROM `users` WHERE `id`=u.user_id)), ')') as userpos  FROM `task_users` AS u WHERE `task_id`=".$DB->F($_REQUEST["task_id"])." AND `ispoln`;";
            $r = $DB->getCell2($sql);
            if ($r) {
                foreach($r as $key => $vl) {
                    $ret["tpl"] .= "<tr id=\"$key\" class='seluser_row'><td class='sunamepos'>".$vl."</td><td><a href='#' id=\"$key\" class='removeseluseritem'><img src='/templates/images/cross.png' title='Удалить' /></a></td></tr>";
                }
            }
            if ($DB->errno())
                $ret["error"] = $sql;
        }
        $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;
    }

    function all_users_by_task_ajax()
    {
        global $DB, $USER;
        if ($_REQUEST["task_id"]!= "") {
            $t = new Task($_REQUEST["task_id"]);
            $sql = "SELECT u.user_id,
CONCAT((SELECT `fio` FROM `users` WHERE `id`=u.user_id) ,
' (' ,
(SELECT `title` FROM `user_pos` WHERE `id` IN (SELECT `pos_id` FROM `users` WHERE `id`=u.user_id)),
')') as userpos
FROM `task_users` AS u
join users as us on us.id = u.user_id
join tasks as t on t.id = u.task_id
WHERE 1=1
  and case when t.plugin_uid = 'projects' and us.active = 1 then 1
           when t.plugin_uid != 'projects' then 1
           else 0
       end = 1
  and `task_id`=".$DB->F($_REQUEST["task_id"]).";";
            $r = $DB->getCell2($sql);
            if ($r) {
                foreach($r as $key => $vl) {
                    $ret["tpl"] .= "<tr id=\"$key\" class='seluser_row'><td class='sunamepos'>".$vl."</td><td><a href='#' id=\"$key\" class='removeseluseritem'><img src='/templates/images/cross.png' title='Удалить' /></a></td></tr>";
                }
            }
            if ($DB->errno())
                $ret["error"] = $sql;
        }
        $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;
    }




    function users()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("users.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        if ($_REQUEST["user_ids"] == "") {
            if ($_REQUEST["task_id"]!= "") {
                $t = new Task($_REQUEST["task_id"]);
                if ($t->getPluginUid() == "projects" || $t->getPluginUid() == "bills") {
                    $sql = "SELECT `user_id` FROM `task_users` WHERE `task_id`=".$DB->F($_REQUEST["task_id"])." AND `ispoln`;";
                    $r = $DB->getCell($sql);
                    $_REQUEST["user_ids"] = preg_replace("/,$/", "", implode(",", $r));
                }
            }
        }
        //print_r($_REQUEST["user_ids"]);
        //die();
        $checked = explode(',', $_REQUEST['user_ids']);
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseUsers');

        $sql = "SELECT `id`, `name` FROM `user_otdels` ORDER BY `order`, `name`";
        foreach($otdels = $DB->getCell2($sql) as $otdel_id=>$otdel_name) {
            $tpl->setCurrentBlock('ou');
            $tpl->setVariable('OTDEL_ID', $otdel_id);
            $tpl->setVariable('OTDEL_NAME', $otdel_name);

            $sql = "SELECT `id`, `fio`, `pos_id` FROM `users` WHERE `otdel_id`=".$DB->F($otdel_id)." AND `active` ORDER BY `fio`";
            $DB->query($sql);
            while(list($user_id, $user_fio, $user_pos) = $DB->fetch()) {
                $tpl->setCurrentBlock('user');
                $tpl->setVariable('USER_ID', $user_id);
                $tpl->setVariable('USER_FIO', $user_fio);
                $tpl->setVariable("USER_POS", adm_users_plugin::getPos($user_pos));
                //$tpl->setVariable('USER_POS', $user_pos ? $user_pos : "&mdash;");
                $tpl->setVariable('USER_CHK', in_array($user_id, $checked) ? 'checked' : '');
                $tpl->parse('user');
            }
            $DB->free();

            $tpl->parse('ou');
        }


        UIHeader($tpl);
        $tpl->show();
    }

    function users2()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("users2.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        $checked = $_REQUEST['user_id'];
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseUser');

        $sql = "SELECT `id`, `name` FROM `user_otdels` ORDER BY `order`, `name`";
        foreach($otdels = $DB->getCell2($sql) as $otdel_id=>$otdel_name) {
            $tpl->setCurrentBlock('ou');
            $tpl->setVariable('OTDEL_ID', $otdel_id);
            $tpl->setVariable('OTDEL_NAME', $otdel_name);

            $sql = "SELECT `id`, `fio` FROM `users` WHERE `otdel_id`=".$DB->F($otdel_id)." AND `active` ORDER BY `fio`";
            foreach($users = $DB->getCell2($sql) as $user_id=>$user_fio) {
                $tpl->setCurrentBlock('user');
                $tpl->setVariable('USER_ID', $user_id);
                $tpl->setVariable('USER_FIO', $user_fio);
                $tpl->setVariable('USER_CHK', $user_id == $checked ? 'checked' : '');
                $tpl->parse('user');
            }
            $DB->free();

            $tpl->parse('ou');
        }


        UIHeader($tpl);
        $tpl->show();
    }

    function users3()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("users2.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        $checked = $_REQUEST['user_id'];
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseUser');

        $sql = "SELECT `id`, `name` FROM `user_otdels` ORDER BY `order`, `name`";
        foreach($otdels = $DB->getCell2($sql) as $otdel_id=>$otdel_name) {
            $tpl->setCurrentBlock('ou');
            $tpl->setVariable('OTDEL_ID', $otdel_id);
            $tpl->setVariable('OTDEL_NAME', $otdel_name);

            $sql = "SELECT `id`, `fio` FROM `users` WHERE `otdel_id`=".$DB->F($otdel_id)." AND `active` AND `contr_user`=0 ORDER BY `fio`";
            foreach($users = $DB->getCell2($sql) as $user_id=>$user_fio) {
                $tpl->setCurrentBlock('user');
                $tpl->setVariable('USER_ID', $user_id);
                $tpl->setVariable('USER_FIO', $user_fio);
                $tpl->setVariable('USER_CHK', $user_id == $checked ? 'checked' : '');
                $tpl->parse('user');
            }
            $DB->free();

            $tpl->parse('ou');
        }


        UIHeader($tpl);
        $tpl->show();
    }

    function wtypes()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("wtypes.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $cType = @$_REQUEST['ctype'];
        $checked = explode(',', $_REQUEST['wtypes_ids']);
        $qcs_chk = explode(',', $_REQUEST['wtypes_qcs']);
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseWTypes');
        if ($cType == "0" || !$cType) UIError("Не определен тип заявок для контрагента!", "Ошибка", false, false, false, false, true, true);
        if (false !== strpos($cType, "1")) {
            $tpl->setCurrentBlock('tt');
            $tpl->setVariable('CTYPE_NAME', "Подключение");

            $sql = "SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`=".$DB->F('connections')." ORDER BY `title`";
            foreach($wtypes = $DB->getCell2($sql) as $wtype_id=>$wtype_title) {
                $tpl->setCurrentBlock('wtype_items');
                $tpl->setVariable('WTYPE_ID', $wtype_id);
                $tpl->setVariable('WTYPE_TITLE', $wtype_title);
                $tpl->setVariable('WTYPE_CHK', in_array($wtype_id, $checked) ? 'checked' : '');
                $extra = '';
                if (in_array($wtype_id, $qcs_chk)) {
                        $extra = 'checked';
                } else {
                        if (!in_array($wtype_id, $checked)) {
                                $extra = 'disabled';
                        }
                }
                $tpl->setVariable('WTYPE_QCS_CHK', $extra);
                $tpl->parse('wtype_items');
            }
            $DB->free();
            $tpl->parse('tt');
        }
        if (false !== strpos($cType, "2")) {
            $tpl->setCurrentBlock('tt');
            $tpl->setVariable('CTYPE_NAME', "Сервисное обслуживание / Ремонт");

            $sql = "SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`=".$DB->F('services')." ORDER BY `title`";
            foreach($wtypes = $DB->getCell2($sql) as $wtype_id=>$wtype_title) {
                $tpl->setCurrentBlock('wtype_items');
                $tpl->setVariable('WTYPE_ID', $wtype_id);
                $tpl->setVariable('WTYPE_TITLE', $wtype_title);
                $tpl->setVariable('WTYPE_CHK', in_array($wtype_id, $checked) ? 'checked' : '');
                $extra = '';
                if (in_array($wtype_id, $qcs_chk)) {
                        $extra = 'checked';
                } else {
                        if (!in_array($wtype_id, $checked)) {
                                $extra = 'disabled';
                        }
                }
                $tpl->setVariable('WTYPE_QCS_CHK', $extra);
                $tpl->parse('wtype_items');
            }
            $DB->free();

            $tpl->parse('tt');
        }

        if (false !== strpos($cType, "3")) {
            $tpl->setCurrentBlock('tt');
            $tpl->setVariable('CTYPE_NAME', "ТТ (Абонентские аварии)");

            $sql = "SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`=".$DB->F('accidents')." ORDER BY `title`";
            foreach($wtypes = $DB->getCell2($sql) as $wtype_id=>$wtype_title) {
                $tpl->setCurrentBlock('wtype_items');
                $tpl->setVariable('WTYPE_ID', $wtype_id);
                $tpl->setVariable('WTYPE_TITLE', $wtype_title);
                $tpl->setVariable('WTYPE_CHK', in_array($wtype_id, $checked) ? 'checked' : '');
                $extra = '';
                if (in_array($wtype_id, $qcs_chk)) {
                        $extra = 'checked';
                } else {
                        if (!in_array($wtype_id, $checked)) {
                                $extra = 'disabled';
                        }
                }
                $tpl->setVariable('WTYPE_QCS_CHK', $extra);
                $tpl->parse('wtype_items');
            }
            $DB->free();

            $tpl->parse('tt');
        }

        if (false !== strpos($cType, "4")) {
            $tpl->setCurrentBlock('tt');
            $tpl->setVariable('CTYPE_NAME', "ГП (Линейные аварии)");

            $sql = "SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`=".$DB->F('gp')." ORDER BY `title`";
            foreach($wtypes = $DB->getCell2($sql) as $wtype_id=>$wtype_title) {
                $tpl->setCurrentBlock('wtype_items');
                $tpl->setVariable('WTYPE_ID', $wtype_id);
                $tpl->setVariable('WTYPE_TITLE', $wtype_title);
                $tpl->setVariable('WTYPE_CHK', in_array($wtype_id, $checked) ? 'checked' : '');
                $extra = '';
                if (in_array($wtype_id, $qcs_chk)) {
                        $extra = 'checked';
                } else {
                        if (!in_array($wtype_id, $checked)) {
                                $extra = 'disabled';
                        }
                }
                $tpl->setVariable('WTYPE_QCS_CHK', $extra);
                $tpl->parse('wtype_items');
            }
            $DB->free();
            $tpl->parse('tt');
        }

        if (false !== strpos($cType, "5")) {
            $tpl->setCurrentBlock('tt');
            $tpl->setVariable('CTYPE_NAME', "ТМ (Телемаркетинг)");

            $sql = "SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`=".$DB->F('tm_module')." ORDER BY `title`";
            foreach($wtypes = $DB->getCell2($sql) as $wtype_id=>$wtype_title) {
                $tpl->setCurrentBlock('wtype_items');
                $tpl->setVariable('WTYPE_ID', $wtype_id);
                $tpl->setVariable('WTYPE_TITLE', $wtype_title);
                $tpl->setVariable('WTYPE_CHK', in_array($wtype_id, $checked) ? 'checked' : '');
                $extra = '';
                if (in_array($wtype_id, $qcs_chk)) {
                        $extra = 'checked';
                } else {
                        if (!in_array($wtype_id, $checked)) {
                                $extra = 'disabled';
                        }
                }
                $tpl->setVariable('WTYPE_QCS_CHK', $extra);
                $tpl->parse('wtype_items');
            }
            $DB->free();
            $tpl->parse('tt');
        }

        $tpl->setCurrentBlock('tt');
        $tpl->setVariable('CTYPE_NAME', $otdel_name);
        $sql = "SELECT `id`, `fio`, `dolzn` FROM `users` WHERE `otdel_id`=".$DB->F($otdel_id)." AND `active` ORDER BY `fio`";
        foreach($users = $DB->getCell2($sql) as $user_id=>$user_fio) {
            $tpl->setCurrentBlock('user');
            $tpl->setVariable('USER_ID', $user_id);
            $tpl->setVariable('USER_FIO', $user_fio);
            $tpl->setVariable('USER_CHK', in_array($user_id, $checked) ? 'checked' : '');
            $tpl->parse('user');
        }
        $DB->free();
        $tpl->parse('tt');
        UIHeader($tpl);
        $tpl->show();
    }

    function areasBySC() {
        global $DB;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("areassc.tmpl.htm");
        $sc_id = @$_REQUEST["sc_id"];
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $checked = explode(',', $_REQUEST['areas_ids']);
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseWTypes');

        if ($sc_id) {
            $sql_add = "`id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=".$DB->F($sc_id).")";
        } else $sql_add = "1";
            $sql = "SELECT `id`, `title`, `desc`, `kladr_code` FROM `list_area` WHERE $sql_add;";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error(), "Ошибка", false, false, false, false, true, true);
            if ($DB->num_rows()) {
                $tpl->setCurrentBlock("tt");
                while (list($id, $title, $desc, $kladr) = $DB->fetch()) {
                    $tpl->setCurrentBlock("area_items");
                    $tpl->setVariable("AREA_ID", $id);
                    $tpl->setVariable("AREA_TITLE", $title . ($desc !="" ? " (".$desc.")" : ""));
                    $tpl->setVariable("AREA_CHK", in_array($id, $checked) ? 'checked' : '');
                    $tpl->parse("area_items");
                }
                $tpl->parse("tt");
            }
        /*} else {
            UIError("Не указан Сервисный центр", "Ошибка", false, false, false, false, true, true);
        }*/
        UIHeader($tpl);
        $tpl->show();

    }

    function contragents() {
        global $DB;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("contr.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $checked = explode(',', $_REQUEST['contrs_ids']);
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseContr');
        $sql = "SELECT `id`, `contr_title` FROM `list_contr` WHERE (`contr_type` OR `contr_type2` OR `contr_type3` OR `contr_type4`) ORDER BY `contr_title`";
        foreach($dis = $DB->getCell2($sql) as $contr_id=>$contr_title) {
                $tpl->setCurrentBlock('contr');
                $tpl->setVariable('CONTR_ID', $contr_id);
                $tpl->setVariable('CONTR_VAL', $contr_title);
                $tpl->setVariable('CONTR_CHK', in_array($contr_id, $checked) ? 'checked' : '');
                $tpl->parse('contr');

        }
        UIHeader($tpl);
        $tpl->show();
    }

    function areaList() {
        global $DB;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("areas.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $checked = explode(',', $_REQUEST['areas_ids']);
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseWTypes');
        $sql = "SELECT `id`, `title` FROM `list_district` ORDER BY `title`";
        foreach($dis = $DB->getCell2($sql) as $dis_id=>$dis_name) {
            $tpl->setCurrentBlock('ou');
            $tpl->setVariable('DIS_NAME', $dis_name);

            $sql = "SELECT `id`, `title` FROM `list_area` WHERE `dis_id`=".$DB->F($dis_id)." ORDER BY `title`";
            foreach($users = $DB->getCell2($sql) as $area_id=>$area_title) {
                $tpl->setCurrentBlock('area');
                $tpl->setVariable('AREA_ID', $area_id);
                $tpl->setVariable('AREA_VAL', $area_title);
                $tpl->setVariable('AREA_CHK', in_array($area_id, $checked) ? 'checked' : '');
                $tpl->parse('area');
            }

            $tpl->parse('ou');
        }
        $tpl->setCurrentBlock("ou");
        $tpl->setVariable("DIS_NAME", "Без округа");
        $sql = "SELECT `id`, `title` FROM `list_area` WHERE `dis_id`='0' OR `dis_id`='' ORDER BY `title`";
            foreach($users = $DB->getCell2($sql) as $area_id=>$area_title) {
                $tpl->setCurrentBlock('area');
                $tpl->setVariable('AREA_ID', $area_id);
                $tpl->setVariable('AREA_VAL', preg_replace("/\s{2,}/", "", $area_title));
                $tpl->setVariable('AREA_CHK', in_array($area_id, $checked) ? 'checked' : '');
                $tpl->parse('area');
            }

        $tpl->parse("ou");
        UIHeader($tpl);
        $tpl->show();
    }

    function emplListBySC() {
        global $DB;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("emplsc.tmpl.htm");
        $sc_id = @$_REQUEST["sc_id"];
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $checked = explode(',', $_REQUEST['empl_ids']);
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseWTypes');

        if ($sc_id) {
            $tpl->setCurrentBlock('ou');
            $sql = "SELECT `id`, `fio` FROM `users` WHERE `id` IN (SELECT `user_id` FROM `list_empl`) AND `id` NOT IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`!=".$DB->F($sc_id).") AND `active` ORDER BY `fio`";
            foreach($users = $DB->getCell2($sql) as $user_id=>$user_fio) {
                $tpl->setCurrentBlock('user');
                $tpl->setVariable('EMPL_ID', $user_id);
                $tpl->setVariable('EMPL_FIO', $user_fio);
                $tpl->setVariable('EMPL_CHK', in_array($user_id, $checked) ? 'checked' : '');
                $tpl->parse('user');
            }
            $DB->free();
            $tpl->parse('ou');
        } else {
            UIError("Не указан Сервисный центр", "Ошибка", false, false, false, false, true, true);
        }
        UIHeader($tpl);
        $tpl->show();
    }

    function chiefsBySC() {
        global $DB;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("emplsc.tmpl.htm");
        $sc_id = @$_REQUEST["sc_id"];
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $checked = explode(',', $_REQUEST['chief_ids']);
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseWTypes');

        if ($sc_id) {
            $tpl->setCurrentBlock('ou');
//            $sql = "SELECT
//                `id`,
//                `fio`
//            FROM
//                `users`
//            WHERE
//                `id` IN (
//                    SELECT
//                        `user_id`
//                    FROM
//                        `link_sc_user`
//                    WHERE
//                        `sc_id`=".$DB->F($sc_id)."
//                )
//                AND `active`
//            ORDER BY
//                `fio`";
            $sql = "SELECT
                `id`,
                `fio`
            FROM
                `users`
            WHERE
                `active`
            ORDER BY
                `fio`";
            foreach($users = $DB->getCell2($sql) as $user_id=>$user_fio) {
                $tpl->setCurrentBlock('user');
                $tpl->setVariable('EMPL_ID', $user_id);
                $tpl->setVariable('EMPL_FIO', $user_fio);
                $tpl->setVariable('EMPL_CHK', in_array($user_id, $checked) ? 'checked' : '');
                $tpl->parse('user');
            }
            $DB->free();
            $tpl->parse('ou');
        } else {
            UIError("Не указан Сервисный центр", "Ошибка", false, false, false, false, true, true);
        }
        UIHeader($tpl);
        $tpl->show();
    }

    function projects()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("projects.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        $checked = explode(',', $_REQUEST['projects_ids']);
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseUsers');

        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
		$_REQUEST["filter_rpp"] = getcfg('rows_per_page');

        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_status_id'] = $this->getCookie('filter_status_id', $_REQUEST['filter_status_id']);
        $_REQUEST['filter_author_ids'] = $this->getCookie('filter_author_ids', $_REQUEST['filter_author_ids']);
        $_REQUEST['filter_author_fio'] = $this->getCookie('filter_author_fio', $_REQUEST['filter_author_fio']);
		$_REQUEST['filter_ispoln_ids'] = $this->getCookie('filter_ispoln_ids', $_REQUEST['filter_ispoln_ids']);
        $_REQUEST['filter_ispoln_fio'] = $this->getCookie('filter_ispoln_fio', $_REQUEST['filter_ispoln_fio']);
        $_REQUEST["filter_ptype_id"] = $this->getCookie('filter_ptype_id', $_REQUEST['filter_ptype_id']);
        $_REQUEST["filter_zakaz_id"] = $this->getCookie('filter_zakaz_id', $_REQUEST['filter_zakaz_id']);

        $_REQUEST["filter_pc_date_from"] = $this->getCookie('filter_pc_date_from', $_REQUEST['filter_pc_date_from']);
        $_REQUEST["filter_pc_date_to"] = $this->getCookie('filter_pc_date_to', $_REQUEST['filter_pc_date_to']);
        $_REQUEST["filter_pd_date_from"] = $this->getCookie('filter_pd_date_from', $_REQUEST['filter_pd_date_from']);
        $_REQUEST["filter_pd_date_to"] = $this->getCookie('filter_pd_date_to', $_REQUEST['filter_pd_date_to']);

        $tpl->setVariable("PB_ACTION", $_SERVER["REQUEST_URI"]);
        $tpl->setVariable('FILTER_ID', htmlspecialchars($_REQUEST['filter_id']));
        //$tpl->setVariable('FILTER_STATUS_OPTIONS', $this->getStatusOptions($_REQUEST['filter_status_id'], true));
		$sql = "SELECT `id`, `name` FROM `task_status` WHERE `plugin_uid`='projects' ORDER BY `name`";
        $tpl->setVariable('FILTER_STATUS_OPTIONS', array2options($DB->getCell2($sql),$_REQUEST['filter_status_id']));
        $tpl->setVariable('FILTER_AUTHOR_IDS', htmlspecialchars($_REQUEST['filter_author_ids']));
		$tpl->setVariable('FILTER_AUTHOR_FIO', htmlspecialchars($_REQUEST['filter_author_fio']));
		$tpl->setVariable('FILTER_ISPOLN_IDS', htmlspecialchars($_REQUEST['filter_ispoln_ids']));
		$tpl->setVariable('FILTER_ISPOLN_FIO', htmlspecialchars($_REQUEST['filter_ispoln_fio']));
		$tpl->setVariable("FILTER_ZAKAZ_OPTIONS", kontr_plugin::getClientList(intval($_REQUEST["filter_zakaz_id"])));
        $tpl->setVariable("FILTER_PTYPE_OPTIONS", project_types_plugin::getOptions(intval($_REQUEST["filter_ptype_id"])));

        $tpl->setVariable("FILTER_PC_DATE_FROM", $_REQUEST["filter_pc_date_from"]);
        $tpl->setVariable("FILTER_PC_DATE_TO", $_REQUEST["filter_pc_date_to"]);
        $tpl->setVariable("FILTER_PD_DATE_FROM", $_REQUEST["filter_pd_date_from"]);
        $tpl->setVariable("FILTER_PD_DATE_TO", $_REQUEST["filter_pd_date_to"]);


		$filter = "ts.plugin_uid = 'projects' AND tsu.user_id=".$DB->F($USER->getId());
		$order = "ts.lastcmm_date DESC";
		$filter .= $_REQUEST["filter_ptype_id"]>0 ? " AND p.type=".$DB->F($_REQUEST["filter_ptype_id"])." " : "";
		$filter .= $_REQUEST["filter_zakaz_id"]>0 ? " AND p.zakazchik=".$DB->F($_REQUEST["filter_zakaz_id"])." " : "";

        if ($_REQUEST["filter_pc_date_from"]) {
            $_REQUEST["filter_pc_date_from"] = date("Y-m-d", strtotime($_REQUEST["filter_pc_date_from"]));
            $filter .= " AND ts.date_reg >=".$DB->F($_REQUEST["filter_pc_date_from"]);
        }

        if ($_REQUEST["filter_pc_date_to"]) {
            $_REQUEST["filter_pc_date_to"] = date("Y-m-d", strtotime($_REQUEST["filter_pc_date_to"]));
            $filter .= " AND ts.date_reg <=".$DB->F($_REQUEST["filter_pc_date_to"]);
        }

        /*if ($_REQUEST["filter_pd_date_from"]) {
            $_REQUEST["filter_pс_date_from"] = date("Y-m-d", strtotime($_REQUEST["filter_pс_date_from"]));
            $filter .= " AND DATE_FORMAT(p.deadline, '%Y-%m-%d')>=".$DB->F($_REQUEST["filter_pd_date_from"]);
        }

        if ($_REQUEST["filter_pd_date_to"]) {
            $_REQUEST["filter_pс_date_to"] = date("Y-m-d", strtotime($_REQUEST["filter_pс_date_to"]));
            $filter .= " AND DATE_FORMAT(p.deadline, '%Y-%m-%d')<=".$DB->F($_REQUEST["filter_pd_date_to"]);
        } */

        if($_REQUEST['filter_id']) $filter .= " AND ts.id=".$DB->F($_REQUEST['filter_id']);
		if($_REQUEST['filter_status_id']) $filter .= " AND ts.status_id=".$DB->F($_REQUEST['filter_status_id']);
		if($_REQUEST['filter_author_ids'] && ($ids = array_map('intval', explode(',', $_REQUEST['filter_author_ids']))) && sizeof($ids)) $filter .= " AND ts.author_id IN (".implode(',', $ids).")";
		if($_REQUEST['filter_ispoln_ids'] && ($ids = array_map('intval', explode(',', $_REQUEST['filter_ispoln_ids']))) && sizeof($ids)) $filter .= " AND tsu2.user_id IN (".implode(',', $ids).") AND tsu.ispoln";

        $sql = "(SELECT
                    ts.id AS TASK_ID,
					ts.task_title AS TASK_TITLE,
					ts.task_comment AS TASK_COMMENT,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    ts.lastcmm_user_id AS lastuser,
					us.fio AS AUTHOR_FIO,
                    (SELECT `title` FROM `project_types` WHERE `id`=p.type) AS ptype,
                    (SELECT `contr_title` FROM `list_contr` WHERE `id`=p.zakazchik) AS zakazchik,
                    (SELECT `id` FROM `list_contr` WHERE `id`=p.zakazchik) AS zakazchik_id,
                    DATE_FORMAT(p.deadline, '%d.%m.%Y') as deadline
                FROM `task_users` AS tsu
                JOIN `tasks` AS ts ON tsu.task_id=ts.id
                LEFT JOIN `projects` AS p ON p.task_id=ts.id
                LEFT JOIN `task_status` AS st ON ts.status_id=st.id
				LEFT JOIN `users` AS us ON ts.author_id=us.id
				LEFT JOIN `task_users` AS tsu2 ON tsu2.task_id=ts.id
                WHERE $filter AND p.deadline<".$DB->F(date("Y-m-d"))."
				GROUP BY ts.id
                ORDER BY $order) UNION (SELECT
                    ts.id AS TASK_ID,
					ts.task_title AS TASK_TITLE,
					ts.task_comment AS TASK_COMMENT,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    ts.lastcmm_user_id AS lastuser,
					us.fio AS AUTHOR_FIO,
                    (SELECT `title` FROM `project_types` WHERE `id`=p.type) AS ptype,
                    (SELECT `contr_title` FROM `list_contr` WHERE `id`=p.zakazchik) AS zakazchik,
                    (SELECT `id` FROM `list_contr` WHERE `id`=p.zakazchik) AS zakazchik_id,
                    DATE_FORMAT(p.deadline, '%d.%m.%Y') as deadline
                FROM `task_users` AS tsu
                JOIN `tasks` AS ts ON tsu.task_id=ts.id
                LEFT JOIN `projects` AS p ON p.task_id=ts.id
                LEFT JOIN `task_status` AS st ON ts.status_id=st.id
				LEFT JOIN `users` AS us ON ts.author_id=us.id
				LEFT JOIN `task_users` AS tsu2 ON tsu2.task_id=ts.id
                WHERE $filter
				GROUP BY ts.id
                ORDER BY p.deadline ASC, $order)";
                //LIMIT {$_REQUEST['filter_start']},{$_REQUEST["filter_rpp"]}";
        //die($sql);
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error()." ".$sql, "Ошибка", false, false, false, false, true, true);
        $total = $DB->getFoundRows();
        while($a = $DB->fetch(true)) {
            $lastuser = adm_users_plugin::getUser($a["lastuser"]);
            $a["LASTUSER"] = $lastuser["fio"];
			$a['PROJECT_CHK'] = in_array($a['TASK_ID'], $checked) ? 'checked' : '';
            $a['HREF'] = $this->getLink('viewtask', "task_id={$a['TASK_ID']}");
			$a['ISPOLN_FIO'] = Task::getTaskIspoln($a['TASK_ID'], ', ');
			$a["ZAKAZ_NAME"] = $a["zakazchik"] ? $a["zakazchik"] : "&mdash;";
            $a["PTYPE"] = $a["ptype"] ? $a["ptype"] : "&mdash;";
            $a["DEADLINE"] = ($a["deadline"] && $a["deadline"] != "01.01.1970" && $a["deadline"] != "00.00.0000") ? $a["deadline"] : false;
            if (strtotime($a["DEADLINE"]) && strtotime($a["DEADLINE"])<strtotime(date("d.m.Y"))) {
                $tpl->setVariable("OUTDATED", "outdated");
            }
            $tpl->setCurrentBlock('row');
            $tpl->setVariable($a);
            $tpl->parse('row');
        }
        $DB->free();

        //$tpl->setVariable('TOTAL', $total);
        //$tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"], $total, "#start-%s"));


        UIHeader($tpl);
        $tpl->show();
    }

    function bills() {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile("bills.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        $checked = explode(',', $_REQUEST['bills_ids']);
        $tpl->setVariable('APPLY_FN', isset($_REQUEST['applyfn']) ? $_REQUEST['applyfn'] : 'onAfterBrowseUsers');
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
		$_REQUEST["filter_rpp"] = getcfg('rows_per_page');

        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_status_id'] = $this->getCookie('filter_status_id', $_REQUEST['filter_status_id']);
        $_REQUEST['filter_author_ids'] = $this->getCookie('filter_author_ids', $_REQUEST['filter_author_ids']);
        $_REQUEST['filter_author_fio'] = $this->getCookie('filter_author_fio', $_REQUEST['filter_author_fio']);
		$_REQUEST['filter_ispoln_ids'] = $this->getCookie('filter_ispoln_ids', $_REQUEST['filter_ispoln_ids']);
        $_REQUEST['filter_ispoln_fio'] = $this->getCookie('filter_ispoln_fio', $_REQUEST['filter_ispoln_fio']);
        $_REQUEST['filter_poluch'] = $this->getCookie('filter_poluch', $_REQUEST['filter_poluch']);
        $_REQUEST['filter_plat_id'] = $this->getCookie('filter_plat_id', $_REQUEST['filter_plat_id']);


        $tpl->setVariable("BB_ACTION", $_SERVER["REQUEST_URI"]);
        $tpl->setVariable('FILTER_ID', htmlspecialchars($_REQUEST['filter_id']));
        $sql = "SELECT `id`, `name` FROM `task_status` WHERE `plugin_uid`='bills' ORDER BY `name`";
        $tpl->setVariable('FILTER_STATUS_OPTIONS', array2options($DB->getCell2($sql),$_REQUEST['filter_status_id']));
		$tpl->setVariable('FILTER_AUTHOR_IDS', htmlspecialchars($_REQUEST['filter_author_ids']));
		$tpl->setVariable('FILTER_AUTHOR_FIO', htmlspecialchars($_REQUEST['filter_author_fio']));
		$tpl->setVariable('FILTER_ISPOLN_IDS', htmlspecialchars($_REQUEST['filter_ispoln_ids']));
		$tpl->setVariable('FILTER_ISPOLN_FIO', htmlspecialchars($_REQUEST['filter_ispoln_fio']));
		$tpl->setVariable("FILTER_POLUCH", kontr_plugin::getSupplList($_REQUEST['filter_poluch']));
        $tpl->setVariable('FILTER_PLAT_OPTIONS', bills_plugin::getPlatOptions($_REQUEST['filter_plat_id']));

		$filter = "ts.plugin_uid = 'bills' AND tsu.user_id=".$DB->F($USER->getId());
		$order = "ts.lastcmm_date DESC";

		if($_REQUEST['filter_id']) $filter .= " AND ts.id=".$DB->F($_REQUEST['filter_id']);
		if($_REQUEST['filter_status_id']) $filter .= " AND ts.status_id=".$DB->F($_REQUEST['filter_status_id']);
		if($_REQUEST['filter_author_ids'] && ($ids = array_map('intval', explode(',', $_REQUEST['filter_author_ids']))) && sizeof($ids)) $filter .= " AND ts.author_id IN (".implode(',', $ids).")";
		if($_REQUEST['filter_ispoln_ids'] && ($ids = array_map('intval', explode(',', $_REQUEST['filter_ispoln_ids']))) && sizeof($ids)) $filter .= " AND tsu2.user_id IN (".implode(',', $ids).") AND tsu.ispoln";
		if($_REQUEST['filter_poluch']) $filter .= " AND bl.poluch LIKE '%".addslashes($_REQUEST['filter_poluch'])."%'";
		if($_REQUEST['filter_plat_id']) $filter .= " AND bl.plat_id=".$DB->F($_REQUEST['filter_plat_id']);

        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
					bl.poluch AS POLUCH,
					bl.num AS NUMBER,
					ts.task_comment AS TASK_COMMENT,
					bl.sum AS SUM,
                    pl.name AS PLAT,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
					DATE_FORMAT(bl.date_srok, '%d.%m.%Y') AS DATE_SROK
                FROM `task_users` AS tsu
                JOIN `tasks` AS ts ON tsu.task_id=ts.id
				JOIN `bills` AS bl ON ts.id=bl.task_id
                LEFT JOIN `task_status` AS st ON ts.status_id=st.id
				LEFT JOIN `bills_plat` AS pl ON bl.plat_id=pl.id
				LEFT JOIN `task_users` AS tsu2 ON tsu2.task_id=ts.id
                WHERE $filter
				GROUP BY ts.id
                ORDER BY $order";
                //LIMIT {$_REQUEST['filter_start']},{$_REQUEST["filter_rpp"]}";

        $DB->query($sql);
        $total = $DB->getFoundRows();

		while($a = $DB->fetch(true)) {
			$a['BILL_CHK'] = in_array($a['TASK_ID'], $checked) ? 'checked' : '';
            $a['HREF'] = $this->getLink('viewbill', "task_id={$a['TASK_ID']}");
			$a['ISPOLN_FIO'] = Task::getTaskIspoln($a['TASK_ID'], ', ');
			$a["SUM"] = number_format($a["SUM"], 2, ".", " ");
            $tpl->setCurrentBlock('row');
            $tpl->setVariable($a);
            $tpl->parse('row');
        }
        $DB->free();

        //$tpl->setVariable('TOTAL', $total);
        //$tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"], $total, "#start-%s"));

        UIHeader($tpl);
        $tpl->show();
    }


}
