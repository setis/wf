<?php
/**
 * Plugin Header
 * @author kblp
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Продажи";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Заявки ТМ";
    $PLUGINS[$plugin_uid]['events']['viewticket'] = "Просмотр заявки ТМ";
    $PLUGINS[$plugin_uid]['events']['vieworderdoc'] = "Печать заказ-наряда";
    $PLUGINS[$plugin_uid]['events']['viewlist_export'] = "Заявки ТМ. Выгрузка.";
    $PLUGINS[$plugin_uid]['events']['checkticketexists_ajax'] = "Проверка заявки на существование - Вид работ, Адрес";
    $PLUGINS[$plugin_uid]['events']['getwtypelist'] = "Список видов работ";
    $PLUGINS[$plugin_uid]['events']['getagrlist'] = "Список договоров";
    $PLUGINS[$plugin_uid]['events']['get_services_for_sale'] = "Список услуг на продажу в модуле ТМ";
    $PLUGINS[$plugin_uid]['events']['queryStatus'] = "Запрос текущего статуса Заявки";
    $PLUGINS[$plugin_uid]['events']['getQuestionnaireForm_ajax'] = "Анкета опроса заказчика";
    $PLUGINS[$plugin_uid]['events']['load_equipment'] = "Список оборудования";
    $PLUGINS[$plugin_uid]['events']['load_tarifs'] = "Список тарифов";
}

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['newticket'] = "Новая заявка ТМ";
    $PLUGINS[$plugin_uid]['events']['saveticket'] = "Сохранение зявки";
    $PLUGINS[$plugin_uid]['events']['updateticket'] = "Обновление зявки";
    $PLUGINS[$plugin_uid]['events']['removefromgfx'] = "Удаление из графика";
    $PLUGINS[$plugin_uid]['events']['deleteticket'] = "Удаление заявки";
    $PLUGINS[$plugin_uid]['events']['updateContrDataA'] = "Обновление типа заявки ";
    $PLUGINS[$plugin_uid]['events']['acceptticket_ajax'] = "Принятие заявки техником";
    $PLUGINS[$plugin_uid]['events']['rejectticket_ajax'] = "Отказ техника от заявки";
    $PLUGINS[$plugin_uid]['events']['closeticket'] = "Закрытие заявки";

    $PLUGINS[$plugin_uid]['events']['calcticket'] = "Расчет заявки";
    $PLUGINS[$plugin_uid]['events']['getcurrentcalc_ajax'] = "Загрузка расчета заявки";
    $PLUGINS[$plugin_uid]['events']['checktaskstatus_ajax'] = "Проверка статуса заявки для перевода в статус Сдача отчета";
    $PLUGINS[$plugin_uid]['events']['return_tmc_from_ticktet'] = "Возврат ТМЦ по ГП";
    $PLUGINS[$plugin_uid]['events']['delete_returned_tmc_from_ticket'] = "Откат (отмена списания) ТМЦ по ГП";
}
?>
