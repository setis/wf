<?php
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use classes\tickets\TelemarketingTicket;
use classes\User;
use Symfony\Component\HttpFoundation\Request;
use models\Task as TaskEntity;

class tm_module_plugin extends Plugin
{
    protected $extra_keys = array(
        'task_id',
        'internet_tarif_id',
        'internet_ap',
        'internet_speed',
        'internet_equipment',
        'ctv_tarif_id',
        'ctv_ap',
        'ctv_channel',
        'ctv_equipment',
        'ctv_prov',
        'internet_plus_tv_tarif_id',
        'internet_plus_tv_ap',
        'internet_plus_tv_speed',
        'internet_plus_tv_channel',
        'internet_plus_tv_equipment1',
        'internet_plus_tv_equipment2',
        'phone_tarif_id',
        'phone_ap',
        'phone_minutes',
        'vas1_name',
        'vas1_value',
        'vas2_name',
        'vas2_value',
        'vas3_name',
        'vas3_value',
        'ispoln_id',
        'service_for_sale_id',
        'sold_equipment',
        'tarif_info',
        'vas_info',
        'subtype',
        'sales_channel',
        'clnttnum',
        'current_operator',
        'current_services',
        'current_ap',
        'current_comments'
    );

    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function viewlist()
    {
        global $DB, $USER;

        $g_user_id = $_SESSION["user"]["id"];
        $sort_ids = array(
            1 => "Дата изменения",
            2 => "Дата создания",
            3 => "Дата в графике",
            4 => "Техник"
        );
        $sort_sql = array(
            1 => "ts.lastcmm_date",
            2 => "ts.date_reg",
            3 => "gfx.c_date",
            4 => "fio"
        );
        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );
        $rpp = array(
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250",
            500 => "500"
        );
        $LKUser = adm_users_plugin::isLKUser($USER->getId());
        $LKContrID=0;
        if ($LKUser) {
            $LKContrID = adm_users_plugin::getLKUserContrID($USER->getId());
            $tpl_add = "_lk";
        } else {
            $tpl_add = "";
        }
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/list$tpl_add.tmpl.htm");
        else
            $tpl->loadTemplatefile("list$tpl_add.tmpl.htm");
        if ($USER->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }
        if (!$USER->isTech($USER->getId()) || ($USER->isTech($USER->getId())) && $USER->isChiefTech($USER->getId())) {
            $tpl->setCurrentBlock("notistech");

            // LK
            if ($LKUser) {
                $tpl->setVariable("TEMP_ISROSTEL1", "style='display:none;'");
            }
            // --------
            $tpl->setVariable("PLUGIN1_UID", $this->getUID());
            $tpl->parse("notistech");

        }
        // LK
        if ($LKUser) {
            $tpl->setVariable("TEMP_ISROSTEL", "style='display:none;'");
        }
        // --------
        $filter = "ts.plugin_uid=" . $DB->F($this->getUID());
        $isTech = User::isTech($g_user_id);
        $isChiefTech = User::isChiefTech($g_user_id);
        if (!$LKUser) {
            if (($isTech || $isChiefTech) && !$USER->checkAccess("fullaccesstoallsc")) {
                if ($isChiefTech) {
                    $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $isChiefTech);
                    if ($_REQUEST['filter_sc_id']) $filter .= " AND tt.sc_id=" . $DB->F($_REQUEST['filter_sc_id']);
                    if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']) . " OR tu.user_id=" . $DB->F($_REQUEST['filter_empl_id']) . ")";
                    $tpl->setCurrentBlock("techlist");
                    if ($_REQUEST['filter_empl_id']) {
                        $tpl->setVariable("FILTER_EMPL_ID", $_REQUEST['filter_empl_id']);
                        $usrName = adm_users_plugin::getUser($_REQUEST["filter_empl_id"]);
                        $tpl->setVariable("FILTER_EMPL_NAME", $usrName["fio"]);
                    }
                    $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
                    $tpl->parse("techlist");
                } elseif ($isTech) {
                    $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $g_user_id);
                    $virtAlias = adm_empl_plugin::isVirtual($g_user_id);
                    if ($virtAlias) {
                        if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($g_user_id) . " OR tu.user_id=" . $DB->F($g_user_id) . " OR gfx.empl_id=" . $DB->F($virtAlias) . " OR tu.user_id=" . $DB->F($virtAlias) . ")";
                    } else {
                        if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($g_user_id) . " OR tu.user_id=" . $DB->F($g_user_id) . ")";

                    }

                }
            } else {
                $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $_REQUEST['filter_sc_id']);
                $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);
                if ($_REQUEST['filter_sc_id'] == "-1")
                    $filter .= " AND ifnull(tt.sc_id,0)=0";
                elseif ($_REQUEST['filter_sc_id']) {
                    $filter .= " AND tt.sc_id=" . $DB->F($_REQUEST['filter_sc_id']);
                }
                if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']) . " OR tu.user_id=" . $DB->F($_REQUEST['filter_empl_id']) . ")";
                $tpl->setCurrentBlock("sclist");
                if ($LKUser) {
                    $tpl->setVariable("TEMP_ISROSTEL2", "style='display:none;'");
                }
                $tpl->setVariable('FILTER_SC_OPTIONS', $this->getSCOptions($_REQUEST['filter_sc_id']));
                $tpl->parse("sclist");
                $tpl->setCurrentBlock("techlist");
                if ($LKUser) {
                    $tpl->setVariable("TEMP_ISROSTEL3", "style='display:none;'");
                }
                if ($_REQUEST['filter_empl_id']) {
                    $tpl->setVariable("FILTER_EMPL_ID", $_REQUEST['filter_empl_id']);
                    $usrName = adm_users_plugin::getUser($_REQUEST["filter_empl_id"]);
                    $tpl->setVariable("FILTER_EMPL_NAME", $usrName["fio"]);
                }
                $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
                $tpl->parse("techlist");
            }
        }

        $_REQUEST["date_gfx"] = $this->getCookie('date_gfx', $_REQUEST['date_gfx']);
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_cnt_id'] = $this->getCookie('filter_cnt_id', $_REQUEST['filter_cnt_id']);
        $_REQUEST['filter_type'] = $this->getCookie('filter_type', $_REQUEST['filter_type']);
        $_REQUEST['filter_clnt_type'] = $this->getCookie('filter_clnt_type', $_REQUEST['filter_clnt_type']);
        $_REQUEST['filter_status_id'] = $this->getCookie('filter_status_id', $_REQUEST['filter_status_id']);
        $_REQUEST['filter_ods_id'] = $this->getCookie('filter_ods_id', $_REQUEST['filter_ods_id']);
        $_REQUEST['filter_area_id'] = $this->getCookie('filter_area_id', $_REQUEST['filter_area_id']);
        $_REQUEST['filter_ctgrnum_id'] = $this->getCookie('filter_ctgrnum_id', $_REQUEST['filter_ctgrnum_id']);
        $_REQUEST['filter_addr'] = $this->getCookie('filter_addr', $_REQUEST['filter_addr']);
        $_REQUEST['filter_addr_flat'] = $this->getCookie('filter_addr_flat', $_REQUEST['filter_addr_flat']);
        if ($_REQUEST["timefrom"] != '') {
            $_REQUEST["timefrom"] = $this->getCookie('timefrom', $_REQUEST["timefrom"]);
        }
        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ? $_REQUEST['filter_sort_order'] : key($sort_order));
        if (!isset($_REQUEST["filter_rpp"])) $_REQUEST["filter_rpp"] = $USER->getRpp();
        if ($_REQUEST["filter_rpp"] != $USER->getRpp()) {
            $sql = "UPDATE `users` SET `rpp`=" . $DB->F($_REQUEST["filter_rpp"]) . " WHERE `id`=" . $DB->F($USER->getId()) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
        } else {
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        }
        if ($_REQUEST['date_gfx']) $filter .= " AND gfx.c_date=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['date_gfx'])));
        if ($_REQUEST['filter_id']) $filter .= " AND ts.id=" . $DB->F($_REQUEST['filter_id']);
        if ($_REQUEST['filter_cnt_id']) $filter .= " AND tt.cnt_id=" . $DB->F($_REQUEST['filter_cnt_id']);
        if ($_REQUEST['filter_type']) $filter .= " AND tt.task_type=" . $DB->F($_REQUEST['filter_type']);
        if ($_REQUEST['filter_clnt_type']) $filter .= " AND tt.clnt_type=" . $DB->F($_REQUEST['filter_clnt_type']);
        if ($_REQUEST['filter_status_id']) $filter .= " AND ts.status_id=" . $DB->F($_REQUEST['filter_status_id']);
        if ($_REQUEST['filter_ods_id']) $filter .= " AND ods.id=" . $DB->F($_REQUEST['filter_ods_id']);
        if ($_REQUEST['filter_area_id'] == "-1")
            $filter .= " AND addr.area_id=0";
        elseif ($_REQUEST['filter_area_id']) {
            $filter .= " AND addr.area_id=" . $DB->F($_REQUEST['filter_area_id']);
        }
        if ($_REQUEST['filter_ctgrnum_id']) $filter .= " AND lower(tt.clnttnum) LIKE " . $DB->F("%" . strtolower($_REQUEST['filter_ctgrnum_id']) . "%");
        if ($_REQUEST['filter_addr']) $filter .= "AND tt.dom_id IN (SELECT la.id FROM `list_addr` AS la, `kladr_street` AS ks WHERE
                                                 LEFT(la.kladrcode, 15) = LEFT(ks.code, 15) AND ks.name LIKE " . $DB->F($_REQUEST['filter_addr'] . "%") . ")";
        if ($_REQUEST["filter_addr_flat"]) $filter .= " AND tt.kv LIKE " . $DB->F(strtolower($_REQUEST['filter_addr_flat']) . "%");

        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];

        $statusList = adm_statuses_plugin::getFullList("tm_module");
        if ($statusList) {
            $tpl->setCurrentBlock("sl-items");
            foreach ($statusList as $st_item) {
                $tpl->setCurrentBlock("statuslist");
                if ($_REQUEST['filter_status_id'] == $st_item["id"])
                    $tpl->setVariable("FF_SELECTED", 'ffstisel');
                $tpl->setVariable("FF_STATUS_ID", $st_item["id"]);
                $tpl->setVariable("FF_STATUS", $st_item["name"]);
                $tpl->setVariable("FF_STATUS_COLOR", $st_item["color"]);
                $tpl->parse("statuslist");
            }
            if (!$_REQUEST['filter_status_id'])
                $tpl->setVariable("FF_ALL_SELECTED", "ffstisel");
            $tpl->parse("sl-items");

        }
        if (($isChiefTech && $isTech) || !$isTech) {

            $_REQUEST['filter_client_phone'] = $this->getCookie('filter_client_phone', $_REQUEST['filter_client_phone']);
            $tpl->setCurrentBlock("is_clnt_phone");
            $tpl->setVariable("FILTER_CLIENT_PHONE", $_REQUEST['filter_client_phone'] ? $_REQUEST['filter_client_phone'] : "");
            $tpl->parse("is_clnt_phone");
            if ($_REQUEST['filter_client_phone']) {
                $phone = preg_replace("/(\D+)/", "", $_REQUEST["filter_client_phone"]);
                $filter .= " AND (`clnt_tel1` LIKE '%" . $phone . "%' OR `clnt_tel2` LIKE '%" . $phone . "%') ";
            }
        } else {
            $_REQUEST['filter_client_phone'] = false;
        }
        $tpl->setVariable('FILTER_ID', htmlspecialchars($_REQUEST['filter_id']));
        $tpl->setVariable("FILTER_DATE_GFX", $_REQUEST['date_gfx'] ? $_REQUEST['date_gfx'] : "");
        $tpl->setVariable("FILTER_ADDR", $_REQUEST['filter_addr'] ? $_REQUEST['filter_addr'] : "");
        $tpl->setVariable("FILTER_ADDR_FLAT", $_REQUEST['filter_addr_flat'] ? $_REQUEST['filter_addr_flat'] : "");
        $tpl->setVariable('FILTER_CNT_OPTIONS', $this->getContrOptions($_REQUEST['filter_cnt_id']));
        $tpl->setVariable('FILTER_TYPE_OPTIONS', $this->getTypeOptions($_REQUEST['filter_type']));
        $tpl->setVariable('FILTER_CLNT_TYPE_SEL_' . $_REQUEST['filter_clnt_type'], 'selected');
        $tpl->setVariable('FILTER_STATUS_OPTIONS', $this->getStatusOptions($_REQUEST['filter_status_id']));
        $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
        $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
        if (($isChiefTech || $isTech) && !$USER->checkAccess("fullaccesstoallsc")) {
            $sc_id = $isChiefTech ? $isChiefTech : User::getScIdByUserId($g_user_id);
            $areaData = $DB->getCell2("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `title`;");
            $areaData["-1"] = "-- не указан";
            $tpl->setVariable('FILTER_AREA_OPTIONS', array2options($areaData, $_REQUEST['filter_area_id']));
        } else {
            if ($_REQUEST['filter_sc_id'] != "0") {
                $areaData = $DB->getCell2("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT area_id FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($_REQUEST["filter_sc_id"]) . ") ORDER BY `title`;");
            } else {
                $areaData = $DB->getCell2("SELECT `id`,`title` FROM `list_area` ORDER BY `title`;");
            }
            $areaData["-1"] = "-- не указан";

            $tpl->setVariable('FILTER_AREA_OPTIONS', array2options($areaData, $_REQUEST['filter_area_id']));
        }
        $tpl->setVariable('CTGRNUM_ID', $_REQUEST['filter_ctgrnum_id']);
        $tpl->setVariable('FILTER_ROWS_PER_PAGE', array2options($rpp, $_REQUEST['filter_rpp']));
        // ROSTEL

        if ($LKUser) {
            $filter .= " AND tt.cnt_id=" . $DB->F($LKContrID) . " ";
        }
        // --------
        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
                    ca.contr_title AS CONTR,
                    typ.title AS TYPE,
                    tt.dom_id,
                    tt.clnt_org_name,
                    tt.clnt_fio,
                    tt.clnt_type,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    (CASE WHEN COALESCE(users.fio,'')<>'' THEN users.fio ELSE users1.fio END) as fio,
                    addr.name, 
                    addr.full_address, 
                    addr.is_not_recognized, 
                    gfx.c_date, gfx.startTime, tt.clnttnum, tt.kv, st.tag, ct.contr_ticket_id, ts.date_fn, st.tag,
                    tt.disallowgfx, tt.asrzerror, tt.wfmplanned, tt.noagrmgts, tt.asrzok, tt.setted
                FROM
                    `tasks` AS ts
                    LEFT JOIN `tickets` AS tt ON ts.id=tt.task_id
                    LEFT JOIN `list_contr` AS ca ON tt.cnt_id=ca.id
                    LEFT JOIN (SELECT * FROM `task_users` WHERE `ispoln`=1 GROUP BY task_users.id, task_id ) AS tu ON tu.task_id=ts.id
                    LEFT JOIN `task_types` AS typ ON tt.task_type=typ.id
                    LEFT JOIN `task_status` AS st ON ts.status_id=st.id
                    LEFT JOIN `gfx` AS gfx ON gfx.task_id=ts.id
                    LEFT JOIN `list_addr` AS addr ON addr.id=tt.dom_id
                    LEFT JOIN `users` AS users ON users.id=gfx.empl_id
                    LEFT JOIN `users` AS users1 ON users1.id=tu.user_id
                    LEFT JOIN `list_ods` AS ods ON ods.id=addr.ods_id
                    LEFT JOIN `contr_tickets` AS ct ON ct.task_id=ts.id
                WHERE $filter
                  and case when ifnull(tt.sc_id,0) = 0 then 1
                           when exists (select * from user_sc_access usa where usa.user_id = ".$USER->getId()." and usa.sc_id = tt.sc_id) then 1
                           else 0
                       end = 1
                GROUP BY ts.id, users.fio, users1.fio, gfx.c_date, gfx.startTime
                ORDER BY
                    $order
                LIMIT {$_REQUEST['filter_start']}," . intval($_REQUEST["filter_rpp"]);


        $DB->query($sql);
        $total = $DB->getFoundRows();
        if ($DB->errno) UIError($DB->error());
        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("delticket-head");
        }

        while ($a = $DB->fetch(true)) {

            /*$resp = $DB->getCell("SELECT user.fio FROM `task_users` AS tu LEFT JOIN `users` AS user ON user.id=tu.user_id WHERE tu.task_id=".$DB->F($a['TASK_ID'])." AND tu.ispoln ;");
            if (!$resp) {*/
            $resp_user = $a['fio'] ? "<i>" . $a['fio'] . "</i>" : false;
            /*} else {
                $r = false;
                foreach($resp as $item){
                    $r[] ="$item";
                }
                $resp_user = implode(", <br/ >", $r);
            }*/
            if ($a["disallowgfx"]) {
                $tpl->setVariable("MGTS_STYLE", "style='background:#FF0000;'");
                $tpl->setVariable("MGTS_INTEGR", "<br />не назначена");
            } else {
                if ($a["asrzerror"]) {
                    $tpl->setVariable("MGTS_STYLE", "style='background:#964E4E;'");
                    $tpl->setVariable("MGTS_INTEGR", "<br />Несоответствие");
                } else {
                    if ($a["noagrmgts"]) {
                        $tpl->setVariable("MGTS_STYLE", "style='background:#FC7474;'");
                        $tpl->setVariable("MGTS_INTEGR", "<br />Не согласовано");
                    } else {
                        if ($a["setted"]) {
                            $tpl->setVariable("MGTS_STYLE", "style='background:#FFF700;'");
                            $tpl->setVariable("MGTS_INTEGR", "<br />Назначена");
                        }
                    }
                }
            }

            if ($a['clnt_type'] == User::CLIENT_TYPE_PHYS) $a['CLIENT'] = $a['clnt_fio'];
            elseif ($a['clnt_type'] == User::CLIENT_TYPE_YUR) $a['CLIENT'] = $a['clnt_org_name'];
            else $a['CLIENT'] = $a['clnt_org_name'] . ', ' . $a['clnt_fio'];
            /*if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "decodeFullKLADR")) {
                $addr = new addr_interface_plugin();
                $addrString = $addr->decodeFullKLADR($a['kladrcode']);
            } */
            if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "decodeFullKLADR")) {
                $addr = new addr_interface_plugin();
                $addrString = $addr->decodeFullKLADRPartial($a['kladrcode']);
            }
            $a["CONTR_TASK_ID"] = $a['contr_ticket_id'] ? $a['contr_ticket_id'] : ($a["clnttnum"] ? $a["clnttnum"] : "<center style='text-decoration:none !important;'>&mdash;</center>");
            $a["HREF_ORDER"] = $this->getLink('vieworderdoc', "id={$a['TASK_ID']}");
            $a['HREF'] = $this->getLink('viewticket', "task_id={$a['TASK_ID']}");

            //JN Address by one row
            if (!empty($a['full_address'])) {
                if($a['is_not_recognized']) {
                    $a['FULL_ADDRESS'] = $a['full_address'] . ' <sup>*</sup>';
                } else {
                    $a['FULL_ADDRESS'] = $a['full_address'];
                }

                $a['ADDR_FLAT'] = $a["kv"];
                $tpl->parse("kladr_addr");
            } else {
                $tpl->setCurrentBlock("no_kladr");
                $a['ADDR_NO_KLADR'] = "<font color=#FF0000>Адрес не распознан</font>";
                $tpl->parse("no_kladr");
            }
            if (preg_match("/otlozh/", $a['tag']) && $a['date_fn']) {
                $a['GFXTIME'] = rudate("d M Y", strtotime($a['date_fn']));
                $a['STATUS_NAME'] .= " до";
                if (date("Y-m-d", strtotime($a['date_fn'])) < date("Y-m-d")) $a['BLINK'] = "blink";
                //die($a['date_fn']);

            } elseif ($a['c_date'] && $a['startTime']) {
                $startHour = (strlen(floor($a['startTime'] / 60)) == 1) ? "0" . (floor($a['startTime'] / 60)) : floor($a['startTime'] / 60);
                $startMin = (strlen(floor($a['startTime'] % 60)) == 1) ? "0" . (floor($a['startTime'] % 60)) : floor($a['startTime'] % 60);
                if (preg_match("/grafik/", $a['tag'])) {
                    $a['GFXTIME'] = rudate("d M Y", strtotime($a['c_date'])) . " " . $startHour . ":" . $startMin;
                }
            }
            $a['USERFIO'] = $resp_user ? $resp_user : "<center>&mdash;</center>";
            $t = new TelemarketingTicket($a["TASK_ID"]);
            $a['AREA'] = $t->getArea() ? $t->getArea() : "<center>&mdash;</center>";
            if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getSC")) {
                $scO = new adm_sc_plugin();
                $sc = $scO->getSC($t->getSCId());
            }
            $a['SC'] = $sc['title'] ? $sc['title'] : "<center>&mdash;</center>";
            $a['CONTR_ID'] = $clnttnum ? $clnttnum : "<center>&mdash;</center>";
            $tpl->setCurrentBlock('row');
            if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
                $tpl->setCurrentBlock("deleteticket");
                $tpl->setVariable("DELETEHREF", $this->getLink('deleteticket', "task_id={$a['TASK_ID']}"));
                $tpl->parse("deleteticket");
            }
            $a["LASTCOMMENTVALUE"] = $t->getLastComment(true);
            $tpl->setVariable($a);
            $tpl->parse("row");
        }
        $DB->free();
        //$DB->query($sql);
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"], $total, "#start-%s"));
        UIHeader($tpl);
        $tpl->show();
        $DB->free();

    }

    function viewlist_export()
    {
        global $USER;
        $LKUser = adm_users_plugin::isLKUser($USER->getId());
        if ($LKUser) UIError("В доступе отказано!");
        header("Content-type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=telemarketing_export_" . date("H_i_d-M-Y") . ".xls");

        global $DB, $USER, $plugin_event, $plugin;;

        $g_user_id = $_SESSION["user"]["id"];
        $sort_ids = array(
            1 => "Дата изменения",
            2 => "Дата создания",
            3 => "Дата в графике",
            4 => "Техник"
        );
        $sort_sql = array(
            1 => "ts.lastcmm_date",
            2 => "ts.date_reg",
            3 => "1",
            4 => "1"
        );
        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/export.tmpl.htm");
        $filter = "ts.plugin_uid=" . $DB->F($this->getUID());
        $isTech = User::isTech($g_user_id);
        $isChiefTech = User::isChiefTech($g_user_id);
        $tpl->setVariable('EVENT_TITLE', getEventTitle($plugin->getUID(), $plugin_event));
        if (($isTech || $isChiefTech) && !$USER->checkAccess("fullaccesstoallsc")) {
            if ($isChiefTech) {
                $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $isChiefTech);
                if ($_REQUEST['filter_sc_id']) $filter .= " AND tt.sc_id=" . $DB->F($_REQUEST['filter_sc_id']);
                if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']) . " OR tu.user_id=" . $DB->F($_REQUEST['filter_empl_id']) . ")";
                $tpl->setCurrentBlock("techlist");
                $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
                $tpl->parse("techlist");
            } elseif ($isTech) {
                $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $g_user_id);
                if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($g_user_id) . " OR tu.user_id=" . $DB->F($g_user_id) . ")";
            }
        } else {
            $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $_REQUEST['filter_sc_id']);
            $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);
            if ($_REQUEST['filter_sc_id']) $filter .= " AND tt.sc_id=" . $DB->F($_REQUEST['filter_sc_id']);
            if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']) . " OR tu.user_id=" . $DB->F($_REQUEST['filter_empl_id']) . ")";
            $tpl->setCurrentBlock("sclist");
            $tpl->setVariable('FILTER_SC_OPTIONS', $this->getSCOptions($_REQUEST['filter_sc_id']));
            $tpl->parse("sclist");
            $tpl->setCurrentBlock("techlist");
            $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
            $tpl->parse("techlist");
        }
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_cnt_id'] = $this->getCookie('filter_cnt_id', $_REQUEST['filter_cnt_id']);
        $_REQUEST['filter_type'] = $this->getCookie('filter_type', $_REQUEST['filter_type']);
        $_REQUEST['filter_clnt_type'] = $this->getCookie('filter_clnt_type', $_REQUEST['filter_clnt_type']);
        $_REQUEST['filter_status_id'] = $this->getCookie('filter_status_id', $_REQUEST['filter_status_id']);
        $_REQUEST['filter_ods_id'] = $this->getCookie('filter_ods_id', $_REQUEST['filter_ods_id']);
        $_REQUEST['filter_area_id'] = $this->getCookie('filter_area_id', $_REQUEST['filter_area_id']);
        $_REQUEST['filter_ctgrnum_id'] = $this->getCookie('filter_ctgrnum_id', $_REQUEST['filter_ctgrnum_id']);
        $_REQUEST['filter_addr'] = $this->getCookie('filter_addr', $_REQUEST['filter_addr']);
        $_REQUEST['filter_addr_flat'] = $this->getCookie('filter_addr_flat', $_REQUEST['filter_addr_flat']);
        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ? $_REQUEST['filter_sort_order'] : key($sort_order));
        $_REQUEST["date_gfx"] = $this->getCookie('date_gfx', $_REQUEST['date_gfx']);


        if ($_REQUEST['date_gfx']) $filter .= " AND gfx.c_date=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['date_gfx'])));
        if ($_REQUEST['filter_id']) $filter .= " AND ts.id=" . $DB->F($_REQUEST['filter_id']);
        if ($_REQUEST['filter_cnt_id']) $filter .= " AND tt.cnt_id=" . $DB->F($_REQUEST['filter_cnt_id']);
        if ($_REQUEST['filter_type']) $filter .= " AND tt.task_type=" . $DB->F($_REQUEST['filter_type']);
        if ($_REQUEST['filter_clnt_type']) $filter .= " AND tt.clnt_type=" . $DB->F($_REQUEST['filter_clnt_type']);
        if ($_REQUEST['filter_status_id']) $filter .= " AND ts.status_id=" . $DB->F($_REQUEST['filter_status_id']);
        if ($_REQUEST['filter_ods_id']) $filter .= " AND ods.id=" . $DB->F($_REQUEST['filter_ods_id']);
        if ($_REQUEST['filter_area_id']) $filter .= " AND addr.area_id=" . $DB->F($_REQUEST['filter_area_id']);
        if ($_REQUEST['filter_ctgrnum_id']) $filter .= " AND lower(tt.clnttnum) LIKE " . $DB->F("%" . strtolower($_REQUEST['filter_ctgrnum_id']) . "%");

        //JN Full address search
        if ($_REQUEST['filter_addr']) {
            $filter .= " AND tt.dom_id IN (select id from list_addr where list_addr.full_address LIKE " . $DB->F("%" . $_REQUEST['filter_addr'] . "%") . ")";
        }

        if ($_REQUEST["filter_addr_flat"]) $filter .= " AND tt.kv LIKE " . $DB->F(strtolower($_REQUEST['filter_addr_flat']) . "%");

        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];

        if (($isChiefTech && $isTech) || !$isTech) {

            $_REQUEST['filter_client_phone'] = $this->getCookie('filter_client_phone', $_REQUEST['filter_client_phone']);
            $tpl->setCurrentBlock("is_clnt_phone");
            $tpl->setVariable("FILTER_CLIENT_PHONE", $_REQUEST['filter_client_phone'] ? $_REQUEST['filter_client_phone'] : "");
            $tpl->parse("is_clnt_phone");
            if ($_REQUEST['filter_client_phone']) {
                $phone = preg_replace("/(\D+)/", "", $_REQUEST["filter_client_phone"]);
                $filter .= " AND (`clnt_tel1` LIKE '%" . $phone . "%' OR `clnt_tel2` LIKE '%" . $phone . "%') ";
            }
        } else {
            $_REQUEST['filter_client_phone'] = false;
        }
        $tpl->setVariable("FILTER_DATE_GFX", $_REQUEST['date_gfx'] ? $_REQUEST['date_gfx'] : "");
        $tpl->setVariable('FILTER_ID', htmlspecialchars($_REQUEST['filter_id']));
        $tpl->setVariable("FILTER_ADDR", $_REQUEST['filter_addr'] ? $_REQUEST['filter_addr'] : "");
        $tpl->setVariable("FILTER_ADDR_FLAT", $_REQUEST['filter_addr_flat'] ? $_REQUEST['filter_addr_flat'] : "");
        $tpl->setVariable('FILTER_CNT_OPTIONS', $this->getContrOptions($_REQUEST['filter_cnt_id']));
        $tpl->setVariable('FILTER_TYPE_OPTIONS', $this->getTypeOptions($_REQUEST['filter_type']));
        $tpl->setVariable('FILTER_CLNT_TYPE_SEL_' . $_REQUEST['filter_clnt_type'], 'selected');
        $tpl->setVariable('FILTER_STATUS_OPTIONS', $this->getStatusOptions($_REQUEST['filter_status_id']));
        $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
        $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
        if (($isTech || $isChiefTech) && !$USER->checkAccess("fullaccesstoallsc")) {
            $sc_id = $isChiefTech ? $isChiefTech : User::getScIdByUserId($g_user_id);
            $tpl->setVariable('FILTER_AREA_OPTIONS', array2options($DB->getCell2("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($sc_id) . ") ORDER BY `title`;"), $_REQUEST['filter_area_id']));
        } else
            $tpl->setVariable('FILTER_AREA_OPTIONS', array2options($DB->getCell2("SELECT `id`,`title` FROM `list_area` ORDER BY `title`;"), $_REQUEST['filter_area_id']));
        $tpl->setVariable('CTGRNUM_ID', $_REQUEST['filter_ctgrnum_id']);

        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y') AS DATE,
                    ca.contr_title AS CONTR,
                    typ.title AS TYPE,
                    tt.dom_id,
                    tt.clnt_org_name,
                    tt.clnt_fio,
                    tt.clnt_type,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    (CASE WHEN COALESCE(users.fio,'')<>'' THEN users.fio ELSE users1.fio END) as fio,
                    addr.name, 
                    addr.kladrcode, 
                    addr.althouse, 
                    gfx.c_date, 
                    gfx.startTime, 
                    tt.clnttnum, 
                    tt.kv, 
                    st.tag, 
                    ct.contr_ticket_id,
                    addr.full_address,
                    addr.is_not_recognized
                FROM
                    `tasks` AS ts
                    LEFT JOIN `tickets` AS tt ON ts.id=tt.task_id
                    LEFT JOIN `list_contr` AS ca ON tt.cnt_id=ca.id
                    LEFT JOIN (SELECT * FROM `task_users` WHERE `ispoln`=1 GROUP BY task_id) AS tu ON tu.task_id=ts.id
                    LEFT JOIN `task_types` AS typ ON tt.task_type=typ.id
                    LEFT JOIN `task_status` AS st ON ts.status_id=st.id
                    LEFT JOIN `gfx` AS gfx ON gfx.task_id=ts.id
                    LEFT JOIN `list_addr` AS addr ON addr.id=tt.dom_id
                    LEFT JOIN `users` AS users ON users.id=gfx.empl_id
                    LEFT JOIN `users` AS users1 ON users1.id=tu.user_id
                    LEFT JOIN `list_ods` AS ods ON ods.id=addr.ods_id
                    LEFT JOIN `contr_tickets` AS ct ON ct.task_id=ts.id
                WHERE $filter
                  and case when ifnull(tt.sc_id,0) = 0 then 1
                           when exists (select * from user_sc_access usa where usa.user_id = ".$USER->getId()." and usa.sc_id = tt.sc_id) then 1
                           else 0
                       end = 1
                GROUP BY TASK_ID
                ORDER BY
                    $order ";

        $DB->query($sql);
        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("delticket-head");
        }
        while ($a = $DB->fetch(true)) {

            $resp = $DB->getCell("SELECT user.fio FROM `task_users` AS tu LEFT JOIN `users` AS user ON user.id=tu.user_id WHERE tu.task_id=" . $DB->F($a['TASK_ID']) . " AND tu.ispoln ;");
            if (!$resp) {
                $resp_user = $a['fio'] ? "<i>" . $a['fio'] . "</i>" : false;
            } else {
                $r = false;
                foreach ($resp as $item) {
                    $r[] = "$item";
                }
                $resp_user = implode(", <br/ >", $r);
            }
            if ($a['clnt_type'] == User::CLIENT_TYPE_PHYS) $a['CLIENT'] = $a['clnt_fio'];
            elseif ($a['clnt_type'] == User::CLIENT_TYPE_YUR) $a['CLIENT'] = $a['clnt_org_name'];
            else $a['CLIENT'] = $a['clnt_org_name'] . ', ' . $a['clnt_fio'];
            /*if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "decodeFullKLADR")) {
                $addr = new addr_interface_plugin();
                $addrString = $addr->decodeFullKLADR($a['kladrcode']);
            } */
            if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "decodeFullKLADR")) {
                $addr = new addr_interface_plugin();
                $addrString = $addr->decodeFullKLADRPartial($a['kladrcode']);
            }
            $a["CONTR_TASK_ID"] = $a['contr_ticket_id'] ? $a['contr_ticket_id'] : "<center style='text-decoration:none !important;'>&mdash;</center>";
            $a["HREF_ORDER"] = $this->getLink('vieworderdoc', "id={$a['TASK_ID']}");
            $a['HREF'] = $this->getLink('viewticket', "task_id={$a['TASK_ID']}");

            //JN Address by one row
            if (!empty($a['full_address'])) {
                if($a['is_not_recognized']) {
                    $a['FULL_ADDRESS'] = $a['full_address'] . ' <sup>*</sup>';
                } else {
                    $a['FULL_ADDRESS'] = $a['full_address'];
                }

                $a['ADDR_FLAT'] = $a["kv"];
                $tpl->parse("kladr_addr");
            } else {
                $tpl->setCurrentBlock("no_kladr");
                $a['ADDR_NO_KLADR'] = "<font color=#FF0000>Адрес не распознан</font>";
                $tpl->parse("no_kladr");
            }

            if ($a['c_date'] && $a['startTime']) {
                $startHour = (strlen(floor($a['startTime'] / 60)) == 1) ? "0" . (floor($a['startTime'] / 60)) : floor($a['startTime'] / 60);
                $startMin = (strlen(floor($a['startTime'] % 60)) == 1) ? "0" . (floor($a['startTime'] % 60)) : floor($a['startTime'] % 60);
                if (preg_match("/grafik/", $a['tag'])) {
                    $a['GFXTIME'] = rudate("d M Y", strtotime($a['c_date'])) . " " . $startHour . ":" . $startMin;
                }
            } else {
                //$a['GFXTIME'] = "<center>&mdash;</center>";
            }
            $a['USERFIO'] = $resp_user ? $resp_user : "<center>&mdash;</center>";
            $t = new TelemarketingTicket($a["TASK_ID"]);
            $a['AREA'] = $t->getArea() ? $t->getArea() : "<center>&mdash;</center>";
            if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getSC")) {
                $scO = new adm_sc_plugin();
                $sc = $scO->getSC($t->getSCId());
            }
            $a['SC'] = $sc['title'] ? $sc['title'] : "<center>&mdash;</center>";
            $a['CONTR_ID'] = $clnttnum ? $clnttnum : "<center>&mdash;</center>";
            $tpl->setCurrentBlock('row');
            if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
                $tpl->setCurrentBlock("deleteticket");
                $tpl->setVariable("DELETEHREF", $this->getLink('deleteticket', "task_id={$a['TASK_ID']}"));
                $tpl->parse("deleteticket");
            }
            $tpl->setVariable($a);
            $tpl->parse("row");
        }
        $DB->free();
        $DB->query($sql);
        $total = $DB->getFoundRows();
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable("CURRENT_USER", $USER->getFio());
        $tpl->setVariable("CURRENT_DATE", rudate("H:i d M Y"));

        $DB->free();

        $tpl->show();
    }

    function updateContrDataA()
    {
        global $DB;
        $task_id = $_POST["task_id"];
        $contr_id = $_POST["contr_id"];
        $type_id = $_POST["type_id"];
        $agr_id = $_POST["agr_id"];
        $err = array();
        if ($task_id && $contr_id && $type_id && $agr_id) {
            $sql = "UPDATE `tickets` SET `cnt_id`=" . $DB->F($contr_id) . ", `task_type`=" . $DB->F($type_id) . ", `agr_id`=" . $DB->F($agr_id) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) $err[] = "error";
            //$sql = "DELETE FROM `list_contr_calc` WHERE `task_id`=".$DB->F($task_id).";";
            //$DB->query($sql);
            //if ($DB->errno()) $err[] = "error";
        } else {
            $err[] = "error";
        }
        echo sizeof($err) ? "error" : "ok";
        return false;

    }

    function deleteticket()
    {
        global $DB, $USER;
        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            if ($id = $_REQUEST["task_id"]) {
                $tmc = $DB->getField("SELECT count(`id`) FROM `tmc_ticket` WHERE `task_id`=" . $DB->F($id) . ";");
                if ($tmc) {
                    UIError("Удаление заявки невозможно, т.к. по ней есть списанные ТМЦ!");
                }
                $cnt = $DB->getField("SELECT count(`id`) FROM `list_skp_tech_calc` WHERE `task_id`=" . $DB->F($id) . ";");
                if ($cnt) {
                    UIError("Удаление заявки невозможно, т.к. по ней выполнен расчет!");
                }
                $err = array();
                $sql = "DELETE FROM service_ticket_extras WHERE task_id=" . $DB->F($id) . ";";
                $DB->query($sql);
                $sql = "DELETE FROM `tasks` WHERE `id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_files` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_comments` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_users` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `tickets` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `gfx` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                $sql = "DELETE FROM `joinedtasks` WHERE (`task_id`=" . $DB->F($id) . " or joined_task_id = " . $DB->F($id) . " );";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                if (!sizeof($err))
                    redirect($this->getLink('viewlist'), "Заявка с ID $id успешно удалена.");
                else
                    redirect($this->getLink('viewlist'), "Удаление заявки с ID $id завершено с ошибками! Обратитесь к разработчикам! " . implode("\r\n", $err));

            } else {
                redirect($this->getLink('viewlist'), "Не указан идентификатор заявки для удаления.");

            }
        } else {
            redirect($this->getLink('viewlist'), "Нет доступа к удалению заявок.");
        }

    }

    function viewticket(Request $request)
    {
        global $DB, $USER, $_config;
        $id = (int) ($_GET['task_id'] ? $_GET['task_id'] : ($_GET['id'] ? $_GET['id'] : "0"));
        if (!$id) UIError("Не указан идентификатор заявки");

        if ($id > 0) {
            $sql = "select max(sc.title) title
from list_sc sc,
tickets tt,
tasks t
where tt.task_id = ".$id."
  and t.id = tt.task_id
  and sc.id = tt.sc_id
  and t.plugin_uid = '".$this->getUID()."'
  and case when ifnull(tt.sc_id,0) = 0 then 0
           when exists (select * from user_sc_access usa where usa.user_id = ".$USER->getId()." and usa.sc_id = tt.sc_id) then 0
           else 1
      end = 1";
            $sc_with_no_access = $DB->getField($sql);
            if (strlen($sc_with_no_access) > 0) {
                #$sc_with_no_access .= $sql;
                UIError("У Вас нет доступа на СЦ ".$sc_with_no_access.", просмотр заявки невозможен.");
            }
        }

        $ticket = new TelemarketingTicket($id);

        $is_readonly = 0; /// EF: в будущем здесь будет механизм, определяющий, доступна ли заявка для редактирования. Пока просто заглушка

        $element_disabled = '';
        $element_readonly = $element_disabled;
        if ($is_readonly == 1) {
            $element_disabled = 'disabled';
            $element_readonly = 'readonly';
        }

        #echo "<pre>\n";
        #print_r($ticket);
        #echo "internet_plus_tv_equipment2 = ". $ticket->service_ticket_extras['internet_plus_tv_equipment2']."\n";
        #echo "</pre>\n";
        #exit;

        /// EF: В этой версии не будет личного кабинета оператора
        #$LKUser = adm_users_plugin::isLKUser($USER->getId());
        $LKUser = false;
        if ($LKUser) {
            $LKContrID = adm_users_plugin::getLKUserContrID($USER->getId());
            $tpl_add = "_lk";
        } else {
            $tpl_add = "";
        }
        if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
            //print_r($ticket->getIspoln());
            //echo $USER->getId();
            //die();
            $tech_id = $DB->getField("SELECT `empl_id` FROM `gfx` WHERE `task_id`=" . $DB->F($ticket->getId()) . ";");
            $vTech = adm_empl_plugin::isVirtual($USER->getId());
            if (array_key_exists($USER->getId(), $ticket->getIspoln()) || array_key_exists($vTech, $ticket->getIspoln()) || $tech_id == $USER->getId()) {
                /*if ($ticket->getStatusId() == 44) {
                    UIError("Прямой доступ к заявке запрещен до ее принятия!");
                } */
            } else {
                UIError("В доступе отказано!");
            }
        }
        if (!$ticket->getId()) UIError("Заявка с таким номером не найдена! Возможно, она была удалена.");
        $currentDefaultRegion = false;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($LKUser) {

            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate() . "/ticket_lk.tmpl.htm");
            else
                $tpl->loadTemplatefile("ticket_lk.tmpl.htm");


        } else {
            if (($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId()))) {
                if ($USER->getTemplate() != "default")
                    $tpl->loadTemplatefile($USER->getTemplate() . "/ro_ticket.tmpl.htm");
                else
                    $tpl->loadTemplatefile("ro_ticket.tmpl.htm");
                //$tpl->loadTemplatefile("ro_ticket.tmpl.htm");
            } else {

                if (!$USER->checkAccess("edit_closed_skp_task", User::ACCESS_WRITE) && ($ticket->getStatusId() == 25)) {
                    if ($USER->getTemplate() != "default")
                        $tpl->loadTemplatefile($USER->getTemplate() . "/ro1_ticket.tmpl.htm");
                    else
                        $tpl->loadTemplatefile("ro1_ticket.tmpl.htm");

                } else {
                    if ($USER->getTemplate() != "default")
                        $tpl->loadTemplatefile($USER->getTemplate() . "/ticket.tmpl.htm");
                    else
                        $tpl->loadTemplatefile("ticket.tmpl.htm");
                    //$tpl->loadTemplatefile("ticket.tmpl.htm");
                }
            }
            if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
                if (array_key_exists($USER->getId(), $ticket->getIspoln())) {
                    if ($ticket->getStatusId() == 44) {
                        $accepted = false;
                    } else {
                        $accepted = true;
                    }
                } else $accepted = true;
            }
        }
        $tpl->setVariable("REASON_LIST", adm_gp_reason_plugin::getOptions());
        $tpl->setVariable("COMMENT_LIST", adm_gp_comment_plugin::getOptions());

        ///// Загрузка и отображение дополнительных параметров заявки
        $options = $this->get_operator_list_options($ticket->service_ticket_extras['ispoln_id']);
        $tpl->setVariable('ISPOLN_OPTIONS', $options);

        /// Подтипы заявок, каналы продаж
        $options = $this->get_subtype_list_options($ticket->service_ticket_extras['subtype']);
        $tpl->setVariable('SUBTYPE_OPTIONS', $options);

        $options = $this->get_channels_list_options($ticket->service_ticket_extras['sales_channel']);
        $tpl->setVariable('SALES_CHANNELS_OPTIONS', $options);

        $opts = $this->get_extra_options($ticket->getContrId());
        /* EF: пока решили редактирование оставить без multiselect
                $internet_equipment = '';
                $ctv_equipment = '';
                $internet_plus_tv_equipment1 = '';
                $internet_plus_tv_equipment2 = '';

                $arr_sel = explode("; ",$ticket->service_ticket_extras['internet_equipment']);
                $arr = $DB->getCell2("SELECT equipment_name id,equipment_name FROM equipment WHERE contr_id = ".$this->getContrID($id));
                $internet_equipment = array2options($arr, $arr_sel);
        */
        #$ticket->service_ticket_extras['internet_tarif_id'],
        #$ticket->service_ticket_extras['ctv_tarif_id'],
        #$ticket->service_ticket_extras['internet_plus_tv_tarif_id'],
        #$ticket->service_ticket_extras['phone_tarif_id']

        $js = $opts[0];
        $arr = $opts[1];
        $arr2 = $opts[2];
        $arr3 = $opts[3];
        $arr4 = $opts[4];

        #echo "<pre>\n".print_r($arr,true)." == ".array2options($arr,0,False)."</pre>\n";
        #echo "<pre>\n".print_r($ticket->service_ticket_extras,true)."</pre>\n";
        #exit;

        $tpl->setVariable('ELEMENT_READONLY', $element_readonly);
        $tpl->setVariable('ELEMENT_DISABLED', $element_disabled);

        $tpl->setVariable('INTERNET_AP', $ticket->service_ticket_extras['internet_ap']);
        $tpl->setVariable('INTERNET_SPEED', $ticket->service_ticket_extras['internet_speed']);
        $tpl->setVariable('INTERNET_EQUIPMENT', $ticket->service_ticket_extras['internet_equipment']);
        $tpl->setVariable('CTV_AP', $ticket->service_ticket_extras['ctv_ap']);
        $tpl->setVariable('CTV_CHANNEL', $ticket->service_ticket_extras['ctv_channel']);
        $tpl->setVariable('CTV_EQUIPMENT', $ticket->service_ticket_extras['ctv_equipment']);
        $tpl->setVariable('CTV_PROV', $ticket->service_ticket_extras['ctv_prov']);
        $tpl->setVariable('INTERNET_PLUS_TV_AP', $ticket->service_ticket_extras['internet_plus_tv_ap']);
        $tpl->setVariable('INTERNET_PLUS_TV_SPEED', $ticket->service_ticket_extras['internet_plus_tv_speed']);
        $tpl->setVariable('INTERNET_PLUS_TV_CHANNEL', $ticket->service_ticket_extras['internet_plus_tv_channel']);
        $tpl->setVariable('INTERNET_PLUS_TV_EQUIPMENT1', $ticket->service_ticket_extras['internet_plus_tv_equipment1']);
        $tpl->setVariable('INTERNET_PLUS_TV_EQUIPMENT2', $ticket->service_ticket_extras['internet_plus_tv_equipment2']);
        $tpl->setVariable('PHONE_AP', $ticket->service_ticket_extras['phone_ap']);
        $tpl->setVariable('PHONE_MINUTES', $ticket->service_ticket_extras['phone_minutes']);
        $tpl->setVariable('VAS1_NAME', $ticket->service_ticket_extras['vas1_name']);
        $tpl->setVariable('VAS1_VALUE', $ticket->service_ticket_extras['vas1_value']);
        $tpl->setVariable('VAS2_NAME', $ticket->service_ticket_extras['vas2_name']);
        $tpl->setVariable('VAS2_VALUE', $ticket->service_ticket_extras['vas2_value']);
        $tpl->setVariable('VAS3_NAME', $ticket->service_ticket_extras['vas3_name']);
        $tpl->setVariable('VAS3_VALUE', $ticket->service_ticket_extras['vas3_value']);
        $tpl->setVariable('SOLD_EQUIPMENT', $ticket->service_ticket_extras['sold_equipment']);
        $tpl->setVariable('CLNTTNUM', $ticket->service_ticket_extras['clnttnum']);
        $tpl->setVariable('TARIF_INFO', $ticket->service_ticket_extras['tarif_info']);
        $tpl->setVariable('VAS_INFO', $ticket->service_ticket_extras['vas_info']);
        $tpl->setVariable('SERVICE_FOR_SALE_ID', $DB->F($ticket->service_ticket_extras['service_for_sale_id']));
        $tpl->setVariable('CURRENT_OPERATOR', $ticket->service_ticket_extras['current_operator']);
        $tpl->setVariable('CURRENT_SERVICES', $ticket->service_ticket_extras['current_services']);
        $tpl->setVariable('CURRENT_AP', $ticket->service_ticket_extras['current_ap']);
        $tpl->setVariable('CURRENT_COMMENTS', $ticket->service_ticket_extras['current_comments']);
        $tpl->setVariable('EXTRA_JS', $js);
        $tpl->setVariable('TARIF_INTERNET_OPTIONS', array2options($arr, $ticket->service_ticket_extras['internet_tarif_id'], False));
        $tpl->setVariable('TARIF_CTV_OPTIONS', array2options($arr2, $ticket->service_ticket_extras['ctv_tarif_id'], False));
        $tpl->setVariable('TARIF_INTERNET_PLUS_TV_OPTIONS', array2options($arr3, $ticket->service_ticket_extras['internet_plus_tv_tarif_id'], False));
        $tpl->setVariable('TARIF_PHONE_OPTIONS', array2options($arr4, $ticket->service_ticket_extras['phone_tarif_id'], False));

        if ($ticket->isTest()) {
            $tpl->touchBlock("test-ticket");
        }
        $sql = "SELECT COUNT(*) FROM `contr_tickets` WHERE `task_id`=" . $DB->F($ticket->getId());
        if ($DB->getField($sql)) {
            $tpl->setVariable('IS_CONTR_READONLY', 'readonly');
        }
        if (!$accepted) {
            $tpl->setVariable("NOTACCEPTED", "style='display:none;'");
        } else {
            $tpl->setVariable("ACCEPTED", "style='display:none;'");

        }
        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("deleteticket");
            $tpl->setVariable("DEL_TASK_ID", $_GET["task_id"]);
            $tpl->setVariable("PLUG_UID", $this->getUID());

            $tpl->parse("deleteticket");
        }
        if ($USER->checkAccess("callviewer")) {
            $tpl->touchBlock("callticket");
        }
        if ($USER->checkAccess("close_skp_task", User::ACCESS_WRITE) && $ticket->getStatusId() == 24) {
            $tpl->setCurrentBlock("closeticket");
            $tpl->setVariable("CL_TASK_ID", $_GET["task_id"]);
            $tpl->setVariable("PLUG1_UID", $this->getUID());
            $tpl->parse("closeticket");
        }

        $rejectList = array(10, 46, 43, 5, 65);
        $doneList = array(61, 54, 52, 1, 26, 23, 12, 25, 24, 49);
        $currentStatus = $ticket->getStatusId();
        if ($USER->checkAccess("qc", User::ACCESS_WRITE)) {
            if (in_array($currentStatus, $rejectList) || in_array($currentStatus, $doneList)) {
                $tpl->setCurrentBlock("opros");
                $tpl->setVariable("OPROS_TASK_ID", $id);
                $tpl->setVariable("PL_UID", $ticket->getPluginUid());
                $tpl->setVariable("TASK_STATUS_ID", $currentStatus);
                $tpl->parse("opros");
            }
        }
        $tpl->setVariable('CONTR', $ticket->getContrName());
        $tpl->setVariable('CONTRID', $ticket->getContrId());

        $tpl->setVariable('TASK_TYPE', $ticket->getTypeName());
        if ($ticket->getDomId()) {
            $tpl->setCurrentBlock("kladr");

            $not_recognised_status = $DB->getField("SELECT `is_not_recognized` FROM list_addr WHERE `id` =" . $ticket->getDomId() . ";");
            $taskEntity = \wf::$em->find(TaskEntity::class, $request->get('task_id'));
            /** @var \models\Ticket $ticketEntity */
            $ticketEntity = $taskEntity->getAnyTicket();

            if(!$ticketEntity instanceof \WF\Task\Model\TechnicalTicketInterface)
                throw new Exception('Wrong ticket type!');

            if ($not_recognised_status) {
                $tpl->setVariable('ADDR', "<i style='color:#FF0000' title='Адрес не распознан'>" . $ticketEntity->getDom()->getFullAddress(). "</i>" );
            } else {
                $tpl->setVariable('ADDR', $ticketEntity->getDom()->getFullAddress());
            }

            $tpl->setVariable('LINK_TO_ADDR', "addr_interface/edit?id=" . $ticket->getDomId());
            $tpl->parse("kladr");
            if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getById")) {
                $addr = new addr_interface_plugin();
                $address = $addr->getById($ticket->getDomId());
                $tpl->setVariable("CURRENT_KLADR_CODE", $address["althouse"] != "" ? $address["kladrcode"] : $address["kladrcode"] . "_" . $address["name"]);
                $tpl->setVariable("CURRENT_HOUSE", $address["althouse"] != "" ? $address["kladrcode"] : $address["kladrcode"] . "_" . $address["name"]);
                $tpl->setVariable("CURRENT_SKC", substr($address["kladrcode"], 0, 11) . "00");
                $tpl->setVariable("CURRENT_REGION_CODE", substr($address["kladrcode"], 0, 11) . "00");
                $currentDefaultRegion = substr($address["kladrcode"], 0, 11) . "00";
                $tpl->setVariable("ALT_HOUSE", $address["althouse"]);
            }
        } else {
            $tpl->setCurrentBlock("no_kladr");
            $tpl->setVariable('NO_ADDR', $ticket->getAddr($ticket->getDomId()));
            $tpl->parse("no_kladr");
            $tpl->setVariable("CURRENT_REGION_CODE", getcfg("default_region"));
            $tpl->setVariable("CURRENT_KLADR_CODE", getcfg("default_region"));
        }
        $tpl->setVariable("DOM_ENTR", $address["entr_count"] ? $address["entr_count"] : " - ");
        $tpl->setVariable("DOM_FLOORS", $address["floorcount"] ? $address["floorcount"] : " - ");
        $tpl->setVariable("DOM_FLATS", $address["flatcount"] ? $address["flatcount"] : " - ");
        $tpl->setVariable("DOM_OPLIST", "<strong> - </strong>");
        $tpl->setVariable('AREATITLE', $ticket->getArea());
        if ($USER->checkAccess("virt_editticket", User::ACCESS_WRITE) || $USER->checkAccess("skpallowodsandaddrparams", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("edit-addr-params");
            if (!$ticket->getDomId()) {

                $tpl->setVariable("ADDR_ENABLED", "disabled=\"disabled\"");
            } else {
                $tpl->setVariable("ADDR_ENABLED", "");

            }
            $tpl->parse("edit-addr-param");
        }
        $tpl->setVariable('DOM_ID', $ticket->getDomId());
        if ($USER->checkAccess("virt_editticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("edit-addr");
        }
        $tpl->setVariable("EXTTYPES", adm_ticket_rtsubtypes_plugin::getOptions($ticket->getExtType()));
        $tpl->setVariable('SC_OPTIONS', $this->getSCOptions($ticket->getSCId()));
        $tpl->setVariable('SC_OPTIONS_VAL', $this->getSCName($ticket->getSCId()));
        $tpl->setVariable('POD', $ticket->getPod() ? $ticket->getPod() : " - ");
        $tpl->setVariable('ETAZH', $ticket->getEtazh() ? $ticket->getEtazh() : " - ");
        $tpl->setVariable('KV', $ticket->getKv() ? $ticket->getKv() : " - ");
        $tpl->setVariable('DOMOFON', $ticket->getDomofon() ? $ticket->getDomofon() : " - ");
        $tpl->setVariable('ADDINFO', $ticket->getAddInfo());
        $tpl->setVariable('SWIP', $ticket->getSwIp());
        $tpl->setVariable('SWPORT', $ticket->getSwPort());
        $tpl->setVariable('PRICE', $ticket->getPrice() != "0.00" ? $ticket->getPrice() : "");
        $tpl->setVariable('ORIENT_PRICE', $ticket->getOrientPrice() != "0.00" ? $ticket->getOrientPrice() : "");
        $tpl->setVariable('SWPLACE', $ticket->getSwPlace());
        $tpl->setVariable('CLNTOPAGR', $ticket->getClntOpAgr());
        # EF: Отключаем старый код
        #$tpl->setVariable('CLNTTNUM', $ticket->getClntTNum() ? $ticket->getClntTNum() : $DB->getField("SELECT `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=" . $DB->F($_GET["task_id"]) . ";"));
        if ($ticket->getContrName() == "МГТС" && $ticket->getTypeName() == "СКП" && (1 == 0)) {
            $tpl->setCurrentBlock("openinasrz");
            $tpl->setVariable("CLNTTNUM_I", $ticket->getClntTNum());
            $tpl->parse("openinasrz");
        }


        $ods = $this->getOds($ticket->getDomId());
        if ($USER->checkAccess("virt_editticket", User::ACCESS_WRITE) || $USER->checkAccess("skpallowodsandaddrparams", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("edit-ods");
            if (!$ods["id"]) {
                $tpl->setVariable("ODS_ENABLED", "disabled=\"disabled\"");
            } else {
                $tpl->setVariable("ODS_ENABLED", "");
            }
            $tpl->parse("edit-ods");
        }
        if ($ods['id']) {
            $tpl->setVariable("ODS_ID", $ods["id"]);
        }
        $tpl->setVariable("ODS_ID", $ods["id"] ? $ods["id"] : "");
        $tpl->setVariable("ODS_NAME", $ods["title"] ? $ods["title"] : "не указана");
        $tpl->setVariable("ODS_ADDR", $ods["address"] ? $ods["address"] : "-");
        $tpl->setVariable("ODS_TEL", $ods["tel"] ? $ods["tel"] : "-");
        $tpl->setVariable("ODS_CONTACTS", $ods["fio"] ? $ods["fio"] : "-");
        $tpl->setVariable("ODS_COMMENT", $ods["comment"] ? $ods["comment"] : "-");

        if ($ticket->getStatusId() == 42 && (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()))) {
            #$tpl->setVariable('FIO', "данные скрыты");
            $tpl->setVariable("LINE_CONTACT", "данные скрыты");
            $tpl->setVariable('PASSP_SN', "данные скрыты");
            $tpl->setVariable('PASSP_VID', "данные скрыты");
            $tpl->setVariable('PASSP_VIDDATE', "данные скрыты");
            $tpl->setVariable("DOC_NUM", "данные скрыты");
            $tpl->setVariable('PHONE1', "данные скрыты");
            $tpl->setVariable('PHONE2', "данные скрыты");
        } else {
            #$tpl->setVariable('FIO', $ticket->getFio());
            $tpl->setVariable("LINE_CONTACT", $ticket->getLineContact());
            $tpl->setVariable('PASSP_SN', $ticket->getPasspSN());
            $tpl->setVariable('PASSP_VID', $ticket->getPasspVidan());
            $tpl->setVariable('PASSP_VIDDATE', $ticket->getPasspVidanDate());
            $tpl->setVariable("DOC_NUM", $ticket->getTicketDocNum());
            $phnes = $ticket->getPhones();
            if ($USER->checkAccess("hidetelnum")) {
                $tpl->setVariable('PHONE1', "");
            } else {
                $tpl->setVariable('PHONE1', ($phnes[0] ? $phnes[0] : '+7'));
            }
            if ($phnes[0]) {
                if ($USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId()))) && $USER->getIntPhone()) {
                    if ($phnes[0] != "70000000000") {
                        $tpl->setCurrentBlock("calltoclient1");
                        $tpl->setVariable('CLNT1PHONE', $phnes[0]);
                        $tpl->setVariable("EXTNUM1", $USER->getIntPhone());
                        $tpl->parse("calltoclient1");
                    }
                } else {
                    if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()) && $USER->getIntPhone()) {
                        if ($phnes[0] != "70000000000") {
                            $tpl->setCurrentBlock("calltoclient1tech");
                            $tpl->setVariable('CLNT1PHONETECH', $phnes[0]);
                            $tpl->setVariable("EXTNUM1TECH", $USER->getIntPhone());
                            $tpl->parse("calltoclient1tech");
                        }
                    }
                }

            }
            if ($USER->checkAccess("hidetelnum")) {
                $tpl->setVariable('PHONE2', "");
            } else {
                $tpl->setVariable('PHONE2', ($phnes[1] ? $phnes[1] : '+7'));
            }
            if ($phnes[1]) {
                if ($USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId()))) && $USER->getIntPhone()) {
                    if ($phnes[1] != "70000000000") {
                        $tpl->setCurrentBlock("calltoclient2");
                        $tpl->setVariable('CLNT2PHONE', $phnes[1]);
                        $tpl->setVariable("EXTNUM2", $USER->getIntPhone());
                        $tpl->parse("calltoclient2");
                    }
                } else {
                    if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()) && $USER->getIntPhone()) {
                        if ($phnes[1] != "70000000000") {
                            $tpl->setCurrentBlock("calltoclient2tech");
                            $tpl->setVariable('CLNT2PHONETECH', $phnes[1]);
                            $tpl->setVariable("EXTNUM2TECH", $USER->getIntPhone());
                            $tpl->parse("calltoclient2tech");
                        }
                    }
                }
            }
        }
        $tpl->setVariable("TMC_CAT_LIST_INFORM", adm_material_cats_plugin::getOptions());
        $tpl->touchBlock('client_type_' . $ticket->getClientType());

        if ($ticket->getClientType() != User::CLIENT_TYPE_PHYS) {
            $a = $tpl->setCurrentBlock("org");
            #if ($a) {echo "The block 'org' is set<br>\n";} else {echo "The block 'org' is NOT set<br>\n";}
            #$tpl->touchBlock("org");
            $tpl->setVariable('ORG_NAME', $ticket->getOrgName());
            if (!$accepted) {
                $tpl->setVariable("ORGNOTACCEPTED", "style='display:none;'");
            }
            $tpl->setVariable('ORG_REQUIRED', "required='required'");
            $tpl->parseCurrentBlock();
        } else {
            $a = $tpl->setCurrentBlock("fiz");
            #if ($a) {echo "The block 'FIZ' is set<br>\n";} else {echo "The block 'FIZ' is NOT set<br>\n";}
            #$tpl->touchBlock("fiz");
            $tpl->setVariable('FIO', $ticket->getFio());
            $tpl->setVariable('FIZ_REQUIRED', "required='required'");
            $tpl->parseCurrentBlock();
        }


        $tpl->setVariable("TASK_FROM", adm_ticket_sources_plugin::getOptions($ticket->getTaskFrom()));
        if ($LKUser) {
            $tpl->setVariable("TASK_FROM_TITLE", adm_ticket_sources_plugin::getById($ticket->getTaskFrom()));
            $sc_name = adm_sc_plugin::getSC($ticket->getSCId());
            $tpl->setVariable("SC_NAME", $sc_name["title"] ? $sc_name["title"] : "&mdash;");
            $tpl->setVariable("SC_LK_ID", $ticket->getSCId());
        } else {
            $tpl->setVariable("SC_LIST", adm_sc_plugin::getScList($ticket->getSCId()));
            $tpl->setVariable("TASK_FROM", adm_ticket_sources_plugin::getOptions($ticket->getTaskFrom()));
        }

        $rejTitle = '';
        $rej_id = $ticket->getRejectReasonId(false);
        if ($rej_id) {
            if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejReasonTitle")) {
                $rej = new adm_rejects_plugin();
                $rejTitle = " (" . $rej->getRejReasonTitle($rej_id) . ")";
            }
        }
        if (User::isTech($USER->getId()) && $USER->getIntPhone()) {
            $tpl->setCurrentBlock("calltogssdisp");
            $tpl->setVariable("DISPGROUPTECHPHONE", getcfg("dispgroupnumber"));
            $tpl->setVariable("TPGSSEXTNUM1TECH", $USER->getIntPhone());
            $tpl->setVariable("TPGSSTECHPHONE", getcfg("dispgroupnumber"));
            $tpl->parse("calltogssdisp");
        }
        $agr_id = $ticket->getAgrId(false);
        if ($agr_id) {
            $phoneList = agreements_plugin::getTPPhones($agr_id);

            if ($phoneList) {
                $tpl->setCurrentBlock("tpnumlist");
                if (User::isTech($USER->getId())) {
                    foreach ($phoneList as $tpi) {
                        $tpl->setCurrentBlock("tech_tnl");
                        $tpl->setVariable("TPTECHPHONE", $tpi["phone"]);
                        if ($tpi["ext"]) {
                            $tpl->setVariable("TPTECHDOB", $tpi["ext"]);
                            $tpl->setVariable("TPTECHDOBTITLE", " доб. " . $tpi["ext"]);
                        }
                        if ($tpi["phonetitle"]) {
                            $tpl->setVariable("BUTTONTITLE", $tpi["phonetitle"]);
                        } else {
                            $tpl->setVariable("BUTTONTITLE", "Позвонить в ТП");
                        }
                        $tpl->setVariable("TPEXTNUM1TECH", $USER->getIntPhone());
                        $tpl->parse("tech_tnl");
                    }
                } else {
                    if ($USER->checkAccess("callout")) {
                        foreach ($phoneList as $tpi) {
                            $tpl->setCurrentBlock("disp_tnl");
                            $tpl->setVariable("TPDISPPHONE", $tpi["phone"]);
                            if ($tpi["ext"]) {
                                $tpl->setVariable("TPDISPDOB", $tpi["ext"]);
                                $tpl->setVariable("TPDISPDOBTITLE", " доб. " . $tpi["ext"]);
                            }

                            if ($tpi["phonetitle"]) {
                                $tpl->setVariable("BUTTONTITLE", $tpi["phonetitle"]);
                            } else {
                                $tpl->setVariable("BUTTONTITLE", "Позвонить в ТП");
                            }
                            $tpl->setVariable("TPDISPEXT", $USER->getIntPhone());
                            $tpl->parse("disp_tnl");
                        }

                    }
                }
                $tpl->parse("tpnumlist");
            }
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getAgrById")) {
                $kontr = new kontr_plugin();
                $agr = $kontr->getAgrById($agr_id);
                if ($agr) {
                    $tpl->setVariable("TASK_AGR", $agr["number"] . " от " . $agr["agr_date"]);
                } else {
                    $tpl->setVariable("TASK_AGR", "ошибка");
                }
                $agr_contacts = $kontr->getAgrById($agr_id);
                if ($agr_contacts) {
                    $tpl->setVariable("AGR_ACCESS", $agr_contacts['access_contact'] ? $agr_contacts['access_contact'] : "<strong>-</strong>");
                    $tpl->setVariable("AGR_TP", $agr_contacts['tp_contact'] ? $agr_contacts['tp_contact'] : "<strong>-</strong>");

                } else {
                    $tpl->setVariable("AGR_ACCESS", "<strong>ошибка</strong>");
                    $tpl->setVariable("AGR_TP", "<strong>ошибка</strong>");
                }
            }
        } else {
            $tpl->setVariable("TASK_AGR", "<strong>не указан</strong>");
            $tpl->setVariable("AGR_ACCESS", "<strong>-</strong>");
            $tpl->setVariable("AGR_TP", "<strong>-</strong>");
        }
        $tpl->setVariable('STATUS_NAME', $ticket->getStatusName() . " " . $rejTitle);

        $tpl->setVariable('STATUS_COLOR', $ticket->getStatusColor());
        $tpl->setVariable('DATE_REG', $ticket->getDateReg());
        $tpl->setVariable("USER_REG", $ticket->getAuthorFio() ? $ticket->getAuthorFio() : 'Система');
        $csts = ($ticket->getStatusId() == 44 /*|| $ticket->getStatusId() == 42*/) ? true : false;
        $forbidComments = ($csts && $USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) ? true : false;
        if (!$forbidComments)
            $tpl->setVariable('TASK_COMMENTS', $ticket->getComments());
        else {
            $tpl->setVariable("TEMP_ISROSTEL", "style='display:none;'");
        }
        $tpl->setVariable('TASK_ID', $ticket->getId());

        $ispolnListTech = $DB->getCell2("SELECT us.id, us.fio FROM `task_users` AS tu JOIN `users` AS us ON tu.user_id=us.id WHERE tu.task_id=" . $DB->F($id) . " AND tu.ispoln AND us.id IN (SELECT `user_id` FROM `link_sc_user` WHERE 1) ORDER BY us.id");
        if (!$ispolnListTech) {
            $tpl->setVariable("TECH_LIST_INFORM_TECH", "<option value=''>-- нет исполнителей --</option>");
        } else {
            $tpl->setVariable("TECH_LIST_INFORM_TECH", array2options($ispolnListTech));
        }
        $tpl->setVariable("R_TMC_CAT_LIST_INFORM", adm_material_cats_plugin::getOptions_nonmetric());


        $sql = "SELECT gfx.c_date, gfx.startTime, user.fio, user.id FROM `gfx` AS gfx LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.task_id=" . $DB->F($_GET['task_id']) . ";";
        $result = $DB->getRow($sql);
        if ($result) {

            $startHour = (strlen(floor($result[1] / 60)) == 1) ? "0" . (floor($result[1] / 60)) : floor($result[1] / 60);
            $startMin = (strlen(floor($result[1] % 60)) == 1) ? "0" . (floor($result[1] % 60)) : floor($result[1] % 60);
            $tpl->setVariable("GFX_DATETIME", rudate("d M Y", strtotime($result[0])) . " " . $startHour . ":" . $startMin);
            $tpl->setVariable("GFX_EMPL", $result[2]);
            $emplPhone = adm_users_plugin::getUserPhone($result[3]);
            $emplPhone = explode(";", $emplPhone);
            if (count($emplPhone) > 1) {
                $phone1 = $emplPhone[0];
                $phone2 = $emplPhone[1];
                if ($phone1) {
                    $phone_clean = preg_replace("/[^0-9]/", "", $phone1);

                    if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
                    {
                        $phone_clean = $phone_clean;
                    } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
                    {
                        $phone_clean = substr($phone_clean, 1);
                    } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
                    {
                        $phone_clean = $phone_clean;
                    } else {
                        $phone_clean = false;
                    }
                    $phone1 = $phone_clean;
                    if ($phone1 && $USER->getIntPhone() && $USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId())))) {
                        $tpl->setVariable("calltotech");
                        $tpl->setVariable("TECHPHONE", $phone1);
                        $tpl->setVariable("EXT3NUM", $USER->getIntPhone());
                        $ctc_tech = new User($result[3]);
                        if ($ctc_tech) {
                            $ctc_phone = $ctc_tech->getIntPhone();

                            $targetPhone = false;

                            if ($ctc_phone && $phnes[0] && $phnes[0] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[0]);
                                $tpl->parse("ctc_button");
                            }
                            if ($ctc_phone && $phnes[1] && $phnes[1] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[1]);
                                $tpl->parse("ctc_button");
                            }
                        }
                        $tpl->parse("calltotech");
                    }
                }
                if ($phone2) {
                    $phone_clean = preg_replace("/[^0-9]/", "", $phone2);

                    if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
                    {
                        $phone_clean = $phone_clean;
                    } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
                    {
                        $phone_clean = substr($phone_clean, 1);
                    } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
                    {
                        $phone_clean = $phone_clean;
                    } else {
                        $phone_clean = false;
                    }
                    $phone2 = $phone_clean;
                    if ($phone2 && $USER->getIntPhone() && $USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId())))) {
                        $tpl->setVariable("calltotech");
                        $tpl->setVariable("TECHPHONE", $phone2);
                        $tpl->setVariable("EXT3NUM", $USER->getIntPhone());
                        $ctc_tech = new User($result[3]);
                        if ($ctc_tech) {
                            $ctc_phone = $ctc_tech->getIntPhone();

                            $targetPhone = false;

                            if ($ctc_phone && $phnes[0] && $phnes[0] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[0]);
                                $tpl->parse("ctc_button");
                            }
                            if ($ctc_phone && $phnes[1] && $phnes[1] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[1]);
                                $tpl->parse("ctc_button");
                            }
                        }
                        $tpl->parse("calltotech");
                    }
                }
            } else {
                if ($emplPhone[0]) {
                    $phone_clean = preg_replace("/[^0-9]/", "", $emplPhone[0]);

                    if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
                    {
                        $phone_cslean = $phone_clean;
                    } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
                    {
                        $phone_clean = substr($phone_clean, 1);
                    } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
                    {
                        $phone_clean = $phone_clean;
                    } else {
                        $phone_clean = false;
                    }
                    $phone = $phone_clean;
                    if ($phone && $USER->getIntPhone() && $USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId())))) {
                        $tpl->setVariable("calltotech");
                        $tpl->setVariable("TECHPHONE", $phone);
                        $tpl->setVariable("EXT3NUM", $USER->getIntPhone());
                        $ctc_tech = new User($result[3]);
                        if ($ctc_tech) {
                            $ctc_phone = $ctc_tech->getIntPhone();

                            $targetPhone = false;

                            if ($ctc_phone && $phnes[0] && $phnes[0] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[0]);
                                $tpl->parse("ctc_button");
                            }
                            if ($ctc_phone && $phnes[1] && $phnes[1] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[1]);
                                $tpl->parse("ctc_button");
                            }
                        }
                        $tpl->parse("calltotech");
                    }
                }
            }
            if ($ticket->getStatusId() != 45 && $ticket->getStatusId() != 50) {
                $tpl->touchBlock("insertinactive");
            }
        } else {
            $tpl->setVariable("GFX_DATETIME", "НЕ НАЗНАЧЕН");
            $tpl->setVariable("GFX_EMPL", "НЕ НАЗНАЧЕН");
            $tpl->touchBlock("removeinactive");
            $tpl->touchBlock("resetinactive");
        }
        $tpl->setVariable('TYPE_OPTIONS', $this->getTypeOptions($this->getTypeID($ticket->getId())));
        /// EF: зачем 3 функции дергать???
        #$tpl->setVariable('CNT_OPTIONS', $this->getContrOptions($this->getContrID($ticket->getId())));
        $tpl->setVariable('CNT_OPTIONS', $this->getContrOptions($ticket->getContrId()));
        if (class_exists("adm_areas_plugin", true) && method_exists("adm_areas_plugin", "getAreaList")) {
            $area = new adm_areas_plugin();
            $tpl->setVariable("AREA_OPTIONS", $area->getAreaList(@$address["area_id"]));
            if ($address["area_id"]) {
                $tpl->setVariable("DIS_AREA_LIST", "disabled=\"disabled\"");
            }
        }

        if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
            if ($ticket->getStatusId() != 45 && $ticket->getStatusId() != 50 && $ticket->getStatusId() != 9 && $ticket->getStatusId() != 47) {
                $tpl->touchBlock("insertinactive");
                $tpl->touchBlock("printinactive");

            }
            if ($ticket->getStatusId() != 9) {
                $tpl->touchBlock("removeinactive");
                $tpl->touchBlock("resetinactive");
            }
            $tpl->touchBlock("resetinactive");
            $tpl->setVariable("NOACCESSTODELETEFROMGFX", "style='display:none;'");
            $tpl->setVariable("ISTECH_DISABLED", "style='display: none;'");
        }

        if ($USER->checkAccess("changedonedate_skp", User::ACCESS_WRITE)) {
            $donedate = $ticket->getDoneDate();
            if ($donedate && $donedate != "00.00.0000") {
                $tpl->setCurrentBlock("editdonedate");
                $tpl->setVariable("DONEDATE", $donedate);
                $tpl->parse("editdonedate");
            } //insertinactive
        }
        $ticket_act = $ticket->getTICKActId();
        if ($ticket_act) {
            $actdate = $DB->getField("SELECT DATE_FORMAT(`cr_date`, '%H:%i %d.%m.%Y') FROM `list_tskpdata_reports` WHERE `id`=" . $DB->F($ticket_act) . ";");
            $tpl->setVariable("ACTDATA", $ticket_act . " от " . $actdate);
        } else {
            $tpl->setVariable("ACTDATA", "&mdash;");
        }
        $currentStatus = $ticket->getStatusId();

        $new_status = adm_statuses_plugin::getStatusByTag("new", "tm_module");
        $report_status = adm_statuses_plugin::getStatusByTag("report", "tm_module");
        $done_status = adm_statuses_plugin::getStatusByTag("done", "tm_module");

        $tpl->setVariable("OP_DATE_FROM", $ticket->getSlotDateBegin() != "00.00.0000" ? $ticket->getSlotDateBegin() : "");
        $tpl->setVariable("OP_DATE_TO", $ticket->getSlotDateEnd() != "00.00.0000" ? $ticket->getSlotDateEnd() : "");
        $tpl->setVariable("OP_TIME_FROM", $ticket->getSlotTimeBegin() != "00:00" ? $ticket->getSlotTimeBegin() : "");
        $tpl->setVariable("OP_TIME_TO", $ticket->getSlotTimeEnd() != "00:00" ? $ticket->getSlotTimeEnd() : "");
        if ($ticket->getDocID() && $ticket->getDocDate()) {
            $tpl->setVariable("DOC_TICKET_PARAMS", $ticket->getDocID() . " от " . $ticket->getDocDate());
        } else {
            $tpl->setVariable("DOC_TICKET_PARAMS", "&mdash;");
        }
        if ($ticket->getTMCDocID() && $ticket->getTMCDocDate()) {
            $tpl->setVariable("DOC_TMC_PARAMS", $ticket->getTMCDocID() . " от " . $ticket->getTMCDocDate());
        } else {
            $tpl->setVariable("DOC_TMC_PARAMS", "&mdash;");
        }


        if (($currentStatus == $new_status["id"] || $currentStatus == $done_status["id"] || $currentStatus == $report_status["id"]) && $USER->checkAccess("edit_contr_ticket_service", User::ACCESS_WRITE)) {
            $tpl->touchBlock("changecontr");
        }

        /**
         * Отрисовка кнопки и блока связанных заявок
         */
        \WF\Legacy\TicketViewModificator::renderAll(new Task($id), $tpl);

        UIHeader($tpl);
        $tpl->show();
    }

    function checktaskstatus_ajax()
    {
        global $DB;
        if ($task_id = $_POST['task_id']) {
            $task = new Task($task_id);
            $currentStatus = $DB->getField("SELECT `status_id` FROM `tasks` WHERE `id`=" . $DB->F($task_id) . ";");
            $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("tm_module") . " AND `tag`=" . $DB->F("report") . ";";
            $status = $DB->getField($sql);
            if ($status == $currentStatus) {
                $ret = "ok";
            } else {
                $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("tm_module") . " AND `tag` LIKE " . $DB->F("%grafik%") . ";";
                $status = $DB->getField($sql);
                if ($status == $currentStatus) {
                    $ret = "change";
                } else {
                    $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("tm_module") . " AND `tag`=" . $DB->F("closed") . ";";
                    $status = $DB->getField($sql);
                    if ($status == $currentStatus) {
                        $ret = "ok";
                    } else {
                        $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("tm_module") . " AND `tag`=" . $DB->F("acc_accepted") . ";";
                        $status = $DB->getField($sql);
                        if ($status == $currentStatus) {
                            $ret = "ok";
                        } else {
                            $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("tm_module") . " AND `tag`=" . $DB->F("ticketexec") . ";";
                            $status = $DB->getField($sql);
                            if ($status == $currentStatus) {
                                $ret = "ok";
                            } else {
                                $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("tm_module") . " AND `tag` LIKE " . $DB->F("%done%") . ";";
                                $status = $DB->getField($sql);
                                if ($status == $currentStatus) {
                                    $ret = "ok";
                                } else {
                                    $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("tm_module") . " AND `tag` LIKE " . $DB->F("%timeout30%") . ";";
                                    $status = $DB->getField($sql);
                                    if ($status == $currentStatus) {
                                        $ret = "ok";
                                    } else {
                                        $ret = "nostatus";
                                    }
                                }
                            }
                        }
                    }

                }
            }
        } else {
            $ret = "error";
        }
        echo $ret;
        return false;
    }

    function queryStatus()
    {
        global $DB;
        if ($task_id = $_POST['task_id']) {
            $sql = "SELECT s.tag FROM `tasks` AS t LEFT JOIN `task_status` AS s ON t.status_id=s.id WHERE t.id=" . $DB->F($task_id) . ";";
            $status_tag = $DB->getField($sql);
            $ret = $DB->errno() ? "error" : ($status_tag ? $status_tag : "none");
        } else {
            $ret = "error";
        }
        echo $ret;
        return false;
    }

    function closeticket()
    {
        global $DB, $USER;
        if ($USER->checkAccess("close_skp_task", User::ACCESS_WRITE)) {
            $task_id = $_REQUEST["task_id"];
            if ($task_id) {
                $ticket = new TelemarketingTicket($task_id);
                if ($ticket->getStatusId() != 24) {
                    UIError("Заявка находится в некорректном статусе!");
                } else {
                    if ($ticket->addComment("Закрытие заявки", "Статус: Закрыта", 25)) {
                        redirect($this->getLink('viewticket', "task_id=" . $task_id), "Заявка успешно Закрыта!");
                    } else {
                        UIError("Не указан идентификатор заявки!");
                    }
                }
            } else {
                UIError("Не указан идентификатор заявки!");
            }
        } else {
            UIError("В доступе отказано!");
        }

    }

    function getwtypelist()
    {
        global $DB;
        $contr_id = $_POST['contr_id'];
        $type = $_POST['type'];
        if ($contr_id === FALSE || $type === FALSE) {
            echo "error";
            return;
        }
        $sql = "SELECT wt.id, wt.title FROM `list_contr_agr` AS ca
                LEFT JOIN `link_agr_wtypes` AS law ON law.agr_id=ca.id
                LEFT JOIN `task_types` AS wt ON wt.id=law.wtype_id WHERE ca.contr_id=" . $DB->F($contr_id) . " AND wt.title IS NOT NULL AND wt.plugin_uid=" . $DB->F($type) . " ORDER BY wt.title;";
        if (array2options($DB->getCell2($sql))) echo array2options($DB->getCell2($sql), $_POST['sel_id']); else echo "<option value=\"\">-- нет видов работ --</option>";

    }

    /// EF: возвращает select услуг на продажу по заданному контрагенту
    function get_services_for_sale()
    {
        global $DB;
        #$contr_id = $_POST['contr_id'];
        $contr_id = $_REQUEST['contr_id'];
        $selected_service_id = intval($_REQUEST['service_id']);
        if ($contr_id <= 0) {
            $contr_id = 0;
        }
        $sql = "select s.id, s.service_name
                  from link_contragent_services l,
                       services_for_sale s
                 where s.id = l.service_id
                   and l.contragent_id = " . $contr_id;
        $arr = $DB->getCell2($sql);
        $options = "<option value=\"0\">-- услуги не заведены --</option>";
        if ($arr) {
            $options .= array2options($arr, $selected_service_id, False);
        }
        echo $options;
    }

    function getagrlist()
    {
        global $DB;
        $contr_id = $_POST['contr_id'];
        $wtype_id = $_POST['wtype_id'];
        if ($contr_id === FALSE || $wtype_id === FALSE) {
            echo "error";
            return;
        }
        $sql = "SELECT `id`, CONCAT(`number`, ' от ', `agr_date`) AS agr FROM `list_contr_agr` WHERE `contr_id`=" . $DB->F($contr_id) . " AND `id` IN (SELECT `agr_id` FROM `link_agr_wtypes` WHERE `wtype_id`=" . $DB->F($wtype_id) . ");";
        if (array2options($DB->getCell2($sql))) echo array2options($DB->getCell2($sql), $_POST['sel_id']); else echo "<option value=\"\">-- нет договоров --</option>";
    }

    /// список оборудования
    function load_equipment()
    {
        global $DB;
        $query_type = 0;
        #$options = "<option value=\"\">-- Не выбрано --</option>";
        $options = "";
        if (isset($_REQUEST['query_type'])) {
            $query_type = $_REQUEST['query_type'];
        }
        $cntr_id = intval($_REQUEST['cntr_id']);
        ### все оборудование контрагента
        if ($query_type == 0) {
            $sql = "select equipment_name id, equipment_name from equipment where contr_id = " . $cntr_id;
            $arr = $DB->getCell2($sql);
            $options .= array2options($arr, "", False);
        }
        ### оборудование интернет + прочее
        if ($query_type == 1) {
            $sql = "select equipment_name id, equipment_name from equipment where equipment_type in ('Интернет','Прочее') and contr_id = " . $cntr_id;
            $arr = $DB->getCell2($sql);
            $options .= array2options($arr, "", False);
        }
        ### оборудование тв + прочее
        if ($query_type == 2) {
            $sql = "select equipment_name id, equipment_name from equipment where equipment_type in ('ТВ','Прочее') and contr_id = " . $cntr_id;
            $arr = $DB->getCell2($sql);
            $options .= array2options($arr, "", False);
        }

        echo $options;
    }

    /// тарифы для контрагента
    function load_tarifs()
    {
        global $DB;
        $contr_id = intval($_REQUEST['contr_id']);
//        $sql = "select tarif_type, tarif_name, tarif_id, property_name, tarif_type_property_value "
//                . " from tarif_options_view "
//                . "where property_name not like '%Оборудование%' "
//                . "and contr_id = " . $contr_id;
        $sql = "select 
    tt.tarif_type,
    t.tarif_name,
    tv.tarif_id,
    tp.property_name,
    tv.tarif_type_property_value
from (((tarifs t 
        join tarif_types tt) 
        join tarif_type_properties tp) 
        join tarif_values tv) 
where ((tt.id = t.tarif_type_id) 
   and (tt.id = tp.tarif_type_id) 
   and (t.id = tv.tarif_id) 
   and (tp.id = tv.tarif_type_property_id))
   and tp.property_name not like '%Оборудование%' 
   and contr_id = " . $contr_id;

        $arr = $DB->getAll($sql);
        $res = array();
        foreach ($arr as $key => $val) {
            $res[$val['tarif_type']] [$val['tarif_id']] ['tarif_name'] = $val['tarif_name'];
            $res[$val['tarif_type']] [$val['tarif_id']] [$val['property_name']] = $val['tarif_type_property_value'];
        }
        $out = json_encode($res, JSON_UNESCAPED_UNICODE);
        #echo "<pre>\n";
        #print_r($arr);
        #print_r($res);
        #echo "</pre>\n";
        echo $out;
    }

    /// Исполнитель (Оператор) выбирается руководителем. По умолчанию - система
    function get_operator_list_options($selected = 0)
    {
        global $DB;
        /* ///// EF: Это было для тестов. Сейчас переключаем на otdel_id = 164
        $sql = "select r.user_id, u.fio
                  from user_roles r,
                       users u
                 where r.role_id = 1
                   and r.user_id = u.id
                   and u.active = 1";
        */
        $sql = "select u.id user_id, u.fio
                  from users u
                 where u.active > 1
                   and u.otdel_id = 164";
        $arr = $DB->getCell2($sql);
        if ($selected == 0) {
            $options = "<option selected value=\"0\">-- Система --</option>";
        } else {
            $options = "<option value=\"0\">-- Система --</option>";
        }
        if ($arr) {
            $options .= array2options($arr, $selected, False);
        }
        return $options;
    }

    /// Подтипы заявок
    function get_subtype_list_options($selected = 0)
    {
        global $DB;
        $sql = "select id,subtype_name
                  from service_subtypes";
        $arr = $DB->getCell2($sql);
        if ($selected == 0) {
            $options = "<option selected value=\"0\">-- Не выбрано --</option>";
        } else {
            $options = "<option value=\"0\">-- Не выбрано --</option>";
        }
        if ($arr) {
            $options .= array2options($arr, $selected, False);
        }
        return $options;
    }

    /// Каналы продаж
    function get_channels_list_options($selected = 0)
    {
        global $DB;
        $sql = "select id,channel_name
                  from sales_channels";
        $arr = $DB->getCell2($sql);
        if ($selected == 0) {
            $options = "<option selected value=\"0\">-- Не выбрано --</option>";
        } else {
            $options = "<option value=\"0\">-- Не выбрано --</option>";
        }
        if ($arr) {
            $options .= array2options($arr, $selected, False);
        }
        return $options;
    }

    /// js для заполнения полей тарифными значениями, а также опции с тарифами
    function get_extra_options($contr_id = 0)
    {
        if ($contr_id < 1) {
            $contr_id = 0;
        }
        global $DB;
        $js = "var tarif_links = new Object();\n";
        $js .= "var ctv_links = new Object();\n";
        $js .= "var internet_plus_tv_links = new Object();\n";
        $js .= "var phone_links = new Object();\n";
//        $sql = "select tarif_id as keyval,"
//                . "tarif_name,"
//                . "tarif_type_id,"
//                . "property_name,"
//                . "tarif_type_property_value "
//                . "from tarif_options_view "
//                . "where contr_id = ".$contr_id." "
//                . "order by property_order";
//

        $sql = "select 
    t.tarif_type_id,
    t.tarif_name,
    tv.tarif_id AS keyval,
    tp.property_name,
    tv.tarif_type_property_value
from (((tarifs t 
        join tarif_types tt) 
        join tarif_type_properties tp) 
        join tarif_values tv) 
where ((tt.id = t.tarif_type_id) 
   and (tt.id = tp.tarif_type_id) 
   and (t.id = tv.tarif_id) 
   and (tp.id = tv.tarif_type_property_id))
   and contr_id = ".$contr_id."
   order by tp.property_order";

        #$sql = "select @@character_set_client, @@character_set_results, @@character_set_connection";
        $res = $DB->getCell3($sql);
        #echo "<pre>\n";
        #print_r($res);
        #echo "</pre>\n";
        $known_tariffs = array();
        $arr = array();
        $arr2 = array();
        $arr3 = array();
        $arr4 = array();
        $arr[0] = "---";
        $arr2[0] = "---";
        $arr3[0] = "---";
        $arr4[0] = "---";
        foreach ($res as $k1 => $v1) {
            foreach ($v1 as $k2 => $v2) {
                #$arr[$v2['property_name']] = $v2['tarif_type_property_value'];
                if ($known_tariffs[$v2['keyval']] != 1) {
                    $known_tariffs[$v2['keyval']] = 1;
                    if ($v2['tarif_type_id'] == 1) {
                        $js .= "tarif_links[" . $v2['keyval'] . "] = new Object();\n";
                    }
                    if ($v2['tarif_type_id'] == 2) {
                        $js .= "ctv_links[" . $v2['keyval'] . "] = new Object();\n";
                    }
                    if ($v2['tarif_type_id'] == 3) {
                        $js .= "internet_plus_tv_links[" . $v2['keyval'] . "] = new Object();\n";
                    }
                    if ($v2['tarif_type_id'] == 4) {
                        $js .= "phone_links[" . $v2['keyval'] . "] = new Object();\n";
                    }
                }
                if ($v2['tarif_type_id'] == 1) {
                    $js .= "tarif_links[" . $v2['keyval'] . "]['" . $v2['property_name'] . "'] = '" . $v2['tarif_type_property_value'] . "';\n";
                    $arr[$v2['keyval']] = $v2['tarif_name'];
                }
                if ($v2['tarif_type_id'] == 2) {
                    $js .= "ctv_links[" . $v2['keyval'] . "]['" . $v2['property_name'] . "'] = '" . $v2['tarif_type_property_value'] . "';\n";
                    $arr2[$v2['keyval']] = $v2['tarif_name'];
                }
                if ($v2['tarif_type_id'] == 3) {
                    $js .= "internet_plus_tv_links[" . $v2['keyval'] . "]['" . $v2['property_name'] . "'] = '" . $v2['tarif_type_property_value'] . "';\n";
                    $arr3[$v2['keyval']] = $v2['tarif_name'];
                }
                if ($v2['tarif_type_id'] == 4) {
                    $js .= "phone_links[" . $v2['keyval'] . "]['" . $v2['property_name'] . "'] = '" . $v2['tarif_type_property_value'] . "';\n";
                    $arr4[$v2['keyval']] = $v2['tarif_name'];
                }
            }
        }
        return array($js, $arr, $arr2, $arr3, $arr4);
    }


    function newticket()
    {
        global $DB, $USER, $_config;
        if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
            UIError("Вам запрещено создавать заявки!", "Отказано в доступе!");
        }
        #$LKUser = adm_users_plugin::isLKUser($USER->getId());
        $LKUser = false;
        if ($LKUser) {
            $LKContrID = adm_users_plugin::getLKUserContrID($USER->getId());
            $contr_id = $LKContrID;
            $type = "tm_module";

            $sql = "SELECT wt.id, wt.title FROM `list_contr_agr` AS ca
                    LEFT JOIN `link_agr_wtypes` AS law ON law.agr_id=ca.id
                    LEFT JOIN `task_types` AS wt ON wt.id=law.wtype_id WHERE ca.contr_id=" . $DB->F($contr_id) . " AND wt.title IS NOT NULL AND wt.plugin_uid=" . $DB->F($type) . " ORDER BY wt.title;";
            $r = $DB->getCell($sql);
            if (!$r) UIError("У Вас нет активных договоров в WF для создания заявок такого типа!");

            $tpl_add = "_lk";
        } else $tpl_add = "";
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("new.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/new" . $tpl_add . ".tmpl.htm");
        else
            $tpl->loadTemplatefile("new" . $tpl_add . ".tmpl.htm");


        /// Исполнитель (Оператор) выбирается руководителем. По умолчанию - система
        $options = $this->get_operator_list_options();
        $tpl->setVariable('ISPOLN_OPTIONS', $options);

        /// Подтипы заявок, каналы продаж
        $options = $this->get_subtype_list_options();
        $tpl->setVariable('SUBTYPE_OPTIONS', $options);

        $options = $this->get_channels_list_options();
        $tpl->setVariable('SALES_CHANNELS_OPTIONS', $options);

        $opts = $this->get_extra_options();
        $js = $opts[0];
        $arr = $opts[1];
        $arr2 = $opts[2];
        $arr3 = $opts[3];
        $arr4 = $opts[4];

        #echo "<pre>\n".print_r($arr,true)." == ".array2options($arr,0,False)."</pre>\n";
        #exit;
        $tpl->setVariable('EXTRA_JS', $js);

        #$tpl->setVariable('TARIF_INTERNET_OPTIONS', array2options($arr,0,False));
        #$tpl->setVariable('TARIF_CTV_OPTIONS', array2options($arr2,0,False));
        #$tpl->setVariable('TARIF_INTERNET_PLUS_TV_OPTIONS', array2options($arr3,0,False));
        #$tpl->setVariable('TARIF_PHONE_OPTIONS', array2options($arr4,0,False));
        $tpl->setVariable('TYPE_OPTIONS', $this->getTypeOptions());
        $tpl->setVariable('CNT_OPTIONS', $this->getContrOptions());
        $tpl->setVariable("LK_CONTR_ID", $LKContrID);
        $tpl->setVariable("LK_CONTR_NAME", kontr_plugin::getByID($LKContrID));
        if (class_exists("adm_areas_plugin", true) && method_exists("adm_areas_plugin", "getAreaList")) {
            $area = new adm_areas_plugin();
            $tpl->setVariable("AREA_OPTIONS", $area->getAreaList());
        }
        $tpl->setVariable("EXTTYPES", adm_ticket_rtsubtypes_plugin::getOptions());

        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKRegionList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_REGION", $addr->getKRegionList(getcfg('default_region')));
        }
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKAreaList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_AREA", $addr->getKAreaList(getcfg('default_region')));
        }
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKCityList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_CITY", $addr->getKCityList(getcfg('default_region')));
        }
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKNPList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_NP", $addr->getKNPList(getcfg('default_region')));
        }
        if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getScList")) {
            $addr = new adm_sc_plugin();
            $tpl->setVariable("SC_OPTIONS", $addr->getScList());
        }

        $tpl->setVariable("TASK_FROM", adm_ticket_sources_plugin::getOptions());
        /*if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getStreetList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_STREET", $addr->getStreetList(getcfg('default_region')));
        } */

        //$tpl->setVariable("FILTER_K_REGION", $this->getKRegionList($currentCode));
        //$tpl->setVariable("FILTER_K_AREA", $this->getKAreaList($currentCode));
        //$tpl->setVariable("FILTER_K_CITY", $this->getKCityList($currentCode));
        //$tpl->setVariable("FILTER_K_NP", $this->getKNPList($currentCode));
        //$tpl->setVariable("FILTER_K_STREET", $this->getStreetList($currentCodeStreet));

        UIHeader($tpl);
        $tpl->show();
    }

    function saveticket(Request $request)
    {
        global $DB;

        $eq1 = join("; ", $_REQUEST['internet_equipment']);
        $eq2 = join("; ", $_REQUEST['ctv_equipment']);
        $eq3 = join("; ", $_REQUEST['internet_plus_tv_equipment1']);
        $eq4 = join("; ", $_REQUEST['internet_plus_tv_equipment2']);

        $err = array();
        if (!$_POST['cnt_id']) {
            $err[] = "Не выбран контрагент";
        }
        if (!$_POST['type_id']) {
            $err[] = "Не выбран вид работ";
        }
        if (!$_POST['clnt_type']) {
            $err[] = "Не выбран тип клиента";
        }
        if ($_POST['clnt_type'] == User::CLIENT_TYPE_YUR && !$_POST['clnt_org_name']) {
            $err[] = "Не заполнено название организации";
        }
        elseif (!$_POST['clnt_fio'] && $_POST['clnt_type'] == User::CLIENT_TYPE_PHYS) {
            $err[] = "Не заполнено ФИО клиента";
        }
        if (sizeof($err)) {
            UIError($err);
        }

        $dom_id = 0;
        $ispoln_ids = explode(',', $_POST['ispoln_id']);

        $sql = "SELECT sc_id FROM `link_sc_area` WHERE area_id=" . $DB->F($_POST["area_id"]) . ";";

        $sc_id = ($_POST['sc_id'] > 0) ? intval($DB->getField($sql)) : null;

        if (!$_POST["addr"]) {
            $_POST["comment"] = "Автомат. из заявки #" . intval($DB->getField("SELECT COUNT(id) FROM `tasks` WHERE 1;") + 1);

            if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "saveT")) {
                $addr = new addr_interface_plugin();
                $dom_id = $addr->saveT($request);
                if (!$dom_id) {
                    UIError("Ошибка создания адреса!");
                }
            } else {
                UIError("Нет модуля ADDR_INTERFACE");
            }
        } else {
            $dom_id = $_POST["addr"];
        }

        //// проверка на заполнение обязательных полей:
        if (
            (strlen($_POST['internet_ap']) == 0)
            && (strlen($_POST['ctv_ap']) == 0)
            && (strlen($_POST['internet_plus_tv_ap']) == 0)
            && (strlen($_POST['phone_ap']) == 0)
        ) {
            UIError("В новой заявке должен быть заполнен хотя бы один подключаемый тариф");
        }

        $task_id = TelemarketingTicket::createTicket($_POST['cnt_id'],
            $_POST['type_id'],
            $dom_id,
            $_POST['pod'],
            $_POST['etazh'],
            $_POST['kv'],
            $_POST['domofon'],
            $_POST['clnt_type'],
            $_POST['clnt_org_name'],
            $_POST['clnt_fio'],
            $_POST['clnt_passp_sn'],
            $_POST['clnt_passport_vid'],
            $_POST['clnt_passport_viddate'],
            $_POST['clnt_phone1'],
            $_POST['clnt_phone2'],
            $ispoln_ids,
            $_POST['cmm'],
            $_POST['addinfo'],
            $_POST["agr_id"],
            $_POST["swip"],
            $_POST["swport"],
            $_POST["swplace"],
            $_POST["clntopagr"],
            $_POST["clnttnum"],
            $sc_id,
            $_POST["orient_price"],
            $_POST["task_from"],
            $_POST["exttype"]);
        $slotBegin = $_POST["date_from"] ? substr($_POST["date_from"], 6, 4) . "-" . substr($_POST["date_from"], 3, 2) . "-" . substr($_POST["date_from"], 0, 2) : "";
        $slotBegin .= ($_POST["time_from"] ? " " . $_POST["time_from"] . ":00" : " 00:00:00");
        $slotEnd = $_POST["date_to"] ? substr($_POST["date_to"], 6, 4) . "-" . substr($_POST["date_to"], 3, 2) . "-" . substr($_POST["date_to"], 0, 2) : "";
        $slotEnd .= ($_POST["time_to"] ? " " . $_POST["time_to"] . ":00" : " 00:00:00");

        $ticket = new TelemarketingTicket($task_id);
        if ($ticket && $slotBegin) {
            $ticket->setSlotBegin($slotBegin);
            $ticket->updateCommit();
        }
        if ($ticket && $slotEnd) {
            $ticket->setSlotEnd($slotEnd);
            $ticket->updateCommit();
        }
        if (!empty($_FILES)) {
            $cm = new comments_plugin();
            $_POST['task_id'] = $task_id;
            $cm->addcmm(true);
        }

        if ($task_id > 0) {
            foreach ($this->extra_keys as $key => $val) {
                if ($val == 'task_id') {
                    $ticket->service_ticket_extras[$val] = $task_id;
                } else {

                    if ($val == 'internet_equipment') {
                        $ticket->service_ticket_extras[$val] = $eq1;
                    } elseif ($val == 'ctv_equipment') {
                        $ticket->service_ticket_extras[$val] = $eq2;
                    } elseif ($val == 'internet_plus_tv_equipment1') {
                        $ticket->service_ticket_extras[$val] = $eq3;
                    } elseif ($val == 'internet_plus_tv_equipment2') {
                        $ticket->service_ticket_extras[$val] = $eq4;
                    } elseif( '_id' === substr($val, -3) ) {
                        $ticket->service_ticket_extras[$val] = ($request->request->getInt($val) < 1 ? null : $request->request->getInt($val));
                    } else {
                        $ticket->service_ticket_extras[$val] = $_POST[$val];
                    }
                }
            }
            $ticket->updateServiceTicketExtras($task_id);
            $extra_id = $DB->insert_id();

            $task = $this->getEm()->getReference(\models\Task::class, $task_id);
            $this->getEm()->refresh($task);
            $event = new \WF\Task\Events\TaskCreatedEvent( $task );
            $this->getDispatcher()->dispatch(\WF\Task\Events\TaskCreatedEvent::NAME, $event);
        }

        redirect($this->getLink('viewticket', "task_id=$task_id"));
    }

    function checkticketexists_ajax()
    {
        global $DB, $USER;
        //if(!$_REQUEST['type_id']) $err[] = "Не выбран вид работ";
        if (!$_REQUEST["addr"]) $err[] = "Не указан существующий адрес";

        $sql = "SELECT t.task_id, (SELECT s.name FROM `task_status` AS s WHERE s.is_active=1 AND s.id IN (SELECT tts.status_id FROM `tasks` AS tts WHERE tts.id=t.task_id)) AS status, (SELECT `contr_title` FROM `list_contr` WHERE `id`=t.cnt_id) AS kontr, (SELECT `title` FROM `task_types` WHERE `id`=t.task_type) AS worktype, (SELECT `plugin_uid` FROM `tasks` WHERE `id`=t.task_id) AS tasktype FROM `tickets` AS t WHERE t.dom_id=" . $DB->F($_REQUEST["addr"]) . ";";
        $task = $DB->query($sql, true);
        if ($DB->errno()) $err[] = $DB->error();
        if ($DB->num_rows()) {
            $ret["result"] = "fail";
            //$ret["task_id"] = $task;
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $tpl->loadTemplatefile($USER->getTemplate() . "/ex_addr_tickets.tmpl.htm");
            while ($r = $DB->fetch(true)) {
                if ($r["status"]) {
                    $tpl->setCurrentBlock("tlist");
                    $link = link2($r["tasktype"] . "/viewticket?id=" . $r["task_id"]);
                    $tpl->setVariable("TASKLINK", $link);
                    $tpl->setVariable("TASKID", $r["task_id"]);
                    $tpl->setVariable("TASKTYPE", $r["worktype"]);
                    $tpl->setVariable("CONTR", $r["kontr"]);
                    $tpl->setVariable("STATUS", $r["status"]);
                    $tpl->parse("tlist");
                }
            }
            $ret["tpl"] = $tpl->get();
        } else {
            $ret["result"] = "ok";

        }
        if (sizeof($err)) {
            $ret["result"] = "error";
        }
        #error_log("checkticketexists_ajax: \n".print_r($ret,true)."\n\n\n",3,"/tmp/checkticketexists_ajax.log");
        echo json_encode($ret);
        return false;
    }

    function vieworderdoc()
    {
        global $DB, $USER;
        if ($id = $_REQUEST["id"]) {
            $ticket = new TelemarketingTicket($id);
            $sql = "SELECT gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.task_id=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->num_rows()) {
                list($c_date, $startTime, $user) = $DB->fetch();
                $DB->free();
            }
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $tpl->loadTemplatefile($USER->getTemplate() . "/orderdoc.tmpl.htm");
            $tpl->setVariable("TASK_ID", $id);
            $tpl->setVariable("OD_NUM", $id);
            $tpl->touchBlock('client_type_' . $ticket->getClientType());
            if ($ticket->getClientType() != User::CLIENT_TYPE_PHYS) {
                $tpl->setVariable("OD_CLNT_INFO", $ticket->getOrgName() . "<br />" . $ticket->getFio());
            } else {
                $tpl->setVariable("OD_CLNT_INFO", $ticket->getFio());
            }
            $tpl->setVariable("OD_CONTRAGENT", $ticket->getContrName());
            $tpl->setVariable("OD_WTYPE", $ticket->getTypeName());
            $phnes = $ticket->getPhones();
            if ($USER->checkAccess("hidetelnum")) {
                $tpl->setVariable("OD_CLNT_TEL", "---");
            } else {
                $tpl->setVariable("OD_CLNT_TEL", ($phnes[0] ? $phnes[0] : '+7') . "<br />" . ($phnes[1] ? $phnes[1] : '+7'));
            }
            $tpl->setVariable("OD_SC", $this->getSc($ticket->getSCId()));
            $tpl->setVariable("OD_AREA", $ticket->getArea());
            $tpl->setVariable("OD_EMPL", @$user ? $user : "---");
            $ods = $this->getOds($ticket->getDomId());
            $tpl->setVariable("OD_ODS", $ods ? $ods["title"] . " " . $ods["tel"] : "---");
            $tpl->setVariable("OD_CREATEDATE", rudate("d M Y H:i"));
            $tpl->setVariable("OD_ADDR", $ticket->getAddr($ticket->getDomId()));
            if (@$c_date && @$startTime) {
                $startHour = (strlen(floor($startTime / 60)) == 1) ? "0" . (floor($startTime / 60)) : floor($startTime / 60);
                $startMin = (strlen(floor($startTime % 60)) == 1) ? "0" . (floor($startTime % 60)) : floor($startTime % 60);
                $tpl->setVariable("OD_GFXTIME", rudate("d M Y", strtotime($c_date)) . " " . $startHour . ":" . $startMin);
            } else $tpl->setVariable("OD_GFXTIME", "---");
            $tpl->setVariable("OD_ADDR_ENTR", $ticket->getPod());
            $tpl->setVariable("OD_CNLT_AGRNUM", $ticket->getClntOpAgr());
            $tpl->setVariable("OD_ADDR_DOMOFON", $ticket->getDomofon());
            $tpl->setVariable("OD_TASK_DEVPLACE", $ticket->getSwPlace());
            $tpl->setVariable("OD_ADDR_FLOOR", $ticket->getEtazh());
            $tpl->setVariable("OD_TASK_IP", $ticket->getSwIp());
            $tpl->setVariable("OD_ADDR_FLAT", $ticket->getKv());
            $tpl->setVariable("OD_TASK_PORT", $ticket->getSwPort());
            $tpl->setVariable("OD_ADDINFO", $ticket->getAddInfo());

            $agr_id = $ticket->getAgrId(false);
            if ($agr_id) {
                if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getAgrById")) {
                    $kontr = new kontr_plugin();
                    $agr_contacts = $kontr->getAgrById($agr_id);
                    if ($agr_contacts) {
                        $tpl->setVariable("OD_CONTR_ACCESS", $agr_contacts['access_contact'] ? $agr_contacts['access_contact'] : "<strong>-</strong>");
                        $tpl->setVariable("OD_CONTR_TP", $agr_contacts['tp_contact'] ? $agr_contacts['tp_contact'] : "<strong>-</strong>");

                    } else {
                        $tpl->setVariable("OD_CONTR_ACCESS", "<strong>ошибка</strong>");
                        $tpl->setVariable("OD_CONTR_TP", "<strong>ошибка</strong>");
                    }
                }
            } else {

                $tpl->setVariable("OD_CONTR_ACCESS", "<strong>-</strong>");
                $tpl->setVariable("OD_CONTR_TP", "<strong>-</strong>");
            }
            if (isset($_REQUEST['printcomments'])) {
                $tpl->setCurrentBlock("print-comments");
                $tpl->setVariable("C_OD_NUM", $id . " / " . $ticket->getClntTNum());
                $tpl->setVariable("C_COMMENTS_LIST", $ticket->getCommentsPrint());
                $tpl->parse("print-comments");
            }
            UIHeader($tpl);
            $tpl->show();
        } else {
            UIError("Ошибка! Не указан идентификатор заявки для печати Заказ-наряда!");
        }
        return false;

    }

    function updateticket(Request $request)
    {
        global $DB, $USER;

        $ticket = new TelemarketingTicket($_REQUEST['task_id']);
        $LKUser = adm_users_plugin::isLKUser($USER->getId());
        $err = array();
        //if(!$_POST['sc_id']) $err[] = "Не выбран сервисный центр";
        if ($ticket->getClientType() == User::CLIENT_TYPE_YUR && !$_POST['org_name']) $err[] = "Не заполнено название организации";
        elseif (!$_POST['fio'] && $ticket->getClientType() == User::CLIENT_TYPE_PHYS) $err[] = "Не заполнено ФИО клиента";
        if (sizeof($err)) UIError($err);

        //// EF: сохранение изменений в дополнительных параметрах заявки
        if ($_REQUEST['task_id'] > 0) {
            foreach ($this->extra_keys as $key => $val) {
                if ($val == 'task_id') {
                    $ticket->service_ticket_extras[$val] = $_REQUEST['task_id'];
                } else {
                    $ticket->service_ticket_extras[$val] = $_POST[$val];
                }
            }
            #error_log("tm_module->saveticket: ".print_r($_POST,true)."\n".print_r($ticket->service_ticket_extras,true)."\n",3,"/ramdisk/sql.log");
            $ticket->updateServiceTicketExtras($_REQUEST['task_id']);
            $extra_id = $DB->insert_id();
        }

        $comment = "";
        if ($_POST['sc_id'] && $ticket->getSCId() != $_POST['sc_id'])
            $comment .= "Сервисный центр: " . $this->getSCName($ticket->getSCId()) . " -> " . $this->getSCName($_POST['sc_id']) . "\r\n";
        /*if($ticket->getPod(false) != $_POST['pod'])
            $comment .= "Подъезд: ".$ticket->getPod(false)." -> ".$_POST['pod']."\r\n";
        if($ticket->getEtazh(false) != $_POST['etazh'])
            $comment .= "Этаж: ".$ticket->getEtazh(false)." -> ".$_POST['etazh']."\r\n";
        if($ticket->getKv(false) != $_POST['kv'])
            $comment .= "Квартира: ".$ticket->getKv(false)." -> ".$_POST['kv']."\r\n";
        if($ticket->getDomofon(false) != $_POST['domofon'])
            $comment .= "Код домофона: ".$ticket->getDomofon(false)." -> ".$_POST['domofon']."\r\n";*/
        if ($ticket->getClientType() == User::CLIENT_TYPE_YUR && $ticket->getOrgName(false) != $_POST['org_name'])
            $comment .= "Название организации: " . $ticket->getOrgName(false) . " -> " . $_POST['org_name'] . "\r\n";
        if ($ticket->getFio(false) != $_POST['fio'])
            $comment .= "ФИО: " . $ticket->getFio(false) . " -> " . $_POST['fio'] . "\r\n";
        if ($ticket->getLineContact(false) != $_POST['clnt_line_fio'])
            $comment .= "Владелец линии: " . $ticket->getLineContact(false) . " -> " . $_POST['clnt_line_fio'] . "\r\n";
        if ($ticket->getPasspSN(false) != $_POST['passp_sn'])
            $comment .= "Паспорт (серия номер): " . $ticket->getPasspSN(false) . " -> " . $_POST['passp_sn'] . "\r\n";
        if ($ticket->getPasspVidan(false) != $_POST['passport_vid'])
            $comment .= "Паспорт (кем выдан): " . $ticket->getPasspVidan(false) . " -> " . $_POST['passport_vid'] . "\r\n";
        if ($ticket->getPasspVidanDate() != $_POST['passport_viddate'])
            $comment .= "Паспорт (дата выдачи): " . $ticket->getPasspVidanDate(false) . " -> " . $_POST['passport_viddate'] . "\r\n";
        $phones = $ticket->getPhones();
        if ($phones[0] != preg_replace("/[^0-9]/", "", $_POST['phone1']))
            $comment .= "Телефон (1): {$phones[0]} -> {$_POST['phone1']}\r\n";
        if ($phones[1] != preg_replace("/[^0-9]/", "", $_POST['phone2']))
            $comment .= "Телефон (2): {$phones[1]} -> {$_POST['phone2']}\r\n";
        if ($ticket->getSwIp(false) != $_POST['swip'])
            $comment .= "IP-адрес: " . $ticket->getSwIp(false) . " -> " . $_POST['swip'] . "\r\n";
        if ($ticket->getSwPort(false) != $_POST['swport'])
            $comment .= "Порт: " . $ticket->getSwPort(false) . " -> " . $_POST['swport'] . "\r\n";
        if ($ticket->getSwPlace(false) != $_POST['swplace'])
            $comment .= "Расположение свитча: " . $ticket->getSwPlace(false) . " -> " . $_POST['swplace'] . "\r\n";
        if ($ticket->getClntOpAgr(false) != $_POST['clntopagr'])
            $comment .= "Номер договора оператора с клиентом: " . $ticket->getClntOpAgr(false) . " -> " . $_POST['clntopagr'] . "\r\n";
        if ($ticket->getClntTNum(false) != $_POST['clnttnum'])
            $comment .= "Номер заявки клиента в базе оператора: " . $ticket->getClntTNum(false) . " -> " . $_POST['clnttnum'] . "\r\n";
        if (!$LKUser) {
            if ($ticket->getOrientPrice(false) != $_POST['orient_price'])
                $comment .= "Ориентировочная стоимость работ: " . $ticket->getOrientPrice(false) . " -> " . $_POST['orient_price'] . "\r\n";
            if ($ticket->getPrice(false) != $_POST['price'])
                $comment .= "Cтоимость работ: " . $ticket->getPrice(false) . " -> " . $_POST['price'] . "\r\n";
        }
        if ($ticket->getTaskFrom(false) != $_POST['task_from'])
            $comment .= "Откуда узнали: " . adm_ticket_sources_plugin::getById($ticket->getTaskFrom(false)) . " -> " . adm_ticket_sources_plugin::getById($_POST['task_from']) . "\r\n";
        if ($ticket->getTicketDocNum(false) != $_POST['ticket_doc_num'])
            $comment .= "Номер квитанции: " . $ticket->getTicketDocNum(false) . " -> " . $_POST['ticket_doc_num'] . "\r\n";
        if ($ticket->getAddInfo() != $_POST["addinfo"]) {
            $comment .= "Примечание: " . $ticket->getAddInfo() . " -> " . $_POST['addinfo'] . "\r\n";
        }
        //die("Current: ".$ticket->getExtType(false) ." NEW: ".$_POST["exttype"]);
        if ($ticket->getExtType(false) != $_POST['exttype'])
            $comment .= "Подтип заявки: " . adm_ticket_rtsubtypes_plugin::getById($ticket->getExtType(false)) . " -> " . adm_ticket_rtsubtypes_plugin::getById($_POST['exttype']) . "\r\n";

        if (isset($_POST["donedate"]) && preg_match("/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/", $_POST["donedate"])) {
            if ($ticket->getDoneDate() != $_POST["donedate"]) {
                $comment .= "ОБНОВЛЕНА ДАТА ВЫПОЛНЕНИЯ: " . $ticket->getDoneDate() . " -> " . $_POST['donedate'] . "\r\n";
                if (!$ticket->setDoneDate($_POST["donedate"])) UIError("Ошибка обновления ДАТЫ ВЫПОЛНЕНИЯ!");
            }
        }

        $taskEntity = $this->getEm()->find(\models\Task::class, $request->get('task_id'));

        $sc_id = ($_POST['sc_id'] > 0) ? $_POST['sc_id'] : null;
        $ticket->setSCId($sc_id);

        if ($request->request->getInt('area_id') !== $ticket->getAreaId()) {
            /** @var \models\Ticket $ticketEntity */
            $ticketEntity = $taskEntity->getAnyTicket();
            if (!$ticketEntity instanceof \WF\Task\Model\TechnicalTicketInterface)
                throw new Exception('Wrong ticket type!');

            $comment .= "Район: " . $ticket->getArea() . " -> " . adm_areas_plugin::getArea($request->get('area_id')) . "\r\n";

            $ticketEntity->getDom()->setAreaId($request->get('area_id'));
            $this->getEm()->persist($ticketEntity);
        }

        /*$ticket->setPod($_POST['pod']);
        $ticket->setEtazh($_POST['etazh']);
        $ticket->setKv($_POST['kv']);
        $ticket->setDomofon($_POST['domofon']);*/
        $ticket->setOrgName($_POST['org_name']);
        $ticket->setFio($_POST['fio']);
        $ticket->setLineContact($_POST['clnt_line_fio']);
        $ticket->setPassp($_POST['passp_sn'], $_POST['passport_vid'], $_POST['passport_viddate']);
        $ticket->setPhones($_POST['phone1'], $_POST['phone2']);
        $ticket->setAddInfo($_POST['addinfo']);
        $ticket->setSwIp($_POST["swip"]);
        $ticket->setSwPort($_POST["swport"]);
        $ticket->setSwPlace($_POST["swplace"]);
        $ticket->setClntOpAgr($_POST["clntopagr"]);
        $ticket->setClntTNum($_POST["clnttnum"]);
        $ticket->setOrientPrice($_POST["orient_price"]);
        $ticket->setTaskFrom($_POST["task_from"]);
        $ticket->setPrice($_POST["price"]);
        $ticket->setTicketDocNum($_POST["ticket_doc_num"]);
        $ticket->setExtType($_POST["exttype"]);

        $slotBegin = $_POST["date_from"] ? substr($_POST["date_from"], 6, 4) . "-" . substr($_POST["date_from"], 3, 2) . "-" . substr($_POST["date_from"], 0, 2) : "";
        $slotBegin .= ($_POST["time_from"] ? " " . $_POST["time_from"] . ":00" : " 00:00:00");
        $slotEnd = $_POST["date_to"] ? substr($_POST["date_to"], 6, 4) . "-" . substr($_POST["date_to"], 3, 2) . "-" . substr($_POST["date_to"], 0, 2) : "";
        $slotEnd .= ($_POST["time_to"] ? " " . $_POST["time_to"] . ":00" : " 00:00:00");

        $ticket->setSlotBegin($slotBegin);
        $ticket->setSlotEnd($slotEnd);


        if ($ticket->updateCommit()) {
            if ($comment) $ticket->addComment($comment, "Обновлены данные");
        }
        if (sizeof($err)) UIError($err);

        redirect($this->getLink('viewticket', "task_id=" . $ticket->getId()));
    }

    static function getContrOptions($sel_id = 0)
    {
        global $DB;
        $sql = "SELECT `id`, `contr_title` FROM `list_contr` WHERE contr_type_tm > 0 ORDER BY `contr_title`";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getPodrOptions($sel_id = 0)
    {
        global $DB;
        $sql = "SELECT `id`, `contr_title` FROM `list_contr` AS lc JOIN `podr` AS p ON lc.id=p.contr_id WHERE p.user_id ORDER BY `contr_title`";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getEmplOptions($sel_id = 0)
    {
        global $DB, $USER;
        $sql_add = "";
        if ($isChiefTech = User::isChiefTech($_SESSION["user"]["id"]) && !$USER->checkAccess("fullaccesstoallsc", User::ACCESS_WRITE)) {
            $sql_add = " AND user.id IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($isChiefTech) . ")";
        }

        $sql = "SELECT user.id, user.fio FROM `list_empl` AS empl LEFT JOIN `users` AS user ON user.id=empl.user_id WHERE user.active='1' " . $sql_add . " ORDER BY user.fio";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getContrName($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT `contr_title` FROM `list_contr` WHERE `id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function getContrID($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT `cnt_id` FROM `tickets` WHERE `task_id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function getTypeID($id, $escape = true)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `task_type` FROM `tickets` WHERE `task_id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function getTypeOptions($sel_id = 0)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`='tm_module' ORDER BY `title`";

        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getTypeName($id, $escape = true)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `title` FROM `task_types` WHERE `plugin_uid`=" . $DB->F($plugin_uid) . " AND `id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }


    static function getTypeDuration($id, $escape = true)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `duration` FROM `task_types` WHERE `plugin_uid`=" . $DB->F($plugin_uid) . " AND `id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function getSCOptions($sel_id = 0)
    {
        global $DB, $USER;
        $sql = "SELECT
            s.id, s.title
            FROM list_sc s,
            user_sc_access usa
            where s.id = usa.sc_id
              and usa.user_id = ".$USER->getId()."
            ORDER BY s.title";
        $result = $DB->getCell2($sql);
        $result = ['-1' => '-- не указан'] + $result;
        return array2options($result, $sel_id, false);
    }

    static function getODSOptions($sel_id = 0)
    {
        global $DB;
        $sql = "SELECT `id`, `title` FROM `list_ods` ORDER BY `title`";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }


    static function getSC($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_sc` WHERE `id`=" . $DB->F($id) . ";";
        return $DB->getField($sql);
    }

    static function getSCName($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_sc` WHERE `id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }


    static function statusCallbackGet($task_id, $status)
    {
        global $DB, $USER;

        if ((false !== strpos($status['tag'], 'cmmpodr'))) {

            if ($USER->checkAccess('tm_module2podr', User::ACCESS_WRITE) && self::getPodrId($task_id)) return $status['name'];
            else return false;
        }
        if ((false !== strpos($status['tag'], 'otlozh'))) {
            return $status['name'] . " до <input type='text' name='otlozh_date' class='datetime_input'>";
        }

        if ((false !== strpos($status['tag'], 'done'))) {
            $t = new TelemarketingTicket($task_id);
            if ($t->getTypeName() != "СКП") {
                return $status['name'];
            } else {
                $user_id = $t->getTaskIspolnIds($task_id);
                if (count($user_id)) {
                    $user_id = $user_id[0];
                    $gfx_user_id = $DB->getField("SELECT `empl_id` FROM `gfx` WHERE `task_id`=" . $DB->F($task_id));
                    if ($user_id == $gfx_user_id) {
                        return $status['name'];
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }

        $err = array();

        $plugin_uid = basename(__FILE__, '.php');
        if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejectReasonsList")) {
            $rej = new adm_rejects_plugin();
            if ($status['tag'] != '') {
                $rejReasons = $rej->getRejectReasonsList(false, $plugin_uid, $status['tag']);
                if ($rejReasons !== FALSE) {
                    return $status['name'] . " <select style=\"margin-left: 10px;width:150px;\" name='reject_" . $status["tag"] . "_id'>" . $rejReasons . "</select>";
                }
            }
        } else {
            $err[] = 'Отсутствует класс Причин отказа!';
        }
        return sizeof($err) ? $err : $status['name'];
    }

    static function statusCallbackSet($task_id, &$status, &$text, &$tag)
    {
        global $DB, $_config, $USER;

        $err = array();
        $contr_status_info = '';
        $task = new Task($task_id);
        if ($status["id"] == "44" || $status["id"] == "45") {
            $c = $DB->getField("SELECT `task_id` FROM `gfx` WHERE `task_id`=" . $DB->F($task_id) . ";");
            if ($c !== FALSE) {
                //$DB->query("DELETE FROM `gfx` WHERE `task_id`=".$DB->F($task_id).";");
                //$task->addComment("", "Автоматическое удаление из графика");
            }
        }
        if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()) && ($status["id"] == 12 || $status["id"] == 46)) {

            /*$u = $DB->getRow("SELECT * FROM `users` WHERE `fio`='Оператор ГСС';", true);
                                //adm_users_plugin::getUser($ticketAuthor);
                                //sendSMS(smsCleanPhone($phone), "Отказ от заявки с ID: ".$task->getId()." зафиксирован.");
            if ($u["id"]) {
                $task->setIspoln($u["id"]);
            } else {
                $task->addComment("Не возможно определить диспетчера задачи", "Ошибка");
            }*/


            /*$ticketAuthor = !!$task->getAuthorId() ? $task->getAuthorId() : $DB->getField("SELECT `user_id` FROM `task_comments` WHERE `task_id`=".$DB->F($task->getId())." AND `status_id`=44 AND `id` ORDER BY `datetime` DESC;");
            if (!!$ticketAuthor) {
                $task->setIspoln($ticketAuthor);
                $task->addComment("Установлен ответственный ID: ".$ticketAuthor);
            } else {
                $task->addComment("Не возможно определить диспетчера задачи", "Ошибка");
            }*/
        }

        if (false !== strpos($status['tag'], 'otlozh')) {
            if (!$_POST['otlozh_date']) $err[] = "Нет даты, до которой отложить";
            else {
                $task = new Task($task_id);
                if ($task->setDateFn($_POST['otlozh_date'])) {
                    $atag = explode(";", $tag);
                    $atag[0] .= " до {$_POST['otlozh_date']}";
                    $tag = implode("; ", $atag);
                }
            }
        }
        $reject_id = $_POST["reject_" . $status["tag"] . "_id"] ? $_POST["reject_" . $status["tag"] . "_id"] : false;
        if ($reject_id) {
            $sql = "UPDATE `tickets` SET `reject_id`=" . $DB->F($reject_id) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) return false;
            $DB->free();
            if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejReasonTitle")) {
                $rej = new adm_rejects_plugin();
                $tag .= ($tag ? ', ' : '') . 'Причина <b>' . $rej->getRejReasonTitle($reject_id) . '</b>';
                $contr_status_info = $rej->getRejReasonTitle($reject_id);
            } else return false;
        } else {
            $sql = "UPDATE `tickets` SET `reject_id`=" . $DB->F("0") . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) return false;
            $DB->free();
        }
        $price = $DB->getField("SELECT `price` FROM `tickets` WHERE `task_id`=" . $DB->F($task_id) . ";");
        if ($_POST['grafik_date'] && preg_match("/grafik/", $status['tag'])) {
            $contr_status_info = $_POST['grafik_date'];
        }

        $ticket = new TelemarketingTicket($task_id);

        $lsnum = $ticket->getClntTNum();
        return sizeof($err) ? $err : true;
    }

    static function getOds($dom_id)
    {
        global $DB;
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getODSId")) {
            $addr = new addr_interface_plugin();
            $ods_id = $addr->getODSId($dom_id);
        } else return false;
        $sql = "SELECT * FROM `list_ods` WHERE `id`=" . $DB->F($ods_id) . ";";
        return $DB->getRow($sql, true);
    }

    static function getPodrId($task_id)
    {
        global $DB;
        $sql = "SELECT `contr_id` FROM `podr_tickets` WHERE `task_id`=" . $DB->F($task_id);
        return $DB->getField($sql, false);
    }

    static function acceptticket_ajax()
    {
        global $DB, $USER;
        $id = $_REQUEST["task_id"];
        if (!$id) {
            $ret["error"] = "Не указан идентификатор заявки!";
            echo json_encode($ret);
            return false;
        }
        $t = new Task($id);
        $status_id = $t->getStatusId();
        if ($status_id == 44) {
            if ($t->addComment("", "Заявка принята инженером " . $USER->getFio() . ". Статус: <b>Назначение в график</b>", 45)) {
                ob_start();
                $t->toMail();
                $ret["error"] = ob_get_contents();
                ob_end_clean();
                $ret["ok"] = "ok";
            } else {
                $ret["error"] = "Ошибка смены статуса заявки!";
            }

        } else {
            $ret["error"] = "Заявка находится в некорректном статусе!";

        }
        echo json_encode($ret);
        return false;

    }

    static function rejectticket_ajax()
    {
        global $DB, $USER;
        $id = $_REQUEST["task_id"];
        if (!$id) {
            $ret["error"] = "Не указан идентификатор заявки!";
            echo json_encode($ret);
            return false;
        }
        $t = new Task($id);
        $status_id = $t->getStatusId();
        if ($status_id == 44) {
            $DB->query("DELETE FROM `gfx` WHERE `task_id`=" . $DB->F($id) . " AND `empl_id`=" . $DB->F($USER->getId()) . ";");

            $DB->query("UPDATE `task_users` SET `ispoln`=0 WHERE `task_id`=" . $DB->F($id) . " AND `user_id`=" . $DB->F($USER->getId()) . ";");
            $DB->query("DELETE FROM `task_users` WHERE `task_id`=" . $DB->F($id) . " AND `user_id`=" . $DB->F($USER->getId()) . ";");
            if ($t->addComment("Причина: " . $_REQUEST["reject_reason"], "Заявка отклонена инженером " . $USER->getFio() . ". Статус: <b>Новая</b>", 8)) {
                $ret["ok"] = "ok";
            } else {
                $ret["error"] = "Ошибка смены статуса заявки!";
            }

        } else {
            $ret["error"] = "Заявка находится в некорректном статусе!";

        }
        echo json_encode($ret);
        return false;

    }

    static function getPriceItemsByTask($task_id)
    {
        global $DB, $USER;
        if (!$task_id) return false;
        $t = new TelemarketingTicket($task_id);
        $sql = "SELECT tc.wt_id AS keyval, tc.price*tc.quant AS price, (SELECT `contrcode` FROM `list_skp_p_price` WHERE `contr_id`=" . $DB->F($t->getContrId()) . " AND `wt_id`=tc.wt_id) AS contr_code FROM `list_skp_tech_calc` AS tc WHERE tc.task_id=" . $DB->F($task_id) . ";";
        $result = $DB->getCell3($sql, true);
        return $result;

    }

}