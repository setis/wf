<?php

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;


class reports_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function main()
    {
        global $DB, $USER;


        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");

        $fail = true;
        if (!$USER->checkAccess('reports_contr')) $tpl->setVariable('DIS_1', 'disabled'); else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_contr_rejects')) $tpl->setVariable('DIS_2', 'disabled'); else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_task_types')) $tpl->setVariable('DIS_3', 'disabled'); else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_ser_contr')) $tpl->setVariable('DIS_4', 'disabled'); else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_ser_contr_rejects')) $tpl->setVariable('DIS_5', 'disabled'); else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_ser_task_types')) $tpl->setVariable('DIS_6', 'disabled'); else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_op_conn')) $tpl->setVariable('DIS_7', 'disabled'); else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_tt_gp_types')) $tpl->setVariable('DIS_9', 'disabled'); else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('acts_tt'))
            $tpl->setVariable('REPORT_ACTS_TT', 'disabled');
        else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_tt_gp')) $tpl->setVariable('DIS_10', 'disabled'); else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_f_gp_conn')) $tpl->setVariable('DIS_11', 'disabled'); else {
            $tpl->setVariable("VISIBLE1", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_f_contr')) $tpl->setVariable('DIS__1', 'disabled'); else {
            $tpl->setVariable("VISIBLE2", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_f_total')) $tpl->setVariable('DIS__4', 'disabled'); else {
            $tpl->setVariable("VISIBLE2", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_f_total_tt')) $tpl->setVariable('DIS__5', 'disabled'); else {
            $tpl->setVariable("VISIBLE2", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_f_tech42')) $tpl->setVariable('DIS__522', 'disabled'); else {
            $tpl->setVariable("VISIBLE2", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_f_sc42')) $tpl->setVariable('DIS__612', 'disabled'); else {
            $tpl->setVariable("VISIBLE2", "visible");
            $fail = false;
        }


        if (!$USER->checkAccess('reports_f_skp_avg_price')) $tpl->setVariable('DIS_R_1', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_f_skp_mr')) $tpl->setVariable('DIS_R_2', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_f_skp_total')) $tpl->setVariable('DIS_R_3', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_f_skp_agents')) $tpl->setVariable('DIS_R_4', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_f_skp_total_mod')) $tpl->setVariable('DIS_R_5', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_f_skp_cons')) $tpl->setVariable('DIS_R_6', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_f_skp_dsnal')) $tpl->setVariable('DIS_R_7', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_f_skp_total2')) $tpl->setVariable('DIS_R_8', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_f_skp_card')) $tpl->setVariable('DIS_R_9', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_f_skp_accn')) $tpl->setVariable('DIS_R_10', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_f_skp_accbn')) $tpl->setVariable('DIS_R_11', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_f_skp_timeouts')) $tpl->setVariable('DIS_R_12', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_f_skp_wtypes')) $tpl->setVariable('DIS_R_13', 'disabled'); else {
            $tpl->setVariable("VISIBLE3", "visible");
            $fail = false;
        }


        if (!$USER->checkAccess('acts_tmc')) $tpl->setVariable('DIS_R__1', 'disabled'); else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('acts_tmc')) $tpl->setVariable('DIS_R__2', 'disabled'); else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('acts_ticket_services')) $tpl->setVariable('DIS_R__3', 'disabled'); else {
            $tpl->setVariable("VISIBLE4", "visible");
            $fail = false;
        }

        // VISIBLE5 Отчёты ТМЦ
        if (!$USER->checkAccess('reports_tmc_sc_move')) $tpl->setVariable('DIS_B_1', 'disabled'); else {
            $tpl->setVariable("VISIBLE5", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_tmc_sc_total')) $tpl->setVariable('DIS_B_2', 'disabled'); else {
            $tpl->setVariable("VISIBLE5", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_tmc_tech_move')) $tpl->setVariable('DIS_B_3', 'disabled'); else {
            $tpl->setVariable("VISIBLE5", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_tmc_tech_total')) $tpl->setVariable('DIS_B_4', 'disabled'); else {
            $tpl->setVariable("VISIBLE5", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_tmc_ttgp_totask')) $tpl->setVariable('DIS_T_1', 'disabled'); else {
            $tpl->setVariable("VISIBLE5", "visible");
            $fail = false;
        }
        if (!$USER->checkAccess('reports_tmc_ttgp_fromtask')) $tpl->setVariable('DIS_T_2', 'disabled'); else {
            $tpl->setVariable("VISIBLE5", "visible");
            $fail = false;
        }

        if ($USER->checkAccess(\WF\Tmc\Controller\TmcController::class)) {
            $tpl->setVariable("VISIBLE5", "visible");
            $fail = false;
        } else {
            $tpl->setVariable('TMC2_REPORT_DISABLED', 'disabled');
        }

        if ($USER->checkAccess(\WF\Tmc\Controller\TmcController::class)) {
            $tpl->setVariable("VISIBLE5", "visible");
            $fail = false;
        } else {
            $tpl->setVariable('TMC2_REPORT_TOTALS_DISABLED', 'disabled');
        }

        // VISIBLE6 Счета
        if (!$USER->checkAccess('reports_b_pays')) $tpl->setVariable('DIS_A_1', 'disabled'); else {
            $tpl->setVariable("VISIBLE6", "visible");
            $fail = false;
        }

        // VISIBLE8 Отчеты по Контролю качества
        if (!$USER->checkAccess('reports_f_tech_qc')) $tpl->setVariable('DIS_QC_1', 'disabled'); else {
            $tpl->setVariable("VISIBLE8", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_f_tech_rates')) $tpl->setVariable('DIS_QC_2', 'disabled'); else {
            $tpl->setVariable("VISIBLE8", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_f_qc_calls_stats')) $tpl->setVariable('DIS_QC_3', 'disabled'); else {
            $tpl->setVariable("VISIBLE8", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_acts_dynamic')) $tpl->setVariable('DIS_AGR_1', 'disabled'); else {
            $tpl->setVariable("VISIBLE9", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_acts_static')) $tpl->setVariable('DIS_AGR_2', 'disabled'); else {
            $tpl->setVariable("VISIBLE9", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_mgts_b2b')) $tpl->setVariable('DIS_B2B_1', 'disabled'); else {
            $tpl->setVariable("VISIBLE10", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_mgts_b2b_tmc')) $tpl->setVariable('DIS_B2B_2', 'disabled'); else {
            $tpl->setVariable("VISIBLE10", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_sik_b2b'))
            $tpl->setVariable('DIS_B2B_3', 'disabled');
        else {
            $tpl->setVariable("VISIBLE10", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_sik_b2b_tmc'))
            $tpl->setVariable('DIS_B2B_4', 'disabled');
        else {
            $tpl->setVariable("VISIBLE10", "visible");
            $fail = false;
        }

        if (!$USER->checkAccess('reports_indocs')) $tpl->setVariable('DIS_IND_1', 'disabled'); else {
            $tpl->setVariable("VISIBLE11", "visible");
            $fail = false;
        }


        //reports_mgts_b2b


        if ($fail) {
            UIError("У Вас нет доступа ни к одному из отчетов! Обратитесь в Вашему руководителю.", "Недостаточно прав доступа");
        }


        UIHeader($tpl);
        $tpl->show();

    }


}


?>
