<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Справочник СКП: &laquo;Виды работ&raquo;";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник СКП: Виды работ";
    $PLUGINS[$plugin_uid]['events']['getOptList_ajax'] = "Список видов работ";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование вида работ";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранить вид работ";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление вида работ";
    
}
?>