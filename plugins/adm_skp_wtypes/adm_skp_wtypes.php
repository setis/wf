<?php

/**
 * Plugin Implementation
 * 
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;



/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 * 
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
 
class adm_skp_wtypes_plugin extends Plugin
{       
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
       
    function isBusy($id) {
        global $DB;
        $cnt = $DB->getField("SELECT COUNT(id) FROM `list_skp_p_price` WHERE `cat_id`=".$DB->F($id));
        if($cnt!=0) { return true; }
        else return false;
    }
    
    function getWTypeTitle($id) {
        global $DB;
        $sql = "SELECT `title` FROM `list_skp_p_wtypes` WHERE `id`=".$DB->F($id).";"; 
        return $DB->getField($sql);
    
    }
    
    function getOptList($sel_id = false, $cat =false) {
        global $DB;        
        if ($cat) {
            $sql_add = " WHERE `cat_id`=".$DB->F($cat)." ";
        }
        return array2options($DB->getCell2("SELECT `id`, `title` FROM `list_skp_p_wtypes` $sql_add ORDER BY `title`"), $sel_id);        
        
    }
    
    function getOptList_ajax() {
        global $DB;        
        $cat = $_REQUEST["cat"];
        $sel_id = 0;
        if (!$cat) {
            echo "error";
            return;   
        }
        $res = array2options($DB->getCell2("SELECT `id`, `title` FROM `list_skp_p_wtypes`  WHERE `cat_id`=".$DB->F($cat)."  ORDER BY `title`"), $sel_id);        
        if (!$res) {
            echo "error";
        } else {
            echo $res;
        }
        return false;
    }
    
    function getList($tType = false, $cat =false) {
        global $DB;
        if ($cat) {
            $sql_add = " WHERE `cat_id`=".$DB->F($cat)." ";
        }
        return $DB->getCell2("SELECT `id`, `title` FROM `list_skp_p_wtypes` $sql_add ORDER BY `title`");        
        
    }
    
    static function getByID($id) {
        global $DB;
        if (!$id) return false;  
        return $DB->getField("SELECT `title` FROM `list_skp_p_wtypes` WHERE `id`=".$DB->F($id).";");                
    }
    
    static function getCodeByID($id, $cnt_id) {
        global $DB;
        if (!$id || !$cnt_id) return false;  
        return $DB->getField("SELECT `contrcode` FROM `list_skp_p_price` WHERE `contr_id`=".$DB->F($cnt_id)." AND `wt_id`=".$DB->F($id).";");                
    }
    
    
    function viewlist() {
        global $DB, $USER, $PLUGINS;
        
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        if ($USER->getTemplate() != "default") 
            $tpl->loadTemplatefile($USER->getTemplate()."/adm_skp_wtypes.tmpl.htm");
        else 
            $tpl->loadTemplatefile("adm_skp_wtypes.tmpl.htm");
        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_name'] = $this->getCookie('filter_name', $_REQUEST['filter_name']);
        $_REQUEST['filter_cat_id'] = $this->getCookie('filter_cat_id', $_REQUEST['filter_cat_id']);
        $tpl->setVariable('FILTER_ID', $_REQUEST['filter_id']);
        $tpl->setVariable('FILTER_NAME', $_REQUEST['filter_name']);
        $tpl->setVariable('FILTER_CAT_OPTIONS', adm_skp_catnames_plugin::getOptList($_REQUEST['filter_cat_id']));
        if ($_REQUEST['filter_name'] != "") {
            $sql_add[] = " l.title LIKE ".$DB->F("%".$_REQUEST['filter_name']."%")."";
        }
        if ($_REQUEST['filter_cat_id']) {
            $sql_add[] = " l.cat_id=".$DB->F($_REQUEST['filter_cat_id'])."";
        }
        if ($_REQUEST['filter_id'] != "") {
            $sql_add[] = " l.id=".$DB->F($_REQUEST['filter_id'])."";
        }
        if (sizeof($sql_add)) {
            $sql_add = " WHERE ".implode(" AND ", $sql_add);
        }
        $sql = "SELECT l.*, (SELECT `fio` FROM `users` WHERE `id`=l.cr_uid), (SELECT `title` FROM `list_skp_p_edizm` WHERE `id`=l.ei_id), (SELECT `title` FROM `list_skp_p_cattitle` WHERE `id`=l.cat_id) FROM `list_skp_p_wtypes` as l $sql_add ORDER BY `id`;"; 
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            while (list($id, $title, $eizm_id, $cat_id, $cr_uid, $cr_date, $username, $edizm_title, $cattitle) = $DB->fetch()) {
                $tpl->setCurrentBlock("vl_item");
                $tpl->setVariable("VLI_ID", $id);
                $tpl->setVariable("VLI_TITLE", $title);
                $tpl->setVariable("VLI_EDIZM", $edizm_title);
                $tpl->setVariable("VLI_CAT", $cattitle);
                $tpl->setVariable("VLI_AUTHOR", $username);
                $tpl->setVariable("VLI_DATE", rudate("H:i:s d M Y", $cr_date));
                $tpl->parse("vl_item");
            }
        } else {
            $tpl->touchBlock("novlitems");
        }
        UIHeader($tpl);
        $tpl->show();
    }
    
    function edit() {
        global $DB, $PLUGINS, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
            else 
                $tpl->loadTemplatefile("edit.tmpl.htm");
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        
        if($id = $_REQUEST['id']) { 
            $sql = "SELECT * FROM `list_skp_p_wtypes` WHERE `id`=".$DB->F($id).";"; 
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Запись с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);
            $tpl->setVariable("TYPE_ID", $result["id"]);
            $tpl->setVariable("TITLE", $result["title"]);
            
        }
        $sel_ei_ids = adm_skp_edizm_plugin::getOptList($result["ei_id"]);
        if ($sel_ei_ids) $sel_ei_ids = "<select name='ei_id'>".$sel_ei_ids."</select>";
        else UIError("Пустой справочник Единиц Измерения!");
        $tpl->setVariable("EDIZM_LIST", $sel_ei_ids);
        $sel_cat_ids = adm_skp_catnames_plugin::getOptList($result["cat_id"]);
        if ($sel_cat_ids) $sel_cat_ids = "<select name='cat_id'>".$sel_cat_ids."</select>";
        else UIError("Пустой справочник Разделов прайса!");
        $tpl->setVariable("CAT_LIST", $sel_cat_ids);
        UIHeader($tpl);
        $tpl->show();
        
    }
    
    function save() {
        global $DB, $USER;
        $err = array();
        if(!$_POST['title']) $err[] = "Не заполнено поле Название!";
        if(sizeof($err)) UIError($err);
        if($_POST['id']) $sql = "UPDATE `list_skp_p_wtypes` SET `title`=".$DB->F($_POST['title']).", `ei_id`=".$DB->F($_POST['ei_id']).", `cat_id`=".$DB->F($_POST['cat_id'])." WHERE `id`=".$DB->F($_POST['id']).";";
        else {
            $sql = "INSERT INTO `list_skp_p_wtypes` (`title`, `ei_id`, `cat_id`, `cr_uid`) VALUES(".$DB->F($_POST['title']).", ".$DB->F($_POST['ei_id']).", ".$DB->F($_POST['cat_id']).", ".$DB->F($USER->getId()).")";
        }
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error(). "<br />".$sql);
        
        if(!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Вид работ сохранен. ID: ".$_POST['id']);
    }
    
    function delete() {
        global $DB;
        if(!($id = $_REQUEST['id'])) $err[] = "Не указан Вид работ!";  
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST["id"])) UIError("Выбранная запись связана с активными Заявками. Удаление невозможно."); 
        $DB->query("DELETE FROM `list_skp_p_wtypes` WHERE `id`='".$_REQUEST['id']."';");
        redirect($this->getLink(),"Вид работ успешно удален.");        
    }
 
    function getRejectReasonsList($selected = false, $plugin_uid=false, $tag = false) {
        global $DB;
        
        $sql = "SELECT * FROM `list_skp_p_wtypes` WHERE 1 ORDER BY `title`;"; 
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        $ret = false;
        if ($DB->num_rows()) {
            while (list($id, $mod, $tag, $title)=$DB->fetch()) {
                if ($id == $selected)
                    $ret .="<option selected='selected' value='$id' >$title</option>";
                else
                    $ret .="<option value='$id'>$title</option>";
            }
        }
        $DB->free();
        return $ret;
    }
}
?>