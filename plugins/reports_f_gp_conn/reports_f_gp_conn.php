<?php

use classes\HTML_Template_IT;
use classes\Plugin;


class reports_f_gp_conn_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function getwtypelist()
    {
        global $DB;
        $contr_id = $_REQUEST['contr_id'];
        $type = $_REQUEST['type'];
        if ($contr_id === FALSE || $type === FALSE) {
            echo "error";
            return;
        }

        $contr_cond = "";
        if ($contr_id > 0) {
            $contr_cond = $contr_id;
        } else {
            $contr_cond = "ca.contr_id";
        }

        $plugin_list = $DB->F($type);
        if ($type == "all") {
            $plugin_list = "'connections','accidents','gp'";
        }

        $sql = "SELECT DISTINCT wt.id, wt.title FROM `list_contr_agr` AS ca
                LEFT JOIN `link_agr_wtypes` AS law ON law.agr_id=ca.id
                LEFT JOIN `task_types` AS wt ON wt.id=law.wtype_id WHERE ca.contr_id=" . $contr_cond . " AND wt.title IS NOT NULL AND wt.plugin_uid IN (" . $plugin_list . ") ORDER BY wt.title;";
        #echo $sql;
        $options = $DB->getCell2($sql);
        $options[0] = "Все виды работ";

        if ($options) {
            #$opts = array(0 => "Все виды работ");
            echo array2options($options, $_REQUEST['sel_id']);
        } else {
            echo "<option value=\"\">-- нет видов работ --</option>";
        }
    }

    function main()
    {
        global $DB, $USER;
        $fintypes = array("0" => "Все", "1" => "Рассчитанные (Проведенные)", "2" => "Не рассчитанные (Не проведенные)");
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01' => "Январь", '02' => "Февраль", '03' => "Март", '04' => "Апрель", '05' => "Май", '06' => "Июнь", '07' => "Июль", '08' => "Август", '09' => "Сентябрь", '10' => "Октябрь", '11' => "Ноябрь", '12' => "Декабрь");

        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
            else
                $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_TECH_TICKET", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            UIHeader($tpl);
            report_left_menu($tpl);
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));

            ### фильтр по типам заявок
            #$type_ids = array ("all" => "Подключения+ТТ+ГП", "connections" => "Подключения", "accidents" => "ТТ", "gp" => "ГП");
            $type_ids = array("connections" => "Подключения", "accidents" => "ТТ", "gp" => "ГП");
            $tpl->setVariable("FILTER_TYPE_OPTIONS", array2options($type_ids, $_POST["type_id"]));

            $scList = adm_sc_plugin::getSCList2Col();
            #$scList[""] = "-- СЦ не указан --";
            $tpl->setVariable("FILTER_SC_OPTIONS", array2options($scList, $_POST["sc_id"]));
            $tpl->setVariable("FILTER_TECH_OPTIONS", adm_empl_plugin::getTechListSKP($_POST["tech_id"]));
            $this->getSession()->set("filter_status_id", $_REQUEST['filter_status_id'] ? $_REQUEST['filter_status_id'] : $this->getSession()->get("filter_status_id"));
            $_REQUEST['filter_status_id'] = $this->getSession()->get("filter_status_id");
            $tpl->setVariable("FILTER_CNT_OPTIONS", $this->getContrList(@$_POST["cnt_id"]));
            if ($_REQUEST['loadphones'] == "on") {
                $tpl->setVariable("LOADPHONES", "checked='checked'");
            }
            if ($_REQUEST['loadfio'] == "on") {
                $tpl->setVariable("LOADFIO", "checked='checked'");
            }

            if ($_REQUEST['loadgfx'] == "on") {
                $tpl->setVariable("LOADGFX", "checked='checked'");
            }
            #$tpl->setVariable('FILTER_STATUS_OPTIONS', adm_statuses_plugin::getOptList($_REQUEST['filter_status_id'], $_POST["type_id"]));
            $dateParams = array(
                0 => "Дата создания",
                #1 => "Дата выполнения",
                #2 => "Дата изменения",
                3 => "Дата выполнения/Отказа",
                #4 => "Прием денег",
                5 => "Дата Закрытия",
                #6 => "Дата Сдачи отчета",
                #7 => "Дата Принято в кассу",
                #8 => "Дата расчета",
                9 => "Дата в графике",
            );
            $tpl->setVariable("FILTER_DATE_PARAMS", array2options($dateParams, $_REQUEST["date_type"]));
            $tpl->setVariable("FILTER_WTYPE_OPTIONS", array2options(adm_ticktypes_plugin::getList($_POST['type_id']), $_POST["filter_wtype_id"]));

        }

        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default")
                $rtpl->loadTemplatefile($USER->getTemplate() . "/report.tmpl.htm");
            else
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
            $rtpl->setVariable("DATETO", $_POST['dateto']);
            //if ($_POST["tech_id"]!=0 && $usr = adm_users_plugin::getUser($_POST["tech_id"])) {

            if ($_REQUEST['loadphones'] == "on") {
                $rtpl->touchBlock("usephones");
            }
            if ($_REQUEST['loadfio'] == "on") {
                $rtpl->touchBlock("userfio");
            }
            if ($_REQUEST['loadgfx'] == "on") {
                $rtpl->touchBlock("gfxdate");
            }
            $datefrom = substr($_POST['datefrom'], 6, 4) . "-" . substr($_POST['datefrom'], 3, 2) . "-" . substr($_POST['datefrom'], 0, 2);
            $dateto = substr($_POST['dateto'], 6, 4) . "-" . substr($_POST['dateto'], 3, 2) . "-" . substr($_POST['dateto'], 0, 2);
            /*
                        $doneStatus = adm_statuses_plugin::getStatusByTag("done", $_POST["type_id"]);
                        $closedStatus = adm_statuses_plugin::getStatusByTag("closed", $_POST["type_id"]);
                        #$accStatus = adm_statuses_plugin::getStatusByTag("acc_accepted", $_POST["type_id"]);
                        $reportStatus = adm_statuses_plugin::getStatusByTag("report", $_POST["type_id"]);
                        $failStatus = adm_statuses_plugin::getStatusByTag("otkaz", $_POST["type_id"]);
                        $failOpStatus = adm_statuses_plugin::getStatusByTag("opotkaz", $_POST["type_id"]);
                        $sList = adm_statuses_plugin::getList($_POST["type_id"]);
            */
            $accStatus = adm_statuses_plugin::getStatusByTag("acc_accepted", $_POST["type_id"]);
            $doneStatus = array();
            $closedStatus = array();
            $reportStatus = array();
            $failStatus = array();
            $failOpStatus = array();
            $sList = array();
            $doneStatus['accidents'] = adm_statuses_plugin::getStatusByTag("done", "accidents");
            $doneStatus['connections'] = adm_statuses_plugin::getStatusByTag("done", "connections");
            $doneStatus['gp'] = adm_statuses_plugin::getStatusByTag("done", "gp");
            $closedStatus['accidents'] = adm_statuses_plugin::getStatusByTag("closed", "accidents");
            $closedStatus['connections'] = adm_statuses_plugin::getStatusByTag("closed", "connections");
            $closedStatus['gp'] = adm_statuses_plugin::getStatusByTag("closed", "gp");
            $reportStatus['accidents'] = adm_statuses_plugin::getStatusByTag("report", "accidents");
            $reportStatus['connections'] = adm_statuses_plugin::getStatusByTag("report", "connections");
            $reportStatus['gp'] = adm_statuses_plugin::getStatusByTag("report", "gp");
            $failStatus['accidents'] = adm_statuses_plugin::getStatusByTag("otkaz", "accidents");
            $failStatus['connections'] = adm_statuses_plugin::getStatusByTag("otkaz", "connections");
            $failStatus['gp'] = adm_statuses_plugin::getStatusByTag("otkaz", "gp");
            $failOpStatus['accidents'] = adm_statuses_plugin::getStatusByTag("opotkaz", "accidents");
            $failOpStatus['connections'] = adm_statuses_plugin::getStatusByTag("opotkaz", "connections");
            $failOpStatus['gp'] = adm_statuses_plugin::getStatusByTag("opotkaz", "gp");
            $sList['accidents'] = adm_statuses_plugin::getList("accidents");
            $sList['connections'] = adm_statuses_plugin::getList("connections");
            $sList['gp'] = adm_statuses_plugin::getList("gp");

            $filter = '';
            if ($_REQUEST["filter_status_id"]) {
                $filter .= " AND t.status_id IN (" . preg_replace("/,$/", "", implode(",", $_REQUEST["filter_status_id"])) . ")";
            }

            switch ($_REQUEST["date_type"]) {
                case 1:
                    $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=" . $DB->F($doneStatus[$_POST['type_id']]["id"]) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_POST['dateto']))) . ") ORDER BY `datetime` DESC) AND t.id NOT IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=" . $DB->F($doneStatus[$_POST['type_id']]["id"]) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>" . $DB->F(date("Y-m-d", strtotime($_POST['dateto']))) . ") ORDER BY `datetime` DESC)";
                    break;
                case 2:
                    $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE `status_id` AND (DATE_FORMAT(`datetime`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_POST['dateto']))) . "))";
                    break;
                case 3:
                    $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE ((`status_id`=" . $DB->F($doneStatus[$_POST['type_id']]["id"]) . " OR `status_id`=" . $DB->F($failStatus[$_POST['type_id']]["id"]) . " OR `status_id`=" . $DB->F($failOpStatus[$_POST['type_id']]["id"]) . ") AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_POST['dateto']))) . ") ORDER BY `datetime` DESC) AND t.id NOT IN (SELECT task_id FROM `task_comments` WHERE ((`status_id`=" . $DB->F($doneStatus[$_POST['type_id']]["id"]) . " OR `status_id`=" . $DB->F($failStatus[$_POST['type_id']]["id"]) . " OR `status_id`=" . $DB->F($failOpStatus[$_POST['type_id']]["id"]) . ") AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>" . $DB->F(date("Y-m-d", strtotime($_POST['dateto']))) . ") ORDER BY `datetime` DESC)";
                    break;
                case 4:
                    // am
                    $filter .= "AND tick.inmoney=1  AND DATE_FORMAT(tick.inmoneydate, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " AND DATE_FORMAT(tick.inmoneydate, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_POST['dateto'])));

                    break;
                case 5:
                    //cl_date
                    $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=" . $DB->F($closedStatus[$_POST['type_id']]["id"]) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_POST['dateto']))) . ") ORDER BY `datetime` DESC) AND t.id NOT IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=" . $DB->F($closedStatus[$_POST['type_id']]["id"]) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>" . $DB->F(date("Y-m-d", strtotime($_POST['dateto']))) . ") ORDER BY `datetime` DESC)";
                    break;
                case 6:
                    //rep_date
                    $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=" . $DB->F($reportStatus[$_POST['type_id']]["id"]) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_POST['dateto']))) . ") ORDER BY `datetime` DESC)";

                    break;
                case 7:
                    //rep_date
                    $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=" . $DB->F($accStatus[$_POST['type_id']]["id"]) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_POST['dateto']))) . ") ORDER BY `datetime` DESC)";

                    break;
                case 9:
                    // schedule date
                    $filter .= "and exists (select * from gfx where gfx.task_id=t.id and c_date >= " . $DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " and c_date <= " . $DB->F(date("Y-m-d", strtotime($_POST['dateto']))) . ")";

                    break;
                default:
                    $filter .= " AND t.date_reg between " . $DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " AND " . $DB->F(date("Y-m-d", strtotime($_POST['dateto'])));
            }


            if ($_REQUEST["tech_id"] != 0) {
                $filter .= " AND g.empl_id=" . $DB->F($_REQUEST["tech_id"]) . " ";
            }
            if ($_REQUEST['filter_wtype_id'] > 0) {
                $filter .= " AND tick.task_type=" . $DB->F($_REQUEST["filter_wtype_id"]) . " ";
            }
            if ($_REQUEST['cnt_id'] != 0) {
                $filter .= " AND tick.cnt_id=" . $DB->F($_REQUEST["cnt_id"]) . " ";
            }
            if ($_REQUEST["sc_id"] > 0) {
                $filter .= " AND tick.sc_id=" . $DB->F($_REQUEST["sc_id"]) . " ";
            } else {
                if ($_REQUEST["sc_id"] == '') {
                    $filter .= " AND !tick.sc_id ";
                }
            }
            $order = " ORDER BY t.date_reg";

            $plugin_list = $DB->F($_POST["type_id"]);
            if ($_POST["type_id"] == "all") {
                $plugin_list = "'connections','accidents','gp'";
            }

            $subquery = "";
            if ($_POST['type_id'] == \models\Task::CONNECTION) {
                $subquery = "(select max(calc_date) from list_tech_calc lc where lc.task_id = t.id)";
            }
            if ($_POST['type_id'] == \models\Task::ACCIDENT) {
                $subquery = "(select max(calc_date) from list_tt_tech_calc lc where lc.task_id = t.id)";
            }
            if ($_POST['type_id'] == \models\Task::GLOBAL_PROBLEM) {
                $subquery = "(select max(calc_date) from gp_calcs gc where gc.task_id = t.id)";
            }

            if ($_REQUEST["date_type"] == 8) {
                $filter .= " and " . $subquery . " between " . $DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " AND " . $DB->F(date("Y-m-d", strtotime($_POST['dateto'])));
            }

            if (in_array($_POST["type_id"], array("connections", "accidents"))) {
                $sql = "SELECT t.id,
                     t.plugin_uid,
                     cnt_id,
                     clnttnum,
                     (select max(contr_title) from list_contr lc where lc.id = cnt_id) contragent,
                     (select max(name) from task_status ts where ts.id = t.status_id) status,
                     (select max(title) from list_rejreasons lr where lr.id = tick.reject_id) reject_reason,
                     (select max(title) from task_types tt where tt.id = tick.task_type) task_type,
                     (select max(title) from list_tsource lt where lt.id = tick.task_from) task_from,
                     (select max(kladrcode) from list_addr la where la.id = tick.dom_id) kladrcode,
                     (select max(title) from list_addr la1, list_area la2 where la1.id = tick.dom_id and la1.area_id = la2.id) list_area_title,
                     (select max(full_address) from list_addr la1 where la1.id = tick.dom_id) full_address,
                     (select max(ifnull(la1.name,la1.althouse)) from list_addr la1, list_area la2 where la1.id = tick.dom_id and la1.area_id = la2.id) house_name,
                     kv,
                     ifnull(clnt_org_name,clnt_fio) fio,
                     clnt_tel1,
                     date_reg,
                     g.c_date, g.time_start, g.time_end,
                     (select max(title) from list_sc ls where ls.id = tick.sc_id) sc_name,
                     (select max(fio) from users u1 where u1.id = g.empl_id) tech_name,
                     (select max(fio) 
                       from users u2, 
                            task_users tu 
                       where u2.id = tu.user_id
                         and tu.ispoln > 0
                         and tu.task_id = t.id
                     ) ispoln_name,
                     1 address_count,
                    " . $subquery . " calc_date
                        FROM `tasks` AS t 
                    LEFT JOIN `gfx` AS g ON g.task_id=t.id 
                    LEFT JOIN tickets AS tick ON tick.task_id=t.id 
                       WHERE t.plugin_uid in (" . $plugin_list . ")
                       $filter 
                    GROUP BY t.id 
                    $order;";
            } else {
                $sql = "SELECT t.id,
                     t.plugin_uid,
                     cnt_id,
                     operator_key clnttnum,
                     (select max(contr_title) from list_contr lc where lc.id = cnt_id) contragent,
                     (select max(name) from task_status ts where ts.id = t.status_id) status,
                     (select max(title) from gp_subtypes lr where lr.id = tick.gpsubtype_id) reject_reason,
                     (select max(title) from gp_types tt where tt.id = tick.gptype_id) task_type,
                     (select max(title) from list_tsource lt where lt.id = tick.task_from) task_from,
                     (select max(kladrcode) from list_addr la, gp_addr ga where la.id = ga.dom_id and ga.task_id = t.id) kladrcode,
                     (select max(title) from list_addr la1, list_area la2, gp_addr ga where la1.id = ga.dom_id and la1.area_id = la2.id and ga.task_id = t.id) list_area_title,
                     (select full_address from list_addr left join gp_addr on list_addr.id = gp_addr.dom_id and gp_addr.task_id = t.id limit 1) full_address,
                     (select max(ifnull(la1.name,la1.althouse)) from list_addr la1, list_area la2, gp_addr ga where la1.id = ga.dom_id and la1.area_id = la2.id and ga.task_id = t.id) house_name,
                     null kv,
                     null fio,
                     null clnt_tel1,
                     date_reg,
                     g.c_date, g.time_start, g.time_end,
                     (select max(title) from list_sc ls where ls.id = tick.sc_id) sc_name,
                     (select max(fio) from users u1 where u1.id = g.empl_id) tech_name,
                     (select max(fio) 
                       from users u2, 
                            task_users tu 
                       where u2.id = tu.user_id
                         and tu.ispoln > 0
                         and tu.task_id = t.id
                     ) ispoln_name,
                     ( select count(*) from gp_addr ga where ga.task_id = t.id ) address_count,
                    " . $subquery . " calc_date
                        FROM `tasks` AS t 
                    LEFT JOIN `gfx` AS g ON g.task_id=t.id 
                    LEFT JOIN gp AS tick ON tick.task_id=t.id
                       WHERE t.plugin_uid in (" . $plugin_list . ")
                       $filter 
                    GROUP BY t.id 
                    $order;";

            }


            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error() . " " . $sql);
            $i = 1;
            $totalAVG = 0;
            $totalREAL = 0;

            $rejectHead = false;
            while ($res = $DB->fetch(true)) {
                #$ticket = new ServiceTicket($res["id"]);
                $rtpl->setCurrentBlock("rep_row");
                $rtpl->setVariable("RRR_ID", $i);
                $rtpl->setVariable("RRR_TYPE_ID", $res["plugin_uid"]);
                $rtpl->setVariable("RR_ID", $res["id"]);
                $scName = $res["sc_name"];
                $rtpl->setVariable("RR_SC", $scName ? $scName : "СЦ не указан");

                #if ($doneStatus) {
                if (1 == 1) {
                    $sql = "SELECT DATE_FORMAT(MAX(`datetime`), " . $DB->F("%d.%m.%Y %H:%i:%s") . ") as doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($res["id"]) . " AND `status_id`=" . $DB->F($doneStatus[$res['plugin_uid']]["id"]) . " ORDER BY doneDate DESC LIMIT 1;";
                    $doneDate = $DB->getField($sql);
                    if ($doneDate) {
                        $rtpl->setVariable("RR_EXDATE", (new DateTime($doneDate))->format('Y-m-d H:i'));
                    } else {
                        if ($failStatus || $failOpStatus) {
                            $sql = "SELECT DATE_FORMAT(MAX(`datetime`), " . $DB->F("%d.%m.%Y %H:%i:%s") . ") as doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($res["id"]) . " AND `status_id`=" . $DB->F($failStatus[$res['plugin_uid']]["id"]) . " ORDER BY doneDate DESC LIMIT 1;";
                            $failDate = $DB->getField($sql);
                            if ($failDate) {
                                $rtpl->setVariable("RR_EXDATE", "<p style=\"color:#FF0000;\">" . (new DateTime($failDate))->format('Y-m-d H:i') . "</p>");
                            } else {
                                $sql = "SELECT DATE_FORMAT(MAX(`datetime`), " . $DB->F("%d.%m.%Y %H:%i:%s") . ") as doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($res["id"]) . " AND `status_id`=" . $DB->F($failOpStatus[$res['plugin_uid']]["id"]) . " ORDER BY doneDate DESC LIMIT 1;";
                                $failDate = $DB->getField($sql);
                                if ($failDate) {
                                    $rtpl->setVariable("RR_EXDATE", "<p style=\"color:#FF0000;\">" . (new DateTime($failDate))->format('Y-m-d H:i') . "</p>");
                                } else {
                                    $rtpl->setVariable("RR_EXDATE", "&mdash;");
                                }
                            }
                        } else {
                            $rtpl->setVariable("RR_EXDATE", "&mdash;");
                        }

                    }
                    $sql = "SELECT DATE_FORMAT(MAX(`datetime`), " . $DB->F("%d.%m.%Y %H:%i:%s") . ") as doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($res["id"]) . " AND `status_id`=" . $DB->F($closedStatus[$res['plugin_uid']]["id"]) . " ORDER BY doneDate DESC LIMIT 1;";
                    #echo $sql."<br>\n";
                    $closeDate = $DB->getField($sql);
                    if ($closeDate) {
                        $rtpl->setVariable("RR_CLOSEDATE", (new DateTime($closeDate))->format('Y-m-d H:i'));
                    } else {
                        $rtpl->setVariable("RR_CLOSEDATE", "&mdash;");
                    }
                    $sql = "SELECT DATE_FORMAT(MAX(`datetime`), " . $DB->F("%d.%m.%Y %H:%i:%s") . ") as doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($res["id"]) . " AND `status_id`=" . $DB->F($reportStatus[$res['plugin_uid']]["id"]) . " ORDER BY doneDate DESC LIMIT 1;";
                    #echo $sql."<br>\n";
                    $repDate = $DB->getField($sql);
                    if ($repDate) {
                        $rtpl->setVariable("RR_REPDATE", (new DateTime($repDate))->format('Y-m-d H:i'));
                    } else {
                        $rtpl->setVariable("RR_REPDATE", "&mdash;");
                    }

                    $sql = "SELECT MAX(`datetime`) AS doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($res["id"]) . " AND `status_id`=" . $DB->F($accStatus[$res['plugin_uid']]["id"]) . " ORDER BY doneDate DESC LIMIT 1;";

                    $accDate = $DB->getField($sql);
                    if ($accDate) {
                        $rtpl->setVariable("RR_MRDATE", (new DateTime($accDate))->format('Y-m-d H:i'));
                    } else {
                        $rtpl->setVariable("RR_MRDATE", "&mdash;");
                    }

                } else {
                    $rtpl->setVariable("RR_EXDATE", "&mdash;");
                }

                $rtpl->setVariable("RR_WTYPENAME", $res['task_type']);
                $rtpl->setVariable("RR_STATUSNAME", $res['status']);
                $rtpl->setVariable("RR_CONTR_NAME", $res['contragent']);
                $rtpl->setVariable("RR_WHEREFROM", $res['task_from']);
                $rtpl->setVariable("RR_AREA", $res['list_area_title']);
                $rtpl->setVariable("RR_CRDATE", (new DateTime($res['date_reg']))->format('Y-m-d H:i'));
                if (strlen($res['calc_date']) > 1) {
                    $rtpl->setVariable("RR_CALC_DATE", (new DateTime($res['calc_date']))->format('Y-m-d H:i'));
                } else {
                    $rtpl->setVariable("RR_CALC_DATE", "&nbsp;");
                }
                $rtpl->setVariable("RR_TECHNAME", $res['tech_name']);
                $rtpl->setVariable("RR_RESPONSIBLE", $res['ispoln_name']);
                $rtpl->setVariable("RR_GSSID", $res['clnttnum']);
                $rtpl->setVariable("REJECTREASON", $res['reject_reason']);

                if ($_REQUEST["loadphones"] == "on") {
                    #$phone = $ticket->getPhones();
                    $rtpl->setCurrentBlock("usephonesbody");
                    $rtpl->setVariable("PHONE1", $res['clnt_tel1'] ? preg_replace("/^7/", "", $res['clnt_tel1']) : "-");
                    #$rtpl->setVariable("PHONE2", $phone[1] ? preg_replace("/^7/", "", $phone[1]) : "-");
                    $rtpl->parse("usephonesbody");
                }
                if ($_REQUEST["loadgfx"] == "on") {
                    #$gfx = $DB->getField("SELECT `c_date` FROM `gfx` WHERE `task_id`=".$DB->F($ticket->getId()).";");

                    $rtpl->setCurrentBlock("gfxdatebody");
                    if ($res['time_start']) {
                        $rtpl->setVariable("RR_GFXDATE", (new DateTime($res['time_start']))->format('Y-m-d H:i') . ' / ' . (new DateTime($res['time_end']))->format('Y-m-d H:i'));
                    } else {
                        $rtpl->setVariable("RR_GFXDATE", "&nbsp;");
                    }
                    $rtpl->parse("gfxdatebody");
                }
                if ($_REQUEST['loadfio'] == "on") {
                    $rtpl->setCurrentBlock("userfiobody");
                    $rtpl->setVariable("USERFIO", $res['fio']);
                    $rtpl->parse("userfiobody");
                }

                $rtpl->setCurrentBlock("kladr_addr");

                $res['full_address'] = preg_replace('/, (кв|оф) .+$/', '', $res['full_address']);
                $explodedAddress = array_reverse(array_map('strrev', explode(' ,', strrev($res['full_address']), 3)));
                $rtpl->setVariable("RR_ADDR_CITY", $explodedAddress[0] ?? '-');
                $rtpl->setVariable("RR_ADDR_STREET", $explodedAddress[1] ?? '-');
                $rtpl->setVariable("RR_ADDR_BUILDING", $explodedAddress[2] ?? '-');
                $extra = "";
                if ($res['address_count'] > 1) {
                    $extra = " + " . ($res['address_count'] - 1) . " домов";
                }
                $rtpl->setVariable("RR_ADDR_FLAT", $res['kv'] . $extra);
                $rtpl->parse("kladr_addr");

                $rtpl->parse("rep_row");
                $i += 1;
            }

            $rtpl->setVariable("AVGPRICETOTAL", $totalAVG);
            $rtpl->setVariable("PRICETOTAL", $totalREAL);
            $colspan = 7;
            if ($rejectHead) {
                $colspan += 1;
            }
            if ($_REQUEST["loadphones"] == "on") {
                $colspan += 2;
            }
            if ($_REQUEST["loadfio"] == "on") {
                $colspan += 1;
            }
            $rtpl->setVariable("COLSPAN_STS", $colspan + 1);
            $DB->free();
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");
                header("Content-disposition: attachment; filename=report_avg_price__" . $datefrom . "-" . $dateto . ".xls");
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();

    }

    function getContrList($sel)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE (contr_type = 1 OR contr_type3 = 1 OR contr_type4 = 1) ORDER BY `contr_title`;"), $sel);
    }
}
