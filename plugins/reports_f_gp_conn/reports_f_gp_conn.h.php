<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2014
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Отчет по статусам заявок";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Отчет по статусам заявок";
    $PLUGINS[$plugin_uid]['events']['getwtypelist'] = "Список видов работ";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events'][''] = "";
    
}
?>
