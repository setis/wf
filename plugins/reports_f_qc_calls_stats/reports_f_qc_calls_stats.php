<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;


require_once(dirname(__FILE__)."/../connections/connections.php");
require_once(dirname(__FILE__)."/../accidents/accidents.php");
require_once(dirname(__FILE__)."/../services/services.php");

function hide_zero ($a) {
        return $a == '0' ? '&nbsp;' : $a;
}
 
class reports_f_qc_calls_stats_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function draw_table ($rows, $table_attributes = '') {
        $out = '';
        $out .= "<table ".$table_attributes.">\n";
        foreach ($rows as $rowid => $cells) {
                $out .= "<tr align=center>\n";
                $cell_counter = 0;
                foreach ($cells as $cellid => $properties) {
                        $out .= "<td";
                        if (array_key_exists('colspan',$properties)) {
                                $out .= " colspan=".$properties['colspan'];
                        }
                        if (array_key_exists('rowspan',$properties)) {
                                $out .= " rowspan=".$properties['rowspan'];
                        }
                        if (array_key_exists('bgcolor',$properties)) {
                                $out .= " bgcolor=".$properties['bgcolor'];
                        }
                        if (array_key_exists('align',$properties)) {
                                $out .= " align=\"".$properties['align']."\"";;
                        }
                        $out .=">".$properties['value']."</td>\n";
                        $cell_counter++;
                }
                $out .= "</tr>\n";
        }
        $out .= "</table>\n";
        return $out;
    }

    function main() {
        global $DB, $USER;
        $interval_options = array(
            '1' => "Дата регистрации заявки",
            '2' => "Дата статуса заявки",
            '3' => "Дата выполнения обзвона",
            '4' => "Дата до которой отложен обзвон"
        );
        $qc_status = array(0 => " -- все -- ", 1 => "Отказ", 2 => "Выполнена");
        $qc_cstatus = array(0 => "Не выполнялся", 1 => "Выполнен", 2 => "Отказ клиента", 3 => "Отложен");
        $rejectList = array(10, 46, 43, /*5, 65*/);
        $doneList = array(61, 54, 52, 1, 26, 23, 12, 25, 24, 49);
        $dl_sql = implode(",", array("61", "1", "12"));
        $rl_sql = implode(",", $rejectList);
        $sl_sql = $dl_sql . "," . $rl_sql;
        $filter_interval = 3;
        if (isset($_REQUEST['filter_interval'])) {
                $filter_interval = $_REQUEST['filter_interval'];
        }

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");

        $filter = array();
        switch ($_REQUEST["filter_interval"]) {
            case "1":
                if ($_REQUEST['filter_qc_id'] > 0)
                    $filter[] = $_REQUEST['filter_qc_id'] == 1 ? "t.status_id IN (" . implode(",", $rejectList) . ")" : "t.status_id IN (" . implode(",", $doneList) . ")";
                else {
                    $filter[] = "t.status_id IN (" . implode(",", $rejectList) . "," . implode(",", $doneList) . ")";
                }
                $filter[] = "t.date_reg >=" . "STR_TO_DATE('".$_REQUEST['datefrom']."','%d.%m.%Y')";
                $filter[] = "t.date_reg <=" . "STR_TO_DATE('".$_REQUEST['dateto']." 23:59:59','%d.%m.%Y %H:%i:%s')";
                break;
            case "2":
                if ($_REQUEST['filter_qc_id'] > 0)
                    $d = $_REQUEST['filter_qc_id'] == 1 ? implode(",", $rejectList) : "1,12,61";
                else {
                    $d = implode(",", $rejectList) . ",1,12,61";
                }
                $filter[] = "t.id IN ((SELECT task_id FROM `task_comments` WHERE `status_id` IN (" . $d . ")
                                            AND `datetime` >=" . "STR_TO_DATE('".$_REQUEST['datefrom']."','%d.%m.%Y')" . " AND datetime<=" . "STR_TO_DATE('".$_REQUEST['dateto']." 23:59:59','%d.%m.%Y %H:%i:%s')" . "))";
                //$sql_join = "LEFT JOIN `task_comments` as tc ON tc.task_id=t.id";
                break;
            case "3":
                if ($_REQUEST['filter_qc_id'] > 0)
                    $filter[] = $_REQUEST['filter_qc_id'] == 1 ? "t.status_id IN (" . implode(",", $rejectList) . ")" : "t.status_id IN (" . implode(",", $doneList) . ")";
                else {
                    $filter[] = "t.status_id IN (" . implode(",", $rejectList) . "," . implode(",", $doneList) . ")";
                }
                $filter[] = "qc.qc_date >=" . "STR_TO_DATE('".$_REQUEST['datefrom']."','%d.%m.%Y')";
                $filter[] = "qc.qc_date <=" . "STR_TO_DATE('".$_REQUEST['dateto']." 23:59:59','%d.%m.%Y %H:%i:%s')";
                break;

            case "4":
                if ($_REQUEST['filter_qc_id'] > 0)
                    $filter[] = $_REQUEST['filter_qc_id'] == 1 ? "t.status_id IN (" . implode(",", $rejectList) . ")" : "t.status_id IN (" . implode(",", $doneList) . ")";
                else {
                    $filter[] = "t.status_id IN (" . implode(",", $rejectList) . "," . implode(",", $doneList) . ")";
                }
                $filter[] = "qc.qc_delayed >=" . "STR_TO_DATE('".$_REQUEST['datefrom']."','%d.%m.%Y')";
                $filter[] = "qc.qc_delayed <=" . "STR_TO_DATE('".$_REQUEST['dateto']." 23:59:59','%d.%m.%Y %H:%i:%s')";
                break;
            default:
                if ($_REQUEST['filter_qc_id'] > 0)
                    $filter[] = $_REQUEST['filter_qc_id'] == 1 ? "t.status_id IN (" . implode(",", $rejectList) . ")" : "t.status_id IN (" . implode(",", $doneList) . ")";
                else {
                    $filter[] = "t.status_id IN (" . implode(",", $rejectList) . "," . implode(",", $doneList) . ")";
                }
                $filter[] = "date_reg >=" . "STR_TO_DATE('".$_REQUEST['datefrom']."','%d.%m.%Y')";
                $filter[] = "t.date_reg <=" . "STR_TO_DATE('".$_REQUEST['dateto']." 23:59:59','%d.%m.%Y %H:%i:%s')";
                break;
        }
        #$tpl->setCurrentBlock("head");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id"]));
        $tpl->setVariable("FILTER_CONTR_OPTIONS", kontr_plugin::getOptListTasks($_REQUEST["contr_id"]));
        $dt_from = @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date('Y-m-d') . " - 1 month"));
        $dt_to = @$_POST['dateto'] ? @$_POST['dateto'] : date("d.m.Y");
        $tpl->setVariable("DATE_FROM", $dt_from);
        $tpl->setVariable("DATE_TO", $dt_to);
        $tpl->setVariable('FILTER_INTERVAL_OPTIONS', array2options($interval_options, $filter_interval));
        $tpl->setVariable('FILTER_QC_OPTIONS', array2options($qc_status, $_REQUEST['filter_qc_id']));
        UIHeader($tpl);
        if (!(isset($_REQUEST['datefrom'])&&(isset($_REQUEST['dateto'])))) {
                $tpl->show();
                return;
        }

        $sql_head = "select 
  ifnull((select fio from users u where u.id = qc.user_id),'N/A') fio,
  ifnull(qc.user_id,0) user_id,
  ifnull(qc.qc_status,-1) qc_status,
  date_format(ifnull(date(qc.qc_date),'2000-01-01'),'%Y%m%d') qc_date,
  t.plugin_uid,
  count(*) cnt
  from tasks t
  LEFT JOIN tickets tick ON tick.task_id=t.id
  JOIN task_users tu ON tu.task_id=t.id and tu.ispoln > 0
  LEFT JOIN tickets_qc AS qc ON qc.task_id=t.id
 where\n";
        $sql_tail = "\ngroup by 
  qc.user_id,
  qc.qc_status,
  date(qc.qc_date)";

        $sql_filter = join("\n and ",$filter);

        $sql = $sql_head.$sql_filter.$sql_tail;
        $DB->query($sql);
        if ($DB->errno()) {
            UIError($DB->error() . $sql);
        }
        if (!$DB->num_rows()) {
            $tpl->touchBlock("norecords");
        }
        $data = array();
        $totals = array();
        $grand_totals = array();
        $fios = array();
        $dates = array();
        $user_totals = array();
        $megatotals = array();
        #$statuses = array();
        while ($rlist = $DB->fetch(true)) {
                $fios[ $rlist['fio'] ] = $rlist['user_id'];
                #$statuses[ $rlist['qc_status'] ] = 1;
                $dates[ $rlist['qc_date'] ] = 1;
                #$dates[ $rlist['qc_date'] ] = 1;
                $data[ $rlist['fio'] ][ $rlist['qc_date'] ][ $rlist['plugin_uid'] ][ $rlist['qc_status'] ] = $rlist['cnt'];
                $totals[ $rlist['fio'] ][ $rlist['qc_date'] ][ $rlist['qc_status'] ] += $rlist['cnt'];
                $grand_totals[ $rlist['qc_date'] ][ $rlist['plugin_uid'] ][ $rlist['qc_status'] ] += $rlist['cnt'];
                $user_totals[ $rlist['fio'] ][ $rlist['plugin_uid'] ][ $rlist['qc_status'] ] += $rlist['cnt'];
                if ($rlist['qc_status'] == 3) {
                        $megatotals[0] += $rlist['cnt'];
                }
                if ($rlist['qc_status'] == 2) {
                        $megatotals[1] += $rlist['cnt'];
                }
                if (($rlist['qc_status'] == 1) && ($rlist['plugin_uid'] == 'services' )) {
                        $megatotals[2] += $rlist['cnt'];
                }
                if (($rlist['qc_status'] == 1) && ($rlist['plugin_uid'] == 'connections')) {
                        $megatotals[3] += $rlist['cnt'];
                }
                if (($rlist['qc_status'] == 1) && ($rlist['plugin_uid'] == 'accidents' )) {
                        $megatotals[4] += $rlist['cnt'];
                }
                if (($rlist['qc_status'] == 1) && ( ($rlist['plugin_uid'] == 'accidents') || ($rlist['plugin_uid'] == 'connections') || ($rlist['plugin_uid'] == 'services' ))) {
                        $megatotals[5] += $rlist['cnt'];
                }
                if ($rlist['qc_status'] == 0) {
                        $megatotals[6] += $rlist['cnt'];
                }
        }
        ksort($fios);
        ksort($dates);

        $i = 0;
        $j = $i;
        $dates_count = 0;
        $rows = array();

        if (count($fios) > 0) {
                $rows[$i][0]['value'] = "ФИО";
                $rows[$i][0]['rowspan'] = 3;
        }

        foreach ($dates as $date => $val) {
                if ($date != '20000101') {
                        $j++;
                        $rows[$i][$j]['value'] = substr($date,6,2).".".substr($date,4,2).".".substr($date,0,4);
                        $rows[$i][$j]['colspan'] = 9;
                }
        }
        $rows[$i][++$j]['value'] = 'Итого за период';
        $rows[$i][$j]['colspan'] = 7;

        $i++;
        $j = 0;
        foreach ($dates as $date => $val) {
                if ($date != '20000101') {
                        $rows[$i][++$j]['value'] = 'СКП';
                        $rows[$i][$j]['colspan'] = 2;
                        $rows[$i][++$j]['value'] = 'Подключение';
                        $rows[$i][$j]['colspan'] = 2;
                        $rows[$i][++$j]['value'] = 'ТТ';
                        $rows[$i][$j]['colspan'] = 2;
                        $rows[$i][++$j]['value'] = 'Перемещенные в статус Отложен';
                        $rows[$i][$j]['rowspan'] = 2;
                        $rows[$i][$j]['bgcolor'] = '#FFC000';
                        $rows[$i][++$j]['value'] = 'Отказ клиента от опроса';
                        $rows[$i][$j]['rowspan'] = 2;
                        $rows[$i][$j]['bgcolor'] = '#E6B9B8';
                        $rows[$i][++$j]['value'] = 'Итого обработанные за день';
                        $rows[$i][$j]['rowspan'] = 2;
                        $rows[$i][$j]['bgcolor'] = '#C6D9F1';
                        $dates_count++;
                }
        }
        $rows[$i][++$j]['value'] = 'Перемещенные в статус Отложен';
        $rows[$i][$j]['rowspan'] = 2;
        $rows[$i][$j]['bgcolor'] = '#FFC000';
        $rows[$i][++$j]['value'] = 'Отказ клиента от опроса';
        $rows[$i][$j]['rowspan'] = 2;
        $rows[$i][$j]['bgcolor'] = '#E6B9B8';
        $rows[$i][++$j]['value'] = 'Количество выполненых обзвонов';
        $rows[$i][$j]['colspan'] = 3;
        $rows[$i][++$j]['value'] = 'Итого по всем видам заявок';
        $rows[$i][$j]['rowspan'] = 2;
        $rows[$i][$j]['bgcolor'] = '#C6D9F1';
        $rows[$i][++$j]['value'] = 'Кол-во не обработанных заявок';
        $rows[$i][$j]['rowspan'] = 2;
        $rows[$i][$j]['bgcolor'] = '#BFBFBF';
        $i++;
        $j = 0;
        foreach ($dates as $date => $val) {
                if ($date != '20000101') {
                        $rows[$i][++$j]['value'] = 'Выполнен + Отказ клиента от опроса';
                        $rows[$i][++$j]['value'] = 'Кол-во заявок, по которым обзвон не выполнялся за все предыдущие периоды + отложенные на эту дату';
                        $rows[$i][$j]['bgcolor'] = '#BFBFBF';
                        $rows[$i][++$j]['value'] = 'Выполнен + Отказ клиента от опроса';
                        $rows[$i][++$j]['value'] = 'Кол-во не обзвоненных за весь предыдущий период';
                        $rows[$i][$j]['bgcolor'] = '#BFBFBF';
                        $rows[$i][++$j]['value'] = 'Выполнен + Отказ клиента от опроса';
                        $rows[$i][++$j]['value'] = 'Кол-во не обзвоненных за весь предыдущий период';
                        $rows[$i][$j]['bgcolor'] = '#BFBFBF';
                }
        }
        $rows[$i][++$j]['value'] = 'СКП';
        $rows[$i][$j]['bgcolor'] = '#92D050';
        $rows[$i][++$j]['value'] = 'Подключение';
        $rows[$i][$j]['bgcolor'] = '#92D050';
        $rows[$i][++$j]['value'] = 'ТТ';
        $rows[$i][$j]['bgcolor'] = '#92D050';

        foreach ($fios as $fio => $user_id) {
                if ($fio != 'N/A') {
                        $i++;
                        $j = 0;
                        $rows[$i][$j]['value'] = $fio;
                        $rows[$i][$j]['align'] = 'left';
                        foreach ($dates as $date => $val) {
                                if ($date != '20000101') {
                                    $rows[$i][++$j]['value'] = hide_zero($data[$fio][$date]['services'][1] + $data[$fio][$date]['services'][2]);
                                    $rows[$i][++$j]['value'] = hide_zero(/*$data['N/A']['20000101']['services'][-1] + */$data[$fio][$date]['services'][0]);
                                    $rows[$i][$j]['bgcolor'] = '#BFBFBF';
                                    $rows[$i][++$j]['value'] = hide_zero($data[$fio][$date]['connections'][1] + $data[$fio][$date]['connections'][2]);
                                    $rows[$i][++$j]['value'] = hide_zero(/*$data['N/A']['20000101']['connections'][-1] + */$data[$fio][$date]['connections'][0]);
                                    $rows[$i][$j]['bgcolor'] = '#BFBFBF';
                                    $rows[$i][++$j]['value'] = hide_zero($data[$fio][$date]['accidents'][1] + $data[$fio][$date]['accidents'][2]);
                                    $rows[$i][++$j]['value'] = hide_zero(/*$data['N/A']['20000101']['accidents'][-1] + */$data[$fio][$date]['accidents'][0]);
                                    $rows[$i][$j]['bgcolor'] = '#BFBFBF';
                                    $rows[$i][++$j]['value'] = hide_zero($totals[$fio][$date][3]);
                                    $rows[$i][$j]['bgcolor'] = '#FFC000';
                                    $rows[$i][++$j]['value'] = hide_zero($totals[$fio][$date][2]);
                                    $rows[$i][$j]['bgcolor'] = '#E6B9B8';
                                    $rows[$i][++$j]['value'] = hide_zero($totals[$fio][$date][1] + $totals[$fio][$date][2]);
                                    $rows[$i][$j]['bgcolor'] = '#C6D9F1';
                                }
                        }
                        ### итоги по оператору
                        $rows[$i][++$j]['value'] = hide_zero($user_totals[$fio]['services'][3] + $user_totals[$fio]['connections'][3] + $user_totals[$fio]['accidents'][3]);
                        $rows[$i][$j]['bgcolor'] = '#FFC000';
                        $rows[$i][++$j]['value'] = hide_zero($user_totals[$fio]['services'][2] + $user_totals[$fio]['connections'][2] + $user_totals[$fio]['accidents'][2]);
                        $rows[$i][$j]['bgcolor'] = '#E6B9B8';
                        $rows[$i][++$j]['value'] = hide_zero($user_totals[$fio]['services'][1]);
                        $rows[$i][$j]['bgcolor'] = '#92D050';
                        $rows[$i][++$j]['value'] = hide_zero($user_totals[$fio]['connections'][1]);
                        $rows[$i][$j]['bgcolor'] = '#92D050';
                        $rows[$i][++$j]['value'] = hide_zero($user_totals[$fio]['accidents'][1]);
                        $rows[$i][$j]['bgcolor'] = '#92D050';
                        $rows[$i][++$j]['value'] = hide_zero($user_totals[$fio]['services'][1] + $user_totals[$fio]['connections'][1] + $user_totals[$fio]['accidents'][1]);
                        $rows[$i][$j]['bgcolor'] = '#C6D9F1';
                        $rows[$i][++$j]['value'] = hide_zero($user_totals[$fio]['services'][0] + $user_totals[$fio]['connections'][0] + $user_totals[$fio]['accidents'][0]);
                }
        }

        # строка Итого:
        $i++;
        $j = 0;
        $rows[$i][$j]['value'] = "<b>Итого:</b>";
        $rows[$i][$j]['align'] = 'left';
        foreach ($dates as $date => $val) {
           if ($date != '20000101') {
                $rows[$i][++$j]['value'] = "<b>".hide_zero($grand_totals[ $date ]['services'][1] + $grand_totals[ $date ]['services'][2])."</b>";
                $rows[$i][++$j]['value'] = "<b>".hide_zero(/*$grand_totals[ '20000101' ]['services'][-1] + */$grand_totals[ $date ]['services'][0])."</b>";
                $rows[$i][++$j]['value'] = "<b>".hide_zero($grand_totals[ $date ]['connections'][1] + $grand_totals[ $date ]['connections'][2])."</b>";
                $rows[$i][++$j]['value'] = "<b>".hide_zero(/*$grand_totals[ '20000101' ]['connections'][-1] + */$grand_totals[ $date ]['connections'][0])."</b>";
                $rows[$i][++$j]['value'] = "<b>".hide_zero($grand_totals[ $date ]['accidents'][1] + $grand_totals[ $date ]['accidents'][2])."</b>";
                $rows[$i][++$j]['value'] = "<b>".hide_zero(/*$grand_totals[ '20000101' ]['accidents'][-1] + */$grand_totals[ $date ]['accidents'][0])."</b>";
                $rows[$i][++$j]['value'] = "<b>".hide_zero($grand_totals[ $date ]['services'][3] + $grand_totals[ $date ]['connections'][3] + $grand_totals[ $date ]['accidents'][3])."</b>";
                $rows[$i][++$j]['value'] = "<b>".hide_zero($grand_totals[ $date ]['services'][2] + $grand_totals[ $date ]['connections'][2] + $grand_totals[ $date ]['accidents'][2])."</b>";
                $rows[$i][++$j]['value'] = "<b>".hide_zero($grand_totals[ $date ]['services'][2] + $grand_totals[ $date ]['connections'][2] + $grand_totals[ $date ]['accidents'][2] + $grand_totals[ $date ]['services'][1] + $grand_totals[ $date ]['connections'][1] + $grand_totals[ $date ]['accidents'][1])."</b>";
           }
        }
        
        ### итого по итогу:
        $rows[$i][++$j]['value'] = "<b>".hide_zero($megatotals[0])."</b>";
        #$rows[$i][$j]['bgcolor'] = '#FFC000';
        $rows[$i][++$j]['value'] = "<b>".hide_zero($megatotals[1])."</b>";
        #$rows[$i][$j]['bgcolor'] = '#E6B9B8';
        $rows[$i][++$j]['value'] = "<b>".hide_zero($megatotals[2])."</b>";
        #$rows[$i][$j]['bgcolor'] = '#92D050';
        $rows[$i][++$j]['value'] = "<b>".hide_zero($megatotals[3])."</b>";
        #$rows[$i][$j]['bgcolor'] = '#92D050';
        $rows[$i][++$j]['value'] = "<b>".hide_zero($megatotals[4])."</b>";
        #$rows[$i][$j]['bgcolor'] = '#92D050';
        $rows[$i][++$j]['value'] = "<b>".hide_zero($megatotals[5])."</b>";
        #$rows[$i][$j]['bgcolor'] = '#C6D9F1';
        $rows[$i][++$j]['value'] = "<b>".hide_zero($megatotals[6])."</b>";
      
        if (isset($_REQUEST['datefrom']) && isset($_REQUEST['dateto'])) {

                if (isset($_POST["createxlsversion"])) {
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-disposition: attachment; filename=report_tech_rating__" . $_POST['datefrom'] . "-" . $_POST['dateto'] . ".xls");
                }

                $rep_date = rudate("d M Y");
                $rep_author = $USER->getFio();
                $content = $this->draw_table($rows,"class=\"tbl\"");
                if ((@$_POST["createprintversion"] ) || ($_POST["createxlsversion"])) {
                        $title = "Статистика отдела координации по обзвонам";
                        include_once(dirname(__FILE__)."/printable.php");
                        return;
                }

                $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
                if ($USER->getTemplate() != "default")
                    $tpl->loadTemplatefile($USER->getTemplate() . "/report.tmpl.htm");
                else
                    $rtpl->loadTemplatefile("report.tmpl.htm");
                UIHeader($tpl);
                $tpl->setVariable("REP_CDATE", $rep_date);
                $tpl->setVariable("REP_AUTHOR", $rep_author);
                $tpl->setVariable('PLUGIN_UID', $this->getUID());
                $tpl->setVariable('FILTER_INTERVAL_OPTIONS', array2options($interval_options, $filter_interval));
                $tpl->setVariable('FILTER_QC_OPTIONS', array2options($qc_status, $_REQUEST['filter_qc_id']));
                $tpl->setVariable("DATE_FROM", $dt_from);
                $tpl->setVariable("DATE_TO", $dt_to);
                $tpl->setVariable('INTERVAL_TYPE', $interval_options[$filter_interval]);
                $tpl->setVariable('FILTER_QC', $qc_status[$_REQUEST['filter_qc_id']]);
                $content = $this->draw_table($rows,"width=100% class=\"report_body table-condensed\" bgcolor=\"white\"");
                $tpl->setVariable("REPORT_HERE", $content);
        }
        $tpl->show();
   } 
}
?>
