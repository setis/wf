<?php
        $is_excel = 0;
        if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");
                header("Content-disposition: attachment; filename=report_qc_calls_stats_" . $_POST['datefrom'] . "-" . $_POST['dateto'] . ".xls");
                $is_excel = 1;
        }
?>
<html>
<head>
<style>
    body {
        background: #FFFFFF !important;
        font-family: Arial;
    }
    .tbl { border-collapse:collapse; border-width:1px; border-style:solid; }
    .tbl td { border-collapse:collapse; border-width:1px; border-style:solid; font-size: 12px; }
</style>
<?php
        if ($is_excel == 0) {
?>
<script type="text/javascript" src="/legacy_js/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        window.print();
    });    
</script>
<?php
        } # if ($is_excel == 0)
?>
</head>
<body>
<h1 align=center><?=$title?></h1>
Период: <strong>с <?=$dt_from?> по <?=$dt_to?></strong><br />
Тип интервала: <strong><?=$interval_options[$filter_interval]?></strong><br />
Группа статусов заявок: <strong><?=$qc_status[$_REQUEST['filter_qc_id']]?></strong><br />
<?=$content?>
<p><small>Отчет создан: <?=$rep_date?>.<br />
Автор: <?=$rep_author?></small></p>
</body>
</html>
