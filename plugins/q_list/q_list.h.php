<?php

/**
 * 
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 * 
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Справочник вопросов модуля Обзвон";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';


if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник вопросов модуля Обзвон";
    $PLUGINS[$plugin_uid]['events']['getQByStatus'] = "Получение списка вопросов для опроса";
    
}

if (wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['editq'] = "Редактор вопроса";
    $PLUGINS[$plugin_uid]['events']['saveq'] = "Сохранение вопроса";
    $PLUGINS[$plugin_uid]['events']['delq'] = "Удаление вопроса";
}

?>