<?php
use classes\HTML_Template_IT;
use classes\Plugin;

class q_list_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__file__, '.php'));
    }

 
    function viewlist()
    {
        global $DB, $USER;
        $qc_status = array(1=>"Отказ", 2=>"Выполнена");
        $tasktypes = array("services"=>"СКП", "connections"=>"Подключение", "accidents"=>"ТТ");
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
         
        if ($USER->checkAccess("showfilter"))
        {
            $tpl->touchBlock("showfilter");
        } else
        {
            $tpl->touchBlock("hidefilter");
        }
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $_REQUEST["filter_qc_id"] = $this->getCookie('filter_qc_id', $_REQUEST['filter_qc_id']);
        if ($_REQUEST['filter_qc_id'] =="") {
            $_REQUEST['filter_qc_id'] = 1;
        }
        $_REQUEST["filter_tasktype_id"] = $this->getCookie('filter_tasktype_id', $_REQUEST['filter_tasktype_id']);
        if ($_REQUEST['filter_tasktype_id'] == "") {
            $_REQUEST['filter_tasktype_id'] = "services";
        }
        $_REQUEST["filter_contrqc_id"] = $this->getCookie('filter_contrqc_id', $_REQUEST['filter_contrqc_id']);
        $tpl->setVariable('FILTER_TASKTYPE_OPTIONS', array2options($tasktypes, $_REQUEST['filter_tasktype_id']));
        $tpl->setVariable('FILTER_QC_OPTIONS', array2options($qc_status, $_REQUEST['filter_qc_id']));
        $tpl->setVariable("FILTER_CONTR_OPTIONS", kontr_plugin::getOptList($_REQUEST["filter_contrqc_id"]));
        if ($_REQUEST["filter_qc_id"]!= "") $filter[] = "t.status_group=".$DB->F($_REQUEST["filter_qc_id"]);
        if ($_REQUEST["filter_tasktype_id"]!= "") $filter[] = "t.plugin_uid=".$DB->F($_REQUEST["filter_tasktype_id"]);
        if ($_REQUEST["filter_contrqc_id"]>0) $filter[] = " (t.contr_id=".$DB->F($_REQUEST["filter_contrqc_id"])." OR t.contr_id=0 or contr_id is null) ";
        
        
        $filter[] = "!t.deleted";
        $filter = implode(" AND ", $filter);
        
        $sql = "SELECT t.*, (SELECT `fio` FROM `users` WHERE `id`=t.user_id) as user, (IF(t.contr_id>0, (SELECT `contr_title` FROM `list_contr` WHERE `id`=t.contr_id), 'все')) as contr_name FROM `qc_qlist` as t WHERE $filter ORDER BY t.iorder ASC;";
        $DB->query($sql);
        $total = $DB->getFoundRows();
        if ($DB->errno())
            UIError($DB->error() . $sql);
        if (!$DB->num_rows()) {
            $tpl->touchBlock("norecords");
        }
        while ($rlist = $DB->fetch(true))
        {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_QORDER", $rlist["iorder"]);
            $tpl->setVariable("LINKTOQ", link2("q_list/editq?id=".$rlist["id"], false));
            $tpl->setVariable("LINKTODELQ", link2("q_list/delq?id=".$rlist["id"], false));
            $tpl->setVariable("VLI_QID", $rlist["id"]);
            $tpl->setVariable("TASK_STATUS", $qc_status[$rlist["status_group"]]);
            $tpl->setVariable("TASK_TYPE", $tasktypes[$rlist["plugin_uid"]]);
            $tpl->setVariable("QTEXT", strlen($rlist["qtext"])>100 ? substr($rlist["qtext"], 0, 100)."..." : $rlist["qtext"]);
            $tpl->setVariable("QTEXTDESC", strlen($rlist["qtextdesc"])>100 ? substr($rlist["qtextdesc"], 0, 100)."..." : $rlist["qtextdesc"]);
            
            $tpl->setVariable("QTYPEYESMBNO", $rlist["q_mbyesno"]==2 ? "Нет" : ($rlist["q_mbyesno"]>0?"Да":"Нет"));
            $tpl->setVariable("QWEIGHT", $rlist["weight"]);
            $tpl->setVariable("CONTRNAME", $rlist['contr_name']);
            
            
            $tpl->setVariable("QTYPEYESNO", $rlist["q_yesno"]==2 ? "Нет" : "Да" );
            $tpl->setVariable("QMARK", $rlist["q_maxcount"]>0 ? $rlist["q_maxcount"] : "&mdash;");
            $tpl->setVariable("YESCOUNT", $rlist["yes_count"]!="" ? $rlist["yes_count"] : "&mdash;");
            $tpl->setVariable("NOCOUNT", $rlist["no_count"]!="" ? $rlist["no_count"] : "&mdash;");
            $tpl->setVariable("ENTERAMMOUNT", $rlist["req_ammount"]>0 ? "Да" : "Нет");
            $tpl->setVariable("ENTERCOMMENT", $rlist["req_comment"]>0 ? "Да" : "Нет");
            $tpl->setVariable("ACTIVE", $rlist["active"]>0 ? "Да" : "Нет");
            if ($rlist["active"]>0) {
                $tpl->setVariable("ACTIVECOLOR", "style='background-color: #B0FFB7 !important;'");
            }
            $tpl->setVariable("EDITDATE", date("H:i d.m.Y", strtotime($rlist["cr_date"])));
            $tpl->setVariable("USER", $rlist["user"]);
            
            $tpl->parse("vl_item");
        }

        UIHeader($tpl);
        $tpl->show();
        return false;
    }

    function editq() {
        global $DB, $USER;
        $qc_status = array(1=>"Отказ", 2=>"Выполнена");
        $tasktypes = array("services"=>"СКП", "connections"=>"Подключение", "accidents"=>"ТТ");
        
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/editq.tmpl.htm");
        else
            $tpl->loadTemplatefile("editq.tmpl.htm");
        $id = $_REQUEST["id"];
        if ($id) {
            $r = $DB->getRow("SELECT * FROM `qc_qlist` WHERE `id`=".$DB->F($id)." AND `deleted`=0;", true);
            if (!$r) UIError("Не найдена запись с указанным ID");
            $tpl->setVariable("Q_ID", $id);
            $tpl->setVariable("QTEXT", $r["qtext"]);
            
            $tpl->setVariable("QTEXTDESC", $r["qtextdesc"]);
            $tpl->setVariable("QA_YES", $r["q_yesno"]==1 ? "selected='selected'" : '');
            $tpl->setVariable("QA_NO", $r["q_yesno"]==2 ? "selected='selected'" : '');
            $tpl->setVariable("QA_MBYES", $r["q_mbyesno"]==1 ? "selected='selected'" : '');
            $tpl->setVariable("QA_MBNO", $r["q_mbyesno"]==2 ? "selected='selected'" : '');
            //ALTER TABLE `qc_qlist` ADD COLUMN `q_mbyesno` TINYINT(1) NOT NULL DEFAULT '0' AFTER `q_yesno`;
            //ALTER TABLE `qc_qlist` ADD COLUMN `weight` INT(10) NOT NULL DEFAULT '0' AFTER `no_count`;
            //ALTER TABLE `tickets_qc_results` ADD COLUMN `new_answer_mark` SMALLINT(5) NULL DEFAULT NULL AFTER `answer_mark`;
            //ALTER TABLE `tickets_qc` ADD COLUMN `qc_new_result` INT(10) NULL DEFAULT NULL AFTER `qc_result`;
            
            $tpl->setVariable("QA_YES_VAL", $r["yes_count"]!="" ? $r["yes_count"] : 0);
            $tpl->setVariable("QA_NO_VAL", $r["no_count"]!="" ? $r["no_count"] : 0);
            $tpl->setVariable("QA_MARK_VAL", $r["q_maxcount"]>0 ? $r["q_maxcount"] : 0);
            $tpl->setVariable("QA_REQ_AMM", $r["req_ammount"]>0 ? "checked='checked'" : '');
            $tpl->setVariable("QA_REQ_COMM", $r["req_comment"]>0 ? "checked='checked'" : '');
            $tpl->setVariable("QA_ACTIVE", $r["active"]>0 ? "checked='checked'" : '');
        } 
        $tpl->setVariable("IORDER", $r['iorder']!= "" ? $r["iorder"] : 0);
        $tpl->setVariable("QA_YES_VAL", $r["yes_count"]!= "" ? $r["yes_count"] : 0);
        $tpl->setVariable("QA_NO_VAL", $r["no_count"]!= "" ? $r["no_count"] : 0);
        $tpl->setVariable("QA_MARK_VAL", $r["q_maxcount"]>0 ? $r["q_maxcount"] : 0);
        $tpl->setVariable("QA_WEIGHT", $r["weight"]>0 ?$r["weight"] :0); 
        $tpl->setVariable("QTYPE",  array2options($tasktypes, $r['plugin_uid']));
        $tpl->setVariable("QSTATUS", array2options($qc_status, $r['status_group']));
        $tpl->setVariable("CONTR_OPTIONS", kontr_plugin::getOptList($r["contr_id"]));
            
        UIHeader($tpl);
        $tpl->show();
        return false;
    }
     
    function saveq() {
        global $DB, $USER;
        if ($_REQUEST['q_id']) {
            $mb = $_REQUEST["qyesmbno"];
            $weight = $_REQUEST["qweight"];
            if ($mb == "1") {
                $_REQUEST["qyesno"] = "2";
            }
            if ($_REQUEST["qyesno"] == "1") {
                $mb = "2";
            }
            $sql = "UPDATE `qc_qlist` SET 
            `iorder`=".$DB->F($_REQUEST["iorder"]).",
            `qtext`=".$DB->F($_REQUEST["qtext"]).",
            `qtextdesc`=".$DB->F($_REQUEST["qtextdesc"]).",
            `q_yesno`=".$DB->F($_REQUEST["qyesno"]).",
            `q_mbyesno`=".$DB->F($mb).",
            `weight`=".$DB->F($weight).",
            `q_maxcount`=".$DB->F($_REQUEST["qyesno"] == 1 ? 0 : ($_REQUEST["qa_mark"]>0 ? $_REQUEST['qa_mark'] : 0)).",
            `yes_count`=".$DB->F($_REQUEST["qyesno"] == 2 ? 0 : ($_REQUEST["qa_yes"]!="" ? $_REQUEST["qa_yes"] : 0)).",
            `no_count`=".$DB->F($_REQUEST["qyesno"] == 2 ? 0 : ($_REQUEST["qa_no"]!= "" ? $_REQUEST["qa_no"] : 0)).",
            `req_ammount`=".$DB->F(isset($_REQUEST["q_req_amm"]) ? 1 : 0).",
            `req_comment`=".$DB->F(isset($_REQUEST["q_req_comm"]) ? 1 : 0).",
            `status_group`=".$DB->F($_REQUEST["qstatus_id"]).",
            `plugin_uid`=".$DB->F($_REQUEST["qtype_id"]).",
            `active`=".$DB->F(isset($_REQUEST["q_active"]) ? 1 : 0).",
            `user_id`=".$DB->F($USER->getId()).", `contr_id`=".$DB->F($_REQUEST["contr_id"])."
            WHERE `id`=".$DB->F($_REQUEST["q_id"])." LIMIT 1;";
        } else {
            $sql = "INSERT INTO `qc_qlist` (`iorder`, `qtext`, `qtextdesc`, `q_yesno`, `q_mbyesno`, `q_maxcount`, `yes_count`, `no_count`, `weight`, `req_ammount`, `req_comment`, `status_group`, `plugin_uid`, `active`, `user_id`, `contr_id`) 
            VALUES (
            ".$DB->F($_REQUEST["iorder"]).",
            ".$DB->F($_REQUEST["qtext"]).",
            ".$DB->F($_REQUEST["qtextdesc"]).",
            ".$DB->F($_REQUEST["qyesno"]).",
            
            ".$DB->F($_REQUEST["qyesmbno"]).",
            
            ".$DB->F($_REQUEST["qa_mark"]>0 ? $_REQUEST["qa_mark"] : 0).",
            ".$DB->F($_REQUEST["qa_yes"]!="" ? $_REQUEST["qa_yes"] : 0).",
            ".$DB->F($_REQUEST["qa_no"]!="" ? $_REQUEST["qa_no"] : 0).",

            ".$DB->F($_REQUEST["qweight"]!="" ? $_REQUEST["qweight"] : 0).",

            ".$DB->F(isset($_REQUEST["q_req_amm"]) ? 1 : 0).",
            ".$DB->F(isset($_REQUEST["q_req_comm"]) ? 1 : 0).",
            ".$DB->F($_REQUEST["qstatus_id"]).",
            ".$DB->F($_REQUEST["qtype_id"]).",
            ".$DB->F(isset($_REQUEST["q_active"]) ? 1 : 0).",
            ".$DB->F($USER->getId()).", ".$DB->F($_REQUEST["contr_id"]).")";
        }
        //echo $sql;
        //die();
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error()."\r\n".$sql);
        else 
            //die($sql);
            redirect(link2("q_list", false), "Запись успешно сохранена!");
        return false;
    }
     
    function delq() {
        global $DB, $USER;
        $id = $_REQUEST["id"];
        if ($id) {
            $sql = "UPDATE `qc_qlist` SET `deleted`=1, `active`=0, `user_id`=".$DB->F($USER->getId())." WHERE `id`=".$DB->F($id).";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            
            redirect(link2("q_list", false), "Запись успешно удалена!");
        } else 
            UIError("Не указан идентификатор для удаления!");
        return false;
    }

    static function getQByStatus($task_type, $status_id, $contr_id) {
        global $DB, $USER;
        $rejectList = array(10, 46, 43, 5, 65);
        $doneList = array(61, 54, 52, 1, 26, 23, 12, 25, 24, 49, 99); 
        $targetStatus = false;
        if (in_array($status_id, $rejectList)) $targetStatus = "1";
        if (in_array($status_id, $doneList)) $targetStatus = "2";
        if (!$targetStatus) return false;
        if ($contr_id>0) {
            $add_sql = " AND (`contr_id`=".$DB->F($contr_id)." OR `contr_id`=0 or contr_id is null) ";
        }
        $sql = "SELECT * FROM `qc_qlist` WHERE `plugin_uid`=".$DB->F($task_type)." AND `status_group`=".$DB->F($targetStatus)." AND `deleted`=0 AND `active` $add_sql ORDER BY `iorder` ASC;";
        $DB->query($sql);
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $res[] = $r;
            }
            return $res;
        } else {
            return false;
        }
    }
    
    static function getQByStatusWA($task_type, $status_id, $task_id, $contr_id) {
        global $DB, $USER;
        $rejectList = array(10, 46, 43, 5, 65);
        $doneList = array(61, 54, 52, 1, 26, 23, 12, 25, 24, 49, 99); 
        $targetStatus = false;
        if (in_array($status_id, $rejectList)) $targetStatus = "1";
        if (in_array($status_id, $doneList)) $targetStatus = "2";
        if (!$targetStatus) return false;
        if ($contr_id>0) {
            $add_sql = " AND (list.contr_id=".$DB->F($contr_id)." OR list.contr_id=0 or contr_id is null) ";
        }
        $sql = "SELECT list.*, res.result, res.answer_mark, res.new_answer_mark, res.mark, res.ammount, res.comment FROM `tickets_qc_results` as res LEFT JOIN `qc_qlist` as list ON res.q_id=list.id 
        WHERE res.task_id=".$DB->F($task_id)." AND list.plugin_uid=".$DB->F($task_type)." AND list.status_group=".$DB->F($targetStatus)." $add_sql GROUP BY list.id ORDER BY list.iorder ASC;";
        $DB->query($sql);
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $res[] = $r;
            }
            return $res;
            //return $sql;
        } else {
            return false;
        }
    }
    
    static function getQByStatusWAQID($qid, $task_id) {
        global $DB, $USER;
        $sql = "SELECT res.result, res.answer_mark, res.new_answer_mark, res.mark, res.ammount, res.comment FROM `tickets_qc_results` as res 
        WHERE res.task_id=".$DB->F($task_id)." AND res.q_id=".$DB->F($qid).";";
        return $DB->getRow($sql, true);
    }

}

?>
