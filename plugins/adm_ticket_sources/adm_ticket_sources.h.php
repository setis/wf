<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Рубрикатор &laquo;Откуда узнали&raquo;";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Рубрикатор \"Откуда узнали\"";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование источника \"Откуда узнали\"";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление источника";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение источника";
    
    
}
?>