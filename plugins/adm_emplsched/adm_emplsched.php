<?php
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\User;
use Symfony\Component\HttpFoundation\Request;
use WF\Technician\Events\TechnicianScheduleCloseEvent;
use WF\Technician\Events\TechnicianScheduleOpenEvent;

class adm_emplsched_plugin extends Plugin
{

    public function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    public function view_schedule()
    {
        global $DB, $USER;
        if (isset($_POST["save"])) {
            if ($this->save($_POST)) {
                redirect($this->getLink('view_schedule') . "?wDate=" . $_POST["startDate"] . "&scFilter=" . $_POST["sc"], "Расписание сохранено");
            } else {
                UIError("Сохранение не выполнено!");
            }
        }

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));

        if ($USER->getTemplate() != "default") {
            $tpl->loadTemplatefile($USER->getTemplate() . "/schedule.tmpl.htm");
        } else {
            $tpl->loadTemplatefile("schedule.tmpl.htm");
        }

        $dateFormat = 'd-m-Y';
        $interfaceDateFormat = 'd M Y';
        $cDate = date('D');
        $_REQUEST["wtype_id"] = $this->getCookie("wtype_id", $_REQUEST["wtype_id"]);

        if ($wtype = adm_ticktypes_plugin::getOptList($_REQUEST["wtype_id"])) {
            $tpl->setVariable("WTYPELIST", $wtype);
        } else {
            UIError("Отсутствует справочник видов работ!");
        }

        $tpl->setVariable("EDITACTION", $this->getUID() . "/loadsched");
        $tpl->setVariable("SAVEACTION", $this->getUID() . "/savesched");
        $tpl->setVariable("RESETACTION", $this->getUID() . "/resetsched");
        $tpl->setVariable("MULTRESETACTION", $this->getUID() . "/resetweektf");
        $tpl->setVariable("MULTEDITACTION", $this->getUID() . "/editweektf");
        $tpl->setVariable("COPYLASTWEEKTFACTION", $this->getUID() . "/copylastweektf");
        $isChiefTech = User::isChiefTech($this->getUserId());

        $scFilter = (isset($_POST["sc"]) ? $_POST["sc"] : @$_REQUEST["scFilter"]);

        $tpl->setCurrentBlock("sc_list");
        if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getFilterList")) {
            $empl = new adm_sc_plugin();
            $scFilterList = $empl->getFilterList($scFilter);
            if ($scFilterList) {
                $tpl->setVariable("SCLIST", $scFilterList);
            } else {
                UIError("Отсутствует список Сервисных Центров!", "Ошибка", true, "Создать СЦ", "/adm_sc/edit");
            }
        } else {
            UIError("Не найден модуль Сервисные Центры!");
        }

        $tpl->parse("sc_list");
        $tpl->setCurrentBlock("sc_list1");
        if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getFilterList")) {
            $empl = new adm_sc_plugin();
            $scFilterList = $empl->getFilterList($scFilter);
            if ($scFilterList) {
                $tpl->setVariable("SCLIST1", $scFilterList);
            } else {
                UIError("Отсутствует список Сервисных Центров!", "Ошибка", true, "Создать СЦ", "/adm_sc/edit");
            }
        } else {
            UIError("Не найден модуль Сервисные Центры!");
        }

        $tpl->parse("sc_list1");

        $startDate = $_REQUEST["wDate"] ? (date('w', strtotime($_REQUEST["wDate"])) == 1 ? $_REQUEST["wDate"] : (date('w', strtotime($_REQUEST["wDate"])) ? date($dateFormat, strtotime($_REQUEST["wDate"]) - (date('w', strtotime($_REQUEST["wDate"])) - 1) * 24 * 60 * 60) : date($dateFormat, strtotime($_REQUEST["wDate"]) - 6 * 24 * 60 * 60))) : ($cDate == "Sun" ? date($dateFormat, strtotime('Mon last week')) : date($dateFormat, strtotime('Mon this week')));
        if ($_REQUEST["tdate"]) {
            $targetDate = $_REQUEST["tdate"];
            $startDate = $_REQUEST["tdate"] ? (date('w', strtotime($_REQUEST["tdate"])) == 1 ? $_REQUEST["tdate"] : (date('w', strtotime($_REQUEST["tdate"])) ? date($dateFormat, strtotime($_REQUEST["tdate"]) - (date('w', strtotime($_REQUEST["tdate"])) - 1) * 24 * 60 * 60) : date($dateFormat, strtotime($_REQUEST["tdate"]) - 6 * 24 * 60 * 60))) : ($cDate == "Sun" ? date($dateFormat, strtotime('Mon last week')) : date($dateFormat, strtotime('Mon this week')));
            $tsd = $_REQUEST["tdate"];
        } else {
            $targetDate = date("d-m-Y");
            $tsd = false;
        }
        $endDate = date($dateFormat, strtotime($startDate) + 6 * 86400);
        $tpl->setVariable("INT_CDATE", rudate($interfaceDateFormat, $targetDate));
        $tpl->setVariable("INT_START", rudate($interfaceDateFormat, $startDate));
        $tpl->setVariable("CURRENTSTARTDATE", $startDate);
        $tpl->setVariable("INT_END", rudate($interfaceDateFormat, $endDate));

        $tpl->setVariable("PREVDAYLINK", $this->getUID() . "/view_schedule/?tdate=" . date("d-m-Y", strtotime($targetDate . " - 1 day")));
        $tpl->setVariable("CURRENTDAYLINK", $this->getUID() . "/view_schedule/?tdate=" . date("d-m-Y"));
        $tpl->setVariable("NEXTDAYLINK", $this->getUID() . "/view_schedule/?tdate=" . date("d-m-Y", strtotime($targetDate . " + 1 day")));


        $tpl->setVariable("NEXTWEEKLINK", $this->getUID() . "/view_schedule/?wDate=" . date($dateFormat, strtotime($endDate) + 2 * 24 * 60 * 60) . ($scFilter ? "&scFilter=" . $scFilter : ""));
        $tpl->setVariable("CURRENTWEEKLINK", $this->getUID() . "/view_schedule" . ($scFilter ? "?scFilter=" . $scFilter : ""));
        $tpl->setVariable("PREVWEEKLINK", $this->getUID() . "/view_schedule/?wDate=" . date($dateFormat, strtotime($startDate) - 2 * 24 * 60 * 60) . ($scFilter ? "&scFilter=" . $scFilter : ""));
        if (!$_REQUEST["wDate"] || (strtotime(date($dateFormat)) >= strtotime($startDate) && strtotime(date($dateFormat)) <= strtotime($endDate))) {
            $tpl->touchBlock("cweekword");
        }
        for ($i = 0; $i < 7; $i++) {
            $tpl->setCurrentBlock("dateGridHead");
            if (date($dateFormat, strtotime($startDate) + $i * 24 * 60 * 60) == date($dateFormat) && !$tsd) {
                $tpl->setVariable("GRIDHEADCURRENTDAY", "gridheadsel");
            } else {
                if (date($dateFormat, strtotime($startDate) + $i * 24 * 60 * 60) == date($dateFormat, strtotime($tsd))) {
                    $tpl->setVariable("GRIDHEADCURRENTDAY", " ");
                } else {
                    if (date($dateFormat, strtotime($startDate) + $i * 24 * 60 * 60) == date($dateFormat)) {
                        $tpl->setVariable("GRIDHEADCURRENTDAY", "gridheadsel hidden-sm hidden-xs");
                    } else {
                        $tpl->setVariable("GRIDHEADCURRENTDAY", "hidden-sm hidden-xs");
                    }
                }
            }
            $tpl->setVariable("GRIDHEADDATE", rudate($interfaceDateFormat, date($interfaceDateFormat, strtotime($startDate) + $i * 24 * 60 * 60)) . "<br />" . rudate("l", date($interfaceDateFormat, strtotime($startDate) + $i * 24 * 60 * 60)));
            $tpl->parse("dateGridHead");
        }

        $empl = new adm_empl_plugin();
        if ($_REQUEST["wtype_id"]) {
            $userFilterList = $empl->getInvEmpl($scFilter, $_REQUEST["wtype_id"]);
        } else {
            $userFilterList = $empl->getInvEmpl($scFilter);
        }

        if ($userFilterList) {
            foreach ($userFilterList as $uParam) {
                $lock = false;
                $tpl->setCurrentBlock("userList");
                $tpl->setVariable("FIO", $uParam['fio']);
                $tpl->setVariable("CURRTECH_ID", $uParam['id']);

                $userTaskCount = 0;
                for ($i = 0; $i < 7; $i++) {

                    $sql = "SELECT COUNT(`task_id`) FROM `gfx` WHERE `c_date`=" . $DB->F(date('Y-m-d', strtotime($startDate) + $i * 86400)) . " AND `empl_id`=" . $DB->F($uParam['id']) . ";";
                    $taskCount = $DB->getField($sql);
                    $userTaskCount += $taskCount;
                    if ($taskCount) {
                        $sql = "SELECT * FROM `gfx` WHERE `c_date`=" . $DB->F(date('Y-m-d', strtotime($startDate) + $i * 86400)) . " AND `empl_id`=" . $DB->F($uParam['id']) . " ORDER BY `startTime` DESC LIMIT 1;";
                        $grec = $DB->getRow($sql, true);
                        $tTTypeL = $DB->getField("SELECT `duration` FROM `task_types` WHERE `id`=(SELECT `task_type` FROM `tickets` WHERE `task_id`=" . $DB->F($grec["task_id"]) . ");");
                        $tTTypeL += $grec["startTime"];
                    }
                    $cellAppendClass = "";


                    if (date($dateFormat, strtotime($startDate) + $i * 24 * 60 * 60) == date($dateFormat) && !$tsd) {
                        $cellAppendClass = "gridbodysel";
                    } else {
                        if (date($dateFormat, strtotime($startDate) + $i * 24 * 60 * 60) == date($dateFormat, strtotime($tsd))) {
                            $cellAppendClass = " ";
                        } else {
                            if (date($dateFormat, strtotime($startDate) + $i * 24 * 60 * 60) == date($dateFormat)) {
                                $cellAppendClass = "hidden-sm hidden-xs gridbodysel";
                            } else {
                                $cellAppendClass = "hidden-sm hidden-xs";
                            }
                        }
                    }
                    if ($taskCount) {
                        $cellAppendClass .= " cellHasTask ";
                    }
                    $tpl->setVariable("SELCURRENTDATE" . $i, "class='" . $cellAppendClass . "'");


                    $sql = "SELECT * FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($uParam['id']) . " AND `sched_date`=" . $DB->F(date("Y-m-d", strtotime($startDate) + $i * 24 * 60 * 60)) . ";";
                    $r = $DB->getRow($sql, true);
                    $tpl->setVariable("SCHED" . $i . "CONTAINER", $uParam['id'] . date("dmY", strtotime($startDate) + $i * 24 * 60 * 60));
                    if ($r) {
                        $hourStart = strlen(floor($r['starttime'] / 60)) == 1 ? "0" . floor($r['starttime'] / 60) : floor($r['starttime'] / 60);
                        $minStart = strlen($r['starttime'] % 60) == 1 ? "0" . $r['starttime'] % 60 : $r['starttime'] % 60;
                        $hourEnd = strlen(floor($r['endtime'] / 60)) == 1 ? "0" . floor($r['endtime'] / 60) : floor($r['endtime'] / 60);
                        $minEnd = strlen($r['endtime'] % 60) == 1 ? "0" . $r['endtime'] % 60 : $r['endtime'] % 60;
                        $tpl->setVariable("SCHED" . $i, $hourStart . ":" . $minStart . " - " . $hourEnd . ":" . $minEnd);
                    } else {
                        $tpl->setVariable("SCHED" . $i, "--:-- - --:--");
                    }
                    if ($USER->checkAccess($this->getUID(), User::ACCESS_WRITE) && ((strtotime($startDate) + $i * 24 * 60 * 60) >= strtotime(date($dateFormat)))) {
                        $sql = "SELECT `wtype_id` FROM `list_empl` WHERE `user_id`=" . $DB->F($uParam['id']) . ";";
                        $wtc = $DB->getCell($sql);
                        $wtc = implode(",", $wtc);
                        if (!(date("H:i") > "10:00" && !$r && !User::isChiefTech($USER->getId()) && (($wtc == "10,37" || $wtc == "37,10") || ($wtc == "10") || ($wtc == "37")) && (strtotime($startDate) + $i * 24 * 60 * 60) == strtotime(date($dateFormat))) || $USER->checkAccess("editskpscheduletoday")) {
                            $tpl->setCurrentBlock("edittf" . $i);
                            $tpl->setVariable("USERID" . $i, $uParam['id']);
                            $tpl->setVariable("CDATE" . $i, date($dateFormat, strtotime($startDate) + $i * 24 * 60 * 60));
                            if ($taskCount != 0) {
                                $tpl->setVariable("FIXINT" . $i, $taskCount > 0 ? 'true' : 'false');
                                $tpl->setVariable("MININTVALUE" . $i, $tTTypeL);
                            } else {
                                $tpl->setVariable("MININTVALUE" . $i, 0);
                            }
                            $tpl->parse("edittf" . $i);
                        }
                        if (!$USER->checkAccess("editskpscheduletoday") && ((date("H:i") > "10:00") && (($wtc == "10,37" || $wtc == "37,10") || ($wtc == "10") || ($wtc == "37")) && (strtotime($startDate) + $i * 24 * 60 * 60) == strtotime(date($dateFormat)))) {
                            $lock = true;
                        }
                    }
                }

                if ($userTaskCount == 0 && $USER->checkAccess($this->getUID(), User::ACCESS_WRITE) && ((strtotime(date($dateFormat)) >= strtotime($startDate) && strtotime(date($dateFormat)) <= strtotime($endDate)) || strtotime($startDate) > strtotime(date($dateFormat)))) {
                    if (strtotime($startDate) > strtotime(date($dateFormat))) {

                        $tpl->setCurrentBlock("copyfromlastweek");
                        $tpl->setVariable("USERIDLW", $uParam['id']);
                        $tpl->setVariable("CDATESTARTLW", $startDate);
                        $tpl->parse("copyfromlastweek");
                    }
                    if (!$lock) {
                        $tpl->setCurrentBlock("edittfmultiple");
                        $tpl->setVariable("USERID", $uParam['id']);
                        if ((strtotime(date($dateFormat)) >= strtotime($startDate) && strtotime(date($dateFormat)) <= strtotime($endDate))) {
                            $c_date = date($dateFormat);
                        } else {
                            $c_date = $startDate;
                        }
                        $tpl->setVariable("CDATESTART", $c_date);
                        $tpl->parse("edittfmultiple");
                    }
                }
                $tpl->parse("userList");
            }
        } else
            UIError("Отсутствует список Техников!", "Ошибка", true, "Добавить Техников", "/adm_empl/addmultiple/");

        UIHeader($tpl);
        $tpl->show();
    }

    function loadsched()
    {
        global $DB, $USER, $PLUGINS;
        $user_id = $_REQUEST['user_id'];
        $c_date = $_REQUEST['c_date'];
        if ($user_id && $c_date) {
            $sql = "SELECT * FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($user_id) . " AND DATE_FORMAT(`sched_date`, '%d-%m-%Y')=" . $DB->F($c_date) . ";";
            $result = $DB->getRow($sql, true);
            if ($result) {
                echo "ok:" . $result['starttime'] . ":" . $result['endtime'];
            } else {
                echo "empty" . $sql;
            }
        } else {
            echo "error";
        }
    }

    public function savesched(Request $request)
    {
        $user_id = $request->get('user_id');
        $c_date = $request->get('c_date');
        $start = $request->get('start');
        $end = $request->get('end');
        if (!($user_id && $c_date && $start && $end)) {
            return "Недостаточно данных для выполнения операции!";
        }

        $em = $this->getEm();
        $date = new \DateTime($c_date);
        $executor = $em->find(\models\User::class, $user_id);

        $timeframe = $em->getRepository(\models\ExecutorTimeframe::class)->findOneBy([
            'executor' => $executor,
            'schedDate' => $date,
        ]);

        if (null === $timeframe) {
            $timeframe = (new models\ExecutorTimeframe())
                ->setUserExecutor($em->getReference(\models\User::class, $user_id))
                ->setSchedDate(new \DateTime($c_date));

        }

        $timeframe->setStartTime($start)
            ->setEndTime($end);

        $em->persist($timeframe);
        $em->flush();
        $event = (new TechnicianScheduleOpenEvent($executor, $timeframe->getTimeStart(), $timeframe->getTimeEnd()));
        $this->getDispatcher()->dispatch(TechnicianScheduleOpenEvent::NAME, $event);

        echo "Изменения сохранены!";

    }

    public function editweektf(Request $request)
    {
        $user_id = $request->get('user_id');
        $c_date = $request->get('c_date');
        $start = $request->get('start');
        $end = $request->get('end');
        if (!($user_id && $c_date && $start && $end)) {
            return "Недостаточно данных для выполнения операции!";
        }

        $this->getEm()->transactional(function (Doctrine\ORM\EntityManagerInterface $em) use ($user_id, $c_date, $start, $end) {
            $cwd = date("w", strtotime($c_date)) == 0 ? 7 : date("w", strtotime($c_date));
            $f_date = date("d-m-Y", (7 - $cwd) * 86400 + strtotime($c_date));
            $executor = $this->getEm()->find(\models\User::class, $user_id);

            $em->createQueryBuilder()
                ->delete(\models\ExecutorTimeframe::class, 'tf')
                ->where('tf.executor = :executor')
                ->andWhere('tf.schedDate BETWEEN :start AND :end')
                ->setParameter('executor', $executor)
                ->setParameter('start', new \DateTime($c_date))
                ->setParameter('end', new \DateTime($f_date))
                ->getQuery()
                ->execute();

            for ($i = 0; $i <= (7 - $cwd); $i++) {
                $ccd = date("Y-m-d", $i * 86400 + strtotime($c_date));

                $timeframe = (new models\ExecutorTimeframe())
                    ->setUserExecutor($executor)
                    ->setSchedDate(new \DateTime($ccd))
                    ->setStartTime($start)
                    ->setEndTime($end);
                $em->persist($timeframe);

                $event = (new TechnicianScheduleOpenEvent($executor, $timeframe->getTimeStart(), $timeframe->getTimeEnd()));
                $this->getDispatcher()->dispatch(TechnicianScheduleOpenEvent::NAME, $event);
            }
        });

        echo "Изменения сохранены!";

    }

    function resetweektf()
    {
        global $DB;

        $user_id = $_POST['user_id'];
        $c_date = $_POST['c_date'];
        $cwd = date("w", strtotime($c_date)) == 0 ? 7 : date("w", strtotime($c_date));
        $f_date = date("d-m-Y", (7 - $cwd) * 86400 + strtotime($c_date));
        $cdate = explode("-", $c_date);
        $fdate = explode("-", $f_date);
        $sql = "DELETE FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($user_id) . " AND `sched_date` >=" . $DB->F($cdate[2] . "-" . $cdate[1] . "-" . $cdate[0]) . " AND `sched_date`<=" . $DB->F($fdate[2] . "-" . $fdate[1] . "-" . $fdate[0]) . ";";
        $DB->query($sql);
        if ($DB->errno()) {
            echo $DB->error();
            return;
        } else {

            /** @var \models\User $tech */
            $tech = $this->getEm()->find(\models\User::class, $user_id);
            $from = new DateTime($c_date . ' 00:00:00');
            $until = clone $from;
            $until->add(new DateInterval('P6DT23H59M59S'));
            $event = (new TechnicianScheduleCloseEvent($tech, $from, $until));
            $this->getDispatcher()->dispatch(TechnicianScheduleCloseEvent::NAME, $event);

            echo "Изменения сохранены!";
        }
    }

    function resetsched(Request $request)
    {
        global $DB;

        $user_id = $request->get('user_id');
        $c_date = $request->get('c_date');

        if ($user_id && $c_date) {
            $sql = "DELETE FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($user_id) . " AND DATE_FORMAT(`sched_date`, '%d-%m-%Y')=" . $DB->F($c_date) . ";";
            $DB->query($sql);
            if ($DB->errno()) {
                return $DB->error();
            } else {

                /** @var \models\User $tech */
                $tech = $this->getEm()->find(\models\User::class, $user_id);
                $from = new DateTime($c_date);
                $until = clone $from;
                $until->add(new DateInterval('PT23H59M59S'));
                $event = (new TechnicianScheduleCloseEvent($tech, $from, $until));
                $this->getDispatcher()->dispatch(TechnicianScheduleCloseEvent::NAME, $event);

                return $event->getResult();
            }
        }

        echo "Недостаточно данных для выполнения операции!";
        return;
    }

    public function copylastweektf(Request $request)
    {
        global $DB;
        $user_id = $request->get('user_id');
        $c_date = $request->get('c_date');

        if (!($user_id && $c_date)) {
            return "Недостаточно данных для выполнения операции!";
        }

        $ss_date = date("d-m-Y", strtotime($c_date) - 7 * 86400);
        $sdow = date("w", strtotime($ss_date)) == 0 ? 7 : date("w", strtotime($ss_date));
        $sf_date = date("d-m-Y", (7 - $sdow) * 86400 + strtotime($ss_date));
        $ssdate = explode("-", $ss_date);
        $sfdate = explode("-", $sf_date);
        $sql = "SELECT user_id, sched_date, starttime, endtime FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($user_id) . " AND `sched_date` >=" . $DB->F($ssdate[2] . "-" . $ssdate[1] . "-" . $ssdate[0]) . " AND `sched_date`<=" . $DB->F($sfdate[2] . "-" . $sfdate[1] . "-" . $sfdate[0]) . " ORDER BY `sched_date`;";
        $DB->query($sql);
        if (!$DB->num_rows()) {
            return "Отсутствует интервал прошлой недели.";
        }

        $timeframes = $this->getEm()->createQueryBuilder()
            ->select('t')
            ->from(\models\ExecutorTimeframe::class, 't')
            ->where('t.executor = :executor')
            ->andWhere('t.schedDate BETWEEN :start AND :end')
            ->setParameter('executor', $user_id)
            ->setParameter('start', new \DateTime($ss_date))
            ->setParameter('end', new \DateTime($sf_date))
            ->getQuery()
            ->getResult();

        if (empty($timeframes)) {
            return "Отсутствует интервал прошлой недели.";
        }

        $this->getEm()->transactional(function (Doctrine\ORM\EntityManagerInterface $em) use ($user_id, $c_date, $start, $end, $timeframes) {
            $cwd = date("w", strtotime($c_date)) == 0 ? 7 : date("w", strtotime($c_date));
            $f_date = date("d-m-Y", (7 - $cwd) * 86400 + strtotime($c_date));
            $executor = $this->getEm()->find(\models\User::class, $user_id);

            $em->createQueryBuilder()
                ->delete(\models\ExecutorTimeframe::class, 'tf')
                ->where('tf.executor = :executor')
                ->andWhere('tf.schedDate BETWEEN :start AND :end')
                ->setParameter('executor', $executor)
                ->setParameter('start', new \DateTime($c_date))
                ->setParameter('end', new \DateTime($f_date))
                ->getQuery()
                ->execute();

            $week = new \DateInterval('P7D');
            /* @var $timeframe \models\ExecutorTimeframe */
            foreach ($timeframes as $timeframe) {
                $date = clone $timeframe->getSchedDate();
                $date->add($week);

                $newTimeframe = (new models\ExecutorTimeframe())
                    ->setUserExecutor($executor)
                    ->setSchedDate($date)
                    ->setStartTime($timeframe->getStartTime())
                    ->setEndTime($timeframe->getEndTime());

                $em->persist($newTimeframe);

                $event = (new TechnicianScheduleOpenEvent($executor, $timeframe->getTimeStart(), $timeframe->getTimeEnd()));
                $this->getDispatcher()->dispatch(TechnicianScheduleOpenEvent::NAME, $event);
            }
        });

        echo "Изменения сохранены!";

    }

}
