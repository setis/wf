<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Расписание работы Техников";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['view_schedule'] = "Расписание работы Техников";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['savesched'] = "Сохранение рабочего интервала";
    $PLUGINS[$plugin_uid]['events']['loadsched'] = "Загрузка рабочего интервала";
    $PLUGINS[$plugin_uid]['events']['resetsched'] = "Удаление рабочего интервала";
    $PLUGINS[$plugin_uid]['events']['editweektf'] = "Редактор рабочего интервала на неделю";
    $PLUGINS[$plugin_uid]['events']['resetweektf'] = "Сброс рабочего интервала на неделю";
    $PLUGINS[$plugin_uid]['events']['copylastweektf'] = "Сброс рабочего интервала на неделю";
}

$plugin_uid = "editskpscheduletoday";


$PLUGINS[$plugin_uid]['name'] = "Редактирование расписания текущего дня техников СКП и МГТС ADSL";
$PLUGINS[$plugin_uid]['hidden'] = true;


?>