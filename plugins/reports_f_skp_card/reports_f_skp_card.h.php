<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2014
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "ОТЧЕТ: Заявки с флагом ДС на Счете (или карте)";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Заявки с флагом ДС на Счете (или карте)";
    
}

?>