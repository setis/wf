<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;


require_once(dirname(__FILE__)."/../services/services.php");

class reports_f_skp_card_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $_REQUEST["sc_id"] = $this->getCookie("sc_id", (isset($_REQUEST["sc_id"]) ? $_REQUEST["sc_id"] : 8));
        $_REQUEST["createreport"] = $this->getCookie("createreport", $_REQUEST["createreport"]);
        $_REQUEST["month"] = $this->getCookie('month', $_REQUEST["month"] ? $_REQUEST["month"] : date("m"));
        $_REQUEST["year"] = $this->getCookie('year', $_REQUEST["year"] ? $_REQUEST["year"] : date("Y"));
        $dateParams = array(0 => "Дата создания", 1 => "Дата выполнения", 2=>"Дата изменения", 3=>"Дата принятия ДС");
        if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
            $cntr = new kontr_plugin();
        }   
        $contrCount = $cntr->getListSer();                    
        if (!isset($_REQUEST['createprintversion']) && !isset($_REQUEST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_SKP_TOTAL", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            $tpl->setVariable("MONTH", array2options($month, $_REQUEST["month"]));
            $tpl->setVariable("YEAR", array2options($year, $_REQUEST["year"]));
            
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id"]));
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListSer(@$_POST["cnt_id"]));
            } 
            
            
        }
        if (@$_REQUEST["createreport"] || @$_REQUEST["createprintversion"] || @$_REQUEST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_REQUEST['createprintversion']) || @$_REQUEST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $period = $_REQUEST["year"]."-".$_REQUEST["month"];
            $rtpl->setVariable('CURRENT_PERIOD', $month[$_REQUEST["month"]]." ".$year[$_REQUEST["year"]]);
            $sc = adm_sc_plugin::getSC($_REQUEST["sc_id"]);
            $rtpl->setVariable("SC_NAME", $_REQUEST["sc_id"] ? $sc['title'] : "Все");
            $sql = "SELECT t.*, (select `fio` from `users` where `id` in (select `empl_id` from `gfx` where `task_id`=t.task_id)) as tech, (select `contr_title` from `list_contr` where `id`=t.cnt_id) as contr_name FROM `tickets` as t WHERE DATE_FORMAT(t.iscard_date, '%Y-%m')=".$DB->F($period)." AND t.iscard>0 AND t.cnt_id=".$DB->F($_POST["cnt_id"])." ";
            
            if ($sc) $sql .= " AND t.sc_id=".$DB->F($_REQUEST["sc_id"])." ";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            if ($DB->num_rows()) {
                while ($res = $DB->fetch(true)) {
                    $rtpl->setCurrentBlock("rep_row");
                    $rtpl->setVariable("ID", $res["task_id"]);
                    $rtpl->setVariable("CONTR", $res["contr_name"]);
                    $rtpl->setVariable("TECH", $res["tech"]);
                    $rtpl->setVariable("DATE_ISCARD", $res["iscard_date"] ? date("d.m.Y", strtotime($res["iscard_date"])) : "&mdash;");
                    $rtpl->parse("rep_row"); 
                }
            } else {
                $rtpl->touchBlock("no-rows");
            }
            $DB->free();
            $rtpl->setVariable("REP_CDATE", date("d.m.Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_REQUEST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_skp_total__".$_REQUEST["month"]."-".$_REQUEST["year"].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_REQUEST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 
    
}
?>