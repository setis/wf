<?php

/**
 * Plugin Implementation
 * 
 * @author kblp
 * @copyright 2011
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\tickets\ConnectionTicket;


require_once(dirname(__FILE__)."/../connections/connections.php");

/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 * 
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
 
class import_conn_akado_plugin extends Plugin
{       
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function viewimport()
    {
        global $DB, $USER;
        
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
         
        $tpl->loadTemplatefile($USER->getTemplate()."/import_conn_akado.tmpl.htm");
        
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $agr_list = array2options(kontr_plugin::getAgrListByContrIDA(getcfg('akado_contr_id')), $_POST["filter_agr_id"]);
        //$tpl->setVariable("FILTER_AGR_OPTIONS", $agr_list);
        $sql = "SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`='connections' ORDER BY `title`";
        $tpl->setVariable("TYPE_OPTIONS", array2options($DB->getCell2($sql)));
        $tpl->setVariable("AKADO_ID", getcfg('akado_contr_id'));
        //die(array2options($DB->getCell2($sql)));
        if (isset($_POST["goimport"])) {
            $csv_mimetypes = array(
                'text/csv',
                'text/plain',
                'application/csv',
                'text/comma-separated-values',
                'application/excel',
                'application/vnd.ms-excel',
                'application/vnd.msexcel',
                'text/anytext',
                'application/octet-stream',
                'application/txt',
            );
            if (!$_POST["type_id"] || ! $_POST["agr_id"]) {
                UIError("Не указан договор и/или вид работ.");
            }
            if (in_array($_FILES['source']['type'], $csv_mimetypes)) {
                
                $csv = array_map('str_getcsv', file($_FILES['source']["tmp_name"]));
                //print_r($csv);
                
                $titles = explode(",",preg_replace("/\t/", ",", $csv[0][0]));
                if (count($titles) == 19) {
                    if (trim($titles[0]) != "Номер сметы" || 
                          trim($titles[1]) != "№ Абонента" || 
                          trim($titles[2]) != "Дата" || 
                          trim($titles[3]) != "Время с" || 
                          trim($titles[4]) != "Время по" || 
                          trim($titles[5]) != "Статус заявки" || 
                          trim($titles[6]) != "Тип абонента" || 
                          trim($titles[7]) != "ФИО Название организации" || 
                          trim($titles[8]) != "Город" || 
                          trim($titles[9]) != "Улица" || 
                          trim($titles[10]) != "Дом" || 
                          trim($titles[11]) != "Корпус" || 
                          trim($titles[12]) != "Строение" || 
                          trim($titles[13]) != "Квартира" || 
                          trim($titles[14]) != "Тел.моб." || 
                          trim($titles[15]) != "Тел. Конт." || 
                          trim($titles[16]) != "Тел.гор." || 
                          trim($titles[17]) != "Услуги" || 
                          trim($titles[18]) != "Примечание")  
                    {              
                        UIError("Формат файла не соответствует утвержденному.");
                    } 
                } else {
                    UIError("Формат файла не соответствует утвержденному.");
                }
                $i = false;
                foreach($csv as $item) {
                    if (!$i) {
                        $i=true;
                        continue;
                    }
                    $taskList[] = explode(",", preg_replace("/\t/", ",", $item[0]));
                }
                
                if (count($taskList)) {
                    $tpl->setCurrentBlock("import_results");
                    foreach($taskList as $task) {
                        //print_r($task);
                        //die();
                        $sql = "SELECT `task_id` FROM `tickets` WHERE `cnt_id`=".$DB->F(getcfg('akado_contr_id'))." AND `clnttnum`=".$DB->F(trim($task[0])).";";
                        $task_id = $DB->getField($sql);
                        $tpl->setCurrentBlock("ir-item");
                        /*if ($task_id) {
                            $tpl->setVariable("SMETA", $task[0]);
                            $tpl->setVariable("TASKID", $task_id);
                            $tpl->setCurrentBlock("islink");
                            $tpl->setVariable("TASK_LINK", link2('connections/viewticket?task_id='.$task_id));
                            $tpl->parse("islink"); 
                            $tpl->touchBlock("linkdub");                               
                            $tpl->setVariable("RESULT", "Уже есть в системе");
                            $tpl->setVariable("RESULTSTYLE", "#FFF700");
                            $t = new ConnectionTicket($task_id);
                            $params = array();
                            $params[] = "Дата подключения: ". $t->getSlotDateBegin()." ".$t->getSlotTimeBegin() ." - ".$t->getSlotTimeEnd();
                            $params[] = "Адрес: ".$task[8]." ".$task[9]." ".$task[10]." ".$task[13];
                            $params[] = "Тип: ".($t->getClientType() == "1" ? "Физ. лицо" : "Юр. лицо");
                            if ($t->getOrgName()) {
                                $params[] = "Компания: ".$t->getOrgName();
                            }
                            $params[] = "ФИО: ".$t->getFio();
                            $params[] = "Телефоны: ".((trim(implode(",", $t->getPhones())) != ",") ? implode(" ", $t->getPhones()) : " - нет - ");
                            $tpl->setVariable("TASKPARAM", implode("<br />", $params));
                         } else {*/
                            $tpl->setVariable("SMETA", $task[0]);
                            $phone1 = $task[14];
                            $phone2 = $task[16];
                            if (!$phone1 && !$phone2) {
                                $phone1 = $task[15];
                            }
                            $dom_id = addr_interface_plugin::getKLADRCodeByAddressNBN(array(
                                        'city_name' => trim($task[8]),
                                        'street_name' => trim($task[9]),
                                        'building_name' => trim(str_replace("'", "",$task[10]))
                                    ));
                            $c_task_id = ConnectionTicket::createTicket(getcfg('akado_contr_id'), 
                                                    intval($_POST["type_id"]), 
                                                    intval($dom_id), 
                                                    ' ', 
                                                    ' ', 
                                                    trim($task[13]), 
                                                    " ", 
                                                    trim($task[6]) == "Ф" ? 1 : 2, 
                                                    trim($task[6]) == "Ф" ? '' : trim($task[7]), 
                                                    trim($task[7]), 
                                                    trim(str_replace("'", "", $phone1)), 
                                                    trim(str_replace("'", "", $phone2)), 
                                                    '', 
                                                    /*trim($task[18])*/'', 
                                                    implode("\r\n", explode("$", $task[17])), 
                                                    $_POST["agr_id"], 
                                                    "", 
                                                    "", 
                                                    "", 
                                                    trim($task[1]), 
                                                    trim($task[0]), 
                                                    0, 
                                                    19, 
                                                    date("Y-m-d H:i:s", strtotime(str_replace(".", ":", $task[3]). " ".$task[2])), 
                                                    date("Y-m-d H:i:s", strtotime(str_replace(".", ":", $task[4]). " ".$task[2])),
                                                    "",
                                                    "", 
                                                    0,
                                                    0, 
                                                    0, 
                                                    0,
                                                    0,
                                                    0);
                            if ($c_task_id) {
                                $t = new ConnectionTicket($c_task_id);
                                $comment = array();
                                if ($task_id) {
                                    $comment[] = "Дубликат заявки № ". $task_id;
                                }
                                $comment[] = "№ сметы: ".trim($task[0]);
                                $comment[] = "ID абонента: ".trim($task[1]);
                                $comment[] = "Дата: ".trim($task[2]);
                                $comment[] = "Время с: ".trim($task[3]);
                                $comment[] = "Время по: ".trim($task[4]);
                                $comment[] = "Статус заявки: ".trim($task[5]);
                                $comment[] = "Тип абонента: ".trim($task[6]);
                                $comment[] = "ФИО Название организации: ".trim($task[7]);
                                $comment[] = "Город: ".trim($task[8]);
                                $comment[] = "Улица: ".trim($task[9]);
                                $comment[] = "Дом: ".trim(str_replace("'", "",$task[10]));
                                $comment[] = "Корпус: ".trim($task[11]);
                                $comment[] = "Строение: ".trim($task[12]);
                                $comment[] = "Квартира: ".trim($task[13]);
                                $comment[] = "Тел.моб.: ".trim(str_replace("'", "", $phone1));
                                $comment[] = "Тел. Конт.: ".trim(str_replace("'", "", $task[15]));
                                $comment[] = "Тел.гор.: ".trim(str_replace("'", "", $phone2));
                                $comment[] = "Услуги: ".implode(", ", explode("$", trim($task[17])));
                                $comment[] = "Примечание: ".trim($task[18]);
                                $t->addComment(implode("\r\n ", $comment), "Импорт АКАДО от ".date("H:i d.m.Y"));
                                $t->setClntOpAgr($task[1]);
                                $tpl->setVariable("RESULT", "<span style='color: #000000;'><strong>OK</strong></span>");
                                $tpl->setCurrentBlock("islink");
                                $tpl->setVariable("TASK_LINK", link2('connections/viewticket?task_id='.$t->getId(), false));
                                $tpl->parse("islink");
                                $tpl->touchBlock("linkdub");
                                $tpl->setVariable("RESULTSTYLE", "#00CC00");
                                $params = array();
                                $params[] = "Дата подключения: ". $t->getSlotDateBegin()." ".$t->getSlotTimeBegin() ." - ".$t->getSlotTimeEnd();
                                $params[] = "Адрес: ".$task[8]." ".$task[9]." ".str_replace("'", "",$task[10])." ".$task[13];
                                $params[] = "Тип: ".($t->getClientType() == "1" ? "Физ. лицо" : "Юр. лицо");
                                if ($t->getOrgName()) {
                                    $params[] = "Компания: ".$t->getOrgName();
                                }
                                $params[] = "ФИО: ".$t->getFio();
                                $params[] = "Телефоны: ".((trim(implode(",", $t->getPhones())) != ",") ? implode(" ", $t->getPhones()) : " - нет - ");
                                $tpl->setVariable("TASKPARAM", implode("<br />", $params));
                                $tpl->setVariable("TASKID", $t->getId());
                            } else {
                                $tpl->setVariable("RESULT", "<span style='color: #FFFFF;'>Ошибка импорта</span>");
                                $tpl->setVariable("RESULTSTYLE", "#FF0000");
                                $tpl->setVariable("TASKPARAM", "<center>&mdash;</center>");
                                $tpl->setVariable("TASKID", "<center>&mdash;</center>");
                            }
                        //}
                        $tpl->parse("ir-item");
                    }
                    $tpl->parse("import_results");
                } else {
                    $tpl->touchBlock("nothing");
                }
                
                
                //die();
            }
            
        }
        UIHeader($tpl);
        $tpl->show();
    }
}
?>