<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2015
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Импорт заявок на подключение АКАДО";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid))
{ 
    $PLUGINS[$plugin_uid]['events']['viewimport'] = "Импорт заявок на подключение АКАДО";
}
?>