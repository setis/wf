<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2015
 */

$plugin_uid = basename(__FILE__, ".h.php");
$PLUGINS[$plugin_uid]['name'] = "ГП: Типы/Подтипы заявок ГП";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';
if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['struct'] = "Типы/Подтипы заявок ГП";
    $PLUGINS[$plugin_uid]['events']['getType'] = "Тип заявок ГП (название)";
    $PLUGINS[$plugin_uid]['events']['getSubType'] = "Подтип заявок ГП (название)";
    $PLUGINS[$plugin_uid]['events']['getTypeId'] = "ИД типа заявки ГП по ИД подтипа";
//    $PLUGINS[$plugin_uid]['events']['getSubTypeOptions_ajax'] = "Список подтипов по ИД типа";
}

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['type_edit'] = "Редактирование типа ГП";
    $PLUGINS[$plugin_uid]['events']['type_save'] = "Сохранение типа ГП";
    $PLUGINS[$plugin_uid]['events']['type_del'] = "Удаление типа ГП";
    $PLUGINS[$plugin_uid]['events']['subtype_del'] = "Удаление подтипа ГП";
    $PLUGINS[$plugin_uid]['events']['subtype_edit'] = "Редактирование подтипа ГП";
    $PLUGINS[$plugin_uid]['events']['subtype_save'] = "Сохранение подтипа ГП";
    $PLUGINS[$plugin_uid]['events']['getTypeOptions'] = "Список типов";
    $PLUGINS[$plugin_uid]['events']['getSubTypeOptions'] = "Список подтипов (по ID типа)";
}

$PLUGINS[$plugin_uid]['events']['getTypeOptions_ajax'] = "Список типов по ИД типа";
$PLUGINS[$plugin_uid]['events']['getSubTypeOptions_ajax'] = "Список подтипов по ИД типа";


$PLUGINS[$plugin_uid]['events']['getTypeOptions1_ajax'] = "Список типов по ИД типа";
$PLUGINS[$plugin_uid]['events']['getSubTypeOptions1_ajax'] = "Список подтипов по ИД типа";


?>