<?php

/**
 * Plugin Implementation
 * 
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;



/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 * 
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
 
class adm_skp_price_plugin extends Plugin
{       
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
        function browse_wtypes_intel() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate()."/wtype_listitem_intel.tmpl.htm");
        if ($_REQUEST['skpwtype_contr_id']) {
            $sql_add = " l.contr_id=".$DB->F($_REQUEST['skpwtype_contr_id'])." ";            
        } else {
            $ret["error"] = "Не указан контрегент!";
            echo json_encode($ret);
            return false;
        }
        
        if ($_REQUEST['q'] != "") {
            $sql_add1[] = " lwt.title LIKE ".$DB->F("%".$_REQUEST['q']."%")."";
            $sql_add1[] = " l.contrcode LIKE ".$DB->F("%".$_REQUEST['q']."%")."";
            $sql_add1[] = " l.price LIKE ".$DB->F("%".$_REQUEST['q']."%")."";
        }
        if (sizeof($sql_add)) {
            $sql_add = " WHERE $sql_add AND (".implode(" OR ", $sql_add1).")";
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS l.*, lwt.ei_id, lwt.title   
                FROM `list_skp_p_price` as l LEFT JOIN `list_skp_p_wtypes` AS lwt ON l.wt_id=lwt.id $sql_add ORDER BY `id` LIMIT 15"; 
        $DB->query($sql);
        $total = $DB->getFoundRows();
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $tpl->setCurrentBlock("srow");
                $tpl->setVariable("ID", $r["wt_id"]);
                $tpl->setVariable("WTYPECODE", $r["contrcode"]);
                $tpl->setVariable("WTYPETITLE", $r["title"]);
                $tpl->setVariable("EI_MINVAL", $r["ei_min"]);
                $tpl->setVariable("EDIZM", adm_skp_edizm_plugin::getByID($r["ei_id"]));
                $tpl->setVariable("PRICE", number_format($r["price"], 2, ".", " "));
                $tpl->setVariable("PRICENONFORMAT", $r["price"]);
                $tpl->setVariable("CTGR", $r["contr"]);
                $tpl->parse("srow");                
            }
        } else {
            $tpl->touchBlock("srownorows");
        } 
        $ret["sql"] = $sql;
        $ret["total"] = $total;
        $ret["pages"] = pages_ajax($_REQUEST['filter_start'], 7, $total, "#start-%s");
        $ret["tpl"] = $tpl->get();
        $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;
    }
    
    function browse_wtypes() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate()."/wtype_listitem.tmpl.htm");
        if ($_REQUEST['skpwtype_contr_id']) {
            $sql_add[] = " l.contr_id=".$DB->F($_REQUEST['skpwtype_contr_id'])." ";            
        } else {
            $ret["error"] = "Не указан контрегент!";
            echo json_encode($ret);
            return false;
        }
        if ($_REQUEST['sswt_ps_id']) {
            $sql_add[] = " l.cat_id=".$DB->F($_REQUEST['sswt_ps_id'])."";
        }
        if ($_REQUEST['filter_id'] != "") {
            $sql_add[] = " l.contrcode=".$DB->F($_REQUEST['filter_id'])."";
        }
        if ($_REQUEST['filter_name'] != "") {
            $sql_add[] = " lwt.title LIKE ".$DB->F("%".$_REQUEST['filter_name']."%")."";
        }
        if (sizeof($sql_add)) {
            $sql_add = " WHERE ".implode(" AND ", $sql_add);
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS l.*, lwt.ei_id, lwt.title 
                
                FROM `list_skp_p_price` as l LEFT JOIN `list_skp_p_wtypes` AS lwt ON l.wt_id=lwt.id $sql_add ORDER BY `id` LIMIT {$_REQUEST['filter_start']},7"; 
        $DB->query($sql);
        $total = $DB->getFoundRows();
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $tpl->setCurrentBlock("srow");
                $tpl->setVariable("ID", $r["wt_id"]);
                $tpl->setVariable("WTYPECODE", $r["contrcode"]);
                $tpl->setVariable("WTYPETITLE", $r["title"]);
                $tpl->setVariable("EI_MINVAL", $r["ei_min"]);
                $tpl->setVariable("EDIZM", adm_skp_edizm_plugin::getByID($r["ei_id"]));
                $tpl->setVariable("PRICE", number_format($r["price"], 2, ".", " "));
                $tpl->setVariable("PRICENONFORMAT", $r["price"]);
                $tpl->setVariable("CTGR", $r["contr"]);
                $tpl->parse("srow");                
            }
        }  
        $ret["sql"] = $sql;
        $ret["total"] = $total;
        $ret["pages"] = pages_ajax($_REQUEST['filter_start'], 7, $total, "#start-%s");
        $ret["tpl"] = $tpl->get();
        $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;
    }
       
    function isBusy($id) {
        global $DB;
        //$cnt = $DB->getField("SELECT COUNT(id) FROM `list_skp_p_price` WHERE `cat_id`=".$DB->F($id));
        if($cnt!=0) { return true; }
        else return false;
    }
    
    static function getOptList($sel_id = false, $cat =false) {
        global $DB;        
        if ($cat) {
            $sql_add = " WHERE `cat_id`=".$DB->F($cat)." ";
        }
        return array2options($DB->getCell2("SELECT `id`, `title` FROM `list_skp_p_wtypes` $sql_add ORDER BY `title`"), $sel_id);        
        
    }
    
    function getList($tType = false, $cat =false) {
        global $DB;
        if ($cat) {
            $sql_add = " WHERE `cat_id`=".$DB->F($cat)." ";
        }
        return $DB->getCell2("SELECT `id`, `title` FROM `list_skp_p_wtypes` $sql_add ORDER BY `title`");          
    }
    
    static function getByID($id) {
        global $DB;
        if (!$id) return false;  
        return $DB->getField("SELECT `title` FROM `list_skp_p_wtypes` WHERE `id`=".$DB->F($id).";");                
    }
    
    function viewlist() {
        global $DB, $USER, $PLUGINS;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        if ($USER->getTemplate() != "default") 
            $tpl->loadTemplatefile($USER->getTemplate()."/adm_skp_price.tmpl.htm");
        else 
            $tpl->loadTemplatefile("adm_skp_wtypes.tmpl.htm");
        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_name'] = $this->getCookie('filter_name', $_REQUEST['filter_name']);
        $_REQUEST['filter_cat_id'] = $this->getCookie('filter_cat_id', $_REQUEST['filter_cat_id']);
        $_REQUEST['filter_contr_id'] = $this->getCookie('filter_contr_id', $_REQUEST['filter_contr_id']);
        
        $tpl->setVariable('FILTER_ID', $_REQUEST['filter_id']);
        $tpl->setVariable('FILTER_NAME', $_REQUEST['filter_name']);
        $tpl->setVariable('FILTER_CAT_OPTIONS', adm_skp_catnames_plugin::getOptList($_REQUEST['filter_cat_id']));
        $tpl->setVariable('FILTER_CONTR_OPTIONS', kontr_plugin::getOptListSer($_REQUEST['filter_contr_id']));
        $f = $DB->getField("SELECT `id` FROM `list_contr` WHERE `contr_type2`='1' ORDER BY `contr_title`;");
        if (!$f) UIError('Нет контрагентов!');
        if ($_REQUEST['filter_contr_id']) {
            $sql_add[] = " l.contr_id=".$DB->F($_REQUEST['filter_contr_id'])." ";            
        } else
            $sql_add[] = " l.contr_id=".$DB->F($f)." ";
        
        //if ($_REQUEST['filter_name'] != "") {
        //    $sql_add[] = " l.title LIKE ".$DB->F("%".$_REQUEST['filter_name']."%")."";
        //}
        if ($_REQUEST['filter_cat_id']) {
            $sql_add[] = " l.cat_id=".$DB->F($_REQUEST['filter_cat_id'])."";
        }
        if ($_REQUEST['filter_id'] != "") {
            $sql_add[] = " l.id=".$DB->F($_REQUEST['filter_id'])."";
        }
        if (sizeof($sql_add)) {
            $sql_add = " WHERE ".implode(" AND ", $sql_add);
        }
        $sql = "SELECT l.*, (SELECT `fio` FROM `users` WHERE `id`=l.cr_uid), (SELECT `title` FROM `list_skp_p_cattitle` WHERE `id`=l.cat_id), IF( ptype = 1, 'Фикс. стоимость', 'Норм. стоимость'), (SELECT `ei_id` FROM `list_skp_p_wtypes` WHERE `id`=l.wt_id), (SELECT `title` FROM `list_skp_p_wtypes` WHERE `id`=l.wt_id) FROM `list_skp_p_price` as l $sql_add ORDER BY `id`;"; 
        //die($sql);
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            while (list($id, $contr_id, $cat_id, $wt_id, $p_type, $price, $ei_min, $contrcode, $cr_uid, $cr_date, $code, $intcode, $username, $cattitle, $ptype_title,  $eizm_id, $wtype_title) = $DB->fetch()) {
                $tpl->setCurrentBlock("vl_item");
                $tpl->setVariable("VLI_ID", $id);
                $tpl->setVariable("VLI_TITLE", $wtype_title);
                $tpl->setVariable("VLI_EDIZM", adm_skp_edizm_plugin::getByID($eizm_id));
                $tpl->setVariable("VLI_CAT", $cattitle);
                $tpl->setVariable("VLI_TYPE", $ptype_title);
                $tpl->setVariable("VLI_PRICE", number_format($price, 2, ".", " "));
                $tpl->setVariable("VLI_MINED",  $p_type == "1" ? "&mdash;" : $ei_min);
                $tpl->setVariable("VLI_CONTR_NAME", kontr_plugin::getByID($contr_id));
                $tpl->setVariable("VLI_AUTHOR", $username);
                $tpl->setVariable("VLI_CODE", $contr_id."-".$cat_id."-".$code);
                $tpl->setVariable("VLI_DATE", rudate("H:i:s d M Y", $cr_date));
                $tpl->parse("vl_item");
            }
        } else {
            $tpl->touchBlock("novlitems");
        }
        UIHeader($tpl);
        $tpl->show();
    }
    
    function edit() {
        global $DB, $PLUGINS, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        if ($USER->getTemplate() != "default") {
                if($id = $_REQUEST['id']) 
                    $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
                else 
                    $tpl->loadTemplatefile($USER->getTemplate()."/new.tmpl.htm");
            } else {
                if($id = $_REQUEST['id']) 
                    $tpl->loadTemplatefile("edit.tmpl.htm");
                else 
                    $tpl->loadTemplatefile("new.tmpl.htm");
            }
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        
        if($id = $_REQUEST['id']) { 
            $sql = "SELECT * FROM `list_skp_p_price` WHERE `id`=".$DB->F($id).";"; 
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Запись с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);
            $tpl->setVariable("PITEM_ID", $result["id"]);
            $tpl->setVariable("TITLE", adm_skp_wtypes_plugin::getByID($result["wt_id"]));
            $tpl->setVariable("PRICE", number_format($result["price"], 2, ".", " "));
            $tpl->setVariable("MIN_ED", $result["ei_min"]);
            $tpl->setVariable("EDIZM", adm_skp_edizm_plugin::getByID($DB->getField("SELECT ei_id FROM `list_skp_p_wtypes` WHERE id=".$DB->F($result["wt_id"]))));
            $tpl->setVariable("CATTITLE", adm_skp_catnames_plugin::getByID($result['cat_id']));
            $tpl->setVariable("CONTRTITLE", kontr_plugin::getByID($result["contr_id"]));
            $tpl->setVariable("CONTRCODE", $result["contrcode"]);
            $tpl->setVariable("CODE", $result["contr_id"]."-".$result["cat_id"]."-".$result["wt_id"]);
            $tpl->setVariable("INTCONTRCODE", $result["intcode"]);
            if ($result["ptype"]) {
                $tpl->setVariable("PTYPE_FIXED", "selected='selected'");
            } else {
                $tpl->setVariable("PTYPE_NORM", "selected='selected'");                
            }
            $tpl->setVariable('PTYPE', intval($result["ptype"]));
        }
        $sel_ei_ids = adm_skp_edizm_plugin::getOptList($result["ei_id"]);
        if ($sel_ei_ids) $sel_ei_ids = "<select name='ei_id'>".$sel_ei_ids."</select>";
        else UIError("Пустой справочник Единиц Измерения!");
        $tpl->setVariable("EDIZM_LIST", $sel_ei_ids);
        if ($_REQUEST["cat_id"] || $_REQUEST["contr"]) 
            $tpl->setVariable("CODE", ($_REQUEST["contr"] ? $_REQUEST["contr"] : "?")."-".($_REQUEST["cat"] ? $_REQUEST["cat"] : "?"));
        $tpl->setVariable("CATLIST", adm_skp_catnames_plugin::getOptList($result["cat_id"] ? $result["cat_id"] : $_REQUEST["cat"]));
        $tpl->setVariable("CONTRLIST", kontr_plugin::getOptListSer($result["contr_id"] ? $result["contr_id"] : $_REQUEST["contr"]));
        UIHeader($tpl);
        $tpl->show();
        
    }
    
    function getPriceItem_ajax() {
        global $DB, $USER;
        $contr_id = $_REQUEST["contr_id"];
        $cat_id = $_REQUEST["cat_id"];
        $wtype_id = $_REQUEST["wtype_id"];
        if (!$contr_id || !$cat_id || !$wtype_id) {
            echo "error";
            return;
        } else {
            $count = $DB->getField("SELECT count(`id`) FROM `list_skp_p_price` WHERE `contr_id`=".$DB->F($contr_id)." AND `cat_id`=".$DB->F($cat_id)." AND `wt_id`=".$DB->F($wtype_id).";");
            if ($count == 2) {
                echo "error1";
            } else {
                $sql = "SELECT `ptype` FROM `list_skp_p_price` WHERE `contr_id`=".$DB->F($contr_id)." AND `cat_id`=".$DB->F($cat_id)." AND `wt_id`=".$DB->F($wtype_id).";";
                $ptype = $DB->getField($sql);                
                if ($ptype!="") {
                    echo $ptype;   
                } else {
                    echo "ok";
                }
            }
            return;
        }
    }
    
    function save() {
        global $DB, $USER;
        $err = array();
        if(sizeof($err)) UIError($err);
        if($_POST['id']) {
            if ($_POST["ptype"] == 0) {
                $sql_add = ", `ei_min`=".$DB->F($_POST['minval'])."";
            }
            $sql = "UPDATE `list_skp_p_price` SET `intcode`=".$DB->F($_POST["intcontrcode"]).", `contrcode`=".$DB->F($_POST["contrcode"]).", `ptype`=".$DB->F($_POST['ptype']).", `price`=".$DB->F(str_replace(" ", "", $_POST['price']))." $sql_add WHERE `id`=".$DB->F($_POST['id']).";";
        } else {
            $count = $DB->getField("SELECT count(`id`) FROM `list_skp_p_price` WHERE `contr_id`=".$DB->F($_POST['contr_id'])." AND `cat_id`=".$DB->F($_POST['cat_id']).";");
            $count = $count+1;
            $sql = "INSERT INTO `list_skp_p_price` (`contr_id`, `cat_id`, `wt_id`, `ptype`, `price`, `ei_min`, `contrcode`, `cr_uid`, `code`, `intcode`) 
                VALUES(".$DB->F($_POST['contr_id']).", ".$DB->F($_POST['cat_id']).", ".$DB->F($_POST['wtype_id']).", ".$DB->F($_POST['ptype']).", ".$DB->F(str_replace(" ", "", $_POST['price'])).", ".$DB->F($_POST["minval"] ? $_POST["minval"] : 1).", ".$DB->F($_POST["contrcode"]).", ".$DB->F($USER->getId()).", ".$DB->F($count).", ".$DB->F($_POST["intcontrcode"]).")";
            
        }
        //die($sql);
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error(). "<br />".$sql);
        
        if(!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Элемент прайса сохранен. ID: ".$_POST['id']);
    }
    
    function delete() {
        global $DB;
        if(!($id = $_REQUEST['id'])) $err[] = "Не указан элемент прайса!";  
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST["id"])) UIError("Выбранная запись связана с активными Заявками. Удаление невозможно."); 
        $DB->query("DELETE FROM `list_skp_p_price` WHERE `id`='".$_REQUEST['id']."';");
        redirect($this->getLink(),"Элемент прайса успешно удален.");        
    }
    
    function getPriceItemNumber_ajax() {
        global $DB;
        $count = $DB->getField("SELECT count(`id`) FROM `list_skp_p_price` WHERE `contr_id`=".$DB->F($_POST['contr_id'])." AND `cat_id`=".$DB->F($_POST['cat_id']).";");
        $count = $count+1;
        echo $count;
    }
 
}
?>