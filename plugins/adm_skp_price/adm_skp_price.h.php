<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Справочник СКП: &laquo;Прайс&raquo;";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';
$PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник СКП: Прайс";
$PLUGINS[$plugin_uid]['events']['browse_wtypes'] = "Получение прайса (обзор)"; 
$PLUGINS[$plugin_uid]['events']['browse_wtypes_intel'] = "Получение прайса (обзор)"; 


if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник СКП: Прайс";
    $PLUGINS[$plugin_uid]['events']['getPriceItem_ajax'] = "Проверка наличия вида цены"; 
    $PLUGINS[$plugin_uid]['events']['getPriceItemNumber_ajax'] = "Получение порядкового номера позиции внутри раздела прайса"; 
    
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование элемента прайса";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранить вид работ";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление вида работ";
    
}
?>