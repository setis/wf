<?php
use classes\User;

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Статьи расходов";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Статьи расходов";
}

if(wf::$user->checkAccess($plugin_uid, User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактор статьи расходов";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление статьи расходов";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение статьи расходов";
    $PLUGINS[$plugin_uid]['events']['create'] = "Добавление статьи расходов";
}