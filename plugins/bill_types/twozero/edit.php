<?php
/** @var \models\BillType $model */
/** @var \models\BillTypeLimit[] $limits */

echo $this->widget('BreadcrumbsWidget', [
    'items' => [
        ['url' => '/', 'label' => 'Рабочий стол'],
        ['url' => '/bills', 'label' => 'Счета'],
        ['url' => '/bill_types', 'label' => 'Статьи расходов'],
        ['active' => true, 'label' => 'Изменение статьи'],
    ]
]);
?>

<form class="form-horizontal" role="form" method="post" action="/bill_types/save/">
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Общая информация </strong></div>
        <div class="panel-body">
            <input type="hidden" name="id" value="<?php echo $model->getId(); ?>"/>
            <div class="form-group">
                <label class="control-label col-sm-3">Тэг</label>
                <div class="col-sm-6"><input class="form-control" placeholder="tag" type="text" name="tag" value="<?php echo $model->getTag(); ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Название</label>
                <div class="col-sm-6">
                    <input class="form-control" placeholder="название *" required="required" type="text" name="title" value="<?php echo $model->getTitle(); ?>"/>
                </div>
            </div>

        </div>

    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Лимит бюджета на текущий, <?php echo date('Y'); ?> год </strong></div>
        <div class="panel-body">
            <?php foreach ($limits as $i => $limit): $i; ?>
                <div class="row form-group">
                    <label class="col-sm-3 text-right control-label"><?php echo sprintf("%d-%'.02d", date('Y'), $i + 1); ?></label>
                    <div class="col-sm-9">
                        <input type="hidden" value="<?php echo $limit->getId(); ?>" name="limits[<?php echo $i; ?>][id]">
                        <input type="hidden" value="<?php echo strtotime(sprintf("%d-%'.02d", date('Y'), $i + 1)); ?>" name="limits[<?php echo $i; ?>][limitAt]">
                        <input type="text" class="form-control" value="<?php echo $limit->getMoneyLimit(); ?>" name="limits[<?php echo $i; ?>][moneyLimit]"
                            <?php echo strtotime(sprintf("%d-%'.02d-01", date('Y'), $i) . '+ 2 month') < strtotime('now') ? /*"readonly='readonly'"*/'' : ''; ?> />

                    </div>
                </div>
            <?php endforeach; ?>


        </div>

    </div>

    <div class="text-right">
            <a class="btn btn-success" href="/bill_types">Назад</a>
            <button type="submit" name="go" value="Сохранить" class="btn btn-primary">Сохранить</button>
    </div>
</form>