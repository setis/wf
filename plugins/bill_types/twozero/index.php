<?php
/** @var \classes\View $this */
/** @var models\BillType[] $bill_types */
/** @var array $data */

echo $this->widget('BreadcrumbsWidget', [
    'items' => [
        ['url' => '/', 'label' => 'Рабочий стол'],
        ['url' => '/bills', 'label' => 'Счета'],
        ['active' => true, 'label' => 'Статьи расходов'],
    ]
]);

$service = new \WF\services\UrlService();
?>

<div class="text-right">
    <a href="/bill_types/create" class="btn btn-success">Добавить статью расходов</a>
</div>
<hr />

<table class="table">
    <thead>
    <tr>
        <th>ID</th>
        <th>Тэг</th>
        <th>Название</th>
        <th>Остаток на текущий месяц, <i class="fa-rub fa"></i></th>
        <th>Годовой лимит, <i class="fa-rub fa"></i></th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $item): ?>
        <tr>
            <td><a href="<?php echo $service->makeUrl($item['billType'], 'bill_types', 'edit'); ?>">
                    <?php echo $item['billType']->getId(); ?>
                </a></td>
            <td><?php echo $item['billType']->getTag(); ?></td>
            <td><?php echo $item['billType']->getTitle(); ?></td>
            <td><?php echo rub($item['monthRest']); ?></td>
            <th><?php echo rub($item['yearLimit']); ?></th>
            <td>
                <a href="<?php echo $service->makeUrl($item['billType'], 'bill_types', 'edit'); ?>" class="text-success">
                    <i class="glyphicon glyphicon-edit"></i>
                </a>

                <a href="<?php echo $service->makeUrl($item['billType'], 'bill_types', 'delete'); ?>" class="text-danger" onclick="if(!confirm('Вы уверены?')) return false;">
                    <i class="glyphicon glyphicon-remove"></i>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>