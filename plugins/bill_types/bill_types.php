<?php
use classes\Plugin;
use \Symfony\Component\HttpFoundation\Request;
use classes\exceptions\NotFoundModelException;
use models\BillTypeLimit;

class bill_types_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function viewlist(Request $request)
    {
        $billTypes = wf::$em->getRepository('models\BillType')->findBy([], ['id' => 'asc']);
        /** @var \repository\BillTypeLimitRepository $repo */
        $repo = wf::$em->getRepository('\\models\\BillTypeLimit');

        $data = [];
        foreach ($billTypes as $billType) {
            $data[] = [
                'billType' => $billType,
                'monthRest' => $repo->getMonthLimitRest($billType),
                'yearLimit' => $repo->getYearLimit($billType, DateTime::createFromFormat('m-d', '12-31'))
            ];
        }

        $this->layout = 'common';
        $this->setPageTitle('Лимиты по статьям расходов');
        echo $this->render('twozero/index', [
            'data' => $data
        ]);
    }

    function create(Request $request)
    {
        /** @var \models\BillType $model */
        $model = new models\BillType();

        /** @var \repository\BillTypeLimitRepository $repo */
        $repo = wf::$em->getRepository('models\BillTypeLimit');
        $limits = $repo->getYearLimits($model,
            new \DateTime(date('Y') . '-12-01 23:59:59'));

        $this->setPageTitle('Лимиты по статьям расходов');
        echo $this->render('twozero/edit', [
            'model' => $model,
            'limits' => $limits
        ]);
    }

    function edit(Request $request)
    {

        /** @var \models\BillType $model */
        $model = wf::$em->find('models\BillType', $request->get('id'));
        if (empty($model))
            throw new NotFoundModelException('Not found model!');

        /** @var \repository\BillTypeLimitRepository $repo */
        $repo = wf::$em->getRepository('models\BillTypeLimit');
        $limits = $repo->getYearLimits($model,
            new \DateTime(date('Y') . '-12-01 23:59:59'));

        $this->setPageTitle('Лимиты по статьям расходов');
        echo $this->render('twozero/edit', [
            'model' => $model,
            'limits' => $limits
        ]);
    }

    function save(Request $request)
    {
        if ($request->request->get('id')) {
            /** @var \models\BillType $billType */
            $billType = wf::$em->find('models\BillType', $request->get('id'));
            if (empty($billType))
                throw new NotFoundModelException('Not found model!');
        } else {
            $billType = new \models\BillType();
        }

        $billType->setTitle($request->get('title'));
        $billType->setTag($request->get('tag'));

        foreach ($request->get('limits', array()) as $limit) {
            $billTypeLimit = wf::$em->find('models\BillTypeLimit', $limit['id']);
            if (empty($billTypeLimit))
                $billTypeLimit = new BillTypeLimit();

            $dt = new \DateTime();
            $dt->setTimestamp((integer)$limit['limitAt']);
            $billTypeLimit->setLimitAt($dt);
            $billTypeLimit->setMoneyLimit($limit['moneyLimit']);

            $billType->addLimit($billTypeLimit);

        }
        wf::$em->persist($billType);
        wf::$em->flush();

        redirect($this->getLink('viewlist'), "Статья расходов сохранена. ID: " . $billType->getId());
    }

    function delete(Request $request)
    {
        $id = $request->get('id');

        if (empty($id))
            UIError("Не указан идентификатор записи!");

        $billType = wf::$em->find('models\BillType', $request->get('id'));

        if(empty($billType))
            UIError("Объект не найден id:{$id}!");

        wf::$em->remove($billType);
        wf::$em->flush();

        redirect($this->getLink(), "Запись успешно удалена.");

    }

    static function getOptions($id = 0)
    {
        global $DB;
        $sql = "SELECT id, title FROM `bill_types` WHERE 1 ORDER BY `title`;";
        $res = $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());
        return array2options($res, $id);
    }

    static function getById($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `bill_types` WHERE `id`=" . $DB->F($id) . ";";
        $res = $DB->getField($sql);
        if ($DB->errno()) UIError($DB->error());
        return $res ? $res : false;
    }


}
