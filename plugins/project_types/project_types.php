<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class project_types_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getOptions($id = 0)
    {
        global $DB;
        $sql = "SELECT id, title FROM `project_types` WHERE 1 ORDER BY `title`;";
        $res = $DB->getCell2($sql);
        if ($DB->errno())
            UIError($DB->error());
        return array2options($res, $id);
    }

    static function getById($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `project_types` WHERE `id`=" . $DB->F($id) . ";";
        $res = $DB->getField($sql);
        if ($DB->errno()) UIError($DB->error());
        return $res ? $res : false;
    }

    function viewlist()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID().".tmpl.htm");

        $sql = "SELECT * FROM `project_types` WHERE 1;";
        $DB->query($sql);
        if ($DB->errno()) {
            UIError($DB->error());
        }
        while (list($id, $tag, $title) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->setVariable("VLI_TAG", $tag);
            $tpl->parse("vl_item");
        }
        $DB->free();
        UIHeader($tpl);
        $tpl->show();
    }

    function edit() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");

        if ($id = $_REQUEST['id']) {
            $sql = "SELECT * FROM `project_types` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Тип проекта с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);

            $tpl->setVariable("ID", $result["id"]);
            $tpl->setVariable("TITLE", $result["title"]);
            $tpl->setVariable("TAG", $result["tag"]);

        }
        UIHeader($tpl);
        $tpl->show();

    }

    function save() {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = array();

        if(!$_POST['title']) $err[] = "Не заполнено название типа проекта";

        if(sizeof($err)) UIError($err);
        if ($_POST["id"]) {
            $sql = "UPDATE `project_types` SET `tag`=".$DB->F($_POST["tag"]).", `title`=".$DB->F($_POST['title'])." WHERE `id`=".$DB->F($_POST["id"]).";";
        } else {
            $sql = "INSERT INTO `project_types` (`tag`, `title`) VALUES(".$DB->F($_POST['tag']).",".$DB->F($_POST['title']).")";
        }
        //$sql = "INSERT INTO `project_types` (`tag`, `title`) VALUES(".$DB->F($_POST['tag']).",".$DB->F($_POST['title']).")";

        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());

        if(!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Тип проекта сохранен. ID: ".$_POST['id'] );
    }

    function delete() {
        global $DB;
        if (!$_REQUEST['id']) $err[] = "Не указан идентификатор записи!";
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST['id'])) UIError("Выбранная запись связана с проектами. Удаление невозможно.");
        $DB->query("DELETE FROM `project_types` WHERE `id`='".$_REQUEST['id']."';");
        redirect($this->getLink(),"Запись успешно удалена.");

    }

    function isBusy($id)
    {
        global $DB;
        $DB->query("SELECT `task_id` FROM `projects` WHERE `type`=" . $DB->F($id) . ";");
        if ($DB->num_rows()) $err = true; else $err = false;
        $DB->free();
        return $err;
    }


}
?>
