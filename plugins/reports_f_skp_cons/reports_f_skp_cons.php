<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;


require_once(dirname(__FILE__)."/../services/services.php");

class reports_f_skp_cons_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $_REQUEST["sc_id"] = $this->getCookie("sc_id", (isset($_REQUEST["sc_id"]) ? $_REQUEST["sc_id"] : 8));
        $_REQUEST["createreport"] = $this->getCookie("createreport", $_REQUEST["createreport"]);
        $_REQUEST["datefrom"] = $this->getCookie("datefrom", $_REQUEST["datefrom"]);
        $_REQUEST["dateto"] = $this->getCookie("dateto", $_REQUEST["dateto"]);
        $_REQUEST["month"] = $this->getCookie('month', $_REQUEST["month"] ? $_REQUEST["month"] : date("m"));
        $_REQUEST["year"] = $this->getCookie('year', $_REQUEST["year"] ? $_REQUEST["year"] : date("Y"));
        //$_REQUEST["month"] = $this->getCookie('month', $_REQUEST["month"] ? $_REQUEST["month"] : date("m"));
        //$_REQUEST["year"] = $this->getCookie('year', $_REQUEST["year"] ? $_REQUEST["year"] : date("Y"));
        $dateParams = array(0 => "Дата создания", 1 => "Дата выполнения", 2=>"Дата изменения", 3=>"Дата принятия ДС");
        if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
            $cntr = new kontr_plugin();
        }   
        $contrCount = $cntr->getListSer();                    
        if (!isset($_REQUEST['createprintversion']) && !isset($_REQUEST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_SKP_TOTAL", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            $tpl->setVariable("MONTH", array2options($month, $_REQUEST["month"]));
            $tpl->setVariable("YEAR", array2options($year, $_REQUEST["year"]));
            $tpl->setVariable("DATE_FROM", $_REQUEST["datefrom"]);
            $tpl->setVariable("DATE_TO", $_REQUEST["dateto"]);
            
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id"]));
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListSer(@$_POST["cnt_id"]));
            }
        }
        if (@$_REQUEST["createreport"] || @$_REQUEST["createprintversion"] || @$_REQUEST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_REQUEST['createprintversion']) || @$_REQUEST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $datefrom = $_REQUEST["datefrom"];
            $dateto = $_REQUEST["dateto"];
            $monthf = $_REQUEST["month"];
            $yearf = $_REQUEST["year"];
            //$period = $_REQUEST["year"]."-".$_REQUEST["month"];
            $rtpl->setVariable('CURRENT_PERIOD', $_REQUEST["datefrom"]." - ".$_REQUEST["dateto"]);
            $sc = adm_sc_plugin::getSC($_REQUEST["sc_id"]);
            $rtpl->setVariable("SC_NAME", $_REQUEST["sc_id"] ? $sc['title'] : "Все");
            $contrList = $cntr->getListSer();
            foreach($contrList as $key=>$value) {
                if (in_array($key, $_POST["cnt_id"])) {
                    $contrTitlesinReport[] = $value;
                }
            }
            $rtpl->setVariable("CONTR_NAMES", implode(", ", $contrTitlesinReport));
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "services");
            $countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "services");
            $acceptedStatus = adm_statuses_plugin::getStatusByTag("acc_accepted", "services");
            $otkaz = adm_statuses_plugin::getStatusByTag("otkaz", "services");
            $opotkaz = adm_statuses_plugin::getStatusByTag("opotkaz", "services");
            $complStatusList[] = $doneStatus["id"];
            $complStatusList[] = $countStatus["id"];
            $complStatusList[] = $completeStatus["id"];
            $complStatusList[] = $acceptedStatus["id"];
            $complStatusList = implode(",", $complStatusList);
            $add_filter = "AND DATE_FORMAT(t.date_reg, '%Y-%m')=".$DB->F($yearf."-".$monthf);
            $add_filter .= $_REQUEST["cnt_id"]!=0 ? " AND tick.cnt_id IN (".implode(",", $_REQUEST["cnt_id"]).")" : "";
            $sc_id = $_REQUEST["sc_id"];
            if ($sc_id) {
                $add_filter .= " AND tick.sc_id=".$DB->F($sc_id)." ";
            }
            $sql = "SELECT 
                    tick.cnt_id, 
                    (SELECT `contr_title` FROM `list_contr` WHERE `id`=tick.cnt_id) as contr_name,
                    COUNT(tick.task_id) as ticktotal, tick.task_id FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id WHERE t.plugin_uid='services' ".$add_filter." GROUP BY tick.cnt_id";
            ///die($sql);
            $DB->query($sql);
            if ($add_filter=="") {
                $rtpl->touchBlock("no-rows");
            } else {
                if ($DB->num_rows()) {
                    $total = 0;
                    $completed_total = 0;
                    $clntreject_total = 0;
                    $opreject_total = 0;
                    $convert_total = 0;
                    $total_ammount = 0;
                    $total_avgcheck = 0;
                    $byReportCount = $DB->num_rows();
                    $pricedivider = 0;
                    while($r = $DB->fetch(true)) {
                        if ($sc_id) {
                            $sc_filter = " AND tick.sc_id=".$DB->F($sc_id)." ";
                        }
            
                        $rtpl->setCurrentBlock("rep_row");
                        $sql = "SELECT COUNT(t.id) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON t.id=tick.task_id WHERE t.status_id IN (".$complStatusList.") AND tick.cnt_id=".$DB->F($r["cnt_id"])." AND t.plugin_uid='services' AND DATE_FORMAT(t.date_reg, '%Y-%m')=".$DB->F($yearf."-".$monthf).$sc_filter.";";
                        $complCount = $DB->getField($sql);
                        $sql = "SELECT COUNT(t.id) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON t.id=tick.task_id WHERE t.status_id=".$DB->F($otkaz["id"])." AND tick.cnt_id=".$DB->F($r["cnt_id"])." AND t.plugin_uid='services' AND DATE_FORMAT(t.date_reg, '%Y-%m')=".$DB->F($yearf."-".$monthf).$sc_filter.";";
                        $crejCount = $DB->getField($sql);
                        $sql = "SELECT COUNT(t.id) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON t.id=tick.task_id WHERE t.status_id=".$DB->F($opotkaz["id"])." AND tick.cnt_id=".$DB->F($r["cnt_id"])." AND t.plugin_uid='services' AND DATE_FORMAT(t.date_reg, '%Y-%m')=".$DB->F($yearf."-".$monthf).$sc_filter.";";
                        $oprejCount = $DB->getField($sql);
                        $sql = "SELECT SUM(tick.orient_price) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON t.id=tick.task_id WHERE t.status_id IN (".$complStatusList.") AND tick.cnt_id=".$DB->F($r["cnt_id"])." AND t.plugin_uid='services' AND DATE_FORMAT(t.date_reg, '%Y-%m')=".$DB->F($yearf."-".$monthf).$sc_filter.";";
                        $priceTotal = $DB->getField($sql);
                        $sql = "SELECT AVG(tick.orient_price) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON t.id=tick.task_id WHERE t.status_id IN (".$complStatusList.") AND tick.cnt_id=".$DB->F($r["cnt_id"])." AND t.plugin_uid='services' AND DATE_FORMAT(t.date_reg, '%Y-%m')=".$DB->F($yearf."-".$monthf).$sc_filter.";";
                        $avgCheck = $DB->getField($sql);
                        if ($priceTotal>0) {
                            $pricedivider += 1;
                        }
                        $rtpl->setVariable("CONTRAGENT", $r["contr_name"]);
                        $rtpl->setVariable("INC_COUNT", $r["ticktotal"]);
                        $rtpl->setVariable("COMPLETED_COUNT", $complCount);
                        $rtpl->setVariable("CLNTREJECT_COUNT", $crejCount);
                        $rtpl->setVariable("OPREJECT_COUNT", $oprejCount);
                        if ($r["ticktotal"])
                            $rtpl->setVariable("CONVERT_TOTAL", (round($complCount/$r["ticktotal"],2)*100)."%");
                        else 
                            $rtpl->setVariable("CONVERT_TOTAL", "0%");
                        $rtpl->setVariable("AMMOUNT_TOTAL", $priceTotal ? number_format($priceTotal, 2, ",", " ") : "0,00");
                        $rtpl->setVariable("AVGCHECK", $avgCheck ? number_format($avgCheck, 2, ",", " ") : "0,00");
                        $rtpl->parse("rep_row");
                        $total += $r["ticktotal"];
                        $completed_total += $complCount;
                        $clntreject_total += $crejCount;
                        $opreject_total += $oprejCount;
                        if ($r["ticktotal"])
                            $convert_total += round($complCount/$r["ticktotal"],2)*100;
                        $total_ammount += $priceTotal;
                        $total_avgcheck += $avgCheck;
                    }
                    $rtpl->setCurrentBlock("report_total");
                    $rtpl->setVariable("TOTAL", $total);
                    $rtpl->setVariable("TOTAL_COMPLETED", $completed_total);
                    $rtpl->setVariable("TOTAL_CREJECT", $clntreject_total);
                    $rtpl->setVariable("TOTAL_OPREJECT", $opreject_total);
                    $rtpl->setVariable("TOTAL_CONVERTVALUE", round(($completed_total/$total)*100, 1)."%");
                    $rtpl->setVariable("TOTAL_AMMOUNT", number_format($total_ammount, 2, ".", " "));
                    if ($pricedivider>0)
                        $rtpl->setVariable("TOTAL_AVGCHECK", number_format(round($total_avgcheck/$pricedivider), 2, ",", " "));
                    else {
                        $rtpl->setVariable("TOTAL_AVGCHECK", number_format(0, 2, ",", " "));                        
                    }
                    $rtpl->parse("report_total");
                } else {
                    $rtpl->touchBlock("no-rows");
                }
            }       
            $DB->free();    
            $rtpl->setVariable("REP_CDATE", date("d.m.Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            $rtpl->setVariable('CURRENT_PERIOD', $month[$_REQUEST["month"]]." ".$year[$_REQUEST["year"]]);
            
             
            if (isset($_REQUEST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_skp_cons__".$_REQUEST["month"]."-".$_REQUEST["year"].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_REQUEST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 
   
}
?>