<?php 
/**
 * Plugin Header
 * @author kblp
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "ТТ";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "ТТ (Абонентские Аварии)";
    $PLUGINS[$plugin_uid]['events']['viewticket'] = "Просмотр заявки";
    $PLUGINS[$plugin_uid]['events']['viewticket1'] = "Просмотр заявки";
    $PLUGINS[$plugin_uid]['events']['vieworderdoc'] = "Печать заказ-наряда";
    $PLUGINS[$plugin_uid]['events']['viewlist_export'] = "Аварии. Выгрузка.";
    $PLUGINS[$plugin_uid]['events']['queryStatus'] = "Запрос текущего статуса Заявки";
    $PLUGINS[$plugin_uid]['events']['getwtypelist'] = "Список видов работ";
    $PLUGINS[$plugin_uid]['events']['getagrlist'] = "Список договоров";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['newticket'] = "Новая заявка на аварию";
    $PLUGINS[$plugin_uid]['events']['saveticket'] = "Сохранение зявки";
    $PLUGINS[$plugin_uid]['events']['updateticket'] = "Обновление зявки";
    $PLUGINS[$plugin_uid]['events']['removefromgfx'] = "Удаление из графика";
    $PLUGINS[$plugin_uid]['events']['deleteticket'] = "Удаление заявки";
    $PLUGINS[$plugin_uid]['events']['calcticket'] = "Расчет заявки";
    $PLUGINS[$plugin_uid]['events']['calc_tmc_ticket'] = "Расчет заявки";
    $PLUGINS[$plugin_uid]['events']['getCurrentTMCCalcA'] = "Загрузка расчета заявки по ТМЦ";
    $PLUGINS[$plugin_uid]['events']['getCurrentCalcA'] = "Загрузка расчета заявки";
    $PLUGINS[$plugin_uid]['events']['checkTaskStatusA'] = "Проверка статуса заявки для перевода в статус Сдача отчета";
    $PLUGINS[$plugin_uid]['events']['updateContrDataA'] = "Изменение Контрагента, договора и вида работ";
    $PLUGINS[$plugin_uid]['events']['clearreport'] = "Удаление (обнуление) расчета по заявке";
    $PLUGINS[$plugin_uid]['events']['movetosmr'] = "Передача заявки в РМС";
    $PLUGINS[$plugin_uid]['events']['return_tmc_from_ticktet'] = "Возврат ТМЦ по ТТ";
    $PLUGINS[$plugin_uid]['events']['delete_returned_tmc_from_ticket'] = "Откат (отмена списания) ТМЦ по ТТ";
}

define('ACCIDENTS_ALLOW_CANCELING', 'accidents.allow_canceling');
$plugin_uid = ACCIDENTS_ALLOW_CANCELING;
$PLUGINS[$plugin_uid]['name'] = "ТТ: Отказные статусы";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "accidents_questionnarie_access";
$PLUGINS[$plugin_uid]['name'] = "ТТ: Доступ к анкете ";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "calc_tt_tmc_ticket";

$PLUGINS[$plugin_uid]['name'] = "ТТ: Расчет ТМЦ по заявкам";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['delete_tt_tmc_ticket'] = "Расчет ТМЦ по заявкам";
}

$plugin_uid = "calc_tt_ticket";

$PLUGINS[$plugin_uid]['name'] = "ТТ. Расчет Заявок";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['tt_deleteticket'] = "Расчет";
}

$plugin_uid = "edit_contr_acc_ticket";

$PLUGINS[$plugin_uid]['name'] = "ТТ. Изменение контрагента в Авариях";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['editcontr'] = "Редактор контрагента";
}

$plugin_uid = 'accidents2contr';

$PLUGINS[$plugin_uid]['name'] = "ТТ. Комментарии контрагенту";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'changeaccctype';

$PLUGINS[$plugin_uid]['name'] = "ТТ. Изменение типа клиента по заявке";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'rooteditaccticket';

$PLUGINS[$plugin_uid]['name'] = "ТТ. Редактирование данных в заявках в любых статусах";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'changeaccdonedate';

$PLUGINS[$plugin_uid]['name'] = "ТТ. Изменение даты выполнения";
$PLUGINS[$plugin_uid]['hidden'] = true;
$plugin_uid = 'scaccfullaccess';
$PLUGINS[$plugin_uid]['name'] = "ТТ. Полный доступ ко всем СЦ";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'deleteacc';
$PLUGINS[$plugin_uid]['name'] = "ТТ. Удаление заявки";
$PLUGINS[$plugin_uid]['hidden'] = true;


$plugin_uid = "ttallowodsandaddrparams";
$PLUGINS[$plugin_uid]['name'] = "ТТ: Редактор параметров адреса и выбор ОДС";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'ttcalcticketafterdeadline';
$PLUGINS[$plugin_uid]['name'] = "ТТ: Редактирование заявки после отчетного периода";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "plk.accident.view.calls";
$PLUGINS[$plugin_uid]['name'] = "ТТ: Просмотр звонков по заявке (ЛК Партнёра)";
$PLUGINS[$plugin_uid]['hidden'] = true;
