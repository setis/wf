<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2011
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
class adm_statuses_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getSmsOptions($sel_id = 0)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`, `name` FROM `sms_templates` ORDER BY `name`"), $sel_id);
    }

    static function getOptList($sel_id = false, $tType = false)
    {
        global $DB;
        if ($tType)
            return array2options($DB->getCell2("SELECT `id`, `name` FROM `task_status` WHERE `plugin_uid`=" . $DB->F($tType) . " ORDER BY `name`"), $sel_id);
        else
            return array2options($DB->getCell2("SELECT `id`, `name` FROM `task_status` ORDER BY `name`"), $sel_id);

    }

    static function getList($tType = false)
    {
        global $DB;
        if ($tType)
            return $DB->getCell2("SELECT `id`, `name` FROM `task_status` WHERE `plugin_uid`=" . $DB->F($tType) . " ORDER BY `name`");
        else
            return $DB->getCell2("SELECT `id`, `name` FROM `task_status` ORDER BY `name`");

    }

    static public function getAgentAllowedStatuses() {
        return array(60, 13, 17, );
    }

    static function getFullList($tType = false)
    {
        global $DB;

        if ($tType) {
            $DB->query("SELECT * FROM `task_status` WHERE `plugin_uid`=" . $DB->F($tType) . " ORDER BY `name`");
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $ret[] = $r;
                }
                $DB->free();
                return $ret;
            } else {
                $DB->free();
                return false;
            }
        } else {
            return false;
        }
    }

    function getByID($id)
    {
        global $DB;
        if (!$id) return false;
        return $DB->getField("SELECT `name` FROM `task_status` WHERE `id`=" . $DB->F($id));
    }

    function sprav()
    {
        global $DB, $USER, $PLUGINS;

        //if(!($plugin = $_GET['plugin'])) UIError("Не указан модуль!");

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/list.tmpl.htm");
        else
            $tpl->loadTemplatefile("list.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $tpl->setVariable('MOD_NAME', htmlspecialchars($PLUGINS[$plugin]['name']));
        $tpl->setVariable('PLUIGIN', htmlspecialchars($plugin));

        $sql = "SELECT st.* FROM `task_status` AS st ORDER BY st.plugin_uid, st.name";
        $DB->query($sql);
        while ($a = $DB->fetch(true)) {
            $tpl->setCurrentBlock('row');
            $tpl->setVariable('STATUS_ID', $a['id']);
            $tpl->setVariable('PLUGIN', Plugin::getNameStatic($a['plugin_uid']));
            $tpl->setVariable('NAME', $a['name']);
            $tpl->setVariable('COLOR', $a['color']);
            $tpl->setVariable('TAG', $a['tag'] ? $a['tag'] : '&mdash;');
            $tpl->setVariable('IS_ACTIVE', $a['is_active'] ? 'Да' : '&mdash;');
            $tpl->setVariable('IS_DEFAULT', $a['is_default'] ? 'Да' : '&mdash;');
            $tpl->setVariable('IS_HIDDEN', $a['is_hidden'] ? 'Да' : '&mdash;');
            $tpl->setVariable('REQUIRE_COMMENT', $a['require_comment'] ? 'Да' : '&mdash;');

            //$tpl->setVariable('SMS_ISPOLN', $a['sms_ispoln'] ? $a['sms_ispoln'] : '&mdash;');
            //$tpl->setVariable('SMS_ZAYAV', $a['sms_zayav'] ? $a['sms_zayav'] : '&mdash;');
            $tpl->parse('row');
        }
        $DB->free();

        UIHeader($tpl);
        $tpl->show();
    }

    function status_edit()
    {
        global $DB, $USER, $PLUGINS;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("editor.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/editor.tmpl.htm");
        else
            $tpl->loadTemplatefile("editor.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());

        if ($status_id = $_REQUEST['status_id']) {
            $a = $DB->getRow("SELECT * FROM `task_status` WHERE id=" . $DB->F($status_id), true);
            $tpl->setVariable('ID', $a['id']);
            $tpl->setVariable('ID_', $a['id']);
            $tpl->setVariable('ID_TEXT', $a['id']);
            $tpl->setVariable('NAME', $a['name']);
            $tpl->setVariable('COLOR', $a['color']);
            $tpl->setVariable('TAG', $a['tag']);
            $tpl->setVariable('ACTIVE_CHK', $a['is_active'] ? 'checked' : '');
            $tpl->setVariable('DEFAULT_CHK', $a['is_default'] ? 'checked' : '');
            $tpl->setVariable('HIDDEN_CHK', $a['is_hidden'] ? 'checked' : '');
            $tpl->setVariable('RC_CHK', $a['require_comment'] ? 'checked' : '');


            $_GET['plugin'] = $a['plugin_uid'];
        } else {
            $tpl->setVariable('NO_ID_DIS', 'disabled');
            $tpl->setVariable('ID_TEXT', "Новый");
        }

        //if(!($plugin = $_GET['plugin'])) UIError("Не указан модуль!");
        $tpl->setVariable('PLUGIN', $plugin);
        $ret = "<select name='plugin'>";
        foreach ($PLUGINS as $uid => $params) {
            $ret .= "<option value='$uid' " . (@$a["plugin_uid"] == $uid ? "selected" : "") . ">" . $params["name"] . "</option>";
        }
        $ret .= "</select>";
        $tpl->setVariable("PLUGIN_LIST", $ret);

        //$tpl->setVariable('MOD_NAME', htmlspecialchars($PLUGINS[$plugin]['name']));

        /*$tpl->setVariable('PLUGIN_OPTIONS', $this->getPluginOptions($a['plugin_uid']));
        $tpl->setVariable('SMS_ISPOLN_OPTIONS', $this->getSmsOptions($a['sms_ispoln']));
        $tpl->setVariable('SMS_ZAYAV_OPTIONS', $this->getSmsOptions($a['sms_zayav']));*/

        //$tpl->setVariable('SMS_ISPOLN', $a['sms_ispoln']);
        //$tpl->setVariable('SMS_ZAYAV', $a['sms_zayav']);

        UIHeader($tpl);
        $tpl->show();
    }

    function status_save()
    {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = array();

        if (!$_POST['plugin']) $err[] = "Не указан модуль";
        if (!$_POST['name']) $err[] = "Не заполнено название";
        if (sizeof($err)) UIError($err);

        if ($status_id = $_POST['status_id']) {
            //$sql = "UPDATE `task_status` SET `name`=".$DB->F($_POST['name']).", `plugin_uid`=".$DB->F($_POST['plugin']).", `color`=".$DB->F($_POST['color']).", `tag`=".$DB->F($_POST['tag'], true).", `is_active`=".($_POST['is_active'] ? 1 : 0).", `is_default`=".($_POST['is_default'] ? 1 : 0).", `is_hidden`=".($_POST['is_hidden'] ? 1 : 0).", `sms_ispoln`=".$DB->F($_POST['sms_ispoln'], true).", `sms_zayav`=".$DB->F($_POST['sms_zayav'], true)." WHERE `id`=".$DB->F($status_id);
            $sql = "UPDATE `task_status` SET `require_comment`=" . $DB->F($_POST['require_comment'] ? $_POST['require_comment'] : 0) . ", `name`=" . $DB->F($_POST['name']) . ", `tag`=" . $DB->F($_POST['tag']) . ", `plugin_uid`=" . $DB->F($_POST['plugin']) . ", `color`=" . $DB->F($_POST['color']) . ", `is_active`=" . $DB->F($_POST['is_active'] ? 1 : 0) . ", `is_default`=" . $DB->F($_POST['is_default'] ? 1 : null) . ", `is_hidden`=" . $DB->F($_POST['is_hidden'] ? 1 : 0) . " WHERE `id`=" . $DB->F($status_id);
        } else {
            $sql = "INSERT INTO `task_status` (`id`,`name`, `plugin_uid`, `color`, `tag`, `is_active`, `is_default`, `is_hidden`, `require_comment`) VALUES(" . $DB->F($_POST['id']) . "," . $DB->F($_POST['name']) . ", " . $DB->F($_POST['plugin']) . ", " . $DB->F($_POST['color']) . ", " . $DB->F($_POST['tag'], true) . ", " . $DB->F($_POST['is_active'] ? 1 : 0) . ", " . $DB->F($_POST['is_default'] ? 1 : null) . ", " . $DB->F($_POST['is_hidden'] ? 1 : 0) . ", " . $DB->F($_POST['require_comment'] ? 1 : 0) . ")";
        }
        $DB->query($sql);
        if (!$status_id) $status_id = $DB->insert_id();
        if ($DB->errno()) {
            UIError($DB->error() . "<br />" . $sql);
        }
        //redirect($this->getLink(), "Изменения сохранены");
        redirect($this->getLink('', "plugin=" . $_POST['plugin']));
    }

    function status_del()
    {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = array();

        if (!($status_id = $_REQUEST['status_id'])) $err[] = "Не указан статус";

        $sql = "SELECT COUNT(*) FROM `tasks` WHERE `status_id`=" . $DB->F($status_id);
        if ($c = $DB->getField($sql)) $err[] = "Нельзя удалить статус, есть $c задач!";

        if (sizeof($err)) UIError($err);

        $plugin = $DB->getField("SELECT `plugin_uid` FROM `task_status` WHERE `id`=" . $DB->F($status_id));

        $sql = "DELETE FROM `task_status` WHERE `id`=" . $DB->F($status_id);
        $DB->query($sql);
        redirect($this->getLink('', "plugin=" . $plugin));
    }

    static function getStatusByTag($tag, $type)
    {
        global $DB;
        if (!$tag || !$type) return false;
        $ret = $DB->getRow("SELECT * FROM `task_status` WHERE `plugin_uid`=" . $DB->F($type) . " AND `tag` LIKE " . $DB->F("%$tag%"), true);
        return $ret ? $ret : false;
    }

    static function getStatusColorByID($id)
    {
        global $DB;
        if (!$id) return false;
        $ret = $DB->getField("SELECT `color` FROM `task_status` WHERE `id`=" . $DB->F($id), true);
        return $ret ? $ret : false;
    }

}
