<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2011
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Статусы заявок";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['sprav'] = "Справочник статусов";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['status_edit'] = "Редактирование статуса";
    $PLUGINS[$plugin_uid]['events']['status_save'] = "Сохранение статуса";
    $PLUGINS[$plugin_uid]['events']['status_del'] = "Удаление статуса";
}
?>