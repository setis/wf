<?php

/**
 * Plugin Implementation
 *
 */
use classes\Bill;
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use classes\User;
use Symfony\Component\HttpFoundation\Request;

class bills_plugin extends Plugin
{

    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function statusCallbackGet($task_id, $status)
    {
        global $DB, $USER;

        $err = array();
        $plugin_uid = basename(__FILE__, '.php');

        $sql = "SELECT `author_id` FROM `tasks` WHERE `id`=" . $DB->F($task_id);
        if ((false !== strpos($status['tag'], 'author')) && ($USER->getId() != $DB->getField($sql))) {
            return false;
        }

        $bill = new Bill($task_id);
        if ((false !== strpos($status['tag'], 'otlozh'))) {
            $r = $DB->getField("SELECT DATE_FORMAT(`delayed`, '%d.%m.%Y') AS dd FROM `bills` WHERE `task_id`=" . $DB->F($task_id) . ";");
            return $status['name'] . "</label> до <input type='text' name='otlozh_date' class='date_input' values='" . $r . "'><label>";
        }

        if ((false !== strpos($status['tag'], 'togo'))) {
            if ($USER->checkAccess("settogostatus", User::ACCESS_WRITE)) {

                return $status['name'];
            } else
                return false;
        }

        if ((false !== strpos($status['tag'], 'odobreno'))) {
            if ($USER->checkAccess("setodobrstatus", User::ACCESS_WRITE)) {
                $etName = $bill->getDirection();
                $selId = 0;
                if ($etName) {
                    $selId = $DB->getField("SELECT `id` FROM `bills_odobr` WHERE `id`=" . $DB->F($etName) . ";");
                }
                return $status['name'] . "</label> <select name='odobr_id'>" . self::getOdobrOptions($selId) . "</select><label>";
            } else
                return false;
        }

        if ((false !== strpos($status['tag'], 'exec'))) {
            if ($USER->checkAccess("settopaystatus", User::ACCESS_WRITE)) {
                $etName = $bill->getDirection();
                $selId = 0;
                if ($etName) {
                    $selId = $DB->getField("SELECT `id` FROM `bills_odobr` WHERE `id`=" . $DB->F($etName) . ";");
                }
                return $status['name'] . "</label> <select name='exec_id'>" . self::getOdobrOptions($selId) . "</select><label>";
            } else
                return false;
        }

        if ((false !== strpos($status['tag'], 'agreed'))) {
            if ($USER->checkAccess("setsoglasstatus", User::ACCESS_WRITE)) {
                $etName = $bill->getDirection();
                $selId = 0;
                if ($etName) {
                    $selId = $DB->getField("SELECT `id` FROM `bills_odobr` WHERE `id`=" . $DB->F(getPlatId) . ";");
                }
                return $status['name'] . "</label> <select name='agreed_id'>" . self::getOdobrOptions($selId) . "</select><label>";
            } else
                return false;
        }


        if ((false !== strpos($status['tag'], 'paid'))) {
            $html = $status['name'] . "</label>
                <input type='text' name='paid' class='money' />&nbsp;&nbsp;
                <input type='text' class='date_input' name='paid_date' />
                <select name='paid_from_type_id'>
                    " . bill_types_plugin::getOptions($bill->getExpTypeId()) . "
                </select>
                <label>";
            return $html;
        }
        if ((false !== strpos($status['tag'], 'part'))) {
            $html = $status['name'] . "</label>
                <input type='text' name='paidpart' class='money' />&nbsp;&nbsp;
                <input type='text' class='date_input' name='paidpart_date' />
                <select name='partial_paid_from_type_id'>
                    " . bill_types_plugin::getOptions($bill->getExpTypeId()) . "
                </select>
            <label>";
            return $html;
        }

        return $status['name'];
        //return sizeof($err) ? $err : $status['name'];
    }

    static function getOdobrOptions($sel_id = 0)
    {
        global $DB;
        $sql = "SELECT `id`, `title` FROM `bills_odobr` ORDER BY `order`, `title`";
        return array2options($DB->getCell2($sql, false), $sel_id);
    }

    static function statusCallbackSet($task_id, &$status, &$text, &$tag)
    {
        global $DB, $_config;

        $err = array();
        $plugin_uid = basename(__FILE__, '.php');
        $bill = new Bill($task_id);

        if (false !== strpos($status['tag'], 'paid')) {
            if (!$bill->addPayment($_POST["paid"], $_POST["paid_date"], $_POST['paid_from_type_id'])) {
                $tag .= "(Ошибка добавления оплаты)";
            } else {
                //addPayment($_POST['odobr_id']);
                $typeTitle = bill_types_plugin::getById($_POST['paid_from_type_id']);
                $tag .= " (Оплата: {$_POST["paid"]} от {$_POST["paid_date"]} из статьи \"{$typeTitle}\")";
            }
            //header('Location: /bills/viewbill?task_id='.$task_id);
            //return false;
        }

        if (false !== strpos($status['tag'], 'part')) {
            if (!$bill->addPayment($_POST["paidpart"], $_POST["paidpart_date"], $_POST['partial_paid_from_type_id'])) {
                $tag .= "(Ошибка добавления частичной оплаты)";
            } else {
                $typeTitle = bill_types_plugin::getById($_POST['partial_paid_from_type_id']);
                $tag .= " (Частичная оплата: {$_POST["paidpart"]} от {$_POST["paidpart_date"]} из статьи \"{$typeTitle}\")";
            }
        }


        if (false !== strpos($status['tag'], 'odobreno')) {
            $bill->setOdobrId($_POST['odobr_id']);
            $tag .= " (" . self::getOdobrTitle($_POST['odobr_id']) . ")";
            $bill->setDirection($_POST['odobr_id']);
        }

        if (false !== strpos($status['tag'], 'exec')) {
            $bill->setExecId($_POST['exec_id']);
            $tag .= " (" . self::getOdobrTitle($_POST['exec_id']) . ")";
            $bill->setDirection($_POST['exec_id']);
        }

        if (false !== strpos($status['tag'], 'agreed')) {
            $bill->setAgreedId($_POST['agreed_id']);
            $tag .= " (" . self::getOdobrTitle($_POST['agreed_id']) . ")";
            $bill->setDirection($_POST['agreed_id']);
        }

        if ((false !== strpos($status['tag'], 'otlozh'))) {
            $_REQUEST["otlozh_date"] = $_REQUEST['otlozh_date'] ? date("Y-m-d", strtotime($_REQUEST["otlozh_date"])) : false;
            if ($_REQUEST['otlozh_date']) {
                //$r = $DB->getField("SELECT count(task_id) FROM `bills` WHERE `task_id`=".$DB->F($task_id).";");
                //if ($r>0)
                $DB->query("UPDATE `bills` SET `delayed`=" . $DB->F($_REQUEST["otlozh_date"]) . " WHERE `task_id`=" . $DB->F($task_id) . ";");
                //else
                //    $DB->query("INSERT INTO `bills` (`task_id`, `type`, `zakazchik`, `delayed`) VALUES (".$DB->F($task_id).", 0, 0, ".$DB->F($_REQUEST['otlozh_date']).");");
                if ($DB->errno()) {
                    $err[] = $DB->error();
                } else {
                    $_REQUEST["otlozh_date"] = date("d.m.Y", strtotime($_REQUEST["otlozh_date"]));
                    $tag .= " до {$_REQUEST['otlozh_date']}";
                }
            }
        }

        return sizeof($err) ? $err : true;
    }

    static function getOdobrTitle($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT `title` FROM `bills_odobr` WHERE `id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    function viewlist(Request $request)
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/list.tmpl.htm");
        else
            $tpl->loadTemplatefile("list.tmpl.htm");
        if ($USER->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }
        $sort_ids = array(
            1 => "ID счета",
            2 => "Дата создания",
            3 => "Дата изменения",
            4 => "Срок оплаты",
            5 => "Статья расходов",
            6 => "Автор",
            7 => "Контрагент",
            8 => "Статус"
        );

        $sort_sql = array(
            1 => "ts.id",
            2 => "ts.date_reg",
            3 => "ts.lastcmm_date",
            4 => "bl.date_srok",
            5 => "bl.exp_type_id",
            6 => "AUTHOR_FIO",
            7 => "bl.poluch",
            8 => "st.name"
        );

        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );

        $rpp = array(
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250"
        );

        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);

        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_status_id'] = array_filter(
            $this->getCookie('filter_status_id', $request->request->has('filter_status_id') ? $request->get('filter_status_id') : null),
            function($e) { return !empty($e) && -1 != $e; }
        );
        $_REQUEST['filter_author_ids'] = $this->getCookie('filter_author_ids', $_REQUEST['filter_author_ids']);
        $_REQUEST['filter_author_fio'] = $this->getCookie('filter_author_fio', $_REQUEST['filter_author_fio']);
        $_REQUEST['filter_ispoln_ids'] = $this->getCookie('filter_ispoln_ids', $_REQUEST['filter_ispoln_ids']);
        $_REQUEST['filter_ispoln_fio'] = $this->getCookie('filter_ispoln_fio', $_REQUEST['filter_ispoln_fio']);
        $_REQUEST['filter_btype_id'] = $this->getCookie('filter_btype_id', $_REQUEST['filter_btype_id']);
        $_REQUEST['filter_poluch'] = $this->getCookie('filter_poluch', $_REQUEST['filter_poluch']);
        $_REQUEST['filter_plat_id'] = $this->getCookie('filter_plat_id', $_REQUEST['filter_plat_id']);
        $_REQUEST['bill_contents'] = $this->getCookie('bill_contents', $_REQUEST['bill_contents']);

        $_REQUEST["filter_bill_date_from"] = $this->getCookie('filter_bill_date_from', $_REQUEST['filter_bill_date_from']);
        $_REQUEST["filter_bill_date_to"] = $this->getCookie('filter_bill_date_to', $_REQUEST['filter_bill_date_to']);
        $_REQUEST["filter_billto_date_from"] = $this->getCookie('filter_billto_date_from', $_REQUEST['filter_billto_date_from']);
        $_REQUEST["filter_billto_date_to"] = $this->getCookie('filter_billto_date_to', $_REQUEST['filter_billto_date_to']);
        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', $_REQUEST['filter_sort_id']);
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', $_REQUEST['filter_sort_order']);
        $_REQUEST['filter_rpp'] = $this->getCookie('filter_rpp', $_REQUEST['filter_rpp']);
        $_REQUEST['filter_suppl'] = $this->getCookie('filter_suppl', $_REQUEST['filter_suppl']);

        $tpl->setVariable('FILTER_ID', htmlspecialchars($_REQUEST['filter_id']));
        $status = wf::$em->getRepository(\models\TaskStatus::class)->findBy(['pluginUid' => \models\Task::BILL]);
        $statuses = array_map(function (\models\TaskStatus $item) {
            return [
                'id' => $item->getId(),
                'text' => $item->getName()
            ];
        }, $status);
        $statusStyles = implode("\n", array_map(function (\models\TaskStatus $item) {
            return ".status_{$item->getId()}{
                background:{$item->getColor()};
            }";
        }, $status));
        $tpl->setVariable('STATUS_STYLE', $statusStyles);
        $tpl->setVariable('FILTER_STATUS_OPTIONS', json_encode($statuses));
        $tpl->setVariable('FILTER_STATUS_OPTIONS_SET', (empty($_REQUEST['filter_status_id'])) ? '""' : json_encode($_REQUEST['filter_status_id']));
        $tpl->setVariable('FILTER_AUTHOR_IDS', htmlspecialchars($_REQUEST['filter_author_ids']));
        $tpl->setVariable('FILTER_AUTHOR_FIO', htmlspecialchars_decode($_REQUEST['filter_author_fio']));
        $tpl->setVariable('FILTER_ISPOLN_IDS', htmlspecialchars($_REQUEST['filter_ispoln_ids']));
        $tpl->setVariable('FILTER_ISPOLN_FIO', htmlspecialchars_decode($_REQUEST['filter_ispoln_fio']));
        $tpl->setVariable("FILTER_POLUCH", kontr_plugin::getClientList($_REQUEST['filter_poluch']));
        $tpl->setVariable("FILTER_SUPPL", kontr_plugin::getSupplList($_REQUEST['filter_suppl']));
        $tpl->setVariable('FILTER_PLAT_OPTIONS', $this->getPlatOptions($_REQUEST['filter_plat_id']));
        $tpl->setVariable("FILTER_BTYPE_OPTIONS", bill_types_plugin::getOptions(intval($_REQUEST["filter_btype_id"])));
        $tpl->setVariable("CONTENTS", $_REQUEST['bill_contents']);
        $tpl->setVariable("FILTER_BILL_DATE_FROM", $_REQUEST["filter_bill_date_from"]);
        $tpl->setVariable("FILTER_BILL_DATE_TO", $_REQUEST["filter_bill_date_to"]);
        $tpl->setVariable("FILTER_BILLTO_DATE_FROM", $_REQUEST["filter_billto_date_from"]);
        $tpl->setVariable("FILTER_BILLTO_DATE_TO", $_REQUEST["filter_billto_date_to"]);
        $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
        $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
        $tpl->setVariable('FILTER_ROWS_PER_PAGE', array2options($rpp, $_REQUEST['filter_rpp']));

        $filter = "
            ts.plugin_uid = " . $DB->F($this->getUID()) . " AND
            EXISTS ( SELECT * FROM task_users t1 WHERE t1.task_id = ts.id AND t1.user_id=" . $DB->F($USER->getId()) . " )";

        if (count($_REQUEST['filter_status_id']) > 0) {
            if (in_array(36, $_REQUEST["filter_status_id"])) {
                $filter .= " AND ts.status_id IN (33, 36, 34, 37, 75, 76)";
            } else {
                if (in_array(35, $_REQUEST["filter_status_id"])) {
                    $filter .= " AND ts.status_id IN (39, 35)";
                } else
                    $filter .= " AND ts.status_id in(" . implode(',', array_map($DB->F, $_REQUEST['filter_status_id'])) . ')';
            }
        }
        if ($_REQUEST['filter_id'])
            $filter .= " AND ts.id=" . $DB->F($_REQUEST['filter_id']);
        if ($_REQUEST['filter_btype_id'])
            $filter .= " AND bl.exp_type_id=" . $DB->F($_REQUEST['filter_btype_id']);

        if ($_REQUEST['filter_author_ids'] && ($ids1 = array_map('intval', explode(',', $_REQUEST['filter_author_ids']))) && sizeof($ids1))
            $filter .= " AND ts.author_id IN (" . implode(',', $ids1) . ")";

        if ($_REQUEST['filter_ispoln_ids'] && ($ids2 = array_map('intval', explode(',', $_REQUEST['filter_ispoln_ids']))) && sizeof($ids2))
            $filter .= " AND EXISTS ( SELECT * FROM task_users t2 WHERE t2.task_id = ts.id AND t2.user_id IN (" . implode(',', $ids2) . ") AND t2.ispoln > 0 )";

        if ($_REQUEST['filter_suppl'])
            $filter .= " AND bl.poluch LIKE '" . addslashes($_REQUEST['filter_suppl']) . "%'";
        if ($_REQUEST['filter_poluch'])
            $filter .= " AND bl.client=" . $DB->F($_REQUEST['filter_poluch']);
        if ($_REQUEST['filter_plat_id'])
            $filter .= " AND bl.plat_id=" . $DB->F($_REQUEST['filter_plat_id']);
        if ($_REQUEST['bill_contents'])
            $filter .= " AND (ts.task_title LIKE " . $DB->F("%" . $_REQUEST['bill_contents'] . "%") . "OR ts.task_comment LIKE " . $DB->F("%" . $_REQUEST['bill_contents'] . "%") . ")";

        if (!$_REQUEST['filter_sort_id'])
            $_REQUEST['filter_sort_id'] = 1;
        if (!$_REQUEST['filter_sort_order'])
            $_REQUEST['filter_sort_order'] = "ASC";
        if ($_REQUEST['filter_rpp'] == "")
            $_REQUEST['filter_rpp'] = 20;


        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];

        if ($_REQUEST["filter_bill_date_from"]) {
            $_REQUEST["filter_bill_date_from"] = date("Y-m-d", strtotime($_REQUEST["filter_bill_date_from"]));
            $filter .= " AND DATE_FORMAT(ts.date_reg, '%Y-%m-%d')>=" . $DB->F($_REQUEST["filter_bill_date_from"]);
        }

        if ($_REQUEST["filter_bill_date_to"]) {
            $_REQUEST["filter_bill_date_to"] = date("Y-m-d", strtotime($_REQUEST["filter_bill_date_to"]));
            $filter .= " AND DATE_FORMAT(ts.date_reg, '%Y-%m-%d')<=" . $DB->F($_REQUEST["filter_bill_date_to"]);
        }

        if ($_REQUEST["filter_billto_date_from"]) {
            $_REQUEST["filter_billto_date_from"] = date("Y-m-d", strtotime($_REQUEST["filter_billto_date_from"]));
            $filter .= " AND DATE_FORMAT(bl.date_srok, '%Y-%m-%d')>=" . $DB->F($_REQUEST["filter_billto_date_from"]);
        }

        if ($_REQUEST["filter_billto_date_to"]) {
            $_REQUEST["filter_billto_date_to"] = date("Y-m-d", strtotime($_REQUEST["filter_billto_date_to"]));
            $filter .= " AND DATE_FORMAT(bl.date_srok, '%Y-%m-%d')<=" . $DB->F($_REQUEST["filter_billto_date_to"]);
        }

        $statusList = adm_statuses_plugin::getFullList("bills");
        if ($statusList) {
            $tpl->setCurrentBlock("sl-items");
            //$statusList[] = array("id"=>"1000", "name"=>"Активные", "color"=>"#99cc33", "plugin_uid"=>"projects", "tag"=>"prall");
            foreach ($statusList as $st_item) {
                $tpl->setCurrentBlock("statuslist");
                if (in_array($st_item["id"], $_REQUEST['filter_status_id'])) {
                    $tpl->setVariable("FF_SELECTED", 'ffstisel');
                }
                $tpl->setVariable("FF_STATUS_ID", $st_item["id"]);
                $tpl->setVariable("FF_STATUS", $st_item["name"]);
                $tpl->setVariable("FF_STATUS_COLOR", $st_item["color"]);
                $tpl->parse("statuslist");
            }
            if (in_array('-1', $_REQUEST['filter_status_id'])) {
                $tpl->setVariable("FF_ALL_SELECTED", "ffstisel");
            }
            $tpl->parse("sl-items");
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    ts.lastcmm_user_id AS lastuser,
					bl.poluch AS POLUCH,
                    bl.client AS CLIENT,
					bl.num AS NUMBER,
					ts.task_comment AS TASK_COMMENT,
					bl.sum AS SUM,
                    us.fio AS AUTHOR_FIO,
                    bl.exp_type_id AS EXPTYPE,
                    bl.agr_id AS POLAGR,
                    pl.name AS PLAT,
                    st.id AS STATUS_ID,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
					DATE_FORMAT(bl.date_srok, '%d.%m.%Y') AS DATE_SROK,
                    DATE_FORMAT(bl.date_srokpost, '%d.%m.%Y') AS DATE_SROKPOST,
                    (select DATE_FORMAT(pay_date, '%d.%m.%Y') from bills_payments where bill_id = ts.id order by pay_date desc limit 1) as LAST_PAY_DATE
                FROM `task_users` AS tsu
                JOIN `tasks` AS ts ON tsu.task_id=ts.id
				JOIN `bills` AS bl ON ts.id=bl.task_id
                LEFT JOIN `task_status` AS st ON ts.status_id=st.id
				LEFT JOIN `bills_plat` AS pl ON bl.plat_id=pl.id
				LEFT JOIN `task_users` AS tsu2 ON tsu2.task_id=ts.id
                LEFT JOIN `users` AS us ON ts.author_id=us.id

                WHERE $filter
				GROUP BY ts.id
                ORDER BY $order
                LIMIT {$_REQUEST['filter_start']},{$_REQUEST["filter_rpp"]};";
        $DB->query($sql);
        $this->getLogger()->debug('query', ['query' => $sql]);
        $total = $DB->getFoundRows();
        $totalsum = 0;
        while ($a = $DB->fetch(true)) {
            $totalsum += $a["SUM"];
            $lastuser = adm_users_plugin::getUser($a["lastuser"]);
            $a["LASTUSER"] = $lastuser["fio"];
            $a['HREF'] = $this->getLink('viewbill', "task_id={$a['TASK_ID']}");
            $a['ISPOLN_FIO'] = Task::getTaskIspoln1($a['TASK_ID']);

            $bill = new Bill($a['TASK_ID']);
            $billPayments = $bill->getPayments();
            if (!empty($billPayments) && $a['STATUS_ID'] == \models\TaskStatus::STATUS_BILL_PARTLY_PAYED) {
                $sum = array_reduce($billPayments, function ($sum, $payment) {
                    return $sum += $payment["ammount"];
                }, 0);

                $a["SUM"] = number_format($a['SUM'] - $sum, 2, ".", " ");
            } else {
                $a["SUM"] = number_format($a["SUM"], 2, ".", " ");
            }


            $a["POLUCH"] = intval($a["POLUCH"]) ? kontr_plugin::getByID($a["POLUCH"]) : ($a["POLUCH"] ? $a["POLUCH"] : "не указан");
            $a["CLIENT"] = intval($a["CLIENT"]) ? kontr_plugin::getByID($a["CLIENT"]) : ($a["CLIENT"] ? $a["CLIENT"] : "не указан");
            $a["EXPTYPE"] = $a["EXPTYPE"] ? bill_types_plugin::getById($a["EXPTYPE"]) : "&mdash;";
            $a["POLAGR"] = $a["POLAGR"] ? kontr_plugin::getAgrByIdT($a["POLAGR"]) : "&mdash;";
            $task = new Task($a['TASK_ID']);
            $status = $task->getStatusArray();
            if (strpos($status['tag'], 'otlozh') !== FALSE) {

                $dd = $DB->getField("SELECT DATE_FORMAT(`delayed`, '%d.%m.%Y') FROM `bills` WHERE `task_id`=" . $DB->F($task->getId()) . ";");
                if ($dd) {
                    $a["STATUS_NAME"] = $task->getStatusName() . " до " . $dd;
                    if ($a["delay1"] <= date('Y-m-d')) {
                        $a["STATUS_COLOR"] = "#FFFCA3";
                    }
                }
            }
            $a["LAST_PAY_DATE"] = $a["LAST_PAY_DATE"] && $a["LAST_PAY_DATE"] != "00.00.0000" ? $a["LAST_PAY_DATE"] : "&mdash;";
            $a["LASTCOMMENTVALUE"] = preg_replace("/'/", "", $task->getLastComment(true));
            $tpl->setCurrentBlock('row');
            $tpl->setVariable($a);
            $tpl->parse('row');
        }
        $tpl->setVariable("TOTALSUMM", number_format($totalsum, 2, ".", " "));
        $DB->free();

        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"], $total, "#start-%s"));

        UIHeader($tpl);
        $tpl->show();
    }

    static function getPlatOptions($sel_id = 0)
    {
        global $DB;
        $sql = "SELECT `id`, `name` FROM `bills_plat` ORDER BY `name`";
        return array2options($DB->getCell2($sql, false), $sel_id);
    }

    function deletepayment()
    {
        global $DB, $USER;
        $bill_id = $_REQUEST["bill_id"];
        if ($id = $_REQUEST["id"]) {
            $DB->query("DELETE FROM `bills_payments` WHERE `id`=" . $DB->F($id) . ";");
            if ($DB->errno())
                $ret["error"] = $DB->error();
            else {
                $ret["ok"] = "ok";
                $bill = new Bill($bill_id);
                $bp = $bill->getPayments();
                $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
                $tpl->loadTemplatefile($USER->getTemplate() . "/payments.tmpl.htm");

                if ($bp) {
                    $tpl->setVariable("NOPAYSHIDDEN", "style='display:none;'");
                    $paid = 0;
                    foreach ($bp as $item) {
                        $tpl->setCurrentBlock("payments");
                        $tpl->setVariable("PID", $item["id"]);
                        $tpl->setVariable("PAMMOUNT", number_format($item["ammount"], 2, ".", " "));
                        $paid += $item["ammount"];
                        $tpl->setVariable("PDATE", rudate("d.m.Y H:i", strtotime($item["pay_date"])));
                        $user = adm_users_plugin::getUser($item["user_id"]);
                        $tpl->setVariable("PUSER", $user["fio"]);
                        $tpl->setVariable("PAYMENT_TYPE", bill_types_plugin::getById($item['type_id']));

                        $tpl->setVariable("POPDATE", rudate("d.m.Y H:i", strtotime($item["op_date"])));
                        $tpl->parse("payments");
                    }
                    $tpl->setVariable("TOTALPAID", number_format($paid, 2, ".", " "));
                    if ($bill->getSum() < $paid) {
                        $tpl->setVariable("PAIDMORETHAN", "style='color:#FF0000;'");
                    }
                    if ($bill->getSum() > $paid) {
                        $tpl->setVariable("PAIDMORETHAN", "style='color:#ff8a0c;'");
                    }
                    if ($bill->getSum() == $paid) {
                        $tpl->setVariable("PAIDMORETHAN", "style='color:#43a917;'");
                    }
                }

                $tpl->setVariable("TOTALREQUIRED", number_format($bill->getSum(), 2, ".", " "));

                $ret["tpl"] = $tpl->get();
            }
        } else {
            $ret["error"] = "Не указан идентификатор платежа!";
        }
        echo json_encode($ret);
        return;
    }

    function getpayment_ajax()
    {
        global $DB, $USER;
        $bill_id = $_REQUEST["bill_id"];

        $bill = new Bill($bill_id);
        $bp = $bill->getPayments();
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/payments.tmpl.htm");

        if ($bp) {
            $tpl->setVariable("NOPAYSHIDDEN", "style='display:none;'");
            $paid = 0;
            foreach ($bp as $item) {
                $tpl->setCurrentBlock("payments");
                $tpl->setVariable("PID", $item["id"]);
                $tpl->setVariable("PAMMOUNT", number_format($item["ammount"], 2, ".", " "));
                $paid += $item["ammount"];
                $tpl->setVariable("PDATE", rudate("d.m.Y H:i", strtotime($item["pay_date"])));
                $user = adm_users_plugin::getUser($item["user_id"]);
                $tpl->setVariable("PUSER", $user["fio"]);
                $tpl->setVariable("PAYMENT_TYPE", bill_types_plugin::getById($item['type_id']));
                $tpl->setVariable("POPDATE", rudate("d.m.Y H:i", strtotime($item["op_date"])));
                $tpl->parse("payments");
            }
            $tpl->setVariable("TOTALPAID", number_format($paid, 2, ".", " "));
            if ($bill->getSum() < $paid) {
                $tpl->setVariable("PAIDMORETHAN", "style='color:#FF0000;'");
            }
            if ($bill->getSum() > $paid) {
                $tpl->setVariable("PAIDMORETHAN", "style='color:#ff8a0c;'");
            }
            if ($bill->getSum() == $paid) {
                $tpl->setVariable("PAIDMORETHAN", "style='color:#43a917;'");
            }
        }

        $tpl->setVariable("TOTALREQUIRED", number_format($bill->getSum(), 2, ".", " "));

        $ret["tpl"] = $tpl->get();

        echo json_encode($ret);
        return;
    }

    function viewbill(Request $request)
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));

        if ($USER->getTemplate() != "default") {
            $tpl->loadTemplatefile($USER->getTemplate() . "/bill.tmpl.htm");
        } else {
            $tpl->loadTemplatefile("bill.tmpl.htm");
        }

        $task_id = $request->get('task_id');

        if (!$task_id) {
            UIError("Не указан идентификатор счета!");
        }

        $err = array();
        $bill = new Bill($task_id);

        if (!$bill->getId()) {
            UIError("Счет с таким номером не найден! Возможно, он был удален.");
        } elseif ($bill->getPluginUid() != $this->getUID()) {
            $err[] = "Задача $task_id не является проектом";
        } elseif (!$bill->isAccessable()) {
            if ($bill->getAuthorFio()) {
                $err[] = "Вас нет в списке связанных лиц, обратитесь к автору счета: <a href='mailto:" . $DB->getField("SELECT `email` FROM `users` WHERE `id`=" . $bill->getAuthorId()) . "'>" . $bill->getAuthorFio() . " (" . $DB->getField("SELECT `email` FROM `users` WHERE `id`=" . $bill->getAuthorId()) . ")</a>";
            } else {
                $err[] = "Автор счета не опознан. Возможно, был удален.";
            }
        }

        if (sizeof($err)) {
            UIError($err, "Ошибка", true, false, false, true);
        }

        $projects = false;
        $sql = "SELECT jt.task_id, t.plugin_uid, t.task_title, DATE_FORMAT(t.date_reg, '%d.%m.%Y %H:%i') AS date_reg, t.status_id FROM `joinedbills` AS jt LEFT JOIN `tasks` AS t ON jt.task_id=t.id WHERE jt.joined_bill_id=" . $DB->F($task_id) . ";";
        $DB->query($sql);
        if ($DB->errno())
            UIError($DB->error());
        if ($DB->num_rows()) {
            $projects = true;
            while ($r = $DB->fetch(true)) {
                $lp[] = $r["task_id"];
                $tpl->setCurrentBlock("lpb-list");
                $tpl->setVariable("ITEMTYPE", "Проект");
                $tt = new Task($r["task_id"]);
                $tpl->setVariable("LSTATE", $tt->getStatusName());
                $tpl->setVariable("ITEMLINK", "projects/viewtask?task_id=" . $r["task_id"]);
                $tpl->setVariable("ITEMID", $r["task_id"]);
                $tpl->setVariable("ITEMTITLE", $r["task_title"]);
                $tpl->setVariable("ITEMDATE", $r["date_reg"]);
                $tpl->parse("lpb-list");
            }
            $tpl->setVariable("LINKED_PROJECTS", implode(",", $lp));
        }
        if (!$projects) {
            $tpl->touchBlock("nojoinedbillsorprojects");
        }
        $tpl->setVariable('TASK_ID', $task_id);
        $tpl->setVariable('TASK_TITLE', $bill->getTitle());
        $tpl->setVariable('TASK_COMMENT', $bill->getComment(true));
        $tpl->setVariable('AUTHOR_FIO', $bill->getAuthorFio());
        $tpl->setVariable('ISPOLN_FIO', $bill->getIspoln('<br>'));
        $tpl->setVariable('USERS_FIO', $bill->getUsers('<br>'));
        $status = $bill->getStatusArray();
        if (strpos($status['tag'], 'otlozh') !== FALSE) {
            $dd = $DB->getField("SELECT DATE_FORMAT(`delayed`, '%d.%m.%Y') FROM `bills` WHERE `task_id`=" . $DB->F($task_id) . ";");
            if ($dd)
                $tpl->setVariable('STATUS_NAME', $bill->getStatusName() . " до " . $dd);
        } else {
            $tpl->setVariable('STATUS_NAME', $bill->getStatusName());
        }
        //$tpl->setVariable('STATUS_NAME', $bill->getStatusName());
        $tpl->setVariable('STATUS_COLOR', $bill->getStatusColor());
        $tpl->setVariable('DATE_REG', $bill->getDateReg("d.m.Y H:i"));
        $tpl->setVariable("EXPTYPE", $bill->getExpTypeId() ? bill_types_plugin::getById($bill->getExpTypeId()) : "&mdash;");
        $tpl->setVariable('DATE_SROK', $bill->getDateSrok("d.m.Y"));
        $tpl->setVariable('POLUCH', intval($bill->getPoluch()) ? (kontr_plugin::getByID($bill->getPoluch()) ? kontr_plugin::getByID($bill->getPoluch()) : "&mdash;") : ($bill->getPoluch() ? $bill->getPoluch() : "&mdash;"));
        $tpl->setVariable('CLIENT', intval($bill->getClient()) ? (kontr_plugin::getByID($bill->getClient()) ? kontr_plugin::getByID($bill->getClient()) : "&mdash;") : ($bill->getClient() ? $bill->getClient() : "&mdash;"));
        $tpl->setVariable('PLAT', $bill->getPlatName()); //. " (" . $bill->getPlatText() . ")");
        $tpl->setVariable('SUM', rub($bill->getSum()));

        /** @var models\Bill $model */
        $model = wf::$em->find(\models\Bill::class, $bill->getId());
        /** @var \repository\BillTypeLimitRepository $repo */
        $repo = wf::$em->getRepository(\models\BillTypeLimit::class);

        $monthLimitRest = $repo->getMonthLimitRest($model->getType());
        $monthLimitReserved = $repo->getMonthLimitReserved($model->getType());

        if ($monthLimitRest < 0)
            $monthLimitRest = '<span style="color: #ff0000">' . rubsign($monthLimitRest) . "</span>";
        else
            $monthLimitRest = rubsign($monthLimitRest);
        $tpl->setVariable('MONTH_REST', $monthLimitRest);

        if ($monthLimitReserved < 0)
            $monthLimitReserved = rubsign(0);
        else
            $monthLimitReserved = rubsign($monthLimitReserved);

        $tpl->setVariable('MONTH_RESERVED', $monthLimitReserved);

        $yearLimitRest = $repo->getYearLimitRest($model->getType());
        $yearLimitReserved = $repo->getYearLimitReserved($model->getType());

        if ($yearLimitRest < 0)
            $yearLimitRest = '<span style="color: #ff0000">' . rubsign($yearLimitRest) . "</span>";
        else
            $yearLimitRest = rubsign($yearLimitRest);
        $tpl->setVariable('YEAR_REST', $yearLimitRest);

        if ($yearLimitReserved < 0)
            $yearLimitReserved = rubsign(0);
        else
            $yearLimitReserved = rubsign($yearLimitReserved);
        $tpl->setVariable('YEAR_RESERVED', $yearLimitReserved);

        $tpl->setVariable("POLUCH_AGR", $bill->getAgrId() ? kontr_plugin::getAgrByIdT($bill->getAgrId()) : "&mdash;");
        $tpl->setVariable("CLIENT_AGR", $bill->getClntAgrId() ? kontr_plugin::getAgrByIdT($bill->getClntAgrId()) : "&mdash;");
        $tpl->setVariable('TASK_COMMENTS', $bill->getComments(true, array(
            'bill_direction' => 1
        )));
        $tpl->setVariable("TASK_PAYDESC", $bill->getBillDesc());
        $tpl->setVariable("SROK_POST", $bill->getDatePost() && $bill->getDatePost() != "0000-00-00" ? $bill->getDatePost() : "&mdash;");
        $bp = $bill->getPayments();
        if ($bp) {
            $tpl->setVariable("NOPAYSHIDDEN", "style='display:none;'");
            $paid = 0;
            foreach ($bp as $item) {
                $tpl->setCurrentBlock("payments");
                $tpl->setVariable("PID", $item["id"]);
                $tpl->setVariable("PAMMOUNT", number_format($item["ammount"], 2, ".", " "));
                $paid += $item["ammount"];
                $tpl->setVariable("PDATE", rudate("d.m.Y", strtotime($item["pay_date"])));
                $user = adm_users_plugin::getUser($item["user_id"]);
                $tpl->setVariable("PUSER", $user["fio"]);
                $tpl->setVariable("PAYMENT_TYPE", bill_types_plugin::getById($item['type_id']));
                $tpl->setVariable("POPDATE", rudate("d.m.Y H:i", strtotime($item["op_date"])));
                $tpl->parse("payments");
            }
            $tpl->setVariable("TOTALPAID", number_format($paid, 2, ".", " "));
            if ($bill->getSum() < $paid) {
                $tpl->setVariable("PAIDMORETHAN", "style='color:#FF0000;'");

                $tpl->setCurrentBlock('partly_payed');
                $tpl->setVariable('PAYED', number_format($paid, 2, ".", " "));
                $tpl->setVariable('PAY_REST', number_format($bill->getSum() - $paid, 2, ".", " "));
                $tpl->parse('partly_payed');
            }
            if ($bill->getSum() > $paid) {
                $tpl->setVariable("PAIDMORETHAN", "style='color:#ff8a0c;'");

                $tpl->setCurrentBlock('partly_payed');
                $tpl->setVariable('PAYED', number_format($paid, 2, ".", " "));
                $tpl->setVariable('PAY_REST', number_format($bill->getSum() - $paid, 2, ".", " "));
                $tpl->parse('partly_payed');
            }
            if ($bill->getSum() == $paid) {
                $tpl->setVariable("PAIDMORETHAN", "style='color:#43a917;'");
            }
        }
        $tpl->setVariable("TOTALREQUIRED", number_format($bill->getSum(), 2, ".", " "));
        UIHeader($tpl);
        $tpl->show();
    }

    function editbill()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("editor.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/editor.tmpl.htm");
        else
            $tpl->loadTemplatefile("editor.tmpl.htm");
        $err = array();
        $bill = new Bill($_REQUEST['task_id']);
        if (!kontr_plugin::getSupplList())
            UIError("Нет списка получателей");
        if ($task_id = $bill->getId()) {
            if ($bill->getPluginUid() != $this->getUID())
                $err[] = "Задача $task_id не является счетом";
            if ($bill->getAuthorId() != $USER->getId() && !$USER->checkAccess("fullaccesstobills", User::ACCESS_WRITE))
                $err[] = "Только автор может отредактировать счет";
            if (sizeof($err))
                UIError($err);


            $tpl->setVariable('TASK_ID', $task_id);
            $tpl->setVariable('ID_TEXT', $task_id);
            $tpl->setVariable('PLAT_OPTIONS', $this->getPlatOptions($bill->getPlatId()));
            $tpl->setVariable('PLAT_TEXT', $bill->getPlatText());
            $tpl->setVariable('TASK_COMMENT', $bill->getComment(false));
            $tpl->setVariable('SUM', $bill->getSum());
            //echo $bill->getPoluch()."<br />";
            //echo $bill->getClient();
            $tpl->setVariable("SUPPL_OPTIONS", kontr_plugin::getSupplList($bill->getPoluch()));
            $tpl->setVariable("CLIENT_OPTIONS", kontr_plugin::getClientList($bill->getClient()));
            $tpl->setVariable('NUM', $bill->getNum());
            $tpl->setVariable('ISPOLN_FIO', $bill->getIspoln('<br>'));
            $tpl->setVariable('SROK', $bill->getDateSrok("d.m.Y"));
            $tpl->setVariable('ISPOLN_IDS', implode(',', array_keys($bill->getIspoln())));
            $tpl->setVariable('USERS_FIO', $bill->getUsers('<br>'));
            $tpl->setVariable("POLUCH_AGR_ID", $bill->getAgrId());
            $tpl->setVariable("CLIENT_AGR_ID", $bill->getClntAgrId());

            $tpl->setVariable('USERS_IDS', implode(',', array_keys($bill->getUsers())));
            $tpl->setVariable("EXPTYPES", bill_types_plugin::getOptions($bill->getExpTypeId()));
            $tpl->setVariable("TASK_PAYDESC", $bill->getBillDesc());
            $tpl->setVariable("SROK_POST", $bill->getDatePost() && $bill->getDatePost() != "0000-00-00" ? $bill->getDatePost() : "");
        } else {
            $tpl->setVariable('ID_TEXT', 'новый');
            $tpl->touchBlock('files');
            $tpl->setVariable("SUPPL_OPTIONS", kontr_plugin::getSupplList());
            $tpl->setVariable('PLAT_OPTIONS', $this->getPlatOptions());
            $tpl->setVariable("EXPTYPES", bill_types_plugin::getOptions());
            $tpl->setVariable("CLIENT_OPTIONS", kontr_plugin::getClientList());
        }

        UIHeader($tpl);
        $tpl->show();
    }

    function savebill(Request $request)
    {

        global $USER, $DB;

        $err = array();
        $bill = new Bill($_POST['task_id']);

        if (!$_POST['comment'])
            $err[] = "Не указано описание";
        if (!$_POST['sum'])
            $err[] = "Не указана сумма";
        //if(!$_POST['client']) $err[] = "Не указан Заказчик";
        if (!$_POST['poluch'])
            $err[] = "Не указан Заказчик";
        if (!$_POST['num'])
            $err[] = "Не указан № счета";
        if (!$_POST['ispoln_ids'])
            $_POST['ispoln_ids'] = $USER->getId();

        if (sizeof($err))
            UIError($err);

        if ($task_id = $bill->getId()) {
            if ($bill->getPluginUid() != $this->getUID())
                $err[] = "Задача $task_id не является cчетом";
            if ($bill->getAuthorId() != $USER->getId() && !$USER->checkAccess("fullaccesstobills", User::ACCESS_WRITE))
                $err[] = "Только автор может отредактировать счет";
            if (sizeof($err))
                UIError($err);

            $bill->setTitle("Счет {$_POST['num']}");
            $bill->setComment($_POST['comment']);
            $cmm = '';
            if ($bill->getComment() != $_POST["comment"]) {
                $cmm .= "Назначение платежа: " . $bill->getComment() . " -> " . $_POST["comment"] . "\r\n";
            }


            //$bill->setIspoln($_POST['ispoln_ids']);
            //$bill->setUsers();
            if ($_POST["ispoln_ids"])
                $cmm .= $bill->setExecutors(explode(',', $_POST["ispoln_ids"]));

            if ($_POST['user_ids']) {
                $cmm .= $bill->setBoundedUsers(explode(',', $_POST['user_ids'] . ',' . $bill->getAuthorId()));
            }

            if ($bill->getPlatId() != $_POST['plat_id']) {
                $cmm .= "Плательщик: " . $bill->getPlatName() . " -> " . $this->getPlatTitle($_POST["plat_id"]) . "\r\n";
            }
            $bill->setPlatId($_POST['plat_id']);
            if ($bill->getPlatText() != $_POST['plat_text']) {
                $cmm .= "Назначение платежа: " . $bill->getPlatText() . " -> " . $_POST["plat_text"] . "\r\n";
            }
            $bill->setPlatText($_POST['plat_text']);
            if ($bill->getSum() != preg_replace("/\s/", "", $_POST['sum'])) {
                $cmm .= "Сумма счета: " . $bill->getSum() . " -> " . preg_replace("/\s/", "", $_POST['sum']) . "\r\n";
            }
            $bill->setSum(preg_replace("/\s/", "", $_POST['sum']));
            if ($_POST["poluch"] != $bill->getPoluch()) {
                $cmm .= "Поставщик: " . kontr_plugin::getByID($bill->getPoluch()) . " -> " . kontr_plugin::getByID($_POST["poluch"]) . "\r\n";
            }
            if ($_POST["client"] != $bill->getClient()) {
                $cmm .= "Заказчик: " . kontr_plugin::getByID($bill->getClient()) . " -> " . kontr_plugin::getByID($_POST["client"]) . "\r\n";
            }
            $bill->setPoluch($_POST['poluch']);
            $bill->setClient($_POST['client']);
            $bill->setNum($_POST['num']);
            if ($bill->getNum() != $_POST["num"]) {
                $cmm .= "Номер счета: " . $bill->getNum() . " -> " . $_POST["num"] . "\r\n";
            }
            $bill->setDateSrok($_POST['srok']);
            if ($bill->getDateSrok() != $_POST["srok"]) {
                $cmm .= "Дата платежа: " . $bill->getDateSrok() . " -> " . $_POST["srok"] . "\r\n";
            }
            if ($bill->getExpTypeId() != $_POST["exptype"]) {
                $cmm .= "Статья расходов: " . $bill->getExpTypeName() . " -> " . $bill->getExpTypeNameById($_POST["exptype"]) . "\r\n";
            }
            $bill->setExpTypeId($_POST["exptype"]);

            if ($bill->getAgrId() != $_POST["poluch_agr"]) {
                $cmm .= "Договор: " . kontr_plugin::getAgrByIdT($bill->getAgrId()) . " -> " . kontr_plugin::getAgrByIdT($_POST["poluch_agr"]);
            }
            $bill->setAgrId($_POST["poluch_agr"]);

            if ($bill->getClntAgrId() != $_POST["client_agr"]) {
                $cmm .= "Проект: " . kontr_plugin::getAgrByIdT($bill->getClntAgrId()) . " -> " . kontr_plugin::getAgrByIdT($_POST["client_agr"]);
            }
            $bill->setClntAgrId($_POST["client_agr"]);

            if ($bill->getBillDesc() != $_POST["paydesc"]) {
                $cmm .= "Описание закупки: " . $bill->getBillDesc() . " -> " . $_POST["paydesc"] . "\r\n";
            }
            $bill->setBillDesc($_POST["paydesc"]);
            if ($bill->getDatePost() != $_POST["srok_post"]) {
                $cmm .= "Срок поставки: " . $bill->getDatePost() . " -> " . $_POST["srok_post"] . "\r\n";
            }
            $bill->setDatePost($_POST["srok_post"]);
            $bill->addComment($cmm);
        } else {
            $task_id = Bill::createBill(
                $request->get('plat_id'),
                $request->get('plat_text'),
                $request->get('comment'),
                preg_replace("/\s/", "", $request->get('sum')),
                $request->get('poluch'),
                $request->get('client'),
                $request->get('num'),
                $request->get('srok'),
                $request->get('ispoln_ids'),
                $request->get('user_ids'),
                $request->get("exptype"),
                $request->get("poluch_agr"),
                $request->get("client_agr"),
                $this->getUserId()
            );
            $bill = new Bill($task_id);
            $bill->setBillDesc($_POST["paydesc"]);
            if (!empty($_POST["srok_post"])) {
                $bill->setDatePost($_POST["srok_post"]);
            }

            foreach ($_FILES['userfile']['error'] as $i => $error) {
                if ($error == UPLOAD_ERR_OK) {
                    if (!$bill->addFile($_FILES['userfile']['tmp_name'][$i],
                        $_FILES['userfile']['name'][$i],
                        $_FILES['userfile']['type'][$i])
                    ) {
                        $err[] = "Не удалось добавить файл '{$_FILES['userfile']['name'][$i]}' к задаче";
                    }
                } elseif ($error != UPLOAD_ERR_NO_FILE) {
                    $err[] = "Не удалось загрузить файл '{$_FILES['userfile']['name'][$i]}' на сервер";
                }
            }

            if (sizeof($err)) {
                UIError($err, "Не удалось обработать все файлы", true, "Продолжить", $this->getLink('viewbill', "task_id=$task_id"));
            }
        }
        redirect($this->getLink('viewbill', "task_id=$task_id"));
    }

    static function getPlatTitle($sel_id)
    {
        global $DB;
        $sql = "SELECT `name` FROM `bills_plat` WHERE `id`=" . $DB->F($sel_id) . ";";
        return $DB->getField($sql);
    }

    function editlinkedprojects()
    {
        global $DB, $USER;
        $id = $_REQUEST["bill_id"];
        if (!$id) {
            $ret["error"] = "Не указан проект!";
            echo json_encode($ret);
            return false;
        }
        $task = new Task($id);
        $sql = "DELETE FROM joinedbills WHERE `joined_bill_id`=" . $DB->F($id) . ";";
        $DB->query($sql);
        if ($DB->errno()) {
            $ret["error"] = $DB->error();
            echo json_encode($ret);
            return false;
        }
        if ($_REQUEST["projects"]) {
            $np = explode(",", $_REQUEST["projects"]);
            foreach ($np as $item) {
                if ($id != $item)
                    $sql_add[] = "(" . $DB->F($item) . ", " . $DB->F($id) . ")";
            }
            if (count($sql_add)) {
                $sql = "INSERT IGNORE INTO `joinedbills` (`task_id`, `joined_bill_id`) VALUES " . implode(", ", $sql_add);
                $DB->query($sql);
                if ($DB->errno()) {
                    $ret["error"] = $DB->error();
                    echo json_encode($ret);
                    return false;
                }
            }
        }
        if ($_REQUEST["projects"]) {
            $task->addComment("Новый список: " . $_REQUEST["projects"], "Обновлен список связанных проектов.");
        } else {
            $task->addComment("Новый список: отсутствует", "Обновлен список связанных проектов.");
        }
        $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;
    }

}
