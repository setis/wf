<?php 
/**
 * Plugin Header
 * @author kblp
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Cчета";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Счета";
    $PLUGINS[$plugin_uid]['events']['viewbill'] = "Просмотр счета";
    $PLUGINS[$plugin_uid]['events']['getpayment_ajax'] = "Получение платежей по счету";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['editbill'] = "Редактирование счета";
    $PLUGINS[$plugin_uid]['events']['savebill'] = "Сохранение счета";  
    $PLUGINS[$plugin_uid]['events']['editlinkedprojects'] = "Сохранение связанных проектов";  
    $PLUGINS[$plugin_uid]['events']['deletepayment'] = "Удаление платежа по счету";  
    
    
}

$plugin_uid = "fullaccesstobills";

$PLUGINS[$plugin_uid]['name'] = "Счета: Редактирование в любых статусах";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "setsoglasstatus";

$PLUGINS[$plugin_uid]['name'] = "Счета: Статус Согласовано";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "setodobrstatus";

$PLUGINS[$plugin_uid]['name'] = "Счета: Статус Одобрено";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "settopaystatus";

$PLUGINS[$plugin_uid]['name'] = "Счета: Статус В оплату";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "settogostatus";

$PLUGINS[$plugin_uid]['name'] = "Счета: Статус К Выдаче";
$PLUGINS[$plugin_uid]['hidden'] = true;


?>