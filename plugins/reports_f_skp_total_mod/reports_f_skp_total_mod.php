<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\tickets\ServiceTicket;


require_once(dirname(__FILE__)."/../services/services.php");

class reports_f_skp_total_mod_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $_REQUEST["sc_id"] = $this->getCookie("sc_id", (isset($_REQUEST["sc_id"]) ? $_REQUEST["sc_id"] : 8));
        $_REQUEST["createreport"] = $this->getCookie("createreport", $_REQUEST["createreport"]);
        $_REQUEST["datefrom"] = $this->getCookie("datefrom", $_REQUEST["datefrom"]);
        $_REQUEST["dateto"] = $this->getCookie("dateto", $_REQUEST["dateto"]);
        //$_REQUEST["month"] = $this->getCookie('month', $_REQUEST["month"] ? $_REQUEST["month"] : date("m"));
        //$_REQUEST["year"] = $this->getCookie('year', $_REQUEST["year"] ? $_REQUEST["year"] : date("Y"));
        $dateParams = array(0 => "Дата создания", 1 => "Дата выполнения", 2=>"Дата изменения", 3=>"Дата принятия ДС");
        if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
            $cntr = new kontr_plugin();
        }   
        $contrCount = $cntr->getListSer();                    
        if (!isset($_REQUEST['createprintversion']) && !isset($_REQUEST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_SKP_TOTAL", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            $tpl->setVariable("DATE_FROM", $_REQUEST["datefrom"]);
            $tpl->setVariable("DATE_TO", $_REQUEST["dateto"]);
            
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id"]));
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListSer(@$_POST["cnt_id"]));
            } 
            
            
        }
        if (@$_REQUEST["createreport"] || @$_REQUEST["createprintversion"] || @$_REQUEST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_REQUEST['createprintversion']) || @$_REQUEST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $datefrom = $_REQUEST["datefrom"];
            $dateto = $_REQUEST["dateto"];
            //$period = $_REQUEST["year"]."-".$_REQUEST["month"];
            $rtpl->setVariable('CURRENT_PERIOD', $_REQUEST["datefrom"]." - ".$_REQUEST["dateto"]);
            $sc = adm_sc_plugin::getSC($_REQUEST["sc_id"]);
            $rtpl->setVariable("SC_NAME", $_REQUEST["sc_id"] ? $sc['title'] : "Все");
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "services");
            $countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "services");
            $otkaz = adm_statuses_plugin::getStatusByTag("otkaz", "services");
            $opotkaz = adm_statuses_plugin::getStatusByTag("opotkaz", "services");
            $accepted = adm_statuses_plugin::getStatusByTag("acc_accepted", "services");
            /*$sql = "SELECT t.id FROM `tasks` AS t 
                            LEFT JOIN `task_users` AS tu ON tu.task_id=t.id 
                            LEFT JOIN tickets AS tick ON tick.task_id=t.id 
                            WHERE t.plugin_uid='services' AND
                                tu.user_id IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`= ".$DB->F($_REQUEST["sc_id"]).")
                                GROUP BY t.id ORDER BY 1;"; 
            *///die($sql);
            if ($_REQUEST["sc_id"]) 
                $sql = "SELECT * FROM `users` WHERE id IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`= ".$DB->F($_REQUEST["sc_id"]).") AND `active` ORDER BY `fio`;";
            else
                $sql = "SELECT * FROM `users` WHERE id IN (SELECT `user_id` FROM `list_empl`) AND `active` ORDER BY `fio`;";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error()." ".$sql);
            $total_techs = 0;
            $total = 0;
            $totalCompleted = 0;
            $prelim_total = 0;
            $total_paid = 0;
            $total_conversion = 0;
            $total_avg_check = 0;
            $totalBonus = 0;
            $totalunpaid = 0;     
            /*if (count($_REQUEST['cnt_id']) != count($contrCount)) {
                $add_filter = " AND tick.cnt_id IN (".preg_replace("/,$/", "", implode(",", $_REQUEST["cnt_id"])).") ";
            }*/
                        
            $add_filter = $_REQUEST["cnt_id"]!=0 ? "AND tick.cnt_id IN (".implode(",", $_REQUEST["cnt_id"]).")" : "";
            
            if ($add_filter=="") {
                $rtpl->touchBlock("no-rows");
            } else {
            while ($res = $DB->fetch(true)) {
                
                /*$sql = "SELECT COUNT(DISTINCT(t.id)) as cnt, SUM(tick.orient_price) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                            WHERE t.id IN (SELECT g.task_id FROM `gfx` AS g WHERE g.empl_id = ".$DB->F($res["id"])." GROUP BY g.task_id) 
                                AND t.plugin_uid='services' 
                                AND DATE_FORMAT(t.date_reg, '%Y-%m')=".$DB->F($period)." AND tick.task_type='10' ".$add_filter."; ";               
                */
                $sql = "SELECT COUNT(DISTINCT(t.id)) as cnt, SUM(tick.orient_price) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                            WHERE t.id IN (SELECT g.task_id FROM `gfx` AS g WHERE g.empl_id = ".$DB->F($res["id"])." GROUP BY g.task_id) 
                                AND t.plugin_uid='services' AND tick.task_type='10'  
                                AND t.id IN (SELECT task_id FROM `task_comments` WHERE 
                                (`status_id`=".$DB->F($doneStatus["id"])." OR `status_id`=".$DB->F($otkaz["id"])." OR `status_id`=".$DB->F($opotkaz["id"])." OR t.status_id=".$DB->F($accepted["id"]).")
                                AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($datefrom)))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($dateto))).")
                                 ".$add_filter.";";
                //die($sql);
                list($gt_count, $orient_ammount1) = $DB->getRow($sql);
                $sql_unpaid = "SELECT SUM(tick.orient_price) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id 
                            WHERE t.id IN (SELECT g.task_id FROM `gfx` AS g WHERE g.empl_id = ".$DB->F($res["id"])." GROUP BY g.task_id) 
                                AND t.plugin_uid='services' AND tick.task_type='10'  AND (t.status_id=".$DB->F($doneStatus["id"])." OR t.status_id=".$DB->F($countStatus["id"])." OR t.status_id=".$DB->F($completeStatus["id"])." OR t.status_id=".$DB->F($accepted["id"]).")
                                AND t.id IN (SELECT task_id FROM `task_comments` WHERE 
                                (`status_id`=".$DB->F($doneStatus["id"])." )
                                AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($datefrom)))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($dateto))).")
                                 ".$add_filter.";";
                /*OR `status_id`=".$DB->F($otkaz["id"])." OR `status_id`=".$DB->F($opotkaz["id"])."*/
                $orient_ammount = $DB->getField($sql_unpaid);
                if ($DB->errno()) UIError($DB->error().' '.$sql);
                if ($gt_count) {
                    $total_techs += 1;
                    $rtpl->setCurrentBlock("rep_row");
                    $rtpl->setVariable("FIO", $res["fio"]);
                    $total+=$gt_count;    
                    $rtpl->setVariable("GT_COUNT", $gt_count);
                    
                    
                    $sql_paid = "SELECT distinct(t.id) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                                WHERE t.id IN (SELECT g.task_id FROM `gfx` AS g WHERE g.empl_id = ".$DB->F($res["id"])." GROUP BY g.task_id) 
                                    AND t.plugin_uid='services' AND (t.status_id=".$DB->F($countStatus["id"])." OR t.status_id=".$DB->F($completeStatus["id"])." OR t.status_id=".$DB->F($accepted["id"]).") 
                                    AND tick.task_type='10' 
                                    AND t.id IN (SELECT task_id FROM `task_comments` WHERE ((`status_id`=".$DB->F($accepted["id"])." OR `status_id`=".$DB->F($countStatus["id"]).")
                                                            AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($datefrom)))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($dateto))).")) ".$add_filter; 
                
                
                    $sql = "SELECT count(distinct(t.id)) as cnt FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                                WHERE t.id IN (SELECT g.task_id FROM `gfx` AS g WHERE g.empl_id = ".$DB->F($res["id"])." GROUP BY g.task_id) 
                                    AND t.plugin_uid='services' AND (t.status_id=".$DB->F($doneStatus["id"])." OR t.status_id=".$DB->F($countStatus["id"])." OR t.status_id=".$DB->F($completeStatus["id"])." OR t.status_id=".$DB->F($accepted["id"]).") 
                                    AND tick.task_type='10' 
                                    AND t.id IN (SELECT task_id FROM `task_comments` WHERE `status_id`=".$DB->F($doneStatus["id"])."
                                                            AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($datefrom)))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($dateto))).") ".$add_filter."  
                                    ;";                
                    $sql_notpaid = "SELECT SUM(tick.orient_price) FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                            WHERE 
                                tick.agr_id IN (SELECT `id` FROM `list_contr_agr` WHERE `contr_id`=tick.cnt_id AND `paytype`!=2) AND
                                t.id NOT IN (SELECT `task_id` FROM `skp_money`) AND  t.id IN (SELECT g.task_id FROM `gfx` AS g WHERE g.empl_id = ".$DB->F($res["id"])." GROUP BY g.task_id) 
                                AND t.plugin_uid='services' AND tick.task_type='10'  AND (t.status_id=".$DB->F($doneStatus["id"])." OR t.status_id=".$DB->F($countStatus["id"])." OR t.status_id=".$DB->F($completeStatus["id"])." OR t.status_id=".$DB->F($accepted["id"]).")
                                AND t.id IN (SELECT task_id FROM `task_comments` WHERE 
                                (`status_id`=".$DB->F($doneStatus["id"])." )
                                AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($datefrom)))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($dateto))).")
                                 ".$add_filter.";";
                    list($np_orient_ammount) = $DB->getRow($sql_notpaid);
                    $totalunpaid += $np_orient_ammount;
                    list($dt_count) = $DB->getRow($sql);
                    if ($DB->errno()) UIError($DB->error()." ".$sql);
                    $conversion = round(($dt_count/$gt_count*100), 1);
                    $totalCompleted+=$dt_count;    
                    list($paid_ammount, $average_check, $cnt)= $DB->getRow("SELECT SUM(ammount), AVG(ammount) as paid, COUNT(task_id) FROM `skp_money` WHERE `task_id` IN ($sql_paid) AND `last`=1;");
                    //die("SELECT SUM(ammount), AVG(ammount) as paid, COUNT(task_id) FROM `skp_money` WHERE `task_id` IN ($sql_paid) AND `last`=1;");
                    if ($DB->errno()) UIError($DB->error()." ". "PAID: SELECT SUM(ammount), AVG(ammount) as paid, COUNT(task_id) FROM `skp_money` WHERE `task_id` IN ($sql_paid) AND `last`=1;");
                    $prelim_total += $orient_ammount;
                    $total_paid += $paid_ammount;
                    $rtpl->setVariable("DT_COUNT", $dt_count ? $dt_count : "&mdash;");
                    $rtpl->setVariable("CON_RATE", $conversion."%");
                    $total_conversion += $conversion;
                    $rtpl->setVariable("AVG_PAYCHECK", round($average_check,0) ? round($average_check,0) : 0);
                    $total_avg_check += $average_check;
                    $rtpl->setVariable("PRELIM_TOTAL", $orient_ammount ? number_format(round($orient_ammount,0), 2, ".", " ") : 0);
                    $rtpl->setVariable("FACT_TOTAL", $paid_ammount ? number_format($paid_ammount, 2, ".", " ") : "0.00");
                    $rtpl->setVariable("NONRECIEVED_TOTAL", $np_orient_ammount ?  number_format($np_orient_ammount, 2, ".", " ") : "0.00");
                    $rtpl->setVariable("PAYCHECK", round($paid_ammount*getcfg('tech_skp_pay_coef'), 0));
                        $plan = kontr_plugin::getAgrPlan($_REQUEST["cnt_id"]);
                        if (!$plan) $plan = kontr_plugin::getAgrPlan(35);
                        if ($plan) {
                             
                            $bonus =0;
                            foreach($plan as $item) {
                                if ($conversion>=$item["convval"] ) {
                                    if ($average_check >=$item["midcheck"]) {
                                        $bonus = round($paid_ammount*$item["bonusval"]/100, 0);
                                        $totalBonus += round($bonus, 0);
                                    }
                                }
                            }
                            $rtpl->setVariable("BONUS", $bonus);                            
                            
                        } else {
                            $rtpl->setVariable("BONUS", "0");                            
                        }
                    /*} else {
                    
                        $rtpl->setVariable("BONUS", "&mdash;");
                    }*/
                    $rtpl->setVariable("TOTAL", round($paid_ammount*getcfg('tech_skp_pay_coef')+$bonus, 0));
                    $rtpl->parse("rep_row");
                }
            }
            
            $DB->free();
            $rtpl->setCurrentBlock("rep-total");
            $rtpl->setVariable("TOTAL_GT_COUNT", $total);
            $rtpl->setVariable("TOTAL_DT_COUNT", $totalCompleted);
            $rtpl->setVariable("TOTAL_AVG_CONVERSION", round($total_conversion/$total_techs, 2)."%");
            $rtpl->setVariable("TOTAL_AVG_CHECK", round($total_avg_check/$total_techs, 0));
            $rtpl->setVariable("TOTAL_BONUS", number_format($totalBonus, 0, '.', ' '));
            $rtpl->setVariable("TOTAL_PRELIM_TOTAL", number_format($prelim_total, 0, '.', ' '));
            $rtpl->setVariable("TOTAL_FACT_TOTAL", number_format($total_paid, 0, '.', ' '));
            $rtpl->setVariable("TOTAL_NONRECIEVED_TOTAL", number_format($totalunpaid, 0, ".", " "));
            $rtpl->setVariable("TOTAL_PAYCHECK", number_format(round($total_paid*getcfg('tech_skp_pay_coef'), 0), 0, ".", " "));
            $rtpl->setVariable("TOTAL_BONUS", $totalBonus);
            $rtpl->setVariable("TOTAL_TOTAL", number_format(round($total_paid*getcfg('tech_skp_pay_coef')+$totalBonus, 0), 0, ".", " "));
            $rtpl->parse("rep-total");
            $rtpl->setVariable("REP_CDATE", date("d.m.Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            }
            
            if (isset($_REQUEST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_skp_total__".$_REQUEST["month"]."-".$_REQUEST["year"].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_REQUEST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 
    function acceptMoney() {
        global $DB, $USER;
        $task_id = $_REQUEST["amtask_id"];
        $docnum = $_REQUEST["tdnum"];
        $ammount = $_REQUEST["ammount"];
                   
        if (!$task_id || !$docnum || !!$ammount) {
            $res["error"] = "Ошибка: форма заполнена не полностью";
            
        }
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        
        $sql = "UPDATE `tickets` SET `inmoney`=1 WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] = $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $sql = "UPDATE `tickets` SET `tdocnum`=".$DB->F($docnum)." WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $sql = "UPDATE `tickets` SET `inmoneydate`=".$DB->F(date("Y-m-d H:i:s"))." WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $sql = "UPDATE `skp_money` SET `last`=0 WHERE `task_id`=".$DB->F($task_id).";";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $dd = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `skp_money` (`task_id`, `ammount`, `le`, `user_id`, `last`) VALUES (".$DB->F($task_id).", ".$DB->F(implode("", explode(" ", $ammount))).", ".$DB->F($dd).", ".$DB->F($USER->getId()).", 1);";
        $DB->query($sql);
        $ret["sql"] .= $sql;
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
        $sql = "SELECT `status_id` FROM `tasks` WHERE `id`=".$DB->F($task_id).";";
        $r = $DB->getField($sql);
        if ($DB->error()) $ret["error"] = $DB->error();
        $DB->free();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $t = new ServiceTicket($task_id);
        if ($r != $countStatus["id"]) {
            $ret["newstatus"] = $countStatus["name"];
            $t->addComment("Прием денег. Сумма: ".number_format($ammount, 2, '.', ' '), "Автоматическое обновление статуса на Сдача отчета при приеме денег от техника", $countStatus["id"]);        
        } else
            $t->addComment("Прием денег. Сумма: ".number_format($ammount, 2, '.', ' '), "Обновление принятой от техника суммы", 0);
        $ret["ok"] = "ok";
        $ret["am"] = number_format($ammount, 2, '.', ' ');
        $ret["dn"] = $docnum;
        $ret["dd"] = $dd;
        echo json_encode($ret);
        return false;
    }
}
?>