<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Категории ТМЦ";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Категории ТМЦ";
    $PLUGINS[$plugin_uid]['events']['getOptionsA'] = "Категории ТМЦ (optionList ajax)";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование Категории ТМЦ";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление Категории ТМЦ";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение Категории ТМЦ";
    
    
}
?>