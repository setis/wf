<?php
use classes\HTML_Template_IT;
use classes\Plugin;

class adm_material_cats_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getOptions($id = 0)
    {
        global $DB;
        $sql = "SELECT * FROM `list_material_cats` WHERE 1;";
        $res = $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());

        return array2options($res, $id);
    }

    static function getOptions_nonmetric($id = 0)
    {
        global $DB;
        $sql = "SELECT * FROM `list_material_cats` WHERE `id` IN (SELECT DISTINCT(`cat_id`) FROM `list_materials` WHERE `metric_flag`='0' AND !`is_num`);";
        $res = $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());

        return array2options($res, $id);
    }

    static function getList()
    {
        global $DB;
        $sql = "SELECT * FROM `list_material_cats` WHERE 1;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        $res = [];
        if ($DB->num_rows()) {
            while ($fetch = $DB->fetch(true)) {
                $res[] = $fetch;
            }
        } else {
            $res = false;
        }
        $DB->free();

        return $res;
    }

    static function getById($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_material_cats` WHERE `id`=" . $DB->F($id) . ";";
        $res = $DB->getField($sql);
        if ($DB->errno()) UIError($DB->error());

        return $res ? $res : false;
    }

    static function getOptionsA()
    {
        global $DB;
        $sql = "SELECT * FROM `list_material_cats` WHERE 1;";
        $res = $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());
        echo array2options($res);
    }

    function viewlist()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
        $sql = "SELECT * FROM `list_material_cats` WHERE 1;";
        $DB->query($sql);
        if ($DB->errno())
            UIError($DB->error());

        $data = $DB->getAll($sql);
        foreach ($data as $row) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $row['id']);
            $tpl->setVariable("VLI_TITLE", $row['title']);
            $tpl->parse("vl_item");
        }

        $DB->free();
        UIHeader($tpl);
        $tpl->show();
    }

    function edit()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        if ($cat_id = $_REQUEST['cat_id']) {
            $sql = "SELECT * FROM `list_material_cats` WHERE `id`=" . $DB->F($cat_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Категория ТМЦ с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);

            $tpl->setVariable("CAT_ID", $result["id"]);
            $tpl->setVariable("CAT_TITLE", $result["title"]);

        }
        UIHeader($tpl);
        $tpl->show();

    }

    function save()
    {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = [];

        if (!$_POST['title']) $err[] = "Не заполнено Название Категории ТМЦ";

        if (sizeof($err)) UIError($err);

        if ($_POST['cat_id']) $sql = "UPDATE `list_material_cats` SET `title`=" . $DB->F($_POST['title']) . " WHERE `id`=" . $DB->F($_POST['cat_id']);
        else {
            $sql = "INSERT INTO `list_material_cats` (`title`) VALUES(" . $DB->F($_POST['title']) . ")";
        }
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());

        if (!$_POST['cat_id']) {
            $_POST['cat_id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Категория ТМЦ сохранена. ID: " . $_POST['cat_id']);
    }

    function delete()
    {
        global $DB;
        if (!($id = $_REQUEST['cat_id'])) $err[] = "Не указан Категория ТМЦ";
        if (sizeof($err)) UIError($err);
        if ($this->isBusy($id)) UIError("Выбранная Категория ТМЦ связана с ТМЦ. Удаление невозможно.");
        $DB->query("DELETE FROM `list_material_cats` WHERE `id`='" . $_REQUEST['cat_id'] . "';");
        redirect($this->getLink(), "Категория ТМЦ успешно удалена.");

    }

    function isBusy($id)
    {
        global $DB;
        $DB->query("SELECT `id` FROM `list_materials` WHERE `cat_id`=" . $DB->F($id) . ";");
        if ($DB->num_rows()) $err = true; else $err = false;
        $DB->free();

        return $err;
    }


}

?>
