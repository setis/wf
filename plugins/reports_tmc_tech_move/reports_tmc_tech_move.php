<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;


require_once(dirname(__FILE__)."/../connections/connections.php");

 
class reports_tmc_tech_move_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;

        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__TMC_TECH_MOVE", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date("d.m.Y") . " - 1 month")));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList(@$_POST["sc_id"]));
            $tpl->setVariable("FILTER_TECH_OPTIONS", array2options(adm_empl_plugin::getSCTechs(@$_POST["sc_id"] ? $_POST["sc_id"] : adm_sc_plugin::getScList(false, true)), $_POST["tech_id"]));
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"] || @$_POST["setflag"]) {
                
                $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
                if ($USER->getTemplate() != "default") 
                    $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
                else 
                    $rtpl->loadTemplatefile("report.tmpl.htm");
                if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                    $rtpl->setCurrentBlock("print_head");
                    $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                    $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                    $rtpl->parse("print_head");
                }
                //$rtpl->setVariable("MONTH", $month[$_POST['month']]);
                //$rtpl->setVariable("YEAR", $year[$_POST['year']]);
                $rtpl->setVariable("TECHNAME", adm_empl_plugin::getTechFIO($_POST["tech_id"]));
                $rtpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date("d.m.Y") . " - 1 month")));
                $rtpl->setVariable("DATE_TO", @$_POST['dateto'] ? @$_POST['dateto'] : date("d.m.Y"));
                $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
                $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
                $dateFrom = substr($_POST['datefrom'], 6, 4)."-".substr($_POST['datefrom'], 3, 2). "-".substr($_POST['datefrom'], 0,2);
                $dateTo = substr($_POST['dateto'], 6, 4)."-".substr($_POST['dateto'], 3, 2). "-".substr($_POST['dateto'], 0,2);
                
                $sql = "SELECT tst.*, DATE_FORMAT(tst.ledit, '%H:%i:%s %d.%m.%Y') as opdate, 
                        (SELECT `fio` FROM `users` WHERE `id`=tst.user_id) as username,
                        (SELECT `fio` FROM `users` WHERE `id`=tst.tech_id) as techname,
                        (SELECT `title` FROM `list_material_cats` WHERE `id`=lmc.id) as tmccatname,
                        (SELECT `title` FROM `list_materials` WHERE `id`=lm.id) as tmcname, 
                        tsc.serial as sn                      
                        FROM `tmc_sc_tech` AS tst 
                        LEFT JOIN `tmc_sc` AS tsc ON tsc.id=tst.tmc_sc_id
                        LEFT JOIN `list_materials` AS lm ON lm.id=tsc.tmc_id
                        LEFT JOIN `list_material_cats` AS lmc ON lmc.id=lm.cat_id                        
                        WHERE tst.tech_id=".$DB->F($_POST['tech_id'])." 
                                AND DATE_FORMAT(tst.ledit, '%Y-%m-%d')>=".$DB->F($dateFrom)." 
                                AND DATE_FORMAT(tst.ledit, '%Y-%m-%d')<=".$DB->F($dateTo)." 
                                AND tst.incoming='1' ORDER BY tmccatname, tst.ledit;";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());                
                while ($r = $DB->fetch(true)) {
                    $rtpl->setCurrentBlock("rep_row");
                    $rtpl->setVariable("ROW_DATE", $r["opdate"]);
                    $rtpl->setVariable("ROW_AUTHOR", $r["username"]);
                    $rtpl->setVariable("ROW_TMC_CAT", $r["tmccatname"]);
                    $rtpl->setVariable("ROW_TMC_NAME", $r["tmcname"]);
                    $rtpl->setVariable("ROW_TMC_SN", $r["sn"] ? $r["sn"] : "&mdash;");
                    $rtpl->setVariable("ROW_TMC_COUNT_INC", $r["count"] ? $r["count"] : "0");
                    $rtpl->setVariable("ROW_TMC_COUNT_GONE", "<center>&mdash;</center>");
                    $sql = "SELECT * FROM `tmc_ticket` WHERE `tmc_tech_sc_id`=".$DB->F($r["id"])." LIMIT 1;";
                    $res = $DB->getRow($sql, true);
                    $rtpl->setVariable("ROW_TMC_OPERATION", "Выдано со склада<br /><small>ID транзакции: ".$r["id"]."</small>");
                    $rtpl->setVariable("ROW_TMC_DOC_ID", "&mdash;");
                    $rtpl->setVariable("ROW_RESPONSIBLE", $r["techname"]);
                    $rtpl->parse("rep_row");   
                }
                $DB->free();
                $rtpl->touchBlock("spacer");
                $sql = "SELECT tst.*, DATE_FORMAT(tst.ledit, '%H:%i:%s %d.%m.%Y') as opdate, 
                        (SELECT `fio` FROM `users` WHERE `id`=tst.user_id) as username,
                        (SELECT `fio` FROM `users` WHERE `id`=tst.tech_id) as techname,
                        (SELECT `title` FROM `list_material_cats` WHERE `id`=lmc.id) as tmccatname,
                        (SELECT `title` FROM `list_materials` WHERE `id`=lm.id) as tmcname, 
                        tsc.serial as sn                        
                        FROM `tmc_sc_tech` AS tst 
                        LEFT JOIN `tmc_sc` AS tsc ON tsc.id=tst.tmc_sc_id
                        LEFT JOIN `list_materials` AS lm ON lm.id=tsc.tmc_id
                        LEFT JOIN `list_material_cats` AS lmc ON lmc.id=lm.cat_id                        
                        WHERE tst.tech_id=".$DB->F($_POST['tech_id'])." 
                                AND DATE_FORMAT(tst.ledit, '%Y-%m-%d')>=".$DB->F($dateFrom)." 
                                AND DATE_FORMAT(tst.ledit, '%Y-%m-%d')<=".$DB->F($dateTo)." 
                                AND (tst.incoming=0 OR tst.incoming IS NULL) AND (tst.inplace=0 OR tst.inplace IS NULL) ORDER BY tmccatname, tst.ledit;";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());                
                while ($r = $DB->fetch(true)) {
                    $rtpl->setCurrentBlock("rep_row");
                    $rtpl->setVariable("ROW_DATE", $r["opdate"]);
                    $rtpl->setVariable("ROW_AUTHOR", $r["username"]);
                    $rtpl->setVariable("ROW_TMC_CAT", $r["tmccatname"]);
                    $rtpl->setVariable("ROW_TMC_NAME", $r["tmcname"]);
                    $rtpl->setVariable("ROW_TMC_SN", $r["sn"] ? $r["sn"] : "&mdash;");
                    $rtpl->setVariable("ROW_TMC_COUNT_INC", "<center>&mdash;</center>");
                    $rtpl->setVariable("ROW_TMC_COUNT_GONE", $r["count"] ? $r["count"] : "0");
                    $sql = "SELECT * FROM `tmc_ticket` WHERE `tmc_tech_sc_id`=".$DB->F($r["id"])." LIMIT 1;";
                    $res = $DB->getRow($sql, true);
                    if ($res) {
                        $rtpl->setVariable("ROW_TMC_OPERATION", "Списание по заявке<br /><small>ID транзакции: ".$res['id']."</small>");
                        $rtpl->setVariable("ROW_TMC_DOC_ID", "<a target=\"_blank\" href=\"connections/viewticket/?task_id=".$res["task_id"]."\">".$res["task_id"]."</a>");
                    } else {
                        $rtpl->setVariable("ROW_TMC_OPERATION", "Списание на склад СЦ<br /><small>ID транзакции: ".$r['id']."</small>");
                        $rtpl->setVariable("ROW_TMC_DOC_ID", "&mdash;");                        
                    }
                    $rtpl->setVariable("ROW_RESPONSIBLE", $r["techname"]);
                    $rtpl->parse("rep_row");   
                }
                $DB->free();
                if (isset($_POST["createxlsversion"])) {
                    header("Content-type: application/vnd.ms-excel");  
                    header("Content-disposition: attachment; filename=report_connections_total_".$_POST['month']."-".$year[$_POST['year']].".xls");  
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    if (isset($_POST['createprintversion'])) {
                        $rtpl->touchBlock("print_footer");
                        $rtpl->show();
                        
                        return;
                    } else {
                        $tpl->setCurrentBlock("reportval");
                        $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                        $tpl->parse("reportval");
                    }
                }
            } else {
                $tpl->touchBlock("notcreated");
                $tpl->setVariable("DISABLED", "disabled='disabled'");
            }
        
        
        $tpl->show();
        
    }

}
?>