<?php

/**
 * Plugin Implementation
 * 
 * @author kblp
 * @copyright 2013
 */

use classes\Plugin;



/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 * 
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
 
class adm_clearbase_plugin extends Plugin
{       
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    function cleartaskdb() {
        global $DB;

        die;
        $err = array();
        $sql = "TRUNCATE `tasks`;";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        $sql = "TRUNCATE `task_files`;";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        $sql = "TRUNCATE `task_comments`;";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        $sql = "TRUNCATE `task_users`;";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        $sql = "TRUNCATE `tickets`;";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        $sql = "TRUNCATE `gfx`;";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        $sql = "TRUNCATE `projects`;";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        $sql = "TRUNCATE `bills`;";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        $sql = "TRUNCATE `gp`;";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        $sql = "TRUNCATE `joinedtasks`;";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        $sql = "TRUNCATE `joinedbills`;";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        if (!sizeof($err))
            redirect($this->getLinkStatic("adm_interface"), "Очистка БД заявок выполнена успешно!");
        else
            redirect($this->getLinkStatic("adm_interface"), "Очистка БД заявок завершена с ошибками! Обратитесь к разработчикам! ".implode("\r\n", $err));
    }

}
?>