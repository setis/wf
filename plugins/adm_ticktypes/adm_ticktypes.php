<?php
use classes\HTML_Template_IT;
use classes\Plugin;


class adm_ticktypes_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getOptList($sel_id = false, $tType = false)
    {
        global $DB;
        if ($tType)
            return array2options($DB->getCell2("SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`=" . $DB->F($tType) . " ORDER BY `title`"), $sel_id);
        else
            return array2options($DB->getCell2("SELECT `id`, `title` FROM `task_types` ORDER BY `title`"), $sel_id);

    }

    static function getList($tType = false)
    {
        global $DB;
        if ($tType)
            return $DB->getCell2("SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`=" . $DB->F($tType) . " ORDER BY `title`");
        else
            return $DB->getCell2("SELECT `id`, `title` FROM `task_types` ORDER BY `title`");

    }

    static function getByID($id)
    {
        global $DB;
        if (!$id) return false;
        return $DB->getField("SELECT `title` FROM `task_types` WHERE `id`=" . $DB->F($id) . ";");
    }

    static function getDurationByID($id)
    {
        global $DB;
        if (!$id) return false;
        return $DB->getField("SELECT `duration` FROM `task_types` WHERE `id`=" . $DB->F($id) . ";");
    }

    function viewlist()
    {
        global $DB, $USER, $PLUGINS;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
        $sql = "SELECT * FROM `task_types` ORDER BY `plugin_uid`;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        while (list($id, $title, $desc, $type, $dur) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            if ($id == 37 || $id == 20) {
                $tpl->setVariable("DISABLEDEL", "hidden");
            }
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $hour = floor($dur / 60);
            $min = $dur % 60;
            $tpl->setVariable("VLI_DURATION", ($hour ? $hour . " ч." : "") . ($min ? $min . " мин." : ""));
            $tpl->setVariable("VLI_TYPE", $PLUGINS[$type]["name"]);
            $tpl->setVariable("VLI_DESC", $desc);
            $tpl->parse("vl_item");
        }
        UIHeader($tpl);
        $tpl->show();
    }

    function edit()
    {
        global $DB, $PLUGINS, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        if ($id = $_REQUEST['id']) {
            $sql = "SELECT * FROM `task_types` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Контрагент с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);
            $tpl->setVariable("CURRENTDURATION", $result["duration"]);
            if ($result["id"] == 20 || $result["id"] == 37) {
                $tpl->setVariable("INPUTS_HIDE", "disabled='disabled'");
            }
            $tpl->setVariable("TYPE_ID", $result["id"]);
            $tpl->setVariable("TITLE", $result["title"]);
            $tpl->setVariable("DESC", $result["desc"]);

        } else $tpl->setVariable("CURRENTDURATION", '30');
        $ret = "<select name='plugin_uid'>";
        foreach ($PLUGINS as $uid => $params) {
            $ret .= "<option value='$uid' " . (@$result["plugin_uid"] == $uid ? "selected" : "") . ">" . $params["name"] . "</option>";
        }
        $ret .= "</select>";
        $tpl->setVariable("PLUGIN_LIST", $ret);
        UIHeader($tpl);
        $tpl->show();

    }

    function save()
    {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = array();

        if (!$_POST['title']) $err[] = "Не заполнено Название Вида работ / Типа заявки";

        if (sizeof($err)) UIError($err);

        if ($_POST['type_id']) $sql = "UPDATE `task_types` SET `title`=" . $DB->F($_POST['title']) . ", `desc`=" . $DB->F($_POST['desc']) . ", `plugin_uid`=" . $DB->F($_POST['plugin_uid']) . ", `duration`=" . $DB->F($_POST["duration"]) . " WHERE `id`=" . $DB->F($_POST['type_id']) . ";";
        else {
            $sql = "INSERT INTO `task_types` (`title`, `desc`, `plugin_uid`, `duration`) VALUES(" . $DB->F($_POST['title']) . ", " . $DB->F($_POST['desc']) . ", " . $DB->F($_POST['plugin_uid']) . ", " . $DB->F($_POST["duration"]) . ");";
        }
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error() . "<br />" . $sql);

        if (!$_POST['type_id']) {
            $_POST['type_id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Вид работ / Тип заявки сохранен. ID: " . $_POST['type_id']);
    }

    function delete()
    {
        global $DB;


        if (!($id = $_REQUEST['type_id'])) $err[] = "Не указан Вид работ!";
        if (sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST["type_id"])) UIError("Выбранный Вид работ связан с активными заявками или Техниками. Удаление невозможно.");
        $DB->query("DELETE FROM `task_types` WHERE `id`='" . $_REQUEST['type_id'] . "';");
        redirect($this->getLink(), "Вид работ успешно удален.");

    }

    function isBusy($id)
    {
        global $DB;
        $fc = $DB->getField("SELECT COUNT(*) FROM `list_empl` WHERE `wtype_id`=" . $DB->F($id));
        //die($fc." ".$id);
        if ($fc != 0) {
            $DB->free();
            return true;
        }

        $DB->free();

        return false;
    }

    function getWTypesList($selected = false)
    {
        global $DB;
        $DB->query("SELECT * FROM `task_types` ORDER BY `title`;");
        if ($DB->errno()) return "<strong style='color:#FF0000;'>Ошибка БД!</strong>";
        if (!$DB->num_rows()) return "<strong style='color:#FF0000;'>Список отсутствует!</strong> <a class='link' href='/adm_ticktypes'>Создать список Видов работ</a>";
        $ret = "";
        while (list($id, $title, $desc) = $DB->fetch()) {
            if (count($selected) > 0 && in_array($id, $selected)) {
                $ret .= "<table><tr><td><input type='checkbox' name='wtype-$id' checked='checked' id='wtype-$id'/></td><td><label for='wtype-$id'>$title</label></td></tr></table>";
            } else {
                $ret .= "<table><tr><td><input type='checkbox' name='wtype-$id' id='wtype-$id'/></td><td><label for='wtype-$id'>$title</label></td></tr></table>";
            }
        }

        return $ret;
    }
}
