<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Реестр выполненных работ МГТС B2B";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Реестр выполненных работ МГТС B2B (Подключение B2B)";
    $PLUGINS[$plugin_uid]['events']['downloadphotoreports'] = "Скачивание фотоотчетов по выбранным заявкам";
    
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events'][''] = "";
    
}
?>