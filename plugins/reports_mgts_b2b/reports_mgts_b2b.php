<?php
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\tickets\ConnectionTicket;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class reports_mgts_b2b_plugin extends Plugin
{
    const PARTNER_ID = 36;

    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    /**
     * @param Request $request
     * @return Response
     */
    function main(Request $request)
    {
        global $DB, $USER;
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate() . "/reports_mgts_b2b.tmpl.htm");
            else
                $tpl->loadTemplatefile("reports_contr.tmpl.htm");
            $tpl->setVariable("REPORT_CONTR", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date('Y-m-d') . " - 1 month")));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default")
                $rtpl->loadTemplatefile($USER->getTemplate() . "/report.tmpl.htm");
            else
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
            $rtpl->setVariable("DATETO", $_POST['dateto']);
            $doneStatus = \models\TaskStatus::STATUS_CONNECTION_DONE;
            $closedStatus = \models\TaskStatus::STATUS_CONNECTION_CLOSED;
            $reportStatus = \models\TaskStatus::STATUS_CONNECTION_REPORT;
            $reporttmcStatus = \models\TaskStatus::STATUS_CONNECTION_CALC_TMC;

            $sql = "SELECT 
                        (SELECT count(`file_id`) FROM `task_photos` WHERE `task_id`=tick.task_id) AS isphoto, 
                        if((SELECT `empl_id` FROM `gfx` WHERE `task_id`=tick.task_id LIMIT 1)>0, (SELECT `fio` FROM `users` WHERE id=(SELECT `empl_id` FROM `gfx` WHERE `task_id`=tick.task_id LIMIT 1)), 0) AS tech, 
                        tick.task_id, 
                        (SELECT SUM(CASE WHEN ltc.smeta_val>0 THEN ltc.smeta_val*ltc.price/100 ELSE ltc.price*ltc.quant END) FROM `list_contr_calc` AS ltc WHERE ltc.task_id=tick.task_id) AS sum,
                        tick.mgts_agreement_number,
                        list_addr.full_address
                    FROM `tickets` AS tick 
                        LEFT JOIN `tasks` AS t ON t.id=tick.task_id
                        LEFT JOIN `task_comments` AS tc ON tc.task_id=t.id
                        LEFT JOIN list_addr ON tick.dom_id = list_addr.id
                    WHERE 
                        t.status_id IN (" . $doneStatus . ", " . $closedStatus . ", " . $reportStatus . ", " . $reporttmcStatus . ")
                        AND tick.cnt_id='" . self::PARTNER_ID . "' 
                        AND (tick.task_type='20')
                        AND t.plugin_uid='" . \models\Task::CONNECTION . "'
                        AND (tc.datetime >= " . $DB->F(date("Y-m-d 00:00:00", strtotime($_POST["datefrom"]))) . "
                            AND tc.datetime <= " . $DB->F(date("Y-m-d 23:59:59", strtotime($_POST["dateto"]))) . "
                            AND tc.status_id=" . $DB->F($doneStatus) . ")
                        GROUP BY t.id ORDER BY t.status_id, tc.datetime";

            $DB->query($sql);
            $total = 0;
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $rtpl->setCurrentBlock("rep_row");
                    $t = new ConnectionTicket($r["task_id"]);
                    if ($r["isphoto"] && !isset($_POST["createxlsversion"])) {
                        $rtpl->setCurrentBlock("checkcol-td");
                        $rtpl->setVariable("PP_TASK_ID", $r["task_id"]);
                        $rtpl->parse("checkcol-td");
                    }
                    $rtpl->setVariable("MGTS_ID", $t->getClntTNum());
                    $rtpl->setVariable("MGTS_HREF", link2("connections/viewticket?task_id=" . $r["task_id"], false));
                    $tc = $DB->getField("SELECT `title` FROM `mgts_tc` WHERE `id`=" . $DB->F($t->getMGTS_TC()) . ";");
                    $rtpl->setVariable("MGTS_TC", $tc ? $tc : "не указан");
                    $rtpl->setVariable("MGTS_ATS", $t->getMGTS_ATS() ? $t->getMGTS_ATS() : "не указан");
                    $rtpl->setVariable("MGTS_ORSH", $t->getMGTS_ORSH() ? $t->getMGTS_ORSH() : "не указан");
                    $rtpl->setVariable("MGTS_STATUS_NAME", $t->getStatusName());
                    $rtpl->setVariable("MGTS_COLOR", $t->getStatusColor());
                    $rtpl->setVariable("MGTS_TECH", $r["tech"] ? $r["tech"] : "&mdash;");

                    $r['full_address'] = trim(str_replace([
                        'г Москва',
                        ', д ',
                        'ул',
                        'пр-кт',
                        'пер',
                        'бул',
                        ' стр ',
                        ' к ',
                        ' влд ',
                    ], [
                        '',
                        ', ',
                        'улица',
                        'проспект',
                        'переулок',
                        'бульвар',
                        ' стр. ',
                        ' корп. ',
                        ' влад. ',
                    ], $r['full_address']), ' ,');

                    $rtpl->setVariable("MGTS_ADDR", $r['full_address'] . ' ' . ($t->getKv() ? $t->getKv() : ''));
                    $rtpl->setVariable("MGTS_AGREEMENT_NUMBER", $r['mgts_agreement_number']);

                    $rtpl->setVariable("MGTS_CLNT", $t->getOrgName() ? $t->getOrgName() : $t->getFio());
                    if ($t->getMGTS_SER())
                        $rtpl->setVariable("MGTS_WTYPE", $DB->getField("SELECT `title` FROM `mgts_services` WHERE `id`=" . $DB->F($t->getMGTS_SER()) . ";"));
                    else
                        $rtpl->setVariable("MGTS_WTYPE", "не указан");
                    $total += $r["sum"];
                    $rtpl->setVariable("MGTS_PRICE", number_format((double)$r["sum"], 2, ",", " "));

                    /** @var \models\TmcHolderItem[] $tmcHolderItems */
                    $tmcHolderItems = $this->getEm()->getRepository(\models\TmcHolderItem::class)
                        ->findTaskItems($this->getEm()->getReference(\models\Task::class, $r['task_id']), \WF\Tmc\Model\TmcMoveInterface::STATE_INSTALLED);

                    $tmc_string = '';
                    foreach ($tmcHolderItems as $res) {
                        if (!$res->getNomenclature()->isMetrical()) {
                            $tmc_string .= $res->getNomenclature()->getName();
                            $tmc_string .= ";" . $res->getSerial();
                            $tmc_string .= ";";
                        }
                    }

                    $rtpl->setVariable('TMC_ITEMS', $tmc_string);

                    $rtpl->parse("rep_row");
                }
                $rtpl->setCurrentBlock("rep_row_total");
                $rtpl->setVariable("TOTAL_PRICE", number_format($total, 2, ",", " "));
                $rtpl->parse("rep_row_total");
            } else {
                $rtpl->touchBlock("no-rows");
            }

            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");
                header("Content-disposition: attachment; filename=report_contr_" . $_POST['datefrom'] . "-" . $_POST['dateto'] . ".xls");
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();

                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            if (isset($_POST["getphotos"])) {
                /** @var \WF\Task\PhotoReports\PhotoPacker $packer */
                $packer = $this->getContainer()->get('wf.task.photo_reports.photo_packer');
                $name = 'mgts_b2b_photo_report_' . date('Ymd_hi') . '.zip';

                $packer->buildArchive($name, $request->get('seltask', []));

                $response = new \Symfony\Component\HttpFoundation\BinaryFileResponse($name);
                $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($name) . '";');
                $response->headers->set('Content-Type', 'application/zip');
                $response->sendHeaders();

                $response->deleteFileAfterSend(true);

                return $response;
            } else
                $tpl->touchBlock("notcreated");
        }

        return new Response($tpl->get());
    }

}
