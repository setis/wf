<?php

/**
 * Plugin Header
 * @author kblp
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Адреса";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['events']['main'] = "Управление справочником адресов";
$PLUGINS[$plugin_uid]['events']['selHouseT'] = "Выбор Дома (для заявок)";
$PLUGINS[$plugin_uid]['events']['setODSIdA'] = "Обновление ОДС для адреса (AJAX)";
$PLUGINS[$plugin_uid]['events']['updateAddrA'] = "Обновление параметров адреса (AJAX)";
if(wf::$user->checkAccess($plugin_uid)) {

    $PLUGINS[$plugin_uid]['events']['selRegion'] = "Выбор региона";
    $PLUGINS[$plugin_uid]['events']['selArea'] = "Выбор района";
    $PLUGINS[$plugin_uid]['events']['selCity'] = "Выбор города";
    $PLUGINS[$plugin_uid]['events']['selStreet'] = "Выбор Улицы";
    $PLUGINS[$plugin_uid]['events']['selHouse'] = "Выбор Дома";
    $PLUGINS[$plugin_uid]['events']['selRegionT'] = "Выбор региона (для заявок)";
    $PLUGINS[$plugin_uid]['events']['selAreaT'] = "Выбор района (для заявок)";
    $PLUGINS[$plugin_uid]['events']['selCityT'] = "Выбор города (для заявок)";
    $PLUGINS[$plugin_uid]['events']['selStreetT'] = "Выбор Улицы (для заявок)";

    $PLUGINS[$plugin_uid]['events']['checkCodeHasStreets'] = "Проверка КЛАДР-кода на наличие улиц";
    $PLUGINS[$plugin_uid]['events']['checkCodeHasHouses'] = "Проверка КЛАДР-кода на наличие домов";
    $PLUGINS[$plugin_uid]['events']['checkCodeHasHousesORStreets'] = "Проверка КЛАДР-кода на наличие улиц или домов (для заявок)";
    $PLUGINS[$plugin_uid]['events']['getRegionTitle'] = "Получение адреса по КЛАДР-коду (до населенного пункта)";
    $PLUGINS[$plugin_uid]['events']['getRecByCodeString'] = "Проверка адреса по КЛАДР-коду до улицы, дома, альт. дома";
    $PLUGINS[$plugin_uid]['events']['getRecByCodeStringIDA'] = "Получение адреса по КЛАДР-коду до улицы, дома, альт. дома";
    $PLUGINS[$plugin_uid]['events']['getRecByCodeStringIDA_n'] = "Получение адреса по КЛАДР-коду до улицы, дома, альт. дома (address bundle)";
    $PLUGINS[$plugin_uid]['events']['getRecByCodeStringIDA_GP'] = "Получение адреса по КЛАДР-коду до улицы, дома, альт. дома. Для ГП";
    $PLUGINS[$plugin_uid]['events']['getstreet'] = "Получение списка улиц по указанному региону";
    $PLUGINS[$plugin_uid]['events']['test'] = "Распознавание адресов";




}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование адреса";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение адреса";
    $PLUGINS[$plugin_uid]['events']['saveT'] = "Сохранение адреса из заявок";
    $PLUGINS[$plugin_uid]['events']['deleteaddr'] = "Удаление адреса";
    $PLUGINS[$plugin_uid]['events']['updateAddrInTaskA'] = "Обновление адреса в заявке";
    $PLUGINS[$plugin_uid]['events']['updateAddrInTaskA_n'] = "Обновление адреса в заявке (address bundle)";

    $PLUGINS[$plugin_uid]['events']['insertAddrA'] = "Создание адреса (AJAX)";
    $PLUGINS[$plugin_uid]['events']['insertAddrA_n'] = "Создание адреса (AJAX) (address bundle)";
}
