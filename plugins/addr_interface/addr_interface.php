<?php

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\User;
use GuzzleHttp\Exception\RequestException;
use models\ListAddr;
use models\ListOds;
use models\ListSc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class addr_interface_plugin extends Plugin
{


    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function decodeFullKLADRPartial($code)
    {
        global $DB;

        $region = $code;
        $resArr = array();
        if (mb_strlen($region) < 13) {
            return [];
        }

        $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(mb_substr($region, 0, 2) . "00000000000") . ";";
        $str = $DB->getRow($sql, true);

        $result = false;
        if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result = mb_strtolower($str['socrname']) . " " . $str['name']; else $result = $str['name'] . " " . mb_strtolower($str['socrname']);
        $area = mb_substr($region, 2, 3);

        if ($area != "000") {
            $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(mb_substr($region, 0, 5) . "00000000") . ";";
            $str = $DB->getRow($sql, true);
            if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня")
                $result .= ", " . mb_strtolower($str['socrname']) . " " . $str['name']; else $result .= ", " . $str['name'] . " " . mb_strtolower($str['socrname']);
        }

        $city = mb_substr($region, 5, 3);
        if ($city != "000") {
            $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(mb_substr($region, 0, 8) . "00000") . ";";
            $str = $DB->getRow($sql, true);
            if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня")
                $result .= ", " . mb_strtolower($str['socrname']) . " " . $str['name']; else $result .= ", " . $str['name'] . " " . mb_strtolower($str['socrname']);
        }

        $town = mb_substr($region, 8, 3);
        if ($town != "000") {
            $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(mb_substr($region, 0, 11) . "00") . ";";
            $str = $DB->getRow($sql, true);
            if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня")
                $result .= ", " . mb_strtolower($str['socrname']) . " " . $str['name']; else $result .= ", " . $str['name'] . " " . mb_strtolower($str['socrname']);
        }

        $resArr["city"] = $result;
        if (mb_strlen($region) >= 15) {
            $street = mb_substr($region, 11, 4);

            if ($street != "0000") {
                $sql = "SELECT k.name, s.socrname FROM `kladr_street` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code =" . $DB->F(mb_substr($region, 0, 15) . "00") . ";";
                $str = $DB->getRow($sql, true);
                if ($str['socrname'] == "Улица" || $str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня")
                    $result = mb_strtolower($str['socrname']) . " " . $str['name']; else $result = $str['name'] . ", " . mb_strtolower($str['socrname']);
            }
        }

        $resArr["street"] = $result;

        return $resArr;
    }

    function main()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/addr_interface.tmpl.htm");
        else
            $tpl->loadTemplatefile("addr_interface.tmpl.htm");

        if ($USER->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }
        $region = getcfg('default_region');
        $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 2) . "00000000000") . ";";
        $str = $DB->getRow($sql, true);
        $result = false;
        if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result = strtolower($str['socrname']) . " " . $str['name']; else $result = $str['name'] . " " . strtolower($str['socrname']);
        $area = substr($region, 2, 3);
        if ($area != "000") {
            $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 5) . "00000000") . ";";
            $str = $DB->getRow($sql, true);
            if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result .= " > " . strtolower($str['socrname']) . " " . $str['name']; else $result .= " > " . $str['name'] . " " . strtolower($str['socrname']);
        }
        $city = substr($region, 5, 3);
        if ($city != "000") {
            $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 8) . "00000") . ";";
            $str = $DB->getRow($sql, true);
            if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result .= " > " . strtolower($str['socrname']) . " " . $str['name']; else $result .= " > " . $str['name'] . " " . strtolower($str['socrname']);
        }
        $town = substr($region, 8, 3);
        if ($town != "000") {
            $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 1) . "00") . ";";
            $str = $DB->getRow($sql, true);
            if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result .= " > " . strtolower($str['socrname']) . " " . $str['name']; else $result .= " > " . $str['name'] . " " . strtolower($str['socrname']);
        }
        if (!$result) $result = "Ошибка в файле Конфигурации!";
        $tpl->setVariable("CURRENT_REGION", $result);

        UIHeader($tpl);
        if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getScList")) {
            $sc = new adm_sc_plugin();
            $tpl->setVariable("FILTER_SC", $sc->getScList(@$_POST["filter_sc"]));
        }
        if (class_exists("adm_areas_plugin", true) && method_exists("adm_areas_plugin", "getAreaList")) {
            $area = new adm_areas_plugin();
            $tpl->setVariable("FILTER_AREA", $area->getAreaList(@$_POST["filter_area"]));
        }
        if (class_exists("adm_districts_plugin", true) && method_exists("adm_districts_plugin", "getDisList")) {
            $dis = new adm_districts_plugin();
            $tpl->setVariable("FILTER_DISTRICT", $dis->getDisList(@$_POST["filter_district"]));
        }


        $_POST['filter_start'] = intval($_POST['filter_start']);
        //print_r($_POST);
        //die();
        $currentCode = (isset($_POST["current_kladr_code"]) && $_POST["current_kladr_code"] != "0") ? $_POST["current_kladr_code"] : getcfg('default_region');

        $currentCodeStreet = $currentCode;
        $currentCode = substr($currentCode, 0, 11) . "00";

        $tpl->setVariable("DEFAULT_REGION", $currentCodeStreet);

        $tpl->setVariable("FILTER_K_REGION", $this->getKRegionList($currentCode));
        $tpl->setVariable("FILTER_K_AREA", $this->getKAreaList($currentCode));
        $tpl->setVariable("FILTER_K_CITY", $this->getKCityList($currentCode));
        $tpl->setVariable("FILTER_K_NP", $this->getKNPList($currentCode));
        $tpl->setVariable("FILTER_K_STREET", $this->getStreetList($currentCodeStreet));
        //die($region);

        $filterCode = strlen($currentCode) <= 13 ? substr($currentCode, 0, 11) : substr($currentCode, 0, 15);
        $filter = substr($filterCode, 0, 2);
        if (strlen($_POST["filter_k_area"]) > 1)
            $filter .= substr($filterCode, 2, 3);
        else $filter .= "[0-9]{3}";
        if (strlen($_POST["filter_k_city"]) > 1)
            $filter .= substr($filterCode, 5, 3);
        else $filter .= "[0-9]{3}";
        if (strlen($_POST["filter_k_np"]) > 1)
            $filter .= substr($filterCode, 8, 3);
        else $filter .= "[0-9]{3}";
        if (strlen($_POST["filter_k_street"]) > 1 && strlen($currentCodeStreet) > 11) {
            $filter .= substr($currentCodeStreet, 11, 4) . "[0-9]{2,4}";
        }
        //else $filter.= "[0-9]{4}";

        $filter_sc = $_POST["filter_sc"] != 0 ? "`sc_id`=" . $DB->F($_POST["filter_sc"]) . " AND " : false;
        $filter_area = $_POST["filter_area"] != 0 ? "`area_id`=" . $DB->F($_POST["filter_area"]) . " AND " : false;
        $cCode = $filterCode != '0' ? "`kladrcode` REGEXP " . $DB->F('^' . $filter) . " AND " : false;
        //print_r($_POST);
        $sql = "SELECT
la.*,
(SELECT title FROM list_area lar WHERE lar.id = la.area_id ) area_name,
(SELECT title FROM list_ods lo WHERE lo.id = la.ods_id ) ods_name,
(SELECT title FROM list_sc ls WHERE ls.id = la.sc_id ) sc_name
FROM list_addr la WHERE ";
        $sql .= $cCode ? $cCode : "";
        $sql .= $filter_area ? $filter_area : "";
        $sql .= $filter_sc ? $filter_sc : "";
        if ($filter_sc || $filter_area || $cCode)
            $sql = substr($sql, 0, strlen($sql) - 4);

        $sql .= " LIMIT " . $_POST['filter_start'] . "," . intval(getcfg('rows_per_page')) . ";";
        $DB->query($sql);
        if ($DB->num_rows()) {
            if ($USER->checkAccess("virt_deladdr", User::ACCESS_WRITE)) {
                $tpl->touchBlock("deladdr-head");
            }

            while ($res = $DB->fetch(true)) {
                $addr = $res[11] != '' ? " (" . $res[11] . ")" : "";
                $tpl->setCurrentBlock("row");
                $tpl->setVariable("ADDR_ID", $res['id']);
                $tpl->setVariable("ADDR", $this->StreetByCode($res['kladrcode']) . ", " . $res['name'] . $addr);
                $tpl->setVariable("KLADR", $res['kladrcode']);
                $tpl->setVariable("FLOOR", $res['floorcount']);
                $tpl->setVariable("FLAT", $res['flatcount']);
                $tpl->setVariable("ENTR", $res['entr_count']);
                $tpl->setVariable("SCTITLE", $res['sc_name']);
                $tpl->setVariable("AREA", $res['area_name']);
                $tpl->setVariable("ODS", $res['ods_name']);
                $tpl->setVariable("COMMENT", $res['comments']);
                $tpl->setVariable("HREF", $this->getLink('edit', "id=" . $res['id']));
                if ($USER->checkAccess("virt_deladdr", User::ACCESS_WRITE)) {
                    $tpl->setCurrentBlock("deladdr");
                    $tpl->setVariable("DELETEHREF", $this->getLink('deleteaddr', "id=" . $res['id']));
                    $tpl->parse("deladdr");
                }
                $tpl->parse("row");

            }
        }
        $DB->free();
        $sql1 = str_replace(" LIMIT " . $_POST['filter_start'] . "," . intval(getcfg('rows_per_page')) . ";", "", $sql);
        $DB->query($sql1);
        $total = $DB->num_rows();
        $DB->free();
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_POST['filter_start'], 0, $total, "#start-%s"));

        $DB->free();
        $tpl->show();
    }

    function getKRegionList($sel)
    {
        global $DB;
        $sel = substr($sel, 0, 2) . "00000000000";
        return array2options($DB->getCell2("SELECT k.code, CONCAT(k.name, ' ', s.socrname) FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON k.socr=s.scname WHERE k.code REGEXP '[0-9]{2}[0]{9}[0]{1}[0]{1}' GROUP BY k.code, k.name, s.socrname ORDER BY FIELD(k.name, 'Санкт-Петербург','Московская','Москва', '*') DESC, k.name ASC;"), $sel);
    }

    function getKAreaList($sel)
    {
        global $DB;
        if (!$sel) $region = '[0-9]{2}'; else $region = substr($sel, 0, 2);
        $sel = substr($sel, 0, 5) . "00000000";
        return array2options($DB->getCell2("SELECT k.code, CONCAT(k.name, ' ', s.socrname) FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON k.socr=s.scname WHERE k.code REGEXP '" . $region . "(([1-9]{1}[0-9]{1}[0-9]{1})|([0-9]{1}[1-9]{1}[0-9]{1})|([0-9]{1}[0-9]{1}[1-9]{1}))[0]{6}[0]{1}[0]{1}' GROUP BY k.code, k.name, s.socrname ORDER BY k.name ASC;"), $sel);
    }

    function getKCityList($sel)
    {
        global $DB;
        if (!$sel) return false;
        $area = substr($sel, 2, 3);
        //$area = $area == "000" ? "[0-9]{3}" : $area;
        $region = substr($sel, 0, 2);
        $sel = substr($sel, 0, 8) . "00000";
        return array2options($DB->getCell2("SELECT k.code, CONCAT(k.name, ' ', s.socrname) FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON k.socr=s.scname WHERE k.code REGEXP '" . $region . $area . "(([1-9]{1}[0-9]{1}[0-9]{1})|([0-9]{1}[1-9]{1}[0-9]{1})|([0-9]{1}[0-9]{1}[1-9]{1}))000[0]{1}[0]{1}' GROUP BY k.code, k.name, s.socrname ORDER BY k.name ASC;"), $sel);
    }

    function getKNPList($sel)
    {
        global $DB;
        if (!$sel) return false;
        $code = substr($sel, 0, 8);
        $sel = substr($sel, 0, 11) . "00";
        return array2options($DB->getCell2("SELECT k.code, CONCAT(k.name, ' ', s.socrname) FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON k.socr=s.scname WHERE k.code REGEXP '" . $code . "(([1-9]{1}[0-9]{1}[0-9]{1})|([0-9]{1}[1-9]{1}[0-9]{1})|([0-9]{1}[0-9]{1}[1-9]{1}))[0]{1}[0]{1}' GROUP BY k.code, k.name, s.socrname ORDER BY k.name ASC;"), $sel);
    }

    function getStreetList($sel)
    {
        global $DB;
        $code = substr($sel, 0, 11) . "%";
        $sel = substr($sel, 0, 17);
        return array2options($DB->getCell2("SELECT s.code, CONCAT(s.name, ' ', sc.socrname) FROM `kladr_street` AS s LEFT JOIN `kladr_socrbase` AS sc ON s.socr=sc.scname WHERE `code` LIKE " . $DB->F($code) . " ORDER BY s.name;"), $sel);
    }

    function StreetByCode($code)
    {
        global $DB;
        return $DB->getField("SELECT CONCAT(s.socr, '. ', s.name) FROM `kladr_street` AS s WHERE s.code=" . $DB->F(substr($code, 0, 15) . "00") . ";");
    }

    function deleteaddr()
    {
        global $USER, $DB;
        if (!$USER->checkAccess("virt_deladdr", User::ACCESS_WRITE)) {
            UIError("Вам запрещено выполнять данную операцию!!");
        }
        $id = $_REQUEST["id"];
        if (!$id) {
            UIError("Не указан идентификатор адреса для удаления!");
        }
        if (!$this->isBusy($id)) {
            $DB->query("DELETE FROM `list_addr` WHERE `id`=" . $DB->F($id) . ";");
            if ($DB->errno()) UIError("MySQL: " . $DB->error()); else
                redirect($this->getLink('main'), "Адрес успешно удален! ID: " . $id);

        } else {
            UIError("Данный адрес связан с активными заявками! Пожалуйста, убедитесь, что адрес не связан с заявками и повторите попытку!");
        }
    }

    function isBusy($id)
    {
        global $DB;
        return $DB->getField("SELECT COUNT(`dom_id`) FROM `tickets` WHERE `dom_id`=" . $DB->F($id) . ";");
    }

    function getRegionTitle()
    {

        global $DB;
        $region = $_POST["code"];
        $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 2) . "00000000000") . ";";
        $str = $DB->getRow($sql, true);
        $result = false;
        if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result = strtolower($str['socrname']) . " " . $str['name']; else $result = $str['name'] . " " . strtolower($str['socrname']);
        $area = substr($region, 2, 3);
        if ($area != "000") {
            $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 5) . "00000000") . ";";
            $str = $DB->getRow($sql, true);
            if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result .= " > " . strtolower($str['socrname']) . " " . $str['name']; else $result .= " > " . $str['name'] . " " . strtolower($str['socrname']);
        }
        $city = substr($region, 5, 3);
        if ($city != "000") {
            $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 8) . "00000") . ";";
            $str = $DB->getRow($sql, true);
            if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result .= " > " . strtolower($str['socrname']) . " " . $str['name']; else $result .= " > " . $str['name'] . " " . strtolower($str['socrname']);
        }
        $town = substr($region, 8, 3);
        if ($town != "000") {
            $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 11) . "00") . ";";
            $str = $DB->getRow($sql, true);
            if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result .= " > " . strtolower($str['socrname']) . " " . $str['name']; else $result .= " > " . $str['name'] . " " . strtolower($str['socrname']);
        }

        echo $result;
        $DB->free();
    }

    function getRecByCodeString()
    {
        global $DB;
        $code = $_POST['code'];
        $name = $_POST['name'];
        $altname = $_POST['altname'];
        $r = explode("_", $code);
        if (count($r) == 2) {
            $code = $r[0];
            $name = $r[1];
        } else {
            $name = $altname;
        }

        $sql = "SELECT * FROM `list_addr` WHERE `kladrcode`=" . $DB->F($code) . " AND `name`=" . $DB->F($name);
        if ($altname != '') $sql .= " AND `althouse`=" . $DB->F($altname) . ";"; else $sql .= ';';
        $result = $DB->getRow($sql, true);
        if ($result) {
            echo $result['althouse'] . ","
                . $result['floorcount'] . ","
                . $result['flatcount'] . ","
                . $result['entr_count'] . ","
                . $result['sc_id'] . ","
                . $result['area_id'] . ","
                . $result['ods_id'] . ","
                . $result['id'];
        } else {
            echo "";
        }
        $DB->free();
    }

    function getRecByCodeStringIDA()
    {
        global $DB;
        $code = $_POST['code'];
        $name = $_POST['name'];
        $altname = $_POST['altname'];
        $r = explode("_", $code);
        if (count($r) == 2) {
            $code = $r[0];
            $name = $r[1];
        } else {
            $name = $altname;
        }

        $sql = "SELECT "
            . "id, "
            . "name, "
            . "kladrcode, "
            . "floorcount, "
            . "flatcount, "
            . "entr_count, "
            . "'', "
            . "sc_id, "
            . "'', "
            . "area_id, "
            . "comments, "
            . "althouse, "
            . "ods_id "
            . "FROM `list_addr` "
            . "WHERE `kladrcode`=" . $DB->F($code)
            . " AND `name`=" . $DB->F($name);
        if ($altname != '') $sql .= " AND `althouse`=" . $DB->F($altname) . ";"; else $sql .= ';';

        $result = $DB->getRow($sql, true);
        if ($result) {
            echo implode("^", $result);
        } else {
            echo "none";
        }
    }

    function getRecByCodeStringIDA_n()
    {
        echo "none";
    }

    function getRecByCodeStringIDA_GP()
    {
        global $DB;
        $code = $_POST['code'];
        $name = $_POST['name'];
        $full_address = $_POST['full_address'];


        $sql = "SELECT "
            . "id, "
            . "name, "
            . "kladrcode, "
            . "floorcount, "
            . "flatcount, "
            . "entr_count, "
            . "'', "
            . "sc_id, "
            . "'', "
            . "area_id, "
            . "comments, "
            . "althouse, "
            . "ods_id "
            . "FROM `list_addr` "
            . "WHERE `kladrcode`=" . $DB->F($code)
            . " AND `name`=" . $DB->F($name)
            . " AND `full_address` =" . $DB->F($full_address) . ";";

        $result = $DB->getRow($sql, true);
        if ($result) {
            echo implode("^", $result);
        } else {
            echo "none";
        }
    }

    function StreetByCodeAddrForm($code)
    {
        global $DB;
        return $DB->getField("SELECT CONCAT(s.name, ' ' ,s.socr) FROM `kladr_street` AS s WHERE s.code=" . $DB->F(substr($code, 0, 15) . "00") . ";");
    }

    function selRegion()
    {

        echo "<option value=\"0\">--все</option>" . $this->getKAreaList($_POST['region']);
    }

    function selArea()
    {

        echo "<option value=\"0\">--все</option>" . $this->getKCityList($_POST['region']);
    }

    function selCity()
    {

        echo "<option value=\"0\">--все</option>" . $this->getKNPList($_POST['region']);
    }

    function selStreet()
    {

        echo "<option value=\"0\">--все</option>" . $this->getStreetList($_POST['region']);
    }

    function selHouse()
    {

        echo "<option value=\"0\">-- выберите дом --</option>" . $this->getHouseList($_POST['region']);
    }

    function getHouseList($sel, $rname = null)
    {
        global $DB;
        if (!$sel) return false;
        $code = substr($sel, 0, 15) . "[0-9]{4}";

        //return array2options(, $sel);
        $DB->query("SELECT s.code, s.name FROM `kladr_doma` AS s WHERE `code` REGEXP " . $DB->F($code) . " ORDER BY s.name;");
        if (!$DB->errno() && $DB->num_rows()) {
            $houseList = array();
            while (list($code, $name) = $DB->fetch()) {
                $houseList = explode(",", $name);
                foreach ($houseList as $item) {
                    if ($code == $sel && $item == $rname)
                        $ret[] = $item . " )<option selected value=\"" . $code . "_" . $item . "\">" . $item . "</option>*";
                    else
                        $ret[] = $item . " )<option value=\"" . $code . "_" . $item . "\">" . $item . "</option>*";
                }
            }
            asort($ret);
            $DB->free();
            return implode("*", preg_replace("/^(.*)( \))/", "", $ret));
        } else {
            $DB->free();
            return false;
        }
    }

    function selRegionT()
    {

        echo "<option value=\"\">--все</option>" . $this->getKAreaList($_POST['region']);
    }

    function selAreaT()
    {

        echo "<option value=\"\">--все</option>" . $this->getKCityList($_POST['region']);
    }

    function selCityT()
    {

        echo "<option value=\"\">--все</option>" . $this->getKNPList($_POST['region']);
    }

    function selStreetT()
    {

        echo "<option value=\"\">--все</option>" . $this->getStreetList($_POST['region']);
    }

    function selHouseT()
    {

        echo "<option value=\"\">-- выберите дом --</option><option value=\"0\">-- нет в списке</option>" . $this->getHouseList($_POST['region']);
    }

    function getstreet()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/suggest.tmpl.htm");
        $code = substr($_POST['parent_id'], 0, 11) . "%";
        $sql = "SELECT s.code, s.name, s.socr FROM `kladr_street` AS s WHERE `code` LIKE " . $DB->F($code) . " AND s.name LIKE " . $DB->F(ltrim(rtrim(preg_replace("/ {2,}/", " ", $_POST["q"]))) . "%") . " ORDER BY s.name;";
        $DB->query($sql);
        if (!$DB->errno() && $DB->num_rows()) {
            $addInfo = " <small>(";
            while (list($code, $name, $socr) = $DB->fetch()) {
                $addInfo .= ")</small>";
                $tpl->setCurrentBlock("row");
                $tpl->setVariable("NAME_REAL", $name);
                $tpl->setVariable("NAME", $name);
                $tpl->setVariable("ID", $code);
                $tpl->setVariable("SOCR", $socr);
                $tpl->parse("row");
            }

        } else {
            $tpl->touchBlock("norows");
        }
        $DB->free();
        $tpl->show();
        //echo $sql;
    }

    function checkCodeHasStreets()
    {
        global $DB;
        $l = strlen(@$_POST['code']);
        if ($l == 13) {
            $code = substr($_POST["code"], 0, 11) . "%";
            $sql = "SELECT COUNT(code) FROM `kladr_street` WHERE `code` LIKE " . $DB->F($code) . ";";
            if ($streetCount = $DB->getField($sql)) {

                echo "1";
            } else {
                echo "0";
            }
            $DB->free();
            return;

        } else {
            echo "0";
        }
        $DB->free();
        return false;
    }

    function checkCodeHasHouses()
    {
        global $DB;
        $l = strlen(@$_POST['code']);
        $code = $l == 13 ? substr($_POST["code"], 0, 11) . "0000[0-9]{4}" : substr($_POST["code"], 0, 15) . "[0-9]{4}";
        $sql = "SELECT COUNT(code) FROM `kladr_doma` WHERE `code` REGEXP " . $DB->F($code) . ";";
        if ($houseCount = $DB->getField($sql)) {
            $DB->free();
            echo "1";
        } else {
            echo "0";
        }
        $DB->free();
        return;
        return false;
    }

    function checkCodeHasHousesORStreets()
    {
        global $DB;
        if (@$_POST['code'] != '') {
            $code = substr($_POST["code"], 0, 11) . "[0-9]{4}[0]{1}[0]{1}";
            $sql = "SELECT COUNT(code) FROM `kladr_street` WHERE `code` REGEXP " . $DB->F($code) . ";";
            if ($streetCount = $DB->getField($sql)) {

                echo "1";
            } else {
                echo "0";
            }
            $DB->free();
            return;
            $code = substr($_POST["code"], 0, 11) . "0000[0-9]{4}";
            $sql = "SELECT COUNT(code) FROM `kladr_doma` WHERE `code` REGEXP " . $DB->F($code) . ";";
            if ($houseCount = $DB->getField($sql)) {
                echo "1";
            } else {
                echo "0";
            }
        } else {
            echo "0";
        }
        $DB->free();
        return false;
    }

    function edit()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($id = $_REQUEST['id']) {
            $result = $DB->getRow("SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($id), true);

            if ($DB->errno())
                UIError($DB->error());
            if (empty($result))
                UIError("Адрес с указанным идентификатором отсутствует.");
            $tpl->setVariable("ADDR_ID", $id);
            $tpl->setVariable("FLOOR_CNT", $result['floorcount']);
            $tpl->setVariable("FLAT_CNT", $result['flatcount']);
            $tpl->setVariable("ENTR_CNT", $result['entr_count']);
            $tpl->setVariable("ADDR_DESC", $result['comments']);
            $tpl->setVariable("ALT_HOUSE", $result['althouse']);
        }

        if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getScList")) {
            $sc = new adm_sc_plugin();
            $tpl->setVariable("SC_LIST", $sc->getScList($result['sc_id']));
        }
        if (class_exists("adm_areas_plugin", true) && method_exists("adm_areas_plugin", "getAreaList")) {
            $area = new adm_areas_plugin();
            $tpl->setVariable("AREA_LIST", $area->getAreaList($result['area_id']));
        }
        if (class_exists("adm_ods_plugin", true) && method_exists("adm_ods_plugin", "getODSList")) {
            $ods = new adm_ods_plugin();
            $tpl->setVariable("ODS_LIST", $ods->getODSList($result['ods_id']));
        }

        $currentCode = $result['kladrcode'] ? $result['kladrcode'] : getcfg('default_region');
        $currentCodeStreet = $currentCode;
        $currentCode = substr($currentCode, 0, 11) . "00";
        $cCode = $result['name'] ? $currentCodeStreet . "_" . $result['name'] : $currentCodeStreet;
        $tpl->setVariable("DEFAULT_REGION", $cCode);
        $tpl->setVariable("FILTER_K_REGION", $this->getKRegionList($currentCode));
        $tpl->setVariable("FILTER_K_AREA", $this->getKAreaList($currentCode));
        $tpl->setVariable("FILTER_K_CITY", $this->getKCityList($currentCode));
        $tpl->setVariable("FILTER_K_NP", $this->getKNPList($currentCode));
        $sql = "SELECT name FROM `kladr_street` WHERE `code` = " . $DB->F(substr($currentCodeStreet, 0, 15) . "00") . ";";
        if ($streetName = $DB->getField($sql)) {
            $tpl->setVariable("F_STREET", $streetName);
            $tpl->setVariable("STREET_CODE", substr($currentCodeStreet, 0, 15) . "00");
        }
        //die($sql);
        //$tpl->setVariable("FILTER_K_STREET", $this->getStreetList($currentCodeStreet));
        if ($id)
            $tpl->setVariable("FILTER_K_HOUSE", $this->getHouseList($currentCodeStreet, $result['name']));
        UIHeader($tpl);
        $tpl->show();
        $DB->free();
    }

    public function save(Request $request)
    {
        global $DB, $USER;
        $err = array();
        if (!$_POST['k_house'] && !$_POST['althouse'])
            $err[] = "Не корректно выбран адрес из КЛАДР. (Не указан Дом)";
        if (!$_POST['sc_id'])
            $err[] = "Не выбран Сервисный Центр.";
        if (!$_POST['area_id'])
            $err[] = "Не выбран Район (системный).";
        //if(!$_POST['ods_id']) $err[] = "Не выбрана ОДС.";
        $house = explode("_", $_POST['current_kladr_code']);
        if (count($house) != 2) {
            $h = $_POST['current_kladr_code'];
            if (strlen($h) > 2) {
                $house[0] = $h;
                $house[1] = $_POST["althouse"];
            } else {
                $err[] = "Ошибка! Обратитесь к разработчикам!";
            }
        }
        if (sizeof($err)) UIError($err);

        $em = $this->getEm();
        $addrId = $request->get('addr_id');
        $odsId = $request->get('ods_id');
        if ($addrId && null !== $listAddr = $em->find(ListAddr::class, $addrId)) {
            /** @var ListAddr $listAddr */
            $listAddr->setName($house[1])
                ->setKladrcode($house[0])
                ->setFloorcount($request->get('floors'))
                ->setFlatcount($request->get('flats'))
                ->setEntrCount($request->get('entr'))
                ->setServiceCenter($em->getReference(ListSc::class, $request->get('sc_id')))
                ->setAreaId($request->get('area_id'))
                ->setComments($request->get('comment'))
                ->setAlthouse($request->get('althouse'))
                ->setOds($odsId ? wf::$em->find(\models\ListOds::class, $odsId) : null);
            $em->persist($listAddr);
            $em->flush();
        } else {
            $listAddr = new ListAddr();
            $listAddr->setName($house[1])
                ->setKladrcode($house[0])
                ->setFloorcount($request->get('floors'))
                ->setFlatcount($request->get('flats'))
                ->setEntrCount($request->get('entr'))
                ->setServiceCenter($em->getReference(ListSc::class, $request->get('sc_id')))
                ->setAreaId($request->get('area_id'))
                ->setComments($request->get('comment'))
                ->setAlthouse($request->get('althouse'))
                ->setOds($odsId ? wf::$em->find(\models\ListOds::class, $odsId) : null);
            $em->persist($listAddr);
            $em->flush();
        }

        redirect($this->getLink('edit') . "?id=" . $listAddr->getId(), "Адрес сохранен. ID: " . $_POST["addr_id"]);
    }

    public function saveT(Request $request)
    {
        $em = \wf::$em;

        $coordinates = null;
        if ($request->get('latitude') && $request->get('longitude')) {
            $coordinates = new \CrEOF\Spatial\PHP\Types\Geometry\Point($request->get('longitude'), $request->get('latitude'));
        }

        $addr = (new \models\ListAddr())
            ->setName($request->request->get('house'))
            ->setKladrcode($request->request->get('kladr'))
            ->setFloorcount($request->request->getInt("floors"))
            ->setFlatcount($request->request->getInt("flats"))
            ->setEntrCount($request->request->getInt("entr"))
            ->setFullAddress($request->request->get('full_address'))
            ->setCoordinates($coordinates)
            ->setIsNotRecognized($request->get('is_not_recognised', false))
            ->setAreaId($request->get('area_id'))
//            ->setServiceCenter($em->getReference(ListSc::class, $request->get('sc_id')))
//            ->setComments($request->request->get("comment"))
//            ->setAlthouse($request->request->get("althouse"))
//            ->setOds($odsId ? wf::$em->find(\models\ListOds::class, $odsId) : null)
        ;

        $em->persist($addr);
        $em->flush();

        return $addr->getId();
    }

    function getODSId($addr_id)
    {
        global $DB;
        $sql = "SELECT `ods_id` FROM `list_addr` WHERE `id`=" . $DB->F($addr_id) . ";";
        return $DB->getField($sql);
    }

    function setODSIdA()
    {
        global $DB;
        $ods_id = $_POST["ods_id"];
        $dom_id = $_POST["dom_id"];
        if (!$ods_id || !$dom_id) {
            echo "error";
            return;
        }
        $DB->query("UPDATE `list_addr` SET `ods_id`=" . $DB->F($ods_id) . " WHERE `id`=" . $DB->F($dom_id) . ";");
        if ($DB->errno()) echo "error";
        else echo "ok";
        return false;
    }

    function updateAddrA()
    {
        global $DB;
        $dom_id = $_POST['dom_id'];
        $entr = $_POST['entr'];
        $floor = $_POST['floor'];
        $flat = $_POST['flat'];
        if (!$dom_id) {
            echo "error";
            return;
        }
        $DB->query("UPDATE `list_addr` SET `floorcount`=" . $DB->F($floor) . ",`flatcount`=" . $DB->F($flat) . ",`entr_count`=" . $DB->F($entr) . " WHERE `id`=" . $DB->F($dom_id) . " ;");
        if ($DB->errno()) {
            echo "error";
            return;
        } else echo "ok";
        return;
    }

    function insertAddrA(Request $request)
    {
        $err = array();
        $house = explode("_", $request->get('current_kladr_code'));
        $house_name = '';
        if (count($house) > 2) {
            $kladr = $house[0];
            for ($i = 1; $i < count($house); $i++) {
                $house_name .= $house[$i];
            }
        } elseif (count($house) != 2) {
            $h = $request->get('current_kladr_code');
            if (strlen($h) > 2) {
                $kladr = $h;
                $house_name = $request->get('althouse');
            } else {
                $err[] = "Ошибка! Обратитесь к разработчикам!";
            }
        } else {
            $kladr = $house[0];
            $house_name = $house[1];
        }

        if (sizeof($err)) {
            echo "error";
            return;
        }

        $em = $this->getEm();
        $listAddr = new \models\ListAddr();
        $listAddr->setName($house_name)
            ->setKladrcode($kladr)
            ->setFloorcount($request->get('floors'))
            ->setFlatcount($request->get('flats'))
            ->setEntrCount($request->get('entr'))
            ->setAreaId($request->get('areaid'))
            ->setComments($request->get('comment'))
            ->setAlthouse($request->get('althouse'))
            ->setOds((null === $odsId = $request->get('ods_id')) ? null : $em->getReference(ListOds::class, $odsId));

        $scId = $request->get('sc_id');
        if (!empty($scId)) {
            $listAddr->setServiceCenter($em->getReference(ListSc::class, $scId));
        }

        $em->persist($listAddr);
        $em->flush();

        echo $listAddr->getId();
    }

    function insertAddrA_n(Request $request)
    {
        $address =  $this->getEm()->getRepository(ListAddr::class)
            ->setAddressProvider($this->getContainer()->get('wf.address.provider'))
            ->findOrCreateByAddressArray([
                'city' => $request->get('address')
            ], [
                'count' => 1
            ]);

        return $address->getId();
    }

    /**
     * @param $code
     * @param $name
     * @param $fullAddress
     * @return bool|int
     */
    public static function checkExist($code, $name, $fullAddress)
    {
        $em = \wf::$em;
        try {
            $model = $em->getRepository(ListAddr::class)
                ->findOneBy([
                    'name' => $name,
                    'fullAddress' => $fullAddress,
                    'kladrcode' => $code
                ]);
        } catch (Exception $e) {
            return false;
        }

        if (null === $model) {
            return false;
        }

        return $model->getId();
    }

    /**
     * @deprecated JN remove this method after TM refactoring
     */
    function updateAddrInTaskA()
    {
        global $DB;
        $task_id = $_POST["task_id"];
        $dom_id = $_POST["dom_id"];
        $pod = $_POST["pod"];
        $etazh = $_POST["etazh"];
        $kv = $_POST["kv"];
        $domofon = $_POST["domofon"];
        $sc_id = $_POST["sc_id"];
        $area_id = $_POST["area_id"];
        if (!$task_id || !$dom_id) {
            echo "error";
            return;
        }
        $sql = "UPDATE `list_addr` SET `sc_id`=" . $DB->F($sc_id) . ", `area_id`=" . $DB->F($area_id) . " WHERE `id`=" . $DB->F($dom_id) . ";";

        $DB->query($sql);
        if ($DB->errno()) {
            echo "error";
            return;
        }


        $addsql = "";
        $currentSC = $DB->getField("SELECT `sc_id` FROM `tickets` WHERE `task_id`=" . $DB->F($task_id) . ";");
        if (!$currentSC) {
            $addsql = ", `sc_id`=" . $DB->F($sc_id) . " ";
        }

        $sql = "UPDATE `tickets` SET `dom_id`=" . $DB->F($dom_id) . ", `pod`=" . $DB->F($pod) . ", `etazh`=" . $DB->F($etazh) . ", `kv`=" . $DB->F($kv) . ",
        `domofon`=" . $DB->F($domofon) . " $addsql WHERE `task_id`=" . $DB->F($task_id) . ";";

        $DB->query($sql);
        if ($DB->errno()) {
            echo "error";
        } else {
            echo "ok";
        }
        return;
    }

    function updateAddrInTaskA_n(Request $request)
    {
        $is_not_recognized = $request->request->getBoolean('is_not_recognized');
        $task_id = $request->get('task_id');
        $dom_id = $request->get('dom_id');

        if (null === $task_id || null === $dom_id) {
            return new Response('error');
        }

        $listAddr = $this->getEm()->find(ListAddr::class, $dom_id);
        $listAddr->setIsNotRecognized($is_not_recognized);

        $this->getEm()->persist($listAddr);

        $ticket = $this->getEm()->find(\models\Ticket::class, $task_id);
        $ticket->setDom($listAddr)
            ->setPod($request->get('pod'))
            ->setEtazh($request->get('etazh'))
            ->setKv($request->get('kv'))
            ->setDomofon($request->get('domofon'));

        $this->getEm()->persist($ticket);

        $this->getEm()->flush();

        return new Response('ok');
    }

    function test()
    {
        set_time_limit(1000);
        global $DB;
        $sql = "SELECT * FROM `_temp_nbn_addr` ORDER BY `slot_key` DESC";
        $DB->query($sql);
        $i = 0;
        $failed = 0;
        while ($res = $DB->fetch(true)) {
            $i += 1;
            echo $res['city_name'] . " " . $res['street_name'] . " <strong style='color:#FF0000'>" . $res['building_name'] . "</strong> -> ";
            $dom_id = $this->getKLADRCodeByAddressNBN($res);
            if ($dom_id) {
                $result = $this->getById($dom_id);
                if ($result) {
                    echo " " . self::decodeFullKLADR($result["kladrcode"]) . " <strong style='color:#FF0000'>" . $result["name"] . "</strong>  <strong>" . $result["kladrcode"] . "</strong>   <strong>" . $dom_id . "</strong> ";
                } else {
                    echo " <strong>Адрес не определен</strong>";
                    $failed += 1;
                }
            } else {
                echo " <strong>Адрес не определен</strong>";
                $failed += 1;
            }
            echo "<br />";
        }
        echo "<br /><strong>$i</strong>";
        echo "<br />Адрес не определен: <strong>$failed</strong>";
        $DB->free();


    }

    /**
     * @param $data $data['city_name'], $data['street_name'], $data['building_name']
     * @return bool
     */
    static function getKLADRCodeByAddressNBN($data)
    {
        $building = str_replace(
            [' ', ',', 'корп.', 'стр.', 'д.', '-'],
            ['', '', 'к', 'стр', '', 'к',], $data["building_name"]);

        /* @var $addrProvider WF\Address\AddressProviderInterface */
        $addrProvider = wf::$container->get('wf.address.provider');

        $fullAddress = sprintf('%s, %s, %s', $data['city_name'], $data['street_name'], $data['building_name']);
        try {
            $addr = $addrProvider->find($fullAddress);
        } catch (RequestException $e) {
            \wf::$logger->alert('Dadata request exception!', [
                'exception' => json_decode($e, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT),
                'fullAddress' => $fullAddress,
            ]);
            return false;
        } finally {
            if (empty($addr) || count($addr) === 0) {
                return false;
            }
        }

        $streetCode = substr($addr[0]->getCode('kladr'), 0, 15);
        if (empty($streetCode)) {
            return false;
        }

        $em = \wf::$em;

        $sql = 'SELECT code FROM kladr_doma WHERE code LIKE :code AND (name LIKE :v1 OR name LIKE :v2 OR name LIKE :v3)';
        $houseCode = $em->getConnection()
            ->fetchColumn($sql, [
                'code' => $streetCode . '%',
                'v1' => $building . ',%',
                'v2' => '%,' . $building . ',%',
                'v3' => '%,' . $building,
            ]);

        $comment = 'Адрес, распознанный системой при получении заявки от Оператора.';

        if ($houseCode) {
            $sql = "SELECT id FROM list_addr WHERE kladrcode = :code AND name = :name";

            $dom_id = $em->getConnection()
                ->fetchColumn($sql, [
                    'code' => $houseCode,
                    'name' => $building,
                ]);

            if ($dom_id) {
                return $dom_id;
            }

            $listAddr = new \models\ListAddr();
            $listAddr->setName($building)
                ->setKladrcode($houseCode)
                ->setComments($comment);
            $em->persist($listAddr);
            $em->flush();

            return $listAddr->getId();
        }

        $sql = "SELECT id FROM list_addr WHERE kladrcode = :code AND althouse LIKE :house";
        $dom_id = $em->getConnection()
            ->fetchColumn($sql, [
                'code' => $streetCode . "00",
                'house' => "%" . $building . "%",
            ]);

        if ($dom_id) {
            return $dom_id;
        }
        return false;
    }

    function getById($addr_id)
    {
        global $DB;
        $sql = "SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($addr_id) . ";";
        return $DB->getRow($sql, true);
    }

    static function decodeFullKLADR($code)
    {

        global $DB;

        $region = $code;
        $result = false;
        if (strlen($region) >= 13) {
            $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 2) . "00000000000") . ";";
            $str = $DB->getRow($sql, true);
            $result = false;
            if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result = strtolower($str['socrname']) . " " . $str['name']; else $result = $str['name'] . " " . strtolower($str['socrname']);
            $area = substr($region, 2, 3);
            if ($area != "000") {
                $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 5) . "00000000") . ";";
                $str = $DB->getRow($sql, true);
                if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result .= ", " . strtolower($str['socrname']) . " " . $str['name']; else $result .= ", " . $str['name'] . " " . strtolower($str['socrname']);
            }
            $city = substr($region, 5, 3);
            if ($city != "000") {
                $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 8) . "00000") . ";";
                $str = $DB->getRow($sql, true);
                if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result .= ", " . strtolower($str['socrname']) . " " . $str['name']; else $result .= ", " . $str['name'] . " " . strtolower($str['socrname']);
            }
            $town = substr($region, 8, 3);
            if ($town != "000") {
                $sql = "SELECT k.name, s.socrname FROM `kladr_kladr` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 11) . "00") . ";";
                $str = $DB->getRow($sql, true);
                if ($str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result .= ", " . strtolower($str['socrname']) . " " . $str['name']; else $result .= ", " . $str['name'] . " " . strtolower($str['socrname']);
            }
            if (strlen($region) >= 15) {
                $street = substr($region, 11, 4);
                if ($street != "0000") {
                    $sql = "SELECT k.name, s.socrname FROM `kladr_street` AS k LEFT JOIN `kladr_socrbase` AS s ON s.scname=k.socr WHERE k.code=" . $DB->F(substr($region, 0, 15) . "00") . ";";
                    $str = $DB->getRow($sql, true);
                    if ($str['socrname'] == "Улица" || $str['socrname'] == "Город" || $str['socrname'] == "Поселок" || $str['socrname'] == "Деревня") $result .= ", " . strtolower($str['socrname']) . " " . $str['name']; else $result .= ", " . $str['name'] . " " . strtolower($str['socrname']);
                }
            }
        }
        //$DB->free();
        return $result;

    }

}
