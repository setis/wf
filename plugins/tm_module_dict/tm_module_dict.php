<?php

/**
 * Plugin Implementation
 * 
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;



/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 * 
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
 
class tm_module_dict_plugin extends Plugin
{       
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
       
    function viewlist()
    {
        global $DB, $USER, $PLUGINS;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default") 
            $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
        else 
            $tpl->loadTemplatefile($this->getUID().".tmpl.htm");

        //// 1. Каналы продаж
        $sql = "select id, channel_name from sales_channels order by channel_name";
        #echo $sql."<br/>\n";
        $DB->query($sql);
        if ($DB->errno()) UIError();
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $tpl->setCurrentBlock("channel_item");
                $tpl->setVariable("CHANNEL_ID", $r["id"]);
                $tpl->setVariable("CHANNEL_NAME", $r["channel_name"]);
                $tpl->parse("channel_item");
            }
        }
        #exit;

        //// 2. Подтипы заявок
        $sql = "select id,subtype_name from service_subtypes";
        #echo $sql."<br/>\n";
        $DB->query($sql);
        if ($DB->errno()) UIError();
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $tpl->setCurrentBlock("subtype_item");
                $tpl->setVariable("SUBTYPE_ID", $r["id"]);
                $tpl->setVariable("SUBTYPE_NAME", $r["subtype_name"]);
                $tpl->parse("subtype_item");
            }
        }

        //// 3. Продаваемые услуги
        $sql = "select id,service_name from services_for_sale";
        #echo $sql."<br/>\n";
        $DB->query($sql);
        if ($DB->errno()) UIError();
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $tpl->setCurrentBlock("serv_item");
                $tpl->setVariable("SERV_ID", $r["id"]);
                $tpl->setVariable("SERV_NAME", $r["service_name"]);
                $tpl->parse("serv_item");
            }
        }

        UIHeader($tpl);
        $tpl->show();
    }
    
    function edit() {
        global $DB, $USER, $PLUGINS;
        $type_id = intval($_REQUEST['type']);
        $id = intval($_REQUEST['id']);
        $action = $_REQUEST['action'];
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        if ($USER->getTemplate() != "default") 
            $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
        else 
            $tpl->loadTemplatefile("edit.tmpl.htm");

        $title = "";
        $action = "add";
        if ($type_id == 1) {
            $tpl->setVariable("DICT_NAME", "Каналы продаж");
            if ($id > 0) {
                $sql = "select channel_name from sales_channels where id = ".$id;
                $ret = $DB->getCell($sql);
                $title = $ret[0];
                $action = "edit";
            }
        }
        if ($type_id == 2) {
            $tpl->setVariable("DICT_NAME", "Подтипы заявки");
            if ($id > 0) {
                $sql = "select subtype_name from service_subtypes where id = ".$id;
                $ret = $DB->getCell($sql);
                $title = $ret[0];
                $action = "edit";
            }
        }
        if ($type_id == 3) {
            $tpl->setVariable("DICT_NAME", "Продаваемые услуги");
            if ($id > 0) {
                $sql = "select service_name from services_for_sale where id = ".$id;
                $ret = $DB->getCell($sql);
                $title = $ret[0];
                $action = "edit";
            }
        }
        $tpl->setVariable("TITLE", $title);
        $tpl->setVariable("ACTION", $action);
        $tpl->setVariable("ID", $id);
        $tpl->setVariable("TYPE_ID", $type_id);
        UIHeader($tpl);
        $tpl->show();
    }
    
    function save() {
        global $DB, $USER;
        $type_id = intval($_REQUEST['type_id']);
        $id = intval($_REQUEST['id']);
        $action = $_REQUEST['action'];
        #echo "<pre>".print_r($_REQUEST,true)."</pre>\n";
        #exit;
        if ($action == "edit") {
            $sql = "";
            if ($type_id == 1) {
                $sql = "update sales_channels set channel_name = ".$DB->F($_REQUEST['title'])." where id = ".$id;
                $DB->query($sql);
            }
            if ($type_id == 2) {
                $sql = "update service_subtypes set subtype_name = ".$DB->F($_REQUEST['title'])." where id = ".$id;
                $DB->query($sql);
            }
            if ($type_id == 3) {
                $sql = "update services_for_sale set service_name = ".$DB->F($_REQUEST['title'])." where id = ".$id;
                #echo $sql."<br/>\n";
                #exit;
                $DB->query($sql);
            }
        }
        if ($action == "add") {
            $sql = "";
            if ($type_id == 1) {
                $sql = "insert into sales_channels (channel_name) values (".$DB->F($_REQUEST['title']).")";
                $DB->query($sql);
            }
            if ($type_id == 2) {
                $sql = "insert into service_subtypes (subtype_name) values (".$DB->F($_REQUEST['title']).")";
                $DB->query($sql);
            }
            if ($type_id == 3) {
                $sql = "insert into services_for_sale (service_name) values (".$DB->F($_REQUEST['title']).")";
                $DB->query($sql);
            }
        }
        redirect($this->getLink('viewlist'), "Изменения сохранены");
    }
    
    function delete() {
        global $DB;
        $type_id = intval($_REQUEST['type']);
        $id = intval($_REQUEST['id']);
        $action = $_REQUEST['action'];
        if (($action == "delete") && ($id > 0)) {
            if ($type_id == 1) {
                $sql = "delete from sales_channels where id = ".$id;
                $DB->query($sql);
            }
            if ($type_id == 2) {
                $sql = "delete from service_subtypes where id = ".$id;
                $DB->query($sql);
            }
            if ($type_id == 3) {
                $sql = "delete from services_for_sale where id = ".$id;
                $DB->query($sql);
            }
       }
       redirect($this->getLink('viewlist'), "Изменения сохранены");
    }
}
?>
