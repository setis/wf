<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
class adm_ods_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getODSById($id)
    {
        global $DB;
        $ods = $DB->getRow("SELECT * FROM `list_ods` WHERE `id`=" . $DB->F($id) . ";", true);

        return $ods ? $ods : false;
    }

    function getODSTitle($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_ods` WHERE `id`=" . $DB->F($id) . ";";

        return $DB->getField($sql) ? $DB->getField($sql) : "<center>-</center>";

    }

    function getOptList($sel_id = false)
    {
        global $DB;

        return array2options($DB->getCell2("SELECT `id`, `title` FROM `list_ods` ORDER BY `title`"), $sel_id);
    }

    function getList()
    {
        global $DB;

        return $DB->getCell2("SELECT `id`, `title` FROM `list_ods` ORDER BY `title`");
    }

    function getByID($id)
    {
        global $DB;
        if (!$id) return false;

        return $DB->getRow("SELECT * FROM `list_ods` WHERE `id`=" . $DB->F($id) . ";", true);
    }


    function viewlist()
    {
        global $DB, $USER, $PLUGINS;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));

        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
        $sql = "SELECT * FROM `list_ods` ORDER BY `title`;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        while (list($id, $title, $fio, $tel, $addr, $comment) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->setVariable("VLI_CONTACT", $fio);
            $tpl->setVariable("VLI_TEL", $tel);
            $tpl->setVariable("VLI_ADDR", $addr);
            $tpl->setVariable("VLI_COMMENT", $comment);
            $tpl->parse("vl_item");
        }
        UIHeader($tpl);
        $tpl->show();
    }

    function edit()
    {
        global $DB, $USER, $PLUGINS;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        if ($id = $_REQUEST['id']) {
            $sql = "SELECT * FROM `list_ods` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Запись с таким идентификатором отсутствует.");
            $result = $DB->fetch();
            $tpl->setVariable("ODS_ID", $result[0]);
            $tpl->setVariable("ODS_TITLE", $result[1]);
            $tpl->setVariable("ODS_CONTACT", $result[2]);
            $tpl->setVariable("ODS_TEL", $result[3]);
            $tpl->setVariable("ODS_ADDR", $result[4]);
            $tpl->setVariable("ODS_COMMENT", $result[5]);
        }
        UIHeader($tpl);
        $tpl->show();

    }

    function save()
    {
        global $DB, $USER;
        //Debugger::dump($_POST, true);
        $err = [];
        if (!$_POST['title']) $err[] = "Не заполнено поле Название причины отказа";
        if (sizeof($err)) UIError($err);
        if ($_POST['id'])
            $sql = "UPDATE `list_ods` SET `title`=" . $DB->F($_POST['title']) . ", `comment`=" . $DB->F($_POST['comment']) . ", `fio`=" . $DB->F($_POST['fio']) . ", `tel`=" . $DB->F($_POST['tel']) . ", `address`=" . $DB->F($_POST['address']) . " WHERE `id`=" . $DB->F($_POST['id']) . ";";
        else
            $sql = "INSERT INTO `list_ods` (`title`, `fio`, `tel`, `address`, `comment`) VALUES(" . $DB->F($_POST['title']) . ", " . $DB->F($_POST['fio']) . ", " . $DB->F($_POST['tel']) . ", " . $DB->F($_POST['address']) . ", " . $DB->F($_POST['comment']) . ")";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error() . "<br />" . $sql);

        if (!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "ОДС сохранена. ID: " . $_POST['id']);
    }

    function delete()
    {
        global $DB;
        if (!($id = $_REQUEST['id'])) $err[] = "Не указан идентификатор ОДС!";
        if (sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST["id"])) UIError("Выбранная запись связана с Адресами. Удаление невозможно.");
        $DB->query("DELETE FROM `list_ods` WHERE `id`='" . $_REQUEST['id'] . "';");
        redirect($this->getLink(), "Запись ОДС успешно удалена.");
    }

    function isBusy($id)
    {
        global $DB;

        if ($DB->getField("SELECT COUNT(*) FROM `list_addr` WHERE `ods_id`=" . $DB->F($id))) {
            $DB->free();

            return true;
        }

        $DB->free();

        return false;
    }

    function getODSList($selected = false)
    {
        global $DB;
        $DB->query("SELECT * FROM `list_ods` ORDER BY `title`;");
        if ($DB->errno()) UIError($DB->error());
        $ret = false;
        while (list($id, $title, $fio, $tel, $address) = $DB->fetch()) {
            if ($id == $selected)
                $ret .= "<option selected='selected' value='$id' >$title</option>";
            else
                $ret .= "<option value='$id'>$title</option>";
        }
        $DB->free();

        return $ret;
    }

    function getODSListA()
    {
        global $DB;

        $DB->query("SELECT * FROM `list_ods` ORDER BY `title`;");
        if ($DB->errno()) {
            echo "error";

            return;
        };
        $ret = false;
        $selected = @$_POST["ods_id"] ? $_POST["ods_id"] : 0;
        while (list($id, $title, $fio, $tel, $address) = $DB->fetch()) {
            if ($id == $selected)
                $ret .= "<option selected='selected' value='$id' >$title</option>";
            else
                $ret .= "<option value='$id'>$title</option>";
        }
        $DB->free();
        echo $ret;
    }

    function getODSParams()
    {
        global $DB;

        if ($id = $_POST["ods_id"]) {
            $ods = $DB->getRow("SELECT * FROM `list_ods` WHERE `id`=" . $DB->F($id) . ";", true);
            if ($ods) {
                echo implode("^", $ods);
            } else {
                echo "error";
            }
        } else {
            echo "error";
        }
    }

    function setODSParamsA()
    {
        global $DB;

        $ods_id = $_POST["ods_id"];
        $ods_name = $_POST["ods_name"];
        $ods_addr = $_POST["ods_addr"];
        $ods_tel = $_POST["ods_tel"];
        $ods_contact = $_POST["ods_contact"];
        $ods_comment = $_POST["ods_comment"];
        if (!$ods_id || !$ods_name || !$ods_addr || !$ods_tel) {
            echo "notfilled";

            return;
        } else {
            $DB->query("UPDATE `list_ods` SET `title`=" . $DB->F($ods_name) . ", `fio`=" . $DB->F($ods_contact) . ", `tel`=" . $DB->F($ods_tel) . ", `address`=" . $DB->F($ods_addr) . ", `comment`=" . $DB->F($ods_comment) . "  WHERE `id`=" . $DB->F($ods_id) . ";");
            if (!$DB->errno()) {
                echo "ok";

                return;
            } else {
                echo "error";

                return;
            }
        }
    }

    function saveA()
    {
        global $DB, $USER;
        $err = [];
        if (!$_POST['new_ods_name']) $err[] = " ";
        if (!$_POST['new_ods_addr']) $err[] = " ";
        if (!$_POST['new_ods_tel']) $err[] = " ";
        if (sizeof($err)) {
            echo "notfilled";

            return;
        }
        $sql = "INSERT INTO `list_ods` (`title`, `fio`, `tel`, `address`, `comment`) VALUES (" . $DB->F($_POST['new_ods_name']) . ", " . $DB->F($_POST['new_ods_contact']) . ", " . $DB->F($_POST['new_ods_tel']) . ", " . $DB->F($_POST['new_ods_addr']) . ", " . $DB->F($_POST['new_ods_comment']) . ")";
        $DB->query($sql);
        if ($DB->errno()) {
            echo "error";

            return;
        }
        echo $DB->insert_id();
    }

}

?>
