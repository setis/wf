<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "ОДС";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник ОДС";
    $PLUGINS[$plugin_uid]['events']['getODSListA'] = "Список ОДС с выделением по ИД";
    $PLUGINS[$plugin_uid]['events']['setODSParamsA'] = "Обновление ОДС (AJAX)";
    $PLUGINS[$plugin_uid]['events']['saveA'] = "Создание ОДС (AJAX)";

}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование ОДС";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранить ОДС";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление ОДС";
    
    
}
$PLUGINS[$plugin_uid]['events']['getODSListA'] = "Список ОДС";
$PLUGINS[$plugin_uid]['events']['getODSParams'] = "ОДС по ИД для заявок";
$PLUGINS[$plugin_uid]['events']['setODSParamsA'] = "Обновление ОДС (AJAX)";
?>