<?php

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;


require_once(dirname(__FILE__)."/../connections/connections.php");


class reports_op_conn_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function main() {
        global $DB, $USER;
        $interval_type = array("0"=>"Дни", "1"=>"Недели", "2"=>"Месяцы");
        $interval_type_single = array("0"=>"день", "1"=>"неделю", "2"=>"месяц");
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");

        $this->getSession()->set("opc_filter_sc", $_REQUEST['opc_filter_sc'] ? $_REQUEST['opc_filter_sc'] : $this->getSession()->get("opc_filter_sc"));
        $_REQUEST['opc_filter_sc'] = $this->getSession()->get("opc_filter_sc");
        $this->getSession()->set("opc_filter_area", $_REQUEST['opc_filter_area'] ? $_REQUEST['opc_filter_area'] : $this->getSession()->get("opc_filter_area"));
        $_REQUEST['opc_filter_area'] = $this->getSession()->get("opc_filter_area");
        $this->getSession()->set("opc_filter_type", $_REQUEST['opc_filter_type'] ? $_REQUEST['opc_filter_type'] : $this->getSession()->get("opc_filter_type"));
        $_REQUEST['opc_filter_type'] = $this->getSession()->get("opc_filter_type");
        $this->getSession()->set("opc_contr_list", $_REQUEST['opc_contr_list'] ? $_REQUEST['opc_contr_list'] : $this->getSession()->get("opc_contr_list"));
        $_REQUEST['opc_contr_list'] = $this->getSession()->get("opc_contr_list");
        if ($_REQUEST["date_from"]!= "" && $_REQUEST["date_to"] != "") {
            if (strtotime($_REQUEST["date_to"]) < strtotime($_REQUEST["date_from"])) {
                UIError("Дата начала периода не может быть больше даты окончания периода!");
                exit;
            }
        } else {
            $_REQUEST["date_from"] = $_REQUEST["date_from"] == "" ? date("d.m.Y", strtotime(date("Y-m-d")." - 1 month")) : $_REQUEST["date_from"];
            $_REQUEST["date_to"] = $_REQUEST["date_to"] == "" ? date("d.m.Y", strtotime(date("Y-m-d"))) : $_REQUEST["date_to"];
        }


        if ($_REQUEST["myearfrom"] == "") $_REQUEST["myearfrom"] = (intval(date("m")) - 1) <= 0 ? (intval(date("Y"))-1) : date("Y");
            ///if ($_REQUEST["myearfrom"] == "") $_REQUEST["myearfrom"] = date("Y");
        if ($_REQUEST["myearto"] == "") $_REQUEST["myearto"] = date("Y");
        if ($_REQUEST["monthto"] == "") $_REQUEST["monthto"] = date("m");
        if ($_REQUEST["monthfrom"] == "") $_REQUEST["monthfrom"] = (intval(date("m")) - 1) <= 0 ? 12 : (intval(date("m")) - 1);
        if ($_REQUEST["wyearto"] == "") $_REQUEST["wyearto"] = date("Y");
        if ($_REQUEST["weekto"] == "") $_REQUEST["weekto"] = date("W");
        $cYear = intval(date("Y"))-1;
        if ($_REQUEST["weekfrom"] == "") $_REQUEST["weekfrom"] = (intval(date("W"))-1) == 0 ? date("W", strtotime($cYear."-12-31")) : date("W")-1;
        if ($_REQUEST["wyearfrom"] == "") $_REQUEST["wyearfrom"] = (intval(date("W")) - 1) <= 0 ? (intval(date("Y"))-1) : date("Y");
        $wYearFrom = $_REQUEST["wyearfrom"];
        $weeksFrom = array();
        $weeksTo = array();
        $mwf = date("W", strtotime((string)$_REQUEST["wyearfrom"]."-12-28"));
        $mwt = date("W", strtotime((string)$_REQUEST["wyearto"]."-12-28"));
        for ($i=1; $i<=$mwf; $i++) {
            if (strlen($i) == 1)
                $weeksFrom["0".$i] = "0".$i;
            else
                $weeksFrom[$i] = $i;

        }
        for ($i=1; $i<=$mwt; $i++) {
            $weeksTo[$i] = $i;
        }
        if ($_REQUEST['opc_filter_sc']) {
                $areaData1 = $DB->getCell2("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT area_id FROM `link_sc_area` WHERE `sc_id` IN (".preg_replace("/,$/", "",implode(",",$_REQUEST["opc_filter_sc"])).")) GROUP BY `id` ORDER BY `title`;");
            } else {
                $areaData1 = $DB->getCell2("SELECT `id`,`title` FROM `list_area` ORDER BY `title`;");
            }
            $areaData["no"] = "-- не указан";
            $finalAreas = $areaData + $areaData1;
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT_CONN_OP", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable("DATE_FROM", $_REQUEST["date_from"]);
            $tpl->setVariable("DATE_TO", $_REQUEST["date_to"]);

            $tpl->setVariable('AREALIST', array2options($finalAreas, $_REQUEST['opc_filter_area'], false, ($_REQUEST["opc_filter_area"] == "" || count($finalAreas) == count($_REQUEST['opc_filter_area']))));
            $wtypes = array2options(adm_ticktypes_plugin::getList("connections"), $_REQUEST["opc_filter_type"]);
            $kontrList = array2options(kontr_plugin::getListConn(), $_REQUEST['opc_contr_list']);
            $tpl->setVariable('CONTRLIST', $kontrList);
            $tpl->setVariable('SERVICELIST', $wtypes);
            $tpl->setVariable('SCLIST', adm_sc_plugin::getScList($_REQUEST['opc_filter_sc']));
            $tpl->setVariable("INTERVAL", array2options($interval_type, $_REQUEST["interval_type"]));
            $tpl->setVariable("WYEARFROM", array2options($year, $_REQUEST["wyearfrom"]));
            $tpl->setVariable("WYEARTO", array2options($year, $_REQUEST["wyearto"]));
            $tpl->setVariable("YEARFROM", array2options($year, $_REQUEST["myearfrom"]));
            $tpl->setVariable("YEARTO", array2options($year, $_REQUEST["myearto"]));
            $tpl->setVariable("MONTHFROM", array2options($month, $_REQUEST["monthfrom"]));
            $tpl->setVariable("MONTHTO", array2options($month, $_REQUEST["monthto"]));
            $tpl->setVariable("WEEKFROM", array2options($weeksFrom, $_REQUEST["weekfrom"]));
            $tpl->setVariable("WEEKTO", array2options($weeksTo, $_REQUEST["weekto"]));
            UIHeader($tpl);
            report_left_menu($tpl);
        }

        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"] || @$_POST["setflag"]) {

                $_REQUEST["date_from"] = substr($_REQUEST["date_from"],6,4)."-".substr($_REQUEST["date_from"],3,2)."-".substr($_REQUEST["date_from"],0,2);
                $_REQUEST["date_to"] = substr($_REQUEST["date_to"],6,4)."-".substr($_REQUEST["date_to"],3,2)."-".substr($_REQUEST["date_to"],0,2);

                $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
                //$areaData1 = $DB->getCell2("SELECT `id`,`title` FROM `list_area` ORDER BY `title`;");
                //$areaData["no"] = "-- не указан";
                //$finalAreas = array_merge($areaData, $areaData1);
                $rtpl->setVariable("PERIODTITLE", $interval_type_single[$_REQUEST["interval_type"]]);
                if (isset($_REQUEST['opc_filter_area'])) {
                    if (is_array($_REQUEST['opc_filter_area'])) {
                        if (count($_REQUEST['opc_filter_area']) != count(($finalAreas))) {
                            $arr = str_replace("no", "0", preg_replace("/,$/", "", implode("," ,$_REQUEST['opc_filter_area'])));
                            $filter .= " AND addr.area_id IN (".$arr.")";
                        } else {
                            $filter .= "";
                        }
                    }
                }
                //die(count($_REQUEST['opc_filter_area']). " ".count($finalAreas));
                if (isset($_REQUEST['opc_filter_type'])) {
                    if (is_array($_REQUEST['opc_filter_type']) && count($_REQUEST["opc_filter_type"])) {
                        $arr = preg_replace("/,$/", "", implode("," ,$_REQUEST['opc_filter_type']));
                        $filter .= " AND tick.task_type IN (".$arr.")";
                    }
                }
                if (isset($_REQUEST['opc_filter_sc'])) {
                    if (is_array($_REQUEST['opc_filter_sc']) && count($_REQUEST["opc_filter_sc"])) {
                        $arr = preg_replace("/,$/", "", implode("," ,$_REQUEST['opc_filter_sc']));
                        $filter .= " AND tick.sc_id IN (".$arr.")";
                    }
                }
                if (isset($_REQUEST['opc_contr_list'])) {
                    if (is_array($_REQUEST['opc_contr_list']) && count($_REQUEST["opc_contr_list"])) {
                        $arr = preg_replace("/,$/", "", implode("," ,$_REQUEST['opc_contr_list']));
                        $filter .= " AND tick.cnt_id IN (".$arr.")";
                    }
                }

                switch($_REQUEST["interval_type"]) {

                    case "0":
                        $dateParam = "%m %d %Y";
                        $dateParam1 = "%Y-%m-%d";
                        $filter .= " AND t.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id='1' AND
                                        (DATE_FORMAT(tc.datetime, '%Y-%m-%d')>=".$DB->F($_REQUEST["date_from"])." AND DATE_FORMAT(tc.datetime, '%Y-%m-%d')<=".$DB->F($_REQUEST["date_to"]).")) ";
                        break;

                    case "1":
                        $dateParam = "%v %Y";
                        $dateParam1 = "%Y-%v";
                        $filter .= " AND t.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id='1' AND
                                        (DATE_FORMAT(tc.datetime,'%Y-%v')>=".$DB->F($_REQUEST["wyearfrom"]."-".$_REQUEST["weekfrom"])." AND DATE_FORMAT(tc.datetime, '%Y-%v')<=".$DB->F($_REQUEST["wyearto"]."-".$_REQUEST["weekto"]).")) ";
                        break;

                    case "2":
                        $dateParam = "%m %Y";
                        $dateParam1 = "%Y-%m";
                        $filter .= " AND t.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id='1' AND
                                        (DATE_FORMAT(tc.datetime,'%Y-%m')>=".$DB->F(date("Y-m", strtotime($_REQUEST["myearfrom"]."-".$_REQUEST["monthfrom"])))." AND DATE_FORMAT(tc.datetime, '%Y-%m')<=".$DB->F(date("Y-m", strtotime($_REQUEST["myearto"]."-".$_REQUEST["monthto"]))).")) ";


                        //$dateFilter = " AND exdate>=".$DB->F(date("Y-m-d", strtotime($_REQUEST["myearfrom"]."-".$_REQUEST["monthfrom"])))." AND exdate<=".$DB->F(date("Y-m-d", strtotime($_REQUEST["myearto"]."-".$_REQUEST["monthto"])))."";
                        break;

                    default:
                        $dateParam = "%m %d %Y";
                        $filter .= " AND t.id IN (SELECT tc.task_id FROM `task_comments` AS tc WHERE tc.status_id='1' AND
                                        (DATE_FORMAT(tc.datetime,'%Y-%m-%d')>=".$DB->F($_REQUEST["date_from"])." AND DATE_FORMAT(tc.datetime, '%Y-%m-%d')<=".$DB->F($_REQUEST["date_to"]).")) ";
                        break;
                }
                if ($_REQUEST["clnttype"] != "0")
                $filter.= " AND tick.clnt_type=".$DB->F($_REQUEST["clnttype"] == "jur" ? 2 : 1);
                $sql = "SELECT t.id, tick.task_type, tick.cnt_id, DATE_FORMAT((select max(`datetime`) from task_comments where task_id=t.id and status_id='1'), '%Y-%m-%d') as exdate,  DATE_FORMAT((select max(`datetime`) from task_comments where task_id=t.id and status_id='1'), ".$DB->F($dateParam).") as exdatef FROM tasks as t
                        LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                        LEFT JOIN `list_addr` AS addr ON addr.id=tick.dom_id
                        WHERE t.plugin_uid='connections' AND (t.status_id='26' OR t.status_id='23' or t.status_id=99) 
                        $filter;";
                //die($sql);
                $tasktypes = "SELECT tick.task_type, (SELECT `title` FROM `task_types` WHERE `id`=tick.task_type) as typeName FROM tasks as t
                        LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                        LEFT JOIN `list_addr` AS addr ON addr.id=tick.dom_id
                        WHERE t.plugin_uid='connections' AND (t.status_id='26' OR t.status_id='23' or t.status_id=99) $filter GROUP BY tick.task_type;";

                $contrNames = "SELECT tick.cnt_id, (SELECT `contr_title` FROM `list_contr` WHERE `id`=tick.cnt_id) as contrName FROM tasks as t
                        LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                        LEFT JOIN `list_addr` AS addr ON addr.id=tick.dom_id
                        WHERE t.plugin_uid='connections' AND (t.status_id='26' OR t.status_id='23' or t.status_id=99) $filter GROUP BY tick.cnt_id;";

                $head = "SELECT DATE_FORMAT((select max(`datetime`) from task_comments where task_id=t.id and status_id='1'), ".$DB->F($dateParam1).") as exdate, DATE_FORMAT((select max(`datetime`) from task_comments where task_id=t.id and status_id='1'), ".$DB->F($dateParam).") as exdatef FROM tasks as t
                        LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                        LEFT JOIN `list_addr` AS addr ON addr.id=tick.dom_id
                        WHERE t.plugin_uid='connections' AND (t.status_id='26' OR t.status_id='23' or t.status_id=99) $filter GROUP BY exdatef ORDER BY exdate ASC;";
                $head = $DB->getCell2($head);
                if (count($head) == 0) {
                    $rtpl->touchBlock("empty");
                } else {
                    $total = false;
                    $totalCol = false;
                    foreach($head as $item) {
                        $rtpl->setCurrentBlock("delitemtitle");
                        $a = explode(" ", $item);
                        switch($_REQUEST["interval_type"]) {
                            case "0":
                                $rtpl->setVariable("CDELITEM", $a[1].".".$a[0]);
                                break;
                            case "1":
                                $rtpl->setVariable("CDELITEM", $a[0]." неделя ".$a[1]);
                                break;
                            case "2":
                                $rtpl->setVariable("CDELITEM", rudate("F", strtotime($a[1]."-".$a[0]), true)." ".$a[1]);
                                break;
                            default:
                                $rtpl->setVariable("CDELITEM", $a[1].".".$a[0]);
                                break;
                        }
                        $rtpl->parse("delitemtitle");
                        $indates[] = "'".$item."'";
                    }
                    $tasktypes = $DB->getCell2($tasktypes);
                    $contrNames = $DB->getCell2($contrNames);
                    $indates = preg_replace("/,$/", "", implode(",",$indates));
                    $tickets = $DB->query($sql);
                    while ($r1 = $DB->fetch(true)) {
                        $ticketData[] = $r1;
                    }
                    foreach($tasktypes as $key => $item) {
                        $rtpl->setCurrentBlock("rep_row_main");
                        $rtpl->setVariable("MAINWTYPETITLE", $item);
                        $headTotal = 0;
                        foreach($head as $k2 => $rd) {

                            $total = 0;
                            foreach($contrNames as $k3 => $contr) {
                                $contrCount[$k2][$k3] = 0;
                            }
                            foreach($ticketData as $item) {
                                if ($item["exdatef"] == $rd && $item["task_type"] == $key) {
                                    $total += 1;
                                    $headTotal += 1;
                                    $contrCount[$k2][$item["cnt_id"]] += 1;
                                }
                            }
                            $totalCol[$rd] += $total;
                            $rtpl->setCurrentBlock("delitem_main");
                            $tds = explode(" ", $rd);
                            $dw = date( "w", strtotime($tds[2]."-".$tds[0]."-".$tds[1]));
                            if ($dw == 0 || $dw == 6) {
                                //$rtpl->setVariable("IS_HOLYDAY_TD1", "purpleback");
                            }
                            $rtpl->setVariable("MAINITEMVAL", $total == 0 ? "" : $total);
                            $rtpl->parse("delitem_main");
                        }
                        $cntrs = count($head) == 0 ? 1 : count($head);
                        $rtpl->setVariable("MAINTOTALPERIOD", $headTotal);
                        $rtpl->setVariable("MAINAVGPERPERIOD", round($headTotal/$cntrs) == 0 ? "<1" : round($headTotal/$cntrs));
                        foreach($contrNames as $k3 => $contr) {
                            $rtpl->setCurrentBlock("rep_row");
                            $rtpl->setVariable("CONTRTITLE", $contr);
                            $i = false;
                            $cTotal = 0;
                            foreach($head as $k2 => $rd) {
                                $rtpl->setCurrentBlock("delitem");
                                $tds = explode(" ", $rd);
                                $dw = date( "w", strtotime($tds[2]."-".$tds[0]."-".$tds[1]));
                                if ($dw == 0 || $dw == 6) {
                                    $rtpl->setVariable("IS_HOLYDAY_TD2", "purpleback");
                                }
                                $rtpl->setVariable('CONTRTEMVAL', $contrCount[$k2][$k3] == 0 ?  "" : $contrCount[$k2][$k3]);
                                $cTotal += $contrCount[$k2][$k3];
                                if ($contrCount[$k2][$k3]>0) {
                                    $i = true;
                                }
                                $rtpl->parse("delitem");
                            }
                            $rtpl->setVariable("CONTRTOTALPERIOD", $cTotal);
                            $cntrs = count($head) == 0 ? 1 : count($head);
                            $rtpl->setVariable("CONTRAVGPERPERIOD",  round($cTotal/$cntrs) == 0 ? "<1" : round($cTotal/$cntrs));
                            if (!$i) {
                                $rtpl->setVariable("HIDEROW", "hidden");
                            }
                            $rtpl->parse("rep_row");
                            //$contrCount[$k2][$k3] = 0;
                        }


                        $rtpl->parse("rep_row_main");
                    }
                    $DB->free($tickets);
                    $rtpl->setCurrentBlock("total");
                    $rtpl->setVariable("AS1", "");
                    $fulltotal = 0;
                    foreach($totalCol as $item) {
                        $rtpl->setCurrentBlock("delitem_total");
                        $rtpl->setVariable("TOTALCDELITEMVAL", $item);
                        $fulltotal += $item;
                        $rtpl->parse("delitem_total");
                    }
                    $rtpl->setVariable("TOTALPERIOD", $fulltotal);
                    if (count($totalCol) == 0) {
                        $dele = 1;
                    } else {
                        $dele = count($totalCol);
                    }
                    $rtpl->setVariable("AVGPERPERIOD", round($fulltotal/$dele));
                    $rtpl->parse("total");
                }

                if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                    $rtpl->setCurrentBlock("print_head");
                    $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                    $rtpl->parse("print_head");
                }

                $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
                $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
                if (isset($_POST["createxlsversion"])) {
                    header("Content-type: application/vnd.ms-excel");
                    header("Content-disposition: attachment; filename=report_operative_connections__".date("H.i.s_d.m.Y").".xls");
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    if (isset($_POST['createprintversion'])) {
                        $rtpl->touchBlock("print_footer");
                        $rtpl->show();

                        return;
                    } else {
                        $tpl->setCurrentBlock("reportval");
                        $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                        $tpl->parse("reportval");
                    }
                }
            } else {
                $tpl->touchBlock("notcreated");
                $tpl->setVariable("DISABLED", "disabled='disabled'");
            }


        $tpl->show();

    }

    function getweeksofyear_ajax() {
        global $DB, $USER;
        $year = $_REQUEST["year"];
        if (!$year) {
            $ret["error"] = "Не указан год";
            echo json_encode($ret);
            return false;
        }
        $weeknum = date("W", strtotime((string)$year."-12-28"));
        for($i=1; $i<=$weeknum; $i+=1) {
            $ret .= "<option value=\"$i\">$i</option>";
        }
        echo $ret;
    }


}
