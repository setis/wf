<?php
/**
 *  mail@artemd.ru
 * 17.03.2016
 */

/**
 * @var models\Task $model
 * @var array $data
 */

?>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Опрос клиента</h4>
</div>
<div class="modal-body">
    <?php d($data); ?>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
</div>
