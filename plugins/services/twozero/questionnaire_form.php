<?php
/**
 *  mail@artemd.ru
 * 17.03.2016
 */

/**
 * @var models\Task $model
 * @var array $data
 * @var string $returnUrl
 */

?>

<form id="QuestForm" action="">
    <input type="hidden" name="task_id" value="<?php echo $model->getId(); ?>"/>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Опрос клиента</h4>
    </div>
    <div class="modal-body">
        <?php foreach ($data as $i => $item): ?>
            <div class="panel panel-warning">
                <div class="panel-heading"><h5 class="panel-title"><?php echo $item['title']; ?></h5></div>
                <div class="panel-body">
                    <?php foreach ($item['questions'] as $k => $question): ?>
                        <div class="checkbox">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-warning">
                                    <input type="checkbox" autocomplete="off"
                                           name="questionnaire[<?php echo $item['title']; ?>][questions][<?php echo $question; ?>]"> <?php echo $question; ?>
                                </label>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <?php if (isset($item['comment']) && $item['comment']): ?>
                        <textarea class="form-control" rows="3" placeholder="<?php echo $item['comment']; ?>"
                                  name="questionnaire[<?php echo $item['title']; ?>][comment]"></textarea>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
        <button type="button" id="QuestFormSubmit" class="btn btn-success" data-dismiss="modal">Сохранить</button>
    </div>
</form>

<script type="text/javascript">
$('#QuestFormSubmit').on('click', function(event){
    $.post('<?php echo $returnUrl; ?>', $('#QuestForm').serialize());
});
</script>
