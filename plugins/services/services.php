<?php

/**
 * Plugin Implementation
 *
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use classes\tickets\ServiceTicket;
use classes\User;
use models\Task as TaskEntity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class services_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getContrName($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT concat(`contr_title`,' (',`official_title`,')') AS val  FROM `list_contr` WHERE `id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function getTypeName($id, $escape = true)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `title` FROM `task_types` WHERE `plugin_uid`=" . $DB->F($plugin_uid) . " AND `id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function getTypeDuration($id, $escape = true)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `duration` FROM `task_types` WHERE `plugin_uid`=" . $DB->F($plugin_uid) . " AND `id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function statusCallbackGet($task_id, $status)
    {
        global $DB, $USER;

        if ((false !== strpos($status['tag'], 'cmmpodr'))) {

            if ($USER->checkAccess('services2podr', User::ACCESS_WRITE) && self::getPodrId($task_id)) return $status['name'];
            else return false;
        }
        if ((false !== strpos($status['tag'], 'otlozh'))) {
            return $status['name'] . " до <input type='text' name='otlozh_date' class='datetime_input'>";
        }

        if ((false !== strpos($status['tag'], 'done'))) {
            $t = new ServiceTicket($task_id);
            if ($t->getTypeName() != "СКП") {
                return $status['name'];
            } else {
                $user_id = $t->getTaskIspolnIds($task_id);
                if (count($user_id)) {
                    $user_id = $user_id[0];
                    $gfx_user_id = $DB->getField("SELECT `empl_id` FROM `gfx` WHERE `task_id`=" . $DB->F($task_id));
                    if ($user_id == $gfx_user_id) {
                        return $status['name'];
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }

        $err = array();

        $plugin_uid = basename(__FILE__, '.php');
        if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejectReasonsList")) {
            $rej = new adm_rejects_plugin();
            if ($status['tag'] != '') {
                $rejReasons = $rej->getRejectReasonsList(false, $plugin_uid, $status['tag']);
                if ($rejReasons !== FALSE) {
                    return $status['name'] . " <select style=\"margin-left: 10px;width:150px;\" name='reject_" . $status["tag"] . "_id'>" . $rejReasons . "</select>";
                }
            }
        } else {
            $err[] = 'Отсутствует класс Причин отказа!';
        }
        return sizeof($err) ? $err : $status['name'];
    }


    // прием ТМЦ с заявки

    static function getPodrId($task_id)
    {
        global $DB;
        $sql = "SELECT `contr_id` FROM `podr_tickets` WHERE `task_id`=" . $DB->F($task_id);
        return $DB->getField($sql, false);
    }

    static function statusCallbackSet($task_id, &$status, &$text, &$tag)
    {
        global $DB;

        $err = array();
        $contr_status_info = '';
        $task = new Task($task_id);
        if ($status["id"] == "44" || $status["id"] == "45") {
            $c = $DB->getField("SELECT `task_id` FROM `gfx` WHERE `task_id`=" . $DB->F($task_id) . ";");
            if ($c !== FALSE) {
                //$DB->query("DELETE FROM `gfx` WHERE `task_id`=".$DB->F($task_id).";");
                //$task->addComment("", "Автоматическое удаление из графика");
            }
        }

        if (false !== strpos($status['tag'], 'otlozh')) {
            if (!$_POST['otlozh_date']) $err[] = "Нет даты, до которой отложить";
            else {
                if ($task->setDateFn($_POST['otlozh_date'])) {
                    $atag = explode(";", $tag);
                    $atag[0] .= " до {$_POST['otlozh_date']}";
                    $tag = implode("; ", $atag);
                }
            }
        }
        if (false !== strpos($status['tag'], 'cmmpodr')) {

            if ($text && ($podr_id = self::getPodrId($task_id))) {
                $sql = "INSERT INTO `podr_updates` (`date_post`, `task_id`, `contr_id`, `cmm`) VALUES(NOW(), " . $DB->F($task_id) . ", " . $DB->F($podr_id) . ", " . $DB->F($text) . ")";
                $DB->query($sql);

                if ($DB->errno()) $err[] = "Не удалось отправить комментарий подрядчику!";
                $DB->free();
            }

            $status = array();
            $tag = "Отправлен комментарий подрядчику";

            return sizeof($err) ? $err : true;
        }
        //die($status['tag']);
        $rejectKey = "reject_" . $status["tag"] . "_id";
        $reject_id = (isset($_POST[$rejectKey]) && $_POST[$rejectKey]) ? $_POST[$rejectKey] : false;
        if ($reject_id) {
            $sql = "UPDATE `tickets` SET `reject_id`=" . $DB->F($reject_id) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) return false;
            $DB->free();
            if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejReasonTitle")) {
                $rej = new adm_rejects_plugin();
                $tag .= ($tag ? ', ' : '') . 'Причина <b>' . $rej->getRejReasonTitle($reject_id) . '</b>';
                $contr_status_info = $rej->getRejReasonTitle($reject_id);
            } else return false;
        } else {
            $sql = "UPDATE `tickets` SET `reject_id`=" . $DB->F("0") . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno())
                return false;
            $DB->free();
        }
        $price = $DB->getField("SELECT `price` FROM `tickets` WHERE `task_id`=" . $DB->F($task_id) . ";");
        if ($_POST['grafik_date'] && preg_match("/grafik/", $status['tag'])) {
            $contr_status_info = $_POST['grafik_date'];
        }

        $sql = "SELECT `contr_id`, `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=" . $DB->F($task_id);
        list($contr_id, $contr_ticket_id) = $DB->getRow($sql);
        //trigger_error("Task_id='$task_id', contr_id='$contr_id', contr_ticket_id='$contr_ticket_id'");
        if ($contr_id == getcfg('nbn_contr_id') && preg_match("/nbn/", $status['tag'])) { // заявка от НБН + статус для них
            try {
                $client = new SoapClient(getcfg('nbn_soap_url'));
                $data = new stdClass();
                $data->login = getcfg('nbn_soap_login');
                $data->password = getcfg('nbn_soap_pass');
                $data->ticket_id = $contr_ticket_id;
                $data->price = (string)$price;
                $data->status = $status['name'];
                $data->status_info = $contr_status_info;
                $data->execution_date = (string)time();
                $result = $client->updateTicket($data);
                trigger_error("NBN SOAP Result code {$result->code}: " . $result->message);
                if ($result->code != 200) {
                    $err[] = "Ошибка обновления даннх в НБН: " . $result->message;
                } else {
                    $tag .= "; статус отправлен а НБН";
                }
            } catch (SoapFault $e) {
                trigger_error("NBN SOAP Exeption! " . $e->getMessage(), E_USER_WARNING);
                $err[] = "Ошибка связи с НБН: " . $e->getMessage();
            }
        }

        $ticket = new ServiceTicket($task_id);

        $lsnum = $ticket->getClntTNum();
        // передача статусов в РТ
        if ($ticket->getContrId() == "70" && $lsnum && preg_match("/rt/", $status['tag'])) {
            if (!$ticket->isRtkMo()) {
                try {
                    $options = array(
                        'login' => getcfg('rt_soap_login'),
                        'password' => getcfg('rt_soap_password'),
                        "trace" => 1, "exceptions" => 1, "encoding" => "UTF-8"
                    );
                    $client = new SoapClient(getcfg('rt_soap_url'), $options);
                    $price = $ticket->getPrice() ? number_format($ticket->getPrice(), 2, ".", "") : 0;
                    $targetDate = false;
                    if ($status["id"] == 9) {
                        $tdate = $DB->getRow("SELECT * FROM `gfx` WHERE `task_id`=" . $DB->F($task_id) . ";", true);
                        if ($tdate) {
                            $timeStart = (strlen(floor($tdate["startTime"] / 60)) == 1 ? "0" . floor($tdate["startTime"] / 60) : floor($tdate["startTime"] / 60)) . ":" . (strlen($tdate["startTime"] % 60) == 1 ? "0" . $tdate["startTime"] % 60 : $tdate["startTime"] % 60);
                            $dateStart = $tdate["c_date"];
                            $targetDate = $dateStart . "T" . $timeStart . ":00.000";
                        }
                    }

                    $processData = array("lsnum" => $lsnum, "status" => $status["id"], "statusDate" => date("Y-m-d") . "T" . date("H:i:s") . ".000", "cost" => $price);
                    //$processData = array("lsnum"=>$lsnum, "status"=>$status["id"], "cost"=>$price);
                    if ($targetDate) {
                        $processData["scheduleDate"] = $targetDate;
                    }
                    $response = $client->process($processData);

                    if ($response->code == 0) {
                        $tag .= "; статус " . ($price ? "и стоимость " : "") . "отправлен в Ростелеком";
                    } else {
                        $tag .= "; Ответ от РТ: " . $response->code;
                    }
                } catch (SoapFault $e) {
                    $tag .= "; ошибка передачи статуса в Ростелеком (Москва) - " . $e->getMessage();
                }
            }
        }
        // конец передача статусов в РТ

        // передача статусов подрядчику начало
        if (($podr_id = self::getPodrId($task_id)) && $DB->getField("SELECT COUNT(*) FROM `podr_sendstatus` WHERE `contr_id`=" . $DB->F($podr_id) . " AND `status_id`=" . $DB->F($status['id']))) {
            $sql = "INSERT INTO `podr_updates` (`date_post`, `task_id`, `contr_id`, `status_id`, `cmm`) VALUES(NOW(), " . $DB->F($task_id) . ", " . $DB->F($podr_id) . ", " . $DB->F($status['id']) . ", " . $DB->F($status['name']) . ")";
            $DB->query($sql);
            if ($DB->errno()) {
                $err[] = sprintf("Ошибка передачи статуса подрядчику %d: %s", $DB->errno(), $DB->error());
            } else {
                $tag .= "; статус отправлен подрядчику";
            }
            $DB->free();
        }
        // передача статусов подрядчику конец


        return sizeof($err) ? $err : true;
    }

    static function acceptticket_ajax()
    {
        global $DB, $USER;
        $id = $_REQUEST["task_id"];
        if (!$id) {
            $ret["error"] = "Не указан идентификатор заявки!";
            echo json_encode($ret);
            return false;
        }
        $t = new Task($id);
        $status_id = $t->getStatusId();
        if ($status_id == 44) {
            if ($t->addComment("", "Заявка принята инженером " . $USER->getFio() . ". Статус: <b>Назначение в график</b>", 45)) {
                ob_start();
                $t->toMail();
                $ret["error"] = ob_get_contents();
                ob_end_clean();
                $ret["ok"] = "ok";
            } else {
                $ret["error"] = "Ошибка смены статуса заявки!";
            }

        } else {
            $ret["error"] = "Заявка находится в некорректном статусе!";

        }
        echo json_encode($ret);
        return false;

    }

    static function rejectticket_ajax()
    {
        global $DB, $USER;
        $id = $_REQUEST["task_id"];
        if (!$id) {
            $ret["error"] = "Не указан идентификатор заявки!";
            echo json_encode($ret);
            return false;
        }
        $t = new Task($id);
        $status_id = $t->getStatusId();
        if ($status_id == 44) {
            $DB->query("DELETE FROM `gfx` WHERE `task_id`=" . $DB->F($id) . " AND `empl_id`=" . $DB->F($USER->getId()) . ";");

            $DB->query("UPDATE `task_users` SET `ispoln`=0 WHERE `task_id`=" . $DB->F($id) . " AND `user_id`=" . $DB->F($USER->getId()) . ";");
            $DB->query("DELETE FROM `task_users` WHERE `task_id`=" . $DB->F($id) . " AND `user_id`=" . $DB->F($USER->getId()) . ";");
            if ($t->addComment("Причина: " . $_REQUEST["reject_reason"], "Заявка отклонена инженером " . $USER->getFio() . ". Статус: <b>Новая</b>", 8)) {
                $ret["ok"] = "ok";
            } else {
                $ret["error"] = "Ошибка смены статуса заявки!";
            }

        } else {
            $ret["error"] = "Заявка находится в некорректном статусе!";

        }
        echo json_encode($ret);
        return false;

    }

    static function getPriceItemsByTask($task_id)
    {
        global $DB, $USER;
        if (!$task_id) return false;
        $t = new ServiceTicket($task_id);
        $sql = "SELECT tc.wt_id AS keyval, tc.price*tc.quant AS price, (SELECT `contrcode` FROM `list_skp_p_price` WHERE `contr_id`=" . $DB->F($t->getContrId()) . " AND `wt_id`=tc.wt_id) AS contr_code FROM `list_skp_tech_calc` AS tc WHERE tc.task_id=" . $DB->F($task_id) . ";";
        $result = $DB->getCell3($sql, true);
        return $result;

    }

    function viewlist(Request $request)
    {
        global $DB, $USER;

        $g_user_id = $this->getUserId();
        $sort_ids = array(
            "Релевантность",
            "Дата изменения",
            "Дата создания",
            "Дата в графике",
            "Техник"
        );
        $sort_sql = array(
            "ts.relevance_level DESC, ts.lastcmm_date",
            "ts.lastcmm_date",
            "ts.date_reg",
            "gfx.c_date",
            "fio"
        );
        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );
        $rpp = array(
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250",
            500 => "500"
        );

        $LKUser = adm_users_plugin::isLKUser($USER->getId());

        $LKContrID = 0;
        if ($LKUser) {
            $LKContrID = adm_users_plugin::getLKUserContrID($USER->getId());
            $tpl_add = "_lk";
        } else {
            $tpl_add = "";
        }
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/list$tpl_add.tmpl.htm");
        else
            $tpl->loadTemplatefile("list$tpl_add.tmpl.htm");
        if ($USER->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }
        if (!$USER->isTech($USER->getId()) || ($USER->isTech($USER->getId())) && $USER->isChiefTech($USER->getId())) {
            $tpl->setCurrentBlock("notistech");

            // LK
            if ($LKUser) {
                $tpl->setVariable("TEMP_ISROSTEL1", "style='display:none;'");
            }
            // --------
            $tpl->setVariable("PLUGIN1_UID", $this->getUID());
            $tpl->parse("notistech");

        }
        // LK
        if ($LKUser) {
            $tpl->setVariable("TEMP_ISROSTEL", "style='display:none;'");
        }
        // --------
        $filter = "ts.plugin_uid=" . $DB->F($this->getUID());
        $isTech = User::isTech($g_user_id);
        $isChiefTech = User::isChiefTech($g_user_id);

        if (!$LKUser) {
            if (($isTech || $isChiefTech) && !$USER->checkAccess("fullaccesstoallsc")) {
                if ($isChiefTech) {
                    #$_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $isChiefTech);
                    if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']) . " OR tu.user_id=" . $DB->F($_REQUEST['filter_empl_id']) . ")";
                    $tpl->setCurrentBlock("techlist");
                    if ($_REQUEST['filter_empl_id']) {
                        $tpl->setVariable("FILTER_EMPL_ID", $_REQUEST['filter_empl_id']);
                        $usrName = adm_users_plugin::getUser($_REQUEST["filter_empl_id"]);
                        $tpl->setVariable("FILTER_EMPL_NAME", $usrName["fio"]);
                    }
                    $tpl->setVariable('FILTER_SC_OPTIONS', $this->getSCOptions($_REQUEST['filter_sc_id']));
                    $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
                    $tpl->parse("techlist");
                } elseif ($isTech) {
                    $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $g_user_id);
                    $virtAlias = adm_empl_plugin::isVirtual($g_user_id);
                    if ($virtAlias) {
                        if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($g_user_id) . " OR tu.user_id=" . $DB->F($g_user_id) . " OR gfx.empl_id=" . $DB->F($virtAlias) . " OR tu.user_id=" . $DB->F($virtAlias) . ")";
                    } else {
                        if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($g_user_id) . " OR tu.user_id=" . $DB->F($g_user_id) . ")";
                    }
                }
            } else {
                $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $request->get('filter_sc_id', -1));
                $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);

                if ($request->get('filter_sc_id', -1) > 0) {
                    $filter .= " AND tt.sc_id=" . $request->request->getInt('filter_sc_id');
                } elseif (0 == $request->get('filter_sc_id', -1)) {
                    $filter .= " AND (tt.sc_id IS NULL OR tt.sc_id = 0)";
                }

                if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']) . " OR tu.user_id=" . $DB->F($_REQUEST['filter_empl_id']) . ")";
                $tpl->setCurrentBlock("sclist");
                if ($LKUser) {
                    $tpl->setVariable("TEMP_ISROSTEL2", "style='display:none;'");
                }
                $tpl->setVariable('FILTER_SC_OPTIONS', $this->getSCOptions($_REQUEST['filter_sc_id']));
                $tpl->parse("sclist");
                $tpl->setCurrentBlock("techlist");
                if ($LKUser) {
                    $tpl->setVariable("TEMP_ISROSTEL3", "style='display:none;'");
                }
                if ($_REQUEST['filter_empl_id']) {
                    $tpl->setVariable("FILTER_EMPL_ID", $_REQUEST['filter_empl_id']);
                    $usrName = adm_users_plugin::getUser($_REQUEST["filter_empl_id"]);
                    $tpl->setVariable("FILTER_EMPL_NAME", $usrName["fio"]);
                }
                $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
                $tpl->parse("techlist");
            }
        }
        $_REQUEST["date_gfx"] = $this->getCookie('date_gfx', $_REQUEST['date_gfx']);
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_cnt_id'] = $this->getCookie('filter_cnt_id', $_REQUEST['filter_cnt_id']);
        $_REQUEST['filter_type'] = $this->getCookie('filter_type', $_REQUEST['filter_type']);
        $_REQUEST['filter_clnt_type'] = $this->getCookie('filter_clnt_type', $_REQUEST['filter_clnt_type']);

        $_REQUEST['filter_status_id'] = array_filter(
            $this->getCookie('filter_status_id', $request->request->has('filter_status_id') ? $request->get('filter_status_id') : null),
            function($e) { return !empty($e) && -1 != $e; }
        );

        $_REQUEST['filter_ods_id'] = $this->getCookie('filter_ods_id', $_REQUEST['filter_ods_id']);

        $_REQUEST['filter_ctgrnum_id'] = $this->getCookie('filter_ctgrnum_id', $_REQUEST['filter_ctgrnum_id']);
        #$_REQUEST['filter_addr'] = $this->getCookie('filter_addr', $_REQUEST['filter_addr']);
        $_REQUEST['filter_addr_flat'] = $this->getCookie('filter_addr_flat', $_REQUEST['filter_addr_flat']);
        $_REQUEST['filter_addr_house'] = $this->getCookie('filter_addr_house', $_REQUEST['filter_addr_house']);
        if ($_REQUEST["timefrom"] != '') {
            $_REQUEST["timefrom"] = $this->getCookie('timefrom', $_REQUEST["timefrom"]);
        }
        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ? $_REQUEST['filter_sort_order'] : key($sort_order));
        if (!isset($_REQUEST["filter_rpp"])) $_REQUEST["filter_rpp"] = $USER->getRpp();
        if ($_REQUEST["filter_rpp"] != $USER->getRpp()) {
            $sql = "UPDATE `users` SET `rpp`=" . $DB->F($_REQUEST["filter_rpp"]) . " WHERE `id`=" . $DB->F($USER->getId()) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
        } else {
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        }
        if ($_REQUEST['date_gfx']) $filter .= " AND gfx.c_date=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['date_gfx'])));
        if ($_REQUEST['filter_id']) $filter .= " AND ts.id=" . $DB->F($_REQUEST['filter_id']);
        if ($_REQUEST['filter_cnt_id']) $filter .= " AND tt.cnt_id=" . $DB->F($_REQUEST['filter_cnt_id']);
        if ($_REQUEST['filter_type']) $filter .= " AND tt.task_type=" . $DB->F($_REQUEST['filter_type']);
        if ($_REQUEST['filter_clnt_type']) $filter .= " AND tt.clnt_type=" . $DB->F($_REQUEST['filter_clnt_type']);

        if (!empty($_REQUEST['filter_status_id'])) {
            $filter .= " AND ts.status_id in(" . implode(',', array_map($DB->F, $_REQUEST['filter_status_id'])) . ')';
        }
        if ($_REQUEST['filter_ods_id']) $filter .= " AND ods.id=" . $DB->F($_REQUEST['filter_ods_id']);

        if ($_REQUEST['filter_ctgrnum_id']) $filter .= " AND lower(tt.clnttnum) LIKE " . $DB->F("%" . strtolower($_REQUEST['filter_ctgrnum_id']) . "%");
        //JN Full address search
        if ($_REQUEST['filter_addr']) {
            $addr = '%' . preg_replace('/[^a-z0-9а-я-]+/iu', '%', $_REQUEST['filter_addr']) . '%';
            $filter .= " AND tt.dom_id IN (select id from list_addr where list_addr.full_address LIKE " . $DB->F($addr) . ")";
        }

        if ($_REQUEST["filter_addr_flat"]) $filter .= " AND tt.kv LIKE " . $DB->F(strtolower($_REQUEST['filter_addr_flat']) . "%");

        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];

        $statusList = adm_statuses_plugin::getFullList("services");
        if ($statusList) {
            $tpl->setCurrentBlock("sl-items");
            foreach ($statusList as $st_item) {
                $tpl->setCurrentBlock("statuslist");
                if (in_array($st_item["id"], $_REQUEST['filter_status_id'])) {
                    $tpl->setVariable("FF_SELECTED", 'ffstisel');
                }
                $tpl->setVariable("FF_STATUS_ID", $st_item["id"]);
                $tpl->setVariable("FF_STATUS", $st_item["name"]);
                $tpl->setVariable("FF_STATUS_COLOR", $st_item["color"]);
                $tpl->parse("statuslist");
            }
            if (in_array('-1', $_REQUEST['filter_status_id'])) {
                $tpl->setVariable("FF_ALL_SELECTED", "ffstisel");
            }
            $tpl->parse("sl-items");

        }
        if (($isChiefTech && $isTech) || !$isTech) {

            $_REQUEST['filter_client_phone'] = $this->getCookie('filter_client_phone', $_REQUEST['filter_client_phone']);
            $tpl->setCurrentBlock("is_clnt_phone");
            $tpl->setVariable("FILTER_CLIENT_PHONE", $_REQUEST['filter_client_phone'] ? $_REQUEST['filter_client_phone'] : "");
            $tpl->parse("is_clnt_phone");
            if ($_REQUEST['filter_client_phone']) {
                $phone = preg_replace("/(\D+)/", "", $_REQUEST["filter_client_phone"]);
                $filter .= " AND (`clnt_tel1` LIKE '%" . $phone . "%' OR `clnt_tel2` LIKE '%" . $phone . "%') ";
            }
        } else {
            $_REQUEST['filter_client_phone'] = false;
        }
        $tpl->setVariable('FILTER_ID', htmlspecialchars($_REQUEST['filter_id']));
        $tpl->setVariable("FILTER_DATE_GFX", $_REQUEST['date_gfx'] ? $_REQUEST['date_gfx'] : "");
        $tpl->setVariable("FILTER_ADDR", $_REQUEST['filter_addr'] ? $_REQUEST['filter_addr'] : "");
        $tpl->setVariable("FILTER_ADDR_HOUSE", $_REQUEST['filter_addr_house'] ? $_REQUEST['filter_addr_house'] : "");
        $tpl->setVariable("FILTER_ADDR_FLAT", $_REQUEST['filter_addr_flat'] ? $_REQUEST['filter_addr_flat'] : "");
        $tpl->setVariable('FILTER_CNT_OPTIONS', $this->getContrOptions($_REQUEST['filter_cnt_id']));
        $tpl->setVariable('FILTER_TYPE_OPTIONS', $this->getTypeOptions($_REQUEST['filter_type']));
        $tpl->setVariable('FILTER_CLNT_TYPE_SEL_' . $_REQUEST['filter_clnt_type'], 'selected');
        $status = wf::$em->getRepository(\models\TaskStatus::class)->findBy(['pluginUid' => \models\Task::SERVICE]);
        $statuses = array_map(function (\models\TaskStatus $item) {
            return [
                'id' => $item->getId(),
                'text' => $item->getName()
            ];
        }, $status);
        $statusStyles = implode("\n", array_map(function (\models\TaskStatus $item) {
            return ".status_{$item->getId()}{
                background:{$item->getColor()};
            }";
        }, $status));
        $tpl->setVariable('STATUS_STYLE', $statusStyles);
        $tpl->setVariable('FILTER_STATUS_OPTIONS', json_encode($statuses));
        $tpl->setVariable('FILTER_STATUS_OPTIONS_SET', (empty($_REQUEST['filter_status_id'])) ? '""' : json_encode($_REQUEST['filter_status_id']));
        $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
        $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
        $tpl->setVariable('FILTER_ODS_OPTIONS', $this->getODSOptions($_REQUEST['filter_ods_id']));

        if (($isChiefTech || $isTech) && !$USER->checkAccess("scaccfullaccess")) {
            $sc_id = $isChiefTech ? $isChiefTech : User::getScIdByUserId($g_user_id);
        }

        $tpl->setVariable('CTGRNUM_ID', $_REQUEST['filter_ctgrnum_id']);
        $tpl->setVariable('FILTER_ROWS_PER_PAGE', array2options($rpp, $_REQUEST['filter_rpp']));

        if ($LKUser) {
            $filter .= " AND tt.cnt_id=" . $DB->F($LKContrID) . " ";
        }

        $sc_id_filter = (int)$_REQUEST['filter_sc_id'];
        if ($sc_id_filter > 0) {
            $filter .= " and tt.sc_id = " . $sc_id_filter;
        }

        // --------
        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    ts.relevance_level,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y %H:%i') AS DATE,
                    ca.contr_title AS CONTR,
                    typ.title AS TYPE,
                    tt.dom_id,
                    tt.clnt_org_name,
                    tt.clnt_fio,
                    tt.clnt_type,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    addr.name, 
                    addr.full_address,
                    addr.is_not_recognized,
                    gfx.c_date, 
                    gfx.startTime, 
                    tt.clnttnum, 
                    tt.kv, 
                    st.tag, 
                    ct.contr_ticket_id, 
                    ts.date_fn, 
                    st.tag,
                    tt.disallowgfx, 
                    tt.asrzerror, 
                    tt.wfmplanned, 
                    tt.noagrmgts, 
                    tt.asrzok, 
                    tt.setted
                FROM
                    `tasks` AS ts
                    LEFT JOIN `tickets` AS tt ON ts.id=tt.task_id
                    LEFT JOIN `list_contr` AS ca ON tt.cnt_id=ca.id
                    LEFT JOIN task_users AS tu ON (tu.task_id=ts.id and tu.ispoln = 1)
                    LEFT JOIN `task_types` AS typ ON tt.task_type=typ.id
                    LEFT JOIN `task_status` AS st ON ts.status_id=st.id
                    LEFT JOIN `gfx` AS gfx ON gfx.task_id=ts.id
                    LEFT JOIN `list_addr` AS addr ON addr.id=tt.dom_id
                    LEFT JOIN `list_ods` AS ods ON ods.id=addr.ods_id
                    LEFT JOIN `contr_tickets` AS ct ON ct.task_id=ts.id
                WHERE $filter
                  and case when ifnull(tt.sc_id,0) = 0 then 1
                           when exists (select * from user_sc_access usa where usa.user_id = " . $USER->getId() . " and usa.sc_id = tt.sc_id) then 1
                           else 0
                       end = 1
                GROUP BY ts.id, c_date, startTime
                ORDER BY
                    $order
                LIMIT " . intval($_REQUEST['filter_start']) . ", " . intval($_REQUEST["filter_rpp"]);
        $DB->query($sql);

        $total = $DB->getFoundRows();
        if ($DB->errno) UIError($DB->error());
        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("delticket-head");
        }

        while ($a = $DB->fetch(true)) {
            if ($a["disallowgfx"]) {
                $tpl->setVariable("MGTS_STYLE", "style='background:#FF0000;'");
                $tpl->setVariable("MGTS_INTEGR", "<br />не назначена");
            } else {
                if ($a["asrzerror"]) {
                    $tpl->setVariable("MGTS_STYLE", "style='background:#964E4E;'");
                    $tpl->setVariable("MGTS_INTEGR", "<br />Несоответствие");
                } else {
                    if ($a["noagrmgts"]) {
                        $tpl->setVariable("MGTS_STYLE", "style='background:#FC7474;'");
                        $tpl->setVariable("MGTS_INTEGR", "<br />Не согласовано");
                    } else {
                        if ($a["setted"]) {
                            $tpl->setVariable("MGTS_STYLE", "style='background:#FFF700;'");
                            $tpl->setVariable("MGTS_INTEGR", "<br />Назначена");
                        }
                    }
                }
            }


            if ($a['clnt_type'] == User::CLIENT_TYPE_PHYS) {
                $a['CLIENT'] = $a['clnt_fio'];
            } elseif ($a['clnt_type'] == User::CLIENT_TYPE_YUR) {
                $a['CLIENT'] = $a['clnt_org_name'];
            } else {
                $a['CLIENT'] = $a['clnt_org_name'] . ', ' . $a['clnt_fio'];
            }

            $a["CONTR_TASK_ID"] = $a['contr_ticket_id'] ? $a['contr_ticket_id'] : ($a["clnttnum"] ? $a["clnttnum"] : "<center style='text-decoration:none !important;'>&mdash;</center>");
            $a["HREF_ORDER"] = $this->getLink('vieworderdoc', "id={$a['TASK_ID']}");
            $a['HREF'] = $this->getLink('viewticket', "task_id={$a['TASK_ID']}");

            //JN Address by one row
            if (!empty($a['full_address'])) {
                if ($a['is_not_recognized']) {
                    $a['FULL_ADDRESS'] = $a['full_address'] . ' <sup>*</sup>';
                } else {
                    $a['FULL_ADDRESS'] = $a['full_address'];
                }

                $a['ADDR_FLAT'] = $a["kv"];
                $tpl->parse("kladr_addr");
            } else {
                $tpl->setCurrentBlock("no_kladr");
                $a['ADDR_NO_KLADR'] = "<font color=#FF0000>Адрес не распознан</font>";
                $tpl->parse("no_kladr");
            }

            if (preg_match("/otlozh/", $a['tag']) && $a['date_fn']) {
                $a['GFXTIME'] = rudate("d M Y", strtotime($a['date_fn']));
                $a['STATUS_NAME'] .= " до";
                if (date("Y-m-d", strtotime($a['date_fn'])) < date("Y-m-d")) $a['BLINK'] = "blink";
                //die($a['date_fn']);

            } elseif ($a['c_date'] && $a['startTime']) {
                $startHour = (strlen(floor($a['startTime'] / 60)) == 1) ? "0" . (floor($a['startTime'] / 60)) : floor($a['startTime'] / 60);
                $startMin = (strlen(floor($a['startTime'] % 60)) == 1) ? "0" . (floor($a['startTime'] % 60)) : floor($a['startTime'] % 60);
                if (preg_match("/grafik/", $a['tag'])) {
                    $a['GFXTIME'] = rudate("d M Y", strtotime($a['c_date'])) . " " . $startHour . ":" . $startMin;
                }
            }
            $a['USERFIO'] = Task::getTaskIspoln2($a['TASK_ID']);
            $t = new ServiceTicket($a["TASK_ID"]);
            if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getSC")) {
                $scO = new adm_sc_plugin();
                $sc = $scO->getSC($t->getSCId());
            }
            $a['SC'] = $sc['title'] ? $sc['title'] : "<center>&mdash;</center>";
            $a['CONTR_ID'] = $clnttnum ? $clnttnum : "<center>&mdash;</center>";
            $tpl->setCurrentBlock('row');
            if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
                $tpl->setCurrentBlock("deleteticket");
                $tpl->setVariable("DELETEHREF", $this->getLink('deleteticket', "task_id={$a['TASK_ID']}"));
                $tpl->parse("deleteticket");
            }
            $a["LASTCOMMENTVALUE"] = strip_tags($t->getLastComment(true));
            $tpl->setVariable($a);
            $tpl->parse("row");
        }
        $DB->free();
        //$DB->query($sql);
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"], $total, "#start-%s"));
        UIHeader($tpl);
        $tpl->show();
        $DB->free();

    }

    static function getSCOptions($sel_id = 0)
    {
        global $DB, $USER;
        $sql = "SELECT
            s.id, s.title
            FROM list_sc s,
            user_sc_access usa
            WHERE s.id = usa.sc_id
            AND usa.user_id = " . $USER->getId() . "
            ORDER BY s.title";
        $result = $DB->getCell2($sql);
        $result[-1] = '-- Все СЦ --';
        $result[0] = '-- Не указан --';

        ksort($result);

        return array2options($result, $sel_id, false);
    }

    static function getEmplOptions($sel_id = 0)
    {
        global $DB, $USER;
        $sql_add = "";
        if ($isChiefTech = User::isChiefTech(\wf::getUserId()) && !$USER->checkAccess("fullaccesstoallsc", User::ACCESS_WRITE)) {
            $sql_add = " AND user.id IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($isChiefTech) . ")";
        }

        $sql = "SELECT user.id, user.fio FROM `list_empl` AS empl LEFT JOIN `users` AS user ON user.id=empl.user_id WHERE user.active='1' {$sql_add} ORDER BY `user`.`fio`";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getContrOptions($sel_id = 0)
    {
        global $DB;
        $sql = "SELECT `id`, concat(`contr_title`,' (',`official_title`,')') AS val FROM `list_contr` WHERE `contr_type2` ORDER BY `contr_title`";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getTypeOptions($sel_id = 0)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`='services' ORDER BY `title`";

        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    static function getODSOptions($sel_id = 0)
    {
        global $DB;
        $sql = "SELECT `id`, `title` FROM `list_ods` ORDER BY `title`";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    function viewlist_export(Request $request)
    {
        global $USER;
        $LKUser = adm_users_plugin::isLKUser($USER->getId());
        if ($LKUser) {
            UIError("В доступе отказано!");
        }

        global $DB, $USER, $plugin_event, $plugin;;

        $g_user_id = $this->getUserId();
        $sort_ids = array(
            1 => "Дата изменения",
            2 => "Дата создания",
            3 => "Дата в графике",
            4 => "Техник"
        );
        $sort_sql = array(
            1 => "ts.lastcmm_date",
            2 => "ts.date_reg",
            3 => "1",
            4 => "1"
        );
        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/export.tmpl.htm");
        $filter = "ts.plugin_uid=" . $DB->F($this->getUID());
        $isTech = User::isTech($g_user_id);
        $isChiefTech = User::isChiefTech($g_user_id);
        $tpl->setVariable('EVENT_TITLE', getEventTitle($plugin->getUID(), $plugin_event));

        if (($isTech || $isChiefTech) && !$USER->checkAccess("fullaccesstoallsc")) {
            if ($isChiefTech) {
                $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $isChiefTech);
                if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']) . " OR tu.user_id=" . $DB->F($_REQUEST['filter_empl_id']) . ")";
                $tpl->setCurrentBlock("techlist");
                $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
                $tpl->parse("techlist");
            } elseif ($isTech) {
                $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $g_user_id);
                if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($g_user_id) . " OR tu.user_id=" . $DB->F($g_user_id) . ")";
            }
        } else {
            $_REQUEST['filter_sc_id'] = $this->getCookie('filter_sc_id', $request->get('filter_sc_id', -1));
            $_REQUEST['filter_empl_id'] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);
            if ($_REQUEST['filter_empl_id']) $filter .= " AND (gfx.empl_id=" . $DB->F($_REQUEST['filter_empl_id']) . " OR tu.user_id=" . $DB->F($_REQUEST['filter_empl_id']) . ")";
            $tpl->setCurrentBlock("sclist");
            $tpl->setVariable('FILTER_SC_OPTIONS', $this->getSCOptions($_REQUEST['filter_sc_id']));
            $tpl->parse("sclist");
            $tpl->setCurrentBlock("techlist");
            $tpl->setVariable('FILTER_EMPL_OPTIONS', $this->getEmplOptions($_REQUEST['filter_empl_id']));
            $tpl->parse("techlist");
        }

        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $_REQUEST['filter_id'] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST['filter_cnt_id'] = $this->getCookie('filter_cnt_id', $_REQUEST['filter_cnt_id']);
        $_REQUEST['filter_type'] = $this->getCookie('filter_type', $_REQUEST['filter_type']);
        $_REQUEST['filter_clnt_type'] = $this->getCookie('filter_clnt_type', $_REQUEST['filter_clnt_type']);
        $_REQUEST['filter_status_id'] = array_filter(
            $this->getCookie('filter_status_id', $request->request->has('filter_status_id') ? $request->get('filter_status_id') : null),
            function($e) { return !empty($e) && -1 != $e; }
        );
        $_REQUEST['filter_ods_id'] = $this->getCookie('filter_ods_id', $_REQUEST['filter_ods_id']);
        $_REQUEST['filter_ctgrnum_id'] = $this->getCookie('filter_ctgrnum_id', $_REQUEST['filter_ctgrnum_id']);
        #$_REQUEST['filter_addr'] = $this->getCookie('filter_addr', $_REQUEST['filter_addr']);
        $_REQUEST['filter_addr_flat'] = $this->getCookie('filter_addr_flat', $_REQUEST['filter_addr_flat']);
        $_REQUEST['filter_addr_house'] = $this->getCookie('filter_addr_house', $_REQUEST['filter_addr_house']);
        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ? $_REQUEST['filter_sort_order'] : key($sort_order));
        $_REQUEST["date_gfx"] = $this->getCookie('date_gfx', $_REQUEST['date_gfx']);


        if ($_REQUEST['date_gfx']) $filter .= " AND gfx.c_date=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['date_gfx'])));
        if ($_REQUEST['filter_id']) $filter .= " AND ts.id=" . $DB->F($_REQUEST['filter_id']);
        if ($_REQUEST['filter_cnt_id']) $filter .= " AND tt.cnt_id=" . $DB->F($_REQUEST['filter_cnt_id']);
        if ($_REQUEST['filter_type']) $filter .= " AND tt.task_type=" . $DB->F($_REQUEST['filter_type']);
        if ($_REQUEST['filter_clnt_type']) $filter .= " AND tt.clnt_type=" . $DB->F($_REQUEST['filter_clnt_type']);

        if (!empty($_REQUEST['filter_status_id'])) {
            $filter .= " AND ts.status_id in(" . implode(',', array_map($DB->F, $_REQUEST['filter_status_id'])) . ')';
        }
        if ($_REQUEST['filter_ods_id']) $filter .= " AND ods.id=" . $DB->F($_REQUEST['filter_ods_id']);

        if ($request->get('filter_sc_id', -1) > 0) {
            $filter .= " AND tt.sc_id=" . $request->request->getInt('filter_sc_id');
        } elseif (0 == $request->get('filter_sc_id', -1)) {
            $filter .= " AND (tt.sc_id IS NULL OR tt.sc_id = 0)";
        }

        if ($_REQUEST['filter_ctgrnum_id']) $filter .= " AND lower(tt.clnttnum) LIKE " . $DB->F("%" . strtolower($_REQUEST['filter_ctgrnum_id']) . "%");
        if ($_REQUEST['filter_addr']) $filter .= "AND tt.dom_id IN (SELECT la.id FROM `list_addr` AS la, `kladr_street` AS ks WHERE
                                                 LEFT(la.kladrcode, 15) = LEFT(ks.code, 15) AND ks.name LIKE " . $DB->F($_REQUEST['filter_addr'] . "%") . ")";
        if ($_REQUEST["filter_addr_house"]) $filter .= " AND (addr.name LIKE " . $DB->F(strtolower($_REQUEST['filter_addr_house']) . "%") . " OR addr.althouse=" . $DB->F("%" . strtolower($_REQUEST['filter_addr_house']) . "%") . ")";
        if ($_REQUEST["filter_addr_flat"]) $filter .= " AND tt.kv LIKE " . $DB->F(strtolower($_REQUEST['filter_addr_flat']) . "%");

        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];

        if (($isChiefTech && $isTech) || !$isTech) {

            $_REQUEST['filter_client_phone'] = $this->getCookie('filter_client_phone', $_REQUEST['filter_client_phone']);
            $tpl->setCurrentBlock("is_clnt_phone");
            $tpl->setVariable("FILTER_CLIENT_PHONE", $_REQUEST['filter_client_phone'] ? $_REQUEST['filter_client_phone'] : "");
            $tpl->parse("is_clnt_phone");
            if ($_REQUEST['filter_client_phone']) {
                $phone = preg_replace("/(\D+)/", "", $_REQUEST["filter_client_phone"]);
                $filter .= " AND (`clnt_tel1` LIKE '%" . $phone . "%' OR `clnt_tel2` LIKE '%" . $phone . "%') ";
            }
        } else {
            $_REQUEST['filter_client_phone'] = false;
        }
        $tpl->setVariable("FILTER_DATE_GFX", $_REQUEST['date_gfx'] ? $_REQUEST['date_gfx'] : "");
        $tpl->setVariable('FILTER_ID', htmlspecialchars($_REQUEST['filter_id']));
        $tpl->setVariable("FILTER_ADDR", $_REQUEST['filter_addr'] ? $_REQUEST['filter_addr'] : "");
        $tpl->setVariable("FILTER_ADDR_HOUSE", $_REQUEST['filter_addr_house'] ? $_REQUEST['filter_addr_house'] : "");
        $tpl->setVariable("FILTER_ADDR_FLAT", $_REQUEST['filter_addr_flat'] ? $_REQUEST['filter_addr_flat'] : "");
        $tpl->setVariable('FILTER_CNT_OPTIONS', $this->getContrOptions($_REQUEST['filter_cnt_id']));
        $tpl->setVariable('FILTER_TYPE_OPTIONS', $this->getTypeOptions($_REQUEST['filter_type']));
        $tpl->setVariable('FILTER_CLNT_TYPE_SEL_' . $_REQUEST['filter_clnt_type'], 'selected');
        $tpl->setVariable('FILTER_STATUS_OPTIONS', $this->getStatusOptions($_REQUEST['filter_status_id']));
        $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
        $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
        $tpl->setVariable('FILTER_ODS_OPTIONS', $this->getODSOptions($_REQUEST['filter_ods_id']));
        if (($isTech || $isChiefTech) && !$USER->checkAccess("fullaccesstoallsc")) {
            $sc_id = $isChiefTech ? $isChiefTech : User::getScIdByUserId($g_user_id);
        }

        $tpl->setVariable('CTGRNUM_ID', $_REQUEST['filter_ctgrnum_id']);

        if (!isset($_REQUEST["filter_rpp"])) $_REQUEST["filter_rpp"] = $USER->getRpp();

        if ($_REQUEST["filter_rpp"] != $USER->getRpp()) {
            $sql = "UPDATE `users` SET `rpp`=" . $_REQUEST["filter_rpp"] . " WHERE `id`=" . $DB->F($USER->getId()) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
        } else {
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    ts.id AS TASK_ID,
                    DATE_FORMAT(ts.date_reg, '%d.%m.%Y') AS DATE,
                    ca.contr_title AS CONTR,
                    typ.title AS TYPE,
                    tt.dom_id,
                    tt.clnt_org_name,
                    tt.clnt_fio,
                    tt.clnt_type,
                    st.name AS STATUS_NAME,
                    st.color AS STATUS_COLOR,
                    addr.name, 
                    addr.full_address,
                    addr.is_not_recognized,
                    DATE_FORMAT(ts.lastcmm_date, '%d.%m.%Y %H:%i') AS DATE_LAST,
                    (CASE WHEN COALESCE(users.fio,'')<>'' THEN users.fio ELSE users1.fio END) as fio,
                    addr.name, addr.kladrcode, addr.althouse, gfx.c_date, gfx.startTime, tt.clnttnum, tt.kv, st.tag, ct.contr_ticket_id
                FROM
                    `tasks` AS ts
                    LEFT JOIN `tickets` AS tt ON ts.id=tt.task_id
                    LEFT JOIN `list_contr` AS ca ON tt.cnt_id=ca.id
                    LEFT JOIN (SELECT * FROM `task_users` WHERE `ispoln`=1 GROUP BY task_id) AS tu ON tu.task_id=ts.id
                    LEFT JOIN `task_types` AS typ ON tt.task_type=typ.id
                    LEFT JOIN `task_status` AS st ON ts.status_id=st.id
                    LEFT JOIN `gfx` AS gfx ON gfx.task_id=ts.id
                    LEFT JOIN `list_addr` AS addr ON addr.id=tt.dom_id
                    LEFT JOIN `users` AS users ON users.id=gfx.empl_id
                    LEFT JOIN `users` AS users1 ON users1.id=tu.user_id
                    LEFT JOIN `list_ods` AS ods ON ods.id=addr.ods_id
                    LEFT JOIN `contr_tickets` AS ct ON ct.task_id=ts.id
                WHERE $filter
                  and case when ifnull(tt.sc_id,0) = 0 then 1
                           when exists (select * from user_sc_access usa where usa.user_id = " . $USER->getId() . " and usa.sc_id = tt.sc_id) then 1
                           else 0
                       end = 1
                GROUP BY TASK_ID
                ORDER BY
                    $order 
                LIMIT " . intval($_REQUEST['filter_start']) . ", " . intval($_REQUEST["filter_rpp"]);

        $DB->query($sql);
        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("delticket-head");
        }
        while ($a = $DB->fetch(true)) {

            $resp = $DB->getCell("SELECT user.fio FROM `task_users` AS tu LEFT JOIN `users` AS user ON user.id=tu.user_id WHERE tu.task_id=" . $DB->F($a['TASK_ID']) . " AND tu.ispoln ;");
            if (!$resp) {
                $resp_user = $a['fio'] ? "<i>" . $a['fio'] . "</i>" : false;
            } else {
                $r = false;
                foreach ($resp as $item) {
                    $r[] = "$item";
                }
                $resp_user = implode(", <br/ >", $r);
            }
            if ($a['clnt_type'] == User::CLIENT_TYPE_PHYS) $a['CLIENT'] = $a['clnt_fio'];
            elseif ($a['clnt_type'] == User::CLIENT_TYPE_YUR) $a['CLIENT'] = $a['clnt_org_name'];
            else $a['CLIENT'] = $a['clnt_org_name'] . ', ' . $a['clnt_fio'];

            $a["CONTR_TASK_ID"] = $a['clnttnum'] ? $a['clnttnum'] : "<center style='text-decoration:none !important;'>&mdash;</center>";
            $a["HREF_ORDER"] = $this->getLink('vieworderdoc', "id={$a['TASK_ID']}");
            $a['HREF'] = $this->getLink('viewticket', "task_id={$a['TASK_ID']}");

            //JN Address by one row
            if (!empty($a['full_address'])) {
                if ($a['is_not_recognized']) {
                    $a['FULL_ADDRESS'] = $a['full_address'] . ' <sup>*</sup>';
                } else {
                    $a['FULL_ADDRESS'] = $a['full_address'];
                }

                $a['ADDR_FLAT'] = $a["kv"];
                $tpl->parse("kladr_addr");
            } else {
                $tpl->setCurrentBlock("no_kladr");
                $a['ADDR_NO_KLADR'] = "<font color=#FF0000>Адрес не распознан</font>";
                $tpl->parse("no_kladr");
            }

            if ($a['c_date'] && $a['startTime']) {
                $startHour = (strlen(floor($a['startTime'] / 60)) == 1) ? "0" . (floor($a['startTime'] / 60)) : floor($a['startTime'] / 60);
                $startMin = (strlen(floor($a['startTime'] % 60)) == 1) ? "0" . (floor($a['startTime'] % 60)) : floor($a['startTime'] % 60);
                if (preg_match("/grafik/", $a['tag'])) {
                    $a['GFXTIME'] = rudate("d M Y", strtotime($a['c_date'])) . " " . $startHour . ":" . $startMin;
                }
            } else {
                //$a['GFXTIME'] = "<center>&mdash;</center>";
            }
            $a['USERFIO'] = $resp_user ? $resp_user : "<center>&mdash;</center>";
            $t = new ServiceTicket($a["TASK_ID"]);
            if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getSC")) {
                $scO = new adm_sc_plugin();
                $sc = $scO->getSC($t->getSCId());
            }
            $a['SC'] = $sc['title'] ? $sc['title'] : "<center>&mdash;</center>";
            $a['CONTR_ID'] = $a['clnttnum'] ? $a['clnttnum'] : "<center>&mdash;</center>";
            $tpl->setCurrentBlock('row');
            if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
                $tpl->setCurrentBlock("deleteticket");
                $tpl->setVariable("DELETEHREF", $this->getLink('deleteticket', "task_id={$a['TASK_ID']}"));
                $tpl->parse("deleteticket");
            }
            $tpl->setVariable($a);
            $tpl->parse("row");
        }
        $DB->free();
        $DB->query($sql);
        $total = $DB->getFoundRows();
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable("CURRENT_USER", $USER->getFio());
        $tpl->setVariable("CURRENT_DATE", rudate("H:i d M Y"));

        $DB->free();

        $data = $tpl->get();
        $response = new \Symfony\Component\HttpFoundation\Response($data, 200, [
            'Content-type' => 'application/vnd.ms-excel',
            'Content-disposition' => "filename=service_export_" . date("H_i_d-M-Y") . ".xls"
        ]);

        return $response;
    }

    function updateContrDataA()
    {
        global $DB;
        $task_id = $_POST["task_id"];
        $contr_id = $_POST["contr_id"];
        $type_id = $_POST["type_id"];
        $agr_id = $_POST["agr_id"];
        $err = array();
        if ($task_id && $contr_id && $type_id && $agr_id) {
            $sql = "UPDATE `tickets` SET `cnt_id`=" . $DB->F($contr_id) . ", `task_type`=" . $DB->F($type_id) . ", `agr_id`=" . $DB->F($agr_id) . " WHERE `task_id`=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) $err[] = "error";
            //$sql = "DELETE FROM `list_contr_calc` WHERE `task_id`=".$DB->F($task_id).";";
            //$DB->query($sql);
            //if ($DB->errno()) $err[] = "error";
        } else {
            $err[] = "error";
        }
        echo sizeof($err) ? "error" : "ok";
        return false;

    }

    function deleteticket()
    {
        global $DB, $USER;
        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            if ($id = $_REQUEST["task_id"]) {
                $tmc = $DB->getField("SELECT count(`id`) FROM `tmc_ticket` WHERE `task_id`=" . $DB->F($id) . ";");
                if ($tmc) {
                    UIError("Удаление заявки невозможно, т.к. по ней есть списанные ТМЦ!");
                }
                $cnt = $DB->getField("SELECT count(`id`) FROM `list_skp_tech_calc` WHERE `task_id`=" . $DB->F($id) . ";");
                if ($cnt) {
                    UIError("Удаление заявки невозможно, т.к. по ней выполнен расчет!");
                }
                $err = array();
                $sql = "DELETE FROM `tasks` WHERE `id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_files` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_comments` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `task_users` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `tickets` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                $sql = "DELETE FROM `gfx` WHERE `task_id`=" . $DB->F($id) . ";";
                $DB->query($sql);
                $sql = "DELETE FROM `joinedtasks` WHERE (`task_id`=" . $DB->F($id) . " OR joined_task_id = " . $DB->F($id) . " );";
                $DB->query($sql);
                if ($DB->errno()) $err[] = $DB->error();
                if (!sizeof($err))
                    redirect($this->getLink('viewlist'), "Заявка с ID $id успешно удалена.");
                else
                    redirect($this->getLink('viewlist'), "Удаление заявки с ID $id завершено с ошибками! Обратитесь к разработчикам! " . implode("\r\n", $err));

            } else {
                redirect($this->getLink('viewlist'), "Не указан идентификатор заявки для удаления.");

            }
        } else {
            redirect($this->getLink('viewlist'), "Нет доступа к удалению заявок.");
        }

    }

    function delete_returned_tmc_from_ticket()
    {
        global $USER, $DB;
        $row_id = $_REQUEST["row_id"];
        $task_id = $_REQUEST["task_id"];
        $and_tmc = $_REQUEST['and_tmc'];
        if ($row_id && $task_id) {
            $tmc_task = $DB->getRow("SELECT * FROM `tmc_ticket` WHERE `id`=" . $DB->F($row_id) . ";", true);
            if ($tmc_task) {
                $isontask = $DB->getRow("SELECT * FROM `tmc_ticket` WHERE `tmc_tech_sc_id`=" . $DB->F($tmc_task["tmc_tech_sc_id"]) . " AND !`isreturn`", true);
                if (!$isontask) {
                    $sql = "DELETE FROM `tmc_ticket` WHERE `id`=" . $DB->F($tmc_task["id"]) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) $err[] = $DB->error();
                    $tmc_sc_task = $DB->getRow("SELECT * FROM `tmc_sc_tech` WHERE `id`=" . $DB->F($tmc_task["tmc_tech_sc_id"]) . " AND `tmc_sc_id`>0 LIMIT 1;", true);
                    if ($tmc_sc_task) {
                        $DB->query("DELETE FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $tmc_sc_task["tmc_sc_id"] . " LIMIT 2;");
                        $r_tmc_sc_serial = $DB->getField("SELECT `serial` FROM `tmc_sc` WHERE `id`=" . $DB->F($tmc_sc_task["tmc_sc_id"]) . ";");
                        $r_tmc_id = $DB->getField("SELECT `tmc_id` FROM `tmc_sc` WHERE `id`=" . $DB->F($tmc_sc_task["tmc_sc_id"]) . ";");
                        $tmctitle = $DB->getField("SELECT `title` FROM `list_materials` WHERE `id`=" . $DB->F($r_tmc_id) . ";");
                        if ($r_tmc_sc_serial) {
                            $DB->query("DELETE FROM `tmc_sc` WHERE `serial`=" . $DB->F($r_tmc_sc_serial) . " LIMIT 2;");
                            $tag = "<strong>Удаление ТМЦ с заявки. </strong>";
                            if ($tmctitle) $tag .= " Тип: " . $tmctitle . " ";
                            $tag .= "S/N: " . $r_tmc_sc_serial;
                            if ($r_tmc_id && $and_tmc) {
                                $DB->query("DELETE FROM `list_materials` WHERE `id`=" . $DB->F($r_tmc_id) . " LIMIT 1;");
                                $tag .= " <strong>Удален тип оборудования: </strong> " . $tmctitle;
                            }
                            if (sizeof($err)) {
                                $ret["error"] = implode('\r\n', $err);
                            } else {
                                $ret["ok"] = "ok";
                                $t = new Task($task_id);
                                $t->addComment("", $tag);
                                $ret["comment"] = $t->getLastComment();
                            }
                        } else {
                            $ret["error"] = "Отсутствует запись о выдаче на склад СЦ!";
                        }
                    } else {
                        $ret["error"] = "Отсутствует запись о выдаче на техника!";
                    }
                } else {
                    $ret["error"] = "Вы пытаетесь удалить принятое по данной заявке ТМЦ, предварительно списав его на эту же заявку! Удалить такое ТМЦ нельзя. Сначала откатите списание на заявку, сохраните отчет, а после, - повторите попытку.";
                }
            } else {
                $ret["error"] = "Отсутствует запись о списании на заявку!";
            }
        } else {
            $ret["error"] = "Недостаточно данных для выполнения операции";
        }
        echo json_encode($ret);
        return false;
    }

    function return_tmc_from_ticktet()
    {
        global $DB, $USER;

        $task_id = $_REQUEST["task_id"];
        $tech_id = $_REQUEST["tech_id"];
        $tmccat = $_REQUEST["r_tmccat_inform"];
        $tmc_name_id = $_REQUEST["r_tmc_name_id"];
        $tmc_name = $_REQUEST["new_tmc_name"];
        $barcode = $_REQUEST["new_tmc_barcode"];
        $serial = $_REQUEST["new_tmc_serial"];
        $desc = $_REQUEST["reason"];
        $cmm = $_REQUEST["comment"];

        if (!$tmc_name_id) {
            $sql = "INSERT INTO `list_materials` (`title`, `cat_id`, `bar_code`, `metric_flag`, `metric_title`) VALUES (" . $DB->F($tmc_name) . ", " . $DB->F($tmccat) . ", " . $DB->F($barcode) . ", " . $DB->F(0) . ", " . $DB->F("шт.") . ");";
            $DB->query($sql);
            $tmc_name_id = $DB->insert_id();
            $wasnew = true;
            $tag = "<strong>Добавлен тип ТМЦ: </strong>" . $tmc_name;
            $DB->free();
        } else {
            $wasnew = false;
            $tmc_name = $DB->getField("SELECT `title` FROM `list_materials` WHERE `id`=" . $DB->F($tmc_name_id) . ";");
        }

        $sql = "SELECT * FROM `tmc_sc` WHERE `serial`=" . $DB->F($serial) . " ORDER BY ledit DESC LIMIT 1;";
        $row = $DB->getRow($sql, true);
        $istmc = isset($row['id']) ? $row['id'] : null;
        if (!$istmc || ($row['incoming'] == 0 && $row['inplace'] == 0)) {
            $sc_id = $DB->getField("SELECT `sc_id` FROM `link_sc_user` WHERE `user_id`=" . $DB->F($tech_id) . ";");
            $dismantled = true; // демонтаж любого оборудования

            // демонтаж оборудования костыль для заведения SN в базе ТМЦ на СЦ
            $sql = 'INSERT INTO tmc_sc SET
                      sc_id = ' . $DB->F($sc_id) . ',
                      tmc_id=' . $DB->F($tmc_name_id) . ',
                      `serial` = ' . $DB->F($serial) . ',
                      `count` = ' . $DB->F(1) . ',
                      user_id = ' . $DB->F($USER->getId()) . ',
                      inplace=' . $DB->F(0) . ',
                      dismantled = ' . $DB->F($dismantled) . ',
                      incoming = ' . $DB->F(1) . ', # оприходовано
                      from_task_id=' . $DB->F($task_id) . ',
                      from_tech_id=' . $DB->F($tech_id) . ';';
            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
                echo json_encode($ret);
                return false;
            }
            $tmc_sc_id_f = $DB->insert_id();
            $DB->free();

            $sql = 'INSERT INTO tmc_sc SET
                      sc_id = ' . $DB->F($sc_id) . ',
                      tmc_id=' . $DB->F($tmc_name_id) . ',
                      `serial` = ' . $DB->F($serial) . ',
                      `count` = ' . $DB->F(1) . ',
                      user_id = ' . $DB->F($USER->getId()) . ',
                      inplace=' . $DB->F(0) . ',
                      dismantled = ' . $DB->F($dismantled) . ',
                      incoming = ' . $DB->F(0) . ', # списано
                      from_task_id=' . $DB->F($task_id) . ',
                      from_tech_id=' . $DB->F($tech_id) . ';';

            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
                echo json_encode($ret);
                return false;
            }
            $tmc_sc_id = $DB->insert_id();
            $DB->free();

            $sql = 'INSERT INTO tmc_sc_tech SET
                      sc_id = ' . $DB->F($sc_id) . ',
                      tech_id=' . $DB->F($tech_id) . ',
                      tmc_sc_id = ' . $DB->F($tmc_sc_id) . ',
                      `count` = ' . $DB->F(1) . ',
                      user_id = ' . $DB->F($USER->getId()) . ',
                      ledit=now(),
                      inplace=' . $DB->F(0) . ',
                      dismantled = ' . $DB->F($dismantled) . ',
                      incoming = ' . $DB->F(1) . ' # оприходовано
                      ;';

            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
                echo json_encode($ret);
                return false;
            }
            $DB->free();

            $sql = 'INSERT INTO tmc_sc_tech SET
                      sc_id = ' . $DB->F($sc_id) . ',
                      tech_id=' . $DB->F($tech_id) . ',
                      tmc_sc_id = ' . $DB->F($tmc_sc_id) . ',
                      `count` = ' . $DB->F(1) . ',
                      user_id = ' . $DB->F($USER->getId()) . ',
                      ledit=now(),
                      inplace=' . $DB->F(1) . ', # на руках
                      dismantled = ' . $DB->F($dismantled) . ',
                      incoming = ' . $DB->F(0) . ' # оприходовано
                      ;';
            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
                echo json_encode($ret);
                return false;
            }
            $tmc_tech_sc_id = $DB->insert_id();
            $DB->free();

            $sql = "INSERT INTO `tmc_ticket` SET
                      `task_id`=" . $DB->F($task_id) . ",
                      `tmc_tech_sc_id` = " . $DB->F($tmc_tech_sc_id) . ",
                      `count` = " . $DB->F(1) . ",
                      `tech_id` = " . $DB->F($tech_id) . ",
                      `user_id` = " . $DB->F($USER->getId()) . ",
                      `isreturn` = " . $DB->F(1) . ",
                      `reason_id` = " . $DB->F($desc) . ",
                      `comment_id` = " . $DB->F($cmm) . ",
                      ledit=now()
                      ;";
            $DB->query($sql);
            if ($DB->errno()) {
                $ret["error"] = $DB->error();
                echo json_encode($ret);
                return false;
            }
            $tmc_task_id = $DB->insert_id();
            $DB->free();
            $DB->query("UPDATE `tmc_sc` SET `tmc_ticket_id`=" . $DB->F($tmc_task_id) . " WHERE `id`=" . $DB->F($tmc_sc_id_f) . ";");
            $DB->free();
            $DB->query("UPDATE `tmc_sc` SET `tmc_ticket_id`=" . $DB->F($tmc_task_id) . " WHERE `id`=" . $DB->F($tmc_sc_id) . ";");
            $DB->free();
            if ($tmc_task_id) {
                $user = $DB->getField("SELECT `fio` FROM `users` WHERE `id`=" . $DB->F($tech_id) . ";");
                if ($wasnew) $add = "wasnew='1'";
                $ret["tpl"] = "<tr id=\"$tmc_task_id\" class='r_tmc_rows rtmc_$tmc_task_id newrtmcid newtmc_id_$tmc_task_id'><td align='center'>" . $tmc_name . "</td><td align='center'>" . $serial . "</td><td>" . $user . "</td><td><a $add id='$tmc_task_id' task_id='$task_id' class='removertmc' href='#'><img src='/templates/images/cross.png' /></a></td></tr>";
                $ret["ok"] = "ok";
                $t = new Task($task_id);
                $t->addComment("", "<strong>Прием ТМЦ с заявки: </strong>" . $tmc_name . ", S/N: " . $serial . " " . $tag);
                $ret["comment"] = $t->getLastComment();
            }
        } else {
            $ret["error"] = "Эта ТМЦ уже находится на нашем складке!";
        }
        echo json_encode($ret);
        return false;
    }

    function viewticket(Request $request)
    {
        /**
         * Кнопка лояльности отображается только на статусе назначена в график
         * Отображается всем ролям
         * После первого сохранения опрос доступен на редактирования только по отдельному праву
         */

        global $DB, $USER;
        $id = (int)($_GET['task_id'] ? $_GET['task_id'] : ($_GET['id'] ? $_GET['id'] : "0"));
        if (!$id) UIError("Не указан идентификатор заявки");

        if ($id > 0) {
            $sql = "SELECT max(sc.title) title
                        FROM list_sc sc,
                        tickets tt,
                        tasks t
                        WHERE tt.task_id = " . $id . "
                          AND t.id = tt.task_id
                          AND sc.id = tt.sc_id
                          AND t.plugin_uid = '" . $this->getUID() . "'
                          AND CASE WHEN ifnull(tt.sc_id,0) = 0 THEN 0
                                   WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = tt.sc_id) THEN 0
                                   ELSE 1
                              END = 1";
            $sc_with_no_access = $DB->getField($sql);
            if (strlen($sc_with_no_access) > 0) {
                #$sc_with_no_access .= $sql;
                UIError("У Вас нет доступа на СЦ " . $sc_with_no_access . ", просмотр заявки невозможен.");
            }
        }

        $ticket = new ServiceTicket($id);

        $LKUser = adm_users_plugin::isLKUser($USER->getId());
        if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
            $tech_id = $DB->getField("SELECT `empl_id` FROM `gfx` WHERE `task_id`=" . $DB->F($ticket->getId()) . ";");
            $vTech = adm_empl_plugin::isVirtual($USER->getId());
            if (array_key_exists($USER->getId(), $ticket->getIspoln()) || array_key_exists($vTech, $ticket->getIspoln()) || $tech_id == $USER->getId()) {
            } else {
                UIError("В доступе отказано!");
            }
        }
        if (!$ticket->getId()) UIError("Заявка с таким номером не найдена! Возможно, она была удалена.");
        $modelTask = \wf::$em->find(TaskEntity::class, $id);
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($LKUser) {
            $tpl->loadTemplatefile($USER->getTemplate() . "/ticket_lk.tmpl.htm");
        } else {
            if (($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId()))) {
                $tpl->loadTemplatefile($USER->getTemplate() . "/ro_ticket.tmpl.htm");
            } else {
                if (!$USER->checkAccess("edit_closed_skp_task", User::ACCESS_WRITE) && ($ticket->getStatusId() == 25)) {
                    $tpl->loadTemplatefile($USER->getTemplate() . "/ro1_ticket.tmpl.htm");
                } else {
                    $tpl->loadTemplatefile($USER->getTemplate() . "/ticket.tmpl.htm");
                }
            }
            if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
                if (array_key_exists($USER->getId(), $ticket->getIspoln())) {
                    if ($ticket->getStatusId() == 44) {
                        $accepted = false;
                    } else {
                        $accepted = true;
                    }
                } else $accepted = true;
            }
        }

        /**
         * check lk user access to task
         */
        if (null !== $this->getUserModel()->getMasterPartner() &&
            (
                !$this->getUserModel()->getAllowAccessCu() || $this->getUserModel()->getMasterPartner() !== $modelTask->getAnyTicket()->getPartner()
            )
        ) {
            throw new AccessDeniedException('У вас нет доступа к этой заявке!');
        }

        $tpl->setVariable("REASON_LIST", adm_gp_reason_plugin::getOptions());
        $tpl->setVariable("COMMENT_LIST", adm_gp_comment_plugin::getOptions());
        if ($ticket->isTest()) {
            $tpl->touchBlock("test-ticket");
        }
        $sql = "SELECT COUNT(*) FROM `contr_tickets` WHERE `task_id`=" . $DB->F($ticket->getId());
        if ($DB->getField($sql)) {
            $tpl->setVariable('IS_CONTR_READONLY', 'readonly');
        }
        if (!$accepted) {
            $tpl->setVariable("NOTACCEPTED", "style='display:none;'");
        } else {
            $tpl->setVariable("ACCEPTED", "style='display:none;'");

        }
        if ($USER->checkAccess("virt_delticket", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("deleteticket");
            $tpl->setVariable("DEL_TASK_ID", $_GET["task_id"]);
            $tpl->setVariable("PLUG_UID", $this->getUID());

            $tpl->parse("deleteticket");
        }
        if ($USER->checkAccess("callviewer") || $USER->checkAccess("plk.service.view.calls")) {
            $tpl->touchBlock("callticket");
        }
        if ($USER->checkAccess("close_skp_task", User::ACCESS_WRITE) && $ticket->getStatusId() == 24) {
            $tpl->setCurrentBlock("closeticket");
            $tpl->setVariable("CL_TASK_ID", $_GET["task_id"]);
            $tpl->setVariable("PLUG1_UID", $this->getUID());
            $tpl->parse("closeticket");
        }

        # EF: Доступ на кнопку с анкетой лояльности и опросником
        if ($USER->checkAccess("services_questionnarie_access", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("questionnarie");
            $tpl->setVariable("QU_TASK_ID", $_GET["task_id"]);
            $tpl->parse("questionnarie");
        }


        /**
         * Отрисовка кнопки и блока связанных заявок
         */
        \WF\Legacy\TicketViewModificator::renderAll(new Task($id), $tpl);

        $rejectList = array(10, 46, 43, 5, 65);
        $doneList = array(61, 54, 52, 1, 26, 23, 12, 25, 24, 49);
        $currentStatus = $ticket->getStatusId();
        if ($USER->checkAccess("qc", User::ACCESS_WRITE)) {
            if (in_array($currentStatus, $rejectList) || in_array($currentStatus, $doneList)) {
                $tpl->setCurrentBlock("opros");
                $tpl->setVariable("OPROS_TASK_ID", $id);
                $tpl->setVariable("PL_UID", $ticket->getPluginUid());
                $tpl->setVariable("TASK_STATUS_ID", $currentStatus);
                $tpl->parse("opros");
            }
        }
        $tpl->setVariable('CONTR', $ticket->getContrName());
        $tpl->setVariable('CONTRID', $ticket->getContrId());
        $tpl->setVariable('TASK_TYPE', $ticket->getTypeName());

        if ($ticket->getDomId()) {
            $tpl->setCurrentBlock("kladr");

            $not_recognised_status = $DB->getField("SELECT `is_not_recognized` FROM list_addr WHERE `id` =" . $ticket->getDomId() . ";");
            $ticketEntity = $modelTask->getAnyTicket();

            if (!$ticketEntity instanceof \WF\Task\Model\TechnicalTicketInterface) {
                throw new Exception('Wrong ticket type!');
            }

            if ($not_recognised_status) {
                $tpl->setVariable('ADDR', "<i style='color:#FF0000' title='Адрес не распознан'>" . $ticketEntity->getDom()->getFullAddress() . "</i>");
            } else {
                $tpl->setVariable('ADDR', $ticketEntity->getDom()->getFullAddress());
            }

            $tpl->setVariable('LINK_TO_ADDR', "addr_interface/edit?id=" . $ticket->getDomId());
            $tpl->parse("kladr");
            if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getById")) {
                $addr = new addr_interface_plugin();
                $address = $addr->getById($ticket->getDomId());
                $tpl->setVariable("CURRENT_KLADR_CODE", $address["althouse"] != "" ? $address["kladrcode"] : $address["kladrcode"] . "_" . $address["name"]);
                $tpl->setVariable("CURRENT_HOUSE", $address["althouse"] != "" ? $address["kladrcode"] : $address["kladrcode"] . "_" . $address["name"]);
                $tpl->setVariable("CURRENT_SKC", substr($address["kladrcode"], 0, 11) . "00");
                $tpl->setVariable("CURRENT_REGION_CODE", substr($address["kladrcode"], 0, 11) . "00");
                $currentDefaultRegion = substr($address["kladrcode"], 0, 11) . "00";
                $tpl->setVariable("ALT_HOUSE", $address["althouse"]);
            }
        } else {
            $tpl->setCurrentBlock("no_kladr");
            $tpl->setVariable('NO_ADDR', $ticket->getAddr($ticket->getDomId()));
            $tpl->parse("no_kladr");
            $tpl->setVariable("CURRENT_REGION_CODE", getcfg('default_region'));
            $tpl->setVariable("CURRENT_KLADR_CODE", getcfg('default_region'));
        }
        $tpl->setVariable("DOM_ENTR", $address["entr_count"] ? $address["entr_count"] : " - ");
        $tpl->setVariable("DOM_FLOORS", $address["floorcount"] ? $address["floorcount"] : " - ");
        $tpl->setVariable("DOM_FLATS", $address["flatcount"] ? $address["flatcount"] : " - ");
        $tpl->setVariable("DOM_OPLIST", "<strong> - </strong>");

        if ($USER->checkAccess("virt_editticket", User::ACCESS_WRITE) || $USER->checkAccess("skpallowodsandaddrparams", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("edit-addr-params");
            if (!$ticket->getDomId()) {

                $tpl->setVariable("ADDR_ENABLED", "disabled=\"disabled\"");
            } else {
                $tpl->setVariable("ADDR_ENABLED", "");

            }
            $tpl->parse("edit-addr-param");
        }
        $tpl->setVariable('DOM_ID', $ticket->getDomId());
        if ($USER->checkAccess("virt_editticket", User::ACCESS_WRITE)) {
            $tpl->touchBlock("edit-addr");
        }
        $tpl->setVariable("EXTTYPES", adm_ticket_rtsubtypes_plugin::getOptions($ticket->getExtType()));
        $tpl->setVariable('SC_OPTIONS', $this->getSCOptions($ticket->getSCId()));
        $tpl->setVariable('SC_OPTIONS_VAL', $this->getSCName($ticket->getSCId()));
        $tpl->setVariable('POD', $ticket->getPod() ? $ticket->getPod() : " - ");
        $tpl->setVariable('ETAZH', $ticket->getEtazh() ? $ticket->getEtazh() : " - ");
        $tpl->setVariable('KV', $ticket->getKv() ? $ticket->getKv() : " - ");
        $tpl->setVariable('DOMOFON', $ticket->getDomofon() ? $ticket->getDomofon() : " - ");
        $tpl->setVariable('ADDINFO', $ticket->getAddInfo());
        $tpl->setVariable('SWIP', $ticket->getSwIp());
        $tpl->setVariable('SWPORT', $ticket->getSwPort());
        $tpl->setVariable('PRICE', $ticket->getPrice() != "0.00" ? $ticket->getPrice() : "");
        $tpl->setVariable('ORIENT_PRICE', $ticket->getOrientPrice() != "0.00" ? $ticket->getOrientPrice() : "");
        $tpl->setVariable('SWPLACE', $ticket->getSwPlace());
        $tpl->setVariable('CLNTOPAGR', $ticket->getClntOpAgr());
        $tpl->setVariable('CLNTTNUM', $ticket->getClntTNum() ? $ticket->getClntTNum() : $DB->getField("SELECT `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=" . $DB->F($_GET["task_id"]) . ";"));
        if ($ticket->getContrName() == "МГТС" && $ticket->getTypeName() == "СКП") {
            $tpl->setCurrentBlock("openinasrz");
            $tpl->setVariable("CLNTTNUM_I", $ticket->getClntTNum());
            $tpl->parse("openinasrz");
        }


        $ods = $this->getOds($ticket->getDomId());
        if ($USER->checkAccess("virt_editticket", User::ACCESS_WRITE) || $USER->checkAccess("skpallowodsandaddrparams", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("edit-ods");
            if (!$ods["id"]) {
                $tpl->setVariable("ODS_ENABLED", "disabled=\"disabled\"");
            } else {
                $tpl->setVariable("ODS_ENABLED", "");
            }
            $tpl->parse("edit-ods");
        }
        if ($ods['id']) {
            $tpl->setVariable("ODS_ID", $ods["id"]);
        }
        $tpl->setVariable("ODS_ID", $ods["id"] ? $ods["id"] : "");
        $tpl->setVariable("ODS_NAME", $ods["title"] ? $ods["title"] : "не указана");
        $tpl->setVariable("ODS_ADDR", $ods["address"] ? $ods["address"] : "-");
        $tpl->setVariable("ODS_TEL", $ods["tel"] ? $ods["tel"] : "-");
        $tpl->setVariable("ODS_CONTACTS", $ods["fio"] ? $ods["fio"] : "-");
        $tpl->setVariable("ODS_COMMENT", $ods["comment"] ? $ods["comment"] : "-");

        if ($ticket->getStatusId() == 42 && (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()))) {
            $tpl->setVariable('FIO', "данные скрыты");
            $tpl->setVariable("LINE_CONTACT", "данные скрыты");
            $tpl->setVariable('PASSP_SN', "данные скрыты");
            $tpl->setVariable('PASSP_VID', "данные скрыты");
            $tpl->setVariable('PASSP_VIDDATE', "данные скрыты");
            $tpl->setVariable("DOC_NUM", "данные скрыты");
            $tpl->setVariable('PHONE1', "данные скрыты");
            $tpl->setVariable('PHONE2', "данные скрыты");
        } else {
            $tpl->setVariable('FIO', $ticket->getFio());
            $tpl->setVariable("LINE_CONTACT", $ticket->getLineContact());
            $tpl->setVariable('PASSP_SN', $ticket->getPasspSN());
            $tpl->setVariable('PASSP_VID', $ticket->getPasspVidan());
            $tpl->setVariable('PASSP_VIDDATE', $ticket->getPasspVidanDate());
            $tpl->setVariable("DOC_NUM", $ticket->getTicketDocNum());
            $phnes = $ticket->getPhones();

            if ($USER->checkAccess("hidetelnum")) {
                $tpl->setVariable('PHONE1', "");
            } else {
                $tpl->setVariable('PHONE1', ($phnes[0] ? $phnes[0] : '+7'));
            }
            if ($phnes[0]) {
                if ($USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId()))) && $USER->getIntPhone()) {
                    if ($phnes[0] != "70000000000") {
                        $tpl->setCurrentBlock("calltoclient1");
                        $tpl->setVariable('CLNT1PHONE', $phnes[0]);
                        $tpl->setVariable("EXTNUM1", $USER->getIntPhone());
                        $tpl->parse("calltoclient1");
                    }
                } else {
                    if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()) && $USER->getIntPhone()) {
                        if ($phnes[0] != "70000000000") {
                            $tpl->setCurrentBlock("calltoclient1tech");
                            $tpl->setVariable('CLNT1PHONETECH', $phnes[0]);
                            $tpl->setVariable("EXTNUM1TECH", $USER->getIntPhone());
                            $tpl->parse("calltoclient1tech");
                        }
                    }
                }

            }
            if ($USER->checkAccess("hidetelnum")) {
                $tpl->setVariable('PHONE2', "");
            } else {
                $tpl->setVariable('PHONE2', ($phnes[1] ? $phnes[1] : '+7'));
            }
            if ($phnes[1]) {
                if ($USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId()))) && $USER->getIntPhone()) {
                    if ($phnes[1] != "70000000000") {
                        $tpl->setCurrentBlock("calltoclient2");
                        $tpl->setVariable('CLNT2PHONE', $phnes[1]);
                        $tpl->setVariable("EXTNUM2", $USER->getIntPhone());
                        $tpl->parse("calltoclient2");
                    }
                } else {
                    if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId()) && $USER->getIntPhone()) {
                        if ($phnes[1] != "70000000000") {
                            $tpl->setCurrentBlock("calltoclient2tech");
                            $tpl->setVariable('CLNT2PHONETECH', $phnes[1]);
                            $tpl->setVariable("EXTNUM2TECH", $USER->getIntPhone());
                            $tpl->parse("calltoclient2tech");
                        }
                    }
                }
            }
        }
        $tpl->setVariable("TMC_CAT_LIST_INFORM", adm_material_cats_plugin::getOptions());
        $tpl->touchBlock('client_type_' . $ticket->getClientType());
        if ($ticket->getClientType() != User::CLIENT_TYPE_PHYS) {
            $tpl->setCurrentBlock("org");
            $tpl->setVariable('ORG_NAME', $ticket->getOrgName());
            if (!$accepted) {
                $tpl->setVariable("ORGNOTACCEPTED", "style='display:none;'");
            }
            $tpl->parse("org");
        }


        $tpl->setVariable("TASK_FROM", adm_ticket_sources_plugin::getOptions($ticket->getTaskFrom()));
        if ($LKUser) {
            $tpl->setVariable("TASK_FROM_TITLE", adm_ticket_sources_plugin::getById($ticket->getTaskFrom()));
            $sc_name = adm_sc_plugin::getSC($ticket->getSCId());
            $tpl->setVariable("SC_NAME", $sc_name["title"] ? $sc_name["title"] : "&mdash;");
            $tpl->setVariable("SC_LK_ID", $ticket->getSCId());
        } else {
            $tpl->setVariable("SC_LIST", adm_sc_plugin::getScList($ticket->getSCId()));
            $tpl->setVariable("TASK_FROM", adm_ticket_sources_plugin::getOptions($ticket->getTaskFrom()));
        }

        $rejTitle = '';
        $rej_id = $ticket->getRejectReasonId(false);
        if ($rej_id) {
            if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejReasonTitle")) {
                $rej = new adm_rejects_plugin();
                $rejTitle = " (" . $rej->getRejReasonTitle($rej_id) . ")";
            }
        }
        if (User::isTech($USER->getId()) && $USER->getIntPhone()) {
            $tpl->setCurrentBlock("calltogssdisp");
            $tpl->setVariable("DISPGROUPTECHPHONE", getcfg('dispgroupnumber'));
            $tpl->setVariable("TPGSSEXTNUM1TECH", $USER->getIntPhone());
            $tpl->setVariable("TPGSSTECHPHONE", getcfg('dispgroupnumber'));
            $tpl->parse("calltogssdisp");
        }
        $agr_id = $ticket->getAgrId(false);
        if ($agr_id) {
            $phoneList = agreements_plugin::getTPPhones($agr_id);

            if ($phoneList) {
                $tpl->setCurrentBlock("tpnumlist");
                if (User::isTech($USER->getId())) {
                    foreach ($phoneList as $tpi) {
                        $tpl->setCurrentBlock("tech_tnl");
                        $tpl->setVariable("TPTECHPHONE", $tpi["phone"]);
                        if ($tpi["ext"]) {
                            $tpl->setVariable("TPTECHDOB", $tpi["ext"]);
                            $tpl->setVariable("TPTECHDOBTITLE", " доб. " . $tpi["ext"]);
                        }
                        if ($tpi["phonetitle"]) {
                            $tpl->setVariable("BUTTONTITLE", $tpi["phonetitle"]);
                        } else {
                            $tpl->setVariable("BUTTONTITLE", "Позвонить в ТП");
                        }
                        $tpl->setVariable("TPEXTNUM1TECH", $USER->getIntPhone());
                        $tpl->parse("tech_tnl");
                    }
                } else {
                    if ($USER->checkAccess("callout")) {
                        foreach ($phoneList as $tpi) {
                            $tpl->setCurrentBlock("disp_tnl");
                            $tpl->setVariable("TPDISPPHONE", $tpi["phone"]);
                            if ($tpi["ext"]) {
                                $tpl->setVariable("TPDISPDOB", $tpi["ext"]);
                                $tpl->setVariable("TPDISPDOBTITLE", " доб. " . $tpi["ext"]);
                            }

                            if ($tpi["phonetitle"]) {
                                $tpl->setVariable("BUTTONTITLE", $tpi["phonetitle"]);
                            } else {
                                $tpl->setVariable("BUTTONTITLE", "Позвонить в ТП");
                            }
                            $tpl->setVariable("TPDISPEXT", $USER->getIntPhone());
                            $tpl->parse("disp_tnl");
                        }

                    }
                }
                $tpl->parse("tpnumlist");
            }
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getAgrById")) {
                $kontr = new kontr_plugin();
                $agr = $kontr->getAgrById($agr_id);
                if ($agr) {
                    $tpl->setVariable("TASK_AGR", $agr["number"] . " от " . $agr["agr_date"]);
                } else {
                    $tpl->setVariable("TASK_AGR", "ошибка");
                }
                $agr_contacts = $kontr->getAgrById($agr_id);
                if ($agr_contacts) {
                    $tpl->setVariable("AGR_ACCESS", $agr_contacts['access_contact'] ? nl2br($agr_contacts['access_contact']) : "<strong>-</strong>");
                    $tpl->setVariable("AGR_TP", $agr_contacts['tp_contact'] ? nl2br($agr_contacts['tp_contact']) : "<strong>-</strong>");

                } else {
                    $tpl->setVariable("AGR_ACCESS", "<strong>ошибка</strong>");
                    $tpl->setVariable("AGR_TP", "<strong>ошибка</strong>");
                }
            }
        } else {
            $tpl->setVariable("TASK_AGR", "<strong>не указан</strong>");
            $tpl->setVariable("AGR_ACCESS", "<strong>-</strong>");
            $tpl->setVariable("AGR_TP", "<strong>-</strong>");
        }
        $tpl->setVariable('STATUS_NAME', $ticket->getStatusName() . " " . $rejTitle);

        $tpl->setVariable('STATUS_COLOR', $ticket->getStatusColor());
        $tpl->setVariable('DATE_REG', $ticket->getDateReg());
        $tpl->setVariable("USER_REG", $ticket->getAuthorFio() ? $ticket->getAuthorFio() : 'Система');
        $csts = ($ticket->getStatusId() == 44 /*|| $ticket->getStatusId() == 42*/) ? true : false;
        $forbidComments = ($csts && $USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) ? true : false;
        if (!$forbidComments)
            $tpl->setVariable('TASK_COMMENTS', $ticket->getComments());
        else {
            $tpl->setVariable("TEMP_ISROSTEL", "style='display:none;'");
        }
        $tpl->setVariable('TASK_ID', $ticket->getId());

        $ispolnListTech = $DB->getCell2("SELECT us.id, us.fio FROM `task_users` AS tu JOIN `users` AS us ON tu.user_id=us.id WHERE tu.task_id=" . $DB->F($id) . " AND tu.ispoln AND us.id IN (SELECT `user_id` FROM `link_sc_user` WHERE 1) ORDER BY us.id");
        if (!$ispolnListTech) {
            $tpl->setVariable("TECH_LIST_INFORM_TECH", "<option value=''>-- нет исполнителей --</option>");
        } else {
            $tpl->setVariable("TECH_LIST_INFORM_TECH", array2options($ispolnListTech));
        }
        $tpl->setVariable("R_TMC_CAT_LIST_INFORM", adm_material_cats_plugin::getOptions_nonmetric());


        $sql = "SELECT gfx.c_date, gfx.startTime, user.fio, user.id FROM `gfx` AS gfx LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.task_id=" . $DB->F($_GET['task_id']) . ";";
        $result = $DB->getRow($sql);
        if ($result) {

            $startHour = (strlen(floor($result[1] / 60)) == 1) ? "0" . (floor($result[1] / 60)) : floor($result[1] / 60);
            $startMin = (strlen(floor($result[1] % 60)) == 1) ? "0" . (floor($result[1] % 60)) : floor($result[1] % 60);
            $tpl->setVariable("GFX_DATETIME", rudate("d M Y", strtotime($result[0])) . " " . $startHour . ":" . $startMin);
            $tpl->setVariable("GFX_EMPL", $result[2]);
            $emplPhone = adm_users_plugin::getUserPhone($result[3]);
            $emplPhone = explode(";", $emplPhone);
            if (count($emplPhone) > 1) {
                $phone1 = $emplPhone[0];
                $phone2 = $emplPhone[1];
                if ($phone1) {
                    $phone_clean = preg_replace("/[^0-9]/", "", $phone1);

                    if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
                    {
                        $phone_clean = $phone_clean;
                    } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
                    {
                        $phone_clean = substr($phone_clean, 1);
                    } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
                    {
                        $phone_clean = $phone_clean;
                    } else {
                        $phone_clean = false;
                    }
                    $phone1 = $phone_clean;
                    if ($phone1 && $USER->getIntPhone() && $USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId())))) {
                        $tpl->setVariable("calltotech");
                        $tpl->setVariable("TECHPHONE", $phone1);
                        $tpl->setVariable("EXT3NUM", $USER->getIntPhone());
                        $ctc_tech = new User($result[3]);
                        if ($ctc_tech) {
                            $ctc_phone = $ctc_tech->getIntPhone();

                            $targetPhone = false;

                            if ($ctc_phone && $phnes[0] && $phnes[0] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[0]);
                                $tpl->parse("ctc_button");
                            }
                            if ($ctc_phone && $phnes[1] && $phnes[1] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[1]);
                                $tpl->parse("ctc_button");
                            }
                        }
                        $tpl->parse("calltotech");
                    }
                }
                if ($phone2) {
                    $phone_clean = preg_replace("/[^0-9]/", "", $phone2);

                    if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
                    {
                        $phone_clean = $phone_clean;
                    } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
                    {
                        $phone_clean = substr($phone_clean, 1);
                    } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
                    {
                        $phone_clean = $phone_clean;
                    } else {
                        $phone_clean = false;
                    }
                    $phone2 = $phone_clean;
                    if ($phone2 && $USER->getIntPhone() && $USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId())))) {
                        $tpl->setVariable("calltotech");
                        $tpl->setVariable("TECHPHONE", $phone2);
                        $tpl->setVariable("EXT3NUM", $USER->getIntPhone());
                        $ctc_tech = new User($result[3]);
                        if ($ctc_tech) {
                            $ctc_phone = $ctc_tech->getIntPhone();

                            $targetPhone = false;

                            if ($ctc_phone && $phnes[0] && $phnes[0] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[0]);
                                $tpl->parse("ctc_button");
                            }
                            if ($ctc_phone && $phnes[1] && $phnes[1] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[1]);
                                $tpl->parse("ctc_button");
                            }
                        }
                        $tpl->parse("calltotech");
                    }
                }
            } else {
                if ($emplPhone[0]) {
                    $phone_clean = preg_replace("/[^0-9]/", "", $emplPhone[0]);

                    if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
                    {
                        $phone_cslean = $phone_clean;
                    } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
                    {
                        $phone_clean = substr($phone_clean, 1);
                    } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
                    {
                        $phone_clean = $phone_clean;
                    } else {
                        $phone_clean = false;
                    }
                    $phone = $phone_clean;
                    if ($phone && $USER->getIntPhone() && $USER->checkAccess("callout") && (!User::isTech($USER->getId()) || (User::isTech($USER->getId()) && User::isChiefTech($USER->getId())))) {
                        $tpl->setVariable("calltotech");
                        $tpl->setVariable("TECHPHONE", $phone);
                        $tpl->setVariable("EXT3NUM", $USER->getIntPhone());
                        $ctc_tech = new User($result[3]);
                        if ($ctc_tech) {
                            $ctc_phone = $ctc_tech->getIntPhone();

                            $targetPhone = false;

                            if ($ctc_phone && $phnes[0] && $phnes[0] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[0]);
                                $tpl->parse("ctc_button");
                            }
                            if ($ctc_phone && $phnes[1] && $phnes[1] != "70000000000") {
                                $tpl->setCurrentBlock("ctc_button");
                                $tpl->setVariable("CTC_TECHEXT", $ctc_phone);
                                $tpl->setVariable("CTC_CLIENTNUMBER", $phnes[1]);
                                $tpl->parse("ctc_button");
                            }
                        }
                        $tpl->parse("calltotech");
                    }
                }
            }
            if ($ticket->getStatusId() != 45 && $ticket->getStatusId() != 50) {
                $tpl->touchBlock("insertinactive");
            }
        } else {
            $tpl->setVariable("GFX_DATETIME", "НЕ НАЗНАЧЕН");
            $tpl->setVariable("GFX_EMPL", "НЕ НАЗНАЧЕН");
            $tpl->touchBlock("removeinactive");
            $tpl->touchBlock("resetinactive");
        }
        $tpl->setVariable('TYPE_OPTIONS', $this->getTypeOptions($this->getTypeID($ticket->getId())));
        $tpl->setVariable('CNT_OPTIONS', $this->getContrOptions($this->getContrID($ticket->getId())));

        if (class_exists("adm_ods_plugin", true) && method_exists("adm_ods_plugin", "getOptList")) {
            $addr = new adm_ods_plugin();
            $tpl->setVariable("ODS_OPTIONS", $addr->getOptList());
        }
        if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
            if ($ticket->getStatusId() != 45 && $ticket->getStatusId() != 50 && $ticket->getStatusId() != 9 && $ticket->getStatusId() != 47) {
                $tpl->touchBlock("insertinactive");
                $tpl->touchBlock("printinactive");

            }
            if ($ticket->getStatusId() != 9) {
                $tpl->touchBlock("removeinactive");
                $tpl->touchBlock("resetinactive");
            }
            $tpl->touchBlock("resetinactive");
            $tpl->setVariable("NOACCESSTODELETEFROMGFX", "style='display:none;'");
            $tpl->setVariable("ISTECH_DISABLED", "style='display: none;'");
        }

        if ($USER->checkAccess("changedonedate_skp", User::ACCESS_WRITE)) {
            $donedate = $ticket->getDoneDate();
            if ($donedate && $donedate != "00.00.0000") {
                $tpl->setCurrentBlock("editdonedate");
                $tpl->setVariable("DONEDATE", $donedate);
                $tpl->parse("editdonedate");
            } //insertinactive
        }
        $ticket_act = $ticket->getTICKActId();
        if ($ticket_act) {
            $actdate = $DB->getField("SELECT DATE_FORMAT(`cr_date`, '%H:%i %d.%m.%Y') FROM `list_tskpdata_reports` WHERE `id`=" . $DB->F($ticket_act) . ";");
            $tpl->setVariable("ACTDATA", $ticket_act . " от " . $actdate);
        } else {
            $tpl->setVariable("ACTDATA", "&mdash;");
        }
        // подрядчик начало
        $podr_id = $ticket->getPodrId();
        if ($USER->checkAccess('services2podr', User::ACCESS_WRITE)) {
            if ($podr_id) {
                $tpl->setVariable('IS_PODR_DIS', 'disabled');
                $tpl->touchBlock('is_podr_fields');
            }
        } else {
            $tpl->setVariable('PODR_DIS', 'disabled');
        }
        $tpl->setVariable('PODR_OPTIONS', $this->getPodrOptions($podr_id));
        // подрядчик конец

        if ($ticket->getIsInMoney()) {
            $tpl->setVariable("MONEYACCEPT_DATE", $ticket->getTicketMoneyDate());
            $sql = "SELECT `ammount` FROM `skp_money` WHERE `last` AND `task_id`=" . $DB->F($ticket->getId()) . ";";
            $ammount = $DB->getField($sql);
            $tpl->setVariable("PRICE", $ammount);
            $tpl->setVariable("MONEY_AMMOUNT", number_format($ammount, 2, '.', ' '));
            $tpl->setVariable("MONEY_DOCNUM", $ticket->getTicketDocNum());

        } else {
            $tpl->setVariable("MONEYACCEPT_DATE", "&mdash;");
            $tpl->setVariable("MONEY_DOCNUM", $ticket->getTicketDocNum() ? $ticket->getTicketDocNum() : "&mdash;");
            $tpl->setVariable("MONEY_AMMOUNT", "&mdash;");
        }
        $currentStatus = $ticket->getStatusId();

        $new_status = adm_statuses_plugin::getStatusByTag("new", "services");
        $report_status = adm_statuses_plugin::getStatusByTag("report", "services");
        $done_status = adm_statuses_plugin::getStatusByTag("done", "services");

        $tpl->setVariable("OP_DATE_FROM", $ticket->getSlotDateBegin() != "00.00.0000" ? $ticket->getSlotDateBegin() : "");
        $tpl->setVariable("OP_DATE_TO", $ticket->getSlotDateEnd() != "00.00.0000" ? $ticket->getSlotDateEnd() : "");
        $tpl->setVariable("OP_TIME_FROM", $ticket->getSlotTimeBegin() != "00:00" ? $ticket->getSlotTimeBegin() : "");
        $tpl->setVariable("OP_TIME_TO", $ticket->getSlotTimeEnd() != "00:00" ? $ticket->getSlotTimeEnd() : "");
        if ($ticket->getDocID() && $ticket->getDocDate()) {
            $tpl->setVariable("DOC_TICKET_PARAMS", $ticket->getDocID() . " от " . $ticket->getDocDate());
        } else {
            $tpl->setVariable("DOC_TICKET_PARAMS", "&mdash;");
        }
        if ($ticket->getTMCDocID() && $ticket->getTMCDocDate()) {
            $tpl->setVariable("DOC_TMC_PARAMS", $ticket->getTMCDocID() . " от " . $ticket->getTMCDocDate());
        } else {
            $tpl->setVariable("DOC_TMC_PARAMS", "&mdash;");
        }


        if (($currentStatus == $new_status["id"] || $currentStatus == $done_status["id"] || $currentStatus == $report_status["id"]) && $USER->checkAccess("edit_contr_ticket_service", User::ACCESS_WRITE)) {
            $tpl->touchBlock("changecontr");
        }
        UIHeader($tpl);
        $tpl->show();
    }

    static function getSCName($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_sc` WHERE `id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function getOds($dom_id)
    {
        global $DB;
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getODSId")) {
            $addr = new addr_interface_plugin();
            $ods_id = $addr->getODSId($dom_id);
        } else return false;
        $sql = "SELECT * FROM `list_ods` WHERE `id`=" . $DB->F($ods_id) . ";";
        return $DB->getRow($sql, true);
    }

    static function getTypeID($id, $escape = true)
    {
        global $DB;
        $plugin_uid = basename(__FILE__, '.php');
        $sql = "SELECT `task_type` FROM `tickets` WHERE `task_id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function getContrID($id, $escape = true)
    {
        global $DB;
        $sql = "SELECT `cnt_id` FROM `tickets` WHERE `task_id`=" . $DB->F($id);
        return $DB->getField($sql, $escape);
    }

    static function getPodrOptions($sel_id = 0)
    {
        global $DB;
        $sql = "SELECT `id`, `contr_title` FROM `list_contr` AS lc JOIN `podr` AS p ON lc.id=p.contr_id WHERE p.user_id ORDER BY `contr_title`";
        return array2options($DB->getCell2($sql), $sel_id, false);
    }

    function checktaskstatus_ajax()
    {
        global $DB;
        if ($task_id = $_POST['task_id']) {
            $task = new Task($task_id);
            $currentStatus = $DB->getField("SELECT `status_id` FROM `tasks` WHERE `id`=" . $DB->F($task_id) . ";");
            $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("services") . " AND `tag`=" . $DB->F("report") . ";";
            $status = $DB->getField($sql);
            if ($status == $currentStatus) {
                $ret = "ok";
            } else {
                $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("services") . " AND `tag` LIKE " . $DB->F("%grafik%") . ";";
                $status = $DB->getField($sql);
                if ($status == $currentStatus) {
                    $ret = "change";
                } else {
                    $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("services") . " AND `tag`=" . $DB->F("closed") . ";";
                    $status = $DB->getField($sql);
                    if ($status == $currentStatus) {
                        $ret = "ok";
                    } else {
                        $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("services") . " AND `tag`=" . $DB->F("acc_accepted") . ";";
                        $status = $DB->getField($sql);
                        if ($status == $currentStatus) {
                            $ret = "ok";
                        } else {
                            $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("services") . " AND `tag`=" . $DB->F("ticketexec") . ";";
                            $status = $DB->getField($sql);
                            if ($status == $currentStatus) {
                                $ret = "ok";
                            } else {
                                $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("services") . " AND `tag` LIKE " . $DB->F("%done%") . ";";
                                $status = $DB->getField($sql);
                                if ($status == $currentStatus) {
                                    $ret = "ok";
                                } else {
                                    $sql = "SELECT `id` FROM `task_status` WHERE `plugin_uid`=" . $DB->F("services") . " AND `tag` LIKE " . $DB->F("%timeout30%") . ";";
                                    $status = $DB->getField($sql);
                                    if ($status == $currentStatus) {
                                        $ret = "ok";
                                    } else {
                                        $ret = "nostatus";
                                    }
                                }
                            }
                        }
                    }

                }
            }
        } else {
            $ret = "error";
        }
        echo $ret;
        return false;
    }

    function getCurrentCalc_ajax()
    {
        global $DB, $USER;

        if ($task_id = $_POST["task_id"]) {
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $task = new ServiceTicket($task_id);
            $sc_id = $task->getSCId();
            if (!$sc_id) {
                echo "<p class=\"redmessage rm11\">Пожалуйста, укажите <strong>Сервисный Центр</strong>!<br /><small>При этом - СЦ должен совпадать с СЦ техника, который выполнял заявку.</small></p>";
                return false;
            }
            $currentStatus = $task->getStatusId();
            $execdoneStatus = adm_statuses_plugin::getStatusByTag("ticketexec", "services");
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "services");
            $countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "services");
            $acceptedStatus = adm_statuses_plugin::getStatusByTag("closed", "acc_accepted");
            $targetStatus = false;
            if ($currentStatus == $acceptedStatus["id"] || $currentStatus == $countStatus["id"] || $currentStatus == $completeStatus["id"]) $targetStatus = true;
            $sql = "SELECT DATE_FORMAT(`datetime`, '%Y-%m-%d') AS doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($task_id) . " AND `status_id`=" . $DB->F($execdoneStatus["id"]) . " ORDER BY donedate DESC;";
            $doneDate = $DB->getField($sql);
            if (date("m") == "01") {
                $df = date("Y-m-d", strtotime(date("Y-m") . "+" . getcfg('connfinreport_dc_january') . "days"));
            } else $df = date("Y-m-d", strtotime(date("Y-m") . "+" . getcfg('connfinreport_dc') . "days"));
            $cd = date("Y-m-d");
            $readonly = false;
            if (!$USER->checkAccess("calcticketinclosedstatus", User::ACCESS_WRITE) && $targetStatus) {
                $readonly = true;
            }
            if ($readonly)
                $tpl->loadTemplatefile($USER->getTemplate() . "/calcreadonly.tmpl.htm");
            else
                $tpl->loadTemplatefile($USER->getTemplate() . "/calc.tmpl.htm");
            $tpl->setVariable("CURRENTSTATUSID", $task->getStatusId());

            $calc_uid = $DB->getField("SELECT `tech_id` FROM `list_tech_calc` WHERE `task_id`=" . $DB->F($task_id) . ";");
            $uid = $calc_uid ? $calc_uid : gfx_plugin::getCurrentTechIdAdGfx($task_id);

            if ($uid) {
                $tpl->setVariable("CALC_TECH_LIST", $uid);
                $u = adm_users_plugin::getUser($uid);
                $tpl->setVariable('CALC_TECH_NAME', $u["fio"]);
            }
            if ($readonly) {
                $tpl->setVariable("AFTERDEADLINE", "disabled='disabled'");
            }
            $tpl->setVariable("CALCMRAMMOUNT", $task->getPrice());


            $cDateW = $DB->getField("SELECT `calc_date` FROM `list_skp_tech_calc` WHERE `task_id`=" . $DB->F($task_id) . ";");
            $tpl->setVariable("TMC_SC_ID", $task->getSCId());
            $cDate = $cDateW ? $cDateW : false;
            if ($cDate) {
                if ($cDate == "0000-00-00")
                    $tpl->setVariable("CALC_DATE", date('d.m.Y'));
                else
                    $tpl->setVariable("CALC_DATE_DISABLED", "disabled='disabled'");
                $tpl->setVariable("CALC_DATE", substr($cDate, 8, 2) . "." . substr($cDate, 5, 2) . "." . substr($cDate, 0, 4));
            } else {
                if ($execdoneStatus) {
                    $sql = "SELECT DATE_FORMAT(`datetime`, " . $DB->F("%d.%m.%Y") . ") as doneDate FROM `task_comments` WHERE `task_id`=" . $DB->F($task_id) . " AND `status_id`=" . $DB->F($execdoneStatus["id"]) . " ORDER BY donedate DESC;";
                    $doneDate = $DB->getField($sql);
                    if ($doneDate) {
                        $tpl->setVariable("CALC_DATE", $doneDate);
                    } else {
                        $tpl->setVariable("CALC_DATE", date('d.m.Y'));
                    }
                } else {
                    $tpl->setVariable("CALC_DATE", date('d.m.Y'));

                }
            }
            $sql = "SELECT * FROM `list_skp_tech_calc` WHERE `task_id`=" . $DB->F($task_id);
            $DB->query($sql);
            if ($DB->num_rows()) {
                $totalPrice = 0;
                $tpl->setVariable("HIDENOROWS", "style='display:none;'");
                while ($r = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("tw_row");

                    $tpl->setVariable("TW_ID", $r["wt_id"]);
                    $tpl->setVariable("TWR_ID", $r["id"]);
                    $tpl->setVariable("TW_CODE", adm_skp_wtypes_plugin::getCodeByID($r["wt_id"], $task->getContrId()));
                    $tpl->setVariable("TW_TYPE_TITLE", adm_skp_wtypes_plugin::getByID($r["wt_id"]));
                    $tpl->setVariable("MINVAL", $r["minval"]);
                    $tpl->setVariable("TW_QUANT", $r["quant"]);
                    $tpl->setVariable("TW_PRICE", number_format($r["price"] * $r["quant"], 2, ".", ""));
                    $tpl->setVariable("REALPRICE", $r["price"]);

                    $totalPrice += $r["quant"] * $r["price"];
                    $tpl->parse("tw_row");
                }
                $tpl->setVariable("TOTALAMMOUNT", number_format($totalPrice, 2, ".", ""));

            } else {
                $tpl->setVariable("HIDEIFNOROWS", "style='display:none;'");
            }
            $DB->free();

            // TMC
            $sql = "SELECT
                tmc_tech.id,
                tmc_tech.count,
                tmc_sc.serial,
                tmc_sc.tmc_id,
                tmc_list.title,
                tickdata.isrent,
                tmc_list.metric_flag
            FROM
                `tmc_ticket` AS tickdata
                LEFT JOIN
                `tmc_sc_tech` AS tmc_tech ON tmc_tech.id=tickdata.tmc_tech_sc_id
                LEFT JOIN
                `tmc_sc` AS tmc_sc ON tmc_sc.id=tmc_tech.tmc_sc_id
                LEFT JOIN
                `list_materials` AS tmc_list ON tmc_list.id=tmc_sc.tmc_id
            WHERE
                !tickdata.isreturn AND
                tickdata.task_id=" . $DB->F($task_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) {
                echo "error " . $sql . " " . $DB->error();
                trigger_error($DB->error());
                return false;
            }
            if ($DB->num_rows()) {
                while ($res = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("tmc_row");
                    if ($readonly) $tpl->setVariable("AFTERDEADLINE_HIDDENTMC", 'hidden');
                    $tpl->setVariable("TMC_ROW_ID", $res["id"]);
                    if ($res["metric_flag"]) {
                        $tpl->setVariable("NO_SERIAL", "&mdash;");
                        $tpl->setVariable("NO_ISRENT", "&mdash;");
                    } else {
                        $tpl->setCurrentBlock("hasserial");
                        $tpl->setVariable("TMC_S_ID", $res["id"]);
                        $tpl->setVariable("TMC_SERIAL", $res["serial"]);
                        $tpl->parse("hasserial");
                        $tpl->setCurrentBlock("isrent");
                        $tpl->setVariable("IS_RENT_CHECKED", $res["isrent"] == '1' ? "checked=\"checked\"" : "");
                        $tpl->setVariable("TMC_IR_ID", $res["id"]);
                        $tpl->parse("isrent");
                        $tpl->setVariable("TMC_COUNT", "1");
                    }
                    $tpl->setVariable("TMC_ID", $res["tmc_id"]);
                    $tpl->setVariable("TMC_TITLE", $res["title"]);
                    $tpl->setVariable("TMC_COUNT", $res["count"]);

                    $tpl->parse("tmc_row");
                }
                $DB->free();
                $tpl->setVariable("NO_TMC_ROWS", "display: none;");

            } else {
                $tpl->setVariable("NO_TMC_ROWS", "");
            }

            $tpl->setVariable("TMC_SC_ID", $task->getSCId());

            // прием ТМЦ

            $sql = "
SELECT
    tmc_sc_tech.id,
    tmc_ticket.id AS tid,
    tmc_sc_tech.count,
    tmc_sc.serial,
    tmc_sc.tmc_id,
    list_materials.title,
    tmc_ticket.isrent,
    list_materials.metric_flag,
    tmc_sc.from_tech_id,
    tmc_ticket.isreturn
FROM
    `tmc_ticket`
        LEFT JOIN
    tmc_sc_tech ON (tmc_sc_tech.id = tmc_ticket.tmc_tech_sc_id)
        LEFT JOIN
    tmc_sc ON (tmc_sc.id = tmc_sc_tech.tmc_sc_id)
        LEFT JOIN
    list_materials ON (tmc_sc.tmc_id = list_materials.id)
WHERE
    tmc_ticket.isreturn AND task_id = " . $DB->F($task_id) . ";
            ";
            $DB->query($sql);
            if ($DB->errno()) {
                echo "error " . $sql . " " . $DB->error();
                trigger_error($DB->error());
                return false;
            }
            if ($DB->num_rows()) {
                while ($res = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("r_tmc_row");
                    $tpl->setVariable("R_TMC_ROW_ID", $res["tid"]);
                    $tpl->setVariable("R_TMC_TECH_ID", $res["tech_id"]);
                    $tpl->setVariable("R_TMC_S_ID", $res["id"]);
                    $tmc_row = $DB->getRow("SELECT `serial`,`from_tech_id`, `tmc_id` FROM `tmc_sc` WHERE `from_task_id`=" . $DB->F($task_id) . " AND `tmc_ticket_id`=" . $DB->F($res["tid"]) . ";", true);
                    $tpl->setVariable("R_TMC_SERIAL", $tmc_row["serial"]);
                    $tpl->setVariable("R_TMC_COUNT", "1");
                    $userName = $DB->getField("SELECT `fio` FROM `users` WHERE `id`=" . $DB->F($tmc_row["from_tech_id"]) . ";");
                    if ($userName) $tpl->setVariable("R_TMC_TECHFIO", $userName);
                    else $tpl->setVariable("R_TMC_TECHFIO", "<center>&mdash;</center>");

                    $tmc_name = $DB->getField("SELECT tmc_list.title FROM `list_materials` AS tmc_list WHERE tmc_list.id=" . $DB->F($tmc_row["tmc_id"]) . ";");
                    $tpl->setVariable("R_TMC_ID", $tmc_row["tmc_id"]);
                    $tpl->setVariable("R_TMC_TITLE", $tmc_name);

                    $tpl->setVariable("RTMCTASK_ID", $task_id);
                    if (!$res["id"]) {
                        $tpl->setVariable("AFTERDEADLINE_HIDDEN_R_TMC", "style='display:none;'");
                    }
                    $tpl->parse("r_tmc_row");
                }
                $DB->free();
                $tpl->setVariable("NO_R_TMC_ROWS", "display: none;");

            } else {
                $tpl->setVariable("NO_R_TMC_ROWS", "");
            }

            // ----------


            $tpl->show();

        } else {
            echo "error";
        }
        return;
    }

    function queryStatus()
    {
        global $DB;
        if ($task_id = $_POST['task_id']) {
            $sql = "SELECT s.tag FROM `tasks` AS t LEFT JOIN `task_status` AS s ON t.status_id=s.id WHERE t.id=" . $DB->F($task_id) . ";";
            $status_tag = $DB->getField($sql);
            $ret = $DB->errno() ? "error" : ($status_tag ? $status_tag : "none");
        } else {
            $ret = "error";
        }
        echo $ret;
        return false;
    }

    function calcticket(Request $request)
    {
        global $DB, $USER;
        ob_start();
        print_r($_POST);
        $ret['tpl'] = ob_get_contents();
        ob_end_clean();
        $sql = "DELETE FROM `list_skp_tech_calc` WHERE `task_id`=" . $DB->F($_REQUEST["calc_tsk_id"]) . ";";
        $DB->query($sql);
        if ($DB->errno()) $ret["error"] = $DB->error(); else {
            if (count($_REQUEST["wtype_ids"])) {
                $cmm = "";
                foreach ($_REQUEST["wtype_ids"] as $item) {
                    $sql = "INSERT INTO `list_skp_tech_calc`
                    (`wt_id`, `price`, `quant`, `minval`, `task_id`, `tech_id`, `user_id`, `calc_date`)
                    VALUES
                    (" . $DB->F($item) . ", " . $DB->F($_REQUEST["prices" . $item]) . ", " . $DB->F($_REQUEST["quant" . $item]) . ",
                    " . $DB->F($_REQUEST["minval" . $item]) . ", " . $DB->F($_REQUEST["calc_tsk_id"]) . ",
                    " . $DB->F($_REQUEST['tech_id']) . ", " . $DB->F($USER->getId()) . ", " . $DB->F(date("Y-m-d H:i:s")) . ");";
                    $wtTitle = $DB->getField("SELECT `title` FROM `list_skp_p_wtypes` WHERE `id`=" . $DB->F($item) . ";");
                    $cmm[] = "Вид работ: " . $wtTitle . ", " . "Кол-во: " . $_REQUEST["quant" . $item] . ", Стоимость: " . $_REQUEST["prices" . $item] . "";
                    $DB->query($sql);
                    if ($DB->errno()) {
                        $ret["error"] = $DB->error();
                        echo json_encode($ret);
                        return false;
                    }
                }
                $task = new ServiceTicket($_REQUEST["calc_tsk_id"]);
                if ($task) {
                    $task->setPrice($_REQUEST['mr_count']);
                    $task->setOrientPrice($_REQUEST['mr_count']);

                    $task->updateCommit();

                    $addStatus = false;
                    if (in_array($task->getStatusId(), [47 /* СКП.Выполнение заявки */, 9 /* СКП.Назначена в график */, 42 /* СКП.Таймаут */])) {
                        $doneStatus = adm_statuses_plugin::getStatusByTag("done", "services");
                        $addStatus = $doneStatus["id"];
                    }

                    if ($addStatus)
                        $tag = "Статус: Выполнена. Стоимость: " . $_REQUEST["mr_count"];
                    else
                        $tag = "Обновление отчета. Стоимость: " . $_REQUEST["mr_count"];

                    $comment = "Отчет по заявке\r\n" . implode("\r\n", $cmm);
                    $task->addComment($comment, $tag, $addStatus);

                    $ret["tpl"] = $task->getLastComment();
                    $ret["ok"] = "ok";

                } else {
                    $ret["error"] = "Ошибка обновления задачи.";
                }

            } else {
                $ret["error"] = "Нет видов работ!";
            }
            $sql = "SELECT tmc_tech.id FROM `tmc_ticket` AS tickdata
                        LEFT JOIN `tmc_sc_tech` AS tmc_tech ON tmc_tech.id=tickdata.tmc_tech_sc_id
                        WHERE tickdata.isreturn=0 AND tickdata.task_id=" . $DB->F($_REQUEST["calc_tsk_id"]) . ";";
            //die($sql);
            $DB->query($sql);
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $res[] = $r;
                    if ($r["metric_flag"] != 1) {
                        $sql = "UPDATE `tmc_sc_tech` SET `inplace`=1, ledit=now() WHERE `inplace`=0 AND `incoming`=0 AND `id`=" . $DB->F($r["id"]) . ";";
                        $DB->query($sql);
                        if ($DB->errno()) {
                            echo "error" . $DB->error() . $sql;
                            trigger_error($DB->error());
                            return false;
                        }
                        $DB->free();
                    } else {
                        $sql = "DELETE FROM `tmc_sc_tech` WHERE `id`=" . $DB->F($r["id"]) . ";";
                        $DB->query($sql);
                        if ($DB->errno()) {
                            echo "error" . $DB->error() . $sql;
                            trigger_error($DB->error());
                            return false;
                        }
                        $DB->free();

                    }
                }
            }
            $DB->free();
            $sql = "DELETE FROM `tmc_ticket` WHERE `task_id`=" . $DB->F($_REQUEST["calc_tsk_id"]) . " AND !`isreturn`;";
            $DB->query($sql);
            if ($DB->errno()) {
                echo "error";
                trigger_error($DB->error());
                return false;
            }
            $skp_bl = "89";
            if (count($_REQUEST["tmc_ids"])) {
                $cmm = array();

                foreach ($_REQUEST["row_id"] as $key => $item) {
                    $serial = $_REQUEST["tmcid_" . $item] ? $_REQUEST["tmcid_" . $item] : false;
                    if ($_REQUEST["tmc_ids"][$key] == $skp_bl) {
                        $cmm[] = "Списана квитанция СКП " . $serial . "\r\n";
                    }
                    if ($serial)
                        $sql = "SELECT `id` FROM `tmc_sc` WHERE `sc_id`=" . $DB->F($_REQUEST["target_sc_id"]) . " AND `tmc_id`=" . $DB->F($_REQUEST["tmc_ids"][$key]) . " AND `serial`=" . $DB->F($serial) . " AND (`incoming`=0 OR `incoming` IS NULL) AND (`inplace`=0 OR `inplace` IS NULL) ;";
                    else
                        $sql = "SELECT `id` FROM `tmc_sc` WHERE `sc_id`=" . $DB->F($_REQUEST["target_sc_id"]) . " AND `tmc_id`=" . $DB->F($_REQUEST["tmc_ids"][$key]) . " AND (`incoming`=0 OR `incoming` IS NULL) AND (`inplace`=0 OR `inplace` IS NULL);";
                    $scRec = $DB->getField($sql);
                    if (empty($scRec)) {
                        $scRec = null;
                    }
                    $sql22 = "SELECT `id` FROM `tmc_sc` WHERE `sc_id`=" . $DB->F($_REQUEST["target_sc_id"]) . " AND `tmc_id`=" . $DB->F($_REQUEST["tmc_ids"][$key]) . " AND (`incoming`=0 OR `incoming` IS NULL) AND (`inplace`=0 OR `inplace` IS NULL);";
                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql;
                        trigger_error($DB->error());
                        return false;
                    }
                    $tmcCount = isset($_REQUEST["tmc_count_" . $_REQUEST["tmc_ids"][$key]]) ? $_REQUEST["tmc_count_" . $_REQUEST["tmc_ids"][$key]] : 1;
                    $sql1 = false;
                    if ($serial && $tmcCount == 1) {
                        $sql = "UPDATE `tmc_sc_tech` SET `inplace`=0, ledit=now() WHERE `tmc_sc_id`=" . $DB->F($scRec, true) . " AND `inplace`=1 AND (`incoming`=0 OR `incoming` IS NULL);";
                        $sql1 = "SELECT `id` FROM  `tmc_sc_tech` WHERE `tmc_sc_id`=" . $DB->F($scRec) . " AND `inplace`=1 AND (`incoming`=0 OR `incoming` IS NULL);";
                        $outRec = $DB->getField($sql1);
                    } else {
                        $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `inplace`, `incoming`, ledit) VALUES
                            (" . $DB->F($_REQUEST["target_sc_id"]) . ", " . $DB->F($_REQUEST["tech_id"]) . ", " . $DB->F($scRec, true) . ", " . $DB->F($tmcCount) . ",
                            " . $DB->F($USER->getId()) . ", " . $DB->F(0) . ", " . $DB->F(0) . ", now())";
                    }
                    $DB->query($sql);
                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql . $sql22;
                        trigger_error($DB->error());
                        return false;
                    }
                    if (!$sql1) $outRec = $DB->insert_id();
                    $DB->free();
                    $isrent = $_POST["isrent" . $item] == "on" ? "1" : "0";
                    $sql = "INSERT INTO `tmc_ticket` (`task_id`, `tmc_tech_sc_id`, `count`, `tech_id`, `isrent`, `user_id`, ledit) VALUES
                            (" . $DB->F($_REQUEST["calc_tsk_id"]) . ", " . $DB->F($outRec) . ", " . $DB->F($tmcCount) . ", " . $DB->F($_REQUEST["tech_id"]) . ",
                            " . $DB->F($isrent) . ", " . $DB->F($USER->getId()) . ", now());";
                    $DB->query($sql);
                    if ($DB->errno()) {
                        echo "error" . $DB->error() . $sql;
                        trigger_error($DB->error());
                        return false;
                    }
                    $DB->free();
                }
                if (sizeof($cmm)) {
                    $task->addComment(implode(" ", $cmm), "Списание квитанций");
                    $ret["tpl"] .= $task->getLastComment();
                }
            }
        }
        $t = $DB->query("UPDATE `tickets` SET  `noagrmgts`=0 AND `asrzerror`=0 WHERE `task_id`=" . $DB->F($_REQUEST["calc_tsk_id"]) . ";");
        $DB->free($t);
        echo json_encode($ret);

        return false;
    }

    function closeticket()
    {
        global $DB, $USER;
        if ($USER->checkAccess("close_skp_task", User::ACCESS_WRITE)) {
            $task_id = $_REQUEST["task_id"];
            if ($task_id) {
                $ticket = new ServiceTicket($task_id);
                if ($ticket->getStatusId() != 24) {
                    UIError("Заявка находится в некорректном статусе!");
                } else {
                    if ($ticket->addComment("Закрытие заявки", "Статус: Закрыта", 25)) {
                        redirect($this->getLink('viewticket', "task_id=" . $task_id), "Заявка успешно Закрыта!");
                    } else {
                        UIError("Не указан идентификатор заявки!");
                    }
                }
            } else {
                UIError("Не указан идентификатор заявки!");
            }
        } else {
            UIError("В доступе отказано!");
        }

    }

    function getwtypelist()
    {
        global $DB;
        $contr_id = $_POST['contr_id'];
        $type = $_POST['type'];
        if ($contr_id === FALSE || $type === FALSE) {
            echo "error";
            return;
        }
        $sql = "SELECT wt.id, wt.title FROM `list_contr_agr` AS ca
                LEFT JOIN `link_agr_wtypes` AS law ON law.agr_id=ca.id
                LEFT JOIN `task_types` AS wt ON wt.id=law.wtype_id WHERE ca.contr_id=" . $DB->F($contr_id) . " AND wt.title IS NOT NULL AND wt.plugin_uid=" . $DB->F($type) . " ORDER BY wt.title;";

        if (array2options($DB->getCell2($sql))) echo array2options($DB->getCell2($sql), $_POST['sel_id']); else echo "<option value=\"\">-- нет видов работ --</option>";

    }

    function getagrlist()
    {
        global $DB;
        $contr_id = $_POST['contr_id'];
        $wtype_id = $_POST['wtype_id'];
        if ($contr_id === FALSE || $wtype_id === FALSE) {
            echo "error";
            return;
        }
        $sql = "SELECT `id`, CONCAT(`number`, ' от ', `agr_date`) AS agr FROM `list_contr_agr` WHERE `contr_id`=" . $DB->F($contr_id) . " AND `id` IN (SELECT `agr_id` FROM `link_agr_wtypes` WHERE `wtype_id`=" . $DB->F($wtype_id) . ");";

        if (array2options($DB->getCell2($sql))) echo array2options($DB->getCell2($sql), $_POST['sel_id']); else echo "<option value=\"\">-- нет договоров --</option>";
    }

    /*
    $reject_id = $_POST["reject_".$status["tag"]."_id"] ? $_POST["reject_".$status["tag"]."_id"] : false;
        if ($reject_id) {
            $sql = "UPDATE `tickets` SET `reject_id`=".$DB->F($reject_id)." WHERE `task_id`=".$DB->F($task_id).";";
            $DB->query($sql);
            if ($DB->errno()) return false;
            if (class_exists("adm_rejects_plugin", true) && method_exists("adm_rejects_plugin", "getRejReasonTitle")) {
                $rej = new adm_rejects_plugin();
                $text .= $rej->getRejReasonTitle($reject_id);
                return true;
            } else return false;
        } else {
            $sql = "UPDATE `tickets` SET `reject_id`=".$DB->F("0")." WHERE `task_id`=".$DB->F($task_id).";";
            $DB->query($sql);
            if ($DB->errno()) return false;
            return true;
        }

    */

    function newticket()
    {
        global $DB, $USER;
        if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
            UIError("Вам запрещено создавать заявки!", "Отказано в доступе!");
        }
        $LKUser = adm_users_plugin::isLKUser($USER->getId());
        if ($LKUser) {
            $LKContrID = adm_users_plugin::getLKUserContrID($USER->getId());
            $contr_id = $LKContrID;
            $type = "services";

            $sql = "SELECT wt.id, wt.title FROM `list_contr_agr` AS ca
                    LEFT JOIN `link_agr_wtypes` AS law ON law.agr_id=ca.id
                    LEFT JOIN `task_types` AS wt ON wt.id=law.wtype_id WHERE ca.contr_id=" . $DB->F($contr_id) . " AND wt.title IS NOT NULL AND wt.plugin_uid=" . $DB->F($type) . " ORDER BY wt.title;";
            $r = $DB->getCell($sql);
            if (!$r) UIError("У Вас нет активных договоров в WF для создания заявок такого типа!");

            $tpl_add = "_lk";
        } else $tpl_add = "";
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("new.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/new" . $tpl_add . ".tmpl.htm");
        else
            $tpl->loadTemplatefile("new" . $tpl_add . ".tmpl.htm");
        $tpl->setVariable('TYPE_OPTIONS', $this->getTypeOptions());
        $tpl->setVariable('CNT_OPTIONS', $this->getContrOptions());
        $tpl->setVariable("LK_CONTR_ID", $LKContrID);
        $tpl->setVariable("LK_CONTR_NAME", kontr_plugin::getByID($LKContrID));
        if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getScList")) {
            $addr = new adm_sc_plugin();
            $tpl->setVariable("SC_OPTIONS", $addr->getScList());
        }
        $tpl->setVariable("EXTTYPES", adm_ticket_rtsubtypes_plugin::getOptions());

        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKRegionList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_REGION", $addr->getKRegionList(getcfg('default_region')));
        }
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKCityList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_CITY", $addr->getKCityList(getcfg('default_region')));
        }
        if (class_exists("addr_interface_plugin", true) && method_exists("addr_interface_plugin", "getKNPList")) {
            $addr = new addr_interface_plugin();
            $tpl->setVariable("FILTER_K_NP", $addr->getKNPList(getcfg('default_region')));
        }
        if (class_exists("adm_ods_plugin", true) && method_exists("adm_ods_plugin", "getOptList")) {
            $addr = new adm_ods_plugin();
            $tpl->setVariable("ODS_OPTIONS", $addr->getOptList());
        }
        $tpl->setVariable("TASK_FROM", adm_ticket_sources_plugin::getOptions());

        UIHeader($tpl);
        $tpl->show();
    }

    function saveticket(Request $request)
    {

        global $DB;
        $err = array();
        if (!$_POST['cnt_id']) {
            $err[] = "Не выбран контрагент";
        }
        if (!$_POST['type_id']) {
            $err[] = "Не выбран вид работ";
        }
        if (!$_POST['clnt_type']) {
            $err[] = "Не выбран тип клиента";
        }
        if ($_POST['clnt_type'] == \classes\User::CLIENT_TYPE_YUR && !$_POST['clnt_org_name']) {
            $err[] = "Не заполнено название организации";
        } elseif (!$_POST['clnt_fio']) {
            $err[] = "Не заполнено ФИО клиента";
        }
        if (sizeof($err)) {
            UIError($err);
        }

        $dom_id = 0;
        $ispoln_ids = explode(',', $_POST['ispoln_id']);
        $sc_id = ($_POST['sc_id'] > 0) ? $_POST['sc_id'] : null;

        $address = $this->getEm()->getRepository(\models\ListAddr::class)
            ->setAddressProvider($this->getContainer()->get('wf.address.provider'))
            ->findOrCreateByAddressArray([
                'city' => $request->get('full_address')
            ], [
                'count' => 1
            ]);

        $dom_id = $address->getId();

        $task_id = ServiceTicket::createTicket(
            $_POST['cnt_id'],
            $_POST['type_id'],
            $dom_id,
            $_POST['pod'],
            $_POST['etazh'],
            $_POST['kv'],
            $_POST['domofon'],
            $_POST['clnt_type'],
            $_POST['clnt_org_name'],
            $_POST['clnt_fio'],
            $_POST['clnt_passp_sn'],
            $_POST['clnt_passport_vid'],
            $_POST['clnt_passport_viddate'],
            $_POST['clnt_phone1'],
            $_POST['clnt_phone2'],
            $ispoln_ids,
            $_POST['cmm'],
            $_POST['addinfo'],
            $_POST["agr_id"],
            $_POST["swip"],
            $_POST["swport"],
            $_POST["swplace"],
            $_POST["clntopagr"],
            $_POST["clnttnum"],
            $sc_id,
            $_POST["orient_price"],
            $_POST["task_from"],
            $_POST["exttype"]
        );

        $ticket = new ServiceTicket($task_id);
        $ticket->setLineContact($_POST['clnt_line_fio'], true);

        if ($request->request->has('date_from')) {
            $slotBegin = new DateTime($request->get('date_from') . ' ' . $request->get('time_from'));
        }
        if ($request->request->has('date_to')) {
            $slotEnd = new DateTime($request->get('date_to') . ' ' . $request->get('time_to'));
        }

        $ticket = new ServiceTicket($task_id);
        if ($ticket && $slotBegin instanceof DateTime) {
            $ticket->setSlotBegin($slotBegin->format('Y-m-d H:i:s'));
            $ticket->updateCommit();
        }
        if ($ticket && $slotEnd instanceof DateTime) {
            $ticket->setSlotEnd($slotEnd->format('Y-m-d H:i:s'));
            $ticket->updateCommit();
        }
        if (!empty($_FILES)) {
            $cm = new comments_plugin();
            $_POST['task_id'] = $task_id;
            $cm->addcmm(true);
        }
        redirect($this->getLink('viewticket', "task_id=$task_id"));
    }

    function checkticketexists_ajax()
    {

        print_r($_POST);

        global $DB, $USER;
        //if(!$_REQUEST['type_id']) $err[] = "Не выбран вид работ";
        if (!$_REQUEST["addr"]) $err[] = "Не указан существующий адрес";

        $sql = "SELECT t.task_id, (SELECT s.name FROM `task_status` AS s WHERE s.is_active=1 AND s.id IN (SELECT tts.status_id FROM `tasks` AS tts WHERE tts.id=t.task_id)) AS status, (SELECT `contr_title` FROM `list_contr` WHERE `id`=t.cnt_id) AS kontr, (SELECT `title` FROM `task_types` WHERE `id`=t.task_type) AS worktype, (SELECT `plugin_uid` FROM `tasks` WHERE `id`=t.task_id) AS tasktype FROM `tickets` AS t WHERE t.dom_id=" . $DB->F($_REQUEST["addr"]) . ";";
        $task = $DB->query($sql, true);
        if ($DB->errno()) $err[] = $DB->error();
        if ($DB->num_rows()) {
            $ret["result"] = "fail";
            //$ret["task_id"] = $task;
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $tpl->loadTemplatefile($USER->getTemplate() . "/ex_addr_tickets.tmpl.htm");
            while ($r = $DB->fetch(true)) {
                if ($r["status"]) {
                    $tpl->setCurrentBlock("tlist");
                    $link = link2($r["tasktype"] . "/viewticket?id=" . $r["task_id"], false);
                    $tpl->setVariable("TASKLINK", $link);
                    $tpl->setVariable("TASKID", $r["task_id"]);
                    $tpl->setVariable("TASKTYPE", $r["worktype"]);
                    $tpl->setVariable("CONTR", $r["kontr"]);
                    $tpl->setVariable("STATUS", $r["status"]);
                    $tpl->parse("tlist");
                }
            }
            $ret["tpl"] = $tpl->get();
        } else {
            $ret["result"] = "ok";

        }
        if (sizeof($err)) {
            $ret["result"] = "error";
        }
        echo json_encode($ret);
        return false;
    }

    function vieworderdoc()
    {
        global $DB, $USER;
        if ($id = $_REQUEST["id"]) {
            $ticket = new ServiceTicket($id);
            $sql = "SELECT gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.task_id=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->num_rows()) {
                list($c_date, $startTime, $user) = $DB->fetch();
                $DB->free();
            }
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $tpl->loadTemplatefile($USER->getTemplate() . "/orderdoc.tmpl.htm");
            $tpl->setVariable("TASK_ID", $id);
            $tpl->setVariable("OD_NUM", $id);
            $tpl->touchBlock('client_type_' . $ticket->getClientType());
            if ($ticket->getClientType() != User::CLIENT_TYPE_PHYS) {
                $tpl->setVariable("OD_CLNT_INFO", $ticket->getOrgName() . "<br />" . $ticket->getFio());
            } else {
                $tpl->setVariable("OD_CLNT_INFO", $ticket->getFio());
            }
            $tpl->setVariable("OD_CONTRAGENT", $ticket->getContrName());
            $tpl->setVariable("OD_WTYPE", $ticket->getTypeName());
            $phnes = $ticket->getPhones();
            if ($USER->checkAccess("hidetelnum")) {
                $tpl->setVariable("OD_CLNT_TEL", "---");
            } else {
                $tpl->setVariable("OD_CLNT_TEL", ($phnes[0] ? $phnes[0] : '+7') . "<br />" . ($phnes[1] ? $phnes[1] : '+7'));
            }
            $tpl->setVariable("OD_SC", $this->getSc($ticket->getSCId()));
            $tpl->setVariable("OD_EMPL", @$user ? $user : "---");
            $ods = $this->getOds($ticket->getDomId());
            $tpl->setVariable("OD_ODS", $ods ? $ods["title"] . " " . $ods["tel"] : "---");
            $tpl->setVariable("OD_CREATEDATE", rudate("d M Y H:i"));
            $tpl->setVariable("OD_ADDR", $ticket->getAddr($ticket->getDomId()));
            if (@$c_date && @$startTime) {
                $startHour = (strlen(floor($startTime / 60)) == 1) ? "0" . (floor($startTime / 60)) : floor($startTime / 60);
                $startMin = (strlen(floor($startTime % 60)) == 1) ? "0" . (floor($startTime % 60)) : floor($startTime % 60);
                $tpl->setVariable("OD_GFXTIME", rudate("d M Y", strtotime($c_date)) . " " . $startHour . ":" . $startMin);
            } else $tpl->setVariable("OD_GFXTIME", "---");
            $tpl->setVariable("OD_ADDR_ENTR", $ticket->getPod());
            $tpl->setVariable("OD_CNLT_AGRNUM", $ticket->getClntOpAgr());
            $tpl->setVariable("OD_ADDR_DOMOFON", $ticket->getDomofon());
            $tpl->setVariable("OD_TASK_DEVPLACE", $ticket->getSwPlace());
            $tpl->setVariable("OD_ADDR_FLOOR", $ticket->getEtazh());
            $tpl->setVariable("OD_TASK_IP", $ticket->getSwIp());
            $tpl->setVariable("OD_ADDR_FLAT", $ticket->getKv());
            $tpl->setVariable("OD_TASK_PORT", $ticket->getSwPort());
            $tpl->setVariable("OD_ADDINFO", $ticket->getAddInfo());

            $agr_id = $ticket->getAgrId(false);
            if ($agr_id) {
                if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getAgrById")) {
                    $kontr = new kontr_plugin();
                    $agr_contacts = $kontr->getAgrById($agr_id);
                    if ($agr_contacts) {
                        $tpl->setVariable("OD_CONTR_ACCESS", $agr_contacts['access_contact'] ? $agr_contacts['access_contact'] : "<strong>-</strong>");
                        $tpl->setVariable("OD_CONTR_TP", $agr_contacts['tp_contact'] ? $agr_contacts['tp_contact'] : "<strong>-</strong>");

                    } else {
                        $tpl->setVariable("OD_CONTR_ACCESS", "<strong>ошибка</strong>");
                        $tpl->setVariable("OD_CONTR_TP", "<strong>ошибка</strong>");
                    }
                }
            } else {

                $tpl->setVariable("OD_CONTR_ACCESS", "<strong>-</strong>");
                $tpl->setVariable("OD_CONTR_TP", "<strong>-</strong>");
            }
            if (isset($_REQUEST['printcomments'])) {
                $tpl->setCurrentBlock("print-comments");
                $tpl->setVariable("C_OD_NUM", $id . " / " . $ticket->getClntTNum());
                $tpl->setVariable("C_COMMENTS_LIST", $ticket->getCommentsPrint());
                $tpl->parse("print-comments");
            }
            UIHeader($tpl);
            $tpl->show();
        } else {
            UIError("Ошибка! Не указан идентификатор заявки для печати Заказ-наряда!");
        }
        return false;

    }

    static function getSC($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_sc` WHERE `id`=" . $DB->F($id) . ";";
        return $DB->getField($sql);
    }

    function updateticket(Request $request)
    {
        global $DB, $USER;
        $ticket = new ServiceTicket($_REQUEST['task_id']);
        $LKUser = adm_users_plugin::isLKUser($USER->getId());
        $err = array();

        if ($ticket->getClientType() == \classes\User::CLIENT_TYPE_YUR && !$_POST['org_name']) {
            $err[] = "Не заполнено название организации";
        } elseif (!$_POST['fio']) {
            $err[] = "Не заполнено ФИО клиента";
        }
        if (sizeof($err)) {
            UIError($err);
        }

        $taskEntity = $this->getEm()->find(\models\Task::class, $request->get('task_id'));

        $comment = "";
        if (isset($_POST['sc_id']) && $ticket->getSCId() != $_POST['sc_id'])
            $comment .= "Сервисный центр: " . $this->getSCName($ticket->getSCId()) . " -> " . $this->getSCName($_POST['sc_id']) . "\r\n";
        if ($ticket->getClientType() == \classes\User::CLIENT_TYPE_YUR && $ticket->getOrgName(false) != $_POST['org_name'])
            $comment .= "Название организации: " . $ticket->getOrgName(false) . " -> " . $_POST['org_name'] . "\r\n";
        if ($ticket->getFio(false) != $_POST['fio'])
            $comment .= "ФИО: " . $ticket->getFio(false) . " -> " . $_POST['fio'] . "\r\n";
        if ($ticket->getLineContact(false) != $_POST['clnt_line_fio'])
            $comment .= "Владелец линии: " . $ticket->getLineContact(false) . " -> " . $_POST['clnt_line_fio'] . "\r\n";
        if ($ticket->getPasspSN(false) != $_POST['passp_sn'])
            $comment .= "Паспорт (серия номер): " . $ticket->getPasspSN(false) . " -> " . $_POST['passp_sn'] . "\r\n";
        if ($ticket->getPasspVidan(false) != $_POST['passport_vid'])
            $comment .= "Паспорт (кем выдан): " . $ticket->getPasspVidan(false) . " -> " . $_POST['passport_vid'] . "\r\n";
        if ($ticket->getPasspVidanDate() != $_POST['passport_viddate'])
            $comment .= "Паспорт (дата выдачи): " . $ticket->getPasspVidanDate(false) . " -> " . $_POST['passport_viddate'] . "\r\n";
        $phones = $ticket->getPhones();
        if ($phones[0] != preg_replace("/[^0-9]/", "", $_POST['phone1']))
            $comment .= "Телефон (1): {$phones[0]} -> {$_POST['phone1']}\r\n";
        if ($phones[1] != preg_replace("/[^0-9]/", "", $_POST['phone2']))
            $comment .= "Телефон (2): {$phones[1]} -> {$_POST['phone2']}\r\n";
        if ($ticket->getSwIp(false) != $_POST['swip'])
            $comment .= "IP-адрес: " . $ticket->getSwIp(false) . " -> " . $_POST['swip'] . "\r\n";
        if ($ticket->getSwPort(false) != $_POST['swport'])
            $comment .= "Порт: " . $ticket->getSwPort(false) . " -> " . $_POST['swport'] . "\r\n";
        if ($ticket->getSwPlace(false) != $_POST['swplace'])
            $comment .= "Расположение свитча: " . $ticket->getSwPlace(false) . " -> " . $_POST['swplace'] . "\r\n";
        if ($ticket->getClntOpAgr(false) != $_POST['clntopagr'])
            $comment .= "Номер договора оператора с клиентом: " . $ticket->getClntOpAgr(false) . " -> " . $_POST['clntopagr'] . "\r\n";
        if ($ticket->getClntTNum(false) != $_POST['clnttnum'])
            $comment .= "Номер заявки клиента в базе оператора: " . $ticket->getClntTNum(false) . " -> " . $_POST['clnttnum'] . "\r\n";
        if (!$LKUser) {
            if ($ticket->getOrientPrice(false) != $_POST['orient_price'])
                $comment .= "Ориентировочная стоимость работ: " . $ticket->getOrientPrice(false) . " -> " . $_POST['orient_price'] . "\r\n";
            if ($ticket->getPrice(false) != $_POST['price'])
                $comment .= "Cтоимость работ: " . $ticket->getPrice(false) . " -> " . $_POST['price'] . "\r\n";
        }
        if ($ticket->getTaskFrom(false) != $_POST['task_from'])
            $comment .= "Откуда узнали: " . adm_ticket_sources_plugin::getById($ticket->getTaskFrom(false)) . " -> " . adm_ticket_sources_plugin::getById($_POST['task_from']) . "\r\n";
        if ($ticket->getTicketDocNum(false) != $_POST['ticket_doc_num'])
            $comment .= "Номер квитанции: " . $ticket->getTicketDocNum(false) . " -> " . $_POST['ticket_doc_num'] . "\r\n";
        if ($ticket->getAddInfo() != $_POST["addinfo"]) {
            $comment .= "Примечание: " . $ticket->getAddInfo() . " -> " . $_POST['addinfo'] . "\r\n";
        }

        if ($ticket->getExtType(false) != $_POST['exttype'])
            $comment .= "Подтип заявки: " . adm_ticket_rtsubtypes_plugin::getById($ticket->getExtType(false)) . " -> " . adm_ticket_rtsubtypes_plugin::getById($_POST['exttype']) . "\r\n";

        if (isset($_POST["donedate"]) && preg_match("/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/", $_POST["donedate"])) {
            if ($ticket->getDoneDate() != $_POST["donedate"]) {
                $comment .= "ОБНОВЛЕНА ДАТА ВЫПОЛНЕНИЯ: " . $ticket->getDoneDate() . " -> " . $_POST['donedate'] . "\r\n";
                if (!$ticket->setDoneDate($_POST["donedate"])) UIError("Ошибка обновления ДАТЫ ВЫПОЛНЕНИЯ!");
            }
        }

        if ($ticket->getSlotDateBegin() != $_POST["date_from"]) {
            $comment .= "График оператора с (дата): " . $ticket->getSlotDateBegin() . " -> " . $_POST['date_from'] . "\r\n";
        }

        if ($ticket->getSlotDateEnd() != $_POST["date_to"]) {
            $comment .= "График оператора по (дата): " . $ticket->getSlotDateEnd() . " -> " . $_POST['date_to'] . "\r\n";
        }

        if ($ticket->getSlotTimeBegin() != $_POST["time_from"]) {
            $comment .= "График оператора с (время): " . $ticket->getSlotTimeBegin() . " -> " . $_POST['time_from'] . "\r\n";
        }

        if ($ticket->getSlotTimeEnd() != $_POST["time_to"]) {
            $comment .= "График оператора по (время): " . $ticket->getSlotTimeEnd() . " -> " . $_POST['time_to'] . "\r\n";
        }

        if (isset($_POST['sc_id'])) {
            $sc_id = ($_POST['sc_id'] > 0) ? $_POST['sc_id'] : null;
            $ticket->setSCId($sc_id);
        }

        $ticket->setOrgName($_POST['org_name']);
        $ticket->setFio($_POST['fio']);
        $ticket->setLineContact($_POST['clnt_line_fio']);
        $ticket->setPassp($_POST['passp_sn'], $_POST['passport_vid'], $_POST['passport_viddate']);
        $ticket->setPhones($_POST['phone1'], $_POST['phone2']);
        $ticket->setAddInfo($_POST['addinfo']);
        $ticket->setSwIp($_POST["swip"]);
        $ticket->setSwPort($_POST["swport"]);
        $ticket->setSwPlace($_POST["swplace"]);
        $ticket->setClntOpAgr($_POST["clntopagr"]);
        $ticket->setClntTNum($_POST["clnttnum"]);
        $ticket->setOrientPrice($_POST["orient_price"]);
        $ticket->setTaskFrom($_POST["task_from"]);
        $ticket->setPrice($_POST["price"]);
        $ticket->setTicketDocNum($_POST["ticket_doc_num"]);
        $ticket->setExtType($_POST["exttype"]);

        $slotBegin = $_POST["date_from"] ? substr($_POST["date_from"], 6, 4) . "-" . substr($_POST["date_from"], 3, 2) . "-" . substr($_POST["date_from"], 0, 2) : "";
        $slotBegin .= ($_POST["time_from"] ? " " . $_POST["time_from"] . ":00" : " 00:00:00");
        $slotEnd = $_POST["date_to"] ? substr($_POST["date_to"], 6, 4) . "-" . substr($_POST["date_to"], 3, 2) . "-" . substr($_POST["date_to"], 0, 2) : "";
        $slotEnd .= ($_POST["time_to"] ? " " . $_POST["time_to"] . ":00" : " 00:00:00");

        $ticket->setSlotBegin($slotBegin);
        $ticket->setSlotEnd($slotEnd);


        if ($ticket->updateCommit()) {
            if ($comment) $ticket->addComment($comment, "Обновлены данные");
        }
        if (isset($_POST['send_to_podr']) && $_POST['podr_id']) {
            $sql = "SELECT p.user_id, us.fio FROM `podr` AS p LEFT JOIN `users` AS us ON p.user_id=us.id WHERE p.contr_id=" . $DB->F($_POST['podr_id']);
            list($podr_user_id, $podr_user_fio) = $DB->getRow($sql);

            $sql = "INSERT INTO `podr_tickets` (`task_id`, `contr_id`) VALUES(" . $DB->F($ticket->getId()) . ", " . $DB->F($_POST['podr_id']) . ")";
            $DB->query($sql);
            if ($DB->errno()) {
                $err[] = "Не удалось отправить заявку подрядчику!";
            } else {
                $status = $this->getStatusesByTag('grafik');

                if ($status) {
                    $status = array_shift($status);
                }
                $tag = "Статус <b>{$status['name']}</b>";

                if ($podr_user_id && $ticket->setIspoln($podr_user_id)) {
                    $tag .= ", задача перепоручена <b>$podr_user_fio</b>";
                }
                $ticket->addComment("Заявка отправлена подрядчику", $tag, $status['id'], $err);
            }
        }

        $this->getEm()->flush();

        if (sizeof($err)) UIError($err);

        redirect($this->getLink('viewticket', "task_id=" . $ticket->getId()));
    }


}