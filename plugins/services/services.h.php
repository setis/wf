<?php 
/**
 * Plugin Header
 * @author kblp
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "СКП";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Заявки на обслуживание и ремонт";
    $PLUGINS[$plugin_uid]['events']['viewticket'] = "Просмотр заявки на обслуживание/ремонт";
    $PLUGINS[$plugin_uid]['events']['vieworderdoc'] = "Печать заказ-наряда";
    $PLUGINS[$plugin_uid]['events']['viewlist_export'] = "Заявки на Сервисное обслуживание и ремонт. Выгрузка.";
    $PLUGINS[$plugin_uid]['events']['checkticketexists_ajax'] = "Проверка заявки на существование - Вид работ, Адрес";
    $PLUGINS[$plugin_uid]['events']['getwtypelist'] = "Список видов работ";
    $PLUGINS[$plugin_uid]['events']['getagrlist'] = "Список договоров";
    $PLUGINS[$plugin_uid]['events']['queryStatus'] = "Запрос текущего статуса Заявки";
}

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['newticket'] = "Новая заявка на обслуживание и ремонт";
    $PLUGINS[$plugin_uid]['events']['saveticket'] = "Сохранение зявки";
    $PLUGINS[$plugin_uid]['events']['updateticket'] = "Обновление зявки";
    $PLUGINS[$plugin_uid]['events']['removefromgfx'] = "Удаление из графика";
    $PLUGINS[$plugin_uid]['events']['deleteticket'] = "Удаление заявки";
    $PLUGINS[$plugin_uid]['events']['updateContrDataA'] = "Обновление типа заявки ";
    $PLUGINS[$plugin_uid]['events']['acceptticket_ajax'] = "Принятие заявки техником";
    $PLUGINS[$plugin_uid]['events']['rejectticket_ajax'] = "Отказ техника от заявки";
    $PLUGINS[$plugin_uid]['events']['closeticket'] = "Закрытие заявки";
    $PLUGINS[$plugin_uid]['events']['calcticket'] = "Расчет заявки";
    $PLUGINS[$plugin_uid]['events']['getcurrentcalc_ajax'] = "Загрузка расчета заявки";
    $PLUGINS[$plugin_uid]['events']['checktaskstatus_ajax'] = "Проверка статуса заявки для перевода в статус Сдача отчета";
    $PLUGINS[$plugin_uid]['events']['return_tmc_from_ticktet'] = "Возврат ТМЦ по ГП";
    $PLUGINS[$plugin_uid]['events']['delete_returned_tmc_from_ticket'] = "Откат (отмена списания) ТМЦ по ГП";
}

$plugin_uid = "services_questionnarie_access";
$PLUGINS[$plugin_uid]['name'] = "СКП: Доступ к анкете";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'services2podr';

$PLUGINS[$plugin_uid]['name'] = "СКП: Заявки на обслуживание и ремонт: передача подрядчику";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'changedonedate_skp';

$PLUGINS[$plugin_uid]['name'] = "СКП: Изменение даты выполнения";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = 'fullaccesstoallsc';

$PLUGINS[$plugin_uid]['name'] = "СКП: Полный доступ ко всем СЦ";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "edit_contr_ticket_service";

$PLUGINS[$plugin_uid]['name'] = "СКП: Изменение контрагента в заявках";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['editcontr'] = "Редактор контрагента";
}


$plugin_uid = "edit_status_service";

$PLUGINS[$plugin_uid]['name'] = "СКП: Изменение статусов заявок";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['editstate'] = "Редактор статуса";
}
 
$plugin_uid = "close_skp_task";

$PLUGINS[$plugin_uid]['name'] = "СКП: Закрытие заявок (Статус)";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['closestate'] = "Редактор статуса";
}

$plugin_uid = "edit_closed_skp_task";

$PLUGINS[$plugin_uid]['name'] = "СКП: Редактирование закрытых заявок";
$PLUGINS[$plugin_uid]['hidden'] = true;

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) { 
    $PLUGINS[$plugin_uid]['events']['closestate'] = "Редактирование";
}

$plugin_uid = "skpallowodsandaddrparams";
$PLUGINS[$plugin_uid]['name'] = "СКП: Редактор параметров адреса и выбор ОДС";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "calcticketinclosedstatus";
$PLUGINS[$plugin_uid]['name'] = "СКП: Редактирование отчета в закрытых статусах";
$PLUGINS[$plugin_uid]['hidden'] = true;

$plugin_uid = "plk.service.view.calls";
$PLUGINS[$plugin_uid]['name'] = "СКП: Просмотр звонков по заявке (ЛК Партнёра)";
$PLUGINS[$plugin_uid]['hidden'] = true;