<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Расчетный лист Контрагента (Подключение)";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Расчетный лист Контрагента (Подключение)";
    $PLUGINS[$plugin_uid]['events']['archive'] = "Архив Расчетных листов Контрагента (Подключение)";
    $PLUGINS[$plugin_uid]['events']['viewreport'] = "Просмотр Архивного Расчетного листа Контрагента (Подключение)";   
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['deletereport'] = "Удаление отчета из Архива";
    
}
?>