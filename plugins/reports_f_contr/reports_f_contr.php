<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\tickets\ConnectionTicket;


require_once(dirname(__FILE__)."/../connections/connections.php");

 
class reports_f_contr_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function archive() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("archive.tmpl.htm");
        if ($USER->getTemplate() != "default") 
            $tpl->loadTemplatefile($USER->getTemplate()."/archive.tmpl.htm");
        else 
            $tpl->loadTemplatefile("archive.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        UIHeader($tpl);
        report_left_menu($tpl);
        $tpl->setVariable("REPORT__FIN_CONTR_LIST", "buttonsel1");
        $sql = "SELECT count(id) FROM `list_f_contr_reports` WHERE 1 ORDER BY `cr_date` DESC";
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $total = $DB->getField($sql);
        $sql = "SELECT * FROM `list_f_contr_reports` WHERE 1 ORDER BY `cr_date` DESC LIMIT ".$_REQUEST['filter_start'].",".getcfg('rows_per_page');
        $DB->query($sql);
        
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            while ($res = $DB->fetch(true)) {
                $uid = adm_users_plugin::getUser($res["cr_uid"]);
                $tpl->setCurrentBlock("row");
                $tpl->setVariable("RR_ID", $res["id"]);
                $tpl->setVariable("RR_PERIOD", base64_decode($res["period"]));
                $tpl->setVariable("RR_CRDATE", $res["cr_date"]);

                $tpl->setVariable("RR_AUTHOR", $uid["fio"]);                
                $tpl->parse("row");   
            }
            $DB->free();
        } else {
            $tpl->touchBlock("norows");
        }
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], getcfg('rows_per_page'), intval($total), "reports_f_sc/archive#start-%s"));
        $tpl->show();    
    }
    
    
    function viewreport() {
        global $DB, $USER;
        if ($id = $_REQUEST["id"]) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile("viewarchiveitem.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/viewarchiveitem.tmpl.htm");
            else 
                $tpl->loadTemplatefile("viewarchiveitem.tmpl.htm");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
            $sql = "SELECT `body` FROM `list_f_contr_reports` WHERE `id`=".$DB->F($id).";";
            $body = $DB->getField($sql);
            if ($DB->errno()) UIError($DB->error());
            if ($body) {
                $tpl->setVariable("REPORT_HERE", base64_decode($body));
                $tpl->show();
            } else {
                UIError("Ошибка БД!");
            }
        } else {
            UIError("Не указан ижентификатор отчета!");
        }
    }
    
    function deletereport() {
        global $DB, $USER;
        if ($id = $_REQUEST["id"]) {
            $sql = "DELETE FROM `list_f_contr_reports` WHERE `id`=".$DB->F($id).";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            redirect($this->getLink('archive'), "Отчет успешно удален из Архива!");
        } else {
            UIError("Не указан ижентификатор отчета!");
        }
    }
    
    function main() {
        global $DB, $USER;
        
        $sort_ids = array(
            1 => "Дата выполнения",
            2 => "Номер заявки оператора",
            3 => "ПИН",
            4 => "Номер заявки",
            5 => "Район",
            6 => "Адрес",
        );
        $sort_sql = array(
            1 => "tc1.datetime",
            2 => "tick.clnttnum",
            3 => "tick.clntopagr",
            4 => "t.id",
			5 => "lar.title",
            6 => "kstreet.NAME",
        );
        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $fintypes = array("0"=>"Все", "1"=>"Рассчитанные (Проведенные)", "2"=>"Не рассчитанные (Не проведенные)");
        
        if ($_POST["setflag"]) {
            if ($_POST["type_id"] == 1) {
                $tpl->setVariable("DISABLED", "disabled='disabled'");
            } else {
                if ($_POST["sc_id"] && $_POST["area_id"]) {
                    $filter = "AND tick.dom_id IN (SELECT `id` FROM `list_addr` WHERE `area_id`=".$DB->F($_POST["area_id"]).")";
                } else {
                    if ($_POST["sc_id"]) {
                        $filter = " AND tick.sc_id=".$DB->F($_POST["sc_id"]);
                    } 
                }
                $filter .= $_POST["type_id"] == 0 ? "" : ($_POST["type_id"] == 1 ? "AND tick.findatecontr IS NOT NULL" : "AND tick.findatecontr IS NULL");
                if (strtotime($_POST["year"]."-".$_POST["month"]) < strtotime("2014-02")) {
                    UIError("Текущая версия отчета \"Расчетный лист Контрагента - Подключение\" не совместима с данными БД ранее февраля 2014 года. Для предотвращения выдачи некорректных данных, генерация отчета прекращена. Пожалуйста, используйте Архивы данного отчета.");
                }
                $status = adm_statuses_plugin::getStatusByTag("closed", "connections");
                $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
                $sql = "SELECT tick.task_id FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id 
                    WHERE tick.cnt_id=".$DB->F($_POST['cnt_id'])." AND tick.agr_id=".$DB->F($_POST["agr_id"])." AND t.status_id=".$DB->F($status["id"])." 
                    AND t.id IN (SELECT task_id FROM `task_comments` WHERE `status_id`=".$DB->F($doneStatus["id"])." 
                    AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"])))."
                    AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]." + 1 month + ".getcfg('connfinreport_dc')." days"))).")
                    AND t.id IN (SELECT task_id FROM `tasks` WHERE DATE_FORMAT(`date_reg`, '%Y-%m')=".$DB->F($_POST["year"]."-".$_POST["month"]).") ". $filter . ";";
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error(). " " . $sql);
                    $r = false;
                    while ($res = $DB->fetch()) {
                        $r[] = $res[0];
                    }
                    $DB->free();
                    if ($r) {
                        $sql = "UPDATE `tickets` SET `findatecontr`=".$DB->F(date("Y-m-d H:i:s"))." WHERE (`findatecontr` IS NULL) AND `task_id` IN (" . implode(',', array_map('intval', $r)) . ")";
                        $DB->query($sql);
                        if ($DB->errno()) UIError($DB->error(). " " . $sql);
                    }
                    $_POST["type_id"] = 0;        
            }       
                   
        } 
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])&& !isset($_POST["createarchiveitem"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_CONTR_LIST", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
            $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
        
            UIHeader($tpl);
            
            report_left_menu($tpl);
            $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            $tpl->setVariable("SELECTED_AGR", @$_POST["agr_id"]);
            $tpl->setVariable("SELECTED_AREA", @$_POST["area_id"]);
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            $tpl->setVariable("FILTER_TASK_TYPE", array2options($fintypes, $_POST["type_id"]));
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListConn")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListConn(@$_POST["cnt_id"]));
            } 
            if (class_exists("adm_statuses_plugin", true) && method_exists("adm_statuses_plugin", "getOptList")) {
                $st = new adm_statuses_plugin();
                $tpl->setVariable("FILTER_STATUS_OPTIONS", $st->getOptList(@$_POST["status_id"], 'connections'));
            } 
            $tpl->setVariable("FILTER_SC_OPTIONS", "<option value=0>-- все --</option>".adm_sc_plugin::getScList(@$_POST["sc_id"]));
            
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"] || @$_POST["setflag"]|| @$_POST["createarchiveitem"]) {
                if (strtotime($_POST["year"]."-".$_POST["month"]) < strtotime("2014-02")) {
                    UIError("Текущая версия отчета \"Расчетный лист Контрагента - Подключение\" не совместима с данными БД ранее февраля 2014 года. Для предотвращения выдачи некорректных данных, генерация отчета прекращена. Пожалуйста, используйте Архивы данного отчета.");
                }
                $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
                if ($USER->getTemplate() != "default") 
                    $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
                else 
                    $rtpl->loadTemplatefile("report.tmpl.htm");
                //$rtpl->loadTemplatefile("report.tmpl.htm");
                if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                    $rtpl->setCurrentBlock("print_head");
                    $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                    $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                    $rtpl->parse("print_head");
                }
                $rtpl->setVariable("MONTH", $month[$_POST['month']]);
                $rtpl->setVariable("YEAR", $year[$_POST['year']]);
                if ($_POST['cnt_id']!=0 && $_POST["agr_id"]) {
                    $rtpl->setVariable("AGR_VAL", kontr_plugin::getAgrByIdT($_POST["agr_id"]));
                    if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getByID")) {
                        $ct = new kontr_plugin();
                        if ($kontr = $ct->getByID($_POST['cnt_id'])) {
                            $rtpl->setVariable("REP_CONTR", $kontr);
                            $status = adm_statuses_plugin::getStatusByTag("closed", "connections");
                            if (count($status)) {
                                $doneStatus = adm_statuses_plugin::getStatusByTag("done", "connections");
                                if ($_POST["sc_id"] && $_POST["area_id"]) {
                                    $filter = " AND tick.sc_id=".$DB->F($_POST["sc_id"]);
                                    $filter .= " AND tick.dom_id IN (SELECT `id` FROM `list_addr` WHERE `area_id`=".$DB->F($_POST["area_id"]).")";
                                } else {
                                    if ($_POST["sc_id"]) {
                                        $filter = " AND tick.sc_id=".$DB->F($_POST["sc_id"]);
                                    } 
                                }
                                $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];           
                                $filter .= $_POST["type_id"] == 0 ? "" : ($_POST["type_id"] == 1 ? "AND tick.findatecontr IS NOT NULL" : "AND tick.findatecontr IS NULL");
                                
                                $sql = "SELECT tc1.task_id FROM `task_comments` AS tc1 
                                        LEFT JOIN `tasks` AS t ON tc1.task_id=t.id
                                        LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                                        LEFT JOIN `list_addr` AS la ON la.id=tick.dom_id
                                        LEFT JOIN `list_area` AS lar ON lar.id=la.area_id
                                        WHERE 
                                            ((tc1.status_id=".$DB->F($doneStatus["id"])." AND (DATE_FORMAT(tc1.datetime, '%Y-%m')<".$DB->F($_POST["year"]."-".$_POST["month"])." 
                                            AND tc1.task_id IN (SELECT task_id FROM `list_contr_calc` WHERE DATE_FORMAT(`update_date`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]." + ".getcfg('connfinreport_dc')." days")))." AND DATE_FORMAT(`update_date`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]." + 1 month + ".getcfg('connfinreport_dc')." days")))."))) 
                                            OR
                                            (tc1.status_id=".$DB->F($doneStatus["id"])." AND (DATE_FORMAT(tc1.datetime, '%Y-%m')=".$DB->F($_POST["year"]."-".$_POST["month"])." AND tc1.task_id IN 
                                            (SELECT task_id FROM `list_contr_calc` WHERE DATE_FORMAT(`update_date`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"])))." 
                                                AND DATE_FORMAT(`update_date`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]." + 1 month + ".getcfg('connfinreport_dc')." days")))."))))
                                            AND tick.agr_id=".$DB->F($_POST["agr_id"])."
                                            AND t.status_id=".$DB->F($status["id"])." ".$filter."
                                        GROUP BY tc1.task_id
                                        ORDER BY " .$order;

                                $base_sql = "SELECT tc1.task_id, DATE_FORMAT(tc1.datetime, '%d.%m.%Y') AS datetime FROM `task_comments` AS tc1 
                                        LEFT JOIN `tasks` AS t ON tc1.task_id=t.id
                                        LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                                        LEFT JOIN `list_addr` AS la ON la.id=tick.dom_id
                                        LEFT JOIN `list_area` AS lar ON lar.id=la.area_id
                                        WHERE                                             
                                            ((tc1.status_id=".$DB->F($doneStatus["id"])." AND (DATE_FORMAT(tc1.datetime, '%Y-%m')<".$DB->F($_POST["year"]."-".$_POST["month"])." AND tc1.task_id IN 
                                            (SELECT task_id FROM `list_contr_calc` WHERE DATE_FORMAT(`update_date`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]. "+ ".getcfg('connfinreport_dc')." days")))." AND DATE_FORMAT(`update_date`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]." + 1 month + ".getcfg('connfinreport_dc')." days")))."))) 
                                            OR
                                            (tc1.status_id=".$DB->F($doneStatus["id"])." AND (DATE_FORMAT(tc1.datetime, '%Y-%m')=".$DB->F($_POST["year"]."-".$_POST["month"])." AND tc1.task_id IN 
                                            (SELECT task_id FROM `list_contr_calc` WHERE DATE_FORMAT(`update_date`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"])))." 
                                                AND DATE_FORMAT(`update_date`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST["year"]."-".$_POST["month"]." + 1 month + ".getcfg('connfinreport_dc')." days")))."))))
                                            AND tick.agr_id=".$DB->F($_POST["agr_id"])."
                                            AND t.status_id=".$DB->F($status["id"])." ".$filter."
                                            
                                        GROUP BY tc1.task_id
                                        ORDER BY " .$order;
                                //die($sql);
                                $i = 1;
                                $total = 0;
                                $total_nds = 0;
                                $total_wnds = 0;
                                $period = $_POST["year"]."-".$_POST["month"]; 
                                $included = $DB->getCell("SELECT DISTINCT(`task_id`) FROM `list_contr_calc` AS lltc WHERE 
                                                            DATE_FORMAT(lltc.update_date, '%Y-%m-%d')>".$DB->F(date("Y-m-d", strtotime($period." + 1 month + ".getcfg('connfinreport_dc')." days"))));
                                $wtList = kontr_plugin::getPItemsByAgrAndTaskFilter($_POST['agr_id'], $sql);
                                if (!count($wtList)) {
                                    $DB->free();
                                    UIError("Отсутствует прайс для контрагента по выбранному договору");
                                }
                                $rtpl->setVariable("WT_COUNT", count($wtList));
                                foreach($wtList as $item) {
                                    $rtpl->setCurrentBlock("wt_col");
                                    $rtpl->setVariable("WT_SERVICENAME", $item["s_title"]);
                                    $rtpl->parse("wt_col");
                                }
                                $DB->query($base_sql);
                                if ($DB->errno()) UIError($DB->error(). " " . $sql);
                                while ($res1 = $DB->fetch(true)) {
                                    $ticket = new ConnectionTicket($res1["task_id"]);
                                    $rtpl->setCurrentBlock("rep_row");
                                    if (in_array($res1["task_id"], $included, true)) {
                                            $rtpl->setVariable("ALREADYCOUNTED", " class='countedandincluded' ");                                            
                                    } else
                                        $rtpl->setVariable("ALREADYCOUNTED", " class='counted' ");
                                    
                                    
                                    
                                    
                                    /*if ($ticket->getFindateContr()) {
                                        $rtpl->setVariable("ALREADYCOUNTED", " class='counted' ");
                                    }*/
                                    $rtpl->setVariable("RR_ID", $i);
                                    
                                    if ($doneStatus) {
                                        /*$sql = "SELECT DATE_FORMAT(`datetime`, '%d.%m.%Y') as doneDate FROM `task_comments` WHERE `task_id`=".$DB->F($res1["task_id"])." AND `status_id`=".$DB->F($doneStatus["id"]).";";
                                        $doneDate = $DB->getField($sql);*/
                                        if ($res1["datetime"]) {
                                            $rtpl->setVariable("RR_DATE", $res1["datetime"]);
                                        } else {
                                            $rtpl->setVariable("RR_DATE", "&mdash;");                                     
                                        }
                                    } else {
                                        $rtpl->setVariable("RR_DATE", "&mdash;");                 
                                    }
                                    $rtpl->setVariable("RR_OPNUM", $ticket->getClntTNum());
                                    $rtpl->setVariable("RR_PIN", $ticket->getClntOpAgr());
                                    
                                    $rtpl->setVariable("RR_GSSNUM", $ticket->getId());
                                    $rtpl->setVariable("RR_AREA", $ticket->getArea());
                                    
                                    //$rtpl->setVariable("RR_ADDRESS", $ticket->getAddr()." кв. ".$ticket->getKv());
                                    //$rtpl->setVariable("RR_SERVICETYPE", $ticket->getTypeName());
                                    $sum = 0;
                                    //print_r($price);
                                    
                                    // TMC
                                    $sql = "SELECT tmc_tech.id, tmc_tech.count, tmc_sc.serial, tmc_sc.tmc_id, tmc_list.title, tickdata.isrent, tmc_list.metric_flag, tmc_list.cat_id FROM `tmc_ticket` AS tickdata 
                                            LEFT JOIN `tmc_sc_tech` AS tmc_tech ON tmc_tech.id=tickdata.tmc_tech_sc_id 
                                            LEFT JOIN `tmc_sc` AS tmc_sc ON tmc_sc.id=tmc_tech.tmc_sc_id 
                                            LEFT JOIN `list_materials` AS tmc_list ON tmc_list.id=tmc_sc.tmc_id 
                                            WHERE tickdata.task_id=".$DB->F($res1["task_id"]).";";
                                    $DB->query($sql);
                                    if ($DB->errno()) { echo "error ".$sql." ".$DB->error(); trigger_error($DB->error()); return false; }
                                    $rowspan = 0;
                                    
                                    $rC = $DB->num_rows();
                                    if ($rC>1) {
                                        $first = true;
                                        $rtpl->setVariable("TMCCOUNT", $rC);
                                        $rowspan = $rC;
                                        while ($res = $DB->fetch(true)) {
                                            if ($first) {
                                                if ($res["metric_flag"]) {
                                                    $rtpl->setVariable("TMC_METRIC_TITLE", $res["title"]);
                                                    $rtpl->setVariable("TMC_METRIC_AMMOUNT", $res["count"]);
                                                    $rtpl->setVariable("TMC_NONMETRIC_TYPE", "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_TITLE", "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_SERIAL", "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_QUANTITY", "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_ISRENT", "&mdash;");
                                                } else {
                                                    $tmcCat = adm_material_cats_plugin::getById($res["cat_id"]);
                                                    $rtpl->setVariable("TMC_METRIC_TITLE", "&mdash;");
                                                    $rtpl->setVariable("TMC_METRIC_AMMOUNT", "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_TYPE", $tmcCat ? $tmcCat : "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_TITLE", $res["title"]);
                                                    $rtpl->setVariable("TMC_NONMETRIC_SERIAL", $res["serial"]);
                                                    $rtpl->setVariable("TMC_NONMETRIC_QUANTITY", $res["count"]);
                                                    $rtpl->setVariable("TMC_NONMETRIC_ISRENT", $res["isrent"] ? "да" : "нет");
                                                }
                                                $first = false;
                                                
                                            } else {
                                                $rtpl->setCurrentBlock("tmc_val");
                                                if ($res["metric_flag"]) {
                                                    $rtpl->setVariable("ATMC_METRIC_TITLE", $res["title"]);
                                                    $rtpl->setVariable("ATMC_METRIC_AMMOUNT", $res["count"]);
                                                    $rtpl->setVariable("ATMC_NONMETRIC_TYPE", "&mdash;");
                                                    $rtpl->setVariable("ATMC_NONMETRIC_TITLE", "&mdash;");
                                                    $rtpl->setVariable("ATMC_NONMETRIC_SERIAL", "&mdash;");
                                                    $rtpl->setVariable("ATMC_NONMETRIC_QUANTITY", "&mdash;");
                                                    $rtpl->setVariable("ATMC_NONMETRIC_ISRENT", "&mdash;");
                                                } else {
                                                    $tmcCat = adm_material_cats_plugin::getById($res["cat_id"]);
                                                    $rtpl->setVariable("ATMC_METRIC_TITLE", "&mdash;");
                                                    $rtpl->setVariable("ATMC_METRIC_AMMOUNT", "&mdash;");
                                                    $rtpl->setVariable("ATMC_NONMETRIC_TYPE",  $tmcCat ? $tmcCat : "&mdash;");
                                                    $rtpl->setVariable("ATMC_NONMETRIC_TITLE", $res["title"]);
                                                    $rtpl->setVariable("ATMC_NONMETRIC_SERIAL", $res["serial"]);
                                                    $rtpl->setVariable("ATMC_NONMETRIC_QUANTITY", $res["count"]);
                                                    $rtpl->setVariable("ATMC_NONMETRIC_ISRENT", $res["isrent"] ? "да" : "нет");
                                                }
                                                if ($ticket->getFindateContr()) {
                                                    $rtpl->setVariable("ALREADYCOUNTEDTMC", " class='counted' ");
                                                }
                                                $rtpl->parse("tmc_val");
                                            }
                                        }
                                        
                        
                                    } else {
                                        $res = $DB->fetch(true);
                                        $rtpl->setVariable("TMCCOUNT", "1");
                                        $rowspan = 1;
                                        if ($DB->num_rows()) {
                                            if ($res["metric_flag"]) {
                                                    $rtpl->setVariable("TMC_METRIC_TITLE", $res["title"]);
                                                    $rtpl->setVariable("TMC_METRIC_AMMOUNT", $res["count"]);
                                                    $rtpl->setVariable("TMC_NONMETRIC_TYPE", "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_TITLE", "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_SERIAL", "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_QUANTITY", "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_ISRENT", "&mdash;");
                                                } else {
                                                    $tmcCat = adm_material_cats_plugin::getById($res["cat_id"]);
                                                    $rtpl->setVariable("TMC_METRIC_TITLE", "&mdash;");
                                                    $rtpl->setVariable("TMC_METRIC_AMMOUNT", "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_TYPE",  $tmcCat ? $tmcCat : "&mdash;");
                                                    $rtpl->setVariable("TMC_NONMETRIC_TITLE", $res["title"]);
                                                    $rtpl->setVariable("TMC_NONMETRIC_SERIAL", $res["serial"]);
                                                    $rtpl->setVariable("TMC_NONMETRIC_QUANTITY", $res["count"]);
                                                    $rtpl->setVariable("TMC_NONMETRIC_ISRENT", $res["isrent"] ? "да" : "нет");
                                                }
                                            
                                        } else {
                                            $rtpl->setVariable("TMC_METRIC_TITLE", "&mdash;");
                                            $rtpl->setVariable("TMC_METRIC_AMMOUNT", "&mdash;");
                                            $rtpl->setVariable("TMC_NONMETRIC_TYPE", "&mdash;");
                                            $rtpl->setVariable("TMC_NONMETRIC_TITLE", "&mdash;");
                                            $rtpl->setVariable("TMC_NONMETRIC_SERIAL", "&mdash;");
                                            $rtpl->setVariable("TMC_NONMETRIC_QUANTITY", "&mdash;");
                                            $rtpl->setVariable("TMC_NONMETRIC_ISRENT", "&mdash;");
                                            $rowspan = 0;
                                            // no records                                        
                                        }
                                        
                                    }
                                    $DB->free();
                                    $addr = $ticket->getAddr($ticket->getDomId());
                                    if (isset($addr["nokladr"])) {
                                        $rtpl->setCurrentBlock("no_kladr");
                                        $rtpl->setVariable("TMCCOUNT2", $rowspan);
                                        $rtpl->parse("no_kladr");
                                    } else {
                                        $rtpl->setCurrentBlock("kladr_addr");
                                        $rtpl->setVariable("TMCCOUNT3", $rowspan);
                                        $rtpl->setVariable("RR_ADDR_CITY", $addr["city"]);
                                        $rtpl->setVariable("RR_ADDR_STREET", $addr["street"]);
                                        $rtpl->setVariable("RR_ADDR_HOUSE", "д.".$addr["house"]);
                                        $rtpl->setVariable("RR_ADDR_FLAT", $ticket->getKv());
                                        $rtpl->parse("kladr_addr");
                                    }
                                    foreach($wtList as $item) {
                                        $price = connections_plugin::getCWTypeID1($ticket->getId(),$item["id"], $_POST["year"]."-".$_POST["month"]);
                                        $rtpl->setCurrentBlock("wt_val");
                                        $rtpl->setVariable("TMCCOUNT1", $rowspan);
                                        $sum += $price[0]["price_type"] != "smeta" ? $price[0]["price"]*$price[0]["quant"] : $price[0]["smeta_val"];
                                        $rtpl->setVariable("WT_RR_PRICE", $price[0]["price_type"] != "smeta" ? $price[0]["price"]*$price[0]["quant"] : $price[0]["smeta_val"]);
                                        $rtpl->parse("wt_val");                                   
                                    }
                                    $nds = round($sum*18/100, 2);
                                    $total += $sum;
                                    $total_nds += $nds;
                                    $total_wnds += $sum + $nds;
                                    $rtpl->setVariable("RR_SUMMWONDS", number_format($sum, 2, ',', ' '));
                                    $rtpl->setVariable("RR_NDSVAL", number_format($nds, 2, ',', ' '));
                                    $rtpl->setVariable("RR_SUMMWNDS", number_format($sum + $nds, 2, ',', ' '));
                                    
                                    
                                    $rtpl->parse("rep_row");
                                    $i+=1;
                                }
                                $rtpl->setVariable("TOTALCOLSPAN", 10+intval(count($wtList)));
                                $rtpl->setVariable("TOTAL_WONDS", number_format($total, 2, ',', ' '));
                                $rtpl->setVariable("TOTAL_NDS", number_format($total_nds, 2, ',', ' '));
                                $rtpl->setVariable("TOTAL_WNDS", number_format($total_wnds, 2, ',', ' '));
                                $DB->free();
                            } else {
                                UIError("Отсутствует статус с тэгом closed");
                            }
                        } else {
                            UIError("Отсутствует Контрагент с указанным ID.");
                        }
                    } else {
                        UIError("Отсутствует модуль Контрагенты.");
                    }
                } else UIError("Не указан контрагент и/или договор!"); 
                
                $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
                $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
                if (isset($_POST["createxlsversion"])) {
                    header("Content-type: application/vnd.ms-excel");  
                    header("Content-disposition: attachment; filename=report_contr_".$_POST['datefrom']."-".$_POST['dateto'].".xls");  
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    if (isset($_POST["createarchiveitem"])) {
                        $result = $rtpl->get();
                        $sql = "INSERT INTO list_f_contr_reports (`period`, `body`, `cr_uid`) VALUES (".$DB->F(base64_encode($month[$_POST['month']]." ".$year[$_POST['year']])).", ".$DB->F(base64_encode($result)).", ".$DB->F($USER->getId()).")";
                        $DB->query($sql);
                        if ($DB->errno()) UIError($DB->error() ." ". $sql);
                        redirect($this->getLink('archive'), "Отчет сохранен в Архиве!");
                    } else {
                        if (isset($_POST['createprintversion'])) {
                            $rtpl->touchBlock("print_footer");
                            $rtpl->show();
                            
                            return;
                        } else {
                            $tpl->setCurrentBlock("reportval");
                            $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                            $tpl->parse("reportval");
                        }
                    }
                }
            } else {
                $tpl->touchBlock("notcreated");
                $tpl->setVariable("DISABLED", "disabled='disabled'");
            }
        
        
        $tpl->show();
        
    }
 

}
?>
