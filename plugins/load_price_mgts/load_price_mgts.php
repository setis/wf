<?php

/**
 * Plugin Implementation
 * 
 * @author kblp
 * @copyright 2011
 */

use classes\HTML_Template_IT;
use classes\Plugin;


 
/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 * 
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
 
class load_price_mgts_plugin extends Plugin
{       
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function viewprice()
    {
        global $DB, $USER;
        $nocp = false;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        $sql = "SELECT l.id, l.contrcode, l.price, (SELECT `title` FROM `list_skp_p_wtypes` WHERE `id`=l.wt_id) as  title, IF( ptype = 1, 1, 0) as ptype FROM `list_skp_p_price` as l WHERE l.contr_id=".$DB->F(getcfg('mgts_contr_id'))." HAVING ptype=1 ORDER BY `id`;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            while ($m = $DB->fetch(true)) {
                $cp[$m["contrcode"]] = $m;
            }
        }
        if (!$m) {
            $nocp = true;
        }
        
        $tpl->loadTemplatefile($USER->getTemplate()."/load_price_mgts.tmpl.htm");
        
        $host = getcfg('mgts_host');
        $user = getcfg('mgts_user');
        $password = getcfg('mgts_password');
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        if (isset($_POST["goimport"])) {
            $tpl->setCurrentBlock("import_results");
            $tpl->setVariable("CURRENT_DATE", rudate("d M Y"));
            try {
                $client = new SoapClient("https://$host:8208/SoapServer.php?wsdl",array("login"=>"$user", "password"=>"$password", "trace" => 1, "exceptions" => 1, "encoding"=> "UTF-8", 'stream_context'=>stream_context_create(array("ssl"=>array("verify_peer"=>false)))));
                $params = $client->getHelpServiceList();
                $i = 1;
                if (count($params)) {
                    foreach($params as $item) {
                        $errStatus = array();
                        $tpl->setCurrentBlock("ir-item");
                        $tpl->setVariable("NUM", $i);
                        $tpl->setVariable("PRICE", number_format($item["SV_PRICE"], 2, ",", " "));
                        if (sizeof($cp[$item["SV_FORIS_CODE"]])) {
                            $tpl->setVariable("IDMGTS", "<a href=\"".link2("adm_skp_price/edit?id=".$cp[$item["SV_FORIS_CODE"]]["id"], false)."\" target=\"_blank\">".$item["SV_ID"]."</a>");
                            $tpl->setVariable("FORIS", "<a href=\"".link2("adm_skp_price/edit?id=".$cp[$item["SV_FORIS_CODE"]]["id"], false)."\" target=\"_blank\">".$item["SV_FORIS_CODE"]."</a>");
                            $tpl->setVariable("TITLE", "<a href=\"".link2("adm_skp_price/edit?id=".$cp[$item["SV_FORIS_CODE"]]["id"], false)."\" target=\"_blank\">".$item["SV_NAME"]."</a>");
                            $sql = "UPDATE `list_skp_p_price` SET `intcode`=".$DB->F($item["SV_ID"])." WHERE `id`=".$DB->F($cp[$item["SV_FORIS_CODE"]]["id"]).";";
                            $kkmm = $DB->query($sql);
                            $DB->free($kkmm);
                            //$a = preg_replace('/[^A-Za-z0-9\-\s]/', '', trim($cp[$item["SV_FORIS_CODE"]]["title"]));
                            //$b = preg_replace('/[^A-Za-z0-9\-\s]/', '', trim($item["SV_NAME"]));
                            $a = preg_replace("/(\")|(&quot;)/", "", trim($item["SV_NAME"]));
                            $b =  str_replace("\"", "", trim($item["SV_NAME"]));
                            if ($a != $b) {
                                $errStatus[] = "Название в WF: ".$cp[$item["SV_FORIS_CODE"]]["title"];
                            }
                            //$tpl->setVariable("TITLE", $a ."  ".$b);
                            if (floatval(trim($item["SV_PRICE"])) != floatval(trim($cp[$item["SV_FORIS_CODE"]]["price"]))) {
                                $errStatus[] = "Цена МГТС: ".$item["SV_PRICE"] .", WF: ".$cp[$item["SV_FORIS_CODE"]]["price"];                                
                            }
                            unset($cp[$item["SV_FORIS_CODE"]]);
                            if (count($errStatus)) {
                                $tpl->setVariable("CPSTATUS", implode("<br />", $errStatus));
                                $tpl->setVariable("CPSTATUS_STYLE", "color: #FFFFFF; background: #eea236; font-weight: bold;");
                            } else {
                                $tpl->setVariable("CPSTATUS", "OK");
                                $tpl->setVariable("CPSTATUS_STYLE", "color: #FFFFFF; background: #5cb85c; font-weight: bold; text-align: center;");                                
                            }
                        } else {
                            $tpl->setVariable("IDMGTS", $item["SV_ID"]);
                            $tpl->setVariable("FORIS", $item["SV_FORIS_CODE"]);
                            $tpl->setVariable("TITLE", $item["SV_NAME"]);
                        
                            $tpl->setVariable("CPSTATUS", "Нет в WF");
                            $tpl->setVariable("CPSTATUS_STYLE", "color: #FFFFFF; background: #FF0000; font-weight: bold; text-align: center;");
                            
                        }
                        $tpl->parse("ir-item");
                        $i+=1;
                    }
                    $first = true;
                    if (count($cp)) {
                        
                        foreach($cp as $item) {
                            $tpl->setCurrentBlock("ir-item");
                            if ($first) {
                                $tpl->setCurrentBlock("int-rows");
                                $tpl->setVariable("ROW_STYLE", "background-color:#d4e73a !important;");
                                $tpl->parse("int-rows");
                                $first = false;
                            }
                            $tpl->setVariable("NUM", $i);
                            $tpl->setVariable("IDMGTS", "<a href=\"".link2("adm_skp_price/edit?id=".$item["id"], false)."\" target=\"_blank\">".$item["id"]."</a>");
                            $tpl->setVariable("FORIS", "<a href=\"".link2("adm_skp_price/edit?id=".$item["contrcode"], false)."\" target=\"_blank\">".$item["contrcode"]."</a>");
                            $tpl->setVariable("TITLE", "<a href=\"".link2("adm_skp_price/edit?id=".$item["title"], false)."\" target=\"_blank\">".$item["title"]."</a>");
                            $tpl->setVariable("PRICE", number_format($item["price"], 2, ",", " "));
                            $tpl->setVariable("CPSTATUS", "Нет в МГТС");
                            $tpl->setVariable("CPSTATUS_STYLE", "color: #000000; background: #FFFFFF; font-weight: bold; text-align: center;");                                

                            $tpl->parse("ir-item");
                            $i+=1;
                        }
                    }
                } else {
                    $tpl->touchBlock("no-records");
                }
            }
            catch (SoapFault $fault) {
                $tpl->setCurrentBlock("error");
                $tpl->setVariable("ERRORCODE", "Ошибка запроса в МГТС: ".$fault->faultstring);                
                $tpl->parse("error");
            }
            if (is_soap_fault($params)) {
               $tpl->setCurrentBlock("error");
               $tpl->setVariable("ERRORCODE", "Ошибка запроса в МГТС: ".$fault->faultstring);                
               $tpl->parse("error");
            }
            $tpl->parse("import_results");
        } else {
        
            if (isset($_POST["goimportxls"])) {
                $tpl->loadTemplatefile($USER->getTemplate()."/load_price_xls.tmpl.htm");
                $tpl->setCurrentBlock("import_results");
                $tpl->setVariable("CURRENT_DATE", rudate("d M Y")); 
                try {
                    $client = new SoapClient("https://$host:8208/SoapServer.php?wsdl",array("login"=>"$user", "password"=>"$password", "trace" => 1, "exceptions" => 1, "encoding"=> "UTF-8", 'stream_context'=>stream_context_create(array("ssl"=>array("verify_peer"=>false)))));
                    $params = $client->getHelpServiceList();
                    $i = 1;
                    if (count($params)) {
                        foreach($params as $item) {
                            $tpl->setCurrentBlock("ir-item");
                            $tpl->setVariable("NUM", $i);
                            $tpl->setVariable("IDMGTS", $item["SV_ID"]);
                            $tpl->setVariable("FORIS", $item["SV_FORIS_CODE"]);
                            $tpl->setVariable("TITLE", $item["SV_NAME"]);
                            $tpl->setVariable("PRICE", number_format($item["SV_PRICE"], 2, ",", " "));
                            $tpl->parse("ir-item");
                            $i+=1;
                        }
                    } else {
                        $tpl->touchBlock("no-records");
                    }
                    header("Content-type: application/vnd.ms-excel");  
                    header("Content-disposition: attachment; filename=mgts_skp_price_".date("H_i_d-M-Y").".xls");  
                }
                catch (SoapFault $fault) {
                    redirect("", "Ошибка запроса в МГТС. Для получения кода и причины ошибки выполните обычный импорт (без xls)");
                    return false;                
                }
                if (is_soap_fault($params)) {
                    redirect("", "Ошибка запроса в МГТС. Для получения кода и причины ошибки выполните обычный импорт (без xls)");
                    return false;      
                }
                $tpl->parse("import_results");
            }
        }
        UIHeader($tpl);
        $tpl->show();
    }
}
?>