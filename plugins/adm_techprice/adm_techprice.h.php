<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Прайс на работы техников";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewprice'] = "Прайс на работы техников";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['jobtype_edit'] = "Редактирование элемента прайса техников (вида работ)";
    $PLUGINS[$plugin_uid]['events']['jobtype_save'] = "Сохранение элемента прайса техников (вида работ)";
    $PLUGINS[$plugin_uid]['events']['jobtype_del'] = "Удаление элемента прайса техников (вида работ)";
}
?>