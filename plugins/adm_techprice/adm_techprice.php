<?php

/**
 * Plugin Implementation
 * 
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;



/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 * 
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
 
class adm_techprice_plugin extends Plugin
{   
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    
    function viewprice()
    {
        global $DB, $USER, $PLUGINS;
        
        //if(!($plugin = $_GET['plugin'])) UIError("Не указан модуль!");
        
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("list.tmpl.htm");
        if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/list.tmpl.htm");
            else 
                $tpl->loadTemplatefile("list.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $sql = "SELECT * FROM `list_tech_price` ORDER BY `title`;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            while (list($id, $title, $stitle, $price_type, $price_val, $p_comment, $arc, $convert, $tf) = $DB->fetch()) {
                $tpl->setCurrentBlock("row");
                $tpl->setVariable("JT_ID", $id);
                $tpl->setVariable("JT_TITLE", $title);
                $tpl->setVariable("JT_SHORT_TITLE", $stitle);
                $tpl->setVariable("JT_TAG", $price_type == "smeta" ? "Смета" : "Прайс");
                if ($price_val<0) 
                $tpl->setVariable("JT_PRICE", "<font color='red'><strong>".$price_val."</strong></font>");
                else
                $tpl->setVariable("JT_PRICE", $price_val. ($price_type!= "price" ? "%" : ""));
                $tpl->setVariable("JT_COMMENT", $p_comment);
                $tpl->setVariable("JT_CONVERT", $convert ? "Да" : "&mdash;");
                $tpl->setVariable("JT_TF", $tf ? "Да" : "&mdash;");
                $tpl->setVariable("JT_ARCHIVE", $arc ? "Да" : "&mdash;");
                
                $tpl->parse("row");
            }
        } else {
            $tpl->touchBlock("no-rec");
        }
        
        $DB->free();
                
        UIHeader($tpl);
        $tpl->show();
    }
    
    function getPItemsByType($type) {
        global $DB;
        if (!$type) return false;
        $sql = "SELECT * FROM `list_tech_price` WHERE `price_type`=".$DB->F($type)." AND `price`>0;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;    
        } else {
            while($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }
    function getPItemsByType1($type) {
        global $DB;
        if (!$type) return false;
        $sql = "SELECT * FROM `list_tech_price` WHERE `price_type`=".$DB->F($type)." AND `price`>0 AND `archive`<1;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;    
        } else {
            while($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }
    
    static function getPItemsById($id) {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT * FROM `list_tech_price` WHERE `id`=".$DB->F($id).";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;    
        } else {
            $ret = $DB->fetch(true);
        }
        $DB->free();
        return $ret;
    }
    
    static function getPItemsList() {
        global $DB;
        $sql = "SELECT * FROM `list_tech_price` WHERE `price`>0;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;    
        } else {
            while($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }
    
    static function getPItemsListRep() {
        global $DB;
        $sql = "SELECT * FROM `list_tech_price` AS ltp WHERE 1;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;    
        } else {
            while($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }

    /**
     * Возврщает прайс из расчётов техников для заявок техников типа Подключение по переданному списку задач \$sql
     * @param $sql
     * @return array|bool
     */
    static function getPItemsListRepSC($sql) {
        global $DB;

        if (!$sql) return false;
        $sql = "
            SELECT
                  *
            FROM
                  `list_tech_price` AS ltp
            WHERE
                  `id` IN (
                      SELECT DISTINCT
                          work_id
                      FROM
                          `list_tech_calc`
                      WHERE
                          `task_id` IN (".$sql.")
                  )
            ;
        ";
        $DB->query($sql);

        if ($DB->errno())
            UIError($DB->error());

        if (!$DB->num_rows()) {
            $ret = false;    
        } else {
            while($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }

        $DB->free();
        return $ret;
    }

    /**
     * Возврщает прайс из расчётов техников для заявок типа ТТ по переданному списку задач \$sql
     * @param $sql
     * @return array|bool
     */
    static function getPItemsListRepSCTT($sql) {
        global $DB;

        if (!$sql)
            return false;
        $sql = "
            SELECT
                  *
            FROM
                  `list_tech_price` AS ltp
            WHERE
                  `id` IN (
                        SELECT DISTINCT
                            work_id
                        FROM
                            `list_tt_tech_calc`
                        WHERE
                            `task_id` IN (".$sql.")
                  )
            ;
        ";
        $DB->query($sql);

        if ($DB->errno())
            UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;    
        } else {
            while($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();

        return $ret;
    }
    
    static function getPItemsListRepSC2($sql) {
        global $DB;
        $sql = "SELECT * FROM `list_tech_price` AS ltp WHERE `id` IN (SELECT DISTINCT work_id FROM `list_tech_calc` WHERE `task_id` IN (".$sql.")) OR `id` IN (SELECT DISTINCT work_id FROM `list_tt_tech_calc` WHERE `task_id` IN (".$sql."));";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;    
        } else {
            while($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }
    
    
    static function getPItemsListRepSC1($sql) {
        global $DB;
        $sql = "SELECT * FROM `list_tech_price` AS ltp WHERE `id` IN (SELECT DISTINCT work_id FROM `list_tech_calc` WHERE `task_id` IN (".$sql."));";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;    
        } else {
            while($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }
    
    
    
    
    static function getPItemsShtrafList() {
        global $DB;
        $sql = "SELECT * FROM `list_tech_price` WHERE `price`<0;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;    
        } else {
            while($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }
    
    static function getPItemsShtrafList1() {
        global $DB;
        $sql = "SELECT * FROM `list_tech_price` WHERE `price`<0 AND `archive`<1;";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;    
        } else {
            while($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }
        
        
    function jobtype_edit()
    {
        global $DB, $USER, $PLUGINS;
        
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("editor.tmpl.htm");
        if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/editor.tmpl.htm");
            else 
                $tpl->loadTemplatefile("editor.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        if ($jt_id = $_REQUEST["jt_id"]) {
            $rec = $DB->getRow("SELECT * FROM `list_tech_price` WHERE `id`=".$DB->F($jt_id).";", true);
            if ($rec) {
                $tpl->setVariable("ID", $rec["id"]);
                $tpl->setVariable("TITLE", $rec["title"]);
                $tpl->setVariable("SHORT_TITLE", $rec["short_title"]);
                if ($rec["price"]<0) {
                    $tpl->setVariable("SHTRAF", "checked='checked'");
                    $tpl->setVariable("CONVERT_BONUS", "disabled='disabled'");
                    $tpl->setVariable("TF_BONUS", "disabled='disabled'");
                }
                if ($rec["price_type"] == "price")
                    $tpl->setVariable("PRICE_TYPE_PRICE", "selected=\"selected\"");
                else
                    $tpl->setVariable("PRICE_TYPE_SMETA", "selected=\"selected\"");
                $tpl->setVariable("PRICE_VAL", $rec["price"] .($rec["price_type"] != "price" ? "%" : ""));
                $tpl->setVariable("COMMENT", $rec["p_comment"]);
                $tpl->setVariable("ARCHIVE", $rec["archive"] ? "checked='checked'" : "");
                if ($rec["archive"]) {
                    $tpl->setVariable("CONVERT_BONUS", "disabled='disabled'");
                    $tpl->setVariable("TF_BONUS", "disabled='disabled'");
                } else {
                    if ($rec["convert_bonus"] && !$rec["archive"] && $rec["price"]>0) {
                        $tpl->setVariable("CONVERT_BONUS", "checked='checked'");
                    }
                    if ($rec["timeslot_bonus"] && !$rec["archive"] && $rec["price"]>0) { 
                        $tpl->setVariable("TF_BONUS", "checked='checked'");                        
                    }
                 }
            } else {
                UIError("Запись с указанным идентификатором отсутствует!");
            }
        } else {
            //$tpl->setVariable("PRICE_VAL", "0.00");
        }
        UIHeader($tpl);
        $tpl->show();
    }
    
    function jobtype_save()
    {
        global $DB, $USER;
        $err = array();
        if ($_POST["title"] && $_POST["short_title"] && $_POST["price"]) {
            if (isset($_POST["shtraf"])) {
                if ($jt_id = $_POST["jt_id"]) {
                    if ($_POST["archive"] == "") {
                        if ($_POST["convert_bonus"] != "") {
                            $sql_add[] = " `convert_bonus`=1 ";
                            $res = $DB->query("UPDATE `list_tech_price` SET `convert_bonus`=0 WHERE 1;");
                            $DB->free($res);
                        } else 
                            $sql_add[] = " `convert_bonus`=0 ";
                        
                        if ($_POST["time_bonus"] != "") {
                            $sql_add[] = " `timeslot_bonus`=1 ";
                            $res = $DB->query("UPDATE `list_tech_price` SET `timeslot_bonus`=0 WHERE 1;");
                            $DB->free($res);
                        } else
                            $sql_add[] = " `timeslot_bonus`=0 ";
                        $sql_add = " , ".implode(",", $sql_add);
                    }
                    $sql = "UPDATE `list_tech_price` SET `archive`=".$DB->F($_POST["archive"]!= "" ? 1 : 0).", `title`=".$DB->F($_POST["title"]).", `short_title`=".$DB->F($_POST["short_title"]).", 
                            `price`=".$DB->F($_POST["price"]).", `price_type`='price', `p_comment`=".$DB->F($_POST["comment"])." $sql_add WHERE `id`=".$DB->F($jt_id).";";    
                } else {
                    $sql = "INSERT INTO `list_tech_price` (`title`, `short_title`, `price_type`, `price`, `p_comment`, `archive`, `convert_bonus`, `timeslot_bonus`) 
                            VALUES (".$DB->F($_POST["title"]).",".$DB->F($_POST["short_title"]).",'price',".$DB->F($_POST["price"]).",".$DB->F($_POST["comment"]).", ".$DB->F($_POST["archive"]!= "" ? 1 : 0).", ".$DB->F($_POST["convert_bonus"]!= "" ? 1 : 0).", ".$DB->F($_POST["time_bonus"]!= "" ? 1 : 0).");";
                }
            } else {
                if ($jt_id = $_POST["jt_id"]) {
                    if ($_POST["archive"] == "") {
                        if ($_POST["convert_bonus"] != "") {
                            $sql_add[] = " `convert_bonus`=1 ";
                            $res = $DB->query("UPDATE `list_tech_price` SET `convert_bonus`=0 WHERE 1;");
                            $DB->free($res);
                            $_POST["price_type"] = "price";
                        } else 
                            $sql_add[] = " `convert_bonus`=0 ";
                        
                        if ($_POST["time_bonus"] != "") {
                            $sql_add[] = " `timeslot_bonus`=1 ";
                            $res = $DB->query("UPDATE `list_tech_price` SET `timeslot_bonus`=0 WHERE 1;");
                            $DB->free($res);
                            $_POST["price_type"] = "price";
                        } else
                            $sql_add[] = " `timeslot_bonus`=0 ";
                        $sql_add = " , ".implode(",", $sql_add);
                    }
                    $sql = "UPDATE `list_tech_price` SET `archive`=".$DB->F($_POST["archive"]!= "" ? 1 : 0).", `title`=".$DB->F($_POST["title"]).", `short_title`=".$DB->F($_POST["short_title"]).", 
                            `price`=".$DB->F($_POST["price"]).", `price_type`=".$DB->F( $_POST["price_type"]).", `p_comment`=".$DB->F($_POST["comment"])." $sql_add WHERE `id`=".$DB->F($jt_id).";";    
                } else {
                    $sql = "INSERT INTO `list_tech_price` (`title`, `short_title`, `price_type`, `price`, `p_comment`, `archive`, `convert_bonus`, `timeslot_bonus`) 
                            VALUES (".$DB->F($_POST["title"]).",".$DB->F($_POST["short_title"]).",".$DB->F($_POST["price_type"]).",".$DB->F($_POST["price"]).",".$DB->F($_POST["comment"]).", ".$DB->F($_POST["archive"]!= "" ? 1 : 0).", ".$DB->F($_POST["convert_bonus"]!= "" ? 1 : 0).", ".$DB->F($_POST["time_bonus"]!= "" ? 1 : 0).");";
                }       
            }    
            $DB->query($sql);
            if (!$jt_id) {
                $jt_id = $DB->insert_id();
            }
            if ($DB->errno()) UIError($DB->error()); 
            redirect($this->getLink('viewprice'), "Запись успешно сохранена!");
        } else {
            UIError("Пожалуйста, заполните форму целиком и повторите!");
        }
    }
    
    function jobtype_del()
    {
        global $DB, $USER;
        if ($jt_id = $_REQUEST["jt_id"]) {
            if (!$this->is_busy($jt_id)) {
                $sql = "DELETE FROM `list_tech_price` WHERE `id`=".$DB->F($jt_id).";";
                $DB->query($sql);
                if (!$DB->errno()) 
                    redirect($this->getLink("viewprice"), "Удаление успешно!");
                else 
                    UIError($DB->error());
            } else {
                UIError("Выбранная запись связана с расчетами по заявкам! Удаление не возможно.");
            }
        } else {
            UIError("Не указан идентификатор записи для удаления!");
        }

    }

    function is_busy($id) {
        global $DB;
        
        $sql = "SELECT COUNT(`work_id`) FROM `list_tech_calc` WHERE `work_id`=".$DB->F($id).";";
        
        $res = $DB->getField($sql) ? $DB->getField($sql) : false;
        
        return $res;
    
        
    }
    
    
}