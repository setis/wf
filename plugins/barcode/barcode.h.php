<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Штрих-коды заявок";
$PLUGINS[$plugin_uid]['hidden'] = true;

$PLUGINS[$plugin_uid]['events']['encodec39'] = "Просмотр штрих-кода (code39)";
$PLUGINS[$plugin_uid]['events']['eprintc39'] = "Просмотр для печати штрих-кода (code39)";
$PLUGINS[$plugin_uid]['events']['printc39'] = "Печать штрих-кода (code39)";

?>