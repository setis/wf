<?php
use classes\Barcode39;
use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;

class barcode_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__file__, '.php'));
    }

    function encodec39()
    {
        // set Barcode39 object
        $target = $_REQUEST["target_id"];
        $type = $_REQUEST['type'];
        switch ($type) {
            case "A":
                $add = "-0";
                break;
            case "T":
                $add = "-1";
                break;
            default:
                $add = "-0";
        }

        $t = new Task($target);

        ob_start();
        if ($t && $target) {
            if ($t->getId() == $target) {
                $bc = new Barcode39($target . $add);
                $bc->barcode_height = 100;
                $bc->barcode_width = 660;
                $bc->draw();
            } else {
                $bc = new Barcode39("NOTASKID");
                $bc->barcode_fg_rgb = [255, 128, 0];
                $bc->barcode_height = 100;
                $bc->barcode_width = 660;
                $bc->draw();

            }
        } else {
            $bc = new Barcode39("ERROR");
            $bc->barcode_fg_rgb = [255, 0, 0];
            $bc->barcode_height = 100;
            $bc->barcode_width = 660;
            $bc->draw();
        }

        $content = ob_get_contents();
        ob_end_clean();

        return new \Symfony\Component\HttpFoundation\Response($content, 200, [
            "Content-type" => "image/gif",
        ]);

    }


    function printc39()
    {
        // set Barcode39 object
        $target = $_REQUEST["target_id"];
        $type = $_REQUEST['type'];
        switch ($type) {
            case "A":
                $add = "-0";
                break;
            case "T":
                $add = "-1";
                break;
            default:
                $add = "-0";
        }
        $t = new Task($target);

        ob_start();
        if ($t && $target) {
            if ($t->getId() == $target) {
                $bc = new Barcode39($target . $add);
                $bc->barcode_height = 100;
                $bc->barcode_width = 660;
                $bc->draw();
            } else {
                $bc = new Barcode39("NOTASKID");
                $bc->barcode_fg_rgb = [255, 128, 0];
                $bc->barcode_height = 100;
                $bc->barcode_width = 660;
                $bc->draw();

            }
        } else {
            $bc = new Barcode39("ERROR");
            $bc->barcode_fg_rgb = [255, 0, 0];
            $bc->barcode_height = 100;
            $bc->barcode_width = 660;
            $bc->draw();
        }

        $content = ob_get_contents();
        ob_end_clean();

        return new \Symfony\Component\HttpFoundation\Response($content, 200, [
            "Content-type" => "image/gif",
        ]);

    }

    function eprintc39()
    {
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("archive.tmpl.htm");
        if (wf::$user->getTemplate() != "default")
            $tpl->loadTemplatefile(wf::$user->getTemplate() . "/print.tmpl.htm");
        else
            $tpl->loadTemplatefile("print.tmpl.htm");
        $img = preg_replace("/\/$/", "", wf::$container->getParameter("http_base")) . preg_replace("/eprintc39/", "printc39", $_SERVER["REQUEST_URI"]);
        $tpl->setVariable("IMG_SOURCE", $img);
        $tpl->setVariable("HTTP_BASE", wf::$container->getParameter("http_base"));
        $tpl->show();

    }
}
