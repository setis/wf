<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class adm_ticket_rtsubtypes_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getOptions($id = 0)
    {
        global $DB;
        $sql = "SELECT id, title FROM `list_rtsubtype` WHERE 1;";
        $res = $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());
        return array2options($res, $id);
    }

    static function getById($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_rtsubtype` WHERE `id`=" . $DB->F($id) . ";";
        $res = $DB->getField($sql);
        if ($DB->errno()) UIError($DB->error());
        return $res ? $res : false;
    }

    static function getByTag($tag)
    {
        global $DB;
        $sql = "SELECT `id` FROM `list_rtsubtype` WHERE `tag`=" . $DB->F($tag) . ";";
        $res = $DB->getField($sql);
        if ($DB->errno()) UIError($DB->error());
        return $res ? $res : false;
    }

    function viewlist()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/adm_ticket_rtsubtypes.tmpl.htm");
        else
                $tpl->loadTemplatefile("adm_ticket_rtsubtypes.tmpl.htm");
        $sql = "SELECT * FROM `list_rtsubtype` WHERE 1;";
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());
        while (list($id, $tag, $title) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TAG", $tag);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->parse("vl_item");
        }
        $DB->free();
        UIHeader($tpl);
        $tpl->show();
    }

    function edit() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
        else
                $tpl->loadTemplatefile("edit.tmpl.htm");
        if ($id = $_REQUEST['id']) {
            $sql = "SELECT * FROM `list_rtsubtype` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Подтип с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);

            $tpl->setVariable("ID", $result["id"]);
            $tpl->setVariable("TITLE", $result["title"]);
            $tpl->setVariable("TAG", $result["tag"]);

        }
        UIHeader($tpl);
        $tpl->show();

    }

    function save() {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = array();

        if(!$_POST['title']) $err[] = "Не заполнено название подтипа";
        if(!$_POST['tag']) $err[] = "Не заполнено значение РТ";

        if(sizeof($err)) UIError($err);

        if($_POST['id']) $sql = "UPDATE `list_rtsubtype` SET `tag`=".$DB->F($_POST['tag']).", `title`=".$DB->F($_POST['title'])." WHERE `id`=".$DB->F($_POST['id']);
        else {
            $sql = "INSERT INTO `list_rtsubtype` (`tag`, `title`) VALUES(".$DB->F($_POST['tag']).",".$DB->F($_POST['title']).")";
        }
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());

        if(!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Подтип сохранен. ID: ".$_POST['id'] );
    }

    function delete() {
        global $DB;
        if (!($id = $_REQUEST['id'])) $err[] = "Не указан идентификатор записи!";
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($id)) UIError("Выбранная запись связана с заявками. Удаление невозможно.");
        $DB->query("DELETE FROM `list_rtsubtype` WHERE `id`='".$_REQUEST['id']."';");
        redirect($this->getLink(),"Запись успешно удалена.");

    }

    function isBusy($id)
    {
        global $DB;
        $DB->query("SELECT `id` FROM `tickets` WHERE `exttype`=" . $DB->F($id) . ";");
        if ($DB->num_rows()) $err = true; else $err = false;
        $DB->free();
        return $err;
    }

}
?>
