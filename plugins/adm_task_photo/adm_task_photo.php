<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2015
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class adm_task_photo_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getOptions($id = 0)
    {
        global $DB;
        $sql = "SELECT id, title FROM `list_photo_types` WHERE 1;";
        $res[""] = "-- выберите значение --";
        $res = $res + $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());
        return array2options($res, $id);
    }

    static function getById($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `list_photo_types` WHERE `id`=" . $DB->F($id) . ";";
        $res = $DB->getField($sql);
        if ($DB->errno()) UIError($DB->error());
        return $res ? $res : false;
    }

    function viewlist()
    {
        global $DB, $USER;
        $plugList = array("connections"=>"Подключения", "accidents"=>"ТТ","services"=>"СКП", "gp"=>"ГП");
        $clientList = kontr_plugin::getClientList();
        $wtypeList = adm_ticktypes_plugin::getList();
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/adm_task_photo.tmpl.htm");
        else
                $tpl->loadTemplatefile("adm_task_photo.tmpl.htm");
        $sql = "SELECT * FROM `list_photo_types` where active = 1;";
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());
        while ($m = $DB->fetch(true)) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $m["id"]);
            $tpl->setVariable("VLI_TITLE", $m["title"]);
            $tpl->setVariable("VLI_TTYPE", $plugList[$m["task_type"]]);
            $tpl->setVariable("VLI_WTYPE", adm_ticktypes_plugin::getByID($m["wtype"]));
            $tpl->setVariable("VLI_CONTR", kontr_plugin::getByID($m["contr_id"]));
            $tpl->setVariable("VLI_REQUIRED", $m["required"]>0 ? "<strong>Да</strong>": "Нет");
            $tpl->setVariable("VLI_ACTIVE", $m["active"]>0 ? "<strong>Да</strong>": "Нет");
            $tpl->parse("vl_item");
        }
        $DB->free();
        UIHeader($tpl);
        $tpl->show();
    }

    function edit() {
        global $DB, $USER;
        $plugList = array("connections"=>"Подключения", "accidents"=>"ТТ","services"=>"СКП", "gp"=>"ГП");
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
        else
                $tpl->loadTemplatefile("edit.tmpl.htm");
        if ($id = $_REQUEST['id']) {
            $sql = "SELECT * FROM `list_photo_types` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Запись с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);
            $tpl->setVariable("ID", $result["id"]);
            $tpl->setVariable("TITLE", $result["title"]);
            if ($result["required"]>0) {
                $tpl->setVariable("ISREQ", "checked='checked'");
            }
            if ($result["active"]>0) {
                $tpl->setVariable("ACTIVE", "checked='checked'");
            }
        }
        $clientList = kontr_plugin::getClientList($result["contr_id"]);
        $wtypeList = adm_ticktypes_plugin::getList();
        $tpl->setVariable("TASKTYPE_LIST", array2options($plugList, $result["task_type"]));
        $tpl->setVariable("WTYPE_LIST", array2options($wtypeList, $result["wtype"]));
        $tpl->setVariable("CONTR_LIST", $clientList);
        UIHeader($tpl);
        $tpl->show();

    }

    function save() {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = array();

        if(!$_POST['title']) $err[] = "Недостаточно данных. Заполните форму.";

        if(sizeof($err)) UIError($err);
        if ($_POST['id']) {
            $sql = "UPDATE `list_photo_types` SET 
                        `title`=".$DB->F($_POST['title']).", 
                        `task_type`=".$DB->F($_POST['tasktype']).", 
                        `wtype`=".$DB->F($_POST['wtype']).", 
                        `contr_id`=".$DB->F($_POST['contr']).", 
                        `required`=".$DB->F($_POST['req']!= "" ? 1 : 0).",
                        `active`=".$DB->F($_POST['active']!= "" ? 1 : 0)."
                    WHERE `id`=".$DB->F($_POST['id']);
        } else {
            $sql = "INSERT INTO `list_photo_types` (`title`, `task_type`, `wtype`, `contr_id`, `required`, `active`) 
                    VALUES(".$DB->F($_POST['title']).", ".$DB->F($_POST['tasktype']).", ".$DB->F($_POST['wtype']).", 
                    ".$DB->F($_POST['contr']).", ".$DB->F($_POST['req']!= "" ? 1 : 0).", ".$DB->F($_POST['active']!= "" ? 1 : 0).")";
        }
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());

        if(!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Запись сохранена. ID: ".$_POST['id'] );
    }

    function delete() {
        global $DB;
        if (!($id = $_REQUEST['id'])) $err[] = "Не указан идентификатор записи!";
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($id)) UIError("Выбранная запись связана с заявками. Удаление невозможно.");
        $DB->query("UPDATE `list_photo_types` SET `active`=0 WHERE `id`='".$_REQUEST['id']."';");
        redirect($this->getLink(),"Запись успешно удалена.");

    }

    function isBusy($id)
    {
        global $DB;
        $DB->query("SELECT `id` FROM `tmc_ticket` WHERE `reason_id`=" . $DB->F($id) . ";");
        //die("SELECT `task_id` FROM `tickets` WHERE `task_from`=".$DB->F($id).";");
        if ($DB->num_rows()) $err = true; else $err = false;
        $DB->free();
        $err = false;
        return $err;
    }

}
?>
