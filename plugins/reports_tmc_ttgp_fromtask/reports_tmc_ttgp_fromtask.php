<?php

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\tickets\AccidentTicket;


require_once(dirname(__FILE__)."/../accidents/accidents.php");


class reports_tmc_ttgp_fromtask_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }


    public static function getcontrlist_ajax() {
        global $DB, $USER;
        $current = $_REQUEST["cval"];
        if ($_REQUEST["type_id"]) {
            if ($_REQUEST["type_id"] == "accidents") {
                if (kontr_plugin::getOptListAcc($current)) {
                    $ret["tpl"] = kontr_plugin::getOptListAcc($current);
                } else {
                    $ret["tpl"] = "<option value=''>-- список пуст --</option>";
                }
            }
            if ($_REQUEST["type_id"] == "gp") {
                if (kontr_plugin::getOptListGP($current)) {
                    $ret["tpl"] = kontr_plugin::getOptListGP($current);
                } else {
                    $ret["tpl"] = "<option value=''>-- список пуст --</option>";
                }
            }
            if ($_REQUEST["type_id"] == "connections") {
                if (kontr_plugin::getOptListConn($current)) {
                    $ret["tpl"] = kontr_plugin::getOptListConn($current);
                } else {
                    $ret["tpl"] = "<option value=''>-- список пуст --</option>";
                }
            }
            if ($_REQUEST["type_id"] == "services") {
                if (kontr_plugin::getOptListSer($current)) {
                    $ret["tpl"] = kontr_plugin::getOptListSer($current);
                } else {
                    $ret["tpl"] = "<option value=''>-- список пуст --</option>";
                }
            }
            $ret["ok"] = "ok";
        } else {
            $ret["error"] = "Не указан тип заявок.";
        }
        echo json_encode($ret);
        return false;
    }

    function main() {
        global $DB, $USER;

        $sort_ids = array(
            1 => "Номер заявки",
            2 => "СЦ",
            3 => "Контрагент",
        );
        $sort_sql = array(
            1 => "t.id",
            2 => "tick.sc_id",
            3 => "tick.cnt_id",
        );
        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );

        $interval_options = array(
            '1' => "Дата создания",
            '2' => "Дата выполнения",
            '3' => "Дата в графике",
            '4' => "Дата расчета"
        );

        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $typelist = array("accidents"=> "ТТ", "gp"=>"ГП", "connections"=>"Подключения", 'services'=>'СКП');


        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])&& !isset($_POST["createarchiveitem"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_CONTR_LIST", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
            $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
            $tpl->setVariable('FILTER_DATETYPE_OPTIONS', array2options($interval_options, $_REQUEST['filter_datetype']));

            UIHeader($tpl);


            $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            $tpl->setVariable("FILTER_TYPE_OPTIONS", array2options($typelist, $_REQUEST["type_id"]));
            if ($_REQUEST["type_id"] == "accidents") {
                $tpl->setVariable("FILTER_CNT_OPTIONS", kontr_plugin::getOptListAcc($_REQUEST["cnt_id"]));
            }
            if ($_REQUEST["type_id"] == "gp") {
                $tpl->setVariable("FILTER_CNT_OPTIONS", kontr_plugin::getOptListGP($_REQUEST["cnt_id"]));
            }
            if ($_REQUEST["type_id"] == "services") {
                $tpl->setVariable("FILTER_CNT_OPTIONS", kontr_plugin::getOptListSer($_REQUEST["cnt_id"]));
            }
            if ($_REQUEST["type_id"] == "connections") {
                $tpl->setVariable("FILTER_CNT_OPTIONS", kontr_plugin::getOptListConn($_REQUEST["cnt_id"]));
            }
            $tpl->setVariable("FILTER_SC_OPTIONS", "<option value=0>-- все --</option>".adm_sc_plugin::getScList(@$_POST["sc_id"]));

        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"] || @$_POST["setflag"]|| @$_POST["createarchiveitem"]) {
                $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
                if ($USER->getTemplate() != "default")
                    $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
                else
                    $rtpl->loadTemplatefile("report.tmpl.htm");
                //$rtpl->loadTemplatefile("report.tmpl.htm");
                if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                    $rtpl->setCurrentBlock("print_head");
                    $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                    $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                    $rtpl->parse("print_head");
                }
                $rtpl->setVariable("TASK_TYPE", $typelist[$_REQUEST['type_id']]);
                $rtpl->setVariable("MONTH", $month[$_POST['month']]);
                $rtpl->setVariable("YEAR", $year[$_POST['year']]);
                /*

                report generator body here

                */
                if ($_REQUEST["sc_id"]) {

                    $filter[] = "tick.sc_id=".$DB->F($_REQUEST["sc_id"]);
                }
                //$filter[] = "=".$_REQUEST["year"]."-".$_REQUEST["month"];
                $filter[] = "tick.cnt_id=".$_REQUEST["cnt_id"];
                $filter[] = "tmc_ticket.isreturn>0";
                $filter = implode(" AND ", $filter);
                if ($filter) $filter = " AND ".$filter;
                if ($_REQUEST["type_id"] == "accidents") {
                    $task_table  = "tickets";
                    $selectAdditional = ', tick.clntopagr as pin, if(tick.clnttnum is null or tick.clnttnum = "", (SELECT `contr_ticket_id` FROM `contr_tickets` WHERE `task_id`=t.id) , tick.clnttnum) as lsnum';
                } else {
                    $task_table = "gp";
                    $selectAdditional = ', null as pin, tick.operator_key as lsnum';
                }
                $order = "ORDER BY ". $sort_sql[$_REQUEST['filter_sort_id']]." ".$_REQUEST['filter_sort_order'];

                $datetype_filter = "";
                $fd = $_POST["year"]."-".$_POST["month"]."-01";
                $d = new DateTime( $fd );
                $td = $d->format( 'Y-m-t' )." 23:59:59";

                if ($_REQUEST['filter_datetype'] == 1) {
                    #$datetype_filter = " AND DATE_FORMAT(t.date_reg, '%Y-%m')=".$DB->F($_POST["year"]."-".$_POST["month"]);
                    $datetype_filter = " AND t.date_reg between '".$fd."' and '".$td."'";
                }
                if ($_REQUEST['filter_datetype'] == 2) {
/*
                    $datetype_filter = " AND (select
DATE_FORMAT(ifnull(min(datetime),'2000-01-01'),'%Y-%m')
  from task_comments tcs where tcs.task_id = t.id
   and tag like '%<b>Выполнена</b>%' ) = ".$DB->F($_POST["year"]."-".$_POST["month"]);
*/
                    $datetype_filter = " AND (select min(datetime) from task_comments tcs where tcs.task_id = t.id and tag like '%<b>Выполнена</b>%') between '".$fd."' and '".$td."'";
                }
                if ($_REQUEST['filter_datetype'] == 3) {
                    $datetype_filter = "and exists (select * from gfx g where g.task_id = t.id and c_date between '".$fd."' and '".$td."')";
                }
                if ($_REQUEST['filter_datetype'] == 4) {
                    $datetype_filter = " AND 1 = 0 ";
                    if ($_REQUEST["type_id"] == "gp") {
                        $datetype_filter = " AND exists (select * from gp_calcs gpc where gpc.task_id = t.id and gpc.calc_date between '".$fd."' and '".$td."') ";
                    }
                    if ($_REQUEST["type_id"] == "connections") {
                        $datetype_filter = " AND exists (select * from list_tech_calc gpc where gpc.task_id = t.id and gpc.calc_date between '".$fd."' and '".$td."') ";
                    }
                    if ($_REQUEST["type_id"] == "services") {
                        $datetype_filter = " AND exists (select * from list_skp_tech_calc gpc where gpc.task_id = t.id and gpc.calc_date between '".$fd."' and '".$td."') ";
                    }
                    if ($_REQUEST["type_id"] == "accidents") {
                        $datetype_filter = " AND exists (select * from list_tt_tech_calc gpc where gpc.task_id = t.id and gpc.calc_date between '".$fd."' and '".$td."') ";
                    }
                }


                $sql = "SELECT tmc_ticket.*, 
                        (SELECT `fio` FROM `users` WHERE `id`=tmc_ticket.tech_id) as user_fio, 
                        tsc.tmc_id, 
                        (SELECT `title` FROM `list_materials` WHERE `id`=tsc.tmc_id) as tmc_name, 
                        tsc.serial, 
                        tsc.count as quant,
                        list_sc.title as sc_title
                        {$selectAdditional}
                        FROM `tmc_ticket` AS tmc_ticket 
                            LEFT JOIN `tmc_sc` AS tsc ON (tsc.tmc_ticket_id=tmc_ticket.id AND tsc.incoming) 
                            LEFT JOIN `tasks` AS t ON (t.id=tmc_ticket.task_id AND t.plugin_uid=".$DB->F($_REQUEST["type_id"]).") 
                            LEFT JOIN `$task_table` AS tick ON tick.task_id=t.id 
                            LEFT JOIN list_sc ON tick.sc_id = list_sc.id
                        WHERE 
                            tsc.tmc_id 
                            ".$datetype_filter."\n".$filter."\n".$order;
            ### EF: работать с датой лучше по диапазону, а не преобразовывать ее в непонятный период
            ###"AND DATE_FORMAT(t.date_reg, '%Y-%m')=".$DB->F($_POST["year"]."-".$_POST["month"])." $filter $order;";

                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error()." ".$sql);
                if ($DB->num_rows()) {
                    $i = 0;
                    while ($r = $DB->fetch(true)) {
                        $i+=1;
                        $rtpl->setCurrentBlock("rep_row");
                        $rtpl->setVariable("RR_ID", $i);
                        $rtpl->setVariable("RR_TASK_ID", $r["task_id"]);
                        $rtpl->setVariable("RR_TASK_LSNUM", htmlspecialchars($r["lsnum"]));
                        $rtpl->setVariable("RR_PIN", htmlspecialchars($r["pin"]));
                        $rtpl->setVariable("RR_SC", htmlspecialchars($r["sc_title"]));
                        $rtpl->setVariable("RR_HREF", link2($_REQUEST["type_id"]."/viewticket?task_id=".$r["task_id"], false));
                        $rtpl->setVariable("RR_TMC_TITLE", $r["tmc_name"]);
                        $rtpl->setVariable("RR_TMC_SERIAL", $r["serial"]);
                        $reason = $DB->getField("SELECT `title` FROM `list_gp_reason` WHERE `id`=".$DB->F($r["reason_id"]));
                        $cmm = $DB->getField("SELECT `title` FROM `list_gp_comment` WHERE `id`=".$DB->F($r["comment_id"]));

                        $rtpl->setVariable("RR_REASON", $reason ? $reason : ($r["desc"] ? $r["desc"] : "<center>&mdash;</center>"));
                        $rtpl->setVariable("RR_COMMENT", $cmm ? $cmm : ($r["cmm"] ? $r["cmm"] : "<center>&mdash;</center>"));
                        $rtpl->setVariable("RR_QUANT", $r["count"]>1 ? $r["count"] : $r["quant"]);
                        if ($_REQUEST["type_id"] != "gp") {
                            $t = new AccidentTicket($r["task_id"]);
                            if ($t) {
                                $addr = $t->getAddr($t->getDomId());
                                $area = $t->getArea();
                                #$rtpl->setVariable("RR_ADDRESS", $addr["city"]." ".$addr["street"]." ".$addr["house"].", ".$t->getKv());
                                $rtpl->setVariable("RR_CITY", $addr["city"]);
                                $rtpl->setVariable("RR_AREA", $area);
                                $rtpl->setVariable("RR_STREET", $addr["street"]);
                                $rtpl->setVariable("RR_HOUSE", $addr["house"]);
                                $rtpl->setVariable("RR_KV", $t->getKv());
                            } else {
                                #$rtpl->setVariable("RR_ADDRESS", "<span class=\"text-danger\">Адрес не определен</span>");
                                $rtpl->setVariable("RR_CITY", "&nbsp;");
                                $rtpl->setVariable("RR_AREA", "&nbsp;");
                                $rtpl->setVariable("RR_STREET", "&nbsp;");
                                $rtpl->setVariable("RR_HOUSE", "&nbsp;");
                                $rtpl->setVariable("RR_KV", "&nbsp;");
                            }
                        } else {
                            $sql = "SELECT `dom_id` FROM `gp_addr` WHERE `task_id`=".$DB->F($r["task_id"])." ORDER BY `id` DESC LIMIT 1;";
                            $fdom_id = $DB->getField($sql);
                            if ($DB->errno()) UIError($DB->error());
                            $laddr = $DB->getRow("SELECT * FROM `list_addr` WHERE `id`=".$DB->F($fdom_id).";", true);
                            if (count($laddr)) {
                                $addr = addr_interface_plugin::decodeFullKLADRPartial($laddr['kladrcode']);
                                if (count($addr)) {
                                    $area_id = $laddr[7];
                                    $area = $area_id ? $DB->getField("select max(title) from list_area where id = ".$area_id) : '';
                                    $addr["house"] = $laddr['name'] ? $laddr['name'] : $laddr['althouse'];
                                    #$rtpl->setVariable("RR_ADDRESS", $addr["city"]." ".$addr["street"]." ".$addr["house"]);
                                    $rtpl->setVariable("RR_CITY", $addr["city"]);
                                    $rtpl->setVariable("RR_AREA", $area);
                                    $rtpl->setVariable("RR_STREET", $addr["street"]);
                                    $rtpl->setVariable("RR_HOUSE", $addr["house"]);
                                    $rtpl->setVariable("RR_KV", "&nbsp;");
                                } else {
                                    #$rtpl->setVariable("RR_ADDRESS", "<span class=\"text-danger\">Адрес не определен</span>");
                                    $rtpl->setVariable("RR_CITY", "&nbsp;");
                                    $rtpl->setVariable("RR_AREA", "&nbsp;");
                                    $rtpl->setVariable("RR_STREET", "&nbsp;");
                                    $rtpl->setVariable("RR_HOUSE", "&nbsp;");
                                    $rtpl->setVariable("RR_KV", "&nbsp;");
                                }
                            }
                        }

                        $rtpl->setVariable("RR_USER", $r["user_fio"]);



                        $rtpl->parse("rep_row");
                    }
                } else {
                    $rtpl->touchBlock("emptyreport");
                }
                $DB->free();

                //$sql = "SELECT * FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON tick.task_id=t.id WHERE t.plugin_uid=".$DB->F($_REQUEST["type_id"]).";";


                // -------------------------------------

                $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
                $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
                if (isset($_POST["createxlsversion"])) {
                    header("Content-type: application/vnd.ms-excel");
                    header("Content-disposition: attachment; filename=report_contr_".$_POST['datefrom']."-".$_POST['dateto'].".xls");
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {

                        if (isset($_POST['createprintversion'])) {
                            $rtpl->touchBlock("print_footer");
                            $rtpl->show();

                            return;
                        } else {
                            $tpl->setCurrentBlock("reportval");
                            $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                            $tpl->parse("reportval");
                        }

                }
            } else {
                $tpl->touchBlock("notcreated");
                $tpl->setVariable("DISABLED", "disabled='disabled'");
            }


        $tpl->show();

    }


}
?>
