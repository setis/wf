<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\User;
use models\ListSc;
use models\User as OrmUser;
use Symfony\Component\HttpFoundation\Request;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
class adm_sc_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    static function getSCParams($id)
    {
        #global $DB;
        #$sql = "SELECT * FROM `list_sc` WHERE `id`=" . $DB->F($id) . ";";
        #return $DB->getRow($sql, true);
        return self::getSC($id);
    }

    static function getSC($id)
    {
        global $DB, $USER;
        $sql = "SELECT * FROM `list_sc` WHERE `id`=" . $DB->F($id) . " AND exists (SELECT *
  FROM user_sc_access usa
 WHERE 
    " . ($USER ? " usa.user_id = " . $USER->getId() : " 1 ") . "
   AND usa.sc_id = list_sc.id) ORDER BY `title`;";
        return $DB->getRow($sql, true);

    }

    static function getSCList2Col()
    {
        global $DB, $USER;
        $sql = "SELECT `id`, `title` FROM `list_sc` WHERE exists (SELECT *
  FROM user_sc_access usa
 WHERE 
    " . ($USER ? " usa.user_id = " . $USER->getId() : " 1 ") . "
   AND usa.sc_id = list_sc.id) ORDER BY `title`;";
        return $DB->getCell2($sql);
    }

    static function getSCIList()
    {
        global $DB, $USER;
        $sql = "SELECT `id`, `title` FROM `list_sc` WHERE `title`!='СКП' AND `title`!='СМР (Росликов)' AND `title`!='СЦ по подключению ЮЛ' AND `title`!='СЦ Тверь' AND exists (SELECT *
  FROM user_sc_access usa
 WHERE 
 " . ($USER ? " usa.user_id = " . $USER->getId() : " 1 ") . "
   AND usa.sc_id = list_sc.id) ORDER BY `title`;";
        return $DB->getCell2($sql);
    }

    static function getScList($sel = false, $first = false)
    {
        global $DB, $USER;
        if ($first) {
            return $DB->getField("SELECT `id` FROM `list_sc` WHERE exists (SELECT *
  FROM user_sc_access usa
 WHERE 
 " . ($USER ? " usa.user_id = " . $USER->getId() : " 1 ") . "
   AND usa.sc_id = list_sc.id) ORDER BY `title` LIMIT 1;");
        } else {
            return array2options($DB->getCell2("SELECT `id`,`title` FROM `list_sc` WHERE exists (SELECT *
  FROM user_sc_access usa
 WHERE 
 " . ($USER ? " usa.user_id = " . $USER->getId() : " 1 ") . "
   AND usa.sc_id = list_sc.id) ORDER BY `title`;"), $sel);
        }
    }

    static function getScListByAccess($sel = false, $first = false)
    {
        global $DB, $USER;
        $sql_filter = '';
        if (!empty($USER)) {
            if ($USER && User::isChiefTech($USER->getId())) {
                if (!$USER->checkAccess("fullaccesstoallsc") && !$USER->checkAccess("scaccfullaccess") && !$USER->checkAccess("scfullaccess") && !$USER->checkAccess("scgpfullaccess")) {
                    // $sql_filter = "WHERE `id`=" . $DB->F($sc_id);
                }
                $ret = "<option value='0'> -- все </option>";
            }
            if (!User::isTech($USER->getId())) {
                $ret = "<option value='0'> -- все </option>";
            }
        }

        //EF: новая система доступов на СЦ
        if (strlen($sql_filter) > 0) {
            $sql_filter .= " and exists (select *
  from user_sc_access usa
 where " . ($USER ? " usa.user_id = " . $USER->getId() : " 1 ") . "
   and usa.sc_id = list_sc.id)";
        } else {
            $sql_filter .= " where exists (select *
  from user_sc_access usa
 where " . ($USER ? " usa.user_id = " . $USER->getId() : " 1 ") . "
   and usa.sc_id = list_sc.id)";
        }
        //echo "<pre>".$sql_filter."</pre>";
        if ($first)
            return $DB->getField("SELECT `id` FROM `list_sc` " . $sql_filter . " ORDER BY `title` LIMIT 1;");
        else
            return $ret . array2options($DB->getCell2("SELECT `id`,`title` FROM `list_sc` $sql_filter ORDER BY `title`;"), $sel);
    }

    /**
     * @param int $sc_id
     * @param null|integer[] $roles
     * @return array
     */
    public static function getScUsers($sc_id, $roles = null)
    {
        global $DB;

        if (is_array($roles)) {
            $roles = implode(', ', array_map('intval', $roles));
        } else {
            $roles = intval($roles);
        }

        if (empty($roles))
            $roles = '0';

        $sql = 'SELECT
                    sc.id AS sc_id, u.*
                FROM
                    users AS u
                        LEFT JOIN
                    link_sc_user AS su ON (su.user_id = u.id)
                        LEFT JOIN
                    list_sc AS sc ON (sc.id = su.sc_id)
                WHERE
                    (u.pos_id IN (' . $roles . ') OR 1 = ' . (int)!$roles . ') AND u.active AND u.otdel_id = ' . $DB->F($sc_id);

        $users = $DB->getAll($sql);

        return $users;
    }

    function getSelList($sel = false)
    {
        global $DB, $USER;
        $sql = "SELECT * FROM list_sc
 WHERE exists (SELECT *
  FROM user_sc_access usa
 WHERE 
 " . ($USER ? " usa.user_id = " . $USER->getId() : " 1 ") . "
   AND usa.sc_id = list_sc.id) ORDER BY `title`;";
        $DB->query($sql);
        if ($DB->errno() || !$DB->num_rows()) return "<strong style='color:#FF0000';>СЦ отсутствуют!</strong> <a class='link' href='/adm_sc/edit'>Создать СЦ</a>";
        $ret = "<select name='sc'><option value='none'>-- выберите СЦ --</option>";
        while (list($id, $title, $desc) = $DB->fetch()) {
            if ($id == $sel)
                $ret .= "<option selected value='$id'>$title</option>";
            else
                $ret .= "<option value='$id'>$title</option>";
        }
        return $ret . "</select>";
    }

    function getFilterList($sel = false, $class = "")
    {
        global $DB, $USER;
        $sql = "SELECT * FROM `list_sc` WHERE `id` IN (SELECT DISTINCT `sc_id` FROM `link_sc_user`) AND exists (SELECT *
  FROM user_sc_access usa
 WHERE 
 " . ($USER ? " usa.user_id = " . $USER->getId() : " 1 ") . "
   AND usa.sc_id = list_sc.id) ORDER BY `title`;";
        $DB->query($sql);
        if ($DB->errno() || !$DB->num_rows()) return false;
        $ret = "<select class='$class' name='sc'><option value='0'>-- все --</option>";
        while (list($id, $title, $desc) = $DB->fetch()) {
            if ($id == $sel)
                $ret .= "<option selected value='$id'>$title</option>";
            else
                $ret .= "<option value='$id'>$title</option>";
        }
        return $ret . "</select>";
    }

    function viewlist()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/adm_sc.tmpl.htm");
        else
            $tpl->loadTemplatefile("adm_sc.tmpl.htm");
        $sql = "SELECT * FROM `list_sc` WHERE exists (SELECT *
  FROM user_sc_access usa
 WHERE usa.user_id = " . $USER->getId() . "
   AND usa.sc_id = list_sc.id) ORDER BY `title`;";
        $DB->query($sql);
        if ($DB->errno()) {
            UIError($DB->error());
        }
        while (list($id, $title, $desc, $chief, $mol) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->setVariable("VLI_DESC", $desc);
            //$tpl->setVariable("VLI_CHIEF", $chief ? $DB->getField("SELECT `fio` FROM `users` WHERE `id`=".$DB->F($chief).";") : "<center>&mdash;</center>");
            if (class_exists("adm_empl_plugin", true) && method_exists("adm_empl_plugin", "getEmplListBySCID")) {
                $empl = new adm_empl_plugin();
                $tpl->setVariable("VLI_EMPL", $empl->getEmplListBySCID($id));
            } else UIError("Не найден модуль Техники!");

            if (class_exists("adm_empl_plugin", true) && method_exists("adm_empl_plugin", "getChiefListBySCID")) {
                $empl = new adm_empl_plugin();
                $tpl->setVariable("VLI_CHIEF", $empl->getChiefListBySCID($id));
            } else UIError("Не найден модуль Техники!");

            if (class_exists("adm_areas_plugin", true) && method_exists("adm_areas_plugin", "getAreaListBySCID")) {
                $area = new adm_areas_plugin();
                $areaList = $area->getAreaListBySCID($id);
                $tpl->setVariable("VLI_AREAS", $areaList ? $areaList : " - ");
            } else UIError("Не найден модуль Районы!");

            if (!empty($mol)) {
                $mol_name = $DB->getField('SELECT fio FROM users WHERE id = ' . $DB->F($mol));
                $tpl->setVariable("MOL_NAME", $mol_name ? $mol_name : " - ");
            }
            $tpl->parse("vl_item");
        }
        $DB->free();
        UIHeader($tpl);
        $tpl->show();


    }

    function edit(Request $request)
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($id = $_REQUEST['id']) {
            /** @var ListSc $model */
            $model = $this->getEm()->getReference(ListSc::class, $id);

            $sql = "SELECT * FROM `list_sc` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->errno())
                UIError($DB->error());
            if (!$DB->num_rows())
                UIError("Сервисный центр идентификатором отсутствует.");
            $result = $DB->fetch(true);
            $tpl->setVariable("ID", $result["id"]);
            $tpl->setVariable("TITLE", $result["title"]);
            $tpl->setVariable("DESC", $result["desc"]);
            $tpl->setCurrentBlock("fulledit");
            $DB->query("SELECT `id`,`title` FROM `list_area` WHERE `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($id) . ") ORDER BY `title`;");
            if ($DB->errno())
                UIError($DB->error());
            $selAreasIDS = false;
            $selAreasTitles = false;
            while (list($areaID, $areaTitle) = $DB->fetch()) {
                $selAreasIDS[] = $areaID;
                $selAreasTitles[] = preg_replace("/\s{2,}/", "", $areaTitle);
            }

            $DB->free();
            $areaIDList = implode(",", (array)$selAreasIDS);
            $areaTitlesList = implode(", ", (array)$selAreasTitles);

            $tpl->setVariable("AREA_IDS", $areaIDList ? $areaIDList : "");
            $tpl->setVariable("AREA_VALS", $areaTitlesList ? $areaTitlesList : "");

            $DB->query("SELECT `id`,`fio` FROM `users` WHERE `id` IN (SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($id) . ") ORDER BY `fio`;");
            if ($DB->errno()) UIError($DB->error());
            $selUserIDS = false;
            $selUserTitles = false;
            while (list($userID, $userFio) = $DB->fetch()) {
                $selUserIDS[] = $userID;
                $selUserTitles[] = preg_replace("/\s{2,}/", "", $userFio);
            }
            $DB->free();
            $userIDList = implode(",", (array)$selUserIDS);
            $userFIOList = implode(", ", (array)$selUserTitles);
            $tpl->setVariable("EMPL_IDS", $userIDList ? $userIDList : "");
            $tpl->setVariable("EMPL_VALS", $userFIOList ? $userFIOList : "");

            $DB->query("SELECT `id`,`fio` FROM `users` WHERE `id` IN (SELECT `user_id` FROM `link_sc_chiefs` WHERE `sc_id`=" . $DB->F($id) . ") ORDER BY `fio`;");
            if ($DB->errno()) UIError($DB->error());
            $selUserIDS = false;
            $selUserTitles = false;
            while (list($userID, $userFio) = $DB->fetch()) {
                $selUserIDS[] = $userID;
                $selUserTitles[] = preg_replace("/\s{2,}/", "", $userFio);
            }
            $DB->free();
            $userIDList = implode(",", (array)$selUserIDS);
            $userFIOList = implode(", ", (array)$selUserTitles);
            $tpl->setVariable("CHIEF_IDS", $userIDList ? $userIDList : "");
            $tpl->setVariable("CHIEF_VALS", $userFIOList ? $userFIOList : "");

            $tpl->setVariable("MOLS_IDS", implode(',', array_map(function (OrmUser $user) {
                return $user->getId();
            }, $model->getMols()->toArray())));

            $tpl->setVariable("MOLS_VALS", implode('<br />', array_map(function (OrmUser $user) {
                return $user->getFio();
            }, $model->getMols()->toArray())));

            $tpl->setVariable("COORDINATORS_IDS", implode(',', array_map(function (OrmUser $user) {
                return $user->getId();
            }, $model->getCoordinators()->toArray())));
            $tpl->setVariable("COORDINATORS_VALS", implode('<br />', array_map(function (OrmUser $user) {
                return $user->getFio();
            }, $model->getCoordinators()->toArray())));

            /** @var ListSc $serviceCenter */
            $serviceCenter = $this->getEm()->find(ListSc::class, $id);
            $mols = $serviceCenter->getMols();
            /** @var \models\User[] $users */
            $users = $this->getEm()->getRepository(\models\User::class)->findBy(['active' => true]);

            foreach ($users as $user) {
                $tpl->setCurrentBlock('mols_list');
                $tpl->setVariable("MOL_ID", $user->getId());
                $tpl->setVariable("MOL_NAME", $user->getFio());
                if ($mols->contains($user)) {
                    $tpl->setVariable("MOL_SELECTED", 'selected="selected"');
                }
                $tpl->parseCurrentBlock();
            }

            $tpl->parse("fulledit");
        } else {
            $tpl->touchBlock("createhead");
            $tpl->touchBlock("createfooter");

        }

        UIHeader($tpl);
        $tpl->show();

    }

    function save(Request $request)
    {
        global $DB, $USER;

        $err = array();

        if (!$_POST['title']) $err[] = "Не заполнено Название СЦ";

        if (sizeof($err)) UIError($err);

        if ($_POST['id'])
            $sql = "UPDATE `list_sc` SET
                `chief`=" . $DB->F($_POST['sc_chief'] ? $_POST["sc_chief"] : 0) . ",
                `title`=" . $DB->F($_POST['title']) . ",
                `desc`=" . $DB->F($_POST['desc']) . "
            WHERE
                `id`=" . $DB->F($_POST['id']) . "
                ;";
        else
            $sql = "INSERT INTO `list_sc`
                (
                `title`,
                `desc`
                )
                VALUES
                (
                " . $DB->F($_POST['title']) . ",
                " . $DB->F($_POST['desc']) . "
                )";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error() . "<br />" . $sql);
        if (!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }

        if (isset($_POST["extedit"])) {
            $DB->query("DELETE FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($_POST["id"]) . ";");
            if ($DB->errno()) UIError($DB->error());
            $empllist = explode(",", $_POST["empls_id"]);
            $sql_add = false;


            if (count($empllist) && !empty($empllist[0])) {
                foreach ($empllist as $item) {
                    if (!empty($item))
                        $sql_add[] = "(" . $DB->F($item) . ", " . $DB->F($_POST["id"]) . ")";
                }

                if ($sql_add) {
                    $query = "INSERT INTO `link_sc_user` (`user_id`, `sc_id`) VALUES " . implode(",", $sql_add) . ";";
                    $DB->query($query);
                    if ($DB->errno())
                        UIError($DB->error() . "INSERT INTO `link_sc_user` (`user_id`, `sc_id`) VALUES " . implode(",", $sql_add) . ";");
                }
            }
            $DB->query("DELETE FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($_POST["id"]) . ";");
            if ($DB->errno()) UIError($DB->error());

            $arealist = explode(",", $_POST["areas_id"]);
            $sql_add = false;
            if (count($arealist) && !empty($arealist[0])) {
                foreach ($arealist as $item) {
                    $sql_add[] = "(" . $DB->F($_POST["id"]) . ", " . $DB->F($item) . ")";
                }
                if ($sql_add) {
                    $DB->query("INSERT INTO `link_sc_area` (`sc_id`, `area_id`) VALUES " . implode(",", $sql_add) . ";");
                    if ($DB->errno()) UIError($DB->error());
                }
            }

            #$sql_new_access = "begin\n";
            $old_chiefs = $DB->getCell("SELECT user_id FROM link_sc_chiefs WHERE sc_id = " . $DB->F($_POST["id"]));
            $empllist = explode(",", $_POST["chief_ids"]);
            $revoke_access_arr = array_diff($old_chiefs, $empllist);
            $delete_ids = join(",", $revoke_access_arr);

            /// error_log("\n revoke_access = \n".$delete_ids."\n\n",3,"/ramdisk/sql.log");
            $DB->query("DELETE FROM `link_sc_chiefs` WHERE `sc_id`=" . $DB->F($_POST["id"]) . ";");
            if (strlen($delete_ids) > 0) {
                $DB->query("DELETE FROM user_sc_access WHERE `sc_id`=" . $DB->F($_POST["id"]) . " AND user_id IN (" . $delete_ids . ");");
            }
            if ($DB->errno()) UIError($DB->error());
            $sql_add = false;

            if (count($empllist) && !empty($empllist[0])) {
                foreach ($empllist as $item) {
                    $sql_add[] = "(" . $DB->F($item) . ", " . $DB->F($_POST["id"]) . ")";
                    #$sql_new_access .= "delete from user_sc_access where sc_id = ".$DB->F($_POST["id"])." and user_id = ".$DB->F($item)."\n";
                }

                if ($sql_add) {
                    $DB->query("INSERT INTO `link_sc_chiefs` (`user_id`, `sc_id`) VALUES " . implode(",", $sql_add) . ";");
                    if ($DB->errno()) UIError($DB->error() . "INSERT INTO `link_sc_chiefs` (`user_id`, `sc_id`) VALUES " . implode(",", $sql_add) . ";");
                }
            }
            $sql = "INSERT INTO user_sc_access (sc_id,user_id) "
                . "SELECT sc_id, user_id FROM link_sc_chiefs lsc "
                . "WHERE lsc.sc_id = " . $DB->F($_POST["id"]) . " "
                . "AND NOT exists "
                . "(SELECT * FROM user_sc_access usa "
                . "WHERE usa.user_id = lsc.user_id "
                . "AND usa.sc_id = lsc.sc_id)";
            $DB->query($sql);
        }

        if (intval($_POST['id']) > 0) {
            $id = intval($_POST['id']);
            /** @var ListSc $model */
            $model = $this->getEm()->find(ListSc::class, $id);

            if ($request->request->has('coordinators_ids')) {
                $coordinators = [];
                $coordinators_ids = array_filter(array_map('trim', explode(',', $request->get('coordinators_ids'))));

                foreach ($coordinators_ids as $coordinator_id) {
                    $coordinators[$coordinator_id] = $this->getEm()->find(OrmUser::class, $coordinator_id);
                }
                $model->setCoordinators($coordinators);
            }

            if ($request->request->has('mols_ids')) {
                $mols = [];
                $mols_ids = array_filter(array_map('trim', explode(',', $request->get('mols_ids'))));
                foreach ($mols_ids as $mol_id) {
                    $mols[$mol_id] = $this->getEm()->find(OrmUser::class, $mol_id);
                }

                $model->setMols($mols);
            }

            $this->getEm()->persist($model);
            $this->getEm()->flush();
        }

        redirect($this->getLink('viewlist'), "СЦ сохранен. ID: " . $_POST['id']);
    }

    function delete()
    {
        global $DB;
        if (!($id = $_REQUEST['id'])) $err[] = "Не указан СЦ";
        if (sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST["id"])) UIError("Выбранный СЦ связан с Районами / Техниками. Удаление невозможно.");
        $DB->query("DELETE FROM `list_sc` WHERE `id`='" . $_REQUEST['id'] . "';");
        redirect($this->getLink(), "СЦ успешно удален.");
    }

    function isBusy($id)
    {
        global $DB;

        if ($DB->getField("SELECT COUNT(*) FROM `link_sc_area` WHERE `sc_id`=" . $DB->F($id) . ";")) return true;
        if ($DB->getField("SELECT COUNT(*) FROM `link_sc_user` WHERE `sc_id`=" . $DB->F($id) . ";")) return true;

        return false;
    }

    function getSCByAreaIDA()
    {
        global $DB;
        $area_id = isset($_POST["area_id"]) ? $_POST["area_id"] : false;
        if (!$area_id) {
            echo "error";
            return;
        } else {
            $res = $DB->getField("SELECT `sc_id` FROM `link_sc_area` WHERE `area_id`=" . $DB->F($area_id) . ";");
            if ($res) {
                echo $res;
            } else {
                echo "error";
            }
            return;
        }
    }
}
