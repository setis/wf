<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2011
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Управление Сервисными Центрами";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'adm_interface';

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник Сервисных Центров";
    $PLUGINS[$plugin_uid]['events']['getSCByAreaIDA'] = "Получение идентификатора СЦ по Району";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование Сервисного Центра";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление  Сервисного Центра";   
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение Сервисного Центра";  

    
    
}
?>