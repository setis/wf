<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\User;
use Doctrine\ORM\EntityManagerInterface;
use models\Agent;
use models\Agreement;
use models\ListContr;
use models\TaskType;
use models\User as OrmUser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
class kontr_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function format_date($in)
    {
        $out = $in;
        $matches = array();
        if (preg_match('/(\d{4})\-(\d\d)\-(\d\d)/', $in, $matches)) {
            $out = $matches[3] . "." . $matches[2] . "." . $matches[1];
        }
        return $out;
    }

    function rutextdate2date($in)
    {
        $out = '';
        $matches = array();
        if (preg_match('/(\d\d)(\.|\/)(\d\d)(\.|\/)(\d{4})/', $in, $matches)) {
            $out = $matches[5] . "-" . $matches[3] . "-" . $matches[1];
        }
        return $out;
    }

    function isBusy($id)
    {
        return false;
    }

    function getOptListConn($sel)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `contr_type`='1' ORDER BY `contr_title`;"), $sel);
    }

    static public function getOptListConnections($sel)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `contr_type`='1' ORDER BY `contr_title`;"), $sel);
    }

    static function getOptListSer($sel)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `contr_type2`='1' ORDER BY `contr_title`;"), $sel);
    }

    static function getOptList($sel)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE 1 ORDER BY `contr_title`;"), $sel);
    }

    static function getOptListTasks($sel)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `contr_type`='1' OR `contr_type2`='1' OR `contr_type3`='1' OR `contr_type4`='1' ORDER BY `contr_title`;"), $sel);
    }

    static function getOptListForAgr($sel)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT lc.id, REPLACE(lc.contr_title, '\"', '') AS contr_title  FROM `list_contr` AS lc WHERE 1 ORDER BY lc.contr_title;"), $sel);
    }

    static function getOptListAcc($sel)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `contr_type3`='1' ORDER BY `contr_title`;"), $sel);
    }


    static function getListConnRep($sql)
    {
        global $DB;
        $query = "SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE (`contr_type`='1' OR `contr_type3`='1') AND `id` IN (SELECT `cnt_id` FROM `tickets` WHERE `task_id` IN (" . $sql . ")) GROUP BY `id` ORDER BY `contr_title`;";
        return $DB->getCell2($query);
    }

    static function getListConn()
    {
        global $DB;
        return $DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `contr_type`='1' ORDER BY `contr_title`;");
    }


    static function getListSer()
    {
        global $DB;
        return $DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `contr_type2`='1' ORDER BY `contr_title`;");
    }

    static function getListSerNal()
    {
        global $DB;
        return $DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `contr_type2`='1' AND `id` IN (SELECT `contr_id` FROM `list_contr_agr` WHERE `paytype`=1) GROUP BY `id` ORDER BY `contr_title`;");
    }


    static function getListAcc()
    {
        global $DB;
        return $DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `contr_type3`='1' ORDER BY `contr_title`;");
    }

    static function getListGP()
    {
        global $DB;
        return $DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `contr_type4`='1' ORDER BY `contr_title`;");
    }

    static function getOptListGP($sel = 0)
    {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`, REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `contr_type4`='1' ORDER BY `contr_title`;"), $sel);
    }

    static function getByID($id)
    {
        global $DB;
        return $DB->getField("SELECT REPLACE(`contr_title`, '\"', '') AS contr_title FROM `list_contr` WHERE `id`=" . $DB->F($id) . ";");
    }

    function getContrByID($id)
    {
        global $DB;
        return $DB->getRow("SELECT * FROM `list_contr` WHERE `id`=" . $DB->F($id) . ";", true);
    }

    function viewlist()
    {

        global $DB, $USER;
        $sort_ids = array(
            1 => "ИД",
            2 => "Название контрагента",
            3 => "Официальное наименование"
        );
        $sort_sql = array(
            1 => "`id`",
            2 => "`contr_title`",
            3 => "`official_title`"
        );

        $sort_bdtype_list = array(
            1 => "Подключение",
            2 => "СКП",
            3 => "Абон. Аварии (ТТ)",
            4 => "Лин. Аварии (ГП)"

        );


        $sort_type_list = array(
            1 => "Поставщик",
            2 => "Заказчик"
        );

        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );

        $rpp = array(
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250"
        );
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }
        if (!isset($_REQUEST["filter_rpp"])) $_REQUEST["filter_rpp"] = $USER->getRpp();
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        if ($_REQUEST["filter_rpp"] != $USER->getRpp()) {
            $sql = "UPDATE `users` SET `rpp`=" . $_REQUEST["filter_rpp"] . " WHERE `id`=" . $DB->F($USER->getId()) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
        } else {
            $_REQUEST["filter_rpp"] = $USER->getRpp();
        }

        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ? $_REQUEST['filter_sort_order'] : key($sort_order));
        $_REQUEST["filter_title"] = $this->getCookie('filter_title', $_REQUEST['filter_title']);
        $_REQUEST["filter_proptype_id"] = $this->getCookie('filter_proptype_id', $_REQUEST['filter_proptype_id']);
        $_REQUEST["filter_type_id"] = $this->getCookie('filter_type_id', $_REQUEST['filter_type_id']);
        $_REQUEST["filter_bd_id"] = $this->getCookie('filter_bd_id', $_REQUEST['filter_bd_id']);
        $_REQUEST["filter_id"] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST["filter_inn"] = $this->getCookie('filter_inn', $_REQUEST['filter_inn']);

        $tpl->setVariable("FILTER_PROPTYPE_OPTIONS", kontr_proptypes_plugin::getOptions($_REQUEST["filter_proptype_id"]));
        $tpl->setVariable("FILTER_TYPE_OPTIONS", array2options($sort_type_list, $_REQUEST["filter_type_id"]));
        $tpl->setVariable("FILTER_TASKBD_OPTIONS", array2options($sort_bdtype_list, $_REQUEST["filter_bd_id"]));
        $tpl->setVariable("FILTER_TITLE", $_REQUEST["filter_title"]);
        $tpl->setVariable("FILTER_ID", $_REQUEST["filter_id"]);
        $tpl->setVariable("FILTER_INN", $_REQUEST["filter_inn"]);
        $tpl->setVariable('FILTER_ROWS_PER_PAGE', array2options($rpp, $_REQUEST['filter_rpp']));
        $tpl->setVariable('FILTER_TI_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_ti_sort_id']));
        $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
        $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];
        if ($_REQUEST["filter_id"]) $filter[] = "lc.id=" . $DB->F($_REQUEST["filter_id"]) . " ";
        if ($_REQUEST["filter_title"]) $filter[] = "(lc.contr_title LIKE " . $DB->F("%" . $_REQUEST["filter_title"] . "%") . " OR lc.official_title LIKE " . $DB->F("%" . $_REQUEST["filter_title"] . "%") . ") ";
        if ($_REQUEST["filter_inn"]) $filter[] = "(lc.inn LIKE " . $DB->F("%" . $_REQUEST["filter_inn"] . "%") . ") ";

        if ($_REQUEST["filter_proptype_id"]) $filter[] = "lc.proptype_id=" . $DB->F($_REQUEST["filter_proptype_id"]) . " ";
        if ($_REQUEST["filter_type_id"]) {
            if ($_REQUEST["filter_type_id"] == 1) {
                $filter[] = "lc.is_supplier";
            } else {
                $filter[] = "lc.is_client";
            }
        }
        if ($_REQUEST["filter_bd_id"]) {
            switch ($_REQUEST["filter_bd_id"]) {
                case "1":
                    $filter[] = " lc.contr_type ";
                    break;

                case "2":
                    $filter[] = " lc.contr_type2 ";
                    break;
                case "3":
                    $filter[] = " lc.contr_type3 ";
                    break;

                case "4":
                    $filter[] = " lc.contr_type4 ";
                    break;

                default:
                    break;
            }
        }

        if (!sizeof($filter)) $filter = " 1 "; else {
            $filter = implode(" AND ", $filter);
        }

         $sql = "SELECT SQL_CALC_FOUND_ROWS lc.*, (SELECT `title` FROM `kontr_proptypes` WHERE `id`=lc.proptype_id) as proptype FROM `list_contr` AS lc $sql_add
                WHERE $filter                    
                ORDER BY
                    $order
                LIMIT {$_REQUEST['filter_start']}," . intval($_REQUEST["filter_rpp"]);


        $DB->query($sql);
        $total = $DB->getFoundRows();
        if ($DB->errno()) UIError($DB->error() . $sql);
        ///if (!$DB->num_rows()) UIError("Контрагенты отсутствуют!", "Справочник Контрагентов", true, "Создать Контрагента", "adm_kontr/edit");
        while ($row = $DB->fetch()) {
            list($id, $title, $type, $type1, $type2, $type3, $desc, $contact, $offtitle, $a1, $a2, $a3, $a4, $proptype, $phone, $fax, $email, $site, $inn, $bn, $itfrom, $itto, $proptype_title) = $row;
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->setVariable("VLI_OFF_TITLE", $offtitle != "" ? $offtitle : "<center>&mdash;</center>");
            $tpl->setVariable("VLI_PROPTYPE", (!empty($row[23]))?$row[23] : "");
//            $tpl->setVariable("VLI_PROPTYPE", kontr_proptypes_plugin::getById($proptype) ? kontr_proptypes_plugin::getById($proptype) : "");
            $agrList = $this->getAgrListByContrID($id);
            if ($agrList) {
                $agrArr = false;
                foreach ($agrList as $key => $val) {
                    $agrArr[] = $key . " от " . $val;
                }
                $tpl->setVariable("VLI_AGR", implode("<br />", $agrArr));
            } else
                $tpl->setVariable("VLI_AGR", "<center>-</center>");
            $tpl->setVariable("VLI_TYPE", $type ? "Подключение" : "");
            //$tpl->setVariable("VLI_CONTACT", $contact);
            /*$tpl->setVariable("VLI_TYPE2", $type1 ? ($type ? "<br />Ремонт/Обслуживание" : "Ремонт/Обслуживание") : "");
            $tpl->setVariable("VLI_TYPE3", $type2 ? (($type || $type1) ? "<br />Аварии" : "Аварии") : "");
            $tpl->setVariable("VLI_TYPE4", $type3 ? (($type || $type1 || $type2) ? "<br />ГП" : "ГП") : "");
            $tpl->setVariable("VLI_DESC", $desc? $desc : "<center>-</center>");
            */
            $tpl->setVariable("VLI_INN", $inn);
            $tpl->setVariable("VLI_PHONE", $phone ? $phone : "&mdash;");
            $tpl->setVariable("VLI_FAX", $fax ? $fax : "&mdash;");
            $tpl->setVariable("VLI_MAIL", $email ? $email : "&mdash;");
            $tpl->setVariable("VLI_SITE", $site ? $site : "&mdash;");
            $sql = "SELECT * FROM `list_contr_contacts` WHERE `contr_id`=" . $DB->F($id) . ";";
            $DB->query($sql);

            if ($DB->num_rows()) {
                $contList1 = array();
                while ($r = $DB->fetch(true)) {
                    $contList1[] = $r["fio"];
                }
                $contList = implode(", <br />", $contList1);
            } else {
                $contList = "&mdash;";
            }
            $DB->free();
            $tpl->setVariable("VLI_WORKERS", $contList);
            //$contact_list = implode(""
            $tpl->parse("vl_item");
        }
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"], $total, "#start-%s"));
        UIHeader($tpl);
        $tpl->show();
    }

    function addfile_ajax()
    {
        global $DB, $USER;
        $files = "";
        $type = $_REQUEST["filetype"];
        $contr_id = $_REQUEST["contr_id"];
        $desc = $_REQUEST["desc"];
        foreach ($_FILES['userfile']['error'] as $i => $error) {
            if ($error == UPLOAD_ERR_OK) {
                $sql = "INSERT INTO `list_contr_files` (`contr_id`, `file_type`, `file_name`, `desc`, `mime_type`, `user_id`) 
                VALUES(" . $DB->F($contr_id) . ", " . $DB->F($type) . ", " . $DB->F($_FILES['userfile']['name'][$i]) . ", " . $DB->F($desc) . ", " . $DB->F($_FILES['userfile']['type'][$i]) . ", " . $DB->F($USER->getId()) . ");";
                $DB->query($sql);
                if (!($file_id = $DB->insert_id())) $err[] = "Ошибка загрузки файла";

                $new_name = getcfg('files_path') . "/c_$file_id.dat";
                if (!copy($_FILES['userfile']['tmp_name'][$i], $new_name)) {
                    trigger_error("Failed to copy '$path' to '$new_name'");

                    $sql = "DELETE FROM `list_contr_files` WHERE `id`=" . $DB->F($file_id);
                    $DB->query($sql);

                    $err[] = "Ошибка копирования файла";
                }
                if (!$file_id)
                    $err[] = "Не удалось добавить файл '{$_FILES['userfile']['name'][$i]}' к контрагенту. $sql";
                //$files .= "<a href=\"".getcfg('http_base')."kontr/getfile?id=".$file_id."&name=".urlencode($_FILES['userfile']['name'][$i]).">".$_FILES['userfile']['name'][$i]."</a><br />";
                //} else
            } elseif ($error != UPLOAD_ERR_NO_FILE) {
                $err[] = "Не удалось загрузить файл '{$_FILES['userfile']['name'][$i]}' на сервер";
            }
        }
        if (sizeof($err)) {
            $ret["error"] = implode("\r\n", $err);
        } else {
            $ret["tpl"] = $files;
            $ret["ok"] = "ok";
            $ret["target"] = $type;
        }
        echo json_encode($ret);
        exit();
    }

    function delete_file_ajax()
    {
        global $USER, $DB;
        $id = $_REQUEST["id"];
        if ($id) {
            $sql = "DELETE FROM `list_contr_files` WHERE `id`=" . $DB->F($id) . " LIMIT 1;";
            $DB->query($sql);
            if ($DB->errno()) $ret["error"] = $DB->error();
            else
                $ret["ok"] = "ok";
        } else {
            $ret["error"] = "Не указан идентификатор файла";
        }
        echo json_encode($ret);
        exit();
    }

    function getfile()
    {
        global $DB, $USER;

        $sql = "SELECT * FROM `list_contr_files` WHERE `id`=" . $DB->F($_REQUEST['id']);
        $f = $DB->getRow($sql, true);
        if (!$f['id']) UIError('Нет такого файла');

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Description: File Transfer");
        header("Content-type: {$f['mime_type']}");
        header("Content-Length: " . filesize(getcfg('files_path') . "/c_{$f['id']}.dat"));
        header("Content-Disposition: attachment; filename=\"{$f['file_name']}\"");
        readfile(getcfg('files_path') . "/c_{$f['id']}.dat");
        exit;
    }

    function files_ajax()
    {
        global $DB, $USER;
        $target = $_REQUEST["target"];
        $contr_id = $_REQUEST["contr_id"];
        if ($target && $contr_id) {
            $sql = "SELECT * FROM `list_contr_files` WHERE `contr_id`=" . $DB->F($contr_id) . " AND `file_type`=" . $DB->F($target) . ";";
            $DB->query($sql);
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $files .= "<tr><td width='30%'><a href=/kontr/getfile?id=" . $r["id"] . "&name=" . urlencode($r["file_name"]) . ">" . $r["file_name"] . "</a></td><td>" . ($r["desc"] ? $r["desc"] : "-- отсутствует --") . "</td><td style='width:16px !important;'><a style='cursor:pointer;' class='deletefile' target_type='" . $r["file_type"] . "' id='" . $r["id"] . "'><img src='/templates/images/cross.png' /></a></td></tr>";
                }
                $ret["tpl"] = "<table class='t1' width='100%'><tr><th>Имя файла</th><th>Описание</th><th></th></tr>" . $files . "</table>";
            } else {
                $ret["tpl"] = "-- файлы не загружены --";
            }
            $ret['ok'] = "ok";
        } else {
            $ret["error"] = "Не указан идентификатор контрагента и/или тип файлов"; /// ." contr_id =".$contr_id." target = ".$target;
        }
        $DB->free();
        echo json_encode($ret);
        exit();
    }

    //// Апдейтит или инсертит записи в tarif
    function upd_tarif($tarif_id, $tarif_name, $tarif_type, $contr_id)
    {
        global $DB, $USER;
        $is_updated = 0;

        if ($tarif_id > 0) {
            $sql = "UPDATE tarifs SET tarif_name = " . $DB->F($tarif_name) . " WHERE contr_id = " . $contr_id . " AND tarif_type_id = " . $tarif_type . " AND id = " . $tarif_id;
            #echo $sql."<br>\n";
            $DB->query($sql);
            #echo "<pre>\n".print_r($DB,true)."</pre>\n";
            #echo "num_rows = ".$DB->num_rows()."<br>\n";
            if ($DB->num_rows() > 0) {
                $is_updated = 1;
            }
        }

        if ($is_updated == 0) {
            $sql = "INSERT INTO tarifs (tarif_type_id, contr_id, tarif_name) VALUES (" . $tarif_type . "," . $contr_id . "," . $DB->F($tarif_name) . ")";
            #echo $sql."<br>\n";
            $DB->query($sql);
            return $DB->insert_id();
        } else {
            return $tarif_id;
        }
    }

    //// Апдейтит или инсертит записи в tarif_values
    function upd_tarif_values($tarif_id, $tarif_type_property_id, $tarif_type_property_value)
    {
        global $DB, $USER;
        $is_updated = 0;
        if ($tarif_id > 0) {
            $sql = "UPDATE tarif_values SET tarif_type_property_value = " . $DB->F($tarif_type_property_value) . " WHERE tarif_id = " . $tarif_id . " AND tarif_type_property_id = " . $tarif_type_property_id;
            #echo $sql."<br>\n";
            $DB->query($sql);
            #echo "num_rows = ".$DB->num_rows()."<br>\n";
            if ($DB->num_rows() > 0) {
                $is_updated = 1;
            }
        }
        if ($is_updated == 0) {
            $sql = "INSERT INTO tarif_values (tarif_id, tarif_type_property_id, tarif_type_property_value) VALUES (" . $tarif_id . "," . $tarif_type_property_id . "," . $DB->F($tarif_type_property_value) . ")";
            #echo $sql."<br>\n";
            $DB->query($sql);
            return 1;
        } else {
            return 2;
        }
    }

    function add_ctv_eq()
    {
        $this->add_internet_eq();
    }

    function add_other_eq()
    {
        $this->add_internet_eq();
    }

    function add_internet_eq()
    {
        global $DB, $USER, $PLUGINS;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/eq.tmpl.htm");
        else
            $tpl->loadTemplatefile("eq.tmpl.htm");

        $contr_id = 0;
        $eq_id = $contr_id;
        $message = "";
        $show_message = $contr_id;
        $tarif_id = $contr_id;
        $action = $contr_id;

        $eq_type = "Интернет";
        $form_title = $PLUGINS[$this->getUID()]['events']['add_internet_eq'];

        if (isset($_REQUEST['eq_type'])) {
            $eq_type = $_REQUEST['eq_type'];
        }

        if ($eq_type == 'ТВ') {
            $form_title = $PLUGINS[$plugin_uid]['events']['add_ctv_eq'];
        }
        if ($eq_type == 'Прочее') {
            $form_title = $PLUGINS[$plugin_uid]['events']['add_other_eq'];
        }

        if ($_REQUEST['contr_id'] > 0) {
            $contr_id = (int)$_REQUEST['contr_id'];
        }
        if ($_REQUEST['eq_id'] > 0) {
            $eq_id = (int)$_REQUEST['eq_id'];
        }
        if ($_REQUEST['action'] > 0) {
            $action = (int)$_REQUEST['action'];
        }


        if (($action == 2) && ($eq_id > 0) && ($contr_id > 0)) {
            $sql = "DELETE FROM equipment WHERE id = " . $eq_id . " AND equipment_type = " . $DB->F($eq_type) . "";
            $DB->query($sql);
            $show_message = 1;
            $message = "Оборудование " . $_REQUEST['name'] . " удалено.<br/>\n";
        }

        if (($action == 1) && ($eq_id >= 0) && ($contr_id > 0)) {
            $is_updated = 0;
            $sql = "UPDATE equipment
SET equipment_name = " . $DB->F($_REQUEST['name']) . "
,equipment_price = " . $DB->F($_REQUEST['price']) . "
,buy_conditions = " . $DB->F($_REQUEST['cond']) . "
,delayed_payment_price = " . $DB->F($_REQUEST['delayed_price']) . "
,delayed_payment_conditions = " . $DB->F($_REQUEST['delayed_cond']) . "
,action_name = " . $DB->F($_REQUEST['act_name']) . "
,action_description = " . $DB->F($_REQUEST['act_descr']) . "
,action_fd = " . $DB->F($this->rutextdate2date($_REQUEST['act_fd'])) . "
,action_td = " . $DB->F($this->rutextdate2date($_REQUEST['act_td'])) . "
,action_other = " . $DB->F($_REQUEST['act_other']) . " 
WHERE id = " . $eq_id . " AND equipment_type = " . $DB->F($eq_type);
            #echo "<pre>\n".$sql."</pre>\n";
            $DB->query($sql);
            #echo "num_rows = ".$DB->num_rows()."<br>\n";
            if ($DB->num_rows() > 0) {
                $is_updated = 1;
            }
            if ($is_updated == 0) {
                $sql = "INSERT INTO equipment (contr_id,
equipment_name,
equipment_type,
equipment_price,
buy_conditions,
delayed_payment_price,
delayed_payment_conditions,
action_name,
action_description,
action_fd,
action_td,
action_other 
) VALUES ( " . $contr_id . "
," . $DB->F($_REQUEST['name']) . "
," . $DB->F($eq_type) . "
," . $DB->F($_REQUEST['price']) . "
," . $DB->F($_REQUEST['cond']) . "
," . $DB->F($_REQUEST['delayed_price']) . "
," . $DB->F($_REQUEST['delayed_cond']) . "
," . $DB->F($_REQUEST['act_name']) . "
," . $DB->F($_REQUEST['act_descr']) . "
," . $DB->F($this->rutextdate2date($_REQUEST['act_fd'])) . "
," . $DB->F($this->rutextdate2date($_REQUEST['act_td'])) . "
," . $DB->F($_REQUEST['act_other']) . "
)";
                #echo "<pre>\n".$sql."</pre>\n";
                $DB->query($sql);
                $eq_id = $DB->insert_id();
                #echo "num_rows = ".$DB->num_rows()."<br>\n";
                if ($DB->num_rows() > 0) {
                    $is_updated = 1;
                }
            }
            $show_message = 1;
            $message = "Оборудование " . $_REQUEST['name'] . " обновлено.<br/>\n";
        }

        $sql = "SELECT 
id,
contr_id,
equipment_name,
equipment_type,
equipment_price,
buy_conditions,
delayed_payment_price,
delayed_payment_conditions,
action_name,
action_description,
action_fd,
action_td,
action_other 
FROM equipment WHERE contr_id = " . $contr_id . " AND equipment_type = " . $DB->F($eq_type) . " AND id = " . $eq_id;

        $DB->query($sql);
        #echo "<pre>".$sql."</pre>\n";
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $id = $r['id'];
                $equipment_name = $r['equipment_name'];
                $equipment_type = $r['equipment_type'];
                $equipment_price = $r['equipment_price'];
                $buy_conditions = $r['buy_conditions'];
                $delayed_payment_price = $r['delayed_payment_price'];
                $delayed_payment_conditions = $r['delayed_payment_conditions'];
                $action_name = $r['action_name'];
                $action_description = $r['action_description'];
                $action_fd = $this->format_date($r['action_fd']);
                $action_td = $this->format_date($r['action_td']);
                $action_other = $r['action_other'];
            }
        }
        $tpl->setVariable("CONTR_ID", $contr_id);
        $tpl->setVariable("EQ_ID", $id);
        $tpl->setVariable("NAME", $equipment_name);
        $tpl->setVariable("PRICE", $equipment_price);
        $tpl->setVariable("COND", $buy_conditions);
        $tpl->setVariable("DELAYED_PRICE", $delayed_payment_price);
        $tpl->setVariable("DELAYED_COND", $delayed_payment_conditions);
        $tpl->setVariable("ACT_NAME", $action_name);
        $tpl->setVariable("ACT_DESCR", $action_description);
        $tpl->setVariable("ACT_FD", $action_fd);
        $tpl->setVariable("ACT_TD", $action_td);
        $tpl->setVariable("ACT_OTHER", $action_other);
        $tpl->setVariable("EQ_TYPE", $eq_type);
        $tpl->setVariable("FORM_TITLE", $form_title);
        $tpl->setVariable("MESSAGE", $message);
        UIHeader($tpl);
        $tpl->show();
    }

    function add_ictv_plan()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/ictv.tmpl.htm");
        else
            $tpl->loadTemplatefile("ictv.tmpl.htm");

        $contr_id = 0;
        $message = "";
        $show_message = $contr_id;
        $tarif_id = $contr_id;
        $action = $contr_id;
        if ($_REQUEST['contr_id'] > 0) {
            $contr_id = (int)$_REQUEST['contr_id'];
        }
        if ($_REQUEST['tarif_id'] > 0) {
            $tarif_id = (int)$_REQUEST['tarif_id'];
        }
        if ($_REQUEST['action'] > 0) {
            $action = (int)$_REQUEST['action'];
        }
        if (($action == 2) && ($tarif_id > 0) && ($contr_id > 0)) {
            $sql = "DELETE FROM tarif_values WHERE tarif_id = " . $tarif_id . " AND tarif_type_property_id IN (8,9,10)";
            $DB->query($sql);
            $sql = "DELETE FROM tarifs WHERE contr_id = " . $contr_id . " AND tarif_type_id = 3 AND id = " . $tarif_id . " AND NOT exists (SELECT * FROM tarif_values WHERE tarif_id = " . $tarif_id . ")";
            $DB->query($sql);
            $show_message = 1;
            $message = "Тарифный план " . $_REQUEST['tarif_name'] . " удален.<br/>\n";
        }

        if (($action == 1) && ($tarif_id >= 0) && ($contr_id > 0)) {
            $tarif_id = $this->upd_tarif($tarif_id, $_REQUEST['tarif_name'], 3, $contr_id);
            $this->upd_tarif_values($tarif_id, 8, $_REQUEST['tarif_ap']);
            $this->upd_tarif_values($tarif_id, 9, $_REQUEST['tarif_speed']);
            $this->upd_tarif_values($tarif_id, 10, $_REQUEST['tarif_channels']);
            $show_message = 1;
            $message = "Тарифный план " . $_REQUEST['tarif_name'] . " обновлен.<br/>\n";
        }

        $sql = "SELECT max(tarif_name) tarif_name,
            max(CASE WHEN tarif_type_property_id = 8 THEN tarif_type_property_value ELSE NULL END) tarif_ap,
            max(CASE WHEN tarif_type_property_id = 9 THEN tarif_type_property_value ELSE NULL END) tarif_speed,
            max(CASE WHEN tarif_type_property_id = 10 THEN tarif_type_property_value ELSE NULL END) tarif_channels
            FROM (
            SELECT 
                t.contr_id,
                tt.tarif_type,
                t.tarif_type_id,
                t.tarif_name,
                tv.tarif_id,
                tp.property_name,
                tv.tarif_type_property_id,
                tp.property_order,
                tv.id AS tarif_value_id,
                tv.tarif_type_property_value
            FROM (((tarifs t 
                    JOIN tarif_types tt) 
                    JOIN tarif_type_properties tp) 
                    JOIN tarif_values tv) 
            WHERE ((tt.id = t.tarif_type_id) 
               AND (tt.id = tp.tarif_type_id) 
               AND (t.id = tv.tarif_id) 
               AND t.contr_id = " . $DB->F($contr_id) . "
               AND tv.tarif_id = " . $DB->F($tarif_id) . "
               AND (tp.id = tv.tarif_type_property_id))
            ) tov";
        $DB->query($sql);
        #echo "<pre>".$sql."</pre>\n";
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $tarif_name = $r['tarif_name'];
                $tarif_ap = $r['tarif_ap'];
                $tarif_speed = $r['tarif_speed'];
                $tarif_channels = $r['tarif_channels'];
            }
        }
        $tpl->setVariable("CONTR_ID", $contr_id);
        $tpl->setVariable("TARIF_NAME", $tarif_name);
        $tpl->setVariable("TARIF_ID", $tarif_id);
        $tpl->setVariable("AP", $tarif_ap);
        $tpl->setVariable("SPEED", $tarif_speed);
        $tpl->setVariable("CHANNELS", $tarif_channels);
        $tpl->setVariable("MESSAGE", $message);
        UIHeader($tpl);
        $tpl->show();
    }


    function add_phone_plan()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/phone.tmpl.htm");
        else
            $tpl->loadTemplatefile("phone.tmpl.htm");

        $contr_id = 0;
        $message = "";
        $show_message = $contr_id;
        $tarif_id = $contr_id;
        $action = $contr_id;
        if ($_REQUEST['contr_id'] > 0) {
            $contr_id = (int)$_REQUEST['contr_id'];
        }
        if ($_REQUEST['tarif_id'] > 0) {
            $tarif_id = (int)$_REQUEST['tarif_id'];
        }
        if ($_REQUEST['action'] > 0) {
            $action = (int)$_REQUEST['action'];
        }
        if (($action == 2) && ($tarif_id > 0) && ($contr_id > 0)) {
            $sql = "DELETE FROM tarif_values WHERE tarif_id = " . $tarif_id . " AND tarif_type_property_id IN (9, 10)";
            $DB->query($sql);
            $sql = "DELETE FROM tarifs WHERE contr_id = " . $contr_id . " AND tarif_type_id = 4 AND id = " . $tarif_id . " AND NOT exists (SELECT * FROM tarif_values WHERE tarif_id = " . $tarif_id . ")";
            $DB->query($sql);
            $show_message = 1;
            $message = "Тарифный план " . $_REQUEST['tarif_name'] . " удален.<br/>\n";
        }

        if (($action == 1) && ($tarif_id >= 0) && ($contr_id > 0)) {
            $tarif_id = $this->upd_tarif($tarif_id, $_REQUEST['tarif_name'], 4, $contr_id);
            $this->upd_tarif_values($tarif_id, 9, $_REQUEST['tarif_ap']);
            $this->upd_tarif_values($tarif_id, 10, $_REQUEST['tarif_speed']);
            $show_message = 1;
            $message = "Тарифный план " . $_REQUEST['tarif_name'] . " обновлен.<br/>\n";
        }
        $sql = "SELECT max(tarif_name) tarif_name,
            max(CASE WHEN tarif_type_property_id = 9 THEN tarif_type_property_value ELSE NULL END) tarif_ap,
            max(CASE WHEN tarif_type_property_id = 10 THEN tarif_type_property_value ELSE NULL END) tarif_speed
            FROM (
            SELECT 
                t.contr_id,
                tt.tarif_type,
                t.tarif_type_id,
                t.tarif_name,
                tv.tarif_id,
                tp.property_name,
                tv.tarif_type_property_id,
                tp.property_order,
                tv.id AS tarif_value_id,
                tv.tarif_type_property_value
            FROM (((tarifs t 
                    JOIN tarif_types tt) 
                    JOIN tarif_type_properties tp) 
                    JOIN tarif_values tv) 
            WHERE ((tt.id = t.tarif_type_id) 
               AND (tt.id = tp.tarif_type_id) 
               AND (t.id = tv.tarif_id) 
               AND t.contr_id = " . $DB->F($contr_id) . "
               AND tv.tarif_id = " . $DB->F($tarif_id) . "
               AND (tp.id = tv.tarif_type_property_id))
            ) tov";

        $DB->query($sql);
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $tarif_name = $r['tarif_name'];
                $tarif_ap = $r['tarif_ap'];
                $tarif_speed = $r['tarif_speed'];
            }
        }
        $tpl->setVariable("CONTR_ID", $contr_id);
        $tpl->setVariable("TARIF_NAME", $tarif_name);
        $tpl->setVariable("TARIF_ID", $tarif_id);
        $tpl->setVariable("AP", $tarif_ap);
        $tpl->setVariable("SPEED", $tarif_speed);
        $tpl->setVariable("MESSAGE", $message);
        UIHeader($tpl);
        $tpl->show();
    }

    function add_ctv_plan()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/ctv.tmpl.htm");
        else
            $tpl->loadTemplatefile("ctv.tmpl.htm");

        $contr_id = 0;
        $message = "";
        $show_message = $contr_id;
        $tarif_id = $contr_id;
        $action = $contr_id;
        if ($_REQUEST['contr_id'] > 0) {
            $contr_id = (int)$_REQUEST['contr_id'];
        }
        if ($_REQUEST['tarif_id'] > 0) {
            $tarif_id = (int)$_REQUEST['tarif_id'];
        }
        if ($_REQUEST['action'] > 0) {
            $action = (int)$_REQUEST['action'];
        }
        if (($action == 2) && ($tarif_id > 0) && ($contr_id > 0)) {
            $sql = "DELETE FROM tarif_values WHERE tarif_id = " . $tarif_id . " AND tarif_type_property_id IN (4,5)";
            $DB->query($sql);
            $sql = "DELETE FROM tarifs WHERE contr_id = " . $contr_id . " AND tarif_type_id = 2 AND id = " . $tarif_id . " AND NOT exists (SELECT * FROM tarif_values WHERE tarif_id = " . $tarif_id . ")";
            $DB->query($sql);
            $show_message = 1;
            $message = "Тарифный план " . $_REQUEST['tarif_name'] . " удален.<br/>\n";
        }

        if (($action == 1) && ($tarif_id >= 0) && ($contr_id > 0)) {
            $tarif_id = $this->upd_tarif($tarif_id, $_REQUEST['tarif_name'], 2, $contr_id);
            $this->upd_tarif_values($tarif_id, 4, $_REQUEST['tarif_ap']);
            $this->upd_tarif_values($tarif_id, 5, $_REQUEST['tarif_speed']);
            $show_message = 1;
            $message = "Тарифный план " . $_REQUEST['tarif_name'] . " обновлен.<br/>\n";
        }
        $sql = "SELECT max(tarif_name) tarif_name,
                max(CASE WHEN tarif_type_property_id = 4 THEN tarif_type_property_value ELSE NULL END) tarif_ap,
                max(CASE WHEN tarif_type_property_id = 5 THEN tarif_type_property_value ELSE NULL END) tarif_speed
                FROM (
                SELECT 
                    t.contr_id,
                    tt.tarif_type,
                    t.tarif_type_id,
                    t.tarif_name,
                    tv.tarif_id,
                    tp.property_name,
                    tv.tarif_type_property_id,
                    tp.property_order,
                    tv.id AS tarif_value_id,
                    tv.tarif_type_property_value
                FROM (((tarifs t 
                        JOIN tarif_types tt) 
                        JOIN tarif_type_properties tp) 
                        JOIN tarif_values tv) 
                WHERE ((tt.id = t.tarif_type_id) 
                   AND (tt.id = tp.tarif_type_id) 
                   AND (t.id = tv.tarif_id) 
                   AND t.contr_id = " . $DB->F($contr_id) . "
                   AND tv.tarif_id = " . $DB->F($tarif_id) . "
                   AND (tp.id = tv.tarif_type_property_id))
                ) tov";

        $DB->query($sql);
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $tarif_name = $r['tarif_name'];
                $tarif_ap = $r['tarif_ap'];
                $tarif_speed = $r['tarif_speed'];
            }
        }
        $tpl->setVariable("CONTR_ID", $contr_id);
        $tpl->setVariable("TARIF_NAME", $tarif_name);
        $tpl->setVariable("TARIF_ID", $tarif_id);
        $tpl->setVariable("AP", $tarif_ap);
        $tpl->setVariable("SPEED", $tarif_speed);
        $tpl->setVariable("MESSAGE", $message);
        UIHeader($tpl);
        $tpl->show();
    }

    function add_internet_plan()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/internet.tmpl.htm");
        else
            $tpl->loadTemplatefile("internet.tmpl.htm");

        $contr_id = 0;
        $message = "";
        $show_message = $contr_id;
        $tarif_id = $contr_id;
        $action = $contr_id;
        if ($_REQUEST['contr_id'] > 0) {
            $contr_id = (int)$_REQUEST['contr_id'];
        }
        if ($_REQUEST['tarif_id'] > 0) {
            $tarif_id = (int)$_REQUEST['tarif_id'];
        }
        if ($_REQUEST['action'] > 0) {
            $action = (int)$_REQUEST['action'];
        }
        if (($action == 2) && ($tarif_id > 0) && ($contr_id > 0)) {
            $sql = "DELETE FROM tarif_values WHERE tarif_id = " . $tarif_id . " AND tarif_type_property_id IN (1,2)";
            $DB->query($sql);
            $sql = "DELETE FROM tarifs WHERE contr_id = " . $contr_id . " AND tarif_type_id = 1 AND id = " . $tarif_id . " AND NOT exists (SELECT * FROM tarif_values WHERE tarif_id = " . $tarif_id . ")";
            $DB->query($sql);
            $show_message = 1;
            $message = "Тарифный план " . $_REQUEST['tarif_name'] . " удален.<br/>\n";
        }

        if (($action == 1) && ($tarif_id >= 0) && ($contr_id > 0)) {
            $tarif_id = $this->upd_tarif($tarif_id, $_REQUEST['tarif_name'], 1, $contr_id);
            $this->upd_tarif_values($tarif_id, 1, $_REQUEST['tarif_ap']);
            $this->upd_tarif_values($tarif_id, 2, $_REQUEST['tarif_speed']);
            $show_message = 1;
            $message = "Тарифный план " . $_REQUEST['tarif_name'] . " обновлен.<br/>\n";
        }
        $sql = "SELECT max(tarif_name) tarif_name,
            max(CASE WHEN tarif_type_property_id = 1 THEN tarif_type_property_value ELSE NULL END) tarif_ap,
            max(CASE WHEN tarif_type_property_id = 2 THEN tarif_type_property_value ELSE NULL END) tarif_speed
            FROM (
            SELECT 
                t.contr_id,
                tt.tarif_type,
                t.tarif_type_id,
                t.tarif_name,
                tv.tarif_id,
                tp.property_name,
                tv.tarif_type_property_id,
                tp.property_order,
                tv.id AS tarif_value_id,
                tv.tarif_type_property_value
            FROM (((tarifs t 
                    JOIN tarif_types tt) 
                    JOIN tarif_type_properties tp) 
                    JOIN tarif_values tv) 
            WHERE ((tt.id = t.tarif_type_id) 
               AND (tt.id = tp.tarif_type_id) 
               AND (t.id = tv.tarif_id) 
               AND t.contr_id = " . $DB->F($contr_id) . "
               AND tv.tarif_id = " . $DB->F($tarif_id) . "
               AND (tp.id = tv.tarif_type_property_id))
            ) tov";
        $DB->query($sql);
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $tarif_name = $r['tarif_name'];
                $tarif_ap = $r['tarif_ap'];
                $tarif_speed = $r['tarif_speed'];
            }
        }
        $tpl->setVariable("CONTR_ID", $contr_id);
        $tpl->setVariable("TARIF_NAME", $tarif_name);
        $tpl->setVariable("TARIF_ID", $tarif_id);
        $tpl->setVariable("AP", $tarif_ap);
        $tpl->setVariable("SPEED", $tarif_speed);
        $tpl->setVariable("MESSAGE", $message);
        UIHeader($tpl);
        $tpl->show();
    }

    function view()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("view.tmpl.htm");
        $bn = array(
            0 => "-- не указан --",
            1 => "Наличный расчет",
            2 => "Безналичный расчет"
        );
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/view.tmpl.htm");
        else
            $tpl->loadTemplatefile("view.tmpl.htm");

        $contr_id = $_REQUEST['contr_id'];
        $sql = "SELECT * FROM `list_contr` WHERE `id`=" . $DB->F($contr_id) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) UIError("Контрагент с таким идентификатором отсутствует.");
        $result = $DB->fetch(true);
        $tpl->setVariable("CONTR_ID", $result["id"]);
        #echo $result["id"];
        #exit;
        $tpl->setVariable("CONTR_TITLE", $result["contr_title"]);
        $tpl->setVariable("CONTR_DESC", $result["contr_desc"] ? $result["contr_desc"] : "&mdash;");
        $tpl->setVariable("CONTR_CONTACT", "<strong>" . $result["contact"] . "</strong>");
        $tpl->setVariable("CONTR_PROPTYPE", kontr_proptypes_plugin::getById($result["proptype_id"]) ? kontr_proptypes_plugin::getById($result["proptype_id"]) : "&mdash;");
        $tpl->setVariable("CONTR_OFF_TITLE", $result["official_title"] ? kontr_proptypes_plugin::getById($result["proptype_id"]) . " " . $result["official_title"] : "&mdash;");
        $tpl->setVariable("INN", $result["inn"] ? $result["inn"] : "&mdash;");
        $tpl->setVariable("PHONE", $result["phone"] ? $result["phone"] : "&mdash;");
        $tpl->setVariable("FAX", $result["fax"] ? $result["fax"] : "&mdash;");
        $tpl->setVariable("EMAIL", $result["email"] ? $result["email"] : "&mdash;");
        $tpl->setVariable("SITE", $result["site"] ? $result["site"] : "&mdash;");
        $tpl->setVariable("PAYTYPE", $bn[$result["bn"]] ? $bn[$result["bn"]] : "не указан");
        $tpl->setVariable("IT_FROM", $result["it_from"] ? date("H:i", strtotime($result["it_from"])) : "&mdash;");
        $tpl->setVariable("IT_TO", $result["it_to"] ? date("H:i", strtotime($result["it_to"])) : "&mdash;");
        $isClient = $this->isClient($contr_id);
        if ($isClient) {
            $tpl->touchBlock("typeclient");
        }
        $isSuplier = $this->isSupplier($contr_id);
        if ($isSuplier) {
            $tpl->touchBlock("typesupplier");
        }
        if (!$isClient && !$isSuplier) {
            $tpl->touchBlock("notype");
        }
        if ($result["contr_type"]) {
            $tpl->touchBlock("typeconn");
        }
        if ($result["contr_type2"]) {
            $tpl->touchBlock("typefix");
        }
        if ($result["contr_type3"]) {
            $tpl->touchBlock("typeacc");
        }
        if ($result["contr_type_tm"]) {
            #$tpl->touchBlock("typetm");
            $tarif_links = "<li><a data-toggle=\"tab\" href=\"#matrix-tariffs\">Тарифы</a></li>
            <li><a data-toggle=\"tab\" href=\"#matrix-equipment\">Оборудование</a></li>
            <li><a data-toggle=\"tab\" href=\"#matrix-services\">Дополнительные услуги</a></li>";
            $tpl->setVariable("TARIF_LINKS", $tarif_links);
        }

        $DB->query("SELECT * FROM `users` WHERE `contr_user`=" . $DB->F($contr_id) . ";");
        if ($DB->num_rows()) {
            $tpl->setVariable("USER_NO_RECORDS", "style='display:none;'");
            while ($r = $DB->fetch(true)) {
                $tpl->setCurrentBlock("userrow");
                if ($r["allow_access_cu"]) {
                    $tpl->setVariable("IS_ACTIVE", "checked='checked'");
                    $tpl->setVariable("STATUS_COLOR", "#99ff99");
                } else {
                    //$tpl->setVariable("STATUS_COLOR", "#e2e4ea");
                }
                $tpl->setVariable("USER_ID", $r["id"]);
                $tpl->setVariable("USER_FIO", $r["fio"]);
                $tpl->setVariable("USER_LOGIN", $r["login"]);
                $tpl->setVariable("USER_EMAIL", $r["email"]);
                $tpl->setVariable("USER_PHONE", $r["phones"]);
                $wtypeList = adm_users_plugin::getLKWtypeList($r["id"]);
                if ($wtypeList) {
                    foreach ($wtypeList as $key => $item) {
                        $tpl->setCurrentBlock("wtype-user-lk-list");
                        $tpl->setVariable("USER_WTYPE_ID", $key);
                        $tpl->setVariable("USER_WTYPE_NAME", $item);
                        $tpl->setVariable("USER_WTYPE_USER_ID", $r["id"]);
                        $tpl->parse("wtype-user-lk-list");
                    }
                } else {
                    $tpl->touchBlock("showallwlu");
                }
                $tpl->parse("userrow");
            }
        } else {
            $tpl->setVariable("USER_NO_RECORDS", " ");
        }
        $DB->free();
        $DB->query("SELECT * FROM `list_contr_contacts` WHERE `contr_id`=" . $DB->F($result["id"]) . ";");
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            $tpl->setVariable("NORECORDS", "style=\"display: none;\"");
            while ($r = $DB->fetch(true)) {
                $tpl->setCurrentBlock("rr-item");
                $tpl->setVariable("ID", $r["id"]);
                $tpl->setVariable("NAME", $r["fio"]);
                $tpl->setVariable("POS", $r["pos"]);
                $tpl->setVariable("PHONE", $r["phone"]);
                $tpl->setVariable("EMAIL", $r["email"]);
                $tpl->setVariable("BDATE", $r["bdate"]);
                $tpl->parse("rr-item");
            }
        } else {
            $tpl->setVariable("NORECIPIENTS", "disabled='disabled'");
        }

        $DB->free();
        $sql = "SELECT list_contr_agr.*, task_status.name as status FROM `list_contr_agr` left join task_status ON task_status.id = list_contr_agr.status_id WHERE `contr_id`=" . $DB->F($contr_id) . " order by status;";
        $DB->query($sql);
        if ($DB->num_rows()) {
            $ret = false;
            while ($a = $DB->fetch(true)) {
                $rDate = explode("-", $a["agr_date"]);
                $sql = "SELECT tt.id, tt.title FROM `link_agr_wtypes` AS lwt LEFT JOIN `task_types` AS tt ON lwt.wtype_id=tt.id WHERE lwt.agr_id=" . $DB->F($a["id"]) . ";";
                $res = $DB->getCell2($sql);
                if (!$res) {
                    $tpl->touchBlock("no-uwl");
                } else {
                    foreach ($res as $key => $v) {
                        if ($a["status_id"] == "84") {
                            $tpl->setCurrentBlock("user-wtype-list");
                            $tpl->setVariable("WTYPE_ID", $key);
                            $tpl->setVariable("WTYPE_NAME", $v);
                            $tpl->setVariable("WTYPE_AGR", $a["number"] . " от" . $rDate["2"] . "." . $rDate[1] . "." . $rDate[0]);
                            $tpl->parse("user-wtype-list");
                        }
                    }
                }
                $tpl->setCurrentBlock("agritem");
                $tpl->setVariable("AGR_ID", $a["id"]);
                $tpl->setVariable("AGR_NUM", $a["number"]);
                if ($res) {
                    $tpl->setVariable("AGR_TYPES", implode(", ", $res));
                } else {
                    $tpl->setVariable("AGR_TYPES", "<center> - </center>");
                }
                $tpl->setVariable("CNTR_ID", $contr_id);
                $tpl->setVariable("AGR_DATE", $rDate["2"] . "." . $rDate[1] . "." . $rDate[0]);
                $tpl->setVariable("AGR_ACCESS", $a["access_contact"]);
                $tpl->setVariable("AGR_TP", $a["tp_contact"]);
                $tpl->setVariable("AGR_COMMENT", $a["coment"]);
                $tpl->setVariable("AGR_STATE", $a["status"]);
                $type = false;
                if ($a['type_price'])
                    $type[] = "Прайс";
                if ($a['type_smeta'])
                    $type[] = "Смета";
                if ($a['type_abon'])
                    $type[] = "Абонентская плата";
                if ($type) {
                    $tpl->setVariable("AGR_W_TYPES", implode(", <br />", $type));
                } else {
                    $tpl->setVariable("AGR_W_TYPES", "<strong>&mdash;</strong>");
                }
                $tpl->parse("agritem");
            }
            $DB->free();
        } else {
            $tpl->touchBlock("noagrs");
        }
        $sql = "SELECT * FROM `kontr_bonus_plan` WHERE `kontr_id`=" . $DB->F($contr_id) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError();
        if ($DB->num_rows()) {
            $tpl->setVariable("NR_HIDDEN", 'hidden');
            while ($r = $DB->fetch(true)) {
                $tpl->setCurrentBlock("planitem");
                $tpl->setVariable("PLAN_ID", $r["id"]);
                $tpl->setVariable("PLAN_PERCENT", $r["convval"]);
                $tpl->setVariable("PLAN_AVGCHECK", $r["midcheck"]);
                $tpl->setVariable("PLAN_BONUS", $r["bonusval"]);
                $tpl->parse("planitem");
            }
        }

        //// EF: Отрисовка тарифных планов контрагента
        if ($result["contr_type_tm"] > 0) {
            //// 1. Тарифные планы Интернет:

            $sql = "SELECT tarif_id, tarif_name, 
max(CASE WHEN tarif_type_property_id = 1 THEN tarif_type_property_value ELSE NULL END) tarif_ap,
max(CASE WHEN tarif_type_property_id = 2 THEN tarif_type_property_value ELSE NULL END) tarif_speed
FROM (
SELECT 
    t.contr_id,
    tt.tarif_type,
    t.tarif_type_id,
    t.tarif_name,
    tv.tarif_id,
    tp.property_name,
    tv.tarif_type_property_id,
    tp.property_order,
    tv.id AS tarif_value_id,
    tv.tarif_type_property_value
FROM (((tarifs t 
        JOIN tarif_types tt) 
        JOIN tarif_type_properties tp) 
        JOIN tarif_values tv) 
WHERE ((tt.id = t.tarif_type_id) 
   AND (tt.id = tp.tarif_type_id) 
   AND (t.id = tv.tarif_id) 
   AND t.tarif_type_id = 1 
   AND t.contr_id = " . $DB->F($contr_id) . "
   AND (tp.id = tv.tarif_type_property_id))
) tov
GROUP BY tarif_name,tarif_id";
            $DB->query($sql);
            if ($DB->errno()) UIError();
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("internet-tarif-item");
                    $tpl->setVariable("TARIF_NAME", $r["tarif_name"]);
                    $tpl->setVariable("TARIF_AP", $r["tarif_ap"]);
                    $tpl->setVariable("TARIF_SPEED", $r["tarif_speed"]);
                    $tpl->setVariable("TARIF_ID", $r["tarif_id"]);
                    $tpl->setVariable("TR_CONTR_ID", $contr_id);
                    $tpl->parse("internet-tarif-item");
                }
            }

            //// 2. Тарифные планы ТВ

            $sql = "SELECT tarif_id, tarif_name, 
max(CASE WHEN tarif_type_property_id = 4 THEN tarif_type_property_value ELSE NULL END) tarif_ap,
max(CASE WHEN tarif_type_property_id = 5 THEN tarif_type_property_value ELSE NULL END) tarif_channels
FROM (
SELECT 
    t.contr_id,
    tt.tarif_type,
    t.tarif_type_id,
    t.tarif_name,
    tv.tarif_id,
    tp.property_name,
    tv.tarif_type_property_id,
    tp.property_order,
    tv.id AS tarif_value_id,
    tv.tarif_type_property_value
FROM (((tarifs t 
        JOIN tarif_types tt) 
        JOIN tarif_type_properties tp) 
        JOIN tarif_values tv) 
WHERE ((tt.id = t.tarif_type_id) 
   AND (tt.id = tp.tarif_type_id) 
   AND (t.id = tv.tarif_id) 
   AND t.tarif_type_id = 2 
   AND t.contr_id = " . $DB->F($contr_id) . "
   AND (tp.id = tv.tarif_type_property_id))
) tov
GROUP BY tarif_name,tarif_id";
            $DB->query($sql);
            if ($DB->errno()) UIError();
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("ctv-tarif-item");
                    $tpl->setVariable("CTARIF_NAME", $r["tarif_name"]);
                    $tpl->setVariable("CTARIF_AP", $r["tarif_ap"]);
                    $tpl->setVariable("CTARIF_CHANNELS", $r["tarif_channels"]);
                    $tpl->setVariable("CTARIF_ID", $r["tarif_id"]);
                    $tpl->setVariable("CTR_CONTR_ID", $contr_id);
                    $tpl->parse("ctv-tarif-item");
                }
            }

            //// 3. Тарифные планы Интернет + ТВ

            $sql = "SELECT tarif_id, tarif_name, 
max(CASE WHEN tarif_type_property_id = 8 THEN tarif_type_property_value ELSE NULL END) tarif_ap,
max(CASE WHEN tarif_type_property_id = 9 THEN tarif_type_property_value ELSE NULL END) tarif_speed,
max(CASE WHEN tarif_type_property_id = 10 THEN tarif_type_property_value ELSE NULL END) tarif_channels
FROM (
SELECT 
    t.contr_id,
    tt.tarif_type,
    t.tarif_type_id,
    t.tarif_name,
    tv.tarif_id,
    tp.property_name,
    tv.tarif_type_property_id,
    tp.property_order,
    tv.id AS tarif_value_id,
    tv.tarif_type_property_value
FROM (((tarifs t 
        JOIN tarif_types tt) 
        JOIN tarif_type_properties tp) 
        JOIN tarif_values tv) 
WHERE ((tt.id = t.tarif_type_id) 
   AND (tt.id = tp.tarif_type_id) 
   AND (t.id = tv.tarif_id) 
   AND t.tarif_type_id = 3
   AND t.contr_id = " . $DB->F($contr_id) . "
   AND (tp.id = tv.tarif_type_property_id))
) tov
GROUP BY tarif_name,tarif_id";
            $DB->query($sql);
            if ($DB->errno()) UIError();
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("ictv-tarif-item");
                    $tpl->setVariable("ICTARIF_NAME", $r["tarif_name"]);
                    $tpl->setVariable("ICTARIF_AP", $r["tarif_ap"]);
                    $tpl->setVariable("ICTARIF_SPEED", $r["tarif_speed"]);
                    $tpl->setVariable("ICTARIF_CHANNELS", $r["tarif_channels"]);
                    $tpl->setVariable("ICTARIF_ID", $r["tarif_id"]);
                    $tpl->setVariable("ICTR_CONTR_ID", $contr_id);
                    $tpl->parse("ictv-tarif-item");
                }
            }

            //// 4. Телефония

            $sql = "SELECT tarif_id, tarif_name, 
max(CASE WHEN tarif_type_property_id = 9 THEN tarif_type_property_value ELSE NULL END) tarif_ap,
max(CASE WHEN tarif_type_property_id = 10 THEN tarif_type_property_value ELSE NULL END) tarif_speed
FROM (
SELECT 
    t.contr_id,
    tt.tarif_type,
    t.tarif_type_id,
    t.tarif_name,
    tv.tarif_id,
    tp.property_name,
    tv.tarif_type_property_id,
    tp.property_order,
    tv.id AS tarif_value_id,
    tv.tarif_type_property_value
FROM (((tarifs t 
        JOIN tarif_types tt) 
        JOIN tarif_type_properties tp) 
        JOIN tarif_values tv) 
WHERE ((tt.id = t.tarif_type_id) 
   AND (tt.id = tp.tarif_type_id) 
   AND (t.id = tv.tarif_id) 
   AND t.tarif_type_id = 4
   AND t.contr_id = " . $DB->F($contr_id) . "
   AND (tp.id = tv.tarif_type_property_id))
) tov
GROUP BY tarif_name,tarif_id";

            $DB->query($sql);
            if ($DB->errno()) UIError();
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("phone-tarif-item");
                    $tpl->setVariable("PTARIF_NAME", $r["tarif_name"]);
                    $tpl->setVariable("PTARIF_AP", $r["tarif_ap"]);
                    $tpl->setVariable("PTARIF_SPEED", $r["tarif_speed"]);
                    $tpl->setVariable("PTARIF_ID", $r["tarif_id"]);
                    $tpl->setVariable("PTR_CONTR_ID", $contr_id);
                    $tpl->parse("phone-tarif-item");
                }
            }

            //// 5. Оборудование Интернет
            $sql = "SELECT id,
contr_id,
equipment_name,
equipment_type,
equipment_price,
buy_conditions,
delayed_payment_price,
delayed_payment_conditions,
action_name,
action_description,
action_fd,
action_td,
action_other
FROM equipment
WHERE contr_id = " . $contr_id . "
  AND equipment_type = 'Интернет'
ORDER BY equipment_name";

            $DB->query($sql);
            if ($DB->errno()) UIError();
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("internet-eq-item");
                    $tpl->setVariable("IEQ_NAME", $r["equipment_name"]);
                    $tpl->setVariable("IEQ_PRICE", $r["equipment_price"]);
                    $tpl->setVariable("IEQ_COND", $r["buy_conditions"]);
                    $tpl->setVariable("IEQ_DELAYED_PRICE", $r["delayed_payment_price"]);
                    $tpl->setVariable("IEQ_DELAYED_COND", $r["delayed_payment_conditions"]);
                    $tpl->setVariable("IEQ_ACT_NAME", $r["action_name"]);
                    $tpl->setVariable("IEQ_ACT_DESCR", $r["action_description"]);
                    $tpl->setVariable("IEQ_ACT_FD", $this->format_date($r["action_fd"]));
                    $tpl->setVariable("IEQ_ACT_TD", $this->format_date($r["action_td"]));
                    $tpl->setVariable("IEQ_ACT_OTHER", $r["action_other"]);
                    $tpl->setVariable("IEQ_CONTR_ID", $r["contr_id"]);
                    $tpl->setVariable("IEQ_ID", $r["id"]);
                    $tpl->parse("internet-eq-item");
                }
            }

            //// 5. Оборудование ТВ
            $sql = "SELECT id,
contr_id,
equipment_name,
equipment_type,
equipment_price,
buy_conditions,
delayed_payment_price,
delayed_payment_conditions,
action_name,
action_description,
action_fd,
action_td,
action_other
FROM equipment
WHERE contr_id = " . $contr_id . "
  AND equipment_type = 'ТВ'
ORDER BY equipment_name";

            $DB->query($sql);
            if ($DB->errno()) UIError();
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("ctv-eq-item");
                    $tpl->setVariable("CEQ_NAME", $r["equipment_name"]);
                    $tpl->setVariable("CEQ_PRICE", $r["equipment_price"]);
                    $tpl->setVariable("CEQ_COND", $r["buy_conditions"]);
                    $tpl->setVariable("CEQ_DELAYED_PRICE", $r["delayed_payment_price"]);
                    $tpl->setVariable("CEQ_DELAYED_COND", $r["delayed_payment_conditions"]);
                    $tpl->setVariable("CEQ_ACT_NAME", $r["action_name"]);
                    $tpl->setVariable("CEQ_ACT_DESCR", $r["action_description"]);
                    $tpl->setVariable("CEQ_ACT_FD", $this->format_date($r["action_fd"]));
                    $tpl->setVariable("CEQ_ACT_TD", $this->format_date($r["action_td"]));
                    $tpl->setVariable("CEQ_ACT_OTHER", $r["action_other"]);
                    $tpl->setVariable("CEQ_CONTR_ID", $r["contr_id"]);
                    $tpl->setVariable("CEQ_ID", $r["id"]);
                    $tpl->parse("ctv-eq-item");
                }
            }

            //// 6. Прочее оборудование
            $sql = "SELECT id,
contr_id,
equipment_name,
equipment_type,
equipment_price,
buy_conditions,
delayed_payment_price,
delayed_payment_conditions,
action_name,
action_description,
action_fd,
action_td,
action_other
FROM equipment
WHERE contr_id = " . $contr_id . "
  AND equipment_type = 'Прочее'
ORDER BY equipment_name";

            $DB->query($sql);
            if ($DB->errno()) UIError();
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("other-eq-item");
                    $tpl->setVariable("OEQ_NAME", $r["equipment_name"]);
                    $tpl->setVariable("OEQ_PRICE", $r["equipment_price"]);
                    $tpl->setVariable("OEQ_COND", $r["buy_conditions"]);
                    $tpl->setVariable("OEQ_DELAYED_PRICE", $r["delayed_payment_price"]);
                    $tpl->setVariable("OEQ_DELAYED_COND", $r["delayed_payment_conditions"]);
                    $tpl->setVariable("OEQ_ACT_NAME", $r["action_name"]);
                    $tpl->setVariable("OEQ_ACT_DESCR", $r["action_description"]);
                    $tpl->setVariable("OEQ_ACT_FD", $this->format_date($r["action_fd"]));
                    $tpl->setVariable("OEQ_ACT_TD", $this->format_date($r["action_td"]));
                    $tpl->setVariable("OEQ_ACT_OTHER", $r["action_other"]);
                    $tpl->setVariable("OEQ_CONTR_ID", $r["contr_id"]);
                    $tpl->setVariable("OEQ_ID", $r["id"]);
                    $tpl->parse("other-eq-item");
                }
            }

            //// 7. Услуги
            $sql = "SELECT 
    id, 
    service_name,
    (SELECT count(*) FROM link_contragent_services l WHERE l.service_id = s.id AND l.contragent_id = " . $contr_id . ") for_sale
  FROM services_for_sale s";
            $DB->query($sql);
            if ($DB->errno()) UIError();
            if ($DB->num_rows()) {
                while ($r = $DB->fetch(true)) {
                    $tpl->setCurrentBlock("serv-item");
                    $tpl->setVariable("SERV_NAME", $r["service_name"]);
                    $tpl->setVariable("SERV_STATE", $r["for_sale"] > 0 ? "Продажа возможна" : "Услуга не предоставляется");
                    $tpl->setVariable("SERV_ID", $r["id"]);
                    $tpl->setVariable("SERV_CONTR_ID", $contr_id);
                    $tpl->parse("serv-item");
                }
            }


        } # if ($result["contr_type_tm"] > 0)
        UIHeader($tpl);
        $tpl->show();

    }

    function enable_service()
    {
        global $DB, $USER;
        $action = $_REQUEST['action'];
        $contr_id = intval($_REQUEST['contr_id']);
        $serv_id = intval($_REQUEST['serv_id']);
        if ($action == 'on') {
            $sql = "SELECT count(*) cnt FROM link_contragent_services WHERE contragent_id = " . $contr_id . " AND service_id = " . $serv_id;
            $DB->query($sql);
            $has_service = 0;
            while ($r = $DB->fetch(true)) {
                if ($r['cnt'] > 0) {
                    $has_service = 1;
                }
            }
            if ($has_service == 0) {
                $sql = "INSERT INTO link_contragent_services (contragent_id, service_id) VALUES (" . $contr_id . "," . $serv_id . ")";
                $DB->query($sql);
            }
        }
        if ($action == 'off') {
            $sql = "DELETE FROM link_contragent_services WHERE contragent_id = " . $contr_id . " AND service_id = " . $serv_id;
            $DB->query($sql);
        }
        $text = "<script>alert('Данные обновлены'); document.location.href='/kontr/view?contr_id=" . $contr_id . "';</script>\n";
        echo $text;
        exit;
    }

    function edit()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        $bn = array(
            0 => "Любой",
            1 => "Наличный расчет",
            2 => "Безналичный расчет"
        );
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        $tpl->setVariable("CONTR_PAYTYPE", array2options($bn, $result["bn"]));
        $tpl->setVariable("CONTR_PROPTYPE_LIST", kontr_proptypes_plugin::getOptions($result["proptype_id"]));
        if ($contr_id = $_REQUEST['contr_id']) {
            $sql = "SELECT * FROM `list_contr` WHERE `id`=" . $DB->F($contr_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Контрагент с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);

            $tpl->setVariable("CONTR_ID", $result["id"]);
            $tpl->setVariable("CONTR_TITLE", $result["contr_title"]);
            $tpl->setVariable("CONTR_DESC", $result["contr_desc"]);
            $tpl->setVariable("CONTR_CONTACT", $result["contact"]);
            $tpl->setVariable("CONTR_OFF_TITLE", $result["official_title"]);
            $tpl->setVariable("CONTR_PROPTYPE_LIST", kontr_proptypes_plugin::getOptions($result["proptype_id"]));
            $tpl->setVariable("CONTR_INN", $result["inn"]);
            $tpl->setVariable("CONTR_PHONE", $result["phone"]);
            $tpl->setVariable("CONTR_FAX", $result["fax"]);
            $tpl->setVariable("CONTR_EMAIL", $result["email"]);
            $tpl->setVariable("CONTR_SITE", $result["site"]);
            $tpl->setVariable("CONTR_PAYTYPE", array2options($bn, $result["bn"]));
            $tpl->setVariable("IT_FROM", $result["it_from"] ? date("H:i", strtotime($result["it_from"])) : "");
            $tpl->setVariable("IT_TO", $result["it_to"] ? date("H:i", strtotime($result["it_to"])) : "");
            if ($result["contr_type"]) {
                $tpl->setVariable("CONTR_TYPE_CONN", "checked='checked'");
            }
            if ($result["contr_type2"]) {
                $tpl->setVariable("CONTR_TYPE_FIX", "checked='checked'");
            }
            if ($result["contr_type3"]) {
                $tpl->setVariable("CONTR_TYPE_ACC", "checked='checked'");
            }
            if ($result["contr_type4"]) {
                $tpl->setVariable("CONTR_TYPE_GP", "checked='checked'");
            }
            if ($result["contr_type_tm"]) {
                $tpl->setVariable("CONTR_TYPE_TM", "checked='checked'");
            }
            if ($result["is_client"]) {
                $tpl->setVariable("CONTR_TYPE_CLIENT", "checked='checked'");
            }
            if ($result["is_supplier"]) {
                $tpl->setVariable("CONTR_TYPE_SUPPLIER", "checked='checked'");
            }
            $tpl->setVariable("LINKTOCONTR", "view?contr_id=" . $contr_id);
            // подрядчик
            $sql = "SELECT us.id, us.fio FROM `podr` AS podr LEFT JOIN `users` AS us ON podr.user_id=us.id WHERE podr.contr_id=" . $DB->F($contr_id);
            if (list($podr_user_id, $podr_user_fio) = $DB->getRow($sql)) {
                $tpl->setVariable('PODR_CHK', 'checked');
                $tpl->setVariable('PODR_USER_ID', $podr_user_id);
                $tpl->setVariable('PODR_USER_FIO', $podr_user_fio);
            } else {
                $tpl->setVariable('NOPODR_CHK', 'checked');
            }

            $sql = "SELECT st.id, st.name, st.color, (SELECT count(*) FROM `podr_sendstatus` AS ps WHERE ps.contr_id=" . $DB->F($contr_id) . " AND ps.status_id=st.id) AS chk FROM `task_status` AS st WHERE st.plugin_uid='services' ORDER BY `name`";
            $DB->query($sql);
            while ($a = $DB->fetch(true)) {
                $tpl->setCurrentBlock("sendstatus_row");
                $tpl->setVariable('SEND_STATUS_ID', $a['id']);
                $tpl->setVariable('SEND_STATUS_NAME', $a['name']);
                $tpl->setVariable('SEND_STATUS_COLOR', $a['color']);
                if ($a['chk']) $tpl->setVariable('SEND_STATUS_CHK', 'checked');
                $tpl->parse("sendstatus_row");
            }
            $DB->free();

            $sql = "SELECT * FROM `podr_status` WHERE `contr_id`=" . $DB->F($contr_id);
            $DB->query($sql);
            while ($a = $DB->fetch(true)) {
                $tpl->setCurrentBlock("nord_row");
                $tpl->setVariable('STATUS_PODR', $a['podr_status']);
                $tpl->setVariable('STATUS_OPTIONS2', $this->getStatusOptionsStatic('services', $a['status_id']));
                $tpl->parse("nord_row");
            }
            $DB->free();

            //агент
            $sql = "SELECT us.id AS user_id, us.fio AS user_fio, ag.work_type_id, ag.agreement_id FROM `agents` AS ag LEFT JOIN `users` AS us ON ag.user_id=us.id WHERE ag.contr_id=" . $DB->F($contr_id);
            if (list($agent_user_id, $agent_user_fio, $agent_wtype_id, $agent_agr_id) = $DB->getRow($sql)) {
                $tpl->setVariable('AGENT_CHK', 'checked');
                $tpl->setVariable('AGENT_USER_ID', $agent_user_id);
                $tpl->setVariable('AGENT_USER_FIO', $agent_user_fio);
                $tpl->setVariable('AGENT_WTYPE_ID', $agent_wtype_id);
                $tpl->setVariable('AGENT_AGR_ID', $agent_agr_id);
            } else {
                $tpl->setVariable('NOAGENT_CHK', 'checked');
            }
            // агентские

            $sql = "SELECT `agentfee` FROM `list_contr` WHERE `id`=" . $DB->F($contr_id);
            $r = $DB->getField($sql);
            $tpl->setVariable("PODR_FEE", intval($r));
            $sql = "SELECT `contragentfee` FROM `list_contr` WHERE `id`=" . $DB->F($contr_id);
            $r = $DB->getField($sql);
            $tpl->setVariable("AGENT_FEE", intval($r));
        }

        $tpl->setVariable('STATUS_OPTIONS', $this->getStatusOptionsStatic('services'));

        UIHeader($tpl);
        $tpl->show();

    }

    static function deleterecipient_ajax()
    {
        global $DB, $USER;
        if ($USER->checkAccess("kontr", User::ACCESS_WRITE)) {
            $id = $_REQUEST["id"];
            if (!$id) $ret["error"] = "Не указан идентификатор контакта";
            $sql = "DELETE FROM `list_contr_contacts` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->errno()) $ret["error"] = $DB->error(); else $ret["ok"] = "ok";
        } else {
            $ret["error"] = "В доступе отказано";
        }
        echo json_encode($ret);
    }

    static function createrecipient_ajax()
    {
        global $DB, $USER;
        if ($USER->checkAccess("kontr", User::ACCESS_WRITE)) {
            $id = $_REQUEST["contact_id"];
            $name = $_REQUEST["name"];
            $pos = $_REQUEST["pos"];
            $phone = $_REQUEST["phone"];
            $email = $_REQUEST["email"];
            $bdate = date("Y-m-d", strtotime($_REQUEST["bdate"]));
            $contr_id = $_REQUEST["contr_id"];
            if ($name && $phone && $pos && $contr_id) {
                if (!$id) {
                    $sql = "INSERT INTO `list_contr_contacts` (`contr_id`, `fio`, `pos`, `phone`, `email`, `bdate`, `user_id`) 
                    VALUES (" . $DB->F($contr_id) . ", " . $DB->F($name) . ", " . $DB->F($pos) . ", " . $DB->F($phone) . ", " . $DB->F($email) . ", " . $DB->F($bdate) . ", " . $DB->F($USER->getId()) . ")";
                    $DB->query($sql);
                    if ($DB->errno()) $ret["error"] = $DB->error(); else {
                        $id = $DB->insert_id();
                        $ret["tpl"] = "<tr class=\"rec_row row_id$id\"><td><a href=\"#\" class=\"editrecipient\" id=\"$id\">$name</a></td><td class=\"pos$id\">$pos</td><td class=\"phone$id\">$phone</td><td class=\"email$id\">$email</td><td class=\"bdate$id\">$bdate</td><td><a style=\"cursor: pointer;\" class=\"delete_recipient\" id=\"$id\"><img src=\"templates/images/cross.png\" /></a></td></tr>";
                        $ret["ok"] = "ok";
                    }
                } else {
                    $sql = "UPDATE `list_contr_contacts` SET `fio`=" . $DB->F($name) . ", `pos`=" . $DB->F($pos) . ", `phone`=" . $DB->F($phone) . ", `email`=" . $DB->F($email) . ", `bdate`=" . $DB->F($bdate) . ", `user_id`=" . $DB->F($USER->getId()) . " WHERE `id`=" . $DB->F($id) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) $ret["error"] = $DB->error(); else {
                        $ret["tpl"] = "<tr class=\"rec_row row_id$id\"><td><a href=\"#\" class=\"editrecipient\" id=\"$id\">$name</a></td><td class=\"pos$id\">$pos</td><td class=\"phone$id\">$phone</td><td class=\"email$id\">$email</td><td class=\"bdate$id\">$bdate</td><td><a style=\"cursor: pointer;\" class=\"delete_recipient\" id=\"$id\"><img src=\"templates/images/cross.png\" /></a></td></tr>";
                        $ret["ok"] = "ok";
                    }
                }
            } else {

                $ret["error"] = "Недостаточно данных для выполнения операции" . $name . " " . $phone . " " . $pos . " " . $contr_id;
            }
        } else {
            $ret["error"] = "В доступе отказано";
        }
        echo json_encode($ret);
        return false;
    }


    function save(Request $request)
    {
        if (!empty($request->get('contr_id'))) {
            $partner = $this->getEm()->find(ListContr::class, $request->get('contr_id'));
        } else {
            $partner = new ListContr();
        }

        $this->getEm()->transactional(function (EntityManagerInterface $em) use ($partner, $request) {
            $partner
                ->setContragentfee($request->request->get('agent_fee'))
                ->setAgentfee($request->request->get('podr_fee'))
                ->setContrTitle($request->get('contr_title'))
                ->setIsSupplier('on' === $request->get('is_supplier'))
                ->setIsClient('on' === $request->get('is_client'))
                ->setBn($request->get('paytype', 0))
                ->setOfficialTitle($request->get('contr_off_title'))
                ->setPhone($request->get('phone'))
                ->setSite($request->get('site'))
                ->setContrDesc($request->get('contr_desc'))
                ->setContrType('on' === $request->get('conn'))
                ->setContrType2('on' === $request->get('fix'))
                ->setContrType3('on' === $request->get('acc'))
                ->setContrType4('on' === $request->get('gp'))
                ->setContrTypeTm('on' === $request->get('tm'))
                ->setEmail($request->get('email'))
                ->setInn($request->get('inn'))
                ->setFax($request->get('fax'))
                ->setItFrom(
                    $request->get('it_from') ? new DateTime($request->get('it_from')) : null)
                ->setItTo($request->get('it_to') ? new DateTime($request->get('it_to')) : null)
                ->setProptypeId($request->get('prop_type'));

            $em->persist($partner);

            if ($request->request->has('agent_user_id') && $request->request->has('is_agent')) {
                $agreement = $request->get('agent_agr_id') ? $em->getReference(Agreement::class, $request->get('agent_agr_id')) : null;
                $taskType = $request->get('agent_wtype_id') ? $em->getReference(TaskType::class, $request->get('agent_wtype_id')) : null;
                $agentUser = $request->get('agent_user_id') ? $em->getReference(OrmUser::class, $request->get('agent_user_id')) : null;

                if (null === $agent = $em->getRepository(Agent::class)
                        ->findOneBy(['partner' => $partner, 'agreement' => $agreement, 'user' => $agentUser])
                ) {
                    $agent = new Agent();
                }

                $agent
                    ->setAgreement($agreement)
                    ->setPartner($partner)
                    ->setTaskType($taskType)
                    ->setUser($agentUser);

                $em->persist($agent);
            }

            $em->flush();
        });

        return new RedirectResponse($this->getLink('view') . "?contr_id=" . $partner->getId());
    }

    function delete()
    {
        global $DB;


        if (!($id = $_REQUEST['contr_id'])) $err[] = "Не указан Контрагент";
        if (sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST["contr_id"])) UIError("Выбранный Контрагент связан с активными заявками. Удаление невозможно.");
        $DB->query("DELETE FROM `list_contr` WHERE `id`='" . $_REQUEST['contr_id'] . "';");
        redirect($this->getLink(), "Контрагент успешно удален.");

    }

    function saveagreement()
    {
        //print_r($_POST);
        global $DB;
        $contr_id = @$_POST["contr_id"];
        $agr_id = @$_POST["agr_id"];
        $agr_num = @$_POST["agr_num"];
        $agr_date = @$_POST["agr_date"];
        $wtypes = @$_POST["wtypes_id"];
        $comment = @$_POST["agr_comment"];
        $access = @$_POST["access_contact"];
        $type_price = @$_POST["type_price"] == "on" ? "1" : "0";
        $type_smeta = @$_POST["type_smeta"] == "on" ? "1" : "0";
        $type_abon = @$_POST["type_abon"] == "on" ? "1" : "0";
        $tp = @$_POST["tp_contact"];
        //print_r($_REQUEST);
        //die();
        if (!$contr_id || !$agr_date || !$agr_num || !$wtypes) {
            UIError("Для сохранения договора недостаточно данных! Пожалуйста, заполните форму целиком!");
            return;
        }
        $aDate = explode(".", $agr_date);
        if ($agr_id) {
            $sql = "DELETE FROM `link_agr_wtypes` WHERE `agr_id`=" . $DB->F($agr_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error() . $sql);
            //$sql = "DELETE FROM `list_agr_price` WHERE `agr_id`=".$DB->F($agr_id).";";
            //$DB->query($sql);
            //if ($DB->errno()) UIError($DB->error().$sql);
            $sql = "UPDATE `list_contr_agr` SET `number`=" . $DB->F($agr_num) . ", 
                    `agr_date`=" . $DB->F($aDate[2] . "-" . $aDate[1] . "-" . $aDate[0]) . ", 
                    `coment`=" . $DB->F($comment) . ", `access_contact`=" . $DB->F($access) . ", 
                    `tp_contact`=" . $DB->F($tp) . ", `type_price`=" . $DB->F($type_price) . ", 
                    `type_smeta`=" . $DB->F($type_smeta) . ", `type_abon`=" . $DB->F($type_abon) . " WHERE `id`=" . $DB->F($agr_id) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error() . $sql);

        } else {
            $sql = "INSERT INTO `list_contr_agr` (`number`, `agr_date`, `contr_id`, `coment`, `access_contact`, `tp_contact`, `type_price`, `type_smeta`, `type_abon`) 
                    VALUES (" . $DB->F($agr_num) . ", " . $DB->F($aDate[2] . "-" . $aDate[1] . "-" . $aDate[0]) . ", " . $DB->F($contr_id) . ", " . $DB->F($comment) . ", " . $DB->F($access) . ",
                    " . $DB->F($tp) . ", " . $DB->F($type_price) . ", " . $DB->F($type_smeta) . ", " . $DB->F($type_abon) . ");";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            $agr_id = $DB->insert_id();
        }
        $sql = "INSERT IGNORE INTO `link_agr_wtypes` (`agr_id`, `wtype_id`) VALUES ";
        $wt = explode(",", $wtypes);
        $sql_add = false;
        foreach ($wt as $item) {
            $sql_add[] = "(" . $DB->F($agr_id) . ", " . $DB->F($item) . ")";
        }
        $sql = $sql . implode(",", $sql_add) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        redirect($this->getLink('viewagreement') . "?contr_id=" . $contr_id . "&id=" . $agr_id, "Договор успешно сохранен. ID: " . $agr_id);

    }

    function getAgrById($id)
    {
        if (!$id)
            return false;
        $sql = "SELECT * FROM `list_contr_agr` WHERE `id`=" . wf::$db->F($id) . ";";
        return wf::$db->getRow($sql, true);
    }

    static function getAgrByIdT($id)
    {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT concat(`number`, ' от ' ,`agr_date`) FROM `list_contr_agr` WHERE `id`=" . $DB->F($id) . ";";
        return $DB->getField($sql, true);
    }


    function getAgrListByContrID($id)
    {
        global $DB;
        if (!$id) return false;
        $result = $DB->getCell2("SELECT `number`, `agr_date` FROM `list_contr_agr` WHERE `contr_id`=" . $DB->F($id) . ";");
        return $result;
    }

    function getAgrListByContrIDA($id)
    {
        global $DB;
        if (!$id) return false;
        $result = $DB->getCell2("SELECT `id`, CONCAT(`number`, ' от ', `agr_date`) AS agr FROM `list_contr_agr` WHERE `contr_id`=" . $DB->F($id) . ";");
        return $result;
    }

    function viewagreement()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("viewagr.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/viewagr.tmpl.htm");
        else
            $tpl->loadTemplatefile("viewagr.tmpl.htm");

        $cType = false;
        if ($contr_id = $_REQUEST['contr_id']) {
            $sql = "SELECT `contr_title`, `official_title`, `contr_type`, `contr_type2` FROM `list_contr` WHERE `id`=" . $DB->F($contr_id) . ";";
            $r = $DB->getRow($sql);
            if ($r) {
                $tpl->setVariable("AGR_CONTR_TITLE", $r[1] . " (" . $r[0] . ")");
                $tpl->setVariable("CONTR_ID", $contr_id);

            } else {
                UIError("Ошибка определения Контрагента для добавления договора!");
            }
            if ($r[2] == 0 && $r[3] == 0) {
                UIError("Не определен тип Контрагента!!  Выберите тип Контрагента в его редакторе, а затем добавляйте договор!");
            }

            if ($r[2] == 1 && $r[3] == 1) {
                $cType = 1;
            }
            if ($r[2] == 0 && $r[3] == 1) {
                $cType = 3;
            }
            if ($r[2] == 1 && $r[3] == 0) {
                $cType = 2;
            }

            $tpl->setVariable("CTYPE", $cType);
            if ($agr_id = $_REQUEST['id']) {
                $tpl->setVariable("AGR_ID", $agr_id);
                $cAgr = $this->getAgrById($agr_id);
                if ($cAgr) {
                    $tpl->setVariable("AGR_NUM", $cAgr["number"]);
                    $rdate = explode("-", $cAgr["agr_date"]);
                    $tpl->setVariable("AGR_DATE", $rdate[2] . "." . $rdate[1] . "." . $rdate[0]);
                    $tpl->setVariable("AGR_COMMENT", $cAgr["coment"] ? $cAgr["coment"] : "&mdash;");
                    $sql = "SELECT tt.id, tt.title FROM `link_agr_wtypes` AS lwt LEFT JOIN `task_types` AS tt ON lwt.wtype_id=tt.id WHERE lwt.agr_id=" . $DB->F($agr_id) . ";";
                    $res = $DB->getCell2($sql);
                    $ids = false;
                    $titles = false;
                    foreach ($res as $key => $val) {
                        $ids[] = $key;
                        $titles[] = $val;
                    }
                    $idsStr = implode(", ", $ids);
                    $titlesStr = implode(", ", $titles);
                    $tpl->setVariable("AGR_WTYPE_IDS", $idsStr);
                    $tpl->setVariable("AGR_WTYPE_VALS", $titlesStr);
                    $tpl->setVariable("AGR_ACCESS", $cAgr["access_contact"]);
                    $tpl->setVariable("AGR_TP", $cAgr["tp_contact"]);
                    $type = false;
                    if ($cAgr['type_price'])
                        $type[] = "Прайс";
                    if ($cAgr['type_smeta'])
                        $type[] = "Смета";
                    if ($cAgr['type_abon'])
                        $type[] = "Абонентская плата";
                    if ($type) {
                        $tpl->setVariable("AGR_TYPE", implode("<br />", $type));
                    } else {
                        $tpl->setVariable("AGR_TYPE", "<strong>&mdash;</strong>");
                    }

                    $sql = "SELECT * FROM `list_agr_price` WHERE `agr_id`=" . $cAgr['id'] . " ORDER BY `title`;";
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error());
                    if ($DB->num_rows()) {
                        while (list($id, $agr_id, $title, $stitle, $price_type, $price_val, $p_comment) = $DB->fetch()) {
                            $tpl->setCurrentBlock("row");
                            $tpl->setVariable("JT_ID", $id);
                            $tpl->setVariable("JT_TITLE", $title);
                            $tpl->setVariable("JT_SHORT_TITLE", $stitle);
                            $tpl->setVariable("JT_PRICETYPE", $price_type);
                            $tpl->setVariable("JT_TAG", $price_type == "smeta" ? "Смета" : "Прайс");
                            $tpl->setVariable("JT_PRICE", $price_type != "price" ? "&mdash;" : $price_val);
                            $tpl->setVariable("JT_COMMENT", $p_comment);
                            $tpl->parse("row");
                        }
                    } else {
                        $tpl->touchBlock("no-rec");
                    }


                } else {
                    UIError("Договор с указанным ID не найден!");
                }
            } else {

            }
            $tpl->setVariable("AGRLINKTOCONTR", "view?contr_id=" . $contr_id);

        } else {
            UIError("Не указан ID контрагента для добавления договора!");
        }

        UIHeader($tpl);
        $tpl->show();


    }

    function editagreement()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("editagr.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/editagr.tmpl.htm");
        else
            $tpl->loadTemplatefile("editagr.tmpl.htm");


        $cType = false;
        if ($contr_id = $_REQUEST['contr_id']) {
            $sql = "SELECT `contr_title`, `official_title`, `contr_type`, `contr_type2`, `is_client`, `is_supplier`, `contr_type3`, `contr_type4` FROM `list_contr` WHERE `id`=" . $DB->F($contr_id) . ";";
            $r = $DB->getRow($sql);
            if ($r) {
                $tpl->setVariable("AGR_CONTR_TITLE", $r[1] . " (" . $r[0] . ")");
                $tpl->setVariable("CONTR_ID", $contr_id);
                if ($r['is_client'])
                    $tpl->setVariable('CONTR_TYPE_CLIENT', "checked='checked'");
                if ($r['is_supplier'])
                    $tpl->setVariable('CONTR_TYPE_SUPPLIER', "checked='checked'");
            } else {
                UIError("Ошибка определения Контрагента для добавления договора!");
            }
            if ($r[2] == 0 && $r[3] == 0 && !$r[6] == 0 && !$r[7] == 0) {
                UIError("Не определен тип Контрагента!!  Выберите тип Контрагента в его редакторе, а затем добавляйте договор!");
            }

            if ($r[2] == 1 && $r[3] == 1) {
                $cType = 1;
            }
            if ($r[2] == 0 && $r[3] == 1) {
                $cType = 3;
            }
            if ($r[2] == 1 && $r[3] == 0) {
                $cType = 2;
            }
            $tpl->setVariable("CTYPE", $cType);
            if ($agr_id = $_REQUEST['id']) {
                $tpl->setVariable("AGR_ID", $agr_id);
                $cAgr = $this->getAgrById($agr_id);
                if ($cAgr) {
                    $tpl->setVariable("AGR_NUM", $cAgr["number"]);
                    $rdate = explode("-", $cAgr["agr_date"]);
                    $tpl->setVariable("AGR_DATE", $rdate[2] . "." . $rdate[1] . "." . $rdate[0]);
                    $tpl->setVariable("AGR_COMMENT", $cAgr["coment"]);
                    $sql = "SELECT tt.id, tt.title FROM `link_agr_wtypes` AS lwt LEFT JOIN `task_types` AS tt ON lwt.wtype_id=tt.id WHERE lwt.agr_id=" . $DB->F($agr_id) . ";";
                    $res = $DB->getCell2($sql);
                    $ids = false;
                    $titles = false;
                    foreach ($res as $key => $val) {
                        $ids[] = $key;
                        $titles[] = $val;
                    }
                    $idsStr = implode(", ", $ids);
                    $titlesStr = implode(", ", $titles);
                    $tpl->setVariable("AGR_WTYPE_IDS", $idsStr);
                    $tpl->setVariable("AGR_WTYPE_VALS", $titlesStr);
                    $tpl->setVariable("AGR_ACCESS", $cAgr["access_contact"]);
                    $tpl->setVariable("AGR_TP", $cAgr["tp_contact"]);
                    if ($cAgr['type_price'])
                        $tpl->setVariable("TYPE_PRICE", "checked='checked'");
                    if ($cAgr['type_smeta'])
                        $tpl->setVariable("TYPE_SMETA", "checked='checked'");
                    if ($cAgr['type_abon'])
                        $tpl->setVariable("TYPE_ABON", "checked='checked'");

                } else {
                    UIError("Договор с указанным ID не найден!");
                }
            } else {

            }
            $tpl->setVariable("AGRLINKTOCONTR", "view?contr_id=" . $contr_id);

        } else {
            UIError("Не указан ID контрагента для добавления договора!");
        }

        UIHeader($tpl);
        $tpl->show();
    }

    function deleteagreement()
    {
        global $DB;

        $agr_id = $_REQUEST["agr_id"];
        $contr_id = $_REQUEST["contr_id"];
        $sql = "SELECT COUNT(`task_id`) FROM `tickets` WHERE `agr_id`=" . $DB->F($agr_id) . ";";
        $fCnt = $DB->getField($sql);
        if (!$fCnt) {
            if ($agr_id && $contr_id) {
                $sql = "DELETE FROM `link_agr_wtypes` WHERE `agr_id`=" . $DB->F($agr_id) . ";";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error() . $sql);
                $sql = "DELETE FROM `list_contr_agr` WHERE `id`=" . $DB->F($agr_id) . ";";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error() . $sql);
                redirect($this->getLink('view') . "?contr_id=" . $contr_id, "Договор успешно удален. ID: " . $agr_id);
            } else {
                UIError("Не указан ID договора для удаления!");

            }
        } else {
            UIError("Договор привязан к действующим заявкам. Удаление невозможно.");
        }

    }

    function getagrlist(Request $request)
    {
        $qb = $this->getEm()->createQueryBuilder()
            ->select('agreement')
            ->from(Agreement::class, 'agreement')
            ->join('agreement.workTypes', 'linkWorks')
            ->join('linkWorks.taskType', 'taskType')
            ->andWhere('agreement.partner = :partner')
            ->andWhere('agreement.status = :status')
            ->setParameter('partner', $this->getEm()->getReference(ListContr::class, $request->get('contr_id')))
            ->setParameter('status', $this->getEm()->getRepository(\models\TaskStatus::class)->findByTag('inaction',
                \models\Task::AGREEMENT))
        ;

        if ($request->request->has('wtype_id') && !empty($request->get('wtype_id'))) {
            $qb->andWhere('taskType in (:works)')
                ->setParameter('works', [$this->getEm()->getReference(TaskType::class, $request->get('wtype_id'))]);
        }
        /** @var Agreement[] $agreements */
        $agreements = $qb->getQuery()->getResult();

        if (empty($agreements)) {
            return new Response('<option value="">-- нет договоров --</option>');
        }

        /**
         * Shawarma hey
         */
        $selectboxContent = '';
        foreach ($agreements as $agreement) {
            $selectboxContent .= sprintf('<option value="%d"%s>%s</option>',
                $agreement->getId(),
                $agreement->getId() === $request->request->getInt('sel_id') ? ' selected selected="selected"' : '',
                $agreement
            );
        }

        return new Response($selectboxContent);
    }

    function getagrlistA()
    {
        global $DB;
        $contr_id = $_POST['contr_id'];
        $filter_by_plugin_uid = '';
        if (isset($_POST['plugin_uid'])) {
            $sql = "SELECT group_concat(id) FROM task_types t WHERE plugin_uid = " . $DB->F($_POST['plugin_uid']) . ";";
            $ids = $DB->getCell($sql);
            if (count($ids) > 0) {
                $filter_by_plugin_uid = " and exists (select * from link_agr_wtypes law where law.agr_id = lca.id and law.wtype_id in (" . $ids[0] . "))";
            }
        }
        if ($contr_id === FALSE) {
            echo "error";
            return;
        }
        $sql = "SELECT `id`, CONCAT(`number`, ' от ', `agr_date`) as agr FROM `list_contr_agr` lca WHERE `status_id`=84 $filter_by_plugin_uid AND `contr_id`=" . $DB->F($contr_id) . ";";

        if (array2options($DB->getCell2($sql))) echo array2options($DB->getCell2($sql)); else echo "<option value=\"\">-- нет договоров -- </option>";
    }

    function getagrlistJA()
    {
        global $DB;
        $contr_id = $_POST['contr_id'];
        $sel_item = $_POST["selitem"];
        if ($contr_id === FALSE) {
            $ret["error"] = "Не указан контрагент";
            echo json_encode($ret);
            return false;
        }
        $sql = "SELECT `id`, CONCAT(`number`, ' от ', `agr_date`) AS agr FROM `list_contr_agr` WHERE `status_id`=84 AND `contr_id`=" . $DB->F($contr_id) . ";";


        if (array2options($DB->getCell2($sql)))
            $ret['tpl'] = array2options($DB->getCell2($sql), intval($_POST["selitem"]));
        else
            $ret['tpl'] = "<option value=\"\">-- нет договоров --</option>";
        $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;
    }

    public function getwtypelist(Request $request)
    {
        global $DB;
        $contr_id = $request->get('contr_id');
        $type = $request->get('type', []);
        $allowAny = $request->get('allow_any', false);

        if (!is_array($type))
            $type = [$type];

        if (empty($contr_id) || empty($type)) {
            echo "error";
            return;
        }

        $sql = "SELECT 
                    wt.id, 
                    wt.title 
                FROM 
                    `list_contr_agr` AS ca 
                    LEFT JOIN 
                    `link_agr_wtypes` AS law ON law.agr_id=ca.id 
                    LEFT JOIN 
                    `task_types` AS wt ON wt.id=law.wtype_id 
                WHERE 
                    ca.status_id=84 
                    AND ca.contr_id=" . $DB->F($contr_id) . " 
                    AND wt.title IS NOT NULL 
                    AND wt.plugin_uid IN (" . implode(', ', array_map(function ($e) {
                return "'" . $e . "'";
            }, $type)) . ") 
                ORDER BY 
                    wt.title
                ;";

        if (array2options($DB->getCell2($sql))) {
            echo ($allowAny ? '<option value="">-- Любой из договора --</option>' : '') . array2options($DB->getCell2($sql), $_POST['sel_id']);
        } else {
            echo "<option value=\"\">-- нет видов работ --</option>";
        }

    }

    function jobtype_edit()
    {
        global $DB, $USER, $PLUGINS;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/editor.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        if ($jt_id = $_REQUEST["jt_id"]) {
            $rec = $DB->getRow("SELECT * FROM `list_agr_price` WHERE `id`=" . $DB->F($jt_id) . ";", true);
            if ($rec) {
                $tpl->setVariable("ID", $rec["id"]);
                $tpl->setVariable("TITLE", $rec["title"]);
                $tpl->setVariable("SHORT_TITLE", $rec["short_title"]);
                if ($rec["price_type"] == "price")
                    $tpl->setVariable("PRICE_TYPE_PRICE", "selected=\"selected\"");
                else
                    $tpl->setVariable("PRICE_TYPE_SMETA", "selected=\"selected\"");
                $tpl->setVariable("PRICE_VAL", $rec["price"] . ($rec["price_type"] != "price" ? "%" : ""));
                $tpl->setVariable("COMMENT", $rec["p_comment"]);
            } else {
                UIError("Запись с указанным идентификатором отсутствует!");
            }
        }
        UIHeader($tpl);
        $tpl->show();
    }

    function jobtype_save(Request $request)
    {
        global $DB, $USER;
        $err = array();

        if ( $_POST["title"] && $_POST["short_title"] && $_POST["price"] != '' ) {
            if ($jt_id = $_POST["jt_id"]) {
                $sql = "UPDATE 
                    `list_agr_price` 
                SET 
                    `archive`=" . $DB->F($_POST["archive"] != "" ? 1 : 0) . ", 
                    `title`=" . $DB->F($_POST["title"]) . ", 
                    `s_title`=" . $DB->F($_POST["short_title"]) . ", 
                    `price`=" . $DB->F( $_POST["price_type"] === 'smeta' ? 100 : floatval($_POST["price"])) . ", 
                    `price_type`=" . $DB->F($_POST["price_type"]) . ", 
                    `p_comment`=" . $DB->F($_POST["comment"]) . " 
                WHERE 
                    `id`=" . $DB->F($jt_id)
                ;
            } else {
                $sql = "INSERT INTO 
                    `list_agr_price` 
                (
                    `agr_id`, 
                    `title`, 
                    `s_title`, 
                    `price_type`, 
                    `price`, 
                    `p_comment`, 
                    `archive`
                ) 
                VALUES 
                (
                    " . $DB->F($_POST["agr_id"]) . ",
                    " . $DB->F($_POST["title"]) . ",
                    " . $DB->F($_POST["short_title"]) . ",
                    " . $DB->F($_POST["price_type"]) . ",
                    " . $DB->F(floatval($_POST["price"])) . ",
                    " . $DB->F($_POST["comment"]) . ",
                    " . $DB->F($_POST["archive"] != "" ? 1 : 0) . "
                )";
            }
            $DB->query($sql);
            if (!$jt_id) {
                $jt_id = $DB->insert_id();
            }
            if ($DB->errno()) {
                echo "error";
                return;
            }
            if (!$_POST["jt_id"]) {

                ?>


                <tr>
                    <input type="hidden" name="jt_id<?php echo $jt_id; ?>" value="<?php echo $jt_id; ?>"/>
                    <input type="hidden" name="price_type<?php echo $jt_id; ?>"
                           value="<?php echo $_POST["price_type"]; ?>"/>
                    <td class="id<?php echo $jt_id; ?>"><a href="#"
                                                           onclick="openEditor('<?php echo $jt_id; ?>', $(this)); return false;"><?php echo $jt_id; ?></a>
                    </td>
                    <td align="center"><?php echo ($_POST["archive"] != "") ? "Да" : ""; ?></td>
                    <td class="title<?php echo $jt_id; ?>"><a href="#"
                                                              onclick="openEditor('<?php echo $jt_id; ?>', $(this)); return false;"><?php echo $_POST["title"]; ?></a>
                    </td>
                    <td class="hidden-xs s_title<?php echo $jt_id; ?>"><?php echo $_POST["short_title"]; ?></td>
                    <td class="hidden-xs tag<?php echo $jt_id; ?>">
                        <center><?php echo $_POST["price_type"] == "price" ? "Прайс" : "Смета"; ?></center>
                    </td>
                    <td class="price<?php echo $jt_id; ?>"
                        align="center"><?php echo $_POST["price_type"] == "price" ? $_POST["price"] : "&mdash;"; ?></td>
                    <td class="hidden-xs comment<?php echo $jt_id; ?>"><?php echo $_POST["comment"]; ?></td>
                    <td class="hidden-xs">
                        <a href="#" onclick="openEditor('<?php echo $jt_id; ?>', $(this)); return false;"
                           title="Редактировать вид работ"><img
                                src="/templates/images/pencil.png"/></a>
                    </td>
                    <td class="hidden-xs">
                        <a href="#"
                           onclick="if (!confirm('Удаление элемента прайс-листа необратимо. Продолжить?')) { return false; } else {deleteItem('<?php echo $jt_id; ?>'); return false;}"
                           title="Удалить вид работ" class="del"><img src="/templates/images/cross.png"/></a>
                    </td>
                </tr>
                <?php
            } else {
                echo "ok";
                return;
            }
        } else {
            echo "error";
            return;
        }
    }

    function jobtype_del()
    {
        global $DB, $USER;
        if ($jt_id = $_POST["item_id"]) {
            if (!$this->is_busy($jt_id)) {
                $sql = "DELETE FROM `list_agr_price` WHERE `id`=" . $DB->F($jt_id) . ";";
                $DB->query($sql);
                if (!$DB->errno())
                    echo "ok";
                else
                    echo "error";
            } else {
                echo "busy";
            }
        } else {
            echo "error";
        }

    }

    function is_busy($id)
    {
        global $DB;

        $sql = "SELECT COUNT(`work_id`) FROM `list_contr_calc` WHERE `work_id`=" . $DB->F($id) . ";";

        $res = $DB->getField($sql) ? $DB->getField($sql) : false;

        return $res;
    }

    function getPItemsByType($type, $agr_id)
    {
        global $DB;
        if (!$type || !$agr_id) return false;
        $sql = "SELECT * FROM `list_agr_price` WHERE `agr_id`=" . $DB->F($agr_id) . " AND `price_type`=" . $DB->F($type) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;
        } else {
            while ($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }

    function getPItemsByType1($type, $agr_id)
    {
        global $DB;
        if (!$type || !$agr_id) return false;
        $sql = "SELECT * FROM `list_agr_price` WHERE `agr_id`=" . $DB->F($agr_id) . " AND `price_type`=" . $DB->F($type) . " AND `archive`<1;";
        $DB->query($sql);
        if ($DB->errno())
            UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;
        } else {
            while ($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }

    static function getPItemsById($id)
    {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT * FROM `list_agr_price` WHERE `id`=" . $DB->F($id) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;
        } else {
            $ret = $DB->fetch(true);
        }
        $DB->free();
        return $ret;
    }

    static function getPItemsByAgr($agr_id)
    {
        global $DB;
        if (!$agr_id) return false;
        $sql = "SELECT * FROM `list_agr_price` WHERE `agr_id`=" . $DB->F($agr_id) . ";";
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$DB->num_rows()) {
            $ret = false;
        } else {
            while ($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }

    static function getPItemsByAgrAndTaskFilter($agr_id, $sql)
    {
        global $DB;
        if (!$agr_id) return false;
        $sql = "SELECT lp.* FROM `list_agr_price` AS lp WHERE (lp.agr_id=" . $DB->F($agr_id) . " AND lp.id IN (SELECT lcc.work_id FROM `list_contr_calc` AS lcc WHERE lcc.task_id IN (" . $sql . "))) GROUP BY lp.id;";

        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error() . " " . $sql);
        if (!$DB->num_rows()) {
            $ret = false;
        } else {
            while ($res = $DB->fetch(true)) {
                $ret[] = $res;
            }
        }
        $DB->free();
        return $ret;
    }

    function saveplanitem_ajax()
    {
        global $DB, $USER;
        if ($_REQUEST["plan_item_id"]) {
            $ret["replace"] = "ok";
            $sql = "UPDATE `kontr_bonus_plan` SET `convval`=" . $DB->F(str_replace("%", "", $_REQUEST["convrate"])) . ", `midcheck`=" . $DB->F(str_replace("%", "", $_REQUEST["avgcheck"])) . ", `bonusval`=" . $DB->F(str_replace("%", "", $_REQUEST["bonus"])) . ", `user_id`=" . $DB->F($USER->getId()) . " WHERE `id`=" . $DB->F($_REQUEST["plan_item_id"]) . ";";
        } else {
            $sql = "INSERT INTO `kontr_bonus_plan` (`kontr_id`, `convval`, `midcheck`, `bonusval`, `user_id`) VALUES (" . $DB->F($_REQUEST["agr_id"]) . ", " . $DB->F(str_replace("%", "", $_REQUEST["convrate"])) . ", " . $DB->F(str_replace("%", "", $_REQUEST["avgcheck"])) . ", " . $DB->F(str_replace("%", "", $_REQUEST["bonus"])) . ", " . $DB->F($USER->getId()) . ");";
        }
        $DB->query($sql);
        $ret["sql"] = $sql;
        if ($DB->errno()) $ret["error"] = $DB->error();
        if ($ret["error"]) {
            echo json_encode($ret);
            return false;
        }
        $cId = $_REQUEST["plan_item_id"] ? $_REQUEST["plan_item_id"] : $DB->insert_id();
        $DB->free();
        $ret["tpl"] = "<tr class=\"planrec\" id=\"" . $cId . "\">";
        $ret["tpl"] .= "<td align='center'><a href=\"#\" class=\"editplan\" id=\"" . $cId . "\" >" . $_REQUEST["convrate"] . "%</a></td>";
        $ret["tpl"] .= "<td align='right'><a href=\"#\" class=\"editplan\" id=\"" . $cId . "\" >" . $_REQUEST["avgcheck"] . "</a></td>";
        $ret["tpl"] .= "<td align='right'>" . $_REQUEST["bonus"] . "%</td>";
        $ret["tpl"] .= "<td><a href=\"#\" class=\"editplan\" id=\"" . $cId . "\" ><img border=\"0\" src=\"templates/images/pencil.png\" /></a></td>";
        $ret["tpl"] .= "<td><a href=\"#\" id=\"" . $cId . "\"  class=\"removeplan\"><img border=\"0\" src=\"templates/images/cross.png\" /></a></td>";
        $ret["tpl"] .= "</tr>";
        $ret["ok"] = "ok";
        echo json_encode($ret);
        return false;
    }

    function getplanitem_ajax()
    {
        global $DB;
        $id = $_REQUEST["id"];
        $sql = "SELECT * FROM `kontr_bonus_plan` WHERE `id`=" . $DB->F($id) . ";";
        $DB->query($sql);
        if ($DB->num_rows()) {
            $r = $DB->fetch(true);
            $ret["agr_id"] = $r["agr_id"];
            $ret["convval"] = $r["convval"];
            $ret["midcheck"] = $r["midcheck"];
            $ret["bonusval"] = $r["bonusval"];
            $ret["id"] = $r["id"];
            $ret["ok"] = "ok";
            $DB->free();
        } else {
            $ret["error"] = "Нет такой записи";
        }
        echo json_encode($ret);
        return false;
    }

    function deleteplanitem_ajax()
    {
        global $DB;
        $sql = "DELETE FROM `kontr_bonus_plan` WHERE `id`=" . $DB->F($_REQUEST["id"]) . ";";
        $DB->query($sql);
        if ($DB->errno()) $ret["error"] = $DB->error();
        else $ret["ok"] = "ok";
        $DB->free();
        echo json_encode($ret);
        return false;


    }

    static function getAgrPlan($id)
    {
        global $DB;
        $result = false;
        if (!$id) return false;
        $sql = "SELECT * FROM `kontr_bonus_plan` WHERE `kontr_id`=" . $DB->F($id) . ";";
        $r = $DB->query($sql);
        if ($DB->num_rows()) {
            while ($r = $DB->fetch(true)) {
                $result[] = $r;
            }
        }
        $DB->free();
        return $result;
    }

    static function isPodr($id)
    {
        global $DB;
        if (!$id) return false;

        $sql = "SELECT * FROM `podr` WHERE `contr_id`=" . $DB->F($id) . ";";
        $DB->query($sql);
        if ($DB->num_rows()) {
            $DB->free();
            return true;
        } else {
            $DB->free();
            return false;
        }
    }


    static function getPodrPercent($id)
    {
        global $DB;
        if (!$id) return false;

        $sql = "SELECT `agentfee` FROM `list_contr` WHERE `id`=" . $DB->F($id) . ";";
        return intval($DB->getField($sql));

    }

    static function getAgentPercent($id)
    {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT `contragentfee` FROM `list_contr` WHERE `id`=" . $DB->F($id) . ";";
        return intval($DB->getField($sql));
    }

    static function isClient($id)
    {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT `is_client` FROM `list_contr` WHERE `id`=" . $DB->F($id) . ";";
        return (bool)$DB->getField($sql);
    }

    static function isSupplier($id)
    {
        global $DB;
        if (!$id) return false;
        $sql = "SELECT `is_supplier` FROM `list_contr` WHERE `id`=" . $DB->F($id) . ";";
        return (bool)$DB->getField($sql);
    }

    static function getSupplList($id = false)
    {
        global $DB;
        $sql = "SELECT `id`, `contr_title` FROM `list_contr` WHERE `is_supplier` ORDER BY `contr_title`;";
        return array2options($DB->getCell2($sql), $id);
    }

    static function getClientList($id = false)
    {
        global $DB;

        $sql = "SELECT `id`, `contr_title` FROM `list_contr` WHERE `is_client` ORDER BY `contr_title`;";
        return array2options($DB->getCell2($sql), $id);
    }

    static function update_isClient($id, $newvalue)
    {
        global $DB;
        if (!$id) return false;
        $sql = "UPDATE `list_contr` SET `is_client`=" . $DB->F(intval($newvalue)) . " WHERE `id`=" . $DB->F($id) . ";";
        $DB->query($sql);
        $result = $DB->error();
        $DB->free();
        return $result ? false : true;
    }

    static function update_isSupplier($id, $newvalue)
    {
        global $DB;
        if (!$id) return false;
        $sql = "UPDATE `list_contr` SET `is_supplier`=" . $DB->F(intval($newvalue)) . " WHERE `id`=" . $DB->F($id) . ";";
        $DB->query($sql);
        $result = $DB->error();
        $DB->free();
        return $result ? false : true;
    }


}

?>
