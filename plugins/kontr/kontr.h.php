<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Контрагенты";
$PLUGINS[$plugin_uid]['hidden'] = false;
$PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник Контрагентов";
$PLUGINS[$plugin_uid]['events']['getwtypelist'] = "Просмотр списка видов работ";
$PLUGINS[$plugin_uid]['events']['getagrlist'] = "Просмотр списка договоров";

if(wf::$user->checkAccess($plugin_uid)) { 
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Справочник Контрагентов";
    $PLUGINS[$plugin_uid]['events']['getagrlistA'] = "Получить список договоров с контрагентом";
    $PLUGINS[$plugin_uid]['events']['getagrlistJA'] = "Получить список договоров с контрагентом";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['edit'] = "Редактирование Контрагента";
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удаление Контрагента";
    $PLUGINS[$plugin_uid]['events']['save'] = "Сохранение Контрагента";
    $PLUGINS[$plugin_uid]['events']['view'] = "Просмотр Контрагента";    
    $PLUGINS[$plugin_uid]['events']['delete'] = "Удалить Контрагента";
    $PLUGINS[$plugin_uid]['events']['saveagreement'] = "Сохранить Договор";
    $PLUGINS[$plugin_uid]['events']['editagreement'] = "Редактор Договора";
    $PLUGINS[$plugin_uid]['events']['viewagreement'] = "Просмотр Договора";
    $PLUGINS[$plugin_uid]['events']['deleteagreement'] = "Удалить Договор";
    $PLUGINS[$plugin_uid]['events']['jobtype_edit'] = "Редактирование элемента прайса контрагентов (вида работ)";
    $PLUGINS[$plugin_uid]['events']['jobtype_save'] = "Сохранение элемента прайса контрагентов (вида работ)";
    $PLUGINS[$plugin_uid]['events']['jobtype_del'] = "Удаление элемента прайса контрагентов (вида работ)";
    $PLUGINS[$plugin_uid]['events']['saveplanitem_ajax'] = "Сохранение эл-та плана премий";
    $PLUGINS[$plugin_uid]['events']['getplanitem_ajax'] = "Получение эл-та плана премий";
    $PLUGINS[$plugin_uid]['events']['deleteplanitem_ajax'] = "Удаление эл-та плана премий";
    $PLUGINS[$plugin_uid]['events']['getAgrPlan'] = "Получение массива плана расчета премии по договору";
    $PLUGINS[$plugin_uid]['events']['deleterecipient_ajax'] = "Удаление контакта";
    $PLUGINS[$plugin_uid]['events']['createrecipient_ajax'] = "Создание/редактирование контакта";
    $PLUGINS[$plugin_uid]['events']['addfile_ajax'] = "Загрузка файлов";
    $PLUGINS[$plugin_uid]['events']['files_ajax'] = "Список файлов";
    $PLUGINS[$plugin_uid]['events']['delete_file_ajax'] = "Удаление файлов";
    $PLUGINS[$plugin_uid]['events']['getfile'] = "Удаление файлов";
    /// EF, доступы на матрицу контрагента
    $PLUGINS[$plugin_uid]['events']['add_internet_plan'] = "Редактирование тарифов Интернет";
    $PLUGINS[$plugin_uid]['events']['add_ctv_plan'] = "Редактирование тарифов Цифрового ТВ";
    $PLUGINS[$plugin_uid]['events']['add_ictv_plan'] = "Редактирование тарифов Интернет + Цифрового ТВ";
    $PLUGINS[$plugin_uid]['events']['add_phone_plan'] = "Редактирование тарифов Телефонии";
    $PLUGINS[$plugin_uid]['events']['add_internet_eq'] = "Редактирование оборудования для подключения Интернет";
    $PLUGINS[$plugin_uid]['events']['add_ctv_eq'] = "Редактирование оборудования для подключения Цифрового ТВ";
    $PLUGINS[$plugin_uid]['events']['add_other_eq'] = "Редактирование дополнительного оборудования для подключения";
    $PLUGINS[$plugin_uid]['events']['enable_service'] = "Разрешение/Запрет продаж дополнительных услуг";
}
?>
