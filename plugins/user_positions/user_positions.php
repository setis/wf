<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2015
 */

use classes\HTML_Template_IT;
use classes\Plugin;

class user_positions_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function viewlist()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
        $sql = "SELECT * FROM `user_pos` WHERE 1;";
        $DB->query($sql);
        if ($DB->errno())
            UIError($DB->error());
        if (!$DB->num_rows())
            UIError("Справочник должностей пуст!", "Ошибка", true, "Создать запись", "user_positions/edit");
        $data = $DB->getAll($sql);
        foreach ($data as $row) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $row['id']);
            $tpl->setVariable("VLI_TITLE", $row['title']);
            $tpl->parse("vl_item");
        }
        $DB->free();
        UIHeader($tpl);
        $tpl->show();
    }

    function edit()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");
        if ($id = $_REQUEST['id']) {
            $sql = "SELECT * FROM `user_pos` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Запись с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);
            $tpl->setVariable("ID", $result["id"]);
            $tpl->setVariable("TITLE", $result["title"]);
        }
        UIHeader($tpl);
        $tpl->show();
    }

    function save()
    {
        global $DB, $USER;
        $err = [];
        if (!$_POST['title']) $err[] = "Не заполнено название должности";
        if (sizeof($err)) UIError($err);
        if ($_POST["id"]) {
            $sql = "UPDATE `user_pos` SET `title`=" . $DB->F($_POST['title']) . " WHERE `id`=" . $DB->F($_POST["id"]) . ";";
        } else {
            $sql = "INSERT INTO `user_pos` (`title`, `user_id`) VALUES(" . $DB->F($_POST['title']) . "," . $DB->F($USER->getId()) . ")";
        }
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if (!$_POST['id']) {
            $_POST['id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Запись сохранена. ID: " . $_POST['id']);
    }

    function isBusy($id)
    {
        global $DB;
        $DB->query("SELECT `id` FROM `users` WHERE `pos_id`=" . $DB->F($id) . ";");
        if ($DB->num_rows()) $err = true; else $err = false;
        $DB->free();

        return $err;
    }

    function delete()
    {
        global $DB;
        if (!($id = $_REQUEST['id'])) $err[] = "Не указан идентификатор записи!";
        if (sizeof($err)) UIError($err);
        if ($this->isBusy($id)) UIError("Выбранная запись связана с пользователями. Удаление невозможно.");
        $DB->query("DELETE FROM `user_pos` WHERE `id`='" . $_REQUEST['id'] . "';");
        redirect($this->getLink(), "Запись успешно удалена.");
    }

    static function getOptions($id = 0)
    {
        global $DB;
        $sql = "SELECT id, title FROM `user_pos` WHERE 1 ORDER BY `title`;";
        $res = $DB->getCell2($sql);
        if ($DB->errno()) UIError($DB->error());

        return "<option value=0>-- не указана --</option>" . array2options($res, $id);
    }

    static function getOptionsFilter($id = 0)
    {
        global $DB;
        $sql = "SELECT id, title FROM `user_pos` WHERE 1 ORDER BY `title`;";
        $res = $DB->getCell2($sql);
        $res[0] = "-- все --";
        ksort($res);
        if ($DB->errno()) UIError($DB->error());

        return array2options($res, $id);
    }

    static function getById($id)
    {
        global $DB;
        $sql = "SELECT `title` FROM `user_pos` WHERE `id`=" . $DB->F($id) . ";";
        $res = $DB->getField($sql);
        if ($DB->errno()) UIError($DB->error());

        return $res ? $res : false;
    }


}

?>
