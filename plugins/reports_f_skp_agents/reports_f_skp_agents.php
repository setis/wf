<?php

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use models\TaskStatus as TS;

class reports_f_skp_agents_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function archive() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("archive.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate()."/archive.tmpl.htm");
        else
            $tpl->loadTemplatefile("archive.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        UIHeader($tpl);
        report_left_menu($tpl);
        $sql = "SELECT count(id) FROM `list_f_skpper_reports` WHERE 1 ORDER BY `cr_date` DESC";
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $total = $DB->getField($sql);
        $sql = "SELECT * FROM `list_f_skpper_reports` WHERE 1 ORDER BY `cr_date` DESC LIMIT ".$_REQUEST['filter_start'].",".getcfg('rows_per_page');
        $DB->query($sql);

        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            while ($res = $DB->fetch(true)) {
                $uid = adm_users_plugin::getUser($res["cr_uid"]);
                $tpl->setCurrentBlock("row");
                $tpl->setVariable("RR_ID", $res["id"]);
                $tpl->setVariable("RR_PERIOD", base64_decode($res["period"]));
                $tpl->setVariable("RR_CRDATE", $res["cr_date"]);

                $tpl->setVariable("RR_AUTHOR", $uid["fio"]);
                $tpl->parse("row");
            }
            $DB->free();
        } else {
            $tpl->touchBlock("norows");
        }
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], getcfg('rows_per_page'), intval($total), "reports_f_sc/archive#start-%s"));
        $tpl->show();
    }

    function viewreport() {
        global $DB, $USER;
        if ($id = $_REQUEST["id"]) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile("viewarchiveitem.tmpl.htm");
            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/viewarchiveitem.tmpl.htm");
            else
                $tpl->loadTemplatefile("viewarchiveitem.tmpl.htm");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
            $sql = "SELECT `body` FROM `list_f_skpper_reports` WHERE `id`=".$DB->F($id).";";
            $body = $DB->getField($sql);
            if ($DB->errno()) UIError($DB->error());
            if ($body) {
                $tpl->setVariable("REPORT_HERE", base64_decode($body));
                $tpl->show();
            } else {
                UIError("Ошибка БД!");
            }
        } else {
            UIError("Не указан ижентификатор отчета!");
        }
    }

    function deletereport() {
        global $DB, $USER;
        if ($id = $_REQUEST["id"]) {
            $sql = "DELETE FROM `list_f_skpper_reports` WHERE `id`=".$DB->F($id).";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            redirect($this->getLink('archive'), "Отчет успешно удален из Архива!");
        } else {
            UIError("Не указан ижентификатор отчета!");
        }
    }

    function main() {
        global $DB, $USER;
        $cntr = new kontr_plugin();

        $_REQUEST["skp_agent_datefrom"] = $this->getCookie("skp_agent_datefrom", @$_REQUEST["skp_agent_datefrom"]);
        $_REQUEST["skp_agent_dateto"] = $this->getCookie("skp_agent_dateto", @$_REQUEST["skp_agent_dateto"]);
        $_REQUEST["tech_id"] = $this->getCookie("tech_id", $_REQUEST["tech_id"]);
        $_REQUEST["filter_status_id"] = $this->getCookie('filter_status_id', $_REQUEST["filter_status_id"]);
        $_REQUEST["date_type"] = $this->getCookie("date_type", $_REQUEST["date_type"]);
        $_REQUEST["filter_wtype_id"] = $this->getCookie("filter_wtype_id", $_REQUEST["filter_wtype_id"]);
        $_REQUEST["createreport"] = $this->getCookie("createreport", $_REQUEST["createreport"]);
        $dateParams = array(0 => "Дата создания", 1 => "Дата выполнения", 2=>"Дата изменения", 3=>"Дата принятия ДС");
        $df_arc = @$_REQUEST['skp_agents_datefrom'] ? @$_REQUEST['skp_agents_datefrom'] : date("d.m.Y");
        $dt_arc = @$_REQUEST['skp_agents_dateto'] ? @$_REQUEST['skp_agents_dateto'] : date("d.m.Y");

        $contrCount = $cntr->getListSer();

        if (!isset($_REQUEST['createprintversion']) && !isset($_REQUEST["createxlsversion"]) && !isset($_POST["createarchiveitem"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");

            $tpl->setVariable("REPORT__AGENTS_SKP_TOTAL", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListSer(@$_POST["cnt_id"]));
            $tpl->setVariable("DATE_FROM", @$_REQUEST['skp_agents_datefrom'] ? @$_REQUEST['skp_agents_datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_REQUEST['skp_agents_dateto'] ? @$_REQUEST['skp_agents_dateto'] : date("d.m.Y"));

            $tpl->setVariable("FILTER_WTYPE_OPTIONS", array2options(adm_ticktypes_plugin::getList("services"), $_REQUEST["filter_wtype_id"]));

        }
        if (@$_REQUEST["createreport"] || @$_REQUEST["createprintversion"] || @$_REQUEST["createxlsversion"] || @$_POST["createarchiveitem"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            if ($USER->getTemplate() != "default")
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else
                $rtpl->loadTemplatefile("report.tmpl.htm");

            if (isset($_REQUEST['createprintversion']) || @$_REQUEST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());

                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("SORTTYPE", $dateParams[$_REQUEST["date_type"]]);
            $rtpl->setVariable("DATEFROM", @$_REQUEST['skp_agents_datefrom'] ? @$_REQUEST['skp_agents_datefrom'] : date("d.m.Y"));
            $rtpl->setVariable("DATETO", @$_REQUEST['skp_agents_dateto'] ? @$_REQUEST['skp_agents_dateto'] : date("d.m.Y"));
            $datefrom = (new DateTime($_REQUEST['skp_agents_datefrom']))->format('Y-m-d');
            $dateto = (new DateTime($_REQUEST['skp_agents_dateto']))->format('Y-m-d');

            if ($_REQUEST['filter_wtype_id']) {
                $filter[] = "tick.task_type=" . $DB->F($_REQUEST["filter_wtype_id"]);
            }

            if (count($_REQUEST['cnt_id']) != count($contrCount) && $hz = preg_replace("/,$/", "", implode(",",$_REQUEST["cnt_id"]))) {
                $filter [] = "tick.cnt_id IN (" . $hz . ")";
            }

            $filter = implode(' AND ', $filter);
            if (!empty($filter)) {
                $filter .= ' AND ';
            }

            $sql = "SELECT 
                        distinct(t.id) 
                    FROM 
                        `tasks` AS t 
                        LEFT JOIN `task_comments` AS tc ON tc.task_id=t.id 
                        LEFT JOIN `tickets` as tick ON tick.task_id=t.id 
                    WHERE 
                        {$filter}
                        t.plugin_uid='services' AND 
                        (
                                t.status_id in (" . implode(', ', [
                    TS::STATUS_SERVICE_DONE,
                    TS::STATUS_SERVICE_CASH_ADOPTED,
                    TS::STATUS_SERVICE_CLOSED,
                    TS::STATUS_SERVICE_REPORT,
                ]) . ")
                        ) AND t.id IN (
                            SELECT 
                                distinct(ttt.task_id) 
                            FROM (
                                select 
                                    task_id, 
                                    max(`datetime`) as datepost 
                                from 
                                    task_comments 
                                where 
                                    status_id=" . $DB->F(TS::STATUS_SERVICE_DONE) . " 
                                group by task_id
                            ) AS ttt
                            WHERE 
                                DATE_FORMAT(ttt.datepost, '%Y-%m-%d')>=" . $DB->F($datefrom) . " AND 
                                DATE_FORMAT(ttt.datepost, '%Y-%m-%d')<=" . $DB->F($dateto) . ")";

            $podr = "SELECT id,contr_title FROM list_contr WHERE id IN (SELECT DISTINCT(`contr_id`) FROM `podr_tickets` WHERE task_id IN ($sql))";
            $this->getLogger('debug')->debug('reports_f_skp_agents strange query', ['query'=>$podr]);
            $podrList = $DB->getCell2($podr);
            $dates = "SELECT DISTINCT(DATE_FORMAT(tc.datetime, '%Y-%m-%d')) as sdate FROM `task_comments` AS tc LEFT JOIN tickets AS tick ON tick.task_id=tc.task_id WHERE $filter tc.status_id='12' AND DATE_FORMAT(tc.datetime, '%Y-%m-%d')>=".$DB->F($datefrom)." AND DATE_FORMAT(tc.datetime, '%Y-%m-%d')<=".$DB->F($dateto)." ORDER BY tc.datetime";
            $r = $DB->getCell($dates);
            foreach($podrList as $item) {
                $rtpl->setCurrentBlock("podrlist");
                $rtpl->setVariable("PODR_NAME", $item);
                $rtpl->parse("podrlist");
            }
            $i = 1;
            $ftotalPodrCount = false;
            $ftotalPodrAmmount = false;
            $ftotalTotalCount = 0;
            $ftotalTotalAmmount = 0;
            $ftotalTotalAgentAmmount = 0;
            $ftotalTotalPodrAmmount = 0;
            foreach($r as $item) {
                $sql = "SELECT 
                            tick.cnt_id as contr_id, 
                            COUNT(DISTINCT(t.id)) as tcount, 
                            SUM(tick.orient_price) as totalammount
                        FROM 
                            `tasks` AS t 
                            LEFT JOIN tickets AS tick ON t.id=tick.task_id 
                        WHERE
                             {$filter}
                            (
                                t.status_id in (" . implode(', ', [
                        TS::STATUS_SERVICE_DONE,
                        TS::STATUS_SERVICE_CASH_ADOPTED,
                        TS::STATUS_SERVICE_CLOSED,
                        TS::STATUS_SERVICE_REPORT,
                    ]) . ")
                            ) AND
                            t.id IN (
                                SELECT 
                                    distinct(ttt.task_id) 
                                FROM (
                                    select 
                                        distinct(task_id), 
                                        max(`datetime`) as datepost 
                                    from 
                                        task_comments 
                                    where 
                                        status_id=" . TS::STATUS_SERVICE_DONE . " 
                                    group by task_id) AS ttt
                                WHERE 
                                    DATE_FORMAT(ttt.datepost, '%Y-%m-%d')=" . $DB->F($item) . "
                            )
                        GROUP BY tick.cnt_id
                        ";
                $DB->query($sql);

                if ($DB->num_rows()) {
                    while($res = $DB->fetch(true)) {
                        $ftotalTotalCount += $res["tcount"];
                        $rtpl->setCurrentBlock("rep_row");
                        $podrDayTotalCount = 0;
                        $podrDayTotalAmmount = 0;
                        $podrs = 0;
                        foreach($podrList as $k => $v) {
                            $sql = "SELECT 
                                        COUNT(DISTINCT(t.id)) as tcount, 
                                        SUM(tick.price) as ammount, 
                                        SUM(tick.orient_price) as ammount1 
                                    FROM 
                                        `tasks` AS t 
                                        LEFT JOIN tickets AS tick ON t.id=tick.task_id 
                                    WHERE 
                                        $filter tick.cnt_id=" . $DB->F($res["contr_id"]) . "  AND 
                                        (
                                            t.status_id in (" . implode(', ', [
                                    TS::STATUS_SERVICE_DONE,
                                    TS::STATUS_SERVICE_CASH_ADOPTED,
                                    TS::STATUS_SERVICE_CLOSED,
                                    TS::STATUS_SERVICE_REPORT,
                                ]) . ")
                                        ) AND
                                        t.id IN (
                                            SELECT 
                                                distinct(tc.task_id) 
                                            FROM 
                                                `task_comments` AS tc 
                                            WHERE (
                                                tc.status_id=" . TS::STATUS_SERVICE_DONE . ") 
                                                AND DATE_FORMAT(tc.datetime, '%Y-%m-%d')=" . $DB->F($item) . " 
                                            GROUP BY tc.task_id
                                        ) AND 
                                        t.id IN (
                                            SELECT 
                                                distinct(task_id) 
                                            FROM 
                                                `podr_tickets` 
                                            WHERE 
                                                `contr_id`=" . $DB->F($k) . "
                                        ) 
                                    GROUP BY tick.cnt_id";
                                    $DB->query($sql);
                                    $pord_res = $DB->fetch($sql);
                                    $rtpl->setCurrentBlock("podrvalues");
                                    $rtpl->setVariable("AGENT_QUANT", $pord_res["tcount"]);
                                    $rtpl->setVariable("AGENT_AMMOUNT", $pord_res["ammount"]);
                                    $podrDayTotalCount += $pord_res["tcount"];
                                    $podrDayTotalAmmount += $pord_res["ammount"];
                                    $ftotalPodrCount[$k] += $pord_res["tcount"];
                                    $ftotalPodrAmmount[$k] += $pord_res["ammount"];
                                    $p = kontr_plugin::getPodrPercent($k);
                                    if ($p>0) {
                                        $podrs += round(($pord_res["ammount"])*$p/100, 2);

                                        $ftotalTotalPodrAmmount += $podrs;
                                    }
                                    $rtpl->parse("podrvalues");
                                    $DB->free();
                        }
                        $rtpl->setVariable("ROW_DATE", $item);
                        $rtpl->setVariable("ROW_AGENT", kontr_plugin::getByID($res["contr_id"]));
                        $rtpl->setVariable("TOTAL_QUANT", $res["tcount"]);
                        $rtpl->setVariable("ROW_QUANT", $res["tcount"]-$podrDayTotalCount);
                        $ftotalTotalAmmount += $res["totalammount"]-$podrDayTotalAmmount;

                        $rtpl->setVariable("AMMOUNT", $res["totalammount"]-$podrDayTotalAmmount);
                        $rtpl->setVariable("TOTAL_COUNT", $res["totalammount"]);
                        $p = kontr_plugin::getAgentPercent($res["contr_id"]);
                        if ($p>0) {
                            $agents = round(($res["totalammount"])*$p/100, 2);
                            $ftotalTotalAgentAmmount += $agents;
                            $rtpl->setVariable("AGENTS_CONTR", $agents);
                        } else {
                            $rtpl->setVariable("AGENTS_CONTR", 0);
                        }

                        if ($podrs>0) {
                            $rtpl->setVariable("AGENTS_GORSERV", $podrs);
                        } else {
                            $rtpl->setVariable("AGENTS_GORSERV", 0);
                        }

                        $rtpl->parse("rep_row");
                    }
                }
                $i+=1;

            }
            $DB->free();

            $totalPodr = 0;
            $totalPodrCount = 0;
            foreach($podrList as $k => $v) {
                $rtpl->setCurrentBlock("podrlist-btm");
                $rtpl->setVariable("BTM_TOTALAGENT", $ftotalPodrCount[$k]);
                $rtpl->setVariable("BTM_TOTALAGENTAMMOUNT", number_format($ftotalPodrAmmount[$k], 2, ".", " "));
                $rtpl->parse("podrlist-btm");
                $totalPodrCount += $ftotalPodrCount[$k];
                $totalPodr += $ftotalPodrAmmount[$k];
            }
            $ftotalGSMAmmount = $ftotalTotalAmmount - $totalPodr;
            $rtpl->setVariable("BTM_TOTALAMMOUNT", number_format($ftotalGSMAmmount, 2, ".", " "));
            $ftotalGSMCount = $ftotalTotalCount - $totalPodrCount;
            $rtpl->setVariable("BTM_COUNT", $ftotalGSMCount);

            $rtpl->setVariable("BTM_TOTALCOUNT", $ftotalTotalCount);
            $rtpl->setVariable("BTM_TOTALMONETAMMOUNT", number_format($ftotalTotalAmmount, 2, ".", " "));
            $rtpl->setVariable("BTM_TOTAL_AGENTS_AMMOUNT", number_format($ftotalTotalPodrAmmount, 2, ".", " "));
            $rtpl->setVariable("BTM_TOTAL_AGENTS_CONTRAGENT",  number_format($ftotalTotalAgentAmmount, 2, ".", " "));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            if ($_POST["shownotempty"]) {
                $result = preg_replace("/<tr hiderow>(\W+<td (.*)\/td>\W+)+<\/tr>/", "", $rtpl->get());
            }
            else {
                $result = $rtpl->get();
            }
            if (isset($_REQUEST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");
                header("Content-disposition: attachment; filename=report_skp_agents__" . $datefrom . "-" . $dateto . ".xls");
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST["createarchiveitem"])) {
                    $sql = "INSERT INTO list_f_skpper_reports (`period`, `body`, `cr_uid`) VALUES (".$DB->F(base64_encode($df_arc." - ".$dt_arc)).", ".$DB->F(base64_encode($result)).", ".$DB->F($USER->getId()).")";
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error() ." ". $sql);
                    redirect($this->getLink('archive'), "Отчет сохранен в Архиве!");
                    die();
                } else {
                    if (isset($_REQUEST['createprintversion'])) {
                        $rtpl->touchBlock("print_footer");
                        $rtpl->show();
                        return;
                    } else {
                        $tpl->setCurrentBlock("reportval");
                        $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                        $tpl->parse("reportval");
                    }
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
    }
}
