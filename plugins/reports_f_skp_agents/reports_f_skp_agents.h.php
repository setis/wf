<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2014
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Отчет СКП за период по выполненным заявкам";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Отчет СКП за период по выполненным заявкам";
    $PLUGINS[$plugin_uid]['events']['archive'] = "Архив отчетов СКП за период";
    $PLUGINS[$plugin_uid]['events']['viewreport'] = "Просмотр Архивного отчета СКП за период";    
}

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['deletereport'] = "Удаление из архива отчетов СКП за период";
}


?>