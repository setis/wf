<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;


/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */

class adm_districts_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function viewlist()
    {
        global $DB, $USER;

        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        $sql = "SELECT dis.id, dis.title, dis.desc, count(distinct area.id) as areacnt FROM `list_district` as `dis` LEFT JOIN `list_area` AS `area` ON area.dis_id=dis.id GROUP BY dis.id ORDER BY areacnt DESC;";
        $DB->query($sql);
        if ($DB->errno()) {
            UIError($DB->error());
        }
        while (list($id, $title, $desc, $areacnt) = $DB->fetch()) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("VLI_ID", $id);
            $tpl->setVariable("VLI_TITLE", $title);
            $tpl->setVariable("VLI_DESC", $desc);
            $tpl->setVariable("VLT_AREACOUNT", $areacnt);
            $tpl->parse("vl_item");
        }
        UIHeader($tpl);
        $tpl->show();
    }

    function edit() {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
        //$tpl->loadTemplatefile("edit.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate()."/edit.tmpl.htm");
        else
            $tpl->loadTemplatefile("edit.tmpl.htm");

        if ($contr_id = $_REQUEST['dis_id']) {
            $sql = "SELECT * FROM `list_district` WHERE `id`=" . $DB->F($contr_id) . ";";
            $DB->query($sql);
            if($DB->errno()) UIError($DB->error());
            if (!$DB->num_rows()) UIError("Округ с таким идентификатором отсутствует.");
            $result = $DB->fetch(true);

            $tpl->setVariable("DIS_ID", $result["id"]);
            $tpl->setVariable("DIS_TITLE", $result["title"]);
            $tpl->setVariable("DIS_DESC", $result["desc"]);

        }
        UIHeader($tpl);
        $tpl->show();

    }

    function getDistrict($id) {
        global $DB;
        return $DB->getField("SELECT `title` FROM `list_district` WHERE `id`=".$DB->F($id).";");
    }

    function save() {
        global $DB, $USER;

        //Debugger::dump($_POST, true);

        $err = array();

        if(!$_POST['title']) $err[] = "Не заполнено Название Округа";

        if(sizeof($err)) UIError($err);

        if($_POST['dis_id']) $sql = "UPDATE `list_district` SET `title`=".$DB->F($_POST['title']).", `desc`=".$DB->F($_POST['desc'])." WHERE `id`=".$DB->F($_POST['dis_id']);
        else {
            $sql = "INSERT INTO `list_district` (`title`, `desc`) VALUES(".$DB->F($_POST['title']).", ".$DB->F($_POST['desc']).")";
        }
        $DB->query($sql);
        if($DB->errno()) UIError($DB->error());

        if(!$_POST['dis_id']) {
            $_POST['dis_id'] = $DB->insert_id();
        }
        redirect($this->getLink('viewlist'), "Округ сохранен. ID: ".$_POST['dis_id'] );
    }

    function delete() {
        global $DB;
        if (!($id = $_REQUEST['dis_id'])) $err[] = "Не указан Округ";
        if(sizeof($err)) UIError($err);
        if ($this->isBusy($_REQUEST["dis_id"])) UIError("Выбранный Округ связан с активными Адресами и/или Районами. Удаление невозможно.");
        $DB->query("DELETE FROM `list_district` WHERE `id`='".$_REQUEST['dis_id']."';");
        redirect($this->getLink(),"Округ успешно удален.");

    }

    function isBusy($id)
    {
        global $DB;
        $DB->query("SELECT `id` FROM `list_area` WHERE `dis_id`=" . $DB->F($id) . ";");

        return $DB->num_rows() ? true : false;
    }

    function getDisList($sel = false) {
        global $DB;
        return array2options($DB->getCell2("SELECT `id`,`title` FROM `list_district` ORDER BY `title`;"), $sel);
    }


}
?>
