<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\tickets\ServiceTicket;


require_once(dirname(__FILE__)."/../services/services.php");

 
class reports_f_skp_avg_price_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        $fintypes = array("0"=>"Все", "1"=>"Рассчитанные (Проведенные)", "2"=>"Не рассчитанные (Не проведенные)");
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_TECH_TICKET", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            UIHeader($tpl);
            report_left_menu($tpl);
            $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
            $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
            $scList = adm_sc_plugin::getSCList2Col();
            $scList[0] = "-- СЦ не указан --";
            $tpl->setVariable("FILTER_SC_OPTIONS", array2options($scList, $_POST["sc_id"]));
            $tpl->setVariable("FILTER_TECH_OPTIONS", adm_empl_plugin::getTechListSKP($_POST["tech_id"]));
            $this->getSession()->set("filter_status_id", $_REQUEST['filter_status_id'] ? $_REQUEST['filter_status_id'] : $this->getSession()->get("filter_status_id"));
            $_REQUEST['filter_status_id'] = $this->getSession()->get("filter_status_id");
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", $cntr->getOptListSer(@$_POST["cnt_id"]));
            }
            if ($_REQUEST['loadphones'] == "on") {
                $tpl->setVariable("LOADPHONES", "checked='checked'");
            }
            if ($_REQUEST['loadfio'] == "on") {
                $tpl->setVariable("LOADFIO", "checked='checked'");
            }
            
            if ($_REQUEST['loadgfx'] == "on") {
                $tpl->setVariable("LOADGFX", "checked='checked'");
            }
            $tpl->setVariable('FILTER_STATUS_OPTIONS', adm_statuses_plugin::getOptList($_REQUEST['filter_status_id'], "services"));
            $dateParams = array(0 => "Дата создания", 1 => "Дата выполнения", 2=>"Дата изменения", 3=>"Дата выполнения/Отказа", 4=>"Прием денег", 5=>"Дата Закрытия", 6=>"Дата Сдачи отчета", 7=>"Дата Принято в кассу");
            $tpl->setVariable("FILTER_DATE_PARAMS", array2options($dateParams, $_REQUEST["date_type"]));
            $tpl->setVariable("FILTER_WTYPE_OPTIONS", array2options(adm_ticktypes_plugin::getList("services"), $_POST["filter_wtype_id"]));
        
        }
        
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $rtpl->setVariable("DATEFROM", $_POST['datefrom']);
            $rtpl->setVariable("DATETO", $_POST['dateto']);
            //if ($_POST["tech_id"]!=0 && $usr = adm_users_plugin::getUser($_POST["tech_id"])) {
                
            $rtpl->setVariable("REP_TECH", $usr['fio']);
            if ($_REQUEST['loadphones'] == "on") {
                $rtpl->touchBlock("usephones");
            }
            if ($_REQUEST['loadfio'] == "on") {
                $rtpl->touchBlock("userfio");
            }
            if ($_REQUEST['loadgfx'] == "on") {
                $rtpl->touchBlock("gfxdate");
            }            
            $datefrom = substr($_POST['datefrom'], 6,4)."-".substr($_POST['datefrom'], 3,2)."-".substr($_POST['datefrom'], 0,2);
            $dateto = substr($_POST['dateto'], 6,4)."-".substr($_POST['dateto'], 3,2)."-".substr($_POST['dateto'], 0,2);
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "services");
            $closedStatus = adm_statuses_plugin::getStatusByTag("closed", "services");
            $accStatus = adm_statuses_plugin::getStatusByTag("acc_accepted", "services");
            $reportStatus = adm_statuses_plugin::getStatusByTag("report", "services");
            $failStatus = adm_statuses_plugin::getStatusByTag("otkaz", "services");
            $failOpStatus = adm_statuses_plugin::getStatusByTag("opotkaz", "services");
            $sList = adm_statuses_plugin::getList("services");
            $useAllStatuses = false;
            if (count($_REQUEST["filter_status_id"]) == count($sList)) {
                $useAllStatuses = true;
            }
            if ($_REQUEST["filter_status_id"]) {
                $filter .= " AND t.status_id IN (".preg_replace("/,$/", "", implode(",", array_map([$DB,'F'],(array)$_REQUEST["filter_status_id"]))).")";
            }
            if ($_REQUEST["date_type"] == 1) {
                $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=".$DB->F($doneStatus["id"])." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST['datefrom'])))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST['dateto']))).") ORDER BY `datetime` DESC) AND t.id NOT IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=".$DB->F($doneStatus["id"])." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>".$DB->F(date("Y-m-d", strtotime($_POST['dateto']))).") ORDER BY `datetime` DESC)";
            } else {
                if ($_REQUEST["date_type"] == 2) {
                    $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE `status_id` AND (DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST['datefrom'])))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST['dateto'])))."))";
                 } else  {                 
                    if ($_REQUEST["date_type"] == 3) {
                        $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE ((`status_id`=".$DB->F($doneStatus["id"])." OR `status_id`=".$DB->F($failStatus["id"])." OR `status_id`=".$DB->F($failOpStatus["id"]).") AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST['datefrom'])))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST['dateto']))).") ORDER BY `datetime` DESC) AND t.id NOT IN (SELECT task_id FROM `task_comments` WHERE ((`status_id`=".$DB->F($doneStatus["id"])." OR `status_id`=".$DB->F($failStatus["id"])." OR `status_id`=".$DB->F($failOpStatus["id"]).") AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>".$DB->F(date("Y-m-d", strtotime($_POST['dateto']))).") ORDER BY `datetime` DESC)";                
                    } else {
                        if ($_REQUEST["date_type"] == 4) {
                            // am
                            $filter .= "AND tick.inmoney=1  AND DATE_FORMAT(tick.inmoneydate, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " AND DATE_FORMAT(tick.inmoneydate, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST['dateto'])));
                        } else {
                            if ($_REQUEST["date_type"] == 5) {
                                //cl_date
                                $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=".$DB->F($closedStatus["id"])." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST['datefrom'])))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST['dateto']))).") ORDER BY `datetime` DESC) AND t.id NOT IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=".$DB->F($closedStatus["id"])." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>".$DB->F(date("Y-m-d", strtotime($_POST['dateto']))).") ORDER BY `datetime` DESC)";
                            } else {
                                if ($_REQUEST["date_type"] == 6) {
                                    //rep_date
                                    $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=".$DB->F($reportStatus["id"])." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST['datefrom'])))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST['dateto']))).") ORDER BY `datetime` DESC)";
                                } else {
                                    if ($_REQUEST["date_type"] == 7) {
                                        //rep_date
                                        $filter .= "AND t.id IN (SELECT task_id FROM `task_comments` WHERE (`status_id`=".$DB->F($accStatus["id"])." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST['datefrom'])))." AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST['dateto']))).") ORDER BY `datetime` DESC)";
                                    } else {
                                        $filter .= " AND DATE_FORMAT(t.date_reg, '%Y-%m-%d')>=".$DB->F(date("Y-m-d", strtotime($_POST['datefrom']))) . " AND DATE_FORMAT(t.date_reg, '%Y-%m-%d')<=".$DB->F(date("Y-m-d", strtotime($_POST['dateto'])));
                                    }
                                }
                            }  
                        }
                        
                    }
                 }
            }
            if ($_REQUEST["tech_id"] != 0) {
                $filter .= " AND g.empl_id=".$DB->F($_REQUEST["tech_id"])." ";
            }
            if ($_REQUEST['filter_wtype_id']) {                
                $filter .= " AND tick.task_type=".$DB->F($_REQUEST["filter_wtype_id"])." ";
            }
            if ( count($_REQUEST['cnt_id']) ) {
                $filter .= " AND tick.cnt_id in (". implode(",", array_map([$DB,'F'],(array)$_REQUEST["cnt_id"])) . ")";
            }
            
            if ( count($_REQUEST['sc_id']) ) {
                $filter .= " AND ifnull(tick.sc_id,0) in (". implode(",", array_map([$DB,'F'],(array)$_REQUEST["sc_id"])) . ")";
            }
            
            $sql = "SELECT 
                t.id, 
                tickets_calls.calldate
            FROM 
                tasks AS t 
                LEFT JOIN 
                gfx AS g ON g.task_id=t.id 
                LEFT JOIN 
                tickets AS tick ON tick.task_id=t.id
                LEFT JOIN
                tickets_calls ON t.id = tickets_calls.task_id
            WHERE 
                t.plugin_uid='services' $filter 
            GROUP BY 
                t.id 
            ORDER BY 
                t.date_reg,
                tickets_calls.calldate ASC";

            $DB->query($sql);
            if ($DB->errno()) {
                UIError($DB->error()." ".$sql);
            }
            $i = 1;
            $totalAVG = 0;
            $totalREAL = 0;
            $rtpl->setVariable("WT_COUNT", count($wtList));
            foreach($wtList as $item) {
                $rtpl->setCurrentBlock("wt_col");
                $rtpl->setVariable("WT_SERVICENAME", $item["short_title"]);
                $rtpl->parse("wt_col");
            }
            $rejectHead = false;
            while ($res = $DB->fetch(true)) {
                $ticket = new ServiceTicket($res["id"]);
                $rtpl->setCurrentBlock("rep_row");
                $rtpl->setVariable("RRR_ID", $i);
                $rtpl->setVariable("RR_ID", $ticket->getId());
                $rtpl->setVariable("RR_FIRST_CALL_DATETIME", empty($res['calldate']) ? '&mdash;' : (new DateTime($res['calldate']))->format('Y-m-d H:i'));
                $scName = $DB->getField("SELECT `title` FROM `list_sc` WHERE `id`=".$DB->F($ticket->getSCId()).";");
                $rtpl->setVariable("RR_SC", $scName ? $scName  : "СЦ не указан");
                
                if ($doneStatus) {
                    $sql = "SELECT 
                        MAX(datetime)
                    FROM 
                      task_comments 
                    WHERE 
                      task_id=".$DB->F($res["id"])." AND 
                      status_id=".$DB->F($doneStatus["id"]);
                    $doneDate = $DB->getField($sql);

                    if ($doneDate) {
                        $rtpl->setVariable("RR_EXDATE", (new DateTime($doneDate))->format('Y-m-d H:i'));
                    } else {
                        if ($failStatus || $failOpStatus) {
                            $sql = "SELECT 
                                MAX(`datetime`)
                            FROM 
                                task_comments
                            WHERE 
                                task_id=".$DB->F($res["id"])." AND 
                                status_id=".$DB->F($failStatus["id"]);
                            $failDate = $DB->getField($sql);
                            if ($failDate) {
                                $rtpl->setVariable("RR_EXDATE", "<p style=\"color:#FF0000;\">".(new DateTime($failDate))->format('Y-m-d H:i')."</p>");
                            } else {
                                $sql = "SELECT DATE_FORMAT(MAX(`datetime`), ".$DB->F("%d.%m.%Y").") as doneDate FROM `task_comments` WHERE `task_id`=".$DB->F($res["id"])." AND `status_id`=".$DB->F($failOpStatus["id"])." ORDER BY doneDate DESC LIMIT 1;";
                                $failDate = $DB->getField($sql);
                                if ($failDate) {
                                    $rtpl->setVariable("RR_EXDATE", "<p style=\"color:#FF0000;\">".(new DateTime($failDate))->format('Y-m-d H:i')."</p>");
                                } else {
                                    $rtpl->setVariable("RR_EXDATE", "&mdash;");                                     
                                }
                            }
                        } else {
                            $rtpl->setVariable("RR_EXDATE", "&mdash;");
                        }   
                                                                           
                    }
                    $sql = "SELECT 
                            MAX(datetime)
                        FROM 
                            task_comments
                        WHERE 
                            task_id=".$DB->F($res["id"])." AND 
                            status_id=".$DB->F($closedStatus["id"]);

                    $closeDate = $DB->getField($sql);
                    if ($closeDate) {
                        $rtpl->setVariable("RR_CLOSEDATE", (new DateTime($closeDate))->format('Y-m-d H:i'));
                    } else {
                        $rtpl->setVariable("RR_CLOSEDATE", "&mdash;");
                    }
                    $sql = "SELECT 
                            MAX(datetime)
                        FROM 
                            task_comments
                        WHERE 
                            task_id=".$DB->F($res["id"])." AND 
                            status_id=".$DB->F($reportStatus["id"]);
                    
                    $repDate = $DB->getField($sql);
                    if ($repDate) {
                        $rtpl->setVariable("RR_REPDATE", (new DateTime($repDate))->format('Y-m-d H:i'));
                    } else {
                        $rtpl->setVariable("RR_REPDATE", "&mdash;");
                    }
                    
                    $sql = "SELECT 
                                MAX(datetime)
                            FROM 
                                task_comments
                            WHERE 
                                task_id=".$DB->F($res["id"])." AND 
                                status_id=".$DB->F($accStatus["id"]);
                    
                    $accDate = $DB->getField($sql);
                    if ($accDate) {
                        $rtpl->setVariable("RR_MRDATE", (new DateTime($accDate))->format('Y-m-d H:i'));
                    } else {
                        $rtpl->setVariable("RR_MRDATE", "&mdash;");
                    }
                } else {
                    $rtpl->setVariable("RR_EXDATE", "&mdash;");
                }
                
                $rtpl->setVariable("RR_AVG_PRICE", $ticket->getOrientPrice() ? round($ticket->getOrientPrice(), 0) : 0);
                $tPrice = $ticket->getPrice();
                $sql = "SELECT `ammount` FROM `skp_money` WHERE `task_id`=".$DB->F($ticket->getId())." AND `last`;";
                $rPrice = $DB->getField($sql);
                $price = $rPrice ? $rPrice : ($tPrice ? $tPrice : 0);
                $rtpl->setVariable("RR_PRICE", round($price,0));
                $totalAVG += $ticket->getOrientPrice();
                $totalREAL += $price;
                $rtpl->setVariable("RR_WTYPENAME", $ticket->getTypeName() ? $ticket->getTypeName() : "&mdash;");
                $rtpl->setVariable("RR_DOCNUM", $ticket->getTicketDocNum() ? $ticket->getTicketDocNum() : "&mdash;");
                $rtpl->setVariable("RR_GSSID", $ticket->getClntTNum());
                $rtpl->setVariable("RR_INTID", $ticket->getId());
                $rtpl->setVariable("RR_STATUSNAME", $ticket->getStatusName());
                $rtpl->setVariable("RR_CONTR_NAME", $ticket->getContrName());
                $statusData = $ticket->getStatusArray();
                //if (preg_match("/otkaz/", $statusData["tag"]) || $rejectHead || $useAllStatuses) {
                    //$rejectHead = true;
                    //$rtpl->touchBlock("rej_head");
                    //$rtpl->setCurrentBlock("rej_body");
                    $reason = adm_rejects_plugin::getByID($ticket->getRejectReasonId());
                    $rtpl->setVariable("REJECTREASON", $reason ? $reason : "&mdash;");
                    //$rtpl->parse("rej_body");
                //}
                $ticketSource = adm_ticket_sources_plugin::getById($ticket->getTaskFrom());
                $rtpl->setVariable("RR_WHEREFROM", $ticketSource ? $ticketSource : "&mdash;");
                $rtpl->setVariable("RR_AREA", $ticket->getArea());
                $rtpl->setVariable("RR_CRDATE", (new DateTime($ticket->getDateReg()))->format('Y-m-d H:i') );
                $resp = $ticket->getTaskIspoln($ticket->getId());
                $resp = array_values($resp);
                $uid = $DB->getField("SELECT `empl_id` FROM `gfx` WHERE `task_id`=".$DB->F($ticket->getId()).";");
                $tech = $uid ? adm_users_plugin::getUser($uid) : false;
                $rtpl->setVariable("RR_TECHNAME", $tech["fio"] ? $tech["fio"] : "&mdash;");
                $rtpl->setVariable("RR_RESPONSIBLE", $resp[0] ? $resp[0] : "&mdash;");
                $addr = $ticket->getAddrN();
                if ($_REQUEST["loadphones"] == "on") {
                    $phone = $ticket->getPhones();
                    $rtpl->setCurrentBlock("usephonesbody");
                    $rtpl->setVariable("PHONE1", $phone[0] ? preg_replace("/^7/", "", $phone[0]) : "-");
                    $rtpl->setVariable("PHONE2", $phone[1] ? preg_replace("/^7/", "", $phone[1]) : "-");
                    $rtpl->parse("usephonesbody");
                }
                if ($_REQUEST["loadgfx"] == "on") {
                    $gfx = $DB->getField("SELECT `c_date` FROM `gfx` WHERE `task_id`=".$DB->F($ticket->getId()).";");
                    
                    $rtpl->setCurrentBlock("gfxdatebody");
                    $rtpl->setVariable("RR_GFXDATE", ($gfx && $gfx != "0000-00-00") ? rudate("d.m.Y", strtotime($gfx)) : "-");
                    $rtpl->parse("gfxdatebody");
                }
                if ($_REQUEST['loadfio'] == "on") {
                    $rtpl->setCurrentBlock("userfiobody");
                    $rtpl->setVariable("USERFIO", $ticket->getFio()?$ticket->getFio() : $ticket->getOrgName());
                    $rtpl->parse("userfiobody");
                }
                //print_r($addr);
                if (isset($addr["nokladr"])) {
                    $rtpl->setCurrentBlock("no_kladr");
                    $rtpl->setVariable("TMCCOUNT2", $rowspan);
                    $rtpl->parse("no_kladr");
                } else {
                    $rtpl->setCurrentBlock("kladr_addr");
                    $rtpl->setVariable("TMCCOUNT3", $rowspan);
                    $rtpl->setVariable("RR_ADDR_CITY", $addr["city"]);
                    $rtpl->setVariable("RR_ADDR_STREET", $addr["street"]);
                    $rtpl->setVariable("RR_ADDR_HOUSE", "д.".$addr["house"]);
                    $rtpl->setVariable("RR_ADDR_FLAT", $ticket->getKv() ? $ticket->getKv() : "&mdash;");
                    $rtpl->parse("kladr_addr");
                }
                $rtpl->setVariable("RR_SUMMWONDS", $sum);
                $rtpl->parse("rep_row");
                $i+=1;
            }
            //die();
            $rtpl->setVariable("AVGPRICETOTAL", $totalAVG);
            $rtpl->setVariable("PRICETOTAL", $totalREAL);
            $colspan = 7;
            if ($rejectHead) {
                $colspan += 1;   
            }   
            if ($_REQUEST["loadphones"] == "on") {
                $colspan += 2;
            }
            if ($_REQUEST["loadfio"] == "on") {
                $colspan += 1;
            }
            $rtpl->setVariable("COLSPAN_STS", $colspan+1);
            $DB->free();
            $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_avg_price__".$datefrom."-".$dateto.".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_POST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 

}
?>
