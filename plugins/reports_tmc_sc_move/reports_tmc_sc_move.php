<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;


require_once(dirname(__FILE__)."/../connections/connections.php");

 
class reports_tmc_sc_move_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;

        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        if (!isset($_POST['createprintversion']) && !isset($_POST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__TMC_SC_MOVE", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            $dateFrom = @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date("d.m.Y") . " - 1 month"));
            $dateTo = @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y");    
            $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m')));
            $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y")));
            $tpl->setVariable("DATE_FROM", $dateFrom);
            $tpl->setVariable("DATE_TO", $dateTo);
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList(@$_POST["sc_id"]));
        }
        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"] || @$_POST["setflag"]) {
                $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
                $rtpl->loadTemplatefile("report.tmpl.htm");
                if ($USER->getTemplate() != "default") 
                    $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
                else 
                    $rtpl->loadTemplatefile("report.tmpl.htm");
                
                $sc = adm_sc_plugin::getSC($_POST["sc_id"]);
                $rtpl->setVariable("SCNAME", $sc['title']);
                $dateFrom = @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y", strtotime(date("d.m.Y") . " - 1 month"));
                $dateTo = @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y");
                $rtpl->setVariable("DATE_FROM", $dateFrom);
                $rtpl->setVariable("DATE_TO", $dateTo);
                $dateFrom = substr($dateFrom, 6, 4)."-".substr($dateFrom, 3, 2). "-".substr($dateFrom, 0,2);
                $dateTo = substr($dateTo, 6, 4)."-".substr($dateTo, 3, 2). "-".substr($dateTo, 0,2);
                
                if (isset($_POST['createprintversion']) || @$_POST["createxlsversion"]) {
                    $rtpl->setCurrentBlock("print_head");
                    $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                    $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                    $rtpl->parse("print_head");
                }
                //$rtpl->setVariable("MONTH", $month[$_POST['month']]);
                //$rtpl->setVariable("YEAR", $year[$_POST['year']]);
                
                $sql = "SELECT tmc.*, lm.title AS tmcname, lmc.title AS cattitle, lm.metric_flag  FROM `tmc_sc` AS tmc LEFT JOIN `list_materials` AS lm ON lm.id=tmc.tmc_id LEFT JOIN `list_material_cats` AS lmc ON lmc.id=lm.cat_id WHERE DATE_FORMAT(tmc.ledit, '%Y-%m-%d')>=".$DB->F($dateFrom)." AND DATE_FORMAT(tmc.ledit, '%Y-%m-%d')<=".$DB->F($dateTo)." AND tmc.sc_id=".$DB->F($_POST["sc_id"])." AND tmc.inplace ORDER BY lmc.id,tmc.ledit;";
                $incEx = false;
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                if ($DB->num_rows()) {
                    while($res = $DB->fetch(true)) {
                        if ($res["count"]>0) {
                            $rtpl->setCurrentBlock("rep_row");
                            $rtpl->setVariable("ROW_DATE", $res["ledit"]);
                            $user_param = adm_users_plugin::getUser($res["user_id"]);
                            $rtpl->setVariable("ROW_AUTHOR", $user_param["fio"]);
                            $rtpl->setVariable("ROW_TMC_CAT", $res["cattitle"]);
                            $rtpl->setVariable("ROW_TMC_NAME", $res["tmcname"]." (".$res["id"].")");
                            $rtpl->setVariable("ROW_TMC_SN", !$res['metric_flag'] ? $res["serial"] : "<center>&mdash;</center>");
                            $rtpl->setVariable("ROW_TMC_COUNT_INC", $res["count"]);
                            $rtpl->setVariable("ROW_TMC_COUNT_GONE", "<center>&mdash;</center>");
                            $rtpl->setVariable("ROW_TMC_OPERATION", "приход");
                            $sql = "SELECT `id` FROM `tmc_sc` WHERE `sc_id`=".$DB->F($res["sc_id"])." AND `count`=".$DB->F($res["count"]*(-1))." AND `tmc_id`=".$DB->F($res["tmc_id"])." AND (`inplace`=0 OR `inplace` IS NULL) AND (`incoming`=0 OR `incoming` IS NULL);";
                            $inctmcid = $DB->getField($sql);
                            if (!$inctmcid) 
                                $sql = "SELECT * FROM `tmc_sc_tech` WHERE `tmc_sc_id`=".$DB->F($res["id"])." AND `ledit` IN (SELECT MAX(`ledit`) FROM tmc_sc_tech WHERE `tmc_sc_id`=".$DB->F($res["id"]).") LIMIT 1;";
                            else 
                                $sql = "SELECT * FROM `tmc_sc_tech` WHERE `tmc_sc_id`=".$DB->F($inctmcid)." AND `ledit` IN (SELECT MAX(`ledit`) FROM tmc_sc_tech WHERE `tmc_sc_id`=".$DB->F($inctmcid).") LIMIT 1;";
                            $DB->query($sql);
                            if ($DB->errno()) UIError($DB->error());
                            if ($DB->num_rows()) {
                                $result = $DB->fetch(true);
                                $tech = adm_users_plugin::getUser($result["tech_id"]);
                                $rtpl->setVariable("ROW_TMC_DOC_ID", "Возврат от техника: <strong><i>".$tech["fio"]."</i></strong><br /><small>ID транзакции: ".$res["id"]."</small>");
                            } else {
                                $rtpl->setVariable("ROW_TMC_DOC_ID", "Приход с главного склада<br /><small>ID транзакции: ".$res["id"]."</small>");
                            }
                            $DB->free();                        
                            //$rtpl->setVariable("ROW_TMC_DOC_ID", $res[""]);
                            $scChiefs = adm_empl_plugin::getChiefListBySC($_POST["sc_id"]);
                            if ($scChiefs) {
                                $scChiefList = '';
                                foreach($scChiefs as $item) {
                                    $scChiefList.=$item."<br />";
                                }
                                $rtpl->setVariable("ROW_RESPONSIBLE", $scChiefList);                            
                            } else
                            $rtpl->setVariable("ROW_RESPONSIBLE", "&mdash;");
                            $rtpl->parse("rep_row");
                        }
                    }
                }
                $DB->free();
                $sql = "SELECT tmc.*, lm.title AS tmcname, lmc.title AS cattitle, lm.metric_flag  FROM `tmc_sc` AS tmc LEFT JOIN `list_materials` AS lm ON lm.id=tmc.tmc_id LEFT JOIN `list_material_cats` AS lmc ON lmc.id=lm.cat_id WHERE DATE_FORMAT(tmc.ledit, '%Y-%m-%d')>=".$DB->F($dateFrom)." AND DATE_FORMAT(tmc.ledit, '%Y-%m-%d')<=".$DB->F($dateTo)." AND tmc.sc_id=".$DB->F($_POST["sc_id"])." AND (tmc.inplace=0 OR tmc.inplace IS NULL) AND (tmc.incoming=0 OR tmc.incoming IS NULL) ORDER BY lmc.id,tmc.ledit;;";
                $incEx = false;
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                if ($DB->num_rows()) {
                    while($res = $DB->fetch(true)) {
                        if ($res["count"]>0) {
                            $rtpl->setCurrentBlock("rep_row");
                            $rtpl->setVariable("ROW_DATE", $res["ledit"]);
                            $user_param = adm_users_plugin::getUser($res["user_id"]);
                            $rtpl->setVariable("ROW_AUTHOR", $user_param["fio"]);
                            $rtpl->setVariable("ROW_TMC_CAT", $res["cattitle"]);
                            $rtpl->setVariable("ROW_TMC_NAME", $res["tmcname"]);
                            $rtpl->setVariable("ROW_TMC_SN", !$res['metric_flag'] ? $res["serial"] : "<center>&mdash;</center>");
                            $rtpl->setVariable("ROW_TMC_COUNT_INC", "<center>&mdash;</center>");
                            $rtpl->setVariable("ROW_TMC_COUNT_GONE", $res["count"]);
                            $rtpl->setVariable("ROW_TMC_OPERATION", "списание");
                            $sql = "SELECT `id` FROM `tmc_sc` WHERE `sc_id`=".$DB->F($res["sc_id"])." AND `count`=".$DB->F($res["count"]*(-1))." AND `tmc_id`=".$DB->F($res["tmc_id"])." AND (`inplace`=0 OR `inplace` IS NULL) AND (`incoming`=0 OR `incoming` IS NULL);";
                            $inctmcid = $DB->getField($sql);
                            if (!$inctmcid) 
                                $sql = "SELECT * FROM `tmc_sc_tech` WHERE `tmc_sc_id`=".$DB->F($res["id"])." AND `ledit` IN (SELECT MAX(`ledit`) FROM tmc_sc_tech WHERE `tmc_sc_id`=".$DB->F($res["id"]).") LIMIT 1;";
                            else 
                                $sql = "SELECT * FROM `tmc_sc_tech` WHERE `tmc_sc_id`=".$DB->F($inctmcid)." AND `ledit` IN (SELECT MAX(`ledit`) FROM tmc_sc_tech WHERE `tmc_sc_id`=".$DB->F($inctmcid).") LIMIT 1;";
                            $DB->query($sql);
                            if ($DB->errno()) UIError($DB->error());
                            if ($DB->num_rows()) {
                                $result = $DB->fetch(true);
                                $tech = adm_users_plugin::getUser($result["tech_id"]);
                                $rtpl->setVariable("ROW_TMC_DOC_ID", "Передано технику: <strong><i>".$tech["fio"]."</i></strong><br /><small>ID транзакции: ".$res["id"]."</small>");
                            } else {
                                $rtpl->setVariable("ROW_TMC_DOC_ID", "Возврат на главный склад<br /><small>ID транзакции: ".$res["id"]."</small>");
                            }
                            $DB->free();                        
                            //$rtpl->setVariable("ROW_TMC_DOC_ID", $res[""]);
                            $scChiefs = adm_empl_plugin::getChiefListBySC($_POST["sc_id"]);
                            if ($scChiefs) {
                                $scChiefList = '';
                                foreach($scChiefs as $item) {
                                    $scChiefList.=$item."<br />";
                                }
                                $rtpl->setVariable("ROW_RESPONSIBLE", $scChiefList);                            
                            } else
                            $rtpl->setVariable("ROW_RESPONSIBLE", "&mdash;");
                            $rtpl->parse("rep_row");
                        }
                    }
                }
                $DB->free();
                $rtpl->setVariable("REP_CDATE", rudate("d M Y"));
                $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
                if (isset($_POST["createxlsversion"])) {
                    header("Content-type: application/vnd.ms-excel");  
                    header("Content-disposition: attachment; filename=report_tmc_sc_move_".$dateFrom."-".$dateTo.".xls");  
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    if (isset($_POST['createprintversion'])) {
                        $rtpl->touchBlock("print_footer");
                        $rtpl->show();
                        
                        return;
                    } else {
                        $tpl->setCurrentBlock("reportval");
                        $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                        $tpl->parse("reportval");
                    }
                }
            } else {
                $tpl->touchBlock("notcreated");
                $tpl->setVariable("DISABLED", "disabled='disabled'");
            }
        
        
        $tpl->show();
        
    }
    
            
    
}
?>