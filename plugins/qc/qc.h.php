<?php

/**
 *
 * Plugin Header
 *
 * @author kblp
 * @copyright 2013
 *
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Обзвон";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['viewlist'] = "Модуль контроля качества обслуживания";
    $PLUGINS[$plugin_uid]['events']['loadoprosform_ajax'] = "Загрузка формы опроса";
    $PLUGINS[$plugin_uid]['events']['loadreject_ajax'] = "Отказ клиента";
    $PLUGINS[$plugin_uid]['events']['loaddelay_ajax'] = "Перенос обзвона по просьбе клиента";
    $PLUGINS[$plugin_uid]['events']['saveoprosform_ajax'] = "Сохранение данных обзвона";
}

$plugin_uid = "qc_editdones";

$PLUGINS[$plugin_uid]['name'] = "Обзвон: Редактирование результатов выполненного обзвона";
$PLUGINS[$plugin_uid]['hidden'] = true;


?>
