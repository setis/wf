<?php

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use Doctrine\ORM\EntityManagerInterface;
use models\Task as OrmTask;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use WF\Task\Events\TaskQcChangedEvent;

class qc_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__file__, '.php'));
    }

    /**
     * Возвращает массив контрагентов, по которым может совершаться обзвон
     *
     * @author Ed <farafonov@gorserv.ru>
     *
     * @param $sel - массив идентификаторов контрагентов
     * @return options list для select
     */
    function getContrOptions($sel)
    {
        global $DB;
        $sql = "SELECT 
  DISTINCT 
   c.id, REPLACE(`contr_title`, '\"', '') contr_title
  FROM list_contr c,
       list_contr_agr a,
       link_agr_wtypes law
 WHERE law.is_qc > 0
   AND law.agr_id = a.id
   AND a.status_id = 84 /* только активные договора */
   AND a.contr_id = c.id";
        return array2options($DB->getCell2($sql), $sel);
    }

    function viewlist()
    {
        global $DB, $USER;

        $sort_ids = array(
            1 => "ID заявки",
            2 => "Дата обзвона",
            3 => "Дата создания заявки",
            4 => "Тип заявки"
        );


        $sort_sql = array(
            1 => "t.id",
            2 => "qc.qc_date",
            3 => "t.date_reg",
            4 => "t.plugin_uid"
        );

        $interval_options = array(
            '1' => "Дата регистрации заявки",
            '2' => "Дата статуса заявки",
            '3' => "Дата выполнения обзвона",
            '4' => "Дата до которой отложен обзвон"
        );

        $sort_order = array(
            'DESC' => "Обратный",
            'ASC' => "Прямой"
        );

        $rpp = array(
            20 => "20",
            50 => "50",
            100 => "100",
            150 => "150",
            200 => "200",
            250 => "250",
            500 => "500"
        );

        $qcres = array(
            0 => "-- все --",
            1 => "Отлично",
            2 => "Удовлетворительно",
            3 => "Плохо",
            4 => "Очень плохо"
        );

        $qc_status = array(0 => " -- все -- ", 1 => "Отказ", 2 => "Выполнена");
        $qc_cstatus = array(0 => "Не выполнялся", 1 => "Выполнен", 2 => "Отказ клиента", 3 => "Отложен");
        $rejectList = array(10, 46, 43, /*5, 65*/);
        $doneList = array(61, 54, 52, 1, 26, 23, 12, 25, 24, 49);
        $dl_sql = implode(",", array("61", "1", "12"));
        $rl_sql = implode(",", $rejectList);
        $sl_sql = $dl_sql . "," . $rl_sql;
        $tasktypes = array("" => "-- все --", "services" => "СКП", "connections" => "Подключение", "accidents" => "ТТ");
        $tasktypesUID = array("'services'", "'connections'", "'accidents'");

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");

        if ($USER->checkAccess("showfilter")) {
            $tpl->touchBlock("showfilter");
        } else {
            $tpl->touchBlock("hidefilter");
        }
        if (!isset($_REQUEST["filter_rpp"]))
            $_REQUEST["filter_rpp"] = 20;
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $_REQUEST['filter_sort_id'] = $this->getCookie('filter_sort_id', isset($sort_ids[$_REQUEST['filter_sort_id']]) ? $_REQUEST['filter_sort_id'] : key($sort_ids));
        $_REQUEST['filter_sort_order'] = $this->getCookie('filter_sort_order', isset($sort_order[$_REQUEST['filter_sort_order']]) ? $_REQUEST['filter_sort_order'] : key($sort_order));
        if (!isset($_REQUEST["filter_rpp"])) $_REQUEST["filter_rpp"] = $USER->getRpp();
        $_REQUEST["filter_task_id"] = $this->getCookie('filter_task_id', $_REQUEST['filter_task_id']);
        $_REQUEST["filter_id"] = $this->getCookie('filter_id', $_REQUEST['filter_id']);
        $_REQUEST["filter_qc_id"] = $this->getCookie('filter_qc_id', $_REQUEST['filter_qc_id']);
        $_REQUEST["filter_qcs_id"] = $this->getCookie('filter_qcs_id', $_REQUEST['filter_qcs_id']);
        $_REQUEST["techname"] = $this->getCookie('techname', $_REQUEST['techname']);
        $_REQUEST["filter_empl_id"] = $this->getCookie('filter_empl_id', $_REQUEST['filter_empl_id']);
        $_REQUEST["username"] = $this->getCookie('username', $_REQUEST['username']);
        $_REQUEST["filter_author_id"] = $this->getCookie('filter_author_id', $_REQUEST['filter_author_id']);
        $_REQUEST["filter_tasktype_id"] = $this->getCookie('filter_tasktype_id', $_REQUEST['filter_tasktype_id']);
        $_REQUEST["filter_contrqc_id"] = $this->getCookie('filter_contrqc_id', $_REQUEST['filter_contrqc_id']);
        if (!is_array($_REQUEST["filter_contrqc_id"]))
            $_REQUEST["filter_contrqc_id"] = array();
        $_REQUEST['filter_ti_date_from'] = $this->getCookie('filter_ti_date_from', $_REQUEST['filter_ti_date_from']);
        $_REQUEST['filter_ti_date_to'] = $this->getCookie('filter_ti_date_to', $_REQUEST['filter_ti_date_to']);
        $_REQUEST["filter_interval"] = $this->getCookie('filter_interval', $_REQUEST['filter_interval']);
        $_REQUEST['filter_qc_result'] = $this->getCookie("filter_qc_result", $_REQUEST["filter_qc_result"]);
        if (!$_REQUEST['filter_qc_result']) $_REQUEST['filter_qc_result'] = "0";
        if (!$_REQUEST['filter_ti_date_from']) $_REQUEST['filter_ti_date_from'] = date("d.m.Y", strtotime(date('Y-m-d') . " - 20 days"));
        if (!$_REQUEST['filter_ti_date_to']) $_REQUEST['filter_ti_date_to'] = date("d.m.Y");


        $tpl->setVariable('FILTER_TASKTYPE_OPTIONS', array2options($tasktypes, $_REQUEST['filter_tasktype_id']));
        $tpl->setVariable('FILTER_QC_OPTIONS', array2options($qc_status, $_REQUEST['filter_qc_id']));
        $tpl->setVariable('FILTER_QCS_OPTIONS', array2options($qc_cstatus, $_REQUEST['filter_qcs_id']));
        $tpl->setVariable('FILTER_TASK_ID', $_REQUEST['filter_task_id']);
        $tpl->setVariable('FILTER_INTERVAL_OPTIONS', array2options($interval_options, $_REQUEST['filter_interval']));

        $tpl->setVariable('FILTER_USER_NAME', $_REQUEST['username']);
        $tpl->setVariable('FILTER_AUTHOR_ID', $_REQUEST['filter_author_id']);
        $tpl->setVariable('FILTER_EMPL_NAME', $_REQUEST['techname']);
        $tpl->setVariable('FILTER_EMPL_ID', $_REQUEST['filter_empl_id']);
        $tpl->setVariable("FILTER_CONTR_OPTIONS", $this->getContrOptions($_REQUEST["filter_contrqc_id"]));
        $tpl->setVariable('FILTER_SORT_OPTIONS', array2options($sort_ids, $_REQUEST['filter_sort_id']));
        $tpl->setVariable('FILTER_ORDER_OPTIONS', array2options($sort_order, $_REQUEST['filter_sort_order']));
        $tpl->setVariable('FILTER_ROWS_PER_PAGE', array2options($rpp, $_REQUEST['filter_rpp']));
        $tpl->setVariable('FILTER_QCRES_OPTIONS', array2options($qcres, $_REQUEST['filter_qc_result']));
        $tpl->setVariable("FILTER_TI_DATE_FROM", $_REQUEST['filter_ti_date_from'] ? $_REQUEST['filter_ti_date_from'] : "");
        $tpl->setVariable("FILTER_TI_DATE_TO", $_REQUEST['filter_ti_date_to'] ? $_REQUEST['filter_ti_date_to'] : "");

        if ($_REQUEST["filter_task_id"] != "") $filter[] = "t.id=" . $DB->F($_REQUEST["filter_task_id"]);
        if ($_REQUEST["filter_tasktype_id"] != "") $filter[] = "t.plugin_uid=" . $DB->F($_REQUEST["filter_tasktype_id"]); else {
            $filter[] = "t.plugin_uid IN (" . implode(", ", $tasktypesUID) . ")";
        }
        if ($_REQUEST["filter_empl_id"] != "") $filter[] = "tu.user_id=" . $DB->F($_REQUEST["filter_empl_id"]) . " AND tu.ispoln";
        else
            $filter[] = "tu.ispoln";


        if ($_REQUEST["filter_author_id"]) {
            $filter[] = " qc.user_id=" . $DB->F($_REQUEST["filter_author_id"]) . " ";
        }

        if (count($_REQUEST["filter_contrqc_id"])) {
            $filter[] = "tick.cnt_id IN (" . implode(",", array_map([$DB, 'F'], (array)$_REQUEST["filter_contrqc_id"])) . ")";
        }

        ### EF: исключаем из обзвона task_type = 43 (Доставка sim), WF-133
        $filter[] = "tick.task_type not in (43)";
        $sql_join = "";
        switch ($_REQUEST["filter_interval"]) {
            case "1":
                if ($_REQUEST['filter_qc_id'] > 0)
                    $filter[] = $_REQUEST['filter_qc_id'] == 1 ? "t.status_id IN (" . implode(",", $rejectList) . ")" : "t.status_id IN (" . implode(",", $doneList) . ")";
                else {
                    $filter[] = "t.status_id IN (" . implode(",", $rejectList) . "," . implode(",", $doneList) . ")";
                }
                $filter[] = "DATE_FORMAT(t.date_reg, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['filter_ti_date_from'])));
                $filter[] = "DATE_FORMAT(t.date_reg, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['filter_ti_date_to'])));
                $filter[] = "ifnull(qc.qc_delayed,now()) <= date(now() + interval 1 day)"; ### просили убрать отложенные заявки из результатов поиска
                break;
            case "2":
                if ($_REQUEST['filter_qc_id'] > 0)
                    $d = $_REQUEST['filter_qc_id'] == 1 ? implode(",", $rejectList) : "1,12,61";
                else {
                    $d = implode(",", $rejectList) . ",1,12,61";
                }
                $filter[] = "t.id IN ((SELECT task_id FROM `task_comments` WHERE `status_id` IN (" . $d . ")
                                            AND (DATE_FORMAT(`datetime`, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['filter_ti_date_from']))) . " AND DATE_FORMAT(`datetime`, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['filter_ti_date_to']))) . ")))";
                $filter[] = "ifnull(qc.qc_delayed,now()) <= date(now() + interval 1 day)"; ### просили убрать отложенные заявки из результатов поиска
                //$sql_join = "LEFT JOIN `task_comments` as tc ON tc.task_id=t.id";
                break;
            case "3":
                if ($_REQUEST['filter_qc_id'] > 0)
                    $filter[] = $_REQUEST['filter_qc_id'] == 1 ? "t.status_id IN (" . implode(",", $rejectList) . ")" : "t.status_id IN (" . implode(",", $doneList) . ")";
                else {
                    $filter[] = "t.status_id IN (" . implode(",", $rejectList) . "," . implode(",", $doneList) . ")";
                }
                $filter[] = "DATE_FORMAT(qc.qc_date, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['filter_ti_date_from'])));
                $filter[] = "DATE_FORMAT(qc.qc_date, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['filter_ti_date_to'])));
                $filter[] = "ifnull(qc.qc_delayed,now()) <= date(now() + interval 1 day)"; ### просили убрать отложенные заявки из результатов поиска
                break;

            case "4":
                if ($_REQUEST['filter_qc_id'] > 0)
                    $filter[] = $_REQUEST['filter_qc_id'] == 1 ? "t.status_id IN (" . implode(",", $rejectList) . ")" : "t.status_id IN (" . implode(",", $doneList) . ")";
                else {
                    $filter[] = "t.status_id IN (" . implode(",", $rejectList) . "," . implode(",", $doneList) . ")";
                }
                $filter[] = "DATE_FORMAT(qc.qc_delayed, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['filter_ti_date_from'])));
                $filter[] = "DATE_FORMAT(qc.qc_delayed, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['filter_ti_date_to'])));
                break;
            default:
                if ($_REQUEST['filter_qc_id'] > 0)
                    $filter[] = $_REQUEST['filter_qc_id'] == 1 ? "t.status_id IN (" . implode(",", $rejectList) . ")" : "t.status_id IN (" . implode(",", $doneList) . ")";
                else {
                    $filter[] = "t.status_id IN (" . implode(",", $rejectList) . "," . implode(",", $doneList) . ")";
                }
                $filter[] = "DATE_FORMAT(t.date_reg, '%Y-%m-%d')>=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['filter_ti_date_from'])));
                $filter[] = "DATE_FORMAT(t.date_reg, '%Y-%m-%d')<=" . $DB->F(date("Y-m-d", strtotime($_REQUEST['filter_ti_date_to'])));
                $filter[] = "ifnull(qc.qc_delayed,now()) <= date(now() + interval 1 day)"; ### просили убрать отложенные заявки из результатов поиска
                break;
        }
        ### EF: фильтр по признаку включения в обзвон
        $filter[] = "exists (select * from link_agr_wtypes law where law.is_qc > 0 and law.agr_id = tick.agr_id)";
        $order = $sort_sql[$_REQUEST['filter_sort_id']] . " " . $_REQUEST['filter_sort_order'];
        if ($_REQUEST["filter_qcs_id"] > 0) {
            $filter[] = "qc.user_id";
            $filter[] = "qc.qc_status=" . $DB->F($_REQUEST["filter_qcs_id"]) . "";
            if ($_REQUEST["filter_qcs_id"] == 1) {
                switch ($_REQUEST["filter_qc_result"]) {
                    case "1":
                        $filter[] = "(qc.qc_new_result==100)";
                        break;

                    case "2":
                        $filter[] = "(qc.qc_new_result>=90 AND qc.qc_new_result<=99)";
                        break;

                    case "3":
                        $filter[] = "(qc.qc_new_result>=86 AND qc.qc_new_result<=89)";
                        break;
                    case "4":
                        $filter[] = "(qc.qc_new_result<86)";
                        break;


                    default:
                        break;
                }
            }
            $filter = implode("\n AND ", $filter);
            $sql = "SELECT SQL_CALC_FOUND_ROWS
                    t.*, (SELECT `contr_title` FROM `list_contr` WHERE `id`=tick.cnt_id) as contr_name,
                    (SELECT `fio` FROM `users` WHERE `id`=tu.user_id) as user_name, tu.user_id,
                    (SELECT `name` FROM `task_status` WHERE `id`=t.status_id) as status_name,
                    (SELECT `color` FROM `task_status` WHERE `id`=t.status_id) as status_color,
                    (SELECT `fio` FROM `users` WHERE `id`=qc.user_id) as csuser,
                    qc.qc_date as csdate, qc.qc_new_result as csresult, qc.user_id, qc.qc_status, qc.qc_delayed,
                    (SELECT MAX(`datetime`) FROM `task_comments` WHERE `task_id`=tick.task_id AND `status_id` IN (" . $sl_sql . ")) as statusdate
                    FROM `tasks` AS t
                                LEFT JOIN `tickets` AS tick ON tick.task_id=t.id
                                LEFT JOIN `task_users` AS tu ON tu.task_id=t.id
                                LEFT JOIN `tickets_qc` AS qc ON qc.task_id=t.id
                                $sql_join
                    WHERE $filter /* GROUP BY t.id */
                    ORDER BY $order
                    LIMIT {$_REQUEST['filter_start']}," . intval($_REQUEST["filter_rpp"]);
        } else {
            # EF: Так фильтровать не следует, иначе рванет при переполнении буфера сортировки
            #$filter[] = "t.id NOT IN (SELECT `task_id` FROM `tickets_qc` WHERE `qc_status`>0)";
            $filter[] = "( not exists ( select * from tickets_qc qc where qc.qc_status>0 and qc.task_id = t.id) or qc.qc_status=3 )";
            $filter[] = "t.status_id not in (5,65)";
            $filter = implode("\n AND ", $filter);
            $sql = "SELECT SQL_CALC_FOUND_ROWS
                    t.*, (SELECT `contr_title` FROM `list_contr` WHERE `id`=tick.cnt_id) as contr_name,
                    (SELECT `fio` FROM `users` WHERE `id`=tu.user_id) as user_name, tu.user_id,
                    (SELECT `name` FROM `task_status` WHERE `id`=t.status_id) as status_name,
                    (SELECT `color` FROM `task_status` WHERE `id`=t.status_id) as status_color,
                    qc.qc_date as csdate, qc.qc_new_result as csresult, qc.user_id, qc.qc_status, qc.qc_delayed,
                    (SELECT MAX(`datetime`) FROM `task_comments` WHERE `task_id`=tick.task_id AND `status_id` IN (" . $sl_sql . ")) as statusdate
                    FROM `tasks` AS t
                                LEFT JOIN `tickets` as tick ON tick.task_id=t.id
                                LEFT JOIN `task_users` as tu ON tu.task_id=t.id
                                LEFT JOIN `tickets_qc` AS qc ON qc.task_id=t.id
                                $sql_join
                    WHERE $filter /* GROUP BY t.id */
                    ORDER BY qc.qc_delayed DESC, $order
                    LIMIT {$_REQUEST['filter_start']}," . intval($_REQUEST["filter_rpp"]);

        }
        # EF: отладочная информация
        #error_log("count: ".count($_REQUEST["filter_contrqc_id"])."\n\n\n",3,"/ramdisk/sql.log");
        #error_log("request: ".print_r($_REQUEST,true)."\n\n\n",3,"/ramdisk/sql.log");
        #error_log("SQL: ".$sql."\n\n\n",3,"/ramdisk/sql.log");

        #echo "<!-- \n";
        #print_r($_REQUEST);
        #print $sql."\n";
        #echo "-->\n";
        #exit;
        $DB->query($sql);
        $total = $DB->getFoundRows();
        if ($DB->errno())
            UIError($DB->error() . $sql);
        if (!$DB->num_rows()) {
            $tpl->touchBlock("norecords");
        }
        while ($rlist = $DB->fetch(true)) {
            $tpl->setCurrentBlock("vl_item");
            $tpl->setVariable("LINKTOTASK", link2($rlist["plugin_uid"] . "/viewticket?task_id=" . $rlist["id"], false));
            $tpl->setVariable("VLI_TASKID", $rlist["id"]);
            $tpl->setVariable("CONTR_NAME", $rlist["contr_name"]);
            $tpl->setVariable("TASK_TYPE", $tasktypes[$rlist["plugin_uid"]]);
            $tpl->setVariable("TASK_USER", $rlist["user_name"]);
            $tpl->setVariable("TASK_STATUS", $rlist["status_name"]);
            $tpl->setVariable("TASK_STATUSDATE", $rlist["statusdate"]);
            $tpl->setVariable("VLI_STATUSCOLOR", "style='background-color: " . $rlist["status_color"] . ";'");
            if ($rlist['qc_delayed'] && $rlist["qc_status"] == 0) {
                $tpl->setVariable("CALLSTATE", "Отложен до " . $rlist['qc_delayed']);
                $tpl->setVariable("BLINK_DELAYED_QC", "class='highlight_red'");
            } else {
                $tpl->setVariable("CALLSTATE", $qc_cstatus[$rlist['qc_status']] ? $qc_cstatus[$rlist['qc_status']] : "&mdash;");
            }
            if ($rlist['qc_status'] == 3) {
                $tpl->setCurrentBlock("delay");
                $tpl->setVariable("CALLSTATE_DELAYED", $rlist["qc_delayed"]);
                $tpl->parse("delay");
            }
            switch ($rlist["qc_status"]) {
                case "1":
                    $tpl->setVariable("VLI_CALLSTATUSCOLOR", "style='background-color: #66D943;'");
                    break;

                case "2":
                    $tpl->setVariable("VLI_CALLSTATUSCOLOR", "style='background-color: #E35252;'");
                    break;

                case "3":
                    $tpl->setVariable("VLI_CALLSTATUSCOLOR", "style='background-color: #FFD900;'");
                    break;

                default:
                    $tpl->setVariable("VLI_CALLSTATUSCOLOR", "style='background-color: #FFFFFF;'");
                    break;
            }
            $tpl->setVariable("CALLDATE", $rlist['csdate'] ? $rlist['csdate'] : "&mdash;");
            $tpl->setVariable("CALLRESULT", $rlist['csresult'] != "" ? $rlist['csresult'] : "&mdash;");
            if ($rlist["csresult"] != "") {
                if ($rlist["csresult"] == 100) {
                    $tpl->setVariable("CALLRESULT_STYLE", "style='background: #B0FFB7; font-weight: bold;' title='Отлично'");
                } else {
                    if ($rlist["csresult"] >= 90 && $rlist["csresult"] <= 99) {
                        $tpl->setVariable("CALLRESULT_STYLE", "style='background: #00ffd2; font-weight: bold;' title='Удовлетворительно'");
                    } else {
                        if ($rlist["csresult"] >= 86 && $rlist["csresult"] <= 89) {
                            $tpl->setVariable("CALLRESULT_STYLE", "style='background: #fff600; font-weight: bold;' title='Плохо'");
                        } else {
                            if ($rlist["csresult"] == 0 && ($rlist['qc_status'] == "2" || $rlist['qc_status'] == "3")) {
                                $tpl->setVariable("CALLRESULT_STYLE", "style='background: inherit; color: #000000; font-weight: bold;'  title='Отказ от опроса'");
                            } else {
                                if ($rlist["csresult"] < 86)
                                    $tpl->setVariable("CALLRESULT_STYLE", "style='background: #FF0000; color: #FFFFFF; font-weight: bold;'  title='Очень плохо'");
                            }
                        }
                    }
                }
            }
            $tpl->setVariable("VLI_USER", $rlist['csuser'] ? $rlist['csuser'] : "&mdash;");
            $tpl->parse("vl_item");
        }
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], $_REQUEST["filter_rpp"],
            $total, "#start-%s"));
        UIHeader($tpl);
        $tpl->show();

    }

    static function get_current_obzvon_data($task_id)
    {
        global $DB;
        if (!$task_id) return false;
        $sql = "SELECT * FROM `tickets_qc` WHERE `task_id`=" . $DB->F($task_id) . ";";
        $result = $DB->getRow($sql, true);
        if ($result) return $result;
        else return -1;
    }

    public function saveoprosform_ajax(Request $request)
    {
        $task_id = $request->get('task_id');
        $qlist = $request->get('qlist');

        if (!($task_id && $qlist)) {
            throw new BadRequestHttpException('"task_id" and "qlist" field required');
//            echo json_encode(['error' => 'Ошибка получения состояния Обзвона по заявке!']);
//            return false;
        }

        $em = $this->getEm();
        /* @var $task OrmTask */
        $task = $em->find(OrmTask::class, $task_id);

        $em->transactional(function (EntityManagerInterface $em) use ($task, $qlist, $request) {
            $em->createQueryBuilder()
                ->delete(models\TaskQcResult::class, 'qr')
                ->where('qr.task = :task')
                ->setParameter('task', $task)
                ->getQuery()->execute();

            $q_mark_result = 0;
            $q_new_mark_result = 0;

            foreach ($qlist as $oid) {
                $answer = $request->get('answer' . $oid);
                if ($answer) {
                    $e = explode(":", $answer);
                    $q_res = $e[0] == "yes" ? 2 : 1;
                    $q_amark = (int)$e[1];
                    $q_mark_result += $q_amark;
                    $newmark = 0;
                    if ($q_amark > 0) {
                        $question = $em->find(\models\QcQuestion::class, $oid);
                        $newmark = $question->getWeight();
                    }
                    $q_new_mark_result += $newmark;
                } else {
                    $mbanswer = $request->get('mbanswer' . $oid);
                    if ($mbanswer) {
                        $e = explode(":", $mbanswer);
                        $q_res = $e[0] == "yes" ? 5 : ($e[0] == "no" ? 0 : 3);
                        $q_amark = (int)$e[1];
                        $q_mark_result += $q_amark;
                        $newmark = 0;
                        if ($q_amark !== 0) {
                            $question = $em->find(\models\QcQuestion::class, $oid);
                            $weight = $question->getWeight();
                            $newmark = $q_amark === 5 ? $weight : round($weight / 2, 1);
                        }

                        $q_new_mark_result += $newmark;
                    } else {
                        $q_res = 0;
                        $q_amark = 0;
                        $newmark = 0;
                    }
                }

                $bcount = $request->get('bcount' . $oid);
                if ($bcount) {
                    $q_mark = $bcount;
                    $q_mark_result += $q_mark;
                } else {
                    $q_mark = 0;
                }

                $ammount = $request->get('ammount' . $oid);
                if ($ammount) {
                    $q_amm = $ammount;
                } else {
                    $q_amm = 0;
                }

                $comment = $request->get('comment' . $oid);
                if ($comment) {
                    $q_cmm = $comment;
                } else {
                    $q_cmm = "";
                }

                $qcResult = (new models\TaskQcResult())
                    ->setTask($task)
                    ->setQuestion($em->getReference(\models\QcQuestion::class, $oid))
                    ->setResult($q_res)
                    ->setAnswerMark($newmark)
                    ->setNewAnswerMark($newmark)
                    ->setMark($q_mark)
                    ->setAmmount($q_amm)
                    ->setComment($q_cmm);
                $em->persist($qcResult);
                $em->flush();
            }

            $qc = $em->getRepository(\models\TaskQc::class)
                ->findOneBy(['task' => $task]);
            if ($qc === null) {
                $qc = (new \models\TaskQc())
                    ->setTask($task);
                $em->persist($qc);
            }

            $user = $em->getReference(models\User::class, $this->getUserId());

            $qc->setResult($q_mark_result)
                ->setNewResult($q_new_mark_result)
                ->setStatus(1)
                ->setComment($request->get('oproscomment'))
                ->setUser($user);
            $task->setQc($qc);

            $comment = (new models\TaskComment())
                ->setAuthor($user)
                ->setText('Результат: ' . $q_new_mark_result)
                ->setTag('Выполнен опрос/обзвон')
                ->setTask($task);

            $em->persist($comment);

            $em->flush();
        });

        $event = new TaskQcChangedEvent($task);
        $this->getDispatcher()->dispatch(TaskQcChangedEvent::NAME, $event);

        echo json_encode(['ok' => 'ok']);
        return false;
    }

    static function loadreject_ajax()
    {
        global $DB, $USER;
        $task_id = $_REQUEST["task_id"];
        $comment = $_REQUEST["cmm"];
        if (!$task_id || !$comment) {
            $ret["error"] = "Ошибка! Недостаточно данных!";
            echo json_encode($ret);
            return false;

        }
        $op = $DB->getField("SELECT `id` FROM `tickets_qc` WHERE `task_id`=" . $DB->F($task_id) . ";");
        if ($op)
            $sql = "UPDATE `tickets_qc` SET `qc_new_result`=0,`qc_result`=0, `qc_status`=2, `qc_delayed`=" . $DB->F('', true) . ", `comment`=" . $DB->F($comment) . ", `user_id`=" . $DB->F($USER->getId()) . ", qc_date=now() WHERE `id`=" . $DB->F($op) . ";";
        else
            $sql = "INSERT INTO `tickets_qc` (`task_id`, `qc_result`, `qc_new_result`, `qc_status`, `comment`, `user_id`, qc_date) VALUES
        (" . $DB->F($task_id) . ", " . $DB->F(0) . ", " . $DB->F(0) . ", " . $DB->F(2) . ", " . $DB->F($comment) . ", " . $DB->F($USER->getId()) . ", now());";
        $DB->query($sql);
        if ($DB->errno()) $ret["error"] = $DB->error();
        else {
            $ret["ok"] = "ok";
            $t = new Task($task_id);
            if ($t) {
                $t->addComment("Комментарий клиента: " . $comment, "Отказ клиента от опроса по качеству обслуживания");
                $ret["tpl"] = $t->getLastComment();
            }
        }
        echo json_encode($ret);
        return false;
    }

    static function loaddelay_ajax()
    {
        global $DB, $USER;
        $task_id = $_REQUEST["task_id"];
        $comment = $_REQUEST["cmm"];
        $date = $_REQUEST["delayto"];
        $time = $_REQUEST["delaytotime"];
        if (!$task_id || !$comment || !$date || !$time) {
            $ret["error"] = "Ошибка! Недостаточно данных!";
            echo json_encode($ret);
            return false;
        }
        $date = date("Y-m-d H:i:s", strtotime($date . " " . $time));
        $query = "SELECT `id` FROM `tickets_qc` WHERE `task_id`=" . $DB->F($task_id) . ";";
        $op = $DB->getField($query);
        if ($op)
            $sql = "UPDATE `tickets_qc` SET `qc_new_result`=0,`qc_result`=0, `qc_status`=3, `qc_delayed`=" . $DB->F($date) . ", `comment`=" . $DB->F($comment) . ", `user_id`=" . $DB->F($USER->getId()) . ", qc_date=now() WHERE `id`=" . $DB->F($op) . ";";
        else
            $sql = "INSERT INTO `tickets_qc` (`task_id`, `qc_result`, `qc_new_result`, `qc_status`, `qc_delayed`, `comment`, `user_id`, qc_date) VALUES
        (" . $DB->F($task_id) . ", " . $DB->F(0) . ", " . $DB->F(0) . ", " . $DB->F(3) . ", " . $DB->F($date) . ", " . $DB->F($comment) . ", " . $DB->F($USER->getId()) . ", now());";
        $DB->query($sql);

        if ($DB->errno())
            $ret["error"] = $DB->error();
        else {
            $ret["ok"] = "ok";
            $t = new Task($task_id);
            if ($t) {
                $t->addComment("Комментарий клиента: " . $comment, "Перенос опроса по качеству обслуживания. Дата: " . $date);
                $ret["tpl"] = $t->getLastComment();
            }
        }
        echo json_encode($ret);
        return false;
    }


    static function loadoprosform_ajax(Request $request)
    {
        global $DB, $USER;

        $task_id = $request->get("task_id");
        $plugin_uid = $_REQUEST["plugin_uid"];
        $current_status = $request->get("current_status");
        $force_reopen = $request->get("force_reopen");
        if (!$task_id || !$plugin_uid || !$current_status) {
            $ret["error"] = "Отсутствуют необходимые данные для запуска формы Обзвона";
            echo json_encode($ret);
            return false;
        }

        $contr_id = $DB->getField("SELECT `cnt_id` FROM `tickets` WHERE `task_id`=" . $DB->F($task_id) . ";");

        $currentObzvonData = self::get_current_obzvon_data($task_id);
        if ($currentObzvonData !== false) {
            $tpl = new HTML_Template_IT(path2("plugins/qc"));
            if ($currentObzvonData == -1 || $currentObzvonData["qc_status"] == "0" || $force_reopen == 1 || $currentObzvonData["qc_status"] == "3" || ($currentObzvonData["qc_status"] == "1" && $USER->checkAccess("qc_editdones"))) {

                /*
                if ($USER->checkAccess("qc_editdones")) {
                        $exopros = true;
                        if ($USER->getTemplate() != "default")
                        $tpl->loadTemplatefile($USER->getTemplate() . "/writeopros.tmpl.htm");
                        else
                            $tpl->loadTemplatefile("writeopros.tmpl.htm");
                    } else {

                */


                $fq = false;
                if ($currentObzvonData["qc_status"] == "1" && $USER->checkAccess("qc_editdones")) {
                    $list = q_list_plugin::getQByStatusWA($plugin_uid, $current_status, $task_id, $contr_id);
                    $fq = true;

                    if (date("Y-m-d", strtotime($currentObzvonData["qc_date"])) < "2015-07-09") {

                        $tpl->loadTemplatefile($USER->getTemplate() . "/readonlyopros.tmpl.htm");
                        $tpl->setVariable("RUNDATE", date("H:i d.m.Y", strtotime($currentObzvonData["qc_date"])));
                        $tpl->setVariable("RUNUSER", $DB->getField("SELECT `fio` FROM `users` WHERE `id`=" . $DB->F($currentObzvonData["user_id"]) . ";"));

                    } else {
                        if ($USER->getTemplate() != "default") {
                            $tpl->loadTemplatefile($USER->getTemplate() . "/writeopros.tmpl.htm");

                        } else
                            $tpl->loadTemplatefile("writeopros.tmpl.htm");
                    }
                } else {
                    $list = q_list_plugin::getQByStatus($plugin_uid, $current_status, $contr_id);


                    if ($USER->getTemplate() != "default")
                        $tpl->loadTemplatefile($USER->getTemplate() . "/newopros.tmpl.htm");
                    else
                        $tpl->loadTemplatefile("newopros.tmpl.htm");
                }

                $i = 1;
                if ($list) {
                    if ($currentObzvonData["qc_status"] == "3") {
                        $tpl->setCurrentBlock("wasdelayed");
                        $tpl->setVariable("DELAYEDDATE", $currentObzvonData["qc_delayed"]);
                        $tpl->setVariable("DELAYEDCOMMENT", $currentObzvonData["comment"] ? $currentObzvonData["comment"] : "&mdash;");
                        $tpl->parse("wasdelayed");
                    }
                    $tpl->setCurrentBlock("run");
                    $class = "odd";
                    $total_mark_result = false;
                    foreach ($list as $key => $q) {
                        $tpl->setVariable("QTASK_ID", $task_id);
                        $tpl->setCurrentBlock("qlist");
                        $tpl->setVariable("QID_ITEM", $q["id"]);
                        $tpl->parse("qlist");
                        $tpl->setCurrentBlock("oitem");
                        if ($class == "odd") {
                            $class = "even";
                        } else {
                            $class = "odd";
                        }
                        $tpl->setVariable("ADDCLASS", $class);
                        $tpl->setVariable("QID", $q["id"]);
                        $tpl->setVariable("NUMBER", $i);
                        if ($q["qtext"] == "" && $q["qtextdesc"] != "") {
                            $tpl->setVariable("TITLE", preg_replace("/\r\n/", "<br />", $q["qtextdesc"]));
                        } else {
                            $tpl->setVariable("TITLE", $q["qtext"]);
                            $tpl->setVariable("DESC", $q["qtextdesc"]);
                        }
                        if ($q["q_yesno"] == 1) {
                            $tpl->setCurrentBlock("qyesno");
                            $weight = $DB->getField("SELECT `weight` FROM `qc_qlist` WHERE `id`=" . $DB->F($q["id"]) . ";");
                            if ($fq) {
                                $qrv = q_list_plugin::getQByStatusWAQID($q["id"], $task_id);
                                $tpl->setVariable("YESNO_ANSWER_YES", $qrv["result"] == 2 ? "checked='checked'" : "");
                                $tpl->setVariable("YESNO_ANSWER_NO", $qrv["result"] != 2 ? "checked='checked'" : "");
                                $total_mark_result += $qrv["answer_mark"];
                            }
                            //$tpl->setVariable("TITLE", $weight);
                            //die($weight);
                            $mark = $q["answer_mark"] != 0 ? ($q["answer_mark"] > 0 ? "(" . $q["answer_mark"] . ")" : "(<font color='#FF0000'>" . $q["answer_mark"] . "</font>)") : "";
                            $tpl->setVariable("YESNO_ANSWER", $q["result"] == 2 ? "Да $mark" : "Нет $mark");

                            $tpl->setVariable("QYNCLASS", $class);
                            $tpl->setVariable("QYNID", $q["id"]);
                            $tpl->setVariable("YESVAL", $q["yes_count"] > 0 ? $weight : 0);
                            $tpl->setVariable("NOVAL", $q["no_count"] > 0 ? $weight : 0);
                            if ($q["yes_count"] != 0) {
                                //$tpl->setVariable("YESVAL_TXT", " (".($q["yes_count"] > 0 ? "<strong>+".$q["yes_count"]."</strong>" : "<strong style='color: red;'>".$q["yes_count"]."</strong>").") ");
                            }
                            if ($q["no_count"] != 0) {
                                //$tpl->setVariable("NOVAL_TXT", " (".($q["no_count"] > 0 ? "<strong>+".$q["no_count"]."</strong>" : "<strong style='color: red;'>".$q["no_count"]."</strong>" ).") ");
                            }
                            $tpl->parse("qyesno");
                        } else {
                            if ($q["q_mbyesno"] == 1) {
                                $tpl->setCurrentBlock("qyesmbno");
                                $weight = $DB->getField("SELECT `weight` FROM `qc_qlist` WHERE `id`=" . $DB->F($q["id"]) . ";");
                                //$tpl->setVariable("TITLE", $weight);
                                if ($fq) {
                                    $qrv = q_list_plugin::getQByStatusWAQID($q["id"], $task_id);
                                    $tpl->setVariable("YESMBNO_ANSWER_YES", $qrv["result"] == 5 ? "checked='checked'" : "");
                                    $tpl->setVariable("YESMBNO_ANSWER_NO", $qrv["result"] == 0 ? "checked='checked'" : "");
                                    $tpl->setVariable("YESMBNO_ANSWER_MB", $qrv["result"] == 3 ? "checked='checked'" : "");
                                    //$weight = $DB->getField("SELECT `weight` FROM `qc_qlist` WHERE `id`=".$DB->F($q["id"]).";");
                                    $total_mark_result += $qrv["answer_mark"];
                                }
                                $tpl->setVariable("QYMBNCLASS", $class);
                                $tpl->setVariable("QYMBNID", $q["id"]);
                                $tpl->setVariable("YW", $weight);
                                $tpl->setVariable("MBW", round($weight / 2));
                                $tpl->setVariable("NW", "0");
                                $tpl->parse("qyesmbno");
                            } else {
                                if ($q["q_maxcount"] > 0) {
                                    $tpl->setCurrentBlock("qball");
                                    if ($fq) {
                                        $qrv = q_list_plugin::getQByStatusWAQID($q["id"], $task_id);
                                        $tpl->setVariable("QBALL_ANSWER_VAL", $qrv["mark"]);
                                        $total_mark_result += $qrv["mark"];
                                        $tpl->setVariable("QBALL_ANSWER", $qrv["mark"] > 0 ? $qrv["mark"] : "<font color='#FF0000'>" . $qrv["mark"] . "</font>");

                                    }
                                    $tpl->setVariable("QBID", $q["id"]);
                                    $tpl->setVariable("QBALLCLASS", $class);
                                    $tpl->setVariable("QBALLMAX", $q["q_maxcount"]);
                                    $tpl->parse("qball");
                                }
                            }
                        }
                        $totalrowspan = 1;
                        if ($q["req_ammount"] > 0) {
                            $totalrowspan += 1;
                            $tpl->setCurrentBlock("ammount");

                            if ($fq) {
                                $qrv = q_list_plugin::getQByStatusWAQID($q["id"], $task_id);
                                $tpl->setVariable("AMMOUNT_ANSWER", number_format($qrv["ammount"], 2, ",", " "));
                                $tpl->setVariable("AMMOUNT_ANSWER_VAL", $qrv["ammount"]);
                            }
                            $tpl->setVariable("AADDCLASS", $class);
                            $tpl->setVariable("QAID", $q["id"]);
                            $tpl->parse("ammount");
                        }
                        if ($q["req_comment"] > 0) {
                            $totalrowspan += 1;
                            $tpl->setCurrentBlock("comment");
                            if ($fq) {
                                $qrv = q_list_plugin::getQByStatusWAQID($q["id"], $task_id);
                                $tpl->setVariable("COMMENT_ANSWER_VAL", $qrv["comment"]);
                                $tpl->setVariable("COMMENT_ANSWER", $qrv["comment"] ? $qrv["comment"] : "&mdash;");
                            }
                            $tpl->setVariable("CADDCLASS", $class);
                            $tpl->setVariable("QCID", $q["id"]);
                            $tpl->parse("comment");
                        }
                        $tpl->setVariable("TOTALROWSPAN", $totalrowspan);
                        $tpl->parse("oitem");
                        $i += 1;
                    }
                    $tpl->setCurrentBlock("oproscomment");
                    $tpl->setVariable("CQTASK_ID", $task_id);
                    $tpl->setVariable("RES_COMMENT_VAL1", $currentObzvonData["comment"] ? $currentObzvonData["comment"] : "");
                    $tpl->parse("oproscomment");
                    if ($total_mark_result !== false) {
                        $tpl->setVariable("OPR_RESULT", $total_mark_result >= 0 ? "<font color='#057a08'>" . $total_mark_result . "</font>" : $total_mark_result);
                    } else {
                        $tpl->setVariable("OPR_RESULT", "&mdash;");
                    }
                    $tpl->setVariable("RES_COMMENT_VAL", $currentObzvonData["comment"] ? $currentObzvonData["comment"] : "");
                    $tpl->setVariable("RES_COMMENT", $currentObzvonData["comment"] ? $currentObzvonData["comment"] : "&mdash;");


                    $tpl->parse("run");
                } else {
                    $tpl->touchBlock("noitems");
                }
            } else {
                if ($currentObzvonData["qc_status"] == 2) {
                    if ($USER->getTemplate() != "default")
                        $tpl->loadTemplatefile($USER->getTemplate() . "/rejectedopros.tmpl.htm");
                    else
                        $tpl->loadTemplatefile("rejectedopros.tmpl.htm");

                    $tpl->setCurrentBlock("wasdelayed");
                    $tpl->setVariable("REJECT_DATE", $currentObzvonData["qc_date"]);
                    $tpl->setVariable("DELAYEDCOMMENT", $currentObzvonData["comment"] ? $currentObzvonData["comment"] : "&mdash;");
                    $tpl->parse("wasdelayed");


                } else {
                    $exopros = false;
                    if ($USER->getTemplate() != "default")
                        $tpl->loadTemplatefile($USER->getTemplate() . "/readonlyopros.tmpl.htm");
                    else
                        $tpl->loadTemplatefile("readonlyopros.tmpl.htm");

                    $list = q_list_plugin::getQByStatusWA($plugin_uid, $current_status, $task_id, $contr_id);
                    $i = 1;
                    $total_mark_result = 0;
                    if ($list) {

                        $tpl->setCurrentBlock("run");

                        $tpl->setVariable("QTASK_ID", $task_id);
                        $tpl->setVariable("RUNDATE", date("H:i d.m.Y", strtotime($currentObzvonData["qc_date"])));
                        $tpl->setVariable("RUNUSER", $DB->getField("SELECT `fio` FROM `users` WHERE `id`=" . $DB->F($currentObzvonData["user_id"]) . ";"));
                        $class = "odd";
                        foreach ($list as $key => $q) {
                            $tpl->setCurrentBlock("qlist");
                            $tpl->setVariable("QID_ITEM", $q["id"]);
                            $tpl->parse("qlist");
                            $tpl->setVariable("OP_TASK_ID", $task_id);
                            if (($q["mark"] > 0 || $q["result"] > 0 || $q["ammount"] > 0 || $q["comment"]) && $q["qtext"]) {
                                $tpl->setCurrentBlock("oitem");
                                if ($class == "odd") {
                                    $class = "even";
                                } else {
                                    $class = "odd";
                                }
                                $tpl->setVariable("ADDCLASS", $class);
                                $tpl->setVariable("QID", $q["id"]);
                                $tpl->setVariable("NUMBER", $i);
                                if ($q["qtext"] == "" && $q["qtextdesc"] != "") {
                                    $tpl->setVariable("TITLE", preg_replace("/\r\n/", "<br />", $q["qtextdesc"]));
                                } else {
                                    $tpl->setVariable("TITLE", $q["qtext"]);

                                }
                                if ($q["q_yesno"] == 1) {
                                    $tpl->setCurrentBlock("qyesno");
                                    $tpl->setVariable("QYNCLASS", $class);
                                    $tpl->setVariable("QYNID", $q["id"]);
                                    $mark = $q["answer_mark"] != 0 ? ($q["answer_mark"] > 0 ? "(" . $q["answer_mark"] . ")" : "(<font color='#FF0000'>" . $q["answer_mark"] . "</font>)") : "";
                                    $tpl->setVariable("YESNO_ANSWER", $q["result"] == 2 ? "Да $mark" : "Нет $mark");
                                    if ($q["result"] == 2) {
                                        $tpl->setVariable("YESVAL", $q["answer_mark"]);
                                    } else {
                                        $tpl->setVariable("NOVAL", $q["answer_mark"]);
                                    }
                                    $tpl->setVariable("YESNO_ANSWER_YES", $q["result"] == 2 ? "checked='checked'" : "");
                                    $tpl->setVariable("YESNO_ANSWER_NO", $q["result"] != 2 ? "checked='checked'" : "");
                                    $total_mark_result += $q["new_answer_mark"] > 0 ? $q["new_answer_mark"] : $q["answer_mark"];


                                    $tpl->parse("qyesno");
                                } else {
                                    if ($q["q_mbyesno"] == 1) {
                                        $tpl->setCurrentBlock("qyesmbno");
                                        $tpl->setVariable("QYMBNCLASS", $class);
                                        $tpl->setVariable("QYMBNID", $q["id"]);

                                        $tpl->setVariable("YESMBNO_ANSWER", $q["result"] > 3 ? "Да" : (($q["result"] <= 3 && $q["result"] > 0) ? "Частично" : "Нет"));

                                        $total_mark_result += $q["new_answer_mark"] ? $q["new_answer_mark"] : $q["answer_mark"];;

                                        $tpl->parse("qyesmbno");
                                    } else {

                                        if ($q["q_maxcount"] > 0) {
                                            $tpl->setCurrentBlock("qball");
                                            $tpl->setVariable("QBID", $q["id"]);
                                            $tpl->setVariable("QBALLCLASS", $class);
                                            $total_mark_result += $q["mark"];
                                            $tpl->setVariable("QBALL_ANSWER", $q["mark"] > 0 ? $q["mark"] : "<font color='#FF0000'>" . $q["mark"] . "</font>");
                                            $tpl->setVariable("QBALL_ANSWER_VAL", $q["mark"]);

                                            $tpl->parse("qball");
                                        }
                                    }
                                }
                                $totalrowspan = 1;
                                if ($q["req_ammount"] > 0) {
                                    $totalrowspan += 1;
                                    $tpl->setCurrentBlock("ammount");
                                    $tpl->setVariable("AADDCLASS", $class);
                                    $tpl->setVariable("AMMOUNT_ANSWER_VAL", $q["ammount"]);
                                    $tpl->setVariable("AMMOUNT_ANSWER", number_format($q["ammount"], 2, ",", " "));
                                    $tpl->parse("ammount");
                                }
                                if ($q["req_comment"] > 0) {
                                    $totalrowspan += 1;
                                    $tpl->setCurrentBlock("comment");
                                    $tpl->setVariable("CADDCLASS", $class);
                                    $tpl->setVariable("COMMENT_ANSWER", $q["comment"] ? $q["comment"] : "&mdash;");
                                    $tpl->setVariable("COMMENT_ANSWER_VAL", $q["comment"]);
                                    $tpl->parse("comment");
                                }
                                $tpl->setVariable("TOTALROWSPAN", $totalrowspan);
                                $tpl->parse("oitem");
                                $i += 1;
                            }
                        }
                        $tpl->setVariable("OPR_RESULT", $total_mark_result >= 0 ? "<font color='#057a08'>" . $total_mark_result . "</font>" : $total_mark_result);
                        $tpl->setVariable("RES_COMMENT", $currentObzvonData["comment"] ? $currentObzvonData["comment"] : "&mdash;");
                        $tpl->parse("run");
                    } else {
                        $tpl->touchBlock("noitems");
                    }

                }
            }
            $ret["tpl"] = $tpl->get();
        } else {
            $ret["error"] = "Ошибка получения состояния Обзвона по заявке!";
            echo json_encode($ret);
            return false;
        }

        echo json_encode($ret);
        exit;
    }

}
