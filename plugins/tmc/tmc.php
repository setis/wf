<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use classes\User;
use Symfony\Component\HttpFoundation\Request;

/**
 * рекомендуется чтобы он наследовал класс Plugin
 * в конструкторе не забывайте вызвать parent::__construct($plugin_uid); (!)
 * @see classes/Plugin.php
 *
 * нужные методы класса соответствуют $_GET['plugin_event'] через заголовочный файл
 */
class tmc_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function main()
    {
        global $DB, $USER, $PLUGINS;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");

        if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
            #header("Location: /tmc/viewtech");
            #die();
            redirect("/tmc/viewtech");
            return;
        } else {
            $tpl->setVariable("SC_OPTIONS", adm_sc_plugin::getScList());
        }

        UIHeader($tpl);
        $tpl->show();
    }


    function importtosc()
    {
        global $DB, $USER, $PLUGINS;
        if (!$USER->checkAccess("tmc_main_stock", User::ACCESS_WRITE)) UIError("В доступе отказано");
        if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) UIError("Техникам запрещен доступ в модуль ТМЦ!");
        if (isset($_POST['import'])) {
            $sc_id = $this->getSession()->get("sc_id");
            if (!$sc_id) UIError("Не указан сервисный центр!");
            if (count($_POST["sel"]) && count($this->getSession()->get("tmc_import_data"))) {
                foreach ($_POST["sel"] as $item) {
                    $sql = "SELECT `id` FROM `list_contr` WHERE `contr_title` = " . $DB->F($this->getSession()->get("tmc_import_data")[$item]['contr']) . ";";
                    $contrID = $DB->getField($sql);
                    $sql = "SELECT `id` FROM `list_material_cats` WHERE `title` = " . $DB->F($this->getSession()->get("tmc_import_data")[$item]['tmc_cat']) . " AND id NOT IN (SELECT tmc.cat_id FROM list_materials AS tmc WHERE tmc.metric_flag = 1 GROUP BY tmc.id);";
                    $tmcCatID = $DB->getField($sql);
                    //if ($this->getSession()->get("tmc_import_data")[$item]["tmc_sn"]) {
                    $sql = "SELECT id FROM list_materials WHERE title=" . $DB->F($this->getSession()->get("tmc_import_data")[$item]["tmc_name"]);
//                  $res = $DB->getField($sql);
//                        $tmcOK = !$res ? true : false;
                    //} else {
                    //    $sql = "SELECT id FROM list_materials WHERE LOWER(title)=".$DB->F(strtolower($this->getSession()->get("tmc_import_data")[$item]["tmc_name"])).";";
                    //    $ismetric = true;
                    //  $res = $DB->getField($sql);
//                        $tmcOK = $res ? true : false;
                    //}
//                    $sql = "SELECT `id` FROM `list_materials` WHERE LOWER(title)=".$DB->F(strtolower($this->getSession()->get("tmc_import_data")[$item]['tmc_name'])).";";
                    $tmcID = $DB->getField($sql);
                    if (!$tmcID) {
                        //if ($ismetric)
                        //$sql = "INSERT INTO (`title`, `cat_id`, `metric_flag`, `metric_title`) VALUES (".$DB->F($this->getSession()->get("tmc_import_data")[$item]['tmc_name']).", ".$DB->F($tmcCatID).", ".$DB->F(1).", ".$DB->F("м.").");";
                        //else
                        $sql = "INSERT INTO `list_materials` (`title`, `cat_id`, `metric_flag`, `metric_title`) VALUES (" . $DB->F($this->getSession()->get("tmc_import_data")[$item]['tmc_name']) . ", " . $DB->F($tmcCatID) . ", " . $DB->F(0) . ", " . $DB->F("шт.") . ");";
                        echo $sql . "<br />";
                        $DB->query($sql);
                        if ($DB->errno()) UIError($DB->error() . " " . $sql);
                        $tmcID = $DB->insert_id();
                        if (!$tmcID) UIError("Невозможно создать новую запись о ТМЦ!");
                    }
                    //if (!$ismetric)
                    //    $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `serial`, `count`, `user_id`, `inplace`)
                    //            VALUES (".$DB->F($sc_id).", ".$DB->F($tmcID).", ".$DB->F($this->getSession()->get("tmc_import_data")[$item]['tmc_sn']).", 1, ".$DB->F($USER->getId()).", 1);";
                    //else {
                    $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `serial`, `count`, `user_id`, `inplace`, ledit)
                                VALUES (" . $DB->F($sc_id) . ", " . $DB->F($tmcID) . ", " . $DB->F($this->getSession()->get("tmc_import_data")[$item]['tmc_sn']) . ", " . $DB->F($this->getSession()->get("tmc_import_data")[$item]['tmc_quan']) . ", " . $DB->F($USER->getId()) . ", 1, now());";
                    //}
                    $DB->query($sql);
                    $lid = $DB->insert_id();
                    if ($DB->errno()) UIError($DB->error());
                    $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `serial`, `count`, `user_id`, `inplace`, `incoming`, ledit)
                                        VALUES (" . $DB->F($sc_id) . ", " . $DB->F($tmcID) . ", " . $DB->F($this->getSession()->get("tmc_import_data")[$item]['tmc_sn']) . ", 1, " . $DB->F($USER->getId()) . ", 0, 1, now());";
                    $DB->query($sql);

                    if ($_REQUEST["sc_tech_id"] && $lid) {
                        $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `inplace`, ledit)
                        VALUES (" . $DB->F($sc_id) . ", " . $DB->F($_POST["sc_tech_id"]) . ", " . $DB->F($lid) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", 1, now());";
                        $DB->query($sql);
                        if ($DB->errno()) UIError($DB->error());
                        $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `incoming`, ledit)
                        VALUES (" . $DB->F($sc_id) . ", " . $DB->F($_POST["sc_tech_id"]) . ", " . $DB->F($lid) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", 1, now());";
                        $DB->query($sql);
                        if ($DB->errno()) UIError($DB->error());
                        $msgadd = " и выданы технику";
                    }
                    if ($DB->errno()) UIError($DB->error());
                }

            } else {
                UIError("Не выбраны ТМЦ для импорта!");
            }
            $this->getSession()->remove("tmc_import_data");
            $this->getSession()->remove("sc_id");
            redirect("viewsc", "ТМЦ успешно импортированы и добавлены на склад $msgadd!");
        } else {
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $tpl->loadTemplatefile($USER->getTemplate() . "/import.tmpl.htm");
            UIHeader($tpl);
            if ($_REQUEST["sc_tech"]) {
                $tpl->setVariable("SCTECH_ID", $_REQUEST["sc_tech"]);
            }
            if (!$_REQUEST["sc_id"]) UIError("Не указан сервисный центр!");
            $file = fopen($_FILES["source"]["tmp_name"], "r");
            if ($file !== FALSE) {
                $result = array();
                while (($data = fgetcsv($file, 1000, ";")) !== FALSE) {
                    $num = count($data);
                    if ($num < 5) UIError("Файл не соответствет формату! " . $num);
                    $result[]["contr"] = from1251toUtf($data[0]);
                    $result[count($result) - 1]["tmc_cat"] = from1251toUtf($data[1]);
                    $result[count($result) - 1]["tmc_name"] = from1251toUtf($data[2]);
                    $result[count($result) - 1]["tmc_quan"] = from1251toUtf($data[3]);
                    $result[count($result) - 1]["tmc_sn"] = from1251toUtf($data[4]);
                    if ($num > 5) {
                        $result[count($result) - 1]["tmc_date"] = from1251toUtf($data[5]);
                        $result[count($result) - 1]["tmc_resp"] = from1251toUtf($data[6]);

                    }
                }
                $this->getSession()->remove("sc_id");
                $this->getSession()->remove("tmc_import_data");
                fclose($file);
                if (count($result)) {
                    $this->getSession()->set("sc_id", $_REQUEST["sc_id"]);
                    $this->getSession()->set("tmc_import_data", $result);
                    $prevContr = false;
                    $prevTmcCat = false;
                    $contrOK = false;
                    $tmccatOK = false;
                    $tmcOK = false;
                    foreach ($result as $key => $item) {
                        if ($prevContr != $item["contr"]) {
                            $prevContr = $item["contr"];
                            $sql = "SELECT COUNT(`contr_title`) FROM `list_contr` WHERE LOWER(`contr_title`) = " . $DB->F(strtolower($prevContr)) . ";";
                            $contrCount = $DB->getField($sql);
                            $contrOK = ($contrCount == 1) ? true : false;
                        }
                        if ($prevTmcCat != $item["tmc_cat"]) {
                            $prevTmcCat = $item["tmc_cat"];
                            $sql = "SELECT COUNT(`id`) FROM `list_material_cats` WHERE LOWER(`title`) = " . $DB->F(strtolower($prevTmcCat)) . " ;";
                            // AND id NOT IN (SELECT tmc.cat_id FROM list_materials AS tmc WHERE tmc.metric_flag = 1 GROUP BY tmc.id)
                            $tmccatCount = $DB->getField($sql);
                            $tmccatOK = ($tmccatCount == 1) ? true : false;
                        }
                        if ($item["tmc_sn"]) {
                            $sql = "SELECT count(id) FROM list_materials WHERE LOWER(title)=" . $DB->F(strtolower($item["tmc_name"])) . " AND `id` IN (SELECT `tmc_id` FROM `tmc_sc` WHERE `serial`=" . $DB->F($item["tmc_sn"]) . ");";
                            $res = $DB->getField($sql);
                            $tmcOK = !$res ? true : false;
                        } else {
                            $sql = "SELECT count(id) FROM list_materials WHERE LOWER(title)=" . $DB->F(strtolower($item["tmc_name"])) . " ;";
                            $res = $DB->getField($sql);
                            $tmcOK = $res ? true : false;
                        }

                        $tpl->setCurrentBlock("row");
                        if ($tmccatOK && $contrOK && $tmcOK) {
                            $tpl->setCurrentBlock("recok");
                            $tpl->setVariable("ROW_REC_ID", $key);
                            $tpl->parse("recok");
                        } else {
                            $tpl->setVariable('NOTOK', "notok");
                        }
                        $tpl->setVariable("ROW_CONTR", $item["contr"]);
                        $tpl->setVariable("ROW_CAT", $item["tmc_cat"]);
                        $tpl->setVariable("ROW_NAME", $item["tmc_name"]);
                        $tpl->setVariable("ROW_QUAN", $item["tmc_quan"]);
                        $tpl->setVariable("ROW_SN", !$tmcOK ? "<strong style='color:red;'>" . $item["tmc_sn"] . "</strong>" : $item["tmc_sn"]);
                        $tpl->parse("row");
                    }
                    //die();
                } else {
                    UIError("Записи отсутствуют!");
                }
            } else {
                UIError("Файл не загружен");
            }
            $tpl->show();
        }
    }

    function viewsc()
    {

        global $DB, $USER, $PLUGINS;
        $inplace_filter = array("0" => "Списано", "1" => "На складе", "2" => "Только демонтированное");
        if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) UIError("Техникам запрещен доступ в модуль ТМЦ!");
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/viewsc.tmpl.htm");
        else
            $tpl->loadTemplatefile("viewsc.tmpl.htm");

        UIHeader($tpl);

        $_REQUEST['filter_tmc_cat_id'] = $this->getCookie('filter_tmc_cat_id', $_REQUEST['filter_tmc_cat_id']);
        $_REQUEST['filter_serial'] = $this->getCookie('filter_serial', $_REQUEST['filter_serial']);
        if (isset($_REQUEST['filter_state']) && $_REQUEST['filter_state'] == 0)
            $_REQUEST["filter_state"] = "0";
        $_REQUEST['filter_state'] = $this->getCookie('filter_state', $_REQUEST['filter_state']);
        $_REQUEST['sc_id_only'] = $this->getCookie('sc_id_only', $_REQUEST['sc_id_only']);

        if (!isset($_REQUEST["filter_state"])) $_REQUEST["filter_state"] = 1;
        if ($USER->checkAccess("tmc_main_stock", User::ACCESS_WRITE)) {
            $tpl->setCurrentBlock("allowed-import");
            $tpl->setVariable("SC1_ID", $_REQUEST['sc_id_only']);
            $tpl->parse("allowed-import");
            $tpl->touchBlock('addnewtmc');
        }

        if (!$_REQUEST["sc_id_only"]) {
            redirect("main", "Не указан СЦ!");
        }
        if ($sc_id = $USER->isChiefTech($USER->getId())) {
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id_only"]));
        }
        $tpl->setVariable("FILTER_OPDATE", $_REQUEST["opdate"]);
        $tpl->setVariable("SC_ID", $_REQUEST["sc_id_only"]);
        $tpl->setVariable("FILTER_TMCCAT_OPTIONS", adm_material_cats_plugin::getOptions($_REQUEST["filter_tmc_cat_id"]));
        $tpl->setVariable("FILTER_SERIAL", $_REQUEST["filter_serial"]);
        if ($_REQUEST['filter_tmc_cat_id']) {
            $isdoc = $DB->getField("SELECT `isdoc` FROM `list_material_cats` WHERE `id`=" . $DB->F($_REQUEST["filter_tmc_cat_id"]) . "; ");
            if ($isdoc > 0) {
                $tpl->setCurrentBlock("isdocfilter");
                $tpl->setVariable("FILTER_DOC_NUM_FROM", $_REQUEST["docnumfrom"] ? $_REQUEST["docnumfrom"] : $DB->getField("SELECT MIN(`docnum`) FROM `tmc_sc`;"));
                $tpl->setVariable("FILTER_DOC_NUM_TO", $_REQUEST["docnumto"] ? $_REQUEST["docnumto"] : $DB->getField("SELECT MAX(`docnum`) FROM `tmc_sc`;"));
                $tpl->parse("isdocfilter");
                $sql_docnumfilter = array();
                if ($_REQUEST["docnumfrom"])
                    $sql_docnumfilter[] = " sc.docnum>=" . $DB->F($_REQUEST["docnumfrom"]) . " ";
                if ($_REQUEST["docnumto"])
                    $sql_docnumfilter[] = " sc.docnum<=" . $DB->F($_REQUEST["docnumto"]) . " ";
            }
        }
        if ($_REQUEST["filter_serial"]) {
            $serial = " AND sc.serial LIKE " . $DB->F("%" . $_REQUEST["filter_serial"] . "%") . " ";
        }
        $tpl->setVariable("FILTER_INPLACE", array2options($inplace_filter, $_REQUEST["filter_state"]));

        $sc_filter = $_REQUEST["sc_id_only"];

        $tmcstate_filter = '';

        if ($_REQUEST["filter_tmc_cat_id"] != 0) {
            $tmccat_filter = "WHERE lmc.id=" . $DB->F($_REQUEST["filter_tmc_cat_id"]);
            $tmclist_filter = "AND lmc.id=" . $DB->F($_REQUEST["filter_tmc_cat_id"]);
        }
        if (isset($_REQUEST["filter_state"])) {
            if ($_REQUEST["filter_state"] == 0) { // "0"=>"Списано",
                $tmcstate_filter = " AND ((!sc.inplace OR sc.inplace IS NULL) OR sc.deleted>0)";
                $tpl->setVariable("IS__HIDDEN", "class='hidden'");
            } elseif ($_REQUEST["filter_state"] == 1) { // "1"=>"На складе"
                $tpl->touchBlock("selall1");
                $tpl->touchBlock("selall2");
                $tpl->setCurrentBlock("movetotech1");
                $tpl->setVariable("TECHLIST", adm_empl_plugin::getSCTechsAll($sc_filter));
                $tpl->setVariable("SC2LIST", adm_sc_plugin::getScList($_REQUEST["sc_id_only"]));
                $tpl->parse("movetotech1");
                $tmcstate_filter = " AND (sc.inplace=1) AND (sc.deleted = 0 OR sc.deleted IS NULL)";
            } elseif ($_REQUEST["filter_state"] == 2) { // "2"=>"Только демонтированное"
                $tpl->touchBlock("selall1");
                $tpl->touchBlock("selall2");
                $tpl->setCurrentBlock("movetotech1");
                $tpl->setVariable("TECHLIST", adm_empl_plugin::getSCTechsAll($sc_filter));
                $tpl->setVariable("SC2LIST", adm_sc_plugin::getScList($_REQUEST["sc_id_only"]));
                $tpl->parse("movetotech1");
                $tmcstate_filter = " AND (sc.inplace=1) AND (sc.deleted = 0 OR sc.deleted IS NULL) AND sc.dismantled = 1";
            }
            if ($_REQUEST["opdate"] != "") {
                $tmcstate_filter .= " AND DATE_FORMAT(sc.ledit, '%Y-%m-%d')=" . $DB->F(date("Y-m-d", strtotime($_REQUEST["opdate"]))) . " ";
            }
        }

        $tmcstate_filter .= " AND (sc.incoming=0 OR sc.incoming IS NULL)";
        $sql = "SELECT lmc.*, (SELECT COUNT(`id`) FROM list_materials WHERE `cat_id`=lmc.id AND `metric_flag`) as ismetric FROM `list_material_cats` AS lmc " . $tmccat_filter . " ORDER BY ismetric DESC, lmc.title ASC";
        if ($_REQUEST["filter_state"] == "0") {
            $tpl->touchBlock("motedtoth");
        }
        $rowcount = 0;
        $usedTmc = array();

        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            $tpl->setVariable("CURRENT_SC", $sc_filter);
            while ($result = $DB->fetch(true)) {
                $add = implode(" AND ", (array)$sql_docnumfilter);
                if ($add) {
                    $add = " AND " . $add;
                } else {
                    $add = " ";
                }
                $sql = "SELECT sc.*, DATE_FORMAT(sc.ledit, '%H:%i:%s %d-%m-%Y') as lastdate, lm.title, lm.metric_flag, lm.metric_title FROM `tmc_sc` AS sc
                        LEFT JOIN `list_materials` AS lm ON lm.id=sc.tmc_id
                        WHERE sc.sc_id=" . $DB->F($sc_filter) . " AND lm.cat_id=" . $DB->F($result["id"]) . $tmcstate_filter . $add . " " . $serial . " ORDER BY lm.title ASC";
//                die($sql);
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                if ($DB->num_rows()) {
                    while ($r = $DB->fetch(true)) {
                        $rowcount += 1;
                        if ($r["metric_flag"] == "1") {
                            if (!in_array($r["tmc_id"], $usedTmc)) {
                                $usedTmc[] = $r["tmc_id"];
                                $tmcCountIN = $DB->getField("SELECT SUM(sc.count) FROM `tmc_sc` AS sc WHERE sc.sc_id=" . $DB->F($sc_filter) . " AND sc.tmc_id=" . $DB->F($r["tmc_id"]) . " AND incoming");
                                $tmcCountOUT = $DB->getField("SELECT SUM(sc.count) FROM `tmc_sc` AS sc WHERE sc.sc_id=" . $DB->F($sc_filter) . " AND sc.tmc_id=" . $DB->F($r["tmc_id"]) . "AND (!sc.incoming OR sc.incoming IS NULL) AND (!sc.inplace OR sc.inplace IS NULL)");
                                $lastDate = $DB->getField("SELECT DATE_FORMAT(MAX(ledit), '%H:%i:%s %d-%m-%Y') as lastdate FROM `tmc_sc` AS sc WHERE sc.sc_id=" . $DB->F($sc_filter) . " AND sc.tmc_id=" . $DB->F($r["tmc_id"]));
                                $tpl->setCurrentBlock("row");
                                $tpl->setVariable("NOTROW", "notrow");
                                $tpl->setVariable("ROW_ID", $r["id"]);
                                $tpl->setVariable("ROW_CATNAME", $result["title"]);
                                $tpl->setVariable("ROW_TITLE", $r["title"]);
                                $tpl->setVariable("ROW_SN", "&mdash;");
                                $tpl->setVariable("ROW_DATE", $lastDate . " (посл)");
                                $tpl->setVariable('DISMANTLED', $result["dismantled"] == 1 ? 'dismantled' : '');
                                if (isset($_REQUEST["filter_state"]) && $_REQUEST["filter_state"] == 0) { // "0"=>"Списано",
                                    $tmcCountOUT = $DB->getField("SELECT SUM(sc.count) FROM `tmc_sc` AS sc WHERE sc.sc_id=" . $DB->F($sc_filter) . " AND sc.tmc_id=" . $DB->F($r["tmc_id"]) . "AND (!sc.incoming OR sc.incoming IS NULL) AND (!sc.inplace OR sc.inplace IS NULL)");
                                    $tpl->setVariable("ROW_COUNT", $tmcCountOUT . " " . $r["metric_title"]);
                                } else {
                                    if (($tmcCountIN - $tmcCountOUT) > 0) {
                                        $tpl->setVariable("ROW_COUNT", $tmcCountIN - $tmcCountOUT . " " . $r["metric_title"]);
                                    } else {
                                        $tpl->setVariable("ROW_COUNT", "<strong style=\"color:#FF0000;\">" . ($tmcCountIN - $tmcCountOUT) . " " . $r["metric_title"] . "</strong>");
                                    }
                                }
                                if (isset($_REQUEST["filter_state"]) && $_REQUEST["filter_state"] == 0) {
                                    $tpl->setVariable("IS_HIDDEN", "class='hidden'");
                                } else {
                                    if (($tmcCountIN - $tmcCountOUT) > 0) {
                                        $tpl->setCurrentBlock("returnmetrictmc");
                                        $tpl->setVariable("ROW_ID_I", $r["id"]);
                                        $tpl->parse("returnmetrictmc");
                                    }
                                    $tpl->setVariable("IS_HIDDEN", "class='hidden'");
                                }
                                $tpl->setVariable("ROW_SC", $r["sc_id"]);
                                $tpl->setVariable("ROW_TMC_ID", $r["tmc_id"]);
                                if ($_REQUEST["filter_state"] == "0") {
                                    $tpl->setCurrentBlock("movedtotd");
                                    $tpl->setVariable("MTTASK_ID", "&mdash;");
                                    $tpl->parse("movedtotd");
                                }
                                $tpl->parse("row");
                            }
                        } else {

                            $tpl->setCurrentBlock("row");
                            if ($_REQUEST["filter_state"] != 0) {
                                $tpl->setCurrentBlock("nonmetric");
                                $tpl->setVariable("ROW_IM_ID", $r["id"]);
                                $tpl->parse("nonmetric");
                            }
                            /*$sql = "SELECT COUNT(tst.id) FROM `tmc_sc_tech` AS tst WHERE tst.tmc_sc_id
                                            IN (SELECT `id` FROM `tmc_sc` WHERE `sc_id`=".$DB->F($sc_filter)." AND `serial`=".$DB->F($r["serial"])." AND `tmc_id`=".$DB->F($r["tmc_id"]).");";
                            $r22 = $DB->getField($sql);
                            if ($r22) {*/
                            $tfio = false;
                            $sql = "SELECT (SELECT `fio` FROM `users` WHERE `id`=tst.tech_id) as fio, tst.ledit FROM `tmc_sc_tech` AS tst WHERE tst.tmc_sc_id = " . $DB->F($r["id"]) . " LIMIT 1";
                            //IN (SELECT `id` FROM `tmc_sc` WHERE `sc_id`=".$DB->F($sc_filter)." AND `serial`=".$DB->F($r["serial"])." AND `tmc_id`=".$DB->F($r["tmc_id"]).") LIMIT 1;";
                            $rm1 = $DB->getRow($sql);
                            if ($rm1) {
                                $tfio = $rm1[0];
                                $lastops = $rm1[0] . " " . $rm1[1];
                                $tpl->setVariable("LASTOPS", "haslastops");
                                $tpl->setVariable("LASTOPSTITLE", "Последняя операция: " . $lastops);
                            } else {
                                if ($r["from_task_id"]) {
                                    $tpl->setVariable("LASTOPS", "haslastops");
                                    $tpl->setVariable("LASTOPSTITLE", "Последняя операция: " . $r["from_date"] . " по заявке " . $r["from_task_id"]);
                                }
                            }
                            //}*/
                            if ($_REQUEST["filter_state"] == "0") {
                                $sql = "SELECT `task_id` FROM `tmc_ticket` WHERE `tmc_tech_sc_id` IN (SELECT `id` FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $DB->F($r["id"]) . ")";
                                $t1 = $DB->getField($sql);
                                $str = array();
                                if ($t1) {
                                    $task = new Task($t1);
                                    if ($task) {
                                        $clnttnum = $DB->getField("select `clnttnum` from `tickets` where `task_id`=" . $DB->F($task->getId()));
                                        $str[] = "<a target=\"_blank\" title=\"Списано на заявку №" . $task->getId() . "\" href=" . link2($task->getPluginUid() . "/viewticket?task_id=" . $task->getId(), false) . ">" . $task->getId() . ($clnttnum ? " ($clnttnum)" : "") . "</a>";
                                    }
                                } else {
                                    $sql = "SELECT `id` FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $DB->F($r["id"]) . ";";
                                }
                                $from_task_id = $DB->getField("SELECT `from_task_id` FROM `tmc_sc` WHERE `id`=" . $DB->F($r["id"]) . ";");
                                if ($from_task_id > 0) {
                                    $task = new Task($from_task_id);
                                    if ($task) {
                                        $clnttnum = $DB->getField("select `clnttnum` from `tickets` where `task_id`=" . $DB->F($task->getId()));
                                        $str[] = "<a target=\"_blank\" title=\"Приход с заявки №" . $from_task_id . "\" href=" . link2($task->getPluginUid() . "/viewticket?task_id=" . $task->getId(), false) . ">" . $task->getId() . ($clnttnum ? " ($clnttnum)" : "") . "</a>";
                                    }
                                }
                                $tpl->setCurrentBlock("movedtotd");
                                $tpl->setVariable("MTTASK_ID", count($str) > 0 ? implode(",", $str) : "&mdash;");
                                $tpl->setVariable("MTTECH_FIO", $tfio ? $tfio : "<center>&mdash;</center>");
                                $tpl->parse("movedtotd");
                            }

                            $tpl->setVariable("ROW_ID", $r["id"]);
                            $tpl->setVariable("ROW_CATNAME", $result["title"]);
                            $tpl->setVariable("ROW_TITLE", $r["title"]);
                            $tpl->setVariable("ROW_SN", $r["serial"]);
                            $tpl->setVariable("ROW_DATE", $r["lastdate"]);
                            $tpl->setVariable("ROW_COUNT", $r["count"] . " " . $r["metric_title"]);
                            $tpl->setVariable('DISMANTLED', $r["dismantled"] == 1 ? 'dismantled' : '');
                            if (isset($_REQUEST["filter_state"]) && $_REQUEST["filter_state"] == 0) {
                                $tpl->setVariable("IS_HIDDEN", "class='hidden'");
                            }
                            $tpl->parse("row");
                        }

                    }

                }
                $DB->free();
            }

            if (!$rowcount) $tpl->touchBlock("no-rows");
        } else {
            UIError("Отсутствуют категории ТМЦ!");
        }
        $DB->free();
        $tpl->show();
    }


    function movetotech(Request $request)
    {
        global $DB, $PLUGINS, $USER;
        $sc_id = $_REQUEST["current_sc"];
        $tech_id = $_REQUEST["tech_id"];
        if (isset($_POST["returntomainsc"])) {
            if (count($_REQUEST["seltmc"])) {
                $sql_in = implode(", ", $_REQUEST['seltmc']);
                $sql = "SELECT * FROM `tmc_sc` WHERE `id` IN (" . $sql_in . ") AND `id` NOT IN (SELECT DISTINCT(`tmc_sc_id`) FROM `tmc_sc_tech` WHERE `tmc_sc_id` IN (" . $sql_in . "));";
                //die($sql);
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                while (list($id, $sc_id, $tmc_id, $serial, $count, $user_id, $ledit, $inplace, $incoming) = $DB->fetch()) {
                    $sql = "DELETE FROM `tmc_sc` WHERE `serial`=" . $DB->F($serial);
                    //echo $sql;
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error());
                    $DB->free();
                }
                //$sql = "UPDATE `tmc_sc` SET `inplace`=0 WHERE `id` IN (".$sql_in.");";
                //$DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                $DB->free();
                redirect("viewsc", "Выбранные ТМЦ удалены! (за исключением ранее выдававшихся)");
                return;
            }
            redirect("viewsc", "Не выбраны ТМЦ для удаления!");
            die();
        } else {
            if ($request->request->has('movetosc')) {
                if ($_REQUEST["current_sc"] == $_REQUEST["sc2_id"]) {
                    redirect("viewsc", "СЦ получатель не может быть СЦ источником!");
                    return;
                } else {
                    if (!$_REQUEST["sc2_id"]) {
                        redirect("viewsc", "Пустое значение СЦ получателя!");
                        return;
                    }
                    $sql_in = implode(", ", $_REQUEST['seltmc']);
                    $sql = "SELECT * FROM `tmc_sc` WHERE `id` IN (" . $sql_in . ") AND `id` NOT IN (SELECT DISTINCT(`tmc_sc_id`) FROM `tmc_sc_tech` WHERE `tmc_sc_id` IN (" . $sql_in . "));";
                    //echo $sql;
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error());
                    while (list($id, $sc_id, $tmc_id, $serial, $count, $user_id, $ledit, $inplace, $incoming) = $DB->fetch()) {
                        $sql = "UPDATE `tmc_sc` SET `sc_id`=" . $_REQUEST["sc2_id"] . ", ledit=now() WHERE `serial`=" . $DB->F($serial) . " AND `tmc_id`=" . $DB->F($tmc_id);
                        //echo $sql."<br />";
                        $r4 = $DB->query($sql);
                        if ($DB->errno()) UIError($DB->error());
                        $DB->free();
                    }
                    //die();
                    $DB->free();
                    //$sql = "UPDATE `tmc_sc` SET `inplace`=0 WHERE `id` IN (".$sql_in.");";
                    //$DB->query($sql);
                    if ($DB->errno()) UIError($DB->error());
                    redirect("viewsc", "ТМЦ переданы выбранному СЦ! (за исключением ранее выдававшихся)");
                    return;
                }
            } else {
                if (count($_REQUEST["seltmc"]) && $sc_id && $tech_id) {

                    $sql1 = "INSERT INTO tmc_sc_tech (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `inplace`, `incoming`, ledit) VALUES ";
                    $sql2 = "INSERT INTO tmc_sc_tech (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `inplace`, `incoming`, ledit) VALUES ";
                    $sql1_add = array();
                    $sql2_add = array();

                    foreach ($_REQUEST["seltmc"] as $item) {
                        $toremove[] = $item;
                        $sql1_add[] = "(" . $DB->F($sc_id) . ", " . $DB->F($tech_id) . ", " . $DB->F($item) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", " . $DB->F(0) . ", " . $DB->F(1) . ", now())";
                        $sql2_add[] = "(" . $DB->F($sc_id) . ", " . $DB->F($tech_id) . ", " . $DB->F($item) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", " . $DB->F(1) . ", " . $DB->F(0) . ", now())";
                    }
                    $sql1 .= implode(", ", $sql1_add);
                    $sql2 .= implode(", ", $sql2_add);
                    $clear = "DELETE FROM `tmc_sc_tech` WHERE `tmc_sc_id` IN (" . preg_replace("/,$/", "", implode(",", $toremove)) . ");";
                    $DB->query($clear);
                    $sql3 = "UPDATE `tmc_sc` SET `inplace`=0, ledit=now() WHERE `id` IN (" . implode(", ", $_REQUEST["seltmc"]) . ");";
                    $DB->query($sql1);
                    if ($DB->errno()) UIError($DB->error());
                    $DB->query($sql2);
                    if ($DB->errno()) UIError($DB->error());
                    $DB->query($sql3);
                    if ($DB->errno()) UIError($DB->error());
                    redirect("viewsc", "ТМЦ успешно выданы Технику!");
                } else {
                    UIError("Недостаточно параметров для выполнения операции!");
                }
            }
        }
    }

    function viewsc_ex()
    {
        global $DB, $PLUGINS, $USER;
        header("Content-type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=tmc_sc__export_" . date("H_i_d-M-Y") . ".xls");

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/scexport.tmpl.htm");


        $_REQUEST['filter_tmc_cat_id'] = $this->getCookie('filter_tmc_cat_id', $_REQUEST['filter_tmc_cat_id']);
        if (isset($_REQUEST['filter_state']) && $_REQUEST['filter_state'] == 0)
            $_REQUEST["filter_state"] = "0";
        $_REQUEST['filter_state'] = $this->getCookie('filter_state', $_REQUEST['filter_state']);
        $_REQUEST['sc_id_only'] = $this->getCookie('sc_id_only', $_REQUEST['sc_id_only']);
        $_REQUEST['filter_serial'] = $this->getCookie('filter_serial', $_REQUEST['filter_serial']);
        $sc = adm_sc_plugin::getSCParams($_REQUEST['sc_id_only']);
        $add = (isset($_REQUEST["filter_state"]) && $_REQUEST["filter_state"] == 0) ? " (Списано)" : "";
        $tpl->setVariable("SCNAME", $sc["title"] . $add);
        $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id_only"]));
        $tpl->setVariable("FILTER_TMCCAT_OPTIONS", adm_material_cats_plugin::getOptions($_REQUEST["filter_tmc_cat_id"]));
        $tpl->setVariable("FILTER_INPLACE", array2options($inplace_filter, $_REQUEST["filter_state"]));

        $sc_filter = $_REQUEST["sc_id_only"];

        if ($_REQUEST["filter_tmc_cat_id"] != 0) {
            $isdoc = $DB->getField("SELECT `isdoc` FROM `list_material_cats` WHERE `id`=" . $DB->F($_REQUEST["filter_tmc_cat_id"]) . "; ");
            if ($isdoc > 0) {
                $tpl->setCurrentBlock("isdocfilter");
                $tpl->setVariable("FILTER_DOC_NUM_FROM", $_REQUEST["docnumfrom"] ? $_REQUEST["docnumfrom"] : $DB->getField("SELECT MIN(`docnum`) FROM `tmc_sc`;"));
                $tpl->setVariable("FILTER_DOC_NUM_TO", $_REQUEST["docnumto"] ? $_REQUEST["docnumto"] : $DB->getField("SELECT MAX(`docnum`) FROM `tmc_sc`;"));
                $tpl->parse("isdocfilter");
                $sql_docnumfilter = array();
                if ($_REQUEST["docnumfrom"])
                    $sql_docnumfilter[] = " sc.docnum>=" . $DB->F($_REQUEST["docnumfrom"]) . " ";
                if ($_REQUEST["docnumto"])
                    $sql_docnumfilter[] = " sc.docnum<=" . $DB->F($_REQUEST["docnumto"]) . " ";
            }
            $tmccat_filter = "WHERE lmc.id=" . $DB->F($_REQUEST["filter_tmc_cat_id"]);
            $tmclist_filter = "AND lmc.id=" . $DB->F($_REQUEST["filter_tmc_cat_id"]);
        }
        if (isset($_REQUEST["filter_state"])) {
            if ($_REQUEST["filter_state"] == 0) {
                $tmcstate_filter = " AND (!sc.inplace OR sc.inplace IS NULL)";
                $tpl->setVariable("IS__HIDDEN", "class='hidden'");

            } else {
                $tmcstate_filter = " AND (sc.inplace=1)";
            }
            if ($_REQUEST["opdate"] != "") {
                $tmcstate_filter .= " AND DATE_FORMAT(sc.ledit, '%Y-%m-%d')=" . $DB->F(date("Y-m-d", strtotime($_REQUEST["opdate"]))) . " ";
            }

        }

        if ($_REQUEST["filter_serial"]) {
            $serial = " AND sc.serial LIKE " . $DB->F("%" . $_REQUEST["filter_serial"] . "%") . " ";
        }
        $tmcstate_filter .= " AND (sc.incoming=0 OR sc.incoming IS NULL)";
        $sql = "SELECT lmc.*, (SELECT COUNT(`id`) FROM list_materials WHERE `cat_id`=lmc.id AND `metric_flag`) as ismetric FROM `list_material_cats` AS lmc " . $tmccat_filter . " ORDER BY ismetric DESC, lmc.title ASC";
        $rowcount = 0;
        $usedTmc = array();
        if ($_REQUEST["filter_state"] == "0") {
            $tpl->touchBlock("motedtoth");
        }
        $DB->query($sql);
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {

            while ($result = $DB->fetch(true)) {
                $add = implode(" AND ", $sql_docnumfilter);
                if ($add) {
                    $add = " AND " . $add;
                } else {
                    $add = " ";
                }
                $sql = "SELECT sc.*, DATE_FORMAT(sc.ledit, '%H:%i:%s %d-%m-%Y') as lastdate, lm.title, lm.metric_flag, lm.metric_title FROM `tmc_sc` AS sc
                        LEFT JOIN `list_materials` AS lm ON lm.id=sc.tmc_id
                        WHERE sc.sc_id=" . $DB->F($sc_filter) . " AND lm.cat_id=" . $DB->F($result["id"]) . $tmcstate_filter . $add . " " . $serial . " ORDER BY lm.title ASC";
                //die($sql);
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                if ($DB->num_rows()) {
                    while ($r = $DB->fetch(true)) {
                        $rowcount += 1;
                        if ($r["metric_flag"] == "1") {
                            if (!in_array($r["tmc_id"], $usedTmc)) {
                                $usedTmc[] = $r["tmc_id"];
                                $tmcCountIN = $DB->getField("SELECT SUM(sc.count) FROM `tmc_sc` AS sc WHERE sc.sc_id=" . $DB->F($sc_filter) . " AND sc.tmc_id=" . $DB->F($r["tmc_id"]) . " AND incoming");
                                $tmcCountOUT = $DB->getField("SELECT SUM(sc.count) FROM `tmc_sc` AS sc WHERE sc.sc_id=" . $DB->F($sc_filter) . " AND sc.tmc_id=" . $DB->F($r["tmc_id"]) . "AND (!sc.incoming OR sc.incoming IS NULL) AND (!sc.inplace OR sc.inplace IS NULL)");
                                $lastDate = $DB->getField("SELECT DATE_FORMAT(MAX(ledit), '%H:%i:%s %d-%m-%Y') as lastdate FROM `tmc_sc` AS sc WHERE sc.sc_id=" . $DB->F($sc_filter) . " AND sc.tmc_id=" . $DB->F($r["tmc_id"]));
                                $tpl->setCurrentBlock("row");
                                $tpl->setVariable("ROW_ID", $r["id"]);
                                $tpl->setVariable("ROW_CATNAME", $result["title"]);
                                $tpl->setVariable("ROW_TITLE", $r["title"]);
                                $tpl->setVariable("ROW_SN", "&mdash;");
                                $tpl->setVariable("ROW_DATE", $lastDate . " (посл)");
                                $tpl->setVariable("ROW_COUNT", $tmcCount . " " . $r["metric_title"]);
                                if (isset($_REQUEST["filter_state"]) && $_REQUEST["filter_state"] == 0) {
                                    $tmcCountOUT = $DB->getField("SELECT SUM(sc.count) FROM `tmc_sc` AS sc WHERE sc.sc_id=" . $DB->F($sc_filter) . " AND sc.tmc_id=" . $DB->F($r["tmc_id"]) . "AND (!sc.incoming OR sc.incoming IS NULL) AND (!sc.inplace OR sc.inplace IS NULL)");
                                    $tpl->setVariable("ROW_COUNT", $tmcCountOUT . " " . $r["metric_title"]);
                                } else
                                    $tpl->setVariable("ROW_COUNT", $tmcCountIN - $tmcCountOUT . " " . $r["metric_title"]);
                                if (isset($_REQUEST["filter_state"]) && $_REQUEST["filter_state"] == 0) {
                                    $tpl->setVariable("IS_HIDDEN", "class='hidden'");
                                }
                                if ($_REQUEST["filter_state"] == "0") {
                                    $tpl->setCurrentBlock("movedtotd");
                                    $tpl->setVariable("MTTASK_ID", "&mdash;");
                                    $tpl->parse("movedtotd");
                                }
                                $tpl->parse("row");
                            }
                        } else {
                            $tpl->setCurrentBlock("row");
                            if ($_REQUEST["filter_state"] == "0") {
                                $sql = "SELECT `task_id` FROM `tmc_ticket` WHERE `tmc_tech_sc_id` IN (SELECT `id` FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $DB->F($r["id"]) . ")";
                                $t1 = $DB->getField($sql);
                                $str = array();
                                if ($t1) {
                                    $task = new Task($t1);
                                    if ($task) {
                                        $str[] = "<a target=\"_blank\" title=\"Списано на заявку №" . $task->getId() . "\" href=" . link2($task->getPluginUid() . "/viewticket?task_id=" . $task->getId(), false) . ">" . $task->getId() . "</a>";
                                    }
                                } else {
                                    $sql = "SELECT `id` FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $DB->F($r["id"]) . ";";
                                }
                                $from_task_id = $DB->getField("SELECT `from_task_id` FROM `tmc_sc` WHERE `id`=" . $DB->F($r["id"]) . ";");
                                if ($from_task_id > 0) {
                                    $task = new Task($from_task_id);
                                    if ($task)
                                        $str[] = "<a target=\"_blank\" title=\"Приход с заявки №" . $from_task_id . "\" href=" . link2($task->getPluginUid() . "/viewticket?task_id=" . $task->getId(), false) . ">" . $task->getId() . "</a>";

                                }
                                $tpl->setCurrentBlock("movedtotd");
                                $tpl->setVariable("MTTASK_ID", count($str) > 0 ? implode(",", $str) : "&mdash;");
                                $tpl->parse("movedtotd");
                            }
                            $tpl->setVariable("ROW_ID", $r["id"]);
                            $tpl->setVariable("ROW_CATNAME", $result["title"]);
                            $tpl->setVariable("ROW_TITLE", $r["title"]);
                            $tpl->setVariable("ROW_SN", $r["serial"]);
                            $tpl->setVariable("ROW_DATE", $r["lastdate"]);
                            $tpl->setVariable("ROW_COUNT", $r["count"] . " " . $r["metric_title"]);
                            if (isset($_REQUEST["filter_state"]) && $_REQUEST["filter_state"] == 0) {
                                $tpl->setVariable("IS_HIDDEN", "class='hidden'");
                            }
                            $tpl->parse("row");
                        }

                    }
                }
                $DB->free();
            }
            if (!$rowcount) $tpl->touchBlock("no-rows");
        }
        $DB->free();
        $tpl->show();
    }

    function addtmc()
    {
        global $DB, $USER, $PLUGINS;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/addtmc.tmpl.htm");
        $tpl->setVariable("SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id_only"]));

        $tpl->setVariable("TMCCAT_OPTIONS", adm_material_cats_plugin::getOptions());

        UIHeader($tpl);
        $tpl->show();
    }

    function addtmc_save()
    {
        global $DB, $USER, $PLUGINS;
        $metric = $DB->getField("SELECT `metric_flag` FROM `list_materials` WHERE `id`=" . $DB->F($_POST["tmc_id"]) . ";");
        $isdoc = $DB->getField("SELECT `is_num` FROM `list_materials` WHERE `id`=" . $DB->F($_POST["tmc_id"]) . ";");
        if ($DB->errno()) UIError($DB->error());
        if ($isdoc) {
            $quant = $_REQUEST["serial_isnum_quant"];
            $start = $_REQUEST["serial_isnum"];
            if ($quant > 0 && $start >= 1) {
                for ($i = 0; $i < $quant; $i += 1) {
                    $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `serial`, `count`,  `user_id`, `inplace`, `docnum`, ledit)
                    VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["tmc_id"]) . ", " . $DB->F($i + $start) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", 1, " . $DB->F($i + $start) . ", now());";
                    $DB->query($sql);
                    $lid = $DB->insert_id();
                    if ($_REQUEST["sc_tech"] != "" && $lid) {
                        $sql = "UPDATE `tmc_sc` SET `inplace`=0, ledit=now() WHERE `inplace` AND `id`=" . $DB->F($lid) . ";";
                        $DB->query($sql);
                        if ($DB->errno()) UIError($DB->error());
                        $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `inplace`, ledit)
                        VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["sc_tech"]) . ", " . $DB->F($lid) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", 1, now());";
                        $DB->query($sql);
                        if ($DB->errno()) UIError($DB->error());
                        $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `incoming`, ledit)
                        VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["sc_tech"]) . ", " . $DB->F($lid) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", 1, now());";
                        $DB->query($sql);
                        if ($DB->errno()) UIError($DB->error());
                        $msgadd = " и выдано технику";
                    }

                    if ($DB->errno()) UIError($DB->error());
                    $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `serial`, `count`, `user_id`, `incoming`, `docnum`, ledit)
                    VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["tmc_id"]) . ", " . $DB->F($i + $start) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", 1, " . $DB->F($i + $start) . ", now());";
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error());
                }
            } else {
                UIError("Некорректные входные данные. Пожалуйста, укажите стартовый номер документа и кол-во документов > 0.");
            }
        } else {
            if ($metric == 1) {
                if (!$_POST["quan"]) UIError("Нельзя выдать нулевое количество ТМЦ");
                /*$count = $DB->getField("SELECT `count` FROM `tmc_sc` WHERE `sc_id`=".$_POST["sc_id"]." AND `tmc_id`=".$_POST["tmc_id"]." AND inplace;");
                if ($DB->errno()) UIError($DB->error());
                if ($count) {
                    $sql = "UPDATE `tmc_sc` SET `count` = ".$DB->F($count + $_POST["quan"])." WHERE `sc_id`=".$_POST["sc_id"]." AND `tmc_id`=".$_POST["tmc_id"]." AND inplace;";
                } else {
                $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `count`, `user_id`, `inplace`)
                VALUES (".$DB->F($_POST["sc_id"]).", ".$DB->F($_POST["tmc_id"]).", ".$DB->F(intval($_POST["quan"])).", ".$DB->F($USER->getId()).", 1);";
                }*/
                $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `count`, `user_id`, `inplace`, ledit)
                VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["tmc_id"]) . ", " . $DB->F(intval($_POST["quan"])) . ", " . $DB->F($USER->getId()) . ", 1, now());";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `count`, `user_id`, `incoming`, ledit)
                VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["tmc_id"]) . ", " . $DB->F(intval($_POST["quan"])) . ", " . $DB->F($USER->getId()) . ", 1, now());";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                if ($_REQUEST["sc_tech"] != "") {
                    $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `count`, `user_id`, `inplace`, ledit)
                    VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["tmc_id"]) . ", " . $DB->F(intval($_POST["quan"])) . ", " . $DB->F($USER->getId()) . ", 0, now());";
                    $DB->query($sql);
                    $tmcscrowid = $DB->insert_id();
                    if ($DB->errno()) UIError($DB->error());
                    $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `inplace`, ledit)
                    VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["sc_tech"]) . ", " . $DB->F($tmcscrowid) . ", " . $DB->F(intval($_POST["quan"])) . ", " . $DB->F($USER->getId()) . ", 1, now());";
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error());
                    $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `incoming`, ledit)
                    VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["sc_tech"]) . ", " . $DB->F($tmcscrowid) . ", " . $DB->F(intval($_POST["quan"])) . ", " . $DB->F($USER->getId()) . ", 1, now());";
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error());
                    $msgadd = " и выдано технику";
                }
                /*$sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `count`, `user_id`, `inplace`, `incoming`)
                VALUES (".$DB->F($_POST["sc_id"]).", ".$DB->F($_POST["tmc_id"]).", ".$DB->F(intval($_POST["quan"])).", ".$DB->F($USER->getId()).", 0, 1);";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());*/

            } else {
                $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `serial`, `count`, `user_id`, `inplace`, ledit)
                VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["tmc_id"]) . ", " . $DB->F($_POST["serial"]) . ", 1, " . $DB->F($USER->getId()) . ", 1, now());";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                $lid = $DB->insert_id();
                if ($_REQUEST["sc_tech"] != "" && $lid) {
                    $sql = "UPDATE `tmc_sc` SET `inplace`=0, ledit=now() WHERE `inplace` AND `id`=" . $DB->F($lid) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error());
                    $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `inplace`, ledit)
                    VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["sc_tech"]) . ", " . $DB->F($lid) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", 1, now());";
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error());
                    $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `incoming`, ledit)
                    VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["sc_tech"]) . ", " . $DB->F($lid) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", 1, now());";
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error());
                    $msgadd = " и выдано технику";

                }
                $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `serial`, `count`, `user_id`, `inplace`, `incoming`, ledit)
                VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["tmc_id"]) . ", " . $DB->F($_POST["serial"]) . ", 1, " . $DB->F($USER->getId()) . ", 0, 1, now());";
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
            }
        }
        redirect("viewsc", "ТМЦ успешно добавлено на склад $msgadd!");

    }

    function viewtech()
    {

        global $DB, $USER, $PLUGINS;
        $inplace_filter = array("0" => "Списано", "1" => "На руках", "2" => "Приход");

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
            $tpl_add = "_tech";
        }
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/viewtech$tpl_add.tmpl.htm");
        else
            $tpl->loadTemplatefile("viewtech$tpl_add.tmpl.htm");
        UIHeader($tpl);

        $_REQUEST['filter_tmc_cat_id'] = $this->getCookie('filter_tmc_cat_id', $_REQUEST['filter_tmc_cat_id']);
        if (isset($_REQUEST['filter_state']) && $_REQUEST['filter_state'] == 0)
            $_REQUEST["filter_state"] = "0";
        $_REQUEST['filter_state'] = $this->getCookie('filter_state', $_REQUEST['filter_state']);
        $_REQUEST['sc_id'] = $this->getCookie('sc_id', $_REQUEST['sc_id']);
        $_REQUEST['sc_tech'] = $this->getCookie('sc_tech', $_REQUEST['sc_tech']);
        $_REQUEST['filter_tech_serial'] = $this->getCookie('filter_tech_serial', $_REQUEST['filter_tech_serial']);
        if ($USER->isTech($USER->getId()) && !$USER->isChiefTech($USER->getId())) {
            $_REQUEST["sc_id"] = $USER->getScIdByUserId($USER->getId());
            $_REQUEST["sc_tech"] = $USER->getId();
        }
        if ($_REQUEST['filter_state'] == "") $_REQUEST['filter_state'] = "0";
        if (!$_REQUEST["sc_id"]) {
            redirect("main", "Не указан СЦ!");
        }
        if ($sc_id = $USER->isChiefTech($USER->getId())) {
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id"]));
        }
        $tpl->setVariable("FILTER_OPDATE", $_REQUEST["opdate"]);
        $tpl->setVariable("SELECTED_TECH_ID", $_REQUEST["sc_tech"]);
        if ($_REQUEST["sc_tech"]) {
            $sc_user = adm_users_plugin::getUser($_REQUEST["sc_tech"]);
            $tpl->setVariable("SELECTED_TECH_NAME", $sc_user["fio"]);
            $tpl->setVariable("SELECTED_TECH_ID", $_REQUEST["sc_tech"]);
        }
        $tpl->setVariable("FILTER_SC_ID", $USER->getScIdByUserId($USER->getId()));
        //$tpl->setVariable("SELECTED_TECH_ID", $_REQUEST["sc_tech"]);
        $tpl->setVariable("FILTER_SC_TECH", adm_empl_plugin::getSCTechsAll($_REQUEST["sc_id"], $_REQUEST['sc_tech']));
        $tpl->setVariable("FILTER_TMCCAT_OPTIONS", adm_material_cats_plugin::getOptions($_REQUEST["filter_tmc_cat_id"]));
        if ($_REQUEST['filter_tmc_cat_id']) {
            $isdoc = $DB->getField("SELECT `isdoc` FROM `list_material_cats` WHERE `id`=" . $DB->F($_REQUEST["filter_tmc_cat_id"]) . "; ");
            if ($isdoc > 0) {
                $tpl->setCurrentBlock("isdocfilter");
                $tpl->setVariable("FILTER_DOC_NUM_FROM", $_REQUEST["docnumfrom"] ? $_REQUEST["docnumfrom"] : $DB->getField("SELECT MIN(`docnum`) FROM `tmc_sc`;"));
                $tpl->setVariable("FILTER_DOC_NUM_TO", $_REQUEST["docnumto"] ? $_REQUEST["docnumto"] : $DB->getField("SELECT MAX(`docnum`) FROM `tmc_sc`;"));
                $tpl->parse("isdocfilter");
                $sql_docnumfilter = array();
                if ($_REQUEST["docnumfrom"])
                    $sql_docnumfilter[] = " sc.docnum>=" . $DB->F($_REQUEST["docnumfrom"]) . " ";
                if ($_REQUEST["docnumto"])
                    $sql_docnumfilter[] = " sc.docnum<=" . $DB->F($_REQUEST["docnumto"]) . " ";
            }
        }

        if ($_REQUEST["filter_tech_serial"]) {
            $serial = " AND sc.serial LIKE " . $DB->F("%" . $_REQUEST["filter_tech_serial"] . "%") . " ";
        }
        $tpl->setVariable("FILTER_TECH_SERIAL", $_REQUEST["filter_tech_serial"]);

        $tpl->setVariable("FILTER_INPLACE", array2options($inplace_filter, $_REQUEST["filter_state"]));

        $sc_filter = $_REQUEST["sc_id"];

        if ($_REQUEST["filter_tmc_cat_id"] != 0) {
            $tmccat_filter = "WHERE lmc.id=" . $DB->F($_REQUEST["filter_tmc_cat_id"]);
            $tmclist_filter = "AND lmc.id=" . $DB->F($_REQUEST["filter_tmc_cat_id"]);
        }
        if (isset($_REQUEST["filter_state"])) {
            if ($_REQUEST["filter_state"] == 0) { // Списано
                $tmcstate_filter = " AND (sct.inplace=0 OR sct.inplace IS NULL) AND (sct.incoming=0 OR sct.incoming IS NULL)";
                $tpl->setVariable("IS__HIDDEN", "class='hidden'");

            } else {
                if ($_REQUEST["filter_state"] == 2) { // Приход
                    $tmcstate_filter = " AND (sct.incoming=1 AND (sct.inplace=0 OR sct.inplace IS NULL))";
                    $tpl->setVariable("IS__HIDDEN", "class='hidden'");
                } else { // На руках
                    $tmcstate_filter = " AND (sct.inplace=1)";
                    $tpl->touchBlock("selall2");
                    $tpl->setCurrentBlock("movetotech1");
                    $tpl->setVariable("TECHLIST", adm_empl_plugin::getSCTechsAll($sc_filter));
                    $tpl->parse("movetotech1");
                }
            }
            if ($_REQUEST["opdate"] != "") {
                $tmcstate_filter .= " AND DATE_FORMAT(sct.ledit, '%Y-%m-%d')=" . $DB->F(date("Y-m-d", strtotime($_REQUEST["opdate"]))) . " ";
            }
        }
        if ($_REQUEST["filter_state"] == "0") {
            $tpl->touchBlock("motedtoth");
        }

        //$tmcstate_filter .=" AND (sct.incoming=0 OR sct.incoming IS NULL)";
        $sql = "SELECT lmc.*, (SELECT COUNT(`id`) FROM list_materials WHERE `cat_id`=lmc.id AND `metric_flag`) as ismetric FROM `list_material_cats` AS lmc " . $tmccat_filter . " ORDER BY ismetric DESC, lmc.title ASC";
        $rowcount = 0;
        $usedTmc = array();
        $DB->query($sql);
//        d($sql);die;
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {

            while ($result = $DB->fetch(true)) {
                $add = implode(" AND ", (array)$sql_docnumfilter);
                if ($add) {
                    $add = " AND " . $add;
                } else {
                    $add = " ";
                }
                $sql = "SELECT
                    sct.*,
                    sc.tmc_id,
                    sc.serial as sernum,
                    DATE_FORMAT(sct.ledit, '%H:%i:%s %d-%m-%Y') as lastdate,
                    lm.title,
                    lm.metric_flag,
                    lm.metric_title
                FROM
                    `tmc_sc_tech` AS sct
                    LEFT JOIN
                    `tmc_sc` AS sc ON sc.id=sct.tmc_sc_id
                    LEFT JOIN
                    `list_materials` AS lm ON lm.id=sc.tmc_id
                WHERE
                    sct.tech_id=" . $DB->F($_REQUEST['sc_tech']) . " AND
                    sct.sc_id=" . $DB->F($sc_filter) . " AND
                    lm.cat_id=" . $DB->F($result["id"]) . $tmcstate_filter . $add . " " . $serial . "
                ORDER BY
                    lm.title ASC";
                $DB->query($sql);
//               d($sql);die;
                if ($DB->errno()) UIError($DB->error());
                if ($DB->num_rows()) {
                    while ($r = $DB->fetch(true)) {
                        $rowcount += 1;
                        if ($r["metric_flag"] == "1") {
                            if (!in_array($r["tmc_id"], $usedTmc)) {
                                $usedTmc[] = $r["tmc_id"];
                                $query = "SELECT
                                    SUM(sct.count)
                                FROM
                                    tmc_sc_tech AS sct
                                    LEFT JOIN
                                    `tmc_sc` AS sc ON sct.tmc_sc_id=sc.id
                                WHERE
                                    sct.tech_id=" . $DB->F($_REQUEST['sc_tech']) . " AND
                                    sct.sc_id=" . $DB->F($sc_filter) . " AND
                                    sct.incoming=1 AND
                                    sct.tmc_sc_id IN (
                                        SELECT
                                            `id`
                                        FROM
                                            `tmc_sc`
                                        WHERE
                                            `tmc_id`=" . $DB->F($r["tmc_id"]) . "
                                    )
                                ;";
                                $tmcCountIn = $DB->getField($query);

                                $query = "SELECT
                                    SUM(sct.count)
                                FROM
                                    tmc_sc_tech AS sct
                                    LEFT JOIN
                                    `tmc_sc` AS sc ON sct.tmc_sc_id=sc.id
                                WHERE
                                    sct.tech_id=" . $DB->F($_REQUEST['sc_tech']) . " AND
                                    sct.sc_id=" . $DB->F($sc_filter) . " AND
                                    (!sct.inplace OR sct.inplace IS NULL) AND
                                    sct.tmc_sc_id IN (
                                        SELECT
                                            `id`
                                        FROM
                                            `tmc_sc`
                                        WHERE
                                            `tmc_id`=" . $DB->F($r["tmc_id"]) . ") AND
                                            (!sct.incoming OR sct.incoming IS NULL) AND
                                            sct.count>0 AND
                                            (!sct.inplace OR sct.inplace IS NULL)
                                        ;";
                                $tmcCountOut = $DB->getField($query);
                                $tpl->setCurrentBlock("row");
                                if ($_REQUEST["filter_state"] == "0") {
                                    $tpl->setCurrentBlock("movedtotd");
                                    $tpl->setVariable("MTTASK_ID", "&mdash;");
                                    $tpl->parse("movedtotd");
                                }
                                $tpl->setVariable("NONMETRICROW", "nonmetric");
                                $tpl->setVariable("ROW_ID", $r["tmc_sc_id"]);
                                $tpl->setVariable("ROW_CATNAME", $result["title"]);
                                $tpl->setVariable("ROW_SC", $sc_filter);
                                $tpl->setVariable("ROW_TMC_ID", $r["tmc_id"]);
                                $tpl->setVariable("ROW_TECH_ID", $_REQUEST['sc_tech']);
                                $tpl->setVariable("ROW_TITLE", $r["title"]);
                                $tpl->setVariable("ROW_SN", "&mdash;");
                                $tpl->setVariable('DISMANTLED', $r["dismantled"] ? 'dismantled' : '');
                                $lastDate = $DB->getField("SELECT DATE_FORMAT(MAX(ledit), '%H:%i:%s %d-%m-%Y') as lastdate FROM tmc_sc_tech AS sct
                                                                WHERE sct.tech_id=" . $DB->F($_REQUEST['sc_tech']) . "
                                                                        AND sct.sc_id=" . $DB->F($sc_filter) . "
                                                                        AND sct.tmc_sc_id IN (SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=" . $DB->F($r["tmc_id"]) . ") $tmcstate_filter;");
                                $user_id = $DB->getField("SELECT sct.user_id FROM tmc_sc_tech AS sct
                                                                WHERE DATE_FORMAT(ledit, '%H:%i:%s %d-%m-%Y')=" . $DB->F($lastDate) . "
                                                                        AND sct.tech_id=" . $DB->F($_REQUEST['sc_tech']) . "
                                                                        AND sct.sc_id=" . $DB->F($sc_filter) . "
                                                                        AND sct.tmc_sc_id IN (SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=" . $DB->F($r["tmc_id"]) . ") $tmcstate_filter;");
                                $auser = adm_users_plugin::getUser($user_id);
                                $tpl->setVariable("ROW_USER", $auser["fio"]);
                                $tpl->setVariable("ROW_DATE", $lastDate . " (посл. операция)");
                                if ($_REQUEST["filter_state"] == 0)
                                    $tpl->setVariable("ROW_COUNT", intval($tmcCountOut) . " " . $r["metric_title"]);
                                else {
                                    if ($_REQUEST["filter_state"] == 1)
                                        $tpl->setVariable("ROW_COUNT", (intval($tmcCountIn - $tmcCountOut) > 0 ? intval($tmcCountIn - $tmcCountOut) . " " . $r["metric_title"] : "<strong style='color:red;'>" . intval($tmcCountIn - $tmcCountOut) . " " . $r["metric_title"] . "</strong>"));
                                    else
                                        $tpl->setVariable("ROW_COUNT", intval($tmcCountIn) . " " . $r["metric_title"]);
                                }
                                if (isset($_REQUEST["filter_state"]) && ($_REQUEST["filter_state"] == 0)) {
                                    $tpl->setVariable("IS_HIDDEN", "class='hidden'");
                                } else {
                                    if (($tmcCountIn - $tmcCountOut) > 0) {
                                        $tpl->touchBlock("returnmetrictmc");
                                    }
                                    $tpl->setVariable("IS_HIDDEN", "class='hidden'");
                                }
                                $tpl->setVariable("TMC_RETURN", "deletemetrtechitem");
                                $tpl->parse("row");
                            }
                        } else {
                            $tpl->setCurrentBlock("row");
                            if ($_REQUEST["filter_state"] == 1) {
                                $tpl->setCurrentBlock("nonmetric");
                                $tpl->setVariable("ROW_IM_ID", $r["tmc_sc_id"]);
                                $tpl->parse("nonmetric");
                            }
                            if ($_REQUEST["filter_state"] == "0") {
                                $sql = "SELECT `task_id` FROM `tmc_ticket` WHERE `tmc_tech_sc_id`=" . $DB->F($r["id"]);
                                $t1 = $DB->getField($sql);
                                $str = array();
                                if ($t1) {
                                    $task = new Task($t1);
                                    if ($task) {
                                        $str[] = "<a target=\"_blank\" title=\"Списано на заявку №" . $task->getId() . "\" href=" . link2($task->getPluginUid() . "/viewticket?task_id=" . $task->getId(), false) . ">" . $task->getId() . "</a>";
                                    }
                                } else {
                                    $sql = "SELECT `id` FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $DB->F($r["id"]) . ";";
                                }

                                $tpl->setCurrentBlock("movedtotd");
                                $tpl->setVariable("MTTASK_ID", count($str) > 0 ? implode(",", $str) : "&mdash;");
                                $tpl->parse("movedtotd");
                            }
                            $tpl->setVariable("ROW_ID", $r["tmc_sc_id"]);
                            $tpl->setVariable("ROW_CATNAME", $result["title"]);
                            $tpl->setVariable("ROW_TITLE", $r["title"]);
                            $tpl->setVariable("ROW_SN", $r["sernum"]);
                            $tpl->setVariable("ROW_DATE", $r["lastdate"]);
                            $tpl->setVariable("ROW_COUNT", $r["count"] . " " . $r["metric_title"]);
                            $tpl->setVariable('DISMANTLED', $r["dismantled"] ? 'dismantled' : '');
                            $auser = adm_users_plugin::getUser($r['user_id']);
                            $tpl->setVariable("ROW_USER", $auser["fio"]);
                            if (isset($_REQUEST["filter_state"]) && ($_REQUEST["filter_state"] == 0 || $_REQUEST["filter_state"] == 2)) {
                                $tpl->setVariable("IS_HIDDEN", "class='hidden'");
                            }
                            $tpl->setVariable("TMC_RETURN", "deletetechitem");
                            $tpl->parse("row");
                        }

                    }
                }
                $DB->free();
            }
            if (!$rowcount) $tpl->touchBlock("no-rows");
        } else {
            UIError("Отсутствуют категории ТМЦ!");
        }
        $DB->free();
        $tpl->show();
    }

    function movetonewtech()
    {
        global $DB, $USER, $PLUGINS;
        if (isset($_REQUEST["returntoprevsc"])) {
            if ($_REQUEST["seltmc"]) {
                foreach ($_REQUEST["seltmc"] as $item) {
                    $tmc = $DB->getRow("select * from tmc_sc_tech where tmc_sc_id=" . $DB->F($item), true);

                    $sql = "UPDATE `tmc_sc` SET `inplace`=1, ledit = now() WHERE `inplace`=0 AND `id`=" . $DB->F($item) . " AND dismantled = " . $DB->F($tmc['dismantled']) . ";";
                    $DB->query($sql);
                    if ($DB->errno()) UIError($DB->error());
                    $sql = "UPDATE `tmc_sc_tech` SET `inplace`=0 WHERE `tmc_sc_id`=".$DB->F($item)." AND `inplace`=1;";
                    //$sql = "DELETE FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $DB->F($item);
                    $DB->query($sql);
                }
            }
        } else {
            if (count($_REQUEST["seltmc"]) && $_REQUEST["tech_id"]) {
                foreach ($_REQUEST["seltmc"] as $item) {
                    $r = $DB->getField("SELECT `tmc_ticket_id` FROM `tmc_sc` WHERE `id`=" . $DB->F($item) . ";");
                    if ($r > 0) {
                        $result = $DB->query("UPDATE `tmc_ticket` SET `tmc_tech_sc_id`=0, ledit=now() WHERE `id`=" . $DB->F($r) . ";");
                        $DB->free($result);
                    }
                }
                $sql = "UPDATE `tmc_sc_tech` SET `user_id`=" . $DB->F($USER->getId()) . ", `tech_id`=" . $DB->F($_REQUEST["tech_id"]) . ", ledit=now() WHERE `tmc_sc_id` IN (" . implode(",", $_REQUEST["seltmc"]) . ")";

                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                $msg = "ТМЦ успешно передано технику!";
            } else {
                $msg = "Не выбраны ТМЦ для передачи или техник!";
            }
        }
        redirect(link2("tmc/viewtech", false), $msg);
        return;
    }

    function viewtech_ex()
    {
        global $DB, $USER, $PLUGINS;
        header("Content-type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=tmc_sc__export_" . date("H_i_d-M-Y") . ".xls");

        $inplace_filter = array("0" => "Списано", "1" => "На руках", "2" => "Приход");

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/techexport.tmpl.htm");

        $_REQUEST['filter_tmc_cat_id'] = $this->getCookie('filter_tmc_cat_id', $_REQUEST['filter_tmc_cat_id']);
        if (isset($_REQUEST['filter_state']) && $_REQUEST['filter_state'] == 0)
            $_REQUEST["filter_state"] = "0";
        $_REQUEST['filter_state'] = $this->getCookie('filter_state', $_REQUEST['filter_state']);
        $_REQUEST['sc_id'] = $this->getCookie('sc_id', $_REQUEST['sc_id']);
        $_REQUEST['sc_tech'] = $this->getCookie('sc_tech', $_REQUEST['sc_tech']);
        $_REQUEST['filter_tech_serial'] = $this->getCookie('filter_tech_serial', $_REQUEST['filter_tech_serial']);

        $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id"]));
        $tpl->setVariable("FILTER_SC_TECH", adm_empl_plugin::getSCTechsAll($_REQUEST["sc_id"], $_REQUEST['sc_tech']));
        $tpl->setVariable("FILTER_TMCCAT_OPTIONS", adm_material_cats_plugin::getOptions($_REQUEST["filter_tmc_cat_id"]));
        $tpl->setVariable("FILTER_INPLACE", array2options($inplace_filter, $_REQUEST["filter_state"]));
        $tpl->setVariable("FILTER_OPDATE", $_REQUEST["opdate"]);
        $sc_filter = $_REQUEST["sc_id"];
        if ($_REQUEST["filter_tech_serial"]) {
            $serial = " AND sc.serial LIKE " . $DB->F("%" . $_REQUEST["filter_tech_serial"] . "%") . " ";
        }
        if ($_REQUEST["filter_tmc_cat_id"] != 0) {
            $tmccat_filter = "WHERE lmc.id=" . $DB->F($_REQUEST["filter_tmc_cat_id"]);
            $tmclist_filter = "AND lmc.id=" . $DB->F($_REQUEST["filter_tmc_cat_id"]);
            $isdoc = $DB->getField("SELECT `isdoc` FROM `list_material_cats` WHERE `id`=" . $DB->F($_REQUEST["filter_tmc_cat_id"]) . "; ");
            if ($isdoc > 0) {
                $tpl->setCurrentBlock("isdocfilter");
                $tpl->setVariable("FILTER_DOC_NUM_FROM", $_REQUEST["docnumfrom"] ? $_REQUEST["docnumfrom"] : $DB->getField("SELECT MIN(`docnum`) FROM `tmc_sc`;"));
                $tpl->setVariable("FILTER_DOC_NUM_TO", $_REQUEST["docnumto"] ? $_REQUEST["docnumto"] : $DB->getField("SELECT MAX(`docnum`) FROM `tmc_sc`;"));
                $tpl->parse("isdocfilter");
                $sql_docnumfilter = array();
                if ($_REQUEST["docnumfrom"])
                    $sql_docnumfilter[] = " sc.docnum>=" . $DB->F($_REQUEST["docnumfrom"]) . " ";
                if ($_REQUEST["docnumto"])
                    $sql_docnumfilter[] = " sc.docnum<=" . $DB->F($_REQUEST["docnumto"]) . " ";
            }
        }

        if (isset($_REQUEST["filter_state"])) {
            if ($_REQUEST["filter_state"] == 0) { // Списано
                $tmcstate_filter = " AND (sct.inplace=0 OR sct.inplace IS NULL) AND (sct.incoming=0 OR sct.incoming IS NULL)";
                $tpl->setVariable("IS__HIDDEN", "class='hidden'");
                $add = " (Списано)";

            } else {
                if ($_REQUEST["filter_state"] == 2) { // Приход
                    $tmcstate_filter = " AND (sct.incoming=1 AND (sct.inplace=0 OR sct.inplace IS NULL))";
                    $add = " (Приход)";
                } else { // На руках
                    $tmcstate_filter = " AND (sct.inplace=1)";
                    $add = " (На руках)";
                }
            }
            if ($_REQUEST["opdate"] != "") {
                $tmcstate_filter .= " AND DATE_FORMAT(sct.ledit, '%Y-%m-%d')=" . $DB->F(date("Y-m-d", strtotime($_REQUEST["opdate"]))) . " ";
            }
        }
        if ($_REQUEST["filter_state"] == "0") {
            $tpl->touchBlock("motedtoth");
        }
        $tech = adm_empl_plugin::getTechFIO($_REQUEST['sc_tech']);
        $tpl->setVariable("TECH_FIO", $tech . $add);
        $tmcstate_filter .= " AND (sct.incoming=0 OR sct.incoming IS NULL)";
        $sql = "SELECT lmc.*, (SELECT COUNT(`id`) FROM list_materials WHERE `cat_id`=lmc.id AND `metric_flag`) as ismetric FROM `list_material_cats` AS lmc " . $tmccat_filter . " ORDER BY ismetric DESC, lmc.title ASC";
        $rowcount = 0;
        $usedTmc = array();
        $DB->query($sql);
//        d($sql);die;
        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {

            while ($result = $DB->fetch(true)) {
                $add = implode(" AND ", $sql_docnumfilter);
                if ($add) {
                    $add = " AND " . $add;
                } else {
                    $add = " ";
                }
                $sql = "SELECT
                    sct.*,
                    sc.tmc_id,
                    sc.serial as sernum,
                    DATE_FORMAT(sct.ledit, '%H:%i:%s %d-%m-%Y') as lastdate,
                    lm.title,
                    lm.metric_flag,
                    lm.metric_title
                FROM
                    `tmc_sc_tech` AS sct
                    LEFT JOIN
                    `tmc_sc` AS sc ON sc.id=sct.tmc_sc_id
                    LEFT JOIN
                    `list_materials` AS lm ON lm.id=sc.tmc_id
                WHERE
                    sct.tech_id=" . $DB->F($_REQUEST['sc_tech']) . " AND
                    sct.sc_id=" . $DB->F($sc_filter) . " AND
                    lm.cat_id=" . $DB->F($result["id"]) . $tmcstate_filter . $add . " " . $serial;
//                die($sql);
                $DB->query($sql);
                if ($DB->errno()) UIError($DB->error());
                if ($DB->num_rows()) {
                    while ($r = $DB->fetch(true)) {
                        $rowcount += 1;

                        if ($r["metric_flag"] == "1") {
                            if (!in_array($r["tmc_id"], $usedTmc)) {
                                $usedTmc[] = $r["tmc_id"];
                                $query = "SELECT
                                    SUM(sct.count)
                                FROM
                                    tmc_sc_tech AS sct
                                    LEFT JOIN
                                    `tmc_sc` AS sc ON sct.tmc_sc_id=sc.id
                                WHERE
                                    sct.tech_id=" . $DB->F($_REQUEST['sc_tech']) . " AND
                                    sct.sc_id=" . $DB->F($sc_filter) . " AND
                                    sct.incoming=1 AND
                                    sct.tmc_sc_id IN (
                                        SELECT
                                            `id`
                                        FROM
                                            `tmc_sc`
                                        WHERE
                                            `tmc_id`=" . $DB->F($r["tmc_id"]) . "
                                    )
                                ;";
                                $tmcCountIn = $DB->getField($query);

                                $query = "SELECT
                                    SUM(sct.count)
                                FROM
                                    tmc_sc_tech AS sct
                                    LEFT JOIN
                                    `tmc_sc` AS sc ON sct.tmc_sc_id=sc.id
                                WHERE
                                    sct.tech_id=" . $DB->F($_REQUEST['sc_tech']) . " AND
                                    sct.sc_id=" . $DB->F($sc_filter) . " AND
                                    (!sct.inplace OR sct.inplace IS NULL) AND
                                    sct.tmc_sc_id IN (
                                        SELECT
                                            `id`
                                        FROM
                                            `tmc_sc`
                                        WHERE
                                            `tmc_id`=" . $DB->F($r["tmc_id"]) . ") AND
                                            (!sct.incoming OR sct.incoming IS NULL) AND
                                            sct.count>0 AND
                                            (!sct.inplace OR sct.inplace IS NULL)
                                        ;";
                                $tmcCountOut = $DB->getField($query);
                                $tpl->setCurrentBlock("row");

                                if ($_REQUEST["filter_state"] == "0") {
                                    $tpl->setCurrentBlock("movedtotd");
                                    $tpl->setVariable("MTTASK_ID", "&mdash;");
                                    $tpl->parse("movedtotd");
                                }
                                $tpl->setVariable("ROW_ID", $r["tmc_sc_id"]);
                                $tpl->setVariable("ROW_CATNAME", $result["title"]);
                                $tpl->setVariable("ROW_SC", $sc_filter);
                                $tpl->setVariable("ROW_TMC_ID", $r["tmc_id"]);
                                $tpl->setVariable("ROW_TECH_ID", $_REQUEST['sc_tech']);
                                $tpl->setVariable("ROW_TITLE", $r["title"]);
                                $tpl->setVariable("ROW_SN", "&mdash;");
                                $lastDate = $DB->getField("SELECT DATE_FORMAT(MAX(ledit), '%H:%i:%s %d-%m-%Y') as lastdate FROM tmc_sc_tech AS sct
                                                                WHERE sct.tech_id=" . $DB->F($_REQUEST['sc_tech']) . "
                                                                        AND sct.sc_id=" . $DB->F($sc_filter) . "
                                                                        AND sct.tmc_sc_id IN (SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=" . $DB->F($r["tmc_id"]) . ") $tmcstate_filter;");
                                $user_id = $DB->getField("SELECT sct.user_id FROM tmc_sc_tech AS sct
                                                                WHERE DATE_FORMAT(ledit, '%H:%i:%s %d-%m-%Y')=" . $DB->F($lastDate) . "
                                                                        AND sct.tech_id=" . $DB->F($_REQUEST['sc_tech']) . "
                                                                        AND sct.sc_id=" . $DB->F($sc_filter) . "
                                                                        AND sct.tmc_sc_id IN (SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=" . $DB->F($r["tmc_id"]) . ") $tmcstate_filter;");
                                $auser = adm_users_plugin::getUser($user_id);
                                $tpl->setVariable("ROW_USER", $auser["fio"]);
                                $tpl->setVariable("ROW_DATE", $lastDate . " (посл. операция)");
                                if (isset($_REQUEST["filter_state"]) && $_REQUEST["filter_state"] == 0)
                                    $tpl->setVariable("ROW_COUNT", intval($tmcCountOut) . " " . $r["metric_title"]);
                                else
                                    $tpl->setVariable("ROW_COUNT", intval($tmcCountIn - $tmcCountOut) . " " . $r["metric_title"]);
                                if (isset($_REQUEST["filter_state"]) && $_REQUEST["filter_state"] == 0) {
                                    $tpl->setVariable("IS_HIDDEN", "class='hidden'");
                                }
                                $tpl->setVariable("TMC_RETURN", "deletemetrtechitem");
                                $tpl->parse("row");

                            }
                        } else {
                            $tpl->setCurrentBlock("row");

                            $tpl->setVariable("ROW_ID", $r["tmc_sc_id"]);
                            $tpl->setVariable("ROW_CATNAME", $result["title"]);
                            $tpl->setVariable("ROW_TITLE", $r["title"]);
                            $tpl->setVariable("ROW_SN", $r["sernum"]);
                            $tpl->setVariable("ROW_DATE", $r["lastdate"]);
                            $tpl->setVariable("ROW_COUNT", $r["count"] . " " . $r["metric_title"]);
                            if (isset($_REQUEST["filter_state"]) && $_REQUEST["filter_state"] == 0) {
                                $tpl->setVariable("IS_HIDDEN", "class='hidden'");
                            }
                            $tpl->setVariable("TMC_RETURN", "deletetechitem");
                            if ($_REQUEST["filter_state"] == "0") {
                                $sql = "SELECT `task_id` FROM `tmc_ticket` WHERE `tmc_tech_sc_id`=" . $DB->F($r["id"]);
                                $t1 = $DB->getField($sql);
                                $str = array();
                                if ($t1) {
                                    $task = new Task($t1);
                                    if ($task) {
                                        $str[] = "<a target=\"_blank\" title=\"Списано на заявку №" . $task->getId() . "\" href=" . link2($task->getPluginUid() . "/viewticket?task_id=" . $task->getId(), false) . ">" . $task->getId() . "</a>";
                                    }
                                } else {
                                    $sql = "SELECT `id` FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $DB->F($r["id"]) . ";";
                                }

                                $tpl->setCurrentBlock("movedtotd");
                                $tpl->setVariable("MTTASK_ID", count($str) > 0 ? implode(",", $str) : "&mdash;");
                                $tpl->parse("movedtotd");
                            }
                            $tpl->parse("row");
                        }

                    }
                }
                $DB->free();
            }
            if (!$rowcount) $tpl->touchBlock("no-rows");
        }
        $DB->free();
        $tpl->show();
    }

    function addtmctech()
    {
        global $DB, $USER, $PLUGINS;

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/addtmctech.tmpl.htm");
        if ($sc_id = $USER->isChiefTech($USER->getId())) {
            $_REQUEST["sc_id"] = $sc_id;
            $tpl->setVariable("SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id"]));
        }
        $tpl->setVariable("TMCCAT_OPTIONS", adm_material_cats_plugin::getOptions());
        $tpl->setVariable("SCTECH_OPTIONS", array2options(adm_empl_plugin::getSCTechs($_REQUEST["sc_id"]), $_REQUEST["sc_tech"]));
        UIHeader($tpl);
        $tpl->show();
    }

    function addtmctech_save()
    {
        global $DB, $USER, $PLUGINS;

        if (isset($_POST["serial"])) {
            //$lid = $DB->getField("SELECT `id` FROM `tmc_sc` WHERE `serial`=".$DB->F($_POST["serial"])." AND `inplace`;");
            $sql = "UPDATE `tmc_sc` SET `inplace`=0, ledit=now() WHERE `id`=" . $_POST["serial"] . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `inplace`, ledit)
            VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["sc_tech"]) . ", " . $DB->F($_POST["serial"]) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", 1, now());";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `incoming`, ledit)
            VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["sc_tech"]) . ", " . $DB->F($_POST["serial"]) . ", " . $DB->F(1) . ", " . $DB->F($USER->getId()) . ", 1, now());";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
        } else {
            if (!$_POST["quan"]) UIError("Нельзя выдать нулевое количество метрических ТМЦ");
            /*$sql = "SELECT `id` FROM `tmc_sc` WHERE `tmc_id`=".$_POST["tmc_id"]." AND inplace=1";
            $sc_inplace = $DB->getField($sql);
            $sql = "UPDATE `tmc_sc` SET `count` = ".$DB->F($_POST["maxval"] - $_POST["quan"])." WHERE `sc_id`=".$_POST["sc_id"]." AND `tmc_id`=".$_POST["tmc_id"]." AND inplace=1;";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());*/
            $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `count`, `user_id`, `inplace`, ledit)
            VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["tmc_id"]) . ", " . $DB->F(intval($_POST["quan"])) . ", " . $DB->F($USER->getId()) . ", 0, now());";
            $DB->query($sql);
            $tmcscrowid = $DB->insert_id();
            if ($DB->errno()) UIError($DB->error());
            $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `inplace`, ledit)
            VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["sc_tech"]) . ", " . $DB->F($tmcscrowid) . ", " . $DB->F(intval($_POST["quan"])) . ", " . $DB->F($USER->getId()) . ", 1, now());";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `incoming`, ledit)
            VALUES (" . $DB->F($_POST["sc_id"]) . ", " . $DB->F($_POST["sc_tech"]) . ", " . $DB->F($tmcscrowid) . ", " . $DB->F(intval($_POST["quan"])) . ", " . $DB->F($USER->getId()) . ", 1, now());";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
        }

        redirect("viewtech", "ТМЦ успешно списано с СЦ и передано Технику!");

    }

    function getTmcListOnCSC_ajax()
    {
        global $DB;
        $cat_id = $_POST["cat_id"];
        $sc_id = $_POST["sc_id"];
        $sql = "SELECT mat.id, mat.title FROM `tmc_sc` AS sc LEFT JOIN `list_materials` AS mat ON mat.id=sc.tmc_id WHERE sc.sc_id=" . $DB->F($sc_id) . " AND mat.cat_id=" . $DB->F($cat_id) . " AND inplace;";
        echo "<option value=\"0\">-- выберите ТМЦ</option>" . array2options($DB->getCell2($sql));
    }

    function getTmcParams_alax()
    {
        global $DB;
        $tmc = $_POST['tmc_id'];
        $sc_id = $_POST['sc_id'];

        if (!$sc_id || !$tmc) {
            $ret["interface"] = "none";
        } else {
            $sql = "SELECT `metric_flag`, `metric_title` FROM `list_materials` AS mat WHERE mat.id=" . $DB->F($tmc) . ";";
            $DB->query($sql);
            if ($DB->num_rows()) {
                $result = $DB->fetch(true);
                if ($result["metric_flag"]) {
                    $tmcCountIN = $DB->getField("SELECT SUM(sc.count) FROM `tmc_sc` AS sc WHERE sc.sc_id=" . $DB->F($sc_id) . " AND sc.tmc_id=" . $DB->F($tmc) . " AND (sc.inplace=1) AND (sc.incoming=0 OR sc.incoming IS NULL)");
                    $tmcCountOUT = $DB->getField("SELECT SUM(sc.count) FROM `tmc_sc` AS sc WHERE sc.sc_id=" . $DB->F($sc_id) . " AND sc.tmc_id=" . $DB->F($tmc) . "AND (!sc.incoming OR sc.incoming IS NULL) AND sc.count>0 AND (!sc.inplace OR sc.inplace IS NULL)");

                    $tmcCount = $tmcCountIN - $tmcCountOUT;
                    $ret["interface"] = "metric";
                    $ret["maxval"] = $tmcCount;
                } else {
                    $serials = $DB->getCell2("SELECT `id`, `serial` FROM `tmc_sc` WHERE `sc_id`=" . $DB->F($sc_id) . " AND `tmc_id`=" . $DB->F($tmc) . " AND inplace=1;");
                    $ret["interface"] = "serials";
                    $ret["html"] = array2options($serials);
                }
            } else {
                $ret["error"] = "Не найдена ТМЦ с указанным идентификатором!";
            }
        }
        $DB->free();
        echo json_encode($ret);
    }

    function deletescitem()
    {
        global $DB;
        if ($id = $_REQUEST["id"]) {


            $sql = "SELECT * FROM `tmc_sc` WHERE `id`=" . $DB->F($id) . " AND `id` IN (SELECT `tmc_sc_id` FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $DB->F($id) . ");";
            $DB->query($sql);

            while ($r = $DB->fetch(true)) {
                if ($r["from_task_id"] > 0) {
                    $sql = "UPDATE `tmc_sc` SET `deleted`=" . $DB->F(1) . ", ledit=now() WHERE `serial`=" . $DB->F($r["serial"]) . " AND `tmc_id`=" . $DB->F($r["tmc_id"]);
                    $DB->query($sql);
                } else {
                    $sql = "DELETE FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $DB->F($r["id"]);
                    $DB->query($sql);

                    $sql = "DELETE FROM `tmc_sc` WHERE `serial`=" . $DB->F($r["serial"]) . " AND `tmc_id`=" . $DB->F($r["tmc_id"]);
                    $DB->query($sql);
                }
            }
            redirect("viewsc", "ТМЦ удалена! (если ранее она не выдавалась)");
        } else {
            UIError("Не указан идентификатор для удаления!");
        }
    }

    function deletemetricscitem_ajax()
    {
        global $DB, $USER;
        $err = array();
        $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `count`, `user_id`, `inplace`, ledit, `serial`)
            VALUES (" . $DB->F($_REQUEST["sc_id"]) . ", " . $DB->F($_REQUEST["tmc_id"]) . ", " . $DB->F(intval($_REQUEST["amount"])) . ", " . $DB->F($USER->getId()) . ", 0, now(), null);";
        $DB->query($sql);
        if ($DB->errno()) $err[] = $DB->error();
        if (sizeof($err)) {
            $ret["error"] = "Ошибка: \r\n" . implode("\r\n", $err);
        } else {
            $ret["ok"] = "ok";
        }
        echo json_encode($ret);

    }

    function deletetechitem()
    {
        global $DB, $USER;
        if ($id = $_REQUEST["id"]) {
            $sql = "UPDATE `tmc_sc` SET `inplace`=1, ledit=now() WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            $sql = "SELECT * FROM `tmc_sc` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->errno())
                UIError($DB->error());
            $r = $DB->fetch(true);
            $DB->free();
            $sql = "UPDATE `tmc_sc_tech` SET `inplace`=0 WHERE `tmc_sc_id`=".$DB->F($id)." AND `inplace`=1;";
            //$sql = "DELETE FROM `tmc_sc_tech` WHERE `tmc_sc_id`=" . $DB->F($id);
            $DB->query($sql);
            /*$r1 = $DB->fetch(true);
            if ($DB->errno()) UIError($DB->error());
            $DB->free();
            $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `inplace`)
            VALUES (".$DB->F($r["sc_id"]).", ".$DB->F($r1["tech_id"]).", ".$DB->F($r["serial"]).", ".$DB->F(1).", ".$DB->F($USER->getId()).", 0);";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            */
        } else {
            UIError("Не указан идентификатор для удаления!");
        }
        redirect("viewtech", "ТМЦ возвращена на склад СЦ!");
    }

    function deletemetrtechitem_ajax()
    {
        global $DB, $USER;
        $err = array();
        $ret = false;
        if ($id = $_REQUEST["id"]) {
            /*$maxval = $DB->getField("SELECT `count` FROM tmc_sc WHERE `sc_id`=".$DB->F($_REQUEST["sc_id"])." AND `tmc_id`=".$DB->F($_REQUEST["tmc_id"])." AND `inplace`=1;");
            if ($DB->errno()) $err[] = $DB->error();
            $sql = "UPDATE `tmc_sc` SET `count` = ".$DB->F($maxval + $_REQUEST["amount"])." WHERE `sc_id`=".$_REQUEST["sc_id"]." AND `tmc_id`=".$_REQUEST["tmc_id"]." AND inplace=1;";
            $DB->query($sql);
            if ($DB->errno()) $err[] = $DB->error();*/
            $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `count`, `user_id`, `inplace`, ledit)
            VALUES (" . $DB->F($_REQUEST["sc_id"]) . ", " . $DB->F($_REQUEST["tmc_id"]) . ", " . $DB->F(intval($_REQUEST["amount"])) . ", " . $DB->F($USER->getId()) . ", 1, now());";
            $DB->query($sql);
            $sql = "INSERT INTO `tmc_sc` (`sc_id`, `tmc_id`, `count`, `user_id`, `inplace`, ledit)
            VALUES (" . $DB->F($_REQUEST["sc_id"]) . ", " . $DB->F($_REQUEST["tmc_id"]) . ", " . $DB->F(intval($_REQUEST["amount"]) * (-1)) . ", " . $DB->F($USER->getId()) . ", 0, now());";
            $DB->query($sql);
            $tmcscrowid = $DB->insert_id();
            if ($DB->errno()) $err[] = $DB->error();
            $sql = "INSERT INTO `tmc_sc_tech` (`sc_id`, `tech_id`, `tmc_sc_id`, `count`, `user_id`, `inplace`, ledit)
            VALUES (" . $DB->F($_REQUEST["sc_id"]) . ", " . $DB->F($_REQUEST["tech_id"]) . ", " . $DB->F($tmcscrowid) . ", " . $DB->F(intval($_REQUEST["amount"])) . ", " . $DB->F($USER->getId()) . ", 0, now());";
            $DB->query($sql);
            if ($DB->errno()) $err[] = $DB->error();
            if (sizeof($err)) {
                $ret["error"] = "Ошибка! \r\n" . implode("\r\n", $err);
            } else {
                $ret["ok"] = "ok";
            }
        } else {
            $ret["error"] = "Не указан идентификатор для удаления!";
        }
        echo json_encode($ret);
    }


}

?>