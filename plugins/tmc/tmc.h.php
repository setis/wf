<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "ТМЦ";
$PLUGINS[$plugin_uid]['hidden'] = false;

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "ТМЦ";
    $PLUGINS[$plugin_uid]['events']['viewsc'] = "Склады Сервисных Центров";
    $PLUGINS[$plugin_uid]['events']['viewtech'] = "ТМЦ у Техников";
    $PLUGINS[$plugin_uid]['events']['newtmc'] = "Добавить ТМЦ на склад СЦ";
    $PLUGINS[$plugin_uid]['events']['viewsc_ex'] = "Экспорт в Excel";
    $PLUGINS[$plugin_uid]['events']['viewtech_ex'] = "Экспорт в Excel";
    $PLUGINS[$plugin_uid]['events']['getTmcListOnCSC_ajax'] = "Список ТМЦ на СЦ";
    $PLUGINS[$plugin_uid]['events']['getTmcParams_alax'] = "Список ТМЦ на СЦ";       
}

if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['addtmc'] = "Добавление ТМЦ";
    $PLUGINS[$plugin_uid]['events']['addtmc_save'] = "Добавление ТМЦ (сохранение)";
    $PLUGINS[$plugin_uid]['events']['addtmctech'] = "Передача ТМЦ технику";
    $PLUGINS[$plugin_uid]['events']['addtmctech_save'] = "Передача ТМЦ технику (сохранение)";
    $PLUGINS[$plugin_uid]['events']['deletescitem'] = "Удаление ТМЦ из СЦ";
    $PLUGINS[$plugin_uid]['events']['deletemetricscitem_ajax'] = "Удаление ТМЦ из СЦ";
    $PLUGINS[$plugin_uid]['events']['deletemetrtechitem_ajax'] = "Удаление ТМЦ у Техника (метр)";
    $PLUGINS[$plugin_uid]['events']['deletetechitem'] = "Удаление ТМЦ у Техника";
    $PLUGINS[$plugin_uid]['events']['importtosc'] = "Импорт ТМЦ из файла";
    $PLUGINS[$plugin_uid]['events']['movetotech'] = "Передача нескольких ТМЦ технику";    
    $PLUGINS[$plugin_uid]['events']['movetonewtech'] = "Передача нескольких ТМЦ между техниками";    
    
}

$plugin_uid = "tmc_main_stock";

$PLUGINS[$plugin_uid]['name'] = "Распределение товаров по складам СЦ";
$PLUGINS[$plugin_uid]['hidden'] = true;
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['mainstocktosc'] = "Выдача ТМЦ на склады СЦ";
    $PLUGINS[$plugin_uid]['events']['importgdstostock'] = "Импорт ТМЦ из файла";
}


?>