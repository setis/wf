<?php

/**
 * Plugin Header
 *
 * @author kblp
 * @copyright 2013
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "График";
$PLUGINS[$plugin_uid]['hidden'] = false;

if (wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['view_schedule'] = "График работ";
    $PLUGINS[$plugin_uid]['events']['view_details'] = "Детальное расписание";
    $PLUGINS[$plugin_uid]['events']['print_orders_day'] = "Пакетная печать Заказ-нарядов за день";
    $PLUGINS[$plugin_uid]['events']['print_orders_day_tech'] = "Пакетная печать заказ-нарядов за день по технику";
    $PLUGINS[$plugin_uid]['events']['printtickets'] = "Пакетная печать заявок за день по технику";
    $PLUGINS[$plugin_uid]['events']['printscticket'] = "Пакетная печать заявок за день по СЦ";


}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['insertticket'] = "Добавление заявки";
    $PLUGINS[$plugin_uid]['events']['insertticketaction'] = "Добавление заявки (action)";
    $PLUGINS[$plugin_uid]['events']['insertticketinfo'] = "Получение информации по Добавлению заявки";
    $PLUGINS[$plugin_uid]['events']['getgfxcell_ajax'] = "Установка заявки в график с получением ячейки графика";
    $PLUGINS[$plugin_uid]['events']['deletefromgfxcell_ajax'] = "Удаление заявки из графика с получением ячейки графика";
    $PLUGINS[$plugin_uid]['events']['movefromgfxcell_ajax'] = "Перенос заявки из графика с получением ячейки графика";

}
