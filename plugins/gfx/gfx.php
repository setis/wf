<?php

/**
 * Plugin Implementation
 *
 * @author kblp
 * @copyright 2013
 */

use classes\HTML_Template_IT;
use classes\Plugin;
use classes\Task;
use classes\tickets\AccidentTicket;
use classes\tickets\ConnectionTicket;
use classes\tickets\ServiceTicket;
use classes\User;
use Gorserv\Gerp\ScheduleBundle\Model\TimeframeInterface;
use models\ExecutorTimeframe;
use models\Gfx as Schedule;
use models\SkpTechDebts;
use models\Task as OrmTask;
use models\TaskStatus;
use models\User as OrmUser;
use models\UserMoneyLimit;
use models\UserStatistic;
use repository\TaskStatusRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class gfx_plugin extends Plugin
{
    private $moneyLimits = [];
    private $debts = [];
    private $userStat = [];

    function __construct()
    {
        parent::__construct($plugin_uid = basename(__file__, '.php'));
    }

    static function getCurrentTechIdAdGfx($task_id)
    {
        global $DB;
        if (!$task_id)
            return false;
        $sql = "SELECT `empl_id` FROM `gfx` WHERE `task_id`=" . $DB->F($task_id) . ";";

        return $DB->getField($sql);

    }

    public function view_schedule()
    {
        global $USER;

        $LKUser = adm_users_plugin::isLKUser($USER->getId());

        if ($LKUser) {
            throw new \Symfony\Component\Security\Core\Exception\AccessDeniedException('У вас нет доступа.');
        }

        $isTech = User::isTech($USER->getId());
        $isChiefTech = User::isChiefTech($USER->getId());

        if (isset($_POST["save"])) {
            if ($this->save($_POST)) {
                redirect($this->getLink('view_schedule') . "?wDate=" . $_POST["startDate"] . "&scFilter=" . $_POST["sc"], "Расписание сохранено");
            } else {
                UIError("Сохранение не выполнено!");
            }
        }

        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/gfx.tmpl.htm");

        $this->getSession()->set("gfx_filter_area_id", $_REQUEST['filter_area_id'] ? $_REQUEST['filter_area_id'] : $this->getSession()->get("gfx_filter_area_id"));
        $_REQUEST['filter_area_id'] = $this->getSession()->get("gfx_filter_area_id");

        $this->getSession()->set("gfx_filter_wtype_id", $_REQUEST['wtype_id'] ? $_REQUEST['wtype_id'] : $this->getSession()->get("gfx_filter_wtype_id"));
        $_REQUEST['wtype_id'] = $this->getSession()->get("gfx_filter_wtype_id");

        if ($isTech && !$isChiefTech) {
            $this->viewScheduleTech($tpl);
        } else {
            $this->viewScheduleOthers($tpl);
        }

        UIHeader($tpl);
        $tpl->show();
    }

    private function viewScheduleTech(HTML_Template_IT $tpl)
    {
        global $DB, $USER;
        $dateFormat = 'd-m-Y';
        $interfaceDateFormat = 'd M Y';
        $cDate = date('D');

        $taskList = [];
        $tpl->loadTemplatefile($USER->getTemplate() . "/gfxtech.tmpl.htm");
        $dayCount = 1;
        $startDate =
            $dayCount != 7 ?
                ($_REQUEST["wDate"] ? $_REQUEST["wDate"] : date($dateFormat))
                :
                (
                    $_REQUEST["wDate"] ?
                        (
                            date('w', strtotime($_REQUEST["wDate"])) == 1 ?
                                $_REQUEST["wDate"]
                                :
                                (
                                    date('w', strtotime($_REQUEST["wDate"])) ?
                                        date($dateFormat, strtotime($_REQUEST["wDate"]) - (date('w', strtotime($_REQUEST["wDate"])) - 1) * 24 * 60 * 60)
                                        :
                                        date($dateFormat, strtotime($_REQUEST["wDate"]) - 6 * 24 * 60 * 60)
                                )
                        )
                        :
                        ($cDate == "Sun" ? date($dateFormat, strtotime('Mon last week')) : date($dateFormat, strtotime('Mon this week')))
                );
        $endDate = date($dateFormat, strtotime($startDate) + ($dayCount - 1) * 86400);

        $tpl->setVariable("INT_START", rudate($interfaceDateFormat, $startDate));
        $tpl->setVariable("CURRENTSTARTDATE", $startDate);
        $tpl->setVariable("INT_END", rudate($interfaceDateFormat, $endDate));
        $tpl->setVariable("NEXTWEEKLINK", $this->getUID() . "/view_schedule/?wDate=" .
            date($dateFormat, strtotime($endDate) + 24 * 60 * 60) . "&dc=$dayCount" . ($scFilter ?
                "&scFilter=" . $scFilter : "") . "&techwtf=" . $techwtf . $addAreas);
        $tpl->setVariable("PREVWEEKLINK", $this->getUID() . "/view_schedule/?wDate=" .
            date($dateFormat, strtotime($startDate) - $dayCount * 24 * 60 * 60) . "&dc=$dayCount" .
            ($scFilter ? "&scFilter=" . $scFilter : "") . "&techwtf=" . $techwtf . $addAreas);
        $tpl->setVariable("CURRENTWEEKLINK", $this->getUID() . "/view_schedule" . ($scFilter ?
                "?scFilter=" . $scFilter . "&dc=$dayCount" : "?dc=$dayCount") . "&techwtf=" . $techwtf .
            $addAreas);

        for ($i = 0; $i < $dayCount; $i++) {
            $tpl->setCurrentBlock("dateGridHead");
            if (date($dateFormat, strtotime($startDate) + $i * 24 * 60 * 60) == date($dateFormat)) {
                $tpl->setVariable("GRIDHEADCURRENTDAY", "class='gridheadsel'");
            }
            $tpl->setVariable("GRIDWIDTH", round(90 / $dayCount) . "%");
            $tpl->setVariable("MULTIPLEPRINTPERDAY", $this->getLink("print_orders_day", "c_date=" . date("Y-m-d", strtotime($startDate) + $i * 24 * 60 * 60) . "&sc_id=" .
                ($scFilter != '' ? $scFilter : "0") . $addAreas));
            $tpl->setVariable("MULTIPLECELLPRINT", $this->getLink("printscticket", "c_date=" .
                date("Y-m-d", strtotime($startDate) + $i * 24 * 60 * 60) . "&sc_id=" . ($scFilter !=
                '' ? $scFilter : "0") . $addAreas));
            $tpl->setVariable("GRIDHEADDATETITLE", rudate("l", date($interfaceDateFormat, strtotime($startDate) + $i * 24 * 60 * 60)) . " " . rudate($interfaceDateFormat, date($interfaceDateFormat, strtotime($startDate) + $i * 24 * 60 * 60)));
            $tpl->setVariable("GRIDHEADDATE", rudate($interfaceDateFormat, date($interfaceDateFormat, strtotime($startDate) + $i * 24 * 60 * 60)) . "<br />" . rudate("l", date($interfaceDateFormat, strtotime($startDate) + $i * 24 * 60 * 60)));
            $tpl->parse("dateGridHead");
        }

        $empl = new adm_empl_plugin();
        $userFilterList = $empl->getInvEmpl($scFilter);
        if ($userFilterList) {
            foreach ($userFilterList as $uParam) {
                if ($techwtf == "1") {
                    $sql = "SELECT COUNT(`user_id`) FROM `link_empl_tf` WHERE `sched_date` >= " . $DB->
                        F(substr($startDate, 6, 4) . "-" . substr($startDate, 3, 2) . "-" . substr($startDate, 0, 2)) . " AND `sched_date` <= " . $DB->F(substr($endDate, 6, 4) . "-" . substr
                            ($endDate, 3, 2) . "-" . substr($endDate, 0, 2)) . " AND `user_id`=" . $DB->F($uParam['id']) .
                        ";";
                    //die($sql);
                    if (!$DB->getField($sql)) {
                        continue;
                    }
                }
                $tpl->setCurrentBlock("userList");
                $tpl->setVariable("FIO", $uParam['fio']);
                $useNBN = $DB->getField("SELECT `usenbnintegr` FROM `users` WHERE `id`=" . $DB->
                    F($uParam['id']) . ";");

                $limits = $DB->getRow("SELECT * FROM `list_empl_money_limits` WHERE `tech_id`=" . $DB->F($uParam['id']) . ";", true);
                if ($limits) {
                    $lower = $limits['limit1'];
                    $upper = $limits['limit2'];
                } else {
                    $lower = 10000;
                    $upper = 15000;
                }
                $debt = $DB->getField("SELECT `ammount` FROM `skp_tech_debts` WHERE `tech_id`=" . $DB->F($uParam['id']) . ";");
                if ($debt > 0) {
                    if ($debt >= $upper) {
                        $tpl->setVariable("DEBTLIMIT", "background: #ffea35 !important;");
                        $tpl->setVariable("DEBTLIMITCOLOR", "#ffea35");
                        $tpl->setVariable("DEBTTITLE", "Превышение лимита ДС " . $debt);
                    }
                }
                if ($useNBN)
                    $tpl->setVariable("ISNBN", "&nbsp;&nbsp;<span class=\"odsval\" title=\"Включена передача расписания в НБН\" style=\"font-size: 9pt;\">" .
                        $uParam['id'] . "</span>");
                $tpl->setVariable("USERID", $uParam['id']);
                if ($USER->checkAccess("callout")) {
                    $tpl->setCurrentBlock("showcallto");
                    $phnes = explode(";", $uParam['phones']);
                    $parsed = false;
                    if (count($phnes) > 1) {
                        if ($phnes[0] != "" && $phnes[0] != "70000000000" && $USER->getIntPhone()) {
                            $parsed = true;
                            $tpl->setCurrentBlock("calltotechfio");
                            $tpl->setVariable("CALLUSERFROM", $USER->getIntPhone());
                            $tpl->setVariable("USERPHONENUM", $phnes[0]);
                            $tpl->parse("calltotechfio");
                        }
                        if ($phnes[1] != "" && $phnes[1] != "70000000000" && $USER->getIntPhone()) {
                            $parsed = true;
                            $tpl->setCurrentBlock("calltotechfio");
                            $tpl->setVariable("CALLUSERFROM", $USER->getIntPhone());
                            $tpl->setVariable("USERPHONENUM", $phnes[1]);
                            $tpl->parse("calltotechfio");
                        }
                    } else {
                        if ($uParam['phones'] != "" && $uParam['phones'] != "70000000000") {
                            $parsed = true;
                            $tpl->setCurrentBlock("calltotechfio");
                            $tpl->setVariable("CALLUSERFROM", $USER->getIntPhone());
                            $tpl->setVariable("USERPHONENUM", $uParam['phones']);
                            $tpl->parse("calltotechfio");
                        }
                    }
                    if (!$parsed) {
                        $tpl->setVariable("HIDEIFNOPHONE", "style='display:none;'");
                    }
                    $tpl->parse("showcallto");
                }
                $tpl->setVariable("USERPHONE", $uParam['phones'] ? $uParam['phones'] : "&mdash;");
                for ($i = 0; $i < $dayCount; $i++) {


                    $sql = "SELECT * FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($uParam['id']) .
                        " AND `sched_date`=" . $DB->F(date("Y-m-d", strtotime($startDate) + $i * 24 * 60 *
                            60)) . ";";
                    $r = $DB->getRow($sql, true);
                    if (date("Y-m-d", strtotime($startDate) + $i * 24 * 60 * 60) < date("Y-m-d")) {
                        $append = "busy";
                        $appendTD = "busytf";
                    } else {
                        $append = "free cell";
                        $appendTD = "";
                    }
                    if ($r) {
                        $wt = ($r['endtime'] - $r['starttime']) / 30;
                        $timeStart = (strlen(floor($r['starttime'] / 60)) == 1 ? "0" . floor($r['starttime'] / 60) : floor($r['starttime'] /
                                60)) . ":" . (strlen($r['starttime'] % 60) == 1 ? "0" . $r['starttime'] % 60 : $r['starttime'] % 60);
                        $timeEnd = (strlen(floor($r['endtime'] / 60)) == 1 ? "0" . floor($r['endtime'] / 60) : floor($r['endtime'] /
                                60)) . ":" . (strlen($r['endtime'] % 60) == 1 ? "0" . $r['endtime'] % 60 : $r['endtime'] % 60);
                        $res = "<table cellpadding='0' cellspacing='0' class='sched seltf'><tr>";
                        $sql = "SELECT 
                                    tsk.id, 
                                    gfx.startTime, 
                                    tsk.plugin_uid, 
                                    ttype.duration, 
                                    status.color, 
                                    addr.name, 
                                    addr.full_address,
                                    addr.althouse, 
                                    ticket.kv, 
                                    addr.ods_id, 
                                    addr.id AS addr_id 
                                FROM `gfx` AS gfx
                                        LEFT JOIN `tasks` AS tsk ON tsk.id=gfx.task_id
                                        LEFT JOIN `tickets` AS ticket ON ticket.task_id=gfx.task_id
                                        LEFT JOIN `task_types` AS ttype ON ttype.id=ticket.task_type
                                        LEFT JOIN `task_status` AS status ON status.id=tsk.status_id
                                        LEFT JOIN `list_addr` AS addr ON addr.id=ticket.dom_id
                                        WHERE 
                                        gfx.empl_id=" . $DB->F($uParam['id']) . " AND gfx.c_date=" . $DB->F(date("Y-m-d", strtotime($startDate) + $i * 24 * 60 *
                                60)) . ";";
                        $DB->query($sql);
                        $ticketsOnSched = [];
                        if ($DB->num_rows()) {
                            while (list($id, $startTime, $tType, $dur, $color, $dom, $kladr, $altdom, $kv, $ods, $addr) = $DB->fetch()) {
                                $ticketsOnSched[$startTime] = [
                                    $id,
                                    $tType,
                                    $dur,
                                    $color,
                                    $dom,
                                    $kladr,
                                    $altdom,
                                    $kv,
                                    $startTime,
                                    $ods,
                                    $addr];
                            }
                            $DB->free();
                        }
                        $draw = false;
                        $width = 0;

                        //echo date("Y-m-d", strtotime($startDate) + $i*24*60*60) ." ". date("Y-m-d")."<br />";
                        for ($k = $r['starttime'] / 30; $k < $r['endtime'] / 30; $k += 1) {
                            if (array_key_exists($k * 30, $ticketsOnSched)) {

                                $draw = $k * 30;
                                $width = $ticketsOnSched[$draw][2] / 30 * (100 / $wt);
                                $type = $ticketsOnSched[$draw][1] == "connections" ? "Подключение" : ($ticketsOnSched[$draw][1] ==
                                "services" ? "Сервисное обслуживание" : "ТТ (Абонентские аварии)");
                                $duration = $ticketsOnSched[$draw][2];
                                $ticket_link = $ticketsOnSched[$draw][1] . "/viewticket?task_id=" . $ticketsOnSched[$draw][0];
                                if ($ticketsOnSched[$draw][1] == "accidents")
                                    $tdstr = "А";
                                else
                                    $tdstr = mb_substr($type, 0, 1);

                                $taskList[] = $ticketsOnSched[$draw][0];
                                $res .= "<td onclick='goTo(\"$ticket_link\"); return false;' ods=\"" . $ticketsOnSched[$draw][9] . "\" kladr=\"" . $ticketsOnSched[$draw][10] .
                                    "\" duration='" . ($duration / 30) . "' align='center' title='Заявка № " . $ticketsOnSched[$draw][0] .
                                    " &#xA;Тип: " . $type . " &#xA;Длительность: " . $duration .
                                    " мин. &#xA;Адрес: " . $ticketsOnSched[$draw][5] . ", кв. " . ($ticketsOnSched[$draw][7] ?
                                        $ticketsOnSched[$draw][7] : "-") . " ' width='" . (floor($width)) .
                                    "%' style='overflow:hidden;background-color: " . $ticketsOnSched[$draw][3] .
                                    "; border: 1px solid " . $ticketsOnSched[$draw][3] . ";' class='tf busy " . $ticketsOnSched[$draw][1] .
                                    " start end'><strong>" . $tdstr . "</strong></td>";
                            }
                            if (($k) * 30 >= ($draw + $ticketsOnSched[$draw][2])) {
                                $draw = false;
                            }


                            if (!$draw) {
                                $res .= "<td width='" . floor(100 / $wt) . "%' style='cursor:default;' class='tf $append'></td>";
                            } else {
                                $width += 100 / $wt;
                            }
                        }

                        $res .= "<input type='hidden' name='rudate' value='" . rudate("d M Y", strtotime
                                ($startDate) + $i * 24 * 60 * 60) . "' />
                                         <input type='hidden' name='date' value='" .
                            date("Y-m-d", strtotime($startDate) + $i * 24 * 60 * 60) . "' />
                                         <input type='hidden' name='user_id' value='" .
                            $uParam['id'] . "' />
                                         <input type='hidden' name='tf_start' value='" .
                            $r['starttime'] . "' />
                                         <input type='hidden' name='tf_end' value='" .
                            $r['endtime'] . "' />
                                         </tr></table>";
                        $res .= "<table class='schedtf' cellpadding='0' width='100%' cellspacing='0' border='0'><tr><td align='left'><small>$timeStart</small></td><td align='right'><small>$timeEnd</small></td></tr></table>";
                        if ($i > 1) {
                            $tpl->setCurrentBlock("sc" . $i);
                        }

                        if (date($dateFormat, strtotime($startDate) + $i * 24 * 60 * 60) == date($dateFormat)) {
                            $tpl->setVariable("SELCURRENTDATE" . $i, "class=' $appendTD '");
                        } else {
                            $tpl->setVariable("SELCURRENTDATE" . $i, "class=' $appendTD  '");
                        }
                        $tpl->setVariable("SCHED" . $i, $res);
                        if ($i > 1) {
                            $tpl->parse("sc" . $i);
                        }
                    } else {
                        if ($i > 1) {
                            $tpl->setCurrentBlock("sc" . $i);
                        }
                        $tpl->setVariable("SCHED" . $i, "");
                        if (date($dateFormat, strtotime($startDate) + $i * 24 * 60 * 60) == date($dateFormat)) {
                            $tpl->setVariable("SELCURRENTDATE" . $i, "class='seltfi_empty $appendTD '");
                        } else {
                            $tpl->setVariable("SELCURRENTDATE" . $i, "class='seltfi_empty $appendTD'");
                        }

                        if ($i > 1) {
                            $tpl->parse("sc" . $i);
                        }
                    }
                }
                $tpl->parse("userList");
            }
            $ticketTypeArray = ["connections" => "Подключение", "services" => "СКП", "accidents" => "ТТ"];
            if (count($taskList)) {
                $sql = 'SELECT t.*, list_addr.full_address, list_addr.is_not_recognized, tickets.kv
                        FROM tasks AS t 
                          LEFT JOIN gfx AS g ON g.task_id = t.id
                          LEFT JOIN tickets ON tickets.task_id = t.id
                          LEFT JOIN list_addr ON list_addr.id = tickets.dom_id
                        WHERE g.startTime AND t.id IN (' . implode(",", $taskList) . ')
                        GROUP BY t.id
                        ORDER BY g.startTime ASC';

                $DB->query($sql);
                if ($DB->num_rows()) {
                    $tpl->setCurrentBlock("ticklist");
                    if ($USER->isTech($USER->getId())) {
                        $tpl->touchBlock("istechcallth");
                    }
                    while ($r = $DB->fetch(true)) {
                        $tpl->setCurrentBlock("tlitem");
                        $tpl->setVariable("TID", $r["id"]);
                        $tpl->setVariable("TTYPE", $ticketTypeArray[$r["plugin_uid"]]);
                        $tpl->setVariable("TLINK", link2($r["plugin_uid"] . "/viewticket?task_id=" . $r["id"], false));
                        switch ($r["plugin_uid"]) {
                            case "services":
                                $t = new ServiceTicket($r["id"]);
                                break;

                            case "connections":
                                $t = new ConnectionTicket($r["id"]);
                                break;

                            case "accidents":
                                $t = new AccidentTicket($r["id"]);
                                break;

                            default:
                                UIError("Не определен тип заявки в графике техника!");
                                break;
                        }

                        //JN Address by one row
                        if (!empty($r['full_address'])) {
                            if ($r['is_not_recognized']) {
                                $r['full_address'] = $r['full_address'] . ' <sup>*</sup>';
                            }

                            $tpl->setVariable("TADDR", $r['full_address'] . ($r['kv'] ? ', кв/оф ' . $r['kv'] : ''));
                        } else {
                            $tpl->setVariable("TADDR", "<strong style='color: #FF0000'>Адрес не задан</strong>");
                        }

                        $tpl->setVariable("TCONTR", $t->getContrName());


                        if ($t !== false)
                            $phnes = $t->getPhones();
                        $clientPhone = ($phnes[0] != "" && $phnes[0] != "70000000000") ? $phnes[0] : (($phnes[1] != "" && $phnes[1] != "70000000000") ? $phnes[1] : false);
                        $tpl->setCurrentBlock("istechcallbody");
                        if ($clientPhone) {
                            $tpl->setVariable("CLIENTPHONENUMBER", $clientPhone);
                            $tpl->setVariable("TECHVIRTEXT", $USER->getIntPhone());
                            $tpl->setVariable("TC_TASK_ID", $id);
                        } else {
                            $tpl->setVariable("NOCALLSINTASK", "hidden");
                        }
                        $tpl->parse("istechcallbody");


                        $dt = $DB->getField("SELECT `startTime` FROM `gfx` WHERE `task_id`=" . $DB->F($r["id"]) . ";");
                        if (!$dt)
                            $dt = "&mdash;";
                        else {
                            $h = $dt / 60;
                            $m = $dt % 60;
                            $dt = date("H:i", strtotime($h . ":" . $m));
                        }
                        $tpl->setVariable("TDT", $dt);
                        $tpl->setVariable("HREF_ORDER", $this->getLinkStatic($r["plugin_uid"] . "/vieworderdoc?id=" . $r["id"]));
                        $tpl->parse("tlitem");
                    }
                    $tpl->parse("ticklist");
                } else {
                    $tpl->touchBlock("notickets");
                }
                $DB->free();
            } else {
                $tpl->touchBlock("notickets");
            }
        }
    }

    private function viewScheduleOthers(HTML_Template_IT $tpl)
    {
        global $DB, $USER;
        $user_id = $USER->getId();
        $dateFormat = 'd-m-Y';
        $interfaceDateFormat = 'd M Y';
        $cDate = date('D');
        $isTech = User::isTech($USER->getId());
        $isChiefTech = User::isChiefTech($user_id);
        $sc_id = false;

        $scFilter = $sc_id ? $sc_id : (isset($_POST["sc"]) ? $_POST["sc"] : @$_REQUEST["scFilter"]);

        $techwtf = $_REQUEST["techwtf"] == "1" ? "1" : (isset($_POST["tech-w-tf"]) ? "1" : "0");
        if (isset($_POST['startDate']) && !isset($_POST['tech-w-tf'])) {
            $techwtf = "0";
        }
        $dayCount = isset($_POST["dc"]) ? intval($_POST["dc"]) : (isset($_REQUEST["dc"]) ? intval($_REQUEST["dc"]) : getcfg('dc'));

        if (preg_match("/iPhone|Android|iPad|iPod|webOS/", $_SERVER['HTTP_USER_AGENT'])) {
            $dayCount = 1;
        }

        if (class_exists("adm_sc_plugin", true) && method_exists("adm_sc_plugin", "getFilterList")) {
            $empl = new adm_sc_plugin();
            $scFilterList = $empl->getFilterList($scFilter);

            if ($scFilterList) {
                $tpl->setCurrentBlock("sc-list");
                $tpl->setVariable("SCLIST", $scFilterList);
                $tpl->parse("sc-list");
                $tpl->setCurrentBlock("sc-list1");
                $scFilterList = $empl->getFilterList($scFilter, "addscsel");
                $tpl->setVariable("SCLIST1", $scFilterList);
                $tpl->parse("sc-list1");
            } else {
                UIError("Отсутствует список Сервисных Центров!", "Ошибка", true, "Создать СЦ", "/adm_sc/edit");
            }
        } else {
            UIError("Не найден модуль Сервисные Центры!");
        }

        if (preg_match("/iPhone|Android|iPad|iPod|webOS/", $_SERVER['HTTP_USER_AGENT'])) {
            $pList = "<select disabled='disabled' name='dc'>";
            for ($i = 1; $i <= 1; $i += 1) {
                if ($dayCount == $i)
                    $sel = "selected='selected'";
                else
                    $sel = "";
                $pList .= "<option $sel value='" . $i . "'>$i дн. </option>";
            }
        } else {
            $pList = "<select name='dc'>";
            for ($i = 1; $i <= 7; $i += 1) {
                $sel = $dayCount == $i ? "selected='selected'" : "";
                $pList .= "<option $sel value='" . $i . "'>$i дн. </option>";
            }
        }
        $pList .= "</select>";

        $wtype = adm_ticktypes_plugin::getOptList($_REQUEST["wtype_id"]);
        if (!$wtype) {
            UIError("Отсутствует справочник видов работ!");
        }

        $tpl->setVariable("WTYPELIST", $wtype);
        $tpl->setVariable("PERIODLIST", $pList);
        $tpl->setVariable("TECH_W_TF_CHECKED", $techwtf == "1" ? "checked=\"checked\"" : "");

        $startDate =
            $dayCount != 7 ?
                ($_REQUEST["wDate"] ? $_REQUEST["wDate"] : date($dateFormat))
                :
                (
                $_REQUEST["wDate"] ?
                    (
                    date('w', strtotime($_REQUEST["wDate"])) == 1 ?
                        $_REQUEST["wDate"]
                        :
                        (
                        date('w', strtotime($_REQUEST["wDate"])) ?
                            date($dateFormat, strtotime($_REQUEST["wDate"]) - (date('w', strtotime($_REQUEST["wDate"])) - 1) * 24 * 60 * 60)
                            :
                            date($dateFormat, strtotime($_REQUEST["wDate"]) - 6 * 24 * 60 * 60)
                        )
                    )
                    :
                    ($cDate == "Sun" ? date($dateFormat, strtotime('Mon last week')) : date($dateFormat, strtotime('Mon this week')))
                );
        $endDate = date($dateFormat, strtotime($startDate) + ($dayCount - 1) * 86400);

        $tpl->setVariable("INT_START", rudate($interfaceDateFormat, $startDate));
        $tpl->setVariable("CURRENTSTARTDATE", $startDate);
        $tpl->setVariable("INT_END", rudate($interfaceDateFormat, $endDate));
        $tpl->setVariable("NEXTWEEKLINK", $this->getUID() . "/view_schedule/?wDate=" . date($dateFormat, strtotime($endDate) + 86400) . "&dc=$dayCount" . ($scFilter ?
                "&scFilter=" . $scFilter : "") . "&techwtf=" . $techwtf . $addAreas);
        $tpl->setVariable("PREVWEEKLINK", $this->getUID() . "/view_schedule/?wDate=" .
            date($dateFormat, strtotime($startDate) - $dayCount * 86400) . "&dc=$dayCount" .
            ($scFilter ? "&scFilter=" . $scFilter : "") . "&techwtf=" . $techwtf . $addAreas);

        $tpl->setVariable("CURRENTWEEKLINK", $this->getUID() . "/view_schedule" .
            ($scFilter ? "?scFilter=" . $scFilter . "&dc=$dayCount" : "?dc=$dayCount") . "&techwtf=" . $techwtf . $addAreas);

        if (!$_REQUEST["wDate"]
            || (strtotime(date($dateFormat)) >= strtotime($startDate)
                && strtotime(date($dateFormat)) <= strtotime($endDate))
        ) {
            $tpl->touchBlock("cweekword");
        }

        $k = 0;
        for ($i = 0; $i < $dayCount; $i++) {
            $tpl->setCurrentBlock("dateGridHead");
            if (date($dateFormat, strtotime($startDate) + $i * 86400) == date($dateFormat)) {
                $tpl->setVariable("GRIDHEADCURRENTDAY", "class='gridheadsel'");
            }
            $tpl->setVariable("GRIDWIDTH", round(90 / $dayCount) . "%");
            $tpl->setVariable("MULTIPLEPRINTPERDAY", $this->getLink("print_orders_day", "c_date=" . date("Y-m-d", strtotime($startDate) + $i * 86400) . "&sc_id=" .
                ($scFilter != '' ? $scFilter : "0") . $addAreas));
            $tpl->setVariable("MULTIPLECELLPRINT", $this->getLink("printscticket", "c_date=" .
                date("Y-m-d", strtotime($startDate) + $i * 86400) . "&sc_id=" . ($scFilter !=
                '' ? $scFilter : "0") . $addAreas));
            $tpl->setVariable("GRIDHEADDATETITLE", rudate("l", date($interfaceDateFormat, strtotime($startDate) + $i * 86400)) . " " . rudate($interfaceDateFormat, date($interfaceDateFormat, strtotime($startDate) + $i * 86400)));
            $tpl->setVariable("GRIDHEADDATE", rudate($interfaceDateFormat, date($interfaceDateFormat, strtotime($startDate) + $i * 86400)) . "<br />" . rudate("l", date($interfaceDateFormat, strtotime($startDate) + $i * 86400)));
            $tpl->parse("dateGridHead");
        }
        $sql = "SELECT `id`, `contr_title` FROM `list_contr` WHERE contr_type OR contr_type2;";
        $contrList = $DB->getCell2($sql);
        if ($contrList) {
            $tpl->setVariable("FILTER_CNT_OPTIONS", array2options($contrList));
        }
        $sql = "SELECT `id`, `title` FROM `task_types` WHERE `plugin_uid`='connections' OR `plugin_uid`='services';";
        $wtylelist = $DB->getCell2($sql);
        if ($wtylelist) {
            $tpl->setVariable("FILTER_TYPE_OPTIONS", array2options($wtylelist));
        }
        if ($scFilter != "" && $scFilter != "0") {
            $area_add = " WHERE `id` IN (SELECT `area_id` FROM `link_sc_area` WHERE `sc_id`=" .
                $DB->F($scFilter) . ")";
        }

        $empl = new adm_empl_plugin();

        if ($_REQUEST["wtype_id"]) {
            $userFilterList = $empl->getInvEmpl($scFilter, $_REQUEST["wtype_id"]) ?: [];
        } else {
            $userFilterList = $empl->getInvEmpl($scFilter, false) ?: [];
        }

        /* @var $startDt DateTime */
        $startDt = DateTime::createFromFormat('d-m-Y', $startDate)->setTime(0, 0, 0);
        /* @var $endDt DateTime */
        $endDt = DateTime::createFromFormat('d-m-Y', $endDate)->setTime(23, 59, 59);

        $ids = array_map(function($row) { return $row['id']; }, $userFilterList);
        $qb = $this->getEm()
            ->createQueryBuilder()
            ->select('user.id user_id, frame.id, frame.schedDate, frame.startTime starttime, frame.endTime endtime')
            ->from(ExecutorTimeframe::class, 'frame')
            ->join('frame.executor', 'user')
            ->where('user.id IN (:user_ids)')
            ->andWhere(':start <= frame.timeEnd AND :end >= frame.timeStart')
            ->setParameter('user_ids', $ids)
            ->setParameter('start', $startDt)
            ->setParameter('end', $endDt)
        ;

        $timeframes = $qb->getQuery()->getArrayResult();

        foreach ($userFilterList as $uParam) {
            $schedule = array_filter($timeframes, function ($timeframe) use($uParam) {
                return $timeframe['user_id'] == $uParam['id'];
            });

            if ($techwtf == 1 && empty($schedule)) {
                continue;
            }

            $tpl->setCurrentBlock("userList");
            $tpl->setVariable("FIO", $uParam['fio']);

            $limits = $this->getMoneyLimit($uParam['id']);
            if ($limits) {
                $upper = $limits['limit2'];
            } else {
                $upper = 15000;
            }

            $gfxData = $this->getGfxData($uParam['id'], $startDt, $endDt);

            $userCellTitle = [];
            $debt = $this->getDebt($uParam['id']);

            $bgStyle = "";
            if (!($uParam['is_virtual']) && ($debt && $debt['amount'] > $upper)) {
                $tpl->setVariable("DEBTLIMITCOLOR", "#ffea35");
                $bgStyle = "background: #ffea35 !important;";
                $userCellTitle[] = "Превышение лимита ДС {$debt['amount']}";
            }

            $stat = $this->getStatistic($uParam['id']);
            if (!($uParam['is_virtual']) && (!$stat || $stat->getWorkDayStartedAt() === null)) {
                $bgStyle = "background: #ffea35 !important;";
                //$tpl->setVariable("DEBTLIMIT", "background: #f7f21a !important;");
                $tpl->setVariable("DEBTLIMITCOLOR", "#f7f21a");
                $userCellTitle[] = "Техник не начал рабочий день";

                $compareDt = new DateTime();
                $compareDt->modify('+1 hour');
                $nowGfx = array_filter($gfxData, function ($row) use ($compareDt) {
                    $dt = new DateTime($row['c_date']);
                    $dt->setTime(0, $row['startTime'], 0);

                    return $dt->format('Ymd') === $compareDt->format('Ymd') && $dt < $compareDt;
                });

                if (count($nowGfx)) {
                    $bgStyle = 'background: url(/templates/twozero/images/red_blinking_gif.gif);';
                    $userCellTitle[] = "Ближайшая заявка менее, чем через час";
                }

            }

            $tpl->setVariable("DEBTLIMIT", $bgStyle);
            if (!empty($userCellTitle)) {
                $tpl->setVariable("DEBTTITLE", implode('<br>', $userCellTitle));
            }

            $tpl->setVariable("USERID", $uParam['id']);
            if ($USER->checkAccess("callout")) {
                $tpl->setCurrentBlock("showcallto");
                $phnes = explode(";", $uParam['phones']);
                $parsed = false;
                if (count($phnes) > 1) {
                    if ($phnes[0] != "" && $phnes[0] != "70000000000" && $USER->getIntPhone()) {
                        $parsed = true;
                        $tpl->setCurrentBlock("calltotechfio");
                        $tpl->setVariable("CALLUSERFROM", $USER->getIntPhone());
                        $tpl->setVariable("USERPHONENUM", $phnes[0]);
                        $tpl->parse("calltotechfio");
                    }
                    if ($phnes[1] != "" && $phnes[1] != "70000000000" && $USER->getIntPhone()) {
                        $parsed = true;
                        $tpl->setCurrentBlock("calltotechfio");
                        $tpl->setVariable("CALLUSERFROM", $USER->getIntPhone());
                        $tpl->setVariable("USERPHONENUM", $phnes[1]);
                        $tpl->parse("calltotechfio");
                    }
                } else {
                    if ($uParam['phones'] != "" && $uParam['phones'] != "70000000000") {
                        $parsed = true;
                        $tpl->setCurrentBlock("calltotechfio");
                        $tpl->setVariable("CALLUSERFROM", $USER->getIntPhone());
                        $tpl->setVariable("USERPHONENUM", $uParam['phones']);
                        $tpl->parse("calltotechfio");
                    }
                }
                if (!$parsed) {
                    $tpl->setVariable("HIDEIFNOPHONE", "style='display:none;'");
                }
                $tpl->parse("showcallto");
            }
            $tpl->setVariable("USERPHONE", $uParam['phones'] ? $uParam['phones'] : "&mdash;");

            $curDt = clone $startDt;
            $tmp = [];
            foreach($schedule as $item) {
                $item['sched_date'] = $item['schedDate']->format('Y-m-d');
                unset($item['schedDate']);
                $tmp[ $item['sched_date'] ] = $item;
            }
            $schedule = $tmp;
            for ($i = 0; $i < $dayCount; $i++) {
                if (date("Y-m-d", strtotime($startDate) + $i * 86400) < date("Y-m-d")) {
                    $append = "busy";
                    $appendTD = "busytf";
                } else {
                    $append = "free cell";
                    $appendTD = "";
                }

                if (isset($schedule[$curDt->format('Y-m-d')])) {
                    $r = $schedule[$curDt->format('Y-m-d')];
                    $wt = ($r['endtime'] - $r['starttime']) / 30;
                    $timeStart = (strlen(floor($r['starttime'] / 60)) == 1 ? "0" . floor($r['starttime'] / 60) : floor($r['starttime'] /
                            60)) . ":" . (strlen($r['starttime'] % 60) == 1 ? "0" . $r['starttime'] % 60 : $r['starttime'] % 60);
                    $timeEnd = (strlen(floor($r['endtime'] / 60)) == 1 ? "0" . floor($r['endtime'] / 60) : floor($r['endtime'] /
                            60)) . ":" . (strlen($r['endtime'] % 60) == 1 ? "0" . $r['endtime'] % 60 : $r['endtime'] % 60);
                    $res = "<table cellpadding='0' cellspacing='0' class='sched seltf'><tr>";

                    $filtered = array_filter($gfxData, function ($row) use ($curDt) {
                        return $row['c_date'] === $curDt->format('Y-m-d');
                    });

                    $ticketsOnSched = [];
                    foreach ($filtered as $row) {
                        $ticketsOnSched[$row['startTime']] = $row;
                    }

                    $draw = false;
                    $width = 0;

                    for ($k = $r['starttime'] / 30; $k < $r['endtime'] / 30; $k += 1) {
                        if (array_key_exists($k * 30, $ticketsOnSched)) {

                            $draw = $k * 30;
                            $width = $ticketsOnSched[$draw]['duration'] / 30 * (100 / $wt);
                            $type = $ticketsOnSched[$draw]['plugin_uid'] == "connections" ? "Подключение" : ($ticketsOnSched[$draw]['plugin_uid'] ==
                            "services" ? "Сервисное обслуживание" : "ТТ (Абонентские аварии)");
                            $duration = $ticketsOnSched[$draw]['duration'];

                            if ($ticketsOnSched[$draw]['plugin_uid'] == "accidents") {
                                $tdstr = "А";
                            } else {
                                $tdstr = mb_substr($type, 0, 1);
                            }

                            $res .= "<td ods=\"" . $ticketsOnSched[$draw][9] . "\" addr='" .$ticketsOnSched[$draw]['full_address'] ."' duration='" . ($duration / 30) . "' align='center' title='Заявка № " . $ticketsOnSched[$draw]['id'] .
                                " &#xA;Тип: " . $type . " &#xA;Длительность: " . $duration .
                                " мин. &#xA;Адрес: " . $ticketsOnSched[$draw]['full_address'] . ", кв. " . ($ticketsOnSched[$draw]['kv'] ?
                                    $ticketsOnSched[$draw]['kv'] : "-") . " ' width='" . (floor($width)) .
                                "%' style='overflow:hidden;background-color: " . $ticketsOnSched[$draw]['color'] .
                                "; border: 1px solid " . $ticketsOnSched[$draw]['color'] . ";' class='tf busy " . $ticketsOnSched[$draw]['plugin_uid'] .
                                " start end'><strong>" . $tdstr . "</strong></td>";
                        }
                        if ($k * 30 >= ($draw + @$ticketsOnSched[(int)$draw]['duration'])) {
                            $draw = false;
                        }

                        if (!$draw) {
                            $res .= "<td width='" . floor(100 / $wt) . "%' class='tf $append'></td>";
                        } else {
                            $width += 100 / $wt;
                        }
                    }

                    $res .= "<input type='hidden' name='rudate' value='" . rudate("d M Y", strtotime
                            ($startDate) + $i * 86400) . "' />
                                     <input type='hidden' name='date' value='" .
                        date("Y-m-d", strtotime($startDate) + $i * 86400) . "' />
                                     <input type='hidden' name='user_id' value='" .
                        $uParam['id'] . "' />
                                     <input type='hidden' name='tf_start' value='" .
                        $r['starttime'] . "' />
                                     <input type='hidden' name='tf_end' value='" .
                        $r['endtime'] . "' />
                                     </tr></table>";
                    $res .= "<table class='schedtf' cellpadding='0' width='100%' cellspacing='0' border='0'><tr><td align='left'><small>$timeStart</small></td><td align='right'><small>$timeEnd</small></td></tr></table>";
                    if ($i > 1) {
                        $tpl->setCurrentBlock("sc" . $i);
                    }

                    if (date($dateFormat, strtotime($startDate) + $i * 86400) == date($dateFormat)) {
                        $tpl->setVariable("SELCURRENTDATE" . $i, "class='seltf seltfi $appendTD '");
                    } else {
                        $tpl->setVariable("SELCURRENTDATE" . $i, "class='seltf seltfi $appendTD  '");
                    }

                    $tpl->setVariable("SCHED" . $i, $res);
                    //$tpl->setVariable("SELCURRENTDATE".$i, "class='seltf seltfi $appendTD'");
                    if ($i > 1) {
                        $tpl->parse("sc" . $i);
                    }
                } else {
                    if ($i > 1) {
                        $tpl->setCurrentBlock("sc" . $i);
                    }
                    $tpl->setVariable("SCHED" . $i, "");
                    if (date($dateFormat, strtotime($startDate) + $i * 86400) == date($dateFormat)) {
                        $tpl->setVariable("SELCURRENTDATE" . $i, "class='seltfi_empty $appendTD '");
                    } else {
                        $tpl->setVariable("SELCURRENTDATE" . $i, "class='seltfi_empty $appendTD'");
                    }

                    if ($i > 1) {
                        $tpl->parse("sc" . $i);
                    }
                }
                $curDt->modify('+1 day');
            }
            $tpl->parse("userList");
        }

        $sql_filter = '';
        $sc_target = User::isChiefTech($USER->getId()) or false;
        if (User::isTech($USER->getId()) && !$sc_target) {
            $tpl->setVariable("IS_TECH_NO_ACCESS", "hidden");
        } else {
            $connnewstatus = adm_statuses_plugin::getStatusByTag("new", "connections");
            $skpnewstatus = adm_statuses_plugin::getStatusByTag("new", "services");
            $ttnewstatus = adm_statuses_plugin::getStatusByTag("new", "accidents");

            if ($sc_target !== false) {
                /* $sql_filter = " AND tick.sc_id=" . $DB->F($sc_target) . ""; */
            } else {
                if ($scFilter != "0" and $scFilter != "" and $scFilter !== false) {
                    $sql_filter = " AND tick.sc_id=" . $DB->F($scFilter);
                }
            }

            $hasTick = false;
            $tpl->setVariable("TICKALIGN", "top");

            $statuses = [
                'accidents' => [
                    'id' => $ttnewstatus["id"],
                    'color' => $ttnewstatus["color"],
                    'title' => 'А'
                ],
                'connections' => [
                    'id' => $connnewstatus["id"],
                    'color' => $connnewstatus["color"],
                    'title' => 'П'
                ],
                'services' => [
                    'id' => $skpnewstatus["id"],
                    'color' => $skpnewstatus["color"],
                    'title' => 'С'
                ],

            ];

            if (User::isTech($USER->getId()) && !$sc_target) {
                $sql_filter .= " AND (t.id IN (SELECT `task_id` FROM `task_users` WHERE `user_id`=" .
                    $DB->F($USER->getId()) . " AND `ispoln`)) ";
                $skpnewstatus["id"] = "45";
            }

            if (User::isTech($USER->getId()) && !$sc_target) {

            } else {
                foreach($statuses as $statuse) {
                    $sql = "SELECT 
                    t.id, 
                    t.task_title, 
                    tick.*,
                    list_addr.full_address,
                    list_area.id AS area_id,
                    list_area.title AS area_title,
                    list_sc.title AS sc_title,
                    list_ods.id AS ods_id,
                    list_ods.title AS ods_tile,
                    list_contr.contr_title,
                    task_types.id AS task_type_id,
                    task_types.title AS task_type_title,
                    task_types.duration
                FROM 
                  tasks AS t 
                  LEFT JOIN 
                  tickets AS tick ON t.id=tick.task_id 
                  LEFT JOIN
                  list_addr ON tick.dom_id = list_addr.id
                  LEFT JOIN
                  list_sc ON tick.sc_id = list_sc.id
                  LEFT JOIN
                  list_ods ON list_addr.ods_id = list_ods.id
                  LEFT JOIN
                  list_contr ON tick.cnt_id = list_contr.id
                  LEFT JOIN 
                  task_types ON tick.task_type = task_types.id
                  LEFT JOIN
                  list_area ON list_addr.area_id = list_area.id
                WHERE 
                  t.status_id= " . $statuse["id"] . $sql_filter . " AND
                  (
                    tick.sc_id IS NULL OR 
                    EXISTS ( SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = tick.sc_id )
                  )
                  ;";

                    $DB->query($sql);
                    if ($DB->num_rows()) {
                        $hasTick = true;
                        $tpl->setCurrentBlock("ticklist");
                        while ($rr = $DB->fetch(true)) {
                            $tpl->setCurrentBlock("titem");
                            $tpl->setVariable("TICKTITLE", "<strong class='freeticket'>{$statuse['title']}</strong>: " . $rr["id"]);
                            $tpl->setVariable("STATUS_COLOR", $statuse["color"]);
                            $tpl->setVariable("TASK_ID", $rr["id"]);
                            $tpl->setVariable("TASK_CONTR", $rr["contr_title"]);
                            $tpl->setVariable("TASK_TYPE", $rr["task_type_title"]);

                            $tpl->setVariable("TASK_TYPE_ID", $rr["task_type_id"]);
                            $tpl->setVariable("TASK_CONTR_ID", $rr["cnt_id"]);

                            $tpl->setVariable("TASK_TITLE", $rr["task_title"]);
                            $tpl->setVariable("TASK_SC", $rr["sc_title"] ?? '-');

                            $tpl->setVariable("ODS_ID", $rr['ods_id']);
                            $tpl->setVariable("ODS_NAME", $rr['ods_tile'] ?? '-');

                            $tpl->setVariable("TASK_HREF", "accidents/viewticket/?task_id=" . $rr["id"]);
                            $tpl->setVariable("DOM_ID", $rr["dom_id"]);

                            $tpl->setVariable("TASK_ADDR", $rr['full_address'] ? sprintf('%s, эт. %s, кв. %s', $rr['full_address'], $rr["etazh"], $rr["kv"]) : '-');

                            $tpl->setVariable("TASK_AREA", $rr['area_title'] ?? '-');
                            $tpl->setVariable("TASK_AREA_ID", $rr["area_id"]);

                            $tpl->setVariable("NBNTIMESLOT", $rr["slot_begin"] . " - " . $rr["slot_end"]);

                            $tpl->setVariable("DURATION", $rr['duration']);
                            $tpl->setVariable("DURATIONDEL", $rr['duration'] / 30);

                            $tpl->parse("titem");
                        }
                        $tpl->parse("ticklist");
                    }
                    $DB->free();

                }
            }

            if (!$hasTick) {
                $tpl->touchBlock("no-tick");
                $tpl->setVariable("TICKALIGN", "middle");
            }
        }
    }

    private function getMoneyLimit($user_id)
    {
        if (empty($this->moneyLimits)) {
            $limits = $this->getEm()->createQueryBuilder()
                ->select('l, u.id')
                ->from(UserMoneyLimit::class, 'l')
                ->join('l.user', 'u')
                ->getQuery()
                ->useResultCache(true, 300)
                ->getArrayResult();

            $this->moneyLimits = [];
            foreach ($limits as $row) {
                $this->moneyLimits[$row['id']] = $row[0];
            }
        }

        return isset($this->moneyLimits[$user_id]) ? $this->moneyLimits[$user_id] : null;
    }

    private function getGfxData($userId, DateTime $startDt, DateTime $endDt)
    {
        // Field order is important! thanks for shaurman
        $sql = "SELECT 
                    tsk.id, 
                    tsk.plugin_uid, 
                    ttype.duration, 
                    status.color, 
                    addr.name, 
                    addr.full_address,
                    addr.althouse, 
                    ticket.kv, 
                    gfx.startTime, 
                    addr.ods_id, 
                    addr.id AS addr_id, 
                    gfx.c_date
                FROM `gfx` AS gfx
                LEFT JOIN `tasks` AS tsk ON tsk.id=gfx.task_id
                LEFT JOIN `tickets` AS ticket ON ticket.task_id=gfx.task_id
                LEFT JOIN `task_types` AS ttype ON ttype.id=ticket.task_type
                LEFT JOIN `task_status` AS status ON status.id=tsk.status_id
                LEFT JOIN `list_addr` AS addr ON addr.id=ticket.dom_id
                WHERE gfx.empl_id = :uid AND gfx.c_date BETWEEN :start_dt AND :end_dt";

        return $this->getConn()->executeQuery($sql, [
            'uid' => $userId,
            'start_dt' => $startDt->format('Y-m-d'),
            'end_dt' => $endDt->format('Y-m-d'),
        ])->fetchAll();
    }

    private function getDebt($user_id)
    {
        if (empty($this->debts)) {
            $debts = $this->getEm()->createQueryBuilder()
                ->select('d, t.id')
                ->from(SkpTechDebts::class, 'd')
                ->join('d.tech', 't')
                ->getQuery()
                ->useResultCache(true, 300)
                ->getArrayResult();

            $this->debts = [];
            foreach ($debts as $row) {
                $this->debts[$row['id']] = $row[0];
            }
        }

        return isset($this->debts[$user_id]) ? $this->debts[$user_id] : null;
    }

    /**
     *
     * @param integer $user_id
     * @return UserStatistic
     */
    private function getStatistic($user_id)
    {
        $date = new DateTime();
        $date->setTime(0, 0, 0);
        if (empty($this->userStat)) {
            $this->userStat = $this->getEm()->createQueryBuilder()
                ->select('s')
                ->from(UserStatistic::class, 's')
                ->join('s.user', 'u')
                ->where('s.date = :d')
                ->setParameter('d', $date)
                ->getQuery()
                ->useResultCache(true, 300)
                ->getResult();
        }

        foreach ($this->userStat as $stat) {
            if ($stat->getUser()->getId() == $user_id) {
                return $stat;
            }
        }

        return null;
    }

    function getgfxcell_ajax(Request $request)
    {
        global $DB, $USER;

        $user_id = $request->get("user_id");
        $targetdate = $request->get("targetdate");
        $task_id = $request->get("task_id");
        $tfStart = $request->get("taskstart");

        if (!$user_id || !$task_id || !$targetdate || !$tfStart) {
            $ret["error"] = "Ошибка! Недостаточно данных!";
            echo json_encode($ret);

            return false;
        }

        $task = new Task($task_id);

        /** @var OrmTask $ormTask */
        $ormTask = $this->getEm()->find(OrmTask::class, $task_id);

        if ($ormTask->getSchedule() /*&& $request->get("move") === "1"*/) {
            $sql = "SELECT * FROM `gfx` WHERE `task_id`=" . $DB->F($task_id) . ";";
            $currentTask = $DB->getRow($sql, true);

            $appendedCommentAfterRemove = wf::$taskManager->removeFromSchedule($ormTask, $request->get('reason_text', false), $this->getUserModel());

            $task->addComment("Перенос заявки. " . $appendedCommentAfterRemove,
                "Удаление из графика без возврата к статусу Новая.");
        }

        $schedule = new Schedule($ormTask);
        $schedule->setDate(new DateTime($targetdate))
            ->setStartTime($tfStart);

        /** @var OrmUser $technician */
        $technician = $this->getEm()->find(OrmUser::class, $user_id);
        $appendedCommentAfterInsert = wf::$taskManager->insertIntoSchedule($ormTask, $schedule, $technician, $this->getUserModel());

        /** @var TaskStatusRepository $taskStatusRepo */
        $taskStatusRepo = $this->getEm()->getRepository(TaskStatus::class);
        /** @var TaskStatus $scheduleState */
        $scheduleState = $taskStatusRepo->findOneByTag('grafik', $ormTask->getPluginUid());
        $schedule = $ormTask->getSchedule();
        $comment = new \models\TaskComment();
        $comment
            ->setText("Поставлена в График Работ. " . $appendedCommentAfterInsert)
            ->setTag(sprintf("Добавлено в график на %s c %s до %s, Исполнитель: %s",
                $schedule->getDate()->format('Y-m-d'),
                $schedule->getStartDateTime()->format('H:i'),
                $schedule->getEndDateTime()->format('H:i'),
                $schedule->getTechnician()->getFio()
            ))
            ->setDatetime(new DateTime())
            ->setStatus($scheduleState);

        wf::$taskManager->addComment($ormTask, $comment);

        $task->toMail();

        $res = "<table cellpadding='0' cellspacing='0' class='sched seltf'><tr>";
        $sql = "SELECT * FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($currentTask["empl_id"]) .
            " AND DATE_FORMAT(`sched_date`, '%Y-%m-%d')=" . $DB->F(date("Y-m-d", strtotime($currentTask["c_date"]))) .
            ";";
        //$ret["tpl"] = $sql;
        $r = $DB->getRow($sql, true);
        if (count($r)) {
            $wt = ($r['endtime'] - $r['starttime']) / 30;
            $timeStart = (strlen(floor($r['starttime'] / 60)) == 1 ? "0" . floor($r['starttime'] / 60) : floor($r['starttime'] /
                    60)) . ":" . (strlen($r['starttime'] % 60) == 1 ? "0" . $r['starttime'] % 60 : $r['starttime'] % 60);
            $timeEnd = (strlen(floor($r['endtime'] / 60)) == 1 ? "0" . floor($r['endtime'] / 60) : floor($r['endtime'] /
                    60)) . ":" . (strlen($r['endtime'] % 60) == 1 ? "0" . $r['endtime'] % 60 : $r['endtime'] % 60);

            $sql = "SELECT tsk.id, gfx.startTime, tsk.plugin_uid, ttype.duration, status.color, addr.name, addr.kladrcode,
                addr.althouse, ticket.kv, addr.ods_id, addr.id AS addr_id FROM `gfx` AS gfx
                LEFT JOIN `tasks` AS tsk ON tsk.id=gfx.task_id
                LEFT JOIN `tickets` AS ticket ON ticket.task_id=gfx.task_id
                LEFT JOIN `task_types` AS ttype ON ttype.id=ticket.task_type
                LEFT JOIN `task_status` AS status ON status.id=tsk.status_id
                LEFT JOIN `list_addr` AS addr ON addr.id=ticket.dom_id
                WHERE gfx.empl_id=" . $DB->F($currentTask["empl_id"]) .
                " AND CASE WHEN ifnull(ticket.sc_id,0) = 0 THEN 1
 WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = ticket.sc_id) THEN 1
 ELSE 0
END = 1"
                . " AND gfx.c_date=" . $DB->F(date("Y-m-d", strtotime($currentTask["c_date"]))) .
                ";";
            //$ret["tpl"] .= $sql;
            $DB->query($sql);
            $ticketsOnSched = [];
            if ($DB->num_rows()) {
                while (list($id, $startTime, $tType, $dur, $color, $dom, $kladr, $altdom, $kv, $ods,
                    $addr) = $DB->fetch()) {
                    $ticketsOnSched[$startTime] = [
                        $id,
                        $tType,
                        $dur,
                        $color,
                        $dom,
                        $kladr,
                        $altdom,
                        $kv,
                        $startTime,
                        $ods,
                        $addr];
                }
            }
            $DB->free();
            $draw = false;
            $width = 0;
            for ($k = $r['starttime'] / 30; $k < $r['endtime'] / 30; $k += 1) {
                if (array_key_exists($k * 30, $ticketsOnSched)) {
                    $draw = $k * 30;
                    $width = $ticketsOnSched[$draw][2] / 30 * (100 / $wt);
                    $type = $ticketsOnSched[$draw][1] == "connections" ? "Подключение" :
                        "Сервисное обслуживание";
                    $duration = $ticketsOnSched[$draw][2];
                    if ($ticketsOnSched[$draw][1] == "accidents")
                        $tdstr = "А";
                    else
                        $tdstr = mb_substr($type, 0, 1);
                    $addr = new addr_interface_plugin();
                    $street = $addr->StreetByCode($ticketsOnSched[$draw][5]);
                    $res .= "<td ods=\"" . $ticketsOnSched[$draw][9] . "\" kladr=\"" . $ticketsOnSched[$draw][10] .
                        "\"  kladr=\"" . $kladr . "\" duration='" . ($duration / 30) .
                        "' align='center' title='Заявка № " . $ticketsOnSched[$draw][0] . " &#xA;Тип: " .
                        $type . " &#xA;Длительность: " . $duration . " мин. &#xA;Адрес: " . $street .
                        " " . $ticketsOnSched[$draw][4] . ($ticketsOnSched[$draw][6] ? " " . $ticketsOnSched[$draw][6] :
                            "") . ", кв. " . ($ticketsOnSched[$draw][7] ? $ticketsOnSched[$draw][7] : "-") .
                        " ' width='" . (floor($width)) . "%' style='overflow:hidden;background-color: " .
                        $ticketsOnSched[$draw][3] . "; border: 1px solid " . $ticketsOnSched[$draw][3] .
                        ";' class='tf busy " . $ticketsOnSched[$draw][1] . " start end'><strong>" . $tdstr .
                        "</strong></td>";
                }
                if (($k) * 30 >= ($draw + $ticketsOnSched[$draw][2])) {
                    $draw = false;
                }
                if (!$draw) {
                    $res .= "<td width='" . floor(100 / $wt) . "%' class='tf free cell'></td>";
                } else {
                    $width += 100 / $wt;
                }
            }

            $res .= "<input type='hidden' name='rudate' value='" . rudate("d M Y", strtotime
                ($currentTask["c_date"])) . "' />
                <input type='hidden' name='date' value='" . date("Y-m-d",
                    strtotime($currentTask["c_date"])) . "' />
                <input type='hidden' name='user_id' value='" . $currentTask["empl_id"] .
                "' />
                <input type='hidden' name='tf_start' value='" . $r['starttime'] . "' />
                <input type='hidden' name='tf_end' value='" . $r['endtime'] . "' />
                </tr></table>";
            $res .= "<table class='schedtf' cellpadding='0' width='100%' cellspacing='0' border='0'><tr><td align='left'><small>$timeStart</small></td><td align='right'><small>$timeEnd</small></td></tr></table>";
        } else
            $res = "";
        $ret["tplprev"] .= $res;


        $res = "<table cellpadding='0' cellspacing='0' class='sched seltf'><tr>";
        $sql = "SELECT * FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($user_id) .
            " AND DATE_FORMAT(`sched_date`, '%Y-%m-%d')=" . $DB->F(date("Y-m-d", strtotime($targetdate))) .
            ";";
        //$ret["tpl"] = $sql;
        $r = $DB->getRow($sql, true);
        if (count($r)) {
            $wt = ($r['endtime'] - $r['starttime']) / 30;
            $timeStart = (strlen(floor($r['starttime'] / 60)) == 1 ? "0" . floor($r['starttime'] / 60) : floor($r['starttime'] /
                    60)) . ":" . (strlen($r['starttime'] % 60) == 1 ? "0" . $r['starttime'] % 60 : $r['starttime'] % 60);
            $timeEnd = (strlen(floor($r['endtime'] / 60)) == 1 ? "0" . floor($r['endtime'] / 60) : floor($r['endtime'] /
                    60)) . ":" . (strlen($r['endtime'] % 60) == 1 ? "0" . $r['endtime'] % 60 : $r['endtime'] % 60);

            $sql = "SELECT tsk.id, gfx.startTime, tsk.plugin_uid, ttype.duration, status.color, addr.name, addr.kladrcode,
                addr.althouse, ticket.kv, addr.ods_id, addr.id AS addr_id FROM `gfx` AS gfx
                LEFT JOIN `tasks` AS tsk ON tsk.id=gfx.task_id
                LEFT JOIN `tickets` AS ticket ON ticket.task_id=gfx.task_id
                LEFT JOIN `task_types` AS ttype ON ttype.id=ticket.task_type
                LEFT JOIN `task_status` AS status ON status.id=tsk.status_id
                LEFT JOIN `list_addr` AS addr ON addr.id=ticket.dom_id
                WHERE gfx.empl_id=" . $DB->F($user_id)
                . " AND CASE WHEN ifnull(ticket.sc_id,0) = 0 THEN 1
 WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = ticket.sc_id) THEN 1
 ELSE 0
END = 1"
                . " AND gfx.c_date=" . $DB->
                F(date("Y-m-d", strtotime($targetdate))) . ";";
            //$ret["tpl"] .= $sql;
            $DB->query($sql);
            $ticketsOnSched = [];
            if ($DB->num_rows()) {
                while (list($id, $startTime, $tType, $dur, $color, $dom, $kladr, $altdom, $kv, $ods,
                    $addr) = $DB->fetch()) {
                    $ticketsOnSched[$startTime] = [
                        $id,
                        $tType,
                        $dur,
                        $color,
                        $dom,
                        $kladr,
                        $altdom,
                        $kv,
                        $startTime,
                        $ods,
                        $addr];
                }
            }
            $DB->free();
            $draw = false;
            $width = 0;
            for ($k = $r['starttime'] / 30; $k < $r['endtime'] / 30; $k += 1) {
                if (array_key_exists($k * 30, $ticketsOnSched)) {
                    $draw = $k * 30;
                    $width = $ticketsOnSched[$draw][2] / 30 * (100 / $wt);
                    $type = $ticketsOnSched[$draw][1] == "connections" ? "Подключение" :
                        "Сервисное обслуживание";
                    $duration = $ticketsOnSched[$draw][2];
                    if ($ticketsOnSched[$draw][1] == "accidents")
                        $tdstr = "А";
                    else
                        $tdstr = mb_substr($type, 0, 1);
                    $addr = new addr_interface_plugin();
                    $street = $addr->StreetByCode($ticketsOnSched[$draw][5]);
                    $res .= "<td ods=\"" . $ticketsOnSched[$draw][9] . "\" kladr=\"" . $ticketsOnSched[$draw][10] .
                        "\" kladr=\"" . $kladr . "\" duration='" . ($duration / 30) .
                        "' align='center' title='Заявка № " . $ticketsOnSched[$draw][0] . " &#xA;Тип: " .
                        $type . " &#xA;Длительность: " . $duration . " мин. &#xA;Адрес: " . $street .
                        " " . $ticketsOnSched[$draw][4] . ($ticketsOnSched[$draw][6] ? " " . $ticketsOnSched[$draw][6] :
                            "") . ", кв. " . ($ticketsOnSched[$draw][7] ? $ticketsOnSched[$draw][7] : "-") .
                        " ' width='" . (floor($width)) . "%' style='overflow:hidden;background-color: " .
                        $ticketsOnSched[$draw][3] . "; border: 1px solid " . $ticketsOnSched[$draw][3] .
                        ";' class='tf busy " . $ticketsOnSched[$draw][1] . " start end'><strong>" . $tdstr .
                        "</strong></td>";
                }
                if (($k) * 30 >= ($draw + $ticketsOnSched[$draw][2])) {
                    $draw = false;
                }
                if (!$draw) {
                    $res .= "<td width='" . floor(100 / $wt) . "%' class='tf free cell'></td>";
                } else {
                    $width += 100 / $wt;
                }
            }

            $res .= "<input type='hidden' name='rudate' value='" . rudate("d M Y", strtotime
                ($targetdate)) . "' />
                <input type='hidden' name='date' value='" . date("Y-m-d",
                    strtotime($targetdate)) . "' />
                <input type='hidden' name='user_id' value='" . $user_id . "' />
                <input type='hidden' name='tf_start' value='" . $r['starttime'] . "' />
                <input type='hidden' name='tf_end' value='" . $r['endtime'] . "' />
                </tr></table>";
            $res .= "<table class='schedtf' cellpadding='0' width='100%' cellspacing='0' border='0'><tr><td align='left'><small>$timeStart</small></td><td align='right'><small>$timeEnd</small></td></tr></table>";
        } else
            $res = "";
        $ret["tpl"] .= $res;
        $ret["ok"] = "ok";
        echo json_encode($ret);

        return false;

    }

    /**
     * @param Request $request
     * @return string|Response
     */
    public function insertticketaction(Request $request)
    {
        $LKUser = adm_users_plugin::isLKUser($this->getCurrentUser()->getId());
        $user_id = $request->get('user_id');
        $task_id = $request->get('task_id');
        $c_date = $request->get('c_date');
        $tfStart = $request->get('taskstart');
        if (!$user_id || !$task_id || !$c_date || !$tfStart) {
            return new Response('error', /* TODO set 500 */
                200);
        }

        $task = new Task($task_id);

        /** @var OrmTask $ormTask */
        $ormTask = $this->getEm()->find(OrmTask::class, $task_id);

        if ($ormTask->getSchedule()) {
            wf::$taskManager->removeFromSchedule($ormTask, $this->getUserModel());
        }

        $schedule = new Schedule($ormTask);
        $schedule->setDate(new DateTime($c_date))
            ->setStartTime($tfStart);

        if ($LKUser && $schedule->getStartDateTime() < (new DateTime())->modify('+6 hour')) {
            return new Response('Не менее 6 часов от текущего вермени!', 500);
        }

        /** @var OrmUser $technician */
        $technician = $this->getEm()->find(OrmUser::class, $user_id);
        try {
            wf::$taskManager->insertIntoSchedule($ormTask, $schedule, $technician, $this->getUserModel());
        } catch (Exception $exception) {
            return new Response('error', /* TODO set 500 */
                200);
        }
        $task->toMail();

        return new Response('ok');
    }

    public function insertticket(Request $request)
    {
        global $DB, $USER;
        $LKUser = adm_users_plugin::isLKUser($USER->getId());
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/insert.tmpl.htm");

        $new_id = $request->get('newticketid', $this->getSession()->get('newticketid', false));
        if (!$new_id) {
            UIError("Не указан идентификатор заявки для установки в График Работ!");
        }

        $this->getSession()->set("newticketid", $new_id);
        $sql = "SELECT ttype.duration, addr.ods_id, ttype.id, t.plugin_uid, addr.area_id, ticket.sc_id
                FROM `tickets` AS ticket
                LEFT JOIN `task_types` AS ttype ON ttype.id=ticket.task_type
                LEFT JOIN `list_addr` AS addr ON addr.id=ticket.dom_id
                LEFT JOIN `tasks` AS t ON t.id=ticket.task_id
                WHERE ticket.task_id = {$DB->F($new_id)}
                    AND CASE WHEN ifnull(ticket.sc_id,0) = 0 THEN 1
                    WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = {$USER->getId()} AND usa.sc_id = ticket.sc_id) THEN 1
                    ELSE 0
                    END = 1";
        $newticketParams = $DB->getRow($sql);
        if (!$newticketParams) {
            UIError("Заявка с ID: $new_id не найдена в системе. Обратитесь к разработчикам!");
        }

        $cTHead = $new_id . " ";
        if ($newticketParams[3] == "connections") {
            $cTHead .= "Подключение";
            $cTicket = new ConnectionTicket($new_id);
        } else {
            if ($newticketParams[3] == "accidents") {
                $cTHead .= "ТТ (Абонентские аварии)";
                $cTicket = new AccidentTicket($new_id);
            } else {
                $cTHead .= "Обслуживание";
                $cTicket = new ServiceTicket($new_id);
                $isSkp = true;
            }
        }

        if (strlen($cTicket->getTypeName()) < 2) {
            UIError("В заявке " . $new_id . " не задан Вид работ, постановка в график невозможна.");
        }

        if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
            $tpl->setVariable("NOACCESSTOSCHEDULE", "display: none;");
        }
        $tpl->setVariable("FL_TICKETNUM_TYPE", $cTHead);
        $tpl->setVariable("FL_TICKETWTYPE", $cTicket->getTypeName());
        $tpl->setVariable("FL_TICKETADDR", $cTicket->getAddr1($cTicket->getDomId()));
        $tpl->setVariable("FL_TICKETAREA", $cTicket->getArea());

        $addr = new addr_interface_plugin();
        $ods_id = $addr->getODSId($cTicket->getDomId());

        $sql = "SELECT `title` FROM `list_ods` WHERE `id`=" . $DB->F($ods_id);
        $tpl->setVariable("FL_TICKETODS", $DB->getField($sql) ? $DB->getField($sql) :
            "не указана");
        $tpl->setVariable("FL_TICKETDUR", ($newticketParams[0]) . " мин");

        $tpl->setVariable("NEWTICKETID", $new_id);
        $tpl->setVariable("NEWTICKETDUR", $newticketParams[0] / 30);
        $tpl->setVariable("NEWTICKETODS", $newticketParams[1]);
        $dateFormat = 'd-m-Y';
        $interfaceDateFormat = 'd M Y';
        $cDate = date('D');
        $tpl->setVariable("EDITACTION", $this->getUID() . "/loadsched");
        $tpl->setVariable("SAVEACTION", $this->getUID() . "/savesched");
        $tpl->setVariable("RESETACTION", $this->getUID() . "/resetsched");
        $tpl->setVariable("MULTRESETACTION", $this->getUID() . "/resetweektf");
        $tpl->setVariable("MULTEDITACTION", $this->getUID() . "/editweektf");
        $tpl->setVariable("COPYLASTWEEKTFACTION", $this->getUID() . "/copylastweektf");
        $scFilter = $newticketParams[5];
        $dayCount = (int)$request->get('dc', getcfg('dc'));

        if (preg_match("/iPhone|Android|iPad|iPod|webOS/", $_SERVER['HTTP_USER_AGENT']) && !$request->request->has('dc')) {
            $dayCount = 1;
        }


        $empl = new adm_sc_plugin();
        $scFilterList = $empl->getFilterList($scFilter);
        if ($scFilterList) {
            $tpl->setCurrentBlock("sc-list");
            $tpl->setVariable("SCLIST", $scFilterList);
            $tpl->parse("sc-list");
        } else {
            UIError("Отсутствует список Сервисных Центров!", "Ошибка", true, "Создать СЦ", "/adm_sc/edit");
        }

        $pList = "<select name='dc'>";
        for ($i = 1; $i <= 7; $i += 1) {
            if ($dayCount == $i)
                $sel = "selected='selected'";
            else
                $sel = "";
            $pList .= "<option $sel value='" . $i . "'>$i дн. </option>";
        }

        $pList .= "</select>";
        $tpl->setVariable("PERIODLIST", $pList);

        $wDate = $request->get('wDate');
        $startDate = $dayCount != 7 ?
            ($wDate ? $wDate : date($dateFormat)) :
            ($wDate ?
                (date('w', strtotime($wDate)) == 1 ?
                    $wDate :
                    (date('w', strtotime($wDate)) ?
                        date($dateFormat, strtotime($wDate) - (date('w', strtotime($wDate)) - 1) * 24 * 60 * 60) :
                        date($dateFormat, strtotime($wDate) - 6 * 24 * 60 * 60))) :
                ($cDate == "Sun" ?
                    date($dateFormat, strtotime('Mon last week')) :
                    date($dateFormat, strtotime('Mon this week'))));


        $endDate = date($dateFormat, strtotime($startDate) + ($dayCount - 1) * 86400);
        $tpl->setVariable("INT_START", rudate("d M", $startDate));
        $tpl->setVariable("CURRENTSTARTDATE", $startDate);
        $tpl->setVariable("INT_END", rudate($interfaceDateFormat, $endDate));
        $tpl->setVariable("NEXTWEEKLINK", $this->getUID() . "/insertticket/?wDate=" .
            date($dateFormat, strtotime($endDate) + 24 * 60 * 60) . "&dc=$dayCount" . ($scFilter ?
                "&scFilter=" . $scFilter : ""));
        $tpl->setVariable("PREVWEEKLINK", $this->getUID() . "/insertticket/?wDate=" .
            date($dateFormat, strtotime($startDate) - $dayCount * 24 * 60 * 60) . "&dc=$dayCount" .
            ($scFilter ? "&scFilter=" . $scFilter : ""));
        $tpl->setVariable("CURRENTWEEKLINK", $this->getUID() . "/insertticket" . ($scFilter ?
                "?scFilter=" . $scFilter . "&dc=$dayCount" : "?dc=$dayCount"));
        if (!$_REQUEST["wDate"] || (strtotime(date($dateFormat)) >= strtotime($startDate) &&
                strtotime(date($dateFormat)) <= strtotime($endDate))
        )
            $tpl->touchBlock("cweekword");
        for ($i = 0; $i < $dayCount; $i++) {
            $tpl->setCurrentBlock("dateGridHead");
            if (date($dateFormat, strtotime($startDate) + $i * 24 * 60 * 60) == date($dateFormat)) {
                $tpl->setVariable("GRIDHEADCURRENTDAY", "class='gridheadsel'");
            }
            $tpl->setVariable("MULTIPLEPRINTPERDAY", $this->getLink("print_orders_day",
                "c_date=" . date("Y-m-d", strtotime($startDate) + $i * 24 * 60 * 60)));
            $tpl->setVariable("GRIDHEADDATETITLE", rudate("l", date($interfaceDateFormat,
                    strtotime($startDate) + $i * 24 * 60 * 60)) . " " . rudate($interfaceDateFormat,
                    date($interfaceDateFormat, strtotime($startDate) + $i * 24 * 60 * 60)));
            $tpl->setVariable("GRIDHEADDATE", rudate($interfaceDateFormat, date($interfaceDateFormat,
                    strtotime($startDate) + $i * 24 * 60 * 60)) . "<br />" . rudate("l", date($interfaceDateFormat,
                    strtotime($startDate) + $i * 24 * 60 * 60)));
            $tpl->parse("dateGridHead");
        }

        $task = $this->getEm()->find(\models\Task::class, $new_id);
        if(null !== $task->getTicket()->getDom()->getOds()) {
            $ods = $task->getTicket()->getDom()->getOds();
            $tpl->setCurrentBlock('ods_block');
            $tpl->setVariable('ODS_TITLE', $ods->getTitle());
            $tpl->setVariable('ODS_ADDRESS', $ods->getAddress());
            $tpl->setVariable('ODS_CONTACTS', $ods->getFio());
            $tpl->setVariable('ODS_PHONE', $ods->getTel());
            $tpl->setVariable('ODS_COMMENT', $ods->getComment());

            $tpl->parse('ods_block');
        }

        $timeStart = new DateTime($startDate);
        $timeEnd = (clone $timeStart)->modify("+ {$dayCount} days");
        $techs = $this->getUsersByTask($new_id, $timeStart, $timeEnd);
        $this->drawGfx($techs, $tpl, $dayCount, $dateFormat, $startDate, $DB, $USER, $newticketParams);

        if (!$LKUser) {
            $nearTechs = $this->getNearUsersByTask($new_id, $timeStart, $timeEnd);
            $nearTechs = array_filter($nearTechs, function ($tech) use ($techs) {
                return !in_array($tech, $techs);
            });
            if(!empty($nearTechs)) {
                $tpl->setVariable('DC', $dayCount + 1);
                $tpl->touchBlock('usersSeparator');
            }
            $this->drawGfx($nearTechs, $tpl, $dayCount, $dateFormat, $startDate, $DB, $USER, $newticketParams, 'nearUserList');
        }

        if (empty($techs) && empty($nearTechs)) {
            throw new \Exception('Нет техников для выполнения этой заявки!');
        }

        UIHeader($tpl);
        $tpl->show();
    }

    /**
     * @param int $taskId
     * @param DateTime $timeStart
     * @param DateTime $timeEnd
     * @return array|\Doctrine\ORM\PersistentCollection|\models\User[]
     * @throws Exception
     */
    private function getUsersByTask($taskId, DateTime $timeStart, DateTime $timeEnd)
    {
        /** @var \models\Task|\Gorserv\Gerp\ScheduleBundle\Model\JobInterface $task */
        $task = $this->getEm()->getReference(\models\Task::class, $taskId);

        if(null === $task->getTicket()->getServiceCenter() && null === $task->getTicket()->getDom()->getCoordinates() ) {
            throw new \Exception('У этой заявки нет координат и сервисного центра!');
        }

        /** @var TimeframeInterface[] $timeframes */
        $timeframes = $this->getContainer()->get('schedule_manager')->findAvailableTimeframes(
            $task,
            $timeStart,
            $timeEnd
        );

        if(empty($timeframes) && null !== $task->getTicket()->getServiceCenter()) {
            $user_filter = $this->getEm()->getFilters()->enable('user.active');
            $user_filter->setParameter('active', true);
            $technicians = $task->getTicket()->getServiceCenter()->getTechnicians();

            $tmp = [];
            foreach ($technicians as $technician) {
                if(!empty($this->getEm()->getRepository(ExecutorTimeframe::class)->findByInterval($technician, $timeStart, $timeEnd))) {
                    $tmp[] = $technician;
                }
            }
            $technicians = $tmp;
            $this->getEm()->getFilters()->disable('user.active');

            return $technicians;
        }

        return array_unique(array_map(function (TimeframeInterface $tf) {
            return $tf->getExecutor();
        }, $timeframes));
    }

    /**
     * @param \models\User[] $userFilterList
     * @param HTML_Template_IT $tpl
     * @param int $dayCount
     * @param $dateFormat
     * @param $startDate
     * @param \classes\DBMySQL $DB
     * @param User $USER
     * @param int $newticketParams
     */
    private function drawGfx($userFilterList, $tpl, $dayCount, $dateFormat, $startDate, $DB, $USER, $newticketParams, $blockName = 'userList')
    {
        global $USER;

        $LKUser = adm_users_plugin::isLKUser($USER->getId());

        foreach ($userFilterList as $uParam) {
            $tpl->setCurrentBlock($blockName);
            $tpl->setVariable("FIO", $LKUser ? 'Техник #' . $uParam->getId() : $uParam->getFio());

            if (!$LKUser && $uParam->getUsenbnintegr()) {
                $tpl->setVariable("ISNBN", "&nbsp;&nbsp;<span class=\"odsval\" title=\"Включена передача расписания в НБН\" style=\"font-size: 9pt;\">" .
                    $uParam->getId() . "</span>");
            }

            $tpl->setVariable("USERID", $uParam->getId());
            $tpl->setVariable("BR", "\r\n");
            $tpl->setVariable("USERPHONE", $LKUser ? null : $uParam->getPhones());
            $tpl->setVariable('USER_SERVICE_CENTERS', implode(', ', $uParam->getMasterServiceCenters()->toArray()));


            for ($i = 0; $i < $dayCount; $i++) {
                if (date($dateFormat, strtotime($startDate) + $i * 86400) == date($dateFormat)) {
                    $tpl->setVariable("SELCURRENTDATE" . $i, "class='gridbodysel'");
                }

                $sql = "SELECT * FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($uParam->getId()) .
                    " AND `sched_date`=" . $DB->F(date("Y-m-d", strtotime($startDate) + $i * 24 * 60 *
                        60)) . ";";
                $r = $DB->getRow($sql, true);

                if ($r && ((strtotime($startDate) + $i * 86400) >= strtotime(date($dateFormat)))) {
                    $wt = ($r['endtime'] - $r['starttime']) / 30;
                    $timeStart = (strlen(floor($r['starttime'] / 60)) == 1 ? "0" . floor($r['starttime'] / 60) : floor($r['starttime'] /
                            60)) . ":" . (strlen($r['starttime'] % 60) == 1 ? "0" . $r['starttime'] % 60 : $r['starttime'] % 60);
                    $timeEnd = (strlen(floor($r['endtime'] / 60)) == 1 ? "0" . floor($r['endtime'] / 60) : floor($r['endtime'] /
                            60)) . ":" . (strlen($r['endtime'] % 60) == 1 ? "0" . $r['endtime'] % 60 : $r['endtime'] % 60);
                    $res = "<table cellpadding='0' cellspacing='0' class='sched seltf'><tr>";

                    $sql = "SELECT tsk.id, gfx.startTime, tsk.plugin_uid, ttype.duration, status.color, addr.ods_id, addr.name, addr.kladrcode, addr.althouse, ticket.kv, ticket.dom_id, addr.full_address
                                        FROM `gfx` AS gfx
                                        LEFT JOIN `tasks` AS tsk ON tsk.id=gfx.task_id
                                        LEFT JOIN `tickets` AS ticket ON ticket.task_id=gfx.task_id
                                        LEFT JOIN `task_types` AS ttype ON ttype.id=ticket.task_type
                                        LEFT JOIN `task_status` AS status ON status.id=tsk.status_id
                                        LEFT JOIN `list_addr` AS addr ON addr.id=ticket.dom_id
                                        WHERE gfx.empl_id=" . $DB->F($uParam->getId())
                        . " AND CASE WHEN ifnull(ticket.sc_id,0) = 0 THEN 1
 WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = ticket.sc_id) THEN 1
 ELSE 0
END = 1"
                        . " AND gfx.c_date=" . $DB->F(date("Y-m-d", strtotime($startDate) + $i * 24 * 60 * 60)) . ";";
                    $DB->query($sql);
                    $ticketsOnSched = [];
                    if ($DB->num_rows()) {
                        while (list($id, $startTime, $tType, $dur, $color, $ods, $dom, $kladr, $altdom, $kv, $dom_id, $full_address) = $DB->fetch()) {
                            $ticketsOnSched[$startTime] = [
                                $id,
                                $tType,
                                $dur,
                                $color,
                                $ods,
                                $dom,
                                $kladr,
                                $altdom,
                                $kv,
                                $startTime,
                                $dom_id,
                                $full_address
                            ];
                        }
                        $DB->free();
                    }
                    $draw = 0;
                    for ($k = $r['starttime'] / 30; $k < $r['endtime'] / 30; $k += 1) {
                        $hours = floor($k*30 / 60);
                        $minutes = ($k*30 % 60);
                        $time = sprintf('%02d:%02d', $hours, $minutes);
                        $hourStartClass = (0 == $k*30 % 60 ? 'hour-start':'');

                        if (array_key_exists($k * 30, $ticketsOnSched)) {
                            $draw = $k * 30;
                        }
                        if ($k * 30 >= ($draw + @$ticketsOnSched[(int)$draw][2])) {
                            $res .= "<td width='" . (100 / $wt) . "%' class='free tf cell {$hourStartClass}'>";
                            if ($k * 30 % 60 === 0) {
                                $res .= '<div class="time-label-container"><div class="time-label">' . $time . '</div></div>';
                            }
                            $res .= "</td>";
                        } else {
                            if ($k * 30 == $draw) {
                                if ($k * 30 == ($draw + $ticketsOnSched[(int)$draw][2] - 30)) {
                                    $addClass = (($newticketParams[1] == $ticketsOnSched[$draw][4]) && ($newticketParams[1] !=
                                            '' && $newticketParams[1] != '0')) ? " ods end start" : " end start";
                                } else
                                    $addClass = " start";
                            } elseif ($k * 30 == ($draw + $ticketsOnSched[$draw][2] - 30)) {

                                $addClass = (($newticketParams[1] == $ticketsOnSched[$draw][4]) && ($newticketParams[1] !=
                                        '' && $newticketParams[1] != '0')) ? " ods end" : " end";
                            } else {
                                $addClass = "";
                            }
                            $type = $ticketsOnSched[$draw][1] == "connections" ? "Подключение" : $ticketsOnSched[$draw][1] ==
                            "services" ? "Сервисное обслуживание" : "Аварии";
                            $duration = $ticketsOnSched[$draw][2];
                            $hourStart = $draw / 60;
                            $minStart = strlen($draw % 60) == 1 ? "0" . $draw % 60 : $draw % 60;

                            $wtPerc = 100 / $wt;

                            $title = $LKUser ? '' : "Заявка № {$ticketsOnSched[$draw][0]} 
Тип: {$type} 
Длительность: {$duration} мин. 
Начало: {$hourStart}:{$minStart}
Адрес: {$ticketsOnSched[$draw][11]} {$ticketsOnSched[$draw][7]}, кв.(оф.) {$ticketsOnSched[$draw][8]}";

                            $res .= <<< TD
                                <td title='{$title}'
                                  width='{$wtPerc}%' 
                                  style='background-color: {$ticketsOnSched[$draw][3]}; border: 1px solid {$ticketsOnSched[$draw][3]};' 
                                  class='tf busy {$ticketsOnSched[$draw][1]} {$addClass} {$hourStartClass}'>
TD;
                            if ($k * 30 % 60 === 0) {
                                $res .= '<div class="time-label-container"><div class="time-label">' . $time . '</div></div>';
                            }
                            $res .= '</td>';
                        }
                    }
                    $res .= "<input type='hidden' name='rudate' value='" . rudate("d M Y", strtotime
                            ($startDate) + $i * 24 * 60 * 60) . "' />
                                         <input type='hidden' name='date' value='" .
                        date("Y-m-d", strtotime($startDate) + $i * 24 * 60 * 60) . "' />
                                         <input type='hidden' name='user_id' value='" .
                        $uParam->getId() . "' />
                                         <input type='hidden' name='tf_start' value='" .
                        $r['starttime'] . "' />
                                         <input type='hidden' name='tf_end' value='" .
                        $r['endtime'] . "' />
                                         </tr></table>";

                    $res .= "<table class='schedtf' cellpadding='0' width='100%' cellspacing='0' border='0'><tr><td align='left'><small>$timeStart</small></td><td align='right'><small>$timeEnd</small></td></tr></table>";

                    if ($i > 1) {
                        $tpl->setCurrentBlock("sc" . $i);
                    }
                    $tpl->setVariable("SCHED" . $i, $res);
                    $tpl->setVariable("SELCURRENTDATE" . $i, "class='seltf'");

                    if ($i > 1) {
                        $tpl->parse("sc" . $i);
                    }
                } else {
                    if ($i > 1) {
                        $tpl->setCurrentBlock("sc" . $i);
                    }
                    $tpl->setVariable("SELCURRENTDATE" . $i, "class='seltfbusy'");
                    $tpl->setVariable("SCHED" . $i, "");
                    if ($i > 1) {
                        $tpl->parse("sc" . $i);
                    }

                }

            }
            $tpl->parse($blockName);
        }
    }

    /**
     * @param int $taskId
     * @param DateTime $timeStart
     * @param DateTime $timeEnd
     * @return array|\Doctrine\ORM\PersistentCollection|\models\User[]
     * @throws Exception
     */
    private function getNearUsersByTask($taskId, DateTime $timeStart, DateTime $timeEnd) {
        /** @var \models\Task|\Gorserv\Gerp\ScheduleBundle\Model\JobInterface $task */
        $task = $this->getEm()->getReference(\models\Task::class, $taskId);

        if(null === $task->getTicket()->getServiceCenter() && null === $task->getTicket()->getDom()->getCoordinates() ) {
            throw new \Exception('У этой заявки нет координат и сервисного центра!');
        }

        $timeframes = $this->getEm()->getRepository(\models\ExecutorTimeframe::class)
            ->findByJob($task, $timeStart, $timeEnd, [
                'strict' => 0,
                'task_area_radius' => 0.02, // TODO: get it from anywhere
            ]);

        return array_unique(array_map(function (TimeframeInterface $tf) {
            return $tf->getExecutor();
        }, $timeframes));
    }

    function printtickets()
    {
        global $DB, $USER;
        if (!$_REQUEST["c_date"] || !$_REQUEST["user_id"]) {
            UIError("Не указан техник и/или дата для печати заявок!");
            exit();
        }
        $c_date = date("Y-m-d", strtotime($_REQUEST["c_date"]));
        $user_id = $_REQUEST["user_id"];

        $sql = "SELECT
            t.*,
            tick.*
        FROM
          `tasks` AS t
          JOIN
          `tickets` AS tick ON tick.task_id=t.id
          JOIN
          gfx ON (gfx.task_id = t.id)
        WHERE
          `c_date` = " . $DB->F($c_date) .
            " AND CASE WHEN ifnull(tick.sc_id,0) = 0 THEN 1
 WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = tick.sc_id) THEN 1
 ELSE 0
END = 1"
            . " AND `empl_id` = " . $DB->F($user_id) . "
        ORDER BY startTime ASC";
        $DB->query($sql);
        if ($DB->errno())
            UIError($DB->error());
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default") {
            $tpl->loadTemplatefile($USER->getTemplate() . "/print_tickets.tmpl.htm");
        } else {
            $tpl->loadTemplatefile("print_tickets.tmpl.htm");
        }
        $tpl->setVariable("SYS_NAME", getcfg('system.name'));
        if ($DB->num_rows()) {
            $user = adm_users_plugin::getUser($user_id);
            $tpl->setVariable("TECHFIO", $user["fio"]);
            $tpl->setVariable("C_DATE", rudate("d M Y", strtotime($c_date)));
            while ($r = $DB->fetch(true)) {
                $tpl->setCurrentBlock("tickrow");
                $tpl->setVariable("ID", $r["id"]);

                switch ($r["plugin_uid"]) {
                    case "connections":
                        $tpl->setVariable("TYPE", "Подключение");
                        $task = new ConnectionTicket($r["id"]);
                        break;
                    case "services":
                        $tpl->setVariable("TYPE", "СКП");
                        $task = new ServiceTicket($r["id"]);
                        break;
                    case "accidents":
                        $tpl->setVariable("TYPE", "Аварии");
                        $task = new AccidentTicket($r["id"]);
                        break;
                    default:
                        $tpl->setVariable("TYPE", 'Неизвестный тип: '.$r["plugin_uid"]);
                        break;
                }
                $tpl->setVariable("ADDRESS", $task->getAddr1($task->getDomId()));
                $tpl->setVariable("POD", $task->getPod() ? $task->getPod() : "&mdash;");
                $tpl->setVariable("FLOOR", $task->getEtazh() ? $task->getEtazh() : "&mdash;");
                $tpl->setVariable("KV", $task->getKv() ? $task->getKv() : "&mdash;");
                $tpl->setVariable("PHONE", sizeof($task->getPhones()) ? implode("<br />", $task->
                getPhones()) : "&mdash;");
                if ($r["plugin_uid"] == "services") {
                    $tpl->setVariable("LOGIN", "&mdash;");
                    $tpl->setVariable("PASS", "&mdash;");
                } else {
                    $tpl->setVariable("LOGIN", $task->getOpLogin() ? $task->getOpLogin() : "&mdash;");
                    $tpl->setVariable("PASS", $task->getOpPassword() ? $task->getOpPassword() :
                        "&mdash;");
                }
                $tpl->setVariable("DATE", rudate("d M Y", $c_date));
                $time = $DB->getField("SELECT `startTime` FROM `gfx` WHERE `empl_id`=" . $DB->F
                    ($user_id) . " AND `task_id`=" . $DB->F($task->getId()) . ";");
                $tpl->setVariable("TIME", floor($time / 60) . ":" . (strlen($time % 60) == 1 ?
                        "00" : $time % 60));
                $tpl->setVariable("DOMOFON", $task->getDomofon());
                $tpl->setVariable("FIO", $task->getFio());
                $tpl->setVariable("COMMENT", $task->getFirstCommentText());
                $tpl->parse("tickrow");
            }

        } else {
            $tpl->touchBlock("no-tickets");
        }
        //print_r($tpl);
        $DB->free();

        $tpl->show();
    }

    public function view_details(Request $request)
    {
        global $DB, $PLUGINS, $USER;
        $LKUser = adm_users_plugin::isLKUser($USER->getId());

        if ($LKUser) {
            throw new \Symfony\Component\Security\Core\Exception\AccessDeniedException('У вас нет доступа.');
        }
        $user_id = @$_POST['user_id'];
        $c_date = @$_POST['c_date'];

        if (!$user_id || !$c_date) {
            UIError('Ошибка: не указаны дата и идентификатор техника!');
        }
        $sql = "SELECT 
                tsk.id,
                cnt.contr_title, 
                gfx.startTime, 
                tsk.plugin_uid,
                ttype.duration, 
                status.name, 
                status.color, 
                ttype.title,
                ticket.kv, 
                status.tag, 
                ticket.dom_id, 
                ticket.sc_id,
                addr.name,
                addr.full_address,
                addr.is_not_recognized 
                FROM `gfx` AS gfx
                LEFT JOIN `tasks` AS tsk ON tsk.id=gfx.task_id
                LEFT JOIN `tickets` AS ticket ON ticket.task_id=gfx.task_id
                LEFT JOIN `task_types` AS ttype ON ttype.id=ticket.task_type
                LEFT JOIN `task_status` AS status ON status.id=tsk.status_id
                LEFT JOIN `list_contr` AS cnt ON cnt.id=ticket.cnt_id
                LEFT JOIN `list_addr` AS addr ON addr.id=ticket.dom_id
                WHERE
                    gfx.empl_id=" . $DB->F($user_id) . "
                    AND
                    CASE
                        WHEN ifnull(ticket.sc_id,0) = 0 THEN 1
                        WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = ticket.sc_id) THEN 1 ELSE 0
                        END = 1
                    AND gfx.c_date=" . $DB->F($c_date) . ";";

        $DB->query($sql);
        if ($DB->num_rows()) {
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $tpl->loadTemplatefile($USER->getTemplate() . "/daily.tmpl.htm");
            if ($USER->isTech($USER->getId())) {
                $tpl->touchBlock("istechcallth");
            }
            $tpl->setVariable("MULTIPLEPRINTFORTECHPERDAY", $this->getLink("print_orders_day_tech",
                "user_id=" . $user_id . "&c_date=" . $c_date));
            while (list($id, $cnt, $stime, $tType, $dur, $status, $status_color, $ttypeTitle,
                $flat, $tag, $dom_id, $sc_id, $dom, $full_address, $is_not_recognized) = $DB->fetch()) {
                $tpl->setCurrentBlock("item");
                if ($USER->isTech($USER->getId())) {
                    $t = new Task($id);
                    switch ($t->getPluginUid()) {
                        case "services":
                            $ticket = new ServiceTicket($id);
                            break;

                        case "connections":
                            $ticket = new ConnectionTicket($id);
                            break;

                        case "accidents":
                            $ticket = new AccidentTicket($id);
                            break;

                        default:
                            $ticket = false;
                            break;
                    }
                    //$ticket = new

                    if ($ticket !== false)
                        $phnes = $ticket->getPhones();
                    $clientPhone = ($phnes[0] != "" && $phnes[0] != "70000000000") ? $phnes[0] : (($phnes[1] !=
                        "" && $phnes[1] != "70000000000") ? $phnes[1] : false);
                    $tpl->setCurrentBlock("istechcallth");
                    if ($clientPhone) {
                        $tpl->setVariable("CLIENTPHONENUMBER", $clientPhone);
                        $tpl->setVariable("TECHVIRTEXT", $USER->getIntPhone());
                        $tpl->setVariable("TC_TASK_ID", $id);
                    } else {
                        $tpl->setVariable("NOCALLSINTASK", "hidden");
                    }
                    $tpl->parse("istechcallth");
                }
                $tpl->setVariable("ITEM_ID", $id);
                $tpl->setVariable("ITEM_TYPE", $ttypeTitle . "<br /><small>" . ($tType ==
                    "connections" ? "<strong>Подключение</strong>" : ($tType == "accidents" ?
                        "<strong>ТТ (Абон. Аварии)</strong>" : "<strong>Ремонт</strong>")) . "</small>");
                $tpl->setVariable("ITEM_STATUS", $status);
                if ($tType == "connections") {
                    $task = new ConnectionTicket($id);
                } else {
                    if ($tType == "accidents")
                        $task = new AccidentTicket($id);
                    else
                        $task = new ServiceTicket($id);
                }
                $startHour = (strlen(floor($stime / 60)) == 1) ? "0" . (floor($stime / 60)) :
                    floor($stime / 60);
                $startMin = (strlen(floor($stime % 60)) == 1) ? "0" . (floor($stime % 60)) :
                    floor($stime % 60);
                $endHour = (strlen(($stime + $dur) / 60) == 1) ? "0" . floor(($stime + $dur) /
                        60) : floor(($stime + $dur) / 60);
                $endMin = (strlen(($stime + $dur) % 60) == 1) ? "0" . floor(($stime + $dur) % 60) :
                    floor(($stime + $dur) % 60);
                $tpl->setVariable("ITEM_TIME", $startHour . ":" . $startMin . "-" . $endHour .
                    ":" . $endMin);
                $tpl->setVariable("ITEM_CTGR", $cnt);
                $duration = $task->getTypeDuration($task->getId());
                if ($duration) {
                    $durationdel = $duration / 30;
                }
                $tpl->setVariable("DURATION", $duration);
                $tpl->setVariable("DURATIONDEL", $durationdel);
                //$tpl->setVariable("ODS_ID", "");
                $tpl->setVariable("TASK_TYPE", $ttypeTitle);
                $tpl->setVariable("TASK_TITLE", $tType == "connections" ?
                    "<strong>Подключение</strong>" : "<strong>Ремонт</strong>");
                if ($sc_id)
                    $sc = adm_sc_plugin::getSC($sc_id);
                else
                    $sc["title"] = "-";
                $tpl->setVariable("TASK_SC", $sc["title"]);

                $sql = "SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($dom_id) . ";";
                $r = $DB->getRow($sql, true);
                if ($r !== false) {
                    $ods = $r["ods_id"];
                } else {
                    $ods = "";
                    $addr = "-";
                }

                $tpl->setVariable("ODS_ID", $ods);
                if ($ods != "")
                    $ods_name = $DB->getField("SELECT `title` FROM `list_ods` WHERE `id`=" . $DB->F
                        ($ods) . ";");
                if ($ods_name != "")
                    $tpl->setVariable("ODS_NAME", $ods_name);
                else {
                    $tpl->setVariable("ODS_NAME", "не указана");
                }

                if (!empty($full_address)) {
                    if ($is_not_recognized) {
                        $full_address = $full_address . ' <sup>*</sup>';
                    }

                    $tpl->setVariable("ITEM_ADDR", $full_address . ", кв.(оф.) " . $flat);
                } else {
                    $tpl->setVariable("ITEM_ADDR", "<font color=#FF0000>Адрес не задан</font>");
                }
                if (!preg_match("/grafik/", $tag)) {
                    $tpl->setVariable("HIDEELEM", "style='display: none;'");
                }
                if (User::isTech($USER->getId()) && !User::isChiefTech($USER->getId())) {
                    $tpl->setVariable("HIDEDELTICKET", "style='display:none;'");
                    $tpl->setVariable("HIDEELEM", "style='display: none;'");
                }
                $tpl->setVariable("C_DATE", $c_date);
                $tpl->setVariable("TECH_ID", $user_id);
                $tpl->setVariable("ITEM_STATUS_COLOR", "background-color:" . $status_color . ";");
                $tpl->setVariable("EDITTASKHREF", $this->getLinkStatic($tType, "viewticket",
                    "task_id=" . $id));
                $tpl->setVariable("HREF_ORDER", $this->getLinkStatic($tType, "vieworderdoc",
                    "id=" . $id));
                $tpl->parse("item");
            }
            $tpl->show();
            $DB->free();
        } else {
            echo "empty";
        }
    }

    function deletefromgfxcell_ajax(Request $request)
    {
        global $DB, $USER;
        $LKUser = adm_users_plugin::isLKUser($USER->getId());

        if ($LKUser) {
            throw new \Symfony\Component\Security\Core\Exception\AccessDeniedException('У вас нет доступа.');
        }
        $user_id = $_REQUEST["user_id"];
        $targetdate = $_REQUEST["targetdate"];
        $task_id = $_REQUEST["task_id"];
        $sc_id = $_REQUEST["sc_id"];

        if (!$user_id || !$task_id || !$targetdate) {
            $ret["error"] = "Ошибка! Недостаточно данных!";
            echo json_encode($ret);

            return false;
        }
        $task = new Task($task_id);

        /** @var OrmTask $ormTask */
        $ormTask = $this->getEm()->find(OrmTask::class, $task_id);
        wf::$taskManager->removeFromSchedule($ormTask, false, $this->getUserModel());

        /** @var TaskStatus $defaultState */
        $defaultState = $this->getEm()->getRepository(TaskStatus::class)->findOneBy([
            'isActive' => true,
            'isDefault' => true,
            'pluginUid' => $task->getPluginUid(),
        ]);


        $res = "<table cellpadding='0' cellspacing='0' class='sched seltf'><tr>";
        $sql = "SELECT * FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($user_id) .
            " AND DATE_FORMAT(`sched_date`, '%Y-%m-%d')=" . $DB->F(date("Y-m-d", strtotime($targetdate))) .
            ";";
        //$ret["tpl"] = $sql;
        $r = $DB->getRow($sql, true);
        if (count($r)) {
            $wt = ($r['endtime'] - $r['starttime']) / 30;
            $timeStart = (strlen(floor($r['starttime'] / 60)) == 1 ? "0" . floor($r['starttime'] / 60) : floor($r['starttime'] /
                    60)) . ":" . (strlen($r['starttime'] % 60) == 1 ? "0" . $r['starttime'] % 60 : $r['starttime'] % 60);
            $timeEnd = (strlen(floor($r['endtime'] / 60)) == 1 ? "0" . floor($r['endtime'] / 60) : floor($r['endtime'] /
                    60)) . ":" . (strlen($r['endtime'] % 60) == 1 ? "0" . $r['endtime'] % 60 : $r['endtime'] % 60);

            $sql = "SELECT tsk.id, gfx.startTime, tsk.plugin_uid, ttype.duration, status.color, addr.name, addr.kladrcode,
                addr.althouse, ticket.kv, addr.ods_id, addr.id AS addr_id FROM `gfx` AS gfx
                LEFT JOIN `tasks` AS tsk ON tsk.id=gfx.task_id
                LEFT JOIN `tickets` AS ticket ON ticket.task_id=gfx.task_id
                LEFT JOIN `task_types` AS ttype ON ttype.id=ticket.task_type
                LEFT JOIN `task_status` AS status ON status.id=tsk.status_id
                LEFT JOIN `list_addr` AS addr ON addr.id=ticket.dom_id
                WHERE gfx.empl_id=" . $DB->F($user_id)
                . " AND CASE WHEN ifnull(ticket.sc_id,0) = 0 THEN 1
 WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = ticket.sc_id) THEN 1
 ELSE 0
END = 1"
                . " AND gfx.c_date=" . $DB->
                F(date("Y-m-d", strtotime($targetdate))) . ";";
            //$ret["tpl"] .= $sql;
            $DB->query($sql);
            $ticketsOnSched = [];
            if ($DB->num_rows()) {
                while (list($id, $startTime, $tType, $dur, $color, $dom, $kladr, $altdom, $kv, $ods,
                    $addr) = $DB->fetch()) {
                    $ticketsOnSched[$startTime] = [
                        $id,
                        $tType,
                        $dur,
                        $color,
                        $dom,
                        $kladr,
                        $altdom,
                        $kv,
                        $startTime,
                        $ods,
                        $addr];
                }
            }
            $DB->free();
            $draw = false;
            $width = 0;
            for ($k = $r['starttime'] / 30; $k < $r['endtime'] / 30; $k += 1) {
                if (array_key_exists($k * 30, $ticketsOnSched)) {
                    $draw = $k * 30;
                    $width = $ticketsOnSched[$draw][2] / 30 * (100 / $wt);
                    $type = $ticketsOnSched[$draw][1] == "connections" ? "Подключение" :
                        "Сервисное обслуживание";
                    $duration = $ticketsOnSched[$draw][2];
                    if ($ticketsOnSched[$draw][1] == "accidents")
                        $tdstr = "А";
                    else
                        $tdstr = mb_substr($type, 0, 1);
                    $addr = new addr_interface_plugin();
                    $street = $addr->StreetByCode($ticketsOnSched[$draw][5]);
                    $res .= "<td ods=\"" . $ticketsOnSched[$draw][9] . "\" kladr=\"" . $ticketsOnSched[$draw][10] .
                        "\" duration='" . ($duration / 30) . "' align='center' title='Заявка № " . $ticketsOnSched[$draw][0] .
                        " &#xA;Тип: " . $type . " &#xA;Длительность: " . $duration .
                        " мин. &#xA;Адрес: " . $street . " " . $ticketsOnSched[$draw][4] . ($ticketsOnSched[$draw][6] ?
                            " " . $ticketsOnSched[$draw][6] : "") . ", кв. " . ($ticketsOnSched[$draw][7] ?
                            $ticketsOnSched[$draw][7] : "-") . " ' width='" . (floor($width)) .
                        "%' style='overflow:hidden;background-color: " . $ticketsOnSched[$draw][3] .
                        "; border: 1px solid " . $ticketsOnSched[$draw][3] . ";' class='tf busy " . $ticketsOnSched[$draw][1] .
                        " start end'><strong>" . $tdstr . "</strong></td>";
                }
                if (($k) * 30 >= ($draw + $ticketsOnSched[$draw][2])) {
                    $draw = false;
                }
                if (!$draw) {
                    $res .= "<td width='" . floor(100 / $wt) . "%' class='tf free cell'></td>";
                } else {
                    $width += 100 / $wt;
                }
            }

            $res .= "<input type='hidden' name='rudate' value='" . rudate("d M Y", strtotime
                ($targetdate)) . "' />
                <input type='hidden' name='date' value='" . date("Y-m-d",
                    strtotime($targetdate)) . "' />
                <input type='hidden' name='user_id' value='" . $user_id . "' />
                <input type='hidden' name='tf_start' value='" . $r['starttime'] . "' />
                <input type='hidden' name='tf_end' value='" . $r['endtime'] . "' />
                </tr></table>";
            $res .= "<table class='schedtf' cellpadding='0' width='100%' cellspacing='0' border='0'><tr><td align='left'><small>$timeStart</small></td><td align='right'><small>$timeEnd</small></td></tr></table>";
        } else
            $res = "";
        $ret["tpl"] .= $res;
        $ret["ok"] = "ok";


        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        $tpl->loadTemplatefile($USER->getTemplate() . "/freeticklist.tmpl.htm");

        $sc_target = User::isChiefTech($USER->getId()) or false;

        if ($sc_target !== false) {
            //$sql_filter = " AND tick.sc_id=" . $DB->F($sc_target) . "";
        } else {
            if ($sc_id != "0" and $sc_id != "" and $sc_id !== false) {
                $sql_filter = " AND tick.sc_id=" . $DB->F($sc_id) . "";
            }
        }

        $connnewstatus = adm_statuses_plugin::getStatusByTag("new", "connections");
        $skpnewstatus = adm_statuses_plugin::getStatusByTag("new", "services");
        $ttnewstatus = adm_statuses_plugin::getStatusByTag("new", "accidents");
        //print_r($connnewstatus);
        //print_r($skpnewstatus);
        //die();
        //freeticktpl

        $sql = "SELECT t.id, t.task_title, tick.* FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON t.id=tick.task_id WHERE tick.task_id>0 AND t.status_id=" .
            $ttnewstatus["id"] . $sql_filter . ";";
        $DB->query($sql);
        if ($DB->num_rows()) {
            $hasTick = true;
            $tpl->setCurrentBlock("ticklist");
            while ($rr = $DB->fetch(true)) {
                $tpl->setCurrentBlock("titem");
                $tpl->setVariable("TICKTITLE", "<strong class='freeticket'>А</strong>: " . $rr["id"]);
                $tpl->setVariable("STATUS_COLOR", $ttnewstatus["color"]);
                $tpl->setVariable("TASK_ID", $rr["id"]);
                $tpl->setVariable("TASK_CONTR", kontr_plugin::getByID($rr["cnt_id"]));
                $tpl->setVariable("TASK_TYPE", adm_ticktypes_plugin::getByID($rr["task_type"]));
                $tpl->setVariable("TASK_TYPE_ID", $rr["task_type"]);
                $tpl->setVariable("TASK_CONTR_ID", $rr["cnt_id"]);
                $tpl->setVariable("TASK_TITLE", $rr["task_title"]);
                if ($rr["sc_id"])
                    $sc = adm_sc_plugin::getSC($rr["sc_id"]);
                else
                    $sc["title"] = "-";
                $tpl->setVariable("TASK_SC", $sc["title"]);

                if ($rr["dom_id"]) {
                    $tpl->setVariable("DOM_ID", $rr["dom_id"]);
                    $sql = "SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($rr["dom_id"]) . ";";
                    $r = $DB->getRow($sql, true);
                    if ($r !== false) {
                        $streetAddress = implode(",", addr_interface_plugin::decodeFullKLADRPartial($r["kladrcode"]));
                        $addr = $streetAddress . ", " . ($r["name"] ? $r["name"] : ($r["althouse"] ? $r["althouse"] :
                                "-")) . " под. " . $rr["pod"] . ", эт. " . $rr["etazh"] . " кв. " . $rr["kv"];
                        $ods = $r["ods_id"];
                    } else {
                        $ods = "";
                        $addr = "-";
                    }
                } else {
                    $tpl->setVariable("DOM_ID", "");
                    $ods = "";
                    $addr = "-";
                }

                $tpl->setVariable("TASK_HREF", "accidents/viewticket/?task_id=" . $rr["id"]);
                $tpl->setVariable("ODS_ID", $ods);
                $tpl->setVariable("TASK_ADDR", $addr);

                $duration = adm_ticktypes_plugin::getDurationByID($rr["task_type"]);
                if (!$duration) {
                    $ret["error"] = "Не определена длительность выполнения для вида работ типа ID: " .
                        $rr["task_type"];
                    //echo json_encode($ret);
                    //return false;
                }
                $tpl->setVariable("DURATION", $duration);
                $tpl->setVariable("DURATIONDEL", $duration / 30);

                if ($ods != "")
                    $ods_name = $DB->getField("SELECT `title` FROM `list_ods` WHERE `id`=" . $DB->F
                        ($ods) . ";");
                if ($ods_name != "")
                    $tpl->setVariable("ODS_NAME", $ods_name);
                else {
                    $tpl->setVariable("ODS_NAME", "не указана");
                }
                $tpl->parse("titem");
            }
            $tpl->parse("ticklist");
        }
        $DB->free();
        if (!$hasTick) {
            $tpl->touchBlock("no-tick");
            $tpl->setVariable("TICKALIGN", "middle");
        }


        $hasTick = false;
        $tpl->setVariable("TICKALIGN", "top");
        $sql = "SELECT t.id, t.task_title, tick.* FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON t.id=tick.task_id WHERE tick.task_id>0 AND t.status_id=" .
            $connnewstatus["id"] . $sql_filter . ";";
        //die($sql);
        $DB->query($sql);
        if ($DB->num_rows()) {
            $hasTick = true;
            $tpl->setCurrentBlock("ticklist");
            while ($rr = $DB->fetch(true)) {
                $tpl->setCurrentBlock("titem");
                $tpl->setVariable("TICKTITLE", "<strong class='freeticket'>П</strong>: " . $rr["id"]);
                $tpl->setVariable("STATUS_COLOR", $connnewstatus["color"]);
                $tpl->setVariable("TASK_ID", $rr["id"]);
                $tpl->setVariable("TASK_CONTR", kontr_plugin::getByID($rr["cnt_id"]));
                $tpl->setVariable("TASK_TYPE", adm_ticktypes_plugin::getByID($rr["task_type"]));
                $tpl->setVariable("TASK_TYPE_ID", $rr["task_type"]);
                $tpl->setVariable("TASK_CONTR_ID", $rr["cnt_id"]);
                $tpl->setVariable("TASK_TITLE", $rr["task_title"]);
                if ($rr["sc_id"])
                    $sc = adm_sc_plugin::getSC($rr["sc_id"]);
                else
                    $sc["title"] = "-";
                $tpl->setVariable("TASK_SC", $sc["title"]);
                if ($rr["dom_id"]) {
                    $tpl->setVariable("DOM_ID", $rr["dom_id"]);
                    $sql = "SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($rr["dom_id"]) . ";";
                    $r = $DB->getRow($sql, true);
                    if ($r !== false) {
                        $streetAddress = implode(",", addr_interface_plugin::decodeFullKLADRPartial($r["kladrcode"]));
                        $addr = $streetAddress . ", " . ($r["name"] ? $r["name"] : ($r["althouse"] ? $r["althouse"] :
                                "-")) . " под. " . $rr["pod"] . ", эт. " . $rr["etazh"] . " кв. " . $rr["kv"];
                        $ods = $r["ods_id"];
                    } else {
                        $ods = "";
                        $addr = "-";
                    }
                } else {
                    $tpl->setVariable("DOM_ID", "");
                    $ods = "";
                    $addr = "-";
                }

                $tpl->setVariable("ODS_ID", $ods);
                $tpl->setVariable("TASK_HREF", "connections/viewticket/?task_id=" . $rr["id"]);

                $tpl->setVariable("TASK_ADDR", $addr);
                $duration = adm_ticktypes_plugin::getDurationByID($rr["task_type"]);
                if (!$duration) {
                    $ret["error1"] = "Не определена длительность выполнения для вида работ типа ID: " .
                        $rr["task_type"];
                    //echo json_encode($ret);
                    //return false;
                }

                $tpl->setVariable("DURATION", $duration);
                $tpl->setVariable("DURATIONDEL", $duration / 30);
                //$durationdel = $duration/30;

                $tpl->setVariable("TASK_TIME", "");
                if ($ods != "")
                    $ods_name = $DB->getField("SELECT `title` FROM `list_ods` WHERE `id`=" . $DB->F
                        ($ods) . ";");
                if ($ods_name != "")
                    $tpl->setVariable("ODS_NAME", $ods_name);
                else {
                    $tpl->setVariable("ODS_NAME", "не указана");
                }
                //$tpl->setVariable("ODS_NAME", "gfsdfa");

                $tpl->parse("titem");
            }
            $tpl->parse("ticklist");
        }
        $DB->free();


        $sql = "SELECT t.id, t.task_title, tick.* FROM `tasks` AS t LEFT JOIN `tickets` AS tick ON t.id=tick.task_id WHERE tick.task_id>0 AND t.status_id=" .
            $skpnewstatus["id"] . $sql_filter . ";";
        $DB->query($sql);
        if ($DB->num_rows()) {
            $hasTick = true;
            $tpl->setCurrentBlock("ticklist");
            while ($rr = $DB->fetch(true)) {
                $tpl->setCurrentBlock("titem");
                $tpl->setVariable("TICKTITLE", "<strong class='freeticket'>С</strong>: " . $rr["id"]);
                $tpl->setVariable("STATUS_COLOR", $skpnewstatus["color"]);
                $tpl->setVariable("TASK_ID", $rr["id"]);
                $tpl->setVariable("TASK_CONTR", kontr_plugin::getByID($rr["cnt_id"]));
                $tpl->setVariable("TASK_TYPE", adm_ticktypes_plugin::getByID($rr["task_type"]));
                $tpl->setVariable("TASK_TYPE_ID", $rr["task_type"]);
                $tpl->setVariable("TASK_CONTR_ID", $rr["cnt_id"]);
                $tpl->setVariable("TASK_TITLE", $rr["task_title"]);
                if ($rr["sc_id"])
                    $sc = adm_sc_plugin::getSC($rr["sc_id"]);
                else
                    $sc["title"] = "-";
                $tpl->setVariable("TASK_SC", $sc["title"]);

                if ($rr["dom_id"]) {
                    $tpl->setVariable("DOM_ID", $rr["dom_id"]);
                    $sql = "SELECT * FROM `list_addr` WHERE `id`=" . $DB->F($rr["dom_id"]) . ";";
                    $r = $DB->getRow($sql, true);
                    if ($r !== false) {
                        $streetAddress = implode(",", addr_interface_plugin::decodeFullKLADRPartial($r["kladrcode"]));
                        $addr = $streetAddress . ", " . ($r["name"] ? $r["name"] : ($r["althouse"] ? $r["althouse"] :
                                "-")) . " под. " . $rr["pod"] . ", эт. " . $rr["etazh"] . " кв. " . $rr["kv"];
                        $ods = $r["ods_id"];
                    } else {
                        $ods = "";
                        $addr = "-";
                    }
                } else {
                    $tpl->setVariable("DOM_ID", "");
                    $ods = "";
                    $addr = "-";
                }

                $tpl->setVariable("TASK_HREF", "services/viewticket/?task_id=" . $rr["id"]);
                $tpl->setVariable("ODS_ID", $ods);
                $tpl->setVariable("TASK_ADDR", $addr);

                $duration = adm_ticktypes_plugin::getDurationByID($rr["task_type"]);
                if (!$duration) {
                    $ret["error1"] = "Не определена длительность выполнения для вида работ типа ID: " .
                        $rr["task_type"];
                }
                $tpl->setVariable("DURATION", $duration);
                $tpl->setVariable("DURATIONDEL", $duration / 30);

                if ($ods != "")
                    $ods_name = $DB->getField("SELECT `title` FROM `list_ods` WHERE `id`=" . $DB->F
                        ($ods) . ";");
                if ($ods_name != "")
                    $tpl->setVariable("ODS_NAME", $ods_name);
                else {
                    $tpl->setVariable("ODS_NAME", "не указана");
                }
                $tpl->parse("titem");
            }
            $tpl->parse("ticklist");
        }
        $DB->free();
        if (!$hasTick) {
            $tpl->touchBlock("no-tick");
            $tpl->setVariable("TICKALIGN", "middle");
        }

        $ret["freeticktpl"] = $tpl->get();

        echo json_encode($ret);

        return false;


    }

    function movefromgfxcell_ajax(Request $request)
    {
        global $DB, $USER;
        $user_id = $request->get('user_id');
        $targetdate = $request->get('targetdate');
        $task_id = $request->get('task_id');
        $tfStart = $request->get('taskstart');

        if (!$user_id || !$task_id || !$targetdate || !$tfStart) {
            $ret["error"] = "Ошибка! Недостаточно данных!";
            echo json_encode($ret);

            return false;
        }

        $task = new Task($task_id);

        /** @var OrmTask $ormTask */
        $ormTask = $this->getEm()->find(OrmTask::class, $task_id);

        if ($ormTask->getSchedule() /*&& $request->get("move") === "1"*/) {
            $sql = "SELECT * FROM `gfx` WHERE `task_id`=" . $DB->F($task_id) . ";";
            $currentTask = $DB->getRow($sql, true);

            $appendedCommentAfterRemove = wf::$taskManager->removeFromSchedule($ormTask, $request->get('reason_text', false), $this->getUserModel());

            $task->addComment("Перенос заявки. " . $appendedCommentAfterRemove,
                "Удаление из графика без возврата к статусу Новая.");
        }

        $schedule = new Schedule($ormTask);
        $schedule->setDate(new DateTime($targetdate))
            ->setStartTime($tfStart);

        /** @var OrmUser $technician */
        $technician = $this->getEm()->find(OrmUser::class, $user_id);
        $appendedCommentAfterInsert = wf::$taskManager->insertIntoSchedule($ormTask, $schedule, $technician, $this->getUserModel());

        /** @var TaskStatusRepository $taskStatusRepo */
        $taskStatusRepo = $this->getEm()->getRepository(TaskStatus::class);
        /** @var TaskStatus $scheduleState */
        $scheduleState = $taskStatusRepo->findOneByTag('grafik', $ormTask->getPluginUid());

        $comment = new \models\TaskComment();
        $comment
            ->setText("Поставлена в График Работ. " . $appendedCommentAfterInsert)
            ->setTag(sprintf("Добавлено в график на %s c %s до %s, Исполнитель: %s",
                $schedule->getDate()->format('Y-m-d'),
                $schedule->getStartDateTime()->format('H:i'),
                $schedule->getEndDateTime()->format('H:i'),
                $schedule->getTechnician()->getFio()
            ))
            ->setDatetime(new DateTime())
            ->setStatus($scheduleState);

        //$task->addComment($comment->getText(), $comment->getTag());

        wf::$taskManager->addComment($ormTask, $comment);

        /////
        $res = "<table cellpadding='0' cellspacing='0' class='sched seltf'><tr>";
        $sql = "SELECT * FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($currentTask["empl_id"]) .
            " AND DATE_FORMAT(`sched_date`, '%Y-%m-%d')=" . $DB->F(date("Y-m-d", strtotime($currentTask["c_date"]))) .
            ";";
        //$ret["tpl"] = $sql;
        $r = $DB->getRow($sql, true);
        if (count($r)) {
            $wt = ($r['endtime'] - $r['starttime']) / 30;
            $timeStart = (strlen(floor($r['starttime'] / 60)) == 1 ? "0" . floor($r['starttime'] / 60) : floor($r['starttime'] /
                    60)) . ":" . (strlen($r['starttime'] % 60) == 1 ? "0" . $r['starttime'] % 60 : $r['starttime'] % 60);
            $timeEnd = (strlen(floor($r['endtime'] / 60)) == 1 ? "0" . floor($r['endtime'] / 60) : floor($r['endtime'] /
                    60)) . ":" . (strlen($r['endtime'] % 60) == 1 ? "0" . $r['endtime'] % 60 : $r['endtime'] % 60);

            $sql = "SELECT tsk.id, gfx.startTime, tsk.plugin_uid, ttype.duration, status.color, addr.name, addr.kladrcode,
                addr.althouse, ticket.kv FROM `gfx` AS gfx
                LEFT JOIN `tasks` AS tsk ON tsk.id=gfx.task_id
                LEFT JOIN `tickets` AS ticket ON ticket.task_id=gfx.task_id
                LEFT JOIN `task_types` AS ttype ON ttype.id=ticket.task_type
                LEFT JOIN `task_status` AS status ON status.id=tsk.status_id
                LEFT JOIN `list_addr` AS addr ON addr.id=ticket.dom_id
                WHERE gfx.empl_id=" . $DB->F($currentTask["empl_id"])
                . " AND CASE WHEN ifnull(ticket.sc_id,0) = 0 THEN 1
 WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = ticket.sc_id) THEN 1
 ELSE 0
END = 1"
                . " AND gfx.c_date=" . $DB->F(date("Y-m-d", strtotime($currentTask["c_date"]))) .
                ";";
            //$ret["tpl"] .= $sql;
            $DB->query($sql);
            $ticketsOnSched = [];
            if ($DB->num_rows()) {
                while (list($id, $startTime, $tType, $dur, $color, $dom, $kladr, $altdom, $kv) =
                    $DB->fetch()) {
                    $ticketsOnSched[$startTime] = [
                        $id,
                        $tType,
                        $dur,
                        $color,
                        $dom,
                        $kladr,
                        $altdom,
                        $kv,
                        $startTime];
                }
            }
            $DB->free();
            $draw = false;
            $width = 0;
            for ($k = $r['starttime'] / 30; $k < $r['endtime'] / 30; $k += 1) {
                if (array_key_exists($k * 30, $ticketsOnSched)) {
                    $draw = $k * 30;
                    $width = $ticketsOnSched[$draw][2] / 30 * (100 / $wt);
                    $type = $ticketsOnSched[$draw][1] == "connections" ? "Подключение" :
                        "Сервисное обслуживание";
                    $duration = $ticketsOnSched[$draw][2];
                    if ($ticketsOnSched[$draw][1] == "accidents")
                        $tdstr = "А";
                    else
                        $tdstr = mb_substr($type, 0, 1);
                    $addr = new addr_interface_plugin();
                    $street = $addr->StreetByCode($ticketsOnSched[$draw][5]);
                    $res .= "<td duration='" . ($duration / 30) .
                        "' align='center' title='Заявка № " . $ticketsOnSched[$draw][0] . " &#xA;Тип: " .
                        $type . " &#xA;Длительность: " . $duration . " мин. &#xA;Адрес: " . $street .
                        " " . $ticketsOnSched[$draw][4] . ($ticketsOnSched[$draw][6] ? " " . $ticketsOnSched[$draw][6] :
                            "") . ", кв. " . ($ticketsOnSched[$draw][7] ? $ticketsOnSched[$draw][7] : "-") .
                        " ' width='" . (floor($width)) . "%' style='overflow:hidden;background-color: " .
                        $ticketsOnSched[$draw][3] . "; border: 1px solid " . $ticketsOnSched[$draw][3] .
                        ";' class='tf busy " . $ticketsOnSched[$draw][1] . " start end'><strong>" . $tdstr .
                        "</strong></td>";
                }
                if (($k) * 30 >= ($draw + $ticketsOnSched[$draw][2])) {
                    $draw = false;
                }
                if (!$draw) {
                    $res .= "<td width='" . floor(100 / $wt) . "%' class='tf free cell'></td>";
                } else {
                    $width += 100 / $wt;
                }
            }

            $res .= "<input type='hidden' name='rudate' value='" . rudate("d M Y", strtotime
                ($targetdate)) . "' />
                <input type='hidden' name='date' value='" . date("Y-m-d",
                    strtotime($targetdate)) . "' />
                <input type='hidden' name='user_id' value='" . $user_id . "' />
                <input type='hidden' name='tf_start' value='" . $r['starttime'] . "' />
                <input type='hidden' name='tf_end' value='" . $r['endtime'] . "' />
                </tr></table>";
            $res .= "<table class='schedtf' cellpadding='0' width='100%' cellspacing='0' border='0'><tr><td align='left'><small>$timeStart</small></td><td align='right'><small>$timeEnd</small></td></tr></table>";
        } else
            $res = "";
        $ret["tplprev"] .= $res;
        $res = "<table cellpadding='0' cellspacing='0' class='sched seltf'><tr>";
        $sql = "SELECT * FROM `link_empl_tf` WHERE `user_id`=" . $DB->F($user_id) .
            " AND DATE_FORMAT(`sched_date`, '%Y-%m-%d')=" . $DB->F(date("Y-m-d", strtotime($targetdate))) .
            ";";
        //$ret["tpl"] = $sql;
        $r = $DB->getRow($sql, true);
        if (count($r)) {
            $wt = ($r['endtime'] - $r['starttime']) / 30;
            $timeStart = (strlen(floor($r['starttime'] / 60)) == 1 ? "0" . floor($r['starttime'] / 60) : floor($r['starttime'] /
                    60)) . ":" . (strlen($r['starttime'] % 60) == 1 ? "0" . $r['starttime'] % 60 : $r['starttime'] % 60);
            $timeEnd = (strlen(floor($r['endtime'] / 60)) == 1 ? "0" . floor($r['endtime'] / 60) : floor($r['endtime'] /
                    60)) . ":" . (strlen($r['endtime'] % 60) == 1 ? "0" . $r['endtime'] % 60 : $r['endtime'] % 60);

            $sql = "SELECT tsk.id, gfx.startTime, tsk.plugin_uid, ttype.duration, status.color, addr.name, addr.kladrcode,
                addr.althouse, ticket.kv FROM `gfx` AS gfx
                LEFT JOIN `tasks` AS tsk ON tsk.id=gfx.task_id
                LEFT JOIN `tickets` AS ticket ON ticket.task_id=gfx.task_id
                LEFT JOIN `task_types` AS ttype ON ttype.id=ticket.task_type
                LEFT JOIN `task_status` AS status ON status.id=tsk.status_id
                LEFT JOIN `list_addr` AS addr ON addr.id=ticket.dom_id
                WHERE gfx.empl_id=" . $DB->F($user_id)
                . " AND CASE WHEN ifnull(ticket.sc_id,0) = 0 THEN 1
 WHEN exists (SELECT * FROM user_sc_access usa WHERE usa.user_id = " . $USER->getId() . " AND usa.sc_id = ticket.sc_id) THEN 1
 ELSE 0
END = 1"
                . " AND gfx.c_date=" . $DB->
                F(date("Y-m-d", strtotime($targetdate))) . ";";
            //$ret["tpl"] .= $sql;
            $DB->query($sql);
            $ticketsOnSched = [];
            if ($DB->num_rows()) {
                while (list($id, $startTime, $tType, $dur, $color, $dom, $kladr, $altdom, $kv) =
                    $DB->fetch()) {
                    $ticketsOnSched[$startTime] = [
                        $id,
                        $tType,
                        $dur,
                        $color,
                        $dom,
                        $kladr,
                        $altdom,
                        $kv,
                        $startTime];
                }
            }
            $DB->free();
            $draw = false;
            $width = 0;
            for ($k = $r['starttime'] / 30; $k < $r['endtime'] / 30; $k += 1) {
                if (array_key_exists($k * 30, $ticketsOnSched)) {
                    $draw = $k * 30;
                    $width = $ticketsOnSched[$draw][2] / 30 * (100 / $wt);
                    $type = $ticketsOnSched[$draw][1] == "connections" ? "Подключение" :
                        "Сервисное обслуживание";
                    $duration = $ticketsOnSched[$draw][2];
                    if ($ticketsOnSched[$draw][1] == "accidents")
                        $tdstr = "А";
                    else
                        $tdstr = mb_substr($type, 0, 1);
                    $addr = new addr_interface_plugin();
                    $street = $addr->StreetByCode($ticketsOnSched[$draw][5]);
                    $res .= "<td duration='" . ($duration / 30) .
                        "' align='center' title='Заявка № " . $ticketsOnSched[$draw][0] . " &#xA;Тип: " .
                        $type . " &#xA;Длительность: " . $duration . " мин. &#xA;Адрес: " . $street .
                        " " . $ticketsOnSched[$draw][4] . ($ticketsOnSched[$draw][6] ? " " . $ticketsOnSched[$draw][6] :
                            "") . ", кв. " . ($ticketsOnSched[$draw][7] ? $ticketsOnSched[$draw][7] : "-") .
                        " ' width='" . (floor($width)) . "%' style='overflow:hidden;background-color: " .
                        $ticketsOnSched[$draw][3] . "; border: 1px solid " . $ticketsOnSched[$draw][3] .
                        ";' class='tf busy " . $ticketsOnSched[$draw][1] . " start end'><strong>" . $tdstr .
                        "</strong></td>";
                }
                if (($k) * 30 >= ($draw + $ticketsOnSched[$draw][2])) {
                    $draw = false;
                }
                if (!$draw) {
                    $res .= "<td width='" . floor(100 / $wt) . "%' class='tf free cell'></td>";
                } else {
                    $width += 100 / $wt;
                }
            }

            $res .= "<input type='hidden' name='rudate' value='" . rudate("d M Y", strtotime
                ($targetdate)) . "' />
                <input type='hidden' name='date' value='" . date("Y-m-d",
                    strtotime($targetdate)) . "' />
                <input type='hidden' name='user_id' value='" . $user_id . "' />
                <input type='hidden' name='tf_start' value='" . $r['starttime'] . "' />
                <input type='hidden' name='tf_end' value='" . $r['endtime'] . "' />
                </tr></table>";
            $res .= "<table class='schedtf' cellpadding='0' width='100%' cellspacing='0' border='0'><tr><td align='left'><small>$timeStart</small></td><td align='right'><small>$timeEnd</small></td></tr></table>";
        } else
            $res = "";

        $ret["tpl"] .= $res;
        $ret["ok"] = "ok";

        echo json_encode($ret);

        return false;
    }

    function print_orders_day_tech()
    {
        global $DB, $USER;
        $user_id = @$_REQUEST["user_id"];
        $cdate = @$_REQUEST["c_date"];
        if (!$user_id || !$cdate) {
            UIError("Ошибка! Не указана дата и/или идентификатор техника для печати Заказ-Нарядов");
        }
        $sql = "SELECT gfx.task_id, t.plugin_uid, gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.empl_id=" .
            $DB->F($user_id) . " AND gfx.c_date=" . $DB->F($cdate) .
            " ORDER BY gfx.starttime;";
        $DB->query($sql);
        if ($DB->num_rows()) {
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $tpl->loadTemplatefile($USER->getTemplate() . "/orders.tmpl.htm");
            while (list($id, $val, $c_date, $startTime, $user) = $DB->fetch()) {
                if ($val == "connections") {
                    $ticket = new ConnectionTicket($id);
                } else {
                    if ($tType == "accidents")
                        $ticket = new AccidentTicket($id);
                    else
                        $ticket = new ServiceTicket($id);
                }
                $tpl->touchBlock($val);
                $tpl->setCurrentBlock("documentbody");
                $tpl->setVariable("TASK_ID", $id);
                $tpl->setVariable("OD_NUM", $id . " / " . $ticket->getClntTNum());
                $tpl->touchBlock('client_type_' . $ticket->getClientType());
                if ($ticket->getClientType() != User::CLIENT_TYPE_PHYS) {
                    $tpl->setVariable("OD_CLNT_INFO", $ticket->getOrgName() . "<br />" . $ticket->
                        getFio());
                } else {
                    $tpl->setVariable("OD_CLNT_INFO", $ticket->getFio());
                }
                $tpl->setVariable("OD_CONTRAGENT", $ticket->getContrName());
                $tpl->setVariable("OD_WTYPE", $ticket->getTypeName());
                $phnes = $ticket->getPhones();
                $tpl->setVariable("OD_CLNT_TEL", $phnes[0] . "<br />" . $phnes[1]);

                $tpl->setVariable("OD_SC", connections_plugin::getSC($ticket->getSCId()));
                $tpl->setVariable("OD_AREA", $ticket->getArea());
                $tpl->setVariable("OD_EMPL", $user);
                $ods = connections_plugin::getOds($ticket->getDomId());
                $tpl->setVariable("OD_ODS", $ods ? $ods["title"] . " " . $ods["tel"] : "---");
                $tpl->setVariable("OD_CREATEDATE", rudate("d M Y H:i"));
                $tpl->setVariable("OD_ADDR", $ticket->getAddr($ticket->getDomId()));
                $startHour = (strlen(floor($startTime / 60)) == 1) ? "0" . (floor($startTime /
                        60)) : floor($startTime / 60);
                $startMin = (strlen(floor($startTime % 60)) == 1) ? "0" . (floor($startTime % 60)) :
                    floor($startTime % 60);
                $tpl->setVariable("OD_GFXTIME", rudate("d M Y", strtotime($c_date)) . " " . $startHour .
                    ":" . $startMin);
                $tpl->setVariable("OD_ADDR_ENTR", $ticket->getPod());
                $tpl->setVariable("OD_CNLT_AGRNUM", "<strong>" . $ticket->getClntOpAgr() .
                    "</strong><span style=\"font-weight:normal;\">, Заявка №" . $ticket->
                    getClntTNum() . "</span>");
                $tpl->setVariable("OD_ADDR_DOMOFON", $ticket->getDomofon());
                $tpl->setVariable("OD_TASK_DEVPLACE", $ticket->getSwPlace());
                $tpl->setVariable("OD_ADDR_FLOOR", $ticket->getEtazh());
                $tpl->setVariable("OD_TASK_IP", $ticket->getSwIp());
                $tpl->setVariable("OD_ADDR_FLAT", $ticket->getKv());
                $tpl->setVariable("OD_TASK_PORT", $ticket->getSwPort());
                $tpl->setVariable("OD_ADDINFO", $ticket->getAddInfo());

                $agr_id = $ticket->getAgrId(false);
                if ($agr_id) {
                    if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin",
                            "getAgrById")
                    ) {
                        $kontr = new kontr_plugin();
                        $agr_contacts = $kontr->getAgrById($agr_id);
                        if ($agr_contacts) {
                            $tpl->setVariable("OD_CONTR_ACCESS", $agr_contacts['access_contact'] ? $agr_contacts['access_contact'] :
                                "<strong>-</strong>");
                            $tpl->setVariable("OD_CONTR_TP", $agr_contacts['tp_contact'] ? $agr_contacts['tp_contact'] :
                                "<strong>-</strong>");

                        } else {
                            $tpl->setVariable("OD_CONTR_ACCESS", "<strong>ошибка</strong>");
                            $tpl->setVariable("OD_CONTR_TP", "<strong>ошибка</strong>");
                        }
                    }
                } else {

                    $tpl->setVariable("OD_CONTR_ACCESS", "<strong>-</strong>");
                    $tpl->setVariable("OD_CONTR_TP", "<strong>-</strong>");
                }
                if (isset($_REQUEST['printcomments'])) {
                    $tpl->setCurrentBlock("print-comments");
                    $tpl->setVariable("C_OD_NUM", $id . " / " . $ticket->getClntTNum());
                    $tpl->setVariable("C_COMMENTS_LIST", $ticket->getComments(false));
                    $tpl->parse("print-comments");
                }
                $tpl->parse("documentbody");
            }
            ///UIHeader($tpl);
            $tpl->show();
            $DB->free();
        } else {
            UIError("Ошибка! Заявки отсутствуют!");
        }
    }

    function printscticket()
    {
        global $DB, $USER;
        if (!$_REQUEST["c_date"] || $_REQUEST["sc_id"] === false) {
            UIError("Не указан СЦ и/или дата для печати заявок!");
            exit();
        }
        $cdate = date("Y-m-d", strtotime($_REQUEST["c_date"]));

        if (count($this->getSession()->get("gfx_filter_area_id"))) {
            $area_list = str_replace("no", "0", implode(",", $this->getSession()->get("gfx_filter_area_id")));
            $areaaddsql = " AND user.id IN (SELECT `user_id` FROM `link_user_area` WHERE `area_id` IN (" .
                $area_list . ")) ";
        } else {
            $areaaddsql = "";
        }

        if (count($this->getSession()->get("gfx_filter_wtype_id"))) {
            $wtt_list = str_replace("no", "0", implode(",", $this->getSession()->get("gfx_filter_wtype_id")));
            $userwtaddsql = " AND user.id IN (SELECT `user_id` FROM `list_empl` WHERE `wtype_id` IN (" .
                $wtt_list . ")) ";
        } else {
            $userwtaddsql = "";
        }
        $userwtaddsql .= " and user.id in (SELECT distinct lsu.user_id FROM link_sc_user lsu, user_sc_access usa where usa.sc_id = lsu.sc_id and usa.user_id = " . $USER->getId() . ")";
        if ($USER->isChiefTech($USER->getId())) {
            $sc = $USER->getScIdByUserId($USER->getId());
            if ($sc) {
                //$userList = preg_replace("/,$/", "", implode(",", $DB->getCell("SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" .
                //    $DB->F($sc) . ";")));
                $userList = preg_replace("/,$/", "", implode(",", $DB->getCell("SELECT lsu.user_id FROM `link_sc_user` lsu, user_sc_access usa WHERE lsu.sc_id = usa.sc_id AND usa.user_id = " . $USER->getId())));
                $sql = "SELECT gfx.task_id, t.plugin_uid, gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.c_date=" .
                    $DB->F($cdate) . " AND gfx.empl_id IN (" . $userList . ") $areaaddsql $userwtaddsql ORDER BY gfx.empl_id, gfx.starttime;";
            } else
                $sql = "SELECT gfx.task_id, t.plugin_uid, gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.c_date=" .
                    $DB->F($cdate) . " AND gfx.empl_id=" . $DB->F($USER->getId()) . " $areaaddsql $userwtaddsql ORDER BY gfx.empl_id, gfx.starttime;";

        } else {
            if ($USER->isTech($USER->getId()))
                $sql = "SELECT gfx.task_id, t.plugin_uid, gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.c_date=" .
                    $DB->F($cdate) . " AND gfx.empl_id=" . $DB->F($USER->getId()) . " $areaaddsql $userwtaddsql ORDER BY gfx.empl_id, gfx.starttime;";
            else {
                if ($_REQUEST["sc_id"] == "0" || $_REQUEST["sc_id"] == "")
                    $sql = "SELECT gfx.task_id, t.plugin_uid, gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.c_date=" .
                        $DB->F($cdate) . " $areaaddsql $userwtaddsql ORDER BY gfx.empl_id, gfx.starttime;";
                else {
                    //$userList = preg_replace("/,$/", "", implode(",", $DB->getCell("SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" .
                    //    $DB->F($_REQUEST["sc_id"]) . ";")));
                    $userList = preg_replace("/,$/", "", implode(",", $DB->getCell("SELECT lsu.user_id FROM `link_sc_user` lsu, user_sc_access usa WHERE lsu.sc_id = usa.sc_id AND usa.user_id = " . $USER->getId())));
                    $sql = "SELECT gfx.task_id, t.plugin_uid, gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.c_date=" .
                        $DB->F($cdate) . " AND gfx.empl_id IN (" . $userList . ") $areaaddsql $userwtaddsql ORDER BY gfx.empl_id, gfx.starttime;";
                }
            }
        }
        $DB->query($sql);
        //error_log("\n\n".$sql."\n\n",3,"/ramdisk/sql.log");
        //die($sql);
        if ($DB->errno())
            UIError($DB->error());
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        if ($USER->getTemplate() != "default") {
            $tpl->loadTemplatefile($USER->getTemplate() . "/print_sc_tickets.tmpl.htm");
        } else {
            $tpl->loadTemplatefile("print_sc_tickets.tmpl.htm");
        }
        $tpl->setVariable("C_DATE", rudate("d M Y", strtotime($cdate)));
        if ($DB->num_rows()) {
            $user = adm_users_plugin::getUser($user_id);
            //$tpl->setVariable("TECHFIO", $user["fio"]);

            while ($r = $DB->fetch(true)) {
                //print_r($r);
                //echo "<br />";
                $tpl->setCurrentBlock("tickrow");
                $tpl->setVariable("ID", $r["task_id"]);

                switch ($r["plugin_uid"]) {
                    case "connections":
                        $tpl->setVariable("TYPE", "Подключение");
                        $task = new ConnectionTicket($r["task_id"]);
                        break;
                    case "services":
                        $tpl->setVariable("TYPE", "СКП");
                        $task = new ServiceTicket($r["task_id"]);
                        break;
                    case "accidents":
                        $tpl->setVariable("TYPE", "Аварии");
                        $task = new AccidentTicket($r["task_id"]);
                        break;
                    default:
                        $tpl->setVariable("TYPE", $task->getPluginUid());

                        break;
                }
                $tpl->setVariable("TECHFIO", $r["fio"]);
                $tpl->setVariable("ADDRESS", $task->getAddr1($task->getDomId()));
                $tpl->setVariable("POD", $task->getPod() ? $task->getPod() : "&mdash;");
                $tpl->setVariable("FLOOR", $task->getEtazh() ? $task->getEtazh() : "&mdash;");
                $tpl->setVariable("KV", $task->getKv() ? $task->getKv() : "&mdash;");
                $phnes = $task->getPhones();
                if ($USER->checkAccess("hidetelnum")) {
                    $tpl->setVariable("PHONE", "---");
                } else {
                    $tpl->setVariable("PHONE", ($phnes[0] ? $phnes[0] : '+7') . "<br />" . ($phnes[1] ? $phnes[1] : '+7'));
                }


                //$tpl->setVariable("PHONE", sizeof($task->getPhones()) ? implode("<br />", $task->
                //   getPhones()) : "&mdash;");
                if ($r["plugin_uid"] == "services") {
                    $tpl->setVariable("LOGIN", "&mdash;");
                    $tpl->setVariable("PASS", "&mdash;");
                } else {
                    $tpl->setVariable("LOGIN", $task->getOpLogin() ? $task->getOpLogin() : "&mdash;");
                    $tpl->setVariable("PASS", $task->getOpPassword() ? $task->getOpPassword() :
                        "&mdash;");
                }
                $tpl->setVariable("DATE", rudate("d M Y", $c_date));
                $time = $r["startTime"]; //$DB->getField("SELECT `startTime` FROM `gfx` WHERE `empl_id`=".$DB->F($user_id)." AND `task_id`=".$DB->F($task->getId()).";");
                $tpl->setVariable("TIME", floor($time / 60) . ":" . (strlen($time % 60) == 1 ?
                        "00" : $time % 60));
                $tpl->setVariable("DOMOFON", $task->getDomofon() ? $task->getDomofon() :
                    "&mdash;");
                $tpl->setVariable("FIO", $task->getFio() ? $task->getFio() : "&mdash;");
                $tpl->setVariable("COMMENT", $task->getFirstCommentText());
                $tpl->parse("tickrow");

            }

        } else {
            $tpl->touchBlock("no-tickets");
        }
        $tpl->show();

        $DB->free();
    }

    function print_orders_day()
    {
        global $DB, $USER;

        $cdate = @$_REQUEST["c_date"];
        if (!$cdate) {
            UIError("Ошибка! Не указана дата для печати Заказ-Нарядов");
        }
        if (count($this->getSession()->get("gfx_filter_area_id"))) {
            $area_list = str_replace("no", "0", implode(",", $this->getSession()->get("gfx_filter_area_id")));
            $areaaddsql = " AND user.id IN (SELECT `user_id` FROM `link_user_area` WHERE `area_id` IN (" .
                $area_list . ")) ";
        } else {
            $areaaddsql = "";
        }

        if (count($this->getSession()->get("gfx_filter_wtype_id"))) {
            $wtt_list = str_replace("no", "0", implode(",", $this->getSession()->get("gfx_filter_wtype_id")));
            $userwtaddsql = " AND user.id IN (SELECT `user_id` FROM `list_empl` WHERE `wtype_id` IN (" .
                $wtt_list . ")) ";
        } else {
            $userwtaddsql = "";
        }
        $userwtaddsql .= " and user.id in (SELECT distinct lsu.user_id FROM link_sc_user lsu, user_sc_access usa where usa.sc_id = lsu.sc_id and usa.user_id = " . $USER->getId() . ")";

        //die($areaaddsql);
        if ($USER->isChiefTech($USER->getId())) {
            $sc = $USER->getScIdByUserId($USER->getId());
            if ($sc) {
                //$userList = preg_replace("/,$/", "", implode(",", $DB->getCell("SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" .
                //    $DB->F($sc) . ";")));
                $userList = preg_replace("/,$/", "", implode(",", $DB->getCell("SELECT lsu.user_id FROM `link_sc_user` lsu, user_sc_access usa WHERE lsu.sc_id = usa.sc_id AND usa.user_id = " . $USER->getId())));
                $sql = "SELECT gfx.task_id, t.plugin_uid, gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.c_date=" .
                    $DB->F($cdate) . " AND gfx.empl_id IN (" . $userList . ") $areaaddsql $userwtaddsql ORDER BY gfx.starttime;";
            } else
                $sql = "SELECT gfx.task_id, t.plugin_uid, gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.c_date=" .
                    $DB->F($cdate) . " AND gfx.empl_id=" . $DB->F($USER->getId()) . " $areaaddsql $userwtaddsql ORDER BY gfx.starttime;";

        } else {
            if ($USER->isTech($USER->getId()))
                $sql = "SELECT gfx.task_id, t.plugin_uid, gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.c_date=" .
                    $DB->F($cdate) . " AND gfx.empl_id=" . $DB->F($USER->getId()) . " $areaaddsql $userwtaddsql ORDER BY gfx.starttime;";
            else {
                if ($_REQUEST["sc_id"] == "0" || $_REQUEST["sc_id"] == "")
                    $sql = "SELECT gfx.task_id, t.plugin_uid, gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.c_date=" .
                        $DB->F($cdate) . " $areaaddsql $userwtaddsql ORDER BY gfx.starttime;";
                else {
                    //$userList = preg_replace("/,$/", "", implode(",", $DB->getCell("SELECT `user_id` FROM `link_sc_user` WHERE `sc_id`=" .
                    //    $DB->F($_REQUEST["sc_id"]) . ";")));
                    $userList = preg_replace("/,$/", "", implode(",", $DB->getCell("SELECT lsu.user_id FROM `link_sc_user` lsu, user_sc_access usa WHERE lsu.sc_id = usa.sc_id AND usa.user_id = " . $USER->getId())));
                    $sql = "SELECT gfx.task_id, t.plugin_uid, gfx.c_date, gfx.startTime, user.fio FROM `gfx` AS gfx LEFT JOIN `tasks` AS t ON t.id=gfx.task_id LEFT JOIN `users` AS user ON user.id=gfx.empl_id WHERE gfx.c_date=" .
                        $DB->F($cdate) . " AND gfx.empl_id IN (" . $userList . ") $areaaddsql $userwtaddsql ORDER BY gfx.starttime;";
                }
            }
        }
        //die($sql);
        //error_log("\n\n".$sql."\n\n",3,'/ramdisk/sql.log');
        $DB->query($sql);
        if ($DB->num_rows()) {
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            $tpl->loadTemplatefile($USER->getTemplate() . "/orders.tmpl.htm");
            while (list($id, $val, $c_date, $startTime, $user) = $DB->fetch()) {
                if ($val == "connections") {
                    $ticket = new ConnectionTicket($id);
                } else {
                    if ($tType == "accidents")
                        $ticket = new AccidentTicket($id);
                    else
                        $ticket = new ServiceTicket($id);
                }
                $tpl->setCurrentBlock("documentbody");
                $tpl->touchBlock($val);

                $tpl->setVariable("TASK_ID", $id);
                $tpl->setVariable("OD_NUM", $id . " / " . $ticket->getClntTNum());
                $tpl->touchBlock('client_type_' . $ticket->getClientType());
                if ($ticket->getClientType() != User::CLIENT_TYPE_PHYS) {
                    $tpl->setVariable("OD_CLNT_INFO", $ticket->getOrgName() . "<br />" . $ticket->
                        getFio());
                } else {
                    $tpl->setVariable("OD_CLNT_INFO", $ticket->getFio());
                }
                $tpl->setVariable("OD_CONTRAGENT", $ticket->getContrName());
                $tpl->setVariable("OD_WTYPE", $ticket->getTypeName());
                $phnes = $ticket->getPhones();
                if ($USER->checkAccess("hidetelnum")) {
                    $tpl->setVariable("OD_CLNT_TEL", "---");
                } else {
                    $tpl->setVariable("OD_CLNT_TEL", ($phnes[0] ? $phnes[0] : '+7') . "<br />" . ($phnes[1] ? $phnes[1] : '+7'));
                }

                $tpl->setVariable("OD_SC", connections_plugin::getSc($ticket->getSCId()));
                $tpl->setVariable("OD_AREA", $ticket->getArea());
                $tpl->setVariable("OD_EMPL", $user);
                $ods = connections_plugin::getOds($ticket->getDomId());
                $tpl->setVariable("OD_ODS", $ods ? $ods["title"] . " " . $ods["tel"] : "---");
                $tpl->setVariable("OD_CREATEDATE", rudate("d M Y H:i"));

                $tpl->setVariable("OD_ADDR", is_array($ticket->getAddr($ticket->getDomId())) ? implode(", ", $ticket->
                getAddr($ticket->getId())) : $ticket->getAddr($ticket->getDomId()));
                $startHour = (strlen(floor($startTime / 60)) == 1) ? "0" . (floor($startTime /
                        60)) : floor($startTime / 60);
                $startMin = (strlen(floor($startTime % 60)) == 1) ? "0" . (floor($startTime % 60)) :
                    floor($startTime % 60);
                $tpl->setVariable("OD_GFXTIME", rudate("d M Y", strtotime($c_date)) . " " . $startHour .
                    ":" . $startMin);
                $tpl->setVariable("OD_ADDR_ENTR", $ticket->getPod());
                $tpl->setVariable("OD_CNLT_AGRNUM", "<strong>" . $ticket->getClntOpAgr() .
                    "</strong><span style=\"font-weight:normal;\">, Заявка №" . $ticket->
                    getClntTNum() . "</span>");
                $tpl->setVariable("OD_ADDR_DOMOFON", $ticket->getDomofon());
                $tpl->setVariable("OD_TASK_DEVPLACE", $ticket->getSwPlace());
                $tpl->setVariable("OD_ADDR_FLOOR", $ticket->getEtazh());
                $tpl->setVariable("OD_TASK_IP", $ticket->getSwIp());
                $tpl->setVariable("OD_ADDR_FLAT", $ticket->getKv());
                $tpl->setVariable("OD_TASK_PORT", $ticket->getSwPort());
                $tpl->setVariable("OD_ADDINFO", $ticket->getAddInfo());
                $agr_id = $ticket->getAgrId(false);
                if ($agr_id) {
                    if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin",
                            "getAgrById")
                    ) {
                        $kontr = new kontr_plugin();
                        $agr_contacts = $kontr->getAgrById($agr_id);
                        if ($agr_contacts) {
                            $tpl->setVariable("OD_CONTR_ACCESS", $agr_contacts['access_contact'] ? $agr_contacts['access_contact'] :
                                "<strong>-</strong>");
                            $tpl->setVariable("OD_CONTR_TP", $agr_contacts['tp_contact'] ? $agr_contacts['tp_contact'] :
                                "<strong>-</strong>");

                        } else {
                            $tpl->setVariable("OD_CONTR_ACCESS", "<strong>ошибка</strong>");
                            $tpl->setVariable("OD_CONTR_TP", "<strong>ошибка</strong>");
                        }
                    }
                } else {

                    $tpl->setVariable("OD_CONTR_ACCESS", "<strong>-</strong>");
                    $tpl->setVariable("OD_CONTR_TP", "<strong>-</strong>");
                }

                if (isset($_REQUEST['printcomments'])) {
                    $tpl->setCurrentBlock("print-comments");
                    $tpl->setVariable("C_OD_NUM", $id . " / " . $ticket->getClntTNum());
                    $tpl->setVariable("C_COMMENTS_LIST", $ticket->getCommentsPrint(false));
                    $tpl->parse("print-comments");
                }
                $tpl->parse("documentbody");
            }
            UIHeader($tpl);
            $tpl->show();
            $DB->free();
        } else {
            UIError("Ошибка! Заявки отсутствуют!");
        }
    }
}

