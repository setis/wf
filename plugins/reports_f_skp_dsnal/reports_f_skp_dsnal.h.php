<?php

/**
 * Plugin Header
 * 
 * @author kblp
 * @copyright 2014
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Отчет по нал. ДС по СКП";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Отчет по нал. ДС по СКП";
    
}

?>