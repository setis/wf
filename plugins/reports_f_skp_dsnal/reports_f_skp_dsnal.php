<?php 

/**
 * Plugin Implementation
 * @author kblp
 */

use classes\HTML_Template_IT;
use classes\Plugin;


require_once(dirname(__FILE__)."/../services/services.php");

class reports_f_skp_dsnal_plugin extends Plugin
{     
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }
    
    function main() {
        global $DB, $USER;
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);
        $month = array('01'=>"Январь", '02'=>"Февраль", '03'=>"Март", '04'=>"Апрель", '05'=>"Май", '06'=>"Июнь", '07'=>"Июль", '08'=>"Август", '09'=>"Сентябрь", '10'=>"Октябрь", '11'=>"Ноябрь", '12'=>"Декабрь");
        $_REQUEST["sc_id"] = $this->getCookie("sc_id", (isset($_REQUEST["sc_id"]) ? $_REQUEST["sc_id"] : 8));
        $_REQUEST["createreport"] = $this->getCookie("createreport", $_REQUEST["createreport"]);
        $_REQUEST["datefrom"] = $this->getCookie("datefrom", $_REQUEST["datefrom"]);
        $_REQUEST["dateto"] = $this->getCookie("dateto", $_REQUEST["dateto"]);
        //$_REQUEST["month"] = $this->getCookie('month', $_REQUEST["month"] ? $_REQUEST["month"] : date("m"));
        //$_REQUEST["year"] = $this->getCookie('year', $_REQUEST["year"] ? $_REQUEST["year"] : date("Y"));
        $dateParams = array(0 => "Дата создания", 1 => "Дата выполнения", 2=>"Дата изменения", 3=>"Дата принятия ДС");
        if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
            $cntr = new kontr_plugin();
        }   
        $contrCount = $cntr->getListSer();                    
        if (!isset($_REQUEST['createprintversion']) && !isset($_REQUEST["createxlsversion"])) {
            $tpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $tpl->loadTemplatefile($USER->getTemplate()."/".$this->getUID().".tmpl.htm");
            else 
                $tpl->loadTemplatefile($this->getUID().".tmpl.htm");
            $tpl->setVariable("REPORT__FIN_SKP_TOTAL", "buttonsel1");
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            UIHeader($tpl);
            report_left_menu($tpl);
            if (strtotime($_REQUEST["datefrom"]) > strtotime($_REQUEST["dateto"])) {
                $t = $_REQUEST["datefrom"];
                $_REQUEST["datefrom"] = $_REQUEST["dateto"];
                $_REQUEST["dateto"] = $t;
            }
            $tpl->setVariable("DATE_FROM", $_REQUEST["datefrom"]);
            $tpl->setVariable("DATE_TO", $_REQUEST["dateto"]);
            
            $tpl->setVariable("FILTER_SC_OPTIONS", adm_sc_plugin::getScList($_REQUEST["sc_id"]));
            $contrNalList = $cntr->getListSerNal();
            if (!count($contrNalList)) UIError("Нет контрагентов с договорами, по которым осуществляется наличный расчет по заявкам");
            if (class_exists("kontr_plugin", true) && method_exists("kontr_plugin", "getOptListSer")) {
                $cntr = new kontr_plugin();
                $tpl->setVariable("FILTER_CNT_OPTIONS", array2options($cntr->getListSerNal(), @$_POST["cnt_id"]));
            }
        }
        if (@$_REQUEST["createreport"] || @$_REQUEST["createprintversion"] || @$_REQUEST["createxlsversion"]) {
            $rtpl = new HTML_Template_IT(path2("plugins/".$this->getUID()));
            //$rtpl->loadTemplatefile("report.tmpl.htm");
            if ($USER->getTemplate() != "default") 
                $rtpl->loadTemplatefile($USER->getTemplate()."/report.tmpl.htm");
            else 
                $rtpl->loadTemplatefile("report.tmpl.htm");
            if (isset($_REQUEST['createprintversion']) || @$_REQUEST["createxlsversion"]) {
                $rtpl->setCurrentBlock("print_head");
                $rtpl->setVariable("PRINT_BASE", getcfg('http_base'));
                $rtpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
                $rtpl->parse("print_head");
            }
            $datefrom = $_REQUEST["datefrom"];
            $dateto = $_REQUEST["dateto"];
            //$period = $_REQUEST["year"]."-".$_REQUEST["month"];
            $rtpl->setVariable('CURRENT_PERIOD', $_REQUEST["datefrom"]." - ".$_REQUEST["dateto"]);
            $sc = adm_sc_plugin::getSC($_REQUEST["sc_id"]);
            $rtpl->setVariable("SC_NAME", $_REQUEST["sc_id"] ? $sc['title'] : "Все");
            $doneStatus = adm_statuses_plugin::getStatusByTag("done", "services");
            $countStatus = adm_statuses_plugin::getStatusByTag("report", "services");
            $completeStatus = adm_statuses_plugin::getStatusByTag("closed", "services");
            $otkaz = adm_statuses_plugin::getStatusByTag("otkaz", "services");
            $opotkaz = adm_statuses_plugin::getStatusByTag("opotkaz", "services");
            $accepted = adm_statuses_plugin::getStatusByTag("acc_accepted", "services");
            $contrList = $cntr->getListSerNal();
            foreach($contrList as $key=>$value) {
                if (in_array($key, $_POST["cnt_id"])) {
                    $contrTitlesinReport[] = $value;
                }
            }
            $rtpl->setVariable("CONTR_NAMES", implode(", ", $contrTitlesinReport));
            $rcount = round((strtotime($dateto)-strtotime($datefrom))/60/60/24);
            $datefrom = date("Y-m-d", strtotime($datefrom));
            $dateto = date("Y-m-d", strtotime($dateto));
            //echo $datefrom." ".$dateto." ";
            //die($rcount);
            $rows = false;
            $total = 0;
            $totalincb = 0;
            $sc_id = $_REQUEST["sc_id"];
            if ($sc_id) {
                $add_filter = " AND t.sc_id=".$DB->F($sc_id)." ";
            }
            $add_filter .= count($_REQUEST["cnt_id"]) ? " AND t.cnt_id IN (".implode(",", $_REQUEST["cnt_id"]).")" : " ";
            for ($i=0; $i<$rcount; $i+=1) {
                $sql_inc = "SELECT  SUM(t.orient_price) as report 
                    FROM `task_comments` AS tc 
                    LEFT JOIN `tickets` AS t ON t.task_id=tc.task_id 
                    LEFT JOIN `tasks` AS task ON task.id=tc.task_id 
                    WHERE (tc.status_id=".$DB->F($countStatus["id"]).") AND DATE_FORMAT(tc.datetime, '%Y-%m-%d')=".$DB->F(date("Y-m-d", strtotime($datefrom." + $i days")))." $add_filter;";
                $t_report = $DB->getField($sql_inc);
                //die($sql_inc);
                $sql_rep = "SELECT SUM(sm.ammount)
                    FROM `task_comments` AS tc 
                    LEFT JOIN `tickets` AS t ON t.task_id=tc.task_id 
                    LEFT JOIN `skp_money` AS sm ON sm.task_id=t.task_id
                    WHERE sm.last AND (tc.status_id=".$DB->F($accepted["id"]).") $add_filter AND DATE_FORMAT(tc.datetime, '%Y-%m-%d')=".$DB->F(date("Y-m-d", strtotime($datefrom." + $i days"))).";";
                //die($sql_rep);
                $r_report = $DB->getField($sql_rep);
                
                
                if ($t_report || $r_report) {
                    $date = '';
                    $reported = 0;
                    $incash = 0;
                    $notincash = '';
                    $date = date("d.m.Y", strtotime($datefrom." + $i days"));
                     
                         
                    $reported += $t_report;
                    $incash += $r_report;
                                
                     
                    if ($reported > 0 || $incash > 0) {
                        $rows = true;
                        $total += $reported;
                        $totalincb += $incash;
                        $rtpl->setCurrentBlock("rep_row");
                        $rtpl->setVariable("RDATE", $date);
                        $rtpl->setVariable("INC", number_format($reported, 2, ",", " "));
                        $rtpl->setVariable("CASHBOX", number_format($incash, 2, ",", " "));
                        $rtpl->setVariable("NOTINCASHBOX", ($reported-$incash)>=0 ? number_format(($reported-$incash), 2, ",", " ") : "<font color='#FF0000'>".number_format(($reported-$incash), 2, ",", " ")."</font>");
                        $rtpl->parse("rep_row");
                    }
                }
                $DB->free();
                
            }
            if (!$rows) {
                $rtpl->touchBlock("no-rows ");
            } else {
                $rtpl->setCurrentBlock("rep-total");
                $rtpl->setVariable("INC_TOTAL", number_format($total, 2, ",", " "));
                $rtpl->setVariable("CASHBOX_TOTAL", number_format($totalincb, 2, ",", " "));
                $rtpl->setVariable("NOTINCASHBOX_TOTAL", ($total-$totalincb)>0 ? number_format(($total-$totalincb), 2, ",", " ") : "<font color='#FF0000'>".number_format(($total-$totalincb), 2, ",", " ")."</font>");
                $rtpl->parse("rep-total");
            }
            $rtpl->setVariable("REP_CDATE", date("d.m.Y"));
            $rtpl->setVariable("REP_AUTHOR", $USER->getFio());
            if (isset($_REQUEST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");  
                header("Content-disposition: attachment; filename=report_skp_total__".$_REQUEST["month"]."-".$_REQUEST["year"].".xls");  
                $rtpl->touchBlock("print_footer");
                $rtpl->show();
                return;
            } else {
                if (isset($_REQUEST['createprintversion'])) {
                    $rtpl->touchBlock("print_footer");
                    $rtpl->show();
                    return;
                } else {
                    $tpl->setCurrentBlock("reportval");
                    $tpl->setVariable("REPORT_HERE", $rtpl->getTpl());
                    $tpl->parse("reportval");
                }
            }
        } else {
            $tpl->touchBlock("notcreated");
        }
        $tpl->show();
        
    }
 
    
}
?>