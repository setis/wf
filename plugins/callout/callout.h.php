<?php

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Набор номера из WF";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['events']['dispcalltech'] = "Голосовой звонок (техник)";
$PLUGINS[$plugin_uid]['events']['dispcalltech1'] = "Голосовой звонок (техник)";
$PLUGINS[$plugin_uid]['events']['dispcalltech_from_rnav'] = "Голосовой звонок (техник) - с нав. панели";


if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['dispcall'] = "Голосовой звонок (диспетчер)";
}

$plugin_uid = "hidetelnum";

$PLUGINS[$plugin_uid]['name'] = "Скрытие номера телефона клиента";
$PLUGINS[$plugin_uid]['hidden'] = true;
