<?php
use classes\Plugin;
use classes\Task;
use classes\tickets\ServiceTicket;

class callout_plugin extends Plugin
{
    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function dispcall()
    {
        /**
         * @global \classes\UserSession $USER
         * @global \classes\DBMySQL $DB
         */
        global $DB, $USER;
        $ret = [];
        $source = $USER->getIntPhone();
        $phone = $_REQUEST["destination"];

        $task_id = $_POST["task_id"];
        $phone = $this->process_number($task_id, $phone);

        if ($task_id) {
            $t = new Task($task_id);
            $plugin_uid = $t->getPluginUid();
            $t->addComment("Звонок с внутреннего номера.", "Звонок с номера: " . $source . " на номер: " . $phone);
            $ret["tpl"] = $t->getLastComment();
        }

        if ((strlen($phone) == 3 || strlen($phone) == 4) && preg_match("/([0-9])+/", $phone)) {
            $phone_clean = $phone;
        } else {
            $phone_clean = preg_replace("/[^0-9]/", "", $phone);

            if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
            {
                $phone_clean = substr($phone_clean, 1, 10);
            } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
            {
                $phone_clean = substr($phone_clean, 1);
            } elseif (strlen($phone_clean) == 11 && preg_match("/^8[0-9]{10}$/", $phone_clean)) // 8903...
            {
                $phone_clean = substr($phone_clean, 1);
            } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
            {
                $phone_clean = $phone_clean;
            } elseif (strlen($phone_clean) == 11 && preg_match("/^7[0-9]{10}$/", $phone_clean)) // 7...
            {
                $phone_clean = substr($phone_clean, 1, 10);
            } elseif (strlen($phone_clean) == 10 && preg_match("/^49[0-9]{8}$/", $phone_clean)) // 49....
            {
                $phone_clean = $phone_clean;
            } else {
                $phone_clean = false;
            }
        }

        if ($source > 0 && $phone_clean != "") {
            if ($USER->isDispatcher() || (time() - intval($this->getSession()->get("outcalls_" . $USER->getId() . "_lastcall", 0))) > 20) {
                $this->getSession()->set("outcalls_" . $USER->getId() . "_lastcall", time());
                $call = new \models\TicketsCalls();
                $call->setTaskId($task_id)
                    ->setPluginUid($plugin_uid)
                    ->setUserId($USER->getId())
                    ->setCallfrom($source)
                    ->setCallto($phone_clean)
                    ->setCalldate(new DateTime())
                    ->setStatus(0)
                    ->setTechphone($phone_clean1);

                $this->getEm()->persist($call);
                $this->getEm()->flush();

                $url = getcfg('asterisk_host') . "extcall.php?source=" . $source . "&dest=" . $phone_clean ."&key=".$call->getId();
                ob_start();
                $cURL = curl_init();

                curl_setopt($cURL, CURLOPT_URL, $url);
                curl_setopt($cURL, CURLOPT_HTTPGET, true);
                curl_setopt($cURL, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Accept: application/json',
                ]);
                $result = @curl_exec($cURL);
                @curl_close($cURL);
                $r1 = ob_get_contents();

                ob_end_clean();

                $r1 = json_decode($r1, true);
                $r1["tpl"] = $ret["tpl"];
                echo json_encode($r1);

                return false;
            } else {
                $err[] = "Вы не можете набирать номер чаще чем 1 раз в 20 сек. Пожалуйста, повторите попытку чуть позже.";
            }
        } else {
            if (!$source) {
                $err[] = "Отсутствует внутренний номер. Укажите его в настройках учетной записи пользователя.";
            }

        }
        if (sizeof($err)) {
            $ret["error"] = implode(" \r\n", $err);
        } else $ret["ok"] = "ok";
        echo json_encode($ret);

        return false;
    }

    function dispcalltech()
    {
        global $DB, $USER;
        $ret = [];

        $source = $USER->getIntPhone();
        $phone = $_REQUEST["destination"];
        $task_id = $_POST["task_id"];
        //// EF: Для звонка при скрытом номере необходимо вычислить настоящий номер телефона
        $phone = $this->process_number($task_id, $phone);

        if ($task_id) {
            $t = new Task($task_id);
            $plugin_uid = $t->getPluginUid();
            $t->addComment("Звонок с виртуального номера.", "Звонок с номера: " . $source . ".");
            $ret["tpl"] = $t->getLastComment();
        }

        $targetTechPhone = $USER->getPhones();
        if ((strlen($phone) == 3 || strlen($phone) == 4) && preg_match("/([0-9])+/", $phone)) {
            $phone_clean = $phone;
        } else {

            $phone_clean = preg_replace("/[^0-9]/", "", $phone);

            if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
            {
                $phone_clean = substr($phone_clean, 1, 10);
            } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
            {
                $phone_clean = substr($phone_clean, 1);
            } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
            {
                $phone_clean = $phone_clean;
            } elseif (strlen($phone_clean) == 11 && preg_match("/^7[0-9]{10}$/", $phone_clean)) // 7...
            {
                $phone_clean = substr($phone_clean, 1, 10);
            } elseif (strlen($phone_clean) == 10 && preg_match("/^49[0-9]{8}$/", $phone_clean)) // 49....
            {
                $phone_clean = $phone_clean;
            } else {
                $phone_clean = false;
            }
        }
        $phone_clean1 = preg_replace("/[^0-9]/", "", $targetTechPhone);

        if (strlen($phone_clean1) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean1)) // 7903...
        {
            $phone_clean1 = substr($phone_clean1, 1, 10);
        } elseif (strlen($phone_clean1) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean1)) // 8903...
        {
            $phone_clean1 = substr($phone_clean1, 1);
        } elseif (strlen($phone_clean1) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean1)) // 903...
        {
            $phone_clean1 = $phone_clean1;
        } elseif (strlen($phone_clean1) == 11 && preg_match("/^7[0-9]{10}$/", $phone_clean1)) // 7...
        {
            $phone_clean1 = substr($phone_clean1, 1, 10);
        } elseif (strlen($phone_clean1) == 10 && preg_match("/^49[0-9]{8}$/", $phone_clean1)) // 49....
        {
            $phone_clean1 = $phone_clean1;
        } else {
            $phone_clean1 = false;
        }


        if ($source > 0 && $phone_clean != "") {
            if ((time() - intval($this->getSession()->get("outcalls_" . $USER->getId() . "_lastcall", 0))) < 20) {
                $err[] = "Вы не можете набирать номер чаще чем 1 раз в 20 сек. Пожалуйста, повторите попытку чуть позже.";
            } else {
                $this->getSession()->set("outcalls_" . $USER->getId() . "_lastcall", time());
                $call = new \models\TicketsCalls();
                $call->setTaskId($task_id)
                    ->setPluginUid($plugin_uid)
                    ->setUserId($USER->getId())
                    ->setCallfrom($source)
                    ->setCallto($phone_clean)
                    ->setCalldate(new DateTime())
                    ->setStatus(0)
                    ->setTechphone($phone_clean1);

                $this->getEm()->persist($call);
                $this->getEm()->flush();

                $url = getcfg('asterisk_host') . "extcall.php?source=" . $source . "&dest=" . $phone_clean ."&key=".$call->getId();
                ob_start();
                $cURL = curl_init();

                curl_setopt($cURL, CURLOPT_URL, $url);
                curl_setopt($cURL, CURLOPT_HTTPGET, true);
                curl_setopt($cURL, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Accept: application/json',
                ]);
                $result = @curl_exec($cURL);
                @curl_close($cURL);
                $r1 = ob_get_contents();
                //@json_decode($result);
                ob_end_clean();


                //echo $r1;
                $r1 = json_decode($r1, true);
                $r1["tpl"] = $ret["tpl"];

                return new \Symfony\Component\HttpFoundation\Response( json_encode($r1) );
            }
        } else {
            if (!$source) {
                $err[] = "Отсутствует внутренний номер. Укажите его в настройках учетной записи пользователя.";
            }
            if ($dest == "") {
                $err[] = "Не указан номер телефона для звонка по внешней линии.";
            }

        }
        if (sizeof($err)) {
            $ret["error"] = implode(" \r\n", $err);
        } else $ret["ok"] = "ok";
        echo json_encode($ret);

        return false;
    }

    ///// EF: функция, обрабатывающая "скрытый" номер
    function process_number($task_id, $phone)
    {
        global $DB, $USER;
        if ($USER->checkAccess("hidetelnum")) {
            $ticket = new ServiceTicket($task_id);
            $real_phones = $ticket->getRealPhones();
            #error_log("real_phones = [".print_r($real_phones,true)."]\n", 3, "/ramdisk/tmp.log");
            if ($phone == "00000001") {
                $phone = $real_phones[0];
            }
            if ($phone == "00000002") {
                $phone = $real_phones[1];
            }
        }

        return $phone;
    }

    function dispcalltech_from_rnav()
    {
        global $DB, $USER;
        $ret = [];
        if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPod')) {
            $intphone = $USER->getId();
        } else {
            if (strstr($_SERVER['HTTP_USER_AGENT'], 'Android')) {
                $intphone = $USER->getId();
            }
        }

        $source = $intphone ? $intphone : $USER->getIntPhone();
        $phone = $_REQUEST["destination"];


        $task_id = $_POST["task_id"];
        if ($task_id) {
            $t = new Task($task_id);
            $plugin_uid = $t->getPluginUid();
            $t->addComment("Звонок с виртуального номера.", "Звонок с номера: " . $source . ".");
            $ret["tpl"] = $t->getLastComment();
            $phone = $this->process_number($task_id, $phone);
        }

        $targetTechPhone = $USER->getPhones();
        if ((strlen($phone) == 3 || strlen($phone) == 4) && preg_match("/([0-9])+/", $phone)) {
            $phone_clean = $phone;
        } else {

            $phone_clean = preg_replace("/[^0-9]/", "", $phone);

            if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
            {
                $phone_clean = substr($phone_clean, 1, 10);
            } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
            {
                $phone_clean = substr($phone_clean, 1);
            } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
            {
                $phone_clean = $phone_clean;
            } elseif (strlen($phone_clean) == 11 && preg_match("/^7[0-9]{10}$/", $phone_clean)) // 7...
            {
                $phone_clean = substr($phone_clean, 1, 10);
            } elseif (strlen($phone_clean) == 10 && preg_match("/^49[0-9]{8}$/", $phone_clean)) // 49....
            {
                $phone_clean = $phone_clean;
            } else {
                $phone_clean = false;
            }
        }
        $phone_clean1 = preg_replace("/[^0-9]/", "", $targetTechPhone);

        if (strlen($phone_clean1) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean1)) // 7903...
        {
            $phone_clean1 = substr($phone_clean1, 1, 10);
        } elseif (strlen($phone_clean1) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean1)) // 8903...
        {
            $phone_clean1 = substr($phone_clean1, 1);
        } elseif (strlen($phone_clean1) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean1)) // 903...
        {
            $phone_clean1 = $phone_clean1;
        } elseif (strlen($phone_clean1) == 11 && preg_match("/^7[0-9]{10}$/", $phone_clean1)) // 7...
        {
            $phone_clean1 = substr($phone_clean1, 1, 10);
        } elseif (strlen($phone_clean1) == 10 && preg_match("/^49[0-9]{8}$/", $phone_clean1)) // 49....
        {
            $phone_clean1 = $phone_clean1;
        } else {
            $phone_clean1 = false;
        }

        if ($source > 0 && $phone_clean != "") {
            if ((time() - intval($this->getSession()->get("outcalls_" . $USER->getId() . "_lastcall", 0))) < 20) {
                $err[] = "Вы не можете набирать номер чаще чем 1 раз в 20 сек. Пожалуйста, повторите попытку чуть позже.";
            } else {
                $this->getSession()->set("outcalls_" . $USER->getId() . "_lastcall", time());
                $call = new \models\TicketsCalls();
                $call->setTaskId($task_id)
                    ->setPluginUid($plugin_uid)
                    ->setUserId($USER->getId())
                    ->setCallfrom($source)
                    ->setCallto($phone_clean)
                    ->setCalldate(new DateTime())
                    ->setStatus(0)
                    ->setTechphone($phone_clean1);

                $this->getEm()->persist($call);
                $this->getEm()->flush();

                $url = getcfg('asterisk_host') . "extcall.php?source=" . $source . "&dest=" . $phone_clean ."&key=".$call->getId();
                ob_start();
                $cURL = curl_init();

                curl_setopt($cURL, CURLOPT_URL, $url);
                curl_setopt($cURL, CURLOPT_HTTPGET, true);
                curl_setopt($cURL, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Accept: application/json',
                ]);
                $result = @curl_exec($cURL);
                @curl_close($cURL);
                $r1 = ob_get_contents();
                //@json_decode($result);
                ob_end_clean();

                $r1 = json_decode($r1, true);
                $r1["tpl"] = $ret["tpl"];
                echo json_encode($r1);
                exit();

                return false;
            }
        } else {
            if (!$source) {
                $err[] = "Отсутствует внутренний номер. Укажите его в настройках учетной записи пользователя.";
            }
            if ($dest == "") {
                $err[] = "Не указан номер телефона для звонка по внешней линии.";
            }

        }
        if (sizeof($err)) {
            $ret["error"] = implode(" \r\n", $err);
        } else $ret["ok"] = "ok";
        echo json_encode($ret);

        return false;
    }

    function dispcalltech1()
    {
        global $DB, $USER;
        $ret = [];
        $source = $_REQUEST["callfrom"];
        $phone = $_REQUEST["destination"];

        $task_id = $_POST["task_id"];
        //// EF: Для звонка при скрытом номере необходимо вычислить настоящий номер телефона
        $phone = $this->process_number($task_id, $phone);
        if ($task_id) {
            $t = new Task($task_id);
            $plugin_uid = $t->getPluginUid();
            $t->addComment("Звонок с виртуального номера.", "Звонок с номера: " . $source . ".");
            $ret["tpl"] = $t->getLastComment();
        } else {
            throw new RuntimeException('Can\'t make call!');
        }

        $targetTechPhone = $DB->getField("SELECT `phones` FROM `users` WHERE `id`=" . $DB->F($source) . ";");

        if ((strlen($phone) == 3 || strlen($phone) == 4) && preg_match("/([0-9])+/", $phone)) {
            $phone_clean = $phone;
        } else {
            $phone_clean = preg_replace("/[^0-9]/", "", $phone);

            if (strlen($phone_clean) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean)) // 7903...
            {
                $phone_clean = substr($phone_clean, 1, 10);
            } elseif (strlen($phone_clean) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean)) // 8903...
            {
                $phone_clean = substr($phone_clean, 1);
            } elseif (strlen($phone_clean) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean)) // 903...
            {
                $phone_clean = $phone_clean;
            } elseif (strlen($phone_clean) == 11 && preg_match("/^7[0-9]{10}$/", $phone_clean)) // 7...
            {
                $phone_clean = substr($phone_clean, 1, 10);
            } elseif (strlen($phone_clean) == 10 && preg_match("/^49[0-9]{8}$/", $phone_clean)) // 49....
            {
                $phone_clean = $phone_clean;
            } else {
                $phone_clean = false;
            }
        }
        $phone_clean1 = preg_replace("/[^0-9]/", "", $targetTechPhone);

        if (strlen($phone_clean1) == 11 && preg_match("/^79[0-9]{9}$/", $phone_clean1)) // 7903...
        {
            $phone_clean1 = substr($phone_clean1, 1, 10);
        } elseif (strlen($phone_clean1) == 11 && preg_match("/^89[0-9]{9}$/", $phone_clean1)) // 8903...
        {
            $phone_clean1 = substr($phone_clean1, 1);
        } elseif (strlen($phone_clean1) == 10 && preg_match("/^9[0-9]{9}$/", $phone_clean1)) // 903...
        {
            $phone_clean1 = $phone_clean1;
        } elseif (strlen($phone_clean1) == 11 && preg_match("/^7[0-9]{10}$/", $phone_clean1)) // 7...
        {
            $phone_clean1 = substr($phone_clean1, 1, 10);
        } elseif (strlen($phone_clean1) == 10 && preg_match("/^49[0-9]{8}$/", $phone_clean1)) // 49....
        {
            $phone_clean1 = $phone_clean1;
        } else {
            $phone_clean1 = false;
        }


        if ($source > 0 && $phone_clean != "") {
            if ((time() - intval($this->getSession()->get("outcalls_" . $USER->getId() . "_lastcall", 0))) < 20) {
                $err[] = "Вы не можете набирать номер чаще чем 1 раз в 20 сек. Пожалуйста, повторите попытку чуть позже.";
            } else {
                $this->getSession()->set("outcalls_" . $USER->getId() . "_lastcall", time());
                $call = new \models\TicketsCalls();
                $call->setTaskId($task_id)
                    ->setPluginUid($plugin_uid)
                    ->setUserId($USER->getId())
                    ->setCallfrom($source)
                    ->setCallto($phone_clean)
                    ->setCalldate(new DateTime())
                    ->setStatus(0)
                    ->setTechphone($phone_clean1);

                $this->getEm()->persist($call);
                $this->getEm()->flush();

                $url = getcfg('asterisk_host') . "extcall.php?source=" . $source . "&dest=" . $phone_clean ."&key=".$call->getId();
                ob_start();
                $cURL = curl_init();

                curl_setopt($cURL, CURLOPT_URL, $url);
                curl_setopt($cURL, CURLOPT_HTTPGET, true);
                curl_setopt($cURL, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Accept: application/json',
                ]);
                $result = @curl_exec($cURL);
                @curl_close($cURL);
                $r1 = ob_get_contents();
                //@json_decode($result);
                ob_end_clean();


                //echo $r1;
                $r1 = json_decode($r1, true);
                $r1["tpl"] = $ret["tpl"];
                echo json_encode($r1);
                exit();

                return false;
            }
        } else {
            if (!$source) {
                $err[] = "Отсутствует внутренний номер. Укажите его в настройках учетной записи пользователя.";
            }
            if ($dest == "") {
                $err[] = "Не указан номер телефона для звонка по внешней линии.";
            }

        }
        if (sizeof($err)) {
            $ret["error"] = implode(" \r\n", $err);
        } else $ret["ok"] = "ok";
        echo json_encode($ret);

        return false;
    }

}
