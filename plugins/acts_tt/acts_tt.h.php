<?php
use classes\User;

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Акты Заявок (ТТ)";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';

if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Акты Заявок (ТТ)";
    $PLUGINS[$plugin_uid]['events']['actarchive'] = "Архив актов Заявок (ТТ)";
    $PLUGINS[$plugin_uid]['events']['viewact'] = "Просмотр акта Заявок (ТТ)";
}
if(wf::$user->checkAccess($plugin_uid, User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['createact'] = "Создание акта Заявок";
    $PLUGINS[$plugin_uid]['events']['deleteact'] = "Удаление акта Заявок (ТТ)";
    $PLUGINS[$plugin_uid]['events']['setsent'] = "Изменение статуса актов Заявок (ТТ)";
    $PLUGINS[$plugin_uid]['events']['removefromact'] = "Удаление заявок из актов (ТТ)";
    $PLUGINS[$plugin_uid]['events']['savenewpin_ajax'] = "Редактирование ПИН-Кода (ТТ)";
    $PLUGINS[$plugin_uid]['events']['saveacttitle_ajax'] = "Редактирование названия акта (ТТ)";
    $PLUGINS[$plugin_uid]['events']['savenewcnumval_ajax'] = "Редактирование номера заявки в 1С (ТТ)";
    
}
?>
