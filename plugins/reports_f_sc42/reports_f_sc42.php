<?php

/**
 * Plugin Implementation
 * @author kblp
 */
use classes\HTML_Template_IT;
use classes\Plugin;

set_time_limit(0); // 200 minutes
ini_set('max_execution_time', 0); // 200 minutes

/**
 * Class reports_f_sc41_plugin
 */
class reports_f_sc42_plugin extends Plugin
{

    function __construct()
    {
        parent::__construct($plugin_uid = basename(__FILE__, '.php'));
    }

    function archive()
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile("archive.tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/archive.tmpl.htm");
        else
            $tpl->loadTemplatefile("archive.tmpl.htm");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        UIHeader($tpl);
        report_left_menu($tpl);
        $tpl->setVariable("REPORT__FIN_SC", "buttonsel1");
        $sql = "SELECT count(id) FROM `list_f_sc4_reports` WHERE 1 ORDER BY `cr_date` DESC";
        $_REQUEST['filter_start'] = intval($_REQUEST['filter_start']);
        $total = $DB->getField($sql);
        $sql = "SELECT * FROM `list_f_sc4_reports` WHERE 1 ORDER BY `cr_date` DESC LIMIT " . $_REQUEST['filter_start'] . "," . getcfg('rows_per_page');
        $DB->query($sql);

        if ($DB->errno()) UIError($DB->error());
        if ($DB->num_rows()) {
            while ($res = $DB->fetch(true)) {
                $uid = adm_users_plugin::getUser($res["cr_uid"]);
                $tpl->setCurrentBlock("row");
                $tpl->setVariable("RR_ID", $res["id"]);
                $tpl->setVariable("RR_PERIOD", base64_decode($res["period"]));
                $tpl->setVariable("RR_CRDATE", $res["cr_date"]);
                $tpl->setVariable("RR_AUTHOR", $uid["fio"]);
                $tpl->parse("row");
            }
            $DB->free();
        } else {
            $tpl->touchBlock("norows");
        }
        $tpl->setVariable('TOTAL', $total);
        $tpl->setVariable('PAGES', pages($_REQUEST['filter_start'], getcfg('rows_per_page'), intval($total), "reports_f_sc/archive#start-%s"));
        $tpl->show();
    }


    function viewreport()
    {
        global $DB, $USER;
        setlocale(LC_NUMERIC, 'et_EE.UTF-8');

        if ($id = $_REQUEST["id"]) {
            $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
            //$tpl->loadTemplatefile("viewarciveitem.tmpl.htm");
            if ($USER->getTemplate() != "default")
                $tpl->loadTemplatefile($USER->getTemplate() . "/viewarciveitem.tmpl.htm");
            else
                $tpl->loadTemplatefile("viewarciveitem.tmpl.htm");
            $tpl->setVariable("USER_TEMPLATE", $USER->getTemplate());
            $tpl->setVariable('PLUGIN_UID', $this->getUID());
            $sql = "SELECT `body` FROM `list_f_sc4_reports` WHERE `id`=" . $DB->F($id) . ";";
            $body = $DB->getField($sql);
            if ($DB->errno()) UIError($DB->error());
            if ($body) {
                $tpl->setVariable("REPORT_HERE", base64_decode($body));
                $tpl->show();
            } else {
                UIError("Ошибка БД!");
            }
        } else {
            UIError("Не указан ижентификатор отчета!");
        }
    }

    function deletereport()
    {
        global $DB, $USER;
        if ($id = $_REQUEST["id"]) {
            $sql = "DELETE FROM `list_f_sc4_reports` WHERE `id`=" . $DB->F($id) . ";";
            $DB->query($sql);
            if ($DB->errno()) UIError($DB->error());
            redirect($this->getLink('archive'), "Отчет успешно удален из Архива!");
        } else {
            UIError("Не указан ижентификатор отчета!");
        }
    }

    /**
     * я хз но поле workflow.tickets.findatetech в текущей версии отчёта не используется
     */
    function main()
    {
        global $DB, $USER;
        $return = '';
        $year = range(2013, date('Y'));
        $year = array_combine($year, $year);

        $month = array('01' => "Январь", '02' => "Февраль", '03' => "Март", '04' => "Апрель", '05' => "Май", '06' => "Июнь", '07' => "Июль", '08' => "Август", '09' => "Сентябрь", '10' => "Октябрь", '11' => "Ноябрь", '12' => "Декабрь");

        $tpl = $this->showForm($month, $year);

        if (@$_POST["createreport"] || @$_POST["createprintversion"] || @$_POST["createxlsversion"] || @$_POST["setflag"] || @$_POST["createarchiveitem"]) {

            if (isset($_POST["createxlsversion"])) {
                header("Content-type: application/vnd.ms-excel");
                header("Content-disposition: attachment; filename=report_fin_sc_" . $_POST['month'] . "-" . $_POST['year'] . ".xls");

                $html = $this->compileReport();
                $this->layout = 'xls';
                $return = $this->render('twozero/wrap', [
                    'content'=>$html
                ]);
            } else {
                $html = $this->compileReport();
                $this->layout = 'fullscreen';
                $return = $this->render('twozero/wrap', [
                    'content'=>$html
                ]);
            }
        } else {
            $tpl->show();
        }

        echo $return;
    }

    private function compileReport()
    {
        $this->layout = null;
        $start = time();
        $period = $_POST["year"] . "-" . $_POST["month"];

        $report = new \classes\reports42\ReportAllScPeriod($period);

        $value = $this->render('twozero\report', array(
            'report' => $report,
            'period' => $period,
            'start' => $start,
        ));

        return $value;
    }

    /**
     * вывод первичной формы
     * @param $month
     * @param $year
     * @param $fintypes
     * @return HTML_Template_IT
     */
    private function showForm($month, $year)
    {
        global $DB, $USER;
        $tpl = new HTML_Template_IT(path2("plugins/" . $this->getUID()));
        //$tpl->loadTemplatefile($this->getUID().".tmpl.htm");
        if ($USER->getTemplate() != "default")
            $tpl->loadTemplatefile($USER->getTemplate() . "/" . $this->getUID() . ".tmpl.htm");
        else
            $tpl->loadTemplatefile($this->getUID() . ".tmpl.htm");
        $tpl->setVariable("REPORT__FIN_SC", "buttonsel1");
        $tpl->setVariable('PLUGIN_UID', $this->getUID());
        $tpl->setVariable("MONTH", array2options($month, isset($_POST["month"]) ? $_POST["month"] : date('m', strtotime('-1 month'))));
        $tpl->setVariable("YEAR", array2options($year, isset($_POST["year"]) ? $_POST["year"] : date("Y", strtotime('-1 month'))));
        UIHeader($tpl);
        report_left_menu($tpl);
        $tpl->setVariable("DATE_FROM", @$_POST['datefrom'] ? @$_POST['datefrom'] : date("d.m.Y"));
        $tpl->setVariable("DATE_TO", @$_POST['datefrom'] ? @$_POST['dateto'] : date("d.m.Y"));
        $tpl->setVariable("FILTER_TECH_OPTIONS", adm_empl_plugin::getTechList($_POST["tech_id"]));
        return $tpl;
    }
}
