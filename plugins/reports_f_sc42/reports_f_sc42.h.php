<?php

/**
 * Plugin Header
 *
 * @author kblp
 * @copyright 2015
 */

$plugin_uid = basename(__FILE__, ".h.php");

$PLUGINS[$plugin_uid]['name'] = "Расчетный лист по Сервисным Центрам v.4.2 (Подключение, ТТ, СКП)";
$PLUGINS[$plugin_uid]['hidden'] = true;
$PLUGINS[$plugin_uid]['menuparent'] = 'reports';



if(wf::$user->checkAccess($plugin_uid)) {
    $PLUGINS[$plugin_uid]['events']['main'] = "Расчетный лист по Сервисным Центрам v.4.2 (Подключение, ТТ, СКП)";
    $PLUGINS[$plugin_uid]['events']['archive'] = "Архив Расчетных листов по Сервисным Центрам (Подключение, ТТ, СКП)";
    $PLUGINS[$plugin_uid]['events']['viewreport'] = "Просмотр Архивного Расчетного листа по Сервисным Центрам (Подключение, ТТ, СКП)";
}
if(wf::$user->checkAccess($plugin_uid, \classes\User::ACCESS_WRITE)) {
    $PLUGINS[$plugin_uid]['events']['deletereport'] = "Удаление отчета из Архива";
}
?>
