<link rel="stylesheet" type="text/css" href="/templates/twozero/css/style_bs.css"/>
<link rel="stylesheet" type="text/css" href="/templates/twozero/css/bootstrap.min.css"/>
<!--[if lte IE 6]>
<link rel="stylesheet" type="text/css" href="/templates/twozero/css/ie.css">
<![endif]-->

<style type="text/css">
    @media (min-width: 1200px) and (max-width: 1400px) {
        td.tf-hidden, th.tf-hidden {
            display: none !important;
        }

        td.tf-visible, th.tf-visible {
            display: table-cell !important;
        }
    }

    #content ul {
        list-style-type: none;
    }

    a.button {
        display: inline-block;
        line-height: 100%;
        width: 180px;
        margin: 0.3em;
    }

    button {
        line-height: 2;
    }

    textarea[name=comment] {
        width: 300px;
        height: 100px;
    }

    .nbncheck {

        font-weight: bold;
        font-size: 10pt;
        margin-right: 20px;
        color: #FF0000;
        cursor: pointer;
    }

    #toTop {
        cursor: pointer;
        display: none;
        position: fixed;
        right: 1px !important;
        width: 100px;
        height: 10;
        float: right !important;
        padding: 10px;
        text-align: center;
        line-height: 12px;
        bottom: 50px;
    }

    #header {
        -webkit-backface-visibility: hidden;
    }


</style>

<link rel="stylesheet" type="text/css" href="/templates/twozero/css/lightbox.css"/>
