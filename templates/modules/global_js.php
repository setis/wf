<script type="text/javascript" src="/legacy_js/jquery.js"></script>
<script type="text/javascript" src="/legacy_js/bootstrap.min.js"></script>
<script type="text/javascript" src="/legacy_js/default.js"></script>

<script type="text/javascript" src="/legacy_js/selectors.js"></script>
<script type="text/javascript" src="/legacy_js/obzvon.js"></script>
<script type="text/javascript" src="/legacy_js/lightbox.js"></script>

<script type="text/javascript">
    var cCnt = 0;
    var drag = false;
    var offset = 0;
    $(document).ready(function () {

        $(document).on("keydown", "input[name='object_id']", function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 65, 86, 90, 110, 118]) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on("mouseup", function () {
            drag = false;
            $("body").removeClass('noselect');
            $(".rightbar").css("background-color", '#FFFFFF');
        });


        $("#movepanel").mousedown(function (e) {
            $("body").addClass('noselect');
            drag = true;
            offset = Math.abs(e.pageY - $(".rightbar").offset().top - $(document).scrollTop());
            //alert($(document).scrollTop() + " " + e.pageY + " " + offset);
            $(".rightbar").css("background-color", '#FFF700');

        });

        $(document).on("click", "#openobject", function () {
            $("input[name=object_id]").val('');
            $("#openobject_dlg").modal("show");
        });

        $("#movepanel").on("mouseup", function () {
            drag = false;
            $("body").removeClass('noselect');
            var obj = $(".rightbar");
            setTimeout(function () {
                if (obj.offset().top > $(window).height()) {
                    obj.css("top", $(window).height() - (obj.height() + 40));
                } else {
                    if (obj.offset().top < 0) {
                        obj.css("top", 50);
                    }
                }
            }, 10);
        });


        $(document).on("mousemove", function (e) {
            if (drag) {
                $(document).off("click", "#openobject");
                if ((e.pageY - offset ) > $(window).height()) {
                    $(".rightbar").css("top", $(window).height() - $(".rightbar").height() - 7);
                } else {
                    if (e.pageY < 0) {
                        $(".rightbar").css("top", 50);
                    } else {
                        $(".rightbar").css("top", e.pageY - (offset));
                    }
                }

            }
        });

        $(document).on("click", "#openobject", function () {
            if (!drag) {
                $("input[name=object_id]").val('');
                $("#openobject_dlg").modal("show");
            }
        });

        $("#openobject_dlg").on("shown.bs.modal", function () {
            $("input[name=object_id]").focus();
        });

        //$("#content").removeClass("floats-container");
        $("#content").addClass("container-fluid");
        $('nav').on('hidden.bs.collapse', function () {
            onResize();

        });
    });

    var onResize = function () {
        $("#container-paddinger").css("padding-top", $(".navbar-fixed-top").height() + 10);
    };

    $(window).resize(onResize);
</script>

