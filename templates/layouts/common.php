<?php
/**
 * @var $this \classes\View
 * @var $content string
 */

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?php echo $this->getPageTitle(); ?></title>

    <meta name="viewport" content="width=device-width, maximum-scale=1.0, height=device-height, target-densityDpi=device-dpi, user-scalable=no"/>
    <meta name="MobileOptimized" content="320"/>
    <meta name="HandheldFriendly" content="true"/>
    <link rel="shortcut icon" href="/favicon.ico"/>

    <?php echo $this->renderPartial('//modules/global_js'); ?>
    <?php echo $this->renderPartial('//modules/global_css'); ?>
    <?php echo $this->header(); ?>
</head>
<body>
<?php echo $this->widget('MainMenuWidget', []); ?>

<div id="content" class="container">
    <?php echo $content; ?>
</div>

<?php
echo $this->widget('FooterWidget', []);
echo $this->widget('SelectMultipleUserWidget', []);
echo $this->widget('RightBarWidget', []);
echo $this->widget('ImpersonateWidget', []);
echo $this->widget('QualityControlWidget', []);
echo $this->widget('CallsListWidget', []);

?>
<?php echo $this->footer(); ?>
<script type="text/javascript">
    $(document).ready(onResize());
</script>
<div id="toTop" style="display: block; z-index: 10000 !important;"><img src="/templates/images/totop.png"/></div>
</body>
</html>

